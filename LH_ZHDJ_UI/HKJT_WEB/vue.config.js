const ispro = process.env.NODE_ENV == 'production';
const isdev = process.env.NODE_ENV == 'development';

 const url = 'http://10.1.50.244:9680'; //方辉
// const url = 'http://10.1.50.247:9680'; //李响
//const url = 'http://192.168.60.234:16799'; //测试环境
// const url = 'http://10.1.50.70:9680'; //测试环境

const TerserPlugin = require('terser-webpack-plugin');
const proxy = isdev
  ? {
      '/ots': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/ots': '/ots'
        }
      },
      '/eval': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/eval': '/eval'
        }
      },
      '/portal': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/portal': '/portal'
        }
      },
      '/duty': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/duty': '/duty'
        }
      },
      '/power': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/power': '/power'
        }
      },
      '/lhdj': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/lhdj': '/lhdj'
        }
      },
      '/document': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/document': '/document'
        }
      },
      '/evaluation': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/evaluation': '/evaluation'
        }
      },
      '/hecheng': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/hecheng': '/hecheng'
        }
      },
      '/lhweb': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/lhweb': '/lhweb'
        }
      },
      '/developing': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/developing': '/developing'
        }
      },
      '/horn': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/horn': '/horn'
        }
      },
      '/urc': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/urc': '/urc'
        }
      },
      '/schedule': {
        target: url,
        changeOrigin: true,
        pathRewrite: {
          '^/schedule': '/schedule'
        }
      }
    }
  : {};
const webpack = require('webpack');
require('events').EventEmitter.defaultMaxListeners = 0;
module.exports = {
  productionSourceMap: false,
  lintOnSave: false, // 是否开启eslint
  transpileDependencies: ['vue2-editor'], //  babel-loader 默认会跳过 node_modules 依赖
  devServer: {
    open: true, // 是否自动弹出浏览器页面
    // host: '10.1.50.245',
    port: '8080',
    https: false, // 是否使用https协议
    hotOnly: false, // 是否开启热更新
    proxy: proxy
  },
  chainWebpack: config => {
    config.entry.app = ['babel-polyfill', './src/main.js'];
    config.plugin('provide').use(webpack.ProvidePlugin, [
      {
        'window.Quill': 'quill/dist/quill.js',
        Quill: 'quill/dist/quill.js'
      }
    ]);
  },
  configureWebpack: config => {
    // config.externals = { AMap: 'AMap' };
    if (ispro) {
      // 将每个依赖包打包成单独的js文件
      config.optimization = {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 20000,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                return `npm.${packageName.replace('@', '')}`;
              }
            }
          }
        },
        minimizer: [
          new TerserPlugin({
            terserOptions: {
              compress: {
                warnings: false,
                drop_console: true, // console
                drop_debugger: true,
                pure_funcs: ['console.log'] // 移除cons
              }
            }
          })
        ]
      };
    }
  }
};
