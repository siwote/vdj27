import Vue from 'vue';
import Router from 'vue-router';
// import login from '@/view/login'
// import notData from '@/view/notData'
// const Router = require("vue-router")

Vue.use(Router);

//获取原型对象上的push函数
const originalReplace = Router.prototype.replace;
Router.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch(err => err);
};

// 解决投票路由问题
const routerPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error);
};

export default new Router({
  mode: 'history',
  scrollBehavior: function(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0
      };
    }
  },
  routes: [
    {
      name: 'login',
      path: '/login',
      component: resolve => require(['@/view/login'], resolve),
      hidden: true
    },
    {
      path: '/register',
      component: () => {
        return import('@/view/register/index.vue');
      },
      hidden: true
    },
    {
      path: '/newsUserCenter',
      component: () => {
        return import('@/view/newsUserCenter/index.vue');
      },
      hidden: true
    },
    {
      path: '/openLink',
      component: () => {
        return import('@/view/openLink/index.vue');
      },
      hidden: true
    },
    {
      path: '/emailAuth',
      component: () => {
        return import('@/view/emailAuth/index.vue');
      },
      hidden: true
    },
    {
      name: '*',
      path: '*',
      component: resolve => require(['@/view/notData'], resolve),
      hidden: true
    }
  ]
});
