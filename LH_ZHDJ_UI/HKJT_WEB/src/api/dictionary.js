import request from '@/utils/request'
import qs from 'qs'
const contextPath = '/data'

// 表信息查询接口
export function getDictionary(data) {
  return request({
    url: contextPath + '/TableAndColumn/tabledictionary',
    method: 'post',
    data: qs.stringify(data)
  })
}
// 列信息查询接口
export function getColumnDictionary(data) {
  return request({
    url: contextPath + '/TableAndColumn/columndictionary',
    method: 'post',
    data: qs.stringify(data)
  })
}
