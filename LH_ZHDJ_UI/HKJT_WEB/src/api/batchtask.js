import request from "@/utils/request";
import qs from "qs";
const loginPath = "/schedule";

const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

//新增定时任务
export function add(data) {
  return request({
    url: loginPath + "/manager/add",
    method: "post",
    headers: headers,
    data: data
  });
}

//删除任务
export function del(data) {
  return request({
    url: loginPath + "/manager/del",
    method: "post",
    data: qs.stringify(data)
  });
}

//定时任务列表
export function list(data) {
  return request({
    url: loginPath + "/manager/list",
    method: "post",
    data: qs.stringify(data)
  });
}

//暂停任务
export function pause(data) {
  return request({
    url: loginPath + "/manager/pause",
    method: "post",
    data: qs.stringify(data)
  });
}

//恢复任务
export function resume(data) {
  return request({
    url: loginPath + "/manager/resume",
    method: "post",
    data: qs.stringify(data)
  });
}

//更新任务信息
export function updateJob(data) {
  return request({
    url: loginPath + "/manager/updateJob",
    method: "post",
    headers: headers,
    data: data
  });
}
