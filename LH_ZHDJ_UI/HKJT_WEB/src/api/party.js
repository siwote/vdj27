import request from '@/utils/request';

import qs from 'qs';

const contextPath = '/lhdj';
const contextPatho = '/horn';
const headers = {
  post: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
};

/* hbl 党组织树接口*/
export function getPartyTree(data) {
  return request({
    url: contextPath + '/dzzorg/tree',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* hbl 党员列表*/
export function getPartyList(data) {
  return request({
    url: contextPath + '/dy/info',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党员基本信息查询
export function getPartyBaesInfo(data) {
  return request({
    url: contextPath + '/dy/essen',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党籍和组织机构
export function getPartyGroup(data) {
  return request({
    url: contextPath + '/dy/party',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党员职务
export function getPartyJob(data) {
  return request({
    url: contextPath + '/dy/post',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 出国出境
export function getPartyGoAbroad(data) {
  return request({
    url: contextPath + '/dy/cgcj',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 困难情况
export function getPartyDifficulty(data) {
  return request({
    url: contextPath + '/dy/knqk',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 奖惩
export function getPartyPrize(data) {
  return request({
    url: contextPath + '/dy/jcxx',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 入党信息
export function getJoinParty(data) {
  return request({
    url: contextPath + '/dy/rdxx',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党员流动
export function getPartyFlow(data) {
  return request({
    url: contextPath + '/dy/lddy',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 公共字典下拉框*/
export function getPartyItem(data) {
  return request({
    url: contextPath + '/dict/item',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 党组织列表查询*/
export function getPartyTable(data) {
  return request({
    url: contextPath + '/dzzorg/list',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 党组织标签页-基本信息*/
export function getPartyGroupBase(data) {
  return request({
    url: contextPath + '/dzzorg/base',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 党组织标签页-所在单位*/
export function getPartyGroupDwInfo(data) {
  return request({
    url: contextPath + '/dzzorg/dwInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 党组织标签页-换届信息*/
export function getPartyGroupHjxx(data) {
  return request({
    url: contextPath + '/dzzorg/hjxx',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 党组织标签页-班子成员*/
export function getPartyGroupBzcy(data) {
  return request({
    url: contextPath + '/dzzorg/bzcy',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 班子成员删除*/
export function delBzcyInfo(data) {
  return request({
    url: contextPath + '/dzzorg/delBzcyInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 党组织标签页-奖惩信息*/
export function getPartyGroupJcxx(data) {
  return request({
    url: contextPath + '/dzzorg/jcxx',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 列表
export function bzlist(data) {
  return request({
    url: contextPath + '/bzcy/query',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 班子成员信息
export function bzInfon(data) {
  return request({
    url: contextPath + '/bzcy/byid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 班子成员(离任)列表
export function bzlrList(data) {
  return request({
    url: contextPath + '/bzcy/querylz',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 班子成员新增
export function bzAdd(data) {
  return request({
    url: contextPath + '/bzcy/add',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 班子成员编辑
export function bzUpdate(data) {
  return request({
    url: contextPath + '/bzcy/update',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 班子成员删除
export function bzDele(data) {
  return request({
    url: contextPath + '/bzcy/dele',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员  党内职务下拉选
export function jobSelect(data) {
  return request({
    url: contextPath + '/bzcy/dnzw',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 班子成员  学历下拉选
export function xlSelect(data) {
  return request({
    url: contextPath + '/bzcy/xl',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 职务级别下拉选
export function zwlevel(data) {
  return request({
    url: contextPath + '/bzcy/zwlevel',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子成员 token
export function saveToken(data) {
  return request({
    url: contextPath + '/savetoken',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划分页查询
export function pageQuery(data) {
  return request({
    url: contextPath + '/brand/pageQuery',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划新闻分页查询
export function newsPageQuery(data) {
  return request({
    url: contextPath + '/brand/newsPageQuery',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划新闻保存
export function saveNews(data) {
  return request({
    url: contextPath + '/brand/saveNews',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划新闻发布
export function publishNews(data) {
  return request({
    url: contextPath + '/brand/publishNews',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划新闻取消发布
export function cancelNews(data) {
  return request({
    url: contextPath + '/brand/cancelNews',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划新闻删除
export function deleteNews(data) {
  return request({
    url: contextPath + '/brand/deleteNews',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划详情
export function detailNews(data) {
  return request({
    url: contextPath + '/brand/detailNews',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划保存
export function save(data) {
  return request({
    url: contextPath + '/brand/save',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划详情修改
export function brandDetail(data) {
  return request({
    url: contextPath + '/brand/brandDetail',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党建领航计划详情
export function detail(data) {
  return request({
    url: contextPath + '/brand/detail',
    method: 'post',
    data: qs.stringify(data)
  });
}

// =======================================================
// 智慧党建修改基本信息接口
// 机构
export function getdzzorgTree(data) {
  return request({
    url: contextPath + '/dzzorg/tree',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 机构
export function getdzzorgTreeTwo(data) {
  return request({
    url: contextPath + '/dzzorg/tree2',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 导出党组织信息
export function exportDZZInfo(data) {
  return request({
    url: contextPath + '/dzzorg/exportDZZInfo',
    method: 'post',
    headers: headers,
    data: data
  });
}

// 党组织基本信息(统计)
export function getTotalsdzzInfo(data) {
  return request({
    url: contextPath + '/dzzorg/dzzInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 通过编码获取Excel模板
export function getExcelTemplate(data) {
  return request({
    url: contextPath + '/excel/getExcelTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党员信息分页查询
export function pageInfo(data) {
  return request({
    url: contextPath + '/member/pageInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党员统计信息分页查询
export function pageAnalysisinfo(data) {
  return request({
    url: contextPath + '/member/pageAnalysisinfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

//获取党员基本信息
export function getBase(data) {
  return request({
    url: contextPath + '/member/getBase',
    method: 'post',
    data: qs.stringify(data)
  });
}

//查询党籍和党组织
export function getMembership(data) {
  return request({
    url: contextPath + '/member/getMembership',
    method: 'post',
    data: qs.stringify(data)
  });
}

//查询党员职务
export function getPost(data) {
  return request({
    url: contextPath + '/member/getPost',
    method: 'post',
    data: qs.stringify(data)
  });
}

//奖惩
export function getRewards(data) {
  return request({
    url: contextPath + '/member/getRewards',
    method: 'post',
    data: qs.stringify(data)
  });
}

//出国境
export function getGoAbroad(data) {
  return request({
    url: contextPath + '/member/getGoAbroad',
    method: 'post',
    data: qs.stringify(data)
  });
}
//困难情况
export function getDifficulty(data) {
  return request({
    url: contextPath + '/member/getDifficulty',
    method: 'post',
    data: qs.stringify(data)
  });
}
//流动情况
export function getFlows(data) {
  return request({
    url: contextPath + '/member/getFlows',
    method: 'post',
    data: qs.stringify(data)
  });
}
//入党情况
export function memberGetJoinParty(data) {
  return request({
    url: contextPath + '/member/getJoinParty',
    method: 'post',
    data: qs.stringify(data)
  });
}

//数据字典下拉框
export function dictItemSel(data) {
  return request({
    url: contextPath + '/member/dictItemSel',
    method: 'post',
    data: qs.stringify(data)
  });
}

//班子成员职务
export function getBzcyzw(data) {
  return request({
    url: contextPath + '/dzzorg/getBzcyzw',
    method: 'post',
    data: qs.stringify(data)
  });
}

//班子成员新增
export function modiBzcyInfo(data) {
  return request({
    url: contextPath + '/dzzorg/modiBzcyInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 书记花名册分页查询
export function pageTSecretaryInfo(data) {
  return request({
    url: contextPath + '/dzzorg/pageTSecretaryInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 书记花名册新增/编辑
export function addTSecretaryInfo(data) {
  return request({
    url: contextPath + '/dzzorg/addTSecretaryInfo',
    method: 'post',
    data: data,
    headers: headers
  });
}

// 书记花名册删除
export function delTSecretaryInfo(data) {
  return request({
    url: contextPath + '/dzzorg/delTSecretaryInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 书记花名册详情
export function getTSecretaryInfo(data) {
  return request({
    url: contextPath + '/dzzorg/getTSecretaryInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 书记花名册巡查分页查询
export function pageQueryTSecretaryInfo(data) {
  return request({
    url: contextPath + '/dzzorg/pageQueryTSecretaryInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党组织机构树
export function orgTrees(data) {
  return request({
    url: contextPath + '/dzzorg/orgTrees',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党组织机构树
export function meetingInspection(data) {
  return request({
    url: contextPatho + '/meeting/meetingInspection',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 导出
export function meetingexcel(data) {
  return request({
    url: contextPatho + '/meeting/excel',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* hbl 党委会组织树接口*/
export function getPartyTreeThree(data) {
  return request({
    url: contextPath + '/dzzorg/tree3',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 先进党组织类型*/
export function listDictItem(data) {
  return request({
    url: contextPath + '/dzzorg/listDictItem',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 添加先进党组织*/
export function addDzzAdvanced(data) {
  return request({
    url: contextPath + '/dzzorg/addDzzAdvanced',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 分页查询先进党组织*/
export function pageDzzAdvanced(data) {
  return request({
    url: contextPath + '/dzzorg/pageDzzAdvanced',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 查询先进党组织详情*/
export function getDzzAdvanced(data) {
  return request({
    url: contextPath + '/dzzorg/getDzzAdvanced',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 删除先进党组织详情*/
export function delDzzAdvanced(data) {
  return request({
    url: contextPath + '/dzzorg/delDzzAdvanced',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*编辑先进党组织详情*/
export function updateDzzAdvanced(data) {
  return request({
    url: contextPath + '/dzzorg/updateDzzAdvanced',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*新增优秀党员/党务工作者*/
export function addDyInfoGood(data) {
  return request({
    url: contextPath + '/member/addDyInfoGood',
    headers,
    method: 'post',
    data
  });
}

/*分页查询优秀党员/党务工作者*/
export function pageDyInfoGood(data) {
  return request({
    url: contextPath + '/member/pageDyInfoGood',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*删除优秀党员/党务工作者*/
export function delDyInfoGood(data) {
  return request({
    url: contextPath + '/member/delDyInfoGood',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*修改优秀党员/党务工作者*/
export function updateDyInfoGood(data) {
  return request({
    url: contextPath + '/member/updateDyInfoGood',
    headers,
    method: 'post',
    data
  });
}

/*查看优秀党员/党务工作者*/
export function getDyInfoGood(data) {
  return request({
    url: contextPath + '/member/getDyInfoGood',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*党员信息tab中查看优秀党员/党务工作者*/
export function listDyInfoGood(data) {
  return request({
    url: contextPath + '/member/listDyInfoGood',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*政治面貌下拉*/
export function listPoliticalLandscape(data) {
  return request({
    url: contextPath + '/member/listPoliticalLandscape',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*新增党组织人员信息子模块*/
export function addDyInfoChildModule(data) {
  return request({
    url: contextPath + '/member/addDyInfoChildModule',
    headers,
    method: 'post',
    data
  });
}

/*分页党组织人员信息子模块*/
export function pageDyInfoChildModule(data) {
  return request({
    url: contextPath + '/member/pageDyInfoChildModule',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*删除党组织人员信息子模块*/
export function delDyInfoChildModule(data) {
  return request({
    url: contextPath + '/member/delDyInfoChildModule',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*查看党组织人员信息子模块*/
export function getDyInfoChildModule(data) {
  return request({
    url: contextPath + '/member/getDyInfoChildModule',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*修改党组织人员信息子模块*/
export function updateDyInfoChildModule(data) {
  return request({
    url: contextPath + '/member/updateDyInfoChildModule',
    headers,
    method: 'post',
    data
  });
}

/*党建工作经费组织机构树*/
export function dzzFundingTree(data) {
  return request({
    url: contextPath + '/dzzorg/dzzFundingTree',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*党建工作经费下拉*/
export function listdzzFunding(data) {
  return request({
    url: contextPath + '/dzzorg/listdzzFunding',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*添加党建工作经费*/
export function addDzzFunding(data) {
  return request({
    url: contextPath + '/dzzorg/addDzzFunding',
    headers,
    method: 'post',
    data
  });
}

/*分页党建工作经费*/
export function pageDzzFunding(data) {
  return request({
    url: contextPath + '/dzzorg/pageDzzFunding',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*删除党建工作经费*/
export function delDzzFunding(data) {
  return request({
    url: contextPath + '/dzzorg/delDzzFunding',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*查看党建工作经费*/
export function getDzzFunding(data) {
  return request({
    url: contextPath + '/dzzorg/getDzzFunding',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*修改党建工作经费*/
export function updateDzzFunding(data) {
  return request({
    url: contextPath + '/dzzorg/updateDzzFunding',
    headers,
    method: 'post',
    data
  });
}

/*驾驶舱党建工作经费统计*/
export function dzzFundingStatistica(data) {
  return request({
    url: contextPath + '/dzzorg/dzzFundingStatistica',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*驾驶舱党员党龄统计*/
export function partyAgeInfo(data) {
  return request({
    url: contextPath + '/member/partyAgeInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*驾驶舱领航计划统计*/
export function pilotPlanStatistical(data) {
  return request({
    url: contextPath + '/cockpit/pilotPlanStatistical',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*驾驶舱领航计划统计*/
export function crucialProjectStatistical(data) {
  return request({
    url: contextPath + '/cockpit/crucialProjectStatistical',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*驾驶舱地图数据*/
export function mapInfo(data) {
  return request({
    url: contextPath + '/cockpit/mapInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*搜索*/
export function page(data) {
  return request({
    url: contextPath + '/search/page',
    method: 'post',
    data: qs.stringify(data)
  });
}
