import request from "@/utils/request";
import qs from "qs";
const urcPath = "/urc";
const evalPath = "/eval";
const headers = {
    post: {
        "Content-Type": "application/json;charset=UTF-8"
    }
};

// 系统参数system.js
// 分页
export function commonPage(data) {
    return request({
        url: urcPath + "/param/page",
        method: "post",
        data: qs.stringify(data)
    });
}
//新增
export function addSysParam(data) {
    return request({
        url: urcPath + "/param/addSysParam",
        method: "post",
        headers: headers,
        data: data
    });
}
//编辑
export function modifySystemParam(data) {
    return request({
        url: urcPath + "/param/modifySystemParam",
        method: "post",
        data: qs.stringify(data)
    });
}
//删除
export function removeSystemParam(data) {
    return request({
        url: urcPath + "/param/removeSystemParam",
        method: "post",
        data: qs.stringify(data)
    });
}

//task.js
// 附件删除
export function removeAttachment(data) {
    return request({
        url: urcPath + "/attachment/removeAttachment",
        method: "post",
        data: qs.stringify(data)
    });
}
// 附件上传预览
export function preview(data) {
    return request({
        url: urcPath + "/attachment/preview",
        method: "post",
        data: qs.stringify(data)
    });
}

// submit.js
//删除考核细则附件
export function removeFileBusiness(data) {
    return request({
        url: evalPath + "/elCheckRules/removeFileBusiness",
        method: "post",
        data: qs.stringify(data)
    });
}

//role.js
// 删除角色
export function roleDelete(data) {
    return request({
        url: urcPath + "/role/remove",
        method: "post",
        data: qs.stringify(data)
    });
}
// 取消用户关联角色
export function removeUserRoleAuth(data) {
    return request({
        url: urcPath + "/role/removeUserRoleAuth",
        method: "post",
        headers: headers,
        data: data
    });
}
// 角色重命名
export function roleRename(data) {
    return request({
        url: urcPath + "/role/rename",
        method: "post",
        headers: headers,
        data: data
    });
}
// 添加角色
export function roleAdd(data) {
    return request({
        url: urcPath + "/role/add",
        method: "post",
        headers: headers,
        data: data
    });
}
// 通过角色分页查询用户信息
export function getRoleUserPage(data) {
    return request({
        url: urcPath + "/role/getRoleUserPage",
        method: "post",
        data: qs.stringify(data)
    });
}
// 角色授权查询
export function getRoleFunctionList(data) {
    return request({
        url: urcPath + "/role/getRoleFunctionList",
        method: "post",
        data: qs.stringify(data)
    });
}
// 分页查询角色
export function getRoleListForPager(data) {
    return request({
        url: urcPath + "/role/getRoleListForPager",
        method: "post",
        data: qs.stringify(data)
    });
}
// 给用户添加角色并赋权
export function addUserRoleAuth(data) {
    return request({
        url: urcPath + "/role/addUserRoleAuth",
        method: "post",
        headers: headers,
        data: data
    });
}
// 给用户添加角色
export function addUserRole(data) {
    return request({
        url: urcPath + "/role/addUserRole",
        method: "post",
        headers: headers,
        data: data
    });
}
// 给角色赋权
export function addRoleAuth(data) {
    return request({
        url: urcPath + "/role/addRoleAuth",
        method: "post",
        headers: headers,
        data: data
    });
}
// 获取标签列表
export function listLable(data) {
    return request({
        url: urcPath + "/lable/list",
        method: "post",
        data: qs.stringify(data)
    });
}
// 0:只查询组织机构；1查询组织机构及用户 新的树
export function getOrgByPorgid(data) {
    return request({
        url: urcPath + "/org/getOrgByPorgid",
        method: "post",
        data: qs.stringify(data)
    });
}
// 数据字典的树
export function tabledictionary(data) {
    return request({
        url: urcPath + "/TableAndColumn/tabledictionary",
        method: "post",
        data: qs.stringify(data)
    });
}
//数据字典对应的table
export function columndictionary(data) {
    return request({
        url: urcPath + "/TableAndColumn/columndictionary",
        method: "post",
        data: qs.stringify(data)
    });
}

//log.js
// 使用统计
export function getUsage(data) {
    return request({
        url: urcPath + "/log/usage",
        method: "post",
        data: qs.stringify(data)
    });
}
export function getSystemLogForPage(data) {
    return request({
        url: urcPath + "/log/getSystemLogForPage",
        method: "post",
        data: qs.stringify(data)
    });
}

//menu.js
// 查询菜单及功能树
export function listAllTree(data) {
    return request({
        url: urcPath + "/function/listAllTree",
        method: "post",
        data: qs.stringify(data)
    });
}
// 校验
export function checkfcturl(data) {
    return request({
        url: urcPath + "/function/checkfcturl",
        method: "post",
        data: qs.stringify(data)
    });
}
// 获取菜单下面的功能
export function betchRemove(data) {
    return request({
        url: urcPath + "/function/batchRemove",
        headers: headers,
        method: "post",
        data: JSON.stringify(data.fctids)
    });
}
// 通过用户userid查询菜单
export function getAuthMenu(data) {
    return request({
        url: urcPath + "/function/list",
        method: "post",
        data: qs.stringify(data)
    });
}
// 删除菜单
export function menuDel(data) {
    return request({
        url: urcPath + "/function/remove",
        method: "post",
        data: qs.stringify(data)
    });
}
// 添加子菜单
export function menuAdd(data) {
    return request({
        url: urcPath + "/function/add",
        headers: headers,
        method: "post",
        data: data
    });
}
// 获取菜单详情
export function getMenuByFctid(data) {
    return request({
        url: urcPath + "/function/fctid",
        method: "post",
        data: qs.stringify(data)
    });
}
// 修改菜单
export function updateMenuByFctid(data) {
    return request({
        url: urcPath + "/function/update",
        headers: headers,
        method: "post",
        data: data
    });
}
// 获取菜单下面的功能
export function getFunctionByPfctid(data) {
    return request({
        url: urcPath + "/function/pfctid",
        method: "post",
        data: qs.stringify(data)
    });
}

//batchtask.js
//新增定时任务
export function add(data) {
    return request({
        url: urcPath + "/batchtask/add",
        method: "post",
        headers: headers,
        data: data
    });
}
//删除任务
export function del(data) {
    return request({
        url: urcPath + "/batchtask/del",
        method: "post",
        data: qs.stringify(data)
    });
}
//定时任务列表
export function list(data) {
    return request({
        url: urcPath + "/batchtask/list",
        method: "post",
        data: qs.stringify(data)
    });
}
//暂停任务
export function pause(data) {
    return request({
        url: urcPath + "/batchtask/pause",
        method: "post",
        data: qs.stringify(data)
    });
}
//恢复任务
export function resume(data) {
    return request({
        url: urcPath + "/batchtask/resume",
        method: "post",
        data: qs.stringify(data)
    });
}
//更新任务信息
export function updateJob(data) {
    return request({
        url: urcPath + "/batchtask/updateJob",
        method: "post",
        headers: headers,
        data: data
    });
}
