import request from "@/utils/request";
import qs from "qs";
const loginPath = "/power";
const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

// 通过用户userid查询菜单
export function getAuthMenu(data) {
  return request({
    url: loginPath + "/function/list",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询菜单及功能树
export function listAllTree(data) {
  return request({
    url: loginPath + "/function/listAllTree",
    method: "post",
    data: qs.stringify(data)
  });
}
// 通过功能fctid查询菜单
export function ByFctid(data) {
  return request({
    url: loginPath + "/function/fctid",
    method: "post",
    data: qs.stringify(data)
  });
}

// 删除菜单
export function menuDel(data) {
  return request({
    url: loginPath + "/function/remove",
    method: "post",
    data: qs.stringify(data)
  });
}

// 添加子菜单
export function menuAdd(data) {
  return request({
    url: loginPath + "/function/add",
    headers: headers,
    method: "post",
    data: data
  });
}

// 获取菜单详情
export function getMenuByFctid(data) {
  return request({
    url: loginPath + "/function/fctid",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改菜单
export function updateMenuByFctid(data) {
  return request({
    url: loginPath + "/function/update",
    headers: headers,
    method: "post",
    data: data
  });
}

// 获取菜单下面的功能
export function getFunctionByPfctid(data) {
  return request({
    url: loginPath + "/function/pfctid",
    method: "post",
    data: qs.stringify(data)
  });
}

// 获取菜单下面的功能
export function betchRemove(data) {
  return request({
    url: loginPath + "/function/batchRemove",
    headers: headers,
    method: "post",
    data: JSON.stringify(data.fctids)
  });
}

// 校验
export function checkfcturl(data) {
  return request({
    url: loginPath + "/function/checkfcturl",
    method: "post",
    data: qs.stringify(data)
  });
}


// ==================================
// 菜单树
export function menuGetTree(data) {
  return request({
    url: loginPath + "/menu/tree",
    method: "post",
    data: qs.stringify(data)
  });
}

// 表格查询菜单
export function menuGetInfo(data) {
  return request({
    url: loginPath + "/menu/getInfo",
    headers: headers,
    method: "post",
    data: data
  });
}

// 增加菜单
export function menuGetAdd(data) {
  return request({
    url: loginPath + "/menu/add",
    headers: headers,
    method: "post",
    data: data
  });
}

// 根据ID查询菜单
export function menuGetData(data) {
  return request({
    url: loginPath + "/menu/get",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改菜单
export function menuGetUpdate(data) {
  return request({
    url: loginPath + "/menu/update",
    headers: headers,
    method: "post",
    data: data
  });
}



// 校验菜单url是否存在
export function menuGetcheckMenuUrl(data) {
  return request({
    url: loginPath + "/menu/checkMenuUrl",
    method: "post",
    data: qs.stringify(data)
  });
}


// 删除菜单
export function menuGetRemove(data) {
  return request({
    url: loginPath + "/menu/remove",
    method: "post",
    data: qs.stringify(data)
  });
}
