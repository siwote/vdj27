import request from '@/utils/request'
import qs from 'qs'
const filePath = '/lhweb';

// 根据业务id查询详情
export function getFormByBusId(data) {
  return request({
    url: filePath + '/form/getFormByBusId',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 保存自定义表单
export function saveForm(data) {
  return request({
    url: filePath + '/form/saveForm',
    method: 'post',
    data: qs.stringify(data)
  })
}
