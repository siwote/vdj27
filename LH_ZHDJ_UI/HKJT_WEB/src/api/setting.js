import request from "@/utils/request";
import qs from "qs";

const settingPath = "/duty";
const powerPath = "/power";
const powerPathu = "/urc";

/* hbl 任务分类查询*/
export function taskSearch(data) {
  return request({
    url: settingPath + "/taskClass/listByFlag",
    method: "post",
    data: qs.stringify(data)
  });
}
/* hbl 任务分类新增 */
export function taskAdd(data) {
  return request({
    url: settingPath + "/taskClass/add",
    method: "post",
    data: qs.stringify(data)
  });
}
/* hbl 任务分类删除 */
export function taskDelete(data) {
  return request({
    url: settingPath + "/taskClass/delete",
    method: "post",
    data: qs.stringify(data)
  });
}
/* hbl 任务分类修改 */
export function taskUpdate(data) {
  return request({
    url: settingPath + "/taskClass/update",
    method: "post",
    data: qs.stringify(data)
  });
}
/*  hbl 任务分类拖拽 */

export function taskMove(data) {
  return request({
    url: settingPath + "/taskClass/move",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 获取任务分类下任务的数量 */
export function gettaskcount(data) {
  return request({
    url: settingPath + "/taskClass/gettaskcount",
    method: "post",
    data: qs.stringify(data)
  });
}
// 积分设置
export function getIntegral(data) {
  return request({
    url: settingPath + "/listRule/listByStatus",
    method: "post",
    data: qs.stringify(data)
  });
}
// 积分设置修改
export function seaveIntegral(data) {
  return request({
    url: settingPath + "/listRule/update",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 组织用户 */

/* 获取组织树 */
export function getOrgTreeData(data) {
  return request({
    url: powerPath + "/organ/listTree",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 添加子组织 */
export function getOrgAdd(data) {
  return request({
    url: powerPath + "/organ/addChildDepart",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 修改组织查询 */
export function getQueryOrg(data) {
  return request({
    url: powerPath + "/organ/queryOrgan",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 子组织重命名 */
export function getOrgRename(data) {
  return request({
    url: powerPath + "/organ/rename",
    method: "post",
    data: qs.stringify(data)
  });
}
/*获取组织机构树下面的子机构  */
export function getChildrenOrg(data) {
  return request({
    url: powerPath + "/organ/listChildOrgan",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 删除组织树的子组织 */
export function getOrgDelete(data) {
  return request({
    url: powerPath + "/organ/delete",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 拖拽树组织 */

export function getOrgMove(data) {
  return request({
    url: powerPath + "/organ/move",
    method: "post",
    data: qs.stringify(data)
  });
}

/*获取用户列表 */
export function getUserData(data) {
  return request({
    url: powerPath + "/organ/page",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 删除用户 */
export function getDeleteUser(data) {
  return request({
    url: powerPath + "/user/delete",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 添加成员 */
export function getAddUser(data) {
  return request({
    url: powerPath + "/user/addPwUser",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 修改成员查询 */
export function getUser(data) {
  return request({
    url: powerPath + "/user/getUser",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 设置人员机构 */
export function getUserSave(data) {
  return request({
    url: powerPath + "/user/save",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 设置左侧菜单 */
export function getAuthMenu(data) {
  return request({
    url: powerPathu + "/function/list",
    method: "post",
    data: qs.stringify(data)
  });
}
/*获取用户组数据（需分页）*/
export function getUserGroup(data) {
  return request({
    url: powerPath + "/usergroup/page",
    method: "post",
    data: qs.stringify(data)
  });
}
/*更新用户组名称*/
export function getUpdateUserGroup(data) {
  return request({
    url: powerPath + "/usergroup/update",
    method: "post",
    data: qs.stringify(data)
  });
}
/*删除用户组*/
export function getDeleteUserGroup(data) {
  return request({
    url: powerPath + "/usergroup/delete",
    method: "post",
    data: qs.stringify(data)
  });
}
/*查询用户组内的人*/
export function getListUser(data) {
  return request({
    url: powerPath + "/usergroup/listUser",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 旧密码 */
export function getOldPwd(data) {
  return request({
    url: powerPath + "/user/oldPwd",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 修改密码 */
export function getUpdatePwd(data) {
  return request({
    url: powerPath + "/user/updatePwd",
    method: "post",
    data: qs.stringify(data)
  });
}
/* 数据授权 */
export function listauth(data) {
  return request({
    url: powerPath + "/role/listauth",
    method: "post",
    data: qs.stringify(data)
  });
}

// 消息已读未读
export function noRead(data) {
  return request({
    url: settingPath + "/remindList/update",
    method: "post",
    data: qs.stringify(data)
  });
}

// 同步企业微信
export function syncDepartments(data) {
  return request({
    url: powerPath + "/weixin/syncDepartments",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询是否同步企业微信后渲染不同按钮状态
export function switchBtn(data) {
  return request({
    url: powerPath + "/params/getOnOff",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改密码
export function modifypwd(data) {
  return request({
    url: powerPathu + "/user/modifypwd",
    method: "post",
    data: qs.stringify(data)
  });
}
