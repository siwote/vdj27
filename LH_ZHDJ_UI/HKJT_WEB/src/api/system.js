import request from '@/utils/request';
import apiUrl from '@/utils/apiUrl';
const { urlPath } = apiUrl;
const { urcPath } = urlPath;
const { portalPath } = urlPath;
import qs from 'qs';
const loginPath = '/urc';
const headers = {
  post: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
};

// 查询菜单
export function listAllFunctionMenus(data) {
  return request({
    url: loginPath + '/functioncfg/listAllFunctionMenus',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 查询菜单及功能树
export function listAllTree(data) {
  return request({
    url: loginPath + '/functioncfg/listAllTree',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询公司拥有菜单及功能
export function listBycompid(data) {
  return request({
    url: loginPath + '/functioncfg/listBycompid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 给公司添加菜单及功能
export function addCompFuctions(data) {
  return request({
    url: loginPath + '/functioncfg/addCompFuctions',
    headers: headers,
    method: 'post',
    data: data
  });
}

// 通过功能fctid查询菜单
export function ByFctid(data) {
  return request({
    url: loginPath + '/functioncfg/fctid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 删除菜单
export function menuDel(data) {
  return request({
    url: loginPath + '/functioncfg/remove',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 添加子菜单
export function menuAdd(data) {
  return request({
    url: loginPath + '/functioncfg/add',
    headers: headers,
    method: 'post',
    data: data
  });
}

// 获取菜单详情
export function getMenuByFctid(data) {
  return request({
    url: loginPath + '/functioncfg/fctid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 修改菜单
export function updateMenuByFctid(data) {
  return request({
    url: loginPath + '/functioncfg/update',
    headers: headers,
    method: 'post',
    data: data
  });
}

// 获取菜单下面的功能
export function getFunctionByPfctid(data) {
  return request({
    url: loginPath + '/functioncfg/pfctid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取菜单下面的功能
export function betchRemove(data) {
  return request({
    url: loginPath + '/functioncfg/batchRemove',
    headers: headers,
    method: 'post',
    data: JSON.stringify(data.fctids)
  });
}

// 校验
export function checkfcturl(data) {
  return request({
    url: loginPath + '/functioncfg/checkfcturl',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 系统参数
// 分页
export function commonPage(data) {
  return request({
    url: loginPath + '/param/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

//新增
export function addSysParam(data) {
  return request({
    url: loginPath + '/param/addSysParam',
    method: 'post',
    headers: headers,
    data: data
  });
}

//编辑
export function modifySystemParam(data) {
  return request({
    url: loginPath + '/param/modifySystemParam',
    method: 'post',
    data: qs.stringify(data)
  });
}

//删除
export function removeSystemParam(data) {
  return request({
    url: loginPath + '/param/removeSystemParam',
    method: 'post',
    data: qs.stringify(data)
  });
}

//获取系统参数详情
export function getParam(data) {
  return request({
    url: loginPath + '/param/get',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 企业维护
// 分页
export function companyPage(data) {
  return request({
    url: loginPath + '/company/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 新增
export function addCompanyInfo(data) {
  return request({
    url: loginPath + '/company/add',
    method: 'post',
    headers: headers,
    data: data
  });
}

// 修改
export function editCompanyInfo(data) {
  return request({
    url: loginPath + '/company/modify',
    method: 'post',
    headers: headers,
    data: data
  });
}

// 查看详情
export function getCompanyInfo(data) {
  return request({
    url: loginPath + '/company/get',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 修改状态
export function setModifystatus(data) {
  return request({
    url: loginPath + '/company/modifystatus',
    method: 'post',
    headers: headers,
    data: data
  });
}


// 查询站点
export function listPrStations(data) {
  return request({
    url: `${portalPath}/station/listPrStations`,
    method: 'post',
    data: qs.stringify(data)
  });
}