import request from '@/utils/request'
import qs from 'qs'
const contextPath = '/lhdj'
/* hbl 党员基本信息*/
export function partyInfo(data) {
  return request({
    url: contextPath + '/member/dyInfo',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 党组织基本信息
export function partyGroupInfo(data) {
  return request({
    url: contextPath + '/dzzorg/dzzInfo',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 学历基本信息
export function partyFeeAllUpdate(data) {
  return request({
    url: contextPath + '/member/xlInfo',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 年龄基本信息 
export function ageInfo(data) {
  return request({
    url: contextPath + '/member/ageInfo',
    method: 'post',
    data: qs.stringify(data)
  })
}
