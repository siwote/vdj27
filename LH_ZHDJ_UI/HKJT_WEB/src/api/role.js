import request from '@/utils/request';
import qs from 'qs';
const contextPath = '/power';
const contextPathl = '/lhdj';
const contextPathh = '/horn';

/* 获取组织树数据 */
// export function getTree(data) {

//   return request({
//     url: contextPath + '/organ/listOrganChildNode',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }

export function getTree(data) {
  return request({
    url: contextPathl + '/selection/listOrganChildNode',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*获取角色列表 */
export function getRoleData(data) {
  return request({
    url: contextPath + '/role/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 添加角色 */
export function getAddRole(data) {
  return request({
    url: contextPath + '/role/insert',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 删除角色 */

export function getRoleDelete(data) {
  return request({
    url: contextPath + '/role/delete',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 编辑角色 */
export function getRoleUpdate(data) {
  return request({
    url: contextPath + '/role/update',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 分配角色 */
export function getRoleBinding(data) {
  return request({
    url: contextPath + '/role/binding',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取角色绑定的信息 */

export function getRoleListBinding(data) {
  return request({
    url: contextPath + '/role/listbinding',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 树选择器新加api

/* 执行对象 获取本级组织树 */
// export function listOrganChildNodeByType(data) {
//   return request({
//     url: contextPath + '/organ/listChildNodeByType',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }

export function listOrganChildNodeByType(data) {
  return request({
    url: contextPathl + '/selection/listChildNodeByType',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getNode(data) {
  return request({
    url: contextPathl + '/selection/getNode',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取所有的组织树 */
export function getOrgAllTree(data) {
  return request({
    url: contextPath + '/organ/listAllChildNodeByType',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function updatephone(data) {
  return request({
    url: contextPath + '/organ/updatephone',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取用户组织树 */

// export function getOrgUserTree(data) {
//   return request({
//     url: contextPath + '/organ/listAllChildNode',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }

export function getOrgUserTree(data) {
  return request({
    url: contextPathl + '/selection/listAllChildNode',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getOrgUserTree2(data) {
  return request({
    url: contextPathl + '/selection/listAllChildNode2',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 角色查询
export function roleList(data) {
  return request({
    url: contextPath + '/role/listbatch',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 用户角色绑定接口
export function useridbindroleid(data) {
  return request({
    url: contextPath + '/role/useridbindroleid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 树
export function getUnderBranchList(data) {
  return request({
    url: contextPathl + '/dzzorg/getUnderBranchList',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 班子
export function getMemberListByPost(data) {
  return request({
    url: contextPathl + '/dzzorg/getMemberListByPost',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 详情
export function getPatrolBranchInfo(data) {
  return request({
    url: contextPathh + '/patrol/getPatrolBranchInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// work
export function listMeetingHeldInfo(data) {
  return request({
    url: contextPathh + '/patrol/listMeetingHeldInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}
