/**
 * 提醒规则api
 */
import request from '@/utils/request'
import qs from 'qs'
const contextPath = '/duty'

const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

export function getRules(data) {
  return request({
    url: contextPath + '/remindRule/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

export function ruleUpdate(data) {
  return request({
    url: contextPath + '/remindRule/update',
    method: 'post',
    data: qs.stringify(data)
  })
}
