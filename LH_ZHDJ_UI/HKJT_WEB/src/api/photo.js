import request from '@/utils/request'
import qs from 'qs'

const filePath = '/hecheng';

/* 高拍仪-保存图片*/
export function addImg(data) {
  return request({
    url: filePath + '/pdfhc/addImg',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* 高拍仪-删除图片*/
export function removeImg(data) {
  return request({
    url: filePath + '/pdfhc/removeImg',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* 高拍仪-合成pdf*/
export function createPdf(data) {
  return request({
    url: filePath + '/pdfhc/createPdf',
    method: 'post',
    data: qs.stringify(data)
  })
}
