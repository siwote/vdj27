import request from '@/utils/request'
import qs from 'qs'

const duesUrl = '/developing'

export function spepage(data) {
    return request({
        url: duesUrl + '/gzjf/page',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function saveapplication(data) {
    return request({
        url: duesUrl + '/gzjf/saveapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function getsummary(data) {
    return request({
        url: duesUrl + '/gzjf/getsummary',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function listpartyfeesummary(data) {
    return request({
        url: duesUrl + '/dfsy/listpartyfeesummary',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function listinterest(data) {
    return request({
        url: duesUrl + '/dfsy/listinterest',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function getfee(data) {
    return request({
        url: duesUrl + '/dfsy/getfee',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function savesum(data) {
    return request({
        url: duesUrl + '/dfsy/savesum',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function getsum(data) {
    return request({
        url: duesUrl + '/dfsy/getsum',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function saveinterest(data) {
    return request({
        url: duesUrl + '/dfsy/saveinterest',
        method: 'post',
        data: qs.stringify(data)
    })
}