import request from "@/utils/request";
import qs from "qs";
const contextPath = "/lhdj";
const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

// 党组织转接
// 转出申请分页
export function partyPage(data) {
  return request({
    url: contextPath + "/dyqy/page",
    method: "post",
    data: qs.stringify(data)
  });
}

// 转入接收分页
export function partyPageTow(data) {
  return request({
    url: contextPath + "/dyqy/page2",
    method: "post",
    data: qs.stringify(data)
  });
}

// 审核分页
export function reviewPage(data) {
  return request({
    url: contextPath + "/dyqy/page3",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增
export function partySave(data) {
  return request({
    url: contextPath + "/dyqy/save",
    method: "post",
    headers: headers,
    data: data
  });
}

// 编辑
export function partyAgain(data) {
  return request({
    url: contextPath + "/dyqy/again",
    method: "post",
    data: qs.stringify(data)
  });
}

// 删除
export function partyDelete(data) {
  return request({
    url: contextPath + "/dyqy/delete",
    method: "post",
    data: qs.stringify(data)
  });
}

// 审核
export function updateReview(data) {
  return request({
    url: contextPath + "/dyqy/update",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增党组织
export function adddzz(data) {
  return request({
    url: contextPath + "/dzz/adddzz",
    method: "post",
    headers: headers,
    data: data
  });
}

// 编辑党组织
export function updatedzz(data) {
  return request({
    url: contextPath + "/dzz/updatedzz",
    method: "post",
    headers: headers,
    data: data
  });
}

// 删除党组织
export function deldzz(data) {
  return request({
    url: contextPath + "/dzz/deldzz",
    method: "post",
    data: qs.stringify(data)
  });
}

// 汇总查询
export function orgTransferInquiry(data) {
  return request({
    url: contextPath + "/dyqy/orgnum",
    method: "post",
    data: qs.stringify(data)
  });
}
// 查看介绍信
export function getPartyLetter(data) {
  return request({
    url: contextPath + "/dyqy/getPartyLetter",
    method: "post",
    data: qs.stringify(data)
  });
}

// 校验党组织code唯一性
export function isCheckCode(data) {
  return request({
    url: contextPath + "/dzz/checkcode",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增党员
export function adddy(data) {
  return request({
    url: contextPath + "/member/adddy",
    method: "post",
    headers: headers,
    data: data
  });
}

// 编辑党员
export function updatedy(data) {
  return request({
    url: contextPath + "/member/updatedy",
    method: "post",
    headers: headers,
    data: data
  });
}

// 删除党员
export function deldy(data) {
  return request({
    url: contextPath + "/member/deldy",
    method: "post",
    data: qs.stringify(data)
  });
}

// 党员身份证号唯一性校验
export function checkzjhm(data) {
  return request({
    url: contextPath + "/member/checkzjhm",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增奖惩信息
export function addjcxx(data) {
  return request({
    url: contextPath + "/member/addjcxx",
    method: "post",
    headers: headers,
    data: data
  });
}

// 修改奖惩信息
export function updatejcxx(data) {
  return request({
    url: contextPath + "/member/updatejcxx",
    method: "post",
    headers: headers,
    data: data
  });
}

// 删除奖惩信息
export function deljcxx(data) {
  return request({
    url: contextPath + "/member/deljcxx",
    method: "post",
    data: qs.stringify(data)
  });
}

// 处分原因
export function getccxx(data) {
  return request({
    url: contextPath + "/member/getccxx",
    method: "post",
    data: qs.stringify(data)
  });
}

// 奖惩列表分页
export function getjcxx(data) {
  return request({
    url: contextPath + "/member/getjcxx",
    method: "post",
    data: qs.stringify(data)
  });
}
