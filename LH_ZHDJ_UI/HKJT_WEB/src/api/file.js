import request from '@/utils/request';
import qs from 'qs';

const filePath = '/document';

/* 资料库-列表展示资源分类查所有的*/
export function listClass(data) {
  return request({
    url: filePath + '/class/listClass',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 资料库-资料分类新增的判断*/
export function limitAddDlType(data) {
  return request({
    url: filePath + '/class/limitAddDlType',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-新建资源分类*/
export function addClass(data) {
  return request({
    url: filePath + '/class/addClass',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-删除资源分类*/
export function deleteClass(data) {
  return request({
    url: filePath + '/class/deleteClass',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-修改资源分类*/
export function updateClass(data) {
  return request({
    url: filePath + '/class/updateClass',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-拖动排序资源分类*/
export function moveClass(data) {
  return request({
    url: filePath + '/class/moveClass',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-文件上传*/
export function uploadFile(data) {
  return request({
    url: filePath + '/manager/uploadFile',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-展示资料管理器*/
export function listDatas(data) {
  return request({
    url: filePath + '/manager/listDatas',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-管理器和文件的树列表*/
export function listDatasAndFiles(data) {
  return request({
    url: filePath + '/manager/listDatasAndFiles',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-资料管理器新增和修改*/
export function saveDatas(data) {
  return request({
    url: filePath + '/manager/saveDatas',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-文件删除*/
export function deleteFile(data) {
  return request({
    url: filePath + '/manager/deleteFile',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-文件重命名*/
export function rename(data) {
  return request({
    url: filePath + '/manager/rename',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-文件下载*/
export function downFile(data) {
  return request({
    url: filePath + '/manager/downFile',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-文件预览*/
export function viewFile(data) {
  return request({
    url: filePath + '/manager/viewFile',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-资料管理器删除*/
export function deleteDatas(data) {
  return request({
    url: filePath + '/manager/deleteDatas',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-资料管理器详情*/
export function getDlDatas(data) {
  return request({
    url: filePath + '/manager/getDlDatas',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-资料转移*/
export function moveDatas(data) {
  return request({
    url: filePath + '/manager/moveDatas',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-资料管理器重命名的判断接口*/
export function duplicateName(data) {
  return request({
    url: filePath + '/manager/duplicateName',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 资料库-高拍仪文件上传*/
export function highMeterUpload(data) {
  return request({
    url: filePath + '/manager/highMeterUpload',
    method: 'post',
    data: qs.stringify(data)
  });
}
