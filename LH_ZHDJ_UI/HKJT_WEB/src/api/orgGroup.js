/**
 * 用户组api
 */
import request from '@/utils/request'
import qs from 'qs'
const contextPath = '/power'
const contextPathT = '/developing'
const contextPathl = '/lhdj'


export function getAddOrgGroup(data) {
  return request({
    url: contextPath + '/usergroup/insert',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getOrgGroup(data) {
  return request({
    url: contextPath + '/usergroup/page',
    method: 'post',
    data: qs.stringify(data)
  })
}

export function getGroupUser(data) {
  return request({
    url: contextPath + '/usergroup/listUser',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 审核人选择器数据源
// export function listRoleMap(data) {
//   return request({
//     url: contextPath + '/organ/listRoleMap',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }

export function listRoleMap(data) {
  return request({
    url: contextPathl + '/selection/listRoleMap',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 党员选择器数据源
export function stepList(data) {
  return request({
    url: contextPathT + '/step/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
