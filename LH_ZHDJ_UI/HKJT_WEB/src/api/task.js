/**
 * 我的任务的api
 */
import request from '@/utils/request';
import qs from 'qs';

const taskPath = '/duty';

/* 部署周期任务 */
export function addPeriodTask(data) {
  return request({
    url: taskPath + '/taskDeploy/deployPeriodTask',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 已发布任务头部条数角标 */
export function tabHeaderData(data) {
  return request({
    url: taskPath + '/taskDeploy/count',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取下次反馈提醒时间 */
export function getTaskRemindTime(data) {
  return request({
    url: taskPath + '/taskDetails/getTimeByTaskid',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取多人任务的执行人 */
export function getTaskSubjects(data) {
  return request({
    url: taskPath + '/taskDetails/listDepolySubjects',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 提交评论 */
export function getTaskComment(data) {
  return request({
    url: taskPath + '/taskDetails/comment',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 删除评论 */
export function deleteTaskComment(data) {
  return request({
    url: taskPath + '/taskDetails/deleteComment',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 周期任务的时间节点 */
export function listTime(data) {
  return request({
    url: taskPath + '/taskDetails/listTimeByTaskid',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 查看退回原因 */
export function getRemarks(data) {
  return request({
    url: taskPath + '/taskDetails/getRemarks',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 我的任务的tab角标 */
export function myTabHeaderData(data) {
  return request({
    url: taskPath + '/myTask/count',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function deleteFile(data) {
  return request({
    url: taskPath + '/file/delete',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function downloadFile(data) {
  return request({
    url: taskPath + '/file/download',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 最新消息列表 */
export function getRemindListPage(data) {
  return request({
    url: taskPath + '/remindList/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 未读消息条数 */
export function getRemindListNum(data) {
  return request({
    url: taskPath + '/remindList/getNum',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 批量已读消息
export function batchupdate(data) {
  return request({
    url: taskPath + '/remindList/batchupdate',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*  hbl 我的任务查看详情 */
export function getNoDetail(data) {
  return request({
    url: taskPath + '/myTask/details',
    method: 'post',
    data: qs.stringify(data)
  });
}

// hbl 查看详情列表信息查询
export function getMessageList(data) {
  return request({
    url: taskPath + '/taskDetails/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

// hbl 新增评论
export function addComment(data) {
  return request({
    url: taskPath + '/myTask/comment',
    method: 'post',
    data: qs.stringify(data)
  });
}
// hbl 删除评论
export function deleteComment(data) {
  return request({
    url: taskPath + '/myTask/deleteComment',
    method: 'post',
    data: qs.stringify(data)
  });
}
// hbl 新增回复
export function addReply(data) {
  return request({
    url: taskPath + '/myTask/reply',
    method: 'post',
    data: qs.stringify(data)
  });
}
// hbl 删除回复
export function deleteReply(data) {
  return request({
    url: taskPath + '/myTask/deleteReply',
    method: 'post',
    data: qs.stringify(data)
  });
}
// hbl 签收 退回
export function updateStatus(data) {
  return request({
    url: taskPath + '/myTask/updateStatus',
    method: 'post',
    data: qs.stringify(data)
  });
}

// hbl 最新进展时间
export function getNewTime(data) {
  return request({
    url: taskPath + '/taskDetails/getRemindTime',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*  hbl 已部署任务查看列表 */
export function getDoneList(data) {
  return request({
    url: taskPath + '/taskDeploy/page',
    method: 'post',
    data: qs.stringify(data)
  });
}
/*  hbl 周期时间点 */
export function getWeekDay(data) {
  return request({
    url: taskPath + '/taskDetails/listTime',
    method: 'post',
    data: qs.stringify(data)
  });
}
/*  hbl  附件列表查询*/
export function fileList(data) {
  return request({
    url: taskPath + '/file/list',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*  hbl  获取最新进展 */
export function getProgrous(data) {
  return request({
    url: taskPath + '/taskDetails/getProgress',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 督办跟踪 */
export function getTaskDeploy(data) {
  return request({
    url: taskPath + '/taskDeploy/oversee',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 催办 */
export function clickurgent(data) {
  return request({
    url: taskPath + '/taskDeploy/clickurgent',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 关注 */
export function foucstask(data) {
  return request({
    url: taskPath + '/taskDeploy/foucstask',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 取消关注 */
export function cancelfocus(data) {
  return request({
    url: taskPath + '/taskDeploy/cancelfocus',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取任务督办各个状态角标 */
export function statuscount(data) {
  return request({
    url: taskPath + '/taskDeploy/statuscount',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*系统动态*/
export function dynamics(data) {
  return request({
    url: taskPath + '/dynamics/page',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 删除 */
export function deleteTask(data) {
  return request({
    url: taskPath + '/taskDeploy/deleteAll',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 点赞 */

export function getParise(data) {
  return request({
    url: taskPath + '/taskDetails/getParise',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 取消点赞 */
export function awayParise(data) {
  return request({
    url: taskPath + '/taskDetails/awayParise',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取点赞数量 */
export function getNum(data) {
  return request({
    url: taskPath + '/taskDetails/getNum',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取任务属性
export function taskAttr(data) {
  return request({
    url: taskPath + '/taskAttribute/query',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 保存任务属性
export function taskAttrSeave(data) {
  return request({
    url: taskPath + '/taskAttribute/save',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 修改任务基本信息 */

export function updateTaskBase(data) {
  return request({
    url: taskPath + '/taskDeploy/updateTaskBase',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 修改任务的时间信息 */
export function updateTaskTimenode(data) {
  return request({
    url: taskPath + '/taskDeploy/updateTaskTimenode',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 完成任务

export function completeTask(data) {
  return request({
    url: taskPath + '/taskDeploy/complete',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 暂停任务 */
export function suspendTask(data) {
  return request({
    url: taskPath + '/taskDeploy/suspend',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取表单信息 */
export function queryform(data) {
  return request({
    url: taskPath + '/taskDeploy/queryform',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 修改执行热 */

export function updateTaskSubjects(data) {
  return request({
    url: taskPath + '/taskDeploy/updateTaskSubjects',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 刷新 */
export function uploadMessage(data) {
  return request({
    url: taskPath + '/taskDetails/feedpage',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取分解策略 */
export function taskStrategy(data) {
  return request({
    url: taskPath + '/taskResolve/strategy',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取分解时间*/
export function taskStage(data) {
  return request({
    url: taskPath + '/taskResolve/stage',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取分解目标单位 */
export function taskUnit(data) {
  return request({
    url: taskPath + '/taskResolve/unit',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取分解策略 */
export function deployTask(data) {
  return request({
    url: taskPath + '/taskResolve/deploy',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 恢复任务 */
export function recoveryTask(data) {
  return request({
    url: taskPath + '/taskDeploy/recovery',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取任务阶段名 */
export function taskResolveListTime(data) {
  return request({
    url: taskPath + '/taskResolve/listtime',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取分解任务阶段详情 */
export function taskResolveListbean(data) {
  return request({
    url: taskPath + '/taskResolve/listbean',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 预览 */
export function pageUploadTask(data) {
  return request({
    url: taskPath + '/taskDeploy/pageUploadTask',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 批量提交 */
export function submitUploadTask(data) {
  return request({
    url: taskPath + '/taskDeploy/submitUploadTask',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 提交分解反馈*/
export function addBack(data) {
  return request({
    url: taskPath + '/taskResolve/save',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 分解获取上次提交的反馈信息 */
export function query(data) {
  return request({
    url: taskPath + '/taskResolve/query',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 分解获取执行对象 */
export function listobj(data) {
  return request({
    url: taskPath + '/taskResolve/listobj',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 分解获取图标数据 */
export function duibi(data) {
  return request({
    url: taskPath + '/taskResolve/duibi',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function zoushi(data) {
  return request({
    url: taskPath + '/taskResolve/zoushi',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function fenbu(data) {
  return request({
    url: taskPath + '/taskResolve/fenbu',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取模板下载路径 */
export function getDown(data) {
  return request({
    url: taskPath + '/taskDeploy/downExcelTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取单位 */
export function getunit(data) {
  return request({
    url: taskPath + '/taskResolve/getunit',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取根节点 */
export function getRoot(data) {
  return request({
    url: taskPath + '/taskResolve/getroot',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 修改分解任务的分解信息 */
export function modity(data) {
  return request({
    url: taskPath + '/taskResolve/modity',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 附件预览 */
export function preview(data) {
  return request({
    url: taskPath + '/file/preview',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 督办跟踪  分管领导 牵头领导 */
export function lingdao(data) {
  return request({
    url: taskPath + '/taskDeploy/lingdao',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 部署普通任务 */
export function add(data) {
  return request({
    url: taskPath + '/taskDeploy/deployCommonTask',
    method: 'post',
    data: qs.stringify(data)
  });
}

//生成新的时间

export function getCycleTimes(data) {
  return request({
    url: taskPath + '/taskDeploy/getCycleTimes',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 任务审核
//王葵
// 下发审核  批量审核
export function auditTask(data) {
  return request({
    // url: taskPath + '/feedbaackaudit/audit',
    url: taskPath + '/feedbaackaudit/auditTask',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 上报审核
export function audit(data) {
  return request({
    url: taskPath + '/feedbaackaudit/audit',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取每条审核的详情
export function getfeeddetials(data) {
  return request({
    url: taskPath + '/feedbaackaudit/getfeeddetials',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取审核的列表内容
export function taskAuditData(data) {
  return request({
    url: taskPath + '/feedbaackaudit/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取提醒时间节点
export function listremindtimes(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listremindtimes',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取反馈详情列表
export function listfeeddetails(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listfeeddetails',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取执行树
export function listtreesubject(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listtreesubject',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 已发任务获取执行树
export function listTreeSubjectNew(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listtreesubjectNew',
    method: 'post',
    data: qs.stringify(data)
  });
}

//获取执行审核日志
export function listauditlog(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listauditlog',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 已发任务
/* 获取已发布任务的表格 */
export function taskData(data) {
  return request({
    url: taskPath + '/taskDeploy/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取任务签收情况 */
export function getSignLog(data) {
  return request({
    url: taskPath + '/taskDetails/getSignLog',
    method: 'post',
    data: qs.stringify(data)
  });
}

//全部签收
export function allPage(data) {
  return request({
    url: taskPath + '/taskDeploy/allPage',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 获取签收日志各个状态角标 */
export function getSignStatus(data) {
  return request({
    url: taskPath + '/taskDetails/getSignStatus',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取多人任务统计数据*/
export function feedCount(data) {
  return request({
    url: taskPath + '/taskDeploy/feedCount',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* 修改和保存草稿 */
export function saveTaskDraft(data) {
  return request({
    url: taskPath + '/taskDeploy/saveTaskDraft',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取我的任务id */
export function getMyTaskId(data) {
  return request({
    url: taskPath + '/taskDetails/getMyTaskId',
    method: 'post',
    data: qs.stringify(data)
  });
}
/* 获取任务详情 */
export function getTaskDetail(data) {
  return request({
    url: taskPath + '/taskDeploy/details',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 我的任务
/* 获取我的任务表格 */
export function myTaskData(data) {
  return request({
    url: taskPath + '/myTask/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*  hbl 添加反馈信息 */
export function addMessage(data) {
  return request({
    url: taskPath + '/myTask/feedback',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取时间点要求
export function getfeedrequirement(data) {
  return request({
    url: taskPath + '/feedbaackaudit/getfeedrequirement',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*签收退回 */
export function signedTask(data) {
  return request({
    url: taskPath + '/myTask/updateStatus',
    method: 'post',
    data: qs.stringify(data)
  });
}

//退回
export function backTask(data) {
  return request({
    url: taskPath + '/myTask/updateback',
    method: 'post',
    data: qs.stringify(data)
  });
}

//w我相关的
export function listrelatedtome(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listrelatedtome',
    method: 'post',
    data: qs.stringify(data)
  });
}

//工作巡查
export function listworkinspection(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listworkinspection',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 草稿

export function listDraftTask(data) {
  return request({
    url: taskPath + '/taskDeploy/listDraftTask',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 草稿发布
export function deployDraftTask(data) {
  return request({
    url: taskPath + '/taskDeploy/deployDraftTask',
    method: 'post',
    data: qs.stringify(data)
  });
}

//草稿获取退回详情
export function getDetialsByTaskId(data) {
  return request({
    url: taskPath + '/myTask/getDetialsByTaskId',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 普通任务变更发布
export function updateCommonTask(data) {
  return request({
    url: taskPath + '/taskDeploy/updateCommonTask',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 普通任务草稿修改发布
export function deployDraftCompelete(data) {
  return request({
    url: taskPath + '/taskDeploy/deployDraftCompelete',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 任务附件上传接口
export function newupload(data) {
  return request({
    url: taskPath + '/file/newupload',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取审核人
export function listSuperiors(data) {
  return request({
    url: taskPath + '/taskDeploy/listSuperiors',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取关联任务

export function myTaskList(data) {
  return request({
    url: taskPath + '/myTask/myTaskList',
    method: 'post',
    data: qs.stringify(data)
  });
}

/*  附件上传*/
export function addFile(data) {
  return request({
    url: taskPath + '/file/newupload',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 附件删除接口
export function newdelete(data) {
  return request({
    url: taskPath + '/file/newdelete',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 任务链
export function listtaskchain(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listtaskchain',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 头部角标

export function pagenum(data) {
  return request({
    url: taskPath + '/feedbaackaudit/pagenum',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 关联的任务回显
export function listRelatTask(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listRelatTask',
    method: 'post',
    data: qs.stringify(data)
  });
}

// hbl 修改反馈信息
export function updateFeedBack(data) {
  return request({
    url: taskPath + '/myTask/updateFeedBack',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 补发时间
export function remindForOne(data) {
  return request({
    url: taskPath + '/taskDeploy/remindForOne',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 获取反馈提醒时间点列表（新）
export function getCycleTimesNew(data) {
  return request({
    url: taskPath + '/taskDeploy/getCycleTimesNew',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取单条反馈的详情
export function modibeforequery(data) {
  return request({
    url: taskPath + '/myTask/modibeforequery',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取服务器的当前时间
export function currentTime(data) {
  return request({
    url: taskPath + '/myTask/currentTime',
    method: 'post',
    data: qs.stringify(data)
  });
}

//获取签收日志
export function listSignIn(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listsignin',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 审核
export function listfeeddetailsaudit(data) {
  return request({
    url: taskPath + '/feedbaackaudit/listfeeddetailsaudit',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 驾驶舱党建任务督查统计
export function taskStatistical(data) {
  return request({
    url: taskPath + '/taskDeploy/taskStatistical',
    method: 'post',
    data: qs.stringify(data)
  });
}
