import request from "@/utils/request";
import qs from "qs";
const loginPath = "/urc";
const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

// 查询所有组织信息组织树
export function listTreeAll(data) {
  return request({
    url: loginPath + "/org/listTreeAll",
    method: "post",
    data: qs.stringify(data)
  });
}
// 查询组织信息组织树
export function listTree(data) {
  return request({
    url: loginPath + "/org/listTree",
    method: "post",
    data: qs.stringify(data)
  });
}

// 搜索组织用户信息
export function searchOrgUser(data) {
  return request({
    url: loginPath + "/user/search",
    method: "post",
    data: qs.stringify(data)
  });
}
// 组织排序
export function sortTree(data) {
  return request({
    url: loginPath + "/org/sortTree",
    method: "post",
    data: qs.stringify(data)
  });
}

// 添加组织
export function orgAdd(data) {
  return request({
    url: loginPath + "/org/add",
    method: "post",
    headers: headers,
    data: data
  });
}

// 添加子组织查询
export function addReturn(data) {
  return request({
    url: loginPath + "/org/addReturn",
    method: "post",
    headers: headers,
    data: data
  });
}

// 修改组织信息
export function orgUpdate(data) {
  return request({
    url: loginPath + "/org/update",
    method: "post",
    headers: headers,
    data: data
  });
}

// 删除组织
export function orgDel(data) {
  return request({
    url: loginPath + "/org/del",
    method: "post",
    data: qs.stringify(data)
  });
}

// 分页查询用户信息
export function listUser(data) {
  return request({
    url: loginPath + "/user/list",
    method: "post",
    data: qs.stringify(data)
  });
}

// 校验账号
export function checkAccount(data) {
  return request({
    url: loginPath + "/user/checkAccount",
    method: "post",
    data: qs.stringify(data)
  });
}
// 添加用户
export function userAdd(data) {
  return request({
    url: loginPath + "/user/add",
    method: "post",
    headers: headers,
    data: data
  });
}

// 修改用户
export function userUpdate(data) {
  return request({
    url: loginPath + "/user/update",
    method: "post",
    headers: headers,
    data: data
  });
}

// 删除用户
export function userDelete(data) {
  return request({
    url: loginPath + "/user/delete",
    method: "post",
    data: qs.stringify(data)
  });
}

// 批量删除用户
export function batchDelete(data) {
  return request({
    url: loginPath + "/user/batchDelete",
    method: "post",
    data: qs.stringify(data)
  });
}
// 禁用用户
export function userProhibit(data) {
  return request({
    url: loginPath + "/user/prohibit",
    method: "post",
    data: qs.stringify(data)
  });
}

// 批量禁用用户
export function batchProhibit(data) {
  return request({
    url: loginPath + "/user/batchProhibit",
    method: "post",
    data: qs.stringify(data)
  });
}

// 解禁用户
export function recover(data) {
  return request({
    url: loginPath + "/user/recover",
    method: "post",
    data: qs.stringify(data)
  });
}
// 批量解禁用户
export function batchRecover(data) {
  return request({
    url: loginPath + "/user/batchRecover",
    method: "post",
    data: qs.stringify(data)
  });
}

// 用户排序
export function userSort(data) {
  return request({
    url: loginPath + "/user/sort",
    method: "post",
    data: qs.stringify(data)
  });
}

// 转移部门
export function transferOrg(data) {
  return request({
    url: loginPath + "/user/transferOrg",
    method: "post",
    headers: headers,
    data: data
  });
}
// 设置上级
export function mgflag(data) {
  return request({
    url: loginPath + "/org/mgflag",
    method: "post",
    headers: headers,
    data: data
  });
}

// 所有功能对用户的授权情况
export function userAllAuthfunctions(data) {
  return request({
    url: loginPath + "/user/userAllAuthfunctions",
    method: "post",
    //headers: headers,
    data: qs.stringify(data)
  });
}

// 创建常用组
export function groupAdd(data) {
  return request({
    url: loginPath + "/group/add",
    method: "post",
    headers: headers,
    data: data
  });
}

// 获取常用组
export function groupList(data) {
  return request({
    url: loginPath + "/group/list",
    method: "post",
    headers: headers,
    data: data
  });
}
// 分级查询组织及组织下的用户
export function getSubOrgAndUsersList(data) {
  return request({
    url: loginPath + "/org/getSubOrgAndUsersList",
    method: "post",
    data: qs.stringify(data)
  });
}
// 获取当前用户组织
export function getorgs(data) {
  return request({
    url: loginPath + "/org/getorgs",
    method: "post",
    data: qs.stringify(data)
  });
}
// 切换用户组织
export function switchorg(data) {
  return request({
    url: loginPath + "/org/switchorg",
    method: "post",
    data: qs.stringify(data)
  });
}

// 同步钉钉用户、组织、角色数据
export function sycDingData(data) {
  return request({
    url: loginPath + "/org/sycDingData",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询当前单位下所有组织
export function getSelfSubOrgList(data) {
  return request({
    url: loginPath + "/org/getSelfSubOrgList",
    method: "post",
    data: qs.stringify(data)
  });
}


// 0:只查询组织机构；1查询组织机构及用户 新的树
export function getOrgByPorgid (data) {
  return request({
      url: loginPath + "/org/getOrgByPorgid",
      method: 'post',
      data: qs.stringify(data)
  });
}


