import request from "@/utils/request";
import qs from "qs";

const loginPath = "/horn";
const loginPathu = "/urc";
const loginPathl = "/lhdj";

const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

// 会议查询列表
export function getMeetingListForPage(data) {
  return request({
    url: loginPath + "/meeting/getMeetingListForPage",
    method: "post",
    data: qs.stringify(data)
  });
}

// 首页信息查询
export function getMeetingTypeStatistics(data) {
  return request({
    url: loginPath + "/meeting/getMeetingTypeStatistics",
    method: "post",
    data: qs.stringify(data)
  });
}

// 根据会议类型查询会议达标信息
export function getSingleTypeStatistics(data) {
  return request({
    url: loginPath + "/meeting/getSingleMeetingTypeStatistics",
    method: "post",
    data: qs.stringify(data)
  });
}

// 增加会议记录
export function addMeeting(data) {
  return request({
    url: loginPath + "/meeting/addMeeting",
    method: "post",
    data: data,
    headers: headers
  });
}
// 删除会议记录信息
export function getRemoveMeeting(data) {
  return request({
    url: loginPath + "/meeting/removeMeeting",
    method: "post",
    data: qs.stringify(data)
  });
}

// 会议指南
export function getSystemParamByKey(data) {
  return request({
    url: loginPath + "/meeting/getSystemParamByKey",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查看会议地点
export function getMeetingPlace(data) {
  return request({
    url: loginPath + "/meeting/getMeetingPlace",
    method: "post",
    data: data,
    headers: headers
  });
}

//  查看会议记录
export function getMeetingRecord(data) {
  return request({
    url: loginPath + "/meeting/getMeetingRecord",
    method: "post",
    data: qs.stringify(data)
  });
}
// 数据复用
export function getLatestMeetingRecord(data) {
  return request({
    url: loginPath + "/meeting/getLatestMeetingRecord",
    method: "post",
    data: qs.stringify(data)
  });
}
//  修改会议记录
export function modifyMeeting(data) {
  return request({
    url: loginPath + "/meeting/modifyMeeting",
    method: "post",
    data: data,
    headers: headers
  });
}

//  查询会议信息，条件可选
export function queryMeetingByAddition(data) {
  return request({
    url: loginPath + "/meeting/queryMeetingByAddition",
    method: "post",
    data: data,
    headers: headers
  });
}

//  删除附件
export function removeAttachment(data) {
  return request({
    url: loginPathu + "/attachment/removeAttachment",
    method: "post",
    data: qs.stringify(data)
  });
}

//  人员选择器党员
export function getMemberByOrgancode(data) {
  return request({
    url: loginPath + "/meeting/getMemberByOrgancode",
    method: "post",
    data: qs.stringify(data)
  });
}

//  人员选择器班子成员
export function bzcy(data) {
  return request({
    url: loginPathl + "/dzzorg/bzcy",
    method: "post",
    data: qs.stringify(data)
  });
}

// 谈心谈话接口
// 分页查询
export function getHearttalkGage(data) {
  return request({
    url: loginPath + "/talk/page",
    method: "post",
    data: qs.stringify(data)
  });
}

//谈心谈话记录删除
export function getHearttalkRemove(data) {
  return request({
    url: loginPath + "/talk/remove",
    method: "post",
    data: qs.stringify(data)
  });
}

//谈心谈话记录新增
export function getHearttalkAdd(data) {
  return request({
    url: loginPath + "/talk/add",
    method: "post",
    data: data,
    headers: headers
  });
}

//谈心谈话记录查看
export function getHearttalkGet(data) {
  return request({
    url: loginPath + "/talk/get",
    method: "post",
    data: data,
    headers: headers
  });
}

//谈心谈话记录修改
export function getHearttalkmModify(data) {
  return request({
    url: loginPath + "/talk/modify",
    method: "post",
    data: data,
    headers: headers
  });
}

// 工作汇报
// 工作汇报分页查询
export function getReportPage(data) {
  return request({
    url: loginPath + "/report/page",
    method: "post",
    data: qs.stringify(data)
  });
}

// 工作汇报记录删除
export function getReportRemove(data) {
  return request({
    url: loginPath + "/report/remove",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增工作汇报
export function getReportAdd(data) {
  return request({
    url: loginPath + "/report/add",
    method: "post",
    data: data,
    headers: headers
  });
}

// 工作汇报查看详情
export function getReportGet(data) {
  return request({
    url: loginPath + "/report/get",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改工作汇报
export function getReportModify(data) {
  return request({
    url: loginPath + "/report/modify",
    method: "post",
    data: data,
    headers: headers
  });
}

// 增加标签会议记录
export function addTagMeeting(data) {
  return request({
    url: loginPath + "/meeting/addTagMeeting",
    method: "post",
    data: data,
    headers: headers
  });
}

// 修改标签会议记录
export function modifyTagMeeting(data) {
  return request({
    url: loginPath + "/meeting/modifyTagMeeting",
    method: "post",
    data: data,
    headers: headers
  });
}

//  标签查看会议记录
export function getMeetingTagRecord(data) {
  return request({
    url: loginPath + "/meeting/getMeetingTagRecord",
    method: "post",
    data: qs.stringify(data)
  });
}

//查询是否设置党员活动日
export function themeCheckSetting(data) {
  return request({
    url: loginPath + "/theme/checkSetting",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询日期区间
export function themeGetDate(data) {
  return request({
    url: loginPath + "/theme/getDate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询日期
export function themeGetDay(data) {
  return request({
    url: loginPath + "/theme/getDay",
    method: "post",
    data: qs.stringify(data)
  });
}

// 设置党员活动日
export function themeSet(data) {
  return request({
    url: loginPath + "/theme/set",
    method: "post",
    data: data,
    headers: headers
  });
}

// 党委中心组学习列表提交接口
export function querysubmitMeeting(data) {
  return request({
    url: loginPath + "/meeting/submitMeeting",
    method: "post",
    data: qs.stringify(data)
  });
}

// 组织生活 === 会议通知接口
// 新增会议通知
export function zuNoticeAdd(data) {
  return request({
    url: loginPath + "/notice/add",
    method: "post",
    data: data,
    headers: headers
  });
}

// 撤销会议通知
export function zuNoticeCancel(data) {
  return request({
    url: loginPath + "/notice/cancel",
    method: "post",
    data: qs.stringify(data)
  });
}

// 会议通知阅读状态
export function zuNoticeCheck(data) {
  return request({
    url: loginPath + "/notice/check",
    method: "post",
    data: data,
    headers: headers
  });
}

// 会议被通知对象查看站内信内容
export function zuNoticeContent(data) {
  return request({
    url: loginPath + "/notice/content",
    method: "post",
    data: data,
    headers: headers
  });
}

// 分页列表
export function zuNoticePage(data) {
  return request({
    url: loginPath + "/notice/page",
    method: "post",
    data: data,
    headers: headers
  });
}

// 根据ID查看通知内容
export function zuNoticeQuery(data) {
  return request({
    url: loginPath + "/notice/query",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改会议通知
export function zuNoticeUpdate(data) {
  return request({
    url: loginPath + "/notice/update",
    method: "post",
    data: qs.stringify(data)
  });
}

// 支部应到人数
export function getShallreachedNum(data) {
  return request({
    url: loginPath + "/meeting/getShallreachedNum",
    method: "post",
    data: qs.stringify(data)
  });
}

// 复用通知内容到会议
export function getNoticeCopy(data) {
  return request({
    url: loginPath + "/notice/copy",
    method: "post",
    data: qs.stringify(data)
  });
}

// 复用通知内容到会议
export function getSub(data) {
  return request({
    url: loginPath + "/notice/getSub",
    method: "post",
    data: qs.stringify(data)
  });
}
// 消息提醒配置
export function addMeetingRemind(data) {
  return request({
    url: loginPath + "/meetingcfg/addMeetingRemind",
    method: "post",
    data: data,
    headers: headers
  });
}

// 保存会议指南
export function configMeetingGuide(data) {
  return request({
    url: loginPath + "/meetingcfg/configMeetingGuide",
    method: "post",
    data: data,
    headers: headers
  });
}

// 根据上级key获取子级内容集合

export function getMeetingGuide(data) {
  return request({
    url: loginPath + "/meetingcfg/getMeetingGuide",
    method: "post",
    data: qs.stringify(data)
  });
}

// 会议自动提交配置项
export function configAutoCommit(data) {
  return request({
    url: loginPath + "/meetingcfg/configAutoCommit",
    method: "post",
    data: qs.stringify(data)
  });
}

// 根据paramkey获取数据
export function getMeetingGuideByKey(data) {
  return request({
    url: loginPath + "/meetingcfg/getMeetingGuideByKey",
    method: "post",
    data: qs.stringify(data)
  });
}

// 通知列表分页
export function listMeetingNotice(data) {
  return request({
    url: loginPath + "/notice/listMeetingNotice",
    method: "post",
    data: qs.stringify(data)
  });
}
//
// 应参会人
export function listGetyingInfo(data) {
  return request({
    url: loginPath + "/meeting/getSub",
    method: "post",
    data: qs.stringify(data)
  });
}

// 党建巡查
// 会议巡查总入口界面
export function listGettypelist(data) {
  return request({
    url: loginPath + "/patrol/gettypelist",
    method: "post",
    data: qs.stringify(data)
  });
}

// 会议PDF服务
export function getMeetingSignPdf(data) {
  return request({
    url: loginPath + "/meetingpdf/getMeetingSignPdf",
    method: "post",
    data: data,
    headers: headers
  });
}

// 党建巡查分页第一层列表
export function pagetotal(data) {
  return request({
    url: loginPath + "/patrol/pagetotal",
    method: "post",
    data: data,
    headers: headers
  });
}

// 党建巡查分页第一层列表(党委会)
export function pageCommittee(data) {
  return request({
    url: loginPath + "/patrol/pageCommittee",
    method: "post",
    data: data,
    headers: headers
  });
}

// 建巡查弹出框分页展示所有下级支部统计
export function pagebranch(data) {
  return request({
    url: loginPath + "/patrol/pagebranch",
    method: "post",
    data: data,
    headers: headers
  });
}

// 党建导出巡查列表数据
export function patrolexport(data) {
  return request({
    url: loginPath + "/patrol/export",
    method: "post",
    data: data,
    headers: headers
  });
}

// 导出会议类型列表
export function meetingViewList(data) {
  return request({
    url: loginPath + "/meetingpdf/meetingViewList",
    method: "post",
    data: qs.stringify(data)
  });
}

// 批量导出PDF
export function meetingBatchExport(data) {
  return request({
    url: loginPath + "/meetingpdf/meetingBatchExport",
    method: "post",
    data: qs.stringify(data)
  });
}

// // 批量导出PDF
// export function meetingpdfprint(data) {
//   return request({
//     url: loginPath + '/meetingpdf/print',
//     method: 'post',
//     responseType: "blob",
//     data: qs.stringify(data)
//   })
// }

// 获取系统当前时间
export function getCurrentTime(data) {
  return request({
    url: loginPath + "/notice/getCurrentTime",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询我的会议列表分页
export function pageMyMeeting(data) {
  return request({
    url: loginPath + "/notice/pageMyMeeting",
    method: "post",
    data: data,
    headers: headers
  });
}

// 参会&不参会
export function toggleAttend(data) {
  return request({
    url: loginPath + "/notice/toggleAttend",
    method: "post",
    data: qs.stringify(data)
  });
}

// 参会&不参会
export function addBeforeQuery(data) {
  return request({
    url: loginPath + "/notice/addBeforeQuery",
    method: "post",
    data: qs.stringify(data)
  });
}

// 党小组下拉
export function getPartyGroupSelection(data) {
  return request({
    url: loginPath + "/meeting/getPartyGroupSelection",
    method: "post",
    data: qs.stringify(data)
  });
}

//根据党小组id查询成员
export function getGroupSubjects(data) {
  return request({
    url: loginPath + "/meeting/getGroupSubjects",
    method: "post",
    data: qs.stringify(data)
  });
}

// 计算缺席人员
export function hyqxry(data) {
  return request({
    url: loginPath + "/notice/hyqxry",
    method: "post",
    data: data,
    headers: headers
  });
}

// 获取缺席人员
export function getqxry(data) {
  return request({
    url: loginPath + "/notice/getqxry",
    method: "post",
    data: qs.stringify(data)
  });
}

// 退回会议
export function rollbackMeeting(data) {
  return request({
    url: loginPath + "/meeting/rollbackMeeting",
    method: "post",
    data: qs.stringify(data)
  });
}

// 添加民主生活会会前材料
export function addBefore(data) {
  return request({
    url: loginPath + "/meetingdemocracy/addBefore",
    method: "post",
    data,
    headers
  });
}
// 添加民主生活会会后材料
export function addAfter(data) {
  return request({
    url: loginPath + "/meetingdemocracy/addAfter",
    method: "post",
    data,
    headers
  });
}

// 查询民主生活会列表页
export function pageDemocracy(data) {
  return request({
    url: loginPath + "/meetingdemocracy/pageDemocracy",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查看民主生活会
export function get(data) {
  return request({
    url: loginPath + "/meetingdemocracy/get",
    method: "post",
    data: qs.stringify(data)
  });
}

// 审核民主生活会
export function auditDemocracy(data) {
  return request({
    url: loginPath + "/meetingdemocracy/auditDemocracy",
    method: "post",
    data: qs.stringify(data)
  });
}

// 删除民主生活会
export function delMeeting(data) {
  return request({
    url: loginPath + "/meetingdemocracy/delete",
    method: "post",
    data: qs.stringify(data)
  });
}

// 编辑民主生活会会前材料
export function updateBefore(data) {
  return request({
    url: loginPath + "/meetingdemocracy/updateBefore",
    method: "post",
    data,
    headers
  });
}

// 编辑民主生活会会后材料
export function updateAfter(data) {
  return request({
    url: loginPath + "/meetingdemocracy/updateAfter",
    method: "post",
    data,
    headers
  });
}

// 会议查询列表
export function list(data) {
  return request({
    url: loginPathl + "/studyplan/list",
    method: "post",
    data: qs.stringify(data)
  });
}

// 会议新增
export function studyplanadd(data) {
  return request({
    url: loginPathl + "/studyplan/add",
    method: "post",
    data: qs.stringify(data)
  });
}

// 会议删除
export function studyplanremove(data) {
  return request({
    url: loginPathl + "/studyplan/remove",
    method: "post",
    data: qs.stringify(data)
  });
}

// 分页查询民主评议党员综合情况
export function pageDemocracyReview(data) {
  return request({
    url: loginPath + "/meeting/pageDemocracyReview",
    method: "post",
    data: qs.stringify(data)
  });
}

// (提交)编辑民主评议党员综合情况
export function modifyDemocracyReview(data) {
  return request({
    url: loginPath + "/meeting/modifyDemocracyReview",
    method: "post",
    data: qs.stringify(data)
  });
}
