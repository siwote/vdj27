import request from "@/utils/request";
import qs from "qs";
const headJson = {
  "Content-Type": "application/json;charset=UTF-8"
};
const headForm = {
  "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
};

const appPath = "/urc/appbusiness";
// 应用分页查询
export function appPage(data) {
  return request({
    url: appPath + "/page",
    method: "post",
    data: qs.stringify(data)
  });
}
// 保存应用
export function appSave(data) {
  return request({
    url: appPath + "/save",
    method: "post",
    data: qs.stringify(data)
  });
}
// 删除应用
export function appRemove(data) {
  return request({
    url: appPath + "/remove",
    method: "post",
    data: qs.stringify(data)
  });
}
// 拖拽应用
export function appMove(data) {
  return request({
    url: appPath + "/move",
    method: "post",
    data: qs.stringify(data)
  });
}

const formListPath = "/urc/appfunction";
//表单列表
export function formListPage(data) {
  return request({
    url: formListPath + "/page",
    method: "post",
    data: qs.stringify(data)
  });
}
//保存表单
export function formListSave(data) {
  return request({
    url: formListPath + "/save",
    method: "post",
    data: qs.stringify(data)
  });
}
// 删除表单
export function formListRemove(data) {
  return request({
    url: formListPath + "/remove",
    method: "post",
    data: qs.stringify(data)
  });
}
// 发布表单
export function startup(data) {
  return request({
    url: formListPath + "/startup",
    method: "post",
    data: qs.stringify(data)
  });
}
// 撤销表单
export function shutdown(data) {
  return request({
    url: formListPath + "/shutdown",
    method: "post",
    data: qs.stringify(data)
  });
}
// 匿名访问设置
export function settingAnonymousAccess(data) {
  return request({
    url: formListPath + "/settingAnonymousAccess",
    method: "post",
    data: qs.stringify(data)
  });
}

// 启用
export function startupEdit(data) {
  return request({
    url: formListPath + "/startupEdit",
    method: "post",
    data: qs.stringify(data)
  });
}

// 禁用
export function shutdownEdit(data) {
  return request({
    url: formListPath + "/shutdownEdit",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询人
export function getsubjects(data) {
  return request({
    url: "/urc/appfunction/getsubjects",
    method: "post",
    data: qs.stringify(data)
  });
}

const appFormPath = "/urc/appform";

// 查看表单
export function appFormGet(data) {
  return request({
    url: appFormPath + "/get",
    method: "post",
    data: qs.stringify(data)
  });
}
// 查询表单所有属性配置信息
export function getFormAttrByAppid(data) {
  return request({
    url: appFormPath + "/getFormAttrByAppid",
    method: "post",
    data: qs.stringify(data)
  });
}
// 查询表单所有属性配置信息
export function settingFormAttrByAppid(data) {
  return request({
    url: appFormPath + "/settingFormAttrByAppid",
    method: "post",
    data: qs.stringify(data)
  });
}

// 重命名Mongo表单数据字段属性及数据结构
export function renameFormAttrcodeByAppid(data) {
  return request({
    url: appFormPath + "/renameFormAttrcodeByAppid",
    method: "post",
    data: qs.stringify(data)
  });
}

// 重命名Mongo存储名称
export function renameCollection(data) {
  return request({
    url: appFormPath + "/renameCollection",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询能绑定自定义应用的菜单
const treePath = "/urc/function";
export function treeCanBindAppFunction(data) {
  return request({
    url: treePath + "/treeCanBindAppFunction",
    method: "post",
    data: qs.stringify(data)
  });
}

// 分页查询表单数据
const appFormData = "/urc/appformdata";
export function getDataPage(data) {
  return request({
    url: appFormData + "/page",
    method: "post",
    data: qs.stringify(data)
  });
}
// 查询表单数据
export function getDataGet(data) {
  return request({
    url: appFormData + "/get",
    method: "post",
    data: qs.stringify(data)
  });
}
// 新增表单数据新的
export function addvotedata(data) {
  return request({
    url: "/urc/appformdata/addVoteData",
    method: "post",
    headers: headJson,
    data: data
  });
}

// 新增表单数据
export function dataAdd(data) {
  return request({
    url: appFormData + "/add",
    method: "post",
    headers: headJson,
    data: data
  });
}

// 修改表单数据
export function dataModify(data) {
  return request({
    url: appFormData + "/modify",
    method: "post",
    headers: headJson,
    data: data
  });
}

// 删除表单数据
export function dataRemove(data) {
  return request({
    url: appFormData + "/remove",
    method: "post",
    data: qs.stringify(data)
  });
}
// 附件上传预览
export function filePreview(data) {
  return request({
    url: appFormData + "/preview",
    method: "post",
    data: qs.stringify(data)
  });
}
// 根据附件ID列表查询
export function getAttachmentListByFileids(data) {
  return request({
    url: appFormData + "/getAttachmentListByFileids",
    method: "post",
    data: qs.stringify(data)
  });
}

//检查表单属性字段唯一性
export function checkAttrCodeOnly(data) {
  return request({
    url: appFormData + "/checkAttrCodeOnly",
    method: "post",
    data: qs.stringify(data)
  });
}

//身份证识别
export function imageCapacity(data) {
  return request({
    url: appFormData + "/image_capacity",
    method: "post",
    data: qs.stringify(data)
  });
}

// 校验身份证号是否已存在
export function getIdNumber(data) {
  return request({
    url: appFormData + "/getIdNumber",
    method: "post",
    data: qs.stringify(data)
  });
}

// 下载模版
export function dataImportTemplate(data) {
  return request({
    url: appFormData + "/dataImportTemplate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 导出
export function exportTable(data) {
  return request({
    url: appFormData + "/export",
    method: "post",
    data: qs.stringify(data)
  });
}

// 导入
export function dataImport(data) {
  return request({
    url: appFormData + "/dataImport",
    method: "post",
    data: qs.stringify(data)
  });
}

export function produce(data) {
  return request({
    url: "/urc/qrcode/produce",
    method: "post",
    data: qs.stringify(data)
  });
}

export function setReadflag(data) {
  return request({
    url: "/urc/appformdata/setReadflag",
    method: "post",
    data: qs.stringify(data)
  });
}

export function listFormAttrcodeContent(data) {
  return request({
    url: "/urc/appformdata/listFormAttrcodeContent",
    method: "post",
    data: qs.stringify(data)
  });
}

export function getResultInfo(data) {
  return request({
    url: "/urc/voteresult/getResultInfo",
    method: "post",
    data: qs.stringify(data)
  });
}
// 人
export function getsubjectsbyvote(data) {
  return request({
    url: "/urc/voteresult/getsubjectsbyvote",
    method: "post",
    data: qs.stringify(data)
  });
}

// 发送提醒
export function sendmessage(data) {
  return request({
    url: "/urc/voteresult/sendmessage",
    method: "post",
    data: data,
    headers: headJson
  });
}
