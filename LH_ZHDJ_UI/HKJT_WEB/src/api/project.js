import request from '@/utils/request'
import qs from 'qs'

const path = '/horn';
const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};
// 新增项目
export function addProject(data) {
  return request({
    url: path + '/pmProject/add',
    headers: headers,
    method: 'post',
    data: data
  });
}

// 分页查询
export function pageProject(data) {
  return request({
    url: path + '/pmProject/my',
    method: 'get',
    params: data
  });
}


// 获取详情
export function getProject(data) {
  return request({
    url: path + '/pmProject/get',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 修改
export function modifyProject(data) {
  return request({
    url: path + '/pmProject/modify',
    headers: headers,
    method: 'post',
    data: data
  });
}

// 我相关的分页
export function listProject(data) {
  return request({
    url: path + '/pmProject/list',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 删除
export function removeProject(data) {
  return request({
    url: path + '/pmProject/remove',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 项目进度列表
export function listProjectRespons(data) {
  return request({
    url: path + '/pmProjectProgressReport/list',
    method: 'post',
    data: qs.stringify(data)
  });
}


// 项目进度汇报
export function addProjectRespons(data) {
  return request({
    url: path + '/pmProjectProgressReport/add',
    method: 'post',
    data: qs.stringify(data)
  });
}


// 项目进度管理评论
export function addProjectComment(data) {
  return request({
    url: path + '/pmProjectProgressComment/add',
    method: 'post',
    data: qs.stringify(data)
  });
}


// 评论列表
export function listProjectComment(data) {
  return request({
    url: path + '/pmProjectProgressComment/list',
    method: 'post',
    data: qs.stringify(data)
  });
}