import request from '@/utils/request';
import qs from 'qs';
const headers = {
  post: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
};
const duesUrl = '/developing';
const duesUrll = '/lhdj';

export function duesHeader(data) {
  return request({
    url: duesUrl + '/dfgl/reporttop',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function getDues(data) {
  return request({
    url: duesUrl + '/dfgl/sjblsave',
    method: 'post',
    data: qs.stringify(data)
  });
}
// export function duesTree(data) {
//     return request({
//         url: duesUrl + '/dfgl/listOrganChildNodeOne',
//         method: 'post',
//         data: qs.stringify(data)
//     })
// }

export function duesTree(data) {
  return request({
    url: duesUrll + '/selection/listOrganChildNodeOne',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function listOrganChildNodeOne(data) {
  return request({
    url: duesUrl + '/dfgl/listOrganChildNodeOne',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function secondTree(data) {
  return request({
    url: duesUrl + '/dfgl/listOrganChildNodeTwo',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function getparty(data) {
  return request({
    url: duesUrl + '/dfgl/page',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function updateDues(data) {
  return request({
    url: duesUrl + '/dfgl/update',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function sqsaveDues(data) {
  return request({
    url: duesUrl + '/dfgl/sqsave',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function listDues(data) {
  return request({
    url: duesUrl + '/dfgl/list',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function jsszDues(data) {
  return request({
    url: duesUrl + '/dfgl/jsszsave',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function deleteDues(data) {
  return request({
    url: duesUrl + '/dfgl/delete',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function reportDues(data) {
  return request({
    url: duesUrl + '/dfgl/reportpage',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function ageDues(data) {
  return request({
    url: duesUrl + '/dfgl/reportpage',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function modiactual(data) {
  return request({
    url: duesUrl + '/dfgl/modiactual',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function reportcost(data) {
  return request({
    url: duesUrl + '/dfgl/report',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function audit(data) {
  return request({
    url: duesUrl + '/dfgl/audit',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getGrandStatusByOrgCode(data) {
  return request({
    url: duesUrl + '/dfgl/getGrandStatusByOrgCode',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党费收交分页查询
export function getinfoPage(data) {
  return request({
    url: duesUrl + '/gzjf/infoPage',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党费信息编辑
export function getinfoPagemodify(data) {
  return request({
    url: duesUrl + '/gzjf/modify',
    method: 'post',
    data: qs.stringify(data)
  });
}

//党费信息去除
export function getremoveInfo(data) {
  return request({
    url: duesUrl + '/gzjf/removeInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党费信息确认
export function getconfirm(data) {
  return request({
    url: duesUrl + '/gzjf/confirm',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党费巡查分页查询
export function getpatrolPage(data) {
  return request({
    url: duesUrl + '/gzjf/patrolPage',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党费汇总
export function getsummary(data) {
  return request({
    url: duesUrl + '/gzjf/summary',
    method: 'post',
    data: qs.stringify(data)
  });
}

//党费导出
export function getexcel(data) {
  return request({
    url: duesUrl + '/gzjf/excel',
    method: 'post',
    data: qs.stringify(data)
  });
}

//显示大额党费人数
export function patrolDetailsPage(data) {
  return request({
    url: duesUrl + '/gzjf/patrolDetailsPage',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党费基数修改
export function updatePersonDues(data) {
  return request({
    url: duesUrl + '/dfgl/update',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 驾驶舱党员党费统计
export function duesstatistical(data) {
  return request({
    url: duesUrl + '/dfgl/duesStatistical',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 党支部批量更新每个党员的实交党费
export function batchModiactual(data) {
  return request({
    url: duesUrl + '/dfgl/batchModiactual',
    method: 'post',
    data,
    headers
  });
}

//查询维护党费查询列表页;
export function pageMaintainDues(data) {
  return request({
    url: duesUrl + '/dfgl/pageMaintainDues',
    method: 'post',
    data: qs.stringify(data)
  });
}

//修改维护党费;
export function modifyMaintainDues(data) {
  return request({
    url: duesUrl + '/dfgl/modifyMaintainDues',
    method: 'post',
    data: qs.stringify(data)
  });
}
