import request from "@/utils/request";
import qs from "qs";

const checkPath = "/evaluation";

/* hbl 查询考核列表*/
export function queryassess(data) {
  return request({
    url: checkPath + "/pack/queryassess",
    method: "post",
    data: qs.stringify(data)
  });
}

/* hbl 创建体系*/
export function addSystem(data) {
  return request({
    url: checkPath + "/pack/savepack",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改体系
export function updatepack(data) {
  return request({
    url: checkPath + "/pack/updatepack",
    method: "post",
    data: qs.stringify(data)
  });
}

// 删除
export function deletepackage(data) {
  return request({
    url: checkPath + "/pack/deletepackage",
    method: "post",
    data: qs.stringify(data)
  });
}

// 发布
export function release(data) {
  return request({
    url: checkPath + "/pack/release",
    method: "post",
    data: qs.stringify(data)
  });
}

// 复制考核体系
export function copyassess(data) {
  return request({
    url: checkPath + "/pack/copyassess",
    method: "post",
    data: qs.stringify(data)
  });
}

// 撤销考核
export function revoke(data) {
  return request({
    url: checkPath + "/pack/revoke",
    method: "post",
    data: qs.stringify(data)
  });
}

// 根据体系id查询评分者候选list
export function evaluate(data) {
  return request({
    url: checkPath + "/pack/evaluate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 根据考核id查询考核数据
export function getassess(data) {
  return request({
    url: checkPath + "/pack/getassess",
    method: "post",
    data: qs.stringify(data)
  });
}

// 根据考核id获取考核对象
export function getobject(data) {
  return request({
    url: checkPath + "/pack/getobject",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增类别
export function saveitem(data) {
  return request({
    url: checkPath + "/pack/saveitem",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改 类别
export function updateitem(data) {
  return request({
    url: checkPath + "/pack/updateitem",
    method: "post",
    data: qs.stringify(data)
  });
}

// 删除指标或类别
export function deleteTree(data) {
  return request({
    url: checkPath + "/pack/delete",
    method: "post",
    data: qs.stringify(data)
  });
}

// 指标列表
export function packItem(data) {
  return request({
    url: checkPath + "/pack/item",
    method: "post",
    data: qs.stringify(data)
  });
}

// 类别树
export function packTree(data) {
  return request({
    url: checkPath + "/pack/tree",
    method: "post",
    data: qs.stringify(data)
  });
}

// 类别指标拖拽
export function move(data) {
  return request({
    url: checkPath + "/pack/move",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增指标
export function savetarget(data) {
  return request({
    url: checkPath + "/pack/savetarget",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改评分标准
export function updatetarget(data) {
  return request({
    url: checkPath + "/pack/updatetarget",
    method: "post",
    data: qs.stringify(data)
  });
}

// ------- ----自评-------------
// 自评分页查询
export function resultselfeval(data) {
  return request({
    url: checkPath + "/resultselfeval/page",
    method: "post",
    data: qs.stringify(data)
  });
}

// 自评签收
export function resultselfevalSign(data) {
  return request({
    url: checkPath + "/resultselfeval/sign",
    method: "post",
    data: qs.stringify(data)
  });
}

// 自评上报
export function reportEval(data) {
  return request({
    url: checkPath + "/resultselfeval/reportEval",
    method: "post",
    data: qs.stringify(data)
  });
}

// 自评详情
export function formInfo(data) {
  return request({
    url: checkPath + "/resultselfeval/formInfo",
    method: "post",
    data: qs.stringify(data)
  });
}

// 保存考核评分
export function saveSelfEvalResult(data) {
  return request({
    url: checkPath + "/resultselfeval/saveSelfEvalResult",
    method: "post",
    data: qs.stringify(data)
  });
}

// 自评状态统计
export function getSelfStatus(data) {
  return request({
    url: checkPath + "/resultselfeval/getSelfStatus",
    method: "post",
    data: qs.stringify(data)
  });
}

// ------------他评---------------

// 他评分页查询
export function pageQuery(data) {
  return request({
    url: checkPath + "/resulteval/pageQuery",
    method: "post",
    data: qs.stringify(data)
  });
}

// 他评状态统计
export function getEvalStatus(data) {
  return request({
    url: checkPath + "/resulteval/getEvalStatus",
    method: "post",
    data: qs.stringify(data)
  });
}

// 他评状态统计
export function otherReportEval(data) {
  return request({
    url: checkPath + "/resulteval/reportEval",
    method: "post",
    data: qs.stringify(data)
  });
}

// 他评评分指标详情
export function getEvalPerforInfo(data) {
  return request({
    url: checkPath + "/resulteval/getEvalPerforInfo",
    method: "post",
    data: qs.stringify(data)
  });
}

// 保存分数
export function saveScore(data) {
  return request({
    url: checkPath + "/resulteval/saveScore",
    method: "post",
    data: qs.stringify(data)
  });
}

//获取考核对象列表
export function getHzObjList(data) {
  return request({
    url: checkPath + "/asessmentsummary/getEvalObjectList",
    method: "post",
    data: qs.stringify(data)
  });
}

//获取考核对象列表
export function getEvalObjectList(data) {
  return request({
    url: checkPath + "/resulteval/getEvalObjectList",
    method: "post",
    data: qs.stringify(data)
  });
}

// ---------公共-----------

// 指标树
export function getEvalTree(data) {
  return request({
    url: checkPath + "/resultselfeval/getEvalTree",
    method: "post",
    data: qs.stringify(data)
  });
}

// 指标列表
export function getEvalList(data) {
  return request({
    url: checkPath + "/resultselfeval/getEvalList",
    method: "post",
    data: qs.stringify(data)
  });
}

// ---------公布-----------

// 考核结果公布  指标类别树
export function publishTree(data) {
  return request({
    url: checkPath + "/publish/tree",
    method: "post",
    data: qs.stringify(data)
  });
}
// 考核结果公布  考核列表 待公示、公示数
export function publishcount(data) {
  return request({
    url: checkPath + "/publish/publishcount",
    method: "post",
    data: qs.stringify(data)
  });
}

// 考核结果公布 考核列表
export function publish(data) {
  return request({
    url: checkPath + "/publish/publish",
    method: "post",
    data: qs.stringify(data)
  });
}

// 获取被考核对象及得分
export function publishObj(data) {
  return request({
    url: checkPath + "/publish/obj",
    method: "post",
    data: qs.stringify(data)
  });
}

// 获取各评分单位对被考核对象的评分和自评分
export function publishEvaluate(data) {
  return request({
    url: checkPath + "/publish/evaluate",
    method: "post",
    data: qs.stringify(data)
  });
}
// 指标名及自评分、他评分
export function publishItem(data) {
  return request({
    url: checkPath + "/publish/item",
    method: "post",
    data: qs.stringify(data)
  });
}
// 公布
export function publish2(data) {
  return request({
    url: checkPath + "/publish/publish2",
    method: "post",
    data: qs.stringify(data)
  });
}

// ----------查询------------
// 获取考核列表 （考核者 和 被考核者）
export function assess(data) {
  return request({
    url: checkPath + "/result/assess",
    method: "post",
    data: qs.stringify(data)
  });
}

// 评分标准 自评分  他评分
export function resultItem(data) {
  return request({
    url: checkPath + "/result/item",
    method: "post",
    data: qs.stringify(data)
  });
}

// ----------汇总------------

// excel导出
export function exportExcel(data) {
  return request({
    url: checkPath + "/asessmentsummary/exportExcel",
    method: "post",
    data: qs.stringify(data)
  });
}

// 查询所有考核体系
export function getSummSystemList(data) {
  return request({
    url: checkPath + "/asessmentsummary/getSummSystemList",
    method: "post",
    data: qs.stringify(data)
  });
}

// 考核汇总数据
export function getSummData(data) {
  return request({
    url: checkPath + "/asessmentsummary/getSummData",
    method: "post",
    data: qs.stringify(data)
  });
}

// ----------排名------------
// 党组织考核结果排名
export function getOrgrank(data) {
  return request({
    url: checkPath + "/ranking/orgrank",
    method: "post",
    data: qs.stringify(data)
  });
}

// 指标扣分次数排名
export function getItemTwo(data) {
  return request({
    url: checkPath + "/ranking/item2",
    method: "post",
    data: qs.stringify(data)
  });
}

// 评分标准扣分次数排名
export function getItemOne(data) {
  return request({
    url: checkPath + "/ranking/item1",
    method: "post",
    data: qs.stringify(data)
  });
}

// 获取考核列表
export function getAssess(data) {
  return request({
    url: checkPath + "/ranking/assess",
    method: "post",
    data: qs.stringify(data)
  });
}

// 考核排名和扣分次数的总接口
export function rank(data) {
  return request({
    url: checkPath + "/ranking/rank",
    method: "post",
    data: qs.stringify(data)
  });
}

// 审核
export function audit(data) {
  return request({
    url: checkPath + "/selfaudit/audit",
    method: "post",
    data: qs.stringify(data)
  });
}

//审核的详情
export function getaudit(data) {
  return request({
    url: checkPath + "/selfaudit/getaudit",
    method: "post",
    data: qs.stringify(data)
  });
}

//查询体系
export function listpackage(data) {
  return request({
    url: checkPath + "/rectification/listpackage",
    method: "post",
    data: qs.stringify(data)
  });
}

//发布整改
export function save(data) {
  return request({
    url: checkPath + "/rectification/save",
    method: "post",
    data: qs.stringify(data)
  });
}

//获取整改信息
export function getpackageinfo(data) {
  return request({
    url: checkPath + "/rectification/getpackageinfo",
    method: "post",
    data: qs.stringify(data)
  });
}

//获取考核对象列表new
export function getEvalObjectListnew(data) {
  return request({
    url: checkPath + "/rectification/getEvalObjectList",
    method: "post",
    data: qs.stringify(data)
  });
}

//整改反馈保存
export function savefeedback(data) {
  return request({
    url: checkPath + "/rectification/savefeedback",
    method: "post",
    data: qs.stringify(data)
  });
}

//整改反馈详情
export function getfeedback(data) {
  return request({
    url: checkPath + "/rectification/getfeedback",
    method: "post",
    data: qs.stringify(data)
  });
}

//整改审核
export function checkaudit(data) {
  return request({
    url: checkPath + "/rectification/audit",
    method: "post",
    data: qs.stringify(data)
  });
}

//整改审核记录详情
export function listaudit(data) {
  return request({
    url: checkPath + "/rectification/listaudit",
    method: "post",
    data: qs.stringify(data)
  });
}
