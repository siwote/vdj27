// 工作汇报相关的api,部分接手前的api存放在meeting中
import request from "@/utils/request";
import qs from "qs";
const contextPath = "/horn";
const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};
const headForm = {
  "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
};

// 分页查询工作报告模板
export function getTemplate(data) {
  return request({
    url: contextPath + "/report/getTemplate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 新增工作报告模板
export function saveTemplate(data) {
  return request({
    url: contextPath + "/report/saveTemplate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 工作汇报模板删除
export function delTemplate(data) {
  return request({
    url: contextPath + "/report/delTemplate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 重新上传工作报告模板
export function updateTemplate(data) {
  return request({
    url: contextPath + "/report/updateTemplate",
    method: "post",
    data: qs.stringify(data)
  });
}

// 批量审核退回

export function batchModify(data) {
  return request({
    url: contextPath + "/report/batchModify",
    method: "post",
    data: qs.stringify(data)
  });
}
