import request from "@/utils/request";
import qs from "qs";

const contextPath = "/lhdj";
const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

/* 添加党建领航计划*/
export function addTDzzPartyBrand(data) {
  return request({
    url: contextPath + "/pilotPlan/addTDzzPartyBrand",
    headers,
    method: "post",
    data
  });
}

/* 查看党建领航计划*/
export function getTDzzPartyBrand(data) {
  return request({
    url: contextPath + "/pilotPlan/getTDzzPartyBrand",
    method: "post",
    data: qs.stringify(data)
  });
}

/* 修改党建领航计划*/
export function updateTDzzPartyBrand(data) {
  return request({
    url: contextPath + "/pilotPlan/updateTDzzPartyBrand",
    headers,
    method: "post",
    data
  });
}

/* 新增领航计划动态*/
export function addBrandDynamic(data) {
  return request({
    url: contextPath + "/pilotPlan/addBrandDynamic",
    headers,
    method: "post",
    data
  });
}

/* 分页查询查看领航计划动态*/
export function pageBrandDynamic(data) {
  return request({
    url: contextPath + "/pilotPlan/pageBrandDynamic",
    method: "post",
    data: qs.stringify(data)
  });
}

/*修改(发布,撤销发布)领航计划动态*/
export function updateBrandDynamic(data) {
  return request({
    url: contextPath + "/pilotPlan/updateBrandDynamic",
    headers,
    method: "post",
    data
  });
}

/* 查看单个领航计划动态*/
export function getBrandDynamic(data) {
  return request({
    url: contextPath + "/pilotPlan/getBrandDynamic",
    method: "post",
    data: qs.stringify(data)
  });
}

/*分页查询党建领航计划*/
export function pageTDzzPartyBrand(data) {
  return request({
    url: contextPath + "/pilotPlan/pageTDzzPartyBrand",
    method: "post",
    data: qs.stringify(data)
  });
}
// 添加(撤销)领航计划点赞
export function modifyBrandPraise(data) {
  return request({
    url: contextPath + "/pilotPlan/modifyBrandPraise",
    headers,
    method: "post",
    data
  });
}

/*删除单个动态*/
export function delBrandDynamic(data) {
  return request({
    url: contextPath + "/pilotPlan/delBrandDynamic",
    method: "post",
    data: qs.stringify(data)
  });
}

/*获取当前组织状态(我的领航计划)*/
export function getStatusByOrgCode(data) {
  return request({
    url: contextPath + "/pilotPlan/getStatusByOrgCode",
    method: "post",
    data: qs.stringify(data)
  });
}

/*删除党建领航计划*/
export function delTDzzPartyBrand(data) {
  return request({
    url: contextPath + "/pilotPlan/delTDzzPartyBrand",
    method: "post",
    data: qs.stringify(data)
  });
}

/*(提交/上报/通过/退回)品牌*/
// (1：提交/2：上报/3：通过:4：退回)品牌
export function reportAuditPartyBrand(data) {
  return request({
    url: contextPath + "/pilotPlan/reportAuditPartyBrand",
    method: "post",
    data: qs.stringify(data)
  });
}

/*查看当前组织级别*/
export function getLevelByOrgCode(data) {
  return request({
    url: contextPath + "/pilotPlan/getLevelByOrgCode",
    method: "post",
    data: qs.stringify(data)
  });
}

/*发布,撤销品牌动态*/
export function updateDynamicStatus(data) {
  return request({
    url: contextPath + "/pilotPlan/updateDynamicStatus",
    method: "post",
    data: qs.stringify(data)
  });
}
