import request from '@/utils/request';
import qs from 'qs';
const filePath = '/developing';
const headers = {
  post: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
};
// 查询步骤分类Tree
export function queryFzdyType(data) {
  return request({
    url: filePath + '/type/queryFzdyType',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 保存步骤分类
export function saveFzdyType(data) {
  return request({
    url: filePath + '/type/saveFzdyType',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 删除步骤分类
export function delFzdyType(data) {
  return request({
    url: filePath + '/type/delFzdyType',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 分页查询模板列表
export function pageTemplate(data) {
  return request({
    url: filePath + '/temp/pageTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 保存模板
export function saveTemplate(data) {
  return request({
    url: filePath + '/temp/saveTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 删除模板
export function delTemplate(data) {
  return request({
    url: filePath + '/temp/delTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询要求/书写配置信息
export function queryStandardAsk(data) {
  return request({
    url: filePath + '/steup/queryStandardAsk',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 保存要求/书写配置信息
export function saveStandardAsk(data) {
  return request({
    url: filePath + '/steup/saveStandardAsk',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 分页查询提醒规则列表
export function pageRemindRule(data) {
  return request({
    url: filePath + '/remindrule/pageRemindRule',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 删除提醒规则
export function delRemindRuleById(data) {
  return request({
    url: filePath + '/remindrule/delRemindRuleById',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询规则详情
export function queryRuleById(data) {
  return request({
    url: filePath + '/remindrule/queryRuleById',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询提醒点下拉框值
export function queryFzdyDict(data) {
  return request({
    url: filePath + '/remindrule/queryFzdyDict',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 保存提醒规则
export function saveRemindRule(data) {
  return request({
    url: filePath + '/remindrule/saveRemindRule',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 分页查询消息模板列表
export function pageMsgTemplate(data) {
  return request({
    url: filePath + '/remindrule/pageMsgTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 删除消息模板
export function delMsgTemplate(data) {
  return request({
    url: filePath + '/remindrule/delMsgTemplate',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 保存消息模板
export function saveMsgTemp(data) {
  return request({
    url: filePath + '/remindrule/saveMsgTemp',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员管理
// 党员类别下拉列表
export function listDylbSel(data) {
  return request({
    url: filePath + '/baseinfo/listDylbSel',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员信息详情
export function queryFzdyById(data) {
  return request({
    url: filePath + '/baseinfo/queryFzdyById',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员管理分页
export function queryPageFzdy(data) {
  return request({
    url: filePath + '/baseinfo/queryPageFzdy',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员信息作废
export function delFzdy(data) {
  return request({
    url: filePath + '/baseinfo/delFzdy',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员信息恢复
export function recoverFzdy(data) {
  return request({
    url: filePath + '/baseinfo/recoverFzdy',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员组织关系转接 转出
export function fzdyZzgxzj(data) {
  return request({
    url: filePath + '/baseinfo/fzdyZzgxzj',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 新增选项集合
export function listAddSel(data) {
  return request({
    url: filePath + '/baseinfo/listAddSel',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员保存
export function saveFzdy(data) {
  return request({
    url: filePath + '/baseinfo/saveFzdy',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员组织关系转接列表
export function queryZzgxAccept(data) {
  return request({
    url: filePath + '/baseinfo/queryZzgxAccept',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员组织关系转接审核
export function receiveFzdy(data) {
  return request({
    url: filePath + '/baseinfo/receiveFzdy',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员递交入党申请书
export function submitFzdy(data) {
  return request({
    url: filePath + '/step/process',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员确认进入下一步
export function nextFzdy(data) {
  return request({
    url: filePath + '/step/nextStage',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发展党员是否可以编辑
export function getMap(data) {
  return request({
    url: filePath + '/step/getmap',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 审核党员列表
export function auditPage(data) {
  return request({
    url: filePath + '/step/auditPage',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 审核党员列表
export function enterSpecificStage(data) {
  return request({
    url: filePath + '/step/enterSpecificStage',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 指定修改党员某一步信息
export function audit(data) {
  return request({
    url: filePath + '/step/audit',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 巡查党员信息
export function pageStep(data) {
  return request({
    url: filePath + '/step/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询申请党员的个人信息
export function getStep(data) {
  return request({
    url: filePath + '/step/get',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询申请党员的个人信息
export function listGzgw(data) {
  return request({
    url: filePath + '/step/listgzgw',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 打包资料
export function datapackaging(data) {
  return request({
    url: filePath + '/step/datapackaging',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 资料导查询
export function listfiles(data) {
  return request({
    url: filePath + '/step/listfiles',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 导出打包接口
export function fileexport(data) {
  return request({
    url: filePath + '/step/fileexport',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 导出打包合成PDF
export function mergePdfFiles(data) {
  return request({
    url: filePath + '/step/mergePdfFiles',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取发展党员5环节数据
export function listlink(data) {
  return request({
    url: filePath + '/baseinfo/listlink',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 修改发展党员5环节
export function updatelink(data) {
  return request({
    url: filePath + '/baseinfo/updatelink',
    method: 'post',
    headers: headers,
    data: data
  });
}

// 发展党员：正式党员步骤延迟党员预备期
export function addHisLink(data) {
  return request({
    url: filePath + '/baseinfo/addHisLink',
    method: 'post',
    headers: headers,
    data: data
  });
}

// 分页查询发展党员：正式党员步骤延迟党员预备期
export function pageHisLink(data) {
  return request({
    url: filePath + '/baseinfo/pageHisLink',
    method: 'post',
    data: qs.stringify(data)
  });
}
// 修改发展党员5环节
export function getSj(data) {
  return request({
    url: filePath + '/step/getSj',
    method: 'post',
    data: qs.stringify(data)
  });
}
