import request from "@/utils/request";
import qs from "qs";

const loginPath = "/horn";

const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

// 退回会议
export function rollbackMeeting(data) {
  return request({
    url: loginPath + "/meeting/rollbackMeeting",
    method: "post",
    data: qs.stringify(data)
  });
}
