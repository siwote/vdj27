import request from '@/utils/request'
import qs from 'qs'
const contextPath = '/power'
const contextPathl = '/lhdj'

/* hbl 授权角色查询*/
export function getRoles(data) {
  return request({
    url: contextPath + '/role/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* 用户查询 */
// export function groupRoles(data) {
//   return request({
//     url: contextPath + '/organ/listAll',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }
export function groupRoles(data) {
  return request({
    url: contextPathl + '/dzzorg/listAll',
    method: 'post',
    data: qs.stringify(data)
  })
}

export function listAllUnder(data) {
  return request({
    url: contextPathl + '/dzzorg/listAllUnder',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* hbl 授权授权*/
export function getPower(data) {
  return request({
    url: contextPath + '/menu/treeMenu',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* hbl 授权保存*/
export function severPower(data) {
  return request({
    url: contextPath + '/menu/saveAuth',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* hbl 数据授权选中样式 */
// export function typeCheck(data) {
//   return request({
//     url: contextPath + '/menu/listIds',
//     // url: contextPath + '/organ/listAll',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }

export function typeCheck(data) {
  return request({
    url: contextPathl + '/dzzorg/listIds ',
    // url: contextPath + '/organ/listAll',
    method: 'post',
    data: qs.stringify(data)
  })
}

/* hbl 数据授权保存 */
// export function saveTypeCheck(data) {
//   return request({
//     url: contextPath + '/menu/saveAuthClass',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }

export function saveTypeCheck(data) {
  return request({
    url: contextPathl + '/dzzorg/saveAuthClass',
    method: 'post',
    data: qs.stringify(data)
  })
}
// /* hbl 用户树的子组织查询*/
// export function getOrganTree(data) {
//   return request({
//     url: contextPath + '/organ/listOrganChildNode',
//     method: 'post',
//     data: qs.stringify(data)
//   })
// }
export function getOrganTree(data) {
  return request({
    url: contextPathl + '/selection/listOrganChildNode',
    method: 'post',
    data: qs.stringify(data)
  })
}

// 登录日志统计
export function getLoginLog(data) {
  return request({
    url: contextPathl + '/dzzorg/loginLog',
    method: 'post',
    data: qs.stringify(data)
  })
}
