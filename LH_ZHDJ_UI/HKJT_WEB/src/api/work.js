import request from '@/utils/request'
import qs from 'qs'

const duesUrl = '/developing'

export function listwork(data) {
    return request({
        url: duesUrl + '/dfsy/list',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function savework(data) {
    return request({
        url: duesUrl + '/dfsy/save',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function deletework(data) {
    return request({
        url: duesUrl + '/dfsy/delete',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function movework(data) {
    return request({
        url: duesUrl + '/dfsy/move',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function elistwork(data) {
    return request({
        url: duesUrl + '/gzjf/list',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function setlistamount(data) {
    return request({
        url: duesUrl + '/gzjf/setlistamount',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function savesummary(data) {
    return request({
        url: duesUrl + '/gzjf/savesummary',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function auditpage(data) {
    return request({
        url: duesUrl + '/gzjf/auditpage',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function application(data) {
    return request({
        url: duesUrl + '/gzjf/auditworkfundingapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function applicationpage(data) {
    return request({
        url: duesUrl + '/gzjf/applicationpage',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function listapprovallog(data) {
    return request({
        url: duesUrl + '/gzjf/listapprovallog',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function getworkfundingapplication(data) {
    return request({
        url: duesUrl + '/gzjf/getworkfundingapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function fundingapplication(data) {
    return request({
        url: duesUrl + '/gzjf/saveworkfundingapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function findflie(data) {
    return request({
        url: duesUrl + '/gzjf/savereimbursementfundingapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function auditworkfundingapplication(data) {
    return request({
        url: duesUrl + '/gzjf/auditworkfundingapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function settlement(data) {
    return request({
        url: duesUrl + '/gzjf/settlement',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function jfgetsummary(data) {
    return request({
        url: duesUrl + '/gzjf/getsummary',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function getclassinfo(data) {
    return request({
        url: duesUrl + '/gzjf/getclassinfo',
        method: 'post',
        data: qs.stringify(data)
    })
}
//党费使用申请保存
export function savepartyapplication(data) {
    return request({
        url: duesUrl + '/dfsy/savepartyapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}

//党费列表数据源
export function listpartyfeeapplicationform(data) {
    return request({
        url: duesUrl + '/dfsy/listpartyfeeapplicationform',
        method: 'post',
        data: qs.stringify(data)
    })
}

//党费申请审核
export function auditpartyfeeapplication(data) {
    return request({
        url: duesUrl + '/dfsy/auditpartyfeeapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
//党费报销申请保存
export function savereimbursementfundingapplication(data) {
    return request({
        url: duesUrl + '/dfsy/savereimbursementfundingapplication',
        method: 'post',
        data: qs.stringify(data)
    })
}
export function lowerpartyfeepage(data) {
    return request({
        url: duesUrl + '/dfsy/lowerpartyfeepage',
        method: 'post',
        data: qs.stringify(data)
    })
}
// 党组织获取下拨党费余额以及自留党费余额
export function getbalance(data) {
    return request({
        url: duesUrl + '/dfsy/getbalance',
        method: 'post',
        data: qs.stringify(data)
    })
}
// 下拨党费列表汇总 
export function getsummary(data) {
    return request({
        url: duesUrl + '/dfsy/getsummary',
        method: 'post',
        data: qs.stringify(data)
    })
}
// 下拨党费保存
export function savelowerpartyfee(data) {
    return request({
        url: duesUrl + '/dfsy/savelowerpartyfee',
        method: 'post',
        data: qs.stringify(data)
    })
}