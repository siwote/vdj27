import request from "@/utils/request";
import qs from "qs";
const powerPath = "/power";

// 免登陆接口
export function loginFree(data) {
  return request({
    url: powerPath + "/freeLogin/loginFree",
    method: "post",
    data: qs.stringify(data)
  });
}
