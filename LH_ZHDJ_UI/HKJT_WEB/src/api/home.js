import request from "@/utils/request";
import qs from "qs";

const headers = {
  post: {
    "Content-Type": "application/json;charset=UTF-8"
  }
};

const duesUrl = "/developing";
const urlh = "/horn";
const urlhdj = "/lhdj";
export function gettime(data) {
  return request({
    url: duesUrl + "/home/get",
    method: "post",
    data: qs.stringify(data)
  });
}
export function getlist(data) {
  return request({
    url: duesUrl + "/home/list",
    method: "post",
    data: qs.stringify(data)
  });
}
export function modi(data) {
  return request({
    url: duesUrl + "/home/modi",
    method: "post",
    data: qs.stringify(data)
  });
}

// 书记和党员查询待办事项列表
export function pagelist(data) {
  return request({
    url: urlh + "/todo/pagelist",
    method: "post",
    data: qs.stringify(data)
  });
}

// 修改会议待办状态
export function updatestatus(data) {
  return request({
    url: urlh + "/todo/updatestatus",
    method: "post",
    data: qs.stringify(data)
  });
}

// 待办事项操作-书记
export function handleToDo(data) {
  return request({
    url: urlh + "/todo/handleToDo",
    method: "post",
    data: data,
    headers: headers
  });
}

// 检测是否需要参加
export function checkMeetingStatus(data) {
  return request({
    url: urlh + "/todo/checkMeetingStatus",
    method: "post",
    data: qs.stringify(data)
  });
}

// 待办事项操作-党员
export function handleTAttend(data) {
  return request({
    url: urlh + "/todo/handleTAttend",
    method: "post",
    data: qs.stringify(data)
  });
}

// 活跃
export function getActiveSituation(data) {
  return request({
    url: urlhdj + "/dzzorg/getActiveSituation",
    method: "post",
    data: qs.stringify(data)
  });
}

// 总支工作台
export function getOrgGeneral(data) {
  return request({
    url: urlhdj + "/dzzorg/getOrgGeneral",
    method: "post",
    data: qs.stringify(data)
  });
}

// 支部工作台
export function getBranchGeneral(data) {
  return request({
    url: urlhdj + "/dzzorg/getBranchGeneral",
    method: "post",
    data: qs.stringify(data)
  });
}

// 快捷入口查询
export function listFunction(data) {
  return request({
    url: urlh + "/workbench/listFunction",
    method: "post",
    data: qs.stringify(data)
  });
}

// 快捷入口保存
export function saveFunction(data) {
  return request({
    url: urlh + "/workbench/saveFunction",
    method: "post",
    data: qs.stringify(data)
  });
}

// 快捷入口删除
export function delFunction(data) {
  return request({
    url: urlh + "/workbench/delFunction",
    method: "post",
    data: qs.stringify(data)
  });
}

// 指标监测查询
export function indexMonitor(data) {
  return request({
    url: urlh + "/workbench/indexMonitor",
    method: "post",
    data: qs.stringify(data)
  });
}

// 支部分页查询
export function workbenchpage(data) {
  return request({
    url: urlh + "/workbench/page",
    method: "post",
    data: qs.stringify(data)
  });
}

// 分页查询
export function listDynamicsType(data) {
  return request({
    url: urlh + "/workbench/listDynamicsType",
    method: "post",
    data: qs.stringify(data)
  });
}

// 待办事项分页-未办
export function queryList(data) {
  return request({
    url: urlh + "/workbench/todo/query",
    method: "post",
    data: data,
    headers: headers
  });
}

// 获取事项类型
export function categoryType() {
  return request({
    url: urlh + "/workbench/todo/category",
    method: "get"
  });
}

// 党组织攻坚项目统计
export function crucialProjectStatistical(data) {
  return request({
    url: urlh + "/pmProject/crucialProjectStatistical",
    method: "post",
    data: qs.stringify(data)
  });
}

// 通知公告
export function page(data) {
  return request({
    url: "urc/notice/page",
    method: "post",
    data: qs.stringify(data)
  });
}

// 通知公告详情
export function noticeget(data) {
  return request({
    url: "urc/notice/get",
    method: "post",
    data: qs.stringify(data)
  });
}
