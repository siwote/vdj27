import request from '@/utils/request';

import qs from 'qs';

const contextPath = '/lhdj';
const headers = {
  post: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
};
/* hbl 查询党小组信息*/
export function getMaPartyGroup(data) {
  return request({
    url: contextPath + '/partygroup/getPartyGroup',
    method: 'post',
    data: qs.stringify(data)
  });
}

/* hbl 查询可选择的党小组成员*/
export function getOptionalMember(data) {
  return request({
    url: contextPath + '/partygroup/getOptionalMember',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 增加党小组
export function addPartyGroup(data) {
  return request({
    url: contextPath + '/partygroup/addPartyGroup',
    method: 'post',
    data: data,
    headers: headers
  });
}

// 删除党小组
export function delPartyGroup(data) {
  return request({
    url: contextPath + '/partygroup/delPartyGroup',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 修改党小组信息
export function getPartyJob(data) {
  return request({
    url: contextPath + '/partygroup/modifyPartyGroup',
    method: 'post',
    data: data,
    headers: headers
  });
}

// 查询党小组信息列表
export function queryPartyGroupList(data) {
  return request({
    url: contextPath + '/partygroup/queryPartyGroupList',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 换届提醒配置查询
export function queryElectionConfig(data) {
  return request({
    url: contextPath + '/election/queryElectionConfig',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 换届提醒配置保存
export function saveElectionConfig(data) {
  return request({
    url: contextPath + '/election/saveElectionConfig',
    method: 'post',
    data: data,
    headers: headers
  });
}

// 换届统计头部
export function getElecInfoByOrg(data) {
  return request({
    url: contextPath + '/dzzorg/getElecInfoByOrg',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 换届分页
export function getdzzpageElection(data) {
  return request({
    url: contextPath + '/dzzorg/pageElection',
    method: 'post',
    data: qs.stringify(data)
  });
}

// --------------------------以上换届相关已作废

// 分页查询组织换届信息
export function page(data) {
  return request({
    url: contextPath + '/transition/page',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询最新一条的换届信息
export function getLatestHJXXInfo(data) {
  return request({
    url: contextPath + '/transition/getLatestHJXXInfo',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 查询角色人员
export function pageRole(data) {
  return request({
    url: contextPath + '/transition/pageRole',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 换届维护
export function add(data) {
  return request({
    url: contextPath + '/transition/add',
    method: 'post',
    data,
    headers
  });
}

// 换届维护
export function update(data) {
  return request({
    url: contextPath + '/transition/update',
    method: 'post',
    data,
    headers
  });
}

// 工作台统计数据
export function statisticalHJXX(data) {
  return request({
    url: contextPath + '/transition/statisticalHJXX',
    method: 'post',
    data: qs.stringify(data)
  });
}
