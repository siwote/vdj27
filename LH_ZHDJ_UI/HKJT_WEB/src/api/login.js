import request from '@/utils/request';
import qs from 'qs';
const loginPath = '/power';
const loginPathu = '/urc';
const loginPathl = '/lhdj';

import apiUrl from '@/utils/apiUrl';
const { urlPath } = apiUrl;
const { urcPath } = urlPath;

// 免登陆接口
export function loginToken(data) {
  return request({
    url: loginPathu + '/authentication/loginToken',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 登录
export function loginto(data) {
  return request({
    url: loginPathu + '/authentication/login',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 登录cookie
export function makeSessionDto(data) {
  return request({
    url: loginPathl + '/dzzorg/makeSessionDto',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 通过用户userid查询菜单
export function getAuthMenu(data) {
  return request({
    url: loginPathu + '/function/list',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function verification(data) {
  return request({
    url: loginPathu + '/authentication/captcha',
    method: 'post',
    data: qs.stringify(data)
  });
}
export function login(data) {
  return request({
    url: loginPathu + '/authentication/login',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getInfo(token) {
  let data = {
    token: token
  };
  return request({
    url: loginPath + '/user/info',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function logout(data) {
  return request({
    url: loginPathu + '/authentication/loginout',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function authMenuButton(data) {
  return request({
    url: loginPath + '/menu/authMenuButton',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getCode(data) {
  return request({
    url: loginPath + '/user/getCode',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getAunnt(data) {
  return request({
    url: loginPath + '/user/recoverPwd',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 通过userid查询配置
export function baseset(data) {
  return request({
    url: loginPathu + '/baseset/compid',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发送短信
export function sendMessage(data) {
  return request({
    url: loginPathu + '/new/authentication/sendSMSCode',
    method: 'post',
    data: qs.stringify(data)
  });
}

// 发送短信的登录接口

export function loginMessage(data) {
  return request({
    url: loginPathu + '/new/authentication/login',
    method: 'post',
    data: qs.stringify(data)
  });
}






















export function skin(data) {
  return request({
    url: `${urcPath}/authentication/skin?account=${data.account}`,
    method: 'get'
  });
}


// 校验手机&邮箱是否唯一
export function checkExInfo(data) {
  return request({
    url: `${urcPath}/user/checkExInfo`,
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取系统参数详情
export function getParam(data) {
  return request({
    url: `${urcPath}/param/get`,
    method: 'post',
    data: qs.stringify(data)
  });
}


// 获取消息数目
export function countMsgNumByStatus(data) {
  return request({
    url: `${urcPath}/msg/countMsgNumByStatus`,
    method: 'post',
    data: qs.stringify(data)
  });
}


// 发送验证码
export function sendVerify(data) {
  return request({
    url: `${urcPath}/userVerify/send`,
    method: 'post',
    data: qs.stringify(data)
  });
}


// 校验验证码
export function verifyRetreve(data) {
  return request({
    url: `${urcPath}/userVerify/verifyRetreve`,
    method: 'post',
    data: qs.stringify(data)
  });
}

//
export function updatekAuthStatus(data) {
  return request({
    url: `${urcPath}/userVerify/updatekAuthStatus`,
    method: 'post',
    data: qs.stringify(data)
  });
}

// 校验是否认证
export function checkAuthStatus(data) {
  return request({
    url: `${urcPath}/userVerify/checkAuthStatus`,
    method: 'post',
    data: qs.stringify(data)
  });
}


export function checkUserEmailUrl(data) {
  return request({
    url: `${urcPath}/userVerify/checkUserEmailUrl`,
    method: 'post',
    data: qs.stringify(data)
  });
}



export function sendAuthEmail(data) {
  return request({
    url: `${urcPath}/userVerify/sendAuthEmail`,
    method: 'post',
    data: qs.stringify(data)
  });
}



export function updatekAuthStatusByEmail(data) {
  return request({
    url: `${urcPath}/userVerify/updatekAuthStatusByEmail`,
    method: 'post',
    data: qs.stringify(data)
  });
}


// 点击菜单
export function saveLog(data) {
  return request({
    url: `${urcPath}/log/saveLog`,
    method: 'post',
    data: qs.stringify(data)
  });
}

// 获取登陆设置
export function getCacheLoginStyle(data) {
  return request({
    url: `${urcPath}/company/getCacheLoginStyle`,
    method: 'post',
    data: qs.stringify(data)
  });
}


// 埋点
export function dataBuryingPointAdd(data) {
  return request({
    url: `${urcPath}/dataBuryingPoint/add`,
    method: 'post',
    data: qs.stringify(data)
  });
}

export function dataBuryingPointModify(data) {
  return request({
    url: `${urcPath}/dataBuryingPoint/modify`,
    method: 'post',
    data: qs.stringify(data)
  });
}