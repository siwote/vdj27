import Vue from 'vue';
import Vuex from 'vuex';
import auth from '@/utils/auth';
//挂载Vuex
Vue.use(Vuex);

//创建VueX对象
const store = new Vuex.Store({
  state: {
    condition: {},
    //存放的键值对就是所要管理的状态
    name: '898989898989',
    obj: {},
    // 发布会议存值
    params: {
      typeNum: 0,
      starttime: '',
      endtime: '',
      likeStr: '',
      mrtype: '',
      pager: {
        pageNo: 1,
        pageSize: 15,
        userSort: true,
        sort: 'createtime desc'
      },
      orgcode: auth.getCookie('orgcode')
    },
    //  我的会议存志
    meetingData: {
      typeNum: 0,
      starttime: '',
      endtime: '',
      likeStr: '',
      mrtype: '',
      pager: {
        pageNo: 1,
        pageSize: 15,
        userSort: true,
        sort: 'createtime desc'
      },
      orgcode: auth.getCookie('orgcode'),
      userid: auth.getCookie('Account')
    },
    search: ''
  },
  mutations: {
    setName(state, name) {
      state.name = name;
    },
    setValue(state, params) {
      state.params = params;
    },
    setmeetValue(state, meetingData) {
      state.meetingData = meetingData;
    },
    setmeetCondition(state, condition) {
      state.condition = condition;
    },
    setSearch(state, data) {
      state.search = data;
    }
  },
  actions: {}
});

export default store;
