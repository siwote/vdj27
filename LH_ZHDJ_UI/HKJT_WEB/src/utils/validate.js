/**
 * Created by jiachenpan on 16/11/18.
 * 部分表单的校验
 */

/* 合法uri */
export function validateURL(textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return urlregex.test(textval)
}

/* 小写字母 */
export function validateLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母 */
export function validateUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母 */
export function validatAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}
/* 手机号码 */
export function validatPhone(str) {
  const reg = /^1[3|4|5|8][0-9]\d{4,8}$/
  return reg.test(str)
}
/* 密码 */
export function validatPwd(str) {
  const reg = /^[a-zA-Z0-9]{6}$/
  return reg.test(str)
}

/* 起止时间校验 */
export function validatTime(value) {
  var message = '';
  if (value&&value.length != 0) {
    var endY = parseInt(value[1].split("-")[0]);
    var startY = parseInt(value[0].split("-")[0]);
    var endM = parseInt(value[1].split("-")[1]);
    var startM = parseInt(value[0].split("-")[1]);
    var endD = parseInt(value[1].split("-")[2]);
    var startD = parseInt(value[0].split("-")[2]);
    var endTime = Date.parse(new Date(endY, endM, endD));
    var startTime = Date.parse(new Date(startY, startM, startD));
    var endDate = Date.parse(new Date(startY + 2, startM, startD));
    if (endTime == startTime) {
      message = "任务起止时间不可在同一天";
    } else if (endTime > endDate) {
      message = "时间跨度在两年内";
    } else {
      message = '';
    }
  } else {
    message = "请先选择任务起止时间";
  }
  return message;
}

/* 周期性截止时间校验 */
export function validatendTime(value, that) {
  var rg = /^[0-9]+\d*$/, message;
  if (value) {
    if (rg.test(value)) {
      if (that.form.rules == "Y" && (value > 365 || value <= 0)) {
        message =
          "每年自动产生任务,截止天数输入值最大为365,最小值为1";
      } else if (
        that.form.rules == "Q" &&
        (value > 90 || value <= 0)
      ) {

        message = "每季度自动产生任务,截止天数输入值最大为90,最小值为1";
      } else if (
        that.form.rules == "M" &&
        (value > 28 || value <= 0)
      ) {
        message =
          "每月自动产生任务,截止天数输入值最大为28,最小值为1";
      } else if (
        that.form.rules == "W" &&
        (value > 7 || value <= 0)
      ) {
        message =
          "每周自动产生任务,截止天数输入值最大为7,最小值为1";
      } else {
        message = '';
      }
    } else {
      message = "天数只能为正整数";
    }
  } else {
    if (that.form.rules == "D") {
      message = '';
    } else {
      message = "请选择任务截止时间";
    }
  }
  return message;
}

/* 周期任务生成周期校验 */
export function validatSTime(value, that) {
  var rg = /^[1-9]+\d*$/, message;
  if (value) {
    if (value == "D") {
      message = '';
    } else {
      if (that.form.day) {
        if (rg.test(that.form.day)) {
          if (that.form.day >= 1) {
            if (value == "Y" && that.form.day > 365) {
              message =
                "每年自动产生任务,天数输入值最大为365";
            } else if (value == "Q" && that.form.day > 90) {
              message =
                "每季度自动产生任务,天数输入值最大为90"

            } else if (value == "M" && that.form.day > 28) {
              message =
                "每月自动产生任务,天数输入值最大为28"
                ;
            } else if (value == "W" && that.form.day > 7) {
              message =
                "每周自动产生任务,天数输入值最大为7"
                ;
            } else {
              message = '';
            }
          } else {
            message = "天数最小值为1";
          }
        } else {
          message = "天数只能为正整数";
        }
      } else {
        message = "请选择任务生成周期";
      }
    }
  } else {
    message = "任务生成周期选择有误";
  }
  return message;
}

/* 周期性反馈时间校验 */
export function validatBackTime(value, that) {
  var rg = /^[1-9]+\d*$/, message;
  if (that.backTime.rules != "D") {
    if (value) {
      if (rg.test(value)) {
        if (that.backTime.rules == "Q" && value > Number(that.backTime.every) * 90) {
          message = "每季度反馈,天数输入值最大为" + Number(that.backTime.every) * 90 + '天';
        } else if (
          that.backTime.rules == "M" &&
          value > Number(that.backTime.every) * 28
        ) {
          message = "每月反馈,天数输入值最大为" + Number(that.backTime.every) * 28 + '天';
        } else if (
          that.backTime.rules == "W" &&
          value > Number(that.backTime.every) * 7
        ) {
          message = "每周反馈,天数输入值最大为" + Number(that.backTime.every) * 7 + '天';
        } else {
          message = '';
        }
      } else {
        message = "天数只能为大于0的正整数";
      }
    } else {
      message = "必填项";
    }
  } else {
    message = "";
  }
  return message;
}

/* 分解任务分解策略时间校验 */
export function validatDay(value, that) {
  var rg = /^[0-9]+\d*$/, message;
  if (value) {
    if (rg.test(value)) {
      if (
        that.form.decomposeTime == "year" &&
        (value > 365 || value <= 0)
      ) {
        message = "每年后截止天数输入值最大为365,最小值为1";
      } else if (
        that.form.decomposeTime == "quarter" &&
        (value > 90 || value <= 0)
      ) {
        message = "每季度后截止天数输入值最大为90,最小值为1";
      } else if (
        that.form.decomposeTime == "month" &&
        (value > 28 || value <= 0)
      ) {
        message = "每月截止天数输入值最大为28,最小值为1";
      } else if (
        that.form.decomposeTime == "halfyear" &&
        (value > 181 || value <= 0)
      ) {
        message = "每半年截止天数输入值最大为181,最小值为1";
      } else {
        message = "";
      }
    } else {
      message = "天数只能为正整数";
    }
  } else if (that.form.decomposeTime && !value) {
    message = "请填写截止时间";
  }
  return message;
}

export function formDate() {
        var myDate = new Date();
        var new_year = myDate.getFullYear()
   return new Date(new_year,11,31)
}

export function formDatef() {
  var myDate = new Date();
  var new_year = myDate.getFullYear()
return new Date(new_year,0,1)
}
export const Throttle = (fn, t) => {
  let last;
  let timer;
  let interval = t || 500;
  return function () {
      let args = arguments;
      let now = +new Date();
      if (last && now - last < interval) {
          clearTimeout(timer);
          timer = setTimeout(() => {
              last = now;
              fn.apply(this, args);
          }, interval);
      } else {
          last = now;
          fn.apply(this, args);
      }
  }
};
