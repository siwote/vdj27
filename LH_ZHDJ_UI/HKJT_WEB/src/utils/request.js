// 请求拦截和响应拦截

import axios from 'axios';
import router from '../router';
// import { Message, MessageBox } from "element-ui";
import { oneMessage } from '@/utils/resetMessage.js';
import auth from '@/utils/auth';

// import { logout } from '@/api/login';

// 创建axios实例
//测试地址
// window.host = 'http://10.1.50.192:9999'
// window.requestUrl = 'http://10.1.50.192:9052'
// window.mapUrl = 'http://10.1.50.192:9053'

//线上地址10.1.50.193 8886
// window.host = 'http://172.20.3.114:9999'
window.requestUrl = process.env.BASE_API;
// window.mapUrl = 'http://172.20.11.117:9053'
const service = axios.create({
  timeout: 600000
});

// request请求拦截器
service.interceptors.request.use(
  config => {
    if (auth.getCookie('Session-Id')) {
      config.headers['sessionid'] = auth.getCookie('Session-Id'); // 让每个请求携带自定义的值
    }
    return config;
  },
  error => {
    // Do something with request error
    console.log(error); // for debug
    Promise.reject(error);
  }
);
var time = 0;
// respone响应拦截器
service.interceptors.response.use(
  response => {
    const res = response.data;
    switch (res.code) {
      case '000000':
        /*响应成功*/
        return res;

      case 'PW00001':
        auth.clearLogin();
        // window.open('http://10.1.50.192:9060/mydesk/index', "_top");
        router.replace({
          name: 'login'
        });
        oneMessage({
          message: res.message,
          type: 'error',
          duration: 1 * 1000
        });
        break;

      case 'PW00002':
        router.replace({
          name: 'notFount'
        });
        oneMessage({
          message: res.message,
          type: 'error',
          duration: 1 * 1000
        });
        break;

      case 'PW00003':
        auth.clearLogin();
        router.replace({
          name: 'login'
        });
        time = time + 1;
        if (time == 1) {
          oneMessage({
            message: res.message,
            type: 'error',
            duration: 3 * 1000
          });
        }
        break;

      case '666666':
        oneMessage({
          message: res.message,
          type: 'warning',
          duration: 3 * 1000
        });
        break;

      default:
        oneMessage({
          message: res.message,
          type: 'error',
          duration: 2 * 1000
        });
        break;
    }
    return Promise.reject(new Error('error'));
  },
  err => {
    // 这里是返回状态码不为200时候的错误处理
    if (err && err.response) {
      switch (err.response.status) {
        case 400:
          err.message = '请求错误';
          break;

        case 401:
          err.message = '未授权，请登录';
          break;

        case 403:
          err.message = '拒绝访问';
          break;

        case 404:
          router.replace({
            path: '*'
          });
          break;

        case 408:
          err.message = '请求超时';
          break;

        case 500:
          err.message = '服务器内部错误';

          break;

        case 501:
          err.message = '服务未实现';
          break;

        case 502:
          err.message = '系统出错，请联系管理员';
          break;

        case 503:
          err.message = '服务不可用';
          break;

        case 504:
          err.message = '系统出错，请联系管理员';
          break;

        case 505:
          err.message = 'HTTP版本不受支持';
          break;

        default:
          err.message = '网络错误';
          break;
      }
    } else {
      err.message = '网络出现问题，请稍后再试';
    }
    oneMessage({
      message: err.message,
      type: 'error',
      duration: 2 * 1000
    });
    return Promise.reject(err);
  }
);
export default service;
