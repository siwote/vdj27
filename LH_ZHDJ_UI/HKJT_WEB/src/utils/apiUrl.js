class ApiUrl {
    constructor (){
        this.urlPath = {
            appPath: '/urc/appbusiness',
            appFormPath: '/urc/appform',
            appFormData: '/urc/appformdata',
            formListPath: '/urc/appfunction',
            qrcode: '/urc/qrcode',
            treePath: '/urc/function',
            urcPath: '/urc',
            portalPath: '/portal',
            evalPath: '/eval',
            actionAvatar: '/urc/img/upload', // 组织上传头像
            addAttachment: '/urc/attachment/addAttachment', // 上传附件
            dataImport: '/urc/appformdata/dataImport', // appbuild
            setPreferences: '/urc/baseset/addAndUpdate', // 系统便好设置
            appFormFile: '/urc/appformdata/image_capacity'
        };
    }
}

const apiUrl = new ApiUrl();

export default apiUrl;