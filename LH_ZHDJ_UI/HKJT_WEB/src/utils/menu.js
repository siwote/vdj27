// 动态菜单 动态路由的数据处理
import lazyLoading from '@/utils/lazyLoading';

class Menu {
  constructor() {
    this.news = {
      fctname: '消息通知',
      fcturl: 'news',
      urlflag: 0,
      classify: 0
    };
    this.person = {
      fctname: '个人中心',
      fcturl: 'personCenter',
      urlflag: 0,
      classify: 0
    };
    this.email = {
      fctname: '绑定邮箱',
      fcturl: 'bindEmail',
      urlflag: 0,
      classify: 0
    };

    this.basicNew = {
      fctname: '三会一课详情',
      fcturl: 'affairsManagement/orgLife/sessions/basicLef',
      urlflag: 0,
      classify: 0
    };
    this.themepartyday = {
      fctname: '主题党日模板',
      fcturl: 'affairsManagement/orgLife/themepartyday',
      urlflag: 0,
      classify: 0
    };
    this.delifemeeting = {
      fctname: '民主生活会模板',
      fcturl: 'affairsManagement/orgLife/delifemeeting',
      urlflag: 0,
      classify: 0
    };
    this.readyTaskDetails = {
      fctname: '已发任务详情',
      fcturl: 'task/readyTask/readyTaskDetailsLi',
      urlflag: 0,
      classify: 0
    };
    this.myTask = {
      fctname: '我的任务',
      fcturl: 'task/myTask/myTaskDetails',
      urlflag: 0,
      classify: 0
    };
    this.draftTask = {
      fctname: '草稿箱',
      fcturl: 'task/draftTask/draftTaskDetails',
      urlflag: 0,
      classify: 0
    };
    this.giveTaskDetails = {
      fctname: '下发任务审核详情',
      fcturl: 'task/giveTask/giveTaskDetails',
      urlflag: 0,
      classify: 0
    };
    this.lookTaskDetails = {
      fctname: '上报任务审核详情',
      fcturl: 'task/lookTask/lookTaskDetails',
      urlflag: 0,
      classify: 0
    };
    this.aboutTask = {
      fctname: '我相关的',
      fcturl: 'task/aboutTask/aboutTaskDetails',
      urlflag: 0,
      classify: 0
    };
    this.taskPatrol = {
      fctname: '工作巡查',
      fcturl: 'task/taskPatrol/taskPatrolDetails',
      urlflag: 0,
      classify: 0
    };
    this.PatrolList = {
      fctname: '组织生活详情',
      fcturl: 'affairsManagement/orgLife/organizationalLife/PatrolList',
      urlflag: 0,
      classify: 0
    };
    this.reportProject = {
      fctname: '项目进度汇报',
      fcturl: 'project/aboutProject/aboutProjectReport',
      urlflag: 0,
      classify: 0
    };
    this.addProject = {
      fctname: '项目发布',
      fcturl: 'project/myProject/addProject',
      urlflag: 0,
      classify: 0
    };
    this.endProject = {
      fctname: '项目结项',
      fcturl: 'project/myProject/endProject',
      urlflag: 0,
      classify: 0
    };
    this.myProject = {
      fctname: '项目查看',
      fcturl: 'project/myProject/myProjectDetails',
      urlflag: 0,
      classify: 0
    };
    // 头部搜索
    this.search = {
      fctname: '搜索',
      fcturl: 'search',
      urlflag: 0,
      classify: 0
    };
    /*
     * this.notice = {
     *   fctname: "通知公告",
     *   fcturl: "allNotice",
     *   urlflag: 0,
     *   classify: 0
     * };
     */
  }

  generaMenu(mRouters, data, fpath) {
    data.forEach(item => {
      // 如果不是外部链接 并且不是菜单分类不是H5
      if (
        Number(item.urlflag) !== 1 &&
        Number(item.classify) !== 1 &&
        Number(item.classify) !== 3
      ) {
        const menu = {
          name: item.fcturl.indexOf('?') !== -1 ? item.fcturl.split('?')[0] : item.fcturl,
          path: item.fcturl.indexOf('?') !== -1 ? item.fcturl.split('?')[0] : item.fcturl,
          meta: {
            name: item.fctname,
            fctid: item.fctid
          }
        };

        this.returnMenuObj(item, menu, fpath);

        if (item.childList && item.childList.length > 0) {
          this.returnChild(item.childList, menu);
        }
        mRouters.push(menu);
      }
    });
  }

  returnMenuObj(item, menuObj, fpath) {
    let menu = menuObj;
    let arr = [];
    // 有选择remarks3 即菜单分类为信息管理

    if (item.remarks3 && Number(item.remarks3) > 0) {
      menu.component = () => {
        return import('@/view/optCenter/newCenter/newModle.vue');
      };
      menu.meta.remarks3 = item.remarks3;
    } else {
      // 如果菜单分类是PC
      if (Number(item.classify) === 0) {
        arr = this.returnArr(item);
        menu = this.returnMenu(arr, menu, fpath);
      } else if (Number(item.classify) === 2) {
        // 如果菜单分类是appbuilder
        menu.component = () => {
          return import('@/view/syscenter/application/appBuilder.vue');
        };
        menu.meta.externalid = item.externalid;
      }
    }
    return menu;
  }

  returnArr(item) {
    let arr = [];
    // 没有选择remarks3 如果有模版id 有传值

    if (item.fcturl.indexOf('?') !== -1 && item.fcturl.split('?')[1]) {
      const attr = item.fcturl.split('?')[1];

      if (attr.indexOf('&') !== -1) {
        arr = attr.split('&').filter(a => {
          return a.indexOf('templateID=') !== -1;
        });
      } else {
        arr.push(attr);
      }
      arr = arr
        .map(b => {
          return b.split('templateID=');
        })
        .flat(2)
        .filter(c => {
          return c && Number(c) > 0;
        });
      arr = Array.from(new Set(arr)).filter(d => {
        return d;
      });
    }
    return arr;
  }

  returnMenu(arr, menuObj, fpath) {
    const menu = menuObj;

    if (arr.length > 0) {
      menu.component = () => {
        return import('@/view/optCenter/newCenter/newModle.vue');
      };
      menu.meta.remarks3 = arr.join(',');
    } else {
      menu.component = lazyLoading(
        fpath ? `${fpath}/${menu.name}/index.vue` : `${menu.name}/index.vue`
      );
    }
    return menu;
  }

  returnChild(childList, menu) {
    for (let i = 0; i < childList.length; i++) {
      if (Number(childList[i].urlflag) !== 1 && Number(childList[i].classify) !== 1) {
        menu.redirect = {};
        menu.redirect.name =
          childList[i].fcturl.indexOf('?') !== -1
            ? childList[i].fcturl.split('?')[0]
            : childList[i].fcturl;
        break;
      }
    }
    menu.children = [];
    this.generaMenu(menu.children, childList, menu.name);
  }

  menuUtils(routers, asyncRouterMap) {
    asyncRouterMap.push(
      this.news,
      this.person,
      this.email,
      this.basicNew,
      this.themepartyday,
      this.delifemeeting,
      this.readyTaskDetails,
      this.myTask,
      this.draftTask,
      this.giveTaskDetails,
      this.lookTaskDetails,
      this.aboutTask,
      this.taskPatrol,
      this.PatrolList,
      this.reportProject,
      this.addProject,
      this.endProject,
      this.myProject,
      this.search
    );
    const mRouters = [];

    this.generaMenu(mRouters, asyncRouterMap);
    const obj = {
      name: 'home',
      path: '/',
      component: lazyLoading('index.vue'),
      children: mRouters
    };

    obj.redirect = obj.children[0].name;
    routers.push(obj);
    return routers;
  }
}

const menuData = new Menu();

export default menuData;
