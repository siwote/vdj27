//本文件是任务中心部分新版文件上传的混入
import router from "@/router";
import { getHzm, toLower } from "@/utils/util";
import QWebChannel from "@/utils/qwebchannel.js";
export default {
  // 文件上传相关的变量
  data() {
    return {
      loading: false,
      fileType:
        ".jpg, .png, .jpeg, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .JPG, .PNG, .JPEG, .DOC, .DOCX, .XLS, .XLSX, .PPT, .PPTX, .PDF.mp3, .mp4,.MP3,.MP4",

      fileData: {
        remark: "",
        type: ""
      },
      interval: 0, //加载的定时器
      percentage: 0, //加载进度条初始值
      colors: [
        { color: "#f56c6c", percentage: 20 },
        { color: "#e6a23c", percentage: 40 },
        { color: "#5cb87a", percentage: 60 },
        { color: "#1989fa", percentage: 80 },
        { color: "#6f7ad3", percentage: 100 }
      ],
      fileList: [],
      fileListArr: [],
      showheight: false,
      operMesg: "",
      showShoot: false
    };
  },
  // 上传相关的方法
  methods: {
    submitheight() {
      this.showheight = false;
    },
    closeheight() {
      this.showheight = false;
    },
    close() {
      this.showShoot = false;
    },
    open() {
      this.showShoot = true;
    },
    closeheightopen() {
      this.showShoot = false;
    },
    closeall(val) {
      this.showShoot = false;
      if (this.scId !== undefined) {
        this.scId = val.id;
      }
      if (this.fileLimit > this.fileListArr.length) {
        this.fileListArr.push(val);
        this.fileList.push(val);
        if (this.rightData && this.rightData.id === 100) {
          const arrone = [];
          this.fileList.map(item => {
            if (item.response) {
              arrone.push(item.response.data.id);
            } else if (item.id) {
              arrone.push(item.id);
            }
          });
          this.filess = arrone.join(",");
        } else if (this.rightData && this.rightData.id === 200) {
          this.atFileList.push(val);
          const arrtwo = [];
          this.atFileList.map(item => {
            if (item.response) {
              arrtwo.push(item.response.data.id);
            } else if (item.id) {
              arrtwo.push(item.id);
            }
          });
          this.activistFiles = arrtwo.join(",");
        } else if (this.rightData && this.rightData.id === 300) {
          this.dpFileList.push(val);
          const arrthree = [];
          this.dpFileList.map(item => {
            if (item.response) {
              arrthree.push(item.response.data.id);
            } else if (item.id) {
              arrthree.push(item.id);
            }
          });
          this.developFiles = arrthree.join(",");
        } else if (this.rightData && this.rightData.id === 400) {
          this.ryFileList.push(val);
          const arrfour = [];
          this.ryFileList.map(item => {
            if (item.response) {
              arrfour.push(item.response.data.id);
            } else if (item.id) {
              arrfour.push(item.id);
            }
          });
          this.readyPoliticalReviewFiles = arrfour.join(",");
        } else if (this.rightData && this.rightData.id === 500) {
          this.flFileList.push(val);
          const arrfive = [];
          this.flFileList.map(item => {
            if (item.response) {
              arrfive.push(item.response.data.id);
            } else if (item.id) {
              arrfive.push(item.id);
            }
          });
          this.formalfiles = arrfive.join(",");
        }
      } else {
        this.$message.warning(`当前限制选择 ${this.fileLimit} 个文件`);
      }
    },
    gaopaiyi() {
      var that = this;
      //连接websocket
      var socket = new WebSocket("ws://127.0.0.1:12345");
      socket.onclose = function() {
        that.close();
        that.showheight = true;
        that.operMesg = "未检测到驱动";
      };
      socket.onopen = function() {
        new QWebChannel(socket, function(channel) {
          // 获取注册对象
          window.dialog = channel.objects.dialog;
          dialog.html_loaded("one");
          window.onunload = function() {
            dialog.get_actionType("closeSignal");
          };
          that.open();
          dialog.sendPrintInfo.connect(function(message) {
            if (message.indexOf("No equipment found!") >= 0) {
              dialog.get_actionType("closeSignal");
              that.close();
              that.showheight = true;
              that.operMesg = "高拍仪未连接";
            }
            if (message.indexOf("priModel") >= 0) {
              that.open();
            }
          });
        });
      };
    },
    onProgress() {
      let that = this;
      let endPro = 95;
      that.loading = true;
      that.interval = setInterval(function() {
        if (that.percentage < endPro) {
          that.percentage++;
        }
      }, 500);
    },
    handleExceed(files, fileList) {
      this.$message.warning(
        `当前限制选择 ${this.fileLimit} 个文件，本次选择了 ${
          files.length
        } 个文件，共选择了 ${files.length + fileList.length} 个文件`
      );
    },
    beforeUpload(file) {
      this.percentage = 0;
      if (file.name.lastIndexOf(".") > 80) {
        this.$message({
          type: "warning",
          message: "文件名称最多80字",
          duration: 1500
        });
        return false;
      }
      var ft = getHzm(file);
      this.fileData.remark = "";
      this.fileData.type = "0";
      const isLt500M = file.size < 524288000;
      var isHaveOne = false;
      if (this.fileType.indexOf(ft) == -1) {
        this.$message({
          type: "warning",
          message: "暂不支持上传该文件类型",
          duration: 1500
        });
        return false;
      }
      if (!isLt500M) {
        this.$message({
          type: "warning",
          message: "上传附件不超过500M",
          duration: 1500
        });
        return false;
      }
      if (this.fileListArr.length != 0) {
        this.fileListArr.forEach(item => {
          if (toLower(item.name) == toLower(file.name)) {
            isHaveOne = true;
            this.$message({
              type: "warning",
              message: "资料名不能重复",
              duration: 1500
            });
            return false;
          }
        });
      }
      return isLt500M && !isHaveOne;
    },
    handleSuccess(res, file, fileList) {
      var that = this;
      clearInterval(that.interval);
      setTimeout(() => {
        this.percentage = 100;
      }, 100);
      setTimeout(() => {
        this.loading = false;
        this.percentage = 0;
      }, 200);
      if (res.data) {
        this.fileListArr.push(res.data);
        this.fileList = [...this.fileListArr];
      } else if (res.code == "PW00001") {
        this.$auth.clearLogin();
        router.replace({
          name: "login"
        });
      } else {
        this.$message({
          type: "warning",
          message: res.message,
          duration: 3000
        });
      }
    },
    handleError(res) {
      this.$message.error("上传失败，请重新上传");
    },
    getDelFile(id) {
      if (this.fileListArr.length != 0) {
        this.fileListArr.forEach((item, index) => {
          if (item.id == id) {
            this.fileListArr.splice(index, 1);
          }
        });
      }

      if (this.fileList.length != 0) {
        this.fileList.forEach((item, index) => {
          if (item.response) {
            if (item.response.data.id == id) {
              this.fileList.splice(index, 1);
            }
          } else {
            if (item.id == id) {
              this.fileList.splice(index, 1);
            }
          }
        });
      }
      this.fileList = [...this.fileListArr];
    },
    getNewFile(val) {
      this.fileListArr = val;
    }
  }
};
