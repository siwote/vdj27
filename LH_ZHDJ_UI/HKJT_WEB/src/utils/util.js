// 公共js

// 动态菜单 动态路由的数据处理
import lazyLoading from '@/utils/lazyLoading';
/* 文件预览 */
import { preview } from '@/api/task';
// 上传附件  生成uuid
import auth from '@/utils/auth';

export function menuUtils(routers, asyncRouterMap) {
  asyncRouterMap.forEach(item => {
    if (item.id == 1) {
      var obj = [
        {
          name: 'readyTask/readyTaskDetails',
          url: 'readyTask/readyTaskDetails/:taskid/:isChain', //taskid主任务id  是否从任务链进来的
          remarks3: 'task/readyTask/readyTaskDetails.vue',
          childs: [],
          hidden: true
        },
        {
          name: 'draftTask/draftTaskDetails',
          url: 'draftTask/draftTaskDetails/:taskid', //taskid主任务的id
          remarks3: 'task/draftTask/draftTaskDetails.vue',
          childs: [],
          hidden: true
        },
        {
          name: 'myTask/myTaskDetails',
          url: 'myTask/myTaskDetails/:taskid/:mytaskid/:remarks6', //taskid主任务的id mytaskid子任务id
          remarks3: 'task/myTask/myTaskDetails.vue',
          childs: [],
          hidden: true
        },
        {
          name: 'lookTask/lookTaskDetails',
          url: 'lookTask/lookTaskDetails/:auditid/:taskid/:mytaskid/:flag/:time', //flag 退回标识auditid  taskid主任务的id mytaskid 子任务的id
          remarks3: 'task/lookTask/lookTaskDetails.vue',
          childs: [],
          hidden: true
        },
        {
          name: 'giveTask/giveTaskDetails',
          url: 'giveTask/giveTaskDetails/:auditid/:taskid/:mytaskid/:flag/:time', //flag 退回标识auditid  taskid主任务的id mytaskid 子任务的id
          remarks3: 'task/giveTask/giveTaskDetails.vue',
          childs: [],
          hidden: true
        },
        {
          name: 'aboutTask/aboutTaskDetails',
          url: 'aboutTask/aboutTaskDetails/:taskid', //taskid主任务的id
          remarks3: 'task/aboutTask/aboutTaskDetails.vue',
          childs: [],
          hidden: true
        },
        {
          name: 'taskPatrol/taskPatrolDetails',
          url: 'taskPatrol/taskPatrolDetails/:taskid/:mytaskid/:flag', //taskid主任务的id
          remarks3: 'task/taskPatrol/taskPatrolDetails.vue',
          childs: [],
          hidden: true
        }
      ];
      item.childs.push(...obj);
    }
    if (item.id == 300) {
      var obj = [
        {
          name: 'sessions/basicTable',
          url: 'sessions/basicTable/:mTypeName/:mType/:flag/:years/:handleType', //taskid主任务id  是否从任务链进来的
          remarks3: 'orgLife/sessions/basicTable.vue',
          childs: [],
          hidden: true
        }
      ];
      item.childs.push(...obj);
    }
  });
  var mRouters = [];
  generaMenu(mRouters, asyncRouterMap);
  var obj = {};
  (obj.name = 'home'), (obj.path = '/'), (obj.component = lazyLoading('index.vue'));
  obj.hidden = false;
  obj.children = mRouters;
  obj.redirect = obj.children[0].name;
  routers.push(obj);
  return routers;
}

function generaMenu(mRouters, data) {
  data.forEach(item => {
    let menu = {};
    menu.component = lazyLoading(item.remarks3);
    menu.name = item.name ? item.name : item.url;
    menu.path = item.url;
    if (item.pid == 0) {
      if (item.menuname != '首页') {
        menu.redirect = {};
        if (item.childs[0].childs.length != 0) {
          menu.redirect.name = item.childs[0].childs[0].url;
        } else {
          menu.redirect.name = item.childs[0].url;
        }
      }
    }
    if (item.hidden) {
      menu.hidden = item.hidden;
    }
    if (item.childs) {
      menu.children = [];
      generaMenu(menu.children, item.childs);
    }
    mRouters.push(menu);
  });
}

// 页面div拖动边缘 放大缩小效果

export function move(e, that, wid) {
  e.preventDefault();
  //算出鼠标相对元素的位置
  var disX = e.screenX;
  /* 保存原始的元素位置 */
  var leftOneBefore = that.$refs.contentOne.getBoundingClientRect().width;
  var leftTwoBefore = that.$refs.contentTwo.getBoundingClientRect().width;

  document.onmousemove = e => {
    //鼠标按下并移动的事件
    //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
    var left = e.screenX - disX;
    var leftOne = leftOneBefore + left;
    var leftTwo = leftTwoBefore - left;

    if (leftTwo < 500 || leftOne < wid) {
      return;
    }
    that.contentOneStyleObj.width = leftOne + 'px';
    that.contentTwoStyleObj.width = leftTwo + 'px';
    that.contentTwoStyleObj.left = leftOne + 'px';
  };
  document.onmouseup = e => {
    document.onmousemove = null;
    document.onmouseup = null;
  };
}
export function moveTwo(e, that, wid) {
  e.preventDefault();
  //算出鼠标相对元素的位置
  var disX = e.screenX;
  /* 保存原始的元素位置 */
  var leftOneBefore = that.$refs.contentTree.getBoundingClientRect().width;
  var leftTwoBefore = that.$refs.contentFour.getBoundingClientRect().width;

  document.onmousemove = e => {
    //鼠标按下并移动的事件
    //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
    var left = e.screenX - disX;
    var leftOne = leftOneBefore + left;
    var leftTwo = leftTwoBefore - left;
    if (leftTwo < 300 || leftOne < wid) {
      return;
    }
    that.contentTreeStyleObj.width = leftOne + 'px';
    that.contentFourStyleObj.width = leftTwo + 'px';
    that.contentFourStyleObj.left = leftOne + 'px';
  };
  document.onmouseup = e => {
    document.onmousemove = null;
    document.onmouseup = null;
  };
}

export function moveTree(e, that, wid, maxWid) {
  e.preventDefault();
  //算出鼠标相对元素的位置
  var disX = e.screenX;
  /* 保存原始的元素位置 */
  var leftOneBefore = that.$refs.contentOne.getBoundingClientRect().width;
  var leftTwoBefore = that.$refs.contentTwo.getBoundingClientRect().width;

  document.onmousemove = e => {
    //鼠标按下并移动的事件
    //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
    var left = e.screenX - disX;
    var leftOne = leftOneBefore + left;
    var leftTwo = leftTwoBefore - left;

    if (leftTwo < maxWid || leftOne < wid) {
      return;
    }
    that.contentOneStyleObj.width = leftOne + 'px';
    that.contentTwoStyleObj.width = leftTwo + 'px';
    that.contentTwoStyleObj.left = leftOne + 'px';
  };
  document.onmouseup = e => {
    document.onmousemove = null;
    document.onmouseup = null;
  };
}

// 文件的预览
export function getHzm(file) {
  return file.name
    ? file.name.split('.')[file.name.split('.').length - 1]
    : file.attaname.split('.')[file.attaname.split('.').length - 1];
}

export function showFile(file) {
  /* 获取后缀名 */
  var str = file.name
      ? file.name.split('.')[file.name.split('.').length - 1]
      : file.attaname.split('.')[file.attaname.split('.').length - 1],
    href = '',
    name = '';
  /* 拼接路径  转码  防止文件名内有特殊符号*/
  name = file.name ? file.name : file.attaname;
  if (name.indexOf("'") != -1) {
    href = file.name ? file.path + escape(file.name) : file.path + escape(file.attaname);
  } else {
    href = file.name
      ? file.path + encodeURIComponent(file.name)
      : file.path + encodeURIComponent(file.attaname);
  }
  if (str == 'rar' || str == 'zip') {
    window.open(href);
  } else if (
    str == 'jpg' ||
    str == 'png' ||
    str == 'jpeg' ||
    str == 'JPG' ||
    str == 'PNG' ||
    str == 'JPEG'
  ) {
    var myWindow = window.open('');
    myWindow.document.write("<div style='text-align:center'><img src='" + href + "'></div>");
  } else if (
    str == 'doc' ||
    str == 'docx' ||
    str == 'xls' ||
    str == 'xlsx' ||
    str == 'ppt' ||
    str == 'pptx' ||
    str == 'pdf'
  ) {
    var params = {};
    params.path = encodeURIComponent(encodeURIComponent(file.path));
    params.uuid = file.uuid ? file.uuid : file.remarks3;
    params.filename = file.name ? file.name : file.attaname;
    preview(params).then(res => {
      var path = res.data;
      window.open(path);
    });
  }
}

// 字符长度的转化
export function utf8_strlen(str) {
  var str_encode = escape(str);
  var cnt = 0;
  for (var i = 0; i < str_encode.length; i++) {
    if (str_encode.charAt(i) == '%') {
      if (str_encode.charAt(i + 1) == 'u') {
        var value = parseInt(str_encode.substr(i + 2, 4), 16);
        if (value < 0x0800) {
          cnt += 2;
        } else {
          cnt += 3;
        }
        i = i + 5;
      } else {
        cnt++;
        i = i + 2;
      }
    } else {
      cnt++;
    }
  }
  return cnt;
}

// 生成uuid
export function createUuid(that) {
  var date = Date.parse(new Date());
  that.uuid = date + '' + auth.getCookie('User-Id');
}

// import Vue from "vue";
import CryptoJS from 'crypto-js';

export function Encrypt(word) {
  let key = 'adsyto1234567890';
  let iv = '123456789channel';

  key = CryptoJS.enc.Utf8.parse(key);
  iv = CryptoJS.enc.Utf8.parse(iv);

  let srcs = CryptoJS.enc.Utf8.parse(word);
  let encrypted = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });

  return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
}

export function Decrypt(word) {
  let key = 'adsyto1234567890';
  let iv = '123456789channel';

  key = CryptoJS.enc.Utf8.parse(key);
  iv = CryptoJS.enc.Utf8.parse(iv);

  let base64 = CryptoJS.enc.Base64.parse(word);
  let src = CryptoJS.enc.Base64.stringify(base64);

  let decrypt = CryptoJS.AES.decrypt(src, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });

  let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
}

// 数组根据key去重，例如根据id去重：传入参数（要去重的数组,'id'）
export function getNewArr(arr, key) {
  for (var i = 0, uarr = []; i < arr.length; i++) {
    var flag = false;
    for (var j = 0; j < uarr.length; j++) {
      if (key) {
        if (uarr[j][key] == arr[i][key]) {
          flag = true;
          break;
        }
      } else {
        if (uarr[j] == arr[i]) {
          flag = true;
          break;
        }
      }
    }
    if (!flag) {
      uarr[uarr.length] = arr[i];
    }
  }
  return uarr;
}
// 提醒时间格式化，返回新字段data1
export function remindTimeFormate(arr) {
  arr.forEach(time => {
    time.data1 = '';
    var remindSpan = time.data.split(',')[0];
    var remindNum = time.data.split(',')[2];
    if (time.flag == 2) {
      if (time.data.split(',').indexOf('Y') != -1) {
        time.data1 = '每' + remindSpan + '年第' + remindNum + '天';
      } else if (time.data.split(',').indexOf('Q') != -1) {
        time.data1 = '每' + remindSpan + '季度第' + remindNum + '天';
      } else if (time.data.split(',').indexOf('M') != -1) {
        time.data1 = '每' + remindSpan + '月第' + remindNum + '天';
      } else if (time.data.split(',').indexOf('W') != -1) {
        time.data1 = '每' + remindSpan + '周第' + remindNum + '天';
      } else if (time.data.split(',').indexOf('D') != -1) {
        time.data1 = '每' + remindSpan + '天';
      }
    } else {
      time.data1 = time.data;
    }
  });
}

// 全屏效果

export function bigDecompose(that) {
  if (!that.isBig) {
    var element = document.getElementsByTagName('jmnodes')[0];
    that.isBig = true;
    var a = window.innerHeight + 'px';
    var b = window.innerWidth + 'px';
    that.style.height = a;
    that.style.width = b;
    element.style.height = '100%';
    element.style.width = '100%';
  } else {
    that.isBig = false;
    that.style.height = '600px';
    that.style.width = '100%';
  }
}
export function initSelectFile(that) {
  that.$refs.selectFiles.files = [];
  that.$refs.selectFiles.setSelectDocs();
  that.$auth.removeLocalStorage('expandedK');
  that.$nextTick(() => {
    that.$refs.selectFiles.$refs.uploadFile.clearFiles();
    that.$refs.selectFiles.fileList = [];
    that.$refs.selectFiles.fileListArr = [];
    that.$refs.selectFiles.srcIds = [];
    that.$refs.selectFiles.activeTabs = 'first';
    that.$refs.selectFiles.$refs.classT.expandedK = [];
    that.$refs.selectFiles.$refs.classT.noFirstLoad = false;
    that.$refs.selectFiles.$refs.classT.fetchTreeData();
  });
}
export function toLower(str) {
  var ind = str.lastIndexOf('.'); //取到文件名开始到最后一个点的长度
  var len = str.length; //取到文件名长度
  var hzm = str.substring(ind + 1, len); //截
  var name = str.substring(0, ind).trim(); //去掉后缀名的文件名
  return name + '.' + hzm.toLowerCase(); //返回后缀名转小写完整的文件名
}
export function isJSON(str) {
  if (typeof str == 'string') {
    try {
      var obj = JSON.parse(str);
      if (typeof obj == 'object' && obj) {
        if (Object.prototype.toString.call(obj) == '[object Array]') {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
