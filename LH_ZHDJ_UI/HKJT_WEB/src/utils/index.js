import auth from "@/utils/auth";
import variables from "@/style/index.scss";
import axios from "axios";
import qs from "qs";
/**
 *  函数防抖
 *  @param {Function} func  包装的函数
 *  @param {num} delay      延迟时间
 *  @param {boolean} immediate 第一次滚动会执行两次  开始滚动和结束滚动的时候
 *  @return {*}
 */
class Funutil {
  systemUrl = "/urc";
  powerUrl = "/power";
  Base64 = require("js-base64").Base64;
  // issql = /select|update|delete|truncate|join|union|exec|insert|drop|count|'|"|;|>|<|%/i;
  // isstrn = /^[a-zA-Z0-9\u4e00-\u9fa5]+$/;
  // isstrn2 = /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/;
  isserver = /^[^@]+@([^\.]+)\..*$/;
  // isemail = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z]{2,5}$/;
  pickerOptions = {
    shortcuts: [
      {
        text: "最近一周",
        onClick(picker) {
          const end = new Date();
          const start = new Date();
          start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
          picker.$emit("pick", [start, end]);
        }
      },
      {
        text: "最近一个月",
        onClick(picker) {
          const end = new Date();
          const start = new Date();
          start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
          picker.$emit("pick", [start, end]);
        }
      },
      {
        text: "最近三个月",
        onClick(picker) {
          const end = new Date();
          const start = new Date();
          start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
          picker.$emit("pick", [start, end]);
        }
      }
    ]
  };
  headerStyle = {
    "background-color": "#f9fafb",
    "border-bottom": "1px #E8E8E8 solid"
  };

  constructor() {
    this.issql = /and|exec|insert|select|union|update|delete|truncate|join|drop|count|'/i;
    this.isstrn = /^[a-zA-Z0-9\u4e00-\u9fa5]+$/;
    this.isstrn1 = /^[a-zA-Z0-9,，\u4e00-\u9fa5]+$/;
    this.isstrn2 = /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/;
    this.isstrn3 = /^[a-zA-Z0-9（）()\u4e00-\u9fa5]+$/;
    this.isacc = /^[a-zA-Z0-9%￥$*/|。，！……^、&【】——（）[\]？；——：’‘“”~·`!@#()-_+{}:"?,.';=《》<>]+$/;
    this.ispwd = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~`·!！@@#¥$%^……&*（）()—+_=【】「」{}/[\]\\|、｜”“‘’：；:;"'》《，。<>,.?？])[\da-zA-Z~`·!！@@#¥$%^……&*（）()—+_=【】「」{}/[\]\\|、｜”“‘’：；:;"'》《，。<>,.?？]+$/;
    this.isphone = /^1[3456789]\d{9}$/;
    this.istel = /^0?\d{3}-\d{7,8}$/;
    this.isemail = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z]{2,5}$/;
    this.isurl = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
    this.isNumber = /^[0-9]*$/;
    this.chalk = "";
    this.version = require("element-ui/package.json").version;
    this.menuBackColor = "--menu-back";
    this.menuIconColor = "--menu-icon";
    this.menuIconFontColor = "--menu-iconfont";
    this.primaryColorColor = "--primary-color";
    this.submenuBackColor = "--submenu-back";
    this.submenuTitleColor = "--submenu-title";
    this.submenuTitleHoverColor = "--submenu-title-hover";
    this.menuItemBorderColor = "--menu-item-border";
    this.menuHeaderBackColor = "--menu-header-back";
    this.menuHeaderBackActiveColor = "--menu-header-back-active";
    this.headerBackImgColor = "--header-back-img";
    this.leftAsideImgColor = "--left-aside-img";
    this.asideTitleColor = "--aside-title";
    this.menuItemBorderRightColor = "--menu-item-border-right";
    this.equipmentmodel = "Unknown";
    this.browser = "";
    // 浏览器版本  edition;
    this.edition = navigator.appVersion;
    this.ACCEPT_CONFIG = {
      image: [".png", ".jpg", ".jpeg", ".gif", ".bmp"],
      video: [".mp4", ".rmvb", ".mkv", ".wmv", ".flv"],
      document: [
        ".doc",
        ".docx",
        ".xls",
        ".xlsx",
        ".ppt",
        ".pptx",
        ".pdf",
        ".txt",
        ".tif",
        ".tiff"
      ],
      getAll() {
        return [...this.image, ...this.video, ...this.document];
      }
    };
    this.fileSize = 500;
  }
  trim(value) {
    return typeof value === "string" ? value.replace(/\s+/g, "") : value;
  }
  thisyear = this.formatDate(new Date(), "yyyy");
  //换肤b
  async setColor(val, themes, ORIGINAL_THEME) {
    let value,
      theme = themes;

    if (theme.toLowerCase() === "#29354d") {
      theme = "#1f69e6";
      value = "#1f69e6";
    } else {
      value = val;
    }
    if (this.iEVersion() === 15) {
      this.themeChange(val);
    }
    const oldVal = this.chalk ? theme : ORIGINAL_THEME;

    if (typeof value !== "string") {
      return;
    }
    const themeCluster = this.getThemeCluster(value.replace("#", ""));
    const originalCluster = this.getThemeCluster(oldVal.replace("#", ""));
    const getHandler = (variable, id) => {
      return () => {
        const originalClusters = this.getThemeCluster(
          ORIGINAL_THEME.replace("#", "")
        );
        const newStyle = this.updateStyle(
          this[variable],
          originalClusters,
          themeCluster
        );
        let styleTag = document.getElementById(id);

        if (!styleTag) {
          styleTag = document.createElement("style");
          styleTag.setAttribute("id", id);
          document.head.appendChild(styleTag);
        }
        styleTag.innerText = newStyle;
      };
    };

    if (!this.chalk) {
      const url = `https://unpkg.com/element-ui@${this.version}/lib/theme-chalk/index.css`;

      await this.getCSSString(url, "chalk");
    }

    const chalkHandler = getHandler("chalk", "chalk-style");

    chalkHandler();

    const styles = [].slice
      .call(document.querySelectorAll("style"))
      .filter(style => {
        const text = style.innerText;

        return (
          new RegExp(oldVal, "i").test(text) && !/Chalk Variables/.test(text)
        );
      });

    styles.forEach(style => {
      const { innerText } = style;

      if (typeof innerText !== "string") {
        return;
      }
      style.innerText = this.updateStyle(
        innerText,
        originalCluster,
        themeCluster
      );
    });
  }

  updateStyle(style, oldCluster, newCluster) {
    let newStyle = style;

    oldCluster.forEach((color, index) => {
      newStyle = newStyle.replace(new RegExp(color, "ig"), newCluster[index]);
    });
    return newStyle;
  }

  getCSSString(url, variable) {
    return new Promise(resolve => {
      const xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          this[variable] = xhr.responseText.replace(/@font-face{[^}]+}/, "");
          resolve();
        }
      };
      xhr.open("GET", url);
      xhr.send();
    });
  }

  getThemeCluster(theme) {
    const tintColor = (color, tint) => {
      let red = parseInt(color.slice(0, 2), 16);
      let green = parseInt(color.slice(2, 4), 16);
      let blue = parseInt(color.slice(4, 6), 16);

      if (tint === 0) {
        // when primary color is in its rgb space
        return [red, green, blue].join(",");
      }
      red += Math.round(tint * (255 - red));
      green += Math.round(tint * (255 - green));
      blue += Math.round(tint * (255 - blue));

      red = red.toString(16);
      green = green.toString(16);
      blue = blue.toString(16);

      return `#${red}${green}${blue}`;
    };

    const shadeColor = (color, shade) => {
      let red = parseInt(color.slice(0, 2), 16);
      let green = parseInt(color.slice(2, 4), 16);
      let blue = parseInt(color.slice(4, 6), 16);

      red = Math.round((1 - shade) * red);
      green = Math.round((1 - shade) * green);
      blue = Math.round((1 - shade) * blue);

      red = red.toString(16);
      green = green.toString(16);
      blue = blue.toString(16);

      return `#${red}${green}${blue}`;
    };

    const clusters = [theme];

    for (let i = 0; i <= 9; i++) {
      clusters.push(tintColor(theme, Number((i / 10).toFixed(2))));
    }
    clusters.push(shadeColor(theme, 0.1));
    return clusters;
  }

  themeChange(val) {
    if (val.toLowerCase() === "#29354d") {
      variables.theme = "#1F69E6";
      document
        .getElementsByTagName("body")[0]
        .style.setProperty("--default-color", "#1F69E6");
    } else {
      variables.theme = val;
      document
        .getElementsByTagName("body")[0]
        .style.setProperty("--default-color", val);
    }
    if (val.toLowerCase() === "#1f69e6") {
      this.funOne();
    } else if (val.toLowerCase() === "#ef3943") {
      this.funTwo(val);
    } else if (val.toLowerCase() === "#5ac79a") {
      this.funThree(val);
    } else if (val.toLowerCase() === "#29354d") {
      this.funFour(val);
    } else {
      this.funFive(val);
    }
  }

  funOne() {
    let menuBack = "#3e86ff";
    const menuIcon = "#74b6ff";
    const menuIconfont = "#74b6ff";
    let primaryColor = "#004ed1";
    let menuItemBorder = "#004ed1";
    let submenuTitle = "#fff";
    let subMenuBack = "#1f69e6";
    let submenuTitleHover = "#fff";
    let submenuTitleHoverOpt = 0.7;
    let asideTitle = "#fff";
    let menuItemBorderRight = "none";
    let menuHeaderBackColor = "#fff";

    if (
      auth.getCookie("menuLayout") &&
      auth.getCookie("menuLayout") === "style_up_down"
    ) {
      menuBack = "#F8F9FB";
      primaryColor = "#DDEAFF";
      menuItemBorder = "transparent";
      menuItemBorderRight = "4px solid #3E86FF";
      submenuTitle = "#212A3D";
      subMenuBack = "#F2F2F2";
      submenuTitleHover = "#212A3D";
      asideTitle = "#3E86FF";
      submenuTitleHoverOpt = 1;
      menuHeaderBackColor = "#2A73EE";
    }
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuBackColor, this.colorRgba(menuBack, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconColor, this.colorRgba(menuIcon, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuIconFontColor,
        this.colorRgba(menuIconfont, 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.primaryColorColor,
        this.colorRgba(primaryColor, 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuBackColor, this.colorRgba(subMenuBack, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.submenuTitleColor,
        this.colorRgba(submenuTitle, submenuTitleHoverOpt)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.submenuTitleHoverColor,
        this.colorRgba(submenuTitleHover, submenuTitleHoverOpt)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuItemBorderColor,
        this.colorRgba(menuItemBorder, 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackColor,
        this.colorRgba(menuHeaderBackColor, 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackActiveColor,
        this.colorRgba("#004ed1", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.headerBackImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.leftAsideImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.asideTitleColor, this.colorRgba(asideTitle, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuItemBorderRightColor,
        this.colorRgba(menuItemBorderRight, 1)
      );
  }

  funTwo(val) {
    let menuHeaderBackColor = "#fff";
    let menuBackColor = val;
    let leftAsideImg = "none";
    let submenuBackColor = "#CE2A32";
    let primaryColorColor = "#B81C24";
    let menuItemBorderColor = "#B81C24";
    let menuIconColor = "#FFFFFF";
    let menuIconFontColor = "#FFBCBF";
    let submenuTitleColor = "#FFFFFF";
    let submenuTitleHoverColor = "#FFFFFF";
    let headerBackImgColor = `url(${require("@/assets/logorr.png")})`;

    if (
      auth.getCookie("menuLayout") &&
      auth.getCookie("menuLayout") === "style_up_down"
    ) {
      menuHeaderBackColor = "#EF3942";
      menuBackColor = this.colorRgba("#F6E7DB", 1);
      leftAsideImg = `url(${require("@/assets/menul.png")})`;
      submenuBackColor = this.colorRgba("#EDD7CE", 1);
      primaryColorColor =
        "linear-gradient(90deg, #EF3942 0%, rgba(239, 57, 66, 0.35) 100%)";
      menuItemBorderColor = this.colorRgba("#EF3942", 1);
      menuIconColor = this.colorRgba("#5C5047", 0.5);
      menuIconFontColor = this.colorRgba("#FFA5A9", 1);
      submenuTitleColor = this.colorRgba("#5C5047", 1);
      submenuTitleHoverColor = this.colorRgba("#5C5047", 1);
      headerBackImgColor = "none";
    }
    // 侧边菜单的背景色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuBackColor, menuBackColor);
    // 侧边菜单的上面的功能导航的字体颜色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconColor, menuIconColor);
    // 侧边了上面的菜单图标的颜色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconFontColor, menuIconFontColor);
    // 侧边栏激活颜色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.primaryColorColor, primaryColorColor);
    // 侧边栏下拉出菜单的背景色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuBackColor, submenuBackColor);
    // 侧边栏下啦出菜单的字体颜色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuTitleColor, submenuTitleColor);
    // 下啦菜单激活颜色
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuTitleHoverColor, submenuTitleHoverColor);
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuItemBorderColor, menuItemBorderColor);
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackColor,
        this.colorRgba(menuHeaderBackColor, 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackActiveColor,
        this.colorRgba("#D8353D", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.headerBackImgColor, headerBackImgColor);
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.leftAsideImgColor, leftAsideImg);
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.asideTitleColor, this.colorRgba("#fff", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuItemBorderRightColor, "none");
  }

  funThree(val) {
    let menuHeaderBackColor = "#fff";

    if (
      auth.getCookie("menuLayout") &&
      auth.getCookie("menuLayout") === "style_up_down"
    ) {
      menuHeaderBackColor = "#429D77";
    }

    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuBackColor, this.colorRgba(val, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconColor, this.colorRgba("#C9FFE9", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconFontColor, this.colorRgba("#b7e6d3", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.primaryColorColor, this.colorRgba("#239667", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuBackColor, this.colorRgba("#3CA97C", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuTitleColor, this.colorRgba("#C9FFE9", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.submenuTitleHoverColor,
        this.colorRgba("#C9FFE9", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuItemBorderColor,
        this.colorRgba("#239667", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackColor,
        this.colorRgba(menuHeaderBackColor, 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackActiveColor,
        this.colorRgba("#239667", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.headerBackImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.leftAsideImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.asideTitleColor, this.colorRgba("#fff", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuItemBorderRightColor, "none");
  }

  funFour(val) {
    let menuHeaderBackColor = "#fff";

    if (
      auth.getCookie("menuLayout") &&
      auth.getCookie("menuLayout") === "style_up_down"
    ) {
      menuHeaderBackColor = "linear-gradient(90deg, #0051C1 0%, #358DF9 100%)";
    }
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuBackColor, this.colorRgba(val, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconColor, this.colorRgba("#74B6FF", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconFontColor, this.colorRgba("#90C4FF", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.primaryColorColor,
        "linear-gradient(90deg, rgba(0, 81, 193, 0.74) 0%, rgba(43, 129, 239, 0.09) 100%)"
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuBackColor, this.colorRgba("#21293D", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuTitleColor, this.colorRgba("#B8C8EC", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.submenuTitleHoverColor,
        this.colorRgba("#B8C8EC", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuItemBorderColor,
        this.colorRgba("#3D85FF", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuHeaderBackColor, menuHeaderBackColor);
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackActiveColor,
        this.colorRgba("#3377FF", 1)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.headerBackImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.leftAsideImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.asideTitleColor, this.colorRgba("#fff", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuItemBorderRightColor, "none");
  }

  funFive(val) {
    let menuHeaderBackColor = "#fff";

    if (
      auth.getCookie("menuLayout") &&
      auth.getCookie("menuLayout") === "style_up_down"
    ) {
      menuHeaderBackColor = this.colorRgba(val, 0.9);
    }
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuBackColor, this.colorRgba(val, 0.85));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconColor, this.colorRgba("#ffffff", 0.4));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuIconFontColor, this.colorRgba(val, 0.5));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.primaryColorColor,
        this.colorRgba("#000000", 0.2)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuBackColor, this.colorRgba(val, 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.submenuTitleColor, this.colorRgba("#fff", 0.7));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.submenuTitleHoverColor,
        this.colorRgba("#fff", 0.7)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuItemBorderColor,
        this.colorRgba("#000000", 0.2)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuHeaderBackColor, menuHeaderBackColor);
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(
        this.menuHeaderBackActiveColor,
        this.colorRgba("#000000", 0.2)
      );
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.headerBackImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.leftAsideImgColor, "none");
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.asideTitleColor, this.colorRgba("#fff", 1));
    document
      .getElementsByTagName("body")[0]
      .style.setProperty(this.menuItemBorderRightColor, "none");
  }

  colorRgba(sHex, alpha) {
    // 十六进制颜色值的正则表达式
    const reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    /* 16进制颜色转为RGB格式 */
    let sColor = sHex.toLowerCase();

    if (sColor && reg.test(sColor)) {
      if (sColor.length === 4) {
        let sColorNew = "#";

        for (let i = 1; i < 4; i += 1) {
          sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
        }
        sColor = sColorNew;
      }
      // 处理六位的颜色值
      const sColorChange = [];

      for (let i = 1; i < 7; i += 2) {
        sColorChange.push(parseInt(`0x${sColor.slice(i, i + 2)}`, 0));
      }
      return `rgba(${sColorChange.join(",")},${alpha})`;
    }
    return sColor;
  }
  //换肤e

  iEVersion() {
    const { userAgent } = navigator; // 取得浏览器的userAgent字符串
    const isIE =
      userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; // 判断是否IE<11浏览器
    const isEdge = userAgent.indexOf("Edge") > -1 && !isIE; // 判断是否IE的Edge浏览器
    const isIE11 =
      userAgent.indexOf("Trident") > -1 && userAgent.indexOf("rv:11.0") > -1;

    if (isIE) {
      return this.returnIe();
    }
    if (isEdge) {
      return "edge"; // edge
    }
    if (isIE11) {
      return 11; // IE11
    }
    return 15; // 不是ie浏览器
  }

  returnIe() {
    const fIEVersion = parseFloat(RegExp["$1"]);

    if (fIEVersion === 7) {
      return 7;
    } else if (fIEVersion === 8) {
      return 8;
    } else if (fIEVersion === 9) {
      return 9;
    } else if (fIEVersion === 10) {
      return 10;
    }
    return 6; // IE版本<=7
  }

  // 获取浏览器信息
  getOsInfo(htmlNumber, type) {
    const htmlpath = location.href;
    let html = "";
    let htmlName = "";
    const userAgent = navigator.userAgent.toLowerCase();

    // 屏幕尺寸 1920*1080  screensize;
    const screensize = `${window.screen.width}*${window.screen.height}`;

    if (htmlNumber && htmlNumber.indexOf(",") > -1) {
      html = htmlNumber.split(",")[0];
      htmlName = htmlNumber.split(",")[1];
    }
    this.returnBrowser(userAgent);
    const { browser } = this;
    const { edition } = this;

    this.returnEequipmentmodel(userAgent);
    const { equipmentmodel } = this;

    return {
      htmlpath,
      type,
      html,
      htmlName,
      browser,
      edition,
      screensize,
      equipmentmodel
    };
  }

  returnBrowser(userAgent) {
    /*
     * 判断是否IE<11浏览器
     * 浏览器  browser;
     */
    if (
      userAgent.indexOf("compatible") > -1 &&
      userAgent.indexOf("msie") > -1
    ) {
      this.edition = userAgent.match(/msie ([\d.]+)/)[1] || "";
      this.browser = "IE";
    } else if (
      userAgent.indexOf("trident") > -1 &&
      userAgent.indexOf("rv:11.0") > -1
    ) {
      this.edition = 11;
      this.browser = "IE";
    } else if (userAgent.indexOf("firefox") >= 0) {
      this.edition = userAgent.match(/firefox\/([\d.]+)/)[1] || "";
      this.browser = "Firefox";
    } else if (userAgent.indexOf("chrome") >= 0) {
      this.edition = userAgent.match(/chrome\/([\d.]+)/)[1] || "";
      this.browser = "Chrome";
    } else if (userAgent.indexOf("opera") >= 0) {
      this.edition = userAgent.match(/opera.([\d.]+)/)[1] || "";
      this.browser = "Opera";
    } else if (userAgent.indexOf("safari") >= 0) {
      this.edition = userAgent.match(/version\/([\d.]+)/)[1] || "";
      this.browser = "Safari";
    }
    if (userAgent.indexOf("edge") >= 0) {
      this.edition = userAgent.match(/edge\/([\d.]+)/)[1] || "";
      this.browser = "edge";
    }
    // 遨游浏览器
    if (userAgent.indexOf("maxthon") >= 0) {
      this.edition = userAgent.match(/maxthon\/([\d.]+)/)[1] || "";
      this.browser = "傲游浏览器";
    }
    // QQ浏览器
    if (userAgent.indexOf("qqbrowser") >= 0) {
      this.edition = userAgent.match(/qqbrowser\/([\d.]+)/)[1] || "";
      this.browser = "QQ浏览器";
    }
    // 搜狗浏览器
    if (userAgent.indexOf("se 2.x") >= 0) {
      this.browser = "搜狗浏览器";
    }
  }

  returnEequipmentmodel(userAgent) {
    // 设备型号windows mac  equipmentmodel
    if (userAgent.indexOf("win") > -1) {
      if (userAgent.indexOf("windows nt 5.0") > -1) {
        this.equipmentmodel = "Windows 2000";
      } else if (
        userAgent.indexOf("windows nt 5.1") > -1 ||
        userAgent.indexOf("windows nt 5.2") > -1
      ) {
        this.equipmentmodel = "Windows XP";
      } else if (userAgent.indexOf("windows nt 6.0") > -1) {
        this.equipmentmodel = "Windows Vista";
      } else if (
        userAgent.indexOf("windows nt 6.1") > -1 ||
        userAgent.indexOf("windows 7") > -1
      ) {
        this.equipmentmodel = "Windows 7";
      } else if (
        userAgent.indexOf("windows nt 6.2") > -1 ||
        userAgent.indexOf("windows 8") > -1
      ) {
        this.equipmentmodel = "Windows 8";
      } else if (userAgent.indexOf("windows nt 6.3") > -1) {
        this.equipmentmodel = "Windows 8.1";
      } else if (userAgent.indexOf("windows nt 10.0") > -1) {
        this.equipmentmodel = "Windows 10";
      }
    } else if (userAgent.indexOf("iphone") > -1) {
      this.equipmentmodel = "Iphone";
    } else if (userAgent.indexOf("mac") > -1) {
      this.equipmentmodel = "Mac";
    } else if (
      userAgent.indexOf("x11") > -1 ||
      userAgent.indexOf("unix") > -1 ||
      userAgent.indexOf("sunname") > -1 ||
      userAgent.indexOf("bsd") > -1
    ) {
      this.equipmentmodel = "Unix";
    } else if (userAgent.indexOf("linux") > -1) {
      if (userAgent.indexOf("android") > -1) {
        this.equipmentmodel = "Android";
      } else {
        this.equipmentmodel = "Linux";
      }
    }
  }

  // 设置地址栏icon图标
  setTitleIcon(iconurl) {
    let $favicon = document.querySelector('link[rel="icon"]');

    if ($favicon !== null) {
      $favicon.href = iconurl;
    } else {
      $favicon = document.createElement("link");
      $favicon.rel = "icon";
      $favicon.href = iconurl;
      document.head.appendChild($favicon);
    }
  }
  formatDate(date, format) {
    if (!date) return;
    if (!format) format = "yyyy-MM-dd";
    switch (typeof date) {
      case "string":
        date = new Date(date.replace(/\-/g, "/"));
        break;

      case "number":
        date = new Date(date);
        break;
    }
    if (!date instanceof Date) return;
    var dict = {
      yyyy: date.getFullYear(),
      M: date.getMonth() + 1,
      d: date.getDate(),
      H: date.getHours(),
      m: date.getMinutes(),
      s: date.getSeconds(),
      MM: ("" + (date.getMonth() + 101)).substr(1),
      dd: ("" + (date.getDate() + 100)).substr(1),
      HH: ("" + (date.getHours() + 100)).substr(1),
      mm: ("" + (date.getMinutes() + 100)).substr(1),
      ss: ("" + (date.getSeconds() + 100)).substr(1)
    };
    return format.replace(/(yyyy|MM?|dd?|HH?|ss?|mm?)/g, function() {
      return dict[arguments[0]];
    });
  }
  getFiveYear() {
    var arr = [];
    for (var i = 0; i < 5; i++) {
      arr.push({
        value: this.thisyear - 2 + i,
        label: this.thisyear - 2 + i + "年"
      });
    }
    return arr;
  }
  getMonthOptions() {
    var arr = [];
    for (var i = 1; i < 32; i++) {
      arr.push({
        value: i,
        label: i + ""
      });
    }
    return arr;
  }
  getWeekOptions() {
    var arr = [];
    var dayArr = ["一", "二", "三", "四", "五", "六", "日"];
    for (var i = 0; i < 7; i++) {
      arr.push({
        value: i + 1,
        label: dayArr[i]
      });
    }
    return arr;
  }
  getHzm(file, name) {
    return file[name].split(".")[file[name].split(".").length - 1];
  }
  move(e, that, wid) {
    e.preventDefault();
    //算出鼠标相对元素的位置
    var disX = e.screenX;
    /* 保存原始的元素位置 */
    var leftOneBefore = that.$refs.contentOne.getBoundingClientRect().width;
    var leftTwoBefore = that.$refs.contentTwo.getBoundingClientRect().width;

    document.onmousemove = e => {
      //鼠标按下并移动的事件
      //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
      var left = e.screenX - disX;
      var leftOne = leftOneBefore + left;
      var leftTwo = leftTwoBefore - left;

      if (leftTwo < 500 || leftOne < wid) {
        return;
      }
      that.contentOneStyleObj.width = leftOne + "px";
      that.contentTwoStyleObj.width = leftTwo + "px";
    };
    document.onmouseup = e => {
      document.onmousemove = null;
      document.onmouseup = null;
    };
  }

  blobToDataURL(blob, callback) {
    var a = new FileReader();
    a.onload = function(e) {
      callback(e.target.result);
    };
    a.readAsDataURL(blob);
  }

  getBrowser(n) {
    var ua = navigator.userAgent.toLowerCase(),
      s,
      name = "",
      ver = 0;
    //探测浏览器
    (s = ua.match(/msie ([\d.]+)/))
      ? _set("ie", _toFixedVersion(s[1]))
      : (s = ua.match(/firefox\/([\d.]+)/))
      ? _set("firefox", _toFixedVersion(s[1]))
      : (s = ua.match(/chrome\/([\d.]+)/))
      ? _set("chrome", _toFixedVersion(s[1]))
      : (s = ua.match(/opera.([\d.]+)/))
      ? _set("opera", _toFixedVersion(s[1]))
      : (s = ua.match(/version\/([\d.]+).*safari/))
      ? _set("safari", _toFixedVersion(s[1]))
      : 0;

    function _toFixedVersion(ver, floatLength) {
      ver = ("" + ver).replace(/_/g, ".");
      floatLength = floatLength || 1;
      ver = String(ver).split(".");
      ver = ver[0] + "." + (ver[1] || "0");
      ver = Number(ver).toFixed(floatLength);
      return ver;
    }
    function _set(bname, bver) {
      name = bname;
      ver = bver;
    }
    return n == "n" ? name : n == "v" ? ver : name + ver;
  }
  // 获取当前月的最后一天
  getCurrentMonthLast(years, mouth) {
    var date = new Date();
    var currentMonth = mouth;
    var nextMonth = currentMonth;
    var nextMonthFirstDay = new Date(years, nextMonth, 1);
    var oneDay = 1000 * 60 * 60 * 24;
    var lastTime = new Date(nextMonthFirstDay - oneDay);
    var month = parseInt(lastTime.getMonth() + 1);
    var day = lastTime.getDate();
    if (month < 10) {
      month = "0" + month;
    }
    if (day < 10) {
      day = "0" + day;
    }
    var nowthisyear = this.formatDate(
      years + "-" + month + "-" + day,
      "yyyy年MM月dd"
    );
    return nowthisyear;
  }

  // 导出文件流
  exportTable(params, url) {
    let header = {
      headers: {
        sessionid: auth.getCookie("Session-Id")
      }
    };
    axios.defaults.headers.common["sessionid"] = auth.getCookie("Session-Id");
    axios({
      method: "POST",
      url: url,
      responseType: "blob",
      data: params
    })
      .then(response => {
        let fileName = response.headers["content-disposition"]
          .split(";")[1]
          .split("filename=")[1];
        let blob = new Blob([response.data], {
          type: "multipary/form-data"
        });
        if (window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(blob, decodeURI(fileName));
        } else {
          let objectUrl = URL.createObjectURL(blob);
          let link = document.createElement("a");
          link.style.display = "none";
          link.href = objectUrl;
          link.setAttribute("download", decodeURI(fileName));
          document.body.appendChild(link);
          link.click();
        }
      })
      .catch(() => {});
  }

  // 给选中的日期加一天，以保证查询的准确
  // 可参考组织生活-工作汇报中的日期选择器
  addTime(time) {
    var timestamp = Date.parse(new Date(time));
    timestamp = timestamp / 1000;
    timestamp += 86400; //加一天
    var newTime = this.formatDate(new Date(timestamp * 1000), "yyyy-MM-dd");
    return newTime;
  }

  // 下载文件
  // 没把file放在这边是因为每个接口获得的fileid可能对应字段不同(如fileId,file_id)
  downFile(data) {
    var headers = {
      headers: {
        sessionid: auth.getCookie("Session-Id")
      }
    };
    axios.defaults.headers.common["sessionid"] = auth.getCookie("Session-Id");
    axios({
      method: "POST",
      url: "/urc/baseset/compid",
      headers
    })
      .then(res => {
        const address = res.data.data.address;
        // const address = 'http://10.1.50.247:9680';// 本地调试用

        window.location.href =
          address + "/document/manager/downFile?fileid=" + data.fileid;
      })
      .catch(() => {});
  }
}
let util = new Funutil();
export default util;

export function debounce(func, delay, immediate = false) {
  let timer,
    context = this;
  return (...args) => {
    if (immediate) {
      func.apply(context, args);
      immediate = false;
      return;
    }
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(context, args);
    }, delay);
  };
}
