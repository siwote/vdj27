export default {
  area: [
    {
      pinyin: "BeiJing",
      children: [
        {
          pinyin: "BeiJing",
          children: [
            {
              pinyin: "DongCheng",
              shortname: "东城区"
            },
            {
              pinyin: "XiCheng",
              shortname: "西城区"
            },
            {
              pinyin: "ZhaoYang",
              shortname: "朝阳区"
            },
            {
              pinyin: "FengTai",
              shortname: "丰台区"
            },
            {
              pinyin: "ShiJingShan",
              shortname: "石景山区"
            },
            {
              pinyin: "HaiDian",
              shortname: "海淀区"
            },
            {
              pinyin: "MenTouGou",
              shortname: "门头沟区"
            },
            {
              pinyin: "FangShan",
              shortname: "房山区"
            },
            {
              pinyin: "TongZhou",
              shortname: "通州区"
            },
            {
              pinyin: "ShunYi",
              shortname: "顺义区"
            },
            {
              pinyin: "ChangPing",
              shortname: "昌平区"
            },
            {
              pinyin: "DaXing",
              shortname: "大兴区"
            },
            {
              pinyin: "HuaiRou",
              shortname: "怀柔区"
            },
            {
              pinyin: "PingGu",
              shortname: "平谷区"
            },
            {
              pinyin: "MiYun",
              shortname: "密云区"
            },
            {
              pinyin: "YanQing",
              shortname: "延庆区"
            }
          ],
          shortname: "直辖区"
        }
      ],
      shortname: "北京市"
    },
    {
      pinyin: "TianJin",
      children: [
        {
          pinyin: "TianJin",
          children: [
            {
              pinyin: "HePing",
              shortname: "和平区"
            },
            {
              pinyin: "HeDong",
              shortname: "河东区"
            },
            {
              pinyin: "HeXi",
              shortname: "河西区"
            },
            {
              pinyin: "NanKai",
              shortname: "南开区"
            },
            {
              pinyin: "HeBei",
              shortname: "河北区"
            },
            {
              pinyin: "HongQiao",
              shortname: "红桥区"
            },
            {
              pinyin: "DongLi",
              shortname: "东丽区"
            },
            {
              pinyin: "XiQing",
              shortname: "西青区"
            },
            {
              pinyin: "JinNan",
              shortname: "津南区"
            },
            {
              pinyin: "BeiChen",
              shortname: "北辰区"
            },
            {
              pinyin: "WuQing",
              shortname: "武清区"
            },
            {
              pinyin: "BaoDi",
              shortname: "宝坻区"
            },
            {
              pinyin: "BinHai",
              shortname: "滨海新区"
            },
            {
              pinyin: "NingHe",
              shortname: "宁河区"
            },
            {
              pinyin: "JingHai",
              shortname: "静海区"
            },
            {
              pinyin: "JiZhou",
              shortname: "蓟州区"
            }
          ],
          shortname: "直辖区"
        }
      ],
      shortname: "天津市"
    },
    {
      pinyin: "HeBei",
      children: [
        {
          pinyin: "ShiJiaZhuang",
          children: [
            {
              pinyin: "ShiJiaZhuang",
              shortname: "市辖区"
            },
            {
              pinyin: "ChangAn",
              shortname: "长安区"
            },
            {
              pinyin: "QiaoXi",
              shortname: "桥西区"
            },
            {
              pinyin: "XinHua",
              shortname: "新华区"
            },
            {
              pinyin: "JingXingKuang",
              shortname: "井陉矿区"
            },
            {
              pinyin: "YuHua",
              shortname: "裕华区"
            },
            {
              pinyin: "GaoCheng",
              shortname: "藁城区"
            },
            {
              pinyin: "LuQuan",
              shortname: "鹿泉区"
            },
            {
              pinyin: "LuanCheng",
              shortname: "栾城区"
            },
            {
              pinyin: "JingXing",
              shortname: "井陉县"
            },
            {
              pinyin: "ZhengDing",
              shortname: "正定县"
            },
            {
              pinyin: "XingTang",
              shortname: "行唐县"
            },
            {
              pinyin: "LingShou",
              shortname: "灵寿县"
            },
            {
              pinyin: "GaoYi",
              shortname: "高邑县"
            },
            {
              pinyin: "ShenZe",
              shortname: "深泽县"
            },
            {
              pinyin: "ZanHuang",
              shortname: "赞皇县"
            },
            {
              pinyin: "WuJi",
              shortname: "无极县"
            },
            {
              pinyin: "PingShan",
              shortname: "平山县"
            },
            {
              pinyin: "YuanShi",
              shortname: "元氏县"
            },
            {
              pinyin: "ZhaoXian",
              shortname: "赵县"
            },
            {
              pinyin: "ShiJiaZhuangGao",
              shortname: "石家庄高新技术产业开发区"
            },
            {
              pinyin: "ShiJiaZhuangXunHuanHuaGongYuan",
              shortname: "石家庄循环化工园区"
            },
            {
              pinyin: "XinJi",
              shortname: "辛集市"
            },
            {
              pinyin: "JinZhou",
              shortname: "晋州市"
            },
            {
              pinyin: "XinLe",
              shortname: "新乐市"
            }
          ],
          shortname: "石家庄市"
        },
        {
          pinyin: "TangShan",
          children: [
            {
              pinyin: "TangShan",
              shortname: "市辖区"
            },
            {
              pinyin: "LuNan",
              shortname: "路南区"
            },
            {
              pinyin: "LuBei",
              shortname: "路北区"
            },
            {
              pinyin: "GuYe",
              shortname: "古冶区"
            },
            {
              pinyin: "KaiPing",
              shortname: "开平区"
            },
            {
              pinyin: "FengNan",
              shortname: "丰南区"
            },
            {
              pinyin: "FengRun",
              shortname: "丰润区"
            },
            {
              pinyin: "CaoFeiDian",
              shortname: "曹妃甸区"
            },
            {
              pinyin: "LuanNan",
              shortname: "滦南县"
            },
            {
              pinyin: "LeTing",
              shortname: "乐亭县"
            },
            {
              pinyin: "QianXi",
              shortname: "迁西县"
            },
            {
              pinyin: "YuTian",
              shortname: "玉田县"
            },
            {
              pinyin: "TangShanLuTaiJingKaiQu",
              shortname: "唐山市芦台经济技术开发区"
            },
            {
              pinyin: "TangShanHanGuGuanLi",
              shortname: "唐山市汉沽管理区"
            },
            {
              pinyin: "TangShanGao",
              shortname: "唐山高新技术产业开发区"
            },
            {
              pinyin: "HeBeiTangShanHaiGangJingKaiQu",
              shortname: "河北唐山海港经济开发区"
            },
            {
              pinyin: "ZunHua",
              shortname: "遵化市"
            },
            {
              pinyin: "QianAn",
              shortname: "迁安市"
            },
            {
              pinyin: "LuanZhou",
              shortname: "滦州市"
            }
          ],
          shortname: "唐山市"
        },
        {
          pinyin: "QinHuangDao",
          children: [
            {
              pinyin: "QinHuangDao",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiGang",
              shortname: "海港区"
            },
            {
              pinyin: "ShanHaiGuan",
              shortname: "山海关区"
            },
            {
              pinyin: "BeiDaiHe",
              shortname: "北戴河区"
            },
            {
              pinyin: "FuNing",
              shortname: "抚宁区"
            },
            {
              pinyin: "QingLong",
              shortname: "青龙满族自治县"
            },
            {
              pinyin: "ChangLi",
              shortname: "昌黎县"
            },
            {
              pinyin: "LuLong",
              shortname: "卢龙县"
            },
            {
              pinyin: "QinHuangDaoJingKaiQu",
              shortname: "秦皇岛市经济技术开发区"
            },
            {
              pinyin: "BeiDaiHe",
              shortname: "北戴河新区"
            }
          ],
          shortname: "秦皇岛市"
        },
        {
          pinyin: "HanDan",
          children: [
            {
              pinyin: "HanDan",
              shortname: "市辖区"
            },
            {
              pinyin: "HanShan",
              shortname: "邯山区"
            },
            {
              pinyin: "CongTai",
              shortname: "丛台区"
            },
            {
              pinyin: "FuXing",
              shortname: "复兴区"
            },
            {
              pinyin: "FengFengKuang",
              shortname: "峰峰矿区"
            },
            {
              pinyin: "FeiXiang",
              shortname: "肥乡区"
            },
            {
              pinyin: "YongNian",
              shortname: "永年区"
            },
            {
              pinyin: "LinZhang",
              shortname: "临漳县"
            },
            {
              pinyin: "ChengAn",
              shortname: "成安县"
            },
            {
              pinyin: "DaMing",
              shortname: "大名县"
            },
            {
              pinyin: "SheXian",
              shortname: "涉县"
            },
            {
              pinyin: "CiXian",
              shortname: "磁县"
            },
            {
              pinyin: "QiuXian",
              shortname: "邱县"
            },
            {
              pinyin: "JiZe",
              shortname: "鸡泽县"
            },
            {
              pinyin: "GuangPing",
              shortname: "广平县"
            },
            {
              pinyin: "GuanTao",
              shortname: "馆陶县"
            },
            {
              pinyin: "WeiXian",
              shortname: "魏县"
            },
            {
              pinyin: "QuZhou",
              shortname: "曲周县"
            },
            {
              pinyin: "HanDanJingKaiQu",
              shortname: "邯郸经济技术开发区"
            },
            {
              pinyin: "HanDanJiNan",
              shortname: "邯郸冀南新区"
            },
            {
              pinyin: "WuAn",
              shortname: "武安市"
            }
          ],
          shortname: "邯郸市"
        },
        {
          pinyin: "XingTai",
          children: [
            {
              pinyin: "XingTai",
              shortname: "市辖区"
            },
            {
              pinyin: "QiaoDong",
              shortname: "桥东区"
            },
            {
              pinyin: "QiaoXi",
              shortname: "桥西区"
            },
            {
              pinyin: "XingTai",
              shortname: "邢台县"
            },
            {
              pinyin: "LinCheng",
              shortname: "临城县"
            },
            {
              pinyin: "NeiQiu",
              shortname: "内丘县"
            },
            {
              pinyin: "BaiXiang",
              shortname: "柏乡县"
            },
            {
              pinyin: "LongYao",
              shortname: "隆尧县"
            },
            {
              pinyin: "RenXian",
              shortname: "任县"
            },
            {
              pinyin: "NanHe",
              shortname: "南和县"
            },
            {
              pinyin: "NingJin",
              shortname: "宁晋县"
            },
            {
              pinyin: "JuLu",
              shortname: "巨鹿县"
            },
            {
              pinyin: "XinHe",
              shortname: "新河县"
            },
            {
              pinyin: "GuangZong",
              shortname: "广宗县"
            },
            {
              pinyin: "PingXiang",
              shortname: "平乡县"
            },
            {
              pinyin: "WeiXian",
              shortname: "威县"
            },
            {
              pinyin: "QingHe",
              shortname: "清河县"
            },
            {
              pinyin: "LinXi",
              shortname: "临西县"
            },
            {
              pinyin: "HeBeiXingTaiJingKaiQu",
              shortname: "河北邢台经济开发区"
            },
            {
              pinyin: "NanGong",
              shortname: "南宫市"
            },
            {
              pinyin: "ShaHe",
              shortname: "沙河市"
            }
          ],
          shortname: "邢台市"
        },
        {
          pinyin: "BaoDing",
          children: [
            {
              pinyin: "BaoDing",
              shortname: "市辖区"
            },
            {
              pinyin: "JingXiu",
              shortname: "竞秀区"
            },
            {
              pinyin: "LianChi",
              shortname: "莲池区"
            },
            {
              pinyin: "ManCheng",
              shortname: "满城区"
            },
            {
              pinyin: "QingYuan",
              shortname: "清苑区"
            },
            {
              pinyin: "XuShui",
              shortname: "徐水区"
            },
            {
              pinyin: "LaiShui",
              shortname: "涞水县"
            },
            {
              pinyin: "FuPing",
              shortname: "阜平县"
            },
            {
              pinyin: "DingXing",
              shortname: "定兴县"
            },
            {
              pinyin: "TangXian",
              shortname: "唐县"
            },
            {
              pinyin: "GaoYang",
              shortname: "高阳县"
            },
            {
              pinyin: "RongCheng",
              shortname: "容城县"
            },
            {
              pinyin: "LaiYuan",
              shortname: "涞源县"
            },
            {
              pinyin: "WangDu",
              shortname: "望都县"
            },
            {
              pinyin: "AnXin",
              shortname: "安新县"
            },
            {
              pinyin: "YiXian",
              shortname: "易县"
            },
            {
              pinyin: "QuYang",
              shortname: "曲阳县"
            },
            {
              pinyin: "LiXian",
              shortname: "蠡县"
            },
            {
              pinyin: "ShunPing",
              shortname: "顺平县"
            },
            {
              pinyin: "BoYe",
              shortname: "博野县"
            },
            {
              pinyin: "XiongXian",
              shortname: "雄县"
            },
            {
              pinyin: "BaoDingGao",
              shortname: "保定高新技术产业开发区"
            },
            {
              pinyin: "BaoDingBaiGouXinCheng",
              shortname: "保定白沟新城"
            },
            {
              pinyin: "ZhuoZhou",
              shortname: "涿州市"
            },
            {
              pinyin: "DingZhou",
              shortname: "定州市"
            },
            {
              pinyin: "AnGuo",
              shortname: "安国市"
            },
            {
              pinyin: "GaoBeiDian",
              shortname: "高碑店市"
            }
          ],
          shortname: "保定市"
        },
        {
          pinyin: "ZhangJiaKou",
          children: [
            {
              pinyin: "ZhangJiaKou",
              shortname: "市辖区"
            },
            {
              pinyin: "QiaoDong",
              shortname: "桥东区"
            },
            {
              pinyin: "QiaoXi",
              shortname: "桥西区"
            },
            {
              pinyin: "XuanHua",
              shortname: "宣化区"
            },
            {
              pinyin: "XiaHuaYuan",
              shortname: "下花园区"
            },
            {
              pinyin: "WanQuan",
              shortname: "万全区"
            },
            {
              pinyin: "ChongLi",
              shortname: "崇礼区"
            },
            {
              pinyin: "ZhangBei",
              shortname: "张北县"
            },
            {
              pinyin: "KangBao",
              shortname: "康保县"
            },
            {
              pinyin: "GuYuan",
              shortname: "沽源县"
            },
            {
              pinyin: "ShangYi",
              shortname: "尚义县"
            },
            {
              pinyin: "YuXian",
              shortname: "蔚县"
            },
            {
              pinyin: "YangYuan",
              shortname: "阳原县"
            },
            {
              pinyin: "HuaiAn",
              shortname: "怀安县"
            },
            {
              pinyin: "HuaiLai",
              shortname: "怀来县"
            },
            {
              pinyin: "ZhuoLu",
              shortname: "涿鹿县"
            },
            {
              pinyin: "ChiCheng",
              shortname: "赤城县"
            },
            {
              pinyin: "ZhangJiaKouGao",
              shortname: "张家口市高新技术产业开发区"
            },
            {
              pinyin: "ZhangJiaKouChaBeiGuanLi",
              shortname: "张家口市察北管理区"
            },
            {
              pinyin: "ZhangJiaKouSaiBeiGuanLi",
              shortname: "张家口市塞北管理区"
            }
          ],
          shortname: "张家口市"
        },
        {
          pinyin: "ChengDe",
          children: [
            {
              pinyin: "ChengDe",
              shortname: "市辖区"
            },
            {
              pinyin: "ShuangQiao",
              shortname: "双桥区"
            },
            {
              pinyin: "ShuangLuan",
              shortname: "双滦区"
            },
            {
              pinyin: "YingShouYingZiKuang",
              shortname: "鹰手营子矿区"
            },
            {
              pinyin: "ChengDe",
              shortname: "承德县"
            },
            {
              pinyin: "XingLong",
              shortname: "兴隆县"
            },
            {
              pinyin: "LuanPing",
              shortname: "滦平县"
            },
            {
              pinyin: "LongHua",
              shortname: "隆化县"
            },
            {
              pinyin: "FengNing",
              shortname: "丰宁满族自治县"
            },
            {
              pinyin: "KuanCheng",
              shortname: "宽城满族自治县"
            },
            {
              pinyin: "WeiChang",
              shortname: "围场满族蒙古族自治县"
            },
            {
              pinyin: "ChengDeGao",
              shortname: "承德高新技术产业开发区"
            },
            {
              pinyin: "PingQuan",
              shortname: "平泉市"
            }
          ],
          shortname: "承德市"
        },
        {
          pinyin: "CangZhou",
          children: [
            {
              pinyin: "CangZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "XinHua",
              shortname: "新华区"
            },
            {
              pinyin: "YunHe",
              shortname: "运河区"
            },
            {
              pinyin: "CangXian",
              shortname: "沧县"
            },
            {
              pinyin: "QingXian",
              shortname: "青县"
            },
            {
              pinyin: "DongGuang",
              shortname: "东光县"
            },
            {
              pinyin: "HaiXing",
              shortname: "海兴县"
            },
            {
              pinyin: "YanShan",
              shortname: "盐山县"
            },
            {
              pinyin: "SuNing",
              shortname: "肃宁县"
            },
            {
              pinyin: "NanPi",
              shortname: "南皮县"
            },
            {
              pinyin: "WuQiao",
              shortname: "吴桥县"
            },
            {
              pinyin: "XianXian",
              shortname: "献县"
            },
            {
              pinyin: "MengCun",
              shortname: "孟村回族自治县"
            },
            {
              pinyin: "HeBeiCangZhouJingKaiQu",
              shortname: "河北沧州经济开发区"
            },
            {
              pinyin: "CangZhouGao",
              shortname: "沧州高新技术产业开发区"
            },
            {
              pinyin: "CangZhouBoHai",
              shortname: "沧州渤海新区"
            },
            {
              pinyin: "PoTou",
              shortname: "泊头市"
            },
            {
              pinyin: "RenQiu",
              shortname: "任丘市"
            },
            {
              pinyin: "HuangHua",
              shortname: "黄骅市"
            },
            {
              pinyin: "HeJian",
              shortname: "河间市"
            }
          ],
          shortname: "沧州市"
        },
        {
          pinyin: "LangFang",
          children: [
            {
              pinyin: "LangFang",
              shortname: "市辖区"
            },
            {
              pinyin: "AnCi",
              shortname: "安次区"
            },
            {
              pinyin: "GuangYang",
              shortname: "广阳区"
            },
            {
              pinyin: "GuAn",
              shortname: "固安县"
            },
            {
              pinyin: "YongQing",
              shortname: "永清县"
            },
            {
              pinyin: "XiangHe",
              shortname: "香河县"
            },
            {
              pinyin: "DaiCheng",
              shortname: "大城县"
            },
            {
              pinyin: "WenAn",
              shortname: "文安县"
            },
            {
              pinyin: "DaChang",
              shortname: "大厂回族自治县"
            },
            {
              pinyin: "LangFangJingKaiQu",
              shortname: "廊坊经济技术开发区"
            },
            {
              pinyin: "BaZhou",
              shortname: "霸州市"
            },
            {
              pinyin: "SanHe",
              shortname: "三河市"
            }
          ],
          shortname: "廊坊市"
        },
        {
          pinyin: "HengShui",
          children: [
            {
              pinyin: "HengShui",
              shortname: "市辖区"
            },
            {
              pinyin: "TaoCheng",
              shortname: "桃城区"
            },
            {
              pinyin: "JiZhou",
              shortname: "冀州区"
            },
            {
              pinyin: "ZaoQiang",
              shortname: "枣强县"
            },
            {
              pinyin: "WuYi",
              shortname: "武邑县"
            },
            {
              pinyin: "WuQiang",
              shortname: "武强县"
            },
            {
              pinyin: "RaoYang",
              shortname: "饶阳县"
            },
            {
              pinyin: "AnPing",
              shortname: "安平县"
            },
            {
              pinyin: "GuCheng",
              shortname: "故城县"
            },
            {
              pinyin: "JingXian",
              shortname: "景县"
            },
            {
              pinyin: "FuCheng",
              shortname: "阜城县"
            },
            {
              pinyin: "HeBeiHengShuiGao",
              shortname: "河北衡水高新技术产业开发区"
            },
            {
              pinyin: "HengShuiBinHu",
              shortname: "衡水滨湖新区"
            },
            {
              pinyin: "ShenZhou",
              shortname: "深州市"
            }
          ],
          shortname: "衡水市"
        }
      ],
      shortname: "河北省"
    },
    {
      pinyin: "ShanXi",
      children: [
        {
          pinyin: "TaiYuan",
          children: [
            {
              pinyin: "TaiYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "XiaoDian",
              shortname: "小店区"
            },
            {
              pinyin: "YingZe",
              shortname: "迎泽区"
            },
            {
              pinyin: "XingHuaLing",
              shortname: "杏花岭区"
            },
            {
              pinyin: "JianCaoPing",
              shortname: "尖草坪区"
            },
            {
              pinyin: "WanBoLin",
              shortname: "万柏林区"
            },
            {
              pinyin: "JinYuan",
              shortname: "晋源区"
            },
            {
              pinyin: "QingXu",
              shortname: "清徐县"
            },
            {
              pinyin: "YangQu",
              shortname: "阳曲县"
            },
            {
              pinyin: "LouFan",
              shortname: "娄烦县"
            },
            {
              pinyin: "ShanXiShiFan",
              shortname: "山西转型综合改革示范区"
            },
            {
              pinyin: "GuJiao",
              shortname: "古交市"
            }
          ],
          shortname: "太原市"
        },
        {
          pinyin: "DaTong",
          children: [
            {
              pinyin: "DaTong",
              shortname: "市辖区"
            },
            {
              pinyin: "XinRong",
              shortname: "新荣区"
            },
            {
              pinyin: "PingCheng",
              shortname: "平城区"
            },
            {
              pinyin: "YunGang",
              shortname: "云冈区"
            },
            {
              pinyin: "YunZhou",
              shortname: "云州区"
            },
            {
              pinyin: "YangGao",
              shortname: "阳高县"
            },
            {
              pinyin: "TianZhen",
              shortname: "天镇县"
            },
            {
              pinyin: "GuangLing",
              shortname: "广灵县"
            },
            {
              pinyin: "LingQiu",
              shortname: "灵丘县"
            },
            {
              pinyin: "HunYuan",
              shortname: "浑源县"
            },
            {
              pinyin: "ZuoYun",
              shortname: "左云县"
            },
            {
              pinyin: "ShanXiDaTongJingKaiQu",
              shortname: "山西大同经济开发区"
            }
          ],
          shortname: "大同市"
        },
        {
          pinyin: "YangQuan",
          children: [
            {
              pinyin: "YangQuan",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengQu",
              shortname: "城区"
            },
            {
              pinyin: "KuangQu",
              shortname: "矿区"
            },
            {
              pinyin: "JiaoQu",
              shortname: "郊区"
            },
            {
              pinyin: "PingDing",
              shortname: "平定县"
            },
            {
              pinyin: "YuXian",
              shortname: "盂县"
            }
          ],
          shortname: "阳泉市"
        },
        {
          pinyin: "ChangZhi",
          children: [
            {
              pinyin: "ChangZhi",
              shortname: "市辖区"
            },
            {
              pinyin: "LuZhou",
              shortname: "潞州区"
            },
            {
              pinyin: "ShangDang",
              shortname: "上党区"
            },
            {
              pinyin: "TunLiu",
              shortname: "屯留区"
            },
            {
              pinyin: "LuCheng",
              shortname: "潞城区"
            },
            {
              pinyin: "XiangYuan",
              shortname: "襄垣县"
            },
            {
              pinyin: "PingShun",
              shortname: "平顺县"
            },
            {
              pinyin: "LiCheng",
              shortname: "黎城县"
            },
            {
              pinyin: "HuGuan",
              shortname: "壶关县"
            },
            {
              pinyin: "ZhangZi",
              shortname: "长子县"
            },
            {
              pinyin: "WuXiang",
              shortname: "武乡县"
            },
            {
              pinyin: "QinXian",
              shortname: "沁县"
            },
            {
              pinyin: "QinYuan",
              shortname: "沁源县"
            },
            {
              pinyin: "ShanXiChangZhiGao",
              shortname: "山西长治高新技术产业园区"
            }
          ],
          shortname: "长治市"
        },
        {
          pinyin: "JinCheng",
          children: [
            {
              pinyin: "JinCheng",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengQu",
              shortname: "城区"
            },
            {
              pinyin: "QinShui",
              shortname: "沁水县"
            },
            {
              pinyin: "YangCheng",
              shortname: "阳城县"
            },
            {
              pinyin: "LingChuan",
              shortname: "陵川县"
            },
            {
              pinyin: "ZeZhou",
              shortname: "泽州县"
            },
            {
              pinyin: "GaoPing",
              shortname: "高平市"
            }
          ],
          shortname: "晋城市"
        },
        {
          pinyin: "ShuoZhou",
          children: [
            {
              pinyin: "ShuoZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ShuoCheng",
              shortname: "朔城区"
            },
            {
              pinyin: "PingLu",
              shortname: "平鲁区"
            },
            {
              pinyin: "ShanYin",
              shortname: "山阴县"
            },
            {
              pinyin: "YingXian",
              shortname: "应县"
            },
            {
              pinyin: "YouYu",
              shortname: "右玉县"
            },
            {
              pinyin: "ShanXiShuoZhouJingKaiQu",
              shortname: "山西朔州经济开发区"
            },
            {
              pinyin: "HuaiRen",
              shortname: "怀仁市"
            }
          ],
          shortname: "朔州市"
        },
        {
          pinyin: "JinZhong",
          children: [
            {
              pinyin: "JinZhong",
              shortname: "市辖区"
            },
            {
              pinyin: "YuCi",
              shortname: "榆次区"
            },
            {
              pinyin: "YuShe",
              shortname: "榆社县"
            },
            {
              pinyin: "ZuoQuan",
              shortname: "左权县"
            },
            {
              pinyin: "HeShun",
              shortname: "和顺县"
            },
            {
              pinyin: "XiYang",
              shortname: "昔阳县"
            },
            {
              pinyin: "ShouYang",
              shortname: "寿阳县"
            },
            {
              pinyin: "TaiGu",
              shortname: "太谷县"
            },
            {
              pinyin: "QiXian",
              shortname: "祁县"
            },
            {
              pinyin: "PingYao",
              shortname: "平遥县"
            },
            {
              pinyin: "LingShi",
              shortname: "灵石县"
            },
            {
              pinyin: "JieXiu",
              shortname: "介休市"
            }
          ],
          shortname: "晋中市"
        },
        {
          pinyin: "YunCheng",
          children: [
            {
              pinyin: "YunCheng",
              shortname: "市辖区"
            },
            {
              pinyin: "YanHu",
              shortname: "盐湖区"
            },
            {
              pinyin: "LinYi",
              shortname: "临猗县"
            },
            {
              pinyin: "WanRong",
              shortname: "万荣县"
            },
            {
              pinyin: "WenXi",
              shortname: "闻喜县"
            },
            {
              pinyin: "JiShan",
              shortname: "稷山县"
            },
            {
              pinyin: "XinJiang",
              shortname: "新绛县"
            },
            {
              pinyin: "JiangXian",
              shortname: "绛县"
            },
            {
              pinyin: "YuanQu",
              shortname: "垣曲县"
            },
            {
              pinyin: "XiaXian",
              shortname: "夏县"
            },
            {
              pinyin: "PingLu",
              shortname: "平陆县"
            },
            {
              pinyin: "RuiCheng",
              shortname: "芮城县"
            },
            {
              pinyin: "YongJi",
              shortname: "永济市"
            },
            {
              pinyin: "HeJin",
              shortname: "河津市"
            }
          ],
          shortname: "运城市"
        },
        {
          pinyin: "XinZhou",
          children: [
            {
              pinyin: "XinZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "XinFu",
              shortname: "忻府区"
            },
            {
              pinyin: "DingXiang",
              shortname: "定襄县"
            },
            {
              pinyin: "WuTai",
              shortname: "五台县"
            },
            {
              pinyin: "DaiXian",
              shortname: "代县"
            },
            {
              pinyin: "FanShi",
              shortname: "繁峙县"
            },
            {
              pinyin: "NingWu",
              shortname: "宁武县"
            },
            {
              pinyin: "JingLe",
              shortname: "静乐县"
            },
            {
              pinyin: "ShenChi",
              shortname: "神池县"
            },
            {
              pinyin: "WuZhai",
              shortname: "五寨县"
            },
            {
              pinyin: "KeLan",
              shortname: "岢岚县"
            },
            {
              pinyin: "HeQu",
              shortname: "河曲县"
            },
            {
              pinyin: "BaoDe",
              shortname: "保德县"
            },
            {
              pinyin: "PianGuan",
              shortname: "偏关县"
            },
            {
              pinyin: "WuTaiShanFengJingQu",
              shortname: "五台山风景名胜区"
            },
            {
              pinyin: "YuanPing",
              shortname: "原平市"
            }
          ],
          shortname: "忻州市"
        },
        {
          pinyin: "LinFen",
          children: [
            {
              pinyin: "LinFen",
              shortname: "市辖区"
            },
            {
              pinyin: "YaoDu",
              shortname: "尧都区"
            },
            {
              pinyin: "QuWo",
              shortname: "曲沃县"
            },
            {
              pinyin: "YiCheng",
              shortname: "翼城县"
            },
            {
              pinyin: "XiangFen",
              shortname: "襄汾县"
            },
            {
              pinyin: "HongDong",
              shortname: "洪洞县"
            },
            {
              pinyin: "GuXian",
              shortname: "古县"
            },
            {
              pinyin: "AnZe",
              shortname: "安泽县"
            },
            {
              pinyin: "FuShan",
              shortname: "浮山县"
            },
            {
              pinyin: "JiXian",
              shortname: "吉县"
            },
            {
              pinyin: "XiangNing",
              shortname: "乡宁县"
            },
            {
              pinyin: "DaNing",
              shortname: "大宁县"
            },
            {
              pinyin: "XiXian",
              shortname: "隰县"
            },
            {
              pinyin: "YongHe",
              shortname: "永和县"
            },
            {
              pinyin: "PuXian",
              shortname: "蒲县"
            },
            {
              pinyin: "FenXi",
              shortname: "汾西县"
            },
            {
              pinyin: "HouMa",
              shortname: "侯马市"
            },
            {
              pinyin: "HuoZhou",
              shortname: "霍州市"
            }
          ],
          shortname: "临汾市"
        },
        {
          pinyin: "LvLiang",
          children: [
            {
              pinyin: "LvLiang",
              shortname: "市辖区"
            },
            {
              pinyin: "LiShi",
              shortname: "离石区"
            },
            {
              pinyin: "WenShui",
              shortname: "文水县"
            },
            {
              pinyin: "JiaoCheng",
              shortname: "交城县"
            },
            {
              pinyin: "XingXian",
              shortname: "兴县"
            },
            {
              pinyin: "LinXian",
              shortname: "临县"
            },
            {
              pinyin: "LiuLin",
              shortname: "柳林县"
            },
            {
              pinyin: "ShiLou",
              shortname: "石楼县"
            },
            {
              pinyin: "LanXian",
              shortname: "岚县"
            },
            {
              pinyin: "FangShan",
              shortname: "方山县"
            },
            {
              pinyin: "ZhongYang",
              shortname: "中阳县"
            },
            {
              pinyin: "JiaoKou",
              shortname: "交口县"
            },
            {
              pinyin: "XiaoYi",
              shortname: "孝义市"
            },
            {
              pinyin: "FenYang",
              shortname: "汾阳市"
            }
          ],
          shortname: "吕梁市"
        }
      ],
      shortname: "山西省"
    },
    {
      pinyin: "NeiMengGu",
      children: [
        {
          pinyin: "HuHeHaoTe",
          children: [
            {
              pinyin: "HuHeHaoTe",
              shortname: "市辖区"
            },
            {
              pinyin: "XinCheng",
              shortname: "新城区"
            },
            {
              pinyin: "HuiMin",
              shortname: "回民区"
            },
            {
              pinyin: "YuQuan",
              shortname: "玉泉区"
            },
            {
              pinyin: "SaiHan",
              shortname: "赛罕区"
            },
            {
              pinyin: "TuMoTeZuoQi",
              shortname: "土默特左旗"
            },
            {
              pinyin: "TuoKeTuo",
              shortname: "托克托县"
            },
            {
              pinyin: "HeLinGeEr",
              shortname: "和林格尔县"
            },
            {
              pinyin: "QingShuiHe",
              shortname: "清水河县"
            },
            {
              pinyin: "WuChuan",
              shortname: "武川县"
            },
            {
              pinyin: "HuHeHaoTeJinHaiGongYeYuan",
              shortname: "呼和浩特金海工业园区"
            },
            {
              pinyin: "HuHeHaoTeJingKaiQu",
              shortname: "呼和浩特经济技术开发区"
            }
          ],
          shortname: "呼和浩特市"
        },
        {
          pinyin: "BaoTou",
          children: [
            {
              pinyin: "BaoTou",
              shortname: "市辖区"
            },
            {
              pinyin: "DongHe",
              shortname: "东河区"
            },
            {
              pinyin: "KunDuLun",
              shortname: "昆都仑区"
            },
            {
              pinyin: "QingShan",
              shortname: "青山区"
            },
            {
              pinyin: "ShiGuai",
              shortname: "石拐区"
            },
            {
              pinyin: "BaiYunEBoKuang",
              shortname: "白云鄂博矿区"
            },
            {
              pinyin: "JiuYuan",
              shortname: "九原区"
            },
            {
              pinyin: "TuMoTeYouQi",
              shortname: "土默特右旗"
            },
            {
              pinyin: "GuYang",
              shortname: "固阳县"
            },
            {
              pinyin: "DaErHanMaoMingAn",
              shortname: "达尔罕茂明安联合旗"
            },
            {
              pinyin: "BaoTouXiTuGao",
              shortname: "包头稀土高新技术产业开发区"
            }
          ],
          shortname: "包头市"
        },
        {
          pinyin: "WuHai",
          children: [
            {
              pinyin: "WuHai",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiBoWan",
              shortname: "海勃湾区"
            },
            {
              pinyin: "HaiNan",
              shortname: "海南区"
            },
            {
              pinyin: "WuDa",
              shortname: "乌达区"
            }
          ],
          shortname: "乌海市"
        },
        {
          pinyin: "ChiFeng",
          children: [
            {
              pinyin: "ChiFeng",
              shortname: "市辖区"
            },
            {
              pinyin: "HongShan",
              shortname: "红山区"
            },
            {
              pinyin: "YuanBaoShan",
              shortname: "元宝山区"
            },
            {
              pinyin: "SongShan",
              shortname: "松山区"
            },
            {
              pinyin: "ALuKeErQinQi",
              shortname: "阿鲁科尔沁旗"
            },
            {
              pinyin: "BaLinZuoQi",
              shortname: "巴林左旗"
            },
            {
              pinyin: "BaLinYouQi",
              shortname: "巴林右旗"
            },
            {
              pinyin: "LinXi",
              shortname: "林西县"
            },
            {
              pinyin: "KeShiKeTengQi",
              shortname: "克什克腾旗"
            },
            {
              pinyin: "WengNiuTeQi",
              shortname: "翁牛特旗"
            },
            {
              pinyin: "KaLaQinQi",
              shortname: "喀喇沁旗"
            },
            {
              pinyin: "NingCheng",
              shortname: "宁城县"
            },
            {
              pinyin: "AoHanQi",
              shortname: "敖汉旗"
            }
          ],
          shortname: "赤峰市"
        },
        {
          pinyin: "TongLiao",
          children: [
            {
              pinyin: "TongLiao",
              shortname: "市辖区"
            },
            {
              pinyin: "KeErQin",
              shortname: "科尔沁区"
            },
            {
              pinyin: "KeErQinZuoYiZhongQi",
              shortname: "科尔沁左翼中旗"
            },
            {
              pinyin: "KeErQinZuoYiHouQi",
              shortname: "科尔沁左翼后旗"
            },
            {
              pinyin: "KaiLu",
              shortname: "开鲁县"
            },
            {
              pinyin: "KuLunQi",
              shortname: "库伦旗"
            },
            {
              pinyin: "NaiManQi",
              shortname: "奈曼旗"
            },
            {
              pinyin: "ZaLuTeQi",
              shortname: "扎鲁特旗"
            },
            {
              pinyin: "TongLiaoJingKaiQu",
              shortname: "通辽经济技术开发区"
            },
            {
              pinyin: "HuoLinGuoLe",
              shortname: "霍林郭勒市"
            }
          ],
          shortname: "通辽市"
        },
        {
          pinyin: "EErDuoSi",
          children: [
            {
              pinyin: "EErDuoSi",
              shortname: "市辖区"
            },
            {
              pinyin: "DongSheng",
              shortname: "东胜区"
            },
            {
              pinyin: "KangBaShen",
              shortname: "康巴什区"
            },
            {
              pinyin: "DaLaTeQi",
              shortname: "达拉特旗"
            },
            {
              pinyin: "ZhunGeErQi",
              shortname: "准格尔旗"
            },
            {
              pinyin: "ETuoKeQianQi",
              shortname: "鄂托克前旗"
            },
            {
              pinyin: "ETuoKeQi",
              shortname: "鄂托克旗"
            },
            {
              pinyin: "HangJinQi",
              shortname: "杭锦旗"
            },
            {
              pinyin: "WuShenQi",
              shortname: "乌审旗"
            },
            {
              pinyin: "YiJinHuoLuoQi",
              shortname: "伊金霍洛旗"
            }
          ],
          shortname: "鄂尔多斯市"
        },
        {
          pinyin: "HuLunBeiEr",
          children: [
            {
              pinyin: "HuLunBeiEr",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiLaEr",
              shortname: "海拉尔区"
            },
            {
              pinyin: "ZhaLaiNuoEr",
              shortname: "扎赉诺尔区"
            },
            {
              pinyin: "ARongQi",
              shortname: "阿荣旗"
            },
            {
              pinyin: "MoLiDaWa",
              shortname: "莫力达瓦达斡尔族自治旗"
            },
            {
              pinyin: "ELunChunZiZhiQi",
              shortname: "鄂伦春自治旗"
            },
            {
              pinyin: "EWenKeZuZiZhiQi",
              shortname: "鄂温克族自治旗"
            },
            {
              pinyin: "ChenBaErHuQi",
              shortname: "陈巴尔虎旗"
            },
            {
              pinyin: "XinBaErHuZuoQi",
              shortname: "新巴尔虎左旗"
            },
            {
              pinyin: "XinBaErHuYouQi",
              shortname: "新巴尔虎右旗"
            },
            {
              pinyin: "ManZhouLi",
              shortname: "满洲里市"
            },
            {
              pinyin: "YaKeShi",
              shortname: "牙克石市"
            },
            {
              pinyin: "ZhaLanTun",
              shortname: "扎兰屯市"
            },
            {
              pinyin: "EErGuNa",
              shortname: "额尔古纳市"
            },
            {
              pinyin: "GenHe",
              shortname: "根河市"
            }
          ],
          shortname: "呼伦贝尔市"
        },
        {
          pinyin: "BaYanNaoEr",
          children: [
            {
              pinyin: "BaYanNaoEr",
              shortname: "市辖区"
            },
            {
              pinyin: "LinHe",
              shortname: "临河区"
            },
            {
              pinyin: "WuYuan",
              shortname: "五原县"
            },
            {
              pinyin: "DengKou",
              shortname: "磴口县"
            },
            {
              pinyin: "WuLTeQianQi",
              shortname: "乌拉特前旗"
            },
            {
              pinyin: "WuLTeZhongQi",
              shortname: "乌拉特中旗"
            },
            {
              pinyin: "WuLTeHouQi",
              shortname: "乌拉特后旗"
            },
            {
              pinyin: "HangJinHouQi",
              shortname: "杭锦后旗"
            }
          ],
          shortname: "巴彦淖尔市"
        },
        {
          pinyin: "WuLanChaBu",
          children: [
            {
              pinyin: "WuLanChaBu",
              shortname: "市辖区"
            },
            {
              pinyin: "JiNing",
              shortname: "集宁区"
            },
            {
              pinyin: "ZhuoZi",
              shortname: "卓资县"
            },
            {
              pinyin: "HuaDe",
              shortname: "化德县"
            },
            {
              pinyin: "ShangDu",
              shortname: "商都县"
            },
            {
              pinyin: "XingHe",
              shortname: "兴和县"
            },
            {
              pinyin: "LiangCheng",
              shortname: "凉城县"
            },
            {
              pinyin: "ChaHaErYouYiQianQi",
              shortname: "察哈尔右翼前旗"
            },
            {
              pinyin: "ChaHaErYouYiZhongQi",
              shortname: "察哈尔右翼中旗"
            },
            {
              pinyin: "ChaHaErYouYiHouQi",
              shortname: "察哈尔右翼后旗"
            },
            {
              pinyin: "SiZiWangQi",
              shortname: "四子王旗"
            },
            {
              pinyin: "FengZhen",
              shortname: "丰镇市"
            }
          ],
          shortname: "乌兰察布市"
        },
        {
          pinyin: "XingAn",
          children: [
            {
              pinyin: "WuLanHaoTe",
              shortname: "乌兰浩特市"
            },
            {
              pinyin: "AErShan",
              shortname: "阿尔山市"
            },
            {
              pinyin: "KeErQinYouYiQianQi",
              shortname: "科尔沁右翼前旗"
            },
            {
              pinyin: "KeErQinYouYiZhongQi",
              shortname: "科尔沁右翼中旗"
            },
            {
              pinyin: "ZhaLaiTeQi",
              shortname: "扎赉特旗"
            },
            {
              pinyin: "TuQuan",
              shortname: "突泉县"
            }
          ],
          shortname: "兴安盟"
        },
        {
          pinyin: "XiLinGuoLe",
          children: [
            {
              pinyin: "ErLianHaoTe",
              shortname: "二连浩特市"
            },
            {
              pinyin: "XiLinHaoTe",
              shortname: "锡林浩特市"
            },
            {
              pinyin: "ABaGaQi",
              shortname: "阿巴嘎旗"
            },
            {
              pinyin: "SuNiTeZuoQi",
              shortname: "苏尼特左旗"
            },
            {
              pinyin: "SuNiTeYouQi",
              shortname: "苏尼特右旗"
            },
            {
              pinyin: "DongWuZhuMuQinQi",
              shortname: "东乌珠穆沁旗"
            },
            {
              pinyin: "XiWuZhuMuQinQi",
              shortname: "西乌珠穆沁旗"
            },
            {
              pinyin: "TaiPuSiQi",
              shortname: "太仆寺旗"
            },
            {
              pinyin: "XiangHuangQi",
              shortname: "镶黄旗"
            },
            {
              pinyin: "ZhengXiangBaiQi",
              shortname: "正镶白旗"
            },
            {
              pinyin: "ZhengLanQi",
              shortname: "正蓝旗"
            },
            {
              pinyin: "DuoLun",
              shortname: "多伦县"
            },
            {
              pinyin: "WuLGai",
              shortname: "乌拉盖管委会"
            }
          ],
          shortname: "锡林郭勒盟"
        },
        {
          pinyin: "ALaShan",
          children: [
            {
              pinyin: "ALaShanZuoQi",
              shortname: "阿拉善左旗"
            },
            {
              pinyin: "ALaShanYouQi",
              shortname: "阿拉善右旗"
            },
            {
              pinyin: "EJiNaQi",
              shortname: "额济纳旗"
            },
            {
              pinyin: "NeiALaShanJingKaiQu",
              shortname: "内蒙古阿拉善经济开发区"
            }
          ],
          shortname: "阿拉善盟"
        }
      ],
      shortname: "内蒙古自治区"
    },
    {
      pinyin: "LiaoNing",
      children: [
        {
          pinyin: "ShenYang",
          children: [
            {
              pinyin: "ShenYang",
              shortname: "市辖区"
            },
            {
              pinyin: "HePing",
              shortname: "和平区"
            },
            {
              pinyin: "ShenHe",
              shortname: "沈河区"
            },
            {
              pinyin: "DaDong",
              shortname: "大东区"
            },
            {
              pinyin: "HuangGu",
              shortname: "皇姑区"
            },
            {
              pinyin: "TieXi",
              shortname: "铁西区"
            },
            {
              pinyin: "SuJiaTun",
              shortname: "苏家屯区"
            },
            {
              pinyin: "HunNan",
              shortname: "浑南区"
            },
            {
              pinyin: "ShenBei",
              shortname: "沈北新区"
            },
            {
              pinyin: "YuHong",
              shortname: "于洪区"
            },
            {
              pinyin: "LiaoZhong",
              shortname: "辽中区"
            },
            {
              pinyin: "KangPing",
              shortname: "康平县"
            },
            {
              pinyin: "FaKu",
              shortname: "法库县"
            },
            {
              pinyin: "XinMin",
              shortname: "新民市"
            }
          ],
          shortname: "沈阳市"
        },
        {
          pinyin: "DaLian",
          children: [
            {
              pinyin: "DaLian",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhongShan",
              shortname: "中山区"
            },
            {
              pinyin: "XiGang",
              shortname: "西岗区"
            },
            {
              pinyin: "ShaHeKou",
              shortname: "沙河口区"
            },
            {
              pinyin: "GanJingZi",
              shortname: "甘井子区"
            },
            {
              pinyin: "LvShunKou",
              shortname: "旅顺口区"
            },
            {
              pinyin: "JinZhou",
              shortname: "金州区"
            },
            {
              pinyin: "PuLanDian",
              shortname: "普兰店区"
            },
            {
              pinyin: "ChangHai",
              shortname: "长海县"
            },
            {
              pinyin: "WaFangDian",
              shortname: "瓦房店市"
            },
            {
              pinyin: "ZhuangHe",
              shortname: "庄河市"
            }
          ],
          shortname: "大连市"
        },
        {
          pinyin: "AnShan",
          children: [
            {
              pinyin: "AnShan",
              shortname: "市辖区"
            },
            {
              pinyin: "TieDong",
              shortname: "铁东区"
            },
            {
              pinyin: "TieXi",
              shortname: "铁西区"
            },
            {
              pinyin: "LiShan",
              shortname: "立山区"
            },
            {
              pinyin: "QianShan",
              shortname: "千山区"
            },
            {
              pinyin: "TaiAn",
              shortname: "台安县"
            },
            {
              pinyin: "XiuYan",
              shortname: "岫岩满族自治县"
            },
            {
              pinyin: "HaiCheng",
              shortname: "海城市"
            }
          ],
          shortname: "鞍山市"
        },
        {
          pinyin: "FuShun",
          children: [
            {
              pinyin: "FuShun",
              shortname: "市辖区"
            },
            {
              pinyin: "XinFu",
              shortname: "新抚区"
            },
            {
              pinyin: "DongZhou",
              shortname: "东洲区"
            },
            {
              pinyin: "WangHua",
              shortname: "望花区"
            },
            {
              pinyin: "ShunCheng",
              shortname: "顺城区"
            },
            {
              pinyin: "FuShun",
              shortname: "抚顺县"
            },
            {
              pinyin: "XinBin",
              shortname: "新宾满族自治县"
            },
            {
              pinyin: "QingYuan",
              shortname: "清原满族自治县"
            }
          ],
          shortname: "抚顺市"
        },
        {
          pinyin: "BenXi",
          children: [
            {
              pinyin: "BenXi",
              shortname: "市辖区"
            },
            {
              pinyin: "PingShan",
              shortname: "平山区"
            },
            {
              pinyin: "XiHu",
              shortname: "溪湖区"
            },
            {
              pinyin: "MingShan",
              shortname: "明山区"
            },
            {
              pinyin: "NanFen",
              shortname: "南芬区"
            },
            {
              pinyin: "BenXi",
              shortname: "本溪满族自治县"
            },
            {
              pinyin: "HuanRen",
              shortname: "桓仁满族自治县"
            }
          ],
          shortname: "本溪市"
        },
        {
          pinyin: "DanDong",
          children: [
            {
              pinyin: "DanDong",
              shortname: "市辖区"
            },
            {
              pinyin: "YuanBao",
              shortname: "元宝区"
            },
            {
              pinyin: "ZhenXing",
              shortname: "振兴区"
            },
            {
              pinyin: "ZhenAn",
              shortname: "振安区"
            },
            {
              pinyin: "KuanDian",
              shortname: "宽甸满族自治县"
            },
            {
              pinyin: "DongGang",
              shortname: "东港市"
            },
            {
              pinyin: "FengCheng",
              shortname: "凤城市"
            }
          ],
          shortname: "丹东市"
        },
        {
          pinyin: "JinZhou",
          children: [
            {
              pinyin: "JinZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "GuTa",
              shortname: "古塔区"
            },
            {
              pinyin: "LingHe",
              shortname: "凌河区"
            },
            {
              pinyin: "TaiHe",
              shortname: "太和区"
            },
            {
              pinyin: "HeiShan",
              shortname: "黑山县"
            },
            {
              pinyin: "YiXian",
              shortname: "义县"
            },
            {
              pinyin: "LingHai",
              shortname: "凌海市"
            },
            {
              pinyin: "BeiZhen",
              shortname: "北镇市"
            }
          ],
          shortname: "锦州市"
        },
        {
          pinyin: "YingKou",
          children: [
            {
              pinyin: "YingKou",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhanQian",
              shortname: "站前区"
            },
            {
              pinyin: "XiShi",
              shortname: "西市区"
            },
            {
              pinyin: "BaYuQuan",
              shortname: "鲅鱼圈区"
            },
            {
              pinyin: "LaoBian",
              shortname: "老边区"
            },
            {
              pinyin: "GaiZhou",
              shortname: "盖州市"
            },
            {
              pinyin: "DaShiQiao",
              shortname: "大石桥市"
            }
          ],
          shortname: "营口市"
        },
        {
          pinyin: "FuXin",
          children: [
            {
              pinyin: "FuXin",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiZhou",
              shortname: "海州区"
            },
            {
              pinyin: "XinQiu",
              shortname: "新邱区"
            },
            {
              pinyin: "TaiPing",
              shortname: "太平区"
            },
            {
              pinyin: "QingHeMen",
              shortname: "清河门区"
            },
            {
              pinyin: "XiHe",
              shortname: "细河区"
            },
            {
              pinyin: "FuXin",
              shortname: "阜新蒙古族自治县"
            },
            {
              pinyin: "ZhangWu",
              shortname: "彰武县"
            }
          ],
          shortname: "阜新市"
        },
        {
          pinyin: "LiaoYang",
          children: [
            {
              pinyin: "LiaoYang",
              shortname: "市辖区"
            },
            {
              pinyin: "BaiTa",
              shortname: "白塔区"
            },
            {
              pinyin: "WenSheng",
              shortname: "文圣区"
            },
            {
              pinyin: "HongWei",
              shortname: "宏伟区"
            },
            {
              pinyin: "GongChangLing",
              shortname: "弓长岭区"
            },
            {
              pinyin: "TaiZiHe",
              shortname: "太子河区"
            },
            {
              pinyin: "LiaoYang",
              shortname: "辽阳县"
            },
            {
              pinyin: "DengTa",
              shortname: "灯塔市"
            }
          ],
          shortname: "辽阳市"
        },
        {
          pinyin: "PanJin",
          children: [
            {
              pinyin: "PanJin",
              shortname: "市辖区"
            },
            {
              pinyin: "ShuangTaiZi",
              shortname: "双台子区"
            },
            {
              pinyin: "XingLongTai",
              shortname: "兴隆台区"
            },
            {
              pinyin: "DaWa",
              shortname: "大洼区"
            },
            {
              pinyin: "PanShan",
              shortname: "盘山县"
            }
          ],
          shortname: "盘锦市"
        },
        {
          pinyin: "TieLing",
          children: [
            {
              pinyin: "TieLing",
              shortname: "市辖区"
            },
            {
              pinyin: "YinZhou",
              shortname: "银州区"
            },
            {
              pinyin: "QingHe",
              shortname: "清河区"
            },
            {
              pinyin: "TieLing",
              shortname: "铁岭县"
            },
            {
              pinyin: "XiFeng",
              shortname: "西丰县"
            },
            {
              pinyin: "ChangTu",
              shortname: "昌图县"
            },
            {
              pinyin: "DiaoBingShan",
              shortname: "调兵山市"
            },
            {
              pinyin: "KaiYuan",
              shortname: "开原市"
            }
          ],
          shortname: "铁岭市"
        },
        {
          pinyin: "ZhaoYang",
          children: [
            {
              pinyin: "ZhaoYang",
              shortname: "市辖区"
            },
            {
              pinyin: "ShuangTa",
              shortname: "双塔区"
            },
            {
              pinyin: "LongCheng",
              shortname: "龙城区"
            },
            {
              pinyin: "ZhaoYang",
              shortname: "朝阳县"
            },
            {
              pinyin: "JianPing",
              shortname: "建平县"
            },
            {
              pinyin: "KaLaQinZuoYi",
              shortname: "喀喇沁左翼蒙古族自治县"
            },
            {
              pinyin: "BeiPiao",
              shortname: "北票市"
            },
            {
              pinyin: "LingYuan",
              shortname: "凌源市"
            }
          ],
          shortname: "朝阳市"
        },
        {
          pinyin: "HuLuDao",
          children: [
            {
              pinyin: "HuLuDao",
              shortname: "市辖区"
            },
            {
              pinyin: "LianShan",
              shortname: "连山区"
            },
            {
              pinyin: "LongGang",
              shortname: "龙港区"
            },
            {
              pinyin: "NanPiao",
              shortname: "南票区"
            },
            {
              pinyin: "SuiZhong",
              shortname: "绥中县"
            },
            {
              pinyin: "JianChang",
              shortname: "建昌县"
            },
            {
              pinyin: "XingCheng",
              shortname: "兴城市"
            }
          ],
          shortname: "葫芦岛市"
        }
      ],
      shortname: "辽宁省"
    },
    {
      pinyin: "JiLin",
      children: [
        {
          pinyin: "ChangChun",
          children: [
            {
              pinyin: "ChangChun",
              shortname: "市辖区"
            },
            {
              pinyin: "NanGuan",
              shortname: "南关区"
            },
            {
              pinyin: "KuanCheng",
              shortname: "宽城区"
            },
            {
              pinyin: "ZhaoYang",
              shortname: "朝阳区"
            },
            {
              pinyin: "ErDao",
              shortname: "二道区"
            },
            {
              pinyin: "LvYuan",
              shortname: "绿园区"
            },
            {
              pinyin: "ShuangYang",
              shortname: "双阳区"
            },
            {
              pinyin: "JiuTai",
              shortname: "九台区"
            },
            {
              pinyin: "NongAn",
              shortname: "农安县"
            },
            {
              pinyin: "ChangChunJingKaiQu",
              shortname: "长春经济技术开发区"
            },
            {
              pinyin: "ChangChunJingYueGao",
              shortname: "长春净月高新技术产业开发区"
            },
            {
              pinyin: "ChangChunGao",
              shortname: "长春高新技术产业开发区"
            },
            {
              pinyin: "ChangChunQiCheJingKaiQu",
              shortname: "长春汽车经济技术开发区"
            },
            {
              pinyin: "YuShu",
              shortname: "榆树市"
            },
            {
              pinyin: "DeHui",
              shortname: "德惠市"
            }
          ],
          shortname: "长春市"
        },
        {
          pinyin: "JiLin",
          children: [
            {
              pinyin: "JiLin",
              shortname: "市辖区"
            },
            {
              pinyin: "ChangYi",
              shortname: "昌邑区"
            },
            {
              pinyin: "LongTan",
              shortname: "龙潭区"
            },
            {
              pinyin: "ChuanYing",
              shortname: "船营区"
            },
            {
              pinyin: "FengMan",
              shortname: "丰满区"
            },
            {
              pinyin: "YongJi",
              shortname: "永吉县"
            },
            {
              pinyin: "JiLinJingKaiQu",
              shortname: "吉林经济开发区"
            },
            {
              pinyin: "JiLinGao",
              shortname: "吉林高新技术产业开发区"
            },
            {
              pinyin: "JiLinZhongGuoXinJiaPoShiPin",
              shortname: "吉林中国新加坡食品区"
            },
            {
              pinyin: "JiaoHe",
              shortname: "蛟河市"
            },
            {
              pinyin: "HuaDian",
              shortname: "桦甸市"
            },
            {
              pinyin: "ShuLan",
              shortname: "舒兰市"
            },
            {
              pinyin: "PanShi",
              shortname: "磐石市"
            }
          ],
          shortname: "吉林市"
        },
        {
          pinyin: "SiPing",
          children: [
            {
              pinyin: "SiPing",
              shortname: "市辖区"
            },
            {
              pinyin: "TieXi",
              shortname: "铁西区"
            },
            {
              pinyin: "TieDong",
              shortname: "铁东区"
            },
            {
              pinyin: "LiShu",
              shortname: "梨树县"
            },
            {
              pinyin: "YiTong",
              shortname: "伊通满族自治县"
            },
            {
              pinyin: "GongZhuLing",
              shortname: "公主岭市"
            },
            {
              pinyin: "ShuangLiao",
              shortname: "双辽市"
            }
          ],
          shortname: "四平市"
        },
        {
          pinyin: "LiaoYuan",
          children: [
            {
              pinyin: "LiaoYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "LongShan",
              shortname: "龙山区"
            },
            {
              pinyin: "XiAn",
              shortname: "西安区"
            },
            {
              pinyin: "DongFeng",
              shortname: "东丰县"
            },
            {
              pinyin: "DongLiao",
              shortname: "东辽县"
            }
          ],
          shortname: "辽源市"
        },
        {
          pinyin: "TongHua",
          children: [
            {
              pinyin: "TongHua",
              shortname: "市辖区"
            },
            {
              pinyin: "DongChang",
              shortname: "东昌区"
            },
            {
              pinyin: "ErDaoJiang",
              shortname: "二道江区"
            },
            {
              pinyin: "TongHua",
              shortname: "通化县"
            },
            {
              pinyin: "HuiNan",
              shortname: "辉南县"
            },
            {
              pinyin: "LiuHe",
              shortname: "柳河县"
            },
            {
              pinyin: "MeiHeKou",
              shortname: "梅河口市"
            },
            {
              pinyin: "JiAn",
              shortname: "集安市"
            }
          ],
          shortname: "通化市"
        },
        {
          pinyin: "BaiShan",
          children: [
            {
              pinyin: "BaiShan",
              shortname: "市辖区"
            },
            {
              pinyin: "HunJiang",
              shortname: "浑江区"
            },
            {
              pinyin: "JiangYuan",
              shortname: "江源区"
            },
            {
              pinyin: "FuSong",
              shortname: "抚松县"
            },
            {
              pinyin: "JingYu",
              shortname: "靖宇县"
            },
            {
              pinyin: "ChangBai",
              shortname: "长白朝鲜族自治县"
            },
            {
              pinyin: "LinJiang",
              shortname: "临江市"
            }
          ],
          shortname: "白山市"
        },
        {
          pinyin: "SongYuan",
          children: [
            {
              pinyin: "SongYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "NingJiang",
              shortname: "宁江区"
            },
            {
              pinyin: "QianGuoErLuoSi",
              shortname: "前郭尔罗斯蒙古族自治县"
            },
            {
              pinyin: "ChangLing",
              shortname: "长岭县"
            },
            {
              pinyin: "QianAn",
              shortname: "乾安县"
            },
            {
              pinyin: "JiLinSongYuanJingKaiQu",
              shortname: "吉林松原经济开发区"
            },
            {
              pinyin: "FuYu",
              shortname: "扶余市"
            }
          ],
          shortname: "松原市"
        },
        {
          pinyin: "BaiCheng",
          children: [
            {
              pinyin: "BaiCheng",
              shortname: "市辖区"
            },
            {
              pinyin: "TaoBei",
              shortname: "洮北区"
            },
            {
              pinyin: "ZhenLai",
              shortname: "镇赉县"
            },
            {
              pinyin: "TongYu",
              shortname: "通榆县"
            },
            {
              pinyin: "JiLinBaiChengJingKaiQu",
              shortname: "吉林白城经济开发区"
            },
            {
              pinyin: "TaoNan",
              shortname: "洮南市"
            },
            {
              pinyin: "DaAn",
              shortname: "大安市"
            }
          ],
          shortname: "白城市"
        },
        {
          pinyin: "YanBian",
          children: [
            {
              pinyin: "YanJi",
              shortname: "延吉市"
            },
            {
              pinyin: "TuMen",
              shortname: "图们市"
            },
            {
              pinyin: "DunHua",
              shortname: "敦化市"
            },
            {
              pinyin: "HuiChun",
              shortname: "珲春市"
            },
            {
              pinyin: "LongJing",
              shortname: "龙井市"
            },
            {
              pinyin: "HeLong",
              shortname: "和龙市"
            },
            {
              pinyin: "WangQing",
              shortname: "汪清县"
            },
            {
              pinyin: "AnTu",
              shortname: "安图县"
            }
          ],
          shortname: "延边朝鲜族自治州"
        }
      ],
      shortname: "吉林省"
    },
    {
      pinyin: "HeiLongJiang",
      children: [
        {
          pinyin: "HaErBin",
          children: [
            {
              pinyin: "HaErBin",
              shortname: "市辖区"
            },
            {
              pinyin: "DaoLi",
              shortname: "道里区"
            },
            {
              pinyin: "NanGang",
              shortname: "南岗区"
            },
            {
              pinyin: "DaoWai",
              shortname: "道外区"
            },
            {
              pinyin: "PingFang",
              shortname: "平房区"
            },
            {
              pinyin: "SongBei",
              shortname: "松北区"
            },
            {
              pinyin: "XiangFang",
              shortname: "香坊区"
            },
            {
              pinyin: "HuLan",
              shortname: "呼兰区"
            },
            {
              pinyin: "ACheng",
              shortname: "阿城区"
            },
            {
              pinyin: "ShuangCheng",
              shortname: "双城区"
            },
            {
              pinyin: "YiLan",
              shortname: "依兰县"
            },
            {
              pinyin: "FangZheng",
              shortname: "方正县"
            },
            {
              pinyin: "BinXian",
              shortname: "宾县"
            },
            {
              pinyin: "BaYan",
              shortname: "巴彦县"
            },
            {
              pinyin: "MuLan",
              shortname: "木兰县"
            },
            {
              pinyin: "TongHe",
              shortname: "通河县"
            },
            {
              pinyin: "YanShou",
              shortname: "延寿县"
            },
            {
              pinyin: "ShangZhi",
              shortname: "尚志市"
            },
            {
              pinyin: "WuChang",
              shortname: "五常市"
            }
          ],
          shortname: "哈尔滨市"
        },
        {
          pinyin: "QiQiHaEr",
          children: [
            {
              pinyin: "QiQiHaEr",
              shortname: "市辖区"
            },
            {
              pinyin: "LongSha",
              shortname: "龙沙区"
            },
            {
              pinyin: "JianHua",
              shortname: "建华区"
            },
            {
              pinyin: "TieFeng",
              shortname: "铁锋区"
            },
            {
              pinyin: "AngAngXi",
              shortname: "昂昂溪区"
            },
            {
              pinyin: "FuLaErJi",
              shortname: "富拉尔基区"
            },
            {
              pinyin: "NianZiShan",
              shortname: "碾子山区"
            },
            {
              pinyin: "MeiLiSi",
              shortname: "梅里斯达斡尔族区"
            },
            {
              pinyin: "LongJiang",
              shortname: "龙江县"
            },
            {
              pinyin: "YiAn",
              shortname: "依安县"
            },
            {
              pinyin: "TaiLai",
              shortname: "泰来县"
            },
            {
              pinyin: "GanNan",
              shortname: "甘南县"
            },
            {
              pinyin: "FuYu",
              shortname: "富裕县"
            },
            {
              pinyin: "KeShan",
              shortname: "克山县"
            },
            {
              pinyin: "KeDong",
              shortname: "克东县"
            },
            {
              pinyin: "BaiQuan",
              shortname: "拜泉县"
            },
            {
              pinyin: "NeHe",
              shortname: "讷河市"
            }
          ],
          shortname: "齐齐哈尔市"
        },
        {
          pinyin: "JiXi",
          children: [
            {
              pinyin: "JiXi",
              shortname: "市辖区"
            },
            {
              pinyin: "JiGuan",
              shortname: "鸡冠区"
            },
            {
              pinyin: "HengShan",
              shortname: "恒山区"
            },
            {
              pinyin: "DiDao",
              shortname: "滴道区"
            },
            {
              pinyin: "LiShu",
              shortname: "梨树区"
            },
            {
              pinyin: "ChengZiHe",
              shortname: "城子河区"
            },
            {
              pinyin: "MaShan",
              shortname: "麻山区"
            },
            {
              pinyin: "JiDong",
              shortname: "鸡东县"
            },
            {
              pinyin: "HuLin",
              shortname: "虎林市"
            },
            {
              pinyin: "MiShan",
              shortname: "密山市"
            }
          ],
          shortname: "鸡西市"
        },
        {
          pinyin: "HeGang",
          children: [
            {
              pinyin: "HeGang",
              shortname: "市辖区"
            },
            {
              pinyin: "XiangYang",
              shortname: "向阳区"
            },
            {
              pinyin: "GongNong",
              shortname: "工农区"
            },
            {
              pinyin: "NanShan",
              shortname: "南山区"
            },
            {
              pinyin: "XingAn",
              shortname: "兴安区"
            },
            {
              pinyin: "DongShan",
              shortname: "东山区"
            },
            {
              pinyin: "XingShan",
              shortname: "兴山区"
            },
            {
              pinyin: "LuoBei",
              shortname: "萝北县"
            },
            {
              pinyin: "SuiBin",
              shortname: "绥滨县"
            }
          ],
          shortname: "鹤岗市"
        },
        {
          pinyin: "ShuangYaShan",
          children: [
            {
              pinyin: "ShuangYaShan",
              shortname: "市辖区"
            },
            {
              pinyin: "JianShan",
              shortname: "尖山区"
            },
            {
              pinyin: "LingDong",
              shortname: "岭东区"
            },
            {
              pinyin: "SiFangTai",
              shortname: "四方台区"
            },
            {
              pinyin: "BaoShan",
              shortname: "宝山区"
            },
            {
              pinyin: "JiXian",
              shortname: "集贤县"
            },
            {
              pinyin: "YouYi",
              shortname: "友谊县"
            },
            {
              pinyin: "BaoQing",
              shortname: "宝清县"
            },
            {
              pinyin: "RaoHe",
              shortname: "饶河县"
            }
          ],
          shortname: "双鸭山市"
        },
        {
          pinyin: "DaQing",
          children: [
            {
              pinyin: "DaQing",
              shortname: "市辖区"
            },
            {
              pinyin: "SaErTu",
              shortname: "萨尔图区"
            },
            {
              pinyin: "LongFeng",
              shortname: "龙凤区"
            },
            {
              pinyin: "RangHuLu",
              shortname: "让胡路区"
            },
            {
              pinyin: "HongGang",
              shortname: "红岗区"
            },
            {
              pinyin: "DaTong",
              shortname: "大同区"
            },
            {
              pinyin: "ZhaoZhou",
              shortname: "肇州县"
            },
            {
              pinyin: "ZhaoYuan",
              shortname: "肇源县"
            },
            {
              pinyin: "LinDian",
              shortname: "林甸县"
            },
            {
              pinyin: "DuErBoTe",
              shortname: "杜尔伯特蒙古族自治县"
            },
            {
              pinyin: "DaQingGao",
              shortname: "大庆高新技术产业开发区"
            }
          ],
          shortname: "大庆市"
        },
        {
          pinyin: "YiChun",
          children: [
            {
              pinyin: "YiChun",
              shortname: "市辖区"
            },
            {
              pinyin: "YiChun",
              shortname: "伊春区"
            },
            {
              pinyin: "NanCha",
              shortname: "南岔区"
            },
            {
              pinyin: "YouHao",
              shortname: "友好区"
            },
            {
              pinyin: "XiLin",
              shortname: "西林区"
            },
            {
              pinyin: "CuiLuan",
              shortname: "翠峦区"
            },
            {
              pinyin: "XinQing",
              shortname: "新青区"
            },
            {
              pinyin: "MeiXi",
              shortname: "美溪区"
            },
            {
              pinyin: "JinShanZhun",
              shortname: "金山屯区"
            },
            {
              pinyin: "WuYing",
              shortname: "五营区"
            },
            {
              pinyin: "WuMaHe",
              shortname: "乌马河区"
            },
            {
              pinyin: "TangWangHe",
              shortname: "汤旺河区"
            },
            {
              pinyin: "DaiLing",
              shortname: "带岭区"
            },
            {
              pinyin: "WuYiLing",
              shortname: "乌伊岭区"
            },
            {
              pinyin: "HongXing",
              shortname: "红星区"
            },
            {
              pinyin: "ShangGanLing",
              shortname: "上甘岭区"
            },
            {
              pinyin: "JiaYin",
              shortname: "嘉荫县"
            },
            {
              pinyin: "TieLi",
              shortname: "铁力市"
            }
          ],
          shortname: "伊春市"
        },
        {
          pinyin: "JiaMuSi",
          children: [
            {
              pinyin: "JiaMuSi",
              shortname: "市辖区"
            },
            {
              pinyin: "XiangYang",
              shortname: "向阳区"
            },
            {
              pinyin: "QianJin",
              shortname: "前进区"
            },
            {
              pinyin: "DongFeng",
              shortname: "东风区"
            },
            {
              pinyin: "JiaoQu",
              shortname: "郊区"
            },
            {
              pinyin: "HuaNan",
              shortname: "桦南县"
            },
            {
              pinyin: "HuaChuan",
              shortname: "桦川县"
            },
            {
              pinyin: "TangYuan",
              shortname: "汤原县"
            },
            {
              pinyin: "TongJiang",
              shortname: "同江市"
            },
            {
              pinyin: "FuJin",
              shortname: "富锦市"
            },
            {
              pinyin: "FuYuan",
              shortname: "抚远市"
            }
          ],
          shortname: "佳木斯市"
        },
        {
          pinyin: "QiTaiHe",
          children: [
            {
              pinyin: "QiTaiHe",
              shortname: "市辖区"
            },
            {
              pinyin: "XinXing",
              shortname: "新兴区"
            },
            {
              pinyin: "TaoShan",
              shortname: "桃山区"
            },
            {
              pinyin: "QieZiHe",
              shortname: "茄子河区"
            },
            {
              pinyin: "BoLi",
              shortname: "勃利县"
            }
          ],
          shortname: "七台河市"
        },
        {
          pinyin: "MuDanJiang",
          children: [
            {
              pinyin: "MuDanJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "DongAn",
              shortname: "东安区"
            },
            {
              pinyin: "YangMing",
              shortname: "阳明区"
            },
            {
              pinyin: "AiMin",
              shortname: "爱民区"
            },
            {
              pinyin: "XiAn",
              shortname: "西安区"
            },
            {
              pinyin: "LinKou",
              shortname: "林口县"
            },
            {
              pinyin: "MuDanJiangJingKaiQu",
              shortname: "牡丹江经济技术开发区"
            },
            {
              pinyin: "SuiFenHe",
              shortname: "绥芬河市"
            },
            {
              pinyin: "HaiLin",
              shortname: "海林市"
            },
            {
              pinyin: "NingAn",
              shortname: "宁安市"
            },
            {
              pinyin: "MuLeng",
              shortname: "穆棱市"
            },
            {
              pinyin: "DongNing",
              shortname: "东宁市"
            }
          ],
          shortname: "牡丹江市"
        },
        {
          pinyin: "HeiHe",
          children: [
            {
              pinyin: "HeiHe",
              shortname: "市辖区"
            },
            {
              pinyin: "AiHui",
              shortname: "爱辉区"
            },
            {
              pinyin: "NenJiang",
              shortname: "嫩江县"
            },
            {
              pinyin: "XunKe",
              shortname: "逊克县"
            },
            {
              pinyin: "SunWu",
              shortname: "孙吴县"
            },
            {
              pinyin: "BeiAn",
              shortname: "北安市"
            },
            {
              pinyin: "WuDaLianChi",
              shortname: "五大连池市"
            }
          ],
          shortname: "黑河市"
        },
        {
          pinyin: "SuiHua",
          children: [
            {
              pinyin: "SuiHua",
              shortname: "市辖区"
            },
            {
              pinyin: "BeiLin",
              shortname: "北林区"
            },
            {
              pinyin: "WangKui",
              shortname: "望奎县"
            },
            {
              pinyin: "LanXi",
              shortname: "兰西县"
            },
            {
              pinyin: "QingGang",
              shortname: "青冈县"
            },
            {
              pinyin: "QingAn",
              shortname: "庆安县"
            },
            {
              pinyin: "MingShui",
              shortname: "明水县"
            },
            {
              pinyin: "SuiLeng",
              shortname: "绥棱县"
            },
            {
              pinyin: "AnDa",
              shortname: "安达市"
            },
            {
              pinyin: "ZhaoDong",
              shortname: "肇东市"
            },
            {
              pinyin: "HaiLun",
              shortname: "海伦市"
            }
          ],
          shortname: "绥化市"
        },
        {
          pinyin: "DaXingAnLing",
          children: [
            {
              pinyin: "MoHe",
              shortname: "漠河市"
            },
            {
              pinyin: "HuMa",
              shortname: "呼玛县"
            },
            {
              pinyin: "TaHe",
              shortname: "塔河县"
            },
            {
              pinyin: "JiaGeDaQi",
              shortname: "加格达奇区"
            },
            {
              pinyin: "SongLing",
              shortname: "松岭区"
            },
            {
              pinyin: "XinLin",
              shortname: "新林区"
            },
            {
              pinyin: "HuZhong",
              shortname: "呼中区"
            }
          ],
          shortname: "大兴安岭地区"
        }
      ],
      shortname: "黑龙江省"
    },
    {
      pinyin: "ShangHai",
      children: [
        {
          pinyin: "ShangHai",
          children: [
            {
              pinyin: "HuangPu",
              shortname: "黄浦区"
            },
            {
              pinyin: "XuHui",
              shortname: "徐汇区"
            },
            {
              pinyin: "ChangNing",
              shortname: "长宁区"
            },
            {
              pinyin: "JingAn",
              shortname: "静安区"
            },
            {
              pinyin: "PuTuo",
              shortname: "普陀区"
            },
            {
              pinyin: "HongKou",
              shortname: "虹口区"
            },
            {
              pinyin: "YangPu",
              shortname: "杨浦区"
            },
            {
              pinyin: "MinXing",
              shortname: "闵行区"
            },
            {
              pinyin: "BaoShan",
              shortname: "宝山区"
            },
            {
              pinyin: "JiaDing",
              shortname: "嘉定区"
            },
            {
              pinyin: "PuDong",
              shortname: "浦东新区"
            },
            {
              pinyin: "JinShan",
              shortname: "金山区"
            },
            {
              pinyin: "SongJiang",
              shortname: "松江区"
            },
            {
              pinyin: "QingPu",
              shortname: "青浦区"
            },
            {
              pinyin: "FengXian",
              shortname: "奉贤区"
            },
            {
              pinyin: "ChongMing",
              shortname: "崇明区"
            }
          ],
          shortname: "直辖区"
        }
      ],
      shortname: "上海市"
    },
    {
      pinyin: "JiangSu",
      children: [
        {
          pinyin: "NanJing",
          children: [
            {
              pinyin: "NanJing",
              shortname: "市辖区"
            },
            {
              pinyin: "XuanWu",
              shortname: "玄武区"
            },
            {
              pinyin: "QinHuai",
              shortname: "秦淮区"
            },
            {
              pinyin: "JianYe",
              shortname: "建邺区"
            },
            {
              pinyin: "GuLou",
              shortname: "鼓楼区"
            },
            {
              pinyin: "PuKou",
              shortname: "浦口区"
            },
            {
              pinyin: "QiXia",
              shortname: "栖霞区"
            },
            {
              pinyin: "YuHuaTai",
              shortname: "雨花台区"
            },
            {
              pinyin: "JiangNing",
              shortname: "江宁区"
            },
            {
              pinyin: "LiuHe",
              shortname: "六合区"
            },
            {
              pinyin: "LiShui",
              shortname: "溧水区"
            },
            {
              pinyin: "GaoChun",
              shortname: "高淳区"
            }
          ],
          shortname: "南京市"
        },
        {
          pinyin: "WuXi",
          children: [
            {
              pinyin: "WuXi",
              shortname: "市辖区"
            },
            {
              pinyin: "XiShan",
              shortname: "锡山区"
            },
            {
              pinyin: "HuiShan",
              shortname: "惠山区"
            },
            {
              pinyin: "BinHu",
              shortname: "滨湖区"
            },
            {
              pinyin: "LiangXi",
              shortname: "梁溪区"
            },
            {
              pinyin: "XinWu",
              shortname: "新吴区"
            },
            {
              pinyin: "JiangYin",
              shortname: "江阴市"
            },
            {
              pinyin: "YiXing",
              shortname: "宜兴市"
            }
          ],
          shortname: "无锡市"
        },
        {
          pinyin: "XuZhou",
          children: [
            {
              pinyin: "XuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "GuLou",
              shortname: "鼓楼区"
            },
            {
              pinyin: "YunLong",
              shortname: "云龙区"
            },
            {
              pinyin: "JiaWang",
              shortname: "贾汪区"
            },
            {
              pinyin: "QuanShan",
              shortname: "泉山区"
            },
            {
              pinyin: "TongShan",
              shortname: "铜山区"
            },
            {
              pinyin: "FengXian",
              shortname: "丰县"
            },
            {
              pinyin: "PeiXian",
              shortname: "沛县"
            },
            {
              pinyin: "SuiNing",
              shortname: "睢宁县"
            },
            {
              pinyin: "XuZhouJingKaiQu",
              shortname: "徐州经济技术开发区"
            },
            {
              pinyin: "XinYi",
              shortname: "新沂市"
            },
            {
              pinyin: "PiZhou",
              shortname: "邳州市"
            }
          ],
          shortname: "徐州市"
        },
        {
          pinyin: "ChangZhou",
          children: [
            {
              pinyin: "ChangZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "TianNing",
              shortname: "天宁区"
            },
            {
              pinyin: "ZhongLou",
              shortname: "钟楼区"
            },
            {
              pinyin: "XinBei",
              shortname: "新北区"
            },
            {
              pinyin: "WuJin",
              shortname: "武进区"
            },
            {
              pinyin: "JinTan",
              shortname: "金坛区"
            },
            {
              pinyin: "LiYang",
              shortname: "溧阳市"
            }
          ],
          shortname: "常州市"
        },
        {
          pinyin: "SuZhou",
          children: [
            {
              pinyin: "SuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "HuQiu",
              shortname: "虎丘区"
            },
            {
              pinyin: "WuZhong",
              shortname: "吴中区"
            },
            {
              pinyin: "XiangCheng",
              shortname: "相城区"
            },
            {
              pinyin: "GuSu",
              shortname: "姑苏区"
            },
            {
              pinyin: "WuJiang",
              shortname: "吴江区"
            },
            {
              pinyin: "SuZhouGongYeYuan",
              shortname: "苏州工业园区"
            },
            {
              pinyin: "ChangShu",
              shortname: "常熟市"
            },
            {
              pinyin: "ZhangJiaGang",
              shortname: "张家港市"
            },
            {
              pinyin: "KunShan",
              shortname: "昆山市"
            },
            {
              pinyin: "TaiCang",
              shortname: "太仓市"
            }
          ],
          shortname: "苏州市"
        },
        {
          pinyin: "NanTong",
          children: [
            {
              pinyin: "NanTong",
              shortname: "市辖区"
            },
            {
              pinyin: "ChongChuan",
              shortname: "崇川区"
            },
            {
              pinyin: "GangZha",
              shortname: "港闸区"
            },
            {
              pinyin: "TongZhou",
              shortname: "通州区"
            },
            {
              pinyin: "RuDong",
              shortname: "如东县"
            },
            {
              pinyin: "NanTongJingKaiQu",
              shortname: "南通经济技术开发区"
            },
            {
              pinyin: "QiDong",
              shortname: "启东市"
            },
            {
              pinyin: "RuGao",
              shortname: "如皋市"
            },
            {
              pinyin: "HaiMen",
              shortname: "海门市"
            },
            {
              pinyin: "HaiAn",
              shortname: "海安市"
            }
          ],
          shortname: "南通市"
        },
        {
          pinyin: "LianYunGang",
          children: [
            {
              pinyin: "LianYunGang",
              shortname: "市辖区"
            },
            {
              pinyin: "LianYun",
              shortname: "连云区"
            },
            {
              pinyin: "HaiZhou",
              shortname: "海州区"
            },
            {
              pinyin: "GanYu",
              shortname: "赣榆区"
            },
            {
              pinyin: "DongHai",
              shortname: "东海县"
            },
            {
              pinyin: "GuanYun",
              shortname: "灌云县"
            },
            {
              pinyin: "GuanNan",
              shortname: "灌南县"
            },
            {
              pinyin: "LianYunGangJingKaiQu",
              shortname: "连云港经济技术开发区"
            },
            {
              pinyin: "LianYunGangGao",
              shortname: "连云港高新技术产业开发区"
            }
          ],
          shortname: "连云港市"
        },
        {
          pinyin: "HuaiAn",
          children: [
            {
              pinyin: "HuaiAn",
              shortname: "市辖区"
            },
            {
              pinyin: "HuaiAn",
              shortname: "淮安区"
            },
            {
              pinyin: "HuaiYin",
              shortname: "淮阴区"
            },
            {
              pinyin: "QingJiangPu",
              shortname: "清江浦区"
            },
            {
              pinyin: "HongZe",
              shortname: "洪泽区"
            },
            {
              pinyin: "LianShui",
              shortname: "涟水县"
            },
            {
              pinyin: "XuYi",
              shortname: "盱眙县"
            },
            {
              pinyin: "JinHu",
              shortname: "金湖县"
            },
            {
              pinyin: "HuaiAnJingKaiQu",
              shortname: "淮安经济技术开发区"
            }
          ],
          shortname: "淮安市"
        },
        {
          pinyin: "YanCheng",
          children: [
            {
              pinyin: "YanCheng",
              shortname: "市辖区"
            },
            {
              pinyin: "TingHu",
              shortname: "亭湖区"
            },
            {
              pinyin: "YanDu",
              shortname: "盐都区"
            },
            {
              pinyin: "DaFeng",
              shortname: "大丰区"
            },
            {
              pinyin: "XiangShui",
              shortname: "响水县"
            },
            {
              pinyin: "BinHai",
              shortname: "滨海县"
            },
            {
              pinyin: "FuNing",
              shortname: "阜宁县"
            },
            {
              pinyin: "SheYang",
              shortname: "射阳县"
            },
            {
              pinyin: "JianHu",
              shortname: "建湖县"
            },
            {
              pinyin: "YanChengJingKaiQu",
              shortname: "盐城经济技术开发区"
            },
            {
              pinyin: "DongTai",
              shortname: "东台市"
            }
          ],
          shortname: "盐城市"
        },
        {
          pinyin: "YangZhou",
          children: [
            {
              pinyin: "YangZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "GuangLing",
              shortname: "广陵区"
            },
            {
              pinyin: "HanJiang",
              shortname: "邗江区"
            },
            {
              pinyin: "JiangDu",
              shortname: "江都区"
            },
            {
              pinyin: "BaoYing",
              shortname: "宝应县"
            },
            {
              pinyin: "YangZhouJingKaiQu",
              shortname: "扬州经济技术开发区"
            },
            {
              pinyin: "YiZheng",
              shortname: "仪征市"
            },
            {
              pinyin: "GaoYou",
              shortname: "高邮市"
            }
          ],
          shortname: "扬州市"
        },
        {
          pinyin: "ZhenJiang",
          children: [
            {
              pinyin: "ZhenJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "JingKou",
              shortname: "京口区"
            },
            {
              pinyin: "RunZhou",
              shortname: "润州区"
            },
            {
              pinyin: "DanTu",
              shortname: "丹徒区"
            },
            {
              pinyin: "ZhenJiang",
              shortname: "镇江新区"
            },
            {
              pinyin: "DanYang",
              shortname: "丹阳市"
            },
            {
              pinyin: "YangZhong",
              shortname: "扬中市"
            },
            {
              pinyin: "JuRong",
              shortname: "句容市"
            }
          ],
          shortname: "镇江市"
        },
        {
          pinyin: "TaiZhou",
          children: [
            {
              pinyin: "TaiZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiLing",
              shortname: "海陵区"
            },
            {
              pinyin: "GaoGang",
              shortname: "高港区"
            },
            {
              pinyin: "JiangYan",
              shortname: "姜堰区"
            },
            {
              pinyin: "TaiZhouYiYaoGao",
              shortname: "泰州医药高新技术产业开发区"
            },
            {
              pinyin: "XingHua",
              shortname: "兴化市"
            },
            {
              pinyin: "JingJiang",
              shortname: "靖江市"
            },
            {
              pinyin: "TaiXing",
              shortname: "泰兴市"
            }
          ],
          shortname: "泰州市"
        },
        {
          pinyin: "SuQian",
          children: [
            {
              pinyin: "SuQian",
              shortname: "市辖区"
            },
            {
              pinyin: "SuCheng",
              shortname: "宿城区"
            },
            {
              pinyin: "SuYu",
              shortname: "宿豫区"
            },
            {
              pinyin: "ShuYang",
              shortname: "沭阳县"
            },
            {
              pinyin: "SiYang",
              shortname: "泗阳县"
            },
            {
              pinyin: "SiHong",
              shortname: "泗洪县"
            },
            {
              pinyin: "SuQianJingKaiQu",
              shortname: "宿迁经济技术开发区"
            }
          ],
          shortname: "宿迁市"
        }
      ],
      shortname: "江苏省"
    },
    {
      pinyin: "ZheJiang",
      children: [
        {
          pinyin: "HangZhou",
          children: [
            {
              pinyin: "HangZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ShangCheng",
              shortname: "上城区"
            },
            {
              pinyin: "XiaCheng",
              shortname: "下城区"
            },
            {
              pinyin: "JiangGan",
              shortname: "江干区"
            },
            {
              pinyin: "GongShu",
              shortname: "拱墅区"
            },
            {
              pinyin: "XiHu",
              shortname: "西湖区"
            },
            {
              pinyin: "BinJiang",
              shortname: "滨江区"
            },
            {
              pinyin: "XiaoShan",
              shortname: "萧山区"
            },
            {
              pinyin: "YuHang",
              shortname: "余杭区"
            },
            {
              pinyin: "FuYang",
              shortname: "富阳区"
            },
            {
              pinyin: "LinAn",
              shortname: "临安区"
            },
            {
              pinyin: "TongLu",
              shortname: "桐庐县"
            },
            {
              pinyin: "ChunAn",
              shortname: "淳安县"
            },
            {
              pinyin: "JianDe",
              shortname: "建德市"
            }
          ],
          shortname: "杭州市"
        },
        {
          pinyin: "NingBo",
          children: [
            {
              pinyin: "NingBo",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiShu",
              shortname: "海曙区"
            },
            {
              pinyin: "JiangBei",
              shortname: "江北区"
            },
            {
              pinyin: "BeiLun",
              shortname: "北仑区"
            },
            {
              pinyin: "ZhenHai",
              shortname: "镇海区"
            },
            {
              pinyin: "YinZhou",
              shortname: "鄞州区"
            },
            {
              pinyin: "FengHua",
              shortname: "奉化区"
            },
            {
              pinyin: "XiangShan",
              shortname: "象山县"
            },
            {
              pinyin: "NingHai",
              shortname: "宁海县"
            },
            {
              pinyin: "YuYao",
              shortname: "余姚市"
            },
            {
              pinyin: "CiXi",
              shortname: "慈溪市"
            }
          ],
          shortname: "宁波市"
        },
        {
          pinyin: "WenZhou",
          children: [
            {
              pinyin: "WenZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LuCheng",
              shortname: "鹿城区"
            },
            {
              pinyin: "LongWan",
              shortname: "龙湾区"
            },
            {
              pinyin: "OuHai",
              shortname: "瓯海区"
            },
            {
              pinyin: "DongTou",
              shortname: "洞头区"
            },
            {
              pinyin: "YongJia",
              shortname: "永嘉县"
            },
            {
              pinyin: "PingYang",
              shortname: "平阳县"
            },
            {
              pinyin: "CangNan",
              shortname: "苍南县"
            },
            {
              pinyin: "WenCheng",
              shortname: "文成县"
            },
            {
              pinyin: "TaiShun",
              shortname: "泰顺县"
            },
            {
              pinyin: "WenZhouJingKaiQu",
              shortname: "温州经济技术开发区"
            },
            {
              pinyin: "RuiAn",
              shortname: "瑞安市"
            },
            {
              pinyin: "YueQing",
              shortname: "乐清市"
            }
          ],
          shortname: "温州市"
        },
        {
          pinyin: "JiaXing",
          children: [
            {
              pinyin: "JiaXing",
              shortname: "市辖区"
            },
            {
              pinyin: "NanHu",
              shortname: "南湖区"
            },
            {
              pinyin: "XiuZhou",
              shortname: "秀洲区"
            },
            {
              pinyin: "JiaShan",
              shortname: "嘉善县"
            },
            {
              pinyin: "HaiYan",
              shortname: "海盐县"
            },
            {
              pinyin: "HaiNing",
              shortname: "海宁市"
            },
            {
              pinyin: "PingHu",
              shortname: "平湖市"
            },
            {
              pinyin: "TongXiang",
              shortname: "桐乡市"
            }
          ],
          shortname: "嘉兴市"
        },
        {
          pinyin: "HuZhou",
          children: [
            {
              pinyin: "HuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "WuXing",
              shortname: "吴兴区"
            },
            {
              pinyin: "NanXun",
              shortname: "南浔区"
            },
            {
              pinyin: "DeQing",
              shortname: "德清县"
            },
            {
              pinyin: "ChangXing",
              shortname: "长兴县"
            },
            {
              pinyin: "AnJi",
              shortname: "安吉县"
            }
          ],
          shortname: "湖州市"
        },
        {
          pinyin: "ShaoXing",
          children: [
            {
              pinyin: "ShaoXing",
              shortname: "市辖区"
            },
            {
              pinyin: "YueCheng",
              shortname: "越城区"
            },
            {
              pinyin: "KeQiao",
              shortname: "柯桥区"
            },
            {
              pinyin: "ShangYu",
              shortname: "上虞区"
            },
            {
              pinyin: "XinChang",
              shortname: "新昌县"
            },
            {
              pinyin: "ZhuJi",
              shortname: "诸暨市"
            },
            {
              pinyin: "ShengZhou",
              shortname: "嵊州市"
            }
          ],
          shortname: "绍兴市"
        },
        {
          pinyin: "JinHua",
          children: [
            {
              pinyin: "JinHua",
              shortname: "市辖区"
            },
            {
              pinyin: "WuCheng",
              shortname: "婺城区"
            },
            {
              pinyin: "JinDong",
              shortname: "金东区"
            },
            {
              pinyin: "WuYi",
              shortname: "武义县"
            },
            {
              pinyin: "PuJiang",
              shortname: "浦江县"
            },
            {
              pinyin: "PanAn",
              shortname: "磐安县"
            },
            {
              pinyin: "LanXi",
              shortname: "兰溪市"
            },
            {
              pinyin: "YiWu",
              shortname: "义乌市"
            },
            {
              pinyin: "DongYang",
              shortname: "东阳市"
            },
            {
              pinyin: "YongKang",
              shortname: "永康市"
            }
          ],
          shortname: "金华市"
        },
        {
          pinyin: "QuZhou",
          children: [
            {
              pinyin: "QuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "KeCheng",
              shortname: "柯城区"
            },
            {
              pinyin: "QuJiang",
              shortname: "衢江区"
            },
            {
              pinyin: "ChangShan",
              shortname: "常山县"
            },
            {
              pinyin: "KaiHua",
              shortname: "开化县"
            },
            {
              pinyin: "LongYou",
              shortname: "龙游县"
            },
            {
              pinyin: "JiangShan",
              shortname: "江山市"
            }
          ],
          shortname: "衢州市"
        },
        {
          pinyin: "ZhouShan",
          children: [
            {
              pinyin: "ZhouShan",
              shortname: "市辖区"
            },
            {
              pinyin: "DingHai",
              shortname: "定海区"
            },
            {
              pinyin: "PuTuo",
              shortname: "普陀区"
            },
            {
              pinyin: "DaiShan",
              shortname: "岱山县"
            },
            {
              pinyin: "ShengSi",
              shortname: "嵊泗县"
            }
          ],
          shortname: "舟山市"
        },
        {
          pinyin: "TaiZhou",
          children: [
            {
              pinyin: "TaiZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "JiaoJiang",
              shortname: "椒江区"
            },
            {
              pinyin: "HuangYan",
              shortname: "黄岩区"
            },
            {
              pinyin: "LuQiao",
              shortname: "路桥区"
            },
            {
              pinyin: "SanMen",
              shortname: "三门县"
            },
            {
              pinyin: "TianTai",
              shortname: "天台县"
            },
            {
              pinyin: "XianJu",
              shortname: "仙居县"
            },
            {
              pinyin: "WenLing",
              shortname: "温岭市"
            },
            {
              pinyin: "LinHai",
              shortname: "临海市"
            },
            {
              pinyin: "YuHuan",
              shortname: "玉环市"
            }
          ],
          shortname: "台州市"
        },
        {
          pinyin: "LiShui",
          children: [
            {
              pinyin: "LiShui",
              shortname: "市辖区"
            },
            {
              pinyin: "LianDu",
              shortname: "莲都区"
            },
            {
              pinyin: "QingTian",
              shortname: "青田县"
            },
            {
              pinyin: "JinYun",
              shortname: "缙云县"
            },
            {
              pinyin: "SuiChang",
              shortname: "遂昌县"
            },
            {
              pinyin: "SongYang",
              shortname: "松阳县"
            },
            {
              pinyin: "YunHe",
              shortname: "云和县"
            },
            {
              pinyin: "QingYuan",
              shortname: "庆元县"
            },
            {
              pinyin: "JingNing",
              shortname: "景宁畲族自治县"
            },
            {
              pinyin: "LongQuan",
              shortname: "龙泉市"
            }
          ],
          shortname: "丽水市"
        }
      ],
      shortname: "浙江省"
    },
    {
      pinyin: "AnHui",
      children: [
        {
          pinyin: "HeFei",
          children: [
            {
              pinyin: "HeFei",
              shortname: "市辖区"
            },
            {
              pinyin: "YaoHai",
              shortname: "瑶海区"
            },
            {
              pinyin: "LuYang",
              shortname: "庐阳区"
            },
            {
              pinyin: "ShuShan",
              shortname: "蜀山区"
            },
            {
              pinyin: "BaoHe",
              shortname: "包河区"
            },
            {
              pinyin: "ChangFeng",
              shortname: "长丰县"
            },
            {
              pinyin: "FeiDong",
              shortname: "肥东县"
            },
            {
              pinyin: "FeiXi",
              shortname: "肥西县"
            },
            {
              pinyin: "LuJiang",
              shortname: "庐江县"
            },
            {
              pinyin: "HeFeiGao",
              shortname: "合肥高新技术产业开发区"
            },
            {
              pinyin: "HeFeiJingKaiQu",
              shortname: "合肥经济技术开发区"
            },
            {
              pinyin: "HeFeiXinZhanGao",
              shortname: "合肥新站高新技术产业开发区"
            },
            {
              pinyin: "ChaoHu",
              shortname: "巢湖市"
            }
          ],
          shortname: "合肥市"
        },
        {
          pinyin: "WuHu",
          children: [
            {
              pinyin: "WuHu",
              shortname: "市辖区"
            },
            {
              pinyin: "JingHu",
              shortname: "镜湖区"
            },
            {
              pinyin: "YiJiang",
              shortname: "弋江区"
            },
            {
              pinyin: "JiuJiang",
              shortname: "鸠江区"
            },
            {
              pinyin: "SanShan",
              shortname: "三山区"
            },
            {
              pinyin: "WuHu",
              shortname: "芜湖县"
            },
            {
              pinyin: "FanChang",
              shortname: "繁昌县"
            },
            {
              pinyin: "NanLing",
              shortname: "南陵县"
            },
            {
              pinyin: "WuWei",
              shortname: "无为县"
            },
            {
              pinyin: "WuHuJingKaiQu",
              shortname: "芜湖经济技术开发区"
            },
            {
              pinyin: "AnHuiWuHuChangJiangDaQiaoJingK",
              shortname: "安徽芜湖长江大桥经济开发区"
            }
          ],
          shortname: "芜湖市"
        },
        {
          pinyin: "BengBu",
          children: [
            {
              pinyin: "BengBu",
              shortname: "市辖区"
            },
            {
              pinyin: "LongZiHu",
              shortname: "龙子湖区"
            },
            {
              pinyin: "BengShan",
              shortname: "蚌山区"
            },
            {
              pinyin: "YuHui",
              shortname: "禹会区"
            },
            {
              pinyin: "HuaiShang",
              shortname: "淮上区"
            },
            {
              pinyin: "HuaiYuan",
              shortname: "怀远县"
            },
            {
              pinyin: "WuHe",
              shortname: "五河县"
            },
            {
              pinyin: "GuZhen",
              shortname: "固镇县"
            },
            {
              pinyin: "BengBuGao",
              shortname: "蚌埠市高新技术开发区"
            },
            {
              pinyin: "BengBuJingKaiQu",
              shortname: "蚌埠市经济开发区"
            }
          ],
          shortname: "蚌埠市"
        },
        {
          pinyin: "HuaiNan",
          children: [
            {
              pinyin: "HuaiNan",
              shortname: "市辖区"
            },
            {
              pinyin: "DaTong",
              shortname: "大通区"
            },
            {
              pinyin: "TianJiaAn",
              shortname: "田家庵区"
            },
            {
              pinyin: "XieJiaJi",
              shortname: "谢家集区"
            },
            {
              pinyin: "BaGongShan",
              shortname: "八公山区"
            },
            {
              pinyin: "PanJi",
              shortname: "潘集区"
            },
            {
              pinyin: "FengTai",
              shortname: "凤台县"
            },
            {
              pinyin: "ShouXian",
              shortname: "寿县"
            }
          ],
          shortname: "淮南市"
        },
        {
          pinyin: "MaAnShan",
          children: [
            {
              pinyin: "MaAnShan",
              shortname: "市辖区"
            },
            {
              pinyin: "HuaShan",
              shortname: "花山区"
            },
            {
              pinyin: "YuShan",
              shortname: "雨山区"
            },
            {
              pinyin: "BoWang",
              shortname: "博望区"
            },
            {
              pinyin: "DangTu",
              shortname: "当涂县"
            },
            {
              pinyin: "HanShan",
              shortname: "含山县"
            },
            {
              pinyin: "HeXian",
              shortname: "和县"
            }
          ],
          shortname: "马鞍山市"
        },
        {
          pinyin: "HuaiBei",
          children: [
            {
              pinyin: "HuaiBei",
              shortname: "市辖区"
            },
            {
              pinyin: "DuJi",
              shortname: "杜集区"
            },
            {
              pinyin: "XiangShan",
              shortname: "相山区"
            },
            {
              pinyin: "LieShan",
              shortname: "烈山区"
            },
            {
              pinyin: "SuiXi",
              shortname: "濉溪县"
            }
          ],
          shortname: "淮北市"
        },
        {
          pinyin: "TongLing",
          children: [
            {
              pinyin: "TongLing",
              shortname: "市辖区"
            },
            {
              pinyin: "TongGuan",
              shortname: "铜官区"
            },
            {
              pinyin: "YiAn",
              shortname: "义安区"
            },
            {
              pinyin: "JiaoQu",
              shortname: "郊区"
            },
            {
              pinyin: "ZongYang",
              shortname: "枞阳县"
            }
          ],
          shortname: "铜陵市"
        },
        {
          pinyin: "AnQing",
          children: [
            {
              pinyin: "AnQing",
              shortname: "市辖区"
            },
            {
              pinyin: "YingJiang",
              shortname: "迎江区"
            },
            {
              pinyin: "DaGuan",
              shortname: "大观区"
            },
            {
              pinyin: "YiXiu",
              shortname: "宜秀区"
            },
            {
              pinyin: "HuaiNing",
              shortname: "怀宁县"
            },
            {
              pinyin: "TaiHu",
              shortname: "太湖县"
            },
            {
              pinyin: "SuSong",
              shortname: "宿松县"
            },
            {
              pinyin: "WangJiang",
              shortname: "望江县"
            },
            {
              pinyin: "YueXi",
              shortname: "岳西县"
            },
            {
              pinyin: "AnHuiAnQingJingKaiQu",
              shortname: "安徽安庆经济开发区"
            },
            {
              pinyin: "TongCheng",
              shortname: "桐城市"
            },
            {
              pinyin: "QianShan",
              shortname: "潜山市"
            }
          ],
          shortname: "安庆市"
        },
        {
          pinyin: "HuangShan",
          children: [
            {
              pinyin: "HuangShan",
              shortname: "市辖区"
            },
            {
              pinyin: "TunXi",
              shortname: "屯溪区"
            },
            {
              pinyin: "HuangShan",
              shortname: "黄山区"
            },
            {
              pinyin: "HuiZhou",
              shortname: "徽州区"
            },
            {
              pinyin: "SheXian",
              shortname: "歙县"
            },
            {
              pinyin: "XiuNing",
              shortname: "休宁县"
            },
            {
              pinyin: "YiXian",
              shortname: "黟县"
            },
            {
              pinyin: "QiMen",
              shortname: "祁门县"
            }
          ],
          shortname: "黄山市"
        },
        {
          pinyin: "ChuZhou",
          children: [
            {
              pinyin: "ChuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LangYa",
              shortname: "琅琊区"
            },
            {
              pinyin: "NanQiao",
              shortname: "南谯区"
            },
            {
              pinyin: "LaiAn",
              shortname: "来安县"
            },
            {
              pinyin: "QuanJiao",
              shortname: "全椒县"
            },
            {
              pinyin: "DingYuan",
              shortname: "定远县"
            },
            {
              pinyin: "FengYang",
              shortname: "凤阳县"
            },
            {
              pinyin: "SuChuXianDaiChanYeYuan",
              shortname: "苏滁现代产业园"
            },
            {
              pinyin: "ChuZhouJingKaiQu",
              shortname: "滁州经济技术开发区"
            },
            {
              pinyin: "TianChang",
              shortname: "天长市"
            },
            {
              pinyin: "MingGuang",
              shortname: "明光市"
            }
          ],
          shortname: "滁州市"
        },
        {
          pinyin: "FuYang",
          children: [
            {
              pinyin: "FuYang",
              shortname: "市辖区"
            },
            {
              pinyin: "YingZhou",
              shortname: "颍州区"
            },
            {
              pinyin: "YingDong",
              shortname: "颍东区"
            },
            {
              pinyin: "YingQuan",
              shortname: "颍泉区"
            },
            {
              pinyin: "LinQuan",
              shortname: "临泉县"
            },
            {
              pinyin: "TaiHe",
              shortname: "太和县"
            },
            {
              pinyin: "FuNan",
              shortname: "阜南县"
            },
            {
              pinyin: "YingShang",
              shortname: "颍上县"
            },
            {
              pinyin: "FuYangHeFeiXianDaiChanYeYuan",
              shortname: "阜阳合肥现代产业园区"
            },
            {
              pinyin: "FuYangJingKaiQu",
              shortname: "阜阳经济技术开发区"
            },
            {
              pinyin: "JieShou",
              shortname: "界首市"
            }
          ],
          shortname: "阜阳市"
        },
        {
          pinyin: "SuZhou",
          children: [
            {
              pinyin: "SuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "YongQiao",
              shortname: "埇桥区"
            },
            {
              pinyin: "DangShan",
              shortname: "砀山县"
            },
            {
              pinyin: "XiaoXian",
              shortname: "萧县"
            },
            {
              pinyin: "LingBi",
              shortname: "灵璧县"
            },
            {
              pinyin: "SiXian",
              shortname: "泗县"
            },
            {
              pinyin: "SuZhouMaAnShanXianDaiChanYeYua",
              shortname: "宿州马鞍山现代产业园区"
            },
            {
              pinyin: "SuZhouJingKaiQu",
              shortname: "宿州经济技术开发区"
            }
          ],
          shortname: "宿州市"
        },
        {
          pinyin: "LuAn",
          children: [
            {
              pinyin: "LuAn",
              shortname: "市辖区"
            },
            {
              pinyin: "JinAn",
              shortname: "金安区"
            },
            {
              pinyin: "YuAn",
              shortname: "裕安区"
            },
            {
              pinyin: "YeJi",
              shortname: "叶集区"
            },
            {
              pinyin: "HuoQiu",
              shortname: "霍邱县"
            },
            {
              pinyin: "ShuCheng",
              shortname: "舒城县"
            },
            {
              pinyin: "JinZhai",
              shortname: "金寨县"
            },
            {
              pinyin: "HuoShan",
              shortname: "霍山县"
            }
          ],
          shortname: "六安市"
        },
        {
          pinyin: "BoZhou",
          children: [
            {
              pinyin: "BoZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "QiaoCheng",
              shortname: "谯城区"
            },
            {
              pinyin: "GuoYang",
              shortname: "涡阳县"
            },
            {
              pinyin: "MengCheng",
              shortname: "蒙城县"
            },
            {
              pinyin: "LiXin",
              shortname: "利辛县"
            }
          ],
          shortname: "亳州市"
        },
        {
          pinyin: "ChiZhou",
          children: [
            {
              pinyin: "ChiZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "GuiChi",
              shortname: "贵池区"
            },
            {
              pinyin: "DongZhi",
              shortname: "东至县"
            },
            {
              pinyin: "ShiTai",
              shortname: "石台县"
            },
            {
              pinyin: "QingYang",
              shortname: "青阳县"
            }
          ],
          shortname: "池州市"
        },
        {
          pinyin: "XuanCheng",
          children: [
            {
              pinyin: "XuanCheng",
              shortname: "市辖区"
            },
            {
              pinyin: "XuanZhou",
              shortname: "宣州区"
            },
            {
              pinyin: "LangXi",
              shortname: "郎溪县"
            },
            {
              pinyin: "GuangDe",
              shortname: "广德县"
            },
            {
              pinyin: "JingXian",
              shortname: "泾县"
            },
            {
              pinyin: "JiXi",
              shortname: "绩溪县"
            },
            {
              pinyin: "JingDe",
              shortname: "旌德县"
            },
            {
              pinyin: "XuanChengJingKaiQu",
              shortname: "宣城市经济开发区"
            },
            {
              pinyin: "NingGuo",
              shortname: "宁国市"
            }
          ],
          shortname: "宣城市"
        }
      ],
      shortname: "安徽省"
    },
    {
      pinyin: "FuJian",
      children: [
        {
          pinyin: "FuZhou",
          children: [
            {
              pinyin: "FuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "GuLou",
              shortname: "鼓楼区"
            },
            {
              pinyin: "TaiJiang",
              shortname: "台江区"
            },
            {
              pinyin: "CangShan",
              shortname: "仓山区"
            },
            {
              pinyin: "MaYi",
              shortname: "马尾区"
            },
            {
              pinyin: "JinAn",
              shortname: "晋安区"
            },
            {
              pinyin: "ChangLe",
              shortname: "长乐区"
            },
            {
              pinyin: "MinHou",
              shortname: "闽侯县"
            },
            {
              pinyin: "LianJiang",
              shortname: "连江县"
            },
            {
              pinyin: "LuoYuan",
              shortname: "罗源县"
            },
            {
              pinyin: "MinQing",
              shortname: "闽清县"
            },
            {
              pinyin: "YongTai",
              shortname: "永泰县"
            },
            {
              pinyin: "PingTan",
              shortname: "平潭县"
            },
            {
              pinyin: "FuQing",
              shortname: "福清市"
            }
          ],
          shortname: "福州市"
        },
        {
          pinyin: "XiaMen",
          children: [
            {
              pinyin: "XiaMen",
              shortname: "市辖区"
            },
            {
              pinyin: "SiMing",
              shortname: "思明区"
            },
            {
              pinyin: "HaiCang",
              shortname: "海沧区"
            },
            {
              pinyin: "HuLi",
              shortname: "湖里区"
            },
            {
              pinyin: "JiMei",
              shortname: "集美区"
            },
            {
              pinyin: "TongAn",
              shortname: "同安区"
            },
            {
              pinyin: "XiangAn",
              shortname: "翔安区"
            }
          ],
          shortname: "厦门市"
        },
        {
          pinyin: "PuTian",
          children: [
            {
              pinyin: "PuTian",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengXiang",
              shortname: "城厢区"
            },
            {
              pinyin: "HanJiang",
              shortname: "涵江区"
            },
            {
              pinyin: "LiCheng",
              shortname: "荔城区"
            },
            {
              pinyin: "XiuYu",
              shortname: "秀屿区"
            },
            {
              pinyin: "XianYou",
              shortname: "仙游县"
            }
          ],
          shortname: "莆田市"
        },
        {
          pinyin: "SanMing",
          children: [
            {
              pinyin: "SanMing",
              shortname: "市辖区"
            },
            {
              pinyin: "MeiLie",
              shortname: "梅列区"
            },
            {
              pinyin: "SanYuan",
              shortname: "三元区"
            },
            {
              pinyin: "MingXi",
              shortname: "明溪县"
            },
            {
              pinyin: "QingLiu",
              shortname: "清流县"
            },
            {
              pinyin: "NingHua",
              shortname: "宁化县"
            },
            {
              pinyin: "DaTian",
              shortname: "大田县"
            },
            {
              pinyin: "YouXi",
              shortname: "尤溪县"
            },
            {
              pinyin: "ShaXian",
              shortname: "沙县"
            },
            {
              pinyin: "JiangLe",
              shortname: "将乐县"
            },
            {
              pinyin: "TaiNing",
              shortname: "泰宁县"
            },
            {
              pinyin: "JianNing",
              shortname: "建宁县"
            },
            {
              pinyin: "YongAn",
              shortname: "永安市"
            }
          ],
          shortname: "三明市"
        },
        {
          pinyin: "QuanZhou",
          children: [
            {
              pinyin: "QuanZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LiCheng",
              shortname: "鲤城区"
            },
            {
              pinyin: "FengZe",
              shortname: "丰泽区"
            },
            {
              pinyin: "LuoJiang",
              shortname: "洛江区"
            },
            {
              pinyin: "QuanGang",
              shortname: "泉港区"
            },
            {
              pinyin: "HuiAn",
              shortname: "惠安县"
            },
            {
              pinyin: "AnXi",
              shortname: "安溪县"
            },
            {
              pinyin: "YongChun",
              shortname: "永春县"
            },
            {
              pinyin: "DeHua",
              shortname: "德化县"
            },
            {
              pinyin: "JinMen",
              shortname: "金门县"
            },
            {
              pinyin: "ShiShi",
              shortname: "石狮市"
            },
            {
              pinyin: "JinJiang",
              shortname: "晋江市"
            },
            {
              pinyin: "NanAn",
              shortname: "南安市"
            }
          ],
          shortname: "泉州市"
        },
        {
          pinyin: "ZhangZhou",
          children: [
            {
              pinyin: "ZhangZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "XiangCheng",
              shortname: "芗城区"
            },
            {
              pinyin: "LongWen",
              shortname: "龙文区"
            },
            {
              pinyin: "YunXiao",
              shortname: "云霄县"
            },
            {
              pinyin: "ZhangPu",
              shortname: "漳浦县"
            },
            {
              pinyin: "ZhaoAn",
              shortname: "诏安县"
            },
            {
              pinyin: "ChangTai",
              shortname: "长泰县"
            },
            {
              pinyin: "DongShan",
              shortname: "东山县"
            },
            {
              pinyin: "NanJing",
              shortname: "南靖县"
            },
            {
              pinyin: "PingHe",
              shortname: "平和县"
            },
            {
              pinyin: "HuaAn",
              shortname: "华安县"
            },
            {
              pinyin: "LongHai",
              shortname: "龙海市"
            }
          ],
          shortname: "漳州市"
        },
        {
          pinyin: "NanPing",
          children: [
            {
              pinyin: "NanPing",
              shortname: "市辖区"
            },
            {
              pinyin: "YanPing",
              shortname: "延平区"
            },
            {
              pinyin: "JianYang",
              shortname: "建阳区"
            },
            {
              pinyin: "ShunChang",
              shortname: "顺昌县"
            },
            {
              pinyin: "PuCheng",
              shortname: "浦城县"
            },
            {
              pinyin: "GuangZe",
              shortname: "光泽县"
            },
            {
              pinyin: "SongXi",
              shortname: "松溪县"
            },
            {
              pinyin: "ZhengHe",
              shortname: "政和县"
            },
            {
              pinyin: "ShaoWu",
              shortname: "邵武市"
            },
            {
              pinyin: "WuYiShan",
              shortname: "武夷山市"
            },
            {
              pinyin: "JianOu",
              shortname: "建瓯市"
            }
          ],
          shortname: "南平市"
        },
        {
          pinyin: "LongYan",
          children: [
            {
              pinyin: "LongYan",
              shortname: "市辖区"
            },
            {
              pinyin: "XinLuo",
              shortname: "新罗区"
            },
            {
              pinyin: "YongDing",
              shortname: "永定区"
            },
            {
              pinyin: "ChangTing",
              shortname: "长汀县"
            },
            {
              pinyin: "ShangHang",
              shortname: "上杭县"
            },
            {
              pinyin: "WuPing",
              shortname: "武平县"
            },
            {
              pinyin: "LianCheng",
              shortname: "连城县"
            },
            {
              pinyin: "ZhangPing",
              shortname: "漳平市"
            }
          ],
          shortname: "龙岩市"
        },
        {
          pinyin: "NingDe",
          children: [
            {
              pinyin: "NingDe",
              shortname: "市辖区"
            },
            {
              pinyin: "JiaoCheng",
              shortname: "蕉城区"
            },
            {
              pinyin: "XiaPu",
              shortname: "霞浦县"
            },
            {
              pinyin: "GuTian",
              shortname: "古田县"
            },
            {
              pinyin: "PingNan",
              shortname: "屏南县"
            },
            {
              pinyin: "ShouNing",
              shortname: "寿宁县"
            },
            {
              pinyin: "ZhouNing",
              shortname: "周宁县"
            },
            {
              pinyin: "ZheRong",
              shortname: "柘荣县"
            },
            {
              pinyin: "FuAn",
              shortname: "福安市"
            },
            {
              pinyin: "FuDing",
              shortname: "福鼎市"
            }
          ],
          shortname: "宁德市"
        }
      ],
      shortname: "福建省"
    },
    {
      pinyin: "JiangXi",
      children: [
        {
          pinyin: "NanChang",
          children: [
            {
              pinyin: "NanChang",
              shortname: "市辖区"
            },
            {
              pinyin: "DongHu",
              shortname: "东湖区"
            },
            {
              pinyin: "XiHu",
              shortname: "西湖区"
            },
            {
              pinyin: "QingYunPu",
              shortname: "青云谱区"
            },
            {
              pinyin: "WanLi",
              shortname: "湾里区"
            },
            {
              pinyin: "QingShanHu",
              shortname: "青山湖区"
            },
            {
              pinyin: "XinJian",
              shortname: "新建区"
            },
            {
              pinyin: "NanChang",
              shortname: "南昌县"
            },
            {
              pinyin: "AnYi",
              shortname: "安义县"
            },
            {
              pinyin: "JinXian",
              shortname: "进贤县"
            }
          ],
          shortname: "南昌市"
        },
        {
          pinyin: "JingDeZhen",
          children: [
            {
              pinyin: "JingDeZhen",
              shortname: "市辖区"
            },
            {
              pinyin: "ChangJiang",
              shortname: "昌江区"
            },
            {
              pinyin: "ZhuShan",
              shortname: "珠山区"
            },
            {
              pinyin: "FuLiang",
              shortname: "浮梁县"
            },
            {
              pinyin: "LePing",
              shortname: "乐平市"
            }
          ],
          shortname: "景德镇市"
        },
        {
          pinyin: "PingXiang",
          children: [
            {
              pinyin: "PingXiang",
              shortname: "市辖区"
            },
            {
              pinyin: "AnYuan",
              shortname: "安源区"
            },
            {
              pinyin: "XiangDong",
              shortname: "湘东区"
            },
            {
              pinyin: "LianHua",
              shortname: "莲花县"
            },
            {
              pinyin: "ShangLi",
              shortname: "上栗县"
            },
            {
              pinyin: "LuXi",
              shortname: "芦溪县"
            }
          ],
          shortname: "萍乡市"
        },
        {
          pinyin: "JiuJiang",
          children: [
            {
              pinyin: "JiuJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "LianXi",
              shortname: "濂溪区"
            },
            {
              pinyin: "XunYang",
              shortname: "浔阳区"
            },
            {
              pinyin: "ChaiSang",
              shortname: "柴桑区"
            },
            {
              pinyin: "WuNing",
              shortname: "武宁县"
            },
            {
              pinyin: "XiuShui",
              shortname: "修水县"
            },
            {
              pinyin: "YongXiu",
              shortname: "永修县"
            },
            {
              pinyin: "DeAn",
              shortname: "德安县"
            },
            {
              pinyin: "DuChang",
              shortname: "都昌县"
            },
            {
              pinyin: "HuKou",
              shortname: "湖口县"
            },
            {
              pinyin: "PengZe",
              shortname: "彭泽县"
            },
            {
              pinyin: "RuiChang",
              shortname: "瑞昌市"
            },
            {
              pinyin: "GongQingCheng",
              shortname: "共青城市"
            },
            {
              pinyin: "LuShan",
              shortname: "庐山市"
            }
          ],
          shortname: "九江市"
        },
        {
          pinyin: "XinYu",
          children: [
            {
              pinyin: "XinYu",
              shortname: "市辖区"
            },
            {
              pinyin: "YuShui",
              shortname: "渝水区"
            },
            {
              pinyin: "FenYi",
              shortname: "分宜县"
            }
          ],
          shortname: "新余市"
        },
        {
          pinyin: "YingTan",
          children: [
            {
              pinyin: "YingTan",
              shortname: "市辖区"
            },
            {
              pinyin: "YueHu",
              shortname: "月湖区"
            },
            {
              pinyin: "YuJiang",
              shortname: "余江区"
            },
            {
              pinyin: "GuiXi",
              shortname: "贵溪市"
            }
          ],
          shortname: "鹰潭市"
        },
        {
          pinyin: "GanZhou",
          children: [
            {
              pinyin: "GanZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhangGong",
              shortname: "章贡区"
            },
            {
              pinyin: "NanKang",
              shortname: "南康区"
            },
            {
              pinyin: "GanXian",
              shortname: "赣县区"
            },
            {
              pinyin: "XinFeng",
              shortname: "信丰县"
            },
            {
              pinyin: "DaYu",
              shortname: "大余县"
            },
            {
              pinyin: "ShangYou",
              shortname: "上犹县"
            },
            {
              pinyin: "ChongYi",
              shortname: "崇义县"
            },
            {
              pinyin: "AnYuan",
              shortname: "安远县"
            },
            {
              pinyin: "LongNan",
              shortname: "龙南县"
            },
            {
              pinyin: "DingNan",
              shortname: "定南县"
            },
            {
              pinyin: "QuanNan",
              shortname: "全南县"
            },
            {
              pinyin: "NingDu",
              shortname: "宁都县"
            },
            {
              pinyin: "YuDu",
              shortname: "于都县"
            },
            {
              pinyin: "XingGuo",
              shortname: "兴国县"
            },
            {
              pinyin: "HuiChang",
              shortname: "会昌县"
            },
            {
              pinyin: "XunWu",
              shortname: "寻乌县"
            },
            {
              pinyin: "ShiCheng",
              shortname: "石城县"
            },
            {
              pinyin: "RuiJin",
              shortname: "瑞金市"
            }
          ],
          shortname: "赣州市"
        },
        {
          pinyin: "JiAn",
          children: [
            {
              pinyin: "JiAn",
              shortname: "市辖区"
            },
            {
              pinyin: "JiZhou",
              shortname: "吉州区"
            },
            {
              pinyin: "QingYuan",
              shortname: "青原区"
            },
            {
              pinyin: "JiAn",
              shortname: "吉安县"
            },
            {
              pinyin: "JiShui",
              shortname: "吉水县"
            },
            {
              pinyin: "XiaJiang",
              shortname: "峡江县"
            },
            {
              pinyin: "XinGan",
              shortname: "新干县"
            },
            {
              pinyin: "YongFeng",
              shortname: "永丰县"
            },
            {
              pinyin: "TaiHe",
              shortname: "泰和县"
            },
            {
              pinyin: "SuiChuan",
              shortname: "遂川县"
            },
            {
              pinyin: "WanAn",
              shortname: "万安县"
            },
            {
              pinyin: "AnFu",
              shortname: "安福县"
            },
            {
              pinyin: "YongXin",
              shortname: "永新县"
            },
            {
              pinyin: "JingGangShan",
              shortname: "井冈山市"
            }
          ],
          shortname: "吉安市"
        },
        {
          pinyin: "YiChun",
          children: [
            {
              pinyin: "YiChun",
              shortname: "市辖区"
            },
            {
              pinyin: "YuanZhou",
              shortname: "袁州区"
            },
            {
              pinyin: "FengXin",
              shortname: "奉新县"
            },
            {
              pinyin: "WanZai",
              shortname: "万载县"
            },
            {
              pinyin: "ShangGao",
              shortname: "上高县"
            },
            {
              pinyin: "YiFeng",
              shortname: "宜丰县"
            },
            {
              pinyin: "JingAn",
              shortname: "靖安县"
            },
            {
              pinyin: "TongGu",
              shortname: "铜鼓县"
            },
            {
              pinyin: "FengCheng",
              shortname: "丰城市"
            },
            {
              pinyin: "ZhangShu",
              shortname: "樟树市"
            },
            {
              pinyin: "GaoAn",
              shortname: "高安市"
            }
          ],
          shortname: "宜春市"
        },
        {
          pinyin: "FuZhou",
          children: [
            {
              pinyin: "FuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LinChuan",
              shortname: "临川区"
            },
            {
              pinyin: "DongXiangQu",
              shortname: "东乡区"
            },
            {
              pinyin: "NanCheng",
              shortname: "南城县"
            },
            {
              pinyin: "LiChuan",
              shortname: "黎川县"
            },
            {
              pinyin: "NanFeng",
              shortname: "南丰县"
            },
            {
              pinyin: "ChongRen",
              shortname: "崇仁县"
            },
            {
              pinyin: "LeAn",
              shortname: "乐安县"
            },
            {
              pinyin: "YiHuang",
              shortname: "宜黄县"
            },
            {
              pinyin: "JinXi",
              shortname: "金溪县"
            },
            {
              pinyin: "ZiXi",
              shortname: "资溪县"
            },
            {
              pinyin: "GuangChang",
              shortname: "广昌县"
            }
          ],
          shortname: "抚州市"
        },
        {
          pinyin: "ShangRao",
          children: [
            {
              pinyin: "ShangRao",
              shortname: "市辖区"
            },
            {
              pinyin: "XinZhou",
              shortname: "信州区"
            },
            {
              pinyin: "GuangFeng",
              shortname: "广丰区"
            },
            {
              pinyin: "ShangRao",
              shortname: "上饶县"
            },
            {
              pinyin: "YuShan",
              shortname: "玉山县"
            },
            {
              pinyin: "YanShan",
              shortname: "铅山县"
            },
            {
              pinyin: "HengFeng",
              shortname: "横峰县"
            },
            {
              pinyin: "YiYang",
              shortname: "弋阳县"
            },
            {
              pinyin: "YuGan",
              shortname: "余干县"
            },
            {
              pinyin: "PoYang",
              shortname: "鄱阳县"
            },
            {
              pinyin: "WanNian",
              shortname: "万年县"
            },
            {
              pinyin: "WuYuan",
              shortname: "婺源县"
            },
            {
              pinyin: "DeXing",
              shortname: "德兴市"
            }
          ],
          shortname: "上饶市"
        }
      ],
      shortname: "江西省"
    },
    {
      pinyin: "ShanDong",
      children: [
        {
          pinyin: "JiNan",
          children: [
            {
              pinyin: "JiNan",
              shortname: "市辖区"
            },
            {
              pinyin: "LiXia",
              shortname: "历下区"
            },
            {
              pinyin: "ShiZhong",
              shortname: "市中区"
            },
            {
              pinyin: "HuaiYin",
              shortname: "槐荫区"
            },
            {
              pinyin: "TianQiao",
              shortname: "天桥区"
            },
            {
              pinyin: "LiCheng",
              shortname: "历城区"
            },
            {
              pinyin: "ChangQing",
              shortname: "长清区"
            },
            {
              pinyin: "ZhangQiu",
              shortname: "章丘区"
            },
            {
              pinyin: "JiYang",
              shortname: "济阳区"
            },
            {
              pinyin: "PingYin",
              shortname: "平阴县"
            },
            {
              pinyin: "ShangHe",
              shortname: "商河县"
            },
            {
              pinyin: "JiNanGao",
              shortname: "济南高新技术产业开发区"
            }
          ],
          shortname: "济南市"
        },
        {
          pinyin: "QingDao",
          children: [
            {
              pinyin: "QingDao",
              shortname: "市辖区"
            },
            {
              pinyin: "ShiNan",
              shortname: "市南区"
            },
            {
              pinyin: "ShiBei",
              shortname: "市北区"
            },
            {
              pinyin: "HuangDao",
              shortname: "黄岛区"
            },
            {
              pinyin: "LaoShan",
              shortname: "崂山区"
            },
            {
              pinyin: "LiCang",
              shortname: "李沧区"
            },
            {
              pinyin: "ChengYang",
              shortname: "城阳区"
            },
            {
              pinyin: "JiMo",
              shortname: "即墨区"
            },
            {
              pinyin: "QingDaoGao",
              shortname: "青岛高新技术产业开发区"
            },
            {
              pinyin: "JiaoZhou",
              shortname: "胶州市"
            },
            {
              pinyin: "PingDu",
              shortname: "平度市"
            },
            {
              pinyin: "LaiXi",
              shortname: "莱西市"
            }
          ],
          shortname: "青岛市"
        },
        {
          pinyin: "ZiBo",
          children: [
            {
              pinyin: "ZiBo",
              shortname: "市辖区"
            },
            {
              pinyin: "ZiChuan",
              shortname: "淄川区"
            },
            {
              pinyin: "ZhangDian",
              shortname: "张店区"
            },
            {
              pinyin: "BoShan",
              shortname: "博山区"
            },
            {
              pinyin: "LinZi",
              shortname: "临淄区"
            },
            {
              pinyin: "ZhouCun",
              shortname: "周村区"
            },
            {
              pinyin: "HuanTai",
              shortname: "桓台县"
            },
            {
              pinyin: "GaoQing",
              shortname: "高青县"
            },
            {
              pinyin: "YiYuan",
              shortname: "沂源县"
            }
          ],
          shortname: "淄博市"
        },
        {
          pinyin: "ZaoZhuang",
          children: [
            {
              pinyin: "ZaoZhuang",
              shortname: "市辖区"
            },
            {
              pinyin: "ShiZhong",
              shortname: "市中区"
            },
            {
              pinyin: "XueCheng",
              shortname: "薛城区"
            },
            {
              pinyin: "YiCheng",
              shortname: "峄城区"
            },
            {
              pinyin: "TaiErZhuang",
              shortname: "台儿庄区"
            },
            {
              pinyin: "ShanTing",
              shortname: "山亭区"
            },
            {
              pinyin: "TengZhou",
              shortname: "滕州市"
            }
          ],
          shortname: "枣庄市"
        },
        {
          pinyin: "DongYing",
          children: [
            {
              pinyin: "DongYing",
              shortname: "市辖区"
            },
            {
              pinyin: "DongYing",
              shortname: "东营区"
            },
            {
              pinyin: "HeKou",
              shortname: "河口区"
            },
            {
              pinyin: "KenLi",
              shortname: "垦利区"
            },
            {
              pinyin: "LiJin",
              shortname: "利津县"
            },
            {
              pinyin: "GuangRao",
              shortname: "广饶县"
            },
            {
              pinyin: "DongYingJingKaiQu",
              shortname: "东营经济技术开发区"
            },
            {
              pinyin: "DongYingGangJingKaiQu",
              shortname: "东营港经济开发区"
            }
          ],
          shortname: "东营市"
        },
        {
          pinyin: "YanTai",
          children: [
            {
              pinyin: "YanTai",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhiFu",
              shortname: "芝罘区"
            },
            {
              pinyin: "FuShan",
              shortname: "福山区"
            },
            {
              pinyin: "MuPing",
              shortname: "牟平区"
            },
            {
              pinyin: "LaiShan",
              shortname: "莱山区"
            },
            {
              pinyin: "ChangDao",
              shortname: "长岛县"
            },
            {
              pinyin: "YanTaiGao",
              shortname: "烟台高新技术产业开发区"
            },
            {
              pinyin: "YanTaiJingKaiQu",
              shortname: "烟台经济技术开发区"
            },
            {
              pinyin: "LongKou",
              shortname: "龙口市"
            },
            {
              pinyin: "LaiYang",
              shortname: "莱阳市"
            },
            {
              pinyin: "LaiZhou",
              shortname: "莱州市"
            },
            {
              pinyin: "PengLai",
              shortname: "蓬莱市"
            },
            {
              pinyin: "ZhaoYuan",
              shortname: "招远市"
            },
            {
              pinyin: "QiXia",
              shortname: "栖霞市"
            },
            {
              pinyin: "HaiYang",
              shortname: "海阳市"
            }
          ],
          shortname: "烟台市"
        },
        {
          pinyin: "WeiFang",
          children: [
            {
              pinyin: "WeiFang",
              shortname: "市辖区"
            },
            {
              pinyin: "WeiCheng",
              shortname: "潍城区"
            },
            {
              pinyin: "HanTing",
              shortname: "寒亭区"
            },
            {
              pinyin: "FangZi",
              shortname: "坊子区"
            },
            {
              pinyin: "KuiWen",
              shortname: "奎文区"
            },
            {
              pinyin: "LinQu",
              shortname: "临朐县"
            },
            {
              pinyin: "ChangLe",
              shortname: "昌乐县"
            },
            {
              pinyin: "WeiFangBinHaiJingKaiQu",
              shortname: "潍坊滨海经济技术开发区"
            },
            {
              pinyin: "QingZhou",
              shortname: "青州市"
            },
            {
              pinyin: "ZhuCheng",
              shortname: "诸城市"
            },
            {
              pinyin: "ShouGuang",
              shortname: "寿光市"
            },
            {
              pinyin: "AnQiu",
              shortname: "安丘市"
            },
            {
              pinyin: "GaoMi",
              shortname: "高密市"
            },
            {
              pinyin: "ChangYi",
              shortname: "昌邑市"
            }
          ],
          shortname: "潍坊市"
        },
        {
          pinyin: "JiNing",
          children: [
            {
              pinyin: "JiNing",
              shortname: "市辖区"
            },
            {
              pinyin: "RenCheng",
              shortname: "任城区"
            },
            {
              pinyin: "YanZhou",
              shortname: "兖州区"
            },
            {
              pinyin: "WeiShan",
              shortname: "微山县"
            },
            {
              pinyin: "YuTai",
              shortname: "鱼台县"
            },
            {
              pinyin: "JinXiang",
              shortname: "金乡县"
            },
            {
              pinyin: "JiaXiang",
              shortname: "嘉祥县"
            },
            {
              pinyin: "WenShang",
              shortname: "汶上县"
            },
            {
              pinyin: "SiShui",
              shortname: "泗水县"
            },
            {
              pinyin: "LiangShan",
              shortname: "梁山县"
            },
            {
              pinyin: "JiNingGao",
              shortname: "济宁高新技术产业开发区"
            },
            {
              pinyin: "QuFu",
              shortname: "曲阜市"
            },
            {
              pinyin: "ZouCheng",
              shortname: "邹城市"
            }
          ],
          shortname: "济宁市"
        },
        {
          pinyin: "TaiAn",
          children: [
            {
              pinyin: "TaiAn",
              shortname: "市辖区"
            },
            {
              pinyin: "TaiShan",
              shortname: "泰山区"
            },
            {
              pinyin: "DaiYue",
              shortname: "岱岳区"
            },
            {
              pinyin: "NingYang",
              shortname: "宁阳县"
            },
            {
              pinyin: "DongPing",
              shortname: "东平县"
            },
            {
              pinyin: "XinTai",
              shortname: "新泰市"
            },
            {
              pinyin: "FeiCheng",
              shortname: "肥城市"
            }
          ],
          shortname: "泰安市"
        },
        {
          pinyin: "WeiHai",
          children: [
            {
              pinyin: "WeiHai",
              shortname: "市辖区"
            },
            {
              pinyin: "HuanCui",
              shortname: "环翠区"
            },
            {
              pinyin: "WenDeng",
              shortname: "文登区"
            },
            {
              pinyin: "WeiHaiHuoJuGao",
              shortname: "威海火炬高技术产业开发区"
            },
            {
              pinyin: "WeiHaiJingKaiQu",
              shortname: "威海经济技术开发区"
            },
            {
              pinyin: "WeiHaiLinGangJingKaiQu",
              shortname: "威海临港经济技术开发区"
            },
            {
              pinyin: "RongCheng",
              shortname: "荣成市"
            },
            {
              pinyin: "RuShan",
              shortname: "乳山市"
            }
          ],
          shortname: "威海市"
        },
        {
          pinyin: "RiZhao",
          children: [
            {
              pinyin: "RiZhao",
              shortname: "市辖区"
            },
            {
              pinyin: "DongGang",
              shortname: "东港区"
            },
            {
              pinyin: "LanShan",
              shortname: "岚山区"
            },
            {
              pinyin: "WuLian",
              shortname: "五莲县"
            },
            {
              pinyin: "JuXian",
              shortname: "莒县"
            },
            {
              pinyin: "RiZhaoJingKaiQu",
              shortname: "日照经济技术开发区"
            }
          ],
          shortname: "日照市"
        },
        {
          pinyin: "LaiWu",
          children: [
            {
              pinyin: "LaiWu",
              shortname: "市辖区"
            },
            {
              pinyin: "LaiCheng",
              shortname: "莱城区"
            },
            {
              pinyin: "GangCheng",
              shortname: "钢城区"
            }
          ],
          shortname: "莱芜市"
        },
        {
          pinyin: "LinYi",
          children: [
            {
              pinyin: "LinYi",
              shortname: "市辖区"
            },
            {
              pinyin: "LanShan",
              shortname: "兰山区"
            },
            {
              pinyin: "LuoZhuang",
              shortname: "罗庄区"
            },
            {
              pinyin: "HeDong",
              shortname: "河东区"
            },
            {
              pinyin: "YiNan",
              shortname: "沂南县"
            },
            {
              pinyin: "TanCheng",
              shortname: "郯城县"
            },
            {
              pinyin: "YiShui",
              shortname: "沂水县"
            },
            {
              pinyin: "LanLing",
              shortname: "兰陵县"
            },
            {
              pinyin: "FeiXian",
              shortname: "费县"
            },
            {
              pinyin: "PingYi",
              shortname: "平邑县"
            },
            {
              pinyin: "JuNan",
              shortname: "莒南县"
            },
            {
              pinyin: "MengYin",
              shortname: "蒙阴县"
            },
            {
              pinyin: "LinShu",
              shortname: "临沭县"
            },
            {
              pinyin: "LinYiGao",
              shortname: "临沂高新技术产业开发区"
            },
            {
              pinyin: "LinYiJingKaiQu",
              shortname: "临沂经济技术开发区"
            },
            {
              pinyin: "LinYiLinGangJingKaiQu",
              shortname: "临沂临港经济开发区"
            }
          ],
          shortname: "临沂市"
        },
        {
          pinyin: "DeZhou",
          children: [
            {
              pinyin: "DeZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "DeCheng",
              shortname: "德城区"
            },
            {
              pinyin: "LingCheng",
              shortname: "陵城区"
            },
            {
              pinyin: "NingJin",
              shortname: "宁津县"
            },
            {
              pinyin: "QingYun",
              shortname: "庆云县"
            },
            {
              pinyin: "LinYi",
              shortname: "临邑县"
            },
            {
              pinyin: "QiHe",
              shortname: "齐河县"
            },
            {
              pinyin: "PingYuan",
              shortname: "平原县"
            },
            {
              pinyin: "XiaJin",
              shortname: "夏津县"
            },
            {
              pinyin: "WuCheng",
              shortname: "武城县"
            },
            {
              pinyin: "DeZhouJingKaiQu",
              shortname: "德州经济技术开发区"
            },
            {
              pinyin: "DeZhouYunHeJingKaiQu",
              shortname: "德州运河经济开发区"
            },
            {
              pinyin: "LeLing",
              shortname: "乐陵市"
            },
            {
              pinyin: "YuCheng",
              shortname: "禹城市"
            }
          ],
          shortname: "德州市"
        },
        {
          pinyin: "LiaoCheng",
          children: [
            {
              pinyin: "LiaoCheng",
              shortname: "市辖区"
            },
            {
              pinyin: "DongChangFu",
              shortname: "东昌府区"
            },
            {
              pinyin: "YangGu",
              shortname: "阳谷县"
            },
            {
              pinyin: "ShenXian",
              shortname: "莘县"
            },
            {
              pinyin: "ChiPing",
              shortname: "茌平县"
            },
            {
              pinyin: "DongE",
              shortname: "东阿县"
            },
            {
              pinyin: "GuanXian",
              shortname: "冠县"
            },
            {
              pinyin: "GaoTang",
              shortname: "高唐县"
            },
            {
              pinyin: "LinQing",
              shortname: "临清市"
            }
          ],
          shortname: "聊城市"
        },
        {
          pinyin: "BinZhou",
          children: [
            {
              pinyin: "BinZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "BinCheng",
              shortname: "滨城区"
            },
            {
              pinyin: "ZhanHua",
              shortname: "沾化区"
            },
            {
              pinyin: "HuiMin",
              shortname: "惠民县"
            },
            {
              pinyin: "YangXin",
              shortname: "阳信县"
            },
            {
              pinyin: "WuDi",
              shortname: "无棣县"
            },
            {
              pinyin: "BoXing",
              shortname: "博兴县"
            },
            {
              pinyin: "ZouPing",
              shortname: "邹平市"
            }
          ],
          shortname: "滨州市"
        },
        {
          pinyin: "HeZe",
          children: [
            {
              pinyin: "HeZe",
              shortname: "市辖区"
            },
            {
              pinyin: "MuDan",
              shortname: "牡丹区"
            },
            {
              pinyin: "DingTao",
              shortname: "定陶区"
            },
            {
              pinyin: "CaoXian",
              shortname: "曹县"
            },
            {
              pinyin: "ShanXian",
              shortname: "单县"
            },
            {
              pinyin: "ChengWu",
              shortname: "成武县"
            },
            {
              pinyin: "JuYe",
              shortname: "巨野县"
            },
            {
              pinyin: "YunCheng",
              shortname: "郓城县"
            },
            {
              pinyin: "JuanCheng",
              shortname: "鄄城县"
            },
            {
              pinyin: "DongMing",
              shortname: "东明县"
            },
            {
              pinyin: "HeZeJingKaiQu",
              shortname: "菏泽经济技术开发区"
            },
            {
              pinyin: "HeZeGao",
              shortname: "菏泽高新技术开发区"
            }
          ],
          shortname: "菏泽市"
        }
      ],
      shortname: "山东省"
    },
    {
      pinyin: "HeNan",
      children: [
        {
          pinyin: "ZhengZhou",
          children: [
            {
              pinyin: "ZhengZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhongYuan",
              shortname: "中原区"
            },
            {
              pinyin: "ErQi",
              shortname: "二七区"
            },
            {
              pinyin: "GuanCheng",
              shortname: "管城回族区"
            },
            {
              pinyin: "JinShui",
              shortname: "金水区"
            },
            {
              pinyin: "ShangJie",
              shortname: "上街区"
            },
            {
              pinyin: "HuiJi",
              shortname: "惠济区"
            },
            {
              pinyin: "ZhongMu",
              shortname: "中牟县"
            },
            {
              pinyin: "ZhengZhouJingKaiQu",
              shortname: "郑州经济技术开发区"
            },
            {
              pinyin: "ZhengZhouGao",
              shortname: "郑州高新技术产业开发区"
            },
            {
              pinyin: "ZhengZhouHangKongGangJingKaiQu",
              shortname: "郑州航空港经济综合实验区"
            },
            {
              pinyin: "GongYi",
              shortname: "巩义市"
            },
            {
              pinyin: "XingYang",
              shortname: "荥阳市"
            },
            {
              pinyin: "XinMi",
              shortname: "新密市"
            },
            {
              pinyin: "XinZheng",
              shortname: "新郑市"
            },
            {
              pinyin: "DengFeng",
              shortname: "登封市"
            }
          ],
          shortname: "郑州市"
        },
        {
          pinyin: "KaiFeng",
          children: [
            {
              pinyin: "KaiFeng",
              shortname: "市辖区"
            },
            {
              pinyin: "LongTing",
              shortname: "龙亭区"
            },
            {
              pinyin: "ShunHe",
              shortname: "顺河回族区"
            },
            {
              pinyin: "GuLou",
              shortname: "鼓楼区"
            },
            {
              pinyin: "YuWangTai",
              shortname: "禹王台区"
            },
            {
              pinyin: "XiangFu",
              shortname: "祥符区"
            },
            {
              pinyin: "QiXian",
              shortname: "杞县"
            },
            {
              pinyin: "TongXu",
              shortname: "通许县"
            },
            {
              pinyin: "WeiShi",
              shortname: "尉氏县"
            },
            {
              pinyin: "LanKao",
              shortname: "兰考县"
            }
          ],
          shortname: "开封市"
        },
        {
          pinyin: "LuoYang",
          children: [
            {
              pinyin: "LuoYang",
              shortname: "市辖区"
            },
            {
              pinyin: "LaoCheng",
              shortname: "老城区"
            },
            {
              pinyin: "XiGong",
              shortname: "西工区"
            },
            {
              pinyin: "ChanHe",
              shortname: "瀍河回族区"
            },
            {
              pinyin: "JianXi",
              shortname: "涧西区"
            },
            {
              pinyin: "JiLi",
              shortname: "吉利区"
            },
            {
              pinyin: "LuoLong",
              shortname: "洛龙区"
            },
            {
              pinyin: "MengJin",
              shortname: "孟津县"
            },
            {
              pinyin: "XinAn",
              shortname: "新安县"
            },
            {
              pinyin: "LuanChuan",
              shortname: "栾川县"
            },
            {
              pinyin: "SongXian",
              shortname: "嵩县"
            },
            {
              pinyin: "RuYang",
              shortname: "汝阳县"
            },
            {
              pinyin: "YiYang",
              shortname: "宜阳县"
            },
            {
              pinyin: "LuoNing",
              shortname: "洛宁县"
            },
            {
              pinyin: "YiChuan",
              shortname: "伊川县"
            },
            {
              pinyin: "LuoYangGao",
              shortname: "洛阳高新技术产业开发区"
            },
            {
              pinyin: "YanShi",
              shortname: "偃师市"
            }
          ],
          shortname: "洛阳市"
        },
        {
          pinyin: "PingDingShan",
          children: [
            {
              pinyin: "PingDingShan",
              shortname: "市辖区"
            },
            {
              pinyin: "XinHua",
              shortname: "新华区"
            },
            {
              pinyin: "WeiDong",
              shortname: "卫东区"
            },
            {
              pinyin: "ShiLong",
              shortname: "石龙区"
            },
            {
              pinyin: "ZhanHe",
              shortname: "湛河区"
            },
            {
              pinyin: "BaoFeng",
              shortname: "宝丰县"
            },
            {
              pinyin: "YeXian",
              shortname: "叶县"
            },
            {
              pinyin: "LuShan",
              shortname: "鲁山县"
            },
            {
              pinyin: "JiaXian",
              shortname: "郏县"
            },
            {
              pinyin: "PingDingShanGao",
              shortname: "平顶山高新技术产业开发区"
            },
            {
              pinyin: "PingDingShanXinCheng",
              shortname: "平顶山市新城区"
            },
            {
              pinyin: "WuGang",
              shortname: "舞钢市"
            },
            {
              pinyin: "RuZhou",
              shortname: "汝州市"
            }
          ],
          shortname: "平顶山市"
        },
        {
          pinyin: "AnYang",
          children: [
            {
              pinyin: "AnYang",
              shortname: "市辖区"
            },
            {
              pinyin: "WenFeng",
              shortname: "文峰区"
            },
            {
              pinyin: "BeiGuan",
              shortname: "北关区"
            },
            {
              pinyin: "YinDu",
              shortname: "殷都区"
            },
            {
              pinyin: "LongAn",
              shortname: "龙安区"
            },
            {
              pinyin: "AnYang",
              shortname: "安阳县"
            },
            {
              pinyin: "TangYin",
              shortname: "汤阴县"
            },
            {
              pinyin: "HuaXian",
              shortname: "滑县"
            },
            {
              pinyin: "NeiHuang",
              shortname: "内黄县"
            },
            {
              pinyin: "AnYangGao",
              shortname: "安阳高新技术产业开发区"
            },
            {
              pinyin: "LinZhou",
              shortname: "林州市"
            }
          ],
          shortname: "安阳市"
        },
        {
          pinyin: "HeBi",
          children: [
            {
              pinyin: "HeBi",
              shortname: "市辖区"
            },
            {
              pinyin: "HeShan",
              shortname: "鹤山区"
            },
            {
              pinyin: "ShanCheng",
              shortname: "山城区"
            },
            {
              pinyin: "QiBin",
              shortname: "淇滨区"
            },
            {
              pinyin: "JunXian",
              shortname: "浚县"
            },
            {
              pinyin: "QiXian",
              shortname: "淇县"
            },
            {
              pinyin: "HeBiJingKaiQu",
              shortname: "鹤壁经济技术开发区"
            }
          ],
          shortname: "鹤壁市"
        },
        {
          pinyin: "XinXiang",
          children: [
            {
              pinyin: "XinXiang",
              shortname: "市辖区"
            },
            {
              pinyin: "HongQi",
              shortname: "红旗区"
            },
            {
              pinyin: "WeiBin",
              shortname: "卫滨区"
            },
            {
              pinyin: "FengQuan",
              shortname: "凤泉区"
            },
            {
              pinyin: "MuYe",
              shortname: "牧野区"
            },
            {
              pinyin: "XinXiang",
              shortname: "新乡县"
            },
            {
              pinyin: "HuoJia",
              shortname: "获嘉县"
            },
            {
              pinyin: "YuanYang",
              shortname: "原阳县"
            },
            {
              pinyin: "YanJin",
              shortname: "延津县"
            },
            {
              pinyin: "FengQiu",
              shortname: "封丘县"
            },
            {
              pinyin: "ChangYuan",
              shortname: "长垣县"
            },
            {
              pinyin: "XinXiangGao",
              shortname: "新乡高新技术产业开发区"
            },
            {
              pinyin: "XinXiangJingKaiQu",
              shortname: "新乡经济技术开发区"
            },
            {
              pinyin: "XinXiangPingYuanShiFan",
              shortname: "新乡市平原城乡一体化示范区"
            },
            {
              pinyin: "WeiHui",
              shortname: "卫辉市"
            },
            {
              pinyin: "HuiXianShi",
              shortname: "辉县市"
            }
          ],
          shortname: "新乡市"
        },
        {
          pinyin: "JiaoZuo",
          children: [
            {
              pinyin: "JiaoZuo",
              shortname: "市辖区"
            },
            {
              pinyin: "JieFang",
              shortname: "解放区"
            },
            {
              pinyin: "ZhongZhan",
              shortname: "中站区"
            },
            {
              pinyin: "MaCun",
              shortname: "马村区"
            },
            {
              pinyin: "ShanYang",
              shortname: "山阳区"
            },
            {
              pinyin: "XiuWu",
              shortname: "修武县"
            },
            {
              pinyin: "BoAi",
              shortname: "博爱县"
            },
            {
              pinyin: "WuZhi",
              shortname: "武陟县"
            },
            {
              pinyin: "WenXian",
              shortname: "温县"
            },
            {
              pinyin: "JiaoZuoShiFan",
              shortname: "焦作城乡一体化示范区"
            },
            {
              pinyin: "QinYang",
              shortname: "沁阳市"
            },
            {
              pinyin: "MengZhou",
              shortname: "孟州市"
            }
          ],
          shortname: "焦作市"
        },
        {
          pinyin: "PuYang",
          children: [
            {
              pinyin: "PuYang",
              shortname: "市辖区"
            },
            {
              pinyin: "HuaLong",
              shortname: "华龙区"
            },
            {
              pinyin: "QingFeng",
              shortname: "清丰县"
            },
            {
              pinyin: "NanLe",
              shortname: "南乐县"
            },
            {
              pinyin: "FanXian",
              shortname: "范县"
            },
            {
              pinyin: "TaiQian",
              shortname: "台前县"
            },
            {
              pinyin: "PuYang",
              shortname: "濮阳县"
            },
            {
              pinyin: "HeNanPuYangGongYeYuan",
              shortname: "河南濮阳工业园区"
            },
            {
              pinyin: "PuYangJingKaiQu",
              shortname: "濮阳经济技术开发区"
            }
          ],
          shortname: "濮阳市"
        },
        {
          pinyin: "XuChang",
          children: [
            {
              pinyin: "XuChang",
              shortname: "市辖区"
            },
            {
              pinyin: "WeiDu",
              shortname: "魏都区"
            },
            {
              pinyin: "JianAn",
              shortname: "建安区"
            },
            {
              pinyin: "YanLing",
              shortname: "鄢陵县"
            },
            {
              pinyin: "XiangCheng",
              shortname: "襄城县"
            },
            {
              pinyin: "XuChangJingKaiQu",
              shortname: "许昌经济技术开发区"
            },
            {
              pinyin: "YuZhou",
              shortname: "禹州市"
            },
            {
              pinyin: "ChangGe",
              shortname: "长葛市"
            }
          ],
          shortname: "许昌市"
        },
        {
          pinyin: "TaHe",
          children: [
            {
              pinyin: "TaHe",
              shortname: "市辖区"
            },
            {
              pinyin: "YuanHui",
              shortname: "源汇区"
            },
            {
              pinyin: "YanCheng",
              shortname: "郾城区"
            },
            {
              pinyin: "ShaoLing",
              shortname: "召陵区"
            },
            {
              pinyin: "WuYang",
              shortname: "舞阳县"
            },
            {
              pinyin: "LinYing",
              shortname: "临颍县"
            },
            {
              pinyin: "TaHeJingKaiQu",
              shortname: "漯河经济技术开发区"
            }
          ],
          shortname: "漯河市"
        },
        {
          pinyin: "SanMenXia",
          children: [
            {
              pinyin: "SanMenXia",
              shortname: "市辖区"
            },
            {
              pinyin: "HuBin",
              shortname: "湖滨区"
            },
            {
              pinyin: "ShanZhou",
              shortname: "陕州区"
            },
            {
              pinyin: "MianChi",
              shortname: "渑池县"
            },
            {
              pinyin: "LuShi",
              shortname: "卢氏县"
            },
            {
              pinyin: "HeNanSanMenXiaJingKaiQu",
              shortname: "河南三门峡经济开发区"
            },
            {
              pinyin: "YiMa",
              shortname: "义马市"
            },
            {
              pinyin: "LingBao",
              shortname: "灵宝市"
            }
          ],
          shortname: "三门峡市"
        },
        {
          pinyin: "NanYang",
          children: [
            {
              pinyin: "NanYang",
              shortname: "市辖区"
            },
            {
              pinyin: "WanCheng",
              shortname: "宛城区"
            },
            {
              pinyin: "WoLong",
              shortname: "卧龙区"
            },
            {
              pinyin: "NanZhao",
              shortname: "南召县"
            },
            {
              pinyin: "FangCheng",
              shortname: "方城县"
            },
            {
              pinyin: "XiXia",
              shortname: "西峡县"
            },
            {
              pinyin: "ZhenPing",
              shortname: "镇平县"
            },
            {
              pinyin: "NeiXiang",
              shortname: "内乡县"
            },
            {
              pinyin: "XiChuan",
              shortname: "淅川县"
            },
            {
              pinyin: "SheQi",
              shortname: "社旗县"
            },
            {
              pinyin: "TangHe",
              shortname: "唐河县"
            },
            {
              pinyin: "XinYe",
              shortname: "新野县"
            },
            {
              pinyin: "TongBai",
              shortname: "桐柏县"
            },
            {
              pinyin: "NanYangGao",
              shortname: "南阳高新技术产业开发区"
            },
            {
              pinyin: "NanYangShiFan",
              shortname: "南阳市城乡一体化示范区"
            },
            {
              pinyin: "DengZhou",
              shortname: "邓州市"
            }
          ],
          shortname: "南阳市"
        },
        {
          pinyin: "ShangQiu",
          children: [
            {
              pinyin: "ShangQiu",
              shortname: "市辖区"
            },
            {
              pinyin: "LiangYuan",
              shortname: "梁园区"
            },
            {
              pinyin: "SuiYang",
              shortname: "睢阳区"
            },
            {
              pinyin: "MinQuan",
              shortname: "民权县"
            },
            {
              pinyin: "SuiXian",
              shortname: "睢县"
            },
            {
              pinyin: "NingLing",
              shortname: "宁陵县"
            },
            {
              pinyin: "ZheCheng",
              shortname: "柘城县"
            },
            {
              pinyin: "YuCheng",
              shortname: "虞城县"
            },
            {
              pinyin: "XiaYi",
              shortname: "夏邑县"
            },
            {
              pinyin: "YuDongZongHeWuLiuChanYeJuJi",
              shortname: "豫东综合物流产业聚集区"
            },
            {
              pinyin: "HeNanShangQiuJingKaiQu",
              shortname: "河南商丘经济开发区"
            },
            {
              pinyin: "YongCheng",
              shortname: "永城市"
            }
          ],
          shortname: "商丘市"
        },
        {
          pinyin: "XinYang",
          children: [
            {
              pinyin: "XinYang",
              shortname: "市辖区"
            },
            {
              pinyin: "ShiHe",
              shortname: "浉河区"
            },
            {
              pinyin: "PingQiao",
              shortname: "平桥区"
            },
            {
              pinyin: "LuoShan",
              shortname: "罗山县"
            },
            {
              pinyin: "GuangShan",
              shortname: "光山县"
            },
            {
              pinyin: "XinXian",
              shortname: "新县"
            },
            {
              pinyin: "ShangCheng",
              shortname: "商城县"
            },
            {
              pinyin: "GuShi",
              shortname: "固始县"
            },
            {
              pinyin: "HuangChuan",
              shortname: "潢川县"
            },
            {
              pinyin: "HuaiBin",
              shortname: "淮滨县"
            },
            {
              pinyin: "XiXian",
              shortname: "息县"
            },
            {
              pinyin: "XinYangGao",
              shortname: "信阳高新技术产业开发区"
            }
          ],
          shortname: "信阳市"
        },
        {
          pinyin: "ZhouKou",
          children: [
            {
              pinyin: "ZhouKou",
              shortname: "市辖区"
            },
            {
              pinyin: "ChuanHui",
              shortname: "川汇区"
            },
            {
              pinyin: "FuGou",
              shortname: "扶沟县"
            },
            {
              pinyin: "XiHua",
              shortname: "西华县"
            },
            {
              pinyin: "ShangShui",
              shortname: "商水县"
            },
            {
              pinyin: "ShenQiu",
              shortname: "沈丘县"
            },
            {
              pinyin: "DanCheng",
              shortname: "郸城县"
            },
            {
              pinyin: "HuaiYang",
              shortname: "淮阳县"
            },
            {
              pinyin: "TaiKang",
              shortname: "太康县"
            },
            {
              pinyin: "LuYi",
              shortname: "鹿邑县"
            },
            {
              pinyin: "HeNanZhouKouJingKaiQu",
              shortname: "河南周口经济开发区"
            },
            {
              pinyin: "XiangCheng",
              shortname: "项城市"
            }
          ],
          shortname: "周口市"
        },
        {
          pinyin: "ZhuMaDian",
          children: [
            {
              pinyin: "ZhuMaDian",
              shortname: "市辖区"
            },
            {
              pinyin: "YiCheng",
              shortname: "驿城区"
            },
            {
              pinyin: "XiPing",
              shortname: "西平县"
            },
            {
              pinyin: "ShangCai",
              shortname: "上蔡县"
            },
            {
              pinyin: "PingYu",
              shortname: "平舆县"
            },
            {
              pinyin: "ZhengYang",
              shortname: "正阳县"
            },
            {
              pinyin: "QueShan",
              shortname: "确山县"
            },
            {
              pinyin: "BiYang",
              shortname: "泌阳县"
            },
            {
              pinyin: "RuNan",
              shortname: "汝南县"
            },
            {
              pinyin: "SuiPing",
              shortname: "遂平县"
            },
            {
              pinyin: "XinCai",
              shortname: "新蔡县"
            },
            {
              pinyin: "HeNanZhuMaDianJingKaiQu",
              shortname: "河南驻马店经济开发区"
            }
          ],
          shortname: "驻马店市"
        },
        {
          pinyin: "HeNan",
          children: [
            {
              pinyin: "JiYuan",
              shortname: "济源市"
            }
          ],
          shortname: "直辖县"
        }
      ],
      shortname: "河南省"
    },
    {
      pinyin: "HuBei",
      children: [
        {
          pinyin: "WuHan",
          children: [
            {
              pinyin: "WuHan",
              shortname: "市辖区"
            },
            {
              pinyin: "JiangAn",
              shortname: "江岸区"
            },
            {
              pinyin: "JiangHan",
              shortname: "江汉区"
            },
            {
              pinyin: "QiaoKou",
              shortname: "硚口区"
            },
            {
              pinyin: "HanYang",
              shortname: "汉阳区"
            },
            {
              pinyin: "WuChang",
              shortname: "武昌区"
            },
            {
              pinyin: "QingShan",
              shortname: "青山区"
            },
            {
              pinyin: "HongShan",
              shortname: "洪山区"
            },
            {
              pinyin: "DongXiHu",
              shortname: "东西湖区"
            },
            {
              pinyin: "HanNan",
              shortname: "汉南区"
            },
            {
              pinyin: "CaiDian",
              shortname: "蔡甸区"
            },
            {
              pinyin: "JiangXia",
              shortname: "江夏区"
            },
            {
              pinyin: "HuangPo",
              shortname: "黄陂区"
            },
            {
              pinyin: "XinZhou",
              shortname: "新洲区"
            }
          ],
          shortname: "武汉市"
        },
        {
          pinyin: "HuangShi",
          children: [
            {
              pinyin: "HuangShi",
              shortname: "市辖区"
            },
            {
              pinyin: "HuangShiGang",
              shortname: "黄石港区"
            },
            {
              pinyin: "XiSaiShan",
              shortname: "西塞山区"
            },
            {
              pinyin: "XiaLu",
              shortname: "下陆区"
            },
            {
              pinyin: "TieShan",
              shortname: "铁山区"
            },
            {
              pinyin: "YangXin",
              shortname: "阳新县"
            },
            {
              pinyin: "DaYe",
              shortname: "大冶市"
            }
          ],
          shortname: "黄石市"
        },
        {
          pinyin: "ShiYan",
          children: [
            {
              pinyin: "ShiYan",
              shortname: "市辖区"
            },
            {
              pinyin: "MaoJian",
              shortname: "茅箭区"
            },
            {
              pinyin: "ZhangWan",
              shortname: "张湾区"
            },
            {
              pinyin: "YunYang",
              shortname: "郧阳区"
            },
            {
              pinyin: "YunXi",
              shortname: "郧西县"
            },
            {
              pinyin: "ZhuShan",
              shortname: "竹山县"
            },
            {
              pinyin: "ZhuXi",
              shortname: "竹溪县"
            },
            {
              pinyin: "FangXian",
              shortname: "房县"
            },
            {
              pinyin: "DanJiangKou",
              shortname: "丹江口市"
            }
          ],
          shortname: "十堰市"
        },
        {
          pinyin: "YiChang",
          children: [
            {
              pinyin: "YiChang",
              shortname: "市辖区"
            },
            {
              pinyin: "XiLing",
              shortname: "西陵区"
            },
            {
              pinyin: "WuJiaGang",
              shortname: "伍家岗区"
            },
            {
              pinyin: "DianJun",
              shortname: "点军区"
            },
            {
              pinyin: "XiaoTing",
              shortname: "猇亭区"
            },
            {
              pinyin: "YiLing",
              shortname: "夷陵区"
            },
            {
              pinyin: "YuanAn",
              shortname: "远安县"
            },
            {
              pinyin: "XingShan",
              shortname: "兴山县"
            },
            {
              pinyin: "ZiGui",
              shortname: "秭归县"
            },
            {
              pinyin: "ZhangYang",
              shortname: "长阳土家族自治县"
            },
            {
              pinyin: "WuFeng",
              shortname: "五峰土家族自治县"
            },
            {
              pinyin: "YiDu",
              shortname: "宜都市"
            },
            {
              pinyin: "DangYang",
              shortname: "当阳市"
            },
            {
              pinyin: "ZhiJiang",
              shortname: "枝江市"
            }
          ],
          shortname: "宜昌市"
        },
        {
          pinyin: "XiangYang",
          children: [
            {
              pinyin: "XiangYang",
              shortname: "市辖区"
            },
            {
              pinyin: "XiangCheng",
              shortname: "襄城区"
            },
            {
              pinyin: "FanCheng",
              shortname: "樊城区"
            },
            {
              pinyin: "XiangZhou",
              shortname: "襄州区"
            },
            {
              pinyin: "NanZhang",
              shortname: "南漳县"
            },
            {
              pinyin: "GuCheng",
              shortname: "谷城县"
            },
            {
              pinyin: "BaoKang",
              shortname: "保康县"
            },
            {
              pinyin: "LaoHeKou",
              shortname: "老河口市"
            },
            {
              pinyin: "ZaoYang",
              shortname: "枣阳市"
            },
            {
              pinyin: "YiCheng",
              shortname: "宜城市"
            }
          ],
          shortname: "襄阳市"
        },
        {
          pinyin: "EZhou",
          children: [
            {
              pinyin: "EZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LiangZiHu",
              shortname: "梁子湖区"
            },
            {
              pinyin: "HuaRong",
              shortname: "华容区"
            },
            {
              pinyin: "ECheng",
              shortname: "鄂城区"
            }
          ],
          shortname: "鄂州市"
        },
        {
          pinyin: "JingMen",
          children: [
            {
              pinyin: "JingMen",
              shortname: "市辖区"
            },
            {
              pinyin: "DongBao",
              shortname: "东宝区"
            },
            {
              pinyin: "DuoDao",
              shortname: "掇刀区"
            },
            {
              pinyin: "ShaYang",
              shortname: "沙洋县"
            },
            {
              pinyin: "ZhongXiang",
              shortname: "钟祥市"
            },
            {
              pinyin: "JingShan",
              shortname: "京山市"
            }
          ],
          shortname: "荆门市"
        },
        {
          pinyin: "XiaoGan",
          children: [
            {
              pinyin: "XiaoGan",
              shortname: "市辖区"
            },
            {
              pinyin: "XiaoNan",
              shortname: "孝南区"
            },
            {
              pinyin: "XiaoChang",
              shortname: "孝昌县"
            },
            {
              pinyin: "DaWu",
              shortname: "大悟县"
            },
            {
              pinyin: "YunMeng",
              shortname: "云梦县"
            },
            {
              pinyin: "YingCheng",
              shortname: "应城市"
            },
            {
              pinyin: "AnLu",
              shortname: "安陆市"
            },
            {
              pinyin: "HanChuan",
              shortname: "汉川市"
            }
          ],
          shortname: "孝感市"
        },
        {
          pinyin: "JingZhou",
          children: [
            {
              pinyin: "JingZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ShaShi",
              shortname: "沙市区"
            },
            {
              pinyin: "JingZhou",
              shortname: "荆州区"
            },
            {
              pinyin: "GongAn",
              shortname: "公安县"
            },
            {
              pinyin: "JianLi",
              shortname: "监利县"
            },
            {
              pinyin: "JiangLing",
              shortname: "江陵县"
            },
            {
              pinyin: "JingZhouJingKaiQu",
              shortname: "荆州经济技术开发区"
            },
            {
              pinyin: "ShiShou",
              shortname: "石首市"
            },
            {
              pinyin: "HongHu",
              shortname: "洪湖市"
            },
            {
              pinyin: "SongZi",
              shortname: "松滋市"
            }
          ],
          shortname: "荆州市"
        },
        {
          pinyin: "HuangGang",
          children: [
            {
              pinyin: "HuangGang",
              shortname: "市辖区"
            },
            {
              pinyin: "HuangZhou",
              shortname: "黄州区"
            },
            {
              pinyin: "TuanFeng",
              shortname: "团风县"
            },
            {
              pinyin: "HongAn",
              shortname: "红安县"
            },
            {
              pinyin: "LuoTian",
              shortname: "罗田县"
            },
            {
              pinyin: "YingShan",
              shortname: "英山县"
            },
            {
              pinyin: "XiShui",
              shortname: "浠水县"
            },
            {
              pinyin: "QiChun",
              shortname: "蕲春县"
            },
            {
              pinyin: "HuangMei",
              shortname: "黄梅县"
            },
            {
              pinyin: "LongGanHuGuanLi",
              shortname: "龙感湖管理区"
            },
            {
              pinyin: "MaCheng",
              shortname: "麻城市"
            },
            {
              pinyin: "WuXue",
              shortname: "武穴市"
            }
          ],
          shortname: "黄冈市"
        },
        {
          pinyin: "XianNing",
          children: [
            {
              pinyin: "XianNing",
              shortname: "市辖区"
            },
            {
              pinyin: "XianAn",
              shortname: "咸安区"
            },
            {
              pinyin: "JiaYu",
              shortname: "嘉鱼县"
            },
            {
              pinyin: "TongCheng",
              shortname: "通城县"
            },
            {
              pinyin: "ChongYang",
              shortname: "崇阳县"
            },
            {
              pinyin: "TongShan",
              shortname: "通山县"
            },
            {
              pinyin: "ChiBi",
              shortname: "赤壁市"
            }
          ],
          shortname: "咸宁市"
        },
        {
          pinyin: "SuiZhou",
          children: [
            {
              pinyin: "SuiZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ZengDu",
              shortname: "曾都区"
            },
            {
              pinyin: "SuiXian",
              shortname: "随县"
            },
            {
              pinyin: "GuangShui",
              shortname: "广水市"
            }
          ],
          shortname: "随州市"
        },
        {
          pinyin: "EnShi",
          children: [
            {
              pinyin: "EnShi",
              shortname: "恩施市"
            },
            {
              pinyin: "LiChuan",
              shortname: "利川市"
            },
            {
              pinyin: "JianShi",
              shortname: "建始县"
            },
            {
              pinyin: "BaDong",
              shortname: "巴东县"
            },
            {
              pinyin: "XuanEn",
              shortname: "宣恩县"
            },
            {
              pinyin: "XianFeng",
              shortname: "咸丰县"
            },
            {
              pinyin: "LaiFeng",
              shortname: "来凤县"
            },
            {
              pinyin: "HeFeng",
              shortname: "鹤峰县"
            }
          ],
          shortname: "恩施土家族苗族自治州"
        },
        {
          pinyin: "HuBei",
          children: [
            {
              pinyin: "XianTao",
              shortname: "仙桃市"
            },
            {
              pinyin: "QianJiang",
              shortname: "潜江市"
            },
            {
              pinyin: "TianMen",
              shortname: "天门市"
            },
            {
              pinyin: "ShenNongJiaLin",
              shortname: "神农架林区"
            }
          ],
          shortname: "直辖县"
        }
      ],
      shortname: "湖北省"
    },
    {
      pinyin: "HuNan",
      children: [
        {
          pinyin: "ChangSha",
          children: [
            {
              pinyin: "ChangSha",
              shortname: "市辖区"
            },
            {
              pinyin: "FuRong",
              shortname: "芙蓉区"
            },
            {
              pinyin: "TianXin",
              shortname: "天心区"
            },
            {
              pinyin: "YueLu",
              shortname: "岳麓区"
            },
            {
              pinyin: "KaiFu",
              shortname: "开福区"
            },
            {
              pinyin: "YuHua",
              shortname: "雨花区"
            },
            {
              pinyin: "WangCheng",
              shortname: "望城区"
            },
            {
              pinyin: "ChangSha",
              shortname: "长沙县"
            },
            {
              pinyin: "LiuYang",
              shortname: "浏阳市"
            },
            {
              pinyin: "NingXiang",
              shortname: "宁乡市"
            }
          ],
          shortname: "长沙市"
        },
        {
          pinyin: "ZhuZhou",
          children: [
            {
              pinyin: "ZhuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "HeTang",
              shortname: "荷塘区"
            },
            {
              pinyin: "LuSong",
              shortname: "芦淞区"
            },
            {
              pinyin: "ShiFeng",
              shortname: "石峰区"
            },
            {
              pinyin: "TianYuan",
              shortname: "天元区"
            },
            {
              pinyin: "LuKou",
              shortname: "渌口区"
            },
            {
              pinyin: "YouXian",
              shortname: "攸县"
            },
            {
              pinyin: "ChaLing",
              shortname: "茶陵县"
            },
            {
              pinyin: "YanLing",
              shortname: "炎陵县"
            },
            {
              pinyin: "YunLongShiFan",
              shortname: "云龙示范区"
            },
            {
              pinyin: "LiLing",
              shortname: "醴陵市"
            }
          ],
          shortname: "株洲市"
        },
        {
          pinyin: "XiangTan",
          children: [
            {
              pinyin: "XiangTan",
              shortname: "市辖区"
            },
            {
              pinyin: "YuHu",
              shortname: "雨湖区"
            },
            {
              pinyin: "YueTang",
              shortname: "岳塘区"
            },
            {
              pinyin: "XiangTan",
              shortname: "湘潭县"
            },
            {
              pinyin: "HuNanXiangTanGao",
              shortname: "湖南湘潭高新技术产业园区"
            },
            {
              pinyin: "XiangTanZhaoShanShiFan",
              shortname: "湘潭昭山示范区"
            },
            {
              pinyin: "XiangTanJiuHuaShiFan",
              shortname: "湘潭九华示范区"
            },
            {
              pinyin: "XiangXiang",
              shortname: "湘乡市"
            },
            {
              pinyin: "ShaoShan",
              shortname: "韶山市"
            }
          ],
          shortname: "湘潭市"
        },
        {
          pinyin: "HengYang",
          children: [
            {
              pinyin: "HengYang",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhuHui",
              shortname: "珠晖区"
            },
            {
              pinyin: "YanFeng",
              shortname: "雁峰区"
            },
            {
              pinyin: "ShiGu",
              shortname: "石鼓区"
            },
            {
              pinyin: "ZhengXiang",
              shortname: "蒸湘区"
            },
            {
              pinyin: "NanYue",
              shortname: "南岳区"
            },
            {
              pinyin: "HengYang",
              shortname: "衡阳县"
            },
            {
              pinyin: "HengNan",
              shortname: "衡南县"
            },
            {
              pinyin: "HengShan",
              shortname: "衡山县"
            },
            {
              pinyin: "HengDong",
              shortname: "衡东县"
            },
            {
              pinyin: "QiDong",
              shortname: "祁东县"
            },
            {
              pinyin: "HengYangBaoShui",
              shortname: "衡阳综合保税区"
            },
            {
              pinyin: "HuNanHengYangGao",
              shortname: "湖南衡阳高新技术产业园区"
            },
            {
              pinyin: "HuNanHengYangSongMuJingKaiQu",
              shortname: "湖南衡阳松木经济开发区"
            },
            {
              pinyin: "LeiYang",
              shortname: "耒阳市"
            },
            {
              pinyin: "ChangNing",
              shortname: "常宁市"
            }
          ],
          shortname: "衡阳市"
        },
        {
          pinyin: "ShaoYang",
          children: [
            {
              pinyin: "ShaoYang",
              shortname: "市辖区"
            },
            {
              pinyin: "ShuangQing",
              shortname: "双清区"
            },
            {
              pinyin: "DaXiang",
              shortname: "大祥区"
            },
            {
              pinyin: "BeiTa",
              shortname: "北塔区"
            },
            {
              pinyin: "ShaoDong",
              shortname: "邵东县"
            },
            {
              pinyin: "XinShao",
              shortname: "新邵县"
            },
            {
              pinyin: "ShaoYang",
              shortname: "邵阳县"
            },
            {
              pinyin: "LongHui",
              shortname: "隆回县"
            },
            {
              pinyin: "DongKou",
              shortname: "洞口县"
            },
            {
              pinyin: "SuiNing",
              shortname: "绥宁县"
            },
            {
              pinyin: "XinNing",
              shortname: "新宁县"
            },
            {
              pinyin: "ChengBu",
              shortname: "城步苗族自治县"
            },
            {
              pinyin: "WuGang",
              shortname: "武冈市"
            }
          ],
          shortname: "邵阳市"
        },
        {
          pinyin: "YueYang",
          children: [
            {
              pinyin: "YueYang",
              shortname: "市辖区"
            },
            {
              pinyin: "YueYangLou",
              shortname: "岳阳楼区"
            },
            {
              pinyin: "YunXi",
              shortname: "云溪区"
            },
            {
              pinyin: "JunShan",
              shortname: "君山区"
            },
            {
              pinyin: "YueYang",
              shortname: "岳阳县"
            },
            {
              pinyin: "HuaRong",
              shortname: "华容县"
            },
            {
              pinyin: "XiangYin",
              shortname: "湘阴县"
            },
            {
              pinyin: "PingJiang",
              shortname: "平江县"
            },
            {
              pinyin: "YueYangQuYuanGuanLi",
              shortname: "岳阳市屈原管理区"
            },
            {
              pinyin: "MiLuo",
              shortname: "汨罗市"
            },
            {
              pinyin: "LinXiang",
              shortname: "临湘市"
            }
          ],
          shortname: "岳阳市"
        },
        {
          pinyin: "ChangDe",
          children: [
            {
              pinyin: "ChangDe",
              shortname: "市辖区"
            },
            {
              pinyin: "WuLing",
              shortname: "武陵区"
            },
            {
              pinyin: "DingCheng",
              shortname: "鼎城区"
            },
            {
              pinyin: "AnXiang",
              shortname: "安乡县"
            },
            {
              pinyin: "HanShou",
              shortname: "汉寿县"
            },
            {
              pinyin: "LiXian",
              shortname: "澧县"
            },
            {
              pinyin: "LinLi",
              shortname: "临澧县"
            },
            {
              pinyin: "TaoYuan",
              shortname: "桃源县"
            },
            {
              pinyin: "ShiMen",
              shortname: "石门县"
            },
            {
              pinyin: "ChangDeXiDongTingGuanLi",
              shortname: "常德市西洞庭管理区"
            },
            {
              pinyin: "JinShiShi",
              shortname: "津市市"
            }
          ],
          shortname: "常德市"
        },
        {
          pinyin: "ZhangJiaJie",
          children: [
            {
              pinyin: "ZhangJiaJie",
              shortname: "市辖区"
            },
            {
              pinyin: "YongDing",
              shortname: "永定区"
            },
            {
              pinyin: "WuLingYuan",
              shortname: "武陵源区"
            },
            {
              pinyin: "CiLi",
              shortname: "慈利县"
            },
            {
              pinyin: "SangZhi",
              shortname: "桑植县"
            }
          ],
          shortname: "张家界市"
        },
        {
          pinyin: "YiYang",
          children: [
            {
              pinyin: "YiYang",
              shortname: "市辖区"
            },
            {
              pinyin: "ZiYang",
              shortname: "资阳区"
            },
            {
              pinyin: "HeShan",
              shortname: "赫山区"
            },
            {
              pinyin: "NanXian",
              shortname: "南县"
            },
            {
              pinyin: "TaoJiang",
              shortname: "桃江县"
            },
            {
              pinyin: "AnHua",
              shortname: "安化县"
            },
            {
              pinyin: "YiYangDaTongHuGuanLi",
              shortname: "益阳市大通湖管理区"
            },
            {
              pinyin: "HuNanYiYangGao",
              shortname: "湖南益阳高新技术产业园区"
            },
            {
              pinyin: "YuanJiang",
              shortname: "沅江市"
            }
          ],
          shortname: "益阳市"
        },
        {
          pinyin: "ChenZhou",
          children: [
            {
              pinyin: "ChenZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "BeiHu",
              shortname: "北湖区"
            },
            {
              pinyin: "SuXian",
              shortname: "苏仙区"
            },
            {
              pinyin: "GuiYang",
              shortname: "桂阳县"
            },
            {
              pinyin: "YiZhang",
              shortname: "宜章县"
            },
            {
              pinyin: "YongXing",
              shortname: "永兴县"
            },
            {
              pinyin: "JiaHe",
              shortname: "嘉禾县"
            },
            {
              pinyin: "LinWu",
              shortname: "临武县"
            },
            {
              pinyin: "RuCheng",
              shortname: "汝城县"
            },
            {
              pinyin: "GuiDong",
              shortname: "桂东县"
            },
            {
              pinyin: "AnRen",
              shortname: "安仁县"
            },
            {
              pinyin: "ZiXing",
              shortname: "资兴市"
            }
          ],
          shortname: "郴州市"
        },
        {
          pinyin: "YongZhou",
          children: [
            {
              pinyin: "YongZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LingLing",
              shortname: "零陵区"
            },
            {
              pinyin: "LengShuiTan",
              shortname: "冷水滩区"
            },
            {
              pinyin: "QiYang",
              shortname: "祁阳县"
            },
            {
              pinyin: "DongAn",
              shortname: "东安县"
            },
            {
              pinyin: "ShuangPai",
              shortname: "双牌县"
            },
            {
              pinyin: "DaoXian",
              shortname: "道县"
            },
            {
              pinyin: "JiangYong",
              shortname: "江永县"
            },
            {
              pinyin: "NingYuan",
              shortname: "宁远县"
            },
            {
              pinyin: "LanShan",
              shortname: "蓝山县"
            },
            {
              pinyin: "XinTian",
              shortname: "新田县"
            },
            {
              pinyin: "JiangHua",
              shortname: "江华瑶族自治县"
            },
            {
              pinyin: "YongZhouJingKaiQu",
              shortname: "永州经济技术开发区"
            },
            {
              pinyin: "YongZhouJinDongGuanLi",
              shortname: "永州市金洞管理区"
            },
            {
              pinyin: "YongZhouHuiLongWeiGuanLi",
              shortname: "永州市回龙圩管理区"
            }
          ],
          shortname: "永州市"
        },
        {
          pinyin: "HuaiHua",
          children: [
            {
              pinyin: "HuaiHua",
              shortname: "市辖区"
            },
            {
              pinyin: "HeCheng",
              shortname: "鹤城区"
            },
            {
              pinyin: "ZhongFang",
              shortname: "中方县"
            },
            {
              pinyin: "YuanLing",
              shortname: "沅陵县"
            },
            {
              pinyin: "ChenXi",
              shortname: "辰溪县"
            },
            {
              pinyin: "XuPu",
              shortname: "溆浦县"
            },
            {
              pinyin: "HuiTong",
              shortname: "会同县"
            },
            {
              pinyin: "MaYang",
              shortname: "麻阳苗族自治县"
            },
            {
              pinyin: "XinHuang",
              shortname: "新晃侗族自治县"
            },
            {
              pinyin: "ZhiJiang",
              shortname: "芷江侗族自治县"
            },
            {
              pinyin: "JingZhou",
              shortname: "靖州苗族侗族自治县"
            },
            {
              pinyin: "TongDao",
              shortname: "通道侗族自治县"
            },
            {
              pinyin: "HuaiHuaHongJiangGuanLi",
              shortname: "怀化市洪江管理区"
            },
            {
              pinyin: "HongJiang",
              shortname: "洪江市"
            }
          ],
          shortname: "怀化市"
        },
        {
          pinyin: "LouDi",
          children: [
            {
              pinyin: "LouDi",
              shortname: "市辖区"
            },
            {
              pinyin: "LouXing",
              shortname: "娄星区"
            },
            {
              pinyin: "ShuangFeng",
              shortname: "双峰县"
            },
            {
              pinyin: "XinHua",
              shortname: "新化县"
            },
            {
              pinyin: "LengShuiJiang",
              shortname: "冷水江市"
            },
            {
              pinyin: "LianYuan",
              shortname: "涟源市"
            }
          ],
          shortname: "娄底市"
        },
        {
          pinyin: "XiangXi",
          children: [
            {
              pinyin: "JiShou",
              shortname: "吉首市"
            },
            {
              pinyin: "LuXi",
              shortname: "泸溪县"
            },
            {
              pinyin: "FengHuang",
              shortname: "凤凰县"
            },
            {
              pinyin: "HuaYuan",
              shortname: "花垣县"
            },
            {
              pinyin: "BaoJing",
              shortname: "保靖县"
            },
            {
              pinyin: "GuZhang",
              shortname: "古丈县"
            },
            {
              pinyin: "YongShun",
              shortname: "永顺县"
            },
            {
              pinyin: "LongShan",
              shortname: "龙山县"
            },
            {
              pinyin: "HuNanJiShouJingKaiQu",
              shortname: "湖南吉首经济开发区"
            },
            {
              pinyin: "HuNanYongShunJingKaiQu",
              shortname: "湖南永顺经济开发区"
            }
          ],
          shortname: "湘西土家族苗族自治州"
        }
      ],
      shortname: "湖南省"
    },
    {
      pinyin: "GuangDong",
      children: [
        {
          pinyin: "GuangZhou",
          children: [
            {
              pinyin: "GuangZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "LiWan",
              shortname: "荔湾区"
            },
            {
              pinyin: "YueXiu",
              shortname: "越秀区"
            },
            {
              pinyin: "HaiZhu",
              shortname: "海珠区"
            },
            {
              pinyin: "TianHe",
              shortname: "天河区"
            },
            {
              pinyin: "BaiYun",
              shortname: "白云区"
            },
            {
              pinyin: "HuangPu",
              shortname: "黄埔区"
            },
            {
              pinyin: "PanYu",
              shortname: "番禺区"
            },
            {
              pinyin: "HuaDu",
              shortname: "花都区"
            },
            {
              pinyin: "NanSha",
              shortname: "南沙区"
            },
            {
              pinyin: "CongHua",
              shortname: "从化区"
            },
            {
              pinyin: "ZengCheng",
              shortname: "增城区"
            }
          ],
          shortname: "广州市"
        },
        {
          pinyin: "ShaoGuan",
          children: [
            {
              pinyin: "ShaoGuan",
              shortname: "市辖区"
            },
            {
              pinyin: "WuJiang",
              shortname: "武江区"
            },
            {
              pinyin: "ZhenJiang",
              shortname: "浈江区"
            },
            {
              pinyin: "QuJiang",
              shortname: "曲江区"
            },
            {
              pinyin: "ShiXing",
              shortname: "始兴县"
            },
            {
              pinyin: "RenHua",
              shortname: "仁化县"
            },
            {
              pinyin: "WengYuan",
              shortname: "翁源县"
            },
            {
              pinyin: "RuYuan",
              shortname: "乳源瑶族自治县"
            },
            {
              pinyin: "XinFeng",
              shortname: "新丰县"
            },
            {
              pinyin: "LeChang",
              shortname: "乐昌市"
            },
            {
              pinyin: "NanXiong",
              shortname: "南雄市"
            }
          ],
          shortname: "韶关市"
        },
        {
          pinyin: "ShenZhen",
          children: [
            {
              pinyin: "ShenZhen",
              shortname: "市辖区"
            },
            {
              pinyin: "LuoHu",
              shortname: "罗湖区"
            },
            {
              pinyin: "FuTian",
              shortname: "福田区"
            },
            {
              pinyin: "NanShan",
              shortname: "南山区"
            },
            {
              pinyin: "BaoAn",
              shortname: "宝安区"
            },
            {
              pinyin: "LongGang",
              shortname: "龙岗区"
            },
            {
              pinyin: "YanTian",
              shortname: "盐田区"
            },
            {
              pinyin: "LongHua",
              shortname: "龙华区"
            },
            {
              pinyin: "PingShan",
              shortname: "坪山区"
            },
            {
              pinyin: "GuangMing",
              shortname: "光明区"
            }
          ],
          shortname: "深圳市"
        },
        {
          pinyin: "ZhuHai",
          children: [
            {
              pinyin: "ZhuHai",
              shortname: "市辖区"
            },
            {
              pinyin: "XiangZhou",
              shortname: "香洲区"
            },
            {
              pinyin: "DouMen",
              shortname: "斗门区"
            },
            {
              pinyin: "JinWan",
              shortname: "金湾区"
            }
          ],
          shortname: "珠海市"
        },
        {
          pinyin: "ShanTou",
          children: [
            {
              pinyin: "ShanTou",
              shortname: "市辖区"
            },
            {
              pinyin: "LongHu",
              shortname: "龙湖区"
            },
            {
              pinyin: "JinPing",
              shortname: "金平区"
            },
            {
              pinyin: "HaoJiang",
              shortname: "濠江区"
            },
            {
              pinyin: "ChaoYang",
              shortname: "潮阳区"
            },
            {
              pinyin: "ChaoNan",
              shortname: "潮南区"
            },
            {
              pinyin: "ChengHai",
              shortname: "澄海区"
            },
            {
              pinyin: "NanAo",
              shortname: "南澳县"
            }
          ],
          shortname: "汕头市"
        },
        {
          pinyin: "FoShan",
          children: [
            {
              pinyin: "FoShan",
              shortname: "市辖区"
            },
            {
              pinyin: "ChanCheng",
              shortname: "禅城区"
            },
            {
              pinyin: "NanHai",
              shortname: "南海区"
            },
            {
              pinyin: "ShunDe",
              shortname: "顺德区"
            },
            {
              pinyin: "SanShui",
              shortname: "三水区"
            },
            {
              pinyin: "GaoMing",
              shortname: "高明区"
            }
          ],
          shortname: "佛山市"
        },
        {
          pinyin: "JiangMen",
          children: [
            {
              pinyin: "JiangMen",
              shortname: "市辖区"
            },
            {
              pinyin: "PengJiang",
              shortname: "蓬江区"
            },
            {
              pinyin: "JiangHai",
              shortname: "江海区"
            },
            {
              pinyin: "XinHui",
              shortname: "新会区"
            },
            {
              pinyin: "TaiShan",
              shortname: "台山市"
            },
            {
              pinyin: "KaiPing",
              shortname: "开平市"
            },
            {
              pinyin: "HeShan",
              shortname: "鹤山市"
            },
            {
              pinyin: "EnPing",
              shortname: "恩平市"
            }
          ],
          shortname: "江门市"
        },
        {
          pinyin: "ZhanJiang",
          children: [
            {
              pinyin: "ZhanJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "ChiKan",
              shortname: "赤坎区"
            },
            {
              pinyin: "XiaShan",
              shortname: "霞山区"
            },
            {
              pinyin: "PoTou",
              shortname: "坡头区"
            },
            {
              pinyin: "MaZhang",
              shortname: "麻章区"
            },
            {
              pinyin: "SuiXi",
              shortname: "遂溪县"
            },
            {
              pinyin: "XuWen",
              shortname: "徐闻县"
            },
            {
              pinyin: "LianJiang",
              shortname: "廉江市"
            },
            {
              pinyin: "LeiZhou",
              shortname: "雷州市"
            },
            {
              pinyin: "WuChuan",
              shortname: "吴川市"
            }
          ],
          shortname: "湛江市"
        },
        {
          pinyin: "MaoMing",
          children: [
            {
              pinyin: "MaoMing",
              shortname: "市辖区"
            },
            {
              pinyin: "MaoNan",
              shortname: "茂南区"
            },
            {
              pinyin: "DianBai",
              shortname: "电白区"
            },
            {
              pinyin: "GaoZhou",
              shortname: "高州市"
            },
            {
              pinyin: "HuaZhou",
              shortname: "化州市"
            },
            {
              pinyin: "XinYi",
              shortname: "信宜市"
            }
          ],
          shortname: "茂名市"
        },
        {
          pinyin: "ZhaoQing",
          children: [
            {
              pinyin: "ZhaoQing",
              shortname: "市辖区"
            },
            {
              pinyin: "DuanZhou",
              shortname: "端州区"
            },
            {
              pinyin: "DingHu",
              shortname: "鼎湖区"
            },
            {
              pinyin: "GaoYao",
              shortname: "高要区"
            },
            {
              pinyin: "GuangNing",
              shortname: "广宁县"
            },
            {
              pinyin: "HuaiJi",
              shortname: "怀集县"
            },
            {
              pinyin: "FengKai",
              shortname: "封开县"
            },
            {
              pinyin: "DeQing",
              shortname: "德庆县"
            },
            {
              pinyin: "SiHui",
              shortname: "四会市"
            }
          ],
          shortname: "肇庆市"
        },
        {
          pinyin: "HuiZhou",
          children: [
            {
              pinyin: "HuiZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "HuiCheng",
              shortname: "惠城区"
            },
            {
              pinyin: "HuiYang",
              shortname: "惠阳区"
            },
            {
              pinyin: "BoLuo",
              shortname: "博罗县"
            },
            {
              pinyin: "HuiDong",
              shortname: "惠东县"
            },
            {
              pinyin: "LongMen",
              shortname: "龙门县"
            }
          ],
          shortname: "惠州市"
        },
        {
          pinyin: "MeiZhou",
          children: [
            {
              pinyin: "MeiZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "MeiJiang",
              shortname: "梅江区"
            },
            {
              pinyin: "MeiXian",
              shortname: "梅县区"
            },
            {
              pinyin: "DaBu",
              shortname: "大埔县"
            },
            {
              pinyin: "FengShun",
              shortname: "丰顺县"
            },
            {
              pinyin: "WuHua",
              shortname: "五华县"
            },
            {
              pinyin: "PingYuan",
              shortname: "平远县"
            },
            {
              pinyin: "JiaoLing",
              shortname: "蕉岭县"
            },
            {
              pinyin: "XingNing",
              shortname: "兴宁市"
            }
          ],
          shortname: "梅州市"
        },
        {
          pinyin: "ShanWei",
          children: [
            {
              pinyin: "ShanWei",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengQu",
              shortname: "城区"
            },
            {
              pinyin: "HaiFeng",
              shortname: "海丰县"
            },
            {
              pinyin: "LuHe",
              shortname: "陆河县"
            },
            {
              pinyin: "LuFeng",
              shortname: "陆丰市"
            }
          ],
          shortname: "汕尾市"
        },
        {
          pinyin: "HeYuan",
          children: [
            {
              pinyin: "HeYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "YuanCheng",
              shortname: "源城区"
            },
            {
              pinyin: "ZiJin",
              shortname: "紫金县"
            },
            {
              pinyin: "LongChuan",
              shortname: "龙川县"
            },
            {
              pinyin: "LianPing",
              shortname: "连平县"
            },
            {
              pinyin: "HePing",
              shortname: "和平县"
            },
            {
              pinyin: "DongYuan",
              shortname: "东源县"
            }
          ],
          shortname: "河源市"
        },
        {
          pinyin: "YangJiang",
          children: [
            {
              pinyin: "YangJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "JiangCheng",
              shortname: "江城区"
            },
            {
              pinyin: "YangDong",
              shortname: "阳东区"
            },
            {
              pinyin: "YangXi",
              shortname: "阳西县"
            },
            {
              pinyin: "YangChun",
              shortname: "阳春市"
            }
          ],
          shortname: "阳江市"
        },
        {
          pinyin: "QingYuan",
          children: [
            {
              pinyin: "QingYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "QingCheng",
              shortname: "清城区"
            },
            {
              pinyin: "QingXin",
              shortname: "清新区"
            },
            {
              pinyin: "FoGang",
              shortname: "佛冈县"
            },
            {
              pinyin: "YangShan",
              shortname: "阳山县"
            },
            {
              pinyin: "LianShan",
              shortname: "连山壮族瑶族自治县"
            },
            {
              pinyin: "LianNan",
              shortname: "连南瑶族自治县"
            },
            {
              pinyin: "YingDe",
              shortname: "英德市"
            },
            {
              pinyin: "LianZhou",
              shortname: "连州市"
            }
          ],
          shortname: "清远市"
        },
        {
          pinyin: "DongGuan",
          children: [
            {
              pinyin: "DongCheng",
              shortname: "东城街道办事处"
            },
            {
              pinyin: "NanCheng",
              shortname: "南城街道办事处"
            },
            {
              pinyin: "WanJiang",
              shortname: "万江街道办事处"
            },
            {
              pinyin: "GuanCheng",
              shortname: "莞城街道办事处"
            },
            {
              pinyin: "ShiJie",
              shortname: "石碣镇"
            },
            {
              pinyin: "ShiLong",
              shortname: "石龙镇"
            },
            {
              pinyin: "ChaShan",
              shortname: "茶山镇"
            },
            {
              pinyin: "ShiPai",
              shortname: "石排镇"
            },
            {
              pinyin: "QiShi",
              shortname: "企石镇"
            },
            {
              pinyin: "HengLi",
              shortname: "横沥镇"
            },
            {
              pinyin: "QiaoTou",
              shortname: "桥头镇"
            },
            {
              pinyin: "XieGang",
              shortname: "谢岗镇"
            },
            {
              pinyin: "DongKeng",
              shortname: "东坑镇"
            },
            {
              pinyin: "ChangPing",
              shortname: "常平镇"
            },
            {
              pinyin: "LiaoBu",
              shortname: "寮步镇"
            },
            {
              pinyin: "ZhangMu",
              shortname: "樟木头镇"
            },
            {
              pinyin: "DaLang",
              shortname: "大朗镇"
            },
            {
              pinyin: "HuangJiang",
              shortname: "黄江镇"
            },
            {
              pinyin: "QingXi",
              shortname: "清溪镇"
            },
            {
              pinyin: "TangSha",
              shortname: "塘厦镇"
            },
            {
              pinyin: "FengGang",
              shortname: "凤岗镇"
            },
            {
              pinyin: "DaLing",
              shortname: "大岭山镇"
            },
            {
              pinyin: "ChangAn",
              shortname: "长安镇"
            },
            {
              pinyin: "HuMen",
              shortname: "虎门镇"
            },
            {
              pinyin: "HouJie",
              shortname: "厚街镇"
            },
            {
              pinyin: "ShaTian",
              shortname: "沙田镇"
            },
            {
              pinyin: "DaoJiao",
              shortname: "道滘镇"
            },
            {
              pinyin: "HongMei",
              shortname: "洪梅镇"
            },
            {
              pinyin: "MaYong",
              shortname: "麻涌镇"
            },
            {
              pinyin: "WangNiu",
              shortname: "望牛墩镇"
            },
            {
              pinyin: "ZhongTang",
              shortname: "中堂镇"
            },
            {
              pinyin: "GaoBu",
              shortname: "高埗镇"
            },
            {
              pinyin: "SongShanHu",
              shortname: "松山湖管委会"
            },
            {
              pinyin: "DongGuanGang",
              shortname: "东莞港"
            },
            {
              pinyin: "DongGuanShengTaiYuan",
              shortname: "东莞生态园"
            }
          ],
          shortname: "东莞市"
        },
        {
          pinyin: "ZhongShan",
          children: [
            {
              pinyin: "ShiQi",
              shortname: "石岐区街道办事处"
            },
            {
              pinyin: "DongQuJieDaoBanShiChu",
              shortname: "东区街道办事处"
            },
            {
              pinyin: "HuoJuKaiFaQu",
              shortname: "火炬开发区街道办事处"
            },
            {
              pinyin: "XiQuJieDaoBanShiChu",
              shortname: "西区街道办事处"
            },
            {
              pinyin: "NanQuJieDaoBanShiChu",
              shortname: "南区街道办事处"
            },
            {
              pinyin: "WuGuiShan",
              shortname: "五桂山街道办事处"
            },
            {
              pinyin: "XiaoLan",
              shortname: "小榄镇"
            },
            {
              pinyin: "HuangPu",
              shortname: "黄圃镇"
            },
            {
              pinyin: "MinZhong",
              shortname: "民众镇"
            },
            {
              pinyin: "DongFeng",
              shortname: "东凤镇"
            },
            {
              pinyin: "DongSheng",
              shortname: "东升镇"
            },
            {
              pinyin: "GuZhen",
              shortname: "古镇镇"
            },
            {
              pinyin: "ShaXi",
              shortname: "沙溪镇"
            },
            {
              pinyin: "TanZhou",
              shortname: "坦洲镇"
            },
            {
              pinyin: "GangKou",
              shortname: "港口镇"
            },
            {
              pinyin: "SanJiao",
              shortname: "三角镇"
            },
            {
              pinyin: "HengLan",
              shortname: "横栏镇"
            },
            {
              pinyin: "NanTou",
              shortname: "南头镇"
            },
            {
              pinyin: "FuSha",
              shortname: "阜沙镇"
            },
            {
              pinyin: "NanLang",
              shortname: "南朗镇"
            },
            {
              pinyin: "SanXiang",
              shortname: "三乡镇"
            },
            {
              pinyin: "BanFu",
              shortname: "板芙镇"
            },
            {
              pinyin: "DaYong",
              shortname: "大涌镇"
            },
            {
              pinyin: "ShenWan",
              shortname: "神湾镇"
            }
          ],
          shortname: "中山市"
        },
        {
          pinyin: "ChaoZhou",
          children: [
            {
              pinyin: "ChaoZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "XiangQiao",
              shortname: "湘桥区"
            },
            {
              pinyin: "ChaoAn",
              shortname: "潮安区"
            },
            {
              pinyin: "RaoPing",
              shortname: "饶平县"
            }
          ],
          shortname: "潮州市"
        },
        {
          pinyin: "JieYang",
          children: [
            {
              pinyin: "JieYang",
              shortname: "市辖区"
            },
            {
              pinyin: "RongCheng",
              shortname: "榕城区"
            },
            {
              pinyin: "JieDong",
              shortname: "揭东区"
            },
            {
              pinyin: "JieXi",
              shortname: "揭西县"
            },
            {
              pinyin: "HuiLai",
              shortname: "惠来县"
            },
            {
              pinyin: "PuNing",
              shortname: "普宁市"
            }
          ],
          shortname: "揭阳市"
        },
        {
          pinyin: "YunFu",
          children: [
            {
              pinyin: "YunFu",
              shortname: "市辖区"
            },
            {
              pinyin: "YunCheng",
              shortname: "云城区"
            },
            {
              pinyin: "YunAn",
              shortname: "云安区"
            },
            {
              pinyin: "XinXing",
              shortname: "新兴县"
            },
            {
              pinyin: "YuNan",
              shortname: "郁南县"
            },
            {
              pinyin: "LuoDing",
              shortname: "罗定市"
            }
          ],
          shortname: "云浮市"
        }
      ],
      shortname: "广东省"
    },
    {
      pinyin: "GuangXi",
      children: [
        {
          pinyin: "NanNing",
          children: [
            {
              pinyin: "NanNing",
              shortname: "市辖区"
            },
            {
              pinyin: "XingNing",
              shortname: "兴宁区"
            },
            {
              pinyin: "QingXiu",
              shortname: "青秀区"
            },
            {
              pinyin: "JiangNan",
              shortname: "江南区"
            },
            {
              pinyin: "XiXiangTang",
              shortname: "西乡塘区"
            },
            {
              pinyin: "LiangQing",
              shortname: "良庆区"
            },
            {
              pinyin: "YongNing",
              shortname: "邕宁区"
            },
            {
              pinyin: "WuMing",
              shortname: "武鸣区"
            },
            {
              pinyin: "LongAn",
              shortname: "隆安县"
            },
            {
              pinyin: "MaShan",
              shortname: "马山县"
            },
            {
              pinyin: "ShangLin",
              shortname: "上林县"
            },
            {
              pinyin: "BinYang",
              shortname: "宾阳县"
            },
            {
              pinyin: "HengXian",
              shortname: "横县"
            }
          ],
          shortname: "南宁市"
        },
        {
          pinyin: "LiuZhou",
          children: [
            {
              pinyin: "LiuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengZhong",
              shortname: "城中区"
            },
            {
              pinyin: "YuFeng",
              shortname: "鱼峰区"
            },
            {
              pinyin: "LiuNan",
              shortname: "柳南区"
            },
            {
              pinyin: "LiuBei",
              shortname: "柳北区"
            },
            {
              pinyin: "LiuJiang",
              shortname: "柳江区"
            },
            {
              pinyin: "LiuCheng",
              shortname: "柳城县"
            },
            {
              pinyin: "LuZhai",
              shortname: "鹿寨县"
            },
            {
              pinyin: "RongAn",
              shortname: "融安县"
            },
            {
              pinyin: "RongShui",
              shortname: "融水苗族自治县"
            },
            {
              pinyin: "SanJiang",
              shortname: "三江侗族自治县"
            }
          ],
          shortname: "柳州市"
        },
        {
          pinyin: "GuiLin",
          children: [
            {
              pinyin: "GuiLin",
              shortname: "市辖区"
            },
            {
              pinyin: "XiuFeng",
              shortname: "秀峰区"
            },
            {
              pinyin: "DieCai",
              shortname: "叠彩区"
            },
            {
              pinyin: "XiangShan",
              shortname: "象山区"
            },
            {
              pinyin: "QiXing",
              shortname: "七星区"
            },
            {
              pinyin: "YanShan",
              shortname: "雁山区"
            },
            {
              pinyin: "LinGui",
              shortname: "临桂区"
            },
            {
              pinyin: "YangShuo",
              shortname: "阳朔县"
            },
            {
              pinyin: "LingChuan",
              shortname: "灵川县"
            },
            {
              pinyin: "QuanZhou",
              shortname: "全州县"
            },
            {
              pinyin: "XingAn",
              shortname: "兴安县"
            },
            {
              pinyin: "YongFu",
              shortname: "永福县"
            },
            {
              pinyin: "GuanYang",
              shortname: "灌阳县"
            },
            {
              pinyin: "LongShengGeZu",
              shortname: "龙胜各族自治县"
            },
            {
              pinyin: "ZiYuan",
              shortname: "资源县"
            },
            {
              pinyin: "PingLe",
              shortname: "平乐县"
            },
            {
              pinyin: "GongCheng",
              shortname: "恭城瑶族自治县"
            },
            {
              pinyin: "LiPu",
              shortname: "荔浦市"
            }
          ],
          shortname: "桂林市"
        },
        {
          pinyin: "WuZhou",
          children: [
            {
              pinyin: "WuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "WanXiu",
              shortname: "万秀区"
            },
            {
              pinyin: "ChangZhou",
              shortname: "长洲区"
            },
            {
              pinyin: "LongWei",
              shortname: "龙圩区"
            },
            {
              pinyin: "CangWu",
              shortname: "苍梧县"
            },
            {
              pinyin: "TengXian",
              shortname: "藤县"
            },
            {
              pinyin: "MengShan",
              shortname: "蒙山县"
            },
            {
              pinyin: "CenXi",
              shortname: "岑溪市"
            }
          ],
          shortname: "梧州市"
        },
        {
          pinyin: "BeiHai",
          children: [
            {
              pinyin: "BeiHai",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiCheng",
              shortname: "海城区"
            },
            {
              pinyin: "YinHai",
              shortname: "银海区"
            },
            {
              pinyin: "TieShanGang",
              shortname: "铁山港区"
            },
            {
              pinyin: "HePu",
              shortname: "合浦县"
            }
          ],
          shortname: "北海市"
        },
        {
          pinyin: "FangChengGang",
          children: [
            {
              pinyin: "FangChengGang",
              shortname: "市辖区"
            },
            {
              pinyin: "GangKou",
              shortname: "港口区"
            },
            {
              pinyin: "FangCheng",
              shortname: "防城区"
            },
            {
              pinyin: "ShangSi",
              shortname: "上思县"
            },
            {
              pinyin: "DongXing",
              shortname: "东兴市"
            }
          ],
          shortname: "防城港市"
        },
        {
          pinyin: "QinZhou",
          children: [
            {
              pinyin: "QinZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "QinNan",
              shortname: "钦南区"
            },
            {
              pinyin: "QinBei",
              shortname: "钦北区"
            },
            {
              pinyin: "LingShan",
              shortname: "灵山县"
            },
            {
              pinyin: "PuBei",
              shortname: "浦北县"
            }
          ],
          shortname: "钦州市"
        },
        {
          pinyin: "GuiGang",
          children: [
            {
              pinyin: "GuiGang",
              shortname: "市辖区"
            },
            {
              pinyin: "GangBei",
              shortname: "港北区"
            },
            {
              pinyin: "GangNan",
              shortname: "港南区"
            },
            {
              pinyin: "TanTang",
              shortname: "覃塘区"
            },
            {
              pinyin: "PingNan",
              shortname: "平南县"
            },
            {
              pinyin: "GuiPing",
              shortname: "桂平市"
            }
          ],
          shortname: "贵港市"
        },
        {
          pinyin: "YuLin",
          children: [
            {
              pinyin: "YuLin",
              shortname: "市辖区"
            },
            {
              pinyin: "YuZhou",
              shortname: "玉州区"
            },
            {
              pinyin: "FuMian",
              shortname: "福绵区"
            },
            {
              pinyin: "RongXian",
              shortname: "容县"
            },
            {
              pinyin: "LuChuan",
              shortname: "陆川县"
            },
            {
              pinyin: "BoBai",
              shortname: "博白县"
            },
            {
              pinyin: "XingYe",
              shortname: "兴业县"
            },
            {
              pinyin: "BeiLiu",
              shortname: "北流市"
            }
          ],
          shortname: "玉林市"
        },
        {
          pinyin: "BoSe",
          children: [
            {
              pinyin: "BoSe",
              shortname: "市辖区"
            },
            {
              pinyin: "YouJiang",
              shortname: "右江区"
            },
            {
              pinyin: "TianYang",
              shortname: "田阳县"
            },
            {
              pinyin: "TianDong",
              shortname: "田东县"
            },
            {
              pinyin: "PingGuo",
              shortname: "平果县"
            },
            {
              pinyin: "DeBao",
              shortname: "德保县"
            },
            {
              pinyin: "NaPo",
              shortname: "那坡县"
            },
            {
              pinyin: "LingYun",
              shortname: "凌云县"
            },
            {
              pinyin: "LeYe",
              shortname: "乐业县"
            },
            {
              pinyin: "TianLin",
              shortname: "田林县"
            },
            {
              pinyin: "XiLin",
              shortname: "西林县"
            },
            {
              pinyin: "LongLinGeZu",
              shortname: "隆林各族自治县"
            },
            {
              pinyin: "JingXi",
              shortname: "靖西市"
            }
          ],
          shortname: "百色市"
        },
        {
          pinyin: "HeZhou",
          children: [
            {
              pinyin: "HeZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "BaBu",
              shortname: "八步区"
            },
            {
              pinyin: "PingGui",
              shortname: "平桂区"
            },
            {
              pinyin: "ZhaoPing",
              shortname: "昭平县"
            },
            {
              pinyin: "ZhongShan",
              shortname: "钟山县"
            },
            {
              pinyin: "FuChuan",
              shortname: "富川瑶族自治县"
            }
          ],
          shortname: "贺州市"
        },
        {
          pinyin: "HeChi",
          children: [
            {
              pinyin: "HeChi",
              shortname: "市辖区"
            },
            {
              pinyin: "JinChengJiang",
              shortname: "金城江区"
            },
            {
              pinyin: "YiZhou",
              shortname: "宜州区"
            },
            {
              pinyin: "NanDan",
              shortname: "南丹县"
            },
            {
              pinyin: "TianE",
              shortname: "天峨县"
            },
            {
              pinyin: "FengShan",
              shortname: "凤山县"
            },
            {
              pinyin: "DongLan",
              shortname: "东兰县"
            },
            {
              pinyin: "LuoCheng",
              shortname: "罗城仫佬族自治县"
            },
            {
              pinyin: "HuanJiang",
              shortname: "环江毛南族自治县"
            },
            {
              pinyin: "BaMa",
              shortname: "巴马瑶族自治县"
            },
            {
              pinyin: "DouAn",
              shortname: "都安瑶族自治县"
            },
            {
              pinyin: "DaHua",
              shortname: "大化瑶族自治县"
            }
          ],
          shortname: "河池市"
        },
        {
          pinyin: "LaiBin",
          children: [
            {
              pinyin: "LaiBin",
              shortname: "市辖区"
            },
            {
              pinyin: "XingBin",
              shortname: "兴宾区"
            },
            {
              pinyin: "XinCheng",
              shortname: "忻城县"
            },
            {
              pinyin: "XiangZhou",
              shortname: "象州县"
            },
            {
              pinyin: "WuXuan",
              shortname: "武宣县"
            },
            {
              pinyin: "JinXiu",
              shortname: "金秀瑶族自治县"
            },
            {
              pinyin: "HeShan",
              shortname: "合山市"
            }
          ],
          shortname: "来宾市"
        },
        {
          pinyin: "ChongZuo",
          children: [
            {
              pinyin: "ChongZuo",
              shortname: "市辖区"
            },
            {
              pinyin: "JiangZhou",
              shortname: "江州区"
            },
            {
              pinyin: "FuSui",
              shortname: "扶绥县"
            },
            {
              pinyin: "NingMing",
              shortname: "宁明县"
            },
            {
              pinyin: "LongZhou",
              shortname: "龙州县"
            },
            {
              pinyin: "DaXin",
              shortname: "大新县"
            },
            {
              pinyin: "TianDeng",
              shortname: "天等县"
            },
            {
              pinyin: "PingXiang",
              shortname: "凭祥市"
            }
          ],
          shortname: "崇左市"
        }
      ],
      shortname: "广西壮族自治区"
    },
    {
      pinyin: "HaiNan",
      children: [
        {
          pinyin: "HaiKou",
          children: [
            {
              pinyin: "HaiKou",
              shortname: "市辖区"
            },
            {
              pinyin: "XiuYing",
              shortname: "秀英区"
            },
            {
              pinyin: "LongHua",
              shortname: "龙华区"
            },
            {
              pinyin: "QiongShan",
              shortname: "琼山区"
            },
            {
              pinyin: "MeiLan",
              shortname: "美兰区"
            }
          ],
          shortname: "海口市"
        },
        {
          pinyin: "SanYa",
          children: [
            {
              pinyin: "SanYa",
              shortname: "市辖区"
            },
            {
              pinyin: "HaiTang",
              shortname: "海棠区"
            },
            {
              pinyin: "JiYang",
              shortname: "吉阳区"
            },
            {
              pinyin: "TianYa",
              shortname: "天涯区"
            },
            {
              pinyin: "YaZhou",
              shortname: "崖州区"
            }
          ],
          shortname: "三亚市"
        },
        {
          pinyin: "SanSha",
          children: [
            {
              pinyin: "XiSha",
              shortname: "西沙群岛"
            },
            {
              pinyin: "NanSha",
              shortname: "南沙群岛"
            },
            {
              pinyin: "ZhongSha",
              shortname: "中沙群岛的岛礁及其海域"
            }
          ],
          shortname: "三沙市"
        },
        {
          pinyin: "DanZhou",
          children: [
            {
              pinyin: "NaDa",
              shortname: "那大镇"
            },
            {
              pinyin: "HeQing",
              shortname: "和庆镇"
            },
            {
              pinyin: "NanFeng",
              shortname: "南丰镇"
            },
            {
              pinyin: "DaCheng",
              shortname: "大成镇"
            },
            {
              pinyin: "YaXing",
              shortname: "雅星镇"
            },
            {
              pinyin: "LanYang",
              shortname: "兰洋镇"
            },
            {
              pinyin: "GuangCun",
              shortname: "光村镇"
            },
            {
              pinyin: "MuTang",
              shortname: "木棠镇"
            },
            {
              pinyin: "HaiTou",
              shortname: "海头镇"
            },
            {
              pinyin: "EMan",
              shortname: "峨蔓镇"
            },
            {
              pinyin: "WangWu",
              shortname: "王五镇"
            },
            {
              pinyin: "BaiMa",
              shortname: "白马井镇"
            },
            {
              pinyin: "ZhongHe",
              shortname: "中和镇"
            },
            {
              pinyin: "PaiPu",
              shortname: "排浦镇"
            },
            {
              pinyin: "DongCheng",
              shortname: "东成镇"
            },
            {
              pinyin: "XinZhou",
              shortname: "新州镇"
            },
            {
              pinyin: "YangPuJingKaiQu",
              shortname: "洋浦经济开发区"
            },
            {
              pinyin: "HuaNanReZuoXueYuan",
              shortname: "华南热作学院"
            }
          ],
          shortname: "儋州市"
        },
        {
          pinyin: "HaiNan",
          children: [
            {
              pinyin: "WuZhiShan",
              shortname: "五指山市"
            },
            {
              pinyin: "QiongHai",
              shortname: "琼海市"
            },
            {
              pinyin: "WenChang",
              shortname: "文昌市"
            },
            {
              pinyin: "WanNing",
              shortname: "万宁市"
            },
            {
              pinyin: "DongFang",
              shortname: "东方市"
            },
            {
              pinyin: "DingAn",
              shortname: "定安县"
            },
            {
              pinyin: "TunChang",
              shortname: "屯昌县"
            },
            {
              pinyin: "ChengMai",
              shortname: "澄迈县"
            },
            {
              pinyin: "LinGao",
              shortname: "临高县"
            },
            {
              pinyin: "BaiSha",
              shortname: "白沙黎族自治县"
            },
            {
              pinyin: "ChangJiang",
              shortname: "昌江黎族自治县"
            },
            {
              pinyin: "LeDong",
              shortname: "乐东黎族自治县"
            },
            {
              pinyin: "LingShui",
              shortname: "陵水黎族自治县"
            },
            {
              pinyin: "BaoTing",
              shortname: "保亭黎族苗族自治县"
            },
            {
              pinyin: "QiongZhong",
              shortname: "琼中黎族苗族自治县"
            }
          ],
          shortname: "直辖县"
        }
      ],
      shortname: "海南省"
    },
    {
      pinyin: "ChongQing",
      children: [
        {
          pinyin: "ChongQing",
          children: [
            {
              pinyin: "WanZhou",
              shortname: "万州区"
            },
            {
              pinyin: "FuLing",
              shortname: "涪陵区"
            },
            {
              pinyin: "YuZhong",
              shortname: "渝中区"
            },
            {
              pinyin: "DaDuKou",
              shortname: "大渡口区"
            },
            {
              pinyin: "JiangBei",
              shortname: "江北区"
            },
            {
              pinyin: "ShaPingBa",
              shortname: "沙坪坝区"
            },
            {
              pinyin: "JiuLongPo",
              shortname: "九龙坡区"
            },
            {
              pinyin: "NanAn",
              shortname: "南岸区"
            },
            {
              pinyin: "BeiBei",
              shortname: "北碚区"
            },
            {
              pinyin: "QiJiang",
              shortname: "綦江区"
            },
            {
              pinyin: "DaZu",
              shortname: "大足区"
            },
            {
              pinyin: "YuBei",
              shortname: "渝北区"
            },
            {
              pinyin: "BaNan",
              shortname: "巴南区"
            },
            {
              pinyin: "QianJiang",
              shortname: "黔江区"
            },
            {
              pinyin: "ChangShou",
              shortname: "长寿区"
            },
            {
              pinyin: "JiangJin",
              shortname: "江津区"
            },
            {
              pinyin: "HeChuan",
              shortname: "合川区"
            },
            {
              pinyin: "YongChuan",
              shortname: "永川区"
            },
            {
              pinyin: "NanChuan",
              shortname: "南川区"
            },
            {
              pinyin: "BiShan",
              shortname: "璧山区"
            },
            {
              pinyin: "TongLiang",
              shortname: "铜梁区"
            },
            {
              pinyin: "TongNan",
              shortname: "潼南区"
            },
            {
              pinyin: "RongChang",
              shortname: "荣昌区"
            },
            {
              pinyin: "KaiZhou",
              shortname: "开州区"
            },
            {
              pinyin: "LiangPing",
              shortname: "梁平区"
            },
            {
              pinyin: "WuLong",
              shortname: "武隆区"
            }
          ],
          shortname: "直辖区"
        },
        {
          pinyin: "ChongQing",
          children: [
            {
              pinyin: "ChengKou",
              shortname: "城口县"
            },
            {
              pinyin: "FengDu",
              shortname: "丰都县"
            },
            {
              pinyin: "DianJiang",
              shortname: "垫江县"
            },
            {
              pinyin: "ZhongXian",
              shortname: "忠县"
            },
            {
              pinyin: "YunYang",
              shortname: "云阳县"
            },
            {
              pinyin: "FengJie",
              shortname: "奉节县"
            },
            {
              pinyin: "WuShan",
              shortname: "巫山县"
            },
            {
              pinyin: "WuXi",
              shortname: "巫溪县"
            },
            {
              pinyin: "ShiZhu",
              shortname: "石柱土家族自治县"
            },
            {
              pinyin: "XiuShan",
              shortname: "秀山土家族苗族自治县"
            },
            {
              pinyin: "YouYang",
              shortname: "酉阳土家族苗族自治县"
            },
            {
              pinyin: "PengShui",
              shortname: "彭水苗族土家族自治县"
            }
          ],
          shortname: "直辖县"
        }
      ],
      shortname: "重庆市"
    },
    {
      pinyin: "SiChuan",
      children: [
        {
          pinyin: "ChengDu",
          children: [
            {
              pinyin: "ChengDu",
              shortname: "市辖区"
            },
            {
              pinyin: "JinJiang",
              shortname: "锦江区"
            },
            {
              pinyin: "QingYang",
              shortname: "青羊区"
            },
            {
              pinyin: "JinNiu",
              shortname: "金牛区"
            },
            {
              pinyin: "WuHou",
              shortname: "武侯区"
            },
            {
              pinyin: "ChengHua",
              shortname: "成华区"
            },
            {
              pinyin: "LongQuanYi",
              shortname: "龙泉驿区"
            },
            {
              pinyin: "QingBaiJiang",
              shortname: "青白江区"
            },
            {
              pinyin: "XinDu",
              shortname: "新都区"
            },
            {
              pinyin: "WenJiang",
              shortname: "温江区"
            },
            {
              pinyin: "ShuangLiu",
              shortname: "双流区"
            },
            {
              pinyin: "PiDou",
              shortname: "郫都区"
            },
            {
              pinyin: "JinTang",
              shortname: "金堂县"
            },
            {
              pinyin: "DaYi",
              shortname: "大邑县"
            },
            {
              pinyin: "PuJiang",
              shortname: "蒲江县"
            },
            {
              pinyin: "XinJin",
              shortname: "新津县"
            },
            {
              pinyin: "DuJiangYan",
              shortname: "都江堰市"
            },
            {
              pinyin: "PengZhou",
              shortname: "彭州市"
            },
            {
              pinyin: "QiongLai",
              shortname: "邛崃市"
            },
            {
              pinyin: "ChongZhou",
              shortname: "崇州市"
            },
            {
              pinyin: "JianYang",
              shortname: "简阳市"
            }
          ],
          shortname: "成都市"
        },
        {
          pinyin: "ZiGong",
          children: [
            {
              pinyin: "ZiGong",
              shortname: "市辖区"
            },
            {
              pinyin: "ZiLiuJing",
              shortname: "自流井区"
            },
            {
              pinyin: "GongJing",
              shortname: "贡井区"
            },
            {
              pinyin: "DaAn",
              shortname: "大安区"
            },
            {
              pinyin: "YanTan",
              shortname: "沿滩区"
            },
            {
              pinyin: "RongXian",
              shortname: "荣县"
            },
            {
              pinyin: "FuShun",
              shortname: "富顺县"
            }
          ],
          shortname: "自贡市"
        },
        {
          pinyin: "PanZhiHua",
          children: [
            {
              pinyin: "PanZhiHua",
              shortname: "市辖区"
            },
            {
              pinyin: "DongQu",
              shortname: "东区"
            },
            {
              pinyin: "XiQu",
              shortname: "西区"
            },
            {
              pinyin: "RenHe",
              shortname: "仁和区"
            },
            {
              pinyin: "MiYi",
              shortname: "米易县"
            },
            {
              pinyin: "YanBian",
              shortname: "盐边县"
            }
          ],
          shortname: "攀枝花市"
        },
        {
          pinyin: "LuZhou",
          children: [
            {
              pinyin: "LuZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "JiangYang",
              shortname: "江阳区"
            },
            {
              pinyin: "NaXi",
              shortname: "纳溪区"
            },
            {
              pinyin: "LongMaTan",
              shortname: "龙马潭区"
            },
            {
              pinyin: "LuXian",
              shortname: "泸县"
            },
            {
              pinyin: "HeJiang",
              shortname: "合江县"
            },
            {
              pinyin: "XuYong",
              shortname: "叙永县"
            },
            {
              pinyin: "GuLin",
              shortname: "古蔺县"
            }
          ],
          shortname: "泸州市"
        },
        {
          pinyin: "DeYang",
          children: [
            {
              pinyin: "DeYang",
              shortname: "市辖区"
            },
            {
              pinyin: "JingYang",
              shortname: "旌阳区"
            },
            {
              pinyin: "LuoJiang",
              shortname: "罗江区"
            },
            {
              pinyin: "ZhongJiang",
              shortname: "中江县"
            },
            {
              pinyin: "GuangHan",
              shortname: "广汉市"
            },
            {
              pinyin: "ShiFang",
              shortname: "什邡市"
            },
            {
              pinyin: "MianZhu",
              shortname: "绵竹市"
            }
          ],
          shortname: "德阳市"
        },
        {
          pinyin: "MianYang",
          children: [
            {
              pinyin: "MianYang",
              shortname: "市辖区"
            },
            {
              pinyin: "FuCheng",
              shortname: "涪城区"
            },
            {
              pinyin: "YouXian",
              shortname: "游仙区"
            },
            {
              pinyin: "AnZhou",
              shortname: "安州区"
            },
            {
              pinyin: "SanTai",
              shortname: "三台县"
            },
            {
              pinyin: "YanTing",
              shortname: "盐亭县"
            },
            {
              pinyin: "ZiTong",
              shortname: "梓潼县"
            },
            {
              pinyin: "BeiChuan",
              shortname: "北川羌族自治县"
            },
            {
              pinyin: "PingWu",
              shortname: "平武县"
            },
            {
              pinyin: "JiangYou",
              shortname: "江油市"
            }
          ],
          shortname: "绵阳市"
        },
        {
          pinyin: "GuangYuan",
          children: [
            {
              pinyin: "GuangYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "LiZhou",
              shortname: "利州区"
            },
            {
              pinyin: "ZhaoHua",
              shortname: "昭化区"
            },
            {
              pinyin: "ChaoTian",
              shortname: "朝天区"
            },
            {
              pinyin: "WangCang",
              shortname: "旺苍县"
            },
            {
              pinyin: "QingChuan",
              shortname: "青川县"
            },
            {
              pinyin: "JianGe",
              shortname: "剑阁县"
            },
            {
              pinyin: "CangXi",
              shortname: "苍溪县"
            }
          ],
          shortname: "广元市"
        },
        {
          pinyin: "SuiNing",
          children: [
            {
              pinyin: "SuiNing",
              shortname: "市辖区"
            },
            {
              pinyin: "ChuanShan",
              shortname: "船山区"
            },
            {
              pinyin: "AnJu",
              shortname: "安居区"
            },
            {
              pinyin: "PengXi",
              shortname: "蓬溪县"
            },
            {
              pinyin: "SheHong",
              shortname: "射洪县"
            },
            {
              pinyin: "DaYing",
              shortname: "大英县"
            }
          ],
          shortname: "遂宁市"
        },
        {
          pinyin: "NeiJiang",
          children: [
            {
              pinyin: "NeiJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "ShiZhong",
              shortname: "市中区"
            },
            {
              pinyin: "DongXing",
              shortname: "东兴区"
            },
            {
              pinyin: "WeiYuan",
              shortname: "威远县"
            },
            {
              pinyin: "ZiZhong",
              shortname: "资中县"
            },
            {
              pinyin: "NeiJiangJingKaiQu",
              shortname: "内江经济开发区"
            },
            {
              pinyin: "LongChang",
              shortname: "隆昌市"
            }
          ],
          shortname: "内江市"
        },
        {
          pinyin: "LeShan",
          children: [
            {
              pinyin: "LeShan",
              shortname: "市辖区"
            },
            {
              pinyin: "ShiZhong",
              shortname: "市中区"
            },
            {
              pinyin: "ShaWan",
              shortname: "沙湾区"
            },
            {
              pinyin: "WuTongQiao",
              shortname: "五通桥区"
            },
            {
              pinyin: "JinKouHe",
              shortname: "金口河区"
            },
            {
              pinyin: "QianWei",
              shortname: "犍为县"
            },
            {
              pinyin: "JingYan",
              shortname: "井研县"
            },
            {
              pinyin: "JiaJiang",
              shortname: "夹江县"
            },
            {
              pinyin: "MuChuan",
              shortname: "沐川县"
            },
            {
              pinyin: "EBian",
              shortname: "峨边彝族自治县"
            },
            {
              pinyin: "MaBian",
              shortname: "马边彝族自治县"
            },
            {
              pinyin: "EMeiShan",
              shortname: "峨眉山市"
            }
          ],
          shortname: "乐山市"
        },
        {
          pinyin: "NanChong",
          children: [
            {
              pinyin: "NanChong",
              shortname: "市辖区"
            },
            {
              pinyin: "ShunQing",
              shortname: "顺庆区"
            },
            {
              pinyin: "GaoPing",
              shortname: "高坪区"
            },
            {
              pinyin: "JiaLing",
              shortname: "嘉陵区"
            },
            {
              pinyin: "NanBu",
              shortname: "南部县"
            },
            {
              pinyin: "YingShan",
              shortname: "营山县"
            },
            {
              pinyin: "PengAn",
              shortname: "蓬安县"
            },
            {
              pinyin: "YiLong",
              shortname: "仪陇县"
            },
            {
              pinyin: "XiChong",
              shortname: "西充县"
            },
            {
              pinyin: "LangZhong",
              shortname: "阆中市"
            }
          ],
          shortname: "南充市"
        },
        {
          pinyin: "MeiShan",
          children: [
            {
              pinyin: "MeiShan",
              shortname: "市辖区"
            },
            {
              pinyin: "DongPo",
              shortname: "东坡区"
            },
            {
              pinyin: "PengShan",
              shortname: "彭山区"
            },
            {
              pinyin: "RenShou",
              shortname: "仁寿县"
            },
            {
              pinyin: "HongYa",
              shortname: "洪雅县"
            },
            {
              pinyin: "DanLeng",
              shortname: "丹棱县"
            },
            {
              pinyin: "QingShen",
              shortname: "青神县"
            }
          ],
          shortname: "眉山市"
        },
        {
          pinyin: "YiBin",
          children: [
            {
              pinyin: "YiBin",
              shortname: "市辖区"
            },
            {
              pinyin: "CuiPing",
              shortname: "翠屏区"
            },
            {
              pinyin: "NanXi",
              shortname: "南溪区"
            },
            {
              pinyin: "XuZhou",
              shortname: "叙州区"
            },
            {
              pinyin: "JiangAn",
              shortname: "江安县"
            },
            {
              pinyin: "ChangNing",
              shortname: "长宁县"
            },
            {
              pinyin: "GaoXian",
              shortname: "高县"
            },
            {
              pinyin: "GongXian",
              shortname: "珙县"
            },
            {
              pinyin: "JunLian",
              shortname: "筠连县"
            },
            {
              pinyin: "XingWen",
              shortname: "兴文县"
            },
            {
              pinyin: "PingShan",
              shortname: "屏山县"
            }
          ],
          shortname: "宜宾市"
        },
        {
          pinyin: "GuangAn",
          children: [
            {
              pinyin: "GuangAn",
              shortname: "市辖区"
            },
            {
              pinyin: "GuangAn",
              shortname: "广安区"
            },
            {
              pinyin: "QianFeng",
              shortname: "前锋区"
            },
            {
              pinyin: "YueChi",
              shortname: "岳池县"
            },
            {
              pinyin: "WuSheng",
              shortname: "武胜县"
            },
            {
              pinyin: "LinShui",
              shortname: "邻水县"
            },
            {
              pinyin: "HuaYing",
              shortname: "华蓥市"
            }
          ],
          shortname: "广安市"
        },
        {
          pinyin: "DaZhou",
          children: [
            {
              pinyin: "DaZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "TongChuan",
              shortname: "通川区"
            },
            {
              pinyin: "DaChuan",
              shortname: "达川区"
            },
            {
              pinyin: "XuanHan",
              shortname: "宣汉县"
            },
            {
              pinyin: "KaiJiang",
              shortname: "开江县"
            },
            {
              pinyin: "DaZhu",
              shortname: "大竹县"
            },
            {
              pinyin: "QuXian",
              shortname: "渠县"
            },
            {
              pinyin: "DaZhouJingKaiQu",
              shortname: "达州经济开发区"
            },
            {
              pinyin: "WanYuan",
              shortname: "万源市"
            }
          ],
          shortname: "达州市"
        },
        {
          pinyin: "YaAn",
          children: [
            {
              pinyin: "YaAn",
              shortname: "市辖区"
            },
            {
              pinyin: "YuCheng",
              shortname: "雨城区"
            },
            {
              pinyin: "MingShan",
              shortname: "名山区"
            },
            {
              pinyin: "YingJing",
              shortname: "荥经县"
            },
            {
              pinyin: "HanYuan",
              shortname: "汉源县"
            },
            {
              pinyin: "ShiMian",
              shortname: "石棉县"
            },
            {
              pinyin: "TianQuan",
              shortname: "天全县"
            },
            {
              pinyin: "LuShan",
              shortname: "芦山县"
            },
            {
              pinyin: "BaoXing",
              shortname: "宝兴县"
            }
          ],
          shortname: "雅安市"
        },
        {
          pinyin: "BaZhong",
          children: [
            {
              pinyin: "BaZhong",
              shortname: "市辖区"
            },
            {
              pinyin: "BaZhou",
              shortname: "巴州区"
            },
            {
              pinyin: "EnYang",
              shortname: "恩阳区"
            },
            {
              pinyin: "TongJiang",
              shortname: "通江县"
            },
            {
              pinyin: "NanJiang",
              shortname: "南江县"
            },
            {
              pinyin: "PingChang",
              shortname: "平昌县"
            },
            {
              pinyin: "BaZhongJingKaiQu",
              shortname: "巴中经济开发区"
            }
          ],
          shortname: "巴中市"
        },
        {
          pinyin: "ZiYang",
          children: [
            {
              pinyin: "ZiYang",
              shortname: "市辖区"
            },
            {
              pinyin: "YanJiang",
              shortname: "雁江区"
            },
            {
              pinyin: "AnYue",
              shortname: "安岳县"
            },
            {
              pinyin: "LeZhi",
              shortname: "乐至县"
            }
          ],
          shortname: "资阳市"
        },
        {
          pinyin: "ABa",
          children: [
            {
              pinyin: "MaErKang",
              shortname: "马尔康市"
            },
            {
              pinyin: "WenChuan",
              shortname: "汶川县"
            },
            {
              pinyin: "LiXian",
              shortname: "理县"
            },
            {
              pinyin: "MaoXian",
              shortname: "茂县"
            },
            {
              pinyin: "SongPan",
              shortname: "松潘县"
            },
            {
              pinyin: "JiuZhaiGou",
              shortname: "九寨沟县"
            },
            {
              pinyin: "JinChuan",
              shortname: "金川县"
            },
            {
              pinyin: "XiaoJin",
              shortname: "小金县"
            },
            {
              pinyin: "HeiShui",
              shortname: "黑水县"
            },
            {
              pinyin: "RangTang",
              shortname: "壤塘县"
            },
            {
              pinyin: "ABa",
              shortname: "阿坝县"
            },
            {
              pinyin: "RuoErGai",
              shortname: "若尔盖县"
            },
            {
              pinyin: "HongYuan",
              shortname: "红原县"
            }
          ],
          shortname: "阿坝藏族羌族自治州"
        },
        {
          pinyin: "GanZi",
          children: [
            {
              pinyin: "KangDing",
              shortname: "康定市"
            },
            {
              pinyin: "LuDing",
              shortname: "泸定县"
            },
            {
              pinyin: "DanBa",
              shortname: "丹巴县"
            },
            {
              pinyin: "JiuLong",
              shortname: "九龙县"
            },
            {
              pinyin: "YaJiang",
              shortname: "雅江县"
            },
            {
              pinyin: "DaoFu",
              shortname: "道孚县"
            },
            {
              pinyin: "LuHuo",
              shortname: "炉霍县"
            },
            {
              pinyin: "GanZi",
              shortname: "甘孜县"
            },
            {
              pinyin: "XinLong",
              shortname: "新龙县"
            },
            {
              pinyin: "DeGe",
              shortname: "德格县"
            },
            {
              pinyin: "BaiYu",
              shortname: "白玉县"
            },
            {
              pinyin: "ShiQu",
              shortname: "石渠县"
            },
            {
              pinyin: "SeDa",
              shortname: "色达县"
            },
            {
              pinyin: "LiTang",
              shortname: "理塘县"
            },
            {
              pinyin: "BaTang",
              shortname: "巴塘县"
            },
            {
              pinyin: "XiangCheng",
              shortname: "乡城县"
            },
            {
              pinyin: "DaoCheng",
              shortname: "稻城县"
            },
            {
              pinyin: "DeRong",
              shortname: "得荣县"
            }
          ],
          shortname: "甘孜藏族自治州"
        },
        {
          pinyin: "LiangShan",
          children: [
            {
              pinyin: "XiChang",
              shortname: "西昌市"
            },
            {
              pinyin: "MuLi",
              shortname: "木里藏族自治县"
            },
            {
              pinyin: "YanYuan",
              shortname: "盐源县"
            },
            {
              pinyin: "DeChang",
              shortname: "德昌县"
            },
            {
              pinyin: "HuiLi",
              shortname: "会理县"
            },
            {
              pinyin: "HuiDong",
              shortname: "会东县"
            },
            {
              pinyin: "NingNan",
              shortname: "宁南县"
            },
            {
              pinyin: "PuGe",
              shortname: "普格县"
            },
            {
              pinyin: "BuTuo",
              shortname: "布拖县"
            },
            {
              pinyin: "JinYang",
              shortname: "金阳县"
            },
            {
              pinyin: "ZhaoJue",
              shortname: "昭觉县"
            },
            {
              pinyin: "XiDe",
              shortname: "喜德县"
            },
            {
              pinyin: "MianNing",
              shortname: "冕宁县"
            },
            {
              pinyin: "YueXi",
              shortname: "越西县"
            },
            {
              pinyin: "GanLuo",
              shortname: "甘洛县"
            },
            {
              pinyin: "MeiGu",
              shortname: "美姑县"
            },
            {
              pinyin: "LeiBo",
              shortname: "雷波县"
            }
          ],
          shortname: "凉山彝族自治州"
        }
      ],
      shortname: "四川省"
    },
    {
      pinyin: "GuiZhou",
      children: [
        {
          pinyin: "GuiYang",
          children: [
            {
              pinyin: "GuiYang",
              shortname: "市辖区"
            },
            {
              pinyin: "NanMing",
              shortname: "南明区"
            },
            {
              pinyin: "YunYan",
              shortname: "云岩区"
            },
            {
              pinyin: "HuaXi",
              shortname: "花溪区"
            },
            {
              pinyin: "WuDang",
              shortname: "乌当区"
            },
            {
              pinyin: "BaiYun",
              shortname: "白云区"
            },
            {
              pinyin: "GuanShanHu",
              shortname: "观山湖区"
            },
            {
              pinyin: "KaiYang",
              shortname: "开阳县"
            },
            {
              pinyin: "XiFeng",
              shortname: "息烽县"
            },
            {
              pinyin: "XiuWen",
              shortname: "修文县"
            },
            {
              pinyin: "QingZhen",
              shortname: "清镇市"
            }
          ],
          shortname: "贵阳市"
        },
        {
          pinyin: "LiuPanShui",
          children: [
            {
              pinyin: "ZhongShan",
              shortname: "钟山区"
            },
            {
              pinyin: "LiuZhiTe",
              shortname: "六枝特区"
            },
            {
              pinyin: "ShuiCheng",
              shortname: "水城县"
            },
            {
              pinyin: "PanZhou",
              shortname: "盘州市"
            }
          ],
          shortname: "六盘水市"
        },
        {
          pinyin: "ZunYi",
          children: [
            {
              pinyin: "ZunYi",
              shortname: "市辖区"
            },
            {
              pinyin: "HongHuaGang",
              shortname: "红花岗区"
            },
            {
              pinyin: "HuiChuan",
              shortname: "汇川区"
            },
            {
              pinyin: "BoZhou",
              shortname: "播州区"
            },
            {
              pinyin: "TongZi",
              shortname: "桐梓县"
            },
            {
              pinyin: "SuiYang",
              shortname: "绥阳县"
            },
            {
              pinyin: "ZhengAn",
              shortname: "正安县"
            },
            {
              pinyin: "DaoZhen",
              shortname: "道真仡佬族苗族自治县"
            },
            {
              pinyin: "WuChuan",
              shortname: "务川仡佬族苗族自治县"
            },
            {
              pinyin: "FengGang",
              shortname: "凤冈县"
            },
            {
              pinyin: "MeiTan",
              shortname: "湄潭县"
            },
            {
              pinyin: "YuQing",
              shortname: "余庆县"
            },
            {
              pinyin: "XiShui",
              shortname: "习水县"
            },
            {
              pinyin: "ChiShui",
              shortname: "赤水市"
            },
            {
              pinyin: "RenHuai",
              shortname: "仁怀市"
            }
          ],
          shortname: "遵义市"
        },
        {
          pinyin: "AnShun",
          children: [
            {
              pinyin: "AnShun",
              shortname: "市辖区"
            },
            {
              pinyin: "XiXiu",
              shortname: "西秀区"
            },
            {
              pinyin: "PingBa",
              shortname: "平坝区"
            },
            {
              pinyin: "PuDing",
              shortname: "普定县"
            },
            {
              pinyin: "ZhenNing",
              shortname: "镇宁布依族苗族自治县"
            },
            {
              pinyin: "GuanLing",
              shortname: "关岭布依族苗族自治县"
            },
            {
              pinyin: "ZiYun",
              shortname: "紫云苗族布依族自治县"
            }
          ],
          shortname: "安顺市"
        },
        {
          pinyin: "BiJie",
          children: [
            {
              pinyin: "BiJie",
              shortname: "市辖区"
            },
            {
              pinyin: "QiXingGuan",
              shortname: "七星关区"
            },
            {
              pinyin: "DaFang",
              shortname: "大方县"
            },
            {
              pinyin: "QianXi",
              shortname: "黔西县"
            },
            {
              pinyin: "JinSha",
              shortname: "金沙县"
            },
            {
              pinyin: "ZhiJin",
              shortname: "织金县"
            },
            {
              pinyin: "NaYong",
              shortname: "纳雍县"
            },
            {
              pinyin: "WeiNing",
              shortname: "威宁彝族回族苗族自治县"
            },
            {
              pinyin: "HeZhang",
              shortname: "赫章县"
            }
          ],
          shortname: "毕节市"
        },
        {
          pinyin: "TongRen",
          children: [
            {
              pinyin: "TongRen",
              shortname: "市辖区"
            },
            {
              pinyin: "BiJiang",
              shortname: "碧江区"
            },
            {
              pinyin: "WanShan",
              shortname: "万山区"
            },
            {
              pinyin: "JiangKou",
              shortname: "江口县"
            },
            {
              pinyin: "YuPing",
              shortname: "玉屏侗族自治县"
            },
            {
              pinyin: "ShiQian",
              shortname: "石阡县"
            },
            {
              pinyin: "SiNan",
              shortname: "思南县"
            },
            {
              pinyin: "YinJiang",
              shortname: "印江土家族苗族自治县"
            },
            {
              pinyin: "DeJiang",
              shortname: "德江县"
            },
            {
              pinyin: "YanHe",
              shortname: "沿河土家族自治县"
            },
            {
              pinyin: "SongTao",
              shortname: "松桃苗族自治县"
            }
          ],
          shortname: "铜仁市"
        },
        {
          pinyin: "QianXiNan",
          children: [
            {
              pinyin: "XingYi",
              shortname: "兴义市"
            },
            {
              pinyin: "XingRen",
              shortname: "兴仁市"
            },
            {
              pinyin: "PuAn",
              shortname: "普安县"
            },
            {
              pinyin: "QingLong",
              shortname: "晴隆县"
            },
            {
              pinyin: "ZhenFeng",
              shortname: "贞丰县"
            },
            {
              pinyin: "WangMo",
              shortname: "望谟县"
            },
            {
              pinyin: "CeHeng",
              shortname: "册亨县"
            },
            {
              pinyin: "AnLong",
              shortname: "安龙县"
            }
          ],
          shortname: "黔西南布依族苗族自治州"
        },
        {
          pinyin: "QianDongNan",
          children: [
            {
              pinyin: "KaiLi",
              shortname: "凯里市"
            },
            {
              pinyin: "HuangPing",
              shortname: "黄平县"
            },
            {
              pinyin: "ShiBing",
              shortname: "施秉县"
            },
            {
              pinyin: "SanSui",
              shortname: "三穗县"
            },
            {
              pinyin: "ZhenYuan",
              shortname: "镇远县"
            },
            {
              pinyin: "CenGong",
              shortname: "岑巩县"
            },
            {
              pinyin: "TianZhu",
              shortname: "天柱县"
            },
            {
              pinyin: "JinPing",
              shortname: "锦屏县"
            },
            {
              pinyin: "JianHe",
              shortname: "剑河县"
            },
            {
              pinyin: "TaiJiang",
              shortname: "台江县"
            },
            {
              pinyin: "LiPing",
              shortname: "黎平县"
            },
            {
              pinyin: "RongJiang",
              shortname: "榕江县"
            },
            {
              pinyin: "CongJiang",
              shortname: "从江县"
            },
            {
              pinyin: "LeiShan",
              shortname: "雷山县"
            },
            {
              pinyin: "MaJiang",
              shortname: "麻江县"
            },
            {
              pinyin: "DanZhai",
              shortname: "丹寨县"
            }
          ],
          shortname: "黔东南苗族侗族自治州"
        },
        {
          pinyin: "QianNan",
          children: [
            {
              pinyin: "DuYun",
              shortname: "都匀市"
            },
            {
              pinyin: "FuQuan",
              shortname: "福泉市"
            },
            {
              pinyin: "LiBo",
              shortname: "荔波县"
            },
            {
              pinyin: "GuiDing",
              shortname: "贵定县"
            },
            {
              pinyin: "WengAn",
              shortname: "瓮安县"
            },
            {
              pinyin: "DuShan",
              shortname: "独山县"
            },
            {
              pinyin: "PingTang",
              shortname: "平塘县"
            },
            {
              pinyin: "LuoDian",
              shortname: "罗甸县"
            },
            {
              pinyin: "ChangShun",
              shortname: "长顺县"
            },
            {
              pinyin: "LongLi",
              shortname: "龙里县"
            },
            {
              pinyin: "HuiShui",
              shortname: "惠水县"
            },
            {
              pinyin: "SanDou",
              shortname: "三都水族自治县"
            }
          ],
          shortname: "黔南布依族苗族自治州"
        }
      ],
      shortname: "贵州省"
    },
    {
      pinyin: "YunNan",
      children: [
        {
          pinyin: "KunMing",
          children: [
            {
              pinyin: "KunMing",
              shortname: "市辖区"
            },
            {
              pinyin: "WuHua",
              shortname: "五华区"
            },
            {
              pinyin: "PanLong",
              shortname: "盘龙区"
            },
            {
              pinyin: "GuanDu",
              shortname: "官渡区"
            },
            {
              pinyin: "XiShan",
              shortname: "西山区"
            },
            {
              pinyin: "DongChuan",
              shortname: "东川区"
            },
            {
              pinyin: "ChengGong",
              shortname: "呈贡区"
            },
            {
              pinyin: "JinNing",
              shortname: "晋宁区"
            },
            {
              pinyin: "FuMin",
              shortname: "富民县"
            },
            {
              pinyin: "YiLiang",
              shortname: "宜良县"
            },
            {
              pinyin: "ShiLin",
              shortname: "石林彝族自治县"
            },
            {
              pinyin: "SongMing",
              shortname: "嵩明县"
            },
            {
              pinyin: "LuQuan",
              shortname: "禄劝彝族苗族自治县"
            },
            {
              pinyin: "XunDian",
              shortname: "寻甸回族彝族自治县"
            },
            {
              pinyin: "AnNing",
              shortname: "安宁市"
            }
          ],
          shortname: "昆明市"
        },
        {
          pinyin: "QuJing",
          children: [
            {
              pinyin: "QuJing",
              shortname: "市辖区"
            },
            {
              pinyin: "QiLin",
              shortname: "麒麟区"
            },
            {
              pinyin: "ZhanYi",
              shortname: "沾益区"
            },
            {
              pinyin: "MaLong",
              shortname: "马龙区"
            },
            {
              pinyin: "LuLiang",
              shortname: "陆良县"
            },
            {
              pinyin: "ShiZong",
              shortname: "师宗县"
            },
            {
              pinyin: "LuoPing",
              shortname: "罗平县"
            },
            {
              pinyin: "FuYuan",
              shortname: "富源县"
            },
            {
              pinyin: "HuiZe",
              shortname: "会泽县"
            },
            {
              pinyin: "XuanWei",
              shortname: "宣威市"
            }
          ],
          shortname: "曲靖市"
        },
        {
          pinyin: "YuXi",
          children: [
            {
              pinyin: "YuXi",
              shortname: "市辖区"
            },
            {
              pinyin: "HongTa",
              shortname: "红塔区"
            },
            {
              pinyin: "JiangChuan",
              shortname: "江川区"
            },
            {
              pinyin: "ChengJiang",
              shortname: "澄江县"
            },
            {
              pinyin: "TongHai",
              shortname: "通海县"
            },
            {
              pinyin: "HuaNing",
              shortname: "华宁县"
            },
            {
              pinyin: "YiMen",
              shortname: "易门县"
            },
            {
              pinyin: "EShan",
              shortname: "峨山彝族自治县"
            },
            {
              pinyin: "XinPing",
              shortname: "新平彝族傣族自治县"
            },
            {
              pinyin: "YuanJiang",
              shortname: "元江哈尼族彝族傣族自治县"
            }
          ],
          shortname: "玉溪市"
        },
        {
          pinyin: "BaoShan",
          children: [
            {
              pinyin: "BaoShan",
              shortname: "市辖区"
            },
            {
              pinyin: "LongYang",
              shortname: "隆阳区"
            },
            {
              pinyin: "ShiDian",
              shortname: "施甸县"
            },
            {
              pinyin: "LongLing",
              shortname: "龙陵县"
            },
            {
              pinyin: "ChangNing",
              shortname: "昌宁县"
            },
            {
              pinyin: "TengChong",
              shortname: "腾冲市"
            }
          ],
          shortname: "保山市"
        },
        {
          pinyin: "ZhaoTong",
          children: [
            {
              pinyin: "ZhaoTong",
              shortname: "市辖区"
            },
            {
              pinyin: "ZhaoYang",
              shortname: "昭阳区"
            },
            {
              pinyin: "LuDian",
              shortname: "鲁甸县"
            },
            {
              pinyin: "QiaoJia",
              shortname: "巧家县"
            },
            {
              pinyin: "YanJin",
              shortname: "盐津县"
            },
            {
              pinyin: "DaGuan",
              shortname: "大关县"
            },
            {
              pinyin: "YongShan",
              shortname: "永善县"
            },
            {
              pinyin: "SuiJiang",
              shortname: "绥江县"
            },
            {
              pinyin: "ZhenXiong",
              shortname: "镇雄县"
            },
            {
              pinyin: "YiLiang",
              shortname: "彝良县"
            },
            {
              pinyin: "WeiXin",
              shortname: "威信县"
            },
            {
              pinyin: "ShuiFu",
              shortname: "水富市"
            }
          ],
          shortname: "昭通市"
        },
        {
          pinyin: "LiJiang",
          children: [
            {
              pinyin: "LiJiang",
              shortname: "市辖区"
            },
            {
              pinyin: "GuCheng",
              shortname: "古城区"
            },
            {
              pinyin: "YuLong",
              shortname: "玉龙纳西族自治县"
            },
            {
              pinyin: "YongSheng",
              shortname: "永胜县"
            },
            {
              pinyin: "HuaPing",
              shortname: "华坪县"
            },
            {
              pinyin: "NingLang",
              shortname: "宁蒗彝族自治县"
            }
          ],
          shortname: "丽江市"
        },
        {
          pinyin: "PuEr",
          children: [
            {
              pinyin: "PuEr",
              shortname: "市辖区"
            },
            {
              pinyin: "SiMao",
              shortname: "思茅区"
            },
            {
              pinyin: "NingEr",
              shortname: "宁洱哈尼族彝族自治县"
            },
            {
              pinyin: "MoJiang",
              shortname: "墨江哈尼族自治县"
            },
            {
              pinyin: "JingDong",
              shortname: "景东彝族自治县"
            },
            {
              pinyin: "JingGu",
              shortname: "景谷傣族彝族自治县"
            },
            {
              pinyin: "ZhenYuan",
              shortname: "镇沅彝族哈尼族拉祜族自治县"
            },
            {
              pinyin: "JiangCheng",
              shortname: "江城哈尼族彝族自治县"
            },
            {
              pinyin: "MengLian",
              shortname: "孟连傣族拉祜族佤族自治县"
            },
            {
              pinyin: "LanCang",
              shortname: "澜沧拉祜族自治县"
            },
            {
              pinyin: "XiMengWaZuZiZhiXian",
              shortname: "西盟佤族自治县"
            }
          ],
          shortname: "普洱市"
        },
        {
          pinyin: "LinCang",
          children: [
            {
              pinyin: "LinCang",
              shortname: "市辖区"
            },
            {
              pinyin: "LinXiang",
              shortname: "临翔区"
            },
            {
              pinyin: "FengQing",
              shortname: "凤庆县"
            },
            {
              pinyin: "YunXian",
              shortname: "云县"
            },
            {
              pinyin: "YongDe",
              shortname: "永德县"
            },
            {
              pinyin: "ZhenKang",
              shortname: "镇康县"
            },
            {
              pinyin: "ShuangJiang",
              shortname: "双江拉祜族佤族布朗族傣族自治县"
            },
            {
              pinyin: "GengMa",
              shortname: "耿马傣族佤族自治县"
            },
            {
              pinyin: "CangYuan",
              shortname: "沧源佤族自治县"
            }
          ],
          shortname: "临沧市"
        },
        {
          pinyin: "ChuXiong",
          children: [
            {
              pinyin: "ChuXiong",
              shortname: "楚雄市"
            },
            {
              pinyin: "ShuangBai",
              shortname: "双柏县"
            },
            {
              pinyin: "MouDing",
              shortname: "牟定县"
            },
            {
              pinyin: "NanHua",
              shortname: "南华县"
            },
            {
              pinyin: "YaoAn",
              shortname: "姚安县"
            },
            {
              pinyin: "DaYao",
              shortname: "大姚县"
            },
            {
              pinyin: "YongRen",
              shortname: "永仁县"
            },
            {
              pinyin: "YuanMou",
              shortname: "元谋县"
            },
            {
              pinyin: "WuDing",
              shortname: "武定县"
            },
            {
              pinyin: "LuFeng",
              shortname: "禄丰县"
            }
          ],
          shortname: "楚雄彝族自治州"
        },
        {
          pinyin: "HongHe",
          children: [
            {
              pinyin: "GeJiu",
              shortname: "个旧市"
            },
            {
              pinyin: "KaiYuan",
              shortname: "开远市"
            },
            {
              pinyin: "MengZi",
              shortname: "蒙自市"
            },
            {
              pinyin: "MiLe",
              shortname: "弥勒市"
            },
            {
              pinyin: "PingBian",
              shortname: "屏边苗族自治县"
            },
            {
              pinyin: "JianShui",
              shortname: "建水县"
            },
            {
              pinyin: "ShiPing",
              shortname: "石屏县"
            },
            {
              pinyin: "LuXi",
              shortname: "泸西县"
            },
            {
              pinyin: "YuanYang",
              shortname: "元阳县"
            },
            {
              pinyin: "HongHe",
              shortname: "红河县"
            },
            {
              pinyin: "JinPing",
              shortname: "金平苗族瑶族傣族自治县"
            },
            {
              pinyin: "LvChun",
              shortname: "绿春县"
            },
            {
              pinyin: "HeKou",
              shortname: "河口瑶族自治县"
            }
          ],
          shortname: "红河哈尼族彝族自治州"
        },
        {
          pinyin: "WenShan",
          children: [
            {
              pinyin: "WenShan",
              shortname: "文山市"
            },
            {
              pinyin: "YanShan",
              shortname: "砚山县"
            },
            {
              pinyin: "XiChou",
              shortname: "西畴县"
            },
            {
              pinyin: "MaLiPo",
              shortname: "麻栗坡县"
            },
            {
              pinyin: "MaGuan",
              shortname: "马关县"
            },
            {
              pinyin: "QiuBei",
              shortname: "丘北县"
            },
            {
              pinyin: "GuangNan",
              shortname: "广南县"
            },
            {
              pinyin: "FuNing",
              shortname: "富宁县"
            }
          ],
          shortname: "文山壮族苗族自治州"
        },
        {
          pinyin: "XiShuangBanNa",
          children: [
            {
              pinyin: "JingHong",
              shortname: "景洪市"
            },
            {
              pinyin: "MengHai",
              shortname: "勐海县"
            },
            {
              pinyin: "MengLa",
              shortname: "勐腊县"
            }
          ],
          shortname: "西双版纳傣族自治州"
        },
        {
          pinyin: "DaLi",
          children: [
            {
              pinyin: "DaLi",
              shortname: "大理市"
            },
            {
              pinyin: "YangBi",
              shortname: "漾濞彝族自治县"
            },
            {
              pinyin: "XiangYun",
              shortname: "祥云县"
            },
            {
              pinyin: "BinChuan",
              shortname: "宾川县"
            },
            {
              pinyin: "MiDu",
              shortname: "弥渡县"
            },
            {
              pinyin: "NanJian",
              shortname: "南涧彝族自治县"
            },
            {
              pinyin: "WeiShan",
              shortname: "巍山彝族回族自治县"
            },
            {
              pinyin: "YongPing",
              shortname: "永平县"
            },
            {
              pinyin: "YunLong",
              shortname: "云龙县"
            },
            {
              pinyin: "ErYuan",
              shortname: "洱源县"
            },
            {
              pinyin: "JianChuan",
              shortname: "剑川县"
            },
            {
              pinyin: "HeQing",
              shortname: "鹤庆县"
            }
          ],
          shortname: "大理白族自治州"
        },
        {
          pinyin: "DeHong",
          children: [
            {
              pinyin: "RuiLi",
              shortname: "瑞丽市"
            },
            {
              pinyin: "MangShi",
              shortname: "芒市"
            },
            {
              pinyin: "LiangHe",
              shortname: "梁河县"
            },
            {
              pinyin: "YingJiang",
              shortname: "盈江县"
            },
            {
              pinyin: "LongChuan",
              shortname: "陇川县"
            }
          ],
          shortname: "德宏傣族景颇族自治州"
        },
        {
          pinyin: "NuJiang",
          children: [
            {
              pinyin: "LuShui",
              shortname: "泸水市"
            },
            {
              pinyin: "FuGong",
              shortname: "福贡县"
            },
            {
              pinyin: "GongShan",
              shortname: "贡山独龙族怒族自治县"
            },
            {
              pinyin: "LanPing",
              shortname: "兰坪白族普米族自治县"
            }
          ],
          shortname: "怒江傈僳族自治州"
        },
        {
          pinyin: "DiQing",
          children: [
            {
              pinyin: "XiangGeLiLa",
              shortname: "香格里拉市"
            },
            {
              pinyin: "DeQin",
              shortname: "德钦县"
            },
            {
              pinyin: "WeiXi",
              shortname: "维西傈僳族自治县"
            }
          ],
          shortname: "迪庆藏族自治州"
        }
      ],
      shortname: "云南省"
    },
    {
      pinyin: "XiZang",
      children: [
        {
          pinyin: "LaSa",
          children: [
            {
              pinyin: "LaSa",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengGuan",
              shortname: "城关区"
            },
            {
              pinyin: "DuiLongDeQing",
              shortname: "堆龙德庆区"
            },
            {
              pinyin: "DaZi",
              shortname: "达孜区"
            },
            {
              pinyin: "LinZhou",
              shortname: "林周县"
            },
            {
              pinyin: "DangXiong",
              shortname: "当雄县"
            },
            {
              pinyin: "NiMu",
              shortname: "尼木县"
            },
            {
              pinyin: "QuShui",
              shortname: "曲水县"
            },
            {
              pinyin: "MoZhuGongKa",
              shortname: "墨竹工卡县"
            },
            {
              pinyin: "GeErMuZangQingGongYeYuan",
              shortname: "格尔木藏青工业园区"
            },
            {
              pinyin: "LaSaJingKaiQu",
              shortname: "拉萨经济技术开发区"
            },
            {
              pinyin: "XiZangWenHuaLvYouChuangYiYuan",
              shortname: "西藏文化旅游创意园区"
            },
            {
              pinyin: "DaZiGongYeYuan",
              shortname: "达孜工业园区"
            }
          ],
          shortname: "拉萨市"
        },
        {
          pinyin: "RiKaZe",
          children: [
            {
              pinyin: "SangZhuZi",
              shortname: "桑珠孜区"
            },
            {
              pinyin: "NanMuLin",
              shortname: "南木林县"
            },
            {
              pinyin: "JiangZi",
              shortname: "江孜县"
            },
            {
              pinyin: "DingRi",
              shortname: "定日县"
            },
            {
              pinyin: "SaJia",
              shortname: "萨迦县"
            },
            {
              pinyin: "LaZi",
              shortname: "拉孜县"
            },
            {
              pinyin: "AngRen",
              shortname: "昂仁县"
            },
            {
              pinyin: "XieTongMen",
              shortname: "谢通门县"
            },
            {
              pinyin: "BaiLang",
              shortname: "白朗县"
            },
            {
              pinyin: "RenBu",
              shortname: "仁布县"
            },
            {
              pinyin: "KangMa",
              shortname: "康马县"
            },
            {
              pinyin: "DingJie",
              shortname: "定结县"
            },
            {
              pinyin: "ZhongBa",
              shortname: "仲巴县"
            },
            {
              pinyin: "YaDong",
              shortname: "亚东县"
            },
            {
              pinyin: "JiLong",
              shortname: "吉隆县"
            },
            {
              pinyin: "NieLaMu",
              shortname: "聂拉木县"
            },
            {
              pinyin: "SaGa",
              shortname: "萨嘎县"
            },
            {
              pinyin: "GangBa",
              shortname: "岗巴县"
            }
          ],
          shortname: "日喀则市"
        },
        {
          pinyin: "ChangDu",
          children: [
            {
              pinyin: "KaRuo",
              shortname: "卡若区"
            },
            {
              pinyin: "JiangDa",
              shortname: "江达县"
            },
            {
              pinyin: "GongJue",
              shortname: "贡觉县"
            },
            {
              pinyin: "LeiWuQi",
              shortname: "类乌齐县"
            },
            {
              pinyin: "DingQing",
              shortname: "丁青县"
            },
            {
              pinyin: "ChaYa",
              shortname: "察雅县"
            },
            {
              pinyin: "BaSu",
              shortname: "八宿县"
            },
            {
              pinyin: "ZuoGong",
              shortname: "左贡县"
            },
            {
              pinyin: "MangKang",
              shortname: "芒康县"
            },
            {
              pinyin: "LuoLong",
              shortname: "洛隆县"
            },
            {
              pinyin: "BianBa",
              shortname: "边坝县"
            }
          ],
          shortname: "昌都市"
        },
        {
          pinyin: "LinZhi",
          children: [
            {
              pinyin: "BaYi",
              shortname: "巴宜区"
            },
            {
              pinyin: "GongBuJiangDa",
              shortname: "工布江达县"
            },
            {
              pinyin: "MiLin",
              shortname: "米林县"
            },
            {
              pinyin: "MoTuo",
              shortname: "墨脱县"
            },
            {
              pinyin: "BoMi",
              shortname: "波密县"
            },
            {
              pinyin: "ChaYu",
              shortname: "察隅县"
            },
            {
              pinyin: "LangXian",
              shortname: "朗县"
            }
          ],
          shortname: "林芝市"
        },
        {
          pinyin: "ShanNan",
          children: [
            {
              pinyin: "ShanNan",
              shortname: "市辖区"
            },
            {
              pinyin: "NaiDong",
              shortname: "乃东区"
            },
            {
              pinyin: "ZaNang",
              shortname: "扎囊县"
            },
            {
              pinyin: "GongGa",
              shortname: "贡嘎县"
            },
            {
              pinyin: "SangRi",
              shortname: "桑日县"
            },
            {
              pinyin: "QiongJie",
              shortname: "琼结县"
            },
            {
              pinyin: "QuSong",
              shortname: "曲松县"
            },
            {
              pinyin: "CuoMei",
              shortname: "措美县"
            },
            {
              pinyin: "LuoZha",
              shortname: "洛扎县"
            },
            {
              pinyin: "JiaCha",
              shortname: "加查县"
            },
            {
              pinyin: "LongZi",
              shortname: "隆子县"
            },
            {
              pinyin: "CuoNa",
              shortname: "错那县"
            },
            {
              pinyin: "LangKaZi",
              shortname: "浪卡子县"
            }
          ],
          shortname: "山南市"
        },
        {
          pinyin: "NaQu",
          children: [
            {
              pinyin: "SeNi",
              shortname: "色尼区"
            },
            {
              pinyin: "JiaLi",
              shortname: "嘉黎县"
            },
            {
              pinyin: "BiRu",
              shortname: "比如县"
            },
            {
              pinyin: "NieRong",
              shortname: "聂荣县"
            },
            {
              pinyin: "AnDuo",
              shortname: "安多县"
            },
            {
              pinyin: "ShenZha",
              shortname: "申扎县"
            },
            {
              pinyin: "SuoXian",
              shortname: "索县"
            },
            {
              pinyin: "BanGe",
              shortname: "班戈县"
            },
            {
              pinyin: "BaQing",
              shortname: "巴青县"
            },
            {
              pinyin: "NiMa",
              shortname: "尼玛县"
            },
            {
              pinyin: "ShuangHu",
              shortname: "双湖县"
            }
          ],
          shortname: "那曲市"
        },
        {
          pinyin: "ALi",
          children: [
            {
              pinyin: "PuLan",
              shortname: "普兰县"
            },
            {
              pinyin: "ZhaDa",
              shortname: "札达县"
            },
            {
              pinyin: "GaEr",
              shortname: "噶尔县"
            },
            {
              pinyin: "RiTu",
              shortname: "日土县"
            },
            {
              pinyin: "GeJi",
              shortname: "革吉县"
            },
            {
              pinyin: "GaiZe",
              shortname: "改则县"
            },
            {
              pinyin: "CuoQin",
              shortname: "措勤县"
            }
          ],
          shortname: "阿里地区"
        }
      ],
      shortname: "西藏自治区"
    },
    {
      pinyin: "ShanXi",
      children: [
        {
          pinyin: "XiAn",
          children: [
            {
              pinyin: "XiAn",
              shortname: "市辖区"
            },
            {
              pinyin: "XinCheng",
              shortname: "新城区"
            },
            {
              pinyin: "BeiLin",
              shortname: "碑林区"
            },
            {
              pinyin: "LianHu",
              shortname: "莲湖区"
            },
            {
              pinyin: "BaQiao",
              shortname: "灞桥区"
            },
            {
              pinyin: "WeiYang",
              shortname: "未央区"
            },
            {
              pinyin: "YanTa",
              shortname: "雁塔区"
            },
            {
              pinyin: "YanLiang",
              shortname: "阎良区"
            },
            {
              pinyin: "LinTong",
              shortname: "临潼区"
            },
            {
              pinyin: "ChangAn",
              shortname: "长安区"
            },
            {
              pinyin: "GaoLing",
              shortname: "高陵区"
            },
            {
              pinyin: "HuYi",
              shortname: "鄠邑区"
            },
            {
              pinyin: "LanTian",
              shortname: "蓝田县"
            },
            {
              pinyin: "ZhouZhi",
              shortname: "周至县"
            }
          ],
          shortname: "西安市"
        },
        {
          pinyin: "TongChuan",
          children: [
            {
              pinyin: "TongChuan",
              shortname: "市辖区"
            },
            {
              pinyin: "WangYi",
              shortname: "王益区"
            },
            {
              pinyin: "YinTai",
              shortname: "印台区"
            },
            {
              pinyin: "YaoZhou",
              shortname: "耀州区"
            },
            {
              pinyin: "YiJun",
              shortname: "宜君县"
            }
          ],
          shortname: "铜川市"
        },
        {
          pinyin: "BaoJi",
          children: [
            {
              pinyin: "BaoJi",
              shortname: "市辖区"
            },
            {
              pinyin: "WeiBin",
              shortname: "渭滨区"
            },
            {
              pinyin: "JinTai",
              shortname: "金台区"
            },
            {
              pinyin: "ChenCang",
              shortname: "陈仓区"
            },
            {
              pinyin: "FengXiang",
              shortname: "凤翔县"
            },
            {
              pinyin: "QiShan",
              shortname: "岐山县"
            },
            {
              pinyin: "FuFeng",
              shortname: "扶风县"
            },
            {
              pinyin: "MeiXian",
              shortname: "眉县"
            },
            {
              pinyin: "LongXian",
              shortname: "陇县"
            },
            {
              pinyin: "QianYang",
              shortname: "千阳县"
            },
            {
              pinyin: "LinYou",
              shortname: "麟游县"
            },
            {
              pinyin: "FengXian",
              shortname: "凤县"
            },
            {
              pinyin: "TaiBai",
              shortname: "太白县"
            }
          ],
          shortname: "宝鸡市"
        },
        {
          pinyin: "XianYang",
          children: [
            {
              pinyin: "XianYang",
              shortname: "市辖区"
            },
            {
              pinyin: "QinDu",
              shortname: "秦都区"
            },
            {
              pinyin: "YangLing",
              shortname: "杨陵区"
            },
            {
              pinyin: "WeiCheng",
              shortname: "渭城区"
            },
            {
              pinyin: "SanYuan",
              shortname: "三原县"
            },
            {
              pinyin: "JingYang",
              shortname: "泾阳县"
            },
            {
              pinyin: "QianXian",
              shortname: "乾县"
            },
            {
              pinyin: "LiQuan",
              shortname: "礼泉县"
            },
            {
              pinyin: "YongShou",
              shortname: "永寿县"
            },
            {
              pinyin: "ChangWu",
              shortname: "长武县"
            },
            {
              pinyin: "XunYi",
              shortname: "旬邑县"
            },
            {
              pinyin: "ChunHua",
              shortname: "淳化县"
            },
            {
              pinyin: "WuGong",
              shortname: "武功县"
            },
            {
              pinyin: "XingPing",
              shortname: "兴平市"
            },
            {
              pinyin: "BinZhou",
              shortname: "彬州市"
            }
          ],
          shortname: "咸阳市"
        },
        {
          pinyin: "WeiNan",
          children: [
            {
              pinyin: "WeiNan",
              shortname: "市辖区"
            },
            {
              pinyin: "LinWei",
              shortname: "临渭区"
            },
            {
              pinyin: "HuaZhou",
              shortname: "华州区"
            },
            {
              pinyin: "TongGuan",
              shortname: "潼关县"
            },
            {
              pinyin: "DaLi",
              shortname: "大荔县"
            },
            {
              pinyin: "HeYang",
              shortname: "合阳县"
            },
            {
              pinyin: "ChengCheng",
              shortname: "澄城县"
            },
            {
              pinyin: "PuCheng",
              shortname: "蒲城县"
            },
            {
              pinyin: "BaiShui",
              shortname: "白水县"
            },
            {
              pinyin: "FuPing",
              shortname: "富平县"
            },
            {
              pinyin: "HanCheng",
              shortname: "韩城市"
            },
            {
              pinyin: "HuaYin",
              shortname: "华阴市"
            }
          ],
          shortname: "渭南市"
        },
        {
          pinyin: "YanAn",
          children: [
            {
              pinyin: "YanAn",
              shortname: "市辖区"
            },
            {
              pinyin: "BaoTa",
              shortname: "宝塔区"
            },
            {
              pinyin: "AnSai",
              shortname: "安塞区"
            },
            {
              pinyin: "YanChang",
              shortname: "延长县"
            },
            {
              pinyin: "YanChuan",
              shortname: "延川县"
            },
            {
              pinyin: "ZiChang",
              shortname: "子长县"
            },
            {
              pinyin: "ZhiDan",
              shortname: "志丹县"
            },
            {
              pinyin: "WuQi",
              shortname: "吴起县"
            },
            {
              pinyin: "GanQuan",
              shortname: "甘泉县"
            },
            {
              pinyin: "FuXian",
              shortname: "富县"
            },
            {
              pinyin: "LuoChuan",
              shortname: "洛川县"
            },
            {
              pinyin: "YiChuan",
              shortname: "宜川县"
            },
            {
              pinyin: "HuangLong",
              shortname: "黄龙县"
            },
            {
              pinyin: "HuangLing",
              shortname: "黄陵县"
            }
          ],
          shortname: "延安市"
        },
        {
          pinyin: "HanZhong",
          children: [
            {
              pinyin: "HanZhong",
              shortname: "市辖区"
            },
            {
              pinyin: "HanTai",
              shortname: "汉台区"
            },
            {
              pinyin: "NanZheng",
              shortname: "南郑区"
            },
            {
              pinyin: "ChengGu",
              shortname: "城固县"
            },
            {
              pinyin: "YangXian",
              shortname: "洋县"
            },
            {
              pinyin: "XiXiang",
              shortname: "西乡县"
            },
            {
              pinyin: "MianXian",
              shortname: "勉县"
            },
            {
              pinyin: "NingQiang",
              shortname: "宁强县"
            },
            {
              pinyin: "LueYang",
              shortname: "略阳县"
            },
            {
              pinyin: "ZhenBa",
              shortname: "镇巴县"
            },
            {
              pinyin: "LiuBa",
              shortname: "留坝县"
            },
            {
              pinyin: "FoPing",
              shortname: "佛坪县"
            }
          ],
          shortname: "汉中市"
        },
        {
          pinyin: "YuLin",
          children: [
            {
              pinyin: "YuLin",
              shortname: "市辖区"
            },
            {
              pinyin: "YuYang",
              shortname: "榆阳区"
            },
            {
              pinyin: "HengShan",
              shortname: "横山区"
            },
            {
              pinyin: "FuGu",
              shortname: "府谷县"
            },
            {
              pinyin: "JingBian",
              shortname: "靖边县"
            },
            {
              pinyin: "DingBian",
              shortname: "定边县"
            },
            {
              pinyin: "SuiDe",
              shortname: "绥德县"
            },
            {
              pinyin: "MiZhi",
              shortname: "米脂县"
            },
            {
              pinyin: "JiaXian",
              shortname: "佳县"
            },
            {
              pinyin: "WuBao",
              shortname: "吴堡县"
            },
            {
              pinyin: "QingJian",
              shortname: "清涧县"
            },
            {
              pinyin: "ZiZhou",
              shortname: "子洲县"
            },
            {
              pinyin: "ShenMu",
              shortname: "神木市"
            }
          ],
          shortname: "榆林市"
        },
        {
          pinyin: "AnKang",
          children: [
            {
              pinyin: "AnKang",
              shortname: "市辖区"
            },
            {
              pinyin: "HanBin",
              shortname: "汉滨区"
            },
            {
              pinyin: "HanYin",
              shortname: "汉阴县"
            },
            {
              pinyin: "ShiQuan",
              shortname: "石泉县"
            },
            {
              pinyin: "NingShan",
              shortname: "宁陕县"
            },
            {
              pinyin: "ZiYang",
              shortname: "紫阳县"
            },
            {
              pinyin: "LanGao",
              shortname: "岚皋县"
            },
            {
              pinyin: "PingLi",
              shortname: "平利县"
            },
            {
              pinyin: "ZhenPing",
              shortname: "镇坪县"
            },
            {
              pinyin: "XunYang",
              shortname: "旬阳县"
            },
            {
              pinyin: "BaiHe",
              shortname: "白河县"
            }
          ],
          shortname: "安康市"
        },
        {
          pinyin: "ShangLuo",
          children: [
            {
              pinyin: "ShangLuo",
              shortname: "市辖区"
            },
            {
              pinyin: "ShangZhou",
              shortname: "商州区"
            },
            {
              pinyin: "LuoNan",
              shortname: "洛南县"
            },
            {
              pinyin: "DanFeng",
              shortname: "丹凤县"
            },
            {
              pinyin: "ShangNan",
              shortname: "商南县"
            },
            {
              pinyin: "ShanYang",
              shortname: "山阳县"
            },
            {
              pinyin: "ZhenAn",
              shortname: "镇安县"
            },
            {
              pinyin: "ZhaShui",
              shortname: "柞水县"
            }
          ],
          shortname: "商洛市"
        }
      ],
      shortname: "陕西省"
    },
    {
      pinyin: "GanSu",
      children: [
        {
          pinyin: "LanZhou",
          children: [
            {
              pinyin: "LanZhou",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengGuan",
              shortname: "城关区"
            },
            {
              pinyin: "QiLiHe",
              shortname: "七里河区"
            },
            {
              pinyin: "XiGu",
              shortname: "西固区"
            },
            {
              pinyin: "AnNing",
              shortname: "安宁区"
            },
            {
              pinyin: "HongGu",
              shortname: "红古区"
            },
            {
              pinyin: "YongDeng",
              shortname: "永登县"
            },
            {
              pinyin: "GaoLan",
              shortname: "皋兰县"
            },
            {
              pinyin: "YuZhong",
              shortname: "榆中县"
            },
            {
              pinyin: "LanZhou",
              shortname: "兰州新区"
            }
          ],
          shortname: "兰州市"
        },
        {
          pinyin: "JiaYuGuan",
          children: [
            {
              pinyin: "JiaYuGuan",
              shortname: "市辖区"
            }
          ],
          shortname: "嘉峪关市"
        },
        {
          pinyin: "JinChang",
          children: [
            {
              pinyin: "JinChang",
              shortname: "市辖区"
            },
            {
              pinyin: "JinChuan",
              shortname: "金川区"
            },
            {
              pinyin: "YongChang",
              shortname: "永昌县"
            }
          ],
          shortname: "金昌市"
        },
        {
          pinyin: "BaiYin",
          children: [
            {
              pinyin: "BaiYin",
              shortname: "市辖区"
            },
            {
              pinyin: "BaiYin",
              shortname: "白银区"
            },
            {
              pinyin: "PingChuan",
              shortname: "平川区"
            },
            {
              pinyin: "JingYuan",
              shortname: "靖远县"
            },
            {
              pinyin: "HuiNing",
              shortname: "会宁县"
            },
            {
              pinyin: "JingTai",
              shortname: "景泰县"
            }
          ],
          shortname: "白银市"
        },
        {
          pinyin: "TianShui",
          children: [
            {
              pinyin: "TianShui",
              shortname: "市辖区"
            },
            {
              pinyin: "QinZhou",
              shortname: "秦州区"
            },
            {
              pinyin: "MaiJi",
              shortname: "麦积区"
            },
            {
              pinyin: "QingShui",
              shortname: "清水县"
            },
            {
              pinyin: "QinAn",
              shortname: "秦安县"
            },
            {
              pinyin: "GanGu",
              shortname: "甘谷县"
            },
            {
              pinyin: "WuShan",
              shortname: "武山县"
            },
            {
              pinyin: "ZhangJiaChuan",
              shortname: "张家川回族自治县"
            }
          ],
          shortname: "天水市"
        },
        {
          pinyin: "WuWei",
          children: [
            {
              pinyin: "WuWei",
              shortname: "市辖区"
            },
            {
              pinyin: "LiangZhou",
              shortname: "凉州区"
            },
            {
              pinyin: "MinQin",
              shortname: "民勤县"
            },
            {
              pinyin: "GuLang",
              shortname: "古浪县"
            },
            {
              pinyin: "TianZhu",
              shortname: "天祝藏族自治县"
            }
          ],
          shortname: "武威市"
        },
        {
          pinyin: "ZhangYe",
          children: [
            {
              pinyin: "ZhangYe",
              shortname: "市辖区"
            },
            {
              pinyin: "GanZhou",
              shortname: "甘州区"
            },
            {
              pinyin: "SuNan",
              shortname: "肃南裕固族自治县"
            },
            {
              pinyin: "MinYue",
              shortname: "民乐县"
            },
            {
              pinyin: "LinZe",
              shortname: "临泽县"
            },
            {
              pinyin: "GaoTai",
              shortname: "高台县"
            },
            {
              pinyin: "ShanDan",
              shortname: "山丹县"
            }
          ],
          shortname: "张掖市"
        },
        {
          pinyin: "PingLiang",
          children: [
            {
              pinyin: "PingLiang",
              shortname: "市辖区"
            },
            {
              pinyin: "KongTong",
              shortname: "崆峒区"
            },
            {
              pinyin: "JingChuan",
              shortname: "泾川县"
            },
            {
              pinyin: "LingTai",
              shortname: "灵台县"
            },
            {
              pinyin: "ChongXin",
              shortname: "崇信县"
            },
            {
              pinyin: "ZhuangLang",
              shortname: "庄浪县"
            },
            {
              pinyin: "JingNing",
              shortname: "静宁县"
            },
            {
              pinyin: "HuaTing",
              shortname: "华亭市"
            }
          ],
          shortname: "平凉市"
        },
        {
          pinyin: "JiuQuan",
          children: [
            {
              pinyin: "JiuQuan",
              shortname: "市辖区"
            },
            {
              pinyin: "SuZhou",
              shortname: "肃州区"
            },
            {
              pinyin: "JinTa",
              shortname: "金塔县"
            },
            {
              pinyin: "GuaZhou",
              shortname: "瓜州县"
            },
            {
              pinyin: "SuBei",
              shortname: "肃北蒙古族自治县"
            },
            {
              pinyin: "AKeSai",
              shortname: "阿克塞哈萨克族自治县"
            },
            {
              pinyin: "YuMen",
              shortname: "玉门市"
            },
            {
              pinyin: "DunHuang",
              shortname: "敦煌市"
            }
          ],
          shortname: "酒泉市"
        },
        {
          pinyin: "QingYang",
          children: [
            {
              pinyin: "QingYang",
              shortname: "市辖区"
            },
            {
              pinyin: "XiFeng",
              shortname: "西峰区"
            },
            {
              pinyin: "QingCheng",
              shortname: "庆城县"
            },
            {
              pinyin: "HuanXian",
              shortname: "环县"
            },
            {
              pinyin: "HuaChi",
              shortname: "华池县"
            },
            {
              pinyin: "HeShui",
              shortname: "合水县"
            },
            {
              pinyin: "ZhengNing",
              shortname: "正宁县"
            },
            {
              pinyin: "NingXian",
              shortname: "宁县"
            },
            {
              pinyin: "ZhenYuan",
              shortname: "镇原县"
            }
          ],
          shortname: "庆阳市"
        },
        {
          pinyin: "DingXi",
          children: [
            {
              pinyin: "DingXi",
              shortname: "市辖区"
            },
            {
              pinyin: "AnDing",
              shortname: "安定区"
            },
            {
              pinyin: "TongWei",
              shortname: "通渭县"
            },
            {
              pinyin: "LongXi",
              shortname: "陇西县"
            },
            {
              pinyin: "WeiYuan",
              shortname: "渭源县"
            },
            {
              pinyin: "LinTao",
              shortname: "临洮县"
            },
            {
              pinyin: "ZhangXian",
              shortname: "漳县"
            },
            {
              pinyin: "MinXian",
              shortname: "岷县"
            }
          ],
          shortname: "定西市"
        },
        {
          pinyin: "LongNan",
          children: [
            {
              pinyin: "LongNan",
              shortname: "市辖区"
            },
            {
              pinyin: "WuDu",
              shortname: "武都区"
            },
            {
              pinyin: "ChengXian",
              shortname: "成县"
            },
            {
              pinyin: "WenXian",
              shortname: "文县"
            },
            {
              pinyin: "DangChang",
              shortname: "宕昌县"
            },
            {
              pinyin: "KangXian",
              shortname: "康县"
            },
            {
              pinyin: "XiHe",
              shortname: "西和县"
            },
            {
              pinyin: "LiXian",
              shortname: "礼县"
            },
            {
              pinyin: "HuiXian",
              shortname: "徽县"
            },
            {
              pinyin: "LiangDang",
              shortname: "两当县"
            }
          ],
          shortname: "陇南市"
        },
        {
          pinyin: "LinXia",
          children: [
            {
              pinyin: "LinXia",
              shortname: "临夏市"
            },
            {
              pinyin: "LinXia",
              shortname: "临夏县"
            },
            {
              pinyin: "KangLe",
              shortname: "康乐县"
            },
            {
              pinyin: "YongJing",
              shortname: "永靖县"
            },
            {
              pinyin: "GuangHe",
              shortname: "广河县"
            },
            {
              pinyin: "HeZheng",
              shortname: "和政县"
            },
            {
              pinyin: "DongXiangZuZiZhiXian",
              shortname: "东乡族自治县"
            },
            {
              pinyin: "JiShiShan",
              shortname: "积石山保安族东乡族撒拉族自治县"
            }
          ],
          shortname: "临夏回族自治州"
        },
        {
          pinyin: "GanNan",
          children: [
            {
              pinyin: "HeZuo",
              shortname: "合作市"
            },
            {
              pinyin: "LinTan",
              shortname: "临潭县"
            },
            {
              pinyin: "ZhuoNi",
              shortname: "卓尼县"
            },
            {
              pinyin: "ZhouQu",
              shortname: "舟曲县"
            },
            {
              pinyin: "DieBu",
              shortname: "迭部县"
            },
            {
              pinyin: "MaQu",
              shortname: "玛曲县"
            },
            {
              pinyin: "LuQu",
              shortname: "碌曲县"
            },
            {
              pinyin: "XiaHe",
              shortname: "夏河县"
            }
          ],
          shortname: "甘南藏族自治州"
        }
      ],
      shortname: "甘肃省"
    },
    {
      pinyin: "QingHai",
      children: [
        {
          pinyin: "XiNing",
          children: [
            {
              pinyin: "XiNing",
              shortname: "市辖区"
            },
            {
              pinyin: "ChengDong",
              shortname: "城东区"
            },
            {
              pinyin: "ChengZhong",
              shortname: "城中区"
            },
            {
              pinyin: "ChengXi",
              shortname: "城西区"
            },
            {
              pinyin: "ChengBei",
              shortname: "城北区"
            },
            {
              pinyin: "DaTong",
              shortname: "大通回族土族自治县"
            },
            {
              pinyin: "HuangZhong",
              shortname: "湟中县"
            },
            {
              pinyin: "HuangYuan",
              shortname: "湟源县"
            }
          ],
          shortname: "西宁市"
        },
        {
          pinyin: "HaiDong",
          children: [
            {
              pinyin: "LeDou",
              shortname: "乐都区"
            },
            {
              pinyin: "PingAn",
              shortname: "平安区"
            },
            {
              pinyin: "MinHe",
              shortname: "民和回族土族自治县"
            },
            {
              pinyin: "HuZhu",
              shortname: "互助土族自治县"
            },
            {
              pinyin: "HuaLong",
              shortname: "化隆回族自治县"
            },
            {
              pinyin: "XunHua",
              shortname: "循化撒拉族自治县"
            }
          ],
          shortname: "海东市"
        },
        {
          pinyin: "HaiBei",
          children: [
            {
              pinyin: "MenYuan",
              shortname: "门源回族自治县"
            },
            {
              pinyin: "QiLian",
              shortname: "祁连县"
            },
            {
              pinyin: "HaiYan",
              shortname: "海晏县"
            },
            {
              pinyin: "GangCha",
              shortname: "刚察县"
            }
          ],
          shortname: "海北藏族自治州"
        },
        {
          pinyin: "HuangNan",
          children: [
            {
              pinyin: "TongRen",
              shortname: "同仁县"
            },
            {
              pinyin: "JianZha",
              shortname: "尖扎县"
            },
            {
              pinyin: "ZeKu",
              shortname: "泽库县"
            },
            {
              pinyin: "HeNan",
              shortname: "河南蒙古族自治县"
            }
          ],
          shortname: "黄南藏族自治州"
        },
        {
          pinyin: "HaiNan",
          children: [
            {
              pinyin: "GongHe",
              shortname: "共和县"
            },
            {
              pinyin: "TongDe",
              shortname: "同德县"
            },
            {
              pinyin: "GuiDe",
              shortname: "贵德县"
            },
            {
              pinyin: "XingHai",
              shortname: "兴海县"
            },
            {
              pinyin: "GuiNan",
              shortname: "贵南县"
            }
          ],
          shortname: "海南藏族自治州"
        },
        {
          pinyin: "GuoLuo",
          children: [
            {
              pinyin: "MaQin",
              shortname: "玛沁县"
            },
            {
              pinyin: "BanMa",
              shortname: "班玛县"
            },
            {
              pinyin: "GanDe",
              shortname: "甘德县"
            },
            {
              pinyin: "DaRi",
              shortname: "达日县"
            },
            {
              pinyin: "JiuZhi",
              shortname: "久治县"
            },
            {
              pinyin: "MaDuo",
              shortname: "玛多县"
            }
          ],
          shortname: "果洛藏族自治州"
        },
        {
          pinyin: "YuShu",
          children: [
            {
              pinyin: "YuShu",
              shortname: "玉树市"
            },
            {
              pinyin: "ZaDuo",
              shortname: "杂多县"
            },
            {
              pinyin: "ChenDuo",
              shortname: "称多县"
            },
            {
              pinyin: "ZhiDuo",
              shortname: "治多县"
            },
            {
              pinyin: "NangQian",
              shortname: "囊谦县"
            },
            {
              pinyin: "QuMaLai",
              shortname: "曲麻莱县"
            }
          ],
          shortname: "玉树藏族自治州"
        },
        {
          pinyin: "HaiXi",
          children: [
            {
              pinyin: "GeErMu",
              shortname: "格尔木市"
            },
            {
              pinyin: "DeLingHa",
              shortname: "德令哈市"
            },
            {
              pinyin: "MangYa",
              shortname: "茫崖市"
            },
            {
              pinyin: "WuLan",
              shortname: "乌兰县"
            },
            {
              pinyin: "DuLan",
              shortname: "都兰县"
            },
            {
              pinyin: "TianJun",
              shortname: "天峻县"
            },
            {
              pinyin: "DaChaiDan",
              shortname: "大柴旦行政委员会"
            }
          ],
          shortname: "海西蒙古族藏族自治州"
        }
      ],
      shortname: "青海省"
    },
    {
      pinyin: "NingXia",
      children: [
        {
          pinyin: "YinChuan",
          children: [
            {
              pinyin: "YinChuan",
              shortname: "市辖区"
            },
            {
              pinyin: "XingQing",
              shortname: "兴庆区"
            },
            {
              pinyin: "XiXia",
              shortname: "西夏区"
            },
            {
              pinyin: "JinFeng",
              shortname: "金凤区"
            },
            {
              pinyin: "YongNing",
              shortname: "永宁县"
            },
            {
              pinyin: "HeLan",
              shortname: "贺兰县"
            },
            {
              pinyin: "LingWu",
              shortname: "灵武市"
            }
          ],
          shortname: "银川市"
        },
        {
          pinyin: "ShiZuiShan",
          children: [
            {
              pinyin: "ShiZuiShan",
              shortname: "市辖区"
            },
            {
              pinyin: "DaWuKou",
              shortname: "大武口区"
            },
            {
              pinyin: "HuiNong",
              shortname: "惠农区"
            },
            {
              pinyin: "PingLuo",
              shortname: "平罗县"
            }
          ],
          shortname: "石嘴山市"
        },
        {
          pinyin: "WuZhong",
          children: [
            {
              pinyin: "WuZhong",
              shortname: "市辖区"
            },
            {
              pinyin: "LiTong",
              shortname: "利通区"
            },
            {
              pinyin: "HongSiBao",
              shortname: "红寺堡区"
            },
            {
              pinyin: "YanChi",
              shortname: "盐池县"
            },
            {
              pinyin: "TongXin",
              shortname: "同心县"
            },
            {
              pinyin: "QingTongXia",
              shortname: "青铜峡市"
            }
          ],
          shortname: "吴忠市"
        },
        {
          pinyin: "GuYuan",
          children: [
            {
              pinyin: "GuYuan",
              shortname: "市辖区"
            },
            {
              pinyin: "YuanZhou",
              shortname: "原州区"
            },
            {
              pinyin: "XiJi",
              shortname: "西吉县"
            },
            {
              pinyin: "LongDe",
              shortname: "隆德县"
            },
            {
              pinyin: "JingYuan",
              shortname: "泾源县"
            },
            {
              pinyin: "PengYang",
              shortname: "彭阳县"
            }
          ],
          shortname: "固原市"
        },
        {
          pinyin: "ZhongWei",
          children: [
            {
              pinyin: "ZhongWei",
              shortname: "市辖区"
            },
            {
              pinyin: "ShaPoTou",
              shortname: "沙坡头区"
            },
            {
              pinyin: "ZhongNing",
              shortname: "中宁县"
            },
            {
              pinyin: "HaiYuan",
              shortname: "海原县"
            }
          ],
          shortname: "中卫市"
        }
      ],
      shortname: "宁夏回族自治区"
    },
    {
      pinyin: "XinJiang",
      children: [
        {
          pinyin: "WuLuMuQi",
          children: [
            {
              pinyin: "WuLuMuQi",
              shortname: "市辖区"
            },
            {
              pinyin: "TianShan",
              shortname: "天山区"
            },
            {
              pinyin: "ShaYiBaKe",
              shortname: "沙依巴克区"
            },
            {
              pinyin: "XinShiQu",
              shortname: "新市区"
            },
            {
              pinyin: "ShuiMoGou",
              shortname: "水磨沟区"
            },
            {
              pinyin: "TouTunHe",
              shortname: "头屯河区"
            },
            {
              pinyin: "DaBanCheng",
              shortname: "达坂城区"
            },
            {
              pinyin: "MiDong",
              shortname: "米东区"
            },
            {
              pinyin: "WuLuMuQi",
              shortname: "乌鲁木齐县"
            },
            {
              pinyin: "WuLuMuQiJingKaiQu",
              shortname: "乌鲁木齐经济技术开发区"
            },
            {
              pinyin: "WuLuMuQiGao",
              shortname: "乌鲁木齐高新技术产业开发区"
            }
          ],
          shortname: "乌鲁木齐市"
        },
        {
          pinyin: "KeLaMaYi",
          children: [
            {
              pinyin: "KeLaMaYi",
              shortname: "市辖区"
            },
            {
              pinyin: "DuShanZi",
              shortname: "独山子区"
            },
            {
              pinyin: "KeLaMaYi",
              shortname: "克拉玛依区"
            },
            {
              pinyin: "BaiJianTan",
              shortname: "白碱滩区"
            },
            {
              pinyin: "WuErHe",
              shortname: "乌尔禾区"
            }
          ],
          shortname: "克拉玛依市"
        },
        {
          pinyin: "TuLuFan",
          children: [
            {
              pinyin: "GaoChang",
              shortname: "高昌区"
            },
            {
              pinyin: "ShanShan",
              shortname: "鄯善县"
            },
            {
              pinyin: "TuoKeXun",
              shortname: "托克逊县"
            }
          ],
          shortname: "吐鲁番市"
        },
        {
          pinyin: "HaMi",
          children: [
            {
              pinyin: "YiZhou",
              shortname: "伊州区"
            },
            {
              pinyin: "BaLiKun",
              shortname: "巴里坤哈萨克自治县"
            },
            {
              pinyin: "YiWu",
              shortname: "伊吾县"
            }
          ],
          shortname: "哈密市"
        },
        {
          pinyin: "ChangJi",
          children: [
            {
              pinyin: "ChangJi",
              shortname: "昌吉市"
            },
            {
              pinyin: "FuKang",
              shortname: "阜康市"
            },
            {
              pinyin: "HuTuBi",
              shortname: "呼图壁县"
            },
            {
              pinyin: "MaNaSi",
              shortname: "玛纳斯县"
            },
            {
              pinyin: "QiTai",
              shortname: "奇台县"
            },
            {
              pinyin: "JiMuSaEr",
              shortname: "吉木萨尔县"
            },
            {
              pinyin: "MuLei",
              shortname: "木垒哈萨克自治县"
            }
          ],
          shortname: "昌吉回族自治州"
        },
        {
          pinyin: "BoErTaLa",
          children: [
            {
              pinyin: "BoLe",
              shortname: "博乐市"
            },
            {
              pinyin: "ALaShanKou",
              shortname: "阿拉山口市"
            },
            {
              pinyin: "JingHe",
              shortname: "精河县"
            },
            {
              pinyin: "WenQuan",
              shortname: "温泉县"
            }
          ],
          shortname: "博尔塔拉蒙古自治州"
        },
        {
          pinyin: "BaYinGuoLeng",
          children: [
            {
              pinyin: "KuErLe",
              shortname: "库尔勒市"
            },
            {
              pinyin: "LunTai",
              shortname: "轮台县"
            },
            {
              pinyin: "YuLi",
              shortname: "尉犁县"
            },
            {
              pinyin: "RuoQiang",
              shortname: "若羌县"
            },
            {
              pinyin: "QieMo",
              shortname: "且末县"
            },
            {
              pinyin: "YanQi",
              shortname: "焉耆回族自治县"
            },
            {
              pinyin: "HeJing",
              shortname: "和静县"
            },
            {
              pinyin: "HeShuo",
              shortname: "和硕县"
            },
            {
              pinyin: "BoHu",
              shortname: "博湖县"
            },
            {
              pinyin: "KuErLeJingKaiQu",
              shortname: "库尔勒经济技术开发区"
            }
          ],
          shortname: "巴音郭楞蒙古自治州"
        },
        {
          pinyin: "AKeSu",
          children: [
            {
              pinyin: "AKeSu",
              shortname: "阿克苏市"
            },
            {
              pinyin: "WenSu",
              shortname: "温宿县"
            },
            {
              pinyin: "KuChe",
              shortname: "库车县"
            },
            {
              pinyin: "ShaYa",
              shortname: "沙雅县"
            },
            {
              pinyin: "XinHe",
              shortname: "新和县"
            },
            {
              pinyin: "BaiCheng",
              shortname: "拜城县"
            },
            {
              pinyin: "WuShi",
              shortname: "乌什县"
            },
            {
              pinyin: "AWaTi",
              shortname: "阿瓦提县"
            },
            {
              pinyin: "KePing",
              shortname: "柯坪县"
            }
          ],
          shortname: "阿克苏地区"
        },
        {
          pinyin: "KeZiLeSu",
          children: [
            {
              pinyin: "ATuShi",
              shortname: "阿图什市"
            },
            {
              pinyin: "AKeTao",
              shortname: "阿克陶县"
            },
            {
              pinyin: "AHeQi",
              shortname: "阿合奇县"
            },
            {
              pinyin: "WuQia",
              shortname: "乌恰县"
            }
          ],
          shortname: "克孜勒苏柯尔克孜自治州"
        },
        {
          pinyin: "KaShi",
          children: [
            {
              pinyin: "KaShi",
              shortname: "喀什市"
            },
            {
              pinyin: "ShuFu",
              shortname: "疏附县"
            },
            {
              pinyin: "ShuLe",
              shortname: "疏勒县"
            },
            {
              pinyin: "YingJiSha",
              shortname: "英吉沙县"
            },
            {
              pinyin: "ZePu",
              shortname: "泽普县"
            },
            {
              pinyin: "ShaChe",
              shortname: "莎车县"
            },
            {
              pinyin: "YeCheng",
              shortname: "叶城县"
            },
            {
              pinyin: "MaiGeTi",
              shortname: "麦盖提县"
            },
            {
              pinyin: "YuePuHu",
              shortname: "岳普湖县"
            },
            {
              pinyin: "JiaShi",
              shortname: "伽师县"
            },
            {
              pinyin: "BaChu",
              shortname: "巴楚县"
            },
            {
              pinyin: "TaShenKuErGan",
              shortname: "塔什库尔干塔吉克自治县"
            }
          ],
          shortname: "喀什地区"
        },
        {
          pinyin: "HeTian",
          children: [
            {
              pinyin: "HeTian",
              shortname: "和田市"
            },
            {
              pinyin: "HeTian",
              shortname: "和田县"
            },
            {
              pinyin: "MoYu",
              shortname: "墨玉县"
            },
            {
              pinyin: "PiShan",
              shortname: "皮山县"
            },
            {
              pinyin: "LuoPu",
              shortname: "洛浦县"
            },
            {
              pinyin: "CeLe",
              shortname: "策勒县"
            },
            {
              pinyin: "YuTian",
              shortname: "于田县"
            },
            {
              pinyin: "MinFeng",
              shortname: "民丰县"
            }
          ],
          shortname: "和田地区"
        },
        {
          pinyin: "YiLi",
          children: [
            {
              pinyin: "YiNing",
              shortname: "伊宁市"
            },
            {
              pinyin: "KuiTun",
              shortname: "奎屯市"
            },
            {
              pinyin: "HuoErGuoSi",
              shortname: "霍尔果斯市"
            },
            {
              pinyin: "YiNing",
              shortname: "伊宁县"
            },
            {
              pinyin: "ChaBuChaEr",
              shortname: "察布查尔锡伯自治县"
            },
            {
              pinyin: "HuoCheng",
              shortname: "霍城县"
            },
            {
              pinyin: "GongLiu",
              shortname: "巩留县"
            },
            {
              pinyin: "XinYuan",
              shortname: "新源县"
            },
            {
              pinyin: "ZhaoSu",
              shortname: "昭苏县"
            },
            {
              pinyin: "TeKeSi",
              shortname: "特克斯县"
            },
            {
              pinyin: "NiLeKe",
              shortname: "尼勒克县"
            }
          ],
          shortname: "伊犁哈萨克自治州"
        },
        {
          pinyin: "TaCheng",
          children: [
            {
              pinyin: "TaCheng",
              shortname: "塔城市"
            },
            {
              pinyin: "WuSu",
              shortname: "乌苏市"
            },
            {
              pinyin: "EMin",
              shortname: "额敏县"
            },
            {
              pinyin: "ShaWan",
              shortname: "沙湾县"
            },
            {
              pinyin: "TuoLi",
              shortname: "托里县"
            },
            {
              pinyin: "YuMin",
              shortname: "裕民县"
            },
            {
              pinyin: "HeBuKeSaiEr",
              shortname: "和布克赛尔蒙古自治县"
            }
          ],
          shortname: "塔城地区"
        },
        {
          pinyin: "ALeTai",
          children: [
            {
              pinyin: "ALeTai",
              shortname: "阿勒泰市"
            },
            {
              pinyin: "BuErJin",
              shortname: "布尔津县"
            },
            {
              pinyin: "FuYun",
              shortname: "富蕴县"
            },
            {
              pinyin: "FuHai",
              shortname: "福海县"
            },
            {
              pinyin: "HaBaHe",
              shortname: "哈巴河县"
            },
            {
              pinyin: "QingHe",
              shortname: "青河县"
            },
            {
              pinyin: "JiMuNai",
              shortname: "吉木乃县"
            }
          ],
          shortname: "阿勒泰地区"
        },
        {
          pinyin: "XinJiang",
          children: [
            {
              pinyin: "ShiHeZi",
              shortname: "石河子市"
            },
            {
              pinyin: "ALaEr",
              shortname: "阿拉尔市"
            },
            {
              pinyin: "TuMuShuKe",
              shortname: "图木舒克市"
            },
            {
              pinyin: "WuJiaQu",
              shortname: "五家渠市"
            },
            {
              pinyin: "TieMenGuan",
              shortname: "铁门关市"
            }
          ],
          shortname: "直辖县"
        }
      ],
      shortname: "新疆维吾尔自治区"
    },
    {
      pinyin: "Hong Kong",
      children: [
        {
          pinyin: "Hong Kong Island",
          children: [
            {
              pinyin: "Central and Western District",
              shortname: "中西区"
            },
            {
              pinyin: "Wan Chai District",
              shortname: "湾仔区"
            },
            {
              pinyin: "Eastern District",
              shortname: "东区"
            },
            {
              pinyin: "Southern District",
              shortname: "南区"
            }
          ],
          shortname: "香港岛"
        },
        {
          pinyin: "Kowloon",
          children: [
            {
              pinyin: "Yau Tsim Mong",
              shortname: "油尖旺区"
            },
            {
              pinyin: "Sham Shui Po",
              shortname: "深水埗区"
            },
            {
              pinyin: "Jiulongcheng",
              shortname: "九龙城区"
            },
            {
              pinyin: "Wong Tai Sin",
              shortname: "黄大仙区"
            },
            {
              pinyin: "Kwun Tong",
              shortname: "观塘区"
            }
          ],
          shortname: "九龙"
        },
        {
          pinyin: "New Territories",
          children: [
            {
              pinyin: "Tsuen Wan",
              shortname: "荃湾区"
            },
            {
              pinyin: "Tuen Mun",
              shortname: "屯门区"
            },
            {
              pinyin: "Yuen Long",
              shortname: "元朗区"
            },
            {
              pinyin: "North District",
              shortname: "北区"
            },
            {
              pinyin: "Tai Po",
              shortname: "大埔区"
            },
            {
              pinyin: "Sai Kung",
              shortname: "西贡区"
            },
            {
              pinyin: "Sha Tin",
              shortname: "沙田区"
            },
            {
              pinyin: "Kwai Tsing",
              shortname: "葵青区"
            },
            {
              pinyin: "Outlying Islands",
              shortname: "离岛区"
            }
          ],
          shortname: "新界"
        }
      ],
      shortname: "香港特别行政区"
    },
    {
      pinyin: "Macau",
      children: [
        {
          pinyin: "MacauPeninsula",
          children: [
            {
              pinyin: "Nossa Senhora de Fatima",
              shortname: "花地玛堂区"
            },
            {
              pinyin: "Santo Antonio",
              shortname: "圣安多尼堂区"
            },
            {
              pinyin: "Sé",
              shortname: "大堂区"
            },
            {
              pinyin: "Sao Lazaro",
              shortname: "望德堂区"
            },
            {
              pinyin: "Sao Lourenco",
              shortname: "风顺堂区"
            }
          ],
          shortname: "澳门半岛"
        },
        {
          pinyin: "Taipa",
          children: [
            {
              pinyin: "Our Lady Of Carmel's Parish",
              shortname: "嘉模堂区"
            }
          ],
          shortname: "氹仔岛"
        },
        {
          pinyin: "Coloane",
          children: [
            {
              pinyin: "St Francis Xavier's Parish",
              shortname: "圣方济各堂区"
            }
          ],
          shortname: "路环岛"
        }
      ],
      shortname: "澳门特别行政区"
    },
    {
      pinyin: "Taiwan",
      children: [
        {
          pinyin: "Changhua County",
          children: [
            {
              pinyin: "Fangyuan Township",
              shortname: "芳苑乡"
            },
            {
              pinyin: "Fenyuan Township",
              shortname: "芬园乡"
            },
            {
              pinyin: "Fuxing Township",
              shortname: "福兴乡"
            },
            {
              pinyin: "Hemei Township",
              shortname: "和美镇"
            },
            {
              pinyin: "Huatan Township",
              shortname: "花坛乡"
            },
            {
              pinyin: "Lukang Township",
              shortname: "鹿港镇"
            },
            {
              pinyin: "Pitou Township",
              shortname: "埤头乡"
            },
            {
              pinyin: "Puxin Township",
              shortname: "埔心乡"
            },
            {
              pinyin: "Puyan Township",
              shortname: "埔盐乡"
            },
            {
              pinyin: "Shengang Township",
              shortname: "伸港乡"
            },
            {
              pinyin: "Shetou Township",
              shortname: "社头乡"
            },
            {
              pinyin: "Tianwei Township",
              shortname: "田尾乡"
            },
            {
              pinyin: "Tianzhong Township",
              shortname: "田中镇"
            },
            {
              pinyin: "Xianxi Township",
              shortname: "线西乡"
            },
            {
              pinyin: "Xihu Township",
              shortname: "溪湖镇"
            },
            {
              pinyin: "Xiushui Township",
              shortname: "秀水乡"
            },
            {
              pinyin: "Xizhou Township",
              shortname: "溪州乡"
            },
            {
              pinyin: "Yongjing Township",
              shortname: "永靖乡"
            },
            {
              pinyin: "Yuanlin City",
              shortname: "员林市"
            },
            {
              pinyin: "Zhutang Township",
              shortname: "竹塘乡"
            },
            {
              pinyin: "Beidou Township",
              shortname: "北斗镇"
            },
            {
              pinyin: "Changhua City",
              shortname: "彰化市"
            },
            {
              pinyin: "Dacheng Township",
              shortname: "大城乡"
            },
            {
              pinyin: "Dacun Township",
              shortname: "大村乡"
            },
            {
              pinyin: "Erlin Township",
              shortname: "二林镇"
            },
            {
              pinyin: "Ershui Township",
              shortname: "二水乡"
            }
          ],
          shortname: "彰化县"
        },
        {
          pinyin: "Chiayi City",
          children: [
            {
              pinyin: "East Dist.",
              shortname: "东区"
            },
            {
              pinyin: "West Dist.",
              shortname: "西区"
            }
          ],
          shortname: "嘉义市"
        },
        {
          pinyin: "Chiayi County",
          children: [
            {
              pinyin: "Alishan Township",
              shortname: "阿里山乡"
            },
            {
              pinyin: "Budai Township",
              shortname: "布袋镇"
            },
            {
              pinyin: "Dalin Township",
              shortname: "大林镇"
            },
            {
              pinyin: "Dapu Township",
              shortname: "大埔乡"
            },
            {
              pinyin: "Dongshi Township",
              shortname: "东石乡"
            },
            {
              pinyin: "Fanlu Township",
              shortname: "番路乡"
            },
            {
              pinyin: "Liujiao Township",
              shortname: "六脚乡"
            },
            {
              pinyin: "Lucao Township",
              shortname: "鹿草乡"
            },
            {
              pinyin: "Meishan Township",
              shortname: "梅山乡"
            },
            {
              pinyin: "Minxiong Township",
              shortname: "民雄乡"
            },
            {
              pinyin: "Puzi City",
              shortname: "朴子市"
            },
            {
              pinyin: "Shuishang Township",
              shortname: "水上乡"
            },
            {
              pinyin: "Taibao City",
              shortname: "太保市"
            },
            {
              pinyin: "Xikou Township",
              shortname: "溪口乡"
            },
            {
              pinyin: "Xingang Township",
              shortname: "新港乡"
            },
            {
              pinyin: "Yizhu Township",
              shortname: "义竹乡"
            },
            {
              pinyin: "Zhongpu Township",
              shortname: "中埔乡"
            },
            {
              pinyin: "Zhuqi Township",
              shortname: "竹崎乡"
            }
          ],
          shortname: "嘉义县"
        },
        {
          pinyin: "Hsinchu City",
          children: [
            {
              pinyin: "Xiangshan Dist.",
              shortname: "香山区"
            },
            {
              pinyin: "East Dist.",
              shortname: "东区"
            },
            {
              pinyin: "North Dist.",
              shortname: "北区"
            }
          ],
          shortname: "新竹市"
        },
        {
          pinyin: "Hsinchu County",
          children: [
            {
              pinyin: "Emei Township",
              shortname: "峨眉乡"
            },
            {
              pinyin: "Guanxi Township",
              shortname: "关西镇"
            },
            {
              pinyin: "Hengshan Township",
              shortname: "横山乡"
            },
            {
              pinyin: "Hukou Township",
              shortname: "湖口乡"
            },
            {
              pinyin: "Jianshi Township",
              shortname: "尖石乡"
            },
            {
              pinyin: "Qionglin Township",
              shortname: "芎林乡"
            },
            {
              pinyin: "Wufeng Township",
              shortname: "五峰乡"
            },
            {
              pinyin: "Xinfeng Township",
              shortname: "新丰乡"
            },
            {
              pinyin: "Xinpu Township",
              shortname: "新埔镇"
            },
            {
              pinyin: "Zhubei City",
              shortname: "竹北市"
            },
            {
              pinyin: "Zhudong Township",
              shortname: "竹东镇"
            },
            {
              pinyin: "Baoshan Township",
              shortname: "宝山乡"
            },
            {
              pinyin: "Beipu Township",
              shortname: "北埔乡"
            }
          ],
          shortname: "新竹县"
        },
        {
          pinyin: "Hualien County",
          children: [
            {
              pinyin: "Zhuoxi Township",
              shortname: "卓溪乡"
            },
            {
              pinyin: "Fengbin Township",
              shortname: "丰滨乡"
            },
            {
              pinyin: "Fenglin Township",
              shortname: "凤林镇"
            },
            {
              pinyin: "Fuli Township",
              shortname: "富里乡"
            },
            {
              pinyin: "Guangfu Township",
              shortname: "光复乡"
            },
            {
              pinyin: "Hualien City",
              shortname: "花莲市"
            },
            {
              pinyin: "Ji’an Township",
              shortname: "吉安乡"
            },
            {
              pinyin: "Ruisui Township",
              shortname: "瑞穗乡"
            },
            {
              pinyin: "Shoufeng Township",
              shortname: "寿丰乡"
            },
            {
              pinyin: "Wanrong Township",
              shortname: "万荣乡"
            },
            {
              pinyin: "Xincheng Township",
              shortname: "新城乡"
            },
            {
              pinyin: "Xiulin Township",
              shortname: "秀林乡"
            },
            {
              pinyin: "Yuli Township",
              shortname: "玉里镇"
            }
          ],
          shortname: "花莲县"
        },
        {
          pinyin: "Kaohsiung City",
          children: [
            {
              pinyin: "Alian Dist.",
              shortname: "阿莲区"
            },
            {
              pinyin: "Daliao Dist.",
              shortname: "大寮区"
            },
            {
              pinyin: "Dashe Dist.",
              shortname: "大社区"
            },
            {
              pinyin: "Dashu Dist.",
              shortname: "大树区"
            },
            {
              pinyin: "Fengshan Dist.",
              shortname: "凤山区"
            },
            {
              pinyin: "Gangshan Dist.",
              shortname: "冈山区"
            },
            {
              pinyin: "Gushan Dist.",
              shortname: "鼓山区"
            },
            {
              pinyin: "Hunei Dist.",
              shortname: "湖内区"
            },
            {
              pinyin: "Jiaxian Dist.",
              shortname: "甲仙区"
            },
            {
              pinyin: "Lingya Dist.",
              shortname: "苓雅区"
            },
            {
              pinyin: "Linyuan Dist.",
              shortname: "林园区"
            },
            {
              pinyin: "Liugui Dist.",
              shortname: "六龟区"
            },
            {
              pinyin: "Luzhu Dist.",
              shortname: "路竹区"
            },
            {
              pinyin: "Maolin Dist.",
              shortname: "茂林区"
            },
            {
              pinyin: "Meinong Dist.",
              shortname: "美浓区"
            },
            {
              pinyin: "Mituo Dist.",
              shortname: "弥陀区"
            },
            {
              pinyin: "Namaxia Dist.",
              shortname: "那玛夏区"
            },
            {
              pinyin: "Nanzi Dist.",
              shortname: "楠梓区"
            },
            {
              pinyin: "Neimen Dist.",
              shortname: "内门区"
            },
            {
              pinyin: "Niaosong Dist.",
              shortname: "鸟松区"
            },
            {
              pinyin: "Qianjin Dist.",
              shortname: "前金区"
            },
            {
              pinyin: "Qianzhen Dist.",
              shortname: "前镇区"
            },
            {
              pinyin: "Qiaotou Dist.",
              shortname: "桥头区"
            },
            {
              pinyin: "Qieding Dist.",
              shortname: "茄萣区"
            },
            {
              pinyin: "Qijin Dist.",
              shortname: "旗津区"
            },
            {
              pinyin: "Qishan Dist.",
              shortname: "旗山区"
            },
            {
              pinyin: "Renwu Dist.",
              shortname: "仁武区"
            },
            {
              pinyin: "Sanmin Dist.",
              shortname: "三民区"
            },
            {
              pinyin: "Shanlin Dist.",
              shortname: "杉林区"
            },
            {
              pinyin: "Taoyuan Dist.",
              shortname: "桃源区"
            },
            {
              pinyin: "Tianliao Dist.",
              shortname: "田寮区"
            },
            {
              pinyin: "Xiaogang Dist.",
              shortname: "小港区"
            },
            {
              pinyin: "Xinxing Dist.",
              shortname: "新兴区"
            },
            {
              pinyin: "Yanchao Dist.",
              shortname: "燕巢区"
            },
            {
              pinyin: "Yancheng Dist.",
              shortname: "盐埕区"
            },
            {
              pinyin: "Yong’an Dist.",
              shortname: "永安区"
            },
            {
              pinyin: "Ziguan Dist.",
              shortname: "梓官区"
            },
            {
              pinyin: "Zuoying Dist.",
              shortname: "左营区"
            }
          ],
          shortname: "高雄市"
        },
        {
          pinyin: "Keelung City",
          children: [
            {
              pinyin: "Anle Dist.",
              shortname: "安乐区"
            },
            {
              pinyin: "Nuannuan Dist.",
              shortname: "暖暖区"
            },
            {
              pinyin: "Qidu Dist.",
              shortname: "七堵区"
            },
            {
              pinyin: "Ren’ai Dist.",
              shortname: "仁爱区"
            },
            {
              pinyin: "Xinyi Dist.",
              shortname: "信义区"
            },
            {
              pinyin: "Zhongshan Dist.",
              shortname: "中山区"
            },
            {
              pinyin: "Zhongzheng Dist.",
              shortname: "中正区"
            }
          ],
          shortname: "基隆市"
        },
        {
          pinyin: "Kinmen County",
          children: [
            {
              pinyin: "Jincheng Township",
              shortname: "金城镇"
            },
            {
              pinyin: "Jinhu Township",
              shortname: "金湖镇"
            },
            {
              pinyin: "Jinning Township",
              shortname: "金宁乡"
            },
            {
              pinyin: "Jinsha Township",
              shortname: "金沙镇"
            },
            {
              pinyin: "Lieyu Township",
              shortname: "烈屿乡"
            },
            {
              pinyin: "Wuqiu Township",
              shortname: "乌坵乡"
            }
          ],
          shortname: "金门县"
        },
        {
          pinyin: "Lienchiang County",
          children: [
            {
              pinyin: "Beigan Township",
              shortname: "北竿乡"
            },
            {
              pinyin: "Dongyin Township",
              shortname: "东引乡"
            },
            {
              pinyin: "Juguang Township",
              shortname: "莒光乡"
            },
            {
              pinyin: "Nangan Township",
              shortname: "南竿乡"
            }
          ],
          shortname: "连江县"
        },
        {
          pinyin: "Miaoli County",
          children: [
            {
              pinyin: "Touwu Township",
              shortname: "头屋乡"
            },
            {
              pinyin: "Xihu Township",
              shortname: "西湖乡"
            },
            {
              pinyin: "Yuanli Township",
              shortname: "苑里镇"
            },
            {
              pinyin: "Zaoqiao Township",
              shortname: "造桥乡"
            },
            {
              pinyin: "Zhunan Township",
              shortname: "竹南镇"
            },
            {
              pinyin: "Zhuolan Township",
              shortname: "卓兰镇"
            },
            {
              pinyin: "Dahu Township",
              shortname: "大湖乡"
            },
            {
              pinyin: "Gongguan Township",
              shortname: "公馆乡"
            },
            {
              pinyin: "Houlong Township",
              shortname: "后龙镇"
            },
            {
              pinyin: "Miaoli City",
              shortname: "苗栗市"
            },
            {
              pinyin: "Nanzhuang Township",
              shortname: "南庄乡"
            },
            {
              pinyin: "Sanwan Township",
              shortname: "三湾乡"
            },
            {
              pinyin: "Sanyi Township",
              shortname: "三义乡"
            },
            {
              pinyin: "Shitan Township",
              shortname: "狮潭乡"
            },
            {
              pinyin: "Tai’an Township",
              shortname: "泰安乡"
            },
            {
              pinyin: "Tongluo Township",
              shortname: "铜锣乡"
            },
            {
              pinyin: "Tongxiao Township",
              shortname: "通霄镇"
            },
            {
              pinyin: "Toufen City",
              shortname: "头份市"
            }
          ],
          shortname: "苗栗县"
        },
        {
          pinyin: "Nanhai Islands",
          children: [
            {
              pinyin: "Dongsha Islands",
              shortname: "东沙群岛"
            },
            {
              pinyin: "Nansha Islands",
              shortname: "南沙群岛"
            }
          ],
          shortname: "南海岛"
        },
        {
          pinyin: "Nantou County",
          children: [
            {
              pinyin: "Caotun Township",
              shortname: "草屯镇"
            },
            {
              pinyin: "Guoxing Township",
              shortname: "国姓乡"
            },
            {
              pinyin: "Jiji Township",
              shortname: "集集镇"
            },
            {
              pinyin: "Lugu Township",
              shortname: "鹿谷乡"
            },
            {
              pinyin: "Mingjian Township",
              shortname: "名间乡"
            },
            {
              pinyin: "Nantou City",
              shortname: "南投市"
            },
            {
              pinyin: "Puli Township",
              shortname: "埔里镇"
            },
            {
              pinyin: "Ren’ai Township",
              shortname: "仁爱乡"
            },
            {
              pinyin: "Shuili Township",
              shortname: "水里乡"
            },
            {
              pinyin: "Xinyi Township",
              shortname: "信义乡"
            },
            {
              pinyin: "Yuchi Township",
              shortname: "鱼池乡"
            },
            {
              pinyin: "Zhongliao Township",
              shortname: "中寮乡"
            },
            {
              pinyin: "Zhushan Township",
              shortname: "竹山镇"
            }
          ],
          shortname: "南投县"
        },
        {
          pinyin: "New Taipei City",
          children: [
            {
              pinyin: "Bali Dist.",
              shortname: "八里区"
            },
            {
              pinyin: "Banqiao Dist.",
              shortname: "板桥区"
            },
            {
              pinyin: "Gongliao Dist.",
              shortname: "贡寮区"
            },
            {
              pinyin: "Jinshan Dist.",
              shortname: "金山区"
            },
            {
              pinyin: "Linkou Dist.",
              shortname: "林口区"
            },
            {
              pinyin: "Luzhou Dist.",
              shortname: "芦洲区"
            },
            {
              pinyin: "Pinglin Dist.",
              shortname: "坪林区"
            },
            {
              pinyin: "Pingxi Dist.",
              shortname: "平溪区"
            },
            {
              pinyin: "Ruifang Dist.",
              shortname: "瑞芳区"
            },
            {
              pinyin: "Sanchong Dist.",
              shortname: "三重区"
            },
            {
              pinyin: "Sanxia Dist.",
              shortname: "三峡区"
            },
            {
              pinyin: "Sanzhi Dist.",
              shortname: "三芝区"
            },
            {
              pinyin: "Shenkeng Dist.",
              shortname: "深坑区"
            },
            {
              pinyin: "Shiding Dist.",
              shortname: "石碇区"
            },
            {
              pinyin: "Shimen Dist.",
              shortname: "石门区"
            },
            {
              pinyin: "Shuangxi Dist.",
              shortname: "双溪区"
            },
            {
              pinyin: "Shulin Dist.",
              shortname: "树林区"
            },
            {
              pinyin: "Taishan Dist.",
              shortname: "泰山区"
            },
            {
              pinyin: "Tamsui Dist.",
              shortname: "淡水区"
            },
            {
              pinyin: "Tucheng Dist.",
              shortname: "土城区"
            },
            {
              pinyin: "Wanli Dist.",
              shortname: "万里区"
            },
            {
              pinyin: "Wugu Dist.",
              shortname: "五股区"
            },
            {
              pinyin: "Wulai Dist.",
              shortname: "乌来区"
            },
            {
              pinyin: "Xindian Dist.",
              shortname: "新店区"
            },
            {
              pinyin: "Xinzhuang Dist.",
              shortname: "新庄区"
            },
            {
              pinyin: "Xizhi Dist.",
              shortname: "汐止区"
            },
            {
              pinyin: "Yingge Dist.",
              shortname: "莺歌区"
            },
            {
              pinyin: "Yonghe Dist.",
              shortname: "永和区"
            },
            {
              pinyin: "Zhonghe Dist.",
              shortname: "中和区"
            }
          ],
          shortname: "新北市"
        },
        {
          pinyin: "Penghu County",
          children: [
            {
              pinyin: "Baisha Township",
              shortname: "白沙乡"
            },
            {
              pinyin: "Huxi Township",
              shortname: "湖西乡"
            },
            {
              pinyin: "Magong City",
              shortname: "马公市"
            },
            {
              pinyin: "Qimei Township",
              shortname: "七美乡"
            },
            {
              pinyin: "Wang’an Township",
              shortname: "望安乡"
            },
            {
              pinyin: "Xiyu Township",
              shortname: "西屿乡"
            }
          ],
          shortname: "澎湖县"
        },
        {
          pinyin: "Pingtung County",
          children: [
            {
              pinyin: "Sandimen Township",
              shortname: "三地门乡"
            },
            {
              pinyin: "Shizi Township",
              shortname: "狮子乡"
            },
            {
              pinyin: "Taiwu Township",
              shortname: "泰武乡"
            },
            {
              pinyin: "Wandan Township",
              shortname: "万丹乡"
            },
            {
              pinyin: "Wanluan Township",
              shortname: "万峦乡"
            },
            {
              pinyin: "Wutai Township",
              shortname: "雾台乡"
            },
            {
              pinyin: "Xinpi Township",
              shortname: "新埤乡"
            },
            {
              pinyin: "Xinyuan Township",
              shortname: "新园乡"
            },
            {
              pinyin: "Yanpu Township",
              shortname: "盐埔乡"
            },
            {
              pinyin: "Zhutian Township",
              shortname: "竹田乡"
            },
            {
              pinyin: "Changzhi Township",
              shortname: "长治乡"
            },
            {
              pinyin: "Chaozhou Township",
              shortname: "潮州镇"
            },
            {
              pinyin: "Checheng Township",
              shortname: "车城乡"
            },
            {
              pinyin: "Chunri Township",
              shortname: "春日乡"
            },
            {
              pinyin: "Donggang Township",
              shortname: "东港镇"
            },
            {
              pinyin: "Fangliao Township",
              shortname: "枋寮乡"
            },
            {
              pinyin: "Fangshan Township",
              shortname: "枋山乡"
            },
            {
              pinyin: "Gaoshu Township",
              shortname: "高树乡"
            },
            {
              pinyin: "Hengchun Township",
              shortname: "恆春镇"
            },
            {
              pinyin: "Jiadong Township",
              shortname: "佳冬乡"
            },
            {
              pinyin: "Jiuru Township",
              shortname: "九如乡"
            },
            {
              pinyin: "Kanding Township",
              shortname: "崁顶乡"
            },
            {
              pinyin: "Laiyi Township",
              shortname: "来义乡"
            },
            {
              pinyin: "Ligang Township",
              shortname: "里港乡"
            },
            {
              pinyin: "Linbian Township",
              shortname: "林边乡"
            },
            {
              pinyin: "Linluo Township",
              shortname: "麟洛乡"
            },
            {
              pinyin: "Liuqiu Township",
              shortname: "琉球乡"
            },
            {
              pinyin: "Majia Township",
              shortname: "玛家乡"
            },
            {
              pinyin: "Manzhou Township",
              shortname: "满州乡"
            },
            {
              pinyin: "Mudan Township",
              shortname: "牡丹乡"
            },
            {
              pinyin: "Nanzhou Township",
              shortname: "南州乡"
            },
            {
              pinyin: "Neipu Township",
              shortname: "内埔乡"
            },
            {
              pinyin: "Pingtung City",
              shortname: "屏东市"
            }
          ],
          shortname: "屏东县"
        },
        {
          pinyin: "Taichung City",
          children: [
            {
              pinyin: "Wuqi Dist.",
              shortname: "梧栖区"
            },
            {
              pinyin: "Wuri Dist.",
              shortname: "乌日区"
            },
            {
              pinyin: "Xinshe Dist.",
              shortname: "新社区"
            },
            {
              pinyin: "Xitun Dist.",
              shortname: "西屯区"
            },
            {
              pinyin: "Beitun Dist.",
              shortname: "北屯区"
            },
            {
              pinyin: "Central Dist.",
              shortname: "中区"
            },
            {
              pinyin: "Dadu Dist.",
              shortname: "大肚区"
            },
            {
              pinyin: "Dajia Dist.",
              shortname: "大甲区"
            },
            {
              pinyin: "Dali Dist.",
              shortname: "大里区"
            },
            {
              pinyin: "Daya Dist.",
              shortname: "大雅区"
            },
            {
              pinyin: "Da’an Dist.",
              shortname: "大安区"
            },
            {
              pinyin: "Dongshi Dist.",
              shortname: "东势区"
            },
            {
              pinyin: "East Dist.",
              shortname: "东区"
            },
            {
              pinyin: "Fengyuan Dist.",
              shortname: "丰原区"
            },
            {
              pinyin: "Heping Dist.",
              shortname: "和平区"
            },
            {
              pinyin: "Houli Dist.",
              shortname: "后里区"
            },
            {
              pinyin: "Longjing Dist.",
              shortname: "龙井区"
            },
            {
              pinyin: "Nantun Dist.",
              shortname: "南屯区"
            },
            {
              pinyin: "North Dist.",
              shortname: "北区"
            },
            {
              pinyin: "Qingshui Dist.",
              shortname: "清水区"
            },
            {
              pinyin: "Shalu Dist.",
              shortname: "沙鹿区"
            },
            {
              pinyin: "Shengang Dist.",
              shortname: "神冈区"
            },
            {
              pinyin: "Shigang Dist.",
              shortname: "石冈区"
            },
            {
              pinyin: "South Dist.",
              shortname: "南区"
            },
            {
              pinyin: "Taiping Dist.",
              shortname: "太平区"
            },
            {
              pinyin: "Tanzi Dist.",
              shortname: "潭子区"
            },
            {
              pinyin: "Waipu Dist.",
              shortname: "外埔区"
            },
            {
              pinyin: "West Dist.",
              shortname: "西区"
            },
            {
              pinyin: "Wufeng Dist.",
              shortname: "雾峰区"
            }
          ],
          shortname: "台中市"
        },
        {
          pinyin: "Tainan City",
          children: [
            {
              pinyin: "Jiali Dist.",
              shortname: "佳里区"
            },
            {
              pinyin: "Jiangjun Dist.",
              shortname: "将军区"
            },
            {
              pinyin: "Liujia Dist.",
              shortname: "六甲区"
            },
            {
              pinyin: "Liuying Dist.",
              shortname: "柳营区"
            },
            {
              pinyin: "Longqi Dist.",
              shortname: "龙崎区"
            },
            {
              pinyin: "Madou Dist.",
              shortname: "麻豆区"
            },
            {
              pinyin: "Nanhua Dist.",
              shortname: "南化区"
            },
            {
              pinyin: "Nanxi Dist.",
              shortname: "楠西区"
            },
            {
              pinyin: "North Dist.",
              shortname: "北区"
            },
            {
              pinyin: "Qigu Dist.",
              shortname: "七股区"
            },
            {
              pinyin: "Rende Dist.",
              shortname: "仁德区"
            },
            {
              pinyin: "Shanhua Dist.",
              shortname: "善化区"
            },
            {
              pinyin: "Shanshang Dist.",
              shortname: "山上区"
            },
            {
              pinyin: "South Dist.",
              shortname: "南区"
            },
            {
              pinyin: "West Central Dist.",
              shortname: "中西区"
            },
            {
              pinyin: "Xiaying Dist.",
              shortname: "下营区"
            },
            {
              pinyin: "Xigang Dist.",
              shortname: "西港区"
            },
            {
              pinyin: "Xinhua Dist.",
              shortname: "新化区"
            },
            {
              pinyin: "Xinshi Dist.",
              shortname: "新市区"
            },
            {
              pinyin: "Xinying Dist.",
              shortname: "新营区"
            },
            {
              pinyin: "Xuejia Dist.",
              shortname: "学甲区"
            },
            {
              pinyin: "Yanshui Dist.",
              shortname: "盐水区"
            },
            {
              pinyin: "Yongkang Dist.",
              shortname: "永康区"
            },
            {
              pinyin: "Yujing Dist.",
              shortname: "玉井区"
            },
            {
              pinyin: "Zuozhen Dist.",
              shortname: "左镇区"
            },
            {
              pinyin: "Anding Dist.",
              shortname: "安定区"
            },
            {
              pinyin: "Annan Dist.",
              shortname: "安南区"
            },
            {
              pinyin: "Anping Dist.",
              shortname: "安平区"
            },
            {
              pinyin: "Baihe Dist.",
              shortname: "白河区"
            },
            {
              pinyin: "Beimen Dist.",
              shortname: "北门区"
            },
            {
              pinyin: "Danei Dist.",
              shortname: "大内区"
            },
            {
              pinyin: "Dongshan Dist.",
              shortname: "东山区"
            },
            {
              pinyin: "East Dist.",
              shortname: "东区"
            },
            {
              pinyin: "Guanmiao Dist.",
              shortname: "关庙区"
            },
            {
              pinyin: "Guantian Dist.",
              shortname: "官田区"
            },
            {
              pinyin: "Guiren Dist.",
              shortname: "归仁区"
            },
            {
              pinyin: "Houbi Dist.",
              shortname: "后壁区"
            }
          ],
          shortname: "台南市"
        },
        {
          pinyin: "Taipei City",
          children: [
            {
              pinyin: "Beitou Dist.",
              shortname: "北投区"
            },
            {
              pinyin: "Datong Dist.",
              shortname: "大同区"
            },
            {
              pinyin: "Da’an Dist.",
              shortname: "大安区"
            },
            {
              pinyin: "Nangang Dist.",
              shortname: "南港区"
            },
            {
              pinyin: "Neihu Dist.",
              shortname: "内湖区"
            },
            {
              pinyin: "Shilin Dist.",
              shortname: "士林区"
            },
            {
              pinyin: "Songshan Dist.",
              shortname: "松山区"
            },
            {
              pinyin: "Wanhua Dist.",
              shortname: "万华区"
            },
            {
              pinyin: "Wenshan Dist.",
              shortname: "文山区"
            },
            {
              pinyin: "Xinyi Dist.",
              shortname: "信义区"
            },
            {
              pinyin: "Zhongshan Dist.",
              shortname: "中山区"
            },
            {
              pinyin: "Zhongzheng Dist.",
              shortname: "中正区"
            }
          ],
          shortname: "台北市"
        },
        {
          pinyin: "Taitung County",
          children: [
            {
              pinyin: "Beinan Township",
              shortname: "卑南乡"
            },
            {
              pinyin: "Changbin Township",
              shortname: "长滨乡"
            },
            {
              pinyin: "Chenggong Township",
              shortname: "成功镇"
            },
            {
              pinyin: "Chishang Township",
              shortname: "池上乡"
            },
            {
              pinyin: "Daren Township",
              shortname: "达仁乡"
            },
            {
              pinyin: "Dawu Township",
              shortname: "大武乡"
            },
            {
              pinyin: "Donghe Township",
              shortname: "东河乡"
            },
            {
              pinyin: "Guanshan Township",
              shortname: "关山镇"
            },
            {
              pinyin: "Haiduan Township",
              shortname: "海端乡"
            },
            {
              pinyin: "Jinfeng Township",
              shortname: "金峰乡"
            },
            {
              pinyin: "Lanyu Township",
              shortname: "兰屿乡"
            },
            {
              pinyin: "Ludao Township",
              shortname: "绿岛乡"
            },
            {
              pinyin: "Luye Township",
              shortname: "鹿野乡"
            },
            {
              pinyin: "Taimali Township",
              shortname: "太麻里乡"
            },
            {
              pinyin: "Taitung City",
              shortname: "台东市"
            },
            {
              pinyin: "Yanping Township",
              shortname: "延平乡"
            }
          ],
          shortname: "台东县"
        },
        {
          pinyin: "Taoyuan City",
          children: [
            {
              pinyin: "Bade Dist.",
              shortname: "八德区"
            },
            {
              pinyin: "Daxi Dist.",
              shortname: "大溪区"
            },
            {
              pinyin: "Dayuan Dist.",
              shortname: "大园区"
            },
            {
              pinyin: "Fuxing Dist.",
              shortname: "复兴区"
            },
            {
              pinyin: "Guanyin Dist.",
              shortname: "观音区"
            },
            {
              pinyin: "Guishan Dist.",
              shortname: "龟山区"
            },
            {
              pinyin: "Longtan Dist.",
              shortname: "龙潭区"
            },
            {
              pinyin: "Luzhu Dist.",
              shortname: "芦竹区"
            },
            {
              pinyin: "Pingzhen Dist.",
              shortname: "平镇区"
            },
            {
              pinyin: "Taoyuan Dist.",
              shortname: "桃园区"
            },
            {
              pinyin: "Xinwu Dist.",
              shortname: "新屋区"
            },
            {
              pinyin: "Yangmei Dist.",
              shortname: "杨梅区"
            },
            {
              pinyin: "Zhongli Dist.",
              shortname: "中坜区"
            }
          ],
          shortname: "桃园市"
        },
        {
          pinyin: "Yilan County",
          children: [
            {
              pinyin: "Datong Township",
              shortname: "大同乡"
            },
            {
              pinyin: "Diaoyutai",
              shortname: "钓鱼台"
            },
            {
              pinyin: "Dongshan Township",
              shortname: "冬山乡"
            },
            {
              pinyin: "Jiaoxi Township",
              shortname: "礁溪乡"
            },
            {
              pinyin: "Luodong Township",
              shortname: "罗东镇"
            },
            {
              pinyin: "Nan’ao Township",
              shortname: "南澳乡"
            },
            {
              pinyin: "Sanxing Township",
              shortname: "三星乡"
            },
            {
              pinyin: "Su’ao Township",
              shortname: "苏澳镇"
            },
            {
              pinyin: "Toucheng Township",
              shortname: "头城镇"
            },
            {
              pinyin: "Wujie Township",
              shortname: "五结乡"
            },
            {
              pinyin: "Yilan City",
              shortname: "宜兰市"
            },
            {
              pinyin: "Yuanshan Township",
              shortname: "员山乡"
            },
            {
              pinyin: "Zhuangwei Township",
              shortname: "壮围乡"
            }
          ],
          shortname: "宜兰县"
        },
        {
          pinyin: "Yunlin County",
          children: [
            {
              pinyin: "Baozhong Township",
              shortname: "褒忠乡"
            },
            {
              pinyin: "Beigang Township",
              shortname: "北港镇"
            },
            {
              pinyin: "Citong Township",
              shortname: "莿桐乡"
            },
            {
              pinyin: "Dapi Township",
              shortname: "大埤乡"
            },
            {
              pinyin: "Dongshi Township",
              shortname: "东势乡"
            },
            {
              pinyin: "Douliu City",
              shortname: "斗六市"
            },
            {
              pinyin: "Dounan Township",
              shortname: "斗南镇"
            },
            {
              pinyin: "Erlun Township",
              shortname: "二崙乡"
            },
            {
              pinyin: "Gukeng Township",
              shortname: "古坑乡"
            },
            {
              pinyin: "Huwei Township",
              shortname: "虎尾镇"
            },
            {
              pinyin: "Kouhu Township",
              shortname: "口湖乡"
            },
            {
              pinyin: "Linnei Township",
              shortname: "林内乡"
            },
            {
              pinyin: "Lunbei Township",
              shortname: "崙背乡"
            },
            {
              pinyin: "Mailiao Township",
              shortname: "麦寮乡"
            },
            {
              pinyin: "Shuilin Township",
              shortname: "水林乡"
            },
            {
              pinyin: "Sihu Township",
              shortname: "四湖乡"
            },
            {
              pinyin: "Taixi Township",
              shortname: "台西乡"
            },
            {
              pinyin: "Tuku Township",
              shortname: "土库镇"
            },
            {
              pinyin: "Xiluo Township",
              shortname: "西螺镇"
            },
            {
              pinyin: "Yuanchang Township",
              shortname: "元长乡"
            }
          ],
          shortname: "云林县"
        }
      ],
      shortname: "台湾省"
    }
  ]
};
