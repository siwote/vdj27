//对于cookies操作

import Cookies from 'js-cookie';
const { Base64 } = require('js-base64');
class Auth {
    getCookie(key) {
        const data = Cookies.get(key);

        if (data) {
            return Base64.decode(data);
        }
        return data;
    }

    setCookie(key, value) {
        return Cookies.set(key, Base64.encode(value));
    }

    removeCookie(key) {
        return Cookies.remove(key);
    }

    clearLogin() {
        this.removeCookie('Session-Id');
        this.removeCookie('User-Name');
        this.removeCookie('User-Id');
        this.removeCookie('User-Account');
        this.removeCookie('Current-Org-Id');
        this.removeCookie('Current-Org-Name');
        this.removeCookie('Current-Org-SorgName');
        this.removeCookie('Org-Id');
        this.removeCookie('compid');
        this.removeCookie('loginCenter');
        this.removeCookie('menuLayout');
        this.removeCookie('skin');
        this.removeCookie('emailLogin');
        this.removeCookie('phoneLogin');
        this.removeCookie('isUpdatePwd');
        this.removeCookie('isPageData');

        this.removeLocalStorage('clickMore');
        this.removeLocalStorage('siteM2Menu');
        this.removeLocalStorage('siteM2Btn');
        this.removeLocalStorage('currentOrgs');
        this.removeLocalStorage('treeData');
        this.removeLocalStorage('roleCodes');
        this.removeLocalStorage('avatar');

        this.removeLocalStorage('timeUpdatePwd');
        this.removeLocalStorage('btnclickId');



        this.removeCookie('Org-Names');
        this.removeCookie('Account');
        this.removeCookie('orgcode');
        this.removeCookie('Org-User');
        this.removeCookie('currorgan-Id');
        this.removeCookie('Organ-Name');
        this.removeCookie('Organ-id');

        this.removeCookie('second-orgfzdyid');
        this.removeCookie('custom-Id');
        this.removeCookie('Organ-Code');

        this.removeCookie('is-JsonData');
        this.removeCookie('User-Id');
        this.removeCookie('is-Chain');
        this.removeCookie('is-orgcode');
        this.removeCookie('accflag');
        this.removeCookie('post');
        this.removeLocalStorage('myID');
        this.removeLocalStorage('account');
        this.removeLocalStorage('phone');
        this.removeLocalStorage('siteJKMenu');

        this.removeLocalStorage('siteM2Menu');
        this.removeLocalStorage('siteM2Btn');
        this.removeLocalStorage('currentOrgs');
        this.removeLocalStorage('treeData');
        this.removeLocalStorage('roleCodes');
        this.removeLocalStorage('second-Name');
        this.removeLocalStorage('acc');
        this.removeLocalStorage('appTypes');

    }

    setLogin(data) {

        this.setCookie('User-Id', data.userid);
        this.setCookie('Session-Id', data.sessionid);
        this.setCookie('User-Name', data.username);
        this.setCookie('User-Account', data.loginAccount);
        this.setCookie('Org-Id', data.orgids);
        // type 0 单位  1组织
        this.setCookie('Current-Org-Id', data.currentOrg.orgid);
        this.setCookie('Current-Org-Name', data.currentOrg.orgname);
        this.setCookie('Current-Org-SorgName', data.currentOrg.sorgname);

        this.setCookie('compid', data.compid);
        // 是否认证
        this.setCookie('loginCenter', data.authentication);
        // 是否允许修改密码与圆密码相同
        this.setCookie('isUpdatePwd', data.updatePwd);

        this.setCookie('menuLayout', data.menuStyle);
        this.setCookie('skin', data.skin);

        this.setCookie('emailLogin', data.email ? data.email : '');
        this.setCookie('phoneLogin', data.phone ? data.phone : '');
        this.setLocalStorage('siteM2Btn', JSON.stringify(data.authButtons));
        this.setLocalStorage('currentOrgs', JSON.stringify(data.orgs));
        this.setLocalStorage('avatar', data.avatar);
        this.setLocalStorage('timeUpdatePwd', data.timeUpdatePwd); // 是否修改密码



        this.setCookie('Org-Names', data.orgnames);
        this.setCookie('Account', data.loginAccount);
        this.setCookie('orgcode', data.currentOrg.orgcode);
        this.setCookie('second-Name', data.currentOrg.orgname);
        this.setCookie('Org-User', data.accflag);
        this.setCookie('currorgan-Id', data.currorganid);
        this.setCookie('Organ-Name', data.currentOrg.orgname);
        this.setCookie('Organ-id', data.currentOrg.orgid);

        this.setLocalStorage('siteM2Btn', JSON.stringify(data.authButtons));
        this.setLocalStorage('currentOrgs', JSON.stringify(data.orgs));
    }


    setCookieFuc(data) {
        this.setCookie('Organ-Code', data.currentOrg.orgtypeid);
        this.setCookie('custom-Id', data.custom);// 32位
        this.setCookie('second-orgfzdyid', data.secondOrgan ? data.secondOrgan.orgfzdyid : data.secondOrgan);
        this.setCookie('post', data.post);
        this.setCookie('accflag', data.accflag);
    }


    setLocalStorage(key, data) {
        return localStorage.setItem(key, Base64.encode(data));
    }

    getLocalStorage(key) {
        const data = localStorage.getItem(key);

        if (data) {
            return Base64.decode(data);
        }
        return data;

    }

    removeLocalStorage(key) {
        return localStorage.removeItem(key);
    }
}

const auth = new Auth();

export default auth;