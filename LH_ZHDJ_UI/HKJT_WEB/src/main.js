// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Vue from 'vue'
import "babel-polyfill";
import Vue from "vue";

import App from "./App";
import store from "./store";

import auth from "@/utils/auth.js";

import util from "@/utils/index";
Vue.prototype.$util = util;
Vue.prototype.$auth = auth;
Vue.prototype.isHasPerm = true; // 是否开启权限

import ElementUI from "element-ui";
import "./style/index.scss";
import locale from "element-ui/lib/locale/lang/zh-CN";
Vue.use(ElementUI, {
  locale,
  size: "small"
});
import "@/permission";
Vue.config.productionTip = false;
import Icon from "vue2-svg-icon/Icon.vue";
Vue.component("icon", Icon);

import Print from "vue-print-nb";
Vue.use(Print); //注册打印

// 系统管理
import SysManager from "lh-Manager";
Vue.use(SysManager);
// 信息
import lhNews from "lh-News";
Vue.use(lhNews);
// 问卷
import ResultPage from "lh-result-page";
Vue.use(ResultPage);

//复制内容到剪贴板
import VueClipboard from "vue-clipboard2";
Vue.use(VueClipboard);


//2021 5-31 田斌要求去除应用管理
// import FormContainer from "lh-form-container";
// Vue.use(FormContainer);

//断点续传
import uploader from "vue-simple-uploader";
Vue.use(uploader);
//附文档
import VueEditor from "vue2-editor";
Vue.use(VueEditor);

import Vant from "vant";
import "vant/lib/index.css";
import router from "./router";
Vue.use(Vant);

// 按钮权限
Vue.prototype.hasPerm = function(permisson) {
  var btns = JSON.parse(auth.getLocalStorage("siteM2Btn"));
  var obj = "";
  if (!btns) {
    return false;
  } else {
    for (var i = 0; i < btns.length; i++) {
      if (btns[i].fcturl == permisson) {
        obj = btns[i];
        break;
      }
    }
    return obj;
  }
};

Vue.filter("dateTime", function(number, formatStr) {
  const formateArr = ["Y", "M", "D", "h", "m"];
  const returnArr = [];
  const date = new Date(number * 1000);
  let format = formatStr;

  returnArr.push(date.getFullYear());
  returnArr.push(
    date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1
  );
  returnArr.push(date.getDate() < 10 ? `0${date.getDate()}` : date.getDate());
  returnArr.push(
    date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()
  );
  returnArr.push(
    date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
  );

  for (const i in returnArr) {
    if (Object.prototype.hasOwnProperty.call(returnArr, i)) {
      format = format.replace(formateArr[i], returnArr[i]);
    }
  }
  return format;
});
//表单设计器输入框禁止输入[],''
Vue.prototype.nochar = function(value) {
  value = value.replace(/\[|]|,|'|'/g, "");
  return value;
};

// 防抖处理
Vue.directive("noMoreClick", {
  inserted(el, binding) {
    el.addEventListener("click", e => {
      el.classList.add("is-disabled");
      el.disabled = true;
      setTimeout(() => {
        el.disabled = false;
        el.classList.remove("is-disabled");
      }, 2000);
    });
  }
});

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => {
    return h(App);
  }
}).$mount("#app");

Vue.filter("date", function(time) {
  //过滤器  时间
  if (!time) {
    return time;
  } else {
    time = time.substring(0, 10);
    return time;
  }
});

Vue.directive("loadmore", {
  bind(el, binding) {
    var selectWrap = el.querySelector(".el-table__body-wrapper");
    selectWrap.addEventListener("scroll", function() {
      var sign = 100;
      var scrollDistance =
        this.scrollHeight - this.scrollTop - this.clientHeight;
      if (scrollDistance <= sign) {
        binding.value();
      }
    });
  }
});

// 若是没有开启Devtools工具，在开发环境中开启，在生产环境中关闭
if (process.env.NODE_ENV == "development") {
  Vue.config.devtools = true;
} else {
  Vue.config.devtools = false;
}
