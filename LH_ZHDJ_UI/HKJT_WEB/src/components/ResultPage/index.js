import ResultSuccess from './index.vue'
import ResultFail from './ResultFail.vue'
import ResultError from './ResultError'
import ResultWarning from './ResultWarning'



import './result.scss'

ResultSuccess.install = function (Vue) {
    Vue.component(ResultSuccess.name, ResultSuccess)
}
ResultFail.install = function (Vue) {
    Vue.component(ResultFail.name, ResultFail)
}
const components = [
    ResultSuccess,
    ResultFail,
    ResultError,
    ResultWarning
]
const install = function (Vue) {
    components.forEach(component => {
        Vue.component(component.name, component)
    })
}
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}
export {
    install,
    ResultSuccess,
    ResultFail,
    ResultError,
    ResultWarning
}
export default {
    install,
    ResultSuccess,
    ResultFail,
    ResultError,
    ResultWarning
}