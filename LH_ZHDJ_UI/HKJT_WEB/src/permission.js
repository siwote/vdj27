import router from './router';
import NProgress from 'nprogress'; // Progress 进度条
import 'nprogress/nprogress.css'; // Progress 进度条样式
import auth from '@/utils/auth'; // 验权
import menu from '@/utils/menu';

const whiteList = ['/login', '/noRight', '/register', '/emailAuth']; // 不重定向白名单
import { loginToken, getAuthMenu, loginto, makeSessionDto } from '@/api/login';
import { loginFree } from '@/api/freeLogin';
import * as dd from 'dingtalk-jsapi';
const Base64 = require('js-base64').Base64;

//路由拦截器
let getRouter = [];
router.beforeEach((to, from, next) => {
  //判断是否免登陆
  if (to.query.account) {
    const params = {
      account: to.query.account,
      passwd: Base64.encode(123456),
      captcha: null,
      channel: 'PC',
      type: '0'
    };
    loginto(params)
      .then(res => {
        auth.clearLogin();
        if (res.code === '000000') {
          for (let i = 0; i < res.data.roleCodes.length; i++) {
            if (res.data.roleCodes[i] === 'ADMIN') {
              auth.setLocalStorage('roleCodes', true);
            }
          }
          // this.disabled = false;
          // this.isloading = false;
          auth.setLogin(res.data);
          makeSessionDto().then(res => {
            auth.setCookieFuc(res.data);
          });
          auth.setLocalStorage('appTypes', res.data.appTypes);

          // auth.clearLogin();
          // auth.setLogin(res.data);
          auth.setLocalStorage('acc', 1);

          getMenu(res.data.sessionid, to, 1, function (nexto) {
            next(nexto);
          });
        } else {
          auth.clearLogin();
          next('/login');
        }
      })
      .catch(() => {
        auth.clearLogin();
        next('/noRight');
      });
  } else if (to.query.param) {
    // 第三方账户免登陆
    const params = {
      param: to.query.param
    };
    loginFree(params)
      .then(res => {
        auth.clearLogin();
        if (res.code == '000000') {
          auth.setLogin(res.data);
          getMenu(res.data.sessionid, to, 1, function (nexto) {
            next({
              path: '/'
            });
          });
        } else {
          auth.clearLogin();
          next('/login');
        }
      })
      .catch(() => {
        auth.clearLogin();
        next('/notData');
      });
  } else {
    NProgress.start();
    // 判断是否登录
    // 如果已经登录
    if (auth.getCookie('Session-Id')) {
      // 判断菜单变量是否为空
      if (!getRouter || getRouter.length === 0) {
        // 如果菜单变量为空  判断是否有缓存菜单
        getRouter = auth.getLocalStorage('siteM2Menu')
          ? JSON.parse(auth.getLocalStorage('siteM2Menu'))
          : [];
        routerGo(to, from, next, getRouter);
      } else {
        if (to.path === '/login') {
          next({
            path: '/'
            // path: '/home'
          });
        } else {
          next();
        }
      }
    } else {
      auth.clearLogin();
      //不是钉钉打开
      if (dd.env.platform === 'notInDingTalk') {
        if (whiteList.indexOf(to.path) !== -1) {
          next();
        } else {
          // window.open('http://10.1.50.192:9060/mydesk/index', '_top');
          next('/login');
          NProgress.done();
        }
      } else {
        if (whiteList.indexOf(to.path) !== -1) {
          next();
        } else {
          isDD(to, next);
        }
      }
      // 从钉钉后台自动携带code
      // var str = window.location.search;
      // if (str.indexOf('code=') != -1) {
      //   urlCode = str.substr(str.lastIndexOf('code=') + 5);
      // }
    }
  }
});
router.afterEach(() => {
  NProgress.done(); // 结束Progress
});

function isDD(to, next) {
  dd.ready(() => {
    dd.runtime.permission.requestAuthCode({
      corpId: 'ding985378d8f0ba8f6735c2f4657eb6378f', // 企业id
      onSuccess(info) {
        const urlCode = info.code; // 通过该免登授权码可以获取用户身份
        //钉钉免登录
        const params = {
          account: '111111',
          passwd: '111111',
          captcha: '111111',
          channel: 'PC',
          token: urlCode,
          type: '4'
        };
        loginto(params)
          .then(res => {
            if (res.code === '000000') {
              auth.setLogin(res.data);
              getMenu(res.data.sessionid, to, 2, function (nexto) {
                next(nexto);
              });
            } else {
              next('/login');
            }
          })
          .catch(() => {
            next('/login');
            NProgress.done();
          });
      },
      onFail() {
        next('/login');
        NProgress.done();
      }
    });
  });
}

function routerGo(to, from, next, getRouter) {
  // 生成路由
  let hostRouters = menu.menuUtils(router.options.routes, getRouter);
  router.addRoutes(hostRouters);
  if (to.path === '/login') {
    next({
      path: '/'
      // path: '/home'
    });
  } else {
    next({
      path: to.path,
      query: to.query
    });
  }
}
function getMenu(sessionid, to, n, callBack) {
  const params = {
    channel: 'PC',
    userid: sessionid
  };
  getAuthMenu(params)
    .then(res => {
      let asyncRouterMap = res.data;
      if (res.code === '000000' && asyncRouterMap.length !== 0) {
        auth.removeLocalStorage('siteM2Menu');
        auth.setLocalStorage('siteM2Menu', JSON.stringify(res.data));
        let a = to.fullPath;
        if (n === 2) {
          callBack({
            path: to.path
          });
        } else {
          callBack({
            path: a.slice(0, a.indexOf('account') - 1)
          });
        }
      } else if (res.code === 'PW0000') {
        callBack('/noRight');
      } else {
        callBack('/noRight');
      }
    })
    .catch(() => {
      callBack('/noRight');
    });
}
