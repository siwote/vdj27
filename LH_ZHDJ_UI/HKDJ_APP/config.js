export default {
    // http://192.168.60.234:16677 测试环境
    // http://192.168.101.78:16667 正式环境
    // base_url: 'http://192.168.60.234:16799', // API地址
    // upload_img_url: 'http://192.168.60.234:16799/urc/attachment/addAttachment', // 图片上传接口
    base_url: 'http://10.1.50.247:9680', // API地址
    upload_img_url: 'http://10.1.50.247:9680/urc/attachment/addAttachment', // 李响
    // 如果打包成正式环境,这里需要改成客户正式的域名
    // base_url: 'http://zhdj.apcc2.cn:16667',
    // upload_img_url: 'http://zhdj.apcc2.cn:16667/urc/attachment/addAttachment',
};
