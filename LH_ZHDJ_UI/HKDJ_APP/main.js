import Vue from "vue";
import App from "./App";
Vue.config.productionTip = false;

import "./style/index.scss";
import "./style/meeting.scss";
import "./style/todo.scss";

// 请求模块
import $ajax from "./utils/ajax";
Vue.prototype.$ajax = $ajax;
// 常用工具
import $util from "./utils/index";
Vue.prototype.$util = $util;
// 上传图片插件
import $uploader from "./utils/uploader";
Vue.prototype.$uploader = $uploader;

// 按钮权限
Vue.prototype.hasPerm = function (permisson) {
  var btns = uni.getStorageSync("siteM2Btn");
  var obj = "";
  if (!btns) {
    return false;
  } else {
    for (var i = 0; i < btns.length; i++) {
      if (btns[i].fcturl == permisson) {
        obj = btns[i];
        break; 
      }
    }

    return obj;
  }
};

App.mpType = "app";

const app = new Vue({
  ...App,
});
app.$mount();
