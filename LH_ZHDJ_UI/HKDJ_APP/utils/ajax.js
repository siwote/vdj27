// this.$ajax.post('/urc/authentication/loginToken', {参数},'参数类型是json时传字符串或者true').then(res => {})
import config from '../config.js';

function request(url, data, method, headers) {
    var sessionid = uni.getStorageSync('sessionid');
    return new Promise((resolve, reject) => {
        uni.request({
            url: config.base_url + url,
            data: data,
            method: method,
            header: {
                sessionid: sessionid,
                'Content-Type': headers
                    ? 'application/json;charset=UTF-8'
                    : 'application/x-www-form-urlencoded;charset=UTF-8',
            },
            success(res) {
                resolve(res.data);
                if (res.data.code == 'PW00001') {
                    clearLogin();
                } else if (res.data.code != '000000') {
                    setTimeout(() => {
                        uni.showToast({
                            title: res.data.message,
                            icon: 'none',
                        });
                    }, 200);
                }
            },
            fail() {
                reject(false);
                uni.showToast({
                    title: '请检查网络',
                    icon: 'none',
                });
            },
            complete() {},
        });
    });
}

function clearLogin() {
    // uni.removeStorageSync('sessionid');
    // uni.removeStorageSync('username');
    // uni.removeStorageSync('account');
    // uni.removeStorageSync('password');
    // uni.removeStorageSync('orgids');
    // uni.removeStorageSync('orgid');
    // uni.removeStorageSync('pcode');
    // uni.removeStorageSync('roleCodes');
    // uni.removeStorageSync('custom');
    // uni.removeStorageSync('loginAccount');
    // uni.removeStorageSync('organcode');
    // uni.removeStorageSync('idcard');
    // uni.removeStorageSync('Partyrdsj');

    // uni.removeStorageSync('Organ-Code');
    // uni.removeStorageSync('post');
    // uni.removeStorageSync('accflag');
    // uni.removeStorageSync('loginTime');
    uni.clearStorage();
}

function isLogin(callback) {
    var sessionid = uni.getStorageSync('sessionid');
    if (sessionid) {
        callback(true);
    } else {
        callback(false);
    }
}

function setLogin(data) {
    uni.setStorageSync('sessionid', data.sessionid);
    uni.setStorageSync('username', data.username);
    uni.setStorageSync('userid', data.userid);
    uni.setStorageSync('curOrgid', data.currentOrg.orgid);
    uni.setStorageSync('curOrgname', data.currentOrg.orgname);
    uni.setStorageSync('siteM2Btn', data.authButtons);
    uni.setStorageSync('organcode', data.currentOrg.orgcode);
    uni.setStorageSync('loginAccount', data.loginAccount);
    uni.setStorageSync('orgids', data.orgids);
    if (data.roleCodes.indexOf('ADMIN') != -1) {
        uni.setStorageSync('roleCodes', true);
    }

    BaseInfo();
}
// 查询sessiondto
function setCookieFuc(data) {
    uni.setStorageSync('custom', data.custom);
    uni.setStorageSync('Organ-Code', data.currentOrg.orgtypeid);
    uni.setStorageSync('post', data.post);
    uni.setStorageSync('accflag', data.accflag);
    uni.setStorageSync('idcard', data.idcard);
}

function lgin(authButtons) {
    for (var i = 0; i < authButtons.length; i++) {
        if (authButtons[i].fctcode == 100226) {
            return true;
        }
    }
}

function loginToken(account, token, callback) {
    request(
        '/urc/authentication/loginToken',
        {
            account: account,
            channel: 'H5',
            token: token,
            type: 4,
        },
        'POST'
    ).then(res => {
        if (res && res.data) {
            if (lgin(res.data.authButtons)) {
                setLogin(res.data);
                callback();
            } else {
                setTimeout(() => {
                    uni.redirectTo({
                        url: '/pages/nolo/index?backShow=1',
                    });
                }, 200);
            }
        } else {
            if (res.message == '您没有操作权限，请联系管理员') {
                setTimeout(() => {
                    uni.redirectTo({
                        url: '/pages/nolo/index?backShow=1',
                    });
                }, 200);
            }
            clearLogin();
        }
    });
}

function ddNoLogin(urlCode) {
    request(
        '/urc/authentication/login',
        {
            account: '111111',
            passwd: '111111',
            captcha: '111111',
            channel: 'H5',
            token: urlCode,
            type: '4',
        },
        'POST'
    ).then(res => {
        if (res && res.data) {
            setLogin(res.data);
            uni.redirectTo({
                url: '/pages/index/index',
            });
        } else {
            clearLogin();
        }
    });
}

function login(params, callback) {
    request('/urc/new/authentication/login', params, 'POST').then(res => {
        if (res && res.data) {
            setLogin(res.data);
            callback(res);
            uni.reLaunch({
                url: '../main/main',
            });
        } else {
            callback(false);
            clearLogin();
        }
    });
}

function loginWithMessage(params, callback) {
    request('/urc/new/authentication/login', params, 'POST').then(res => {
        if (res && res.data) {
            setLogin(res.data);
            callback(res);
            uni.reLaunch({
                url: '../main/main',
            });
        } else {
            callback(false);
            clearLogin();
        }
    });
}
function BaseInfo(params, callback) {
    var params = {
        idNumber: uni.getStorageSync('loginAccount'),
    };
    request('/lhdj/member/partyBaseInfo', params, 'POST').then(res => {
        if (res && res.data) {
            uni.setStorageSync('Partyrdsj', res.data.rdsj);
            uni.setStorageSync('idcard', res.data.zjhm);
        }
    });
}
export default {
    async get(url, data, headers) {
        return uni.getStorageSync('sessionid') ? await request(url, data, 'GET', headers) : null;
    },
    async post(url, data, headers) {
        return uni.getStorageSync('sessionid') ? await request(url, data, 'POST', headers) : null;
    },
    async wxPost(url, data, headers) {
        return await request(url, data, 'POST', headers);
    },
      async Feepost(url, data, headers) {
        return  await request(url, data, 'POST', headers) ;
    },
    isLogin(callback) {
        return isLogin(callback);
    },
    clearLogin() {
        return clearLogin();
    },
    setLogin(data) {
        return setLogin(data);
    },
    loginToken(account, token, callback) {
        return loginToken(account, token, callback);
    },
    ddNoLogin(urlCode) {
        return ddNoLogin(urlCode);
    },
    login(params, callback) {
        return login(params, callback);
    },
    setCookieFuc(data) {
        return setCookieFuc(data);
    },
    loginWithMessage(params, callback) {
        return loginWithMessage(params, callback);
    },
};
