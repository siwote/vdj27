/*
	封装 uni-app 图片上传功能
	
  使用前先new 一下
  async choose_img_upload(n) {
    let uploader = new this.$uploader();
    let path_arr = await uploader.choose_and_upload(n); 
  }
	
	所有方法均返回 promise 对象 可使用then() 写后续业务 或 使用 async await
	
	choose	选择图片
		参数 num 为要选择的图片数量
	upload_one 上传一张图片
		参数 path  选择成功后返回的 缓存文件图片路径
	upload  上传多张图片
		参数 path_arr 选择图片成功后 返回的图片路径数组
	choose_and_upload  选择图片并上传
		参数 num 为要选择的图片数量
		
*/

// 引入配置信息或者自己创建个 config 对象
import config from "../config.js";
class Uploader {
  constructor() {

  }
  choose(num) {
    return new Promise((resolve, reject) => {
      uni.chooseImage({
        count: num,
        success(res) {
          // 缓存文件路径
          resolve(res.tempFilePaths)
        },
        fail(err) {
          reject(err)
        }
      })
    });
  }
  choosevideo(num) {
    return new Promise((resolve, reject) => {
      uni.chooseVideo({
        count: num,
        success(res) {
          resolve(res.tempFilePath)
        },
        fail(err) {
          reject(err)
        }
      })
    });
  }
  upload_one(path) {
    return new Promise((resolve, reject) => {
      uni.showLoading({
        title: '上传中'
      })
      uni.uploadFile({
        url: config.upload_img_url, //仅为示例，非真实的接口地址
        filePath: path,
        name: 'file',
        header: {
          'sessionid': uni.getStorageSync("sessionid")
        },
        success: (uploadFileRes) => {
          if ("string" === typeof (uploadFileRes.data)) {
            resolve(JSON.parse(uploadFileRes.data).data)
          } else {
            resolve(uploadFileRes.data.data)
          }

        },
        complete() {
          uni.hideLoading()
        }
      });
    });
  }
  upload(path_arr) {
    let num = path_arr.length;
    return new Promise(async (resolve, reject) => {
      let img_urls = []
      for (let i = 0; i < num; i++) {
        let img_url = await this.upload_one(path_arr[i]);
        img_urls.push(img_url)
      };
      resolve(img_urls)
    });


  }
  choose_and_upload(num) {
    return new Promise(async (resolve, reject) => {
      let path_arr = await this.choose(num);
      let img_urls = await this.upload(path_arr);
      resolve(img_urls);
    });
  }
  choosevideo_and_upload(num) {
    return new Promise(async (resolve, reject) => {
      let path_arr = await this.choosevideo(num);
      let img_urls = await this.upload_one(path_arr);
      resolve(img_urls);
    });
  }
}
export default Uploader;