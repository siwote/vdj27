class Funutil {
  // Base64 = require("js-base64").Base64;
  thisyear = this.formatDate(new Date(), "yyyy");
  formatDate(date, format) {
    if (!date) return;
    if (!format) format = "yyyy-MM-dd";
    switch (typeof date) {
      case "string":
        date = new Date(date.replace(/\-/g, "/"));
        break;
      case "number":
        date = new Date(date);
        break;
    }
    if (!date instanceof Date) return;
    var dict = {
      yyyy: date.getFullYear(),
      M: date.getMonth() + 1,
      d: date.getDate(),
      H: date.getHours(),
      m: date.getMinutes(),
      s: date.getSeconds(),
      MM: ("" + (date.getMonth() + 101)).substr(1),
      dd: ("" + (date.getDate() + 100)).substr(1),
      HH: ("" + (date.getHours() + 100)).substr(1),
      mm: ("" + (date.getMinutes() + 100)).substr(1),
      ss: ("" + (date.getSeconds() + 100)).substr(1),
    };
    return format.replace(/(yyyy|MM?|dd?|HH?|ss?|mm?)/g, function () {
      return dict[arguments[0]];
    });
  }
  getFiveYear() {
    var arr = [];
    for (var i = 0; i < 5; i++) {
      arr.push(this.thisyear - 2 + i);
    }
    return arr;
  }
  getTenYear() {
    var arr = [];
    for (var i = 0; i < 10; i++) {
      arr.push(this.thisyear - i);
    }
    return arr;
  }
  isRunY(year) {
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
      return true;
    } else {
      return false;
    }
  }
  dayByM(year, months) {
    var isr = this.isRunY(year);
    var days = 31;
    var dArr = [];
    switch (months) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        days = 31;
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        days = 30;
        break;
      case 2:
        days = isr ? 29 : 28;
        break;
    }
    for (var i = 1; i <= days; i++) {
      if (i < 10) {
        i = "0" + i;
      }
      dArr.push("" + i);
    }
    return dArr;
  }
  getTableData(dataArrary) {
    if (dataArrary.area.length != 0 || dataArrary.orgs.length != 0) {
      var data = [];
      data.push(...dataArrary.area);
      data.push(...dataArrary.orgs);
      data.forEach((element) => {
        if (dataArrary.stage) {
          for (let key in dataArrary.stage) {
            element.targetunit = dataArrary.stage.targetunit;
            if (key == "area" + element.id) {
              element.target = dataArrary.stage[key];
            } else if (key == "org" + element.subjectid) {
              element.target = dataArrary.stage[key];
            }
          }
        } else {
          for (let i = 0; i < dataArrary.table.length; i++) {
            for (let key in dataArrary.table[i]) {
              element.targetunit = dataArrary.table[i].targetunit;
              if (key == "area" + element.id) {
                element.target = dataArrary.table[i][key];
              } else if (key == "org" + element.subjectid) {
                element.target = dataArrary.table[i][key];
              }
            }
          }
        }
      });
      return data;
    }
  }
  hideDrawer(topics, y, that) {
    that.showRigth = false;
    that.topicids = "";
    if (topics && topics.length > 0) {
      that.topicids = topics
        .map((item) => {
          return item.id;
        })
        .join(",");
    }
    if (y) {
      that.years = y;
    }
    that.pageNo = 1;
    that.taskData = [];
    that.getListTask();
  }
  initTaskData(that) {
    that.years = this.thisyear;
    that.topicids = "";
    that.pageNo = 1;
    that.taskData = [];
    that.getListTask();
  }
  getStageInfo(obj) {
    for (let i = 0; i < obj.table.length; i++) {
      var data = [];
      data.push(...obj.area);
      data.push(...obj.orgs);
      obj.table[i].data = data;
    }
    for (let j = 0; j < obj.table.length; j++) {
      if (obj.table[j].data && obj.table[j].data.length != 0) {
        for (let key in obj.table[j]) {
          for (let k = 0; k < obj.table[j].data.length; k++) {
            if (key == "area" + obj.table[j].data[k].id) {
              obj.table[j].data[k].target = obj.table[j][key];
            }
            if (key == "org" + obj.table[j].data[k].subjectid) {
              obj.table[j].data[k].target = obj.table[j][key];
            }
          }
        }
      }
    }
    return obj.table;
  }
  deleteFile(file, that, t) {
    uni.showModal({
      content: "删除后不可恢复,是否确认删除",
      showCancel: true,
      success: (res) => {
        if (res.confirm) {
          that.$ajax
            .post(
              "/ots/attachment/removeAttachment",
              {
                fileid: file.fileid,
              },
              true
            )
            .then((res) => {
              if (res.code == "000000") {
                if (t == "deploy") {
                  that.deployForm.otsFileBusiness = that.deployForm.otsFileBusiness.filter(
                    (f) => f.fileid != file.fileid
                  );
                } else {
                  that.otsFileBusiness = that.otsFileBusiness.filter(
                    (f) => f.fileid != file.fileid
                  );
                }
              }
            });
        } else if (res.cancel) {
          return false;
        }
      },
    });
  }
  //树递归处理Type问题
  setTrees(arr) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].childList && arr[i].type == 0) {
        this.setTrees(arr[i].childList);
      } else {
        if (arr[i].type == 1) {
          arr.splice(i);
        }
      }
    }
    return arr;
  }
  ///获取会议属性得到相对应的值
  checkType(arr) {
    var data = [];
    arr.forEach((item, index) => {
      if (item == "theoretical_study") {
        arr[index] = "理论学习";
      } else if (item == "appraisal_of_members") {
        arr[index] = "民主评议党员";
      } else if (item == "org_life_meeting") {
        arr[index] = "组织生活会";
      }
    });
    arr = arr.join(",");
    return arr;
  }
  tagcode(arr) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] == "org_life_meeting") {
        arr[i] = "组织生活会";
      } else if (arr[i] == "appraisal_of_members") {
        arr[i] = "民主评议党员";
      } else if (arr[i] == "theoretical_study") {
        arr[i] = "理论学习";
      }
    }
    return arr;
  }

  tagcodename(arr) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] == "组织生活会") {
        arr[i] = "org_life_meeting";
      } else if (arr[i] == "民主评议党员") {
        arr[i] = "appraisal_of_members";
      } else if (arr[i] == "理论学习") {
        arr[i] = "theoretical_study";
      }
    }
    return arr;
  }

  typecode(val) {
    if (val == "branch_party_congress") {
      val = "支部党员大会";
    } else if (val == "branch_committee") {
      val = "支部委员会";
    } else if (val == "party_group_meeting") {
      val = "党小组会";
    } else if (val == "party_lecture") {
      val = "上党课";
    }
    return val;
  }
}

let util = new Funutil();
export default util;
