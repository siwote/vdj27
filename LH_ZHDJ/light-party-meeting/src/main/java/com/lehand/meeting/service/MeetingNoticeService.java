package com.lehand.meeting.service;

import com.alibaba.fastjson.JSONArray;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.cache.CacheComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.Constant;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.constant.common.MeetingPMtypeEnum;
import com.lehand.meeting.constant.common.MessageEnum;
import com.lehand.meeting.dto.*;
import com.lehand.meeting.pojo.MeetingAbsentSubject;
import com.lehand.meeting.pojo.MeetingNoticeRecord;
import com.lehand.meeting.pojo.MeetingRecordSubject;
import com.lehand.meeting.pojo.TkTaskRemindList;
import com.lehand.meeting.service.jobservice.SendMessageService;
import com.lehand.meeting.utils.WeChatUtils;
import com.lehand.module.appbuilder.dto.AppFunctionDto;
import com.lehand.module.appbuilder.service.AppFunctionService;
import com.lehand.module.common.pojo.SystemParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

@Service
public class MeetingNoticeService {
    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    MeetingService meetingService;
    @Resource
    CheckStandardService checkStandardService;
    @Resource
    WorkbenchService workbenchService;
    @Resource
    AppFunctionService appFunctionService;
    @Resource
    private SendMessageService sendMessageService;

    @Resource
    private CacheComponent cacheComponent;

    @Value("${wx_service_url}")
    private String url;

    /**
     * 新增会议通知
     *
     * @param session
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingNoticeRecord addMeetingNotice(Session session, MNoticeAddReq req) throws Exception {
        List<MeetingRecordSubject> meetingRecordSubjects = req.getMeetingRecordSubjects();
        if (meetingRecordSubjects == null || meetingRecordSubjects.size() < 1) {
            LehandException.throwException("通知对象为空");
        }

        if (StringUtils.isEmpty(req.getId())) {
            return doAddNotice(session, req, meetingRecordSubjects);
        }
        return updateMeetingNotice(session, req);
    }

    private MeetingNoticeRecordDto doAddNotice(Session session, MNoticeAddReq req, List<MeetingRecordSubject> meetingRecordSubjects) throws Exception {
        //新增通知
        MeetingNoticeRecordDto record = new MeetingNoticeRecordDto();
        BeanUtils.copyProperties(req, record);
        record.setCompid(session.getCompid());
        record.setCreateid(session.getLoginAccount());
        record.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        record.setSubnum(meetingRecordSubjects.size());
        Long noticeid = generalSqlComponent.insert(MeetingSqlCode.addMeetingNoticeRecord, record);
        record.setId(noticeid);
//        生成二维码code
        WeChatUtils wx = new WeChatUtils(cacheComponent);
//        System.out.println(wx.getToken());
        String ticketCode = wx.getLsTicket(noticeid.intValue());
        record.setRemarks1(ticketCode);
        //update 会议
        generalSqlComponent.update(MeetingSqlCode.updateMeetingNoticeRecord, record);


        //更新附件
        meetingService.updateAttachmentInfo(record.getId(), req.getMNoticeAttachList(), MagicConstant.NUM_STR.NUM_11, session);

        //新增通知对象
        AddNoticeSubject(meetingRecordSubjects, noticeid, session.getCompid());

        //发送通知
        AddRemindList(meetingRecordSubjects, record, session);

        //发送待办事项
        AddTodoList(meetingRecordSubjects, record, session.getCompid());

        //生成系统动态
        String month = record.getNoticetime().substring(5, 7);
        String dynamicstext = "召开" + month + "月" + record.getTypename();
        workbenchService.spliceSysParam(session.getCompid(), noticeid, session.getCurrentOrg().getOrgid(), session.getCurrentOrg().getOrgcode(),
                session.getCurrentOrg().getOrgname(), dynamicstext, "meet", "组织生活", session.getUserid());

        //处理关联关系 TODDO 下个版本
        List<String> tags = req.getTagcode();
        if (tags != null && tags.size() > 0) {
            List<String> tagids = meetingService.updateMeetingTags(noticeid, tags, req.getOrgcode(), session, 1);
            record.setTagcode(tagids);
        }

        if (StringUtils.isEmpty(req.getVoteflag())) {
            return record;
        }
        if (req.getVoteflag() == 1) {
            //保存投票关系
            createVote(record, req.getVoteflag(), JSONArray.toJSONString(req.getMeetingRecordSubjects()), session);
        }

        return record;
    }

    /**
     * 同步创建投票活动
     *
     * @param record
     * @param voteflag
     * @param subs
     */
    private void createVote(MeetingNoticeRecordDto record, int voteflag, String subs, Session session) throws Exception {
        AppFunctionDto dto = new AppFunctionDto();
        dto.setAppname(record.getTopic());
        //dto.setStarttime(record.getNoticetime());
        //dto.setOrgcode(session.getCurrentOrg().getOrgcode());
        dto.setStatus(0);
        dto.setAppurlstatus(1);
        dto.setTrieslimit(1);
        dto.setBizid(999L);
        //dto.setVotedsubjects(subs);
        String defaultForm = " {\"list\":[],\"config\":{\"formWidth\":\"70%\",\"labelWidth\":150,\"labelPosition\":\"right\",\"paddingLeft\":170,\"size\":\"small\",\"formClass\":\"\",\"storagetab\":\"\",\"oldstoragetab\":\"\"}}";
        dto.setForm(defaultForm);
        appFunctionService.saveAppFunction(dto, session);
        addVoteRelation(record.getCompid(), record.getId(), voteflag, 0, 0);
    }

    /**
     * 添加关联关系
     *
     * @param compid
     * @param id
     * @param voteflag
     * @param subjecttype
     * @param extra_attr
     */
    private void addVoteRelation(Long compid, Long id, int voteflag, int subjecttype, int extra_attr) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("compid", compid);
        param.put("subjecttype", subjecttype);
        param.put("subjectid", id);
        param.put("extra_attr", extra_attr);
        param.put("extra_attr_value", voteflag);
        generalSqlComponent.insert(MeetingSqlCode.addVoteRelation, param);
    }

    /**
     * 发送待办事项
     *
     * @param meetingRecordSubjects
     * @param record
     * @param compid
     */
    private void AddTodoList(List<MeetingRecordSubject> meetingRecordSubjects, MeetingNoticeRecordDto record, Long compid) {
        String month = record.getNoticetime().substring(5, 7);
        String msg = "参加" + month + "月" + record.getTypename() + "会议。";
        if (meetingRecordSubjects.size() > 0) {
            for (MeetingRecordSubject subject : meetingRecordSubjects) {
                String idcard = subject.getIdcard();
                if (StringUtils.isEmpty(idcard)) {
                    continue;
                }
                Map<String, Object> user = generalSqlComponent.query(MeetingSqlCode.queryUrcUserByIdno, new Object[]{idcard});
                if (user == null) {
                    continue;
                }
                Map<String, Object> param = new HashMap<>();
                param.put("compid", 1);
                param.put("busitype", 0);
                param.put("businessid", record.getId());
                param.put("mcode", record.getType());
                param.put("mcodename", record.getTypename());
                param.put("title", msg);
                param.put("content", "");
                param.put("customdata", "");
                param.put("ttype", 0);
                param.put("status", 0);
                param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
                param.put("senderid", 1);
                param.put("sendername", "系统管理员");
                param.put("updatetime", 0);
                param.put("remarks1", 0);
                param.put("remarks2", 0);
                param.put("remarks3", "");
                param.put("remarks4", "");
                param.put("recivuserid", user.get("userid"));
                param.put("recivusername", user.get("username"));
                //插入待办
                generalSqlComponent.insert(MeetingSqlCode.addMeetingToDoRecord, param);
            }
        }
    }

    /**
     * 发送通知
     *
     * @param meetingRecordSubjects
     * @param record
     * @param session
     */
    private void AddRemindList(List<MeetingRecordSubject> meetingRecordSubjects, MeetingNoticeRecordDto record, Session session) throws Exception {
        String msg = "【智慧党建】: 您好,请于" + record.getNoticetime() + "在" + record.getPlace() + "参加" + record.getTypename() + ",请准时参加!";
        Long compid = session.getCompid();
        if (meetingRecordSubjects.size() > 0) {
            String idcards = "";
            for (MeetingRecordSubject subject : meetingRecordSubjects) {
                idcards += "'" + subject.getIdcard() + "'" + ",";
                String idcard = subject.getIdcard();
                if (StringUtils.isEmpty(idcard)) {
                    continue;
                }
                Map<String, Object> user = generalSqlComponent.query(MeetingSqlCode.queryUrcUserByIdno, new Object[]{idcard});
                String phone = generalSqlComponent.query(SqlCode.getPhoneByIdCard, new Object[]{idcard});
                if (user == null || StringUtils.isEmpty(phone)) {
                    continue;
                }
                String userid = user.get("userid").toString();
                TkTaskRemindList ls = new TkTaskRemindList();
                ls.setCompid(compid);
                ls.setNoteid(-1L);
                ls.setModule(10);//模块(0:任务发布,1:任务反馈)
                ls.setMid(record.getId());//对应模块的ID
                ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format(new Date()));//提醒日期
                ls.setRulecode(MessageEnum.MNOTICE_DEPLOY.getCode());//编码
                ls.setRulename(MessageEnum.MNOTICE_DEPLOY.getMsg());
                ls.setOptuserid(0L);
                ls.setOptusername("系统消息");
                ls.setUserid(Long.parseLong(userid));//被提醒人
                ls.setUsername(subject.getUsername());//
                ls.setIsread(0);//
                ls.setIshandle(0);//
                ls.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
                ls.setMesg(msg);//消息
                ls.setRemarks1(1);//备注1
                ls.setRemarks2(0);//备注2
                ls.setRemarks3("组织生活");//备注3
                ls.setRemarks4(MagicConstant.STR.EMPTY_STR);//备注3
                ls.setRemarks5(MagicConstant.STR.EMPTY_STR);//备注3
                ls.setRemarks6(MagicConstant.STR.EMPTY_STR);//备注3
                ls.setRemark(record.getRemark());
                long id = generalSqlComponent.insert(MeetingSqlCode.addTTRLByTuihui, ls);
                SmsSendDto smsSendDto = new SmsSendDto();
                smsSendDto.setCompid(compid);
                smsSendDto.setUsermark(userid);
                smsSendDto.setPhone(phone);
                smsSendDto.setContent(msg);
                smsSendDto.setBusinessid(Long.parseLong(Constant.ADD_MEETING_NOTICE));
                sendMessageService.attendMeetingSms(smsSendDto);
                ls.setId(id);
            }
            idcards = idcards.substring(0, idcards.length() - 1);
            //NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+idcards+"&content="+msg+"&sendAcc="+"'"+session.getLoginAccount()+"'")));
        }
    }

    /**
     * 插入通知对象
     *
     * @param meetingRecordSubjects
     * @param compid
     * @return
     */
    public void AddNoticeSubject(List<MeetingRecordSubject> meetingRecordSubjects, Long noticeid, Long compid) {
        if (meetingRecordSubjects.size() > 0) {
            for (MeetingRecordSubject subject : meetingRecordSubjects) {
                subject.setCompid(compid);
                subject.setMrid(noticeid);
                subject.setBusitype(2);
//                subject.setUsertype("6");
                generalSqlComponent.insert(MeetingSqlCode.saveMeetingRecordSubject, subject);
            }
        }
    }

    /**
     * 取消会议通知
     *
     * @param session
     * @param noticeid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int cancelMeetingNotice(Session session, Long noticeid) {
        //校验会议是否已经开始了
        Map map = generalSqlComponent.query(MeetingSqlCode.getNoticeStatusById, new Object[]{session.getCompid(), noticeid, DateEnum.YYYYMMDDHHMMDD.format()});
        if (map == null) {
            LehandException.throwException("会议已经开始，撤回失败！");
        }

        //修改通知状态
        int upNotice = generalSqlComponent.update(MeetingSqlCode.updateMNoticeStatusById,
                new Object[]{1, session.getLoginAccount(), DateEnum.YYYYMMDDHHMMDD.format(new Date()), noticeid, session.getCompid()});
        //修改消息状态
        int upMesg = generalSqlComponent.update(MeetingSqlCode.updateRemidListHandleByMid, new Object[]{2, 10, noticeid, session.getCompid()});

        //撤回系统动态
        workbenchService.delSysDynamics(session.getCompid(), noticeid, session.getCurrentOrg().getOrgid());

        String msg = "【智慧党建】: 您好,原定于" + map.get("noticetime") + "在" + map.get("place") + "召开的" + map.get("typename") + "暂时取消,给您带来的不便敬请谅解!";

        //根据会议id获取参与人
        List<MeetingRecordSubject> meetingRecordSubjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMrid2,
                new Object[]{noticeid, 2, session.getCompid()});
        if (meetingRecordSubjects.size() > 0) {
            String idcards = "";
            for (MeetingRecordSubject subject : meetingRecordSubjects) {
                idcards += "'" + subject.getIdcard() + "'" + ",";
                String idcard = subject.getIdcard();
                if (StringUtils.isEmpty(idcard)) {
                    continue;
                }
                Map<String, Object> user = generalSqlComponent.query(MeetingSqlCode.queryUrcUserByIdno, new Object[]{idcard});
                String phone = generalSqlComponent.query(SqlCode.getPhoneByIdCard, new Object[]{idcard});
                if (user == null || StringUtils.isEmpty(phone)) {
                    continue;
                }
                String userid = user.get("userid").toString();
                SmsSendDto smsSendDto = new SmsSendDto();
                smsSendDto.setCompid(session.getCompid());
                smsSendDto.setUsermark(userid);
                smsSendDto.setPhone(phone);
                smsSendDto.setContent(msg);
                smsSendDto.setBusinessid(Long.parseLong(Constant.CANCEL_MEETING_NOTICE));
                sendMessageService.attendMeetingSms(smsSendDto);
            }
        }
        return upMesg;
    }

    /**
     * 修改会议通知
     *
     * @param session
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingNoticeRecordDto updateMeetingNotice(Session session, MNoticeAddReq req) throws Exception {
        List<MeetingRecordSubject> meetingRecordSubjects = req.getMeetingRecordSubjects();
        if (meetingRecordSubjects.size() < 1) {
            LehandException.throwException("通知对象为空");
        }
        if (StringUtils.isEmpty(req.getId())) {
            LehandException.throwException("通知ID为空");
        }

        MeetingNoticeRecordDto record = new MeetingNoticeRecordDto();
        BeanUtils.copyProperties(req, record);
        record.setCompid(session.getCompid());
        record.setUpdateid(session.getLoginAccount());
        record.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        record.setSubnum(meetingRecordSubjects.size());
        WeChatUtils wx = new WeChatUtils(cacheComponent);
//        System.err.println(wx.getToken()+"-wxtoken");
        String ticketCode = wx.getLsTicket(req.getId().intValue());
        record.setRemarks1(ticketCode);
        generalSqlComponent.insert(MeetingSqlCode.updateMeetingNoticeRecord, record);
        record.setId(req.getId());
        //更新附件
        meetingService.updateAttachmentInfo(record.getId(), req.getMNoticeAttachList(), MagicConstant.NUM_STR.NUM_11, session);

        //删除原通知对象
        generalSqlComponent.delete(MeetingSqlCode.delMeetingSubject, new Object[]{record.getId(), 2, session.getCompid()});
        //新增通知对象
        AddNoticeSubject(meetingRecordSubjects, record.getId(), session.getCompid());

        //发送通知
        AddRemindList(meetingRecordSubjects, record, session);

        //发送待办事项
        AddTodoList(meetingRecordSubjects, record, session.getCompid());

        //处理关联关系
        //删除原有关联关系
        generalSqlComponent.delete(MeetingSqlCode.deleteMeetingTagsByMeetingId, new Object[]{req.getId(), 1, session.getCompid()});
        List<String> tags = req.getTagcode();
        if (tags != null && tags.size() > 0) {
            List<String> tagids = meetingService.updateMeetingTags(req.getId(), tags, req.getOrgcode(), session, 1);
            record.setTagcode(tagids);
        }
        updateVoteRelation(record.getCompid(), record.getId(), req.getVoteflag(), 0, 0);
        return record;
    }

    /**
     * 修改投票关联
     *
     * @param compid
     * @param id
     * @param voteflag
     * @param subjecttype
     * @param extra_attr
     */
    private void updateVoteRelation(Long compid, Long id, int voteflag, int subjecttype, int extra_attr) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("compid", compid);
        param.put("subjecttype", subjecttype);
        param.put("subjectid", id);
        param.put("extra_attr", extra_attr);
        param.put("extra_attr_value", voteflag);
        generalSqlComponent.insert(MeetingSqlCode.updateVoteRelation, param);
    }

    /**
     * 根据ID查看通知内容
     *
     * @param session
     * @param noticeid
     * @return
     */
    public MNoticeInfoDto queryMeetingNotice(Session session, Long noticeid) {
        MNoticeInfoDto dto = generalSqlComponent.query(MeetingSqlCode.getMeetingNoticeById, new Object[]{noticeid, session.getCompid()});
        List<MeetingRecordSubject> meetingRecordSubjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMrid2,
                new Object[]{noticeid, 2, session.getCompid()});
        dto.setMeetingRecordSubjects(meetingRecordSubjects);
        //勾选项
        List<String> list = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{noticeid, 1, session.getCompid()});
        dto.setTagcode(list);
        dto.setMNoticeAttachList(meetingService.getAttachmentInfo(String.valueOf(noticeid), 3, MagicConstant.NUM_STR.NUM_11, session));
        String voteFlag = generalSqlComponent.query(MeetingSqlCode.getMeetingExtraAttr, new Object[]{session.getCompid(), 0, noticeid, 0});
        dto.setVoteflag(StringUtils.isEmpty(voteFlag) ? 0 : Integer.parseInt(voteFlag));
        return dto;
    }

    /*
     * 会议通知阅读状态
     *
     * @param session
     * @param noticeid
     * @return
     */
    public List<Map<String, Object>> checkmeetingNoticeStatus(Session session, Long noticeid) {
        return null;
    }

    /**
     * 会议被通知对象查看站内信内容
     *
     * @param session
     * @param noticeid
     * @return
     */
    public Object meetingNoticeContent(Session session, Long noticeid) {
        return null;
    }

    /**
     * 查看支部下会议通知列表
     *
     * @param session
     * @param req
     * @return
     */
    public Pager pageMeetingNotice(Session session, MNoticePageReq req) throws ParseException {
        Pager pager = req.getPager();
        req.setCompid(session.getCompid());
        if(req.getStarttime()!=null&&!"".equals(req.getStarttime())){
            req.setStarttime(req.getStarttime() + " 00:00:00");
            req.setEndtime(req.getEndtime() + " 23:59:59");
        }

        //关联会议记录 //20200608 因筛选条件格式变动和查询方式改变而改动
        List<String> tagcode = req.getTagcode();
        if (req.getTagcode() == null || tagcode.size() < 1) {
            pager = generalSqlComponent.pageQuery(MeetingSqlCode.pageMeetingByOrg, req, pager);
        } else {
            pager = generalSqlComponent.pageQuery(MeetingSqlCode.pageTagMNoticeByOrg, req, pager);
        }

        List<MNoticeListDto> list = (List<MNoticeListDto>) pager.getRows();
        if (list.size() < 1) {
            return pager;
        }
        //移动端存在状态查询所以要过滤一些不需要的数据
        Iterator<MNoticeListDto> iterator = list.iterator();
        while (iterator.hasNext()) {
            MNoticeListDto next = iterator.next();
            next.setRevokeble(booleanRevokeble(next.getNoticetime()));
            next.setVisible(false);
            next.setCheckIsEnd(checkMeetIsOver(session.getCompid(), next.getId()));
            String starttime = next.getNoticetime();
            //移动端过滤数据
            filterdata(req, iterator, next, starttime);
        }
        pager.setRows(list);
        return pager;
    }

    /***
     * 仅用于手机端民主生活会
     * @param session
     * @param req
     * @return
     * @throws ParseException
     */
    public Pager pageMeetingNotice2(Session session, MNoticePageReq req) throws ParseException {
        Pager pager = req.getPager();
        req.setCompid(session.getCompid());

        //关联会议记录 //20200608 因筛选条件格式变动和查询方式改变而改动
        List<String> tagcode = req.getTagcode();
        pager = generalSqlComponent.pageQuery(MeetingSqlCode.pageMeetingByOrg2, req, pager);

        List<MNoticeListDto> list = (List<MNoticeListDto>) pager.getRows();
        if (list.size() < 1) {
            return pager;
        }
        //移动端存在状态查询所以要过滤一些不需要的数据
        Iterator<MNoticeListDto> iterator = list.iterator();
        while (iterator.hasNext()) {
            MNoticeListDto next = iterator.next();
            next.setRevokeble(booleanRevokeble(next.getNoticetime()));
            next.setVisible(false);
            next.setCheckIsEnd(checkMeetIsOver(session.getCompid(), next.getId()));
            String starttime = next.getNoticetime();
            //移动端过滤数据
            filterdata(req, iterator, next, starttime);
        }
        pager.setRows(list);
        return pager;
    }

    /**
     * 移动端过滤数据
     *
     * @param req
     * @param iterator
     * @param next
     * @param starttime
     * @throws ParseException
     */
    private void filterdata(MNoticePageReq req, Iterator<MNoticeListDto> iterator, MNoticeListDto next, String starttime) throws ParseException {
        if (!StringUtils.isEmpty(req.getQuerystatus())) {
            switch (req.getQuerystatus()) {//移动端状态查询判断（移动端查询所有该字段传空字符串）
                case "0"://未开始
                    if ("1".equals(req.getFlag())) {//党员活动日入口进入
                        if (DateEnum.now().after(DateEnum.YYYYMMDDHHMM.parse(starttime))) {
                            iterator.remove();
                        }
                    } else {//会议入口或者民主生活会入口进入多一个过滤条件就是要过滤党员活动日的数据
                        if (DateEnum.now().after(DateEnum.YYYYMMDDHHMM.parse(starttime))
                                || MeetingPMtypeEnum.THEME_PARTY_DAY.getCode().equals(next.getType())) {
                            iterator.remove();
                        }
                    }
                    break;
                case "1"://进行中
                    if ("1".equals(req.getFlag())) {//党员活动日入口进入
                        if (next.getCheckIsEnd() || DateEnum.now().before(DateEnum.YYYYMMDDHHMM.parse(starttime))) {
                            iterator.remove();
                        }
                    } else {//会议入口或者民主生活会入口进入多一个过滤条件就是要过滤党员活动日的数据
                        if (next.getCheckIsEnd() || DateEnum.now().before(DateEnum.YYYYMMDDHHMM.parse(starttime)) || MeetingPMtypeEnum.THEME_PARTY_DAY.getCode().equals(next.getType())) {
                            iterator.remove();
                        }
                    }
                    break;
                case "2"://已结束
                    if ("1".equals(req.getFlag())) {//党员活动日入口进入
                        if (!next.getCheckIsEnd()) {
                            iterator.remove();
                        }
                    } else {//会议入口或者民主生活会入口进入多一个过滤条件就是要过滤党员活动日的数据
                        if (!next.getCheckIsEnd() || MeetingPMtypeEnum.THEME_PARTY_DAY.getCode().equals(next.getType())) {
                            iterator.remove();
                        }
                    }
                    break;
                default:
                    //移动端查询所有走这里
                    if ("0".equals(req.getFlag()) || "2".equals(req.getFlag())) {
                        //移动端要去党员活动日
                        if (MeetingPMtypeEnum.THEME_PARTY_DAY.getCode().equals(next.getType())) {
                            iterator.remove();
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 判断会议能否撤回
     *
     * @param noticetime
     * @return
     */
    private boolean booleanRevokeble(String noticetime) {
        try {
            Date noticedate = DateEnum.parseStr(noticetime, "yyyy-MM-dd HH:mm");
            Date nowDate = new Date();
            if (nowDate.after(noticedate)) {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 已读&未读人员名单
     *
     * @param id
     * @param session
     * @return
     */
    private Map<String, Object> getReadSub(Long id, Session session) {
        Map<String, Object> map = new HashMap<String, Object>(2);
        List<String> readedSub = generalSqlComponent.query(MeetingSqlCode.getMNoticeSubName,
                new HashMap<String, Object>() {{
                    put("mid", id);
                    put("compid", session.getCompid());
                    put("module", 10);
                    put("issend", 1);
                }});
        List<String> unreadedSub = generalSqlComponent.query(MeetingSqlCode.getMNoticeSubName, new HashMap<String, Object>() {{
            put("mid", id);
            put("compid", session.getCompid());
            put("module", 10);
            put("issend", 0);
        }});
        map.put("readedSub", readedSub);
        map.put("unreadedSub", unreadedSub);
        return map;
    }

    /**
     * 列表中阅读状态统计
     *
     * @param noticeid
     * @param session
     * @return
     */
    public Map<String, Object> getReadStatus(Long noticeid, Session session) {
        Map<String, Object> map = new HashMap<String, Object>(2);
        Long total = generalSqlComponent.query(MeetingSqlCode.countRemindListByNoticeId,
                new HashMap() {{
                    put("noticeid", noticeid);
                    put("compid", session.getCompid());
                }});
        Long readed = generalSqlComponent.query(MeetingSqlCode.countRemindListByNoticeId,
                new HashMap() {{
                    put("noticeid", noticeid);
                    put("compid", session.getCompid());
                    put("isRead", 1);
                }});
        map.put("total", total);
        map.put("readed", readed);
        return map;
    }

    /**
     * 复用会议通知的内容到会议中
     *
     * @param session
     * @param noticeid
     * @return
     */
    public MeetingRecordDto copyMNoticeToMeeting(Session session, Long noticeid) {
        MeetingRecordDto dto = new MeetingRecordDto();

        //获取会议内容
        MNoticeInfoDto notice = queryMeetingNotice(session, noticeid);
        if (notice == null) {
            return dto;
        }
        //勾选项
        List<String> list2 = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{noticeid, 1, session.getCompid()});
        dto.setTagcode(list2);
        //转换通知数据成会议数据
        dto.setMeetingAttachList(notice.getMNoticeAttachList());
        dto.setContext(notice.getRemark());
        dto.setTopic(notice.getTopic());
        dto.setPlace(notice.getPlace());
        dto.setMrtype(notice.getType());
        dto.setStarttime(notice.getNoticetime());
        //将会议通知的人员类别转换为会议的参会人类别
        /*List<MeetingRecordSubject> list = notice.getMeetingRecordSubjects();
        if (list==null||list.size() < 1) {
            return dto;
        }
        for (MeetingRecordSubject sub : list) {
            sub.setUsertype(MagicConstant.NUM_STR.NUM_0);
        }
        dto.setMeetingRecordSubjects(list);*/
        //将微信扫码签到的人员加到当前会议参与人
        List<MeetingRecordSubject> wxSignList = generalSqlComponent.query(MeetingSqlCode.getWxSignSubjects, new Object[]{session.getCompid(),noticeid,0,1});
        if(wxSignList == null || wxSignList.size() < 1){
            return dto;
        }
        wxSignList.forEach(wxSign->{
            String userid = wxSign.getUserid();
            dto.setMeetingRecordSubjects(wxSignList);
        });
        return dto;
    }

    public List<MeetingRecordSubject> getAttendSubByType(Session session, String type, String orgcode) {
        List<MeetingRecordSubject> result = new ArrayList<MeetingRecordSubject>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        list = meetingService.getMeetingSub(session, type, orgcode);
        if (list.size() < 1) {
            return result;
        }
        for (Map<String, Object> map : list) {
            MeetingRecordSubject sub = new MeetingRecordSubject();
            sub.setUsertype("6");
            sub.setIdcard(String.valueOf(map.get("zjhm")));
            sub.setBusitype(2);
            sub.setUserid(String.valueOf(map.get("userid")));
            sub.setUsername(String.valueOf(map.get("xm")));
            sub.setPost(String.valueOf(map.get("dnzwname")));
            result.add(sub);
        }
        return result;
    }

    /**
     * 关联会议通知时查询列表
     *
     * @param orgcode
     * @param mrtype
     * @param session
     * @return
     */
    public Pager listMeetingNotice(String orgcode, String mrtype, Session session, int day, Pager pager) {
        //判断是否是党员大会（党员大会包含标签的几种会议类型）
        if (mrtype.trim().equals("branch_party_congress")) {
            SystemParam systemParam = generalSqlComponent.query(MeetingSqlCode.getSysParam,
                    new Object[]{session.getCompid(), "branch_party_congress_array"});
            if (systemParam != null && !StringUtils.isEmpty(systemParam.getParamvalue())) {
                mrtype = systemParam.getParamvalue();
            }
        }

        //存在时限限制
        Date d = DateEnum.add(Calendar.MONTH, new Date(), day * (-1));

        Pager page = generalSqlComponent.pageQuery(MeetingSqlCode.queryNoticeByCodeAndType,
                new Object[]{orgcode, session.getCompid(), mrtype, DateEnum.YYYYMMDDHHMM.format(d)}, pager);
        PagerUtils<MNoticeAddReq> pagerUtils = new PagerUtils<MNoticeAddReq>(page);
        List<MNoticeAddReq> list = pagerUtils.getRows();
        for (MNoticeAddReq notice : list) {
            List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMrid,
                    new Object[]{notice.getId(), 2, session.getCompid()});
            notice.setMeetingRecordSubjects(subjects);
        }
        page.setRows(list);
        return page;
    }

    /**
     * 系统当前时间
     *
     * @param session
     * @return
     */
    public String getCurrentTime(Session session) {
        Date nowDate = new Date();
        String DateStr = DateEnum.parseDate(nowDate, "yyyy-MM-dd HH:mm:ss");
        return DateStr;
    }

    /**
     * 获取我的会议
     *
     * @param req
     * @param session
     * @return
     */
    public Pager pageMyMeeting(MeetingMyPageReq req, Session session) {
        req.setCompid(session.getCompid());
        Pager pager = req.getPager();
        pager = generalSqlComponent.pageQuery(MeetingSqlCode.getMyMeetingPage, req, req.getPager());
        List<MeetingMyDto> list = (List<MeetingMyDto>) pager.getRows();
        if (list.size() < 1) {
            return pager;
        }

        //判断能不能点击参加不参加
        for (MeetingMyDto dto : list) {
            dto.setRevokeble(booleanRevokeble(dto.getNoticetime()));
        }
        pager.setRows(list);
        return pager;
    }

    @Transactional(rollbackFor = Exception.class)
    public int toggleAttend(int status, Long id, String userid, Session session) throws ParseException {
        if (checkMeetIsOver(session.getCompid(), id)) {
            LehandException.throwException("会议已结束,请刷新页面！");
        }
        //首先处理待办
        generalSqlComponent.update(MeetingSqlCode.updateTotoByMeetingAttend, new Object[]{status, 0, id, session.getUserid(), session.getCompid()});
        return generalSqlComponent.update(MeetingSqlCode.updateMeetingAttendStatus, new Object[]{status, id, userid, session.getCompid()});
    }

    /**
     * 根据会议ID判断会议是否已经结束
     *
     * @param compid
     * @param meetid
     * @return
     * @throws ParseException
     */
    public Boolean checkMeetIsOver(Long compid, Long meetid) {
        //首先处理待办
        boolean flag = false;
        Map map = generalSqlComponent.query(MeetingSqlCode.getMeetingRecord, new Object[]{compid, meetid});

        if (map == null) {
            return flag;
        }
        if (StringUtils.isEmpty(map.get("endtime"))) {
            return flag;
        }
        try {
            if (DateEnum.YYYYMMDDHHMM.parse(map.get("endtime").toString()).before(DateEnum.YYYYMMDDHHMM.parse(DateEnum.YYYYMMDDHHMM.format()))) {
                return true;
            }
        } catch (Exception e) {
            return flag;
        }
        return flag;
    }


    /**
     * 新增会议前查询当前机构上一个会议的地点
     *
     * @return
     */
    public Map<String, Object> addBeforeQuery(Session session) {
        return generalSqlComponent.query(MeetingSqlCode.getUpMeeting, new Object[]{session.getCompid(), session.getLoginAccount()});
    }

    public List<MeetingRecordSubject> hyqxry(String noticeid, Session session, List<MeetingRecordSubject> subjects) {
        //应该参会人员
        List<MeetingRecordSubject> meetingRecordSubjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMrid,
                new Object[]{noticeid, 2, session.getCompid()});
        //实际参会人员
        Map<String, Object> map = new HashMap<String, Object>();
        for (MeetingRecordSubject li : subjects) {
            map.put(li.getUserid(), li);
        }

        //遍历出未参会的人员
        List<MeetingRecordSubject> list = new ArrayList<MeetingRecordSubject>();
        for (MeetingRecordSubject li : meetingRecordSubjects) {
            if (map.get(li.getUserid()) == null) {
                list.add(li);
            }
        }
        return list;
    }

    public  List<MeetingAbsentSubject>  getqxry(String noticeid,Session session){
        List<MeetingAbsentSubject> list  = generalSqlComponent.query(MeetingSqlCode.getMeetingAbsentSub,new Object[]{noticeid,session.getCompid()});
        return list;
    }
}
