package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.pojo.HornComment;
import com.lehand.meeting.pojo.HornCommentReply;
import com.lehand.meeting.service.MeetingCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(value = "会议评论", tags = { "会议评论" })
@RestController
@RequestMapping("/horn/comment")
public class CommentController extends BaseController {
    private static final Logger logger = LogManager.getLogger(CommentController.class);
    @Resource
    private MeetingCommentService meetingCommentService;


    @OpenApi(optflag=0)
    @ApiOperation(value = "会议评论查询", notes = "会议评论查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "moduleid", value = "会议id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
            @ApiImplicitParam(name = "mdcode", value = "会议类型", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/listHornComment")
    public Message listHornComment(Long moduleid, Long mdcode){
        Message msg = new Message();
        try{
            msg.setData(meetingCommentService.listHornComment(moduleid,mdcode, getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("会议评论查询错误");
        }
        return msg;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "保存会议评论", notes = "保存会议评论", httpMethod = "POST")
    @RequestMapping("/addHornComment")
    public Message addHornComment(@RequestBody HornComment hornComment) {
        Message msg = new Message();
        try {
            msg.setData(meetingCommentService.addHornComment(hornComment, getSession()));
            msg.success();
        } catch (Exception e) {
            msg.fail("保存会议评论信息失败！");
            e.printStackTrace();
        }
        return msg;
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "修改会议评论", notes = "修改会议评论", httpMethod = "POST")
    @RequestMapping("/modifyHornComment")
    public Message modifyHornComment(@RequestBody HornComment hornComment) {
        Message msg = new Message();
        try {
            msg.setData(meetingCommentService.modifyHornComment(hornComment, getSession()));
            msg.success();
        } catch (Exception e) {
            msg.fail("评论修改异常！");
            e.printStackTrace();
        }
        return msg;
    }

    @OpenApi(optflag=3)
    @ApiOperation(value = "删除会议评论", notes = "删除会议评论", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会议评论ID", required = true, dataType = "Long", defaultValue = "")
    })
    @RequestMapping("/removeHornComment")
    public Message removeHornComment(Long id) {
        Message msg = new Message();
        msg.setData(meetingCommentService.removeHornComment(id, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "保存会议评论回复", notes = "保存会议评论回复", httpMethod = "POST")
    @RequestMapping("/addHornCommentReply")
    public Message HornCommentReply(@RequestBody HornCommentReply hornCommentReply) {
        Message msg = new Message();
        try {
            msg.setData(meetingCommentService.addHornCommentReply(hornCommentReply, getSession()));
            msg.success();
        } catch (Exception e) {
            msg.fail("保存会议评论回复信息失败！");
            e.printStackTrace();
        }
        return msg;
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "修改会议评论回复", notes = "修改会议评论回复", httpMethod = "POST")
    @RequestMapping("/modifyHornCommentReply")
    public Message modifyHornCommentReply(@RequestBody HornCommentReply hornCommentReply) {
        Message msg = new Message();
        try {
            msg.setData(meetingCommentService.modifyHornCommentReply(hornCommentReply, getSession()));
            msg.success();
        } catch (Exception e) {
            msg.fail("评论回复修改异常！");
            e.printStackTrace();
        }
        return msg;
    }

    @OpenApi(optflag=3)
    @ApiOperation(value = "删除会议评论回复", notes = "删除会议评论回复", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会议评论回复ID", required = true, dataType = "Long", defaultValue = "")
    })
    @RequestMapping("/removeHornCommentReply")
    public Message removeHornCommentReply(Long id) {
        Message msg = new Message();
        msg.setData(meetingCommentService.removeHornCommentReply(id, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "会议评论和回复查询", notes = "会议评论和回复查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "moduleid", value = "会议id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
            @ApiImplicitParam(name = "mdcode", value = "会议类型", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/listHornCommentAndReply")
    public Message listHornCommentAndReply(Long moduleid,Long mdcode){
        Message msg = new Message();
        try{
            msg.setData(meetingCommentService.listHornCommentAndReply(moduleid,mdcode,getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("会议评论和回复查询错误");
        }
        return msg;
    }

}
