package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingRecordTopics;
import io.swagger.annotations.ApiModel;

/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class MeetingRecordTopicsDto extends MeetingRecordTopics {

    private static final long serialVersionUID = 1L;

}
