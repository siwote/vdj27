package com.lehand.meeting.constant;

/**
 * 常量
 * 
 * @author huruohan
 * @date 2019/05/09
 */
public class Constant {
    // 分类名称的最大长度
    public static final int MAX_DLTYPE_NAME = 33;
    // 文件夹名称的最大长度
    public static final int MAX_DLDATA_NAME = 100;
    // 文件名称的最大长度
    public static final int MAX_DLFILE_NAME=80;
    // 资料夹保存文件的最多数量
    public static final int MAX_DLFILE_SAVE=50;
    // 最高层级分类的pid=0
    public static final Long TOP_PID_DLTYPE = 0L;
    // 拖拽的类型3:在目标里面，1：在目标上面，2：在目标下面
    public static final String TYPE_1 = "1";
    public static final String TYPE_2 = "2";
    public static final String TYPE_3 = "3";
    
    //文件格式
    public static final String MP4="mp4";
    public static final String JPG="jpg";
    public static final String RAR="rar";
    public static final String ZIP="zip";
    public static final String DOC="doc";
    public static final String DOCX="docx";
    public static final String XLS="xls";
    public static final String XLSX="xlsx";
    public static final String PPT="ppt";
    public static final String PPTX="pptx";
    
    //资料管理器的businessname=0
    public static final short BUSINESSNAME_DATA=0;
    
    //小数点
    public static final String SPORT=".";
    
    //高拍仪
    public static final String GPY="gpy";
    
    //高拍仪上传的code的value值
    public static final String UPLOAD_PDF_CODE_VALUE="000000";
    
    //高拍仪上传的code的key
    public static final String UPLOAD_PDF_CODE_KEY="code";

    //短信发送的业务场景
    //参加会议
    public static final  String ADD_MEETING_NOTICE = "1";

    //取消会议
    public static final String CANCEL_MEETING_NOTICE = "2";

    //换届提醒
    public static final String CHANGE_TERM_REMIND= "5";

    //工作汇报,状态（ 0:删除,1:保存,2:提交(待审核),3:撤销,4:(审核通过),5:(审核驳回))
    /**
     * 0:删除
     */
    public static final Integer MEETING_REPORT_STATUS_ZERO= 0;
    /**
     * 1:保存
     */
    public static final Integer MEETING_REPORT_STATUS_ONE= 1;
    /**
     * 2:提交(待审核)
     */
    public static final Integer MEETING_REPORT_STATUS_TWO= 2;
    /**
     * 3:撤销,
     */
    public static final Integer MEETING_REPORT_STATUS_THREE= 3;
    /**
     * 4:(审核通过)
     */
    public static final Integer MEETING_REPORT_STATUS_FOUR= 4;
    /**
     * 5:(审核驳回)
     */
    public static final Integer MEETING_REPORT_STATUS_FIVE= 5;
}