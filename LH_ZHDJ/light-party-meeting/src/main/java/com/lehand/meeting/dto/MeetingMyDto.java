package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="我的会议列表", description="我的会议列表")
public class MeetingMyDto extends  MNoticeListDto{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="参会状态", name="pstatus")
    private int pstatus;

    @ApiModelProperty(value="实际参会状态", name="apstatus")
    private int apstatus;

    @ApiModelProperty(value="参会人id", name="userid")
    private String userid;

    @ApiModelProperty(value="参会人名", name="username")
    private String username;

    @ApiModelProperty(value="职务", name="post")
    private String post;

    @ApiModelProperty(value="职务名", name="postname")
    private String postname;

    @ApiModelProperty(value="身份证号码", name="idcard")
    private String idcard;

    public int getPstatus() {
        return pstatus;
    }

    public void setPstatus(int pstatus) {
        this.pstatus = pstatus;
    }

    public int getApstatus() {
        return apstatus;
    }

    public void setApstatus(int apstatus) {
        this.apstatus = apstatus;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPostname() {
        return postname;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
}
