package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.MeetingTalkSqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.dto.MeetingTalkRecordDto;
import com.lehand.meeting.pojo.MeetingRecordSubject;
import com.lehand.meeting.pojo.MeetingTalkRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("meetingTalkService")
public class MeetingTalkService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private MeetingService meetingService;

    /**
     * 新增数据,基础接口
     */
    public Long insert(MeetingTalkRecordDto meetingTalkRecordDto) {
        if(meetingTalkRecordDto.getCompid() == null){
            LehandException.throwException("新增的对象compid为空。");
        }
        Long id = generalSqlComponent.insert(MeetingTalkSqlCode.addMeetingTalkRecord,meetingTalkRecordDto);
        return id;
    }

    /**
     * 新增数据
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingTalkRecordDto addMeetingTalkRecord(Session session, MeetingTalkRecordDto meetingTalkRecordDto) {
        if(StringUtils.isEmpty(meetingTalkRecordDto.getTopic())) {
            LehandException.throwException("主题不能为空！");
        }
        if(StringUtils.isEmpty(meetingTalkRecordDto.getRecordtime())) {
            LehandException.throwException("记录时间不能为空！");
        }

        meetingTalkRecordDto.setCompid(session.getCompid());
        meetingTalkRecordDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingTalkRecordDto.setOrgid(String.valueOf(session.getCurrentOrg().getOrgid()));
        meetingTalkRecordDto.setOrgname(session.getCurrentOrg().getOrgname());
        meetingTalkRecordDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingTalkRecordDto.setCreateuserid(session.getUserid());
            if (null==meetingTalkRecordDto.getOrgcode()){
            meetingTalkRecordDto.setOrgcode(session.getCurrentOrg().getOrgcode());
        }

        Long talkId = insert(meetingTalkRecordDto);
        meetingTalkRecordDto.setId(talkId);

        //增加会议相关人信息
        addMeetingTalkMember(meetingTalkRecordDto, session);

        //更新附件
        meetingService.updateAttachmentInfo(talkId,meetingTalkRecordDto.getMeetingAttachList(),MagicConstant.NUM_STR.NUM_10,session);
        return meetingTalkRecordDto;
    }

    /**
     * 谈心谈话成员添加
     * @param meetingTalkRecordDto  谈心谈话主体信息
     */
    public void addMeetingTalkMember(MeetingTalkRecordDto meetingTalkRecordDto, Session session) {
        if(meetingTalkRecordDto.getMeetingRecordSubjects() == null) {
            LehandException.throwException("谈心谈话人员不能为空");
        }

        List<MeetingRecordSubject> subjects = meetingTalkRecordDto.getMeetingRecordSubjects();
        for(MeetingRecordSubject subject : subjects) {
            subject.setCompid(session.getCompid());
            subject.setMrid(meetingTalkRecordDto.getId());
            subject.setBusitype(1);
            if(StringUtils.isEmpty(subject.getUserid()) || StringUtils.isEmpty(subject.getUsername())) {
                LehandException.throwException("谈话人信息不能为空！");
            }
            generalSqlComponent.insert(MeetingSqlCode.saveMeetingRecordSubject, subject);
        }

    }

    /**
     * 根据id查询
     * @param meetingTalkRecord 谈话主体内容
     * @param session 会话
     */
    public MeetingTalkRecordDto getMeetingTalkRecord(Session session, MeetingTalkRecord meetingTalkRecord) {
        if(meetingTalkRecord == null || StringUtils.isEmpty(meetingTalkRecord.getId())) {
            LehandException.throwException("主键ID不能为空！");
        }

        MeetingTalkRecordDto result = generalSqlComponent.query(MeetingTalkSqlCode.getMeetingTalkRecordById,new Object[]{session.getCompid(), meetingTalkRecord.getId()});
        MeetingRecordSubject s = new MeetingRecordSubject();
        s.setCompid(session.getCompid());
        s.setBusitype(1);
        s.setMrid(meetingTalkRecord.getId());
        List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubject, s);
        result.setMeetingRecordSubjects(subjects);

        //附件
        result.setMeetingAttachList(meetingService.getAttachmentInfo(String.valueOf(meetingTalkRecord.getId()),3,MagicConstant.NUM_STR.NUM_10, session));
        return result;
    }

    /**
     * 多条件查询列表
     */
    public List<Map<String,Object>> listMeetingTalkRecord(Session session, MeetingTalkRecordDto meetingTalkRecordDto) {
        meetingTalkRecordDto.setCompid(session.getCompid());
        return generalSqlComponent.query(MeetingTalkSqlCode.listMeetingTalkRecord,meetingTalkRecordDto);
    }

    /**
     * 分页查询
     */
    public Pager pageMeetingTalkRecord(Session session, MeetingTalkRecordDto meetingTalkRecordDto, Pager pager) {
        meetingTalkRecordDto.setCompid(session.getCompid());
        Pager page = generalSqlComponent.pageQuery(MeetingTalkSqlCode.pageMeetingTalkRecord,meetingTalkRecordDto,pager);
        PagerUtils<MeetingTalkRecordDto> pagerUtils = new PagerUtils<MeetingTalkRecordDto>(page);
        MeetingRecordSubject s = new MeetingRecordSubject();
        s.setCompid(session.getCompid());
        s.setBusitype(1);
        for(MeetingTalkRecordDto record : pagerUtils.getRows()) {
            s.setMrid(record.getId());
            List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubject, s);
            record.setMeetingRecordSubjects(subjects);
        }
        return pagerUtils;
    }

    /**
     * 根据主键修改数据
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingTalkRecordDto modifyMeetingTalkRecord(Session session ,MeetingTalkRecordDto meetingTalkRecordDto) {
        if(StringUtils.isEmpty(meetingTalkRecordDto.getTopic())) {
            LehandException.throwException("主题不能为空！");
        }
        if(StringUtils.isEmpty(meetingTalkRecordDto.getRecordtime())) {
            LehandException.throwException("记录时间不能为空！");
        }

        meetingTalkRecordDto.setCompid(session.getCompid());
        meetingTalkRecordDto.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        meetingTalkRecordDto.setUpdateuserid(session.getUserid());
        MeetingTalkRecordDto old = getMeetingTalkRecord(session, meetingTalkRecordDto);
        if(old == null) {
            LehandException.throwException("该条记录已经不存在，重刷新后重试！");
        }
        generalSqlComponent.update(MeetingTalkSqlCode.modifyMeetingTalkRecordByid,meetingTalkRecordDto);

        //更新谈话人，先删除后添加
        MeetingRecordSubject sub = new MeetingRecordSubject();
        sub.setBusitype(1);
        sub.setCompid(session.getCompid());
        sub.setMrid(meetingTalkRecordDto.getId());
        generalSqlComponent.delete(MeetingSqlCode.delMeetingSubjectOpt, sub);
        addMeetingTalkMember(meetingTalkRecordDto, session);

        //更新附件
        meetingService.updateAttachmentInfo(meetingTalkRecordDto.getId(),meetingTalkRecordDto.getMeetingAttachList(),MagicConstant.NUM_STR.NUM_11,session);
        return meetingTalkRecordDto;
    }

    /**
     * 逻辑删除记录
     */
    @Transactional(rollbackFor = Exception.class)
    public int removeMeetingTalkRecord(Session session , Long id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id为空，不允许删除所有数据");
        }
        MeetingTalkRecordDto meetingTalkRecordDto = new MeetingTalkRecordDto();
        meetingTalkRecordDto.setId(id);
        meetingTalkRecordDto.setCompid(session.getCompid());
        MeetingTalkRecordDto old =  getMeetingTalkRecord(session, meetingTalkRecordDto);
        if(old == null) {
            LehandException.throwException("该条记录已经不存在，重刷新后重试！");
        }
        return generalSqlComponent.update(MeetingTalkSqlCode.updateTalkRecordStatus, new Object[] {meetingTalkRecordDto.getCompid(), meetingTalkRecordDto.getId()});
    }

}
