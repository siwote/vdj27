package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="会议记录扩展表", description="会议记录扩展表")
public class MeetingRecordExtend implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="", name="mrid")
    private Long mrid;

    @ApiModelProperty(value="组织机构ID", name="orglifeid")
    private Long orglifeid;

    @ApiModelProperty(value="组织机构名称", name="dcappraisalid")
    private Long dcappraisalid;

    @ApiModelProperty(value="", name="compid")
    private Long compid;

    @ApiModelProperty(value="备注1", name="mreremark1")
    private String mreremark1;

    @ApiModelProperty(value="备注2", name="mreremark2")
    private String mreremark2;

    @ApiModelProperty(value="备注3", name="mreremark3")
    private String mreremark3;

    @ApiModelProperty(value="备注4", name="mreremark4")
    private String mreremark4;


    public Long getMrid() {
        return mrid;
    }

    public void setMrid(Long mrid) {
        this.mrid = mrid;
    }

    public Long getOrglifeid() {
        return orglifeid;
    }

    public void setOrglifeid(Long orglifeid) {
        this.orglifeid = orglifeid;
    }

    public Long getDcappraisalid() {
        return dcappraisalid;
    }

    public void setDcappraisalid(Long dcappraisalid) {
        this.dcappraisalid = dcappraisalid;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getMreremark1() {
        return mreremark1;
    }

    public void setMreremark1(String mreremark1) {
        this.mreremark1 = mreremark1;
    }

    public String getMreremark2() {
        return mreremark2;
    }

    public void setMreremark2(String mreremark2) {
        this.mreremark2 = mreremark2;
    }

    public String getMreremark3() {
        return mreremark3;
    }

    public void setMreremark3(String mreremark3) {
        this.mreremark3 = mreremark3;
    }

    public String getMreremark4() {
        return mreremark4;
    }

    public void setMreremark4(String mreremark4) {
        this.mreremark4 = mreremark4;
    }
}