package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.meeting.constant.common.SysConstants.MTYPE;
import com.lehand.meeting.utils.NumberUtils;
import com.lehand.meeting.utils.OrgUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 工作台类
 */
@Service
public class WorkbenchService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;
    //党支部code
    @Value("${jk.organ.dangzhibu}")
    private String dangzhibu;

    //党支部code
    @Value("${jk.organ.lianhedangzhibu}")
    private String lianhedangzhibu;

    @Resource
    private CheckStandardService checkStandardService;

    @Resource private OrgUtils orgUtils;


    /**
     * 查询快捷功能入口列表
     * @return
     * @param session
     */
    public List<Map<String,Object>> listFunction(Session session) {
        return generalSqlComponent.query(MeetingSqlCode.listFastFunctionEnter,new Object[]{session.getCompid(),session.getUserid()});
    }

    /**
     * 保存登录人的快捷入口
     * @param fctid
     * @param session
     */
    public void saveFunction(String fctid, Session session) {
        if(StringUtils.isEmpty(fctid)){
            delFunctions(session);
            return;
        }
        String[] fctids = fctid.split(",");
        Map param = new HashMap();
        param.put("compid",session.getCompid());
        param.put("userid",session.getUserid());
        //查询功能信息urc_function
        delFunctions(session);
        for (String id : fctids) {
            Map map = generalSqlComponent.query(MeetingSqlCode.getUrcFunction,new Object[]{session.getCompid(),id});
            param.put("functioncode",map.get("fctcode"));
            param.put("functionname",map.get("fctname"));
            param.put("functionrout",map.get("remarks3"));
            //保存信息到表fast_function_enter,（这里需要做下判断，防止插入报主键冲突，一般情况是用不上的）
            Map fastMap = generalSqlComponent.query(MeetingSqlCode.getFastUrcFunction,new Object[]{session.getCompid(),session.getUserid(),id});
            if(fastMap==null){
                generalSqlComponent.insert(MeetingSqlCode.addFastFunctionEnter,param);
            }
        }
    }

    /**
     * 删除登录人的快捷入口
     * @param functioncode
     * @param session
     */
    public void delFunction(String functioncode, Session session) {
        generalSqlComponent.delete(MeetingSqlCode.delFastFunctionEnter,new Object[]{session.getCompid(),session.getUserid(),functioncode});
    }

    /**
     * 删除快捷入口
     * @param session
     */
    public void delFunctions(Session session) {
        generalSqlComponent.delete(MeetingSqlCode.delFastFunctionEnters,new Object[]{session.getCompid(),session.getUserid()});
    }

    /**
     * 指标监测查询列表
     * @param session
     * @return
     */
    public List<Map<String,Object>> indexMonitor(/**Integer type,String startTime,String endTime,*/Session session) throws ParseException {
//        Map<String,Object> map = getStartAndEndTime(type,startTime,endTime);
        Long compid = session.getCompid();
        List<Map<String,Object>> lists = generalSqlComponent.query(MeetingSqlCode.listSysParamByParamkey,new Object[]{compid});
        String orgType = session.getCurrentOrg().getOrgtypeid().toString();
        String orgCode = session.getCurrentOrg().getOrgcode();
        if(dangzhibu.equals(orgType) || lianhedangzhibu.equals(orgType)){
            //党支部指标监测
            for (Map<String, Object> list : lists) {
                list.put("baseConveneNum",getMeetingConveneNum(list.get("paramkey").toString(), compid));
                list.put("actualConveneNum",checkStandardService.getConvenedByOrg(Integer.valueOf(DateEnum.YYYY.format()), orgCode,list,session));
//                list.put("actualConveneNum",checkStandardService.getConvenedByOrg2(orgCode,list,session,map));
            }
        }else{
//            for (Map<String, Object> list : lists) {
//                list.put("baseConveneNum",getMeetingConveneNum(list.get("paramkey").toString(), compid));
////                list.put("actualConveneNum",checkStandardService.getConvenedByOrg(Integer.valueOf(DateEnum.YYYY.format()), orgCode,list,session));
//                list.put("actualConveneNum",checkStandardService.getConvenedByOrgPid(orgCode,list,session,map));
//            }
            lists = generalSqlComponent.query(MeetingSqlCode.listMeetingType,new Object[]{session.getCompid(),session.getCurrentOrg().getOrgcode()});
            //上级党组织指标监测 查询时间太长换成
            /*Map param = new HashMap();
            String organid = generalSqlComponent.query(MeetingSqlCode.getOrganidByorgCode,new Object[]{orgCode});
            String path = orgUtils.getPath(organid);
            param.put("path",path);
            param.put("organid",organid);
            List<Map<String,Object>> organs = generalSqlComponent.query(MeetingSqlCode.listChildOrgan,param);
            //拼接换届信息数据
            Map<String, Object> map = new HashMap<>();
            map.put("paramvalue","换届情况");
            for (Map<String, Object> maps : lists) {
                Map organs2 = getStandardNum(compid, param, maps, true,organs);
                maps.put("standardNum",organs2.get("standardNum"));
                maps.put("noStandardNum",organs2.get("noStandardNum"));
            }
            Map organs3 = getStandardNum(compid, param, map, false,organs);
            map.put("standardNum",organs3.get("electionNormalNum"));
            map.put("noStandardNum",organs3.get("electionOverdueNum"));
            lists.add(map);*/
        }
        for (Map<String,Object> map: lists) {
            if("election".equals(map.get("paramkey"))){
                String organid = generalSqlComponent.query(MeetingSqlCode.getOrganidByorgCode,new Object[]{orgCode});
                String path = orgUtils.getPath(organid);
                long normalnum = getElectionNum(MagicConstant.NUM_STR.NUM_1,organid,path);
                long delaynum = getElectionNum(MagicConstant.NUM_STR.NUM_0,organid,path);
                map.put("noStandardNum",delaynum);
                map.put("standardNum",normalnum);
            }
        }
        return lists;
    }
    /**
     * 换届数量
     * @param status
     * @param organid
     * @return
     */
    private long getElectionNum(String status, String organid,String path) {
        long count = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getHjxxCount,new HashMap(){{put("status",status);put("organid",organid);put("path",path);}});
        return count;
    }
    /**
     * 拼接参数
     * @param compid
     * @param orgid
     * @param orgcode
     * @param orgname
     * @param dynamicstext
     * @param dynamicstype
     * @param dynamicsname
     */
    public void spliceSysParam(Long compid,Long businessid,Long orgid,String orgcode,String orgname,
                               String dynamicstext,String dynamicstype,String dynamicsname,Long userid) {
        Map param = new HashMap();
        param.put("compid",compid);
        param.put("businessid",businessid);
        param.put("orgid",orgid);
        param.put("orgcode",orgcode);
        param.put("orgname",orgname);
        param.put("dynamicstext",dynamicstext);
        param.put("dynamicstype",dynamicstype);
        param.put("dynamicsname",dynamicsname);
        param.put("createtime",DateEnum.YYYYMMDDHHMMDD.format());
        param.put("createuserid",userid);
        param.put("remarks1",0);
        param.put("remarks2",0);
        param.put("remarks3","");
        param.put("remarks4","");
        saveSysDynamics(param);
    }

    /**
     * 新增系统动态
     * @param param
     */
    public void saveSysDynamics(Map param) {
        generalSqlComponent.insert(MeetingSqlCode.addSysDnamics,param);
    }

    /**
     * 删除系统动态
     * @param compid
     * @param businessid
     * @param orgid
     */
    public void delSysDynamics(Long compid,Long businessid,Long orgid) {
        generalSqlComponent.delete(MeetingSqlCode.delSysDnamics,new Object[]{compid,businessid,orgid});
    }

    /**
     * 获取党组织理应召开的次数
     * @param mtype
     * @param compid
     * @return
     */
    private int getMeetingConveneNum(String mtype, Long compid) {
        //获取会议召开频率
        Map<String,Object> map = generalSqlComponent.query(SqlCode.getSysParam2,new Object[]{compid,mtype});
        return checkStandardService.getExpectedvalue(Integer.valueOf(DateEnum.YYYY.format()),map);
    }

    /**
     * 系统动态分页查询
     * @param session
     * @param topic
     * @param startDate
     * @param endDate
     * @param type
     * @param pager
     * @return
     */
    public Pager page(Session session, String topic, String startDate, String endDate, String type, Pager pager) {
        Map param = new HashMap();
        param.put("compid",session.getCompid());
        param.put("topic",topic);
        param.put("startDate",startDate);
        param.put("endDate",endDate);
        param.put("type",type);
        generalSqlComponent.pageQuery(MeetingSqlCode.listSysDnamics,param,pager);
        return pager;
    }

    /**
     * 动态类型查询
     * @return
     */
    public List<Map<String,Object>> listDynamicsType(Session session) {
        return generalSqlComponent.query(MeetingSqlCode.listSysDnamicsType,new Object[]{session.getCompid(),"dynamics_type"});
    }

    public Map<String,Object> getStartAndEndTime(Integer type,String startTime,String endTime){
        Calendar cal = Calendar.getInstance();
        int startYear = cal.get(Calendar.YEAR);;
        int endYear = cal.get(Calendar.YEAR);
        int startMonth = cal.get(Calendar.MONTH) + 1;
        int endMonth = cal.get(Calendar.MONTH) + 1;
        if(type.equals(NumberUtils.num0)){//自定义事件
            startYear = Integer.valueOf(startTime.substring(0,3));
            startMonth = Integer.valueOf(startTime.substring(5,startTime.length()));
            endYear = Integer.valueOf(endTime.substring(0,3));
            endMonth = Integer.valueOf(endTime.substring(5,startTime.length()));
        }else if(type.equals(NumberUtils.num1)){//本月
            startYear = cal.get(Calendar.YEAR);;
            endYear = cal.get(Calendar.YEAR);
            startMonth = cal.get(Calendar.MONTH) + 1;
            endMonth = cal.get(Calendar.MONTH) + 1;
        }else if(type.equals(NumberUtils.num2)){//本季度
            System.out.println("*****************************11111111111111::::"+type);
            if(startMonth>=NumberUtils.num1&&startMonth<=NumberUtils.num3){
                startMonth=NumberUtils.num1;
                endMonth=NumberUtils.num3;
            }
            if(startMonth>=NumberUtils.num4&&startMonth<=NumberUtils.num6){
                startMonth=NumberUtils.num4;
                endMonth=NumberUtils.num5;
            }
            if(startMonth>=NumberUtils.num7&&startMonth<=NumberUtils.num9){
                startMonth=NumberUtils.num7;
                endMonth=NumberUtils.num9;
            }
            if(startMonth>=NumberUtils.num10&&startMonth<=NumberUtils.num12){
                startMonth=NumberUtils.num10;
                endMonth=NumberUtils.num12;
            }
        }else if(type.equals(NumberUtils.num3)){//本年
            startMonth=NumberUtils.num1;
            endMonth=NumberUtils.num12;
        }
        Map<String,Object> map  = new HashMap<String,Object>();
        map.put("startYear",startYear);
        map.put("endYear",endYear);
        map.put("startMonth",startMonth);
        map.put("endMonth",endMonth);
        return map;
    }
}
