package com.lehand.meeting.utils;

/**
 * @program: huaikuang-dj
 * @description: NumberUtils
 * @author: zwd
 * @create: 2020-11-02 15:25
 */
public class NumberUtils {
    public static final int num0 = 0;
    public static final int num1 = 1;
    public static final int num2 = 2;
    public static final int num3 = 3;
    public static final int num4 = 4;
    public static final int num5 = 5;
    public static final int num6 = 6;
    public static final int num7 = 7;
    public static final int num8 = 8;
    public static final int num9 = 9;
    public static final int num10 = 10;
    public static final int num11 = 11;
    public static final int num12 = 12;
    public static final int num30 = 30;
    public static final int num40 = 40;
    public static final int num45 = 45;
    public static final int num50 = 50;
    public static final int num100 = 100;
    public static final int num200 = 200;
    public static final int num300 = 300;
    public static final int num400 = 400;
    public static final int num500 = 500;

    public static final int num1000000001 = 1000000001;

    public static final Long num5000000005 = 5000000005l;
}