package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="会议地点", description="会议地点")
public class MeetingRecordPlace implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="组织机构ID", name="orgid")
    private Long orgid;

    @ApiModelProperty(value="地点", name="place")
    private String place;

    @ApiModelProperty(value="序号", name="seqno")
    private Long seqno;

    @ApiModelProperty(value="", name="compid")
    private Long compid;

    @ApiModelProperty(value="组织编码", name="orgcode")
    private String orgcode;



    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }
}