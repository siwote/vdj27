package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.*;
import com.lehand.meeting.pojo.MeetingAbsentSubject;
import com.lehand.meeting.pojo.MeetingRecordSubject;
import com.lehand.meeting.service.MeetingNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

//@Api(value = "会议通知", tags = { "会议通知" })
@Api(value = "会议", tags = {"会议"})
@RestController
@RequestMapping("/horn/notice")
public class MeetingNoticeController extends BaseController {

    @Resource
    MeetingNoticeService meetingNoticeService;

    @OpenApi(optflag = 1)
//    @ApiOperation(value = "新增会议通知", notes = "新增会议通知", httpMethod = "POST")
    @ApiOperation(value = "新增会议", notes = "新增会议", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message addMeetingNotice(@RequestBody MNoticeAddReq req) {
        Message msg = new Message();
        try {
            msg.setData(meetingNoticeService.addMeetingNotice(getSession(), req));
            msg.success();
        } catch (Exception e) {
           msg.setMessage(e.getMessage());
        }
        return msg;
    }

    /**
     * 新增会议前查询当前机构上一个会议的地点
     *
     * @return
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "新增会议前查询", notes = "新增会议前查询", httpMethod = "POST")
    @RequestMapping(value = "/addBeforeQuery", method = RequestMethod.POST)
    public Message addBeforeQuery() {
        Message msg = new Message();
        msg.setData(meetingNoticeService.addBeforeQuery(getSession()));
        msg.success();
        return msg;
    }


    @OpenApi(optflag = 2)
//    @ApiOperation(value = "撤销会议通知", notes = "撤销会议通知", httpMethod = "POST")
    @ApiOperation(value = "撤销会议", notes = "撤销会议", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "noticeid", value = "通知id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/cancel")
    public Message cancelMeetingNotice(Long noticeid) {
        Message msg = new Message();
        msg.setData(meetingNoticeService.cancelMeetingNotice(getSession(), noticeid));
        msg.success();
        return msg;
    }


//    @OpenApi(optflag=2)
//    @ApiOperation(value = "修改会议通知", notes = "修改会议通知", httpMethod = "POST")
//    @RequestMapping("/update")
//    public Message updateMeetingNotice(@RequestBody MNoticeUpdateReq req){
//        Message msg =  new Message();
//        msg.setData(meetingNoticeService.updateMeetingNotice(getSession(),req));
//        return msg;
//    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "根据ID查看通知内容", notes = "根据ID查看通知内容", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "noticeid", value = "通知id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/query")
    public Message queryMeetingNotice(Long noticeid) {
        Message msg = new Message();
        msg.setData(meetingNoticeService.queryMeetingNotice(getSession(), noticeid));
        msg.success();
        return msg;
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "会议通知阅读状态", notes = "会议通知阅读状态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "noticeid", value = "通知id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/check")
    public Message checkmeetingNoticeStatus(Long noticeid) {
        Message msg = new Message();
        msg.setData(meetingNoticeService.checkmeetingNoticeStatus(getSession(), noticeid));
        msg.success();
        return msg;
    }


//    @OpenApi(optflag=0)
//    @ApiOperation(value = "会议被通知对象查看站内信内容", notes = "会议被通知对象查看站内信内容", httpMethod = "POST")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "noticeid", value = "通知id", required = true, paramType = "query", dataType = "Long", defaultValue = "")
//    })
//    @RequestMapping("/content")
//    public  Message meetingNoticeContent(Long noticeid){
//        Message msg =  new Message();
//        msg.setData(meetingNoticeService.meetingNoticeContent(getSession(),noticeid));
//        msg.success();
//        return msg;
//    }

    @OpenApi(optflag = 0)
//    @ApiOperation(value = "通知列表分页", notes = "通知列表分页", httpMethod = "POST")
    @ApiOperation(value = "会议列表分页", notes = "会议列表分页", httpMethod = "POST")
    @RequestMapping("/page")
    public Message<Pager> pageMeetingNotice(@RequestBody MNoticePageReq req) throws ParseException {
        Message msg = new Message();
        msg.setData(new PagerUtils<MNoticeListDto>(meetingNoticeService.pageMeetingNotice(getSession(), req)));
        msg.success();
        return msg;
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "复用通知内容到会议", notes = "复用通知内容到会议", httpMethod = "POST")
    @RequestMapping("/copy")
    public Message copyMNoticeToMeeting(Long noticeid) {
        Message msg = new Message();
        msg.setData(meetingNoticeService.copyMNoticeToMeeting(getSession(), noticeid));
        msg.success();
        return msg;
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "应参会人(弃用)", notes = "应参会人(弃用)", httpMethod = "POST")
    @RequestMapping("/getSub")
    public Message getAttendSubByType(String type, String orgcode) {
        Message msg = new Message();
        msg.setData(meetingNoticeService.getAttendSubByType(getSession(), type, orgcode));
        msg.success();
        return msg;
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "通知列表分页(弃用)", notes = "通知列表分页(弃用)", httpMethod = "POST")
    @RequestMapping("/listMeetingNotice")
    public Message listMeetingNotice(String orgcode, String mrtype, int day, Pager pager) {
        Message msg = new Message();
        return msg.setData(meetingNoticeService.listMeetingNotice(orgcode, mrtype, getSession(), day, pager)).success();
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "获取系统当前时间", notes = "获取系统当前时间", httpMethod = "POST")
    @RequestMapping("/getCurrentTime")
    public Message getCurrentTime() {
        Message msg = new Message();
        return msg.setData(meetingNoticeService.getCurrentTime(getSession())).success();
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "查询我的会议列表分页", notes = "查询我的会议列表分页", httpMethod = "POST")
    @RequestMapping("/pageMyMeeting")
    public Message pageMyMeeting(@RequestBody MeetingMyPageReq req) {
        Message msg = new Message();
        return msg.setData(new PagerUtils<MeetingMyDto>(meetingNoticeService.pageMyMeeting(req, getSession()))).success();
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "参会&不参会", notes = "参会&不参会", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "状态 1 参加 ,2不参加", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "列表中的id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "列表中的userid", required = true, paramType = "query", dataType = "Long", defaultValue = "")
    })
    @RequestMapping("/toggleAttend")
    public Message toggleAttend(int status, Long id, String userid) {
        Message msg = new Message();
        try {
            msg.setData(meetingNoticeService.toggleAttend(status, id, userid, getSession())).success();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return msg;
    }

    /**
     * @param
     * @return
     * @Description 计算缺席人员
     * @Author zwd
     * @Date 2020/11/5 16:00
     **/
    @OpenApi(optflag = 0)
    @ApiOperation(value = "计算缺席人员", notes = "计算缺席人员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeid", value = "通知id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/hyqxry")
    public Message hyqxry(String noticeid, List<MeetingRecordSubject> subjects) {
        Message message = new Message();
        message.setData(meetingNoticeService.hyqxry(noticeid, getSession(), subjects));
        return message;
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "获取缺席人员", notes = "获取缺席人员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeid", value = "通知id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/getqxry")
    public Message   getqxry(String noticeid){
        Message message = new Message();
        message.setData(meetingNoticeService.getqxry(noticeid, getSession())).success();
        return message;
    }
}
