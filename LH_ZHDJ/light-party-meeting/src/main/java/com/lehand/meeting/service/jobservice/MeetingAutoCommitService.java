package com.lehand.meeting.service.jobservice;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.module.common.pojo.SystemParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class MeetingAutoCommitService {

    private static final Logger logger = LogManager.getLogger(MeetingAutoCommitService.class);

    private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    GeneralSqlComponent generalSqlComponent;

    @Transactional(rollbackFor=Exception.class)
    public void updateStatus()  throws LehandException {
        try {
            synchronized (CREATE) {
                if (CREATE.get()) {
//                    logger.info("之前任务正在执行,本次不处理");
                    return;
                }
                CREATE.set(true);
            }
//            logger.info("会议自动提交开始...");

            SystemParam systemParam = generalSqlComponent.query(MeetingSqlCode.getSysParam, new Object[] {
                    1, SysConstants.MEETING_CFG_CONSTANT.MEETING_AUTO_COMMIt_CFG});

            if(systemParam==null||MagicConstant.NUM_STR.NUM_1.equals(systemParam.getParamvalue())){
                return;
            }

            int hour = systemParam.getRemarks2();
            if(StringUtils.isEmpty(hour)){
                return;
            }

            //查询所有编辑状态的会议
            List<Map<String,Object>> list = generalSqlComponent.query(MeetingSqlCode.listAllUnSubmittedMeeting,new Object[]{1});
            if(list.size()<1){
                return;
            }


            threadPoolTaskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    for (Map<String,Object> map:list) {
                        try {
                            String dateStr = map.get("createtime").toString();
                            Date createDate = DateEnum.parseStr(dateStr,"yyyy-MM-dd HH:mm:ss");
                            Date nowDate = new Date();
                            Long createtime = createDate.getTime();
                            Long nowtime = nowDate.getTime();
                            long diff = nowtime - createtime;
                            long diffHour = diff/(60 * 60 * 1000);
                            int settedTime = Integer.parseInt(String.valueOf(diffHour));
                            if(settedTime<1){
                                continue;
                            }
                            if(settedTime < hour){
                                continue;
                            }
                            int num =  generalSqlComponent.update(MeetingSqlCode.updateMeetingStatusByMrid,
                                    new Object[]{2,1,map.get("mrid")});
                            logger.info("更新了会议"+num+"   ::" +map.get("mrid"));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            logger.info("自动更新操作结束...");
        } catch (Exception e) {
            logger.error("自动更新操作异常",e);
        } finally {
            CREATE.set(false);
        }

    }

    public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
        return threadPoolTaskExecutor;
    }

    public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.threadPoolTaskExecutor = threadPoolTaskExecutor;
    }

}
