package com.lehand.meeting.constant;

public class MeetingSqlCode {

    /**
     * 查询会议分页列表
     * select * from meeting_record where  status <> 0 and status <> 3 and orgcode = :orgcode and mrtype = :mtype  and compid = :compid
     * and starttime >= :starttime
     * and endtime <= :endtime
     * and concat(topic, context) like '%${PARAMS.likeStr}%'
     * status = :status
     * order by starttime desc
     */
    public final static String pagerMeeting = "pagerMeeting";

    /**
     * 查询标签类会议分页列表
     * select * from view_tag_meeting where status<> 0 and status <> 3 and  orgcode = :orgcode and mrtype = :mtype and mryear = :year
     * and starttime >= :starttime
     * and endtime <= :endtime
     * and concat(topic, context) like '%${PARAMS.likeStr}%'
     * <p>
     * and status = :status
     * order by starttime desc
     */
    public final static String pagerTagMeeting = "pagerTagMeeting";

    /**
     * 根据父级编码查询数据集合
     * select * from sys_param where upparamkey = ? and compid = ? order by remarks1 asc
     */
    public static String listSysParamByUpparamkey = "listSysParamByUpparamkey";

    /**
     * 根据组织机构编码和会议类型统计召开数
     * select count(*) from meeting_record where orgcode = ? and mrtype = ? and status  =  ? and compid = ?
     */
    public static String countMeetingTypeByOrgcode = "countMeetingTypeByOrgcode";
    public static String countMeetingTypeByOrgcode2 = "countMeetingTypeByOrgcode2";
    public static String countMeetingTypeByOrgcodePid = "countMeetingTypeByOrgcodePid";

    /**
     * 根据组织机构编码和会议类型统计召开数
     * select count(*) from view_tag_meeting where orgcode = ? and mrtype = ? and status =  ? and compid = ?
     */
    public static String countTagMeetingTypeByOrgcode = "countTagMeetingTypeByOrgcode";

    public static String countTagMeetingTypeByOrgcode2 = "countTagMeetingTypeByOrgcode2";
    public static String countTagMeetingTypeByOrgPid = "countTagMeetingTypeByOrgPid";

    /**
     * 增加会议记录
     * INSERT INTO meeting_record( orgid, orgcode, orgname, mrtype, mryear, mrmonth, starttime, endtime, place, moderatorid, moderatorname, recorderid, recordername, topic, context, status, groupid, groupname, shouldnum, realitynum, createtime, createuserid, updatetime, updateuserid, deletetime, deleteuserid, compid, remark1, remark2, remark3, remark4)
     * VALUES ( :orgid, :orgcode, :orgname, :mrtype, :mryear, :mrmonth, :starttime, :endtime, :place, :moderatorid, :moderatorname, :recorderid, :recordername, :topic, :context, :status, :groupid, :groupname, :shouldnum, :realitynum, :createtime, :createuserid, :updatetime, :updateuserid, :deletetime, :deleteuserid, :compid, :remark1, :remark2, :remark3, :remark4)
     */
    public static String saveMeetingRecord = "saveMeetingRecord";

    /**
     * 增加会议参与人
     * INSERT INTO meeting_record_subject( usertype, userid, idcard, username, compid, remark1, remark2)
     * VALUES (:usertype, :userid, :idcard, :username, :compid, :remark1, :remark2)
     */
    public static String saveMeetingRecordSubject = "saveMeetingRecordSubject";

    /**
     * 根据组织编码查询人员信息
     * select userid,xm,zjhm,organcode,parentcode,dzzmc,dnzwname,xb,xl,csrq,b.item_name xlName,lxdh from view_dy_org_bzcyxx a left join g_dict_item b on a.xl=b.item_code and b.`code` = 'd_dy_xl' where organcode = :organcode and dyzt = '1000000003' and dylb <> '3000000003' and dylb <> '4000000004' and dylb <> '5000000005'
     */
    public static String getMemberByOrgancode = "getMemberByOrgancode";

    /**
     * 查询系统参数
     * SELECT * FROM sys_param WHERE compid=? and paramkey=?
     */
    public static String getSysParam2 = "getSysParam2";

    /**
     * 按月和类型统计会议召开数
     * <p>
     * select count(*) num ,mrmonth from meeting_record where status = 2 and mryear = ? and orgcode = ? and mrtype = ? and compid = ? group by mrmonth
     */
    public static String getConvedNumByMonth = "getConvedNumByMonth";
    /**
     * 根据状态和orgid查会议记录
     * <p>
     * select * from meeting_record where status = ? and compid = ? and orgid =? AND mrtype =?
     */
    public static final String LIST_MEETING_RECORD_BY_STATUS = "select * from meeting_record where status = ? and compid = ? and orgid =? and mrtype =?";


    /**
     * 按月和类型统计会议召开数
     * <p>
     * select count(*) num ,mrmonth from view_tag_meeting where status = 2 and mryear = ? and orgcode = ? and mrtype = ? and compid = ? group by mrmonth
     */
    public static String getConvedTagNumByMonth = "getConvedTagNumByMonth";

    /**
     * 根据会议ID查询会议信息
     * select * from meeting_record where mrid = ? and compid = ?
     */
    public static String getMeetingByMrid = "getMeetingByMrid";

    /**
     * --- SELECT * FROM meeting_notice_record a left join meeting_record_relation b on  a.id=b.noticeid and a.compid=b.compid where  b.mrid=? and a.compid=?
     */
    public static String getMeetingByMrid2 = "getMeetingByMrid2";

    /**
     * 更改会议的状态
     * update meeting_record set status = ? where compid = ? and mrid = ?
     */
    public static String updateMeetingStatusByMrid = "updateMeetingStatusByMrid";

    /**
     * 根据会议ID查询会议参与人
     * select * from meeting_record_subject where mrid = ? and busitype = ? and compid = ?
     */
    public static String getMeetingSubjectByMrid = "getMeetingSubjectByMrid";

    /**
     * 查询会议地点
     * select * from meeting_record_place where orgcode = :orgcode and compid = :compid
     */
    public static String getMeetingPlace = "getMeetingPlace";

    /**
     * 查询会议地点
     * select place from meeting_record_place where orgcode = :orgcode and compid = :compid
     */
    public static String getMeetingPlace2 = "getMeetingPlace2";

    /**
     * 保存会议地点
     * insert into meeting_record_place(orgid, orgcode, place, seqno, compid) values(:orgid, :orgcode, :place, :seqno, :compid)
     */
    public static String saveMeetingPlace = "saveMeetingPlace";

    /**
     * 表单默认值
     * INSERT INTO comm_default_value(compid, classify1, classify2, classify3, formvalue, updatetime, updateuserid) VALUES (:compid, :classify1, :classify2, :classify3, :formvalue, :updatetime, :updateuserid)
     */
    public static String saveDefauleForm = "saveDefauleForm";

    /**
     * 根据账号查询用户信息
     * select * from urc_user where idno = :idno
     */
    public static String queryUrcUserByIdno = "queryUrcUserByIdno";

    /**
     * 修改会议记录
     * update meeting_record set mrtype=:mrtype,mryear = :mryear,mrmonth = :mrmonth,starttime=:starttime,endtime=:endtime,place=:place,moderatorid=:moderatorid,moderatorname=:moderatorname,recorderid=:recorderid,recordername=:recordername,topic=:topic,
     * context=:context,status,groupid=:groupid,groupname=:groupname,shouldnum=:shouldnum,realitynum=:realitynum,updatetime=:updatetime,updateuserid=:updateuserid,remark3=:remark3,remark4=:remark4,remark5=:remark5
     * where mrid = :mrid and compid = :compid
     */
    public static String modifyMeeting = "modifyMeeting";

    /**
     * 删除会议参与人
     * delete from meeting_record_subject where mrid = ? and busitype = ? and compid = ?
     */
    public static String delMeetingSubject = "delMeetingSubject";

    /**
     * 统计会议地点数量
     * select count(*) from meeting_record_place
     */
    public static String countMeetingPlace = "countMeetingPlace";

    /**
     * 查询会议默认表单
     * select * from comm_default_value  where compid = :compid
     * and classify1 = :classify1
     * and classify2 = :classify2
     * and classify3 = :classify3
     */
    public static String getCommDefaultValue = "getCommDefaultValue";

    /**
     * 删除表单记录
     * delete from comm_default_value  where compid = :compid
     * and classify1 = :classify1
     * and classify2 = :classify2
     * and classify3 = :classify3
     */
    public static String delCommDefaultValue = "delCommDefaultValue";

    /**
     * 查询会议信息
     * select * from meeting_record where compid = : compid
     */
    public static String queryMeetingByAddition = "queryMeetingByAddition";

    /**
     * 查询会议信息
     * --  select * from meeting_record where compid = :compid and status = 2 //
     * select * from meeting_record where compid = :compid  // 20200608 取消
     * <p>
     * queryMeetingByAddition2
     */
    public static String queryMeetingByAddition2 = "queryMeetingByAddition2";

    /**
     * 查询文件通过资料管理器id<P/>
     * select a.id,a.compid,a.name,a.dir,a.size,a.type,a.viewfilepath,a.viewtype,a.viewsize,a.usertor,a.remark,a.createtime,a.remark1,a.remark2,a.remark3,a.remark4,a.remark5,a.remark6 from dl_file a right join dl_file_business b on a.id=b.fileid
     * where b.businessid=:businessid and b.businessname=:businessname and b.intostatus = :intostatus and a.compid=:compid and b.compid=:compid
     */
    public static String getAttachmentList = "getAttachmentList";

    /**
     * 根据业务删除文件关联
     * delete from dl_file_business where  businessid = ? and businessname = ? and compid = ?
     */
    public static String delFileBusinessByMbyBusID = "delFileBusinessByMbyBusID";

    /**
     * 根据文件删除文件关联表
     * delete from dl_file_business where fileid = ? and businessid = ? and businessname = ? and compid = ?
     */
    public final static String delFileBusinessByMb = "delFileBusinessByMb";


    /**
     * select * from meeting_record_subject where compid = :compid
     * and mrid = :mrid
     * and busitype = :busitype
     */
    public static final String getMeetingSubject = "getMeetingSubject";

    /**
     * 根据标会议id删除会议标签
     * delete from meeting_tags_map where subjectid = ? and subjecttype = ? and compid = ?
     */
    public static final String deleteMeetingTagsByMeetingId = "deleteMeetingTagsByMeetingId";

    /**
     * 新增会议标签
     * insert into meeting_tags_map set compid = :compid,subjectid = :subjectid,subjecttype = :subjecttype,tagcode = :tagcode
     * ,tagname = :tagname,orgcode = :orgcode
     */
    public static final String insertMeetingTags = "insertMeetingTags";

    /**
     * 根据会议id查询标签编码集合
     * select tagcode  from meeting_tags_map where subjectid = ? and subjecttype = ? and compid = ?
     */
    public static final String getMeetingTagcodeByMrid = "getMeetingTagcodeByMrid";

    /**
     * 删除参加人信息
     * delete from meeting_record_subject where mrid = :mrid and compid = :compid and busitype=:busitype
     */
    public static final String delMeetingSubjectOpt = "delMeetingSubjectOpt";

    /**
     * 分页查询工作汇报
     * select * from meeting_talk_record  where compid = :compid and status != -1
     * and topic like '%${PARAMS.topic}%'
     * and recordtime >= :startDate
     * and recordtime <= :endDate
     */
    public static final String pageMeetingReportRecord = "pageMeetingReportRecord";

    /**
     * 保存工作汇报内容
     * INSERT INTO meeting_report_record(compid, topic, report_content, orgcode, orgid, createuserid, updateuserid, createtime, updatetime, remark1, remark2, orgname, status) VALUES (:compid, :topic, :reportContent, :orgcode, :orgid, :createuserid, :updateuserid, :createtime, :updatetime, :remark1, :remark2, :orgname, :status)
     */
    public static final String saveMeetingReportRecord = "saveMeetingReportRecord";

    /**
     * 查询工作汇报详情
     * select * from meeting_report_record  where compid = ? and id = ?
     */
    public static final String getMeetingReportRecord = "getMeetingReportRecord";

    /**
     * 修改工作汇报
     * update meeting_report_record set topic = :topic,report_content=:reportContent,updatetime=:updatetime,updateuserid=:updateuserid,status=:status,remark2=:remark2  where compid = :compid and id = :id
     */
    public static final String updateMeetingReportRecord = "updateMeetingReportRecord";

    /**
     * 更新工作汇报的状态
     * update meeting_report_record set status = ? where compid = ? and id = ?
     */
    public static final String updateMeetingReportStatus = "updateMeetingReportStatus";

    /**
     * 数据字典项查询
     * select * from g_dict_item
     */
    public static final String queryDcitParams = "queryDcitParams";


    /********************************主题党日活动*************************************/

    /**
     * 保存主题当日具体日期
     * insert into meeting_theme_day(id,year,remindtime,orgcode,status,compid) values (:id,:year,:remindtime,:orgcode,:status,:compid)
     */
    public static final String saveMeetingThemeDay = "saveMeetingThemeDay";

    /**
     * 查询主题党日活动日期
     * select * from meeting_theme_day where compid = :compid
     * (条件可选)
     */
    public static final String queryMtDay = "queryMtDay";

    /**
     * 删除主题党日活动日期
     * delete from meeting_theme_day where compid=:compid and id = :id
     */
    public static final String delMtDayById = "delMtDayById";

    /**
     * 保存主题党日日期
     * insert into meeting_theme_date(year,day,startdate,enddate,orgcode,orgid,orgname,status,compid) values (:year,:day,:startdate,:enddate,:orgcode,:orgid,:orgname,:status,:compid)
     */
    public static final String saveMeetingThemeDate = "saveMeetingThemeDate";

    /**
     * 根据条件查询主题党日日期
     * select * from meeting_theme_date where compid = :compid
     * (条件可选)
     */
    public static final String queryMtdate = "queryMtdate";

    /**
     * 修改主题党日日期
     * update meeting_theme_date set year=:year,day=:day,startdate=:startdate,enddate=:enddate,orgname=:orgname,status=:status where orgcode=:orgcode and compid=:compid
     */
    public static final String updateMeetingThemeDate = "updateMeetingThemeDate";

    /**
     * 删除主题党日日期
     * delete from meeting_theme_date where compid = :compid and orgcode = :orgcode
     */
    public static final String delMeetingThemeDate = "delMeetingThemeDate";


    /**
     * 获取最新保存一条标签会议id
     * select a.mrid from meeting_record a inner join meeting_tags_map b on a.mrid = b.subjectid and a.orgcode = b.orgcode and a.compid = b.compid where
     * a.status <> 0 and a.orgcode = :orgcode and b.tagcode  = :mtype and a.compid = :compid order by a.updatetime , a.createtime desc limit 1
     */
    public static final String getLatestTagMeetingid = "getLatestTagMeetingid";

    /**
     * 获取最新保存一条基础类型会议id
     * select mrid from meeting_record where status <> 0 and orgcode = :orgcode and mrtype = :mtype and compid = :compid order by updatetime , createtime desc limit 1
     */
    public static final String getLatestBaseMeetingid = "getLatestBaseMeetingid";

    /*********************************************************************************** 会议通知*******************************************/

    /**
     * 新增一条会议通知并返回id
     * insert into meeting_notice_record set compid = :compid, orgid = :orgid,orgcode = :orgcode,orgname = :orgname,type = :type,typename = :typename
     * ,topic = :topic,place = :place,noticetime = :noticetime,status = :status,createid = :createid,createtime = :createtime,updateid = :updateid
     * ,updatetime = :updatetime,remarks1 = :remarks1,remarks2 = :remarks2,remark = :remark,subnum = :subnum
     */
    public static final String addMeetingNoticeRecord = "addMeetingNoticeRecord";

    /**
     * 新增一条会议通知并返回id
     * update  meeting_notice_record set compid = :compid, orgid = :orgid,orgcode = :orgcode,orgname = :orgname,type = :type,typename = :typename
     * ,topic = :topic,place = :place,noticetime = :noticetime,status = :status,updateid = :updateid
     * ,updatetime = :updatetime,remarks1 = :remarks1,remarks2 = :remarks2,remark = :remark,subnum = :subnum where id = :id and compid = :compid
     */
    public static final String updateMeetingNoticeRecord = "updateMeetingNoticeRecord";

    /**
     * 发送消息(任务模块的发送消息)
     * INSERT INTO tk_task_remind_list (compid, noteid, module, mid, remind, rulecode,rulename, optuserid, optusername, userid, username, isread, ishandle, issend, mesg, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6,remark)
     * VALUES (:compid,:noteid,:module,:mid,:remind,:rulecode,:rulename,:optuserid,:optusername,:userid,:username,:isread,:ishandle,:issend,:mesg,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6,:remark)
     */
    public static final String addTTRLByTuihui = "addTTRLByTuihui";

    /**
     * 根据通知id获取通知
     * select * from meeting_notice_record where id = ? and compid = ?
     */
    public static final String getMeetingNoticeById = "getMeetingNoticeById";

    /**
     * 根据id修改会议通知状态
     * update meeting_notice_record set status = ?,updateid = ? ,updatetime = ? where id = ? and compid = ?
     */
    public static final String updateMNoticeStatusById = "updateMNoticeStatusById";

    /**
     * select * from meeting_notice_record where compid=? and id=? and noticetime<?
     */
    public static final String getNoticeStatusById = "getNoticeStatusById";

    /**
     * 根据业务id批量修改消息状态
     * update tk_task_remind_list set ishandle = ?  where module = ? and mid = ? and compid = ?
     */
    public static final String updateRemidListHandleByMid = "updateRemidListHandleByMid";

    /**
     * 查询消息数
     * select count(*) from tk_task_remind_list where ishandle <> 2 and module = 10  and compid = :compid
     * <p>
     * and mid = :noticeid
     */
    public static final String countRemindListByNoticeId = "countRemindListByNoticeId";

    /**
     * 按部门分页查询会议通知
     * select * from meeting_notice_record where  orgcode = :orgcode
     * and status = :status
     * and type = :mrtype
     * and createtime >= :starttime
     * and createtime <= :endtime
     * and topic like ${PARAMS.likeStr}
     */
    public static final String pageMNoticeByOrg = "pageMNoticeByOrg";

    /**
     * 按部门分页查询会议通知标签
     * select a.* from view_meeting_notice_record a inner join meeting_tags_map b on a.compid = b.compid and a.id = b.subjectid and b.subjecttype = 1
     * where a.orgcode = :orgcode
     * and a.status = :status
     * and b.tagcode = :mrtype // 20200608 删除
     * and a.type= :mrtype  //20200608添加
     * and b.tagcode in (:tagcode) //20200608添加
     * and a.createtime >= :starttime
     * and a.createtime <= :endtime
     * and topic like ${PARAMS.likeStr}
     * group by a.id
     */
    public static final String pageTagMNoticeByOrg = "pageTagMNoticeByOrg";

    /**
     * 获取通知人员名单
     * select username from tk_task_remind_list a where ishandle <> 2 and module = :module and mid = :mid and issend = :issend and compid = :compid
     */
    public static final String getMNoticeSubName = "getMNoticeSubName";

    /**
     * 更新会议指南信息
     * update sys_param set paramvalue = :paramvalue where paramkey = :paramkey and compid = :compid
     */
    public static final String updateSysParamValue = "updateSysParamValue";

    /**
     * 查询系统参数
     * SELECT * FROM sys_param WHERE compid=? and paramkey=?
     */
    public static final String getSysParam = "getSysParam";

    /**
     * 新增系统参数
     * INSERT INTO sys_param (compid,paramkey, paramname,hassubset, upparamkey, paramvalue, mdcode,classify, canmodify, innerparam, remarks1, remarks2, remarks3, remarks4) VALUES (:compid,:paramkey,:paramname,:hassubset,:upparamkey,:paramvalue,:mdcode,:classify,:canmodify,:innerparam,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static final String addSysParam = "addSysParam";

    /**
     * 更新会议自动提交配置
     * update sys_param set paramvalue=:paramvalue,remarks2=:remarks2 where paramkey=:paramkey and compid=:compid
     */
    public static final String updateMeetingAutoCfg = "updateMeetingAutoCfg";

    /**
     * 查询所有编辑状态的会议
     * select * from meeting_record where status = 1 and compid = ?
     */
    public static final String listAllUnSubmittedMeeting = "listAllUnSubmittedMeeting";

    /**
     * 查询支部应到人数
     * select count(*) from t_dy_info where dylb in ('1000000001','2000000002') and organid = (select organid from t_dzz_info where organcode = ?)
     */
    public static final String countShallReachedByOrg = "countShallReachedByOrg";

    /**
     * 修改会议提醒日期
     * update sys_param set paramvalue=:mrtypecode,remarks2=:mode,remarks3=:mrtypename,remarks4=:days  where paramkey=:paramkey and compid=:compid
     */
    public static String updateMeetingRemindDate = "updateMeetingRemindDate";
    /**
     * 查询班子成员
     * select s.uuid,s.organid,s.userid,s.rzdate, s.username xm,zjhm,s.xb,s.csrq,s.xl,s.dnzwname,s.dnzwsm, s.zwlevel,s.lzdate,s.ordernum,s.lxdh from t_dzz_bzcyxx s inner join (select max(rzdate) rzdate,userid,organid from t_dzz_bzcyxx where organid = :organid group by userid ) t on  s.rzdate = t.rzdate and s.userid = t.userid and s.organid = t.organid where  s.zfbz  = 0
     * and xm like '%${PARAMS.xm}%'
     * order by ordernum asc
     */
    public static final String queryTdzzInfoSimpleBZCYList = "queryTdzzInfoSimpleBZCYList";

    /**
     * 查询符合条件的会议通知记录
     * select * from meeting_notice_record where status = 2 and orgcode = ? and compid = ? and FIND_IN_SET(type,?) and noticetime > ?
     */
    public static final String queryNoticeByCodeAndType = "queryNoticeByCodeAndType";


    /*********************************************************************************** 会议通知*******************************************/
    /**
     * 根据上级编码查询会议类别信息集合
     * select paramkey as code ,paramvalue as name,remarks3 as calmeth,remarks4 as calvalue,remarks1 as seqno from sys_param where upparamkey = ? and compid = ? order by remarks1 asc
     */
    public static final String getMeetingtypeListByUparamKey = "getMeetingtypeListByUparamKey";

    /**
     * 根据编码查询会议类别信息集合
     * <p>
     * select paramkey as code ,paramvalue as name,remarks3 as calmeth,remarks4 as calvalue,remarks1 as seqno from sys_param where paramkey = ? and compid = ? order by remarks1 asc
     */
    public static final String getMeetingtypeListByparamKey = "getMeetingtypeListByparamKey";

    /**
     * 会议巡查第一级列表 (获取直属下级组织机构)
     * select organid,organcode,zzlb,dzzmc as organname  from  t_dzz_info_simple where parentid = :organid
     */
    public static final String MeetingPatrolList = "MeetingPatrolList";

    public static final String MeetingPatrolList2 = "MeetingPatrolList2";

    /**
     * path查询
     * select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public final static String get_meeting_path_num = "get_meeting_path_num";

    /**
     * 统计达标(总的)和会议召开数
     * select IFNULL(sum(t.covered),0)  num ,IF (sum(t.type) > 0, 0, 1)  type  from (
     * select a.organid,a.dzzmc,IFNULL(b.num, 0) covered ,IF(b.num> :calvalue,0,1) type from (select * from t_dzz_info_simple where ${PARAMS.childpath} = :childorganid) a
     * left join (select count(*) num ,orgcode,mrtype count from  ${PARAMS.tablename} where status = 2 and mrtype = :mrtype
     * and time >= :starttime
     * and time <= : endtime
     * group by orgcode,mrtype) b on a.organcode = b.orgcode ) t
     */
    public final static String getBranchMeetingCountAndSum = "getBranchMeetingCountAndSum";

//    /**
//     * 查看支部召开会议统计
//     * select a.organid,a.organcode, a.dzzmc,IFNULL(b.num, 0) covered ,IF(b.num> :calvalue,1,0) type from (select * from t_dzz_info_simple where ${PARAMS.childpath} = :organid) a left join (select count(*) num ,orgcode,mrtype count from  ${PARAMS.tablename} where status = 2 and mrtype = :mrtype
//     *
//     *  and time >= :starttime
//     *  and time <= : endtime
//     *
//     *    group by orgcode,mrtype ) b on a.organcode = b.orgcode
//     *
//     */
    /**
     * 查看支部召开会议统计
     * select t.* from ( select a.organid,a.organcode, a.dzzmc,IFNULL(b.num, 0) covered ,IF(b.num> :calvalue,1,0) type from (select * from t_dzz_info_simple where ${PARAMS.childpath} = :organid) a left join (select count(*) num ,orgcode,mrtype count from  ${PARAMS.tablename} where status = 2 and mrtype = :mrtype
     * <p>
     * and time >= :starttime
     * and time <= : endtime
     * <p>
     * group by orgcode,mrtype ) b on a.organcode = b.orgcode ) t
     * <p>
     * where 1=1
     * <p>
     * and t.type = :type
     */
    public final static String getMeetingBranches = "getMeetingBranches";

    /**
     * 新增文件<P/>
     * insert into dl_file (compid,name,dir,size,type,usertor,remark,createtime,remark1,remark2,remark3,remark4,remark5,remark6)
     * values (:compid,:name,:dir,:size,:type,:usertor,:remark,:createtime,:remark1,:remark2,:remark3,:remark4,:remark5,:remark6)
     */
    public final static String addDlFile = "addDlFile";

    /**
     * 查询提交状态的谈话记录
     * ----- select * from meeting_talk_record where compid = :compid and status = 2
     * <p>
     * select * from meeting_talk_record where compid = :compid  //20200608 取消 状态
     */
    public final static String listMeetingTalkBySel = "listMeetingTalkBySel";

    /**
     * 获取附件
     * select * from dl_file where id=:id and compid=:compid
     */
    public final static String getPrintFile = "getPrintFile";

    /**
     * 附件查询
     * select * from meeting_record_subject where mrid = ? and busitype = ? and usertype = ? and compid = ?
     */
    public final static String getMeetingSubjectByMridByType = "getMeetingSubjectByMridByType";

    /**
     * 查询发布状态的工作汇报，可选条件
     * ---- select * from meeting_report_record where compid=:compid and status = 2
     * <p>
     * select * from meeting_report_record where compid=:compid  //20200608 取消状态
     */
    public final static String queryReportSelection = "queryReportSelection";

    /** *********************************************************************************会议记录变会议 *******************************************/
    /**
     * 查询会议列表信息
     * select * from view_meeting_notice_record where  orgcode = :orgcode
     * and status = :status
     * and mrtype = :mrtype
     * and createtime >= :starttime
     * and createtime <= :endtime
     * and topic like ${PARAMS.likeStr}
     */

    public final static String pageMeetingByOrg = "pageMeetingByOrg";

    /**
     * 新增会议与会议记录关联
     * insert into meeting_record_relation set compid = :compid,noticeid = :noticeid,mrid = :mrid,type = :type
     */
    public final static String addMeetingRecordRelation = "addMeetingRecordRelation";

    /**
     * 删除会议与会议记录的关联关系
     * <p>
     * delete from meeting_record_relation where mrid = ? and compid = ?
     */
    public final static String deleteRecordRelationByMrid = "deleteRecordRelationByMrid";

    /**
     * 我的会议列表
     * select * from view_meeting_user_map where userid = :userid and orgcode = :orgcode and status = 2
     * and  mrtype = :mrtype
     * and  starttime >= :starttime
     * and starttime <= :endtime
     * and topic like '%${PARAMS.likeStr}%'
     */
    public final static String getMyMeetingPage = "getMyMeetingPage";


    /******************************** 待办事项（会议） ********************************/

    /**
     * 查询会议待办事项
     * select * from meeting_to_do_record where compid = :compid
     * and status = :status
     */
    public final static String queryMeetingTodoList = "queryMeetingTodoList";

    /**
     * 更新会议待办状态
     * update meeting_to_do_record set status = :status where compid = :compid and id = :id
     */
    public final static String updateMeetingTodoStatus = "updateMeetingTodoStatus";

    /**
     * 查询会议待办
     * select * from meeting_to_do_record where compid = ? and id = ?
     */
    public final static String queryMeetingTodo = "queryMeetingTodo";


    /**
     * 修改党员会议参加状态
     * update meeting_record_subject set status = ? where compid = ? and mrid = ? and busitype = 2 and usertype = 6
     */
    public final static String updateMeetingAttend = "updateMeetingAttend";

    /**
     * 修改 参会状态
     * update meeting_record_subject set status = ? where busitype  = 2 and usertype = 6 and mrid = ? and userid = ? and compid = ?
     */
    public final static String updateMeetingAttendStatus = "updateMeetingAttendStatus";

    /**
     * select * from meeting_notice_record where compid=? and createid=? order by createtime desc limit 1
     */
    public final static String getUpMeeting = "getUpMeeting";
    /**
     * 获取支部下当下组数
     * <p>
     * select count(*) from t_party_group where orgcode = ? and compid = ?
     */
    public final static String countPartGroupByOrgcode = "countPartGroupByOrgcode";


    /******************************** 会议待办事项批处理 ********************************/

    /**
     * SELECT * FROM sys_param where compid=1 and upparamkey='meeting_remind_time_set'
     */
    public final static String listSysParamMeetTime = "listSysParamMeetTime";

    /**
     * SELECT * FROM meeting_todo_time where compid=? and code=? and publishtime=?
     */
    public final static String getMeetingTodoTime = "getMeetingTodoTime";

    /**
     * INSERT INTO meeting_todo_time (compid, code, publishtime, name, status, remarks1, remarks2, remarks3, remarks4) VALUES (:compid, :code, :publishtime, :name, 0, 0, 0, '', '')
     */
    public final static String addMeetingTodoTime = "addMeetingTodoTime";

    /**
     * SELECT * FROM meeting_todo_time where compid=? and publishtime<=?
     */
    public final static String listMeetingTodoTime = "listMeetingTodoTime";

    /**
     * SELECT * FROM urc_user where compid=? and status=? and postname=?
     */
    public final static String listUrcUserByPostname = "listUrcUserByPostname";

    public final static String listTDzzBzcyxx = "listTDzzBzcyxx";

    /**
     * select b.* from urc_user_org_map a left join urc_user b on a.compid=b.compid and a.userid=b.userid
     * left join urc_organization c on a.compid=c.compid and a.orgid=c.orgid
     * where b.accflag=1 and c.orgtypeid not in ('6300000037','6300000038')
     */
    public final static String listUrcUserByAccflag = "listUrcUserByAccflag";

    /**
     * UPDATE meeting_todo_time SET  status = 2 WHERE compid = ? and code = ? and publishtime = ?
     */
    public final static String modiMeetingTodoTimeStatus = "modiMeetingTodoTimeStatus";

    /**
     * INSERT INTO meeting_to_do_record (compid,busitype, businessid, mcode, mcodename, title, content, customdata, ttype, status, recivuserid, recivusername, createtime, senderid, sendername, updatetime, remarks1, remarks2, remarks3, remarks4)
     * VALUES (:compid, :busitype, :businessid, :mcode, :mcodename, :title, :content, :customdata, :ttype, :status, :recivuserid, :recivusername, :createtime, :senderid, :sendername, :updatetime, :remarks1, :remarks2, :remarks3, :remarks4)
     */
    public final static String addMeetingToDoRecord = "addMeetingToDoRecord";

    /**
     * SELECT * FROM meeting_to_do_record where compid = ? and mcode= ? and left(createtime,10)=? and recivuserid = ? limit 1
     */
    public final static String getMeetingToDoRecord = "getMeetingToDoRecord";

    /**
     * SELECT a.* FROM urc_organization a  where a.compid=? and a.orgid=?
     */
    public final static String getUrcOrganization = "getUrcOrganization";

    /**
     * 查询消息，分割时间串
     * select substring(a.remind,1, 10) as date,a.* from tk_task_remind_list a where a.userid = ? and a.compid = ?
     */
    public final static String queryMsgCalDate = "queryMsgCalDate";

    /**
     * 通过我的会议列表更新待办状态
     * update meeting_to_do_record set status = ? where busitype = ? and businessid = ? and recivuserid = ? and compid = ?
     */
    public final static String updateTotoByMeetingAttend = "updateTotoByMeetingAttend";

    /**
     * 获取个人签名的base64码
     * select jsondata from view_user_account where account = ?  and compid = ?
     */
    public final static String getPersonalSign = "getPersonalSign";

    /**
     * SELECT * FROM view_meeting_notice_record where compid=? and id=? limit 1
     */
    public final static String getMeetingRecord = "getMeetingRecord";

    /**
     * 当前用户未读消息数
     * select count (*) from tk_task_remind_list where userid = ? and isread = ? and compid =?
     */
    public final static String getMsgNumByuserId = "getMsgNumByuserId";


//---------------------------------------工作台--------------------------------

    /**
     * 快捷入口列表查询
     * select * from fast_function_enter where compid=? and userid=?
     */
    public static String listFastFunctionEnter = "listFastFunctionEnter";

    /**
     * select * from urc_function where compid=? and fctcode=? and fctflag=1 limit 1
     */
    public static String getUrcFunction = "getUrcFunction";

    /**
     * select * from fast_function_enter where compid=? and userid=? and functioncode=? limit 1
     */
    public static String getFastUrcFunction = "getFastUrcFunction";

    /**
     * INSERT INTO fast_function_enter (compid, userid, functioncode, functionname, functionrout) VALUES (:compid, :userid, :functioncode, :functionname, :functionrout);
     */
    public static String addFastFunctionEnter = "addFastFunctionEnter";

    /**
     * delete from fast_function_enter where compid=? and userid=? and functioncode=?
     */
    public static String delFastFunctionEnter = "delFastFunctionEnter";

    /**
     * delete from fast_function_enter where compid=? and userid=?
     */
    public static String delFastFunctionEnters = "delFastFunctionEnters";

    /**
     * SELECT paramkey,paramvalue,remarks2,remarks3,remarks4 FROM sys_param where compid=? and paramkey in ('appraisal_of_members','branch_committee','org_life_meeting','party_group_meeting','party_lecture','theme_party_day','theoretical_study','branch_party_congress')
     */
    public static String listSysParamByParamkey = "listSysParamByParamkey";

    /**
     * select organid  from  t_dzz_info where organcode = ?
     */
    public static String getOrganidByorgCode = "getOrganidByorgCode";

    /**
     * select * from t_dzz_info_simple where ${PARAMS.path} = :organid and zzlb in ('6300000037','6400000038')
     */
    public static String listChildOrgan = "listChildOrgan";

    /**
     * SELECT * FROM t_dzz_hjxx_dzz where organid=? order by hjdate desc limit 1
     */
    public static String getLastElectionDate = "getLastElectionDate";

    /**
     * INSERT INTO sys_dynamics (compid, businessid, orgid, orgcode, orgname, dynamicstext, dynamicstype, dynamicsname, createtime, createuserid, remarks1, remarks2, remarks3, remarks4) VALUES (:compid, :businessid, :orgid, :orgcode, :orgname, :dynamicstext, :dynamicstype, :dynamicsname, :createtime, :createuserid, :remarks1, :remarks2, :remarks3, :remarks4)
     */
    public static String addSysDnamics = "addSysDnamics";

    /**
     * delete from sys_dynamics where compid=? and businessid=? and orgid=?
     */
    public static String delSysDnamics = "delSysDnamics";

    /**
     * select * from sys_dynamics
     */
    public static String listSysDnamics = "listSysDnamics";

    /**
     * SELECT count(*) FROM view_tag_meeting where orgcode=? and left(starttime,7)=? and left(createtime,7)<? and mrtype=? and status=2
     */
    public static String queryMeetingRecordNum = "queryMeetingRecordNum";

    /**
     * SELECT count(*) FROM view_tag_meeting where orgcode=? and left(starttime,7)=? and left(createtime,7)<=? and mrtype=? and status=2
     */
    public static String queryMeetingRecordNum2 = "queryMeetingRecordNum2";

    /**
     * SELECT count(*) FROM meeting_record where orgcode=? and left(starttime,7)=? and left(createtime,7)<? and mrtype=? and status=2
     */
    public static String queryMeetingRecordNum3 = "queryMeetingRecordNum3";

    /**
     * SELECT count(*) FROM meeting_record where orgcode=? and left(starttime,7)=? and left(createtime,7)<=? and mrtype=? and status=2
     */
    public static String queryMeetingRecordNum4 = "queryMeetingRecordNum4";

    /**
     * SELECT count(*) FROM meeting_record where orgcode=? and left(starttime,7)>=? and left(starttime,7)<=? and left(createtime,7)>=? and left(createtime,7)<=?
     */
    public static String queryMeetingRecordNum5 = "queryMeetingRecordNum5";

    /**
     * SELECT count(*) FROM meeting_record where orgcode=? and left(starttime,7)>=? and left(starttime,7)<=? and left(createtime,7)<? and left(createtime,7)>?
     */
    public static String queryMeetingRecordNum6 = "queryMeetingRecordNum6";

    /**
     * SELECT count(*) FROM view_tag_meeting where orgcode=? and left(starttime,7)>=? and left(starttime,7)<=? and left(createtime,7)<? and left(createtime,7)>?
     */
    public static String queryMeetingRecordNum9 = "queryMeetingRecordNum9";

    /**
     * SELECT starttime FROM meeting_record where orgcode=? and left(starttime,7) <= left(createtime,7) and mrtype=? and status=2 and mryear=? group by left(starttime,7)
     */
    public static String queryMeetingRecordNum7 = "queryMeetingRecordNum7";

    /**
     * SELECT starttime FROM view_tag_meeting where orgcode=? and left(starttime,7) <= left(createtime,7) and mrtype=? and status=2 and mryear=? group by left(starttime,7)
     */
    public static String queryMeetingRecordNum8 = "queryMeetingRecordNum8";


    /**
     * 查询当前组织下的党小组下拉列表
     * SELECT id,name FROM `t_party_group` where orgcode = ? and compid = ?
     */
    public final static String getPartyGroupSel = "getPartyGroupSel";

    /**
     * 党小组人员查询
     * select memberid as userid ,membername as username,idcard ,:subjecttype as usertype from t_party_group_member where groupid = :id  and compid = :compid
     * and type = :type
     */
    public final static String listGroupMemeberSubjects = "listGroupMemeberSubjects";

    /**
     * INSERT INTO meeting_type_statistics (compid, meetcode, standard, nostandard, remarks1, remarks2) VALUES (:compid, :meetcode, :standard, :nostandard, :remarks1, :remarks2)
     */
    public static String addMeetingTypeStatistics = "addMeetingTypeStatistics";

    /**
     * delete from meeting_type_statistics where compid=?
     */
    public static String delMeetingTypeStatistics = "delMeetingTypeStatistics";

    /**
     * SELECT a.meetcode paramkey,a.remarks2 paramvalue,a.standard standardNum,a.nostandard noStandardNum FROM meeting_type_statistics a where compid=? and orgcode=?
     */
    public static String listMeetingType = "listMeetingType";

    /**
     * SELECT a.orgid FROM urc_organization a left join t_dzz_info b on a.orgcode=b.organcode where a.compid=? and b.zzlb not in ('6300000037','6300000038')
     */
    public static String listOrgDzz = "listOrgDzz";

    /**
     * 根据会议通知id获取整个会议详情
     * select * from view_meeting_notice_record where id = ?  and compid = ?
     */
    public final static String getMeetingInfoByNoticeid = "getMeetingInfoByNoticeid";

    /**
     * select * from  sys_param where compid=? and upparamkey=?
     */
    public static String listSysDnamicsType = "listSysDnamicsType";

    /**
     * 当月召开会议数(标签)
     * select count(*) from view_tag_meeting where mryear = ? and mrmonth = ? and orgcode = ? and mrtype = ? and status =? and compid = ?
     */
    public final static String countTagMeetingTypeByOrgcodeM = "countTagMeetingTypeByOrgcodeM";

    /**
     * 当月召开会议数
     * select count(*) from meeting_record where mryear = ? and mrmonth = ? and orgcode = ? and mrtype = ? and status  =  ? and compid = ?
     */
    public final static String countMeetingTypeByOrgcodeM = "countMeetingTypeByOrgcodeM";


    /**
     * 当前时间段召开(标签)会议数
     * select count(*) from view_tag_meeting where mryear = :year and find_in_set(mrmonth, :months) and orgcode = :orgcode and mrtype = :mrtype  and status  =  :status and compid = :compid
     */
    public final static String countTagMeetingTypeByOrgcodeML = "countTagMeetingTypeByOrgcodeML";

    /**
     * 当前时间段召开(标签)会议数
     * select count(*) from view_tag_meeting where mryear = :year and find_in_set(mrmonth, :months) and orgcode = :orgcode and mrtype = :mrtype  and status  =  :status and compid = :compid
     */
    public final static String countMeetingTypeByOrgcodeML = "countMeetingTypeByOrgcodeML";


    /** ***************************************** 投票相关****************/

    /**
     * 新增投票关联
     * insert into  meeting_extra_attr set compid = :compid,subjecttype = :subjecttype,subjectid = :subjectid,extra_attr = :extra_attr,extra_attr_value = :extra_attr_value
     */
    public final static String addVoteRelation = "addVoteRelation";

    /**
     * 获取会议扩展属性值
     * select extra_attr_value from meeting_extra_attr where compid = ? and subjecttype = ? and subjectid = ? and extra_attr = ?
     */
    public final static String getMeetingExtraAttr = "getMeetingExtraAttr";

    /**
     * 修改投票关联
     * update  meeting_extra_attr set extra_attr_value = :extra_attr_value where compid = :compid and subjecttype = :subjecttype and subjectid = :subjectid and extra_attr = :extra_attr
     */
    public final static String updateVoteRelation = "updateVoteRelation";

    /**
     * 新增投票关联
     * insert into meeting_business_map set compid = :compid ,subject_id = :subject_id,subject_type = :subject_type,subjected_name = :subjected_name,subjected_id = :subjected_id,subjected_extrkey = :subjected_extrkey
     */
    public final static String addMeetingBusinessMap = "addMeetingBusinessMap";

    /**
     * 按会议主体id删除关联
     * delete from meeting_business_map where compid = ? and subject_id = ? and subject_type = ?
     */
    public final static String delMeetingBusinessMapBySid = "delMeetingBusinessMapBySid";

    /**
     * 查询关联列表
     * select * from  meeting_business_map  where compid = ? and subject_id = ? and subject_type = ?
     */
    public final static String lisMeeingBusiMapBySid = "lisMeeingBusiMapBySid";

    /**
     * 会议(会议通知)和会议记录关联表
     * select * from meeting_record_relation where mrid = ?
     */
    public final static String getMeetingRecordRelation = "getMeetingRecordRelation";

    /**
     * 修改通知
     * update meeting_notice_record  set topic=:topic,place=:place,subnum=:realitynum,updateid=:updateid,updatetime=:updatetime
     */
    public final static String updateMNoticePlace = "updateMNoticePlace";

    /**
     * 根据通知id删除缺席人员
     * delete from  meeting_absent_subject where no = ? and compid= ?
     */
    public final static String delMeeAbsentSub = "delMeeAbsentSub";

    /**
     * 增加缺席人员记录
     * insert into meeting_absent_subject set noticeid=:noticeid,userid=:userid,username=:username,compid=:compid
     */
    public final static String saveMeetingAbsentSub = "saveMeetingAbsentSub";

    /**
     * 获取缺席人员
     * select * from meeting_absent_subject where noticeid=? and compid = ?
     */
    public final static String getMeetingAbsentSub = "getMeetingAbsentSub";

    /**
     * 获取所有组织机构
     * select * from t_dzz_info_simple where operatetype !=3 and zzlb != '3100000025' and zzlb != '3900000026'
     */
    public final static String queryTdzzInfoSimple3 = "queryTdzzInfoSimple3";

    /**
     * 获取会议召开次数设置
     * select * from  sys_param where (upparamkey = 'OLM_CODE' or upparamkey='TMOC' or paramkey = 'theoretical_study_time') and paramkey !='TMOC' and paramkey !='theoretical_study_time'
     */
    public final static String getMeetingTypeList = "getMeetingTypeList";

    /**
     * 获取重点工作数量
     * select count(*) from tk_task_deploy where  orgcode =:orgcode and (status =:status1 or status = :status2 or status = :status3 or status = :status4 )
     */
    public final static String getZdgzNum = "getZdgzNum";


    /**
     * 增加会议二维码code
     * update
     */
    public final static String addMeetingTicketCode = "addMeetingTicketCode";


    /**
     * 根据会议ID查询会议参与人
     * select * from meeting_record_subject where mrid = ? and busitype = ? and compid = ? and remark1 = 1;
     */
    public static String getMeetingSubjectByMrid2 = "getMeetingSubjectByMrid2";

    /**
     * 获取通知id
     * select * from meeting_record_relation where mrid = ?
     */
    public static String getMeetingRecordRelation2 = "getMeetingRecordRelation2";


    /**
     * 获取缺席人员名
     * select GROUP_CONCAT(username) from meeting_absent_subject where noticeid = ?
     */
    public static String getMeetingAbsentName = "getMeetingAbsentName";

    /**
     * 按参与人分页查询会议通知
     * select a.* from meeting_notice_record a
     * left join meeting_record_subject b on b.mrid = a.id
     * where b.userid = :userid
     */
    public final static String pageMeetingByOrg2 = "pageMeetingByOrg2";
    /**
     * 查询logo图片base64
     */
    public final static String getBase64Logo = "SELECT logobase64 FROM urc_base_set;";

    /**
     * 获取所有模板
     */
    public static final String pageMeetingReportTemplate = "SELECT * FROM `meeting_report_template` WHERE compid =? AND `orgid` = ? AND `orgcode` = ?";
    /**
     * 添加模板
     */
    public static final String addMeetingReportTemplate = "INSERT INTO meeting_report_template set compid=:compid,file_id=:fileId, file_name=:fileName,orgid=:orgid, orgcode=:orgcode, createtime=:createtime,remark=:remark";
    /**
     * 删除模板
     */
    public static final String delMeetingReportTemplate = "delete from meeting_report_template where id=? ";
    /**
     * 修改模板
     */
    public static final String updateMeetingReportTemplate = "UPDATE meeting_report_template SET file_id=? WHERE id=?";

    /**
     * 根据userid查询党员基本信息
     * SELECT * FROM `t_dy_info`  WHERE userid =?;
     */
    public static String getTDYInfo = "SELECT * FROM `t_dy_info`  WHERE userid =?";

    /**
     * 根据parentcode 获取所有组织code
     * SELECT * FROM t_dzz_info WHERE parentcode =?
     */
    public static String getTDZZInfoByParentCode = "SELECT organcode FROM t_dzz_info WHERE parentcode =?";

    /**
     * 获取微信扫码签到人员信息
     * select * from meeting_record_subject where compid = ? and mrid = ? and usertype = ? and remark1 = ?;
     */
    public static String getWxSignSubjects = "getWxSignSubjects";
    /**
     * select organid,organcode,zzlb,dzzmc as organname  from  t_dzz_info_simple where parentid = :organid and zzlb=\'6100000035\' and (delflag !=1 or delflag is  null)
     */
    public static String MeetingPatrolListByZzlb = "MeetingPatrolListByZzlb";
    /**
     * select t.* from ( select a.organid,a.organcode, a.dzzmc,IFNULL(b.num, 0) covered ,IF(b.num>= :calvalue,1,0) type
     * from (select * from t_dzz_info_simple where ${PARAMS.childpath} = :organid) a
     * left join (select count(*) num ,orgcode,mrtype count from  ${PARAMS.tablename} where status = 2 and mrtype = :mrtype
     */
    public static String getMeetingCommittees = "getMeetingCommittees";
    /**
     * select IFNULL(sum(t.covered),0)  num ,IF (sum(t.type) > 0, 0, 1) type
     * from (\r\n	select a.organid,a.dzzmc,IFNULL(b.num, 0) covered ,IF(b.num>= :calvalue,0,1) type
     * from (select * from t_dzz_info_simple where ${PARAMS.childpath} = :childorganid and (delflag !=1 or delflag is  null) and (operatetype != 3 or operatetype is null)
     * and zzlb = \'6100000035\') a left join (select count(*) num ,orgcode,mrtype count from  ${PARAMS.tablename} where status = 2 and mrtype = :mrtype
     * and time >= :starttime
     * and time <= :endtime
     * group by orgcode,mrtype) b on a.organcode = b.orgcode ) t
     */
    public static String getCommitteeMeetingCountAndSum = "getCommitteeMeetingCountAndSum";

    //会议议题
    public static final String addMeetingRecordTopics = "addMeetingRecordTopics";
    public static final String delMeetingRecordTopicsById = "delMeetingRecordTopicsById";
    public static final String modifyMeetingRecordTopicsById = "modifyMeetingRecordTopicsById";
    public static final String getMeetingRecordTopicsById = "getMeetingRecordTopicsById";
    public static final String listMeetingRecordTopics = "listMeetingRecordTopics";
    public static final String pageMeetingRecordTopics = "pageMeetingRecordTopics";
    public static final String delMeetingRecordTopicsByMrid = "delMeetingRecordTopicsByMrid";

}

