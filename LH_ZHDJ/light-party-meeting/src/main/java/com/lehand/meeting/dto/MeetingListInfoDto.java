package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

import com.lehand.meeting.pojo.MeetingRecord;

import java.util.UUID;

@ApiModel(value="会议列表信息", description="会议列表信息")
public class MeetingListInfoDto extends MeetingRecord {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="表单key", name="formkey")
    private String formkey;

    @ApiModelProperty(value="通知", name="noticeid")
    private String noticeid;



    public String getFormkey() {
        return StringUtils.isEmpty(formkey) ? UUID.randomUUID().toString().replaceAll("-",""):formkey;
    }

    public void setFormkey(String formkey) {
        this.formkey = formkey;
    }

    public String getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(String noticeid) {
        this.noticeid = noticeid;
    }
}
