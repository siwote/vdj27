package com.lehand.meeting.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class RestTemplateUtil {

    /**
     * 创建指定字符集的RestTemplate
     *
     * @param charset
     * @return
     */
    public static RestTemplate getInstance(String charset) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName(charset)));
        return restTemplate;
    }

    /**
     * 获取data内容
     * @param str
     * @return
     */
    public static String getData(String str) {
        if(StringUtils.isEmpty(str)) {
            return "";
        }
        JSONObject object = (JSONObject) JSONObject.parse(str);
        return object.get("data").toString();
    }
}
