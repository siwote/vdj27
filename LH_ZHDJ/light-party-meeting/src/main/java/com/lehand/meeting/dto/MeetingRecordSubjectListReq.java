package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingRecordSubject;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @program: huaikuang-dj
 * @description: MeetingRecordSubjectListReq
 * @author: zwd
 * @create: 2020-11-09 09:07
 */
public class MeetingRecordSubjectListReq implements Serializable {
    @ApiModelProperty(value="会议参加人", name="subjects")
    private List<MeetingRecordSubject> subjects;

    @ApiModelProperty(value="通知id", name="noticeid")
    private String noticeid;

    public List<MeetingRecordSubject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<MeetingRecordSubject> subjects) {
        this.subjects = subjects;
    }

    public String getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(String noticeid) {
        this.noticeid = noticeid;
    }
}