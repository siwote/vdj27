package com.lehand.meeting.service;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.components.converter.doc.DocConverter;
import com.lehand.components.converter.doc.ExcelConverter;
import com.lehand.components.converter.doc.PPTConverter;
import com.lehand.components.converter.doc.WordConverter;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.meeting.constant.Constant;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.constant.common.MeetingPMtypeEnum;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.meeting.dto.MeetingRecordTopicsDto;
import com.lehand.meeting.pojo.*;
import com.lehand.meeting.utils.SysUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import sun.misc.BASE64Decoder;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Date 2020/05/11
 * @Descp 会议相关的PDF文件的生成和转换功能服务类
 */
@Service
public class MeetingPDFService {

    @Resource
    private DFSComponent dFSComponent;

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private MeetingRecordTopicsService meetingRecordTopicsService;

    @Resource
    private MeetingService meetingService;

    @Value("${upload-path}")
    private String uploadTempPath;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;

    //上传路径
    @Value("${spring.http.multipart.location}")private String location;

    /**
     * A4的宽度
     */
    public static final int PAGE_A4_IMAGE_WIDTH = 500;

    /**
     * A4的高度
     */
    public static final int PAGE_A4_IMAGE_HEIGHT = 375;

    /**
     * LOGO的宽度
     */
    public static final int PAGE_LOGO_IMAGE_WIDTH = 50;

    /**
     * LOGO的高度
     */
    public static final int PAGE_LOGO_IMAGE_HEIGHT = 30;

    /**
     * 将生成的签到表PDF文件上传到文件系统并返回文件信息
     * @param fileName
     * @param time
     * @param topic
     * @param nameList
     * @return
     * @throws Exception
     */
    public Map<String,Object> uploadSignToFdfs(String fileName , String time, String topic, List<Map<String,Object>> nameList, Session session) throws Exception {
        String fullFilePath = generateSignPDF(fileName, time, topic, nameList,session);
        return getPdfPreview(fileName, fullFilePath, session, 4);
    }

    /**
     * 返回DFS路径的文件信息
     * @param fileName
     * @param fullFilePath
     * @param session
     * @return
     * @throws Exception
     */
    public Map<String,Object> getPdfPreview(String fileName, String fullFilePath, Session session, int type) throws Exception {
        File file = new File(fullFilePath);
        if(!file.exists()) {
            LehandException.throwException("没有可以打印的会议记录！");
        }
        String name = file.getName();
        String filetype=name.substring(name.lastIndexOf(".")+1).toLowerCase();
        String dfsFilePath = dFSComponent.upload(fullFilePath);

        DlFile dlFile=new DlFile();
        //路径
        dlFile.setDir(dfsFilePath);
        //文件名称
        dlFile.setName(String.format("%s.pdf", fileName));
        //文件大小
        dlFile.setSize(file.length());
        //文件类型
        dlFile.setType(filetype);
        String remark= StringConstant.EMPTY;
        dlFile.setRemark(remark);
        dlFile.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dlFile.setCompid(session.getCompid());
        dlFile.setUsertor(session.getUserid());
        dlFile.setRemark1(0);
        dlFile.setRemark2(0);
        dlFile.setRemark3(StringConstant.EMPTY);
        dlFile.setRemark4(StringConstant.EMPTY);
        dlFile.setRemark5(StringConstant.EMPTY);
        dlFile.setRemark6(StringConstant.EMPTY);
        Long fileId = generalSqlComponent.insert(MeetingSqlCode.addDlFile, dlFile);
        dlFile.setId(fileId);
        FileUtils.forceDeleteOnExit(file);

        //返回的数据集合
        Map<String,Object> result=new HashMap<String,Object>(8);
        //id
        result.put("id", dlFile.getId());
        String dir=dlFile.getDir();
        //上传到服务器后的地址
        result.put("dir", dir);
        //名称，包含后缀名
        result.put("name", dlFile.getName());
        //大小
        result.put("size", dlFile.getSize());
        //类型
        result.put("type", dlFile.getType());
        result.put("url",downIp+dir);
        //前端
        result.put("checked", false);
        //前端
        result.put("disabled", false);
        result.put("intostatus", type);
        return result;
    }

    /**
     * 生成签到表PDF文件
     * @param time
     * @param topic
     * @param nameList
     * @return
     * @throws Exception
     */
    public String generateSignPDF(String fileName, String time, String topic, List<Map<String,Object>> nameList,Session session) throws Exception {
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 22, Font.NORMAL);
        Font FontChinese11Normal = new Font(bfChinese, 11, Font.NORMAL);
        Font FontChinese18 = new Font(bfChinese, 18,Font.NORMAL);

        String uuid = UUID.randomUUID().toString();
        String fullFilePath = String.format("%s/%s.pdf", uploadTempPath, uuid);
        //实例化文档以及属性
        Document document = new Document(PageSize.A4,50,50,30,20);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fullFilePath));
        document.open();
        document.newPage();
        writer.setPageEmpty(false);

        //标题
        document.add(generateTitle(fileName, titlefont));
        //空行
        document.add(new Paragraph(18f, " ", FontChinese18));
        //召开时间
        document.add(addtableLine("召开时间：", time, FontChinese11Normal));
        //议题
        document.add(addtableLine("议程：", topic, FontChinese11Normal));
        //参会人员
        document.add(addtableLine("参会人员：", "", FontChinese11Normal));
        //参会人签名
        int len = nameList.size();
        for(int i = 0 ; i < len +1 ; i = i+3) {
            PdfPTable signTable = new PdfPTable(3);
            signTable.getDefaultCell().setBorder(0);
            signTable.getDefaultCell().setPaddingTop(20);
            if(i >= len) {
                signTable.addCell("");
                signTable.addCell("");
                signTable.addCell("");
                document.add(signTable);
                continue;
            }
            PdfPCell cell1 = new PdfPCell();
            cell1.setBorder(0);
//            cell1.addElement(Image.getInstance(new URL(String.format("file:////%s",createImage(nameList.get(i))))));
            cell1.addElement(Image.getInstance(new URL(String.format("file:////%s",createSignImage(nameList.get(i),session.getCompid())))));
            signTable.addCell(cell1);
            if((i+1) >= len) {
                signTable.addCell("");
                signTable.addCell("");
                document.add(signTable);
                continue;
            }
            PdfPCell cell2 = new PdfPCell();
            cell2.setBorder(0);
//            cell2.addElement(Image.getInstance(new URL(String.format("file:////%s",createImage(nameList.get(i+1))))));
            cell2.addElement(Image.getInstance(new URL(String.format("file:////%s",createSignImage(nameList.get(i+1),session.getCompid())))));
            signTable.addCell(cell2);
            if((i+2) >= len) {
                signTable.addCell("");
                document.add(signTable);
                continue;
            }
            PdfPCell cell3 = new PdfPCell();
            cell3.setBorder(0);
//            cell3.addElement(Image.getInstance(new URL(String.format("file:////%s",createImage(nameList.get(i+2))))));
            cell3.addElement(Image.getInstance(new URL(String.format("file:////%s",createSignImage(nameList.get(i+2),session.getCompid())))));
            signTable.addCell(cell3);

            document.add(signTable);
        }
        document.close();
        //返回生成的PDF的全路径
        return fullFilePath;
    }

    //会议签名处理
    private String createSignImage(Map<String, Object> stringObjectMap,Long compid)throws Exception {
        Object idcard = stringObjectMap.get("idcard");
        String name = stringObjectMap.get("name").toString();
        if(StringUtils.isEmpty(idcard)){
            return createImage(name);
        }
        //获取个人签名
        String jsonData = generalSqlComponent.query(MeetingSqlCode.getPersonalSign,new Object[]{idcard,compid});
        if(StringUtils.isEmpty(jsonData)||MagicConstant.STR.EMPTY_JSON.equals(jsonData)){
            return createImage(name);
        }

        return createBase64Image(jsonData);
    }

    /**
     * 生成base64码
     * @param jsonData
     * @return
     */
    private String createBase64Image(String jsonData) {
        String uuid = "sign_"+UUID.randomUUID().toString();
        File file = new File(String.format("%s%s%s.png", uploadTempPath, File.separator, uuid));
        byte[] b;
        try {
            b = new BASE64Decoder().decodeBuffer(jsonData.replace("data:image/png;base64,", ""));
            for (int i = 0; i < b.length; i++) {
                if(b[i]<0) {
                    b[i]+=256;
                }
            }
            OutputStream out = new FileOutputStream(file.getAbsoluteFile());
            out.write(b);
            out.flush();
            out.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return file.getAbsolutePath();
        }
    }

    /**
     * 添加每一行的明细
     * @param name
     * @param value
     * @return
     * @throws DocumentException
     */
    public PdfPTable addtableLine(String name, String value, Font font) throws DocumentException {
        PdfPTable timeTable = new PdfPTable(2);
        timeTable.setWidthPercentage(100);
        timeTable.getDefaultCell().setBorder(0);
        timeTable.getDefaultCell().setPaddingTop(20);
        int timeWidth[] = {16,84};
        timeTable.setWidths(timeWidth);
        PdfPCell cell11 = new PdfPCell(new Paragraph(name, font));
        cell11.setBorder(0);
        cell11.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        PdfPCell cell12 = new PdfPCell(new Paragraph(value, font));
        cell12.setBorder(0);
        cell12.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        timeTable.addCell(cell11);
        timeTable.addCell(cell12);
        return timeTable;
    }
    /**
     * 创建标题
     * @param titlefont
     * @return
     */
    public Paragraph generateTitle(String title, Font titlefont) {
        Paragraph paragraph = new Paragraph(title, titlefont);
        paragraph.setAlignment(1); //设置文字居中 0靠左   1，居中     2，靠右
        paragraph.setIndentationLeft(12); //设置左缩进
        paragraph.setIndentationRight(12); //设置右缩进
        paragraph.setFirstLineIndent(24); //设置首行缩进
        paragraph.setLeading(20f); //行间距
        paragraph.setSpacingBefore(5f); //设置段落上空白
        paragraph.setSpacingAfter(10f); //设置段落下空白
        return paragraph;
    }
    /**
     * 创建logo
     *
     * @return
     */
    public void generateLogo(Document document) throws Exception {
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap(MeetingSqlCode.getBase64Logo, new Object[]{});
        String logoData = (String) map.get("logobase64");
        Image image = Image.getInstance(createBase64Image(logoData));
        getImageLogo(image);
        document.add(image);
    }
    /**
     * 将字符串生成图片
     * @param name
     * @return
     * @throws Exception
     */
    public String createImage(String name) throws Exception {
        BufferedImage image = createImage(name, new java.awt.Font("微软雅黑", java.awt.Font.PLAIN, 120), 640, 320);
        String uuid = UUID.randomUUID().toString();
        File file = new File(String.format("%s%s%s.png", uploadTempPath, File.separator, uuid));
        if(!file.exists()) {
            file.createNewFile();
        }
        ImageIO.write(image, "png", file);
        System.out.println(file.getAbsolutePath());
        return file.getAbsolutePath();
    }


    /**
     * 将汉字生成图片（java awt）
     * @param str
     * @param font
     * @param width
     * @param height
     * @return
     * @throws Exception
     */
    public BufferedImage createImage(String str, java.awt.Font font,Integer width, Integer height) throws Exception {
        // 创建图片
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_BGR);
        Graphics g = image.getGraphics();
        g.setClip(0, 0, width, height);
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);// 先用黑色填充整张图片,也就是背景
        g.setColor(Color.black);// 在换成黑色
        g.setFont(font);// 设置画笔字体

        /** 用于获得垂直居中y */
        java.awt.Rectangle clip = g.getClipBounds();
        FontMetrics fm = g.getFontMetrics(font);
        int ascent = fm.getAscent();
        int descent = fm.getDescent();
        int y = (clip.height - (ascent + descent)) / 2 + ascent;
        g.drawString(str, 40, y);// 画出字符串
        g.dispose();
        return image;
    }

    /**
     * 导出多个会议记录合成PDF文件
     * @param orgcode
     * @param mrtype
     * @param mrids
     * @param session
     */
    public Map<String, Object> meetingBatchExport(String orgcode, String mrtype, String mrids, Session session) throws Exception {
        switch (mrtype) {
            case  SysConstants.MTYPE.MEETING_TALK : //谈心谈话
                String path2 = talkMeetingBatch(orgcode, mrids, session);
                return getPdfPreview(getMeetingTypeHead(mrtype), path2, session, 5);
            case SysConstants.MTYPE.MEETING_REPORT: //工作汇报
                String path3 = reportMeetingBatch(orgcode, mrids, session);
                return getPdfPreview(getMeetingTypeHead(mrtype), path3, session, 5);
            default:
                String path = branchCommiteMeetingBatch(orgcode, mrids, mrtype, session);
                return getPdfPreview(getMeetingTypeHead(mrtype), path, session, 5);

        }
    }

    /**
     * 回去会议记录内容主体
     * @param orgcode
     * @param mrids
     * @param mrtype
     * @param session
     * @return
     */
    public List<MeetingRecord> getMeetingRecords(String orgcode, String mrids, String mrtype, Session session) {
        //如果是党员大会下的标签会议，统一修改为党员大会类型
        if(mrtype.equals(SysConstants.MTYPE.THEORETICAL_STUDY) || mrtype.equals(SysConstants.MTYPE.ORG_LIFE_MEETING)
                || mrtype.equals(SysConstants.MTYPE.APPRAISAL_OF_MEMBERS)) {
//            mrtype = SysConstants.MTYPE.BRANCH_PARTY_CONGRESS;
            mrtype = "1";
        }

        Map<String, Object> params = new HashMap<>();
        params.put("compid", session.getCompid());
        params.put("orgcode", orgcode);
        params.put("mrids", mrids);
        params.put("mrtype", mrtype);
        return generalSqlComponent.query(MeetingSqlCode.queryMeetingByAddition2, params);
    }

    /**
     * 将附件合并到集合中
     * @param id
     * @param session
     * @return
     */
    public List<Map<String, Object>> mergeAttachment(String id, Session session) {
        //原附件
        List<Map<String, Object>> meetingAttachList = meetingService.getAttachmentInfo(id,3, MagicConstant.NUM_STR.NUM_10,session);
//        //签到表
//        List<Map<String, Object>> signTable = meetingService.getAttachmentInfo(id,4,MagicConstant.NUM_STR.NUM_10,session);
//        if(meetingAttachList.size() < 0 ) {
//            meetingAttachList = new ArrayList<>();
//        }
//        //加入签到表，注意顺序
//        if(signTable.size() > 0) {
//            meetingAttachList.add(signTable.get(0));
//        }
        return meetingAttachList;
    }

    /**
     * 根据会议类别返回不同的会议参加人的字符串
     * @param meetingRecord
     * @param type
     * @param usertype
     * @param compid
     * @return
     */
    public String getMeetingSubjects(MeetingRecord meetingRecord, int type,int usertype ,Long compid) {
        List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMridByType,
                new Object[] {meetingRecord.getMrid(), type,usertype,compid});
        StringBuffer stringBuffer = new StringBuffer();
        for(MeetingRecordSubject subject : subjects) {
            stringBuffer.append(subject.getUsername()).append("  ");
        }
        return stringBuffer.toString();
    }

    public String branchCommiteMeetingBatch(String orgcode, String mrids, String mrtype, Session session) throws Exception {
        //查询会议记录
        List<MeetingRecord> meetingRecords = getMeetingRecords(orgcode, mrids, mrtype, session);
        //保存多个会议记录路径，最终合并返回
        List<String> meetingPdfs = new ArrayList<>();
        for(MeetingRecord meetingRecord : meetingRecords) {
            //会议参加人拼接
            String subjectStr = getMeetingSubjects(meetingRecord, 0, 0,session.getCompid());
            //附件
            List<Map<String, Object>> meetingAttachList = mergeAttachment(String.valueOf(meetingRecord.getMrid()), session);
            //pdf处理
            String singleMeetingPdfPath = branchMeeting(meetingRecord, subjectStr, meetingAttachList);
            meetingPdfs.add(singleMeetingPdfPath);
        }

        //合并PDF并返回最终的可以打印的PDF路径
        String finalPDFPath = String.format("%s%s%s.pdf", uploadTempPath, File.separator, UUID.randomUUID().toString());
        mergePdfFiles(meetingPdfs, finalPDFPath);
        return finalPDFPath;
    }

    public String reportMeetingBatch(String orgcode, String mrids, Session session) throws Exception {
        //查询会议记录
        Map<String, Object> params = new HashMap<>();
        params.put("compid", session.getCompid());
        params.put("orgcode", orgcode);
        params.put("mrids", mrids);
        List<MeetingReportRecord> reportRecordList = generalSqlComponent.query(MeetingSqlCode.queryReportSelection, params);
        //保存多个会议记录路径，最终合并返回
        List<String> meetingPdfs = new ArrayList<>();
        for(MeetingReportRecord meetingReportRecord : reportRecordList) {
            //附件
            List<Map<String, Object>> meetingAttachList = mergeAttachment(String.valueOf(meetingReportRecord.getId()), session);
            //pdf处理
            String singleMeetingPdfPath = reportMeeting(meetingReportRecord, meetingAttachList);
            meetingPdfs.add(singleMeetingPdfPath);
        }

        //合并PDF并返回最终的可以打印的PDF路径
        String finalPDFPath = String.format("%s%s%s.pdf", uploadTempPath, File.separator, UUID.randomUUID().toString());
        mergePdfFiles(meetingPdfs, finalPDFPath);
        return finalPDFPath;
    }

    public String reportMeeting(MeetingReportRecord meetingReportRecord, List<Map<String, Object>> meetingAttachList) throws Exception {
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 22, Font.NORMAL);
        Font fontChinese11Normal = new Font(bfChinese, 11, Font.NORMAL);
        Font fontChinese11Under = new Font(bfChinese, 11, Font.UNDERLINE);

        String uuid = UUID.randomUUID().toString();
        String fullFilePath = String.format("%s/%s.pdf", uploadTempPath, uuid);
        //实例化文档以及属性
        Document document = new Document(PageSize.A4,50,50,30,20);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fullFilePath));
        document.open();
        document.newPage();
        writer.setPageEmpty(false);

        //标题
        String title = getMeetingTypeHead(SysConstants.MTYPE.MEETING_REPORT);
        document.add(generateTitle(title, titlefont));
        //以下是会议记录内容
        PdfPTable pdfPTable = new PdfPTable(4);
        getPdfTableSetting(pdfPTable);
        reportTable(meetingReportRecord, fontChinese11Normal, fontChinese11Under, pdfPTable,document ,writer);

//        document.add(pdfPTable);
        //返回单个会议记录的最终PDF文件路径
        return meetingAttachment(document, fullFilePath, meetingAttachList);
    }

    /**
     * 谈心谈话特殊处理
     * @param orgcode
     * @param mrids
     * @param session
     * @return
     * @throws Exception
     */
    public String talkMeetingBatch(String orgcode, String mrids, Session session) throws Exception {
        //查询会议记录
        Map<String, Object> params = new HashMap<>();
        params.put("compid", session.getCompid());
        params.put("orgcode", orgcode);
        params.put("mrids", mrids);
        List<MeetingTalkRecord> meetingTalkRecordList = generalSqlComponent.query(MeetingSqlCode.listMeetingTalkBySel, params);
        //保存多个会议记录路径，最终合并返回
        List<String> meetingPdfs = new ArrayList<>();
        for(MeetingTalkRecord meetingTalkRecord : meetingTalkRecordList) {
            //会议参加人拼接
            List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMridByType,
                    new Object[] {meetingTalkRecord.getId(), 1,4,session.getCompid()});
            StringBuffer username = new StringBuffer();
            StringBuffer postname = new StringBuffer();
            for(MeetingRecordSubject subject : subjects) {
                username.append(subject.getUsername()).append("  ");
                postname.append(subject.getPost()).append("  ");
            }

            //附件
            List<Map<String, Object>> meetingAttachList = mergeAttachment(String.valueOf(meetingTalkRecord.getId()), session);
            //pdf处理
            String singleMeetingPdfPath = talkMeeting(meetingTalkRecord, new String[] {username.toString(), postname.toString()}, meetingAttachList);
            meetingPdfs.add(singleMeetingPdfPath);
        }

        //合并PDF并返回最终的可以打印的PDF路径
        String finalPDFPath = String.format("%s%s%s.pdf", uploadTempPath, File.separator, UUID.randomUUID().toString());
        mergePdfFiles(meetingPdfs, finalPDFPath);
        return finalPDFPath;
    }

    /**
     * 适用会议类型（支部委员会）
     * @param meetingRecord
     * @param subjects
     * @param meetingAttachList
     * @throws Exception
     */
    public String branchMeeting(MeetingRecord meetingRecord, String subjects, List<Map<String, Object>> meetingAttachList) throws Exception {
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 22, Font.NORMAL);
        Font fontChinese11Normal = new Font(bfChinese, 11, Font.NORMAL);
        Font fontChinese11Under = new Font(bfChinese, 11, Font.UNDERLINE);

        String uuid = UUID.randomUUID().toString();
        String fullFilePath = String.format("%s/%s.pdf", uploadTempPath, uuid);
        //实例化文档以及属性
        Document document = new Document(PageSize.A4,50,50,30,20);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fullFilePath));
        document.open();
        document.newPage();
        writer.setPageEmpty(false);
        //logo
//        generateLogo(document);
        //公司
        String companyName =meetingRecord.getOrgname();
//        companyName="安徽力瀚科技";
        document.add(generateTitle(companyName, titlefont));
        //标题
        String title = getMeetingTypeHead(meetingRecord.getMrtype());
        document.add(generateTitle(title, titlefont));
        //以下是会议记录内容
        PdfPTable pdfPTable = new PdfPTable(4);
        getPdfTableSetting(pdfPTable);

        switch (meetingRecord.getMrtype()) {
            case SysConstants.MTYPE.BRANCH_COMMITTEE://支部委员会
                branchCommiteTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case SysConstants.MTYPE.BRANCH_PARTY_CONGRESS: //党员大会
                branchPartyTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case "1": //2021-04-27去掉会议属性新需求
                branchPartyTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case SysConstants.MTYPE.APPRAISAL_OF_MEMBERS: //民主评议党员
                branchPartyTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case SysConstants.MTYPE.PARTY_GROUP_MEETING: //党小组会
                partyGroupTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable, pdfPTable,document,writer);
                break;
            case  SysConstants.MTYPE.DEMOCRATIC_LIFE_MEETING  : //民主生活会
                branchCommiteTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case  SysConstants.MTYPE.ORG_LIFE_MEETING: //组织生活会
                branchPartyTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case  SysConstants.MTYPE.THEME_PARTY_DAY: //主题党日
                themePartyTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case  SysConstants.MTYPE.THEORETICAL_STUDY: //党委中心组学习
                branchPartyTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case SysConstants.MTYPE.PARTY_LECTURE: //上党课
                lectureTable(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
            case SysConstants.MTYPE.COMMITTEE_MEETING: //党委会
                branchCommiteTableDwh(meetingRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable,document,writer);
                break;
                default:
                    break;
        }
//        document.add(pdfPTable);
        //返回单个会议记录的最终PDF文件路径
        return meetingAttachment(document, fullFilePath, meetingAttachList);
    }

    /**
     * 谈心谈话
     * @param meetingTalkRecord
     * @param subjects
     * @param meetingAttachList
     * @return
     * @throws Exception
     */
    public String talkMeeting(MeetingTalkRecord meetingTalkRecord, String[] subjects, List<Map<String, Object>> meetingAttachList) throws Exception {
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 22, Font.NORMAL);
        Font fontChinese11Normal = new Font(bfChinese, 11, Font.NORMAL);
        Font fontChinese11Under = new Font(bfChinese, 11, Font.UNDERLINE);

        String uuid = UUID.randomUUID().toString();
        String fullFilePath = String.format("%s/%s.pdf", uploadTempPath, uuid);
        //实例化文档以及属性
        Document document = new Document(PageSize.A4,50,50,30,20);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fullFilePath));
        document.open();
        document.newPage();
        writer.setPageEmpty(false);

        //标题
        String title = getMeetingTypeHead(SysConstants.MTYPE.MEETING_TALK);
        document.add(generateTitle(title, titlefont));
        //以下是会议记录内容
        PdfPTable pdfPTable = new PdfPTable(4);
        getPdfTableSetting(pdfPTable);

        talkTable(meetingTalkRecord, fontChinese11Normal, fontChinese11Under, subjects, pdfPTable);

        document.add(pdfPTable);
        //返回单个会议记录的最终PDF文件路径
        return meetingAttachment(document, fullFilePath, meetingAttachList);
    }


    /**
     * 支部委员会的表格（支部委员会、民主生活会）
     * @param meetingRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pdfPTable
     */
    public void branchCommiteTableDwh(MeetingRecord meetingRecord, Font fontChinese11Normal, Font fontChinese11Under,
                                    String subjects, PdfPTable pdfPTable,Document document,PdfWriter writer) throws Exception {
        //第一行
        String[] s1 = {"时间：", meetingRecord.getStarttime(), "地点：", meetingRecord.getPlace()};
        addPdfRowCell4(s1, fontChinese11Normal, pdfPTable);
        //第二行
        String[] s2 = {"主持人：", meetingRecord.getModeratorname(), "记录人：", meetingRecord.getRecordername()};
        addPdfRowCell4(s2, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s3 = {"参会人：", subjects};
        getPdfTableRowCell2(s3, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s4 = {"列席人员：", meetingRecord.getRemark3()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);
        //第五行
        String[] s5 = {"缺席人员：",getMeetingAbsentName(meetingRecord.getMrid().toString())};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);
        //第六行
//        String[] s6 = {"会议决定：", meetingRecord.getDecision()};
//        getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);
        //第七行
        MeetingRecordTopicsDto meetingRecordTopicsDto =new MeetingRecordTopicsDto();
        meetingRecordTopicsDto.setMrid(meetingRecord.getMrid());
        List<MeetingRecordTopicsDto> topicsDtoList = meetingRecordTopicsService.listMeetingRecordTopics(new Session(), meetingRecordTopicsDto);

        StringBuffer sb = new StringBuffer();

        String[] s7 ;
        if(topicsDtoList.size()<1){
            s7 = new String[]{"议题：", meetingRecord.getTopic()};
            getPdfTableRowCell2(s7, fontChinese11Normal, pdfPTable);
        }else{

            for(int i=0;i<topicsDtoList.size();i++){
                s7 =  new String[]{"议题"+(i+1)+"：", topicsDtoList.get(i).getTopics()};
                getPdfTableRowCell2(s7, fontChinese11Normal, pdfPTable);
            }
        }


        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("内容", titlefont));
        //内容行
        getPdfContextNew(meetingRecord.getContext(),fontChinese11Under,document,writer);
    }

    /**
     * 支部委员会的表格（支部委员会、民主生活会）
     * @param meetingRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pdfPTable
     */
    public void branchCommiteTable(MeetingRecord meetingRecord, Font fontChinese11Normal, Font fontChinese11Under, String subjects, PdfPTable pdfPTable,Document document,PdfWriter writer) throws Exception {
        //第一行
        String[] s1 = {"时间：", meetingRecord.getStarttime(), "地点：", meetingRecord.getPlace()};
        addPdfRowCell4(s1, fontChinese11Normal, pdfPTable);
        //第二行
        String[] s2 = {"主持人：", meetingRecord.getModeratorname(), "记录人：", meetingRecord.getRecordername()};
        addPdfRowCell4(s2, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s3 = {"参会人：", subjects};
        getPdfTableRowCell2(s3, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s4 = {"列席人员：", meetingRecord.getRemark3()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);
        //第五行
        String[] s5 = {"缺席人员：",getMeetingAbsentName(meetingRecord.getMrid().toString())};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);
        //第六行
        String[] s6 = {"会议决定：", meetingRecord.getDecision()};
        getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);

        //第七行
        String[] s7 = {"议题：", meetingRecord.getTopic()};
        getPdfTableRowCell2(s7, fontChinese11Normal, pdfPTable);

        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("内容", titlefont));
        //内容行
        getPdfContextNew(meetingRecord.getContext(),fontChinese11Under,document,writer);
    }

    /**
     * 内容行
     * @param context
     * @param fontChinese11Under
     * @param document
     */
    private void getPdfContextNew(String context, Font fontChinese11Under, Document document,PdfWriter writer) throws Exception {
        //去除样式(否则计算字节会错误)
        Pattern ps = Pattern.compile("\\s*|\t|\r|\n");
        Matcher m = ps.matcher(context);
        context = m.replaceAll("");
//        //获取总字节数
//        byte[] con = context.getBytes();
//        int totalNum = con.length;
//        int maxNum = 210;


        java.awt.Font font = new java.awt.Font("UniGB-UCS2-H", Font.UNDERLINE, 11);
        FontMetrics fm = sun.font.FontDesignMetrics.getMetrics(font);
        int totalLen = fm.stringWidth(context);
        int  released = 0;

        if(totalLen<473){
            released = 464 -totalLen;
        }else{
            released = 473- (totalLen -464)%473;
        }
        int tl = released/3;
        if(tl>0){
            StringBuffer buffer = new StringBuffer(context);
            for(int i = 0;i<tl;i++){
                buffer.append(" ");
            }
//            buffer.append(".");
            context = buffer.toString();
        }

//        int remainder = 0;
//
//        if(totalNum<maxNum){
//            remainder = maxNum -(totalNum+12)*3/2;
//        }
//        else{//TODDO 解决最后一行问题
//            remainder = maxNum - (totalNum*3/2)%maxNum;
//        }
//        if(remainder>0){
//            StringBuffer buffer = new StringBuffer(context);
//            for(int i = 0;i<190;i++){
//                buffer.append(" ");
//            }
//            buffer.append("。");
//            context = buffer.toString();
//        }

        Chunk chunk = new Chunk(context, fontChinese11Under);
        chunk.setLineHeight(18);
        Paragraph p = new Paragraph();
        p.add(chunk);
        p.setAlignment(Element.ALIGN_LEFT);
        p.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        p.setFirstLineIndent(20);
        document.add(p);
        Method getPdfDocument = writer.getClass().getDeclaredMethod("getPdfDocument");
        getPdfDocument.setAccessible(true);
        PdfDocument pdfD = (PdfDocument) getPdfDocument.invoke(writer);
        Field getHeight = pdfD.getClass().getDeclaredField("currentHeight");
        getHeight.setAccessible(true);
        float height = (Float)getHeight.get(pdfD);
        int totalheight = 762;


//        java.awt.Font font = new java.awt.Font("Arial Unicode MS", Font.BOLD, 10);
//        java.awt.Font font = new java.awt.Font(""STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
//        new Font(bfChinese, 11, Font.UNDERLINE);
//        第一种方式：
//        FontMetrics fm = sun.font.FontDesignMetrics.getMetrics(font);
////        第二种方式：
////        FontMetrics metrics = new JLabel().getFontMetrics(font);
//        int ws = fm.stringWidth(context);
//        String em = "";
//        String en = "1";
//        String eh = "汉";
//        String eb = "，";
//        int emw = fm.stringWidth(em);
//        int enw = fm.stringWidth(en);
//        int ehw = fm.stringWidth(eh);
//        int ebw = fm.stringWidth(eb);

        float th = totalheight;
        float diff = th - height;
        double diffline = Math.floor(diff/16.9);
        int lines =  (new Double(diffline)).intValue();

        //70个空格
        String empty_space = "                                                                       ";
        String empty_line = empty_space+empty_space+empty_space;
        Paragraph blankRow41 = new Paragraph(18, empty_line, fontChinese11Under);
        blankRow41.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        int j = 0;
        for(int i = 0;i<lines-1;i++){
            document.add(blankRow41);
            System.out.println(i);
            j +=1;
        }
    }

    /**
     *  @param meetingRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pdfPTable
     * @param document
     * @param writer
     */
    public void themePartyTable(MeetingRecord meetingRecord, Font fontChinese11Normal, Font fontChinese11Under, String subjects, PdfPTable pdfPTable, Document document, PdfWriter writer) throws Exception {
        //第一行
        String[] s1 = {"时间：", meetingRecord.getStarttime(), "地点：", meetingRecord.getPlace()};
        addPdfRowCell4(s1, fontChinese11Normal, pdfPTable);
        //第二行
        String[] s2 = {"记录人：", meetingRecord.getRecordername()};
        getPdfTableRowCell2(s2, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s3 = {"参会人：", subjects};
        getPdfTableRowCell2(s3, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s4 = {"列席人员：", meetingRecord.getRemark3()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);
        //第五行
        String[] s5 = {"缺席人员：",getMeetingAbsentName(meetingRecord.getMrid().toString())};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);
        //第六行
        String[] s9 = {"会议决定：", meetingRecord.getDecision()};
        getPdfTableRowCell2(s9, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s6 = {"主题：", meetingRecord.getTopic()};
        getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);
//        //第五行
//        String[] s5 = {"内容：", meetingRecord.getContext()};
//        getPdfContext(s5, fontChinese11Normal, fontChinese11Under, pdfPTable);
        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("活动记录", titlefont));
        //内容行
        getPdfContextNew(meetingRecord.getContext(),fontChinese11Under,document,writer);
    }

    /**
     * 工作汇报
     * @param meetingReportRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param pdfPTable
     */
    public void reportTable(MeetingReportRecord meetingReportRecord, Font fontChinese11Normal, Font fontChinese11Under, PdfPTable pdfPTable,Document document,PdfWriter writer) throws Exception{
        String[] s4 = {"主题：", meetingReportRecord.getTopic()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);

        if(meetingReportRecord.getStatus()==4||meetingReportRecord.getStatus()==5){
            String[] s5 = {"审核状态：", meetingReportRecord.getStatus()==4?"审核通过":"审核驳回"};
            getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);

            String[] s6 = {"审核时间：", meetingReportRecord.getUpdatetime()};
            getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);

            String[] s7 = {"审核意见：", meetingReportRecord.getRemark2()};
            getPdfTableRowCell2(s7, fontChinese11Normal, pdfPTable);
        }

//        String[] s5 = {"工作汇报：", SysUtil.delHTMLTag(meetingReportRecord.getReportContent())};
//        getPdfContext(s5, fontChinese11Normal, fontChinese11Under, pdfPTable);

        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("工作汇报", titlefont));
        //内容行
        getPdfContextNew(SysUtil.delHTMLTag(meetingReportRecord.getReportContent()),fontChinese11Under,document,writer);
    }

    /**
     * 谈心谈话
     * @param meetingTalkRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pdfPTable
     */
    public void talkTable(MeetingTalkRecord meetingTalkRecord, Font fontChinese11Normal, Font fontChinese11Under, String[] subjects, PdfPTable pdfPTable) {
        if(subjects.length < 2) {
            return;
        }
        String[] s4 = {"主题：", meetingTalkRecord.getTopic()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);

        String[] s5 = {"时间：", meetingTalkRecord.getRecordtime()};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);

//        String[] s6 = {"谈话内容：", meetingTalkRecord.getContext()};
        String[] s6 = {"谈话内容：", SysUtil.delHTMLTag(meetingTalkRecord.getContext())};
        getPdfContext(s6, fontChinese11Normal, fontChinese11Under, pdfPTable);

        String[] s7 = {"谈话人姓名：", subjects[0]};
        getPdfTableRowCell2(s7, fontChinese11Normal, pdfPTable);

        String[] s8 = {"谈话人职务：", subjects[1]};
        getPdfTableRowCell2(s8, fontChinese11Normal, pdfPTable);
    }

    /**
     * 党小组会议记录表格
     * @param meetingRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pTable
     * @param pdfPTable
     * @param document
     * @param writer
     */
    public void partyGroupTable(MeetingRecord meetingRecord, Font fontChinese11Normal, Font fontChinese11Under, String subjects, PdfPTable pTable, PdfPTable pdfPTable, Document document, PdfWriter writer) throws Exception {
        //第一行
        String[] s1 = {"时间：", meetingRecord.getStarttime(), "地点：", meetingRecord.getPlace()};
        addPdfRowCell4(s1, fontChinese11Normal, pdfPTable);
        //第二行
        String[] s2 = {"主持人：", meetingRecord.getModeratorname(), "记录人：", meetingRecord.getRecordername()};
        addPdfRowCell4(s2, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s7= {"党小组名称：", meetingRecord.getGroupname()};
        getPdfTableRowCell2(s7, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s3 = {"参会人：", subjects};
        getPdfTableRowCell2(s3, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s4 = {"列席人员：", meetingRecord.getRemark3()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);
        //第五行
        String[] s5 = {"缺席人员：",getMeetingAbsentName(meetingRecord.getMrid().toString())};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);
        //第六行
        String[] s9 = {"会议决定：", meetingRecord.getDecision()};
        getPdfTableRowCell2(s9, fontChinese11Normal, pdfPTable);
        //第六行
        String[] s6 = {"议题：", meetingRecord.getTopic()};
        getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);
//        //第五行
//        String[] s5 = {"内容：", meetingRecord.getContext()};
//        getPdfContext(s5, fontChinese11Normal, fontChinese11Under, pdfPTable);


        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("内容", titlefont));
        //内容行
        getPdfContextNew(meetingRecord.getContext(),fontChinese11Under,document,writer);
    }

    /**
     * 党课
     * @param meetingRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pdfPTable
     * @param document
     * @param writer
     */
    public void lectureTable(MeetingRecord meetingRecord, Font fontChinese11Normal, Font fontChinese11Under, String subjects, PdfPTable pdfPTable, Document document, PdfWriter writer) throws Exception{
        //第一行
        String[] s1 = {"授课时间：", meetingRecord.getStarttime(), "地点：", meetingRecord.getPlace()};
        addPdfRowCell4(s1, fontChinese11Normal, pdfPTable);
        //第二行
        String[] s2 = {"授课人：", meetingRecord.getModeratorname(), "听课人数：", String.valueOf(meetingRecord.getRealitynum())};
        addPdfRowCell4(s2, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s3 = {"参会人：", subjects};
        getPdfTableRowCell2(s3, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s4 = {"列席人员：", meetingRecord.getRemark3()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);
        //第五行
        String[] s5 = {"缺席人员：",getMeetingAbsentName(meetingRecord.getMrid().toString())};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);
        //第六行
        String[] s9 = {"会议决定：", meetingRecord.getDecision()};
        getPdfTableRowCell2(s9, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s6 = {"授课内容：", meetingRecord.getTopic()};
        getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);
//        //第五行
//        String[] s5 = {"情况内容：", meetingRecord.getContext()};
//        getPdfContext(s5, fontChinese11Normal, fontChinese11Under, pdfPTable);

        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("情况内容", titlefont));
        //内容行
        getPdfContextNew(meetingRecord.getContext(),fontChinese11Under,document,writer);
    }

    /**
     * 会议记录表格（支部党员大会、党委中心组学习、组织生活会、民主评议党员）branchPartyTable
     * @param meetingRecord
     * @param fontChinese11Normal
     * @param fontChinese11Under
     * @param subjects
     * @param pdfPTable
     * @param document
     * @param writer
     */
    public void branchPartyTable(MeetingRecord meetingRecord, Font fontChinese11Normal, Font fontChinese11Under, String subjects, PdfPTable pdfPTable, Document document, PdfWriter writer) throws Exception {
        //第一行
        String[] s1 = {"时间：", meetingRecord.getStarttime(), "地点：", meetingRecord.getPlace()};
        addPdfRowCell4(s1, fontChinese11Normal, pdfPTable);
        //第二行
        String[] s2 = {"主持人：", meetingRecord.getModeratorname(), "记录人：", meetingRecord.getRecordername()};
        addPdfRowCell4(s2, fontChinese11Normal, pdfPTable);

        //第二行
        String[] s7 = {"应到人数：", String.valueOf(meetingRecord.getShouldnum()), "实到人数：", String.valueOf(meetingRecord.getRealitynum())};
        addPdfRowCell4(s7, fontChinese11Normal, pdfPTable);
        //第三行
        List<String> list = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[] {meetingRecord.getMrid(),0,meetingRecord.getCompid()});
        StringBuffer stringBuffer = new StringBuffer();
        for(String s : list) {
            stringBuffer.append(MeetingPMtypeEnum.getMsg(s)).append("  ");
        }
        String[] s8 = {"会议属性：", stringBuffer.toString()};
        getPdfTableRowCell2(s8, fontChinese11Normal, pdfPTable);
        //第三行
        String[] s3 = {"参会人：", subjects};
        getPdfTableRowCell2(s3, fontChinese11Normal, pdfPTable);
        //第四行
        String[] s4 = {"列席人员：", meetingRecord.getRemark3()};
        getPdfTableRowCell2(s4, fontChinese11Normal, pdfPTable);
        //第五行
        String[] s5 = {"缺席人员：",getMeetingAbsentName(meetingRecord.getMrid().toString())};
        getPdfTableRowCell2(s5, fontChinese11Normal, pdfPTable);
        //第六行
        String[] s9 = {"会议决定：", meetingRecord.getDecision()};
        getPdfTableRowCell2(s9, fontChinese11Normal, pdfPTable);

        //第七行
        String[] s6 = {"议题：", meetingRecord.getTopic()};
        getPdfTableRowCell2(s6, fontChinese11Normal, pdfPTable);
//        //第五行
//        String[] s5 = {"内容：", meetingRecord.getContext()};
//        getPdfContext(s5, fontChinese11Normal, fontChinese11Under, pdfPTable);

        //将表格加入pdf
        document.add(pdfPTable);

        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titlefont = new Font(bfChinese, 14, Font.NORMAL);
        document.add(generateTitle("内容", titlefont));
        //内容行
        getPdfContextNew(meetingRecord.getContext(),fontChinese11Under,document,writer);
    }


    /*
     * 会议附件文件的追加PDF
     */
    public String meetingAttachment(Document document, String fullFilePath, List<Map<String, Object>> meetingAttachList) throws Exception {
        List<String> files = new ArrayList<String>();
        String finalPDFPath = String.format("%s%s%s.pdf", uploadTempPath, File.separator, UUID.randomUUID().toString());
        files.add(fullFilePath);
        //将图片放在主体记录后面，然后关闭文档流
        for(Map<String, Object> attach : meetingAttachList) {
            String type = attach.get("type").toString();
            boolean picFlag = "png".equals(type) ||"jpeg".equals(type)||"jpg".equals(type);
            if(picFlag) {
                byte[] fileByte = dFSComponent.download(attach.get("dir").toString());
                //3、流转换成File
                File src = getFile(fileByte, location, attach.get("dir").toString());
                Image image = Image.getInstance(src.getAbsolutePath());
                getImageSetting(image);
                document.add(image);
                //空行 (同一页中的加空格,高度过半不加空格)
                float realHeight = image.getScaledHeight();
                if(realHeight<=355){
                    document.add(new Paragraph(20f, " ", getBlankLine(20)));
                }

            }
        }
        //记录文档流结束
        document.close();

        //将word类型的文件转化为PDF已经PDF文件合并
        for(Map<String, Object> attach : meetingAttachList) {
            String type = attach.get("type").toString();
            boolean wordFlag="doc".equals(type)||"docx".equals(type)||"xls".equals(type)||"xlsx".equals(type)|| "ppt".equals(type)||"pptx".equals(type);
            boolean pdfFlag = "pdf".equals(type);

            if(wordFlag) {
                String pdfPath = wordToPdf(attach.get("dir").toString(), type);
                files.add(pdfPath);
            }
            if(pdfFlag) {
                byte[] fileByte = dFSComponent.download(attach.get("dir").toString());
                //3、流转换成File
                File src = getFile(fileByte, location, attach.get("dir").toString());
                files.add(src.getAbsolutePath());
            }
        }
        mergePdfFiles(files, finalPDFPath);
        return finalPDFPath;
    }


    /**
     * 合并原pdf为新文件
     * @param files   pdf绝对路径集
     * @param newfile 新pdf绝对路径
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    public static void mergePdfFiles(List<String> files, String newfile) throws IOException, DocumentException {
        if(files.size() == 0) {
            return;
        }
        Document document = new Document(new PdfReader(files.get(0)).getPageSize(1));
        PdfCopy copy = new PdfCopy(document, new FileOutputStream(newfile));
        PdfReader reader = null;
        document.open();
        for (int i = 0; i < files.size(); i++) {
            reader = new PdfReader(files.get(i));
            int n = reader.getNumberOfPages();
            for (int j = 1; j <= n; j++) {
                document.newPage();
                PdfImportedPage page = copy.getImportedPage(reader, j);
                copy.addPage(page);
            }
        }
        if(reader != null ) {
            reader.close();
        }
        document.close();
    }

    /**
     * 将doc docx xls xlsx ppt pptx 转化为PDF
     * @param path
     * @throws Exception
     */
    public String wordToPdf(String path, String type) throws Exception {
        //2、下载文件
        File target;
        byte[] fileByte = dFSComponent.download(path);
        //3、流转换成File
        File src = getFile(fileByte, location, path);
        //转码后的文件
        target= new File(location+"/"+path+".pdf");
        //4、office转为html
        if(!converter(src,target,type)) {
            LehandException.throwException("文件转码失败！");
        }
        return target.getAbsolutePath();
    }
    /**
     * 图片样式设置
     * 设置图片居中，超过固定宽度后，对图片进行比例缩小
     * @param image
     */
    public void getImageLogo(Image image) {
        float realWidth = image.getScaledWidth();
        float realHeight = image.getScaledHeight();
        image.setAlignment(Image.ALIGN_LEFT);//设置靠左
        //宽度大于A4显示宽度，高度
        if(realWidth > PAGE_A4_IMAGE_WIDTH || realHeight > PAGE_A4_IMAGE_HEIGHT) {
            //宽度超过预期显示宽度
            if(realWidth > PAGE_A4_IMAGE_WIDTH) {
                image.scaleAbsoluteWidth(PAGE_LOGO_IMAGE_WIDTH);
                image.scaleAbsoluteHeight((realHeight * PAGE_LOGO_IMAGE_WIDTH) / realWidth);
                return; //这里返回，是宽度进行缩放后，不再进行高度的缩放
            }
            //高度超过预期显示高度
            if(realHeight > PAGE_A4_IMAGE_HEIGHT) {
                image.scaleAbsoluteHeight(PAGE_LOGO_IMAGE_HEIGHT);
                image.scaleAbsoluteWidth((realWidth * PAGE_LOGO_IMAGE_HEIGHT) / realHeight);
            }
        }
    }
    /**
     * 图片样式设置
     * 设置图片居中，超过固定宽度后，对图片进行比例缩小
     * @param image
     */
    public void getImageSetting(Image image) {
        float realWidth = image.getScaledWidth();
        float realHeight = image.getScaledHeight();
        image.setAlignment(Image.ALIGN_CENTER);//设置居中
        //宽度大于A4显示宽度，高度
        if(realWidth > PAGE_A4_IMAGE_WIDTH || realHeight > PAGE_A4_IMAGE_HEIGHT) {
            //宽度超过预期显示宽度
            if(realWidth > PAGE_A4_IMAGE_WIDTH) {
                image.scaleAbsoluteWidth(PAGE_A4_IMAGE_WIDTH);
                image.scaleAbsoluteHeight((realHeight * PAGE_A4_IMAGE_WIDTH) / realWidth);
                return; //这里返回，是宽度进行缩放后，不再进行高度的缩放
            }

            //高度超过预期显示高度
            if(realHeight > PAGE_A4_IMAGE_HEIGHT) {
                image.scaleAbsoluteHeight(PAGE_A4_IMAGE_HEIGHT);
                image.scaleAbsoluteWidth((realWidth * PAGE_A4_IMAGE_HEIGHT) / realHeight);
            }
        }
    }

    /**
     * 转码
     *
     * @author: huruohan
     * @date: 2019年5月13日 下午2:10:14
     * @param src
     * @param target
     * @param type
     * @return
     * @throws Exception
     */
    private boolean converter(File src,File target,String type) throws Exception {
        DocConverter doc=null;
        synchronized (DocConverter.class) {
            //判断文件是不是需要转码，office都需要转码
            if(Constant.DOC.equals(type)||Constant.DOCX.equals(type)) {
                doc=new WordConverter();
            }else if(Constant.XLS.equals(type)||Constant.XLSX.equals(type)) {
                doc=new ExcelConverter();
            }else if(Constant.PPT.equals(type)||Constant.PPTX.equals(type)) {
                doc=new PPTConverter();
            }else {
                return false;
            }
            try {
                doc.convert(src, target);
            } catch (Exception e) {
                LehandException.throwException("文件格式不正确！");
            }
        }
        return true;
    }

    /**
     * 返回表格一行四个单元格
     * @param s
     * @param fontChinese11Normal
     * @param pdfPTable
     */
    public void addPdfRowCell4(String[] s, Font fontChinese11Normal, PdfPTable pdfPTable) {
        if(s.length <4) { //参数不满足条件
            return;
        }
        PdfPCell cell1 = new PdfPCell(new Paragraph(s[0], fontChinese11Normal));
        cell1.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell1.setBorder(0);
        pdfPTable.addCell(cell1);
        cell1.setFixedHeight(55);
        PdfPCell cell2 = new PdfPCell(new Paragraph(s[1], fontChinese11Normal));
        cell2.setBorder(2);
        cell2.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        pdfPTable.addCell(cell2);
        PdfPCell cell3 = new PdfPCell(new Paragraph(s[2], fontChinese11Normal));
        cell3.setBorder(0);
        cell3.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        pdfPTable.addCell(cell3);
        PdfPCell cell4 = new PdfPCell(new Paragraph(s[3], fontChinese11Normal));
        cell4.setBorder(2);
        cell4.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        pdfPTable.addCell(cell4);
    }

    public void getPdfTableSetting(PdfPTable pdfPTable) throws DocumentException {
        int[] width = {15,35,15,35};
        pdfPTable.setWidths(width);
        pdfPTable.setWidthPercentage(100);
        pdfPTable.getDefaultCell().setBorder(0);
        pdfPTable.getDefaultCell().setPaddingTop(20);
    }

    public void getPdfTableRowCell2(String[] s, Font fontChinese11Normal, PdfPTable pdfPTable) {
        if(s.length < 2) { //参数不满足条件
            return;
        }
        PdfPCell cell13 = new PdfPCell(new Paragraph(s[0], fontChinese11Normal));
        cell13.setBorder(0);
        cell13.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        pdfPTable.addCell(cell13);
        PdfPCell cell14 = new PdfPCell(new Paragraph(s[1], fontChinese11Normal));
        cell14.setBorder(2);
        cell14.setColspan(3);
        cell14.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        pdfPTable.addCell(cell14);
    }

    public void getPdfContext(String[] s, Font fontChinese11Normal, Font fontChinese11Under, PdfPTable pdfPTable) {
        if(s.length < 2) { //参数不满足条件
            return;
        }
        PdfPCell cell17 = new PdfPCell(new Paragraph(s[0], fontChinese11Normal));
        cell17.setBorder(0);
        cell17.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        pdfPTable.addCell(cell17);

        Chunk chunk = new Chunk(s[1], fontChinese11Under);
        chunk.setLineHeight(16);
        Paragraph p = new Paragraph();
        p.add(chunk);
        p.setAlignment(Element.ALIGN_CENTER);
        PdfPCell cell18 = new PdfPCell(p);
        cell18.setBorder(0);
        cell18.setColspan(3);
        cell18.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        pdfPTable.addCell(cell18);
    }

    /**
     * 将字节流转为文件并保存到本地的方法
     * @param data 字节流
     * @param filePath 本地的路径，d:/upload
     * @param fileName fastFds获取到的路径   group1/M01/00/02/wKg84Fyz5tuAI9soAAB8AOLLcUU944.jpg
     * @return
     * @throws IOException
     */
    private File getFile(byte[] data, String filePath,String fileName) throws IOException {
        String targetPath=filePath+"/"+fileName;
        File file  = new File(targetPath);
        //文件存在则删除
        if(file.exists()){
            file.delete();
        }
        //创建路径以及文件
        if(!file.getParentFile().exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //新建输出流，将字节输出到文件中
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(data,0,data.length);
        fos.flush();
        fos.close();
        return file;
    }

    /**
     * 根据会议类型查询会议名称
     * @param mrType
     * @return
     */
    public String getMeetingTypeHead(String mrType) {
        switch (mrType) {
            case SysConstants.MTYPE.BRANCH_COMMITTEE:
                return MeetingPMtypeEnum.BRANCH_COMMITTEE.getMsg();
            case SysConstants.MTYPE.BRANCH_PARTY_CONGRESS:
                return MeetingPMtypeEnum.BRANCH_PARTY_CONGRESS.getMsg();
            case  SysConstants.MTYPE.APPRAISAL_OF_MEMBERS  :
                return MeetingPMtypeEnum.APPRAISAL_OF_MEMBERS.getMsg();
            case  SysConstants.MTYPE.PARTY_GROUP_MEETING  :
                return MeetingPMtypeEnum.PARTY_GROUP_MEETING.getMsg();
            case  SysConstants.MTYPE.DEMOCRATIC_LIFE_MEETING  :
                return MeetingPMtypeEnum.DEMOCRATIC_LIFE_MEETING.getMsg();
            case  SysConstants.MTYPE.MEETING_TALK :
                return MeetingPMtypeEnum.MEETING_TALK.getMsg();
            case  SysConstants.MTYPE.ORG_LIFE_MEETING:
                return MeetingPMtypeEnum.ORG_LIFE_MEETING.getMsg();
            case  SysConstants.MTYPE.THEME_PARTY_DAY:
                return MeetingPMtypeEnum.THEME_PARTY_DAY.getMsg();
            case  SysConstants.MTYPE.THEORETICAL_STUDY:
                return MeetingPMtypeEnum.THEORETICAL_STUDY.getMsg();
            case  SysConstants.MTYPE.MEETING_REPORT:
                return MeetingPMtypeEnum.MEETING_REPORT.getMsg();
            case  SysConstants.MTYPE.PARTY_LECTURE:
                return MeetingPMtypeEnum.PARTY_LECTURE.getMsg();
            case  SysConstants.MTYPE.COMMITTEE_MEETING:
                return MeetingPMtypeEnum.COMMITTEE_MEETING.getMsg();
            default:
                return "会议记录";
        }
    }

    public Font getBlankLine(int size) throws IOException, DocumentException {
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font fontChinese = new Font(bfChinese, size, Font.NORMAL);
        return fontChinese;
    }

    /**
     * 组织生活所有会议类别名称和编码
     * @param session
     * @return
     */
    public List<Map<String,Object>> meetingViewList(Session session) {
        List<Map<String,Object>> list =  new ArrayList<Map<String,Object>>();
        String upparamcode = SysConstants.MTYPE.OLM_CODE;
        meetingService.recursionOrg(upparamcode,list,session.getCompid());

        //谈心谈话没有参数 手动加
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("paramkey","meeting_talk");
        map.put("paramname","string");
        map.put("upparamkey","OLM_CODE");
        map.put("paramvalue","谈心谈话记录");

        list.add(map);
        //工作汇报 手动加
        Map<String,Object> maps = new HashMap<String,Object>();
        maps.put("paramkey","meeting_report");
        maps.put("paramname","string");
        maps.put("upparamkey","OLM_CODE");
        maps.put("paramvalue","工作汇报");
        list.add(maps);
        return list;
    }


//    /**
//     * 递归查询下级类型
//     * @param upparamcode
//     * @param list
//     * @param compid
//     * @return
//     */
//    private List<Map<String,Object>> recursionOrg(String upparamcode, List<Map<String,Object>> list, Long compid) {
//        List<Map<String,Object>>    meet = generalSqlComponent.query(MeetingSqlCode.listSysParamByUpparamkey,new Object[]{upparamcode,compid});
//
//        if(meet.size()>0){
//            for (Map<String,Object> map:meet) {
//                //去掉三会一课父级
//                if(!String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.TMOC)){
//                    list.add(map);
//                }
//                recursionOrg(String.valueOf(map.get("paramkey")),list,compid);
//            }
//        }
//        return list;
//    }

    public Object print(HttpServletResponse response, String id, Session session)  throws Exception{
        Map<String,Object> map =  generalSqlComponent.query(MeetingSqlCode.getPrintFile,
                new HashMap<String,Object>(){{put("compid",session.getCompid());put("id",id);}});
//        File target;
        String path = String.valueOf(map.get("dir"));

        byte[] fileByte = dFSComponent.download(path);
        //3、流转换成File
        File src = getFile(fileByte, location, path);

        //八进制输出流
        response.setContentType("application/octet-stream");
        //这后面可以设置导出Excel的名称
        response.setHeader("Content-disposition", String.format("attachment;filename=%s%s.pdf", URLEncoder.encode(String.valueOf(map.get("name")),"UTF-8"), DateEnum.YYYYMMDDHHMMDD.format()));
        response.getOutputStream().write(fileByte);
        response.flushBuffer();
        return null;
    }

    public String getMeetingAbsentName(String mrid){
        Map<String,Object> meeting = generalSqlComponent.query(MeetingSqlCode.getMeetingRecordRelation2,new Object[]{mrid});
        return generalSqlComponent.query(MeetingSqlCode.getMeetingAbsentName,new Object[]{meeting.get("noticeid")});
    }
}
