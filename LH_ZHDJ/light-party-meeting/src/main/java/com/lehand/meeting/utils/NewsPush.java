package com.lehand.meeting.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.apache.tools.ant.launch.Locator.encodeURI;

/**
 * @author
 * @Title:
 * @Package
 * @Description:
 * @date 2020/8/2415:34
 */
public class NewsPush {
    public static String load(String url, String query) throws Exception {
        URL restURL = new URL(url);
        /*
         * 此处的urlConnection对象实际上是根据URL的请求协议(此处是http)生成的URLConnection类 的子类HttpURLConnection
         */
        HttpURLConnection conn = (HttpURLConnection) restURL.openConnection();
        //请求方式
        conn.setRequestMethod("POST");
        //设置是否从httpUrlConnection读入，默认情况下是true; httpUrlConnection.setDoInput(true);
        conn.setDoOutput(true);
        //allowUserInteraction 如果为 true，则在允许用户交互（例如弹出一个验证对话框）的上下文中对此 URL 进行检查。
        conn.setAllowUserInteraction(false);

//        PrintStream ps = new PrintStream(conn.getOutputStream());
//        ps.print(query);
//        ps.close();

        OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
        out.write(query);
        out.flush(); // 立即充刷至请求体）PrintWriter默认先写在内存缓存中
        BufferedReader bReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        String line, resultStr = "";

        while (null != (line = bReader.readLine())) {
            resultStr += line;
        }
        System.out.println("3412412---" + resultStr);
        bReader.close();
        return resultStr;
    }

    public static void main(String[] args) {
        try {
            String param = encodeURI(encodeURI("receiveAcc='340602197505051619','340602198710042417'&content=测试企业微信文本消息推送&sendAcc=B034020660156"));
            String resultString = load("http://10.1.50.243:9050/message/push/newsPush",param);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
        }
    }
}


