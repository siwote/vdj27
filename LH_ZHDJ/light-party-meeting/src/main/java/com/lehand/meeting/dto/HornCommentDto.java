package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.HornComment;
import com.lehand.meeting.pojo.HornCommentReply;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.io.Serializable;
import java.util.List;

@ApiModel(value="评论dto", description="评论dto")
public class HornCommentDto extends HornComment implements Serializable {

    @ApiModelProperty(value="回复列表", name="list")
    private List<HornCommentReply> list;

    public List<HornCommentReply> getList() {
        return list;
    }

    public void setList(List<HornCommentReply> list) {
        this.list = list;
    }
}