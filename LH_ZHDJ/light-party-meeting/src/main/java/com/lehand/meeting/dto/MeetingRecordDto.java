package com.lehand.meeting.dto;


import com.lehand.meeting.pojo.MeetingAbsentSubject;
import com.lehand.meeting.pojo.MeetingRecordTopics;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

import com.lehand.meeting.pojo.MeetingRecord;
import com.lehand.meeting.pojo.MeetingRecordSubject;

@ApiModel(value = "会议记录Dto", description = "会议记录Dto")
public class MeetingRecordDto extends MeetingRecord {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "会议参加人", name = "meetingRecordSubjects")
    private List<MeetingRecordSubject> meetingRecordSubjects;

    @ApiModelProperty(value = "主持人/讲课人ID(IDCARD)", name = "moderatoridcard")
    private String moderatoridcard;

    @ApiModelProperty(value = "记录人ID(IDCARD)", name = "recorderidcard")
    private String recorderidcard;

    @ApiModelProperty(value = "会议记录", name = "meetingRecordAttachList")
    private List<Map<String, Object>> meetingRecordAttachList;

    @ApiModelProperty(value = "会议签到", name = "meetingSignAttachList")
    private List<Map<String, Object>> meetingSignAttachList;

    @ApiModelProperty(value = "会议附件", name = "meetingAttachList")
    private List<Map<String, Object>> meetingAttachList;

    //2020-0522添加
    @ApiModelProperty(value = "会议(通知id)", name = "noticeid")
    private Long noticeid;

    @ApiModelProperty(value = "待办事项id", name = "todoid")
    private Long todoid;

    @ApiModelProperty(value = "缺席人员", name = "meetingAbsentSubject")
    private List<MeetingAbsentSubject> meetingAbsentSubject;

    @ApiModelProperty(value = "关联标签列表", name = "tagcode")
    private List<String> tagcode;

    @ApiModelProperty(value = "身份证号", name = "idcard")
    private String idcard;

    @ApiModelProperty(value = "用户名称", name = "username")
    private String username;

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<MeetingRecordSubject> getMeetingRecordSubjects() {
        return meetingRecordSubjects;
    }

    public void setMeetingRecordSubjects(List<MeetingRecordSubject> meetingRecordSubjects) {
        this.meetingRecordSubjects = meetingRecordSubjects;
    }

    public List<Map<String, Object>> getMeetingRecordAttachList() {
        return meetingRecordAttachList;
    }

    public void setMeetingRecordAttachList(List<Map<String, Object>> meetingRecordAttachList) {
        this.meetingRecordAttachList = meetingRecordAttachList;
    }

    public List<Map<String, Object>> getMeetingSignAttachList() {
        return meetingSignAttachList;
    }

    public void setMeetingSignAttachList(List<Map<String, Object>> meetingSignAttachList) {
        this.meetingSignAttachList = meetingSignAttachList;
    }

    public List<Map<String, Object>> getMeetingAttachList() {
        return meetingAttachList;
    }

    public void setMeetingAttachList(List<Map<String, Object>> meetingAttachList) {
        this.meetingAttachList = meetingAttachList;
    }

    public String getModeratoridcard() {
        return moderatoridcard;
    }

    public void setModeratoridcard(String moderatoridcard) {
        this.moderatoridcard = moderatoridcard;
    }

    public String getRecorderidcard() {
        return recorderidcard;
    }

    public void setRecorderidcard(String recorderidcard) {
        this.recorderidcard = recorderidcard;
    }

    public Long getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(Long noticeid) {
        this.noticeid = noticeid;
    }

    public Long getTodoid() {
        return todoid;
    }

    public void setTodoid(Long todoid) {
        this.todoid = todoid;
    }

    public List<MeetingAbsentSubject> getMeetingAbsentSubject() {
        return meetingAbsentSubject;
    }

    public void setMeetingAbsentSubject(List<MeetingAbsentSubject> meetingAbsentSubject) {
        this.meetingAbsentSubject = meetingAbsentSubject;
    }

    public List<String> getTagcode() {
        return tagcode;
    }

    public void setTagcode(List<String> tagcode) {
        this.tagcode = tagcode;
    }

}
