package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "主题党日-目标日", description = "主题党日-目标日")
public class MeetingThemeDay implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增长主键", name = "id")
    private Long id;

    @ApiModelProperty(value = "年份", name = "year")
    private Integer year;

    @ApiModelProperty(value = "提醒时间", name = "remindtime")
    private String remindtime;

    @ApiModelProperty(value = "组织编码", name = "orgcode")
    private String orgcode;

    @ApiModelProperty(value = "是否提醒（默认0 ，备用）", name = "status")
    private Integer status;

    @ApiModelProperty(value = "租户ID", name = "compid")
    private Long compid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getRemindtime() {
        return remindtime;
    }

    public void setRemindtime(String remindtime) {
        this.remindtime = remindtime;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
