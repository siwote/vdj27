package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.TkTaskRemindList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="消息提醒", description="消息提醒")
public class TkTaskRemindListDto extends TkTaskRemindList {

    @ApiModelProperty(value="日期", name="date")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
