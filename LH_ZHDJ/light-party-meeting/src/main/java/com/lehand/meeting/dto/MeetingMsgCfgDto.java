package com.lehand.meeting.dto;

import com.lehand.module.common.pojo.SystemParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value="会议提醒配置实体类", description="会议提醒配置实体类")
public class MeetingMsgCfgDto {

    @ApiModelProperty(value="会议提醒列表",name="msgCfgList")
    private List<SystemParam> systemParamList;

    public List<SystemParam> getSystemParamList() {
        return systemParamList;
    }

    public void setSystemParamList(List<SystemParam> systemParamList) {
        this.systemParamList = systemParamList;
    }
}
