package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="会议提醒配置实体类", description="会议提醒配置实体类")
public class MsgCfgDto {

    @ApiModelProperty(value="会议类型编码",name="mrtypecode")
    private String mrtypecode;

    @ApiModelProperty(value="会议类型名称",name="mrtypename")
    private String mrtypename;

    @ApiModelProperty(value="提醒周期（月、季度、年）",name="mode")
    private Integer mode;

    @ApiModelProperty(value="具体的日期（多个逗号隔开）",name="days")
    private String days;

    public String getMrtypecode() {
        return mrtypecode;
    }

    public void setMrtypecode(String mrtypecode) {
        this.mrtypecode = mrtypecode;
    }

    public String getMrtypename() {
        return mrtypename;
    }

    public void setMrtypename(String mrtypename) {
        this.mrtypename = mrtypename;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }
}
