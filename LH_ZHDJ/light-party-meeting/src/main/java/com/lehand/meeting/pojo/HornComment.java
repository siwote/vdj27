package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="评论", description="评论")
public class HornComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="评论id", name="id")
    private Long id;

    @ApiModelProperty(value="模块编码见sys_module", name="mdcode")
    private String mdcode;

    @ApiModelProperty(value="模块ID", name="moduleid")
    private Long moduleid;

    @ApiModelProperty(value="评论人ID", name="userid")
    private Long userid;

    @ApiModelProperty(value="评论人名称", name="username")
    private String username;

    @ApiModelProperty(value="评论时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="评论更新时间", name="updatetime")
    private String updatetime;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    @ApiModelProperty(value="评论内容", name="context")
    private String context;

    @ApiModelProperty(value="组织ID", name="orgid")
    private Long orgid;

    @ApiModelProperty(value="租户ID", name="compid")
    private Long compid;

    @ApiModelProperty(value="评论主体类型(0:用户,1:机构/组/部门,2:角色)备注1", name="sjtype")
    private Integer sjtype;

    @ApiModelProperty(value="评论类型(0:文本,1:语音)", name="repflag")
    private Integer repflag;

    @ApiModelProperty(value="组织编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="组织名称", name="orgname")
    private String orgname;

    @ApiModelProperty(value="状态", name="status")
    private Integer status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMdcode() {
        return mdcode;
    }

    public void setMdcode(String mdcode) {
        this.mdcode = mdcode;
    }

    public Long getModuleid() {
        return moduleid;
    }

    public void setModuleid(Long moduleid) {
        this.moduleid = moduleid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Integer getSjtype() {
        return sjtype;
    }

    public void setSjtype(Integer sjtype) {
        this.sjtype = sjtype;
    }

    public Integer getRepflag() {
        return repflag;
    }

    public void setRepflag(Integer repflag) {
        this.repflag = repflag;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}