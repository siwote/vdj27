package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingRecordSubject;
import com.lehand.meeting.pojo.MeetingTalkRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

/**
 * 谈心谈话记录
 * @author code maker
 */
@ApiModel(value="谈心谈话记录",description="谈心谈话记录")
public class MeetingTalkRecordDto extends MeetingTalkRecord {
	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value="会议参加人", name="meetingRecordSubjects")
	private List<MeetingRecordSubject> meetingRecordSubjects;

	@ApiModelProperty(value="结束日期", name="endDate")
	private String endDate;

	@ApiModelProperty(value="开始日期", name="startDate")
	private String startDate;

    @ApiModelProperty(value="会议附件", name="meetingAttachList")
    private List<Map<String,Object>> meetingAttachList;

	public List<MeetingRecordSubject> getMeetingRecordSubjects() {
		return meetingRecordSubjects;
	}

	public void setMeetingRecordSubjects(List<MeetingRecordSubject> meetingRecordSubjects) {
		this.meetingRecordSubjects = meetingRecordSubjects;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

    public List<Map<String, Object>> getMeetingAttachList() {
        return meetingAttachList;
    }

    public void setMeetingAttachList(List<Map<String, Object>> meetingAttachList) {
        this.meetingAttachList = meetingAttachList;
    }
}
