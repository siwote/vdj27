package com.lehand.meeting.service.jobservice;

import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.DateStageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class AddMeetingTimeService {
    //记录日志
	private static final Logger logger = LogManager.getLogger(AddMeetingTimeService.class);
	//是否正在创建任务
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);
	//线程池
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	@Transactional(rollbackFor=Exception.class)
	public void addMeetingTime() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			List<Map<String,Object>> lists = generalSqlComponent.query(MeetingSqlCode.listSysParamMeetTime,new Object[]{});
			logger.info("定时添加会议时间开始...");
			for (final Map<String,Object> map : lists) {
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						try {
							List<String> dates = DateStageEnum.parse(map.get("paramvalue").toString(),Integer.valueOf(map.get("remarks2").toString()));
							addMeetingTodoTime(map,dates);
						} catch (Exception e) {
							logger.error("添加会议时间异常",e);
						}
					}
				});
			}
			logger.info("定时扫描会议时间结束...");
		} catch (Exception e) {
			logger.error("定时扫描会议时间异常",e);
		} finally {
			CREATE.set(false);
		}
	}

	/**
	 * 添加会议提醒时间
	 * @param map
	 * @param dates
	 */
	private void addMeetingTodoTime(Map<String, Object> map, List<String> dates) throws ParseException {
		String code = map.get("paramkey").toString();
		String name = map.get("remarks3").toString();
		Long compid =1L;
		Map<String,Object> params = new HashMap<>();
		params.put("compid",compid);
		params.put("code",code);
		params.put("name",name);
		for (String date : dates) {
			String publishtime = date + " 00:00:00";
			params.put("publishtime",publishtime);
			Map<String,Object> map1 = generalSqlComponent.query(MeetingSqlCode.getMeetingTodoTime,new Object[]{compid,code,publishtime});
			//判断时间是否已经过了，如果过了就不插入
			if(map1==null && DateEnum.now().before(DateEnum.YYYYMMDDHHMMDD.parse(publishtime))){
				generalSqlComponent.insert(MeetingSqlCode.addMeetingTodoTime,params);
			}
		}
	}

	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
