package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="会议待办", description="会议待办记录实体类")
public class MeetingToDoRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户id", name="compid")
    private Long compid;

    @ApiModelProperty(value="自增主键", name="id")
    private Long id;

    @ApiModelProperty(value="业务所属类型", name="busitype")
    private Integer busitype;

    @ApiModelProperty(value="业务id", name="businessid")
    private Long businessid;

    @ApiModelProperty(value="业务类型编码", name="mcode")
    private String mcode;

    @ApiModelProperty(value="业务类型名称", name="mcodename")
    private String mcodename;

    @ApiModelProperty(value="待办标题", name="title")
    private String title;

    @ApiModelProperty(value="待办详情", name="content")
    private String content;

    @ApiModelProperty(value="自定义数据", name="customdata")
    private String customdata;

    @ApiModelProperty(value="待办类型:0 参会;1创建会议", name="ttype")
    private Integer ttype;

    @ApiModelProperty(value="状态:0未处理,1已处理", name="status")
    private Integer status;

    @ApiModelProperty(value="接收人id", name="recivuserid")
    private Long recivuserid;

    @ApiModelProperty(value="接收人姓名", name="recivusername")
    private String recivusername;

    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="发送人id", name="senderid")
    private Long senderid;

    @ApiModelProperty(value="发送人姓名", name="sendername")
    private String sendername;

    @ApiModelProperty(value="修改时间", name="updatetime")
    private String updatetime;

    @ApiModelProperty(value="备注1", name="remarks1")
    private Integer remarks1;

    @ApiModelProperty(value="备注2", name="remarks2")
    private Integer remarks2;

    @ApiModelProperty(value="备注3", name="remarks3")
    private String remarks3;

    @ApiModelProperty(value="备注4", name="remarks4")
    private String remarks4;


    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBusitype() {
        return busitype;
    }

    public void setBusitype(Integer busitype) {
        this.busitype = busitype;
    }

    public Long getBusinessid() {
        return businessid;
    }

    public void setBusinessid(Long businessid) {
        this.businessid = businessid;
    }

    public String getMcode() {
        return mcode;
    }

    public void setMcode(String mcode) {
        this.mcode = mcode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCustomdata() {
        return customdata;
    }

    public void setCustomdata(String customdata) {
        this.customdata = customdata;
    }

    public Integer getTtype() {
        return ttype;
    }

    public void setTtype(Integer ttype) {
        this.ttype = ttype;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getRecivuserid() {
        return recivuserid;
    }

    public void setRecivuserid(Long recivuserid) {
        this.recivuserid = recivuserid;
    }

    public String getRecivusername() {
        return recivusername;
    }

    public void setRecivusername(String recivusername) {
        this.recivusername = recivusername;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Long getSenderid() {
        return senderid;
    }

    public void setSenderid(Long senderid) {
        this.senderid = senderid;
    }

    public String getSendername() {
        return sendername;
    }

    public void setSendername(String sendername) {
        this.sendername = sendername;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    public Integer getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    public String getRemarks4() {
        return remarks4;
    }

    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    public String getMcodename() {
        return mcodename;
    }

    public void setMcodename(String mcodename) {
        this.mcodename = mcodename;
    }
}