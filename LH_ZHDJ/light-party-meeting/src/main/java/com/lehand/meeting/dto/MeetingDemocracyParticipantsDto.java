package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingDemocracyParticipants;
import com.lehand.meeting.pojo.MeetingRecordDemocracy;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;


/**
 * @ClassName: MeetingDemocracyParticipantsDto
 * @Description: 民主生活会会议记录表扩展类
 * @Author lx
 * @DateTime 2021-04-29 15:24
 */
@ApiModel(value = "民主生活会会议参会人扩展类", description = "民主生活会会议参会人扩展类")
public class MeetingDemocracyParticipantsDto extends MeetingDemocracyParticipants {
    @ApiModelProperty(value = "名称", name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
