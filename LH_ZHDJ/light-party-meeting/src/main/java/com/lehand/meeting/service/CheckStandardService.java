package com.lehand.meeting.service;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.meeting.constant.common.SysConstants.MSTANDARD;
import com.lehand.meeting.constant.common.SysConstants.MSTATUS;
import com.lehand.meeting.constant.common.SysConstants.MTYPE;
import com.lehand.meeting.constant.common.SysConstants.mrequirementtype;
import com.lehand.meeting.dto.MPatrolPagerReq;
import com.lehand.meeting.dto.MeetingTypeDto;
import com.lehand.meeting.pojo.MeetingRecord;
import com.lehand.meeting.utils.NumberUtils;
import com.lehand.meeting.utils.OrgUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 计算达标标准的服务类
 */
@Service
public class CheckStandardService {

    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    OrgUtils orgUtils;

    /**
     *  根据组织机构编码获取未召开月份
     *
     * @param year
     * @param organCode
     * @param map
     * @param session
     * @return
     */
    public String getUnConvenedMonByOrg(int year, String organCode, Map<String,Object> map, Session session) {

        List<Map<String, Object>> mconvednum = getMeetingMonthList(year, organCode, map, session);
        //会议召开频率
       Object standvalueObject=map.get("remarks2");
        int standvalue=0;
       if(standvalueObject!=null){
           standvalue = (int) map.get("remarks2");
       }
        Calendar cal = Calendar.getInstance();
        int currYear = cal.get(Calendar.YEAR);
        int currMonth = year< currYear?12:cal.get(Calendar.MONTH) + 1;
        //会议召开频率编码
        String code = String.valueOf(map.get("remarks3"));       
        //未达标时间段(月,季度,年,半年,年)查询处理
        return makeStr(year,code,currMonth,standvalue,mconvednum);
    }

    private List<Map<String, Object>> getMeetingMonthList(int year, String organCode, Map<String, Object> map, Session session) {
        //判断会议类型是否属于标签类会议
        Boolean flag = tagMeeting(String.valueOf(map.get("paramkey")));
        String sql = "";
        if(flag) {
            sql = MeetingSqlCode.getConvedTagNumByMonth;
        }else {
            sql = MeetingSqlCode.getConvedNumByMonth;
        }
        List<Map<String,Object>> mconvednum = generalSqlComponent.query(sql,
                new Object[]{year, organCode, map.get("paramkey"), session.getCompid()});
        return mconvednum;
    }

    /**
     * 未达标时间段(月,季度,年,半年,年)查询处理
     * @param year 
     * @param code
     * @param currMonth
     * @param standvalue
     * @param mconvednum
     * @return
     */
    private String makeStr(int year, String code, int currMonth, int standvalue, List<Map<String, Object>> mconvednum) {
    	String Str = MagicConstant.STR.EMPTY_STR;
        switch (code) {
 			case mrequirementtype.permon:
 				if(mconvednum.size()<currMonth*standvalue){
 					Str = getMonthUnconvened(currMonth,mconvednum,standvalue);
 	            }
 				break;
 			case mrequirementtype.perseason:  
 				int currQuarter = getCurrentQuarter(currMonth);
                Str = getQuathUnconvened(currQuarter,mconvednum,standvalue);
// 				if(mconvednum.size()<currQuarter*standvalue){
//
// 	            }
 				break;
 			case mrequirementtype.perhalfyear:
 				int currHalfyear = getCurrHalfyear(currMonth);
                Str = getHalfYearUnconvened(currHalfyear,mconvednum,standvalue);
// 				if(mconvednum.size()<currHalfyear*standvalue){
//
// 	            }
 				break;
 			case mrequirementtype.perfyear:
 				if(mconvednum.size()<standvalue) {
 					Str = year+"年召开";
 				}
 				break;
 			default:
 				break;
 		}
         return Str;
	}

	/**
	 * 	按半年统计未达标的半年
	 * @param currHalfyear
	 * @param mconvednum
	 * @param standvalue
	 * @return
	 */
	private String getHalfYearUnconvened(int currHalfyear, List<Map<String, Object>> mconvednum, int standvalue) {
		//半年集合
        Integer[] culist = new Integer[currHalfyear];
        for(int i = 1;i<=currHalfyear;i++){
            culist[i-1] = i;
        }

        //达标半年集合
        Integer[] reallist = makeConvendHalfyearlist(currHalfyear,mconvednum,standvalue);
        List<Integer> unconvened = compare(reallist,culist);       
		return makeReturnInfo(unconvened,"半年");
	}

	/**
	 * 记录达标的半年
	 * @param currHalfyear
	 * @param mconvednum
	 * @param standvalue
	 * @return
	 */
	private Integer[] makeConvendHalfyearlist(int currHalfyear, List<Map<String, Object>> mconvednum, int standvalue) {
		  List<Integer> halfyear = new ArrayList<>();
	        if(mconvednum.size()<1){
	            return mconvednum.toArray(new Integer[0]);
	        }

	        int firsthalf = 0;
	        int secondhalf = 0;

	        for (Map<String, Object> map:mconvednum) {
	            int month = Integer.parseInt(String.valueOf(map.get("mrmonth")));
	            int num = Integer.parseInt(String.valueOf(map.get("num")));
	            if (month >6){
	            	secondhalf +=num;
	            }else {
	            	firsthalf = num;
	            }
	        }

	        if(firsthalf>=standvalue){halfyear.add(1);}
	        if(secondhalf>=standvalue){halfyear.add(2);}
	        return halfyear.toArray(new Integer[halfyear.size()]);
	}

	/**
     * 按季度统计未达标的季度
     * @param currQuarter
     * @param mconvednum
     * @param standvalue
     * @return
     */
    private String getQuathUnconvened(int currQuarter, List<Map<String, Object>> mconvednum, int standvalue) {

        //季度集合
        Integer[] culist = new Integer[currQuarter];
        for(int i = 1;i<=currQuarter;i++){
            culist[i-1] = i;
        }

        //达标季度集合
        Integer[] reallist = makeConvendQuarterlist(currQuarter,mconvednum,standvalue);
//        if(mconvednum.size()<1){
//            List<Integer> unconvened = compare(reallist,culist);
//            return makeReturnInfo(unconvened,"季度");
//        }
        List<Integer> unconvened = compare(reallist,culist);
        return makeReturnInfo(unconvened,"季度");
    }

    /**
     * 记录达标的季度
     * @param mconvednum
     * @param standvalue
     * @return
     */
    private Integer[] makeConvendQuarterlist(int currQuarter,List<Map<String, Object>> mconvednum, int standvalue) {

        List<Integer> quarter = new ArrayList<>();
        if(mconvednum.size()<1){
            return mconvednum.toArray(new Integer[0]);
        }

        int quaone = 0;
        int quatwo = 0;
        int quathree = 0;
        int quafour = 0;

        for (Map<String, Object> map:mconvednum) {
            int month = Integer.parseInt(String.valueOf(map.get("mrmonth")));
            int num = Integer.parseInt(String.valueOf(map.get("num")));
            if (month >= 1 && month <= 3){
                quaone +=num;
            }else if(month>=4&&month<=6){
                quatwo +=num;
            }else if(month>=7&&month<=9){
                quathree +=num;
            }else if(month>=10&&month<=12){
                quafour +=num;
            }
        }
        if(quaone>=standvalue){quarter.add(1);}
        if(quatwo>=standvalue){quarter.add(2);}
        if(quathree>=standvalue){quarter.add(3);}
        if(quafour>=standvalue){quarter.add(4);}
        return quarter.toArray(new Integer[quarter.size()]);
    }

    /**
     * 按月统计未达标月份
     * @param currMonth
     * @param mconvednum
     * @param standvalue
     * @return
     */
    private String getMonthUnconvened(int currMonth, List<Map<String, Object>> mconvednum, int standvalue) {

        //月份集合
        Integer[] culist = new Integer[currMonth];
        for(int i = 1;i<=currMonth;i++){
            culist[i-1] = i;
        }

        //达标月份集合
        Integer[] reallist = new Integer[mconvednum.size()];

        if(mconvednum.size()<1){
            List<Integer> unconvened = compare(reallist,culist);
            return makeReturnInfo(unconvened,"月");
        }
        int j = 0;
        for(Map<String, Object> map:mconvednum){            
            int num = Integer.parseInt(String.valueOf(map.get("num")));
            int mrmonth = Integer.parseInt(String.valueOf(map.get("mrmonth")));
            if(num>=standvalue){
                reallist[j]= mrmonth;
            }
            j++;
        }

        List<Integer> unconvened = compare(reallist,culist);

        return makeReturnInfo(unconvened,"月");
    }

    /**
     * 拼接返回数据
     * @param unconvened
     * @return
     */
    private String makeReturnInfo(List<Integer> unconvened,String s) {

        if(unconvened.size()<1){
            return MagicConstant.STR.EMPTY_STR;
        }

        StringBuffer buffer= new StringBuffer();
        for (Integer month:unconvened) {
            if("半年".equals(s)){
                if(month == 1){
                    buffer.append("上");
                }
                if(month == 2){
                    buffer.append("下");
                }
            }else{
                buffer.append(month);
            }
            buffer.append(s);
            buffer.append(",");
        }
        String str = buffer.toString();
        return str.substring(0,str.length()-1);
    }

    //取两数组不同值
    public static <T> List<T> compare(T[] t1, T[] t2) {
        List<T> list1 = Arrays.asList(t1);
        List<T> list2 = new ArrayList<T>();
        for (T t : t2) {
            if (!list1.contains(t)) {
                list2.add(t);
            }
        }
        return list2;
    }

    /**
     * 查询当前季度
     * @param currMonth
     * @return
     */
    public int getCurrentQuarter(int currMonth) {
        if (currMonth >= 1 && currMonth == 3){
            return 1;
        }else if(currMonth>=4&&currMonth<=6){
            return 2;
        }else if(currMonth>=7&&currMonth<=9){
            return 3;
        }else if(currMonth>=10&&currMonth<=12){
            return 4;
        }
        return 1;
    }


    /**
     * 根据组织机构编码获取会议达标状态
     *
     * @param year
     * @param organCode
     * @param map
     * @param session
     * @return
     */
    public Integer getStandardStatusByOrg(int year, String organCode, Map<String, Object> map, Session session) {

        //查询当前党组织是否有党小组
        Boolean flag = booleanHasPartygroup(organCode,session.getCompid());
        if(!flag&&MTYPE.PARTY_GROUP_MEETING.equals(map.get("paramkey"))){ return MSTANDARD.NO_REQUIREMENT;}

        //达标预期数
        int expectedvalue = getExpectedvalue(year,map);
        //已召开数
//        int convened = getConvenedByOrg(year, organCode,map,session);

        List<Map<String, Object>> mconvednum = getMeetingMonthList(year, organCode, map, session);
        //会议召开频率编码
        String code = String.valueOf(map.get("remarks3"));
        int convened = getConvened(year,code,mconvednum);
        List<Map<String, Object>> meetingMonthList = getMeetingMonthList(year,organCode, map,session);
        System.out.println(expectedvalue+"***********************************************"+convened);
        return expectedvalue>convened? MSTANDARD.UNREACHED: MSTANDARD.REACHED;
    }

    public Integer getConvened(int year, String code, List<Map<String, Object>> mconvednum) {
        Integer num = NumberUtils.num0;
        switch (code) {
            case mrequirementtype.permon:
                num =  mconvednum.size();
                break;
            case mrequirementtype.perseason:
                Integer months = 0;
                Integer num1 = NumberUtils.num0;
                Integer num2 = NumberUtils.num0;
                Integer num3 = NumberUtils.num0;
                Integer num4 = NumberUtils.num0;
                for(Map<String, Object> li:mconvednum){
                    months = Integer.valueOf(li.get("mrmonth").toString());
                    if(months>=NumberUtils.num1&&months<=NumberUtils.num3){
                        num1 = NumberUtils.num1;
                    }
                    if(months>=NumberUtils.num4&&months<=NumberUtils.num6){
                        num2 = NumberUtils.num1;
                    }
                    if(months>=NumberUtils.num7&&months<=NumberUtils.num9){
                        num3 = NumberUtils.num1;
                    }
                    if(months>=NumberUtils.num10&&months<=NumberUtils.num12){
                        num4 = NumberUtils.num1;
                    }
                }
                num = num1+num2+num3+num4;
                break;
            case mrequirementtype.perhalfyear:
                Integer months2 = 0;
                Integer yearnum1 = NumberUtils.num0;
                Integer yearnum2 = NumberUtils.num0;
                for(Map<String, Object> li:mconvednum){
                    months2 = Integer.valueOf(li.get("mrmonth").toString());
                    if(months2>=NumberUtils.num1&&months2<=NumberUtils.num6){
                        yearnum1 = NumberUtils.num1;
                    }
                    if(months2>=NumberUtils.num7&&months2<=NumberUtils.num12){
                        yearnum2 = NumberUtils.num1;
                    }
                }
                num = yearnum1+yearnum2;
                break;
            case mrequirementtype.perfyear:
                for(Map<String, Object> li:mconvednum){
                    num+=Integer.valueOf(li.get("num").toString());
                }
                break;
            default:
                break;
        }
        return num;
    }

    /**
     * 是否有党小组
     * @param organCode
     * @param compid
     * @return
     */
    private Boolean booleanHasPartygroup(String organCode, Long compid) {

        Long count = generalSqlComponent.query(MeetingSqlCode.countPartGroupByOrgcode,new Object[]{organCode,compid});
        if(count>0){
            return true;
        }
        return false;
    }

    /**
     * 计算单表预期数
     * @param year
     * @param map
     * @return
     */
    public int getExpectedvalue(int year,Map<String, Object> map) {

        //达标计算类型
        Object standtype = map.get("remarks3");

        Calendar cal = Calendar.getInstance();
        int currMonth = cal.get(Calendar.MONTH) + 1;
        int currYear = cal.get(Calendar.YEAR);

        //之前年份
        if(year< currYear){
        	return prevYearExpectedvalue(standtype);
        }

        //达标计算基数
        Object standvalueObject=map.get("remarks2");
        int standvalue=0;
        if(standvalueObject!=null){
            standvalue = (int) map.get("remarks2");
        }
        //每月
        if(mrequirementtype.permon.equals(standtype)){
            return standvalue*currMonth;
        }
        //每季度
        if(mrequirementtype.perseason.equals(standtype)){
        	int currQuarter = getCurrentQuarter(currMonth);
        	return standvalue*currQuarter;
        }
        //每半年
        if(mrequirementtype.perhalfyear.equals(standtype)){
        	int currHalfyear = getCurrHalfyear(currMonth);
        	return standvalue*currHalfyear;
        }
        return standvalue;
    }

    /**
     * 判断半年 1:上半年,2;下半年
     * @param currMonth
     * @return
     */
    public int getCurrHalfyear(int currMonth) {
    	if (currMonth > 6){
             return 2;
        }
		return 1;
	}

	/**
     * 当前年份之前的达标要求
     * @param standtype
     * @return
     */
    private int prevYearExpectedvalue(Object standtype) {
		switch (String.valueOf(standtype)) {
		case mrequirementtype.permon:
			return 12;
		case mrequirementtype.perseason:
			return 4;
		case mrequirementtype.perhalfyear:
			return 2;
		case mrequirementtype.perfyear:
			return 1;
		default:
			break;
		}
		return 0;
	}

	/**
     * 根据组织机构编码查询已召开的会议数
     *
     * @param year
     * @param organCode
     * @param map
     * @param session
     * @return
     */
    public Integer getConvenedByOrg(int year, String organCode, Map<String, Object> map, Session session) {
    	Boolean flag = tagMeeting(String.valueOf(map.get("paramkey")));
    	if(flag) {

            return generalSqlComponent.query(MeetingSqlCode.countTagMeetingTypeByOrgcode,
                    new Object[]{year,organCode,map.get("paramkey"), MSTATUS.submitted,session.getCompid()});
    	}
        return generalSqlComponent.query(MeetingSqlCode.countMeetingTypeByOrgcode,
                new Object[]{year,organCode,map.get("paramkey"), MSTATUS.submitted,session.getCompid()});
    }
    public Integer getConvenedByOrg2(String organCode, Map<String, Object> map, Session session,Map<String, Object> parms) {
        Boolean flag = tagMeeting(String.valueOf(map.get("paramkey")));
        parms.put("orgcode",organCode);
        parms.put("paramkey",map.get("paramkey"));
        parms.put("status",MSTATUS.submitted);
        parms.put("compid",session.getCompid());
        parms.put("startYear",parms.get("startYear"));
        parms.put("endYear",parms.get("endYear"));
        parms.put("startMonth",parms.get("startMonth"));
        parms.put("endMonth",parms.get("endMonth"));
        if(flag) {

            return generalSqlComponent.query(MeetingSqlCode.countTagMeetingTypeByOrgcode2, parms);
        }
        return generalSqlComponent.query(MeetingSqlCode.countMeetingTypeByOrgcode2,parms);
    }

    public Integer getConvenedByOrgPid(String organCode, Map<String, Object> map, Session session,Map<String, Object> parms) {
        Boolean flag = tagMeeting(String.valueOf(map.get("paramkey")));
        parms.put("orgcode",organCode);
        parms.put("paramkey",map.get("paramkey"));
        parms.put("status",MSTATUS.submitted);
        parms.put("compid",session.getCompid());
        parms.put("startYear",parms.get("startYear"));
        parms.put("endYear",parms.get("endYear"));
        parms.put("startMonth",parms.get("startMonth"));
        parms.put("endMonth",parms.get("endMonth"));
        if(flag) {

            return generalSqlComponent.query(MeetingSqlCode.countTagMeetingTypeByOrgPid, parms);
        }
        return generalSqlComponent.query(MeetingSqlCode.countMeetingTypeByOrgcodePid,parms);
    }
    
    
    /**
     * 判断会议类型是否属于标签类会议
     * @param mtype
     * @return
     */
	public Boolean tagMeeting(String mtype) {
		List<String> tagstypecode = getTagsMtypeList();
		if(tagstypecode.contains(mtype)) {
			return true;
		}
		return false;
	}

	/**
	 * 为标签类会议的会议编码集合
	 * @return
	 */
	private List<String> getTagsMtypeList() {
		List<String> list = new ArrayList<String>();
		//组织生活会
		list.add("org_life_meeting");
		//民主生活会
//		list.add("democratic_life_meeting");
		//民主评议党员
		list.add("appraisal_of_members");
//		//主题党日
//		list.add("theme_party_day");
        //党委中心组学习
        list.add("theoretical_study");
		return list;
	}


    /**
     * 计算巡查达标状态
     * @param req
     * @param headlist
     * @return
     */
    public Map<String,Object> doCalculate(MPatrolPagerReq req, List<MeetingTypeDto> headlist) {


        Map<String,Object> result = new LinkedHashMap<String,Object>();
        BooleanTImeParam(req);
        //是否标签类会议
        boolean flag = tagMeeting(req.getMrtype());
        req.setTablename(flag?"view_tag_meeting":"view_meeting");
        //循环表头
        for (MeetingTypeDto head: headlist) {
            String standtype =    head.getCalmeth();
            int standvalue =   head.getCalvalue();
            int respectedValue = getRespectedValue(standtype,standvalue);
            req.setCalvalue(respectedValue);
            req.setMrtype(head.getCode());
            Map<String,Object> map = generalSqlComponent.query(MeetingSqlCode.getBranchMeetingCountAndSum, req);
            if(SysConstants.MTYPE.COMMITTEE_MEETING.equals(req.getMrtype())){
                map= generalSqlComponent.query(MeetingSqlCode.getCommitteeMeetingCountAndSum, req);
            }
            map.put("code",head.getCode());
            result.put(head.getProp(), map);
        }
        return result;
    }

    /**
     * 处理巡查时间参数,默认年初年尾
     * @param req
     */
    public void BooleanTImeParam(MPatrolPagerReq req) {
        if(org.springframework.util.StringUtils.isEmpty(req.getStarttime())){
            Calendar cale = Calendar.getInstance();
            int year = cale.get(Calendar.YEAR);
            String starttime = year +"-01-00";
            req.setStarttime(starttime);
        }
        if(org.springframework.util.StringUtils.isEmpty(req.getEndtime())) {
            Calendar cale = Calendar.getInstance();
            int year = cale.get(Calendar.YEAR);
            String endTime = year + "-12-32";
            req.setEndtime(endTime);
        }
    }

    /**
     * 巡查获取期望达标值
     * @param standtype
     * @param standvalue
     * @return
     */
    public int getRespectedValue(String standtype, int standvalue) {

        Calendar cal = Calendar.getInstance();
        int currMonth = cal.get(Calendar.MONTH) + 1;
        int currYear = cal.get(Calendar.YEAR);

        //每月
        if(mrequirementtype.permon.equals(standtype)){
            return standvalue*currMonth;
        }
        //每季度
        if(mrequirementtype.perseason.equals(standtype)){
            int currQuarter = getCurrentQuarter(currMonth);
            return standvalue*currQuarter;
        }
        //每半年
        if(mrequirementtype.perhalfyear.equals(standtype)){
            int currHalfyear = getCurrHalfyear(currMonth);
            return standvalue*currHalfyear;
        }
        return standvalue;
    }


    /**
     * 查询逾期时间段
     * @return
     */
    public  List<Integer> getOverduePeriod(Map<String,Object> mrtype,String orgcode,int year){
        return null;
    }

    /**
     * 查询逾期召开次数
     * @return
     */
    public  Integer getOverdueNum(Map<String,Object> mrtype,String orgcode,int year){
        //会议类型CODE
        String paramkey = mrtype.get("paramkey").toString();
        //召开频率
        Integer remarks2 = Integer.valueOf(mrtype.get("remarks2").toString());
        int overdue = 0;
        //根据会议类型以及机构CODE查询在召开周期内是否存在逾期召开情况
        String month = DateEnum.YYYYMMDD.format().substring(5,7);
        switch (mrtype.get("remarks3").toString()) {
            case mrequirementtype.perfyear:
                //每年召开 民主评议党员 民主生活会
                //根据年份查询会议记录表meeting_record，正常召开次数，如果次数大于remarks2则返回0
                overdue = 0; //perYear(paramkey,orgcode, year, remarks2);
                break;
            case mrequirementtype.perhalfyear:
                //每半年召开 组织生活会
                overdue = perHalfYear(paramkey,orgcode, year, remarks2, Integer.valueOf(month) <= 6);
                break;
            case mrequirementtype.perseason:
                //每季度召开 支部党员大会 党课
                overdue = perQuarter(paramkey,orgcode, year, remarks2, month);
                break;
            default:
                //每月召开 支部委员会 党小组会 主题党日活动 党委中心组学习
                overdue = perMonth(paramkey,orgcode, year, remarks2, month);
                break;
        }
        return overdue;
    }

    private int perMonth(String paramkey,String orgcode, int year, Integer remarks2, String month) {
        int overdue = 0;int num;int num1;
        List<String> lists2 = new ArrayList<>();
        String sql = "";String sql2 = "";String sql3 = "";
        if(tagMeeting(paramkey)){
            sql = MeetingSqlCode.queryMeetingRecordNum8;
            sql2 = MeetingSqlCode.queryMeetingRecordNum;
            sql3 = MeetingSqlCode.queryMeetingRecordNum2;
        }else{
            sql = MeetingSqlCode.queryMeetingRecordNum7;
            sql2 = MeetingSqlCode.queryMeetingRecordNum3;
            sql3 = MeetingSqlCode.queryMeetingRecordNum4;
        }
        List<MeetingRecord> lists = generalSqlComponent.query(sql, new Object[]{orgcode,paramkey,year});
        for (MeetingRecord list : lists) {
            lists2.add(list.getStarttime().substring(5,7));
        }
        for (String s : lists2) {
            num = generalSqlComponent.query(sql3, new Object[]{orgcode, year + "-" + s, year + "-" + s,paramkey});
            num1 = generalSqlComponent.query(sql2, new Object[]{orgcode, year + "-" + s, year + "-" + month,paramkey});
            overdue += num >= remarks2 ? 0 : (remarks2-num >= num1 ? num1 : remarks2-num);
        }
        return overdue;
    }

    private int perQuarter(String paramkey,String orgcode, int year, Integer remarks2, String month) {
        int overdue = 0;
        int m = getCurrentQuarter(Integer.valueOf(month));
        String sql = "";
        if(tagMeeting(paramkey)){
            sql = MeetingSqlCode.queryMeetingRecordNum9;
        }else{
            sql = MeetingSqlCode.queryMeetingRecordNum6;
        }
        if(m==1){//第一季度
            overdue = 0;
        }else if(m==2){//第二季度
            overdue = getTwoOverdue(paramkey, orgcode, year, remarks2, sql);
        }else if(m==3){//第三季度
            overdue = getThreeOverdue(paramkey, orgcode, year, remarks2, overdue, sql);
        }else {//第四季度
            overdue = getFourOverdue(paramkey, orgcode, year, remarks2, overdue, sql);
        }
        return overdue;
    }

    private int getTwoOverdue(String paramkey, String orgcode, int year, Integer remarks2, String sql) {
        int overdue;//第一季度实际创建的会议数量
        int num = getQuery(paramkey, orgcode, year, sql, "-01", "-03", "-01", "-03");
        //第二季度补录第一季度的数量
        int num1 = getQuery(paramkey, orgcode, year, sql, "-01", "-03", "-04", "-06");
        overdue = num >= remarks2 ? 0 : (remarks2-num >= num1 ? num1 : remarks2-num);
        return overdue;
    }

    private int getFourOverdue(String paramkey, String orgcode, int year, Integer remarks2, int overdue, String sql) {
        //第一季度实际创建的会议数量
        int num1 = getQuery(paramkey, orgcode, year, sql, "-01", "-03", "-01", "-03");
        //第二季度实际创建的会议数量
        int num2 = getQuery(paramkey, orgcode, year, sql, "-04", "-06", "-01", "-06");
        //第三季度实际创建的会议数量
        int num3 = getQuery(paramkey, orgcode, year, sql, "-07", "-09", "-01", "-09");
        //第四季度补录第一季度的数量
        int num4 = getQuery(paramkey, orgcode, year, sql, "-01", "-03", "-10", "-12");
        //第四季度补录第二季度的数量
        int num5 = getQuery(paramkey, orgcode, year, sql, "-04", "-06", "-10", "-12");
        //第四季度补录第三季度的数量
        int num6 = getQuery(paramkey, orgcode, year, sql, "-07", "-09", "-10", "-12");
        if(num6+num5+num4>0){
            int overdue1=0;int overdue2=0; int overdue3=0;
            if(num1<remarks2 && num4>0){
                overdue1 = remarks2-num1 >= num4 ? num4 : remarks2-num1;
            }
            if(num2<remarks2 && num5>0){
                overdue2 = remarks2-num2>= num5 ? num5 : remarks2-num2;
            }
            if(num3<remarks2 && num6>0){
                overdue3 = remarks2-num3>= num6 ? num6 : remarks2-num3;
            }
            overdue = overdue3+overdue2+overdue1;
        }
        return overdue;
    }

    private int getThreeOverdue(String paramkey, String orgcode, int year, Integer remarks2, int overdue, String sql) {
        //第一季度实际创建的会议数量
        int num1 = getQuery(paramkey, orgcode, year, sql, "-01", "-03", "-01", "-03");
        //第二季度实际创建的会议数量
        int num2 = getQuery(paramkey, orgcode, year, sql, "-04", "-06", "-01", "-06");
        //第三季度补录第一季度的数量
        int num3 = getQuery(paramkey, orgcode, year, sql, "-01", "-03", "-07", "-09");
        //第三季度补录第一季度的数量
        int num4 = getQuery(paramkey, orgcode, year, sql, "-04", "-06", "-07", "-09");
        if(num3+num4>0){
            int overdue1=0;
            int overdue2=0;
            if(num1<remarks2 && num3>0){
                overdue1 = remarks2-num1 >= num3 ? num3 : remarks2-num1;
            }
            if(num2<remarks2 && num4>0){
                overdue2 = remarks2-num2>= num4 ? num4 : remarks2-num2;
            }
            overdue = overdue2+overdue1;
        }
        return overdue;
    }

    private Integer getQuery(String paramkey, String orgcode, int year, String sql, String s, String s2, String s3, String s4) {
        return generalSqlComponent.query(sql, new Object[]{orgcode, year + s, year + s2, year + s3, year + s4, paramkey});
    }


    private int perHalfYear(String paramkey,String orgcode, int year, Integer remarks2, boolean b) {
        int overdue = 0;
        String sql = "";
        if(tagMeeting(paramkey)){
            sql = MeetingSqlCode.queryMeetingRecordNum5;
        }else{
            sql = MeetingSqlCode.queryMeetingRecordNum6;
        }
        if (!b) {//下半年
            //上半年召开的数量
            int num = getQuery(paramkey, orgcode, year, sql, "-01", "-06", "-01", "-06");
            //下半年补录上半年的数量
            int num1 = getQuery(paramkey, orgcode, year, sql, "-01", "-06", "-07", "-12");
            overdue = num >= remarks2 ? 0 : (remarks2-num >= num1 ? num1 : remarks2-num);
        }
        return overdue;
    }


    /**
     * 当前月召开次数
     * @param map
     * @param orgcode
     * @param year
     * @param month
     * @param session
     * @return
     */
    public int getCurrConvenedByOrg(Map<String, Object> map, String orgcode, int year, int month, Session session) {
        Boolean flag = tagMeeting(String.valueOf(map.get("paramkey")));
        if(flag) {

            return generalSqlComponent.query(MeetingSqlCode.countTagMeetingTypeByOrgcodeM,
                    new Object[]{year,month,orgcode,map.get("paramkey"), MSTATUS.submitted,session.getCompid()});
        }
        return generalSqlComponent.query(MeetingSqlCode.countMeetingTypeByOrgcodeM,
                new Object[]{year,month,orgcode,map.get("paramkey"), MSTATUS.submitted,session.getCompid()});
    }


    /**
     * 当前时间段召开次数
     * @param paramkey
     * @param orgcode
     * @param year
     * @param month
     * @param remarks3
     * @param session
     * @return
     */
    public int getQuanConvenedByOrg(Object paramkey, String orgcode, int year, int month, Object remarks3, Session session){
        if(paramkey==null||remarks3==null){
            return  0;
        }

        //时间段内月份集合
        List<Integer> list = getQuanMonthsByType(remarks3,month);

        String listMons = makeListToStr(list);

        //拼装查询参数
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("year",year);
        params.put("months",org.apache.commons.lang.StringUtils.strip(list.toString(),"[]"));
        params.put("status",MSTATUS.submitted);
        params.put("orgcode",orgcode);
        params.put("mrtype",paramkey);
        params.put("compid",session.getCompid());

        Boolean flag = tagMeeting(String.valueOf(paramkey));
        if(flag) {
            return generalSqlComponent.query(MeetingSqlCode.countTagMeetingTypeByOrgcodeML, params);
        }
        return generalSqlComponent.query(MeetingSqlCode.countMeetingTypeByOrgcodeML,params);
    }

    /**
     * 数组转,号分割字符串
     * @param list
     * @return
     */
    private String makeListToStr(List<Integer> list) {

        StringBuffer buffer = new StringBuffer();
        if(list.size()<1){
            return  MagicConstant.STR.EMPTY_STR;
        }

        for (int i = 0 ;i<list.size();i++) {
            buffer.append(list.get(i));
            if (i< list.size()-1){
                buffer.append(",");
            }
        }
        return buffer.toString();
    }

    /**
     * 根据计算类型获取时间段月份集合
     * @param standtype
     * @param month
     * @return
     */
    private List<Integer> getQuanMonthsByType(Object standtype, int month){
        switch (String.valueOf(standtype)) {
            case mrequirementtype.permon:
                return makeMonList(month);
            case mrequirementtype.perseason:
                return makeSeansonList(getCurrentQuarter(month));
            case mrequirementtype.perhalfyear:
                return makeHafyearList(getCurrHalfyear(month));
            case mrequirementtype.perfyear:
                return makeYearList(month);
            default:
                break;
        }
        return new ArrayList<Integer>();
    }

    /**
     * 半年月份数组
     * @param currHalfyear
     * @return
     */
    private List<Integer> makeHafyearList(int currHalfyear) {
        switch (currHalfyear) {
            case 1:
                return makeIntList(1,6);
            case 2:
                return makeIntList(7,12);
        }
        return new ArrayList<Integer>();
    }

    /**
     * 当前季度月份数组
     * @param currentQuarter
     * @return
     */
    private List<Integer> makeSeansonList(int currentQuarter) {
        switch (currentQuarter) {
            case 4:
                return makeIntList(10,12);
            case 3:
                return makeIntList(7,9);
            case 2:
                return makeIntList(4,6);
            case 1:
                return makeIntList(1,3);
        }
        return new ArrayList<Integer>();
    }

    /**
     * 处理数组
     * @param start
     * @param end
     * @return
     */
    private List<Integer> makeIntList(int start, int end) {

        List<Integer> list = new ArrayList<Integer>();

        if((end-start)<0){
            return list;
        }

        for(int i = start;i>=start&&i<=end; i++){
            list.add(i);
        }
        return list;
    }


    /**
     * 当前月月份数组
     * @param month
     * @return
     */
    private List<Integer> makeMonList(int month) {
        List<Integer> list = new ArrayList<Integer>();
        list.add(month);
        return list;
    }

    /**
     * 当前年月份数组
     * @param month
     * @return
     */
    private List<Integer> makeYearList(int month) {
        List<Integer> list = new ArrayList<Integer>();
        if(month<1){
            return list;
        }

        for(int i = 1;i<=month;i++){
            list.add(i);
        }

        return list;
    }

//    public String meetingStandardStatistics(){
//        Calendar cal = Calendar.getInstance();
//        List<TDzzInfoSimple> list  = generalSqlComponent.query(MeetingSqlCode.queryTdzzInfoSimple3,new Object[]{});
//        //查询所有会议类型
//        List<Map<String,Object>> listTypes = generalSqlComponent.query(MeetingSqlCode.getMeetingTypeList,new Object[]{});
//        //到当前月 月份数据
//        int m = cal.get(Calendar.MONTH) + 1;
//        List<Integer> monthlist = makeYearList(m);
//        //到当前季度 月份数组
//        //到当前年 月份数组
//        //遍历所有组织
//        for(TDzzInfoSimple li:list){
//            for(Map<String,Object> l:listTypes){
//
//            }
//
//        }
//        return null;
//
//    }
//
//    /**
//    * @Description 获取规定时间内会议召开次数
//    * @Author zwd
//    * @Date 2020/11/9 17:32
//    * @param
//    * @return
//    **/
//    public Integer meetingNum(String type,String meetingtype,Integer years,Integer startMonths,Integer endMonths){
//
//        return null;
//    }
}
