package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingGuideDto;
import com.lehand.meeting.dto.MeetingMsgCfgDto;
import com.lehand.meeting.service.MeetingCfgService;
import com.lehand.module.common.pojo.SystemParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(value = "会议配置服务", tags = { "会议配置服务" })
@RestController
@RequestMapping("/horn/meetingcfg")
public class MeetingCfgController extends BaseController {

    @Resource
    private MeetingCfgService meetingCfgService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "根据上级key获取子级内容集合", notes = "根据上级key获取子级内容集合", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "upparamkey", value = "上级Key", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingGuide")
    public Message<List<SystemParam>> getMeetingGuide(String upparamkey){
        Message<List<SystemParam>> msg = new Message<List<SystemParam>>();
        return msg.setData(meetingCfgService.getMeetingGuideList(upparamkey, getSession())).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "保存会议指南", notes = "保存会议指南", httpMethod = "POST")
    @RequestMapping("/configMeetingGuide")
    public Message<MeetingGuideDto> saveMeetingGuide(@RequestBody MeetingGuideDto meetingGuideDto){
        Message<MeetingGuideDto> msg = new Message<MeetingGuideDto>();
        return msg.setData(meetingCfgService.configMeetingGuide(meetingGuideDto, getSession())).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "会议自动提交配置项", notes = "会议自动提交配置项", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "保存是否开启定时提交 (0 开启 1 关闭)", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "hour", value = "保存设定的有效时间值（整数值 单位 h）", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/configAutoCommit")
    public Message<SystemParam> configAutoCommit(int status, int hour) {
        Message<SystemParam> msg = new Message<SystemParam>();
        return msg.setData(meetingCfgService.configAutoCommit(status, hour, getSession())).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "消息提醒配置", notes = "消息提醒配置", httpMethod = "POST")
    @RequestMapping("/addMeetingRemind")
    public Message<MeetingMsgCfgDto> addMeetingRemind(@RequestBody MeetingMsgCfgDto meetingMsgCfgDto){
        Message<MeetingMsgCfgDto> msg = new Message<MeetingMsgCfgDto>();
        meetingCfgService.addMeetingRemind(meetingMsgCfgDto, getSession());
        return msg.setData(null).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "根据paramkey获取数据", notes = "根据paramkey获取数据", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramkey", value = "Key", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingGuideByKey")
    public Message<SystemParam> getMeetingGuideByKey(String paramkey){
        Message<SystemParam> msg = new Message<SystemParam>();
        return msg.setData( meetingCfgService.getMeetingGuide( getSession(),paramkey)).success();
    }
}
