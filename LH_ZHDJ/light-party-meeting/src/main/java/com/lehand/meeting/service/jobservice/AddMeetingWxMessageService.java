package com.lehand.meeting.service.jobservice;


import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.MessageEnum;
import com.lehand.meeting.pojo.TkTaskRemindList;
import com.lehand.meeting.service.MeetingService;
import com.lehand.meeting.utils.NewsPush;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.tools.ant.launch.Locator.encodeURI;

public class AddMeetingWxMessageService {
    //记录日志
	private static final Logger logger = LogManager.getLogger(AddMeetingWxMessageService.class);
	//是否正在创建任务
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);
	//线程池
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	@Resource
	private MeetingService meetingService;

	@Value("${wx_service_url}")
	private String url;

	@Transactional(rollbackFor=Exception.class)
	public void addMeetingWxMessage() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			List<Map<String,Object>> lists = generalSqlComponent.query(MeetingSqlCode.listMeetingTodoTime,new Object[]{1, DateEnum.YYYYMMDDHHMMDD.format()});
			logger.info("定时创建会议待办开始...");
			for (final Map<String,Object> map : lists) {
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						try {
							//查询所有待办接收人的集合
							List<Map<String,Object>> lists = generalSqlComponent.query(MeetingSqlCode.listTDzzBzcyxx,new Object[]{});
							if(CollectionUtils.isEmpty(lists)){
								return;
							}
							String publishtime = map.get("publishtime").toString();
							Integer month = Integer.valueOf(publishtime.substring(publishtime.indexOf("-")+1,publishtime.lastIndexOf("-")));
							for (Map<String, Object> user : lists) {
								String content = "召开"+month+"月"+map.get("name");
								NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+"'"+user.get("zjhm")+"'"+"&content="+content+"&sendAcc=ydjadmin" )));
							}
						} catch (Exception e) {
							logger.error("创建会议待办异常",e);
						}
					}
				});
			}
			logger.info("定时创建会议待办结束...");
		} catch (Exception e) {
			logger.error("定时创建会议待办异常",e);
		} finally {
			CREATE.set(false);
		}
	}

	/**
	 * 生成待办事项
	 * @param map
	 * @param lists
	 */
	private void addMeetingToDoRecord(Map<String, Object> map, List<Map<String, Object>> lists) throws Exception {
		String publishtime = map.get("publishtime").toString();
		Integer month = Integer.valueOf(publishtime.substring(publishtime.indexOf("-")+1,publishtime.lastIndexOf("-")));
		Map<String, Object> param = getStringObjectMap(map, month);
		TkTaskRemindList param2 = getStringObjectMap2(map, month);
		String meetType = map.get("code").toString().substring(0, map.get("code").toString().lastIndexOf("_"));
		Session session = new Session();
		session.setCompid(1L);
		for (Map<String, Object> user : lists) {
			String content = "召开"+month+"月"+map.get("name");
			NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+"'"+user.get("idno")+"'"+"&content="+content+"&sendAcc=ydjadmin" )));
			session.setUserid(Long.valueOf(user.get("userid").toString()));
			param.put("recivuserid",user.get("userid"));
			param.put("recivusername",user.get("username"));
			//查询接收人的组织机构编码
			Map<String,Object> organ = generalSqlComponent.query(MeetingSqlCode.getUrcOrganization,new Object[]{1,user.get("userid")});
			if(organ==null){
				continue;
			}
			//插入待办 插入之前需要判断这个人是否已经合格的完成了待办事项
			// 达标完成了代办就直接跳过，后续这个接收人就不需要发送待办
			if(meetingService.checkMeetingIsConveneOnTime(meetType,session)){
				continue;
			}
			//先查询是否已经有代办
			Map upcoming = generalSqlComponent.query(MeetingSqlCode.getMeetingToDoRecord,
					new Object[]{1, meetType,DateEnum.YYYYMMDD.format(),user.get("userid")});
			//防止出现插入重复数据
			if(upcoming==null){
				//发送待办
				generalSqlComponent.insert(MeetingSqlCode.addMeetingToDoRecord,param);
				//发送消息
				param2.setUserid(Long.parseLong(user.get("userid").toString()));//被提醒人
				param2.setUsername(user.get("username").toString());
				generalSqlComponent.insert(MeetingSqlCode.addTTRLByTuihui, param2);
			}
		}
		//更新查询过的时间状态为创建完成
		modiMeetingTodoTimeStatus(1L,map.get("code").toString(),map.get("publishtime").toString());
	}

	/**
	 * 组装待办参数
	 * @param map
	 * @param month
	 * @return
	 */
	private Map<String, Object> getStringObjectMap(Map<String, Object> map, Integer month) {
		Map<String, Object> param = new HashMap<>();
		param.put("compid",1);
		param.put("busitype",0);
		param.put("businessid",-1L);
		param.put("mcode",map.get("code").toString().substring(0,map.get("code").toString().lastIndexOf("_")));
		param.put("mcodename",map.get("name"));
		param.put("title","召开"+month+"月"+map.get("name"));
		param.put("content","");
		param.put("customdata","");
		param.put("ttype",1);
		param.put("status",0);
		param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
		param.put("senderid",1);
		param.put("sendername","系统管理员");
		param.put("updatetime",0);
		param.put("remarks1",0);
		param.put("remarks2",0);
		param.put("remarks3","");
		param.put("remarks4","");
		return param;
	}

	/**
	 * 组装消息参数
	 * @param map
	 * @param month
	 * @return
	 */
	private TkTaskRemindList getStringObjectMap2(Map<String, Object> map, Integer month) {
		TkTaskRemindList param = new TkTaskRemindList();
		param.setCompid(1L);
		param.setNoteid(-1L);
		param.setModule(10);//模块(0:任务发布,1:任务反馈)
		param.setMid(-1L);//对应模块的ID
		param.setRemind(DateEnum.YYYYMMDDHHMMDD.format(new Date()));//提醒日期
		param.setRulecode(MessageEnum.MNOTICE_CREATE.getCode());//编码
		param.setRulename(MessageEnum.MNOTICE_CREATE.getMsg());
		param.setOptuserid(0L);
		param.setOptusername("系统消息");
		param.setIsread(0);//
		param.setIshandle(0);//
		param.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
		param.setMesg("["+map.get("name")+"]请及时召开"+month+"月"+map.get("name"));//消息
		param.setRemarks1(1);//备注1
		param.setRemarks2(0);//备注2
		param.setRemarks3("组织生活");//备注3
		param.setRemarks4("");//备注3
		param.setRemarks5("");//备注3
		param.setRemarks6("");//备注3
		param.setRemark("");
		return param;
	}

	/**
	 * 更新代办时间状态为创建完成
	 * @param compid
	 * @param code
	 * @param publishtime
	 */
	private void modiMeetingTodoTimeStatus(Long compid,String code,String publishtime) {
		generalSqlComponent.update(MeetingSqlCode.modiMeetingTodoTimeStatus,new Object[]{compid,code,publishtime});
	}



	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
