package com.lehand.meeting.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.cache.CacheComponent;
import com.lehand.horn.partyorgan.util.AccessTokenAuth;
import com.lehand.horn.partyorgan.util.MyX509TrustManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLEncoder;


public class WeChatUtils {
    private static Log log=LogFactory.getLog(WeChatUtils.class);
    //绑定跳转url
    //wch
//    public static String authUrl = "http://10.1.50.175:8086/h5/pages/login/login";
    //测试环境 http://192.168.60.234:16799/
//    public static String authUrl = "http://192.168.60.234:16799/h5/pages/login/login";
    //正式
    public static String authUrl = "http://dj.jac.com.cn/h5/pages/login/login";


    private static AccessTokenAuth token;

    //hj
//    private static String appid = "wx0e0a6134108bf8df";
//    private static String appsecret = "de28f06cf07f263b11dc47cb05157ca6";
    //lx
    private static String appid = "wx6e95e9238dce7688";
    private static String appsecret = "30b0615ad1e3336b00d9b2ab063b3399";
    //正式公众号
//    private static String appid = "wxc0b258c1a83c8a22";
//    private static String appsecret = "a0f4bf9eaccd973937cb6d7ef76214d8";

    private static CacheComponent cacheComponent;


    /**
     * 获取凭证
     * @return

     */
    public String getToken() {
        log.info("进入getToken获取凭证");
        if (token==null) {
            token = new AccessTokenAuth();
            String requestUrl = access_token_url.replace("APPID", appid).replace("APPSECRET", appsecret);
            JSONObject jsonObject = httpRequest(requestUrl, "GET", null);
            // 如果请求成功
            if (null != jsonObject) {
                try {
                    token.setAccess_token(jsonObject.getString("access_token"));
                    token.setExpires_in(jsonObject.getInteger("expires_in"));
                    token.setPulltime(DateEnum.now().getTime()/1000);
                    cacheComponent.set(appid,JSON.toJSONString(token));
                } catch (Exception e) {
                    token = null;
                    // 获取token失败
                    log.info("获取token失败 errcode:{"+ jsonObject.getInteger("errcode") + "} errmsg:{"+ jsonObject.getString("errmsg") + "}");
                    throw new RuntimeException("获取token失败");
                }
            }
        }
        if(token.isDisabled()){
            getRefreshToken(token);
        }
        return token.getAccess_token();
    }

    /**
     * 创建json菜单字符串并发布至微信服务器
     *
     * @param menu
     * @return 0创建菜单成功，其他失败
     */
    public int createMenu(String menu,String configfileId) {
        int result = 0;
        if(token==null){
            return result;
        }
        /*Iterator<Entry<String, AccessToken>> iterator = tokenMap.entrySet().iterator();
        while(iterator.hasNext()){
            Entry<String, AccessToken> next = iterator.next();
            AccessToken t = next.getValue();
            if(t.getC().getId().equals(configfileId)){
                token=t;
                break;
            }
        }*/

        // 拼装创建菜单的url
        String url = menu_create_url.replace("ACCESS_TOKEN", token.getAccess_token());
        log.info(menu);
        // 调用接口创建菜单
        JSONObject jsonObject = httpRequest(url, "POST", menu);
        if (null != jsonObject) {
            if (0 != jsonObject.getInteger("errcode")) {
                result = jsonObject.getInteger("errcode");
                log.info("创建菜单失败 errcode:{" + jsonObject.getInteger("errcode")	+ "} errmsg:{" + jsonObject.getString("errmsg") + "}");
            }
        }
        return result; // 创建成功
    }

    /**
     * 删除自定义菜单
     * @param accessToken
     * @return
     */
    public int deleteMenu(String accessToken) {
        String url = menu_delete_url.replace("ACCESS_TOKEN", accessToken);
        JSONObject jsonObject = httpRequest(url, "GET", null);
        return jsonObject.getInteger("errcode");

    }


    /**
     * 验证服务器地址的有效性，验证签名,
     * 若确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效，成为开发者成功，否则接入失败。
     * @param signature	微信加密签名
     * @param timestamp 时间戳
     * @param nonce	随机数
     * @return
     */
    /*public static boolean checkSignature(String signature,String timestamp,String nonce,String appid){
        AccessToken token=tokenMap.get(appid);

        if(token==null){
            log.info("非法接入，无此公众号信息,appid："+appid);
            return false;
        }
        String[] arr = new String[] { token.getC().getToken(), timestamp, nonce };
        //将token、timestamp、nonce三个参数进行字典序排序
        Arrays.sort(arr);
        StringBuilder content = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            content.append(arr[i]);
        }
        MessageDigest md = null;
        String tmpStr = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
            // 将三个参数字符串拼接成一个字符串进行sha1加密
            byte[] digest = md.digest(content.toString().getBytes());
            tmpStr = SignUtil.byteToStr(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        content = null;
        // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信
        return tmpStr != null ? tmpStr.equals(signature.toUpperCase()) : false;
    }*/

    /**获取用户信息
     * @param openid
     * @return
     */
    public JSONObject getInfo(String openid){
        System.out.println("进入获取用户信息页面");
        String url=get_user_url.replace("ACCESS_TOKEN", getToken()).replace("OPENID", openid);
        JSONObject jsonObject =httpRequest(url, "GET", null);
        return jsonObject;
    }

    /**获取所有用户列表
     * @param openid
     * @return
     */
    public JSONObject getAllUserInfo(String openid){
        String url=userInfoAll.replace("ACCESS_TOKEN", getToken()).replace("NEXT_OPENID", openid);
        JSONObject jsonObject =httpRequest(url, "GET", null);
        return jsonObject;
    }

    /**高级接口，获得用户openid
     * @param
     * @return
     */
    /*public static AccessTokenInWeb getOpenidByCode(String code,String APPID){
        String url = access_token2.replace("APPID", APPID).replace("SECRET", tokenMap.get(APPID).getC().getAppsecret()).replace("CODE", code);
        JSONObject jsonObject =httpRequest(url, "GET", null);
        AccessTokenInWeb a=new AccessTokenInWeb();
        if(!jsonObject.containsKey("errcode")){
            a.setAccess_token(jsonObject.getString("access_token"));
            a.setExpires_in(jsonObject.getInt("expires_in"));
            a.setOpenid(jsonObject.getString("openid"));
            a.setRefresh_token(jsonObject.getString("refresh_token"));
            a.setScope(jsonObject.getString("scope"));
            a.setAppId(APPID);
        }else{
            a.setErrcode(jsonObject.getString("errcode"));
            a.setErrmsg(jsonObject.getString("errmsg"));
        }
        return a;
    }*/
    /**
     * 通过code
     */
    public static String getCode(){
        String url = get_open_id.replace("APPID", appid).replace("REDIRECT_URL", URLEncoder.encode(authUrl));
            String result = HttpRequestUtils.sendGet(url);
            if(!StringUtils.isEmpty(result)){
                JSONObject json = JSONObject.parseObject(result);
                if(json.get("code")!=null){
                    return json.get("code").toString();
                }
            }
        return "";
    }

    /**跳转url，获得code
     * @param request
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getAuthorize(HttpServletRequest request,String scope,String appid) throws UnsupportedEncodingException{
        String url=authorize.replace("APPID", appid).replace("REDIRECT_URI", URLEncoder.encode(request.getRequestURL().toString(),"utf-8")).replace("SCOPE", scope);
        String queryString = request.getQueryString();
        if(!StringUtils.isEmpty(queryString)){
            url = url.replace("STATE", request.getQueryString());
        }
        return url;
    }

    /**刷新高级接口的token
     * @param token
     */
    public void getRefreshToken(AccessTokenAuth token){
        String url=refresh_token.replace("APPID", appid).replace("REFRESH_TOKEN", token.getAccess_token());
        JSONObject jsonObject =httpRequest(url, "GET", null);
        if(!jsonObject.containsKey("errcode")){
            token.setAccess_token(jsonObject.getString("access_token"));
            token.setExpires_in(jsonObject.getInteger("expires_in"));
            token.setPulltime(DateEnum.now().getTime()/1000);
            cacheComponent.set(appid,JSON.toJSONString(token));
            /*token.setOpenid(jsonObject.getString("openid"));
            token.setRefresh_token(jsonObject.getString("refresh_token"));
            token.setScope(jsonObject.getString("scope"));*/
        }else{
            token.setErrcode(jsonObject.getString("errcode"));
            token.setErrmsg(jsonObject.getString("errmsg"));
        }
    }

    /**高级接口获取用户信息
     * @param t
     * @return
     */
    /*public static JSONObject getUserinfoWeb(AccessTokenInWeb t){
        String url=userinfo.replace("ACCESS_TOKEN", t.getAccess_token()).replace("OPENID", t.getOpenid());
        JSONObject jsonObject =httpRequest(url, "GET", null);
        return jsonObject;
    }*/

    /**获取js的ticket
     * @param
     * @return
     */
//    public static String getJsApiTicket(){
//        String requestUrl = "https://api.weixin.qq.com/cgi-bin/token?";
//        String params = "grant_type=client_credential&appid=" + appid + "&secret=" + appsecret + "";
//        String result = HttpRequestUtils.httpGet(requestUrl+params);
//        String access_token = JSONObject.parseObject(result).getString("access_token");
//        requestUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?";
//        params = "access_token=" + access_token + "&type=jsapi";
//        result =  HttpRequestUtils.httpGet(requestUrl+params);
//        String jsapi_ticket = JSONObject.parseObject(result).getString("ticket");
//        int activeTime=Integer.parseInt(JSONObject.parseObject(result).getString("expires_in"));
//        return jsapi_ticket;
//    }
    /**获取js的ticket
     * @param
     * @return
     */
    public static String getJsApiTicket(){
//        String requestUrl = "https://api.weixin.qq.com/cgi-bin/token?";
//        String params = "grant_type=client_credential&appid=" + appid + "&secret=" + appsecret + "";
//        String result = HttpRequestUtils.httpGet(requestUrl+params);
//        String access_token = JSONObject.parseObject(result).getString("access_token");
        String access_token =  token.getAccess_token();
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?";
        String params = "access_token=" + access_token + "&type=jsapi";
        String result =  HttpRequestUtils.httpGet(requestUrl+params);
        String jsapi_ticket = JSONObject.parseObject(result).getString("ticket");
//        int activeTime = Integer.parseInt(JSONObject.parseObject(result).getString("expires_in"));
        cacheComponent.set("access_token",access_token);
        cacheComponent.set("jsapi_ticket", jsapi_ticket);
//        if (activeTime > 7200) {
//            log.info("获取jsapi_ticket失败!");
//        }
        return jsapi_ticket;
    }
    /**发送请求
     * @param requestUrl
     * @param requestMethod
     * @param outputStr
     * @return
     */
    public JSONObject httpRequest(String requestUrl,	String requestMethod, String outputStr) {
        JSONObject jsonObject = null;
        StringBuffer buffer = new StringBuffer();
        try {
            // 创建SSLContext对象，并使用我们指定的信任管理器初始化
            TrustManager[] tm = { new MyX509TrustManager() };
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new java.security.SecureRandom());
            // 从上述SSLContext对象中得到SSLSocketFactory对象
            SSLSocketFactory ssf = sslContext.getSocketFactory();

            // 获取链接
            URL url = new URL(requestUrl);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            // 证书
            conn.setSSLSocketFactory(ssf);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            // 设置请求方式（GET/POST）
            conn.setRequestMethod(requestMethod);
            if ("GET".equalsIgnoreCase(requestMethod)){
                conn.connect();
            }

            // 当有数据需要提交时
            if (null != outputStr && !"".equals(outputStr)) {
                OutputStream outputStream = conn.getOutputStream();
                // 注意编码格式，防止中文乱码
                outputStream.write(outputStr.getBytes("UTF-8"));// 写进去
                outputStream.close();
            }
            log.info("<<<提交请求方式"+requestMethod+",url:"+requestUrl+outputStr!=null?("\r提交数据："+outputStr):"");
            // 将返回的输入流转换成字符串
            InputStream inputStream = conn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(	inputStreamReader);

            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            // 释放资源
            inputStream.close();
            inputStream = null;
            conn.disconnect();
            log.info(">>>>>>>响应数据："+buffer);
            jsonObject = JSONObject.parseObject(buffer.toString());
            log.info(jsonObject.toString());
        } catch (ConnectException ce) {
            log.info("Weixin server connection timed out.");
        } catch (Exception e) {
            // log.error("https request error:{}", e);
        }
        return jsonObject;
    }

    /**
     * 获取生成用户关注二维码所需二维码ticket
     * @param scene_id 传入的参数
     */
    public String getLsTicket(Integer scene_id ){
        String lsTicket ="";
        //1.先获取access_token
        String access_token =  token.getAccess_token();
        System.out.println(access_token);
        //根据access_token获取的创建临时二维码ticket,expire_seconds是临时二维码有效时间，最大是30天，scene_id是传入的参数
        String jsonStr = "{\"expire_seconds\": 2592000,\"action_name\": \"QR_SCENE\", \"action_info\": {\"scene\": {\"scene_id\": "+scene_id+"}}}";
        String requestUrl = ls_ticket_url.replace("ACCESS_TOKEN", access_token);
        JSONObject jsonObject = httpRequest(requestUrl, "POST", jsonStr);
        // 如果请求成功
        if (null != jsonObject) {
            try {
                lsTicket=jsonObject.getString("ticket");
                //提醒：TICKET记得进行UrlEncode
                lsTicket =  URLEncoder.encode(lsTicket,"utf-8");
            } catch (Exception e) {
                // 获取ticket失败
                log.info("获取ticket失败 errcode:{"+ jsonObject.getInteger("errcode") + "} errmsg:{"+ jsonObject.getString("errmsg") + "}");
                throw new RuntimeException("获取ticket失败");
            }
        }
        return lsTicket;

    }




    public WeChatUtils(CacheComponent cacheComponent) {
        // TODO Auto-generated method stub
        System.out.println("-----------------》进入WechatUtil的init方法");
        this.cacheComponent = cacheComponent;
        String tokenjson = cacheComponent.get(appid);
        if(StringUtils.isEmpty(tokenjson)){
            getToken();
        }else{
            token = JSON.toJavaObject(JSON.parseObject(tokenjson),AccessTokenAuth.class);
            if(token.isDisabled()){
                token = null;
                getToken();
            }
        }

        System.out.println("-----------------》init完成");
    }

    //封装微信文本内容
    //type 1提示绑定  2提示签到成功
    public static String createContent(String FromUserName, String ToUserName, String CreateTime,int type){
        String content = "";
        if(type == 1){
            content = strUrl(FromUserName);
        }
        if(type == 2){
            content = sign();
        }
        if(type == 3){
            content = repeatCheck();
        }


        String message = "<xml>"

                +"<ToUserName><![CDATA["+FromUserName+"]]></ToUserName>"

                +"<FromUserName><![CDATA["+ToUserName+"]]></FromUserName>"

                +"<CreateTime>"+CreateTime+"</CreateTime>"

                +"<MsgType><![CDATA[text]]></MsgType>"

                +"<Content><![CDATA["+content+"]]></Content>"

                +"</xml>";
        return message;
    }

    //生成提示信息
    public static String strUrl(String openid){
        String loginurl = "<a href=\""+authUrl+"?openid=" + openid + "\">绑定</a>";
        StringBuilder contentBuffer = new StringBuilder();
        contentBuffer.append("您好：\n");
        contentBuffer.append("微信尚未绑定账号，请绑定账号。\n");
        contentBuffer.append(loginurl).append("\n");
        contentBuffer.append("绑定后，下次扫码可直接签到哦。");
        String content = contentBuffer.toString();
        return content;
    }

    //签到成功
    public static String sign(){
        return "签到成功！";
    }

    //请勿重复签到
    public static String repeatCheck(){
        return "请勿重复签到！";
    }

    private static String access_token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
    private static String menu_create_url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
    private static String menu_delete_url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";
    private static String chat_get_url = "https://api.weixin.qq.com/cgi-bin/customservice/getrecord?access_token=ACCESS_TOKEN";
    private static String get_user_url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
    private static String authorize = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
    private static String access_token2 = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
    private static String userinfo = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
    private static String refresh_token = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN";
    private static String mchdown_real_new = "http://mch.tenpay.com/cgi-bin/mchdown_real_new.cgi";
    private static String delivernotify="http://api.weixin.qq.com/cgi-bin/pay/delivernotify?access_token=ACCESS_TOKEN";
    private static String userInfoAll="https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID";

    private static String jsapi_tick="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
    private static String ls_ticket_url="https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=ACCESS_TOKEN";

    private static String ls_showqrcode_url="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET";

    private static String get_open_id = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URL&response_type=code&scope=snsapi_base&state=123#wechat_redirect";

    //发送党费
    private static String send_dues_message = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

}
