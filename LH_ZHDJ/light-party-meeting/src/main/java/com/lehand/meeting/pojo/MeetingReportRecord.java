package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="工作汇报记录表", description="工作汇报记录表")
public class MeetingReportRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户ID", name="compid")
    private Long compid;

    @ApiModelProperty(value="自增长主键", name="id")
    private Long id;

    @ApiModelProperty(value="主题", name="topic")
    private String topic;

    @ApiModelProperty(value="汇报类容", name="report_content")
    private String reportContent;

    @ApiModelProperty(value="组织编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="组织ID", name="orgid")
    private String orgid;

    @ApiModelProperty(value="创建人id", name="createuserid")
    private Long createuserid;

    @ApiModelProperty(value="修改人id", name="updateuserid")
    private Long updateuserid;

    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="修改时间", name="updatetime")
    private String updatetime;

    @ApiModelProperty(value="备注1", name="remark1")
    private Integer remark1;

    @ApiModelProperty(value="审核驳回意见", name="remark2")
    private String remark2;

    @ApiModelProperty(value="组织名称", name="orgname")
    private String orgname;

    @ApiModelProperty(value="状态（ 0:删除,1:保存,2:提交(待审核),3:撤销,4:(审核通过),5:(审核驳回))", name="status")
    private Integer status;

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public Long getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(Long createuserid) {
        this.createuserid = createuserid;
    }

    public Long getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(Long updateuserid) {
        this.updateuserid = updateuserid;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getRemark1() {
        return remark1;
    }

    public void setRemark1(Integer remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}