package com.lehand.meeting.constant;

public class MeetingTalkSqlCode {
    /**--------------------谈心谈话记录------------------------*/
    /**
     *insert into meeting_talk_record(compid,topic,recordtime,createtime,context,createuserid,updatetime,updateuserid,orgcode,orgid,orgname,status,remark1,remark2) values (:compid,:topic,:recordtime,:createtime,:context,:createuserid,:updatetime,:updateuserid,:orgcode,:orgid,:orgname,:status,:remark1,:remark2)
     */
    public static final String addMeetingTalkRecord = "addMeetingTalkRecord";

    /**
     *select a.* from meeting_talk_record a where a.compid = ? and a.id = ?
     */
    public static final String getMeetingTalkRecordById = "getMeetingTalkRecordById";

    /**
     *select a.* from meeting_talk_record a where a.compid = ? and a.id = ?
     *and a.compid = :compid
     *and a.id = :id
     *and a.topic = :topic
     *and a.recordtime = :recordtime
     *and a.createtime = :createtime
     *and a.context = :context
     *and a.createuserid = :createuserid
     *and a.updatetime = :updatetime
     *and a.updateuserid = :updateuserid
     *and a.orgcode = :orgcode
     *and a.orgid = :orgid
     *and a.remark1 = :remark1
     *and a.remark2 = :remark2
     */
    public static final String listMeetingTalkRecord = "listMeetingTalkRecord";

    /**
     *select a.* from meeting_talk_record a where a.compid = :compid
     * and topic like  '%${PARAMS.topic}%'
     * and recordtime >= :startDate
     * and recordtime <= :endDate
     * page
     */
    public static final String pageMeetingTalkRecord = "pageMeetingTalkRecord";

    /**
     * 修改谈话信息
     * update meeting_talk_record set topic = :topic,recordtime=:recordtime,context=:context,updatetime=:updatetime,updateuserid=:updateuserid ,status=:status where compid=:compid and id = :id
     */
    public static final String modifyMeetingTalkRecordByid = "modifyMeetingTalkRecordByid";

    /**
     *delete from meeting_talk_record  where compid = :compid
     *and compid = :compid
     *and topic = :topic
     *and recordtime = :recordtime
     *and createtime = :createtime
     *and context = :context
     *and createuserid = :createuserid
     *and updatetime = :updatetime
     *and updateuserid = :updateuserid
     *and orgcode = :orgcode
     *and orgid = :orgid
     *and remark1 = :remark1
     *and remark2 = :remark2
     */
    public static final String delMeetingTalkRecord = "delMeetingTalkRecord";

    /**
     *select count(id) from meeting_talk_record where compid = :compid
     * and compid = :compid
     * and topic = :topic
     * and recordtime = :recordtime
     * and createtime = :createtime
     * and context = :context
     * and createuserid = :createuserid
     * and updatetime = :updatetime
     * and updateuserid = :updateuserid
     * and orgcode = :orgcode
     * and orgid = :orgid
     * and remark1 = :remark1
     * and remark2 = :remark2
     */
    public static final String countMeetingTalkRecord = "countMeetingTalkRecord";

    /**
     * 更新谈话记录状态
     * update meeting_talk_record set status = -1 where compid = ? and id = ?
     */
    public static final String updateTalkRecordStatus = "updateTalkRecordStatus";
}
