package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @program: huaikuang-dj
 * @description: MeetingAbsentSubject
 * @author: zwd
 * @create: 2020-11-05 17:18
 */
public class MeetingAbsentSubject implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户id", name="compid")
    private Long compid;

    @ApiModelProperty(value="会议通知id", name="noticeid")
    private Long noticeid;

    @ApiModelProperty(value="缺席人员id", name="userid")
    private String userid;

    @ApiModelProperty(value="缺席人员名", name="username")
    private String username;

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(Long noticeid) {
        this.noticeid = noticeid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}