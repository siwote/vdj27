package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingNoticeRecord;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class MeetingNoticeRecordDto extends MeetingNoticeRecord {

    private static final long serialVersionUID = 1L;

    //2020-05-30
    @ApiModelProperty(value="关联标签列表", name="tagcode")
    private List<String> tagcode;

    public List<String> getTagcode() {
        return tagcode;
    }

    public void setTagcode(List<String> tagcode) {
        this.tagcode = tagcode;
    }
}
