package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.dto.MeetingRecordTopicsDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author code maker
 */
@Service("meetingRecordTopicsService")
public class MeetingRecordTopicsService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 分页查询
     *
     * @param session
     * @param meetingRecordTopicsDto
     * @return
     */
    public Pager pageMeetingRecordTopics(Session session, MeetingRecordTopicsDto meetingRecordTopicsDto, Pager pager) {
        return generalSqlComponent.pageQuery(MeetingSqlCode.pageMeetingRecordTopics, meetingRecordTopicsDto, pager);
    }

    /**
     * 查询列表
     *
     * @param session
     * @param meetingRecordTopicsDto
     * @return
     */
    public List<MeetingRecordTopicsDto> listMeetingRecordTopics(Session session, MeetingRecordTopicsDto meetingRecordTopicsDto) {
        return generalSqlComponent.query(MeetingSqlCode.listMeetingRecordTopics, meetingRecordTopicsDto);
    }

    /**
     * 根据id查询
     *
     * @param id     主键
     * @param
     */
    public MeetingRecordTopicsDto getMeetingRecordTopicsById(Long id) {
        return generalSqlComponent.query(MeetingSqlCode.getMeetingRecordTopicsById, new Object[]{id});
    }

    /**
     * 新增
     *
     * @param meetingRecordTopicsDto
     * @return
     */
    public Long addMeetingRecordTopics(Session session, MeetingRecordTopicsDto meetingRecordTopicsDto) {
        generalSqlComponent.insert(MeetingSqlCode.addMeetingRecordTopics, meetingRecordTopicsDto);
        return meetingRecordTopicsDto.getId();
    }

    /**
     * 根据主键修改
     *
     * @param session
     * @param meetingRecordTopicsDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int modifyMeetingRecordTopics(Session session, MeetingRecordTopicsDto meetingRecordTopicsDto) {
        return generalSqlComponent.update(MeetingSqlCode.modifyMeetingRecordTopicsById, meetingRecordTopicsDto);
    }

    /**
     * 通过主键物理删除
     *
     * @param session
     * @param meetingRecordTopicsDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int removeMeetingRecordTopics(Session session, MeetingRecordTopicsDto meetingRecordTopicsDto) {
        if (meetingRecordTopicsDto.getId() == null) {
            LehandException.throwException("id为空，不允许删除所有数据");
        }
        return generalSqlComponent.delete(MeetingSqlCode.delMeetingRecordTopicsById, meetingRecordTopicsDto);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delMeetingRecordTopicsByMrid(MeetingRecordTopicsDto meetingRecordTopicsDto) {
        generalSqlComponent.delete(MeetingSqlCode.delMeetingRecordTopicsByMrid, meetingRecordTopicsDto);
    }
}
