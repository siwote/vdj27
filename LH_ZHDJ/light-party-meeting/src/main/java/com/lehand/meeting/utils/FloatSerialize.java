package com.lehand.meeting.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class FloatSerialize extends JsonSerializer<Float> {
    @Override
    public void serialize(Float value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        if (value != null) {
            BigDecimal bg = new BigDecimal(value).setScale(1, RoundingMode.UP);
            double num = bg.doubleValue();
            if (Math.round(num) - num == 0) {
                gen.writeString(String.valueOf((long) num));
            }else{
                gen.writeString(String.valueOf(num));
            }
        } else {
            gen.writeString("0");
        }
    }
}
