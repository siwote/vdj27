package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

@ApiModel(value="会议DTO", description="会议签到表DTO")
public class MeetingSignDto {

    @ApiModelProperty(value="签到表名称", name="fileName")
    private String fileName;

    @ApiModelProperty(value="会议时间", name="time")
    private String time;

    @ApiModelProperty(value="会议主题", name="topic")
    private String topic;

    @ApiModelProperty(value="签到人员数组", name="nameList")
    private List<Map<String,Object>> nameList;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<Map<String, Object>> getNameList() {
        return nameList;
    }

    public void setNameList(List<Map<String, Object>> nameList) {
        this.nameList = nameList;
    }
}
