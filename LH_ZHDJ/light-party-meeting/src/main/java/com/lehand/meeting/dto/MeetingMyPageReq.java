package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="我的会议分页查询参数",description="我的会议分页查询参数")
public class MeetingMyPageReq extends MNoticePageReq{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="参会状态", name="pstatus")
    private int pstatus;

    @ApiModelProperty(value="参会人id", name="userid")
    private String userid;

    public int getPstatus() {
        return pstatus;
    }

    public void setPstatus(int pstatus) {
        this.pstatus = pstatus;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
