package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author admin
 *
 */
@ApiModel(value="会议列表查询参数", description="会议列表查询参数")
public class MeetingPageReq implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="请求参数_会议类型",required = true,name="mtype")
    private String mtype;

    @ApiModelProperty(value="请求参数_查询关键字", name="likeStr")
    private String likeStr;

    @ApiModelProperty(value="请求参数_起始时间", name="starttime")
    private String starttime;

    @ApiModelProperty(value="请求参数_结束时间", name="endtime")
    private String endtime;

    @ApiModelProperty(value="请求参数_组织机构编码", name="orgcode",required = true)
    private String orgcode;

    @ApiModelProperty(value="请求参数_结束时间", name="year")
    private String year;

    @ApiModelProperty(hidden = true)
    private String month;

    @ApiModelProperty(value="请求参数_状态", name="status")
    private int status;
    
    @ApiModelProperty(value="租户",hidden = true)
    private Long compid;

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public String getLikeStr() {
        return likeStr;
    }

    public void setLikeStr(String likeStr) {
        this.likeStr = likeStr;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCompid() {
		return compid;
	}

	public void setCompid(Long compid) {
		this.compid = compid;
	}}
