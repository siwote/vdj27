package com.lehand.meeting.constant.common;

public class MagicConstant {

    /**
     * 字符串常量
     */
    public static interface STR {
        //空字符串
        final String  EMPTY_STR = "";

        final String  PATH_STR = "path";

        final String  EMPTY_JSON = "{}";
    }

    /**
     * 数字格式字符串
     */
    public static interface NUM_STR {
        final String  NUM_0 = "0";
        final String NUM_1 = "1";
        final String NUM_2 = "2";
        final String NUM_4 = "4";
        final String NUM_10 = "10";
        final String NUM_11 = "11";
    }
    /**
     * 工作汇报状态
     * 状态（ 0:删除,1:保存,2:提交(待审核),3:撤销,4:(审核通过),5:(审核驳回)）
     */
    public static interface REPORT_INT {
        final Integer  INT_0 = 0;
        final Integer INT_1 = 1;
        final Integer INT_2 = 2;
        final Integer INT_4 = 4;
        final Integer INT_5 = 5;
    }
}
