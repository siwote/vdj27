package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @program: huaikuang-dj
 * @description: meetingRecordRelation
 * @author: zwd
 * @create: 2020-10-29 11:01
 */
@ApiModel(value="会议(会议通知)和会议记录关联表", description="会议(会议通知)和会议记录关联表")
public class MeetingRecordRelation  implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="会议(会议通知id)", name="noticeid")
    private Long noticeid;

    @ApiModelProperty(value="会议记录id", name="mrid")
    private Long mrid;

    @ApiModelProperty(value="会议类型", name="type")
    private String type;

    public Long getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(Long noticeid) {
        this.noticeid = noticeid;
    }

    public Long getMrid() {
        return mrid;
    }

    public void setMrid(Long mrid) {
        this.mrid = mrid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}