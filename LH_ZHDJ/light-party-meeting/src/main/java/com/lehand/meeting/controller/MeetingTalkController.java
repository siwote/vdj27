package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingTalkRecordDto;
import com.lehand.meeting.service.MeetingTalkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@Api(value = "谈心谈话", tags = { "谈心谈话" })
@RestController
@RequestMapping("/horn/talk")
public class MeetingTalkController extends BaseController {


    @Resource
    private MeetingTalkService meetingTalkService;

    /**
     * 分页查询
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message page(Pager pager, String topic, String startDate, String endDate, String orgcode) {
        Message message = new Message();
        message.success();
        MeetingTalkRecordDto meetingTalkRecordDto = new MeetingTalkRecordDto();
        meetingTalkRecordDto.setTopic(topic);
        meetingTalkRecordDto.setStartDate(startDate);
        meetingTalkRecordDto.setEndDate(endDate);
        meetingTalkRecordDto.setOrgcode(orgcode);
        message.setData(new PagerUtils<Map<String,Object>>(
                meetingTalkService.pageMeetingTalkRecord(getSession(),meetingTalkRecordDto,pager)));
        return message;
    }

    /**
     * 列表
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "列表查询", notes = "列表查询", httpMethod = "POST")
    @RequestMapping(value = "/list", method = RequestMethod .POST)
    public Message list(@RequestBody MeetingTalkRecordDto meetingTalkRecordDto) {
        Message message = new Message();
        message.success();
        message.setData(meetingTalkService.listMeetingTalkRecord(getSession(),meetingTalkRecordDto));
        return message;
    }


    /**
     * 新增
     */
    @OpenApi(optflag=1)
    @ApiOperation(value = "新增谈话", notes = "新增谈话", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message add(@RequestBody MeetingTalkRecordDto meetingTalkRecordDto) {
        Message message = new Message();
        message.success();
        message.setData(
                meetingTalkService.addMeetingTalkRecord(getSession(),meetingTalkRecordDto));
        return message;
    }


    /**
     * 查看详情
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message get(@RequestBody MeetingTalkRecordDto meetingTalkRecordDto) {
        Message message = new Message();
        MeetingTalkRecordDto result = meetingTalkService.getMeetingTalkRecord(getSession(),meetingTalkRecordDto);
        message.success();
        message.setData(result);
        return message;
    }

    /**
     * 修改
     */
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message modify(@RequestBody MeetingTalkRecordDto meetingTalkRecordDto) {
        Message message = new Message();
        message.success();
        message.setData(
                meetingTalkService.modifyMeetingTalkRecord(getSession(),meetingTalkRecordDto));
        return message;
    }

    /**
     * 根据id删除，逻辑删除
     */
    @OpenApi(optflag=3)
    @ApiOperation(value = "谈心谈话记录删除", notes = "谈心谈话记录删除（逻辑删除）", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message remove(Long id) {
        Message message = new Message();
        message.success();
        message.setData(
                meetingTalkService.removeMeetingTalkRecord(getSession(),id));
        return message;
    }

}
