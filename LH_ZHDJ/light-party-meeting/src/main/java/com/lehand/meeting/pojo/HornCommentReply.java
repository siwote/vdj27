package com.lehand.meeting.pojo;

import java.io.Serializable;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="评论回复", description="评论回复")
public class HornCommentReply implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户ID", name="compid")
    private Long compid;

    @ApiModelProperty(value="自增长主键", name="id")
    private Long id;

    @ApiModelProperty(value="模块ID", name="moduleid")
    private Long moduleid;

    @ApiModelProperty(value="回复时间", name="replytime")
    private String replytime;


    @ApiModelProperty(value="回复修改时间", name="updatetime")
    private String updatetime;



    @ApiModelProperty(value="回复主体类型(0:用户,1:机构/组/部门,2:角色)", name="sjtype")
    private Integer sjtype;

    @ApiModelProperty(value="回复类型(0:文本,1:语音)", name="repflag")
    private Integer repflag;

    @ApiModelProperty(value="回复内容", name="reptext")
    private String reptext;

    @ApiModelProperty(value="备注1", name="remarks1")
    private Integer remarks1;

    @ApiModelProperty(value="备注2", name="remarks2")
    private Integer remarks2;

    @ApiModelProperty(value="备注3", name="remarks3")
    private String remarks3;

    @ApiModelProperty(value="备注3", name="remarks4")
    private String remarks4;

    @ApiModelProperty(value="回复人", name="repid")
    private Long repid;

    @ApiModelProperty(value="回复人名称", name="repname")
    private String repname;

    @ApiModelProperty(value="评论id", name="commid")
    private Long commid;

    @ApiModelProperty(value="原评论人ID", name="comuserid")
    private Long comuserid;

    @ApiModelProperty(value="原评论人名称", name="comusername")
    private String comusername;

    @ApiModelProperty(value="状态", name="status")
    private Integer status;


    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getModuleid() {
        return moduleid;
    }

    public void setModuleid(Long moduleid) {
        this.moduleid = moduleid;
    }

    public String getReplytime() {
        return replytime;
    }

    public void setReplytime(String replytime) {
        this.replytime = replytime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getSjtype() {
        return sjtype;
    }

    public void setSjtype(Integer sjtype) {
        this.sjtype = sjtype;
    }

    public Integer getRepflag() {
        return repflag;
    }

    public void setRepflag(Integer repflag) {
        this.repflag = repflag;
    }

    public String getReptext() {
        return reptext;
    }

    public void setReptext(String reptext) {
        this.reptext = reptext;
    }

    public Integer getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    public Integer getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    public String getRemarks4() {
        return remarks4;
    }

    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    public Long getRepid() {
        return repid;
    }

    public void setRepid(Long repid) {
        this.repid = repid;
    }

    public String getRepname() {
        return repname;
    }

    public void setRepname(String repname) {
        this.repname = repname;
    }

    public Long getCommid() {
        return commid;
    }

    public void setCommid(Long commid) {
        this.commid = commid;
    }

    public Long getComuserid() {
        return comuserid;
    }

    public void setComuserid(Long comuserid) {
        this.comuserid = comuserid;
    }

    public String getComusername() {
        return comusername;
    }

    public void setComusername(String comusername) {
        this.comusername = comusername;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}