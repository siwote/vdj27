package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MNoticeAddReq;
import com.lehand.meeting.pojo.MeetingNoticeRecord;
import com.lehand.meeting.service.MeetingTodoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;

@Api(value = "会议待办事项", tags = { "会议待办事项" })
@RestController
@RequestMapping("/horn/todo")
public class MeetingTodoController extends BaseController {

    @Resource
    private MeetingTodoService meetingTodoService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记和党员查询待办事项列表", notes = "书记和党员查询待办事项列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "待办类型（0：未办；1：已办）", required = true, paramType = "query", dataType = "int", defaultValue = "0"),
    })
    @RequestMapping(value = "/pagelist", method = RequestMethod.POST)
    public Message pagelist(int status, Pager pager, String type) {
        Message msg = new Message();
        return msg.setData(meetingTodoService.pagelist(status, pager, getSession(), type)).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "待办事项操作-书记", notes = "待办事项操作-书记", httpMethod = "POST")
    @RequestMapping(value = "/handleToDo", method = RequestMethod.POST)
    public Message<MeetingNoticeRecord> handleToDo(@RequestBody MNoticeAddReq req) throws Exception {
        Message<MeetingNoticeRecord> msg = new Message<MeetingNoticeRecord>();
        return msg.setData(meetingTodoService.handleToDo(req, getSession())).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "待办事项操作-党员", notes = "待办事项操作-党员", httpMethod = "POST")
    @RequestMapping(value = "/handleTAttend", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "待办类型ID", required = true, paramType = "query", dataType = "Long", defaultValue = "0"),
            @ApiImplicitParam(name = "status", value = "参会状态(0 默认状态 1:参会 ,2 不参会)", required = true, paramType = "query", dataType = "int", defaultValue = "0")
    })
    public Message handleTAttend(Long id, int status) {
        Message msg = new Message();
        meetingTodoService.handleTAttend(id, status, getSession());
        return msg.success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "修改会议待办状态", notes = "修改会议待办状态", httpMethod = "POST")
    @RequestMapping(value = "/updatestatus", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "待办类型ID", required = true, paramType = "query", dataType = "Long", defaultValue = "0"),
            @ApiImplicitParam(name = "status", value = "待办状态:(0未处理,1已处理)", required = true, paramType = "query", dataType = "Long", defaultValue = "0"),
    })
    public Message updatestatus(Long id, int status) {
        Message msg = new Message();
        meetingTodoService.updateMeetingTodoStatus(status, getSession().getCompid(), id);
        return msg.success();
    }


    @OpenApi(optflag=2)
    @ApiOperation(value = "查询待参加会议的状态", notes = "查询待参加会议的状态", httpMethod = "POST")
    @RequestMapping(value = "/checkMeetingStatus", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "businessid", value = "待办业务ID", required = true, paramType = "query", dataType = "Long", defaultValue = "0")
    })
    public Message checkMeetingStatus (Long businessid) throws ParseException {
        Message msg = new Message();
        msg.setData(meetingTodoService.checkMeetingStatus(businessid,getSession()));
        return msg.success();
    }
}
