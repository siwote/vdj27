package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

@ApiModel(value="修改会议通知参数",description="修改会议通知参数")
public class MNoticeInfoDto extends MNoticeAddReq {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="会议通知id", name="noticeid")
    private Long noticeid;

    public Long getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(Long noticeid) {
        this.noticeid = noticeid;
    }

}
