package com.lehand.meeting.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.meeting.dto.MeetingGuideDto;
import com.lehand.meeting.dto.MeetingMsgCfgDto;
import com.lehand.module.common.pojo.SystemParam;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 会议配置管理类
 */
@Service
public class MeetingCfgService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 根据上级key获取子级内容集合
     * @param upParamKey
     * @param session
     * @return
     */
    public List<SystemParam> getMeetingGuideList(String upParamKey, Session session) {
        return generalSqlComponent.query(MeetingSqlCode.listSysParamByUpparamkey, new Object[] {upParamKey, session.getCompid()});
    }

    /**
     * 修改会议指南的信息
     * @param meetingGuideDto
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingGuideDto configMeetingGuide(MeetingGuideDto meetingGuideDto, Session session) {
        if(meetingGuideDto == null || meetingGuideDto.getSystemParamList().size() < 0) {
            LehandException.throwException("提交的参数信息不完整，请检查后重试！");
        }

        List<SystemParam> systemParamList = meetingGuideDto.getSystemParamList();
        for(SystemParam systemParam : systemParamList) {
            //key值不能为空
            if(StringUtils.isEmpty(systemParam.getParamkey()) || StringUtils.isEmpty(systemParam.getParamvalue())) {
                LehandException.throwException("提交的参数信息不完整，请检查后重试！");
            }
            systemParam.setCompid(session.getCompid());
            //更改会议指南信息
            generalSqlComponent.update(MeetingSqlCode.updateSysParamValue, systemParam);
        }

        return meetingGuideDto;
    }

    /**
     * 会议自动提交配置项
     * @param status  保存是否开启定时提交 (0 开启 1 关闭)
     * @param hour  保存设定的有效时间值（整数值 单位 h）
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public SystemParam configAutoCommit(int status, int hour, Session session) {
        SystemParam systemParam = generalSqlComponent.query(MeetingSqlCode.getSysParam, new Object[] {
                session.getCompid(), SysConstants.MEETING_CFG_CONSTANT.MEETING_AUTO_COMMIt_CFG});

        //赋值参数
        SystemParam param = new SystemParam();
        param.setCompid(session.getCompid());
        param.setParamkey(SysConstants.MEETING_CFG_CONSTANT.MEETING_AUTO_COMMIt_CFG);
        param.setParamname("string");
        param.setHassubset(0);
        param.setParamvalue(String.valueOf(status));//保存是否开启定时提交 (0 开启 1 关闭)
        param.setClassify("busi");
        param.setCanmodify(1);
        param.setInnerparam(0);
        param.setRemarks2(hour);  //保存设定的有效时间值（整数值 单位 h）
        if(systemParam == null) {
            generalSqlComponent.insert(MeetingSqlCode.addSysParam, param);
        } else {
            generalSqlComponent.update(MeetingSqlCode.updateMeetingAutoCfg, param);
        }
        return param;
    }

    /**
     * 会议记录周期性的提醒
     */
    @Transactional(rollbackFor = Exception.class)
    public void addMeetingRemind(MeetingMsgCfgDto meetingMsgCfgDto, Session session) {
        if(meetingMsgCfgDto == null || meetingMsgCfgDto.getSystemParamList().size() < 0) {
            LehandException.throwException("提交的参数信息不完整，请检查后重试！");
        }

        List<SystemParam> msgCfgDtoList = meetingMsgCfgDto.getSystemParamList();
        for(SystemParam systemParam : msgCfgDtoList) {
            if(StringUtils.isEmpty(systemParam.getParamkey()) || StringUtils.isEmpty(systemParam.getParamvalue())) {
                LehandException.throwException("提交的参数信息不完整，请检查后重试！");
            }
            systemParam.setCompid(session.getCompid());
            generalSqlComponent.update(MeetingSqlCode.updateSysParamValue, systemParam);
            //列出提醒点并保存
            makeRemindTimeNotes(systemParam);
        }
    }

    //生成提醒点
    private void makeRemindTimeNotes(SystemParam systemParam) {
        String paramvalue = systemParam.getParamvalue();
        String typecode = systemParam.getParamkey();
        JSONObject jsonObject = JSON.parseObject(paramvalue);

        switch (typecode){
            case SysConstants.TIME_SET.APPRAISAL_OF_MEMBERS_TIME:
                createTimeByYear(jsonObject.get("months"),jsonObject.get("days"));
            case SysConstants.TIME_SET.BRANCH_COMMITTEE_TIME:
                createTimeByMonth(jsonObject.get("days"));
            case SysConstants.TIME_SET.BRANCH_PARTY_CONGRESS_TIME:
                createTimeByQuarter(jsonObject.get("months"),jsonObject.get("days"));
            case SysConstants.TIME_SET.DEMOCRATIC_LIFE_MEETING_TIME:
                createTimeByYear(jsonObject.get("months"),jsonObject.get("days"));
            case SysConstants.TIME_SET.ORG_LIFE_MEETING_TIME:
                createTimeByHalfYear(jsonObject.get("quarter"),jsonObject.get("months"),jsonObject.get("days"));
            case SysConstants.TIME_SET.PARTY_GROUP_MEETING_TIME:
                createTimeByMonth(jsonObject.get("days"));
            case SysConstants.TIME_SET.PARTY_LECTURE_TIME:
                createTimeByQuarter(jsonObject.get("months"),jsonObject.get("days"));
            case SysConstants.TIME_SET.THEME_PARTY_DAY_TIME:
                createTimeByMonth(jsonObject.get("days"));
            case SysConstants.TIME_SET.THEORETICAL_STUDY_TIME:
                createTimeByMonth(jsonObject.get("days"));
            case SysConstants.TIME_SET.COMMITTEE_MEETING_TIME:
                createTimeByHalfYear(jsonObject.get("quarter"),jsonObject.get("months"),jsonObject.get("days"));
            default:
        }
    }

    /**
     * 没半年 xx季度,xxyue,xx日生成 提醒时间
     * @param quarter
     * @param months
     * @param days
     */
    private void createTimeByHalfYear(Object quarter, Object months, Object days) {
    }

    /**
     * 每季度 xx月 xx日生成提醒时间
     * @param months
     * @param days
     */
    private void createTimeByQuarter(Object months, Object days) {
    }

    /**
     * 每月 xx 日生成消息提醒
     * @param days
     */
    private void createTimeByMonth(Object days) {
    }

    /**
     * 每年 X月;XX月,x日,xx日生成消息提醒
     * @param months
     * @param days
     */
    private void createTimeByYear(Object months, Object days) {

    }

    public SystemParam getMeetingGuide(Session session, String paramkey) {
        SystemParam param = generalSqlComponent.query(MeetingSqlCode.getSysParam,new Object[]{session.getCompid(),paramkey});
        return param;
    }
}
