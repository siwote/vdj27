package com.lehand.meeting.dto;

import com.lehand.base.common.Pager;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

//@ApiModel(value="会议通知分页查询参数",description="会议通知分页查询参数")
@ApiModel(value="会议分页查询参数",description="会议分页查询参数")
public class MNoticePageReq implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true, name="compid")
    private Long compid;

    @ApiModelProperty(value="组织机构编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="状态", name="status")
    private String status;

    //2020-06-08 新增
    @ApiModelProperty(value="移动端查询状态（'':全部,0:未开始,1:进行中,2:已结束）", name="querystatus")
    private String querystatus;

    @ApiModelProperty(value="移动端菜单区分（0:会议,1:党员活动日,2:民主生活会）", name="querystatus")
    private String flag;

    @ApiModelProperty(value="开始时间", name="starttime")
    private String starttime;

    @ApiModelProperty(value="结束时间", name="endtime")
    private String endtime;

    @ApiModelProperty(value="查询关键字", name="likeStr")
    private String likeStr;

    @ApiModelProperty(value="分页参数", name="pager")
    private Pager pager;

    // 2020-0525 添加
    @ApiModelProperty(value="会议类型", name="mrtype")
    private String mrtype;

    // 2020-0608 添加
    @ApiModelProperty(value="会议属性(标签)", name="tagcode")
    private List<String> tagcode;

    //2020-11-16
    @ApiModelProperty(value="身份证", name="idcard")
    private String idcard;

    public String getQuerystatus(){return querystatus;}

    public void setQuerystatus(String querystatus){this.querystatus=querystatus;}

    public String getFlag(){return flag;}

    public void setFlag(String flag){this.flag=flag;}

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getLikeStr() {
        return likeStr;
    }

    public void setLikeStr(String likeStr) {
        this.likeStr = likeStr;
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public String getMrtype() {
        return mrtype;
    }

    public void setMrtype(String mrtype) {
        this.mrtype = mrtype;
    }

    public List<String> getTagcode() {
        return tagcode;
    }

    public void setTagcode(List<String> tagcode) {
        this.tagcode = tagcode;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
}
