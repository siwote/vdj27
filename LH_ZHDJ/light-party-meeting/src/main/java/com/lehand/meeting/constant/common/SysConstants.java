package com.lehand.meeting.constant.common;


/**
 * 系统常量
 * @author taogang
 * @version 1.0
 * @date 2019年2月21日, 下午2:04:57
 */
public class SysConstants {
	
	public static interface SystemModule{
		final String horn_dev_member = "devmember5";
	}
	
	/**
	 * 基层党支部组织类型
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:07:25
	 */
	public static interface ZZLB {
		/** 党委 .*/
		final String DW = "6100000035";
		/** 党总支 .*/
		final String DZZ = "6200000036";
		/** 党支部 .*/
		final String DZB = "6300000037";
		/** 联合党支部 .*/
		final String LHDZB = "6400000038";
	}
	
	/**
	 * 党组织类别
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:10:07
	 */
	public static interface ORGTYPE {
		/** 支部 .*/
		final Integer DZB = 1;
		/** 总支.*/
		final Integer DZZ = 2;
		/** 党委 .*/
		final Integer DW = 3;
		/** 党小组 .*/
		final Integer DXZ = 4;
	}
	
	/**
	 * 是否可用 E 可用  D 不可用
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:07:00
	 */
	public static interface YnStr {
		/** 可用 .*/
		final String E = "E";
		/** 不可用 .*/
		final String D = "D";
	}
	
	/**
	 * 党员类别
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:35:21
	 */
	public static interface DYLB {
		/** 正式党员 .*/
//		final Integer ZSDY = 1;
		final String ZSDY = "1000000001";
		/** 预备党员 .*/
//		final Integer YBDY = 2;
		final String YBDY = "2000000002";
		/** 发展对象 .*/
//		final Integer FZDX = 3;
		final String FZDX =  "3000000003";
		/** 积极分子 .*/
//		final Integer JJFZ = 4;
		final String JJFZ = "4000000004";
		/** 申请人 .*/
//		final Integer SQR = 5;
		final String SQR = "5000000005";
	}
	
	/**
	 * 党员状态
	 * @author tangtao
	 * @date 2019年8月1日 
	 * @description 
	 */
	public static interface DYZT{
		
		/** 已跨省（系统）转出*/
		final String XTZC = "5000000001";
		
		/** 正常*/
		final String ZC = "1000000003";
		
		/** 已死亡*/
		final String YSW = "2000000004";
		
		/** 已出党*/
		final String YCD = "3000000005";
		
		/** 已停止党籍*/
		final String YTZDJ = "4000000006";
		
		/** 已删除*/
		final String YSC = "6000000006";
	}
	
	/**
	 * 组织关系转接
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月25日, 上午10:03:36
	 */
	public static interface ZZGX{
		/** 转入 .*/
		final Integer ZR = 2;
		/** 转出 .*/
		final Integer ZC = 1;
	}
	
	public static interface ORGID{
		final String orgId = "64BB6D7CEB6F4400B19990A9FB089AC6";
	}

	/**
	 * 党员性别
	 */
	public static interface DYXB{

		/**男*/
		final String MALE = "1000000003";

		/**女*/
		final String FEMALE = "2000000004";

		/**未说明的*/
		final String UNDEFIND = "9000000001";

		/**未知的*/
		final String UNKNOWN = "0000000002";
	}

	/**
	 *默认密码
	 */
	public static interface DEFAULTPWD {
		final String SIMPLESIX = "3Jvk6N9ujw1fACyEMQ4iqA==";
	}

	/**
	 * 用户证件类型
	 */
	public static interface IDTYPE{

		/**身份证*/
		final int ID_CARD = 0;

		/**护照*/
		final int PASSPROT = 1;

		/**组织机构*/
		final int ORG_CODE = 2;

	}

	/**
	 * 会议类型编码
	 */
	public static interface MTYPE{

		/** 组织生活总体*/
		final String OLM_CODE = "OLM_CODE";

		/** 三会一课*/
		final String TMOC = "TMOC";

		/** 支部委员会*/
		final String  BRANCH_COMMITTEE = "branch_committee";

		/** 党委会*/
		final String  COMMITTEE_MEETING = "committee_meeting";

		/**支部党员大会*/
		final String BRANCH_PARTY_CONGRESS = "branch_party_congress";

		/** 党小组会*/
		final String PARTY_GROUP_MEETING = "party_group_meeting";

		/**上党课 */
		final String PARTY_LECTURE = "party_lecture";

		/**组织生活会 */
		final String  ORG_LIFE_MEETING = "org_life_meeting";

		/**民主评议党员*/
		final String APPRAISAL_OF_MEMBERS = "appraisal_of_members";

		/** 主体党日*/
		final String  THEME_PARTY_DAY = "theme_party_day";

		/**党委中心组学习 */
		final String THEORETICAL_STUDY = "theoretical_study";

		/** 民主生活会*/
		final String DEMOCRATIC_LIFE_MEETING = "democratic_life_meeting";

		/** 谈心谈话*/
		final String MEETING_TALK = "meeting_talk";

		/** 工作汇报*/
		final String MEETING_REPORT = "meeting_report";

	}

	/**
	 * 会议状态
	 */
	public static interface  MSTATUS{

		/** 删除 */
		final int deleted = 0;

		/** 保存 */
		final int saved = 1;

		/** 提交 */
		final int submitted = 2;

		/** 撤销 */
		final int cancled = 3;
	}

	/**
	 * 会议达标状态
	 */
	public static interface MSTANDARD{

		/** 未达标 */
		final int UNREACHED = 0;

		/** 已达标 */
		final int REACHED = 1;

		/** 无要求*/
		final int NO_REQUIREMENT = 2;
	}

	/**
	 * 会议召开要求
	 */
	public static interface mrequirementtype{

		/** 每月 */
		final String permon = "permon";

		/** 每季度 */
		final String perseason = "perseason";
		
		/** 每半年 */
		final String perhalfyear = "perhalfyear";
		
		/** 每年 */
		final String perfyear = "perfyear";
	}


	/**
	 * 默认角色id
	 */
	public static interface DEFAULTROLEID{

		/** 党员*/
		final  long PARTY_MEMBER_ROLE = 10010;

		/** 党支部 */
		final  long PARTY_BRANCH_ROLE = 20010;

		/** 党总支 */
		final  long GENERAL_PARTY_BRANCH_ROLE = 20020;

		/** 党委 */
		final  long PARTY_COMMITTEE_ROLE = 20030;
	}

	/**
	 * 会议配置项
	 */
	public static class MEETING_CFG_CONSTANT {
	    /** 自动提交配置Key值 */
	    public static final String MEETING_AUTO_COMMIt_CFG = "MEETING_AUTO_COMMIt_CFG";
    }

	/**
	 * 消息提醒
	 */
	public static interface TIME_SET{

		/** 组织生活会*/
		final String ORG_LIFE_MEETING_TIME ="org_life_meeting_time";

		/** 民主评议党员 */
		final String APPRAISAL_OF_MEMBERS_TIME ="appraisal_of_members_time";

		/** 支部委员会*/
		final String BRANCH_COMMITTEE_TIME ="branch_committee_time";

		/**支部党员大会 */
		final String BRANCH_PARTY_CONGRESS_TIME ="branch_party_congress_time";

		/**民主生活会 */
		final String DEMOCRATIC_LIFE_MEETING_TIME ="democratic_life_meeting_time";

		/** 党委会*/
		final String COMMITTEE_MEETING_TIME ="committee_meeting_time";

		/** 党小组会*/
		final String PARTY_GROUP_MEETING_TIME ="party_group_meeting_time";

		/** 上党课*/
		final String PARTY_LECTURE_TIME ="party_lecture_time";

		/** 主体党日*/
		final String THEME_PARTY_DAY_TIME ="theme_party_day_time";

		/** 党委中心组学习*/
		final String THEORETICAL_STUDY_TIME ="theoretical_study_time";
	}
}
