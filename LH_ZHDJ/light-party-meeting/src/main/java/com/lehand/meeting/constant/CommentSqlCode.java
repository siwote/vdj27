package com.lehand.meeting.constant;

public class CommentSqlCode {

    /**
     * 根据会议类型id和会议id查询会议评论
     * select * from t_horn_comment where moduleid = :moduleid  and mdcode=:mdcode and compid = :compid order by createtime
     */
    public static String listHornComment = "listHornComment";

 /* `mdcode` varchar(30) NOT NULL COMMENT '模块编码见sys_module',
  `moduleid` bigint(20) NOT NULL COMMENT '模块ID',
  `userid` bigint(20) NOT NULL COMMENT '评论人ID',
  `username` varchar(300) NOT NULL COMMENT '评论人名称',
  `context` longtext COMMENT '评论内容',
  `orgid` bigint(20) DEFAULT NULL COMMENT '组织ID',
  `compid` bigint(20) NOT NULL COMMENT '租户ID',
  `sjtype` int(11) DEFAULT '0' COMMENT '评论主体类型(0:用户,1:机构/组/部门,2:角色)备注1',
  `repflag` int(11) DEFAULT '0' COMMENT '评论类型(0:文本,1:语音)',
  `orgcode` varchar(40) DEFAULT NULL COMMENT '组织编码',
  `orgname` varchar(100) DEFAULT NULL COMMENT '组织名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',*/

    /**
     * 新增会议评论
     * INSERT INTO t_horn_comment (mdcode,moduleid,userid,username,createtime,context,orgid,compid,sjtype,repflag,orgcode,orgname,status)
     * VALUES(:mdcode,:moduleid,:userid,:username,:createtime,:context,:orgid,:compid,:sjtype,:repflag,:orgcode,:orgname,:status)
     */
    public static String addHornComment="addHornComment";

    /**
     * 修改会议评论
     * update t_horn_comment set updatetime=:updatetime ,context=:context where id=:id and compid=:compid
     */
    public static String modifyHornComment="modifyHornComment";

    /**
     * 根据主键删除评论
     * delete from t_horn_comment where id=:id and compid=:compid
     */
    public static String deleteHornCommentById="deleteHornCommentById";

    /*  compid bigint NOT NULL COMMENT '租户ID',
               id bigint NOT NULL AUTO_INCREMENT COMMENT '自增长主键',
               moduleid bigint NOT NULL COMMENT '模块ID',
               replytime DATETIME NOT NULL COMMENT '回复时间',
               sjtype INT NOT NULL COMMENT '回复主体类型(0:用户,1:机构/组/部门,2:角色)',
               repflag INT NOT NULL COMMENT '回复类型(0:文本,1:语音)',
               reptext VARCHAR(5000) NOT NULL COMMENT '回复内容',
               remarks1 INT DEFAULT 0 COMMENT '备注1',
               remarks2 INT DEFAULT 0 COMMENT '备注2',
               remarks3 VARCHAR(50) COMMENT '备注3',
               remarks4 VARCHAR(50) COMMENT '备注3',
               repid bigint COMMENT '回复人',
               repname VARCHAR(100) COMMENT '回复人名称',
               commid bigint COMMENT '评论id',
               comuserid bigint COMMENT '原评论人ID',
               comusername VARCHAR(100) COMMENT '原评论人名称',
               status INT(2) COMMENT '状态',*/
    /**
     * 新增会议评论回复
     * INSERT INTO t_horn_comment_reply (compid,moduleid,replytime,sjtype,repflag,reptext,remarks1,remarks2,remarks3,remarks4,repid,repname,commid,comuserid,comusername,status)
     * VALUES (:compid,:moduleid,:replytime,:sjtype,:repflag,:reptext,:remarks1,:remarks2,:remarks3,:remarks4,:repid,:repname,:commid,:comuserid,:comusername,:status)
     */
    public static String addHornCommentReply="addHornCommentReply";


    /**
     * 修改会议评论回复
     * update t_horn_comment_reply set updatetime=:updatetime ,reptext=:reptext where id=:id and compid=:compid
     */
    public static String modifyHornCommentReply="modifyHornCommentReply";


    /**
     * 根据主键删除会议评论回复
     * delete from t_horn_comment_reply where id=:id and compid=:compid
     */
    public static String deleteHornCommentReplyById="deleteHornCommentReplyById";



    /**
     * 根据评论id查询会议评论回复
     * select * from t_horn_comment_reply where commid=:commid and compid = :compid order by replytime
     */
    public static String listHornCommentReply = "listHornCommentReply";


}
