package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingReportRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

@ApiModel(value="工作汇报视图",description="工作汇报视图")
public class MeetingReportRecordDto extends MeetingReportRecord {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="结束日期", name="endDate")
    private String endDate;

    @ApiModelProperty(value="开始日期", name="startDate")
    private String startDate;

    @ApiModelProperty(value="附件", name="meetingAttachList")
    private List<Map<String,Object>> meetingAttachList;

    @ApiModelProperty(value="上报人员", name="reportName")
    private String reportName;

    @ApiModelProperty(value="审核列表模糊查询", name="auditLike")
    private String auditLike;

    @ApiModelProperty(value="已经审核", name="alreadyAudit")
    private Integer alreadyAudit;

    @ApiModelProperty(value="关联下级code", name="codes")
    private String codes;

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public Integer getAlreadyAudit() {
        return alreadyAudit;
    }

    public void setAlreadyAudit(Integer alreadyAudit) {
        this.alreadyAudit = alreadyAudit;
    }

    public String getAuditLike() {
        return auditLike;
    }

    public void setAuditLike(String auditLike) {
        this.auditLike = auditLike;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public List<Map<String, Object>> getMeetingAttachList() {
        return meetingAttachList;
    }

    public void setMeetingAttachList(List<Map<String, Object>> meetingAttachList) {
        this.meetingAttachList = meetingAttachList;
    }
}
