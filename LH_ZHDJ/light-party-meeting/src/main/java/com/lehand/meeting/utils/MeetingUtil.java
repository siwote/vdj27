package com.lehand.meeting.utils;

import com.alibaba.fastjson.JSON;
import com.lehand.base.constant.DateEnum;
import com.lehand.meeting.pojo.MeetingThemeDate;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MeetingUtil {

    /**
     * 通过日期获取年份
     * @param date
     * @return
     */
    public static int getYear(String date) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(DateEnum.parseStr(date, "yyyy-MM-dd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 通过日期获取月份
     * @param date
     * @return
     */
    public static int getMonth(String date) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(DateEnum.parseStr(date, "yyyy-MM-dd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获取党员活动日一段时间内的日期
     * @return
     */
    public static List<String> getDayList(String startDate, String endDate, int day) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dateList = new ArrayList<>();
        if(StringUtils.isEmpty(startDate) || StringUtils.isEmpty(endDate)) {
            return dateList;
        }
        //开始日期
        Calendar startCal = Calendar.getInstance();
        try {
            startCal.setTime(DateEnum.parseStr(startDate, "yyyy-MM-dd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //结束日期
        Calendar endCal = Calendar.getInstance();
        try {
            endCal.setTime(DateEnum.parseStr(endDate, "yyyy-MM-dd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //开始处理
        while(startCal.getTime().before(endCal.getTime())) {
            startCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
            if(startCal.get(Calendar.DAY_OF_MONTH) >= day) {
                startCal.set(Calendar.DAY_OF_MONTH, day);
            }
            dateList.add(sdf.format(startCal.getTime()));
            startCal.add(Calendar.MONTH, 1); //设置日期
        }
        return dateList;
    }

//    public static void main(String[] args) throws ParseException {
//        Calendar calendar = Calendar.getInstance();
//        Date d = DateEnum.parseStr("2020-12-21", "yyyy-MM-dd");
//        calendar.setTime(new Date());
//        System.out.println(calendar.get(Calendar.YEAR));
//        System.out.println(calendar.get(Calendar.MONTH) + 1);
//    }

}
