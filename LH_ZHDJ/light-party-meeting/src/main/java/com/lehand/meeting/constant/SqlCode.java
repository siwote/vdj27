package com.lehand.meeting.constant;

public class SqlCode {

    /********************************  党员/党组织基本信息统计 ************************************/

    /**
     * 根据组织机构id 获取信息 树
     * select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryBaseDzzTree = "queryBaseDzzTree";

    /**
     * 通过编码获取组织机构简称
     *select remarks6,orgname from pw_organ where compid = ? and orgcode = ?
     */
    public static final String findJCByCode = "findJCByCode";

    /**
     * 班子成员数量
     * select count(*) from t_dzz_bzcyxx a inner join  (select max(rzdate) rzdate,userid ,organid from t_dzz_bzcyxx where  organid = ? group by userid) b
     * on a.rzdate = b.rzdate and a.userid = b.userid and a.organid = b.organid where  a.zfbz = ?
     */
//    public static final String queryHornBZCYCount = "queryHornBZCYCount";
    public static final String queryHornBZCYCount = "select count(*) from t_dzz_bzcyxx a inner join  (select max(rzdate) rzdate,userid ,organid from t_dzz_bzcyxx where  organid = ? group by userid) b on a.rzdate = b.rzdate and a.userid = b.userid and a.organid = b.organid where  a.zfbz = ?";

    /**
     * select b.status,count(1) as value from t_dzz_info_simple a inner join party_dues b on a.organid = b.org_id
     * where a.operatetype != 3 and a.? = ? and year = ? and month = ? group by b.status
     */
    public static final String queryDFInfo = "queryDFInfo";


    /***********************  组织机构补充表业务层  ************************/

    /**
     * select * from t_dzz_info_bc where organid = ?
     */
    public static final String getDzzsjByIdMap = "getDzzsjByIdMap";

    /**
     * 根据编码获取组织机构信息
     * select * from t_dzz_info_simple where organcode = ?
     */
    public static final String getDzzsjByCode = "getDzzsjByCode";

    /**
     * 通过编码获取组织机构简称
     * select remarks6,orgname from pw_organ where compid = ? and orgcode = ?
     */
    public static final String queryFindJCByCode = "queryFindJCByCode";


    /***********************  组织机构业务层  ************************/

    /**
     * 根据组织机构code 获取信息 树
     * select * from t_dzz_info_simple where organcode = :organid and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimple = "queryTdzzInfoSimple";

    /**
     * 根据组织机构id 获取信息 树
     * select * from t_dzz_info_simple where organid = :organid and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimple2 = "queryTdzzInfoSimple2";

    /**
     * 根据idpath查询所有的组织信息
     * select * from t_dzz_info_simple where id_path like '%${PARAMS.id_path }%' and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimpleByIdpath = "queryTdzzInfoSimpleByIdpath";

    /**
     * 根据idpath查询所有的组织信息
     * select * from t_dzz_info_simple where id_path like '%${PARAMS.id_path }%' and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     * and dzzmc like ?
     * order by organcode asc,organtype desc,ordernum asc
     *
     */
    public static final String queryTdzzInfoSimpleByIdpath2 = "queryTdzzInfoSimpleByIdpath2";

    /**
     * select * from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimpleById = "queryTdzzInfoSimpleById";

    /**
     * 模糊搜索组织机构
     * select * from t_dzz_info_simple where operatetype !=3 and dzzmc like ? and id_path like ?  and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimpleLikestr = "queryTdzzInfoSimpleLikestr";

    /**
     * 根据组织结构获取总条数
     * select count(1) from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'
     */
    public static final String queryTdzzInfoSimpleByIdcount = "queryTdzzInfoSimpleByIdcount";


    /**
     * 基础信息查询
     * select distinct a.organid,a.organcode,a.dzzmc,a.zzlb,a.dzzlsgx,d.dzzmc as pdzzmc,b.dzzszdwqk,b.xzqh,f.dwname,f.dwxzlb from t_dzz_info_simple a inner join t_dzz_info b on a.organid = b.organid left join t_dzz_info_simple d on d.organid = a.parentid left join t_dw_info f on b.szdwid = f.uuid where a.operatetype != 3 and a.organid = ?
     */
    public static final String queryTdzzInfoSimpleDzzBase = "queryTdzzInfoSimpleDzzBase";

    /**
     * 单位信息
     * select distinct c.uuid,a.organid,c.dwname,c.dwxzlb,d.dwlsgx,c.dwjldjczzqk,d.isfrdw,d.dwfzr,d.fzrsfsdy,c.dwcode,d.jjlx,d.qygm,d.sfmykjqy,d.zgzgs,d.zgzgzgrs,d.zgzyjsrys,d.sfjygh, d.sfzjzz from t_dzz_info_simple a left join t_dw_info c on a.szdwid = c.uuid left join t_dw_info_bc d on d.uuid = c.uuid where a.operatetype != 3 and a.organid = ?
     */
    public static final String queryTdzzInfoSimpleDwinfoById = "queryTdzzInfoSimpleDwinfoById";

    /**
     * 查询党组织换届信息
     * select * from t_dzz_hjxx_dzz where organid = ?
     */
    public static final String queryTdzzInfoSimpleListById = "queryTdzzInfoSimpleListById";

    /**
     * 查询班子成员
     * select s.uuid,s.organid,s.userid,s.rzdate, s.username xm,zjhm,s.xb,s.csrq,s.xl,s.dnzwname,s.dnzwsm, s.zwlevel,s.lzdate,s.ordernum from t_dzz_bzcyxx s
     * inner join (select max(rzdate) rzdate,userid,organid from t_dzz_bzcyxx where organid = ? group by userid ) t
     * on  s.rzdate = t.rzdate and s.userid = t.userid and s.organid = t.organid where  s.zfbz  = 0  order by ordernum asc
     */
    public static final String queryTdzzInfoSimpleBZCYList = "queryTdzzInfoSimpleBZCYList";


    /**
     * 奖惩查询
     * select * from t_dzz_jcxx_dzz where organid = ? order by jcdate desc
     */
    public static final String queryTdzzInfoSimpleJCXXList = "queryTdzzInfoSimpleJCXXList";


    /**
     * 查询组织自身和当前层级的组织的列表页
     *select * from t_dzz_info_simple where (organid = :organid or parentid = :organid)
     *
     * and dzzmc like ?
     * order by organcode asc,organtype desc,ordernum asc
     */
    public static final String hornOrgTableList = "hornOrgTableList";

    /**
     * 数据字典项查询
     * select * from g_dict_item
     */
    public static final String queryDcitParams = "queryDcitParams";

    /**
     * 从视图查询包含下级的党组织信息
     *select organid,parentid,organcode,parentcode,ifnull(dzzmc,'') as dzzmc ,ifnull(zzlb,'') as zzlb,ifnull(dzzlsgx,'') as dzzlsgx,ifnull(xzqh,'') as xzqh,ifnull(szdwid,'') as szdwid,ifnull(operatetype,'') as operatetype,ifnull(organtype,'') as organtype,ifnull(ordernum,'') as ordernum,ifnull(lxrname,'') as lxrname,ifnull(lxrphone,'') as lxrphone,ifnull(fax,'') as fax,ifnull(dzzjlrq,'') as dzzjlrq,ifnull(address,'') as address,ifnull(dzzsj,'') as dzzsj,id_path from view_dzz_info where id_path like :idPath and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'
     * and dzzmc like '%${PARAMS.likeStr }%'
     * order by organcode asc,organtype desc,ordernum asc
     */
    public static final String queryTdzzInfoOrgView = "queryTdzzInfoOrgView";

    /**
     *从视图查询直辖党组织
     * select organid,parentid,organcode,parentcode,ifnull(dzzmc,'') as dzzmc ,ifnull(zzlb,'') as zzlb,ifnull(dzzlsgx,'') as dzzlsgx,ifnull(xzqh,'') as xzqh,ifnull(szdwid,'') as szdwid,ifnull(operatetype,'') as operatetype,ifnull(organtype,'') as organtype,ifnull(ordernum,'') as ordernum,ifnull(lxrname,'') as lxrname,ifnull(lxrphone,'') as lxrphone,ifnull(fax,'') as fax,ifnull(dzzjlrq,'') as dzzjlrq,ifnull(address,'') as address,ifnull(dzzsj,'') as dzzsj,id_path from view_dzz_info where (organid = :organid or parentid = :organid)
     *  and dzzmc like '%${PARAMS.likeStr }%'
     *  order by organcode asc,organtype desc,ordernum asc
     */
    public static final String queryHornOrgView = "queryHornOrgView";

    /**
     * 党组织信息表
     * INSERT INTO t_dzz_info(organid, parentid, organcode, parentcode, dzzmc, zzlb, dzzlsgx, postcode, fax, address, lxrname, lxrphone, maindwid, szdwid, szdwlb, organtype, deltime, ordernum, creator, updatetime, operorganid, xxwzd, schemaname, delflag, datasources, updator, createtime, dzzjc, operatorname, dzzsj, isfunctional, dzzszdwqk, operatetype, provincecode, xzqh, electdate, expirationdate, gddh, sfxxwz, treecodepath, sumwzd, syncstatus, synctime)
     * VALUES (:organid, :parentid, :organcode, :parentcode, :dzzmc, :zzlb, :dzzlsgx, :postcode, :fax, :address, :lxrname, :lxrphone, :maindwid, :szdwid, :szdwlb, :organtype, :deltime, :ordernum, :creator, :updatetime, :operorganid, :xxwzd, :schemaname, :delflag,:datasources, :updator, :createtime, :dzzjc, :operatorname, :dzzsj, :isfunctional, :dzzszdwqk, :operatetype, :provincecode, :xzqh, :electdate, :expirationdate, :gddh, :sfxxwz, :treecodepath, :sumwzd, :syncstatus, :synctime)
     */
    public static final String saveDZZInfo = "saveDZZInfo";

    /**
     *党组织通用表
     *INSERT INTO t_dzz_info_simple(organid, parentid, organcode, parentcode, dzzmc, zzlb, delflag, maindwid, xxwzd, szdwid, organtype, deltime, datasources, schemaname, ordernum, updatetime, dzzlsgx, szdwlb, dzzjc, dzzsj, path1, path2, path3, path4, path5, path6, path7, path8, path9, path10, id_path, insert_date, isfunctional, dzzszdwqk, operatetype, provincecode, xzqh, sfxxwz, treecode, treecodepath)
     * VALUES (:organid, :parentid, :organcode, :parentcode, :dzzmc, :zzlb, :delflag, :maindwid, :xxwzd, :szdwid, :organtype, :deltime, :datasources, :schemaname, :ordernum, :updatetime, :dzzlsgx, :szdwlb, :dzzjc, :dzzsj, :path1, :path2, :path3, :path4, :path5, :path6, :path7, :path8, :path9, :path10, :id_path, :insert_date, :isfunctional, :dzzszdwqk, :operatetype, :provincecode, :xzqh, :sfxxwz, :treecode, :treecodepath);
     */
    public static final String saveDzzsimple = "saveDzzsimple";

    /**
     * 单位信息表
     *INSERT INTO t_dw_info(uuid, dwname, dwxzlb, dwjldjczzqk, isfrdw, dwcode, organid, xxwzd, dworder, datasources, operatetype, createtime, creator, updatetime, updator, gldworganid, treecodepath, sfxxwz, syncstatus, synctime, zfbz)
     * VALUES (:uuid, :dwname, :dwxzlb, :dwjldjczzqk, :isfrdw, :dwcode, :organid, :xxwzd, :dworder, :datasources, :operatetype, :createtime, :creator, :updatetime, :updator, :gldworganid, :treecodepath, :sfxxwz, :syncstatus, :synctime, :zfbz);
     */
    public static final String saveDwInfo = "saveDwInfo";


    /**
     * 党组织扩展表
     *INSERT INTO t_dzz_info_bc(organid, dzzsj, postcode, fax, address, lxrname, lxrphone, dzzjc, dzzsjzjhm, dzzjlrq, kzpyrq, kzdqpybs, cjpydys, kzpthydws, cjptdys, gddh, electdate, expirationdate, bjbzcsfs, sfxxwz, syncstatus, synctime)
     * VALUES (:organid, :dzzsj, :postcode, :fax, :address, :lxrname, :lxrphone, :dzzjc, :dzzsjzjhm, :dzzjlrq, :kzpyrq, :kzdqpybs, :cjpydys, :kzpthydws, :cjptdys, :gddh, :electdate, :expirationdate, :bjbzcsfs, :sfxxwz, :syncstatus, :synctime);
     */
    public static final String saveDzzInfoBc = "saveDzzInfoBc";

    /**
     * 根据组织编码查询信息
     *select * from t_dzz_info where organcode = :organcode
     */
    public static final String queryDzzInfoByOrgancode = "queryDzzInfoByOrgancode";


    /**
     * 根据组织编码查询信息
     *select * from t_dzz_info_simple where organcode = :organcode
     */
    public static final String queryDzzInfoSimpleByOrgancode = "queryDzzInfoSimpleByOrgancode";

    /**
     * 更新党组织信息表
     * update t_dzz_info set dzzmc = :dzzmc,zzlb=:zzlb,dzzlsgx=:dzzlsgx,dzzsj=:dzzsj,lxrname=:lxrname,lxrphone=:lxrphone,xzqh=:xzqh,dzzszdwqk=:dzzszdwqk,szdwid=:szdwid,szdwlb=:szdwlb  where  organcode = :organcode
     */
    public static final String updateDzzInfoByOrgancode = "updateDzzInfoByOrgancode";


    /**
     * 更新党组织扩展表
     * update t_dzz_info_bc set dzzsj = :dzzsj,lxrname=:lxrname,lxrphone=:lxrphone,dzzjlrq=:dzzjlrq  where organid = :organid
     */
    public static final String updateDzzInfoBc = "updateDzzInfoBc";


    /**
     * 根据组织ID查询党组织扩展表信息
     * select * from t_dzz_info_bc where organid = :organid
     */
    public static final String queryDzzInfoBcByOranid = "queryDzzInfoBcByOranid";

    /**
     * 更新党组织基本信息路径等
     * update t_dzz_info_simple set dzzmc=:dzzmc,zzlb=:zzlb,dzzlsgx=:dzzlsgx,dzzsj=:dzzsj,path1=:path1,path2=:path2,path3=:path3,path4=:path4,path5=:path5,path6=:path6,path7=:path7,path8=:path8,path9=:path9,path10=:path10 where organcode = :organcode
     */
    public static final String updateDzzInfoSimple = "updateDzzInfoSimple";


    /**
     * select count(*) from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static String countTdzzInfoSimpleByPid = "countTdzzInfoSimpleByPid";

    /**
     * select organid from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static String getOrgidsByPid = "getOrgidsByPid";

    /**
     * 保存党建组织用户信息
     * insert into urc_organization(orgname,sorgname,porgid,orgseqno,compid,orgtypeid,orgcode,type,status,orglevel,orgpath,createuserid,createtime)
     * values(:orgname,:sorgname,:porgid,:orgseqno,:compid,:orgtypeid,:orgcode,:type,:status,:orglevel,:orgpath,:createuserid,:createtime)
     */
    public static final String saveHornOrganization = "saveHornOrganization";

    /**
     * 根据组织编码删除党组织信息
     * delete from t_dzz_info where organcode = :organcode
     */
    public static final String delDzzInfoByCode = "delDzzInfoByCode";

    /**
     * 根据组织编码删除党组织信息表
     * delete from t_dzz_info_simple where organcode = :organcode
     */
    public static final String delDzzInfoSimpleByCode = "delDzzInfoSimpleByCode";

    /**
     * 根据系统组织查询上级组织信息
     * select * from urc_organization where orgcode = :organcode
     */
    public static final String queryPorgcode = "queryPorgcode";

    /**
     * 根据组织机构ID删除组织信息
     * delete from urc_organization where orgcode = :organcode
     */
    public static final String delOrganByOrgancode = "delOrganByOrgancode";


    /*********************************************************************** 党员导入********************************************************************/

    /**
     *根据身份证号查询用户数
     * select count(*) from urc_user where idno = ? and  idtype = ?
     */
    public static String countUrcuserByIdno = "countUrcuserByIdno";

    /**
     * 通过身份证号更新用户信息
     * update urc_user set username = ?,sex = ?,phone = ? where idtype = 0 and compid = ? and idno = ?
     */
    public static String updateUrcuserByIdno = "updateUrcuserByIdno";

    /**
     *  新增账户用户
     * insert into urc_user set  username = :username,sex = :sex,phone = :phone,idtype = 0,idno = :idno,compid = :compid,postname = '',wechat = '',landlines = '',useraddr = '',email=''
     */
    public static String insertUrcuser = "insertUrcuser";

    /**
     *  新增账号
     * insert into urc_user_account set userid = ? ,account = ? , passwd = ?,acttype = 0,compid = ?
     */
    public static String insertAccount = "insertAccount";

    /**
     * 根据账号查询用户信息
     * select * from urc_user where idno ?
     */
    public static String queryUrcUserByIdno = "queryUrcUserByIdno";

    /**
     * 根据党组织编码更新用户信息
     * update urc_user set username = ?,sex = ?,phone = ? where idtype = ? and compid = ? and idno = ?
     */
    public static String updateUrcuserByOrgcode = "updateUrcuserByOrgcode";

    /**
     * 根据账户ID删除账户信息
     * delete from urc_user_account where account = ?
     */
    public static String delUaccountByAccount = "delUaccountByAccount";

    /**
     * 新增urc_user_org_map表(用户和组织的关系)
     * INSERT INTO urc_user_org_map (userid, orgid, seqno, compid) VALUES (:userid,:orgid,:seqno,:compid)
     */
    public static String addUserOrgMap = "addUserOrgMap";

    /**
     * 删除urc_user_org_map表(用户和组织的关系)
     * DELETE FROM urc_user_org_map WHERE userid = ? and compid = ?
     */
    public static String removeUserOrgMapByUserid = "removeUserOrgMapByUserid";

    /**
     * 查询系统参数
     * SELECT * FROM sys_param WHERE compid=? and paramkey=?
     */
    public static String getSysParam2 = "getSysParam2";

    /**
     * 根据主体删除角色关联
     * DELETE FROM urc_role_map WHERE sjtype=? and sjid=? and compid=?
     */
    public static String removeRoleMapBySubject = "removeRoleMapBySubject";

    /**
     * 新增角色关系
     * INSERT INTO urc_role_map (sjtype, sjid, roleid, compid,orgid) VALUES (:sjtype,:sjid,:roleid,:compid,:orgid)
     */
    public static String addRoleMap = "addRoleMap";

    /**
     * SELECT * FROM meeting_record WHERE compid=? and createuserid=? and mrtype=? and left(starttime,10)=?
     */
    public static String listMeetingRecord1 = "listMeetingRecord1";

    /**
     * SELECT * FROM meeting_record WHERE compid=? and createuserid=? and mrtype=? and left(starttime,7)=?
     */
    public static String listMeetingRecord2 = "listMeetingRecord2";

    /**
     * SELECT * FROM meeting_record WHERE compid=? and createuserid=? and mrtype=? and left(starttime,10)>=? and left(starttime,10)<=?
     */
    public static String listMeetingRecord3 = "listMeetingRecord3";

    /**
     *党员数(党员党组织模块)
     */
    public final static String getMemberCountByOrganid = "getMemberCountByOrganid";

    /**
     * 班子成员数(党员党组织模块)
     */
    public final static String getBzcyCountByOrganid ="getBzcyCountByOrganid";

    /**
     * 换届信息(党员党组织模块)
     */
    public final static String getLatestHjxxByOrganid = "getLatestHjxxByOrganid";

    /**
     * 根据职务类别获取党员姓名
     * select username from t_dzz_bzcyxx where organid = ? and dnzwname = ? and zfbz = 0 order by ordernum asc
     */
    public final static String getBzcyNamesByPost = "getBzcyNamesByPost";


    /**
     * 查询用户openId
     * select remarks3 from urc_user where userid = ?
     */
    public final static String getQueryOpenId = "getQueryOpenId";

    /**
     * 查询用户user
     * select * from urc_user where  remarks3 = ?
     */
    public final static String getUrcUserByOpenId = "getUrcUserByOpenId";


    /**
     * 获取用户登陆信息
     * SELECT
     * 	*
     * FROM
     * 	urc_user_account
     * 	where
     * 	account = ?
     * 	AND passwd = ?;
     */
    public static String getUserInfo = "getUserInfo";

    /**
     * 更新用户openId
     * update urc_user set remarks3 = ? where userid = ?
     */
    public static String ModifyUrcUserOpenId = "ModifyUrcUserOpenId";


    /**
     *根据身份证获取手机号码
     * select lxdh as phone from t_dy_info where zjhm = ?
     */
    public static String getPhoneByIdCard = "getPhoneByIdCard";


    /**
     * 新增短息发送
     * insert into sms_send (compid, usermark, phone, status, content, createtime, delflag) VALUES (:compid, :usermark,:phone,:status,:content,:createtime, :delflag)
     *
     */
    public static String addSmsSend = "addSmsSend";

    /**
     * 根据身份证号获取用户userId
     * select userid from t_dy_info where zjhm = ?
     */
    public static String getUserIdByIdCard = "getUserIdByIdCard";

    /**
     *根据组织id和党员类别查询
     * select * from t_dy_info where dylb = ? and organid = ? and delflag!=1
     */
    public static final String getTDyInfoList = "getTDyInfoList";
    /**
     * 民主评议党员添加党员综合情况
     *INSERT INTO meeting_democracy_review (mrid, userid) VALUES (:mrid, :userid);
     */
    public static String insertDemocracyReview="insertDemocracyReview";
    /**
     * 民主评议党员根据会议id和uesrid修改综合情况
     *UPDATE meeting_democracy_review SET situation=:situation WHERE mrid=:mrid AND userid=:userid
     */
    public static String modifySituationByMridUserid="modifySituationByMridUserid";
    /**
     * 民主评议党员根据会议id修改综合情况
     *UPDATE meeting_democracy_review SET submit=:submit WHERE mrid=:mrid
     */
    public static String modifySubmitByMrid="modifySubmitByMrid";
    /**
     * 分页获取民主评议党员，党员综合情况
     * SELECT * FROM meeting_democracy_review WHERE mrid=:mrid
     */
    public static String pageDemocracyReview="pageDemocracyReview";
    /**
     *根据组织编码获取所有未删除党员和预备党员
     * SELECT a.* FROM t_dy_info a, t_dzz_info b WHERE a.organid = b.organid AND ( a.dylb = '1000000001' OR a.dylb = '2000000002' ) AND (a.delflag != 1 OR a.delflag IS NULL)  AND b.organcode =?
     */
    public static String getAllTDyInfoByOrgcode="getAllTDyInfoByOrgcode";
}
