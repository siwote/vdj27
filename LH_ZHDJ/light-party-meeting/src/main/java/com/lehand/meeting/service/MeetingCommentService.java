package com.lehand.meeting.service;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.CommentSqlCode;
import com.lehand.meeting.dto.HornCommentDto;
import com.lehand.meeting.pojo.HornComment;
import com.lehand.meeting.pojo.HornCommentReply;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会议评论服务类
 */
@Service
public class MeetingCommentService {
    @Resource
    GeneralSqlComponent generalSqlComponent;

    /**
     * 会议评论查询
     * @param moduleid
     * @param mdcode
     * @return
     */
    public List<HornComment> listHornComment(Long moduleid, Long mdcode, Session session) {
        Map map=new HashMap<>();
        map.put("moduleid",moduleid);
        map.put("mdcode",mdcode);
        map.put("compid",session.getCompid());
        return generalSqlComponent.query(CommentSqlCode.listHornComment,map);
    }

    /**
     * 新增会议评论
     * @param hornComment
     * @param session
     * @return
     */
    public Integer addHornComment(HornComment hornComment, Session session) {
        hornComment.setCompid(session.getCompid());
        hornComment.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        hornComment.setOrgcode(session.getCurrentOrg().getOrgcode());
        hornComment.setOrgid(session.getCurrentOrg().getOrgid());
        hornComment.setOrgname(session.getCurrentOrg().getOrgname());
        hornComment.setStatus(0);
        hornComment.setUserid(session.getUserid());
        hornComment.setUsername(session.getUsername());
        return generalSqlComponent.insert(CommentSqlCode.addHornComment,hornComment);
    }

    /**
     * 修改会议评论
     * @param hornComment
     * @param session
     * @return
     */
    public Integer modifyHornComment(HornComment hornComment, Session session) {
        if (null==hornComment.getId()){
            LehandException.throwException("id不能为空");
        }
        hornComment.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        hornComment.setCompid(session.getCompid());
        return generalSqlComponent.update(CommentSqlCode.modifyHornComment,hornComment);

    }

    /**
     *
     * @param id
     * @param session
     * @return
     */
    public Integer removeHornComment(Long id, Session session) {
        Map map=new HashMap();
        map.put("id",id);
        map.put("compid",session.getCompid());
        return generalSqlComponent.delete(CommentSqlCode.deleteHornCommentById,map);
    }

    /**
     * 保存会议评论回复
     * @param hornCommentReply
     * @param session
     * @return
     */
    public Integer addHornCommentReply(HornCommentReply hornCommentReply, Session session) {
        hornCommentReply.setCommid(session.getCompid());
        hornCommentReply.setRepflag(0);
        hornCommentReply.setReplytime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        hornCommentReply.setSjtype(0);
        hornCommentReply.setRepid(session.getUserid());
        hornCommentReply.setRepname(session.getUsername());
        hornCommentReply.setStatus(0);
        return generalSqlComponent.insert(CommentSqlCode.addHornCommentReply,hornCommentReply);
    }

    /**
     * 根据主键修改会议评论回复
     * @param hornCommentReply
     * @param session
     * @return
     */
    public Integer modifyHornCommentReply(HornCommentReply hornCommentReply, Session session) {
        if (null==hornCommentReply.getId()){
            LehandException.throwException("id不能为空");
        }
        hornCommentReply.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        hornCommentReply.setCompid(session.getCompid());
        return generalSqlComponent.update(CommentSqlCode.modifyHornCommentReply,hornCommentReply);
    }

    /**
     * 删除会议评论回复
     * @param id
     * @param session
     * @return
     */
    public Integer removeHornCommentReply(Long id, Session session) {
        Map map=new HashMap();
        map.put("id",id);
        map.put("compid",session.getCompid());
        return generalSqlComponent.delete(CommentSqlCode.deleteHornCommentReplyById,map);
    }

    /**
     * 查询评论和回复
     * @param moduleid
     * @param mdcode
     * @param session
     * @return
     */
    public List<HornCommentDto>  listHornCommentAndReply(Long moduleid, Long mdcode, Session session) {
        Map map=new HashMap<>();
        map.put("moduleid",moduleid);
        map.put("mdcode",mdcode);
        map.put("compid",session.getCompid());
        List<HornCommentDto> listDto= generalSqlComponent.query(CommentSqlCode.listHornComment,map);
        //评论为空判断
        if (null!=listDto){
            for (HornCommentDto hornCommentDto : listDto) {
                map.put("commid",hornCommentDto.getId());
                List<HornCommentReply>  replies=generalSqlComponent.query(CommentSqlCode.listHornCommentReply,map);
                //回复为空判断
                if (null!=replies){
                    hornCommentDto.setList(replies);
                }
            }
        }

        return listDto;
    }
}
