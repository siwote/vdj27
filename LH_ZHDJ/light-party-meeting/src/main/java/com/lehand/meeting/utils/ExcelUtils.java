package com.lehand.meeting.utils;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ExcelUtils {


	/**
	 * 创建行元素
	 * @param style    样式
	 * @param height   行高
	 * @param value    行显示的内容
	 * @param row1     起始行
	 * @param row2     结束行
	 * @param col1     起始列
	 * @param col2     结束列
	 */
	private static void createRow( HSSFSheet sheet ,HSSFCellStyle style, int height, String value, int row1, int row2, int col1, int col2){
	    if(col2 > 0) {
            sheet.addMergedRegion(new CellRangeAddress(row1, row2, col1, col2));  //设置从第row1行合并到第row2行，第col1列合并到col2列
        }
		HSSFRow rows = sheet.createRow(row1);        //设置第几行
		rows.setHeight((short) height);              //设置行高
		HSSFCell cell = rows.createCell(col1);       //设置内容开始的列
		cell.setCellStyle(style);                    //设置样式
		cell.setCellValue(value);                    //设置该行的值
	}

	/**
	 * 创建样式
	 * @param fontSize   字体大小
	 * @param align  水平位置  左右居中2 居右3 默认居左 垂直均为居中
	 * @param bold   是否加粗
	 * @param border  是否需要边框
	 * @param color
	 * @return
	 */
	private static HSSFCellStyle getStyle(HSSFWorkbook workbook,int fontSize,int align,boolean bold,boolean border,boolean color){
		HSSFFont font = workbook.createFont();
		font.setFontName("宋体");
		font.setFontHeightInPoints((short) fontSize);// 字体大小
		font.setBold(bold);
//		if (bold){
//			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
//		}
		if (color){
			font.setColor(HSSFFont.COLOR_RED);
		}
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);                         //设置字体
//		style.setAlignment(HSSFCellStyle.ALIGN_LEFT);      // 左右居中2 居右3 默认居左
//		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 上下居中1
//		if (border){
//			style.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
//			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
//			style.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
//			style.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
////			style.setBorderRight((short) 2);
////			style.setBorderLeft((short) 2);
////			style.setBorderBottom((short) 2);
////			style.setBorderTop((short) 2);
//			style.setLocked(true);
//		}
		return style;
	}

	/**
	 * 创建excel输出流
	 * @param response   HttpServletResponse
	 * @param tableName 表名
	 * @param tablesStr  表头数组
	 * @param mapList     筛选后的数据
	 */
	public static void write(HttpServletResponse response, String[] tablesStr, List<Map<String,String>> mapList, String tableName){
		try {
			//声明一个工作簿
			HSSFWorkbook workbook = new HSSFWorkbook();
			//生成一个表格，设置表格名称为"逾期反馈表"
			HSSFSheet sheet = workbook.createSheet(tableName);
			//设置表格列宽度为10个字节
			sheet.setDefaultColumnWidth(30);
			sheet.setColumnWidth(0,10000);
			// 表头标题
			HSSFCellStyle titleStyle = getStyle(workbook,22,2,true,true,true);//样式
			createRow(sheet,titleStyle,1000,tableName,0,0,0,tablesStr.length-1);

			//创建第一行表头
			HSSFRow headrow = sheet.createRow(1);
			//遍历添加表头
			for (int i = 0; i < tablesStr.length; i++) {
				//创建一个单元格
				HSSFCell cell = headrow.createCell(i);
				//创建一个内容对象
				HSSFRichTextString text = new HSSFRichTextString(tablesStr[i]);
				//将内容对象的文字内容写入到单元格中并设置单元格格式
				cell.setCellStyle(getStyle(workbook,15,2,true,true,false));
				cell.setCellValue(text);
			}
			int rownum = 2;
            HSSFCellStyle style = getStyle(workbook,12,rownum==2?1:2,false,true,false);
			for (Map<String, String> map : mapList) {
				HSSFRow row = sheet.createRow(rownum);
				for (int i = 0; i < tablesStr.length; i++) {
					HSSFCell cell = row.createCell(i);
					HSSFRichTextString text = new HSSFRichTextString( map.get(tablesStr[i]));
					cell.setCellStyle(style);
					cell.setCellValue(text);
				}
				rownum++;
			}
			//准备将Excel的输出流通过response输出到页面下载
			//八进制输出流
			response.setContentType("application/octet-stream");
			//这后面可以设置导出Excel的名称
			response.setHeader("Content-disposition", String.format("attachment;filename=%s%s.xls",URLEncoder.encode(tableName,"UTF-8"), DateEnum.YYYYMMDDHHMMDD.format()));
			response.flushBuffer();
			workbook.write(response.getOutputStream());
		} catch ( IOException e) {
			LehandException.throwException("导出异常", e);
		}
	}

    /**
     * 导出文件到本地
     * @param tablesStr
     * @param mapList
     * @param tableName
     * @param path
     * @return
     */
	public static String write(String[] tablesStr, List<Map<String,String>> mapList, String tableName, String path){
        File efile = null;
	    FileOutputStream fos = null;
	    try {
			//声明一个工作簿
			HSSFWorkbook workbook = new HSSFWorkbook();
			//生成一个表格，设置表格名称为"逾期反馈表"
			HSSFSheet sheet = workbook.createSheet(tableName);
			//设置表格列宽度为10个字节
			sheet.setDefaultColumnWidth(30);
			sheet.setColumnWidth(0,10000);
			// 表头标题
			HSSFCellStyle titleStyle = getStyle(workbook,22,2,true,true,true);//样式
			createRow(sheet,titleStyle,1000,tableName,0,0,0,tablesStr.length-1);

			//创建第一行表头
			HSSFRow headrow = sheet.createRow(1);
			//遍历添加表头
			for (int i = 0; i < tablesStr.length; i++) {
				//创建一个单元格
				HSSFCell cell = headrow.createCell(i);
				//创建一个内容对象
				HSSFRichTextString text = new HSSFRichTextString(tablesStr[i]);
				//将内容对象的文字内容写入到单元格中并设置单元格格式
				cell.setCellStyle(getStyle(workbook,15,2,true,true,false));
				cell.setCellValue(text);
			}
			int rownum = 2;
			for (Map<String, String> map : mapList) {
				HSSFRow row = sheet.createRow(rownum);
				for (int i = 0; i < tablesStr.length; i++) {
					HSSFCell cell = row.createCell(i);
					HSSFRichTextString text = new HSSFRichTextString( map.get(tablesStr[i]));
					cell.setCellStyle(getStyle(workbook,12,rownum==2?1:2,false,true,false));
					cell.setCellValue(text);

					//设置显示样式
                    HSSFCellStyle cellStyle = workbook.createCellStyle();
//                    cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
//                    cellStyle.setVerticalAlignment(HSSFCellStyle.ALIGN_CENTER);

                    cell.setCellStyle(cellStyle);
				}
				rownum++;
			}

            File dirFile = new File(path);
            if (!dirFile.exists()) {
                dirFile.mkdir();
            }
            efile = new File(path+"/"+ tableName +"-" + DateEnum.YYYYMMDD.format()+"-"+new Random().nextInt(9999) +".xls");
            fos = new FileOutputStream(efile);
            workbook.write(fos);
		} catch ( IOException e) {
			LehandException.throwException("导出异常", e);
		} finally {
            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
		return efile.getPath();
	}

	/******************************************读取excel*****************************/
	/**
	 * 根据fileType不同读取excel文件
	 *
	 * @param orginName
	 * @param is
	 * @throws IOException
	 */
	@SuppressWarnings({ "resource", "deprecation" })
	public static List<List<String>> readExcel(String orginName, InputStream is) {
		String fileType = orginName.substring(orginName.lastIndexOf(".") + 1);
		// return a list contains many list
		List<List<String>> lists = new ArrayList<List<String>>();
		//读取excel文件
		try {
			//获取工作薄
			Workbook wb = null;
			if (fileType.equals("xls")) {
				wb = new HSSFWorkbook(is);
			} else if (fileType.equals("xlsx")) {
				wb = new XSSFWorkbook(is);
			} else {
				return null;
			}

			//读取第一个工作页sheet
			Sheet sheet = wb.getSheetAt(0);
			//第一行为标题
			for (Row row : sheet) {
				ArrayList<String> list = new ArrayList<String>();
				for (Cell cell : row) {
					//根据不同类型转化成字符串
					cell.setCellType(CellType.STRING);
					if(cell.getStringCellValue().trim().equals("无")) {
						list.add("");
					} else {
						list.add(cell.getStringCellValue());
					}
				}
				lists.add(list);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return lists;
	}

}
