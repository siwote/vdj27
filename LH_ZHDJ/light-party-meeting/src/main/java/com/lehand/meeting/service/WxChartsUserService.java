package com.lehand.meeting.service;


import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.CryptogramUtil;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.pojo.MeetingRecordSubject;
import com.lehand.module.urc.pojo.UserAccountView;
import com.lehand.module.urc.service.UserService;
import com.lehand.module.urc.utils.Base64Utils;
import org.apache.logging.log4j.LogManager;
import com.lehand.module.urc.pojo.User;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;


/**
 * 微信扫码业务层
 */

@Service
public class WxChartsUserService {
    private static final Logger logger = LogManager.getLogger(WxChartsUserService.class);

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private UserService userService;

    /**
     * 查询openId信息
     * @param session
     * @return
     */
    public String getWxUserOpenId(Session session){
        String result = generalSqlComponent.query(SqlCode.getQueryOpenId, new Object[]{session.getUserid()});
        return result;
    }


    /**
     * 查询User信息
     * @param openid
     * @return
     */
    public User getUserByOpenId(String openid){
        return generalSqlComponent.query(SqlCode.getUrcUserByOpenId, new Object[]{openid});
    }


    /**
     * 会议签到
     */
    @Transactional(rollbackFor = Exception.class)
    public void signMeeting(User session, String eventKey) {
        MeetingRecordSubject meet = new MeetingRecordSubject();
        String userId = generalSqlComponent.query(SqlCode.getUserIdByIdCard, new Object[]{session.getIdno()});
        meet.setMrid(Long.parseLong(eventKey));
        meet.setBusitype(0);
        meet.setUsertype("0");
        meet.setUserid(userId);
        meet.setIdcard(session.getIdno());
        meet.setUsername(session.getUsername());
        //1是微信签到
        meet.setRemark1("1");
        meet.setCompid(session.getCompid());
        generalSqlComponent.insert(MeetingSqlCode.saveMeetingRecordSubject, meet);
    }

    public UserAccountView getUserViewByAccount(String account, String password) {

        UserAccountView user = this.userService.getUserByAccount(account);
        if (null == user) {
            LehandException.throwException("用户名或密码错误");
        }

        String pwd = "";
        try {
            pwd = CryptogramUtil.AESPlus.encrypt(Base64Utils.encodeToString(Base64Utils.decode(password)));
        } catch (Exception var8) {
            LehandException.throwException("检查密码错误");
        }
        if (!user.getPasswd().equals(pwd)) {
            LehandException.throwException("用户名或密码错误");
        }

        return user;
    }


    /**
     * 更新用户openId绑定系统
     * @param openid
     * @return
     */
    public void updateUserOpenId(String openid,Long userid) {
        //查看是否已经绑定过微信账号
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from urc_user where remarks3 = ? and userid = ?", new Object[]{openid, userid});
        if(map!=null&&map.size()>0){
            LehandException.throwException("该账号已绑定过！！");
        }
        //清除之前绑定的账号
        generalSqlComponent.getDbComponent().update("update urc_user set remarks3 = ? where remarks3 = ? and userid != ?",new Object[]{"",openid,userid});
        generalSqlComponent.update(SqlCode.ModifyUrcUserOpenId,new Object[]{openid,userid});
    }



}
