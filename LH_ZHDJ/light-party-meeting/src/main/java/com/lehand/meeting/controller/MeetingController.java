package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingListInfoDto;
import com.lehand.meeting.dto.MeetingPageReq;
import com.lehand.meeting.dto.MeetingRecordDto;
import com.lehand.meeting.dto.MeetingTagRecordDto;
import com.lehand.meeting.pojo.CommDefaultValue;
import com.lehand.meeting.pojo.MeetingRecord;
import com.lehand.meeting.pojo.MeetingRecordPlace;
import com.lehand.meeting.service.MeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

@Api(value = "三会一课", tags = { "三会一课接口" })
@RestController
@RequestMapping("/horn/meeting")
public class MeetingController extends BaseController {

    private static final Logger logger = LogManager.getLogger(MeetingController.class);
    @Resource
    private MeetingService meetingService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "首页信息查询", notes = "首页信息查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    @RequestMapping("/getMeetingTypeStatistics")
    public Message getMeetingTypeStatistics(int year,String orgcode){
        Message msg = new Message();
        try{
            msg.setData(meetingService.getMeetingTypeStatistics(year,orgcode,getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("首页信息查询错误");
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "会议查询列表", notes = "会议查询列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingListForPage")
    public Message getMeetingListForPage(MeetingPageReq req, Pager pager){
    	Message msg = new Message();
        try{
            msg.setData(new PagerUtils<MeetingListInfoDto>(meetingService.getMeetingListForPage(getSession(),req,pager)));
            msg.success();
        }catch (Exception e){
        	e.printStackTrace();
            msg.fail("列表信息查询错误!!!"+e.getMessage());
        }

        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "根据会议类型查询会议达标信息", notes = "根据会议类型查询会议达标信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "mtype", value = "会议类型", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getSingleMeetingTypeStatistics")
    public Message getSingleMeetingTypeStatistics(int year,String mtype,String orgcode){
        Message msg = new Message();
        try{
            msg.setData(meetingService.getSingleMeetingTypeStatistics(year,mtype,orgcode,getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("会议达标状态查询错误");
        }
        return msg;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "增加会议记录", notes = "增加会议记录", httpMethod = "POST")
    @RequestMapping("/addMeeting")
    public Message addMeeting(@RequestBody MeetingRecordDto meetingRecordDto) {
    	Message msg = new Message();
        try {
            msg.setData(meetingService.addMeeting(meetingRecordDto, getSession(),0));
        } catch (Exception e) {
            LehandException.throwException("添加失败"+e);
        }
        msg.success();
        return msg;
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "修改会议记录", notes = "修改会议记录", httpMethod = "POST")
    @RequestMapping("/modifyMeeting")
    public Message modifyMeeting(@RequestBody MeetingRecordDto meetingRecordDto) {
        Message msg = new Message();
        try {
            msg.setData(meetingService.modifyMeeting(meetingRecordDto, getSession()));
            msg.success();
        } catch (Exception e) {
        	msg.fail(e.getMessage());
            e.printStackTrace();
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询支部下人员信息", notes = "查询支部下人员信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organcode", value = "组织编码", required = true, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "xm", value = "姓名（可选查询）", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMemberByOrgancode")
    public Message getMemberByOrgancode(String organcode, String xm) {
    	Message msg = new Message();
        msg.setData(meetingService.getMemberByOrgancode(organcode, xm));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询系统参数", notes = "查询系统参数（根据编码查询）", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramkey", value = "编码", required = true, dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/getSystemParamByKey")
    public Message getSystemParamByKey(String paramkey) {
        Message msg = new Message();
        msg.setData(meetingService.getSystemParamByKey(paramkey, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=3)
    @ApiOperation(value = "删除会议记录信息", notes = "删除会议记录信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/removeMeeting")
    public Message removeMeeting(String mrid) {
        Message msg = new Message();
        msg.setData(meetingService.removeMeeting(mrid, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "提交会议记录信息", notes = "提交会议记录信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/submitMeeting")
    public Message submitMeeting(String mrid) {
        Message msg = new Message();
        msg.setData(meetingService.submitMeeting(mrid, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查看会议记录", notes = "查看会议记录", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingRecord")
    public Message getMeetingRecord(String mrid) {
        Message msg = new Message();
        msg.setData(meetingService.getMeetingRecord(mrid, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查看会议地点", notes = "查看会议地点", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organcode", value = "组织机构ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingPlace")
    public Message getMeetingPlace(@RequestBody MeetingRecordPlace meetingRecordPlace) {
    	Message msg = new Message();
        msg.setData(meetingService.getMeetingPlace(meetingRecordPlace, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "保存默认表单项", notes = "保存默认表单项", httpMethod = "POST")
    @RequestMapping("/saveDefauleForm")
    public Message saveDefauleForm(@RequestBody CommDefaultValue commDefaultValue) {
    	Message msg = new Message();
        msg.setData(meetingService.saveDefauleForm(commDefaultValue, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "获取默认表单项", notes = "获取默认表单项", httpMethod = "POST")
    @RequestMapping("/getDefauleForm")
    public Message getDefauleForm(@RequestBody CommDefaultValue commDefaultValue) {
        Message msg = new Message();
        msg.setData(meetingService.getDefauleForm(commDefaultValue, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询会议信息，条件可选", notes = "查询会议信息，条件可选", httpMethod = "POST")
    @RequestMapping("/queryMeetingByAddition")
    public Message queryMeetingByAddition(@RequestBody MeetingRecord meetingRecord, Pager pager) {
    	Message msg =  new Message();
        msg.setData(new PagerUtils<List<MeetingRecord>>(meetingService.queryMeetingByAddition(meetingRecord, pager, getSession())));
        return msg.success();
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "增加标签会议记录", notes = "增加标签会议记录", httpMethod = "POST")
    @RequestMapping("/addTagMeeting")
    public Message addTagMeeting(@RequestBody MeetingTagRecordDto dto) {    	
    	Message msg = new Message();
        msg.setData(meetingService.addTagMeeting(dto, getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "修改标签会议记录", notes = "修改标签会议记录", httpMethod = "POST")
    @RequestMapping("/modifyTagMeeting")
    public Message modifyTagMeeting(@RequestBody MeetingTagRecordDto dto) {
        Message msg = new Message();
        msg.setData(meetingService.modifyTagMeeting(dto, getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查看标签会议记录", notes = "查看标签会议记录", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingTagRecord")
    public Message getMeetingTagRecord(String mrid) {
        Message msg = new Message();
        msg.setData(meetingService.getMeetingTagRecord(mrid, getSession()));
        return msg.success();
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "最新一条会议信息", notes = "该组织最新一条会议信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", required = true, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "mtype", value = "会议类型", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getLatestMeetingRecord")
    public Message getLatestMeetingRecord(String orgcode,String mtype) {
        Message msg = new Message();
        msg.setData(meetingService.getLatestMeetingRecord(orgcode,mtype, getSession()));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "支部应到人数", notes = "支部应到人数", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getShallreachedNum")
    public Message getShallreachedNum(String orgcode){
        Message msg = new Message();
        msg.setData(meetingService.getShallreachedNum(getSession(),orgcode));
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "应参会人", notes = "应参会人", httpMethod = "POST")
    @RequestMapping("/getSub")
    public Message getAttendSubByType(String type,String orgcode){
        Message msg = new Message();
        msg.setData(meetingService.getAttendSubByType(getSession(),type,orgcode));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "会议类型集合", notes = "会议类型集合", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "code", value = "编码:默认传 OLM_CODE", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMeetingTypeList")
    public Message getMeetingTypeList(String code){
        Message msg = new Message();
        msg.setData(meetingService.getMeetingTypeList(getSession(),code));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党小组下拉", notes = "党小组下拉", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getPartyGroupSelection")
    public Message getPartyGroupSelection(String orgcode){
        Message msg = new Message();
        msg.setData(meetingService.getPartyGroupSelection(orgcode,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "根据党小组id查询成员", notes = "根据党小组id查询成员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "党小组id", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getGroupSubjects")
    public Message getGroupSubjects(Long id){
        Message msg = new Message();
        msg.setData(meetingService.getGroupSubjects(id,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "H5查询支部下人员信息", notes = "H5查询支部下人员信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organcode", value = "组织编码", required = true, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "xm", value = "姓名（可选查询）", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "zzlb", value = "组织机构类别", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getH5MemberByOrgancode")
    public Message getH5MemberByOrgancode(String organcode, String xm,String zzlb) {
        Message msg = new Message();
        msg.setData(meetingService.getH5MemberByOrgancode(organcode, xm,zzlb));
        return msg.success();
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "会议巡查列表", notes = "会议巡查列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/meetingInspection")
    public Message meetingInspection(String organcode, Integer years, Pager pager){
        Message msg = new Message();
        try{
            msg.setData(new PagerUtils<MeetingListInfoDto>(meetingService.meetingInspection(getSession(),organcode,years,pager)));
            msg.success();
        }catch (Exception e){
            e.printStackTrace();
            msg.fail("列表查询错误");
        }

        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "会议召开情况汇总导出", notes = "会议召开情况汇总导出", httpMethod = "POST")
    @RequestMapping("/excel")
    public void excel(HttpServletResponse response, Integer years, String organcode) {
        exportExcel2(response,years,organcode);
    }

    public void exportExcel2(HttpServletResponse response, Integer years, String organcode) {

        /** 第一步，创建一个Workbook，对应一个Excel文件  */
        XSSFWorkbook wb = new XSSFWorkbook();

        /** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
        XSSFSheet sheet = wb.createSheet(years + "年淮北矿业集团智慧党建平台组织生活汇总表");

        /** 第三步，设置样式以及字体样式*/
        XSSFCellStyle titleStyle = createTitleCellStyle(wb);
        XSSFCellStyle headerStyle2 = createHeadCellStyle2(wb);
        XSSFCellStyle contentStyle = createContentCellStyle(wb);

        /** 第四步，创建标题 ,合并标题单元格 */
        // 行号
        int rowNum = 0;
        // 创建第一页的第一行，索引从0开始
        XSSFRow row0 = sheet.createRow(rowNum++);
        // 设置行高
        row0.setHeight((short) 800);

        String title = years + "年淮北矿业集团智慧党建平台组织生活汇总表";
        XSSFCell c00 = row0.createCell(0);
        c00.setCellValue(title);
        c00.setCellStyle(titleStyle);
        // 合并单元格，参数依次为起始行，结束行，起始列，结束列 （索引0开始）
        //标题合并单元格操作，9为总列数
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 16));

        //第四行
        XSSFRow row3 = sheet.createRow(rowNum++);
        row3.setHeight((short) 500);
        String[] row_third = {"党组织名称", "支部委员会", "", "支部党员大会", "", "党小组会", "","上党课","","组织生活会","","民主评议党员","","党员活动日","","党委中心组学习",""};
        for (int i = 0; i < row_third.length; i++) {
            XSSFCell tempCell = row3.createCell(i);
            tempCell.setCellValue(row_third[i]);
            tempCell.setCellStyle(headerStyle2);
        }

        XSSFRow row4 = sheet.createRow(rowNum++);
        row3.setHeight((short) 700);
        String[] row_four = {"", "召开次数", "逾期召开次数", "召开次数", "逾期召开次数", "召开次数", "逾期召开次数","召开次数","逾期召开次数","召开次数","逾期召开次数","召开次数","逾期召开次数","召开次数","逾期召开次数","召开次数","逾期召开次数"};
        for (int i = 0; i < row_four.length; i++) {
            XSSFCell tempCell = row4.createCell(i);
            tempCell.setCellValue(row_four[i]);
            tempCell.setCellStyle(headerStyle2);
        }

        //合并
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 0, 0));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 2));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 4));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 6));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 7, 8));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 9, 10));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 11, 12));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 13, 14));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 15, 16));

        //第五行开始往后
        Pager pager = new Pager();
        pager.setPageSize(10000);
        meetingService.meetingInspection(getSession(),organcode,years,pager);
        List<Map<String, Object>> dataList = (List<Map<String, Object>>) pager.getRows(); //查询出来的数据

        //循环每一行数据
        for (Map<String, Object> excelData : dataList) {
            XSSFRow tempRow = sheet.createRow(rowNum++);
            tempRow.setHeight((short) 500);
            // 循环单元格填入数据
            for (int j = 0; j < 17; j++) {
                XSSFCell tempCell = tempRow.createCell(j);
                tempCell.setCellStyle(contentStyle);
                String tempValue;
                if (j == 0) {
                    tempValue = excelData.get("organname").toString();
                } else if (j == 1) {
                    tempValue = excelData.get("one").toString();
                } else if (j == 2) {
                    tempValue = excelData.get("two").toString();
                } else if (j == 3) {
                    tempValue = excelData.get("three").toString();
                } else if (j == 4) {
                    tempValue = excelData.get("four").toString();
                } else if (j == 5) {
                    tempValue = excelData.get("five").toString();
                } else if(j == 6){
                    tempValue = excelData.get("six").toString();
                }else if(j == 7){
                    tempValue = excelData.get("seven").toString();
                }else if(j == 8){
                    tempValue = excelData.get("eight").toString();
                }else if(j == 9){
                    tempValue = excelData.get("nine").toString();
                }else if(j == 10){
                    tempValue = excelData.get("ten").toString();
                }else if(j == 11){
                    tempValue = excelData.get("eleven").toString();
                }else if(j == 12){
                    tempValue = excelData.get("twelve").toString();
                }else if(j == 13){
                    tempValue = excelData.get("thirteen").toString();
                }else if(j == 14){
                    tempValue = excelData.get("fourteen").toString();
                }else if(j == 15){
                    tempValue = excelData.get("fifteen").toString();
                }else if(j == 16){
                    tempValue = excelData.get("sixteen").toString();
                }else{
                    tempValue = "";
                }
                tempCell.setCellValue(tempValue);
            }
        }
        sheet.autoSizeColumn((short)1);
        sheet.autoSizeColumn((short)2);
        sheet.autoSizeColumn((short)8);
        sheet.setColumnWidth(10,4000);
        sheet.setColumnWidth(11,4000);
        sheet.setColumnWidth(12,4000);
        sheet.setColumnWidth(13,4000);
        sheet.setColumnWidth(14,4000);
        sheet.setColumnWidth(15,2000);

        String fileName = years + "年淮北矿业集团智慧党建平台组织生活汇总表";
        OutputStream stream = null;
        try {
            response.setContentType("multipart/form-data");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename="+"淮北矿业集团智慧党建平台组织生活汇总表.xlsx");
            response.setHeader("FileName", "淮北矿业集团智慧党建平台组织生活汇总表.xlsx");
            stream = response.getOutputStream();
            wb.write(stream);// 将数据写出去
        }catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                if(stream != null){
                    stream.flush();
                    stream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @ 创建标题样式
     * @param wb
     * @return
     */
    private XSSFCellStyle createTitleCellStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex()); //背景颜色
        cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        XSSFFont headerFont = (XSSFFont) wb.createFont(); // 创建字体样式
        headerFont.setBold(true); //字体加粗
        headerFont.setFontName("黑体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 14); // 设置字体大小
        cellStyle.setFont(headerFont); // 为标题样式设置字体样式

        return cellStyle;
    }

    private XSSFCellStyle createHeadCellStyle2(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex()); //背景颜色
        cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        XSSFFont headerFont = (XSSFFont) wb.createFont(); // 创建字体样式
        headerFont.setBold(true); //字体加粗
        headerFont.setFontName("黑体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 12); // 设置字体大小
        cellStyle.setFont(headerFont); // 为标题样式设置字体样式

        return cellStyle;
    }

    /**
     * @ 创建表头样式
     * @param wb
     * @return
     */
    private static XSSFCellStyle createHeadCellStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyle.setAlignment(HorizontalAlignment.LEFT); //水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        XSSFFont headerFont = wb.createFont(); // 创建字体样式
        headerFont.setBold(true); //字体加粗
        headerFont.setFontName("黑体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 12); // 设置字体大小
        cellStyle.setFont(headerFont); // 为标题样式设置字体样式

        return cellStyle;
    }


    /**
     * @ 创建内容样式
     * @param wb
     * @return
     */
    private static XSSFCellStyle createContentCellStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);// 水平居中
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        // 生成12号字体
        XSSFFont font = wb.createFont();
        font.setColor((short)8);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setFont(font);

        return cellStyle;
    }
    @OpenApi(optflag=2)
    @ApiOperation(value = "退回会议记录", notes = "退回会议记录", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "remark5", value = "会议退回意见", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/rollbackMeeting")
    public Message rollbackMeeting(String mrid,String remark5) {
        Message msg = new Message();
        if(StringUtils.isEmpty(mrid)) {
            LehandException.throwException("参数有误，缺少会议记录的ID！");
        }
        if(StringUtils.isEmpty(remark5)) {
            LehandException.throwException("参数有误，缺少会议记录的退回意见！");
        }
        msg.setData(meetingService.rollbackMeeting(mrid,remark5,getSession()));
        msg.success();
        return msg;
    }
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询民主评议党员综合情况", notes = "分页查询民主评议党员综合情况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/pageDemocracyReview")
    public Message pageDemocracyReview(String mrid,Pager pager) {
        Message msg = new Message();
        if(StringUtils.isEmpty(mrid)) {
            LehandException.throwException("参数有误，缺少会议记录的ID！");
        }
        try {
            msg.setData(meetingService.pageDemocracyReview(mrid,pager,getSession()));
            msg.success();
        }catch (Exception e){
            msg.fail("分页查询失败！"+e.getMessage());
        }
        return msg;
    }
    @OpenApi(optflag=2)
    @ApiOperation(value = "(提交)编辑民主评议党员综合情况", notes = "编辑民主评议党员综合情况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mrid", value = "会议记录ID", required = true, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "党员id,编辑：userid。提交：传空", required = false, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "situation", value = "综合情况）编辑：（优秀，合格，基本合格，不合格。提交：传空", required = false, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "submit", value = "是否提交（0:未提交,1:提交）", required = false, dataType = "int", defaultValue = "")
    })
    @RequestMapping("/modifyDemocracyReview")
    public Message modifyDemocracyReview(String mrid,String userid,String situation,Integer submit) {
        Message msg = new Message();
        if(StringUtils.isEmpty(mrid)) {
            LehandException.throwException("参数有误，缺少会议记录的ID！");
        }
        if(0==submit) {
            if(StringUtils.isEmpty(userid)) {
                LehandException.throwException("参数有误，缺少党员id！");
            }
            if(StringUtils.isEmpty(situation)) {
                LehandException.throwException("参数有误，缺少综合情况！");
            }
        }
        try {
            meetingService.modifyDemocracyReview(mrid,userid,situation,submit,getSession());
            msg.success();
        }catch (Exception e){
            msg.fail("编辑失败！"+e.getMessage());
        }
        return msg;
    }
}
