package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="会议通知", description="会议通知")
public class MeetingNoticeRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户id", name="compid")
    private Long compid;

    @ApiModelProperty(value="自增长主键", name="id")
    private Long id;

    @ApiModelProperty(value="组织机构ID", name="orgid")
    private Long orgid;

    @ApiModelProperty(value="组织机构编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="组织机构名称", name="orgname")
    private String orgname;

    @ApiModelProperty(value="会议通知类型", name="type")
    private String type;

    @ApiModelProperty(value="会议通知类型名称", name="typename")
    private String typename;

    @ApiModelProperty(value="会议通知主题", name="topic")
    private String topic;

    @ApiModelProperty(value="通知会议地点", name="place")
    private String place;

    @ApiModelProperty(value="通知会议时间", name="noticetime")
    private String noticetime;

    @ApiModelProperty(value="会议通知状态（0 保存  1 撤销  2 下发）", name="status")
    private Integer status;

    @ApiModelProperty(value="会议通知状态（0 保存  1 撤销  2 下发）", name="subnum")
    private Integer subnum;

    @ApiModelProperty(value="创建人（登录账户）", name="createid")
    private String createid;

    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="修改人", name="updateid")
    private String updateid;

    @ApiModelProperty(value="修改时间", name="updatetime")
    private String updatetime;

    @ApiModelProperty(value="备注信息", name="remark")
    private String remark;

    @ApiModelProperty(value="备注1", name="remarks1")
    private String remarks1;

    @ApiModelProperty(value="备注2", name="remarks2")
    private String remarks2;

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getNoticetime() {
        return noticetime;
    }

    public void setNoticetime(String noticetime) {
        this.noticetime = noticetime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSubnum() {
        return subnum;
    }

    public void setSubnum(Integer subnum) {
        this.subnum = subnum;
    }

    public String getCreateid() {
        return createid;
    }

    public void setCreateid(String createid) {
        this.createid = createid;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateid() {
        return updateid;
    }

    public void setUpdateid(String updateid) {
        this.updateid = updateid;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(String remarks1) {
        this.remarks1 = remarks1;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }


}