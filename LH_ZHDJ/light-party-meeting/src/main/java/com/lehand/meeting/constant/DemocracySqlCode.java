package com.lehand.meeting.constant;

public class DemocracySqlCode {

    /**
     * 添加会前材料
     */
    public static String saveBeforeDemocracy ="saveBeforeDemocracy";
    /**
     * 保存民主会会议参会人
     */
    public static String saveParticipants="saveParticipants";
    /**
     * 保持会后材料
     */
    public static String saveAfterDemocracy="saveAfterDemocracy";
    /**
     * 删除民主生活会
     */
    public static String deleteDemocracy="deleteDemocracy";
    /**
     * 删除参会人
     */
    public static String deleteParticipants="deleteParticipants";
    /**
     * 修改会前材料
     */
    public static String updateBefore="updateBefore";
    /**
     * 修改会后材料
     */
    public static String updateAfter="updateAfter";
    /**
     * 根据会议id查询民主生活会信息
     */
    public static String getDemocracyById="getDemocracyById";
    /**
     * 根据会议id查询参会人信息
     */
    public static String listParticipantsById="listParticipantsById";
    /**
     * 审核会议
     */
    public static String auditDemocracy="auditDemocracy";
    /**
     * 分页查询列表页
     */
    public static String pageDemocracy="pageDemocracy";
}
