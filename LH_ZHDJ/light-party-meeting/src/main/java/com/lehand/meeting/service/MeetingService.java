package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
import com.lehand.id.core.SnowflakeLongIdGenerator;
import com.lehand.meeting.constant.CommSqlCfgCode;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.constant.common.MeetingPMtypeEnum;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.meeting.constant.common.SysConstants.MTYPE;
import com.lehand.meeting.dto.*;
import com.lehand.meeting.pojo.*;
import com.lehand.meeting.utils.CacheServiceUtil;
import com.lehand.meeting.utils.MeetingUtil;
import com.lehand.meeting.utils.OrgUtils;
import com.lehand.pm.utils.ListUtil;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.bytedeco.javacpp.indexer.HalfArrayIndexer;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.util.*;

@Service
public class MeetingService {

    @Resource
    GeneralSqlComponent generalSqlComponent;

    @Resource
    private CacheServiceUtil cacheServiceUtil;

    @Resource
    protected CheckStandardService checkStandardService;

    @Resource
    private MeetingTodoService meetingTodoService;

    @Resource
    private MeetingNoticeService meetingNoticeService;

    @Resource
    private OrgUtils orgUtils;

    @Resource
    private MeetingPatrolService meetingPatrolService;
    @Resource
    private MeetingRecordTopicsService meetingRecordTopicsService;
    @Autowired
    private SnowflakeLongIdGenerator snowflakeLongIdGenerator;
    //上传路径
    @Value("${spring.http.multipart.location}")
    private String location;
    //预览和上传的路径
    @Value("${jk.manager.down_ip}")
    private String downIp;
    @Value("${meeting.topics.maxnum}")
    private int topicsMaxNum;

    @Value("${jk.organ.yubeidangyuan}")
    private String yubeidangyuan;
    @Value("${jk.organ.zhengshidangyuan}")
    private String zhengshidangyuan;
    /**
     * 三会一课首页
     *
     * @param session
     * @return
     */
    public List<MeetingTypeStatisticsDto> getMeetingTypeStatistics(int year, String orgcode, Session session) {
        List<MeetingTypeStatisticsDto> list = new ArrayList<MeetingTypeStatisticsDto>(4);
        //查询会议三会一课子类型数据
        List<Map<String, Object>> meetingTypes = generalSqlComponent.query(MeetingSqlCode.listSysParamByUpparamkey,
                new Object[]{SysConstants.MTYPE.TMOC, session.getCompid()});

        if (meetingTypes.size() < 1) {
            return list;
        }
        for (Map<String, Object> map : meetingTypes) {
//            list.add(getMeetingTypeStatisticsByOrg(year, session.getCurrentOrg().getOrgcode(), map, session));
            list.add(getMeetingTypeStatisticsByOrg(year, orgcode, map, session));
        }

        return list;
    }

    /**
     * 根据组织机构获取三会一课统计数据
     *
     * @param organCode
     * @param map
     * @param session
     * @return
     */
    private MeetingTypeStatisticsDto getMeetingTypeStatisticsByOrg(int year, String organCode, Map<String, Object> map, Session session) {
        MeetingTypeStatisticsDto dto = new MeetingTypeStatisticsDto();
        dto.setUnConvenedMon(checkStandardService.getUnConvenedMonByOrg(year, organCode, map, session));
        dto.setStandardStatus(checkStandardService.getStandardStatusByOrg(year, organCode, map, session));
        dto.setConvened(checkStandardService.getConvenedByOrg(year, organCode, map, session));
        dto.setRequirement(String.valueOf(map.get("remarks4")));
        dto.setmType(String.valueOf(map.get("paramkey")));
        dto.setmTypeName(String.valueOf(map.get("paramvalue")));
        return dto;
    }

    /**
     * 查询会议列表分页
     *
     * @param session
     * @param req
     * @param pager
     * @return
     */
    public Pager getMeetingListForPage(Session session, MeetingPageReq req, Pager pager) {
        if (StringUtils.isEmpty(pager.getSort())) {
            pager.setUseSort(true);
//            pager.setSort(" createtime desc");
            pager.setSort("status DESC ,createtime DESC");
        }
        req.setCompid(session.getCompid());
//        req.setOrgcode(session.getCurrentOrg().getOrgcode());.
        //巡查状态只查询已提交的会议
        if (!req.getOrgcode().equals(session.getCurrentOrg().getOrgcode())) {
            req.setStatus(2);
        }
        req.setStarttime(StringUtils.isEmpty(req.getStarttime()) ? req.getStarttime() : req.getStarttime().trim() + "-01 00:00");
        req.setEndtime(StringUtils.isEmpty(req.getEndtime()) ? req.getEndtime() : req.getEndtime().trim() + "-31 23:59");
        req.setCompid(session.getCompid());

        //是否为标签类会议
        Boolean flag = checkStandardService.tagMeeting(req.getMtype());
        if (flag) {
            Pager pagerMeeting = pagerMeeting(MeetingSqlCode.pagerTagMeeting, pager, req);
            List<MeetingListInfoDto> meetingListInfoDtos = (List<MeetingListInfoDto>) pagerMeeting.getRows();
            for (MeetingListInfoDto meetingListInfoDto : meetingListInfoDtos) {
                MeetingRecordDto meetingRecord = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{meetingListInfoDto.getMrid(), session.getCompid()});
                if (meetingRecord != null) {
                    meetingListInfoDto.setRemark5(meetingRecord.getRemark5());
                }
            }
            //如果是一对多，查询对应的议题列表
            if(false==ListUtil.isEmpty(meetingListInfoDtos)){
                meetingListInfoDtos.stream().forEach((MeetingListInfoDto each)->{
                    MeetingRecordTopicsDto meetingRecordTopicsDto =new MeetingRecordTopicsDto();
                    meetingRecordTopicsDto.setMrid(each.getMrid());
                    List<MeetingRecordTopicsDto> topicsDtoList=   meetingRecordTopicsService.listMeetingRecordTopics(session,meetingRecordTopicsDto);
                    each.setTopicsList(topicsDtoList);
                });
            }
            return pagerMeeting;
        }
        Pager pagerMeeting=  pagerMeeting(MeetingSqlCode.pagerMeeting, pager, req);
        List<MeetingListInfoDto> meetingListInfoDtos = (List<MeetingListInfoDto>) pagerMeeting.getRows();
        //如果是一对多，查询对应的议题列表
        if(false==ListUtil.isEmpty(meetingListInfoDtos)){
            meetingListInfoDtos.stream().forEach((MeetingListInfoDto each)->{
                MeetingRecordTopicsDto meetingRecordTopicsDto =new MeetingRecordTopicsDto();
                meetingRecordTopicsDto.setMrid(each.getMrid());
                List<MeetingRecordTopicsDto> topicsDtoList=   meetingRecordTopicsService.listMeetingRecordTopics(session,meetingRecordTopicsDto);
                each.setTopicsList(topicsDtoList);
            });
        }
        return pagerMeeting;
    }


    /**
     * 返回分页查询数据
     *
     * @param sqlcode
     * @param pager
     * @param req
     * @return
     */
    public Pager pagerMeeting(String sqlcode, Pager pager, MeetingPageReq req) {
        return generalSqlComponent.pageQuery(sqlcode, req, pager);
    }

    /**
     * 根据会议类型查询达标状态
     *
     * @param mtype
     * @param session
     * @return
     */
    public MeetingTypeStatisticsDto getSingleMeetingTypeStatistics(int year, String mtype, String orgcode, Session session) {
        Map<String, Object> map = generalSqlComponent.query(SqlCode.getSysParam2, new Object[]{session.getCompid(), mtype});
//        return map==null?null:getMeetingTypeStatisticsByOrg(year,session.getCurrentOrg().getOrgcode(),map, session);
        return map == null ? null : getMeetingTypeStatisticsByOrg(year, orgcode, map, session);
    }


    /**
     * 校验会议是否按时召开了
     *
     * @param mtype
     * @param session
     * @return
     */
    public Boolean checkMeetingIsConveneOnTime(String mtype, Session session) {
        boolean flag = false;
        //获取会议召开频率
        Map<String, Object> map = generalSqlComponent.query(SqlCode.getSysParam2, new Object[]{session.getCompid(), mtype});
        Integer num = Integer.valueOf(map.get("remarks2").toString());
        //获取当前月份所属季度
        int i = checkStandardService.getCurrentQuarter(Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5, 7)));
        //获取当前月份所属半年
        int j = checkStandardService.getCurrHalfyear(Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5, 7)));
        switch (mtype) {
            case MTYPE.DEMOCRATIC_LIFE_MEETING:
            case MTYPE.APPRAISAL_OF_MEMBERS:
                //每年召开 民主评议党员 民主生活会
                flag = isYear(mtype, session, num);
                break;
            case MTYPE.ORG_LIFE_MEETING:
                //每半年召开 组织生活会
                flag = isHalfYear(mtype, session, num, j);
                break;
            case MTYPE.BRANCH_PARTY_CONGRESS:
            case MTYPE.PARTY_LECTURE:
                //每季度召开 支部党员大会 党课
                flag = isQuarter(mtype, session, num, i);
                break;
            default:
                //每月召开 支部委员会 党小组会 主题党日活动 党委中心组学习
                flag = isMonth(mtype, session, num);
                break;
        }
        return flag;
    }

    /**
     * 每半年召开类型
     *
     * @param mtype
     * @param session
     * @param num
     * @param j
     * @return
     */
    private boolean isHalfYear(String mtype, Session session, Integer num, int j) {
        List<Map> list;
        boolean flag;
        if (j == 1) {
            list = generalSqlComponent.query(SqlCode.listMeetingRecord3,
                    new Object[]{session.getCompid(), session.getUserid(), mtype,
                            DateEnum.YYYY.format() + "01-01", DateEnum.YYYY.format() + "06-30"});
        } else {
            list = generalSqlComponent.query(SqlCode.listMeetingRecord3,
                    new Object[]{session.getCompid(), session.getUserid(), mtype,
                            DateEnum.YYYY.format() + "06-30", DateEnum.YYYY.format() + "12-31"});
        }
        flag = (list != null && list.size() > 0 ? list.size() : 0) >= num;
        return flag;
    }

    /**
     * 每季度召开类型
     *
     * @param mtype
     * @param session
     * @param num
     * @param i
     * @return
     */
    private boolean isQuarter(String mtype, Session session, Integer num, int i) {
        List<Map> list;
        boolean flag;
        if (i == 1) {
            list = generalSqlComponent.query(SqlCode.listMeetingRecord3,
                    new Object[]{session.getCompid(), session.getUserid(),
                            mtype, DateEnum.YYYY.format() + "01-01", DateEnum.YYYY.format() + "03-31"});
        } else if (i == 2) {
            list = generalSqlComponent.query(SqlCode.listMeetingRecord3,
                    new Object[]{session.getCompid(), session.getUserid(),
                            mtype, DateEnum.YYYY.format() + "04-01", DateEnum.YYYY.format() + "06-30"});
        } else if (i == 3) {
            list = generalSqlComponent.query(SqlCode.listMeetingRecord3,
                    new Object[]{session.getCompid(), session.getUserid(),
                            mtype, DateEnum.YYYY.format() + "07-01", DateEnum.YYYY.format() + "09-30"});
        } else {
            list = generalSqlComponent.query(SqlCode.listMeetingRecord3,
                    new Object[]{session.getCompid(), session.getUserid(),
                            mtype, DateEnum.YYYY.format() + "10-01", DateEnum.YYYY.format() + "12-31"});
        }
        flag = (list != null && list.size() > 0 ? list.size() : 0) >= num;
        return flag;
    }

    /**
     * 每月召开类型
     *
     * @param mtype
     * @param session
     * @param num
     * @return
     */
    private boolean isMonth(String mtype, Session session, Integer num) {
        List<Map> list;
        boolean flag;
        list = generalSqlComponent.query(SqlCode.listMeetingRecord2,
                new Object[]{session.getCompid(), session.getUserid(), mtype, DateEnum.YYYYMMDD.format().substring(0, 7)});
        flag = (list != null && list.size() > 0 ? list.size() : 0) >= num;
        return flag;
    }

    /**
     * 每年召开类型
     *
     * @param mtype
     * @param session
     * @param num
     * @return
     */
    private boolean isYear(String mtype, Session session, Integer num) {
        List<Map> list;
        boolean flag;
        list = generalSqlComponent.query(SqlCode.listMeetingRecord1,
                new Object[]{session.getCompid(), session.getUserid(), mtype, DateEnum.YYYY.format()});
        flag = (list != null && list.size() > 0 ? list.size() : 0) >= num;
        return flag;
    }

    /**
     * 增加会议记录
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingRecordDto addMeeting(MeetingRecordDto meetingRecordDto, Session session,int type) throws Exception {

        if (StringUtils.isEmpty(meetingRecordDto.getPlace())) {
            LehandException.throwException("会议地点不能为空！");
        }
        if(0==type){
            getParam(meetingRecordDto, session);
        }

        if (!StringUtils.isEmpty(meetingRecordDto.getNoticeid())) {
            Map<String, Object> meeting = getMeetingRecord(session, meetingRecordDto.getNoticeid());
            if (MagicConstant.NUM_STR.NUM_2.equals(String.valueOf(meeting.get("mrstatus")))) {
                LehandException.throwException("已提交会议记录,请勿重复操作!");
            } else if (MagicConstant.NUM_STR.NUM_1.equals(String.valueOf(meeting.get("mrstatus")))) {
                //存在会议记录并且事未提交状态，则走修改接口
                meetingRecordDto.setMrid(Long.valueOf(meeting.get("mrid").toString()));
                return modifyMeeting(meetingRecordDto, session);
            }
        }
        //这里新增之前需要判断下该会议是不是已经写过会议记录了（单纯的保存没提交）

        //增加会议内容信息
        Long meetid = generalSqlComponent.insert(MeetingSqlCode.saveMeetingRecord, meetingRecordDto);
        meetingRecordDto.setMrid(meetid);

        //保存会议地点
//        saveMeetingPlace(meetingRecordDto.getPlace(), session);
        saveMeetingPlace(meetingRecordDto.getPlace(), meetingRecordDto.getOrgcode(), session);

        //更新附件
        List<Map<String, Object>> attach = meetingRecordDto.getMeetingAttachList();
        List<Map<String, Object>> sign = meetingRecordDto.getMeetingSignAttachList();
        if (sign != null) {
            attach.addAll(meetingRecordDto.getMeetingSignAttachList());
        }
        updateAttachmentInfo(meetid, attach, MagicConstant.NUM_STR.NUM_10, session);

        //判断是否从会议新增的记录
        if (StringUtils.isEmpty(meetingRecordDto.getNoticeid())) {//不是,新建会议
            List<String> tags = new ArrayList<String>();
            Long noticeid = addNewMeetingNoticeByRecord(meetingRecordDto, tags, session);
            meetingRecordDto.setNoticeid(noticeid);
        }

        //增加会议参与人信息
        addMeetingSubjects(meetingRecordDto, session);

        //保存会议与会议记录之间的关系
        addMRecordRelation(meetid, meetingRecordDto.getNoticeid(), meetingRecordDto.getMrtype(), session.getCompid());

        //待办事项处理
        if (!StringUtils.isEmpty(meetingRecordDto.getTodoid())) {
            meetingTodoService.updateMeetingTodoStatus(1, session.getCompid(), meetingRecordDto.getTodoid());
        }
        MeetingToDoRecord meetingToDoRecord = meetingTodoService.queryMeetingTodo(meetingRecordDto.getTodoid(), session.getCompid());
        //逻辑删除会议待办
        if (MagicConstant.NUM_STR.NUM_2.equals(meetingRecordDto.getStatus())) {
            removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecordDto.getMrtype(), 1, meetingRecordDto.getMryear(), meetingRecordDto.getMrmonth());
        }
        //如果存一对多的议题，单独表保存
        List<MeetingRecordTopicsDto> topicsList = meetingRecordDto.getTopicsList();
        if (topicsList != null && topicsList.size() > 0) {
            if (topicsList.size() > topicsMaxNum) {
                LehandException.throwException("议题数量不能超过" + topicsMaxNum);
            }
            for (MeetingRecordTopicsDto each : topicsList) {
                each.setId(snowflakeLongIdGenerator.generate().value());
                each.setMrid(meetingRecordDto.getMrid());
                meetingRecordTopicsService.addMeetingRecordTopics(session, each);
            }
        }
        return meetingRecordDto;
    }

    private Map<String, Object> getMeetingRecord(Session session, Long noticeid) {
        return generalSqlComponent.query(MeetingSqlCode.getMeetingInfoByNoticeid,
                new Object[]{noticeid, session.getCompid()});
    }

    private void getParam(MeetingRecordDto meetingRecordDto, Session session) {
        meetingRecordDto.setCompid(session.getCompid());
        meetingRecordDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingRecordDto.setCreateuserid(session.getUserid());
        meetingRecordDto.setOrgid(session.getCurrentOrg().getOrgid());
        meetingRecordDto.setOrgname(session.getCurrentOrg().getOrgname());
        meetingRecordDto.setOrgcode(session.getCurrentOrg().getOrgcode());
        meetingRecordDto.setMrmonth(MeetingUtil.getMonth(meetingRecordDto.getStarttime()));
        meetingRecordDto.setMryear(MeetingUtil.getYear(meetingRecordDto.getStarttime()));
        meetingRecordDto.setStatus(meetingRecordDto.getStatus());
        meetingRecordDto.setStarttime(meetingRecordDto.getStarttime().trim());
        meetingRecordDto.setEndtime(meetingRecordDto.getEndtime().trim());
    }

    /**
     * 从会议记录新增会议通知
     *
     * @param meetingRecordDto
     * @return
     */
    private Long addNewMeetingNoticeByRecord(MeetingRecordDto meetingRecordDto, List<String> tags, Session session) {
        MeetingNoticeRecord record = new MeetingNoticeRecord();
        BeanUtils.copyProperties(meetingRecordDto, record);
        record.setCompid(session.getCompid());
        record.setStatus(2);
        record.setType(meetingRecordDto.getMrtype());
        record.setTypename(MeetingPMtypeEnum.getMsg(meetingRecordDto.getMrtype()));
        record.setCreateid(session.getLoginAccount());
        record.setNoticetime(meetingRecordDto.getStarttime());
        record.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        //增加会议通知中的参会人
        List<MeetingRecordSubject> meetingRecordSubjects = meetingRecordDto.getMeetingRecordSubjects();
        List<MeetingRecordSubject> subjects = new ArrayList<MeetingRecordSubject>();
        for (MeetingRecordSubject sub : meetingRecordSubjects) {
            if (!sub.getUsertype().equals(MagicConstant.NUM_STR.NUM_0)) {
                continue;
            }
            subjects.add(sub);
        }
        record.setSubnum(subjects.size());
        Long noticeid = generalSqlComponent.insert(MeetingSqlCode.addMeetingNoticeRecord, record);

        if (meetingRecordSubjects.size() < 1) {
            return noticeid;
        }
        if (subjects.size() < 1) {
            return noticeid;
        }
        meetingNoticeService.AddNoticeSubject(subjects, noticeid, session.getCompid());

        //增加通知的标签
        if (tags != null && tags.size() > 0) {
            //处理关联关系
            List<String> tagids = updateMeetingTags(noticeid, tags, meetingRecordDto.getOrgcode(), session, 1);
        } else {
            return noticeid;
        }


        return noticeid;
    }

    /**
     * 保存会议与会议记录之间的关联
     *
     * @param meetid
     * @param noticeid
     * @param mrtype
     * @param compid
     */
    private void addMRecordRelation(Long meetid, Long noticeid, String mrtype, Long compid) {
        generalSqlComponent.insert(MeetingSqlCode.addMeetingRecordRelation, new HashMap() {{
            put("mrid", meetid);
            put("noticeid", noticeid);
            put("type", mrtype);
            put("compid", compid);
        }});
    }

    /**
     * 修改会议记录
     *
     * @param meetingRecordDto
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingRecordDto modifyMeeting(MeetingRecordDto meetingRecordDto, Session session) throws Exception {
        if (StringUtils.isEmpty(meetingRecordDto.getMrid())) {
            LehandException.throwException("参数有误，缺少会议记录的ID！");
        }

        if (!StringUtils.isEmpty(meetingRecordDto.getNoticeid())) {
            Map<String, Object> meeting = getMeetingRecord(session, meetingRecordDto.getNoticeid());
            if (MagicConstant.NUM_STR.NUM_2.equals(String.valueOf(meeting.get("mrstatus")))) {
                LehandException.throwException("已提交会议记录,请勿重复操作!");
            }
        }
        meetingRecordDto.setCompid(session.getCompid());
        meetingRecordDto.setStarttime(meetingRecordDto.getStarttime().trim());
        meetingRecordDto.setEndtime(meetingRecordDto.getEndtime().trim());
        meetingRecordDto.setMrmonth(MeetingUtil.getMonth(meetingRecordDto.getStarttime()));
        meetingRecordDto.setMryear(MeetingUtil.getYear(meetingRecordDto.getStarttime()));
        //查询会议记录
        MeetingRecordDto meetingRecord = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{meetingRecordDto.getMrid(), session.getCompid()});
        if (meetingRecord == null) {
            LehandException.throwException("会议记录不存在，请刷新后重试！");
        }
//        if(!meetingRecord.getCreateuserid().equals(session.getUserid())) {
//            LehandException.throwException("您没有权限修改会议记录！");
//        }

        //更新会议数据
        meetingRecord.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingRecord.setUpdateuserid(session.getUserid());
        generalSqlComponent.update(MeetingSqlCode.modifyMeeting, meetingRecordDto);

        //参会人员数据更新(先删除后添加)
        generalSqlComponent.delete(MeetingSqlCode.delMeetingSubject, new Object[]{meetingRecordDto.getMrid(), 0, session.getCompid()});

        //获取通知id
        if (StringUtils.isEmpty(meetingRecordDto.getNoticeid())) {
            Map<String, Object> meeting = generalSqlComponent.query(MeetingSqlCode.getMeetingRecordRelation2, new Object[]{meetingRecordDto.getMrid()});
            meetingRecordDto.setNoticeid(Long.valueOf(meeting.get("noticeid").toString()));
        }

        //增加会议参与人信息
        addMeetingSubjects(meetingRecordDto, session);
        //逻辑删除会议待办
        if (MagicConstant.NUM_STR.NUM_2.equals(meetingRecordDto.getStatus())) {
            removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecordDto.getMrtype(), 1, meetingRecordDto.getMryear(), meetingRecordDto.getMrmonth());
        }
        //更新附件
        List<Map<String, Object>> attach = meetingRecordDto.getMeetingAttachList();
        List<Map<String, Object>> sign = meetingRecordDto.getMeetingSignAttachList();
        if (sign != null) {
            attach.addAll(meetingRecordDto.getMeetingSignAttachList());
        }
        updateAttachmentInfo(meetingRecordDto.getMrid(), attach, MagicConstant.NUM_STR.NUM_10, session);
        //修改一对多议题
        List<MeetingRecordTopicsDto> topicsDtoList= meetingRecordDto.getTopicsList();
        if(false ==ListUtil.isEmpty(topicsDtoList)){
            if(topicsDtoList.size()>topicsMaxNum){
                LehandException.throwException("会议议题数量不能超过"+topicsMaxNum);
            }
            MeetingRecordTopicsDto meetingRecordTopicsDto= new MeetingRecordTopicsDto();
            meetingRecordTopicsDto.setMrid(meetingRecordDto.getMrid());
            meetingRecordTopicsService.delMeetingRecordTopicsByMrid(meetingRecordTopicsDto);
            for(MeetingRecordTopicsDto each:topicsDtoList){
                if(each.getId() ==null || each.getId().equals("")){
                    each.setId(snowflakeLongIdGenerator.generate().value());
                }
                each.setMrid(meetingRecordDto.getMrid());
                meetingRecordTopicsService.addMeetingRecordTopics(session,each);
            }
        }
        return meetingRecordDto;
    }

    /**
     * 增加会议参与人员信息
     *
     * @param meetingRecordDto
     * @param session
     */
    public void addMeetingSubjects(MeetingRecordDto meetingRecordDto, Session session) {
        if (meetingRecordDto.getMeetingRecordSubjects().size() > 0) {
            for (MeetingRecordSubject subject : meetingRecordDto.getMeetingRecordSubjects()) {
                subject.setCompid(session.getCompid());
                subject.setMrid(meetingRecordDto.getMrid());
                subject.setBusitype(0);
                generalSqlComponent.insert(MeetingSqlCode.saveMeetingRecordSubject, subject);
            }
        }
        //增加会议缺席人员
        addMeetingAbsentSubject(meetingRecordDto, session);
    }

    /**
     * 根据IDCard查询用户信息
     *
     * @param idno
     * @return
     */
    public Map<String, Object> getUserIdByIdno(String idno) {
        return generalSqlComponent.query(MeetingSqlCode.queryUrcUserByIdno, new Object[]{idno});
    }

    /**
     * 查询支部下人员信息
     *
     * @param organcode
     * @param xm
     * @return
     */
    public List<Map<String, Object>> getMemberByOrgancode(String organcode, String xm) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        paramMap.put("xm", xm);

        List<Map<String, Object>> list = generalSqlComponent.query(MeetingSqlCode.getMemberByOrgancode, paramMap);
        for (Map<String, Object> map : list) {
            map.put("key", map.get("userid"));
            map.put("title", map.get("dzzmc"));
            map.put("slots", setTreeType());
            map.put("dnzwname", map.get("dnzwname"));
            if (StringUtils.isEmpty(map.get("dnzwname"))) {
                map.put("dnzwnameName", "普通党员");
            } else {
                map.put("dnzwnameName", cacheServiceUtil.getName("d_dy_dnzw", map.get("dnzwname")));
            }
        }
        return list;
    }

    /**
     * 设置类型（组织、人员）
     *
     * @return
     */
    public Map<String, Object> setTreeType() {
        Map<String, Object> temp = new HashMap<>();
        temp.put("icon", "user");
        return temp;
    }

    /**
     * 查询系统参数（根据编码查询）
     *
     * @param paramKey
     * @param session
     * @return
     */
    public Map<String, Object> getSystemParamByKey(String paramKey, Session session) {
        return generalSqlComponent.query(MeetingSqlCode.getSysParam2, new Object[]{session.getCompid(), paramKey});
    }

    /**
     * 删除会议
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingRecord removeMeeting(String mrid, Session session) {
        MeetingRecord meetingRecord = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{mrid, session.getCompid()});
        if (meetingRecord == null) {
            LehandException.throwException("会议记录不存在，请刷新后重试！");
        }
        meetingRecord.setDeletetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingRecord.setDeleteuserid(session.getUserid());
        generalSqlComponent.update(MeetingSqlCode.updateMeetingStatusByMrid, new Object[]{0, session.getCompid(), mrid});
        generalSqlComponent.delete(MeetingSqlCode.deleteRecordRelationByMrid, new Object[]{mrid, session.getCompid()});
        //如果会议与议题是一对多，会议议题保存的会议议题表中
        MeetingRecordTopicsDto meetingRecordTopicsDto =new MeetingRecordTopicsDto();
        meetingRecordTopicsDto.setMrid(meetingRecord.getMrid());
        meetingRecordTopicsService.delMeetingRecordTopicsByMrid(meetingRecordTopicsDto);
        return meetingRecord;
    }

    /**
     * 提交会议
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingRecord submitMeeting(String mrid, Session session) {
        MeetingRecord meetingRecord = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{mrid, session.getCompid()});
        if (meetingRecord == null) {
            LehandException.throwException("会议记录不存在，请刷新后重试！");
        }
        generalSqlComponent.update(MeetingSqlCode.updateMeetingStatusByMrid, new Object[]{2, session.getCompid(), mrid});
        List<String> tagCode = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{mrid, 0, 1});
        //逻辑删除会议待办
        if (tagCode != null && tagCode.size() > 0) {
            removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecord.getMrtype(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
            for (String code : tagCode) {
                removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, code, 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
            }
        } else {
            removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecord.getMrtype(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
        }
        return meetingRecord;
    }

    /**
     * 查询会议详情
     *
     * @param mrid
     * @param session
     * @return
     */
    public MeetingRecordDto getMeetingRecord(String mrid, Session session) {
        //查询会议基本信息
        MeetingRecordDto meetingRecordDto = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{mrid, session.getCompid()});
        Map<String, Object> meeting = generalSqlComponent.query(MeetingSqlCode.getMeetingRecordRelation2, new Object[]{meetingRecordDto.getMrid()});
        meetingRecordDto.setNoticeid(Long.valueOf(meeting.get("noticeid").toString()));
        //查询会议的相关人员
        List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMrid, new Object[]{mrid, 0, session.getCompid()});
        meetingRecordDto.setMeetingRecordSubjects(subjects);
        //勾选项
        List<String> list = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{mrid, 0, session.getCompid()});
        if (list != null & list.size() > 0) {
            meetingRecordDto.setTagcode(list);
        } else {
            meetingRecordDto.setTagcode(new ArrayList<>());
        }
        //原附件
        List<Map<String, Object>> meetingAttachList = getAttachmentInfo(mrid, 3, MagicConstant.NUM_STR.NUM_10, session);
        //签到表
        List<Map<String, Object>> signTable = getAttachmentInfo(mrid, 4, MagicConstant.NUM_STR.NUM_10, session);
        setSignAttach(meetingRecordDto, signTable);
        meetingRecordDto.setMeetingAttachList(meetingAttachList);
        meetingRecordDto.setMeetingRecordAttachList(getAttachmentInfo(mrid, 1, MagicConstant.NUM_STR.NUM_10, session));
//        meetingRecordDto.setMeetingSignAttachList(getAttachmentInfo(mrid,2,MagicConstant.NUM_STR.NUM_10,session));

        MeetingRecordTopicsDto meetingRecordTopicsDto = new MeetingRecordTopicsDto();
        meetingRecordTopicsDto.setMrid(meetingRecordDto.getMrid());
        List<MeetingRecordTopicsDto> topicsDtoList = meetingRecordTopicsService.listMeetingRecordTopics(session, meetingRecordTopicsDto);
        meetingRecordDto.setTopicsList(topicsDtoList);
        return meetingRecordDto;
    }

    public void setSignAttach(MeetingRecordDto meetingRecordDto, List<Map<String, Object>> signTable) {
        if (signTable.size() > 0) {
            Map<String, Object> map = signTable.get(0);
            map.put("intostatus", 4);
//            meetingAttachList.add(map);
            List<Map<String, Object>> signAttach = new ArrayList<Map<String, Object>>(1);
            signAttach.add(map);
            meetingRecordDto.setMeetingSignAttachList(signAttach);
        }
    }

    /**
     * 查询会议地点
     *
     * @param meetingRecordPlace
     * @param session
     * @return
     */
    public List<Map<String, Object>> getMeetingPlace(MeetingRecordPlace meetingRecordPlace, Session session) {
        meetingRecordPlace.setCompid(session.getCompid());
        return generalSqlComponent.query(MeetingSqlCode.getMeetingPlace2, meetingRecordPlace);
    }


    /**
     * 根据组织编码保存会议地点，不存在的添加，存在的跳过
     *
     * @param place
     * @param session
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveMeetingPlace(String place, String orgcode, Session session) {
        MeetingRecordPlace meetingRecordPlace = new MeetingRecordPlace();
        meetingRecordPlace.setCompid(session.getCompid());
        meetingRecordPlace.setPlace(place);
//        meetingRecordPlace.setOrgcode(session.getCurrentOrg().getOrgcode());
        meetingRecordPlace.setOrgcode(orgcode);
        meetingRecordPlace.setOrgid(session.getCurrentOrg().getOrgid());
        //查询会议地点是否存在
        List<MeetingRecordPlace> places = generalSqlComponent.query(MeetingSqlCode.getMeetingPlace, meetingRecordPlace);
        if (places.size() == 0) {
            //不存在的地点增加
            Long seqno = generalSqlComponent.query(MeetingSqlCode.countMeetingPlace, new Object[]{});
            meetingRecordPlace.setSeqno(seqno);
            generalSqlComponent.insert(MeetingSqlCode.saveMeetingPlace, meetingRecordPlace);
        }
    }

    /**
     * 保存会议表单的默认值
     *
     * @param commDefaultValue
     * @param session
     * @return
     */
    public CommDefaultValue saveDefauleForm(CommDefaultValue commDefaultValue, Session session) {
        commDefaultValue.setCompid(session.getCompid());
        commDefaultValue.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        commDefaultValue.setUpdateuserid(session.getUserid());
        //删除以前的表单项
        generalSqlComponent.delete(MeetingSqlCode.delCommDefaultValue, commDefaultValue);
        //保存默认表单值
        generalSqlComponent.insert(MeetingSqlCode.saveDefauleForm, commDefaultValue);
        return commDefaultValue;
    }

    /**
     * 查询默认表单
     *
     * @param commDefaultValue
     * @param session
     * @return
     */
    public List<CommDefaultValue> getDefauleForm(CommDefaultValue commDefaultValue, Session session) {
        commDefaultValue.setCompid(session.getCompid());
        return generalSqlComponent.query(MeetingSqlCode.getCommDefaultValue, commDefaultValue);
    }


    /**
     * 保存附件
     *
     * @param meetid
     * @param attachment
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean updateAttachmentInfo(Long meetid, List<Map<String, Object>> attachment, String type, Session session) {

        //删除原有关联
        delFileBusiness(meetid, Long.parseLong(type), session.getCompid());

        if (attachment == null || attachment.size() < 1) {
            return true;
        }

        for (Map<String, Object> att : attachment) {
            Long fileid = Long.valueOf(String.valueOf(att.get("fileid")));
//        	Short classid = Short.valueOf(String.valueOf(att.get("classid")));
            //区分签到表
            Short classid = Short.valueOf(String.valueOf(att.get("intostatus") == null ? att.get("classid") : att.get("intostatus")));
            //新增关联信息
            DlFileBusiness business = new DlFileBusiness();
            business.setCompid(session.getCompid());
            business.setFileid(fileid);
            business.setBusinessid(meetid);
            business.setBusinessname(Short.valueOf(type));
            business.setIntostatus(classid);
            generalSqlComponent.insert(CommSqlCfgCode.addDlFileBusiness, business);
        }

        return true;
    }

    /**
     * 根据业务ID删除业务关联附件
     *
     * @param bussid
     * @param bussname
     * @param compid
     * @return
     */
    private boolean delFileBusiness(Long bussid, long bussname, Long compid) {
        generalSqlComponent.delete(MeetingSqlCode.delFileBusinessByMbyBusID, new Object[]{bussid, bussname, compid});
        return true;
    }

    /**
     * 查询附件列表
     *
     * @param mrid
     * @param session
     * @return
     */
    public List<Map<String, Object>> getAttachmentInfo(String mrid, long type, String busitype, Session session) {
        if (StringUtils.isEmpty(mrid)) {
            return null;
        }
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("compid", session.getCompid());
        param.put("businessname", Short.valueOf(busitype));
        param.put("intostatus", type);
        param.put("businessid", mrid);
        List<Map<String, Object>> list = generalSqlComponent.query(MeetingSqlCode.getAttachmentList, param);
        for (Map<String, Object> map : list) {
            map.put("flpath", String.format("%s%s", downIp, map.get("dir")));
        }
        return list;
    }

    /**
     * 根据附件ID删除附件关联
     *
     * @param fileid
     * @param bussid
     * @param bussname
     * @param compid
     * @return
     */
    public boolean delFileBusiness(Long fileid, Long bussid, Long bussname, Long compid) {
        generalSqlComponent.delete(MeetingSqlCode.delFileBusinessByMb, new Object[]{fileid, bussid, bussname, compid});
        return true;
    }

    /**
     * 查询会议信息，条件可选
     *
     * @param meetingRecord
     * @param pager
     * @return
     */
    public Pager queryMeetingByAddition(MeetingRecord meetingRecord, Pager pager, Session session) {
        meetingRecord.setCompid(session.getCompid());
        return generalSqlComponent.pageQuery(MeetingSqlCode.queryMeetingByAddition, meetingRecord, pager);
    }


    /**
     * 新增会议带标签
     *
     * @param dto
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingTagRecordDto addTagMeeting(MeetingTagRecordDto dto, Session session) {

        if (StringUtils.isEmpty(dto.getPlace())) {
            LehandException.throwException("会议地点不能为空！");
        }
        dto.setCompid(session.getCompid());
        dto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        dto.setCreateuserid(session.getUserid());
        dto.setOrgid(session.getCurrentOrg().getOrgid());
        dto.setOrgname(session.getCurrentOrg().getOrgname());
        dto.setOrgcode(session.getCurrentOrg().getOrgcode());
        dto.setMrmonth(MeetingUtil.getMonth(dto.getStarttime()));
        dto.setMryear(MeetingUtil.getYear(dto.getStarttime()));
        dto.setStatus(dto.getStatus());
        dto.setStarttime(dto.getStarttime().trim());
        dto.setEndtime(dto.getEndtime().trim());

        if (!StringUtils.isEmpty(dto.getNoticeid())) {
            Map<String, Object> meeting = getMeetingRecord(session, dto.getNoticeid());
            if (MagicConstant.NUM_STR.NUM_2.equals(String.valueOf(meeting.get("mrstatus")))) {
                LehandException.throwException("已提交会议记录,请勿重复操作!");
            } else if (MagicConstant.NUM_STR.NUM_1.equals(String.valueOf(meeting.get("mrstatus")))) {
                //存在会议记录并且事未提交状态，则走修改接口
                dto.setMrid(Long.valueOf(meeting.get("mrid").toString()));
                return modifyTagMeeting(dto, session);
            }
        }

        //增加会议内容信息
        Long meetid = generalSqlComponent.insert(MeetingSqlCode.saveMeetingRecord, dto);
        dto.setMrid(meetid);

        //保存会议地点
//	        saveMeetingPlace(dto.getPlace(), session);
        saveMeetingPlace(dto.getPlace(), dto.getOrgcode(), session);


        //判断是否从会议新增的记录
        if (StringUtils.isEmpty(dto.getNoticeid())) {//不是,新建会议
            Long noticeid = addNewMeetingNoticeByRecord(dto, dto.getTagcode(), session);
            dto.setNoticeid(noticeid);
        }

        //增加会议参与人信息
        addMeetingSubjects(dto, session);

        //保存会议与会议记录之间的关系
        addMRecordRelation(meetid, dto.getNoticeid(), dto.getMrtype(), session.getCompid());
        //更新附件
        List<Map<String, Object>> attach = dto.getMeetingAttachList();
        List<Map<String, Object>> sign = dto.getMeetingSignAttachList();
        if (sign != null) {
            attach.addAll(sign);
        }

        updateAttachmentInfo(meetid, attach, MagicConstant.NUM_STR.NUM_10, session);


        //待办事项处理
        if (!StringUtils.isEmpty(dto.getTodoid())) {
            meetingTodoService.updateMeetingTodoStatus(1, session.getCompid(), dto.getTodoid());
        }
        List<String> tagcode = dto.getTagcode();
        //逻辑删除会议待办
        if (tagcode.size() < 1) {
            if (MagicConstant.NUM_STR.NUM_2.equals(dto.getStatus())) {
                removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, dto.getMrtype(), 1, dto.getMryear(), dto.getMrmonth());
            }
            return dto;
        } else {
            if (MagicConstant.NUM_STR.NUM_2.equals(dto.getStatus())) {
                for (String code : tagcode) {
                    removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, code, 1, dto.getMryear(), dto.getMrmonth());
                }
                removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, dto.getMrtype(), 1, dto.getMryear(), dto.getMrmonth());
            }
        }

        //处理关联关系
        List<String> tagids = updateMeetingTags(meetid, dto.getTagcode(), dto.getOrgcode(), session, 0);
        dto.setTagcode(tagids);

        //处理关联投票
        List<Map<String, Object>> list = updateMeetingVoteRelations(dto.getMrid(), dto.getForms(), 1, session);

        return dto;
    }

    /**
     * 保存会议关联投票数据
     *
     * @param meetid
     * @param forms
     * @param session
     * @return
     */
    private List<Map<String, Object>> updateMeetingVoteRelations(Long meetid, List<Map<String, Object>> forms, int type, Session session) {
        System.out.println("***********************************************************" + forms);

        if (forms.size() < 1) {
            return forms;
        }

        // 删除原有关联
        generalSqlComponent.delete(MeetingSqlCode.delMeetingBusinessMapBySid, new Object[]{session.getCompid(), meetid, type});

        for (Map<String, Object> map : forms) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("subject_id", meetid);
            param.put("subject_type", type);
            param.put("subjected_name", map.get("subjected_name"));
            param.put("subjected_id", map.get("subjected_id"));
            param.put("subjected_extrkey", map.get("subjected_extrkey"));
            param.put("compid", session.getCompid());
            generalSqlComponent.insert(MeetingSqlCode.addMeetingBusinessMap, param);
        }
        return forms;
    }


    /**
     * 处理标签关联关系
     *
     * @param meetid
     * @param tagcode
     * @param orgcode
     * @param session
     * @return
     */
    public List<String> updateMeetingTags(Long meetid, List<String> tagcode, String orgcode, Session session, int type) {

        //删除原有关联关系
        generalSqlComponent.delete(MeetingSqlCode.deleteMeetingTagsByMeetingId, new Object[]{meetid, type, session.getCompid()});
        if (tagcode.size() < 1) {
            return tagcode;
        }

        //新增关联关系
        for (String tag : tagcode) {
            Map<String, Object> map = generalSqlComponent.query(SqlCode.getSysParam2, new Object[]{session.getCompid(), tag});
            MeetingTagsMap param = new MeetingTagsMap();
            param.setCompid(session.getCompid());
            param.setOrgcode(orgcode);
            param.setSubjectid(meetid);
            param.setSubjecttype(type);
            param.setTagcode(tag);
            param.setTagname(String.valueOf(map.get("paramvalue")));
            generalSqlComponent.insert(MeetingSqlCode.insertMeetingTags, param);
            if(0==type){
                //民主评议党员-新增党建综合情况
                if(MeetingPMtypeEnum.APPRAISAL_OF_MEMBERS.getCode().equals(tag)){
                    Long orgid = session.getCurrentOrg().getOrgid();
                    if(StringUtils.isEmpty(orgid)){
                        LehandException.throwException("组织id,不能为空！");
                    }
                    Pager pager = new Pager();
                    pager.setPageNo(1);
                    pager.setPageSize(10);
                    Map<String, Object> paramMap = new HashMap<>();
                    paramMap.put("mrid",meetid);
                    generalSqlComponent.pageQuery(SqlCode.pageDemocracyReview,paramMap,pager);
                    List<Map<String, Object>>  rows = (List<Map<String, Object>>) pager.getRows();
                    if(rows.size()<1){
                        List<TDyInfo> list  = generalSqlComponent.query(SqlCode.getAllTDyInfoByOrgcode,new Object[]{orgcode});
                        if(list!=null&&list.size()>0){
                            for (TDyInfo tDyInfo:
                                    list) {
                                paramMap.put("userid",tDyInfo.getUserid());
                                generalSqlComponent.insert(SqlCode.insertDemocracyReview,paramMap);
                            }
                        }
                    }
                }
            }
        }

        return tagcode;
    }

    /**
     * 修改标签类会议
     * TODO 整个组织生活完成时考虑与原查修改并
     *
     * @param dto
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MeetingTagRecordDto modifyTagMeeting(MeetingTagRecordDto dto, Session session) {
        if (StringUtils.isEmpty(dto.getMrid())) {
            LehandException.throwException("参数有误，缺少会议记录的ID！");
        }
        if (!StringUtils.isEmpty(dto.getNoticeid())) {
            Map<String, Object> meeting = getMeetingRecord(session, dto.getNoticeid());
            if (MagicConstant.NUM_STR.NUM_2.equals(String.valueOf(meeting.get("mrstatus")))) {
                LehandException.throwException("已提交会议记录,请勿重复操作!");
            }
        }
        dto.setCompid(session.getCompid());
        dto.setStarttime(dto.getStarttime().trim());
        dto.setEndtime(dto.getEndtime().trim());
        dto.setMrmonth(MeetingUtil.getMonth(dto.getStarttime()));
        dto.setMryear(MeetingUtil.getYear(dto.getStarttime()));
        //查询会议记录
        MeetingRecordDto meetingRecord = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{dto.getMrid(), session.getCompid()});
        if (meetingRecord == null) {
            LehandException.throwException("会议记录不存在，请刷新后重试！");
        }
//	        if(!meetingRecord.getCreateuserid().equals(session.getUserid())) {
//	            LehandException.throwException("您没有权限修改会议记录！");
//	        }

        //更新会议数据
        meetingRecord.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingRecord.setUpdateuserid(session.getUserid());
        generalSqlComponent.update(MeetingSqlCode.modifyMeeting, dto);

        //参会人员数据更新(先删除后添加)
        generalSqlComponent.delete(MeetingSqlCode.delMeetingSubject, new Object[]{dto.getMrid(), 0, session.getCompid()});

        if (StringUtils.isEmpty(dto.getNoticeid())) {
            Map<String, Object> meeting = generalSqlComponent.query(MeetingSqlCode.getMeetingRecordRelation2, new Object[]{dto.getMrid()});
            dto.setNoticeid(Long.valueOf(meeting.get("noticeid").toString()));
        }
        //增加会议参与人信息
        addMeetingSubjects(dto, session);

        //更新附件
        List<Map<String, Object>> attach = dto.getMeetingAttachList();
        List<Map<String, Object>> sign = dto.getMeetingSignAttachList();
        if (sign != null) {
            attach.addAll(sign);
        }
        updateAttachmentInfo(dto.getMrid(), attach, MagicConstant.NUM_STR.NUM_10, session);

        List<String> tagcode = dto.getTagcode();


        //处理关联关系
        List<String> tagids = updateMeetingTags(meetingRecord.getMrid(), dto.getTagcode(), meetingRecord.getOrgcode(), session, 0);
        dto.setTagcode(tagids);

        //处理关联投票
        List<Map<String, Object>> list = updateMeetingVoteRelations(dto.getMrid(), dto.getForms(), 1, session);

        dto.setForms(list);

        if (tagcode.size() < 1) {

            //逻辑删除会议待办
            if (MagicConstant.NUM_STR.NUM_2.equals(dto.getStatus())) {
                removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, dto.getMrtype(), 1, dto.getMryear(), dto.getMrmonth());
            }
        } else {
            if (MagicConstant.NUM_STR.NUM_2.equals(dto.getStatus())) {
                for (String code : tagcode) {
                    removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, code, 1, dto.getMryear(), dto.getMrmonth());
                }
                removeTodoItem(Todo.ItemType.TYPE_MAINTAIN_MEETING, dto.getMrtype(), 1, dto.getMryear(), dto.getMrmonth());
            }
        }
        //更新会议通知表
        updateMeetingNoticerecord(dto, session);
        return dto;
    }

    /**
     * 根据id获取标签类会议详情
     * TODO 整个组织生活完成时考虑与原查询合并
     *
     * @param mrid
     * @param session
     * @return
     */
    public MeetingTagRecordDto getMeetingTagRecord(String mrid, Session session) {
        //查询会议基本信息
        MeetingRecordDto dto = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{mrid, session.getCompid()});
        MeetingTagRecordDto result = new MeetingTagRecordDto();
        result.setCompid(dto.getCompid());
        result.setContext(dto.getContext());
        result.setCreatetime(dto.getDeletetime());
        result.setCreateuserid(dto.getCreateuserid());
        result.setDeletetime(dto.getEndtime());
        result.setDeleteuserid(dto.getDeleteuserid());
        result.setEndtime(dto.getEndtime());
        result.setGroupid(dto.getGroupid());
        result.setGroupname(dto.getGroupname());
        result.setModeratorid(dto.getModeratorid());
        result.setModeratoridcard(dto.getModeratoridcard());
        result.setModeratorname(dto.getModeratorname());
        result.setMrid(dto.getMrid());
        result.setMrmonth(dto.getMrmonth());
        result.setMrtype(dto.getMrtype());
        result.setMryear(dto.getMryear());
        result.setOrgcode(dto.getOrgcode());
        result.setOrgid(dto.getOrgid());
        result.setOrgname(dto.getOrgname());
        result.setPlace(dto.getPlace());
        result.setRealitynum(dto.getRealitynum());
        result.setRecorderid(dto.getRecorderid());
        result.setRecorderidcard(dto.getRecorderidcard());
        result.setRecordername(dto.getRecordername());
        result.setShouldnum(dto.getShouldnum());
        result.setStarttime(dto.getStarttime());
        result.setStatus(dto.getStatus());
        result.setTopic(dto.getTopic());
        result.setUpdatetime(dto.getUpdatetime());
        result.setUpdateuserid(dto.getUpdateuserid());
        result.setRemark3(dto.getRemark3());
        result.setRemark4(dto.getRemark4());
        result.setDecision(dto.getDecision());
        result.setDiscuss(dto.getDiscuss());
        result.setClasshours(dto.getClasshours());
        Map<String, Object> meeting = generalSqlComponent.query(MeetingSqlCode.getMeetingRecordRelation2, new Object[]{dto.getMrid()});
        result.setNoticeid(Long.valueOf(meeting.get("noticeid").toString()));
        //查询会议的相关人员
        List<MeetingRecordSubject> subjects = generalSqlComponent.query(MeetingSqlCode.getMeetingSubjectByMrid, new Object[]{mrid, 0, session.getCompid()});
        result.setMeetingRecordSubjects(subjects);
        //勾选项
        List<String> list = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{mrid, 0, session.getCompid()});
        if (list != null & list.size() > 0) {
            result.setTagcode(list);
        } else {
            result.setTagcode(new ArrayList<>());
        }
        //原附件
        List<Map<String, Object>> meetingAttachList = getAttachmentInfo(mrid, 3, MagicConstant.NUM_STR.NUM_10, session);
        //签到表
        List<Map<String, Object>> signTable = getAttachmentInfo(mrid, 4, MagicConstant.NUM_STR.NUM_10, session);
        setSignAttach(result, signTable);
        result.setMeetingAttachList(meetingAttachList);
        result.setMeetingRecordAttachList(getAttachmentInfo(mrid, 1, MagicConstant.NUM_STR.NUM_10, session));
//        result.setMeetingSignAttachList(getAttachmentInfo(mrid,2,MagicConstant.NUM_STR.NUM_10,session));
        result.setForms(getBusiMapList(mrid, session.getCompid(), 1));
        return result;
    }

    /**
     * 查询关联会议列表
     *
     * @param mrid
     * @param compid
     * @param type
     * @return
     */
    private List<Map<String, Object>> getBusiMapList(String mrid, Long compid, int type) {

        List<Map<String, Object>> list = generalSqlComponent.query(MeetingSqlCode.lisMeeingBusiMapBySid, new Object[]{compid, mrid, type});
        return list;
    }

    /**
     * 根据组织机构编码和会议类型查询最新一条会议信息
     *
     * @param orgcode
     * @param mtype
     * @param session
     * @return
     */
    public MeetingRecordDto getLatestMeetingRecord(String orgcode, String mtype, Session session) {
        if (StringUtils.isEmpty(orgcode) || StringUtils.isEmpty(mtype)) {
            LehandException.throwException("传参异常!");
        }
        Boolean flag = checkStandardService.tagMeeting(mtype);
        Long mrid = getMeetingidBymType(orgcode, mtype, flag, session.getCompid());
        if (StringUtils.isEmpty(mrid)) {
            return null;
        }
        return flag ? getMeetingTagRecord(String.valueOf(mrid), session) : getMeetingRecord(String.valueOf(mrid), session);
    }


    /**
     * 根据会议类型获取会议id
     *
     * @param orgcode
     * @param mtype
     * @param flag
     * @param compid
     * @return
     */
    private Long getMeetingidBymType(String orgcode, String mtype, Boolean flag, Long compid) {
        if (flag) {
            Long mrid = generalSqlComponent.query(MeetingSqlCode.getLatestTagMeetingid, new HashMap() {{
                put("orgcode", orgcode);
                put("mtype", mtype);
                put("compid", compid);
            }});
            return mrid;
        }
        Long mrid = generalSqlComponent.query(MeetingSqlCode.getLatestBaseMeetingid, new HashMap() {{
            put("orgcode", orgcode);
            put("mtype", mtype);
            put("compid", compid);
        }});
        return mrid;
    }

    /**
     * 支部应到人数
     *
     * @param session
     * @param orgcode
     * @return
     */
    public long getShallreachedNum(Session session, String orgcode) {
        if("6300000037".equals(session.getCurrentOrg().getOrgtypeid())
           || "6400000038".equals(session.getCurrentOrg().getOrgtypeid())){
            return generalSqlComponent.query(MeetingSqlCode.countShallReachedByOrg, new Object[]{orgcode});
        }else{
            return generalSqlComponent.getDbComponent().getSingleColumn("select count(*) from t_dzz_bzcyxx where " +
                    "organid = ( select organid from t_dzz_info where organcode = ? ) and zfbz=0",Long.class,
                    new Object[]{orgcode});
        }
    }

    public List<MeetingRecordSubject> getAttendSubByType(Session session, String type, String orgcode) {
        List<MeetingRecordSubject> result = new ArrayList<MeetingRecordSubject>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        list = getMeetingSub(session, type, orgcode);
        if (list.size() < 1) {
            return result;
        }
        for (Map<String, Object> map : list) {
            MeetingRecordSubject sub = new MeetingRecordSubject();
            sub.setUsertype("0");
            sub.setIdcard(String.valueOf(map.get("zjhm")));
            sub.setBusitype(2);
            sub.setUserid(String.valueOf(map.get("userid")));
            sub.setUsername(String.valueOf(map.get("xm")));
            sub.setPost(String.valueOf(map.get("dnzwname")));
            result.add(sub);
        }
        return result;
    }

    public List<Map<String, Object>> getMeetingSub(Session session, String type, String orgcode) {
        List<Map<String, Object>> list;
        if ("6300000037".equals(String.valueOf(session.getCurrentOrg().getOrgtypeid())) && !type.equals(SysConstants.MTYPE.BRANCH_COMMITTEE)) {
            list = getMemberByOrgancode(orgcode, MagicConstant.STR.EMPTY_STR);
        } else {
            String organid = generalSqlComponent.getDbComponent().getSingleColumn("select organid  from  t_dzz_info where organcode = ?", String.class, new Object[]{orgcode});
            list = generalSqlComponent.query(MeetingSqlCode.queryTdzzInfoSimpleBZCYList,
                    new HashMap<String, Object>() {{
                        put("organid", organid);
                        put("xm", MagicConstant.STR.EMPTY_STR);
                    }});
        }
        return list;
    }


    /**
     * 递归查询下级类型
     *
     * @param upparamcode
     * @param list
     * @param compid
     * @return
     */
    public List<Map<String, Object>> recursionOrg(String upparamcode, List<Map<String, Object>> list, Long compid) {
        List<Map<String, Object>> meet = generalSqlComponent.query(MeetingSqlCode.listSysParamByUpparamkey, new Object[]{upparamcode, compid});

        if (meet.size() > 0) {
            for (Map<String, Object> map : meet) {
                //去掉三会一课父级
                if (!String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.TMOC)) {
                    list.add(map);
                }
                recursionOrg(String.valueOf(map.get("paramkey")), list, compid);
            }
        }
        return list;
    }

    /**
     * 获取会议类型
     *
     * @param session
     * @param code
     * @return
     */
    public List<Map<String, Object>> getMeetingTypeList(Session session, String code) {
        List<Map<String, Object>> meet = new ArrayList<Map<String, Object>>();
        meet = recursionOrg(code, meet, session.getCompid());

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> map : meet) {
            Map<String, Object> m = new HashMap<String, Object>();
            if (String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.DEMOCRATIC_LIFE_MEETING)) {
                continue;
            }
            if (String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.THEME_PARTY_DAY)) {
                continue;
            }
            if (String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.THEORETICAL_STUDY)) {
                continue;
            }
            if (String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.ORG_LIFE_MEETING)) {
                continue;
            }
            if (String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.APPRAISAL_OF_MEMBERS)) {
                continue;
            }
            m.put("type", map.get("paramkey"));
            m.put("typename", map.get("paramvalue"));
            list.add(m);
        }
        return list;
    }

    /**
     * 党小组
     *
     * @param orgcode
     * @param session
     * @return
     */
    public List<Map<String, Object>> getPartyGroupSelection(String orgcode, Session session) {
        return generalSqlComponent.query(MeetingSqlCode.getPartyGroupSel, new Object[]{orgcode, session.getCompid()});
    }

    /**
     * 党小组人员信息
     *
     * @param id
     * @param session
     * @return
     */
    public Map<String, Object> getGroupSubjects(Long id, Session session) {

        Map<String, Object> result = new HashMap<String, Object>(2);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        params.put("compid", session.getCompid());
        params.put("type", 1);
        params.put("subjecttype", 1);
        List<Map<String, Object>> groupleader = generalSqlComponent.query(MeetingSqlCode.listGroupMemeberSubjects, params);

        Map<String, Object> param = (Map<String, Object>) ((HashMap<String, Object>) params).clone();
        param.put("type", MagicConstant.STR.EMPTY_STR);
        param.put("subjecttype", 0);
        List<Map<String, Object>> groupmembers = generalSqlComponent.query(MeetingSqlCode.listGroupMemeberSubjects, param);
        result.put("groupleader", groupleader);
        result.put("groupmembers", groupmembers);

        return result;
    }

    /**
     * h5人员选择器
     *
     * @param organcode
     * @param xm
     * @param zzlb
     * @return
     */
    public List<Map<String, Object>> getH5MemberByOrgancode(String organcode, String xm, String zzlb) {
        if (SysConstants.ZZLB.DZB.equals(zzlb) || SysConstants.ZZLB.LHDZB.equals(zzlb)) {
            return getMemberByOrgancode(organcode, xm);
        }

        String organid = orgUtils.getOrganidByOrganCode(organcode);
        List<Map<String, Object>> list = generalSqlComponent.query(MeetingSqlCode.queryTdzzInfoSimpleBZCYList, new HashMap<String, Object>() {{
            put("organid", organid);
            put("xm", xm);
        }});
        for (Map<String, Object> map : list) {
            map.put("key", map.get("userid"));
            map.put("title", MagicConstant.STR.EMPTY_STR);
            map.put("slots", setTreeType());
            map.put("dnzwname", map.get("dnzwname"));
            if (StringUtils.isEmpty(map.get("dnzwname"))) {
                map.put("dnzwnameName", "普通党员");
            } else {
                map.put("dnzwnameName", cacheServiceUtil.getName("d_dy_dnzw", map.get("dnzwname")));
            }
        }
        return list;
    }

    public Pager meetingInspection(Session session, String organcode, Integer years, Pager pager) {
        List<Map<String, Object>> list = meetingPatrolService.getMeetingTypes(session);
        pager = generalSqlComponent.pageQuery(MeetingSqlCode.MeetingPatrolList2, new Object[]{organcode, organcode}, pager);
        List<Map<String, Object>> organs = (List<Map<String, Object>>) pager.getRows();
        for (Map<String, Object> organ : organs) {//循环旗下机构
            for (Map<String, Object> stringObjectMap : list) {//循环会议类型
                //查询召开次数
                Integer num1 = checkStandardService.getConvenedByOrg(years, organ.get("organcode").toString(), stringObjectMap, session);
                //查询逾期召开次数
                Integer num2 = checkStandardService.getOverdueNum(stringObjectMap, organ.get("organcode").toString(), years);

                switch (stringObjectMap.get("paramkey").toString()) {
                    //支部委员会
                    case "branch_committee":
                        organ.put("one", num1);
                        organ.put("two", num2);
                        break;
                    //支部党员大会
                    case "branch_party_congress":
                        organ.put("three", num1);
                        organ.put("four", num2);
                        break;
                    //党小组会
                    case "party_group_meeting":
                        organ.put("five", num1);
                        organ.put("six", num2);
                        break;
                    //上党课
                    case "party_lecture":
                        organ.put("seven", num1);
                        organ.put("eight", num2);
                        break;
                    //组织生活会
                    case "org_life_meeting":
                        organ.put("nine", num1);
                        organ.put("ten", num2);
                        break;
                    //民主评议党员
                    case "appraisal_of_members":
                        organ.put("eleven", num1);
                        organ.put("twelve", num2);
                        break;
                    //党员活动日
                    case "theme_party_day":
                        organ.put("thirteen", num1);
                        organ.put("fourteen", num2);
                        break;
                    //党委中心组学习
                    case "theoretical_study":
                        organ.put("fifteen", num1);
                        organ.put("sixteen", num2);
                        break;
                    default:
                        break;
                }
            }
        }
        return pager;
    }

    /**
     * @param
     * @return
     * @Description修改会议通知
     * @Author zwd
     * @Date 2020/10/29 13:44
     **/
    public void updateMeetingNoticerecord(MeetingTagRecordDto dto, Session session) {
        //查询会议(会议通知)和会议记录关联表
        MeetingRecordRelation mrr = generalSqlComponent.query(MeetingSqlCode.getMeetingRecordRelation, new Object[]{dto.getMrid()});
        if (mrr != null) {
            Map<String, String> parms = new HashMap<String, String>();
            parms.put("topic", dto.getTopic());
            parms.put("realitynum", dto.getRealitynum().toString());
            parms.put("updateid", session.getUserid().toString());
            parms.put("updatetime", String.valueOf(new Date().getTime()));
            parms.put("place", dto.getPlace());
            parms.put("id", mrr.getNoticeid().toString());

            generalSqlComponent.update(MeetingSqlCode.updateMNoticePlace, parms);
        }
    }

    /**
     * @param
     * @return
     * @Description 会议缺席人员
     * @Author zwd
     * @Date 2020/11/5 17:29
     **/
    public void addMeetingAbsentSubject(MeetingRecordDto meetingRecordDto, Session session) {
        //删除缺席人员
        generalSqlComponent.delete(MeetingSqlCode.delMeeAbsentSub, new Object[]{meetingRecordDto.getNoticeid(), session.getCompid()});
        for (MeetingAbsentSubject subject : meetingRecordDto.getMeetingAbsentSubject()) {
            subject.setCompid(session.getCompid());
            subject.setNoticeid(meetingRecordDto.getNoticeid());
            subject.setUserid(subject.getUserid());
            subject.setUserid(subject.getUserid());
            generalSqlComponent.insert(MeetingSqlCode.saveMeetingAbsentSub, subject);
        }
    }


    /**
     * 待办任务begin
     ****************************************************************************************************************/
    @Resource
    private TodoItemService todoItemService;

    /**
     * 待办事项.
     *
     * @param itemType 事项类型
     * @param bizid    该事项对应的业务ID
     * @param compid   当前会话
     * @param mryear   时间日期 YYYY
     * @param mrmonth  时间日期 MM
     */
    private void removeTodoItem(Todo.ItemType itemType, String bizid, long compid, Integer mryear, Integer mrmonth) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        String s = switchCode(bizid, mryear, mrmonth);
        itemDTO.setCategory(itemType.getCode());
        itemDTO.setCompid(compid);
        itemDTO.setBizid(s);
        todoItemService.deleteTodoItem(itemDTO);
    }

    private String switchCode(String bizid, Integer mryear, Integer mrmonth) {
        switch (bizid) {
            case "appraisal_of_members":
                return "appraisal_of_members" + mryear;
            case "branch_committee":
                return "branch_committee" + mryear + mrmonth;
            case "party_group_meeting":
                return "party_group_meeting" + mryear + mrmonth;
            case "party_lecture":
                return "party_lecture" + mryear + getThisQuarter(mrmonth);
            case "org_life_meeting":
                return "org_life_meeting" + mryear + halfAYear(mrmonth);
            case "theme_party_day":
                return "theme_party_day" + mryear + mrmonth;
            case "branch_party_congress":
                return "branch_party_congress" + mryear + getThisQuarter(mrmonth);
            case "democratic_life_meeting":
                return "democratic_life_meeting" + mryear;
        }
        return null;
    }

    public static String getThisQuarter(int month) {
        String b;
        switch (month) {
            case 1:
            case 2:
            case 3:
                return b = "123";
            case 4:
            case 5:
            case 6:
                return b = "456";
            case 7:
            case 8:
            case 9:
                return b = "789";
            case 10:
            case 11:
            case 12:
                return b = "101112";
        }
        return null;
    }

    public static String halfAYear(int month) {
        String b;
        switch (month) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return b = "123456";
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                return b = "789101112";
        }
        return null;
    }

    /**
     * 待办任务end
     ****************************************************************************************************************/
    @Transactional(rollbackFor = Exception.class)
    public String rollbackMeeting(String mrid, String remark5, Session session) {
        //查询会议记录
        MeetingRecordDto meetingRecord = generalSqlComponent.query(MeetingSqlCode.getMeetingByMrid, new Object[]{mrid, session.getCompid()});
        if (meetingRecord == null) {
            LehandException.throwException("会议记录不存在，请刷新后重试！");
        }

        //更新会议数据
        meetingRecord.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        meetingRecord.setUpdateuserid(session.getUserid());
        meetingRecord.setRemark5(remark5);
        meetingRecord.setStatus(MagicConstant.NUM_STR.NUM_4);
        generalSqlComponent.update(MeetingSqlCode.modifyMeeting, meetingRecord);

        //是否还有已提交会议记录
        List<Map<String, Object>> meetingRecordList = generalSqlComponent.getDbComponent().listMap(MeetingSqlCode.LIST_MEETING_RECORD_BY_STATUS, new Object[]{2, 1, meetingRecord.getOrgid(), meetingRecord.getMrtype()});
        Boolean b1 = false, b2 = false, b3 = false;
        if (meetingRecordList.size() > 0 && meetingRecordList != null) {
            for (Map<String, Object> map : meetingRecordList) {
                Long mmrid = (Long) map.get("mrid");
                List<String> tagCodeList = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{mmrid, 0, 1});
                if (tagCodeList.size() > 0 && tagCodeList != null) {
                    for (String tagCode : tagCodeList) {
                        //组织生活
                        if (Todo.MeetType.ORG_LIFE_MEETING.getNameCode().equals(tagCode)) {
                            b1 = true;
                        }
                        //民主评议党员
                        if (Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode().equals(tagCode)) {
                            b2 = true;
                        }
                        //党委中心组学习
                        if ("theoretical_study".equals(tagCode)) {
                            b3 = true;
                        }
                    }
                }
            }
        } else {
            TodoItemDTO tdItem = getTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecord.getMrtype(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
            if (tdItem != null) {
                rollBackTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecord.getMrtype(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
            } else {
                if (Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode().equals(meetingRecord.getMrtype())) {
                    //要修改的待办状态
                    List<String> tagCodeList = generalSqlComponent.query(MeetingSqlCode.getMeetingTagcodeByMrid, new Object[]{meetingRecord.getMrid(), 0, 1});
                    Boolean b11 = false, b22 = false, b33 = false;
                    if (tagCodeList.size() > 0 && tagCodeList != null) {
                        for (String tagCode : tagCodeList) {
                            //组织生活
                            if (Todo.MeetType.ORG_LIFE_MEETING.getNameCode().equals(tagCode)) {
                                b11 = true;
                            }
                            //民主评议党员
                            if (Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode().equals(tagCode)) {
                                b22 = true;
                            }
                        }
                    }
                    if (!b1 && !b2 && !b3) {
                        //是否存在此待办
                        TodoItemDTO todoItem = getTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
                        if (todoItem != null) {
                            rollBackTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, meetingRecord.getMrtype(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
                        } else {
                            setTodoItemDTO(meetingRecord.getOrgid(),
                                    Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode() + meetingRecord.getMryear() + getThisQuarter(meetingRecord.getMrmonth()),
                                    Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameText() + "(" + getQuarter(meetingRecord.getMrmonth()) + ")",
                                    Todo.FunctionURL.BRANCH_PARTY_CONGRE2.getPath() + meetingRecord.getMryear());
                        }
                    }
                    //组织生活
                    if (!b1 && b11) {
                        TodoItemDTO todoItem = getTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, Todo.MeetType.ORG_LIFE_MEETING.getNameCode(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
                        if (todoItem != null) {
                            rollBackTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, Todo.MeetType.ORG_LIFE_MEETING.getNameCode(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
                        } else {
                            setTodoItemDTO(meetingRecord.getOrgid(),
                                    Todo.MeetType.ORG_LIFE_MEETING.getNameCode() + meetingRecord.getMryear() + halfAYear(meetingRecord.getMrmonth()),
                                    Todo.MeetType.ORG_LIFE_MEETING.getNameText() + "(" + getHalfAYear(meetingRecord.getMrmonth()) + ")",
                                    Todo.FunctionURL.ORG_LIFE_METING.getPath());
                        }

                    }
                    //民主评议党员
                    if (!b2 && b22) {
                        TodoItemDTO todoItem = getTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, Todo.MeetType.ORG_LIFE_MEETING.getNameCode(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
                        if (todoItem != null) {
                            rollBackTodoItem(meetingRecord.getOrgid(), Todo.ItemType.TYPE_MAINTAIN_MEETING, Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode(), 1, meetingRecord.getMryear(), meetingRecord.getMrmonth());
                        } else {
                            setTodoItemDTO(meetingRecord.getOrgid(),
                                    Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode() + meetingRecord.getMryear(),
                                    Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameText() + "(" + meetingRecord.getMryear() + "年)",
                                    Todo.FunctionURL.APPR_MEMBERS.getPath());
                        }
                    }
                } else {
                    String switchCode = switchCode(meetingRecord.getMrtype(), meetingRecord.getMryear(), meetingRecord.getMrmonth());
                    Map<String, Object> map = switchPath(meetingRecord.getMrtype(), meetingRecord.getMryear(), meetingRecord.getMrmonth());
                    if (map != null && !(map.size() < 1)) {
                        setTodoItemDTO(meetingRecord.getOrgid(), switchCode,
                                (String) map.get("name"),
                                (String) map.get("path"));
                    }
                }
            }
        }

        return "success";
    }

    /**
     * 修改会议待办状态
     *
     * @param orgid
     * @param itemType
     * @param type
     * @param compid
     * @param mryear
     * @param mrmonth
     */
    private void rollBackTodoItem(Long orgid, Todo.ItemType itemType, String type, long compid, Integer mryear, Integer mrmonth) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        String s = switchCode(type, mryear, mrmonth);
        itemDTO.setCategory(itemType.getCode());
        itemDTO.setCompid(compid);
        itemDTO.setBizid(s);
        itemDTO.setOrgid(orgid);
        todoItemService.rollBackTodoItem(itemDTO);
    }

    private TodoItemDTO getTodoItem(Long orgid, Todo.ItemType itemType, String type, long compid, Integer mryear, Integer mrmonth) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        String s = switchCode(type, mryear, mrmonth);
        itemDTO.setCategory(itemType.getCode());
        itemDTO.setCompid(compid);
        itemDTO.setBizid(s);
        itemDTO.setOrgid(orgid);
        return todoItemService.getTodoItem(itemDTO);
    }

    private void setTodoItemDTO(Long orgid, String Code, String name, String path) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        itemDTO.setCategory(Todo.CONSTANT_ONE);
        itemDTO.setName(name);
        itemDTO.setPath(path);
        itemDTO.setUserid(Long.valueOf(Todo.CONSTANT_ONE));
        itemDTO.setUsername("生成");
        itemDTO.setTime(new Date());
        itemDTO.setCompid(Long.valueOf(Todo.CONSTANT_ONE));
        itemDTO.setBizid(Code);
        itemDTO.setOrgid(orgid);
        itemDTO.setState(Todo.CONSTANT_ZERO);
        todoItemService.createTodoItem(itemDTO);
    }

    /**
     * 获取当前月份半年
     *
     * @param month
     * @return
     */
    public static String getHalfAYear(int month) {
        switch (month) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return "上半年";
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                return "下半年";
        }

        return null;
    }

    /**
     * 获取当前月份季节
     *
     * @param month
     * @return
     */
    public static String getQuarter(int month) {
        switch (month) {
            case 1:
            case 2:
            case 3:
                return "第1季度";
            case 4:
            case 5:
            case 6:
                return "第2季度";
            case 7:
            case 8:
            case 9:
                return "第3季度";
            case 10:
            case 11:
            case 12:
                return "第4季度";
        }
        return null;
    }

    private Map<String, Object> switchPath(String type, Integer mryear, Integer mrmonth) {
        Map<String, Object> map = new HashMap<>();
        switch (type) {
            case "appraisal_of_members":
                map.put("bizid", "appraisal_of_members" + mryear);
                map.put("name", Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameText() + "(" + mryear + "年)");
                map.put("path", Todo.FunctionURL.APPR_MEMBERS.getPath());
                return map;
            case "branch_committee":
                map.put("bizid", "branch_committee" + mryear + mrmonth);
                map.put("name", Todo.MeetType.BRANCH_COMMITTEE.getNameText() + "(" + mrmonth + "月份)");
                map.put("path", Todo.FunctionURL.BRANCH_COMMITTEE1.getPath() + mryear);
                return map;
            case "party_group_meeting":
                map.put("bizid", "party_group_meeting" + mryear + mrmonth);
                map.put("name", Todo.MeetType.PARTY_GROUP_MEETING.getNameText() + "(" + mrmonth + "月份)");
                map.put("path", Todo.FunctionURL.PARTY_GROUP_MEETING3.getPath() + mryear);
                return map;
            case "party_lecture":
                map.put("bizid", "party_lecture" + mryear + getThisQuarter(mrmonth));
                map.put("name", Todo.MeetType.PARTY_LECTURE.getNameText() + "(" + getQuarter(mrmonth) + ")");
                map.put("path", Todo.FunctionURL.PARTY_LECTURE4.getPath() + mryear);
                return map;
            case "org_life_meeting":
                map.put("bizid", "org_life_meeting" + mryear + halfAYear(mrmonth));
                map.put("name", Todo.MeetType.ORG_LIFE_MEETING.getNameText() + "(" + getHalfAYear(mrmonth) + ")");
                map.put("path", Todo.FunctionURL.ORG_LIFE_METING.getPath());
                return map;
            case "theme_party_day":
                map.put("bizid", "theme_party_day" + mryear + mrmonth);
                map.put("name", Todo.MeetType.THEME_PARTY_DAY.getNameText() + "(" + mrmonth + "月份)");
                map.put("path", Todo.FunctionURL.THEME_PARTY_DAY.getPath());
                return map;
            case "branch_party_congress":
                map.put("bizid", "branch_party_congress" + mryear + getThisQuarter(mrmonth));
                map.put("name", Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameText() + "(" + getQuarter(mrmonth) + ")");
                map.put("path", Todo.FunctionURL.BRANCH_PARTY_CONGRE2.getPath() + mryear);
                return map;
            case "democratic_life_meeting":
                map.put("bizid", "democratic_life_meeting" + mryear);
                map.put("name", Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameText() + "(" + mryear + "年)");
                map.put("path", Todo.FunctionURL.DE_LIFE_MEETING.getPath());
                return map;
        }
        return map;
    }

    public void modifyDemocracyReview(String mrid, String userid, String situation, Integer submit, Session session) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mrid",mrid);
        paramMap.put("userid",userid);
        paramMap.put("situation",situation);
        paramMap.put("submit",submit);
        if(0==submit){
            generalSqlComponent.update(SqlCode.modifySituationByMridUserid,paramMap);
        }
        if(1==submit){
            generalSqlComponent.update(SqlCode.modifySubmitByMrid,paramMap);
        }
    }

    public Pager pageDemocracyReview(String mrid,Pager pager, Session session) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mrid",mrid);
        generalSqlComponent.pageQuery(SqlCode.pageDemocracyReview,paramMap,pager);
       List<Map<String, Object>>  rows = (List<Map<String, Object>>) pager.getRows();
        //是否提交
       boolean b= false;
        for (Map<String, Object> map:rows) {
            int submit = (int) map.get("submit");
            String userid = String.valueOf(map.get("userid"));
            if(1==submit){
                b=true;
            }
            Map<String,Object>  dy = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getTDyInfo2,new Object[]{userid});
            map.put("name",dy.get("xm"));
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("date",rows);
        resultMap.put("submit",b);
        pager.setRows(resultMap);
        return pager;
    }
}
