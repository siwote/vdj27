package com.lehand.meeting.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.cache.CacheComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 缓存数据加载
 */
@Component
public class CacheServiceUtil {

    public static final String DCITITEMCACHE = "DCITITEMCACHE";

    @Resource
    private CacheComponent cacheComponent;

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 加载数据字典内容
     */
    public void getAllDictItem() {
        //查询字典信息
        List<Map<String, Object>> list =generalSqlComponent.query(MeetingSqlCode.queryDcitParams, new Object[] {});
        Map<String, String> dictItemMap = new HashMap<>(list.size());

        //将查询结果放置缓存集合中
        for(Map<String, Object> map : list) {
            dictItemMap.put(getKey(map.get("code").toString(),map.get("item_code").toString()), map.get("item_name").toString());
        }

        cacheComponent.set(DCITITEMCACHE, JSON.toJSONString(dictItemMap));
    }

    /**
     * 生成组合Key
     * @param code
     * @param itemCode
     * @return
     */
    private String getKey(String code,String itemCode) {
        return code+ StringConstant.UNDERLINE+itemCode;
    }

    /**
     * 获取键值内容
     * @param code
     * @param itemCode
     * @return
     */
    public String getName(String code,Object itemCode) {
        if (null==itemCode) {
            return StringConstant.EMPTY;
        }

        String cache = cacheComponent.get(DCITITEMCACHE);
        if(StringUtils.isEmpty(cache)) {
            this.getAllDictItem();
            cache = cacheComponent.get(DCITITEMCACHE);
        }

        JSONObject object = (JSONObject) JSONObject.parse(cache);
        String name = (String) object.get(getKey(code,itemCode.toString()));
        return name==null?StringConstant.EMPTY:name;
    }

}
