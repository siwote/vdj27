package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: MeetingDemocracyParticipants
 * @Description: 民主生活会参与人表
 * @Author lx
 * @DateTime 2021-04-29 9:50
 */
@ApiModel(value = "民主生活会参与人表", description = "民主生活会参与人表")
public class MeetingDemocracyParticipants implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "民主生活会会议id", name = "democracyid")
    private long democracyid;
    @ApiModelProperty(value = "用户ID", name = "userid")
    private String userid;
    @ApiModelProperty(value = "用户身份证号码", name = "idcard")
    private String idcard;


    public long getDemocracyid() {
    return democracyid;
  }

  public void setDemocracyid(long democracyid) {
    this.democracyid = democracyid;
  }


  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }


  public String getIdcard() {
    return idcard;
  }

  public void setIdcard(String idcard) {
    this.idcard = idcard;
  }

}
