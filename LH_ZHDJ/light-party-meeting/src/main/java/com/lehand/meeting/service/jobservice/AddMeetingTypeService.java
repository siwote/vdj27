package com.lehand.meeting.service.jobservice;


import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.constant.common.MessageEnum;
import com.lehand.meeting.pojo.TkTaskRemindList;
import com.lehand.meeting.service.CheckStandardService;
import com.lehand.meeting.service.MeetingService;
import com.lehand.meeting.utils.NumberUtils;
import com.lehand.meeting.utils.OrgUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.sound.sampled.Line;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class AddMeetingTypeService {
    //记录日志
	private static final Logger logger = LogManager.getLogger(AddMeetingTypeService.class);
	//是否正在创建任务
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);
	//线程池
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	@Resource private OrgUtils orgUtils;

	@Resource
	private CheckStandardService checkStandardService;

	@Transactional(rollbackFor=Exception.class)
	public void addMeetingType() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			//先删除所有的统计信息
			generalSqlComponent.delete(MeetingSqlCode.delMeetingTypeStatistics,new Object[]{1});
			//查询所有会议类型
			List<Map<String,Object>> lists = generalSqlComponent.query(MeetingSqlCode.listSysParamByParamkey,new Object[]{1});
			//查询所有上级党组织
			List<Map<String,Object>> lists2 = generalSqlComponent.query(MeetingSqlCode.listOrgDzz,new Object[]{1});

			logger.info("定时统计开始...");
			for (final Map<String,Object> stringObjectMap : lists2) {
				Map param = new HashMap();
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						try {
							int remarks1 = 1;
							//获取机构orgid
							String organid = generalSqlComponent.query(MeetingSqlCode.getOrganidByorgCode,new Object[]{stringObjectMap.get("orgcode")});
							String path = orgUtils.getPath(organid);
							param.put("path",path);
							param.put("organid",organid);
							//查询上级党组织下面的所有党支部
							List<Map<String,Object>> organs = generalSqlComponent.query(MeetingSqlCode.listChildOrgan,param);
							for (Map<String, Object> maps : lists) {
								//排序
								switch(maps.get("paramkey") .toString()){
									case "branch_committee":
										remarks1=1;
										break;
									case "branch_party_congress":
										remarks1=2;
										break;
									case "party_group_meeting":
										remarks1=3;
										break;
									case "party_lecture":
										remarks1=4;
										break;
									case "org_life_meeting":
										remarks1=5;
										break;
									case "theme_party_day":
										remarks1=6;
										break;
									case "appraisal_of_members":
										remarks1=7;
										break;
									case "theoretical_study":
										remarks1=8;
										break;
									case "committee_meeting":
										remarks1=11;
										break;
									default:
										break;
								}
								//各会议类型的达标和未达标数
								Map organs2 = getStandardNum(1L, param, maps, true, organs);
								maps.put("standardNum",organs2.get("standardNum"));
								maps.put("noStandardNum",organs2.get("noStandardNum"));
								generalSqlComponent.insert(MeetingSqlCode.addMeetingTypeStatistics,new Object[]{1,stringObjectMap.get("orgcode"),maps.get("paramkey"),organs2.get("standardNum"),organs2.get("noStandardNum"),remarks1,maps.get("paramvalue")});
							}
							Map<String, Object> map = new HashMap<>();
							map.put("paramkey","election");
							map.put("paramvalue","换届情况");
							Map organs3 = getStandardNum(1L, param, map, false,organs);
							map.put("standardNum",organs3.get("electionNormalNum"));
							map.put("noStandardNum",organs3.get("electionOverdueNum"));
							generalSqlComponent.insert(MeetingSqlCode.addMeetingTypeStatistics,new Object[]{1,stringObjectMap.get("orgcode"),map.get("paramkey"),organs3.get("electionNormalNum"),organs3.get("electionOverdueNum"),9,map.get("paramvalue")});

							//重点工作情况
							Map<String,Object> data = getZdgzNum(stringObjectMap);
							map.put("paramkey","keytask");
							map.put("paramvalue","重点工作");
							map.put("standardNum",data.get("standardNum"));
							map.put("noStandardNum",data.get("noStandardNum"));
							generalSqlComponent.insert(MeetingSqlCode.addMeetingTypeStatistics,new Object[]{1,stringObjectMap.get("orgcode"),map.get("paramkey"),map.get("standardNum"),map.get("noStandardNum"),10,map.get("paramvalue")});
						} catch (Exception e) {
							logger.error("统计异常",e);
						}
					}
				});
			}

			logger.info("定时统计结束...");
		} catch (Exception e) {
			logger.error("定时统计异常",e);
		} finally {
			CREATE.set(false);
		}
	}

	private Map<String,Object> getZdgzNum(Map<String, Object> stringObjectMap) {
		Map<String,Object> parms = new HashMap<String,Object>(5);
		parms.put("orgcode", stringObjectMap.get("orgcode"));
		parms.put("status1", NumberUtils.num30);
		parms.put("status2", NumberUtils.num40);
		parms.put("status3", NumberUtils.num45);
		//已发布
		Integer noStandardNum = generalSqlComponent.query(MeetingSqlCode.getZdgzNum,parms);
		//为完成
		parms.put("status4", NumberUtils.num50);
		Integer standardNum = generalSqlComponent.query(MeetingSqlCode.getZdgzNum,parms);
		Map<String,Object> map = new HashMap<String,Object>(2);
		map.put("standardNum",standardNum);
		map.put("noStandardNum",noStandardNum);
		return map;
	}

	private Map getStandardNum(Long compid, Map param, Map meet, Boolean flag,List<Map<String,Object>> organs) throws ParseException {
		//获取当前组织下所有党支部
		Session session = new Session();
		session.setCompid(compid);
		//各类型会议达标支部数
		int standardNum = 0;
		//各类型会议未达标支部数
		int noStandardNum = 0;
		//换届情况正常换届支部数
		int electionNormalNum = 0;
		//换届情况逾期换届支部数
		int electionOverdueNum = 0;
		int baseConveneNum = 0;
		if(flag){
			baseConveneNum = getMeetingConveneNum(meet.get("paramkey").toString(), compid);
		}
		for (Map<String, Object> organ : organs) {
			if(flag){
				int actualConveneNum = checkStandardService.getConvenedByOrg(Integer.valueOf(DateEnum.YYYY.format()),
						organ.get("organcode").toString(),meet,session);
				if(baseConveneNum<=actualConveneNum){
					standardNum++;
				}else{
					noStandardNum++;
				}
			}else{
				Map map = generalSqlComponent.query(MeetingSqlCode.getLastElectionDate,new Object[]{organ.get("organid")});
				if(map==null || StringUtils.isEmpty(map.get("hjdate")) || DateEnum.now().after(DateEnum.YYYYMMDDHHMMDD.parse(map.get("hjdate").toString()))){
					electionOverdueNum++;
				}else{
					electionNormalNum++;
				}
			}
		}
		Map map = new HashMap();
		map.put("standardNum",standardNum);
		map.put("noStandardNum",noStandardNum);
		map.put("electionNormalNum",electionNormalNum);
		map.put("electionOverdueNum",electionOverdueNum);
		return map;
	}


	private int getMeetingConveneNum(String mtype, Long compid) {
		//获取会议召开频率
		Map<String,Object> map = generalSqlComponent.query(SqlCode.getSysParam2,new Object[]{compid,mtype});
		return checkStandardService.getExpectedvalue(Integer.valueOf(DateEnum.YYYY.format()),map);
        /*int conveneNum = 0;
        Integer num = Integer.valueOf(map.get("remarks2").toString());
        //获取当前月份所属季度
        int i = checkStandardService.getCurrentQuarter(Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7)));
        //获取当前月份所属半年
        int j = checkStandardService.getCurrHalfyear(Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7)));
        //获取当前月份
        int k = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
        switch (map.get("remarks3").toString()) {
            case SysConstants.mrequirementtype.perfyear:
                //每年召开 民主评议党员
                conveneNum = num;
                break;
            case SysConstants.mrequirementtype.perhalfyear:
                //每半年召开 组织生活会
                conveneNum = num * j;
                break;
            case SysConstants.mrequirementtype.perseason:
                //每季度召开 支部党员大会 党课
                conveneNum = num * i;
                break;
            default:
                //每月召开 支部委员会 党小组会 主题党日活动 党委中心组学习
                conveneNum = num * k;
                break;
        }
        return conveneNum;*/
	}



	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
