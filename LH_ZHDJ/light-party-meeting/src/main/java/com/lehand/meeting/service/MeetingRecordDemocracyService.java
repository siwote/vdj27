package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfo;
import com.lehand.meeting.constant.DemocracySqlCode;
import com.lehand.meeting.constant.common.MeetingPMtypeEnum;
import com.lehand.meeting.dto.MeetingDemocracyParticipantsDto;
import com.lehand.meeting.dto.MeetingRecordDemocracyDto;
import com.lehand.meeting.dto.MeetingRecordDto;
import com.lehand.meeting.pojo.MeetingAbsentSubject;
import com.lehand.meeting.pojo.MeetingDemocracyParticipants;
import com.lehand.meeting.pojo.MeetingRecordDemocracy;
import com.lehand.meeting.pojo.MeetingRecordSubject;
import com.lehand.meeting.utils.MeetingUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class MeetingRecordDemocracyService {

    private static final Logger logger = LogManager.getLogger(MeetingRecordDemocracyService.class);

    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    MeetingService meetingService;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;

    @Transactional(rollbackFor = Exception.class)
    public void saveBeforeDemocracy(MeetingRecordDemocracyDto dto, Session session) {
        //start 验证必填参数
        checkBeforeParams(dto);
        //end
        //添加创建时间和创建人id
        dto.setBeforecreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dto.setBeforecreateuserid(session.getUserid());
        //添加待审核组织（上级党委）
        Map<String, Object> map = new HashMap<>();
        map.put("organcode",dto.getOrgcode());
        TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
        if(tDzzInfo==null){
            LehandException.throwException("不存在当前党组织");
        }
        //获取上级党委编码
        String parentcode = tDzzInfo.getParentcode();
        if("0".equals(parentcode)){//一级集团党委
            dto.setStatus(3);//审核通过
        }else{
            parentcode=getParentDWCodeByCode(tDzzInfo.getParentcode());
            dto.setStatus(1);//会前待审核
        }
        dto.setAuditorgcode(parentcode);
        //保存民主生活会记录
        Long id = generalSqlComponent.insert(DemocracySqlCode.saveBeforeDemocracy, dto);
        dto.setId(id);
        if("0".equals(parentcode)){//一级集团党委
            dto.setBeforeupdatetime(DateEnum.YYYYMMDDHHMMDD.format());
            dto.setBeforecreateuserid(session.getUserid());
            generalSqlComponent.update(DemocracySqlCode.updateBefore, dto);
        }
        //保存会议地点
        meetingService.saveMeetingPlace(dto.getPlace(), dto.getOrgcode(), session);
        //保存参会人
        saveParticipants(dto,session);
    }

    private void checkBeforeParams(MeetingRecordDemocracyDto dto) {
        if(StringUtils.isEmpty(dto.getTopic())){
            LehandException.throwException("会议主题不能为空");
        }
        if(StringUtils.isEmpty(dto.getPlanstarttime())&&StringUtils.isEmpty(dto.getPlanendtime())){
            LehandException.throwException("会议计划召开时间不能为空");
        }
        if(dto.getListParticipantsDto().size()<1){
            LehandException.throwException("会议参会人不能为空");
        }
        if(StringUtils.isEmpty(dto.getPlace())){
            LehandException.throwException("会议地点不能为空");
        }
        if(StringUtils.isEmpty(dto.getContext())){
            LehandException.throwException("会议主要议程不能为空");
        }
    }

    /**
     * 获取上级党委编码
     * @param parentcode
     * @return
     */
    private String getParentDWCodeByCode(String parentcode) {
        TDzzInfo tDzzInfo = getTDzzInfoByOrgCode(parentcode);
        if(SysConstants.ZZLB.DW.equals(tDzzInfo.getZzlb())){//是否是党委
            return parentcode;
        }
        parentcode = getParentDWCodeByCode(tDzzInfo.getParentcode());
        return parentcode;
    }

    /**
     * 保存民主会会议参会人
     * @param dto
     * @param session
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveParticipants(MeetingRecordDemocracyDto dto, Session session) {
        for (MeetingDemocracyParticipants participants:dto.getListParticipantsDto()) {
            //start 校验必要参数
            if(StringUtils.isEmpty(participants.getIdcard())){
                LehandException.throwException("参会人身份证号不能为空");
            }
            if(StringUtils.isEmpty(participants.getUserid())){
                LehandException.throwException("参会人id不能为空");
            }
            //end
            participants.setDemocracyid(dto.getId());
            generalSqlComponent.insert(DemocracySqlCode.saveParticipants,participants);
        }
    }
    @Transactional(rollbackFor = Exception.class)
    public void saveAfterDemocracy(MeetingRecordDemocracyDto dto, Session session) throws Exception {
        //start 验证必填参数
        checkAfterParams(dto);
        if(StringUtils.isEmpty(String.valueOf(dto.getBeforecreatetime()))){
            LehandException.throwException("会前审核的时间不能为空");
        }
        //end
        //添加创建时间和创建人id
        String nowDateFormat = DateEnum.YYYYMMDDHHMMDD.format();//当前时间
        dto.setAftercreatetime(nowDateFormat);
        dto.setAftercreateuserid(session.getUserid().intValue());
        //添加待审核组织（上级党委）
        Map<String, Object> map = new HashMap<>();
        map.put("organcode",dto.getOrgcode());
        TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
        if(tDzzInfo==null){
            LehandException.throwException("不存在当前党组织");
        }
        //是否逾期报送
        Date throughDate = DateEnum.parseStr(dto.getBeforeupdatetime(), "yyyy-MM-dd HH:mm:ss");//会前审核通过时间
        Date nowDate = DateEnum.parseStr(nowDateFormat, "yyyy-MM-dd HH:mm:ss");//当前时间
        boolean b = differentDaysByMillisecond(throughDate, nowDate);
        if(b){
            dto.setNormalstatus(2);//正常
        }else{
            dto.setNormalstatus(3);//逾期
        }
        //添加会议状态会后待审核
        dto.setStatus(4);
        //获取上级党委编码
        String parentcode = tDzzInfo.getParentcode();
        if("0".equals(parentcode)){//一级集团党委
            dto.setStatus(6);//审核通过
        }
        generalSqlComponent.update(DemocracySqlCode.saveAfterDemocracy,dto);
        if("0".equals(parentcode)){//一级集团党委
            //查询民主会会议信息
            MeetingRecordDemocracyDto result = generalSqlComponent.query(DemocracySqlCode.getDemocracyById, new Object[]{dto.getId()});
            if(result==null){
                LehandException.throwException("不存在当前民主生活会会议");
            }
            meetingService.addMeeting(getMeetingRecordDtoParam(result,session),session,1);
        }
    }
    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     * @param date1
     * @param date2
     * @return
     */
    public boolean differentDaysByMillisecond(Date date1,Date date2)
    {
        long a = date2.getTime() - date1.getTime();//时间间隔
        long b = 15*24*3600*1000;//15天

        return a<b;
    }
    private void checkAfterParams(MeetingRecordDemocracyDto dto) {
        if(StringUtils.isEmpty(dto.getActualstarttime())&&StringUtils.isEmpty(dto.getActualendtime())){
            LehandException.throwException("会议实际召开时间不能为空");
        }
        if(StringUtils.isEmpty(String.valueOf(dto.getId()))){
            LehandException.throwException("会议id不能为空");
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteDemocracy(Integer id) {
        //删除民主生活会参会人
        generalSqlComponent.delete(DemocracySqlCode.deleteParticipants,new Object[]{id});
        //删除民主生活会
        generalSqlComponent.delete(DemocracySqlCode.deleteDemocracy,new Object[]{id});
    }
    @Transactional(rollbackFor = Exception.class)
    public void updateBefore(MeetingRecordDemocracyDto dto, Session session) {
        //start 验证必填参数
        if(StringUtils.isEmpty(String.valueOf(dto.getId()))){
            LehandException.throwException("会议id不能为空");
        }
        checkBeforeParams(dto);
        //end
        //添加编辑时间和编辑人id
        dto.setBeforeupdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dto.setBeforeupdateuserid(session.getUserid().intValue());
        //修改为会前待审核
        dto.setStatus(1);
        generalSqlComponent.update(DemocracySqlCode.updateBefore,dto);
        //删除民主生活会参会人
        generalSqlComponent.delete(DemocracySqlCode.deleteParticipants,new Object[]{dto.getId()});
        //添加参会人
        saveParticipants(dto,session);
    }
    @Transactional(rollbackFor = Exception.class)
    public void updateAfter(MeetingRecordDemocracyDto dto, Session session) {
        //start 验证必填参数
        checkAfterParams(dto);
        //end
        //添加编辑时间和编辑人id
        dto.setAfterupdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dto.setAfterupdateuserid(session.getUserid().intValue());
        //修改为会后待审核
        dto.setStatus(4);
        generalSqlComponent.update(DemocracySqlCode.updateAfter,dto);
    }

    public MeetingRecordDemocracyDto get(Integer id) {
        //start 验证必填参数
        if(StringUtils.isEmpty(String.valueOf(id))){
            LehandException.throwException("会议id不能为空");
        }
        //end
        //查询民主会会议信息
        MeetingRecordDemocracyDto result = generalSqlComponent.query(DemocracySqlCode.getDemocracyById, new Object[]{id});
        if(result==null){
            LehandException.throwException("不存在当前民主生活会会议");
        }
        //参会人信息
        List<MeetingDemocracyParticipantsDto> listParticipantsDto = generalSqlComponent.query(DemocracySqlCode.listParticipantsById, new Object[]{id});
        //添加参会人信息
        result.setListParticipantsDto(listParticipantsDto);
        //会前材料
        List<Map<String, Object>> listBeforefiles = listFile(result.getBeforefiles());
        result.setListBeforefiles(listBeforefiles);
        //（会后）情况报告材料
        List<Map<String, Object>> listSituationfiles = listFile(result.getSituationfiles());
        result.setListSituationfiles(listSituationfiles);
        // (会后）会议记录材料
        List<Map<String, Object>> listRecordfiles = listFile(result.getRecordfiles());
        result.setListRecordfiles(listRecordfiles);
        //（会后）班子对照检查材料
        List<Map<String, Object>> listTeamcontrastfiles = listFile(result.getTeamcontrastfiles());
        result.setListTeamcontrastfiles(listTeamcontrastfiles);
        //（会后）上次整改完成情况材料
        List<Map<String, Object>> listRectificationfiles = listFile(result.getRectificationfiles());
        result.setListRectificationfiles(listRectificationfiles);
        //（会后）班子征求意见情况报告材料
        List<Map<String, Object>> listTeamaskforfiles = listFile(result.getTeamaskforfiles());
        result.setListTeamaskforfiles(listTeamaskforfiles);
        //（会后）班子及成员''四清单''公示情况材料
        List<Map<String, Object>> listTeammembersfiles = listFile(result.getTeammembersfiles());
        result.setListTeammembersfiles(listTeammembersfiles);
        return result;
    }
    /**
     * 获取附件list集合
     * @param fileids
     * @return
     */
    private List<Map<String, Object>> listFile(String fileids) {
        List<Map<String,Object>> listFile = new ArrayList<>();
        if(!org.springframework.util.StringUtils.isEmpty(fileids)){
            String[] fileid = fileids.split(",");
            for (String id:fileid) {
                Map<String,Object> m =  generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",1);put("id",id);}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    m.put("fileid",id);
                    m.put("flpath", String.format("%s%s", downIp, dir));
                    m.put("name",m.get("name").toString().substring(0,m.get("name").toString().lastIndexOf(".")));
                    listFile.add(m);
                }
            }
        }
        return  listFile;
    }
    @Transactional(rollbackFor = Exception.class)
    public void auditDemocracy(int id, int status, String context, Session session) throws Exception {
//        会前(2:审核驳回.3:审核通过).会后(5:审核驳回.6:审核通过)
        //start 验证必填参数
        if(StringUtils.isEmpty(String.valueOf(id))){
            LehandException.throwException("id不能为空");
        }
        if(StringUtils.isEmpty(String.valueOf(status))){
            LehandException.throwException("状态不能为空");
        }
        if(2==status||4==status){
            if(StringUtils.isEmpty(context)){
                LehandException.throwException("驳回意见不能为空");
            }
        }
        //end
        //查询民主会会议信息
        MeetingRecordDemocracyDto result = generalSqlComponent.query(DemocracySqlCode.getDemocracyById, new Object[]{id});
        if(result==null){
            LehandException.throwException("不存在当前民主生活会会议");
        }
        MeetingRecordDemocracy democracy = new MeetingRecordDemocracy();
        democracy.setStatus(status);
        democracy.setId(id);
        if(2==status||3==status){//会前审核
            democracy.setBeforeupdatetime(DateEnum.YYYYMMDDHHMMDD.format());
            democracy.setBeforeupdateuserid(session.getUserid().intValue());
            democracy.setBeforerejected(context);//会前会前审核意见
        }
        if(5==status||6==status){//会后审核
            //会前修改时间和修改人id
            democracy.setBeforeupdatetime(result.getBeforeupdatetime());
            democracy.setBeforeupdateuserid(result.getBeforeupdateuserid());
            democracy.setBeforerejected(result.getBeforerejected());//会前会前审核意见
            //会后修改时间和修改人id
            democracy.setAfterupdatetime(DateEnum.YYYYMMDDHHMMDD.format());
            democracy.setAfterupdateuserid(session.getUserid().intValue());
            democracy.setAfterrejected(context);
        }
        if(6==status){//会后通过
            //添加时间召开年份月份
            democracy.setMryear(MeetingUtil.getYear(result.getActualstarttime()));
            democracy.setMrmonth(MeetingUtil.getMonth(result.getActualstarttime()));
        }
        //会后审核通过
        generalSqlComponent.update(DemocracySqlCode.auditDemocracy,democracy);
        if(6==status){//会后通过
            //会后材料审批通过,添加旧的民主会议记录,之前逻辑不修改
             meetingService.addMeeting(getMeetingRecordDtoParam(result,session),session,1);
        }
    }


    /**
     * 设置民主生活会会议信息
     * @param result
     * @param session
     * @return
     */
    private MeetingRecordDto getMeetingRecordDtoParam(MeetingRecordDemocracyDto result, Session session) {
        MeetingRecordDto meetingRecordDto = new MeetingRecordDto();
        String orgcode = result.getOrgcode();//组织编码
        //参与人
        List<MeetingDemocracyParticipantsDto> listParticipantsDto = generalSqlComponent.query(DemocracySqlCode.listParticipantsById, new Object[]{result.getId()});
        String idcard = listParticipantsDto.get(0).getIdcard();
        String name = listParticipantsDto.get(0).getName();
        String userid = listParticipantsDto.get(0).getUserid();
        //主持人
        meetingRecordDto.setModeratorid(idcard);
        meetingRecordDto.setModeratoridcard(idcard);
        meetingRecordDto.setModeratorname(name);
        //记录人
        meetingRecordDto.setRecorderid(idcard);
        meetingRecordDto.setRecorderidcard(idcard);
        meetingRecordDto.setRecordername(name);
        //会议参与人
        List<MeetingRecordSubject> metingRecordSubjects = new ArrayList<>();
        MeetingRecordSubject meetingRecordSubject = new MeetingRecordSubject();
        meetingRecordSubject.setUsertype("0");
        meetingRecordSubject.setBusitype(2);
        meetingRecordSubject.setUserid(userid);
        meetingRecordSubject.setIdcard(idcard);
        meetingRecordSubject.setUsername(name);
        metingRecordSubjects.add(meetingRecordSubject);
        meetingRecordDto.setMeetingRecordSubjects(metingRecordSubjects);
        //会议缺席人员
        List<MeetingAbsentSubject> meetingAbsentSubjects= new ArrayList<>();
        meetingRecordDto.setMeetingAbsentSubject(meetingAbsentSubjects);
        meetingRecordDto.setContext(result.getContext());//内容
        meetingRecordDto.setMryear(MeetingUtil.getYear(result.getActualstarttime()));//年份
        meetingRecordDto.setMrmonth(MeetingUtil.getMonth(result.getActualstarttime()));//月份
        meetingRecordDto.setStarttime(result.getActualstarttime());//开始时间
        meetingRecordDto.setEndtime(result.getActualendtime());//结束时间
        meetingRecordDto.setOrgname(getTDzzInfoByOrgCode(orgcode).getDzzmc());//查询组织名称
        meetingRecordDto.setMrtype(MeetingPMtypeEnum.DEMOCRATIC_LIFE_MEETING.getCode());//民主会议类型
        meetingRecordDto.setOrgcode(orgcode);//组织编码
        meetingRecordDto.setPlace(result.getPlace());//会议地点
        meetingRecordDto.setCompid(1L);
        meetingRecordDto.setStatus("2");//已经提交状态
        meetingRecordDto.setCreatetime(result.getBeforecreatetime());//创建时间
        meetingRecordDto.setCreateuserid(result.getBeforecreateuserid());//创建人id
        return meetingRecordDto;
    }

    /**
     *
     * @param session
     * @param pager  分页
     * @param topic  会议主题
     * @param status 状态
     * @param normalstatus  是否正常
     * @param starttime 开始时间
     * @param endtime 结束时间
     * @param type 审核页面或详情列表页
     * @return
     */
    public Pager pageDemocracy(Session session, Pager pager, String topic, String status, String normalstatus, String starttime, String endtime,String orgcode, String type) {
        //校验必填参数 start
        if(StringUtils.isEmpty(type)){
            LehandException.throwException("type参数不能为空");
        }
        if(!StringUtils.isEmpty(starttime)||!StringUtils.isEmpty(endtime)){
            if(StringUtils.isEmpty(starttime)||StringUtils.isEmpty(endtime)){
                LehandException.throwException("会议筛选时间不能有一为空");
            }
        }
        //end
        Map<String, Object> paramMap = new HashMap<>();
        //开始时间
        paramMap.put("starttime",starttime);
        //结束时间
        //会议主题
        paramMap.put("topic",topic);
        //状态(会前(1:待审核.2:审核驳回.3:审核通过).会后(4:待审核.5:审核驳回.6:审核通过))
        paramMap.put("status",status);
        //是否正常
        paramMap.put("normalstatus",normalstatus);
        paramMap.put("endtime",endtime);
        //组织编码type 1:查看审核列表页。2:查看详情列表页。
//        String orgcode = session.getCurrentOrg().getOrgcode();
        if("1".equals(type)){
            paramMap.put("status","1,4");//会前待审核或会后待审核
            paramMap.put("auditorgcode",orgcode);
        }else{
            paramMap.put("orgcode",orgcode);
        }
        //列表页查询
        generalSqlComponent.pageQuery(DemocracySqlCode.pageDemocracy,paramMap,pager);
        List<MeetingRecordDemocracyDto> democracyDtos = (List<MeetingRecordDemocracyDto>) pager.getRows();
        for (MeetingRecordDemocracyDto democracyDto: democracyDtos) {
            //查询组织名称
            democracyDto.setOrgName(getTDzzInfoByOrgCode(democracyDto.getOrgcode()).getDzzmc());
        }
        return pager;
    }

    /**
     * 获取党组织信息
     * @param orgcode
     * @return
     */
    private TDzzInfo getTDzzInfoByOrgCode(String orgcode) {
        Map<String, Object> map = new HashMap<>();
        map.put("organcode",orgcode);
        TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
        if(tDzzInfo==null){
            LehandException.throwException("不存在当前党组织");
        }
        return tDzzInfo;
    }

}
