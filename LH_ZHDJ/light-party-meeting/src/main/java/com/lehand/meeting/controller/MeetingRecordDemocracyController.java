package com.lehand.meeting.controller;


import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingRecordDemocracyDto;
import com.lehand.meeting.service.MeetingRecordDemocracyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: MeetingRecordDemocracyController
 * @Description: 民主生活会相关接口
 * @Author lx
 * @DateTime 2021-04-29 9:50
 */
@Api(value = "民主生活会", tags = {"民主生活会相关接口"})
@RestController
@RequestMapping("/horn/meetingdemocracy")
public class MeetingRecordDemocracyController extends BaseController {
    @Resource
    private MeetingRecordDemocracyService democracyService;

    /**
     * Description:添加会前材料
     * @param dto
     * @return
     * @Author lx
     * @DateTime 2021-04-29 9:50
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "添加会前材料", notes = "添加会前材料", httpMethod = "POST")
    @RequestMapping("/addBefore")
    public Message addBefore(@RequestBody MeetingRecordDemocracyDto dto) {
        Message msg = new Message();
        try {
            democracyService.saveBeforeDemocracy(dto, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("添加失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("添加失败");
        }
        return msg;
    }

    /**
     * Description:添加会后材料
     * @param dto
     * @return
     * @Author lx
     * @DateTime 2021-04-29 11:40
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "添加会后材料", notes = "添加会后材料", httpMethod = "POST")
    @RequestMapping("/addAfter")
    public Message addAfter(@RequestBody MeetingRecordDemocracyDto dto) {
        Message msg = new Message();
        try {
            democracyService.saveAfterDemocracy(dto, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("添加失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("添加失败");
        }
        return msg;
    }

    /**
     * Description:删除民主生活会
     * @param id
     * @return
     * @Author lx
     * @DateTime 2021-04-29 14:00
     */
    @OpenApi(optflag = 3)
    @ApiOperation(value = "删除民主生活会", notes = "删除民主生活会", httpMethod = "POST")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "组织生活会id", dataType = "int", paramType = "query", example = "")
    )
    @RequestMapping("/delete")
    public Message delete(Integer id) {
        Message msg = new Message();
        try {
            democracyService.deleteDemocracy(id);
            msg.success();
        } catch (LehandException e) {
            msg.fail("删除失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("删除失败");
        }
        return msg;
    }

    /**
     * Description:编辑会前材料
     * @param dto
     * @return
     * @Author lx
     * @DateTime 2021-04-29 14:00
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "编辑会前材料", notes = "编辑会前材料", httpMethod = "POST")
    @RequestMapping("/updateBefore")
    public Message updateBefore(@RequestBody MeetingRecordDemocracyDto dto) {
        Message msg = new Message();
        try {
            democracyService.updateBefore(dto, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("编辑失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("编辑失败");
        }
        return msg;
    }

    /**
     * Description:编辑会后材料
     * @param dto
     * @return
     * @Author lx
     * @DateTime 2021-04-29 14:00
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "编辑会后材料", notes = "编辑会后材料", httpMethod = "POST")
    @RequestMapping("/updateAfter")
    public Message updateAfter(@RequestBody MeetingRecordDemocracyDto dto) {
        Message msg = new Message();
        try {
            democracyService.updateAfter(dto, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("编辑失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("编辑失败");
        }
        return msg;
    }

    /**
     * Description: 查看民主生活会
     * @param id
     * @return
     * @Author lx
     * @DateTime 2021-04-29 14:00
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看民主生活会", notes = "查看民主生活会", httpMethod = "POST")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "组织生活会id", dataType = "int", example = "")
    )
    @RequestMapping("/get")
    public Message get(Integer id) {
        Message msg = new Message();
        try {
            MeetingRecordDemocracyDto dto = democracyService.get(id);
            msg.setData(dto);
            msg.success();
        } catch (LehandException e) {
            msg.fail("查询失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("查询失败");
        }
        return msg;
    }


    /**
     * Description: 审核
     * @Author lx
     * @DateTime 2021-04-29 15:55
     * @param id
     * @param status
     * @param context
     * @return
     */
    @OpenApi(optflag=2)
    @ApiOperation(value = "审核", notes = "审核", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "组织生活会id", required = true, dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "会前(2:审核驳回.3:审核通过).会后(5:审核驳回.6:审核通过)", required = true, dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "context", value = "审核意见", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/auditDemocracy")
    public Message auditDemocracy(int id, int status,String context) {
        Message msg = new Message();
        try {
            democracyService.auditDemocracy(id,status,context, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("审核失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("审核失败");
        }
        return msg;
    }

    /**
     * Description: 查询民主生活会列表页
     * @author lx
     * @date 2201-04-29 17:20
     * @param topic
     * @param status
     * @param normalstatus
     * @param starttime
     * @param endtime
     * @param type
     * @param pager
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询民主生活会列表页", notes = "查询民主生活会列表页", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", value = "条件筛选:（会议主题）", required = false, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "条件筛选:(会前(1:待审核.2:审核驳回.3:审核通过).会后(4:待审核.5:审核驳回.6:审核通过))。全部：传空或不传。", required = false, dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "normalstatus", value = "条件筛选:(1:未汇报、2:正常汇报、3:逾期汇报)。全部：传空或不传。", required = false, dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "starttime", value = "开始时间", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "endtime", value = "开始时间", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "orgcode", value = "组织编码", dataType = "String",required = true,  defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "1:审核列表页.2:详情列表页", dataType = "String",required = true, defaultValue = "")
    })
    @RequestMapping("/pageDemocracy")
    public Message pageDemocracy(String topic,String status,String normalstatus,String starttime,String endtime,String orgcode,String type, Pager pager) {
        Message message = new Message();
        try {
            pager = democracyService.pageDemocracy(getSession(),pager,topic,status,normalstatus,starttime,endtime,orgcode,type);
            message.setData(pager);
            message.success();
        } catch (Exception e) {
            message.setMessage("查询民主生活会列表页出错"+e.getMessage());
        }
        return message;
    }
}
