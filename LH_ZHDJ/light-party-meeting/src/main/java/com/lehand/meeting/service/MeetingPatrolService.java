package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.HJXXSqlCode;
import com.lehand.horn.partyorgan.dto.TDzzHjxxInfoDto;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.constant.common.SysConstants;
import com.lehand.meeting.dto.*;
import com.lehand.meeting.utils.ExcelUtils;
import com.lehand.meeting.utils.OrgUtils;
import com.lehand.todo.constant.Todo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.*;

/**
 * 会议巡查service
 */
@Service
public class MeetingPatrolService {
    @Resource
    private GeneralSqlComponent generalSqlComponent;
    @Resource private OrgUtils orgUtils;
    @Resource private CheckStandardService checkStandardService;
    @Resource private MeetingService meetingSerivce;
    /**
     *党建巡查分页第一层列表
     * @param session
     * @param req
     * @return
     */
    public  Map<String,Object> pageTotalPatrol(Session session, MPatrolPagerReq req) {
        Map<String,Object> data = new HashMap<String,Object>(2);
        if(StringUtils.isEmpty(req.getOrganid())){
            LehandException.throwException("组织机构id不能为空");
            return data;
        }
        if(!StringUtils.isEmpty(req.getStarttime())&&!StringUtils.isEmpty(req.getEndtime())){
            req.setStarttime(req.getStarttime()+"-00");
            req.setEndtime(req.getEndtime()+"-31");
        }

        // 根据类别查询会议类型
        List<MeetingTypeDto> headlist = getHeadList(req.getMrtype(),session);
        data.put("pager",new PagerUtils<Map<String,Object>>(getTotalPatrolPager(session,req,headlist)));
        data.put("head",headlist);
        return data;
    }

    /**
     * 根据会议类型获取表头
     * @param mrtype
     * @return
     */
    private List<MeetingTypeDto> getHeadList(String mrtype,Session session) {
        //判断是不是标签类会议
        boolean flag =  checkStandardService.tagMeeting(mrtype);
        List<MeetingTypeDto> list = new ArrayList<MeetingTypeDto>();
        if(flag||mrtype.equals(SysConstants.MTYPE.THEME_PARTY_DAY)||mrtype.equals(SysConstants.MTYPE.DEMOCRATIC_LIFE_MEETING)||mrtype.equals(SysConstants.MTYPE.COMMITTEE_MEETING)){
            list =  generalSqlComponent.query(MeetingSqlCode.getMeetingtypeListByparamKey,new Object[]{mrtype,session.getCompid()});
        }else{
             list = generalSqlComponent.query(MeetingSqlCode.getMeetingtypeListByUparamKey,new Object[]{SysConstants.MTYPE.TMOC,session.getCompid()});
        }

        if(list.size()<1){
            return list;
        }
        //拼接前端表格标识
        String key = "remark";
        int i = 1;
        for (MeetingTypeDto  dto:list) {
            dto.setProp(key+i);
            i++;
        }
        return list;
    }

    /**
     * 巡查分页第一层数据
     * @param session
     * @param req
     * @param headlist
     * @return
     */
    private Pager getTotalPatrolPager(Session session, MPatrolPagerReq req, List<MeetingTypeDto> headlist) {
        Pager pager = req.getPager();
        pager = generalSqlComponent.pageQuery(MeetingSqlCode.MeetingPatrolList,req,pager);
        List<MeetingPatrolListDto> list = (List<MeetingPatrolListDto>) pager.getRows();
        if(list.size()<1){return pager;}
        makeList(list,req,session,headlist);
        pager.setRows(list);
        return pager;
    }

    /**
     * 处理统计数据
     * @param list
     * @param req
     * @param session
     * @param headlist
     */
    private void makeList(List<MeetingPatrolListDto> list, MPatrolPagerReq req, Session session, List<MeetingTypeDto> headlist) {

        Integer pathNum  = orgUtils.getPathNum(req.getOrganid())+1;

        String childpath = MagicConstant.STR.PATH_STR+pathNum;
        //循环下级组织机构
        for (MeetingPatrolListDto org: list) {
            req.setChildorganid(org.getOrganid());
            req.setChildpath(childpath);
            req.setCompid(session.getCompid());
            Map<String,Object> map =  checkStandardService.doCalculate(req,headlist);
            org.setValuedata(map);
        }

    }



    /**
     * 建巡查弹出框分页展示所有下级支部统计
     * @param session
     * @param req
     * @return
     */
    public Pager pageBranchPatrol(Session session, MPatrolPagerReq req) {
        Pager pager = req.getPager();
        req.setCompid(session.getCompid());
        String path = orgUtils.getPath(req.getOrganid());
        req.setChildpath(path);
        boolean flag = checkStandardService.tagMeeting(req.getMrtype());
        req.setTablename(flag?"view_tag_meeting":"view_meeting");
        Map<String,Object> map = generalSqlComponent.query(SqlCode.getSysParam2,new Object[]{session.getCompid(),req.getMrtype()});
        //达标计算类型
        Object standtype = map.get("remarks3");
        //达标计算基数
        int standvalue = (int) map.get("remarks2");
        int respectedValue = checkStandardService.getRespectedValue(String.valueOf(standtype),standvalue);
        req.setCalvalue(respectedValue);
        checkStandardService.BooleanTImeParam(req);
        if(SysConstants.MTYPE.COMMITTEE_MEETING.equals(req.getMrtype())){
            return  generalSqlComponent.pageQuery(MeetingSqlCode.getMeetingCommittees,req,pager);
        }
        pager = generalSqlComponent.pageQuery(MeetingSqlCode.getMeetingBranches,req,pager);
        return pager;
    }



    /**
     * 党建导出巡查列表数据
     * @param session
     * @param req
     * @return
     */
    public String exportPatrolList(HttpServletResponse response, Session session, MPatrolExportReq req) {
        Pager pager = req.getPager();
        if(pager== null){
            pager = new Pager();
        }
        pager.setPageNo(1);
        pager.setPageSize(9999);
        req.setPager(pager);
        pager = pageBranchPatrol(session,req);

        //内容
        List<Map<String,String>> mapList = (List<Map<String, String>>) pager.getRows();
        List<Map<String,String>> data = new ArrayList<Map<String,String>>();
        if(mapList.size()>0){
            for (Map<String,String> map:mapList) {
                Map<String,String> rs = new LinkedHashMap<String,String>(3);
                rs.put("党组织名称",map.get("dzzmc"));
                rs.put("会议次数",String.valueOf(map.get("covered")));
                rs.put("状态",String.valueOf(map.get("type")).equals(MagicConstant.NUM_STR.NUM_1)?"达标":"不达标");
                data.add(rs);
            }
        }
        ExcelUtils.write(response,req.getTablesStr(),data,req.getTablenames());
        return null;
    }

    /**
     *巡查类型集合
     * @param session
     * @return
     */
    public List<Map<String,Object>> getPatrolTypeList(Session session) {
        List<Map<String,Object>> meetingTypes = generalSqlComponent.query(MeetingSqlCode.listSysParamByUpparamkey,
                new Object[]{SysConstants.MTYPE.OLM_CODE,session.getCompid()});
        for(int i=0;i<meetingTypes.size();i++){
            if(SysConstants.MTYPE.THEORETICAL_STUDY.equals(meetingTypes.get(i).get("paramkey"))){
                meetingTypes.remove(i);
            }
            //支部和党总支巡查不显示党委会
//            if(SysConstants.ZZLB.DZB.equals(session.getCurrentOrg().getOrgtypeid())||SysConstants.ZZLB.DZZ.equals(session.getCurrentOrg().getOrgtypeid())){
                if(SysConstants.MTYPE.COMMITTEE_MEETING.equals(meetingTypes.get(i).get("paramkey"))){
                    meetingTypes.remove(i);
                }
//            }
        }
        return meetingTypes;
    }

    /**
     *  工作巡查支部概览
     * @param session
     * @return
     */
    public Map<String,Object> getPatrolBranchInfo(String organid,Session session) throws ParseException {
        Map<String,Object> map = new HashMap<String,Object>(6);

        //党员人数
        Long mcount = generalSqlComponent.query(SqlCode.getMemberCountByOrganid,new Object[]{organid});
        map.put("mcount",mcount);

        //支部书记
        Map<String,Object> infoBc = generalSqlComponent.query(SqlCode.getDzzsjByIdMap,new Object[]{organid});
        map.put("secretary",infoBc ==null?MagicConstant.STR.EMPTY_STR:infoBc.get("dzzsj"));

        //班子成员数
        Long tmcount = generalSqlComponent.query(SqlCode.getBzcyCountByOrganid,new Object[]{organid,0});
        map.put("tmcount",tmcount);

        //党建达标状态
        map.put("standardstatus",booleanStandardStatus(organid, session));

        //上届换届时间
//        Map<String,Object> hjxx = generalSqlComponent.query(SqlCode.getLatestHjxxByOrganid,new Object[]{organid});
//        String jmdate = hjxx==null? MagicConstant.STR.EMPTY_STR:String.valueOf(hjxx.get("jmdate"));
//        map.put("hjdate",hjxx==null?"":hjxx.get("hjdate"));
//        map.put("jmdate","null".equals(jmdate)?null:jmdate);
//        map.put("hjstatus",false);

        Map<String,Object> dzzinfo= generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getDzzInfo,new Object[]{organid});
        String hjdate ="";
        String jmdate ="";
        String hjstatus ="";
        if(dzzinfo!=null){
            TDzzHjxxInfoDto hjxxInfoDto= generalSqlComponent.query(HJXXSqlCode.getHJXXLimit1,new Object[]{dzzinfo.get("organcode")});
            if(hjxxInfoDto!=null){
                hjdate = hjxxInfoDto.getActualdate();
                jmdate = hjxxInfoDto.getPlandate();
                hjstatus = String.valueOf(hjxxInfoDto.getStatus());
            }
        }
        map.put("hjdate",hjdate);
        map.put("jmdate",jmdate);
        map.put("hjstatus","1".equals(hjstatus)?true:false);
        if(StringUtils.isEmpty(jmdate)){
            map.put("term",MagicConstant.STR.EMPTY_STR);
            return map;
        }
        //判断换届是否逾期
//        if(!"null".equals(jmdate)&&jmdate!=null&&DateEnum.now().before(DateEnum.YYYYMMDDHHMMDD.parse(jmdate))){
//            map.put("hjstatus",true);
//        }
        return map;
    }


    /**
     * 党建达标状态
     * @param session
     * @return
     */
    private boolean booleanStandardStatus(String organid,Session session) {
        String organcode =  orgUtils.getOrganidByOrganId(organid);
        if(StringUtils.isEmpty(organcode)){
            return false;
        }
        List<Map<String,Object>> list = listMeetingHeldInfo(organcode,session);
        if(list.size()<1){
            return true;
        }
        for(Map<String,Object> map :list){
            if(MagicConstant.NUM_STR.NUM_0.equals(String.valueOf(map.get("statandardstatus")))){
                return false;
            }
        }
        return true;
    }

    /**
     * 会议召开情况
     * @param orgcode
     * @param session
     * @return
     */
    public List<Map<String,Object>> listMeetingHeldInfo(String orgcode, Session session) {

        List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        List<Map<String,Object>> list = getMeetingTypes(session);
        Calendar cale = Calendar.getInstance();
        int year = cale.get(Calendar.YEAR);
        int month = cale.get(Calendar.MONTH)+1;

        if(list.size()<1){
            return result;
        }
        for (Map<String,Object> map:list ){
            Map<String,Object> rmap = getMeetingHeldInfo(session,map,year,month,orgcode);
            //去掉党委会
            if(Todo.MeetType.COMMITTEE_MEETING.getNameCode().equals(rmap.get("mtype"))){
                continue;
            }
            result.add(rmap);
        }
        return result;
    }

    /**
     *  会议信息
     * @return
     */
    private Map<String, Object> getMeetingHeldInfo(Session session,Map<String,Object> map,int year,int month,String orgcode) {

        Map<String,Object> result = new HashMap<String,Object>();

        //当前时间段内达标要求
        int requirement = Integer.parseInt(String.valueOf(map.get("remarks2")));

        result.put("delaynum",checkStandardService.getOverdueNum(map,orgcode,year));
//        result.put("currconverd",checkStandardService.getCurrConvenedByOrg(map,orgcode,year,month,session));
        //当前时间段召开数
        int currconverd = checkStandardService.getQuanConvenedByOrg(map.get("paramkey"),orgcode,year,month,map.get("remarks3"),session);
        result.put("currconverd",currconverd);
//        result.put("statandardstatus",checkStandardService.getStandardStatusByOrg(year,orgcode,map,session));
        result.put("statandardstatus",currconverd>=requirement?1:0);
        result.put("convended",checkStandardService.getConvenedByOrg(year,orgcode,map,session));
        result.put("requirement",map.get("remarks4"));
        result.put("requirementtype",String.valueOf(map.get("remarks3")));
        result.put("mtype",String.valueOf(map.get("paramkey")));
        result.put("mtypeName",String.valueOf(map.get("paramvalue")));
        return result;
    }

    /**
     *  会议类别和参数
     * @return
     */
    public List<Map<String, Object>> getMeetingTypes(Session session) {
        List<Map<String,Object>> meet = new ArrayList<Map<String,Object>>();
        meet = meetingSerivce.recursionOrg(SysConstants.MTYPE.OLM_CODE,meet,session.getCompid());
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for ( Map<String,Object> map:meet) {
            //工作巡查去掉民主生活会
            if(String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.DEMOCRATIC_LIFE_MEETING)){
                continue;
            }
            //工作巡查去掉中心学习
            if(String.valueOf(map.get("paramkey")).equals(SysConstants.MTYPE.THEORETICAL_STUDY)){
                continue;
            }
            list.add(map);
        }
        return list;
    }
    /**
     *党建党委巡查分页第一层列表
     * @param session
     * @param req
     * @return
     */
    public  Map<String,Object> pageCommitteePatrol(Session session, MPatrolPagerReq req) {
        Map<String,Object> data = new HashMap<String,Object>(2);
        if(StringUtils.isEmpty(req.getOrganid())){
            LehandException.throwException("组织机构id不能为空");
            return data;
        }
        if(!StringUtils.isEmpty(req.getStarttime())&&!StringUtils.isEmpty(req.getEndtime())){
            req.setStarttime(req.getStarttime()+"-00");
            req.setEndtime(req.getEndtime()+"-31");
        }

        // 根据类别查询会议类型
        List<MeetingTypeDto> headlist = getHeadList(req.getMrtype(),session);
        data.put("pager",new PagerUtils<Map<String,Object>>(pageCommitteePatrol(session,req,headlist)));
        data.put("head",headlist);
        return data;
    }
    /**
     * 巡查分页第一层数据
     * @param session
     * @param req
     * @param headlist
     * @return
     */
    private Pager pageCommitteePatrol(Session session, MPatrolPagerReq req, List<MeetingTypeDto> headlist) {
        Pager pager = req.getPager();
        pager = generalSqlComponent.pageQuery(MeetingSqlCode.MeetingPatrolListByZzlb,req,pager);
        List<MeetingPatrolListDto> list = (List<MeetingPatrolListDto>) pager.getRows();
        if(list.size()<1){return pager;}
        makeList(list,req,session,headlist);
        pager.setRows(list);
        return pager;
    }
}
