package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * @ClassName: MeetingRecordDemocracy
 * @Description: 民主生活会会议记录表
 * @Author lx
 * @DateTime 2021-04-29 9:50
 */
@ApiModel(value = "民主生活会会议记录表", description = "民主生活会会议记录表")
public class MeetingRecordDemocracy implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *自增长主键
     */
    @ApiModelProperty(value = "自增长主键", name = "id")
    private long id;
    /**
     *会议主题
     */
    @ApiModelProperty(value = "会议主题", name = "topic")
    private String topic;
    /**
     *组织编码
     */
    @ApiModelProperty(value = "组织编码", name = "orgcode")
    private String orgcode;
    /**
     *待审核组织编码
     */
    @ApiModelProperty(value = "待审核组织编码", name = "auditorgcode")
    private String auditorgcode;
    /**
     *年份
     */
    @ApiModelProperty(value = "年份", name = "mryear")
    private Integer mryear;
    /**
     *月份
     */
    @ApiModelProperty(value = "月份", name = "mrmonth")
    private Integer mrmonth;
    /**
     *会前计划开始时间(YY-MM-dd hh:mm)
     */
    @ApiModelProperty(value = "会前计划开始时间(YY-MM-dd hh:mm)", name = "planstarttime")
    private String planstarttime;
    /**
     *会前计划结束时间(YY-MM-dd hh:mm)
     */
    @ApiModelProperty(value = "会前计划结束时间(YY-MM-dd hh:mm)", name = "planendtime")
    private String planendtime;
    /**
     *会后实际结束时间(YY-MM-dd hh:mm)
     */
    @ApiModelProperty(value = "会后实际结束时间(YY-MM-dd hh:mm)", name = "actualendtime")
    private String actualendtime;
    /**
     *会后实际开始时间(YY-MM-dd hh:mm)
     */
    @ApiModelProperty(value = "会后实际开始时间(YY-MM-dd hh:mm)", name = "actualstarttime")
    private String actualstarttime;
    /**
     *会议地点
     */
    @ApiModelProperty(value = "会议地点", name = "place")
    private String place;
    /**
     *会前(1:待审核.2:审核驳回.3:审核通过).会后(4:待审核.5:审核驳回.6:审核通过)
     */
    @ApiModelProperty(value = "会前(1:待审核.2:审核驳回.3:审核通过).会后(4:待审核.5:审核驳回.6:审核通过)", name = "status")
    private long status;
    /**
     *1:未汇报、2:正常汇报、3:逾期汇报
     */
    @ApiModelProperty(value = "1:未汇报、2:正常汇报、3:逾期汇报", name = "normalstatus")
    private long normalstatus;
    /**
     *主要议程
     */
    @ApiModelProperty(value = "主要议程", name = "context")
    private String context;
    /**
     *（会前）材料,附件ids,以逗号'',''拼接
     */
    @ApiModelProperty(value = "（会前）材料,附件ids,以逗号'',''拼接", name = "beforefiles")
    private String beforefiles;
    /**
     *会前审核意见
     */
    @ApiModelProperty(value = "会前审核意见", name = "beforerejected")
    private String beforerejected;
    /**
     *会前创建时间
     */
    @ApiModelProperty(value = "会前创建时间", name = "beforecreatetime")
    private String beforecreatetime;
    /**
     *会前创建人ID
     */
    @ApiModelProperty(value = "会前创建人ID", name = "beforecreateuserid")
    private long beforecreateuserid;
    /**
     *会前修改时间
     */
    @ApiModelProperty(value = "会前修改时间", name = "beforeupdatetime")
    private String beforeupdatetime;
    /**
     *会前修改人ID
     */
    @ApiModelProperty(value = "会前修改人ID", name = "beforeupdateuserid")
    private Integer beforeupdateuserid;
    /**
     *（会后）情况报告材料,附件ids,以逗号'',''拼接
     */
    @ApiModelProperty(value = "（会后）情况报告材料,附件ids,以逗号'',''拼接", name = "situationfiles")
    private String situationfiles;
    /**
     *（会后）会议记录材料,附件ids,以逗号'',''拼接
     */
    @ApiModelProperty(value = "（会后）会议记录材料,附件ids,以逗号'',''拼接", name = "recordfiles")
    private String recordfiles;
    /**
     *（会后）班子对照检查材料,附件ids,以逗号'',''拼接
     */
    @ApiModelProperty(value = "（会后）班子对照检查材料,附件ids,以逗号'',''拼接", name = "teamcontrastfiles")
    private String teamcontrastfiles;
    /**
     *（会后）上次整改完成情况材料,附件ids,以逗号'',''拼接
     */
    @ApiModelProperty(value = "（会后）上次整改完成情况材料,附件ids,以逗号'',''拼接", name = "rectificationfiles")
    private String rectificationfiles;
    /**
     *
     */
    @ApiModelProperty(value = "（会后）班子征求意见情况报告材料,附件ids,以逗号'',''拼接", name = "teamaskforfiles")
    private String teamaskforfiles;
    /**
     *（会后）班子及成员''四清单''公示情况材料,附件ids,以逗号'',''拼接
     */
    @ApiModelProperty(value = "（会后）班子及成员''四清单''公示情况材料,附件ids,以逗号'',''拼接", name = "teammembersfiles")
    private String teammembersfiles;
    /**
     *会后审核意见
     */
    @ApiModelProperty(value = "会后审核意见", name = "afterrejected")
    private String afterrejected;
    /**
     *会后创建时间
     */
    @ApiModelProperty(value = "会后创建时间", name = "aftercreatetime")
    private String aftercreatetime;
    /**
     *会后创建人ID
     */
    @ApiModelProperty(value = "会后创建人ID", name = "aftercreateuserid")
    private Integer aftercreateuserid;
    /**
     *会后修改时间
     */
    @ApiModelProperty(value = "会后修改时间", name = "afterupdatetime")
    private String afterupdatetime;
    /**
     * 会后修改人ID
     */
    @ApiModelProperty(value = "会后修改人ID", name = "afterupdateuserid")
    private Integer afterupdateuserid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getAuditorgcode() {
        return auditorgcode;
    }

    public void setAuditorgcode(String auditorgcode) {
        this.auditorgcode = auditorgcode;
    }

    public Integer getMryear() {
        return mryear;
    }

    public void setMryear(Integer mryear) {
        this.mryear = mryear;
    }

    public Integer getMrmonth() {
        return mrmonth;
    }

    public void setMrmonth(Integer mrmonth) {
        this.mrmonth = mrmonth;
    }

    public String getPlanstarttime() {
        return planstarttime;
    }

    public void setPlanstarttime(String planstarttime) {
        this.planstarttime = planstarttime;
    }

    public String getPlanendtime() {
        return planendtime;
    }

    public void setPlanendtime(String planendtime) {
        this.planendtime = planendtime;
    }

    public String getActualendtime() {
        return actualendtime;
    }

    public void setActualendtime(String actualendtime) {
        this.actualendtime = actualendtime;
    }

    public String getActualstarttime() {
        return actualstarttime;
    }

    public void setActualstarttime(String actualstarttime) {
        this.actualstarttime = actualstarttime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getNormalstatus() {
        return normalstatus;
    }

    public void setNormalstatus(long normalstatus) {
        this.normalstatus = normalstatus;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getBeforefiles() {
        return beforefiles;
    }

    public void setBeforefiles(String beforefiles) {
        this.beforefiles = beforefiles;
    }

    public String getBeforerejected() {
        return beforerejected;
    }

    public void setBeforerejected(String beforerejected) {
        this.beforerejected = beforerejected;
    }

    public String getBeforecreatetime() {
        return beforecreatetime;
    }

    public void setBeforecreatetime(String beforecreatetime) {
        this.beforecreatetime = beforecreatetime;
    }

    public long getBeforecreateuserid() {
        return beforecreateuserid;
    }

    public void setBeforecreateuserid(long beforecreateuserid) {
        this.beforecreateuserid = beforecreateuserid;
    }

    public String getBeforeupdatetime() {
        return beforeupdatetime;
    }

    public void setBeforeupdatetime(String beforeupdatetime) {
        this.beforeupdatetime = beforeupdatetime;
    }

    public Integer getBeforeupdateuserid() {
        return beforeupdateuserid;
    }

    public void setBeforeupdateuserid(Integer beforeupdateuserid) {
        this.beforeupdateuserid = beforeupdateuserid;
    }

    public String getSituationfiles() {
        return situationfiles;
    }

    public void setSituationfiles(String situationfiles) {
        this.situationfiles = situationfiles;
    }

    public String getRecordfiles() {
        return recordfiles;
    }

    public void setRecordfiles(String recordfiles) {
        this.recordfiles = recordfiles;
    }

    public String getTeamcontrastfiles() {
        return teamcontrastfiles;
    }

    public void setTeamcontrastfiles(String teamcontrastfiles) {
        this.teamcontrastfiles = teamcontrastfiles;
    }

    public String getRectificationfiles() {
        return rectificationfiles;
    }

    public void setRectificationfiles(String rectificationfiles) {
        this.rectificationfiles = rectificationfiles;
    }

    public String getTeamaskforfiles() {
        return teamaskforfiles;
    }

    public void setTeamaskforfiles(String teamaskforfiles) {
        this.teamaskforfiles = teamaskforfiles;
    }

    public String getTeammembersfiles() {
        return teammembersfiles;
    }

    public void setTeammembersfiles(String teammembersfiles) {
        this.teammembersfiles = teammembersfiles;
    }

    public String getAfterrejected() {
        return afterrejected;
    }

    public void setAfterrejected(String afterrejected) {
        this.afterrejected = afterrejected;
    }

    public String getAftercreatetime() {
        return aftercreatetime;
    }

    public void setAftercreatetime(String aftercreatetime) {
        this.aftercreatetime = aftercreatetime;
    }

    public Integer getAftercreateuserid() {
        return aftercreateuserid;
    }

    public void setAftercreateuserid(Integer aftercreateuserid) {
        this.aftercreateuserid = aftercreateuserid;
    }

    public String getAfterupdatetime() {
        return afterupdatetime;
    }

    public void setAfterupdatetime(String afterupdatetime) {
        this.afterupdatetime = afterupdatetime;
    }

    public Integer getAfterupdateuserid() {
        return afterupdateuserid;
    }

    public void setAfterupdateuserid(Integer afterupdateuserid) {
        this.afterupdateuserid = afterupdateuserid;
    }
}
