package com.lehand.meeting.service;

import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.pojo.MeetingThemeDate;
import com.lehand.meeting.pojo.MeetingThemeDay;
import com.lehand.meeting.utils.MeetingUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;

@Service
public class MeetingThemeService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 校验当前组织机构是否设置的党员活动日
     * @param session
     * @param year
     * @return
     */
    public boolean checkSetting(Session session, int year) {
        if(StringUtils.isEmpty(year)) {
            LehandException.throwException("请设置查询的年份！");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("year", year);
        paramMap.put("compid", session.getCompid());
        paramMap.put("orgcode", session.getCurrentOrg().getOrgcode());
        List<MeetingThemeDate> dateList = generalSqlComponent.query(MeetingSqlCode.queryMtdate, paramMap);

        if(dateList.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 设置党员活动日日期
     * @param session
     * @param meetingThemeDate
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public MeetingThemeDate setDate(Session session, MeetingThemeDate meetingThemeDate) {
        if(StringUtils.isEmpty(meetingThemeDate.getStartdate())) {
            LehandException.throwException("请先设置开始日期！");
        }
        if(StringUtils.isEmpty(meetingThemeDate.getEnddate())) {
            LehandException.throwException("请先设置结束日期！");
        }
        if(StringUtils.isEmpty(meetingThemeDate.getDay())) {
            LehandException.throwException("请先设置党员活动日(具体日期)！");
        }

        meetingThemeDate.setCompid(session.getCompid());
        meetingThemeDate.setOrgcode(session.getCurrentOrg().getOrgcode());
        meetingThemeDate.setOrgid(session.getCurrentOrg().getOrgid());
        meetingThemeDate.setOrgname(session.getCurrentOrg().getOrgname());
        meetingThemeDate.setYear(MeetingUtil.getYear(meetingThemeDate.getStartdate()));
        meetingThemeDate.setStatus(0);

        //判断是否设置了党员活动日
        MeetingThemeDate meetingThemeDate1 = getDate(session, meetingThemeDate.getYear());
        if(meetingThemeDate1 != null) {
            //更新
            meetingThemeDate.setId(meetingThemeDate1.getId());
            generalSqlComponent.update(MeetingSqlCode.updateMeetingThemeDate, meetingThemeDate);
        } else {
            Long id = generalSqlComponent.insert(MeetingSqlCode.saveMeetingThemeDate, meetingThemeDate);
            meetingThemeDate.setId(id);
        }

        //活动日期保存
        setDay(session, meetingThemeDate);

        return meetingThemeDate;
    }

    /**
     * 设置具体的党员活动日的日期
     * @param session
     * @param meetingThemeDate
     */
    public void setDay(Session session, MeetingThemeDate meetingThemeDate) {
        MeetingThemeDay meetingThemeDay = new MeetingThemeDay();
        meetingThemeDay.setCompid(session.getCompid());
        meetingThemeDay.setId(meetingThemeDate.getId());
        meetingThemeDay.setOrgcode(session.getCurrentOrg().getOrgcode());
        meetingThemeDay.setStatus(0);
        meetingThemeDay.setYear(meetingThemeDate.getYear());

        //删除之前的记录
        generalSqlComponent.delete(MeetingSqlCode.delMtDayById, meetingThemeDay);

        //细化日期
        List<String> dateList = MeetingUtil.getDayList(meetingThemeDate.getStartdate(), meetingThemeDate.getEnddate(), meetingThemeDate.getDay());
        for(String date : dateList) {
            meetingThemeDay.setRemindtime(date);
            generalSqlComponent.insert(MeetingSqlCode.saveMeetingThemeDay, meetingThemeDay);
        }
    }

    /**
     * 查询党员活动日的范围
     * @param session
     * @param year
     * @return
     */
    public MeetingThemeDate getDate(Session session, int year) {
        if(StringUtils.isEmpty(year)) {
            LehandException.throwException("请设置查询的年份！");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("year", year);
        paramMap.put("compid", session.getCompid());
        paramMap.put("orgcode", session.getCurrentOrg().getOrgcode());
        List<MeetingThemeDate> dateList = generalSqlComponent.query(MeetingSqlCode.queryMtdate, paramMap);
        if(dateList.size() > 0) {
            return dateList.get(0);
        }
        return null;
    }

    /**
     * 返回具体的党员活动日的日期
     * @param session
     * @param year
     * @return
     */
    public MeetingThemeDay getDay(Session session, int year) {
        if(StringUtils.isEmpty(year)) {
            LehandException.throwException("请设置查询的年份！");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("year", year);
        paramMap.put("compid", session.getCompid());
        paramMap.put("orgcode", session.getCurrentOrg().getOrgcode());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String month = (calendar.get(Calendar.MONTH) +1) < 10 ? "0" + (calendar.get(Calendar.MONTH) +1):String.valueOf((calendar.get(Calendar.MONTH) +1));
        paramMap.put("remindtime", String.format("%s-%s", calendar.get(Calendar.YEAR), month));
        List<MeetingThemeDay> dayList = generalSqlComponent.query(MeetingSqlCode.queryMtDay, paramMap);
        if(dayList.size() > 0) {
            return dayList.get(0);
        }
        return null;
    }

}
