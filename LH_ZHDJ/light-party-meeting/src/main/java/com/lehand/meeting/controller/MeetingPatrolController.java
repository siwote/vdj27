package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.meeting.dto.MPatrolExportReq;
import com.lehand.meeting.dto.MPatrolPagerReq;
import com.lehand.meeting.service.MeetingPatrolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;

import static com.lehand.components.web.interceptors.BaseInterceptor.getSession;

@Api(value = "会议巡查", tags = { "会议巡查" })
@RestController
@RequestMapping("/horn/patrol")
public class MeetingPatrolController {

    @Resource
    MeetingPatrolService meetingPatrolService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "党建指导分页第一层列表", notes = "党建指导分页第一层列表", httpMethod = "POST")
    @RequestMapping("/pagetotal")
    public Message pageTotalPatrol(@RequestBody  MPatrolPagerReq req){
        Message msg = new Message();
        msg.setData(meetingPatrolService.pageTotalPatrol(getSession(),req));
        msg.success();

        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党建指导组织生活党委会分页第一层列表", notes = "党建指导组织生活党委会分页第一层列表", httpMethod = "POST")
    @RequestMapping("/pageCommittee")
    public Message pageCommitteePatrol(@RequestBody  MPatrolPagerReq req){
        Message msg = new Message();
        msg.setData(meetingPatrolService.pageCommitteePatrol(getSession(),req));
        msg.success();

        return msg;
    }
    @OpenApi(optflag=0)
    @ApiOperation(value = "党建指导弹出框分页展示所有下级支部统计", notes = "党建指导弹出框分页展示所有下级支部统计", httpMethod = "POST")
    @RequestMapping("/pagebranch")
    public Message pageBranchPatrol(@RequestBody MPatrolPagerReq req){
        Message msg = new Message();

        msg.setData(meetingPatrolService.pageBranchPatrol(getSession(),req));
        msg.success();

        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党建指导导出巡查列表数据", notes = "党建指导导出巡查列表数据", httpMethod = "POST")
    @RequestMapping("/export")
    public Message exportPatrolList(HttpServletResponse response, @RequestBody MPatrolExportReq req){
        Message msg = new Message();
        msg.setData(meetingPatrolService.exportPatrolList(response,getSession(),req));
        msg.success();

        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党建指导总入口界面", notes = "党建指导总入口界面", httpMethod = "POST")
    @RequestMapping("/gettypelist")
    public Message getPatrolTypeList(){
        Message msg = new Message();
        msg.setData(meetingPatrolService.getPatrolTypeList(getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "支部情况,包含党建状态", notes = "支部情况,包含党建状态", httpMethod = "POST")
    @RequestMapping("/getPatrolBranchInfo")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "32位组织机构id", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    public Message getPatrolBranchInfo(String organid){
        Message msg = new Message();
        try {
            msg.setData(meetingPatrolService.getPatrolBranchInfo(organid,getSession()));
            msg.success();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "支部情况,包含党建状态", notes = "支部情况,包含党建状态", httpMethod = "POST")
    @RequestMapping("/listMeetingHeldInfo")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    public Message listMeetingHeldInfo(String orgcode){
        Message msg = new Message();
        msg.setData(meetingPatrolService.listMeetingHeldInfo(orgcode,getSession()));
        msg.success();
        return msg;
    }
}
