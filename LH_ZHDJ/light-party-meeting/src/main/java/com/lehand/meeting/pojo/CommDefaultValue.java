package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="会议记录", description="会议记录")
public class CommDefaultValue implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户ID", name="compid")
    private Long compid;

    @ApiModelProperty(value="一级分类", name="classify1")
    private String classify1;

    @ApiModelProperty(value="二级分类", name="classify2")
    private String classify2;

    @ApiModelProperty(value="三级分类", name="classify3")
    private String classify3;

    @ApiModelProperty(value="表单值", name="formvalue")
    private String formvalue;

    @ApiModelProperty(value="更新时间", name="updatetime")
    private String updatetime;

    @ApiModelProperty(value="更新人ID", name="updateuserid")
    private Long updateuserid;

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getClassify1() {
        return classify1;
    }

    public void setClassify1(String classify1) {
        this.classify1 = classify1;
    }

    public String getClassify2() {
        return classify2;
    }

    public void setClassify2(String classify2) {
        this.classify2 = classify2;
    }

    public String getClassify3() {
        return classify3;
    }

    public void setClassify3(String classify3) {
        this.classify3 = classify3;
    }

    public String getFormvalue() {
        return formvalue;
    }

    public void setFormvalue(String formvalue) {
        this.formvalue = formvalue;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Long getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(Long updateuserid) {
        this.updateuserid = updateuserid;
    }
}
