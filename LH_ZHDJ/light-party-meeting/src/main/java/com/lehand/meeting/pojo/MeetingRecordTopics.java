package com.lehand.meeting.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class MeetingRecordTopics implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 会议议题id
     */
    @ApiModelProperty(value = "会议议题id", name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 关联会议id
     */
    @ApiModelProperty(value = "关联会议id", name = "mrid")
    private Long mrid;

    /**
     * 议题内容
     */
    @ApiModelProperty(value = "议题内容", name = "topics")
    private String topics;

    /**
     * 是否前置研究  1 是  0 否
     */
    @ApiModelProperty(value = "是否前置研究  1 是  0 否", name = "preresearch")
    private Integer preresearch;

    /**
     * 预留字段1
     */
    @ApiModelProperty(value = "预留字段1", name = "remarks1")
    private Integer remarks1;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks2")
    private Integer remarks2;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks3")
    private String remarks3;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks4")
    private String remarks4;


    /**
     * setter for id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for id
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for mrid
     *
     * @param mrid
     */
    public void setMrid(Long mrid) {
        this.mrid = mrid;
    }

    /**
     * getter for mrid
     */
    public Long getMrid() {
        return mrid;
    }

    /**
     * setter for topics
     *
     * @param topics
     */
    public void setTopics(String topics) {
        this.topics = topics;
    }

    /**
     * getter for topics
     */
    public String getTopics() {
        return topics;
    }

    /**
     * setter for preresearch
     *
     * @param preresearch
     */
    public void setPreresearch(Integer preresearch) {
        this.preresearch = preresearch;
    }

    /**
     * getter for preresearch
     */
    public Integer getPreresearch() {
        return preresearch;
    }

    /**
     * setter for remarks1
     *
     * @param remarks1
     */
    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    /**
     * getter for remarks1
     */
    public Integer getRemarks1() {
        return remarks1;
    }

    /**
     * setter for remarks2
     *
     * @param remarks2
     */
    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    /**
     * getter for remarks2
     */
    public Integer getRemarks2() {
        return remarks2;
    }

    /**
     * setter for remarks3
     *
     * @param remarks3
     */
    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    /**
     * getter for remarks3
     */
    public String getRemarks3() {
        return remarks3;
    }

    /**
     * setter for remarks4
     *
     * @param remarks4
     */
    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    /**
     * getter for remarks4
     */
    public String getRemarks4() {
        return remarks4;
    }

    private Byte firstissue;

    public Byte getFirstissue() {
        return firstissue;
    }

    public void setFirstissue(Byte firstissue) {
        this.firstissue = firstissue;
    }

    /**
     * MeetingRecordTopicsEntity.toString()
     */
    @Override
    public String toString() {
        return "MeetingRecordTopicsEntity{" +
                "id='" + id + '\'' +
                ", mrid='" + mrid + '\'' +
                ", topics='" + topics + '\'' +
                ", preresearch='" + preresearch + '\'' +
                ", remarks1='" + remarks1 + '\'' +
                ", remarks2='" + remarks2 + '\'' +
                ", remarks3='" + remarks3 + '\'' +
                ", remarks4='" + remarks4 + '\'' +
                '}';
    }

}
