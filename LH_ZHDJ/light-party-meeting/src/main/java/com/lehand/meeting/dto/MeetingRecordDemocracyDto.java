package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingDemocracyParticipants;
import com.lehand.meeting.pojo.MeetingRecordDemocracy;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;


/**
 * @ClassName: MeetingRecordDemocracyDto
 * @Description: 民主生活会会议记录表扩展类
 * @Author lx
 * @DateTime 2021-04-29 9:50
 */
@ApiModel(value = "民主生活会会议记录表扩展类", description = "民主生活会会议记录表扩展类")
public class MeetingRecordDemocracyDto extends MeetingRecordDemocracy {
    /**
     * 组织名称
     */
    @ApiModelProperty(value = "组织名称", name = "orgName")
    public String orgName;
    /**
     * 会议参会人信息集合
     */
    @ApiModelProperty(value = "会议参会人信息集合", name = "listParticipantsDto")
    private List<MeetingDemocracyParticipantsDto> listParticipantsDto;

    public List<MeetingDemocracyParticipantsDto> getListParticipantsDto() {
        return listParticipantsDto;
    }

    public void setListParticipantsDto(List<MeetingDemocracyParticipantsDto> listParticipantsDto) {
        this.listParticipantsDto = listParticipantsDto;
    }

    /**
     *  会前材料附件集合
     */
    @ApiModelProperty(value = "会前材料附件集合", name = "listBeforefiles")
    List<Map<String, Object>> listBeforefiles ;
    /**
     * （会后）情况报告材料附件集合
     */
    @ApiModelProperty(value = "（会后）情况报告材料附件集合", name = "listSituationfiles")
    List<Map<String, Object>> listSituationfiles;
    /**
     * (会后）会议记录材料附件集合
     */
    @ApiModelProperty(value = "(会后）会议记录材料附件集合", name = "listRecordfiles")
    List<Map<String, Object>> listRecordfiles ;
    /**
     * （会后）班子对照检查材料附件集合
     */
    @ApiModelProperty(value = "（会后）班子对照检查材料附件集合", name = "listTeamcontrastfiles")
    List<Map<String, Object>> listTeamcontrastfiles;
    /**
     * （会后）上次整改完成情况材料附件集合
     */
    @ApiModelProperty(value = "（会后）上次整改完成情况材料附件集合", name = "listRectificationfiles")
    List<Map<String, Object>> listRectificationfiles ;
    /**
     * （会后）班子征求意见情况报告材料附件集合
     */
    @ApiModelProperty(value = "（会后）班子征求意见情况报告材料附件集合", name = "listTeamaskforfiles")
    List<Map<String, Object>> listTeamaskforfiles ;
    /**
     * （会后）班子及成员''四清单''公示情况材料附件集合
     */
    @ApiModelProperty(value = "（会后）班子及成员''四清单''公示情况材料附件集合", name = "listTeammembersfiles")
    List<Map<String, Object>> listTeammembersfiles;

    public List<Map<String, Object>> getListBeforefiles() {
        return listBeforefiles;
    }

    public void setListBeforefiles(List<Map<String, Object>> listBeforefiles) {
        this.listBeforefiles = listBeforefiles;
    }

    public List<Map<String, Object>> getListSituationfiles() {
        return listSituationfiles;
    }

    public void setListSituationfiles(List<Map<String, Object>> listSituationfiles) {
        this.listSituationfiles = listSituationfiles;
    }

    public List<Map<String, Object>> getListRecordfiles() {
        return listRecordfiles;
    }

    public void setListRecordfiles(List<Map<String, Object>> listRecordfiles) {
        this.listRecordfiles = listRecordfiles;
    }

    public List<Map<String, Object>> getListTeamcontrastfiles() {
        return listTeamcontrastfiles;
    }

    public void setListTeamcontrastfiles(List<Map<String, Object>> listTeamcontrastfiles) {
        this.listTeamcontrastfiles = listTeamcontrastfiles;
    }

    public List<Map<String, Object>> getListRectificationfiles() {
        return listRectificationfiles;
    }

    public void setListRectificationfiles(List<Map<String, Object>> listRectificationfiles) {
        this.listRectificationfiles = listRectificationfiles;
    }

    public List<Map<String, Object>> getListTeamaskforfiles() {
        return listTeamaskforfiles;
    }

    public void setListTeamaskforfiles(List<Map<String, Object>> listTeamaskforfiles) {
        this.listTeamaskforfiles = listTeamaskforfiles;
    }

    public List<Map<String, Object>> getListTeammembersfiles() {
        return listTeammembersfiles;
    }

    public void setListTeammembersfiles(List<Map<String, Object>> listTeammembersfiles) {
        this.listTeammembersfiles = listTeammembersfiles;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
