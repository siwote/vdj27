package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "会议参与人", description = "会议参与人")
public class MeetingRecordSubject implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "业务ID", name = "mrid")
    private Long mrid;

    @ApiModelProperty(value = "用户类型(0:参与人,1:主持人,2:记录人,3:授课人,4谈话人,5被谈话人,6会议被通知对象)", name = "usertype")
    private String usertype;


    @ApiModelProperty(value = "业务类型: 0:会议,1:谈心谈话,2:会议通知", name = "busitype")
    private Integer busitype;


    @ApiModelProperty(value = "职务", name = "post")
    private String post;

    @ApiModelProperty(value = "用户ID", name = "userid")
    private String userid;

    @ApiModelProperty(value = "身份证号", name = "idcard")
    private String idcard;

    @ApiModelProperty(value = "用户名称", name = "username")
    private String username;

    @ApiModelProperty(value = "租户ID", name = "compid")
    private Long compid;

    @ApiModelProperty(value = "备注1", name = "remark1")
    private String remark1;

    @ApiModelProperty(value = "备注2", name = "remark2")
    private String remark2;

    public Integer getBusitype() {
        return busitype;
    }

    public void setBusitype(Integer busitype) {
        this.busitype = busitype;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }


    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public Long getMrid() {
        return mrid;
    }

    public void setMrid(Long mrid) {
        this.mrid = mrid;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }


}