package com.lehand.meeting.utils;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.pojo.TDzzInfoSimple;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

import static com.lehand.components.db.GeneralSqlOperation.getSingleColumn;

@Service
public class OrgUtils {

    @Resource
    GeneralSqlComponent generalSqlComponent;
    /**
     * 获取组织机构Path
     * @param organid
     * @return
     * @author taogang
     */
    public String getPath(String organid) {
        String path = "";
        if(!StringUtils.isEmpty(organid)) {

            TDzzInfoSimple org =generalSqlComponent.query(SqlCode.queryBaseDzzTree,new Object[]{organid});
            if(null != org) {
                String[] oarr = org.getIdPath().split("/");
                path = "path"+(oarr.length-1);
            }
        }
        return path;
    }

    /**
     * 获取组织机构Path
     * @param orgId
     * @return
     * @author taogang
     */
    public Integer getPathNum(String orgId) {
        Integer num = 0;
        if(!StringUtils.isEmpty(orgId)) {
            TDzzInfoSimple org =generalSqlComponent.query(MeetingSqlCode.get_meeting_path_num,new Object[]{orgId});
            if(null != org) {
                String[] oarr = org.getIdPath().split("/");
                num = oarr.length-1;
            }
        }
        return num;
    }

    /**
     * 根据组织机构编码获取组织机构id
     * @param organCode
     * @return
     */
    public  String  getOrganidByOrganCode(String organCode){
        String organid = generalSqlComponent.getDbComponent().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{organCode});
        return organid;
    }


    /**
     * 根据组织机构id获取组织机构编码
     * @param organid
     * @return
     */
    public  String  getOrganidByOrganId(String organid){
        String organcode = generalSqlComponent.getDbComponent().getSingleColumn("select organcode  from  t_dzz_info where organid = ?",String.class,new Object[]{organid});
        return organcode;
    }
}
