package com.lehand.meeting.dto;

import com.lehand.module.common.pojo.SystemParam;
import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(value="会议指南列表Dto",description="会议指南列表Dto")
public class MeetingGuideDto {

    private List<SystemParam> systemParamList;

    public List<SystemParam> getSystemParamList() {
        return systemParamList;
    }

    public void setSystemParamList(List<SystemParam> systemParamList) {
        this.systemParamList = systemParamList;
    }
}
