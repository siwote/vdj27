package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "工作汇报模板表", description = "工作汇报模板表")
public class MeetingReportTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "租户ID", name = "compid")
    private Long compid;

    @ApiModelProperty(value = "自增长主键", name = "id")
    private Long id;

    @ApiModelProperty(value = "文件id", name = "file_id")
    private String fileId;

    @ApiModelProperty(value = "模板名称", name = "file_name")
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    @ApiModelProperty(value = "组织编码", name = "orgcode")
    private String orgcode;

    @ApiModelProperty(value = "组织ID", name = "orgid")
    private String orgid;

    @ApiModelProperty(value = "创建时间", name = "createtime")
    private String createtime;

    @ApiModelProperty(value = "备注", name = "remark")
    private Integer remark;

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Integer getRemark() {
        return remark;
    }

    public void setRemark(Integer remark) {
        this.remark = remark;
    }
}