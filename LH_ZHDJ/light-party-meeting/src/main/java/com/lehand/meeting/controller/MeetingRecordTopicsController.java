package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingRecordTopicsDto;
import com.lehand.meeting.service.MeetingRecordTopicsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author code maker
 */
@Api(value = "", tags = {""})
@RestController
@RequestMapping("/license/meetingRecordTopics")
public class MeetingRecordTopicsController extends BaseController {

    @Resource
    private MeetingRecordTopicsService meetingRecordTopicsService;

    /**
     * 分页查询
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message<Pager> page(Pager pager, MeetingRecordTopicsDto meetingRecordTopicsDto) {
        Message<Pager> message = new Message<Pager>();

        return message.setData(meetingRecordTopicsService.pageMeetingRecordTopics(getSession(), meetingRecordTopicsDto, pager)).success();
    }

    /**
     * 列表
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "列表查询", notes = "列表查询", httpMethod = "POST")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Message<List<MeetingRecordTopicsDto>> list(MeetingRecordTopicsDto meetingRecordTopicsDto) {
        Message<List<MeetingRecordTopicsDto>> message = new Message<List<MeetingRecordTopicsDto>>();
        return message.setData(
                meetingRecordTopicsService.listMeetingRecordTopics(getSession(), meetingRecordTopicsDto)).success();
    }


    /**
     * 新增
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message<Long> add(MeetingRecordTopicsDto meetingRecordTopicsDto) {
        Message<Long> message = new Message<Long>();

        return message.setData(
                meetingRecordTopicsService.addMeetingRecordTopics(getSession(), meetingRecordTopicsDto)).success();
    }


    /**
     * 查看详情
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message<MeetingRecordTopicsDto> get(MeetingRecordTopicsDto meetingRecordTopicsDto) {
        Message<MeetingRecordTopicsDto> message = new Message<MeetingRecordTopicsDto>();
        MeetingRecordTopicsDto result = meetingRecordTopicsService.getMeetingRecordTopicsById(meetingRecordTopicsDto.getId());

        return message.setData(result).success();
    }

    /**
     * 修改
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message<Integer> modify(MeetingRecordTopicsDto meetingRecordTopicsDto) {
        Message<Integer> message = new Message<Integer>();

        return message.setData(
                meetingRecordTopicsService.modifyMeetingRecordTopics(getSession(), meetingRecordTopicsDto)).success();
    }

    /**
     * 根据id删除，真删除
     */
    @OpenApi(optflag = 3)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message<Integer> remove(MeetingRecordTopicsDto meetingRecordTopicsDto) {
        Message<Integer> message = new Message<Integer>();
        return message.setData(
                meetingRecordTopicsService.removeMeetingRecordTopics(getSession(), meetingRecordTopicsDto)).success();
    }
}
