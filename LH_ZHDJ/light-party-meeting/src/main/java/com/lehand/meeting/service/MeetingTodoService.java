package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.dto.MNoticeAddReq;
import com.lehand.meeting.dto.TkTaskRemindListDto;
import com.lehand.meeting.pojo.MeetingNoticeRecord;
import com.lehand.meeting.pojo.MeetingToDoRecord;
import com.lehand.meeting.pojo.TkTaskRemindList;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

@Service
public class MeetingTodoService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    MeetingNoticeService meetingNoticeService;

    /**
     * 查询待办事项列表
     * @param status
     * @param pager
     * @param session
     * @return
     */
    public Pager pagelist(int status, Pager pager, Session session, String type) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("compid", session.getCompid());
        paramsMap.put("status", status);
        paramsMap.put("userid", session.getUserid());
        paramsMap.put("type", type);
        return generalSqlComponent.pageQuery(MeetingSqlCode.queryMeetingTodoList, paramsMap, pager);
    }

    /**
     * 更新会议待办状态
     * @param status 待办状态
     * @param compid 租户ID
     * @param id 待办主键ID
     */
    public void updateMeetingTodoStatus(int status, Long compid, Long id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("compid", compid);
        paramsMap.put("status", status);
        paramsMap.put("id", id);
        generalSqlComponent.update(MeetingSqlCode.updateMeetingTodoStatus, paramsMap);
    }

    /**
     * 党员参加会议
     * @param id
     * @param status
     * @param session
     */
    public void handleTAttend(Long id, int status, Session session) {
        //查询待办记录
        MeetingToDoRecord meetingToDoRecord = queryMeetingTodo(id, session.getCompid());
        if(meetingToDoRecord == null || StringUtils.isEmpty(meetingToDoRecord.getBusinessid())) {
            LehandException.throwException("待办记录不存在，请刷新后重试！");
        }

        if(meetingToDoRecord.getTtype() != 0) {//党员可参会
            LehandException.throwException("该待办不支持参会操作！");
        }

        //更新党员是否参会
        generalSqlComponent.update(MeetingSqlCode.updateMeetingAttend, new Object[] {status, session.getCompid(), meetingToDoRecord.getBusinessid()});

        //更新待办状态
        updateMeetingTodoStatus(1, session.getCompid(), id);
    }

    /**
     * 会议待办写通知和修改待办状态
     * @param req
     * @param session
     */
    public MeetingNoticeRecord handleToDo(MNoticeAddReq req, Session session) throws Exception {
        //保存会议内容
        MeetingNoticeRecord meetingNoticeRecord = meetingNoticeService.addMeetingNotice(session, req);

        //该条会议待办修改为已办
        if(!StringUtils.isEmpty(req.getTodoid())) {
            updateMeetingTodoStatus(1, session.getCompid(), req.getTodoid());
        }
        return meetingNoticeRecord;
    }

    /**
     * 根据ID查询会议待办记录
     * @param id
     * @param compid
     * @return
     */
    public MeetingToDoRecord queryMeetingTodo(Long id, Long compid) {
        return generalSqlComponent.query(MeetingSqlCode.queryMeetingTodo, new Object[] {compid, id});
    }

    /**
     * 消息按时间分类
     * @param pager
     * @param session
     * @return
     */
    public Pager querymsg(Pager pager, Session session) {
        Pager page = generalSqlComponent.pageQuery(MeetingSqlCode.queryMsgCalDate, new Object[] {session.getUserid(),
                session.getCompid()}, pager);
        PagerUtils<TkTaskRemindListDto> pagerUtils = new PagerUtils<TkTaskRemindListDto>(page);

        //取出分组类别
        List<Object> resultList = new ArrayList<>();
        Map<String, List<TkTaskRemindListDto>>  map = new LinkedHashMap<>();

        for(TkTaskRemindListDto t : pagerUtils.getRows()) {
            map.put(t.getDate(), new ArrayList<TkTaskRemindListDto>());
        }
        //加入分组数据
        for(TkTaskRemindListDto t : pagerUtils.getRows()) {
            List<TkTaskRemindListDto> list = map.get(t.getDate());
            list.add(t);
        }
        //格式化返回
        for(Map.Entry<String, List<TkTaskRemindListDto>> entry : map.entrySet()) {
            Map<String, Object> resultMap = new LinkedHashMap<>();
            resultMap.put("date", entry.getKey());
            resultMap.put("msgs", entry.getValue());
            resultList.add(resultMap);
        }

        pager.setRows(resultList);
        return pager;
    }

    /**
     * 未读消息数
     * @param session
     * @return
     */
    public Long getMsgNum(Session session) {
        return generalSqlComponent.query(MeetingSqlCode.getMsgNumByuserId,new Object[]{session.getUserid(),0,session.getCompid()});
    }

    /**
     * 待办事项中的会议进行状态
     * @param businessid
     * @param session
     * @return
     */
    public boolean checkMeetingStatus(Long businessid, Session session) throws ParseException {
        //view_meeting_notice_record

        Map<String,Object> map = generalSqlComponent.query(MeetingSqlCode.getMeetingInfoByNoticeid,new Object[]{businessid,session.getCompid()});

        if(map==null){
            return true;
        }
        String starttime = String.valueOf(map.get("noticetime")==null? MagicConstant.STR.EMPTY_STR:map.get("noticetime"));
        String endtime = String.valueOf(map.get("endtime")==null? MagicConstant.STR.EMPTY_STR:map.get("endtime"));
        if(!StringUtils.isEmpty(endtime)&&DateEnum.now().after(DateEnum.YYYYMMDDHHMM.parse(endtime))){
            LehandException.throwException("会议已结束,不能参加!");
        }
        if(DateEnum.now().after(DateEnum.YYYYMMDDHHMM.parse(starttime))&&StringUtils.isEmpty(endtime)){
            LehandException.throwException("会议已开始,不能参加!");
        }
        return true;
    }
}
