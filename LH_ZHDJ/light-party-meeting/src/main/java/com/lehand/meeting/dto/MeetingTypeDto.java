package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="党建巡查表头",description="党建巡查表头")
public class MeetingTypeDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="会议类型编码", name="code")
    private String code;

    @ApiModelProperty(value="会议类型名称", name="name")
    private String name;

    @ApiModelProperty(value="计算方式", name="calmeth")
    private String calmeth;

    @ApiModelProperty(value="计算值", name="calvalue")
    private int calvalue;

    @ApiModelProperty(value="排序号", name="seqno")
    private int seqno;

    @ApiModelProperty(value="remark标识", name="prop")
    private String prop;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCalmeth() {
        return calmeth;
    }

    public void setCalmeth(String calmeth) {
        this.calmeth = calmeth;
    }

    public int getCalvalue() {
        return calvalue;
    }

    public void setCalvalue(int calvalue) {
        this.calvalue = calvalue;
    }

    public int getSeqno() {
        return seqno;
    }

    public void setSeqno(int seqno) {
        this.seqno = seqno;
    }

    public String getProp() {
        return prop;
    }

    public void setProp(String prop) {
        this.prop = prop;
    }
}
