package com.lehand.meeting.service.jobservice;


import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.meeting.constant.SqlCode;
import com.lehand.meeting.dto.SmsSendDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.Resource;
import java.util.Date;


@Service
public class SendMessageService {

    @Resource
    GeneralSqlComponent generalSqlComponent;


    @Transactional(rollbackFor = Exception.class)
    public void attendMeetingSms(SmsSendDto smsSendDto){
            smsSendDto.setStatus(0);
            smsSendDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
            smsSendDto.setDelflag(0);
            //插入短信发送内容
            generalSqlComponent.insert(SqlCode.addSmsSend, smsSendDto);
            }

}
