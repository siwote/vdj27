package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.pojo.MeetingThemeDate;
import com.lehand.meeting.service.MeetingThemeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(value = "主题党日活动", tags = { "主题党日活动" })
@RestController
@RequestMapping("/horn/theme")
public class MeetingThemeController extends BaseController {

    @Resource
    private MeetingThemeService meetingThemeService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询是否设置党员活动日", notes = "查询是否设置党员活动日", httpMethod = "POST")
    @RequestMapping(value = "/checkSetting", method = RequestMethod.POST)
    public Message checkMtDate(int year) {
        Message message = new Message();
        message.success().setData(meetingThemeService.checkSetting(getSession(), year));
        return message;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "设置党员活动日", notes = "设置党员活动日", httpMethod = "POST")
    @RequestMapping(value = "/set", method = RequestMethod.POST)
    public Message checkMtDate(@RequestBody MeetingThemeDate meetingThemeDate) {
        Message message = new Message();
        message.success().setData(meetingThemeService.setDate(getSession(), meetingThemeDate));
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询日期区间", notes = "查询日期区间", httpMethod = "POST")
    @RequestMapping(value = "/getDate", method = RequestMethod.POST)
    public Message getDate(int year) {
        Message message = new Message();
        message.success().setData(meetingThemeService.getDate(getSession(), year));
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询日期", notes = "查询日期", httpMethod = "POST")
    @RequestMapping(value = "/getDay", method = RequestMethod.POST)
    public Message getDay(int year) {
        Message message = new Message();
        message.success().setData(meetingThemeService.getDay(getSession(), year));
        return message;
    }
}
