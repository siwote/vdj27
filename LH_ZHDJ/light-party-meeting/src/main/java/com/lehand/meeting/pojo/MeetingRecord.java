package com.lehand.meeting.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.lehand.meeting.dto.MeetingRecordTopicsDto;
import com.lehand.meeting.utils.FloatSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "会议记录", description = "会议记录")
public class MeetingRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增长主键", name = "mrid")
    private Long mrid;

    @ApiModelProperty(value = "组织机构ID", name = "orgid")
    private Long orgid;

    @ApiModelProperty(value = "组织机构名称", name = "orgname")
    private String orgname;

    @ApiModelProperty(value = "组织机构编码", name = "orgcode")
    private String orgcode;

    @ApiModelProperty(value = "会议类型", name = "mrtype")
    private String mrtype;

    @ApiModelProperty(value = "年份", name = "mryear")
    private Integer mryear;

    @ApiModelProperty(value = "月份", name = "mrmonth")
    private Integer mrmonth;

    @ApiModelProperty(value = "开始时间(dd hh:mm)", name = "starttime")
    private String starttime;

    @ApiModelProperty(value = "结束时间(dd hh:mm)", name = "endtime")
    private String endtime;

    @ApiModelProperty(value = "会议地点", name = "place")
    private String place;

    @ApiModelProperty(value = "主持人/讲课人ID", name = "moderatorid")
    private String moderatorid;

    @ApiModelProperty(value = "主持人/讲课人名称", name = "moderatorname")
    private String moderatorname;

    @ApiModelProperty(value = "记录人ID", name = "recorderid")
    private String recorderid;

    @ApiModelProperty(value = "记录人名称", name = "recordername")
    private String recordername;

    @ApiModelProperty(value = "主题/授课内容", name = "topic")
    private String topic;

    @ApiModelProperty(value = "内容(议题)", name = "context")
    private String context;

    @ApiModelProperty(value = "状态(0:删除,1:保存,2:提交,3:撤销)", name = "status")
    private String status;

    @ApiModelProperty(value = "党小组ID", name = "groupid")
    private Long groupid;

    @ApiModelProperty(value = "党小组名称", name = "groupname")
    private String groupname;

    @ApiModelProperty(value = "", name = "shouldnum")
    private Integer shouldnum;

    @ApiModelProperty(value = "实到人数/听课人数", name = "realitynum")
    private Integer realitynum;

    @ApiModelProperty(value = "创建时间", name = "createtime")
    private String createtime;

    @ApiModelProperty(value = "创建人ID", name = "createuserid")
    private Long createuserid;

    @ApiModelProperty(value = "修改时间", name = "updatetime")
    private String updatetime;

    @ApiModelProperty(value = "修改人ID", name = "updateuserid")
    private Long updateuserid;

    @ApiModelProperty(value = "删除时间", name = "deletetime")
    private String deletetime;

    @ApiModelProperty(value = "删除人", name = "deleteuserid")
    private Long deleteuserid;

    @ApiModelProperty(value = "", name = "compid")
    private Long compid;

    @ApiModelProperty(value = "备注1", name = "remark1")
    private String remark1;

    @ApiModelProperty(value = "备注2", name = "remark2")
    private String remark2;

    @ApiModelProperty(value = "备注3(列席人员)", name = "remark3")
    private String remark3;

    @ApiModelProperty(value = "备注4(缺席人员)", name = "remark4")
    private String remark4;

    @ApiModelProperty(value = "备注5(退回意见)", name = "remark5")
    private String remark5;

    @ApiModelProperty(value = "会议决定", name = "decision")
    private String decision;
    @ApiModelProperty(value = "学时", name = "classhours")
    @JsonSerialize(using = FloatSerialize.class)
    private Float classhours;

    @ApiModelProperty(value = "存在多个议题，议题列表", name = "topicsList")
    private List<MeetingRecordTopicsDto> topicsList;

    public String getRemark5() {
        return remark5;
    }

    public void setRemark5(String remark5) {
        this.remark5 = remark5;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public Long getMrid() {
        return mrid;
    }

    public void setMrid(Long mrid) {
        this.mrid = mrid;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getMrtype() {
        return mrtype;
    }

    public void setMrtype(String mrtype) {
        this.mrtype = mrtype;
    }

    public Integer getMryear() {
        return mryear;
    }

    public void setMryear(Integer mryear) {
        this.mryear = mryear;
    }

    public Integer getMrmonth() {
        return mrmonth;
    }

    public void setMrmonth(Integer mrmonth) {
        this.mrmonth = mrmonth;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getModeratorid() {
        return moderatorid;
    }

    public void setModeratorid(String moderatorid) {
        this.moderatorid = moderatorid;
    }

    public String getModeratorname() {
        return moderatorname;
    }

    public void setModeratorname(String moderatorname) {
        this.moderatorname = moderatorname;
    }

    public String getRecorderid() {
        return recorderid;
    }

    public void setRecorderid(String recorderid) {
        this.recorderid = recorderid;
    }

    public String getRecordername() {
        return recordername;
    }

    public void setRecordername(String recordername) {
        this.recordername = recordername;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getGroupid() {
        return groupid;
    }

    public void setGroupid(Long groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public Integer getShouldnum() {
        return shouldnum;
    }

    public void setShouldnum(Integer shouldnum) {
        this.shouldnum = shouldnum;
    }

    public Integer getRealitynum() {
        return realitynum;
    }

    public void setRealitynum(Integer realitynum) {
        this.realitynum = realitynum;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Long getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(Long createuserid) {
        this.createuserid = createuserid;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Long getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(Long updateuserid) {
        this.updateuserid = updateuserid;
    }

    public String getDeletetime() {
        return deletetime;
    }

    public void setDeletetime(String deletetime) {
        this.deletetime = deletetime;
    }

    public Long getDeleteuserid() {
        return deleteuserid;
    }

    public void setDeleteuserid(Long deleteuserid) {
        this.deleteuserid = deleteuserid;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public Float getClasshours() {
        return classhours;
    }

    public void setClasshours(Float classhours) {
        this.classhours = classhours;
    }
    public List<MeetingRecordTopicsDto> getTopicsList() {
        return topicsList;
    }

    public void setTopicsList(List<MeetingRecordTopicsDto> topicsList) {
        this.topicsList = topicsList;
    }

    private Byte discuss;

    public Byte getDiscuss() {
        return discuss;
    }

    public void setDiscuss(Byte discuss) {
        this.discuss = discuss;
    }
}