package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingReportRecordDto;
import com.lehand.meeting.service.MeetingReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(value = "工作汇报", tags = { "工作汇报" })
@RestController
@RequestMapping("/horn/report")
public class MeetingReportController extends BaseController {

    @Resource
    private MeetingReportService meetingReportService;

    /**
     * 分页查询
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", value = "主题或内容模糊查询字段", required = false, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "auditLike", value = "上报组织或会议主题模糊查询字段", required = false, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "状态（ 0:删除,1:保存,2:提交(待审核),3:撤销,4:(审核通过),5:(审核驳回),6:(已审核)", required = false, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "类型(1:审核，2：列表页)", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message page(Pager pager, String topic, String auditLike,String status,String startDate, String endDate, String orgcode, String type) {
        Message message = new Message();
        message.success();
        MeetingReportRecordDto meetingReportRecord = new MeetingReportRecordDto();
        meetingReportRecord.setTopic(topic);
        meetingReportRecord.setStartDate(startDate);
        meetingReportRecord.setEndDate(endDate);
        if(!StringUtils.isEmpty(status)){
            Integer value = Integer.valueOf(status);
            if(6==value){
                meetingReportRecord.setAlreadyAudit(value);
            }else {
                meetingReportRecord.setStatus(value);
            }
        }
        meetingReportRecord.setAuditLike(auditLike);
        message.setData(new PagerUtils<Map<String,Object>>(
                meetingReportService.pageMeetingReport(getSession(), meetingReportRecord, pager,orgcode,type)));
        return message;
    }

    /**
     * 新增
     */
    @OpenApi(optflag=1)
    @ApiOperation(value = "新增工作汇报", notes = "新增工作汇报", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message add(@RequestBody MeetingReportRecordDto meetingReportRecordDto) {
        Message message = new Message();
        message.success();
        message.setData(
                meetingReportService.addMeetingReport(getSession(),meetingReportRecordDto));
        return message;
    }

    /**
     * 查看详情
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message get(Long id) {
        Message message = new Message();
        MeetingReportRecordDto result = meetingReportService.getMeetingReport(getSession(),id);
        message.success();
        message.setData(result);
        return message;
    }

    /**
     * 修改
     */
    @OpenApi(optflag=2)
    @ApiOperation(value = "工作汇报修改", notes = "工作汇报修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message modify(@RequestBody MeetingReportRecordDto meetingReportRecordDto) {
        Message message = new Message();
        message.success();
        message.setData(
                meetingReportService.modifyMeetingReport(getSession(),meetingReportRecordDto));
        return message;
    }

    /**
     * 根据id删除，逻辑删除
     */
    @OpenApi(optflag=3)
    @ApiOperation(value = "工作汇报记录删除", notes = "工作汇报记录删除（逻辑删除）", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message remove(Long id) {
        Message message = new Message();
        meetingReportService.removeMeetingReport(getSession(),id);
        message.success();
        return message;
    }

    /**
     * 分页查询模板
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询模板", notes = "分页查询模板", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgid", value = "组织id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "orgcode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping(value = "/getTemplate", method = RequestMethod.POST)
    public Message getTemplate(Pager pager,String orgid, String orgcode) {
        Message message = new Message();
        message.success();
        message.setData(meetingReportService.pageMeetingReportTemplate(getSession(),orgid,orgcode,pager));
        return message;
    }

    /**
     * 新增模板
     */
    @OpenApi(optflag=1)
    @ApiOperation(value = "新增工作汇报模板", notes = "新增工作汇报模板", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileId", value = "文件id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "fileName", value = "文件名称", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping(value = "/saveTemplate", method = RequestMethod.POST)
    public Message saveTemplate(String fileId, String fileName) {
        Message message = new Message();
        try {
            message.success();
            message.setData(
                    meetingReportService.addMeetingReportTemplate(getSession(),fileId,fileName));
        } catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }

    /**
     * 删除模板
     */
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除工作汇报模板", notes = "删除工作汇报模板", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping(value = "/delTemplate", method = RequestMethod.POST)
    public Message delTemplate(String  id) {
        Message message = new Message();
        try {
            meetingReportService.delMeetingReportTemplate(id);
            message.success();
        } catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }

    /**
     * 重新上传模板
     */
    @OpenApi(optflag=2)
    @ApiOperation(value = "重新上传工作汇报模板", notes = "重新上传工作汇报模板", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "fileId", value = "文件id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping(value = "/updateTemplate", method = RequestMethod.POST)
    public Message updateTemplate(String  id,String  fileId) {
        Message message = new Message();
        try {
            meetingReportService.updateMeetingReportTemplate(id,fileId);
            message.success();
        } catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }

    /**
     * 批量修改
     */
    @OpenApi(optflag=2)
    @ApiOperation(value = "批量审核驳回", notes = "工作汇报修改", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "汇报记录ids，逗号拼接", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "状态(通过：4，驳回：5)", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "Remark2", value = "驳回意见", required = false, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping(value = "/batchModify", method = RequestMethod.POST)
    public Message batchModify(String ids,String status,String Remark2) {
        Message message = new Message();
        try {
            message.success();
            message.setData(meetingReportService.batchModify(getSession(),status,Remark2,ids));
        } catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }
}
