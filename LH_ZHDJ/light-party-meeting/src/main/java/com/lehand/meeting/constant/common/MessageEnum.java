package com.lehand.meeting.constant.common;

public enum MessageEnum {
	
	/** 会议通知下发 .*/
    MNOTICE_DEPLOY("MNOTICE_DEPLOY", "会议通知下发"),

    /** 会议通知创建 .*/
    MNOTICE_CREATE("MNOTICE_CREATE", "会议通知创建"),

	/** 未定义 .*/
	UNKNOWN_CODE("UNKNOWN_CODE", "未定义");
	
	private String code;
    private String msg;

    MessageEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    
    /**
     * 静态方法：根据编码获取信息
     * @param code
     * @return
     */
    public static String getMsg(String code) {
        for (MessageEnum mc : MessageEnum.values()) {
            if(mc.code.equals(code)) {
                return mc.msg;
            }
        }
        return null;
    }

    /**
     * 根据错误编码获取MsgCode枚举对象
     * @param code
     * @return
     */
    public static MessageEnum getMsgCode(String code) {
        for (MessageEnum mc : MessageEnum.values()) {
            if(mc.code.equals(code)) {
                return mc;
            }
        }
        return MessageEnum.UNKNOWN_CODE;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getMsg() {
        return msg;
    }
}
