package com.lehand.meeting.constant.common;

/**
 * 打印导出的会议类型编码名称
 */
public enum MeetingPMtypeEnum {

    /** 支部委员会 .*/
    BRANCH_COMMITTEE("branch_committee", "支部委员会"),

    /** 支部党员大会 .*/
    BRANCH_PARTY_CONGRESS("branch_party_congress", "支部党员大会"),

    /** 党委会 .*/
    COMMITTEE_MEETING("committee_meeting", "党委会"),

    /** 党小组会 .*/
    PARTY_GROUP_MEETING("party_group_meeting", "党小组会"),

    /** 上党课 .*/
    PARTY_LECTURE("party_lecture", "上党课"),

    /** 组织生活会 .*/
    ORG_LIFE_MEETING("org_life_meeting", "组织生活会"),

    /** 民主评议党员 .*/
    APPRAISAL_OF_MEMBERS("appraisal_of_members", "民主评议党员"),

    /** 主题党日 .*/
    THEME_PARTY_DAY("theme_party_day", "党员活动日"),

    /** 党委中心组学习 .*/
    THEORETICAL_STUDY("theoretical_study", "党委中心组学习"),

    /** 民主生活会 .*/
    DEMOCRATIC_LIFE_MEETING("democratic_life_meeting", "民主生活会"),

    /** 谈心谈话 .*/
    MEETING_TALK("meeting_talk", "谈心谈话"),

    /** 工作汇报 .*/
    MEETING_REPORT("meeting_report", "工作汇报"),

    /** 未定义 .*/
    UNKNOWN_CODE("unknown_code", "未定义");


    private String code;
    private String msg;

    MeetingPMtypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 静态方法：根据编码获取信息
     * @param code
     * @return
     */
    public static String getMsg(String code) {
        for (MeetingPMtypeEnum mc : MeetingPMtypeEnum.values()) {
            if(mc.code.equals(code)) {
                return mc.msg;
            }
        }
        return null;
    }

    /**
     * 根据错误编码获取MsgCode枚举对象
     * @param code
     * @return
     */
    public static MeetingPMtypeEnum getMsgCode(String code) {
        for (MeetingPMtypeEnum mc : MeetingPMtypeEnum.values()) {
            if(mc.code.equals(code)) {
                return mc;
            }
        }
        return MeetingPMtypeEnum.UNKNOWN_CODE;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }



}
