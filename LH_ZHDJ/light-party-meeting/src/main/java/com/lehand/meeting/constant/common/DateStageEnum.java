package com.lehand.meeting.constant.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;

/**
 * 将时间周期计算成对应的具体时间点
 *
 * @author pantao
 * @date 2020年05月26日
 */
public enum DateStageEnum {
    Y {
        @Override
        protected List<String> create(String json) {
            //解析json字符串 取出相应的数据
            Map<String, Map<String, Integer>> map = JSON.parseObject(json, Map.class);
            int m = Integer.valueOf(map.get("mouths").get("num1").toString());
            int d1 = Integer.valueOf(map.get("days").get("num1").toString());
            int d2 = Integer.valueOf(map.get("days").get("num2").toString());
            //返回时间集合
            List<String> list = new ArrayList<>();
            list.add(spliceDate(m, d1));
            list.add(spliceDate(m, d2));
            return list;
        }
    },
    H {
        @Override
        protected List<String> create(String json) {
            //解析json字符串 取出相应的数据
            Map<String, Map<String, Integer>> map = JSON.parseObject(json, Map.class);
            int q = Integer.valueOf(map.get("quarters").get("num1").toString());
            int m = Integer.valueOf(map.get("mouths").get("num1").toString());
            int d1 = Integer.valueOf(map.get("days").get("num1").toString());
            int d2 = Integer.valueOf(map.get("days").get("num2").toString());
            int month1 = 0;
            int month2 = 0;
            if (q == 1 && m == 1) {//第一个季度第一个月
                month1 = 1;
                month2 = 7;
            } else if (q == 1 && m == 2) {//第一个季度第二个月
                month1 = 2;
                month2 = 8;
            } else if (q == 1 && m == 3) {//第一个季度第三个月
                month1 = 3;
                month2 = 9;
            } else if (q == 2 && m == 1) {//第二个季度第一个月
                month1 = 4;
                month2 = 10;
            } else if (q == 2 && m == 2) {//第二个季度第二个月
                month1 = 5;
                month2 = 11;
            } else if (q == 2 && m == 3) {//第二个季度第三个月
                month1 = 6;
                month2 = 12;
            }
            List<String> list = new ArrayList<>();
            list.add(spliceDate(month1, d1));
            list.add(spliceDate(month1, d2));
            list.add(spliceDate(month2, d1));
            list.add(spliceDate(month2, d2));
            return list;
        }
    },
    Q {
        @Override
        protected List<String> create(String json) {
            //解析json字符串 取出相应的数据
            Map<String, Map<String, Integer>> map = JSON.parseObject(json, Map.class);
            int m = Integer.valueOf(map.get("mouths").get("num1").toString());
            int d1 = Integer.valueOf(map.get("days").get("num1").toString());
            int d2 = Integer.valueOf(map.get("days").get("num2").toString());
            int month1 = 0;
            int month2 = 0;
            int month3 = 0;
            int month4 = 0;
            if (m == 1) {//第一个月
                month1 = 1;
                month2 = 4;
                month3 = 7;
                month4 = 10;
            } else if (m == 2) {//第二个月
                month1 = 2;
                month2 = 5;
                month3 = 8;
                month4 = 11;
            } else if (m == 3) {//第三个月
                month1 = 3;
                month2 = 6;
                month3 = 9;
                month4 = 12;
            }
            List<String> list = new ArrayList<>();
            list.add(spliceDate(month1, d1));
            list.add(spliceDate(month1, d2));
            list.add(spliceDate(month2, d1));
            list.add(spliceDate(month2, d2));
            list.add(spliceDate(month3, d1));
            list.add(spliceDate(month3, d2));
            list.add(spliceDate(month4, d1));
            list.add(spliceDate(month4, d2));
            return list;
        }
    },
    M {
        @Override
        protected List<String> create(String json) {
            //解析json字符串 取出相应的数据
            Map<String, Map<String, Integer>> map = JSON.parseObject(json, Map.class);
            int d1 = Integer.valueOf(map.get("days").get("num1").toString());
            int d2 = Integer.valueOf(map.get("days").get("num2").toString());
            List<String> list = new ArrayList<>();
            for (int m = 1; m <= 12; m++) {
                list.add(spliceDate(m, d1));
                list.add(spliceDate(m, d2));
            }
            return list;
        }
    };


    /**
     * 判断逻辑确定调用具体的方法
     *
     * @return
     * @author pantao
     * @date 2020年05月26日
     */
    public static List<String> parse(String json, Integer type) {
        String data = "";
        switch (type) {
            case 1:
                data = "Y";
                break;
            case 2:
                data = "H";
                break;
            case 3:
                data = "Q";
                break;
            default:
                data = "M";
                break;
        }
        DateStageEnum dateStageEnum = DateStageEnum.valueOf(data);
        return dateStageEnum.create(json);
    }

    /**
     * 将时间周期计算成具体的时间点
     *
     * @return
     * @author pantao
     * @date 2020年05月26日
     */
    protected abstract List<String> create(String json);


    /**
     * 计算某年某月的最后一天
     *
     * @param yearMonth
     * @return
     * @author pantao
     * @date 2020年05月26日
     */
    public static int getLastDayOfMonth(String yearMonth) {
        //年
        int year = Integer.parseInt(yearMonth.split("-")[0]);
        //月
        int month = Integer.parseInt(yearMonth.split("-")[1]);
        Calendar cal = Calendar.getInstance();
        // 设置年份
        cal.set(Calendar.YEAR, year);
        // 设置月份
        // cal.set(Calendar.MONTH, month - 1);
        //设置当前月的上一个月
        cal.set(Calendar.MONTH, month);
        // 获取某月最大天数
        //int lastDay = cal.getActualMaximum(Calendar.DATE);
        // 获取月份中的最小值，即第一天
        int lastDay = cal.getMinimum(Calendar.DATE);
        // 设置日历中月份的最大天数
        //cal.set(Calendar.DAY_OF_MONTH, lastDay);
        // 上月的第一天减去1就是当月的最后一天
        cal.set(Calendar.DAY_OF_MONTH, lastDay - 1);
        // 格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        return Integer.valueOf(sdf.format(cal.getTime())).intValue();
    }

    /**
     * 获取当前年份
     *
     * @return
     * @author pantao
     * @date 2020年05月26日
     */
    public static String getYear() {
        return DateEnum.YYYY.format();
    }

    /**
     * 获取日期
     *
     * @param m
     * @param d
     * @return
     * @author pantao
     * @date 2020年05月26日
     */
    public static String spliceDate(int m, int d) {
        int lastday = getLastDayOfMonth(getYear() + "-" + m);
        // 判断天数是否大于月份的最后一天
        if (d > lastday) {
            d = lastday;
        }
        // 拼接日期
        return getYear() + "-" + (m > 9 ? m : ("0" + m)) + "-" + (d > 9 ? d : ("0" + d));
    }

    public static void main(String[] args) {
        System.out.println(getLastDayOfMonth("2019-2"));
        System.out.println(getYear());
    }

}
