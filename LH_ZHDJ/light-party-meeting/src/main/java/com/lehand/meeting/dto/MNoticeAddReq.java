package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingNoticeRecord;
import com.lehand.meeting.pojo.MeetingRecordSubject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

@ApiModel(value="新增会议通知参数",description="新增会议通知参数")
public class MNoticeAddReq extends MeetingNoticeRecord {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="会议通知对象", name="meetingRecordSubjects")
    private List<MeetingRecordSubject> meetingRecordSubjects;

    @ApiModelProperty(value="待办Id", name="todoid")
    private Long todoid;

    @ApiModelProperty(value="会议通知附件", name="MNoticeAttachList")
    private List<Map<String,Object>> MNoticeAttachList;

    //2020-05-30
    @ApiModelProperty(value="关联标签列表", name="tagcode")
    private List<String> tagcode;

    //2020-07-23
    @ApiModelProperty(value="是否关联投票 1:是,0:否", name="voteflag",required = false)
    private int voteflag;

    public List<String> getTagcode() {
        return tagcode;
    }

    public void setTagcode(List<String> tagcode) {
        this.tagcode = tagcode;
    }

    public List<Map<String, Object>> getMNoticeAttachList() {
        return MNoticeAttachList;
    }

    public void setMNoticeAttachList(List<Map<String, Object>> MNoticeAttachList) {
        this.MNoticeAttachList = MNoticeAttachList;
    }

    public List<MeetingRecordSubject> getMeetingRecordSubjects() {
        return meetingRecordSubjects;
    }

    public void setMeetingRecordSubjects(List<MeetingRecordSubject> meetingRecordSubjects) {
        this.meetingRecordSubjects = meetingRecordSubjects;
    }

    public Long getTodoid() {
        return todoid;
    }

    public void setTodoid(Long todoid) {
        this.todoid = todoid;
    }

    public int getVoteflag() {
        return voteflag;
    }

    public void setVoteflag(int voteflag) {
        this.voteflag = voteflag;
    }
}
