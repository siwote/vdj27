package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.service.WorkbenchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(value = "工作台", tags = { "工作台" })
@RestController
@RequestMapping("/horn/workbench")
public class WorkbenchController extends BaseController {
    private static final Logger logger = LogManager.getLogger(WorkbenchController.class);
    @Resource
    private WorkbenchService workbenchService;


    @OpenApi(optflag=0)
    @ApiOperation(value = "快捷入口查询", notes = "快捷入口查询", httpMethod = "POST")
    @RequestMapping("/listFunction")
    public Message listFunction(){
        Message msg = new Message();
        try{
            msg.setData(workbenchService.listFunction(getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("快捷入口查询错误");
        }
        return msg;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "快捷入口保存", notes = "快捷入口保存", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fctids", value = "功能ID(多个用逗号拼接)", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/saveFunction")
    public Message saveFunction(String fctids) {
        Message msg = new Message();
        workbenchService.saveFunction(fctids, getSession());
        return msg.success();
    }


    @OpenApi(optflag=2)
    @ApiOperation(value = "快捷入口删除", notes = "快捷入口删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "functioncode", value = "功能CODE", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delFunction")
    public Message delFunction(String functioncode) {
        Message msg = new Message();
        workbenchService.delFunction(functioncode, getSession());
        return msg.success();
    }


    @OpenApi(optflag=3)
    @ApiOperation(value = "指标监测查询", notes = "指标监测查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型0自定义1：本月，2：本季度，3本年", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/indexMonitor")
    public Message indexMonitor(/**Integer type,String startTime,String endTime*/){
        Message msg = new Message();
        try{
            msg.setData(workbenchService.indexMonitor(/**type,startTime,endTime,*/getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("监测指标查询错误");
        }
        return msg;
    }

    /**
     * 系统动态分页查询
     * @param pager
     * @param topic
     * @param startDate
     * @param endDate
     * @param type
     * @return
     */
    @OpenApi(optflag=4)
    @ApiOperation(value = "支部动态分页查询", notes = "支部动态分页查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", value = "主题内容", required = false, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "startDate", value = "开始时间", required = false, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "endDate", value = "结束时间", required = false, dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "类型(全部时传空字符串)", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message page(Pager pager, String topic, String startDate, String endDate, String type) {
        Message message = new Message();
        message.success();
        message.setData(new PagerUtils<Map<String,Object>>(
                workbenchService.page(getSession(),topic,startDate,endDate,type,pager)));
        return message;
    }

    @OpenApi(optflag=3)
    @ApiOperation(value = "动态类型查询", notes = "动态类型查询", httpMethod = "POST")
    @RequestMapping("/listDynamicsType")
    public Message listDynamicsType(){
        Message msg = new Message();
        try{
            msg.setData(workbenchService.listDynamicsType(getSession()));
            msg.success();
        }catch (Exception e){
            logger.error(e);
            msg.fail("查询错误");
        }
        return msg;
    }
}
