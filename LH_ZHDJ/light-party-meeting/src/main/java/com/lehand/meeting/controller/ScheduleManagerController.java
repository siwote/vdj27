package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.constant.ScheduleCode;
import com.lehand.meeting.pojo.ScheduleJobEntity;
import com.lehand.meeting.utils.RestTemplateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Api(value = "定时任务配置", tags = { "定时任务配置（可选）" })
@RestController
@RequestMapping("/schedule/manager")
public class ScheduleManagerController extends BaseController {

    @Value("${batch_service_url}")
    private String batchserviceUrl;

    @OpenApi(optflag=0)
    @ApiOperation(value = "定时任务列表", notes = "定时任务列表", httpMethod = "POST")
    @RequestMapping("/list")
    public Message list(@RequestBody ScheduleJobEntity scheduleJobEntity, Pager pager) {
        Message msg = new Message();
        try {
            RestTemplate restTemplate = RestTemplateUtil.getInstance("utf-8");
            String url = String.format("%s%s", batchserviceUrl, ScheduleCode.SCHEDULE_LIST);
            ResponseEntity<String> result = restTemplate.postForEntity(url, scheduleJobEntity, String.class);
            if(result.getStatusCodeValue() == 200) {
                msg.setData(RestTemplateUtil.getData(result.getBody()));
                msg.success();
            } else {
                msg.fail("定时任务查询失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            msg.fail("定时任务查询失败！");
        }
        return msg;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "新增定时任务", notes = "新增定时任务", httpMethod = "POST")
    @RequestMapping("/add")
    public Message add(@RequestBody ScheduleJobEntity scheduleJobEntity) {
        Message msg = new Message();
        try {
            RestTemplate restTemplate = RestTemplateUtil.getInstance("utf-8");
            String url = String.format("%s%s", batchserviceUrl, ScheduleCode.SCHEDULE_ADD);
            ResponseEntity<String> result = restTemplate.postForEntity(url, scheduleJobEntity, String.class);
            if(result.getStatusCodeValue() == 200) {
                msg.setData(RestTemplateUtil.getData(result.getBody()));
                msg.success();
            } else {
                msg.fail("新增定时任务失败！");
            }
        } catch (Exception e) {
            msg.fail("新增定时任务失败！");
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "暂停任务", notes = "暂停任务", httpMethod = "POST")
    @ApiImplicitParams({
    @ApiImplicitParam(name = "jobid", value = "任务ID(必填)", paramType = "query", defaultValue = ""),
    })
    @RequestMapping("/pause")
    public Message pause(Long jobid) {
        Message msg = new Message();
        try {
            RestTemplate restTemplate = RestTemplateUtil.getInstance("utf-8");
            MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
            paramMap.add("jobid", jobid);
            String url = String.format("%s%s", batchserviceUrl, ScheduleCode.SCHEDULE_PAUSE);
            ResponseEntity<String> result = restTemplate.postForEntity(url, paramMap, String.class);
            if(result.getStatusCodeValue() == 200) {
                msg.setData(RestTemplateUtil.getData(result.getBody()));
                msg.success();
            } else {
                msg.fail("暂停任务失败！");
            }
        } catch (Exception e) {
            msg.fail("暂停任务失败！");
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "恢复任务", notes = "恢复执行任务", httpMethod = "POST")
    @ApiImplicitParams({
    @ApiImplicitParam(name = "jobid", value = "任务ID(必填)", paramType = "query", defaultValue = ""),
    })
    @RequestMapping("/resume")
    public Message resume(Long jobid) {
        Message msg = new Message();
        try {
            RestTemplate restTemplate = RestTemplateUtil.getInstance("utf-8");
            MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
            paramMap.add("jobid", jobid);
            String url = String.format("%s%s", batchserviceUrl, ScheduleCode.SCHEDULE_RESUME);
            ResponseEntity<String> result = restTemplate.postForEntity(url, paramMap, String.class);
            if(result.getStatusCodeValue() == 200) {
                msg.setData(RestTemplateUtil.getData(result.getBody()));
                msg.success();
            } else {
                msg.fail("恢复任务失败！");
            }
        } catch (Exception e) {
            msg.fail("恢复任务失败！");
        }
        return msg;
    }

    @OpenApi(optflag=3)
    @ApiOperation(value = "删除任务", notes = "删除任务", httpMethod = "POST")
    @ApiImplicitParams({
    @ApiImplicitParam(name = "jobid", value = "任务ID(必填)", paramType = "query", defaultValue = ""),
    })
    @RequestMapping("/del")
    public Message del(Long jobid) {
        Message msg = new Message();
        try {
            RestTemplate restTemplate = RestTemplateUtil.getInstance("utf-8");
            MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
            paramMap.add("jobid", jobid);
            String url = String.format("%s%s", batchserviceUrl, ScheduleCode.SCHEDULE_DEL);
            ResponseEntity<String> result = restTemplate.postForEntity(url, paramMap, String.class);
            if(result.getStatusCodeValue() == 200) {
                msg.setData(RestTemplateUtil.getData(result.getBody()));
                msg.success();
            } else {
                msg.fail("删除任务失败！");
            }
        } catch (Exception e) {
            msg.fail("删除任务失败！");
        }
        return msg;
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "更新任务信息", notes = "更新任务信息", httpMethod = "POST")
    @RequestMapping("/updateJob")
    public Message updateJob(@RequestBody ScheduleJobEntity scheduleJobEntity) {
        Message msg = new Message();
        try {
            RestTemplate restTemplate = RestTemplateUtil.getInstance("utf-8");
            String url = String.format("%s%s", batchserviceUrl, ScheduleCode.SCHEDULE_UPDATEJOB);
            ResponseEntity<String> result = restTemplate.postForEntity(url, scheduleJobEntity, String.class);
            if(result.getStatusCodeValue() == 200) {
                msg.setData(RestTemplateUtil.getData(result.getBody()));
                msg.success();
            } else {
                msg.fail("删除任务失败！");
            }
        } catch (Exception e) {
            msg.fail("删除任务失败！");
        }
        return msg;
    }
}
