package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="标签表", description="标签表")
public class MeetingTagsMap implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户id", name="compid")
    private Long compid;

    @ApiModelProperty(value="标签关联主体id", name="subjectid")
    private Long subjectid;

    @ApiModelProperty(value="主体类型 0:会议", name="subjecttype")
    private Integer subjecttype;

    @ApiModelProperty(value="标签编码", name="tagcode")
    private String tagcode;

    @ApiModelProperty(value="标签名称", name="tagname")
    private String tagname;

    @ApiModelProperty(value="组织编码", name="orgcode")
    private String orgcode;


    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Long subjectid) {
        this.subjectid = subjectid;
    }

    public Integer getSubjecttype() {
        return subjecttype;
    }

    public void setSubjecttype(Integer subjecttype) {
        this.subjecttype = subjecttype;
    }

    public String getTagcode() {
        return tagcode;
    }

    public void setTagcode(String tagcode) {
        this.tagcode = tagcode;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }


}