package com.lehand.meeting.dto;

import com.lehand.base.common.Pager;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="党建巡查分页查询参数",description="党建巡查分页查询参数")
public class MPatrolPagerReq extends  MPatrolReq {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="分页参数", name="pager")
    private Pager pager;

    @ApiModelProperty(value="会议类型编码", name="mrtype")
    private String mrtype;

    @ApiModelProperty(value="达标状态:0 为达标;1:已达标", name="type")
    private Integer type;

    @ApiModelProperty(value="直属下级path", name="childpath",hidden = true)
    private String childpath;

    @ApiModelProperty(value="直属下级id", name="childorganid",hidden = true)
    private String childorganid;

    @ApiModelProperty(value="表名", name="tablename",hidden = true)
    private String tablename;

    @ApiModelProperty(value="计算值", name="calvalue",hidden = true)
    private int calvalue;

    public String getMrtype() {
        return mrtype;
    }

    public void setMrtype(String mrtype) {
        this.mrtype = mrtype;
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public String getChildpath() {
        return childpath;
    }

    public void setChildpath(String childpath) {
        this.childpath = childpath;
    }

    public String getChildorganid() {
        return childorganid;
    }

    public void setChildorganid(String childorganid) {
        this.childorganid = childorganid;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public int getCalvalue() {
        return calvalue;
    }

    public void setCalvalue(int calvalue) {
        this.calvalue = calvalue;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
