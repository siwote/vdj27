package com.lehand.meeting.controller;

import com.lehand.base.common.Message;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.meeting.dto.MeetingSignDto;
import com.lehand.meeting.service.MeetingPDFService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Api(value = "会议PDF服务", tags = { "会议PDF服务" })
@RestController
@RequestMapping("/horn/meetingpdf")
public class MeetingPDFController extends BaseController {

    @Resource
    private MeetingPDFService meetingPDFService;

    @OpenApi(optflag=1)
    @ApiOperation(value = "会议签到表文件", notes = "会议签到表文件", httpMethod = "POST")
    @RequestMapping(value = "/getMeetingSignPdf", method = RequestMethod.POST)
    public Message<Map<String,Object>> getMeetingSignPdf(@RequestBody MeetingSignDto meetingSignDto) {
        Message<Map<String,Object>> msg = new Message<Map<String,Object>>();
        try {
            msg.setData(meetingPDFService.uploadSignToFdfs(meetingSignDto.getFileName(), meetingSignDto.getTime(),
                    meetingSignDto.getTopic(), meetingSignDto.getNameList(), getSession()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "批量导出PDF", notes = "批量导出PDF", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织机构编码", dataType = "String", paramType = "query", example = ""),
            @ApiImplicitParam(name = "mrtype", value = "会议类型", dataType = "String", paramType = "query", example = ""),
            @ApiImplicitParam(name = "mrids", value = "需要导出的会议ID集合（无为空，多个都好隔开）", dataType = "String", paramType = "query", example = "")
    })
    @RequestMapping(value = "/meetingBatchExport", method = RequestMethod.POST)
    public Message<Object> meetingBatchExport(String orgcode, String mrtype, String mrids) {
        Message<Object> msg = new Message<Object>();
        if(StringUtils.isEmpty(orgcode)) {
            LehandException.throwException("组织机构编码不能为空！");
        }
        if(StringUtils.isEmpty(mrtype)) {
            LehandException.throwException("会议类型不能为空！");
        }
        try {
            return msg.setData(meetingPDFService.meetingBatchExport(orgcode, mrtype, mrids, getSession())).success();
        } catch (Exception e) {
            e.printStackTrace();
            return msg.fail("会议打印异常，请稍后重试！"+e.getMessage());
        }
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "会议类型列表", notes = "会议类型列表", httpMethod = "POST")
    @RequestMapping(value = "/meetingViewList", method = RequestMethod.POST)
    public Message<List<Map<String,Object>>> meetingViewList(){
        Message<List<Map<String,Object>>> msg = new Message<List<Map<String,Object>>>();
        msg.setData(meetingPDFService.meetingViewList(getSession())).success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "打印", notes = "打印", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "附件id", dataType = "String", paramType = "query", example = "")
    })
    @RequestMapping(value = "/print", method = RequestMethod.POST)
    public Message print(HttpServletResponse response, String id){
        Message msg = new Message();
        try {
            msg.setData(meetingPDFService.print(response,id,getSession())).success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }
}

