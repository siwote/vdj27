package com.lehand.meeting.dto;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;

public class MeetingTagRecordDto extends MeetingRecordDto{
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="关联标签列表", name="tagcode")
	private List<String> tagcode;

	@ApiModelProperty(value="关联投票列表", name="forms")
	private List<Map<String,Object>> forms;

	public List<String> getTagcode() {
		return tagcode;
	}

	public void setTagcode(List<String> tagcode) {
		this.tagcode = tagcode;
	}

	public List<Map<String, Object>> getForms() {
		return forms;
	}

	public void setForms(List<Map<String, Object>> forms) {
		this.forms = forms;
	}
}
