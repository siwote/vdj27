package com.lehand.meeting.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfo;
import com.lehand.meeting.constant.Constant;
import com.lehand.meeting.constant.MeetingSqlCode;
import com.lehand.meeting.constant.common.MagicConstant;
import com.lehand.meeting.dto.MeetingReportRecordDto;
import com.lehand.meeting.pojo.MeetingReportRecord;
import com.lehand.meeting.pojo.MeetingReportTemplate;
import com.lehand.todo.constant.Todo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MeetingReportService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private MeetingService meetingService;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")
    private String downIp;

    /**
     * 分页查询
     */
    public Pager pageMeetingReport(Session session, MeetingReportRecordDto meetingReportRecordDto, Pager pager, String orgcode, String type) {
        if(StringUtils.isEmpty(orgcode)){
            LehandException.throwException("组织编码不能为空！");
        }
        if("1".equals(type)){
            List<String> listCode =generalSqlComponent.getDbComponent().listSingleColumn(MeetingSqlCode.getTDZZInfoByParentCode,String.class,new Object[]{orgcode});
            if(listCode!=null&&listCode.size()>0){
                meetingReportRecordDto.setCodes(StringUtils.strip(listCode.toString(),"[]"));
            }else{
                pager.setRows(new ArrayList<>());
                return pager;
            }
        }else{
            meetingReportRecordDto.setOrgcode(orgcode);
        }
        meetingReportRecordDto.setCompid(session.getCompid());
        return generalSqlComponent.pageQuery(MeetingSqlCode.pageMeetingReportRecord, meetingReportRecordDto,pager);
    }

    /**
     * 保存工作汇报
     * @param session
     * @param meetingReportRecordDto
     * @return
     */
    public MeetingReportRecord addMeetingReport(Session session, MeetingReportRecordDto meetingReportRecordDto) {
        if(StringUtils.isEmpty(meetingReportRecordDto.getTopic())) {
            LehandException.throwException("汇报主题不能为空！");
        }
        if(StringUtils.isEmpty(meetingReportRecordDto.getReportContent())) {
            LehandException.throwException("汇报内容不能为空！");
        }
        meetingReportRecordDto.setCompid(session.getCompid());
        meetingReportRecordDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        meetingReportRecordDto.setCreateuserid(session.getUserid());
        meetingReportRecordDto.setOrgid(String.valueOf(session.getCurrentOrg().getOrgid()));
        meetingReportRecordDto.setOrgname(session.getCurrentOrg().getOrgname());
        meetingReportRecordDto.setOrgcode(session.getCurrentOrg().getOrgcode());
        Long id = generalSqlComponent.insert(MeetingSqlCode.saveMeetingReportRecord, meetingReportRecordDto);
        meetingReportRecordDto.setId(id);

        //更新附件
        meetingService.updateAttachmentInfo(id, meetingReportRecordDto.getMeetingAttachList(), MagicConstant.NUM_STR.NUM_10, session);
        //添加待办
        Map<String, Object> map = new HashMap<>();
        map.put("organcode",meetingReportRecordDto.getOrgcode());
        TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
        addTodoItem(session, String.valueOf(id),tDzzInfo.getParentcode(),Todo.ItemType.TYPE_MEETING_REPORT_AUDIT.getCode(),Todo.FunctionURL.MEETING_REPORT_AUDIT_PATH,meetingReportRecordDto.getTopic());
        return meetingReportRecordDto;
    }

    /**
     *工作汇报生成待办事项
     * @param session
     * @param id  工作汇报id
     * @param orgcode  要生成的待办组织编码
     * @param category   生成待办类型
     * @param meetingReportAuditPath 待办事项公用字段
     */
    private void addTodoItem(Session session, String id, String orgcode, Integer category, Todo.FunctionURL meetingReportAuditPath,String topic) {
        Map<String,Object> todoItem = new HashMap<>(14);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        todoItem.put("bizid", "MEETING_REPORT_"+id);
        todoItem.put("orgid", orgcode);
        todoItem.put("category",category);
        todoItem.put("name","["+topic+"]"+meetingReportAuditPath.getName());
        todoItem.put("path",meetingReportAuditPath.getPath());
        todoItem.put("state",0);
        todoItem.put("userid",null);
        todoItem.put("username",null);
        todoItem.put("time", sdf.format(new Date()));
        todoItem.put("compid", session.getCompid());
        todoItem.put("deleted",0);
        todoItem.put("node",0);
        generalSqlComponent.query(SqlCode.addTodoItem,todoItem);
    }

    /**
     *工作汇报删除待办事项
     * @param id 工作汇报id
     */
    private void updateTodoItem(String id) {
        generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{11,"MEETING_REPORT_"+id});
    }
    /**
     * 根据ID查询工作汇报
     * @param session
     * @param id
     * @return
     */
    public MeetingReportRecordDto get(Session session, Long id) {
        if(StringUtils.isEmpty(String.valueOf(id))) {
            LehandException.throwException("主键ID不能为空！");
        }
        return generalSqlComponent.query(MeetingSqlCode.getMeetingReportRecord, new Object[] {session.getCompid(), id});
    }

    /**
     * 查询工作报告记录
     * @param session
     * @param id
     * @return
     */
    public MeetingReportRecordDto getMeetingReport(Session session, Long id) {
        MeetingReportRecordDto meetingReportRecordDto = get(session, id);
        if(meetingReportRecordDto == null) {
            LehandException.throwException("工作汇报记录不存在！");
        }

        //上报人员  组织
        String createuserid = String.valueOf(meetingReportRecordDto.getCreateuserid());
        String orgname = meetingReportRecordDto.getOrgname();
//        Map<String,Object> dyData = generalSqlComponent.getDbComponent().getMap(SqlCode.getTDYInfo, new Object[]{createuserid});
//        String xm = StringUtils.isEmpty((String) dyData.get("xm"))?"":(String) dyData.get("xm");
        meetingReportRecordDto.setReportName(orgname);
        //附件
        meetingReportRecordDto.setMeetingAttachList(meetingService.getAttachmentInfo(String.valueOf(id),3, MagicConstant.NUM_STR.NUM_10,session));
        return meetingReportRecordDto;
    }

    /**
     * 修改工作汇报
     * @param session
     * @param meetingReportRecordDto
     * @return
     */
    public MeetingReportRecordDto modifyMeetingReport(Session session, MeetingReportRecordDto meetingReportRecordDto) {
        if(StringUtils.isEmpty(String.valueOf(meetingReportRecordDto.getId()))) {
            LehandException.throwException("主键ID不存在！");
        }
        if(StringUtils.isEmpty(meetingReportRecordDto.getTopic())) {
            LehandException.throwException("汇报主题不能为空！");
        }
        if(StringUtils.isEmpty(meetingReportRecordDto.getReportContent())) {
            LehandException.throwException("汇报内容不能为空！");
        }
        meetingReportRecordDto.setCompid(session.getCompid());
        meetingReportRecordDto.setUpdateuserid(session.getUserid());
        meetingReportRecordDto.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        MeetingReportRecordDto reportDto = get(session, meetingReportRecordDto.getId());
        if(reportDto == null) {
            LehandException.throwException("工作汇报记录不存在,请刷新后重试！");
        }
        generalSqlComponent.update(MeetingSqlCode.updateMeetingReportRecord, meetingReportRecordDto);

        //删除待办
        updateTodoItem(String.valueOf(meetingReportRecordDto.getId()));
        //生成驳回待办
        if(Constant.MEETING_REPORT_STATUS_FIVE==meetingReportRecordDto.getStatus()){
            addTodoItem(session, String.valueOf(meetingReportRecordDto.getId()),meetingReportRecordDto.getOrgcode(),Todo.ItemType.TYPE_MEETING_REPORT_AUDIT.getCode(),Todo.FunctionURL.MEETING_REPORT_AUDIT_BACK_PATH,reportDto.getTopic());
        }
        //驳回编辑后重新生成待办
        if(Constant.MEETING_REPORT_STATUS_TWO==meetingReportRecordDto.getStatus()){
            Map<String, Object> map = new HashMap<>();
            map.put("organcode",session.getCurrentOrg().getOrgcode());
            TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
            addTodoItem(session, String.valueOf(meetingReportRecordDto.getId()),tDzzInfo.getParentcode(),Todo.ItemType.TYPE_MEETING_REPORT_AUDIT.getCode(),Todo.FunctionURL.MEETING_REPORT_AUDIT_PATH,reportDto.getTopic());
        }
        //更新附件信息
        meetingService.updateAttachmentInfo(meetingReportRecordDto.getId(),meetingReportRecordDto.getMeetingAttachList(),MagicConstant.NUM_STR.NUM_10,session);
        return meetingReportRecordDto;
    }

    /**
     * 逻辑删除工作汇报
     * @param session
     * @param id
     * @return
     */
    public void removeMeetingReport(Session session, Long id) {
        if(StringUtils.isEmpty(String.valueOf(id))) {
            LehandException.throwException("主键ID不存在！");
        }

        MeetingReportRecordDto reportDto = get(session, id);
        if(reportDto == null) {
            LehandException.throwException("工作汇报记录不存在,请刷新后重试！");
        }
        generalSqlComponent.update(MeetingSqlCode.updateMeetingReportStatus, new Object[] {0, session.getCompid(), id});
        //删除待办
        updateTodoItem(String.valueOf(id));
    }

    /**
     * 获取当前组织上传模板
     * @param session
     * @param orgid
     * @param orgcode
     * @return
     */
    public Pager pageMeetingReportTemplate(Session session, String orgid, String orgcode,Pager pager) {
        List<Map<String, Object>> list = generalSqlComponent.getDbComponent().listMap(MeetingSqlCode.pageMeetingReportTemplate, new Object[]{session.getCompid(), orgid, orgcode});
        generalSqlComponent.getDbComponent().pageMap(pager,MeetingSqlCode.pageMeetingReportTemplate, new Object[]{session.getCompid(), orgid, orgcode});
        List<Map<String, Object>> pageRows = (List<Map<String, Object>>) pager.getRows();
        if(pageRows!=null&&pageRows.size()>0){
            for (Map<String, Object> map: pageRows) {
                Map<String,Object> m =  generalSqlComponent.query(MeetingSqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",session.getCompid());put("id",map.get("file_id"));}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    map.put("fileid",map.get("file_id"));
                    map.put("flpath", String.format("%s%s", downIp, dir));
                    map.put("name",m.get("name").toString().substring(0,m.get("name").toString().indexOf(".")));
                }
            }
        }
        return  pager;
    }

    /**
     * 添加关联文件模板
     * @param session
     * @return
     */
    public MeetingReportTemplate addMeetingReportTemplate(Session session, String fileId, String fileName) {
        MeetingReportTemplate meetingReportTemplate =new MeetingReportTemplate();
        meetingReportTemplate.setFileId(fileId);
        meetingReportTemplate.setFileName(fileName);
        meetingReportTemplate.setCompid(session.getCompid());
        meetingReportTemplate.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        meetingReportTemplate.setOrgid(String.valueOf(session.getCurrentOrg().getOrgid()));
        meetingReportTemplate.setOrgcode(session.getCurrentOrg().getOrgcode());
        generalSqlComponent.getDbComponent().insert(MeetingSqlCode.addMeetingReportTemplate, meetingReportTemplate);
        return meetingReportTemplate;
    }

    /**
     * 删除模板
     * @param id
     */
    public void delMeetingReportTemplate(String id) {
        generalSqlComponent.getDbComponent().delete(MeetingSqlCode.delMeetingReportTemplate, new Object[] {id});
    }

    /**
     * 重新上传修改模板
     * @param id
     * @param fileId
     */
    public void updateMeetingReportTemplate(String id, String fileId) {
        generalSqlComponent.getDbComponent().update(MeetingSqlCode.updateMeetingReportTemplate, new Object[] {fileId,id});
    }

    public boolean batchModify(Session session,String status,String Remark2, String ids) {
        if(StringUtils.isEmpty(ids)) {
            LehandException.throwException("主键ID不能传空！");
        }
        if(StringUtils.isEmpty(status)) {
            LehandException.throwException("状态参数不能传空！");
        }
        String[] split = ids.split(",");
        for (String id: split) {
            MeetingReportRecordDto reportDto = generalSqlComponent.query(MeetingSqlCode.getMeetingReportRecord, new Object[]{session.getCompid(), id});
            if(reportDto == null) {
                LehandException.throwException("工作汇报记录不存在,请刷新后重试！");
            }
            reportDto.setCompid(session.getCompid());
            reportDto.setUpdateuserid(session.getUserid());
            reportDto.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
            reportDto.setRemark2(Remark2);
            reportDto.setStatus(Integer.valueOf(status));

            generalSqlComponent.update(MeetingSqlCode.updateMeetingReportRecord, reportDto);

            //删除待办
            updateTodoItem(id);
            //生成驳回待办
            if(Constant.MEETING_REPORT_STATUS_FIVE==Integer.valueOf(status)){
                addTodoItem(session,id,reportDto.getOrgcode(),Todo.ItemType.TYPE_MEETING_REPORT_AUDIT.getCode(),Todo.FunctionURL.MEETING_REPORT_AUDIT_BACK_PATH,reportDto.getTopic());
            }
        }
        return true;
    }
}
