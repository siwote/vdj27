package com.lehand.meeting.dto;

import com.lehand.meeting.pojo.MeetingNoticeRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

@ApiModel(value="会议通知列表", description="会议通知列表")
public class MNoticeListDto  extends MeetingNoticeRecord {

    private static final long serialVersionUID = 1L;
//2020-0520取消
//    @ApiModelProperty(value="已读&未读人员名单", name="noticeSub")
//    private Map<String,Object> noticeSub;
//
//    @ApiModelProperty(value="已读人数", name="noticeSubNum")
//    private Map<String,Object> noticeSubNum;

    @ApiModelProperty(value="附加字段", name="visible")
    private boolean visible;

    @ApiModelProperty(value="判断会议是否已经结束", name="checkIsEnd")
    private boolean checkIsEnd;

    //2020-0520添加
    @ApiModelProperty(value="会议记录状态:0 不存在,1 已保存,2 已提交", name="mrstatus")
    private String mrstatus;

    @ApiModelProperty(value="能否撤回&能否参加取消参加", name="revokeble")
    private boolean revokeble;

    @ApiModelProperty(value="会议记录id", name="mrid")
    private String mrid;

    @ApiModelProperty(value="参会人数", name="num")
    private String num;

//    public Map<String, Object> getNoticeSub() {
//        return noticeSub;
//    }
//
//    public void setNoticeSub(Map<String, Object> noticeSub) {
//        this.noticeSub = noticeSub;
//    }
//
//    public Map<String, Object> getNoticeSubNum() {
//        return noticeSubNum;
//    }
//
//    public void setNoticeSubNum(Map<String, Object> noticeSubNum) {
//        this.noticeSubNum = noticeSubNum;
//    }

    public boolean getCheckIsEnd() {
        return checkIsEnd;
    }

    public void setCheckIsEnd(boolean checkIsEnd) {
        this.checkIsEnd = checkIsEnd;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getMrstatus() {
        return mrstatus;
    }

    public void setMrstatus(String mrstatus) {
        this.mrstatus = mrstatus;
    }

    public String getMrid() {
        return mrid;
    }

    public void setMrid(String mrid) {
        this.mrid = mrid;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public boolean isRevokeble() {
        return revokeble;
    }

    public void setRevokeble(boolean revokeble) {
        this.revokeble = revokeble;
    }
}
