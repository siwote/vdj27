package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="会议类型统计", description="会议类型统计")
public class MeetingTypeStatisticsDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="会议类型编码", name="mType")
    private String mType;

    @ApiModelProperty(value="会议类型名称", name="mTypeName")
    private String mTypeName;

    @ApiModelProperty(value="未召开记录月份", name="unConvenedMon")
    private String unConvenedMon;

    @ApiModelProperty(value="已召开次数", name="convened")
    private Integer convened;

    @ApiModelProperty(value="要求说明", name="requirement")
    private String requirement;

    @ApiModelProperty(value="达标状态", name="standardStatus")
    private Integer standardStatus;

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmTypeName() {
        return mTypeName;
    }

    public void setmTypeName(String mTypeName) {
        this.mTypeName = mTypeName;
    }

    public String getUnConvenedMon() {
        return unConvenedMon;
    }

    public void setUnConvenedMon(String unConvenedMon) {
        this.unConvenedMon = unConvenedMon;
    }

    public Integer getConvened() {
        return convened;
    }

    public void setConvened(Integer convened) {
        this.convened = convened;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public Integer getStandardStatus() {
        return standardStatus;
    }

    public void setStandardStatus(Integer standardStatus) {
        this.standardStatus = standardStatus;
    }
}
