package com.lehand.meeting.controller;

import com.alibaba.fastjson.JSONObject;
import com.lehand.base.common.Message;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.cache.CacheComponent;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.constant.Constant;
import com.lehand.horn.partyorgan.controller.HornBaseController;
import com.lehand.horn.partyorgan.service.NewLoginService;
import com.lehand.horn.partyorgan.util.MessageUtil;
import com.lehand.meeting.service.WxChartsUserService;
import com.lehand.meeting.utils.HttpRequestUtils;
import com.lehand.meeting.utils.WeChatUtils;
import com.lehand.module.urc.config.SessionConfig;
import com.lehand.module.urc.dto.LoginRequest;
import com.lehand.module.urc.pojo.User;
import com.lehand.module.urc.pojo.UserAccountView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

@Api(value = "微信", tags = { "微信" })
@RestController
@RequestMapping("/power/wx")
public class WxchartsController extends HornBaseController{

    private static final Logger logger = LogManager.getLogger(WxchartsController.class);

    @Resource
    private CacheComponent cacheComponent;

    @Resource
    private WxChartsUserService wxChartsUserService;

    @Resource
    private NewLoginService newLoginService;

    @Resource
    private SessionConfig sessionConfig;

    @OpenApi(optflag=1)
    @ApiOperation(value = "微信回调", notes = "微信回调", httpMethod = "POST")
    @RequestMapping(value = "/rs", method = {RequestMethod.GET, RequestMethod.POST})
    public void get(HttpServletRequest request, HttpServletResponse response, Session session) throws Exception {
        //如果为get请求，则为开发者模式验证
        PrintWriter out = response.getWriter();
        if ("get".equals(request.getMethod().toLowerCase())) {
            //doGet();//在开发者模式验证中已处理，在此省略
            System.out.println("开发者模式");
            String echostr = request.getParameter("echostr");
            out.print(echostr);
        } else {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            try {
                Map<String, String> map = MessageUtil.xmlToMap(request);
                String ToUserName = map.get("ToUserName");
                String FromUserName = map.get("FromUserName");
                request.getSession().setAttribute("openid",FromUserName);
                String CreateTime = map.get("CreateTime");
                String MsgType = map.get("MsgType");
                String message = "";
                if (MsgType.equals(Constant.WXSTATUS.EVENT)) {
                    //从集合中，获取是哪一种事件传入
                    String eventType = map.get("Event");
                    //对获取到的参数进行处理,会议id
                    String eventKey = map.get("EventKey");
                    //扫描带参数的二维码，如果用户未关注，则可关注公众号，事件类型为subscribe；用户已关注，则事件类型为SCAN
                    if (eventType.equals(Constant.WXSTATUS.EVENT_SCAN)) {
                        //根据openID 获取用户信息
                        User user = wxChartsUserService.getUserByOpenId(FromUserName);
                        //如果结果为空,则未绑定,如果不为空,则已绑定
                        if(user == null){
                            message = WeChatUtils.createContent(FromUserName,ToUserName,CreateTime,1);
                        }else{//已绑定
                            try {
                                wxChartsUserService.signMeeting(user,eventKey);
                            } catch (Exception e) {
                                logger.error("签到异常："+e.getMessage());
                                LehandException.throwException("请勿重复签到!");
                                //提示请勿重复签到
                               message =  WeChatUtils.createContent(FromUserName,ToUserName,CreateTime,3);
                               out.print(message);
                            }
                            //提示签到成功
                            message = WeChatUtils.createContent(FromUserName,ToUserName,CreateTime,2);
                        }
                    }
                }
                //返回转换后的XML字符串
                out.print(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.close();
        }
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "登陆", notes = "登陆", httpMethod = "POST")
    @RequestMapping(value = "/loginByWeiXin", method = RequestMethod.POST)
    public Message sign(
            @RequestParam(value = "account") String account ,
            @RequestParam(value = "passwd") String passwd,
            @RequestParam(value = "openid") String openid,
            @ApiIgnore LoginRequest req,
            HttpServletRequest request, HttpServletResponse response
    ){
        UserAccountView user  = wxChartsUserService.getUserViewByAccount(account,passwd);

        if (0 == user.getStatus()) {
            LehandException.throwException("该用户已被禁用!");
        }
            //校验验证码
//        this.newLoginService.checkCaptcha(req,user);

        //绑定
        Message msg = new Message();
//        User user1 = new User();
        Long userid = user.getUserid();
        try {
            wxChartsUserService.updateUserOpenId(openid,userid);
            msg.success();
        } catch (LehandException e) {
            msg.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            logger.error("绑定异常: " + e.getMessage());
            msg.fail("绑定失败,请重新绑定!");
        }
        req.setAccount(user.getAccount());
        Session session = sessionConfig.createSession(req, request, user);
        session.setLogintime(Long.parseLong(create_timestamp()));
//        return  msg.success().setData(session).setLog(user.getUsername() + "登录成功!");
        return msg;
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "签名", notes = "签名", httpMethod = "POST")
    @RequestMapping(value = "/doGetSign", method = RequestMethod.POST)
    public Message doGetSign(@RequestParam(value = "url") String url){
        Message msg = new Message();
        String urlStr = null;
        try {
            urlStr = URLDecoder.decode(url,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return msg.success().setData(doSign(urlStr));
    }


    public  Map<String, String> doSign(String url) {
        Map<String, String> ret = new HashMap<String, String>(16);
        String noncestr = create_nonce_str();
        String timestamp = create_timestamp();
        String jsapi_ticket;
        jsapi_ticket = WeChatUtils.getJsApiTicket();
        String string1;
        String signature = "";

        //注意这里参数名必须全部小写，且必须有序
        string1 = "jsapi_ticket=" + jsapi_ticket +
                "&noncestr=" + noncestr +
                "&timestamp=" + timestamp +
                "&url=" + url;
        System.out.println(string1);

        try
        {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(string1.getBytes("UTF-8"));
            signature = byteToHex(crypt.digest());
            System.out.println(signature);
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }

        ret.put("url", url);
        ret.put("jsapi_ticket", jsapi_ticket);
        ret.put("appId",Constant.APP_ID );
        ret.put("nonceStr", noncestr);
        ret.put("timestamp", timestamp);
        ret.put("signature", signature);

        return ret;
    }



    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    private static String create_nonce_str() {
        return RandomStringUtils.randomAlphanumeric(16);
//        return UUID.randomUUID().toString();
    }

    private static String create_timestamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "微信", notes = "微信", httpMethod = "GET")
    @RequestMapping(value = "/callBack", method = RequestMethod.GET)
    public void callBack(HttpServletRequest request, HttpServletResponse response,String code,String ip){
        String openId = "";
        try {
            System.out.println("code:"+code);
            String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
            String urlStr = url.replace("APPID", Constant.APP_ID).replace("SECRET", Constant.APP_SECRET).replace("CODE", code);
            String res = HttpRequestUtils.sendGet(urlStr);
            logger.info("获取url成功,结果为: " + res);
            JSONObject jsonData = JSONObject.parseObject(res);
            if(!jsonData.containsKey("errcode")){
                openId = jsonData.get("openid").toString();
                logger.info("openID值为: " + openId);
            }
//
//            Map<String, String> map = MessageUtil.xmlToMap(request);
//            String ToUserName = map.get("ToUserName");
//            String FromUserName = map.get("FromUserName");
//            request.getSession().setAttribute("openid",FromUserName);
//            String CreateTime = map.get("CreateTime");
//            String MsgType = map.get("MsgType");
//            openId=FromUserName;
        } catch (Exception e) {
            logger.error("获取url异常: ", e.getMessage());
        }
            try {
//                response.sendRedirect(String.format("http://df4gjw.natappfree.cc/h5/pages/login/login"+"?openid=%s", openId));
                response.sendRedirect(String.format(WeChatUtils.authUrl+"?openid=%s", openId));
            } catch (IOException e) {
                logger.error("重定向失败: ", e.getMessage());
            }
        }

}
