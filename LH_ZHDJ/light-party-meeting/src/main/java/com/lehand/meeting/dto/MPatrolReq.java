package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="党建巡查参数",description="党建巡查参数")
public class MPatrolReq  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true, name="compid")
    private Long compid;

    @ApiModelProperty(value="组织机构id" , name="organid")
    private String organid;



    @ApiModelProperty(value="开始时间", name="starttime")
    private String starttime;

    @ApiModelProperty(value="结束时间", name="endtime")
    private String endtime;

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}
