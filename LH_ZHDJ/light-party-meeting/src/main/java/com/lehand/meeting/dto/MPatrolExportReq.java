package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class MPatrolExportReq extends MPatrolPagerReq {

    @ApiModelProperty(value="文件名", name="tablenames")
    private String tablenames;

    @ApiModelProperty(value="列名", name="tablesStr")
    private String[] tablesStr;

    public String getTablenames() {
        return tablenames;
    }

    public void setTablenames(String tablenames) {
        this.tablenames = tablenames;
    }

    public String[] getTablesStr() {
        return tablesStr;
    }

    public void setTablesStr(String[] tablesStr) {
        this.tablesStr = tablesStr;
    }
}
