package com.lehand.meeting.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="谈心谈话记录", description="谈心谈话记录")
public class MeetingTalkRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="租户id", name="compid")
    private Long compid;

    @ApiModelProperty(value="主键id", name="id")
    private Long id;

    @ApiModelProperty(value="主题", name="topic")
    private String topic;

    @ApiModelProperty(value="谈话时间", name="recordtime")
    private String recordtime;

    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="谈话类容", name="context")
    private String context;

    @ApiModelProperty(value="创建人ID", name="createuserid")
    private Long createuserid;

    @ApiModelProperty(value="修改时间", name="updatetime")
    private String updatetime;

    @ApiModelProperty(value="修改人ID", name="updateuserid")
    private Long updateuserid;

    @ApiModelProperty(value="组织机构编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="组织机构id", name="orgid")
    private String orgid;

    @ApiModelProperty(value="备注1", name="remark1")
    private Integer remark1;

    @ApiModelProperty(value="备注2", name="remark2")
    private String remark2;

    @ApiModelProperty(value="状态（ 0  正常,  -1 删除）", name="status")
    private Integer status;

    @ApiModelProperty(value="组织机构名称", name="orgname")
    private String orgname;


    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getRecordtime() {
        return recordtime;
    }

    public void setRecordtime(String recordtime) {
        this.recordtime = recordtime;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Long getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(Long createuserid) {
        this.createuserid = createuserid;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Long getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(Long updateuserid) {
        this.updateuserid = updateuserid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public Integer getRemark1() {
        return remark1;
    }

    public void setRemark1(Integer remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }
}