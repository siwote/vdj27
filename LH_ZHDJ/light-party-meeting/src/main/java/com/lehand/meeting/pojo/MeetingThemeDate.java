package com.lehand.meeting.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "主题党日日期", description = "主题党日日期")
public class MeetingThemeDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增长主键", name = "id")
    private Long id;

    @ApiModelProperty(value = "年份", name = "year")
    private Integer year;

    @ApiModelProperty(value = "主题党日日期( 1~31)", name = "day")
    private Integer day;

    @ApiModelProperty(value = "开始日期", name = "startdate")
    private String startdate;

    @ApiModelProperty(value = "结束日期", name = "enddate")
    private String enddate;

    @ApiModelProperty(value = "组织编码", name = "orgcode")
    private String orgcode;

    @ApiModelProperty(value = "组织ID", name = "orgid")
    private Long orgid;

    @ApiModelProperty(value = "组织名称", name = "orgname")
    private String orgname;

    @ApiModelProperty(value = "状态  (-1:失效；0 有效，用于判定回显上次设置时间)", name = "status")
    private Integer status;

    @ApiModelProperty(value = "租户ID", name = "compid")
    private Long compid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }
}
