package com.lehand.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@ApiModel(value="党建巡查列表数据",description="党建巡查列表数据")
public class MeetingPatrolListDto  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="组织机构id", name="organid")
    private String organid ;

    @ApiModelProperty(value="组织机构编码", name="organcode")
    private String organcode;

    @ApiModelProperty(value="组织类别编码", name="zzlb")
    private String zzlb;

    @ApiModelProperty(value="组织机构名称", name="organname")
    private String organname;

    @ApiModelProperty(value="统计值对象", name="valuedata")
    private Map<String,Object> valuedata;


    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }

    public String getOrgancode() {
        return organcode;
    }

    public void setOrgancode(String organcode) {
        this.organcode = organcode;
    }

    public String getZzlb() {
        return zzlb;
    }

    public void setZzlb(String zzlb) {
        this.zzlb = zzlb;
    }

    public String getOrganname() {
        return organname;
    }

    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public Map<String, Object> getValuedata() {
        return valuedata;
    }

    public void setValuedata(Map<String, Object> valuedata) {
        this.valuedata = valuedata;
    }
}
