package com.lehand.meeting.constant;

public class ScheduleCode {

    public static final String SCHEDULE_LIST = "/batchtask/list";

    public static final String SCHEDULE_ADD = "/batchtask/add";

    public static final String SCHEDULE_PAUSE = "/batchtask/pause";

    public static final String SCHEDULE_RESUME = "/batchtask/resume";

    public static final String SCHEDULE_DEL = "/batchtask/del";

    public static final String SCHEDULE_UPDATEJOB = "/batchtask/updateJob";
}
