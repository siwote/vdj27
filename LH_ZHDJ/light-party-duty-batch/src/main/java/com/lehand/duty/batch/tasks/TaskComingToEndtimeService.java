package com.lehand.duty.batch.tasks;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.duty.pojo.TkMyTask;
import com.lehand.base.constant.DateEnum;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.dao.TkMyTaskDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkRemindRule;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.service.TkMyTaskLogService;
import com.lehand.duty.service.TkRemindRuleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;

public class TaskComingToEndtimeService {

	private static final Logger logger = LogManager.getLogger(TaskComingToEndtimeService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务
	
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkMyTaskLogService tkMyTaskLogService;
	@Resource private TkRemindRuleService tkRemindRuleService;
	@Resource private DutyCacheComponent dutyCacheComponent;
	
	@Resource
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@SuppressWarnings("unchecked")
	public void comingToEndtime() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			
			
//			Date current = DateUtil.beforeDay(days);
			//查询子表任务按结束时间升序
			final List<TkMyTask> list = tkMyTaskDao.listComingToEndtime() ;
			if (list.size()<=0) {
				return;
			}
			logger.info("定时操作即将到达截止时间开始...");
			for (final TkMyTask info : list) {
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							//判断是否开启了
							TkRemindRule rule = dutyCacheComponent.getTkRemindRule(info.getCompid(),"BACK_END_BEFORE");
							if (!rule.isOpen()) {
								return;
							}
							//将具体的日期时间取出来
							String date = rule.getParams();
							//json转MAP
							Map<String,Object> map = JSON.parseObject(date, Map.class);
							Double day = Double.valueOf(map.get("day").toString());
							String time = map.get("time").toString();
							//day为double类型所以换算成秒
							int days = new Double(day*3600*24).intValue();
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(DateEnum.YYYYMMDDHHMMDD.parse(info.getDatenode()));
							calendar.add(Calendar.SECOND, -days);
							Date current = calendar.getTime();
							String now = DateEnum.YYYYMMDDHHMMDD.format(current);
							//获取任务的暂停和恢复的时间
							TkTaskDeploy deploy = tkTaskDeployDao.get(info.getCompid(),info.getTaskid());
							String suspendtime = deploy.getRemarks5();//暂停时间
							String recoverytime = deploy.getRemarks6();//恢复时间
							if(now.compareTo(DateEnum.YYYYMMDDHHMMDD.format())<=0) {
								if(!StringUtils.isEmpty(suspendtime) && !StringUtils.isEmpty(recoverytime)) {//任务已经恢复了
									if(now.compareTo(suspendtime)<0 || now.compareTo(recoverytime)>0) {
										//消息产生的时间处于暂停与恢复之间则消息不写入登记簿,反之正常发送
										TaskProcessEnum.BACK_END_BEFORE.handle(null, info,time,now);
									}						
								}else {//一般没有暂停的任务
									TaskProcessEnum.BACK_END_BEFORE.handle(null, info,time,now);
								}
								//任务即将结束发提醒标识1
								tkMyTaskDao.updateRemarks2(info.getCompid(),info.getId());
							}
						} catch (Exception e) {
							logger.error("操作即将到达截止时间异常",e);
						}
					}
				});
			}
			logger.info("定时操作即将到达截止时间结束...");
		} catch (Exception e) {
			logger.error("定时操作即将到达截止时间异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	public static void main(String[] args) throws ParseException {
		Double day=2.0;
		int days = new Double(day*3600*24).intValue();
//		System.out.println(days);
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(DateEnum.YYYYMMDDHHMMDD.parse("2019-01-13 23:59:59")); 
		calendar.add(Calendar.SECOND, -days);
		Date current = calendar.getTime();
		String now = DateEnum.YYYYMMDDHHMMDD.format(current);
		System.out.println(now);
	}
}
