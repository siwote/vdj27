package com.lehand.duty.batch.tasks;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkTaskFeedTime;
import com.lehand.duty.pojo.TkTaskRemindList;
import com.lehand.duty.service.TkTaskRemindNoteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import com.lehand.duty.dto.Subject;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.lang.Long;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;


public class FeedRemindNoticeService {

	private static final Logger logger = LogManager.getLogger(FeedRemindNoticeService.class);
	
	private static final AtomicBoolean NOTICE = new AtomicBoolean(false);

	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	@Resource GeneralSqlComponent generalSqlComponent;
	
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	public void notice() throws LehandException {

		try {
			synchronized (NOTICE) {
				if (NOTICE.get()) {
					logger.info("之前提醒任务正在处理,本次不处理");
					return;
				}
				NOTICE.set(true);
			}
			List<Long> compids = generalSqlComponent.query(SqlCode.listCompid, new Object[]{});
			String now = DateEnum.YYYYMMDDHHMMDD.format();
			Session session = new Session();
			for (Long compid : compids) {
				session.setCompid(compid);
				List<TkTaskFeedTime> feedTimes = generalSqlComponent.query(SqlCode.listRemindFeedTime, new Object[]{compid,now,now});
				String timeids = generalSqlComponent.query(SqlCode.listTimeIds, new Object[]{compid,now,now});
				List<Map<String,Object>> subjectList = generalSqlComponent.query(SqlCode.listRemindSubject, new Object[]{compid, compid, now, now});
				if(CollectionUtils.isEmpty(feedTimes)||CollectionUtils.isEmpty(subjectList)|| StringUtils.isEmpty(timeids)){
					continue;
				}
				HashMap<String, Object> params = new HashMap<>();
				params.put("status",2);//不能发送
				params.put("ids",timeids);
				generalSqlComponent.query(SqlCode.updateTimeStatus,params);

				Map<String, List<Subject>> subjectMap = listToMap(subjectList);
				for (TkTaskFeedTime feedTime : feedTimes) {
					Long taskid = feedTime.getTaskid();
					List<Subject> subjects = subjectMap.get(String.valueOf(taskid));
					if(CollectionUtils.isEmpty(subjects)){
						continue;
					}
					for (Subject subject : subjects) {
						threadPoolTaskExecutor.execute(new Runnable() {
							public void run() {
								try {
									insert(session, Module.MY_TASK.getValue(),taskid, "FEED_REMIND",
											"反馈节点", "您有一个任务需要在"+feedTime.getFeedtime()+"之前反馈，反馈要求："+feedTime.getRequirement(),
											subject.getSubjectid(),subject.getSubjectname());
									params.put("status",1);//已发送
									params.put("ids",timeids);
									generalSqlComponent.query(SqlCode.updateTimeStatus, params);
								} catch (Exception e) {
									logger.error("消息处理异常",e);
								}
							}
						});

					}
				}
			}

		} catch (Exception e) {
			logger.error("消息处理异常",e);
		} finally {
			NOTICE.set(false);
		}
	}

	public Map<String, List<Subject>> listToMap(List<Map<String, Object>> subjectList) {
		HashMap<String, List<Subject>> subjectMap = new HashMap<>();
		for (Map<String, Object> map : subjectList) {
			Long taskid = (Long) map.get("taskid");
			Long subjectid = (Long) map.get("subjectid");
			String subjectname = (String) map.get("subjectname");
			Subject subject = new Subject();
			subject.setSubjectname(subjectname);
			subject.setSubjectid(subjectid);

			List<Subject> subjects = subjectMap.get(String.valueOf(taskid));
			if(subjects==null){
				subjects = new ArrayList<Subject>();
				subjectMap.put(String.valueOf(taskid),subjects);
			}
			subjects.add(subject);

		}
		return subjectMap;
	}

	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
	/**
	   * 消息对象的创建
	 * @param session 发送人信息
	 * @param module 模块
	 * @param mid 模块对应的id
	 * @param rulecode 提醒规则
	 * @param rulename 规则名称
	 * @param mesg 提醒消息文本
	 * @param subjectid 被提醒人id
	 * @param subjectname 被提醒人名称
	 * @return
	 */
	public void insert(Session session,int module,Long mid,String rulecode,String rulename,String mesg,Long subjectid,String subjectname) {
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(session.getCompid());
		ls.setNoteid(-1L);
		ls.setModule(module);//模块(0:任务发布,1:任务反馈)
		ls.setMid(mid);//对应模块的ID
		ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format());//提醒日期
		ls.setRulecode(rulecode);//编码
		ls.setRulename(rulename);
		ls.setOptuserid(0l);
		ls.setOptusername("系统消息");
		ls.setUserid(subjectid);//被提醒人
		ls.setUsername(subjectname);//
		ls.setIsread(0);//
		ls.setIshandle(0);//
		ls.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
		ls.setMesg(mesg);//消息
		ls.setRemarks1(1);//备注1
		ls.setRemarks2(0);//备注2
		ls.setRemarks3("工作督查");//备注3
		ls.setRemarks4(Constant.EMPTY);//备注3
		ls.setRemarks5(Constant.EMPTY);//备注3
		ls.setRemarks6(Constant.EMPTY);//备注3
		long id = generalSqlComponent.insert(SqlCode.addTTRLByTuihui, ls);
		ls.setId(id);
	}
	
}
