package com.lehand.duty.batch.tasks;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.duty.dao.TkMyTaskDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkMyTaskLog;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.service.TkMyTaskLogService;
import com.lehand.duty.service.TkRemindRuleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;


public class TaskOverNoBackService {

	private static final Logger logger = LogManager.getLogger(TaskOverNoBackService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkMyTaskLogService tkMyTaskLogService;
	@Resource private TkRemindRuleService tkRemindRuleService;
	
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	public void overNoBack() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			final List<TkMyTask> list = tkMyTaskDao.listOverBack() ;
			if(list.size()<=0) {
				return;
			}
			logger.info("定时操作逾期任务开始...");
			for (final TkMyTask info : list) {
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							//查询该任务是否存在反馈记录
							int num = tkMyTaskLogService.listFeedBack(info.getCompid(),info.getId());
							//获取任务的暂停和恢复的时间
							TkTaskDeploy deploy = tkTaskDeployDao.get(info.getCompid(),info.getTaskid());
							String suspendtime ="";
							String recoverytime ="";
							if(deploy != null) {
								suspendtime = deploy.getRemarks5();//暂停时间
								recoverytime = deploy.getRemarks6();//恢复时间
							}
							String now = DateEnum.YYYYMMDDHHMMDD.format();
							if(num<=0) {//不存在
								if(!StringUtils.isEmpty(suspendtime) && !StringUtils.isEmpty(recoverytime)) {//任务已经恢复了
									if(now.compareTo(suspendtime)<0 || now.compareTo(recoverytime)>0) {
										//消息产生的时间处于暂停与恢复之间则消息不写入登记簿,反之正常发送
										createOverBack(info);
									}
								}else {//一般没有暂停的任务
									createOverBack(info);
								}
							}
							//更新逾期标识
							tkMyTaskDao.updateRemarks1(info.getCompid(),info.getId());
						} catch (Exception e) {
							logger.error("创建操作逾期任务异常",e);
						}
					}
				});
			}
			logger.info("定时操作逾期任务结束...");
		} catch (Exception e) {
			logger.error("定时操作逾期任务异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	/**
	 * 调用统一的操作方法
	 * @param mytask
	 * @throws LehandException
	 */
	public void createOverBack(TkMyTask mytask) throws LehandException {
		TaskProcessEnum.OVER_BACK.handle(null, mytask);
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
