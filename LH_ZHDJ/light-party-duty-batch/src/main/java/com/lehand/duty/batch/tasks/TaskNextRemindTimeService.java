package com.lehand.duty.batch.tasks;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.dao.TkMyTaskDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkRemindRule;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskRemindNote;
import com.lehand.duty.service.TkTaskRemindNoteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;

public class TaskNextRemindTimeService {

	private static final Logger logger = LogManager.getLogger(TaskNextRemindTimeService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private DutyCacheComponent dutyCacheComponent;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@SuppressWarnings("unchecked")
	public void nextRemindTime() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			
			final List<TkTaskRemindNote> list = tkTaskRemindNoteService.list() ;
			if (list.size()<=0) {
				return;
			}
			logger.info("定时操作下次反馈提醒开始...");
			for (final TkTaskRemindNote info : list) {
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							//判断是否开启了
							TkRemindRule rule = dutyCacheComponent.getTkRemindRule(info.getCompid(),"BACK_NEXT_BEFORE");
							if (!rule.isOpen()) {
								return;
							}
							//将具体的日期时间取出来
							String date1 = rule.getParams();
							Map<String,Object> map = JSON.parseObject(date1, Map.class);
							Double day = Double.valueOf(map.get("day").toString());
							String time = map.get("time").toString()+":00";
							int days = new Double(day*3600*24).intValue();
							String date = info.getRemindtime();
							Date d = null;
							if(date.length()<19) {
								d = DateEnum.YYYYMMDD.parse(date);
							}else {
								d = DateEnum.YYYYMMDDHHMMDD.parse(date);
							}
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(d); 
							calendar.add(Calendar.SECOND, -days);
							Date current = calendar.getTime();
							String now = DateEnum.YYYYMMDDHHMMDD.format(current);
							//获取任务的暂停和恢复的时间
							TkTaskDeploy deploy = tkTaskDeployDao.get(info.getCompid(),info.getMid());
							String suspendtime ="";
							String recoverytime ="";
							if(deploy != null) {
								suspendtime = deploy.getRemarks5();//暂停时间
								recoverytime = deploy.getRemarks6();//恢复时间
							}
							if(now.compareTo(DateEnum.YYYYMMDDHHMMDD.format())<=0) {
								List<TkMyTask> mytask = tkMyTaskDao.listByTaskid(info.getCompid(),info.getMid());
								if(mytask!=null && mytask.size()>0) {
									for (TkMyTask my : mytask) {
										if(!StringUtils.isEmpty(suspendtime) && !StringUtils.isEmpty(recoverytime)) {//任务已经恢复了
											if(now.compareTo(suspendtime)<0 || now.compareTo(recoverytime)>0) {
												//消息产生的时间处于暂停与恢复之间则消息不写入登记簿,反之正常发送
												TaskProcessEnum.BACK_NEXT_BEFORE.handle(null, my,time,date);
											}						
										}else {//一般没有暂停的任务
											TaskProcessEnum.BACK_NEXT_BEFORE.handle(null, my,time,date);	
										}
									}//更新扫描标识
									tkTaskRemindNoteService.updateRemarks2(info.getCompid(),info.getId());
								}
							}
						} catch (Exception e) {
							logger.error("定时操作下次反馈提醒开始异常",e);
						}
					}
				});
			}
			logger.info("定时操作下次反馈提醒开始结束...");
		} catch (Exception e) {
			logger.error("定时操作下次反馈提醒开始异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
}
