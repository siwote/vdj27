package com.lehand.duty.batch.tasks;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.lehand.duty.pojo.TkMyTask;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dao.TkMyTaskDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskSubject;
import com.lehand.duty.pojo.TkTaskTimeNode;
import com.lehand.duty.service.TkTaskSubjectService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;


public class TaskCreateService {

	private static final String DELETE_TASK_BACK_WHERE_TASKID = "DELETE FROM tk_my_task WHERE taskid=?";
	private static final String DELETE_SUBJECT_WHERE_TASKID = "DELETE FROM tk_task_subject WHERE taskid=?";
	private static final String DELETE_TIME_NODE_WHERE_TASKID = "DELETE FROM tk_task_time_node WHERE taskid=?";
	private static final String DELETE = "DELETE FROM tk_my_task WHERE taskid=? and datenode=?";
	
//	private static final String FIND_TASKDEPLOY_UPDATE = "SELECT * FROM tk_task_deploy where remarks1=?";
	private static final String FIND_BY_STATUS = "SELECT * FROM tk_task_time_node where status=? or status="+ Constant.TkTaskTimeNode.STATUS_TEMP;
	private static final String FIND = "SELECT * FROM tk_task_time_node where  status=? and enddate>?";//crtdate<=? and 一次性将所有时间节点都创建完
	private static final String FIND_UPDATE_STATUS = "UPDATE tk_task_time_node set status="+Constant.TkTaskTimeNode.STATUS_TEMP+" where  status=? and enddate>?";//一次性创建完crtdate<=? and
	private static final String UPDATE_TIMENODE = "UPDATE tk_task_time_node SET status=? WHERE taskid=? and enddate=?";
	
	private static final Logger logger = LogManager.getLogger(TaskCreateService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务
//	private static final AtomicBoolean UPDATE = new AtomicBoolean(false);//是否变更任务信息
	
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkTaskSubjectService tkTaskSubjectService;
	@Resource private TkMyTaskDao tkTaskBackDao;
	@Resource
	GeneralSqlComponent generalSqlComponent;
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@PostConstruct
	public void init() {
		List<TkTaskTimeNode> list = generalSqlComponent.getDbComponent().listBean(FIND_BY_STATUS,
				TkTaskTimeNode.class,new Object[] {Constant.TkTaskTimeNode.STATUS_CREATE_ING});
		logger.warn("之前正在创建的时间节点的个数:{}",list.size());
		for (TkTaskTimeNode info : list) {
			logger.warn("处理{}任务的{}时间节点...",info.getTaskid(),info.getEnddate());
			deleteTkTaskBacks(info);
			update(info,Constant.TkTaskTimeNode.STATUS_NO_CREATE);
			logger.warn("处理{}任务的{}时间节点完成",info.getTaskid(),info.getEnddate());
		}
	}

	public void create() {
		try {
			List<TkTaskTimeNode>  list = null;
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在创建,本次不处理");
					return;
				}
				list = findAndUpdate();
				CREATE.set(true);
			}

			if (list==null || list.size()<=0) {
				return;
			}
			logger.info("定时创建任务开始...");
			for (final TkTaskTimeNode info : list) {
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							createOneTimeNode(info);
						} catch (Exception e) {
							update(info,Constant.TkTaskTimeNode.STATUS_CREATE_FIAL);
							logger.error("创建任务节点异常",e);
						}
					}
				});
			}
			logger.info("定时创建任务结束...");
		} catch (Exception e) {
			logger.error("定时创建任务异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	/**
	 * 查询未创建的时间节点
	 * @return
	 */
	@Transactional(rollbackFor=Exception.class)
	public List<TkTaskTimeNode> findAndUpdate(){
		String now = DateEnum.YYYYMMDD.format()+" 00:00:00";
		Object[] args = new Object[] {Constant.TkTaskTimeNode.STATUS_NO_CREATE,now};
		List<TkTaskTimeNode> list =generalSqlComponent.getDbComponent().listBean(FIND,TkTaskTimeNode.class,args);
		if (list==null || list.size()<=0) {
			return list;
		}
		generalSqlComponent.getDbComponent().update(FIND_UPDATE_STATUS, args);
		return list;
	}
	
	/**
	 * 创建一个时间节点的任务
	 * @param info
	 * @throws Exception 
	 */
	@Transactional(rollbackFor=Exception.class)
	public void createOneTimeNode(TkTaskTimeNode info) throws Exception {
		try {
			TkTaskDeploy deploy = tkTaskDeployDao.get(info.getCompid(),info.getTaskid());
			update(info,Constant.TkTaskTimeNode.STATUS_CREATE_ING);
			long taskid = info.getTaskid();
			List<TkTaskSubject> subjects = tkTaskSubjectService.findSubjects(info.getCompid(),taskid,Constant.TkTaskSubject.FLAG_EXCUTE);
			for (TkTaskSubject subject : subjects) {
				createSubjectTaskByTkTaskTimeNode(deploy, info, subject);
			}
			update(info,Constant.TkTaskTimeNode.STATUS_CREATE_FINISH);
		} catch (Exception e) {
			update(info,Constant.TkTaskTimeNode.STATUS_CREATE_FIAL);
			logger.error("创建{}-{}时间节点的任务异常",info.getTaskid(),info.getEnddate(),e);
			throw e;
		}
	}

	/**
	 * 更新时间节点的状态
	 * @param info
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	public void update(TkTaskTimeNode info,int status) {
		logger.info("更新{}-{}条记录的状态..",info.getTaskid(),info.getEnddate());
		generalSqlComponent.getDbComponent().update(UPDATE_TIMENODE, new Object[]{status,info.getTaskid(),info.getEnddate()});
	}
	
	/**
	 * 创建执行主体的任务
	 * @param node
	 * @param subject
	 * @throws LehandException 
	 */
	public void createSubjectTaskByTkTaskTimeNode(TkTaskDeploy deploy,TkTaskTimeNode node,TkTaskSubject subject) throws LehandException {
		TkMyTask info = new TkMyTask();
		info.setCompid(deploy.getCompid());
		info.setTaskid(node.getTaskid());//任务ID(自增长主键)
		info.setDatenode(node.getEnddate());//时间节点(等价于截至日)
		info.setSjtype(subject.getSjtype());//主体类型(0:用户,1:机构/组/部门,2:角色)
		info.setSubjectid(subject.getSubjectid());//主体ID(用户ID、角色ID、机构ID等等)
		info.setSubjectname(subject.getSubjectname());
		info.setCreatetime(node.getCrtdate());//创建时间(这里原本是写的当前时间，但是在我的任务，任务详情中有个任务的开始时间，所以将时间节点表中的创建时间赋给我的任务的创建时间)2018/12/17修改
		info.setNewtime(Constant.EMPTY);//最新反馈时间
		info.setProgress(0.0);//反馈进度值
		info.setStatus(Constant.TkMyTask.STATUS_NO_SGIN);//状态(0:删除,1:草稿,2:未签收,3:已签收,4:暂停,5:完成)
		info.setScore(0.0);//评分
		info.setIntegral(0.0);//积分
		info.setRemarks1(0);//备注1
		info.setRemarks2(0);//备注2
		info.setRemarks3(Constant.EMPTY);//备注3
		info.setRemarks4(Constant.EMPTY);//备注3
		info.setRemarks5(Constant.EMPTY);//备注3
		info.setRemarks6(Constant.EMPTY);//备注3
		info.setAnnex(deploy.getAnnex());
		info.setPublisher(deploy.getPublisher());
		info.setTphone(deploy.getTphone());
		tkTaskBackDao.insert(info);
		TaskProcessEnum.CREATE.handle(null, deploy, subject, info);
	}
	
	/**
	 * 删除任务反馈信息
	 * @param info
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW,noRollbackFor=Exception.class)
	public void deleteTkTaskBacks(TkTaskTimeNode info) {
		generalSqlComponent.getDbComponent().delete(DELETE, new Object[] {info.getTaskid(),info.getEnddate()});
	}

	/**
	 * 删除时间节点和主体信息
	 * @param taskid
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW,noRollbackFor=Exception.class)
	public void delete(Long taskid) {
		Object[] args = new Object[] {taskid};
		generalSqlComponent.getDbComponent().delete(DELETE_TIME_NODE_WHERE_TASKID, args);
		generalSqlComponent.getDbComponent().delete(DELETE_SUBJECT_WHERE_TASKID, args);
		generalSqlComponent.getDbComponent().delete(DELETE_TASK_BACK_WHERE_TASKID, args);
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
