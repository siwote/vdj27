package com.lehand.duty.batch.tasks;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.duty.common.Constant;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.pojo.TkRemindRule;
import com.lehand.duty.pojo.TkTaskRemindNote;
import com.lehand.duty.service.TkTaskRemindNoteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;

public class RemindNoticeService {

	private static final Logger logger = LogManager.getLogger(RemindNoticeService.class);
	
	private static final AtomicBoolean NOTICE = new AtomicBoolean(false);
	
	@Resource private DutyCacheComponent dutyCacheComponent;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	public void notice() throws LehandException {
		try {
			List<TkTaskRemindNote> list = null;
			synchronized (NOTICE) {
				if (NOTICE.get()) {
					logger.info("之前提醒任务正在处理,本次不处理");
					return;
				}
				String now = DateEnum.YYYYMMDDHHMMDD.format();
				list = tkTaskRemindNoteService.findNeedSendRemind(Constant.TkTaskRemindNote.SEND_NO, now);
				logger.info("序号提醒内容的数量:{}",list.size());
				if (list.size()<=0) {
					return;
				}
				for (TkTaskRemindNote note : list) {
					String rulecode = note.getRulecode().trim();
					RemindRuleEnum rule = RemindRuleEnum.valueOf(rulecode);
					if (rule==null) {
						logger.error("{}编码不存在!-->id:{}",rulecode,note.getId());
						continue;
					}
					TkRemindRule rr = dutyCacheComponent.getTkRemindRule(note.getCompid(),rulecode);
					if (!rr.isOpen()) {
						logger.info("{}规则没有打开,则不创建提醒!",rr.getCode());
						continue;
					}
					updateSend(note,Constant.TkTaskRemindNote.SEND_NOT);
					threadPoolTaskExecutor.execute(new Runnable() {
						public void run() {
							try {
								logger.info("提醒信息-->ID:{},模块:{},MID：{},日期:{},编码:{}",note.getId(),note.getModule(),note.getMid(),note.getRemindtime(),note.getRulecode());
								rule.notice(rr, note,null);
								updateSend(note,Constant.TkTaskRemindNote.SEND_YES);
							} catch (Exception e) {
								logger.error("消息处理异常",e);
							}
						}
					});
				}
				NOTICE.set(true);
			}
			
		} catch (Exception e) {
			logger.error("消息处理异常",e);
		} finally {
			NOTICE.set(false);
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void updateSend(TkTaskRemindNote note,int issend) {
		tkTaskRemindNoteService.update(note.getCompid(),issend, note.getId());
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
