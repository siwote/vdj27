package com.lehand.duty.batch.tasks;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.duty.dao.TkMyTaskDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.pojo.TkIntegralRule;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskRemindNote;
import com.lehand.duty.service.TkIntegralDetailsService;
import com.lehand.duty.service.TkIntegralRuleService;
import com.lehand.duty.service.TkMyTaskLogService;
import com.lehand.duty.service.TkTaskRemindNoteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import com.lehand.base.constant.DateEnum;

public class TaskOverRemindService {

	private static final Logger logger = LogManager.getLogger(TaskOverRemindService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkMyTaskLogService tkMyTaskLogService;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	@Resource private TkIntegralDetailsService tkIntegralDetailsService;
	@Resource private TkIntegralRuleService tkIntegralRuleService;
	
	private String REMIND_BACK="REMINDBACK";//积分规则表中配置的code
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	

	public void overRemind() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			final List<TkTaskRemindNote> list = tkTaskRemindNoteService.listOverRemind();
			if (list.size()<=0) {
				return;
			}
			logger.info("定时操作过了提醒时间而未反馈的任务开始...");
			for (final TkTaskRemindNote info : list) {
				TkIntegralRule tkIntegralRule = tkIntegralRuleService.get(info.getCompid(),REMIND_BACK);
				Double inteval = tkIntegralRule.getInteval();
				if(inteval<=0) {//积分规则中扣分为0就无需往下执行了
					continue;
				}
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							String now = DateEnum.YYYYMMDDHHMMDD.format();
							//获取任务的暂停和恢复的时间
							TkTaskDeploy deploy = tkTaskDeployDao.get(info.getCompid(),info.getMid());
							String suspendtime ="";
							String recoverytime ="";
							if(deploy != null) {
								suspendtime = deploy.getRemarks5();//暂停时间
								recoverytime = deploy.getRemarks6();//恢复时间
							}
							//查询该任务的所有子任务
							List<TkMyTask> mytasks = tkMyTaskDao.listByTaskid(info.getCompid(),info.getMid());
							//查找该提醒时间的上一个时间
							TkTaskRemindNote tkTaskRemindNote = tkTaskRemindNoteService.getMinRemind(info.getCompid(),info.getMid());
							for (TkMyTask mytask : mytasks) {
								//查询该任务是否存在反馈记录
								int num = tkMyTaskLogService.listFeedBack(info.getCompid(),mytask.getId(),tkTaskRemindNote.getRemindtime(),DateEnum.YYYYMMDDHHMMDD.format());
								if(num<=0) {//不存在
									if(!StringUtils.isEmpty(suspendtime) && !StringUtils.isEmpty(recoverytime)) {//任务已经恢复了
										if(now.compareTo(suspendtime)<0 || now.compareTo(recoverytime)>0) {
											//消息产生的时间处于暂停与恢复之间则消息不写入登记簿,反之正常发送
											tkIntegralDetailsService.insert(mytask.getCompid(),mytask.getSubjectid(), mytask.getSubjectname(), REMIND_BACK, mytask.getTaskid(), mytask.getId(), -inteval,0L);
										}						
									}else {//一般没有暂停的任务
										tkIntegralDetailsService.insert(mytask.getCompid(),mytask.getSubjectid(), mytask.getSubjectname(), REMIND_BACK, mytask.getTaskid(), mytask.getId(), -inteval,0L);
									}
								}
							}
							//更新扫描标识
							tkTaskRemindNoteService.updateRemarks1(info.getCompid(),info.getId());
						} catch (Exception e) {
							logger.error("定时操作过了提醒时间而未反馈的任务异常",e);
						}
					}
				});
			}
			logger.info("定时操作过了提醒时间而未反馈的任务结束...");
		} catch (Exception e) {
			logger.error("定时操作过了提醒时间而未反馈的任务异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
