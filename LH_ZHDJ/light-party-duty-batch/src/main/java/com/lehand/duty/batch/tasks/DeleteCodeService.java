package com.lehand.duty.batch.tasks;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.duty.common.SqlCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;

public class DeleteCodeService {

	private static final Logger logger = LogManager.getLogger(DeleteCodeService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务
	
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public void deleteCode() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			logger.info("定时操作删除验证码开始...");
			threadPoolTaskExecutor.execute(new Runnable() {
				public void run() {
					try {
						generalSqlComponent.delete(SqlCode.delPwCode, new Object[] {DateEnum.YYYYMMDDHHMMDD.format()});
					} catch (Exception e) {
						logger.error("删除验证码异常",e);
					}
				}
			});
			logger.info("定时操作删除验证码结束...");
		} catch (Exception e) {
			logger.error("定时操作删除验证码异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
