package com.lehand.duty.batch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.lehand.base.util.SpringUtil;

@EnableTransactionManagement
@SpringBootApplication
@PropertySource(value = { "file:config/application.yml"})//,"file:config/exception.properties" })
@ImportResource(value = { "file:config/spring.xml" })
@ComponentScan(basePackages= {"com.lehand.**"})
public class DutyBatchApplication {

	private static final Logger logger = LogManager.getLogger(DutyBatchApplication.class);
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(DutyBatchApplication.class, args);
		SpringUtil.setContext(context);
	}
 
//	@Bean
//	public HttpMessageConverters fastJsonConfigure() {
//		FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//		FastJsonConfig fastJsonConfig = new FastJsonConfig();
//		fastJsonConfig.setSerializerFeatures(//SerializerFeature.PrettyFormat,
//							                 SerializerFeature.WriteNullStringAsEmpty,
//							                 SerializerFeature.WriteMapNullValue,
//							                 SerializerFeature.WriteNullListAsEmpty,
//							                 SerializerFeature.WriteNullBooleanAsFalse);
//		fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");// 日期格式化
//		converter.setFastJsonConfig(fastJsonConfig);
//		return new HttpMessageConverters(converter);
//	}
	
	@Bean
    public ServletRegistrationBean DruidStatViewServlet(){
        System.out.println("servletRegistrationBean configure start.");
        //org.springframework.boot.context.embedded.ServletRegistrationBean提供类的进行注册.
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(),"/druid/*");
        //添加初始化参数
        servletRegistrationBean.addInitParameter("allow","127.0.0.1");
        servletRegistrationBean.addInitParameter("loginUsername","lehand");
        servletRegistrationBean.addInitParameter("loginPassword","lehand@123");
        //是否可以重置
        servletRegistrationBean.addInitParameter("resetEnable","fase");
        return servletRegistrationBean;
    }

    /**
     * 注册一个：filterRegistrationBean
     * @return
     */
    @Bean
    public FilterRegistrationBean druidStatFilter(){
        logger.info("druidbatch filer configure start.");
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
        //添加过滤规则.
        filterRegistrationBean.addUrlPatterns("/*");
        //添加不需要忽略的格式信息.
        filterRegistrationBean.addInitParameter("exclusions","*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return filterRegistrationBean;
    }
}
