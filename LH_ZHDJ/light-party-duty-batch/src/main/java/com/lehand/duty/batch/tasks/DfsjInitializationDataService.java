package com.lehand.duty.batch.tasks;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;

public class DfsjInitializationDataService {

	private static final Logger logger = LogManager.getLogger(DfsjInitializationDataService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	//党支部code
	@Value("${jk.organ.dangzhibu}")
	private String dangzhibu;
	
	//党支部code
	@Value("${jk.organ.lianhedangzhibu}")
	private String lianhedangzhibu;

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	
	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	

	public void initializationData() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			final List<Map<String,Object>> list = db().listMap("select a.remarks2,a.orgtypeid,b.organid from urc_organization a left join t_dzz_info b on a.orgcode=b.organcode where a.orgtypeid=? or a.orgtypeid=? group by a.orgid", new Object[] {dangzhibu,lianhedangzhibu});
			if (list.size()<=0) {
				return;
			}
			logger.info("定时生成各个党支部的党费收缴任务开始...");
			
			int year = LocalDate.now().getYear();
			int month = LocalDate.now().getMonthValue();
			//遍历各个党支部
			for (final Map<String,Object> maps : list) {
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							//查询h_dy_jfjs
							List<Map<String,Object>> list2 = db().listMap("select * from h_dy_jfjs  where compid=? and organid=?", new Object[] {1,maps.get("organid")});
							//检查h_dy_jfinfo该月是否有数据并且数据是否全面
							List<Map<String,Object>> list1 = db().listMap("select * from h_dy_jfinfo  where compid=? and year=? and month=? and organid=? and status=1", new Object[] {1,year,month,maps.get("organid")});
							if((list1==null || list1.size()<=0) && list2!=null && list2.size()>0) {
								for (Map<String, Object> map : list2) {
//									Map<String,Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and min<=? and max>? limit 1", new Object[] {1,Double.valueOf(map.get("basefee").toString()),Double.valueOf(map.get("basefee").toString())});
									Map<String, Object> jssz =db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[] {1, map.get("basefee")});
									if(jssz==null) {
										LehandException.throwException("党费缴纳比例未设置，请先设置再操作！");
									}
									if(map.get("onpost")!=null && jssz!=null) {
										Map<String, Object> params = getParams(Double.valueOf(map.get("basefee").toString()), map.get("userid").toString(), map.get("username").toString(), maps.get("organid").toString(), map.get("onpost").toString(), map.get("adjusttime").toString(), Long.valueOf(map.get("ajuserid").toString()), map.get("ajusername").toString(), 1L, Integer.valueOf(year), Integer.valueOf(month), jssz.get("ratio").toString());
										db().insert("INSERT INTO h_dy_jfinfo (userid, year, month, basefee, rate, payable, actual, status, remark, onpost, organid, username, ajuserid, ajusername, adjusttime, compid) VALUES (:userid, :year, :month, :basefee, :rate, :payable, :actual, :status, :remark, :onpost, :organid, :username, :ajuserid, :ajusername, :adjusttime, :compid)", params);
									}
								}
							}
						} catch (Exception e) {
							logger.error("定时生成各个党支部的党费收缴任务异常",e);
						}
					}
				});
			}
			logger.info("定时生成各个党支部的党费收缴任务结束...");
		} catch (Exception e) {
			logger.error("定时生成各个党支部的党费收缴任务异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	
	public Map<String, Object> getParams(Double basefee, String userid, String username, String organid, String onpost,
			String adjusttime, Long ajuserid, String ajusername, Long compid, int year, int month,
			String jssz) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("userid", userid);
		params.put("year", year);
		params.put("month", month);
		params.put("basefee", basefee);
		
		Double ratio = Double.valueOf(jssz);
		params.put("rate", ratio);
		
	    DecimalFormat df=new DecimalFormat("0.00");
	    Double yjdf = basefee*ratio/100;
	    String yjdf2 = df.format(yjdf);
		params.put("payable", yjdf2);
		
		params.put("actual", 0);
		params.put("status", 1);
		params.put("remark", "");
		params.put("onpost", onpost);
		params.put("organid", organid);
		params.put("username", username);
		params.put("ajuserid", ajuserid);
		params.put("ajusername", ajusername);
		params.put("adjusttime", adjusttime);
		params.put("compid", compid);
		if("0".equals(onpost)) {//不在岗
			params.put("basefee", 0);
			params.put("rate", 0);
			params.put("payable", basefee);
		}
		return params;
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
