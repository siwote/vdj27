package com.lehand.horn.partyorgan.dto.info;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class ElectionExportReq implements Serializable {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="列名", name="tablesStr")
    private String[] tablesStr;

    @ApiModelProperty(value="组织机构id", name="organid")
    private String organid;

    @ApiModelProperty(value="状态", name="status")
    private String status;

    public String[] getTablesStr() {
        return tablesStr;
    }

    public void setTablesStr(String[] tablesStr) {
        this.tablesStr = tablesStr;
    }

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
