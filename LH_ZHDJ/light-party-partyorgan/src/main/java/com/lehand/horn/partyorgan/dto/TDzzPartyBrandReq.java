package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * 领航计划(党建品牌)表扩展类
 * @author lx
 */
@ApiModel(value="领航计划(党建品牌)表扩展类",description="领航计划(党建品牌)表扩展类")
public class TDzzPartyBrandReq extends TDzzPartyBrand {

    @ApiModelProperty(value="品牌类型(1:培育品牌、2:示范品牌、3:推优上级品牌，默认培育品牌)名称",name="typeName")
    private String typeName;

    @ApiModelProperty(value="品牌logo图片",name="file")
    private Map<String,Object> file;

    @ApiModelProperty(value="党组织名称",name="orgname")
    private String orgName;

    @ApiModelProperty(value="当前组织是否点赞，0是，1否",name="isPraise")
    private Integer isPraise;

    public Integer getIsPraise() {
        return isPraise;
    }

    public void setIsPraise(Integer isPraise) {
        this.isPraise = isPraise;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Map<String, Object> getFile() {
        return file;
    }

    public void setFile(Map<String, Object> file) {
        this.file = file;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


}