package com.lehand.horn.partyorgan.pojo.djpp;


public class OrgNews  {
	
    /**
    *
    * This field was generated by MyBatis Generator.
    * This field corresponds to the database column org_news.compid
    *
    * @mbg.generated Mon Sep 16 18:30:42 CST 2019
    */
   private Long compid;

   /**
    *
    * This field was generated by MyBatis Generator.
    * This field corresponds to the database column org_news.id
    *
    * @mbg.generated Mon Sep 16 18:30:42 CST 2019
    */
   private Long id;

   /**
    * This method was generated by MyBatis Generator.
    * This method returns the value of the database column org_news.compid
    *
    * @return the value of org_news.compid
    *
    * @mbg.generated Mon Sep 16 18:30:42 CST 2019
    */
   public Long getCompid() {
       return compid;
   }

   /**
    * This method was generated by MyBatis Generator.
    * This method sets the value of the database column org_news.compid
    *
    * @param compid the value for org_news.compid
    *
    * @mbg.generated Mon Sep 16 18:30:42 CST 2019
    */
   public void setCompid(Long compid) {
       this.compid = compid;
   }

   /**
    * This method was generated by MyBatis Generator.
    * This method returns the value of the database column org_news.id
    *
    * @return the value of org_news.id
    *
    * @mbg.generated Mon Sep 16 18:30:42 CST 2019
    */
   public Long getId() {
       return id;
   }

   /**
    * This method was generated by MyBatis Generator.
    * This method sets the value of the database column org_news.id
    *
    * @param id the value for org_news.id
    *
    * @mbg.generated Mon Sep 16 18:30:42 CST 2019
    */
   public void setId(Long id) {
       this.id = id;
   }
	
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.orgcode
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String orgcode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.title
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    
    private String orgname;
      
    public String getOrgname() {
		return orgname;
	}

	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

	private String title;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.titlepic
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String titlepic;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.status
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private Short status;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.publishtime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String publishtime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.creater
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String creater;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.createtime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String createtime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.modifier
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String modifier;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.moditime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String moditime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.remark1
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private Long remark1;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.remark2
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private Long remark2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.remark3
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String remark3;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.remark4
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String remark4;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column org_news.content
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    private String content;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.orgcode
     *
     * @return the value of org_news.orgcode
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getOrgcode() {
        return orgcode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.orgcode
     *
     * @param orgcode the value for org_news.orgcode
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode == null ? null : orgcode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.title
     *
     * @return the value of org_news.title
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.title
     *
     * @param title the value for org_news.title
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.titlepic
     *
     * @return the value of org_news.titlepic
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getTitlepic() {
        return titlepic;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.titlepic
     *
     * @param titlepic the value for org_news.titlepic
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setTitlepic(String titlepic) {    	
        this.titlepic = titlepic;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.status
     *
     * @return the value of org_news.status
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public Short getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.status
     *
     * @param status the value for org_news.status
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.publishtime
     *
     * @return the value of org_news.publishtime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getPublishtime() {
        return publishtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.publishtime
     *
     * @param publishtime the value for org_news.publishtime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setPublishtime(String publishtime) {
        this.publishtime = publishtime == null ? null : publishtime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.creater
     *
     * @return the value of org_news.creater
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getCreater() {
        return creater;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.creater
     *
     * @param creater the value for org_news.creater
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setCreater(String creater) {
        this.creater = creater == null ? null : creater.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.createtime
     *
     * @return the value of org_news.createtime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.createtime
     *
     * @param createtime the value for org_news.createtime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.modifier
     *
     * @return the value of org_news.modifier
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.modifier
     *
     * @param modifier the value for org_news.modifier
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.moditime
     *
     * @return the value of org_news.moditime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getModitime() {
        return moditime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.moditime
     *
     * @param moditime the value for org_news.moditime
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setModitime(String moditime) {
        this.moditime = moditime == null ? null : moditime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.remark1
     *
     * @return the value of org_news.remark1
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public Long getRemark1() {
        return remark1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.remark1
     *
     * @param remark1 the value for org_news.remark1
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setRemark1(Long remark1) {
        this.remark1 = remark1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.remark2
     *
     * @return the value of org_news.remark2
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public Long getRemark2() {
        return remark2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.remark2
     *
     * @param remark2 the value for org_news.remark2
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setRemark2(Long remark2) {
        this.remark2 = remark2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.remark3
     *
     * @return the value of org_news.remark3
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getRemark3() {
        return remark3;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.remark3
     *
     * @param remark3 the value for org_news.remark3
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.remark4
     *
     * @return the value of org_news.remark4
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getRemark4() {
        return remark4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.remark4
     *
     * @param remark4 the value for org_news.remark4
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setRemark4(String remark4) {
        this.remark4 = remark4 == null ? null : remark4.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column org_news.content
     *
     * @return the value of org_news.content
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column org_news.content
     *
     * @param content the value for org_news.content
     *
     * @mbg.generated Mon Sep 16 18:30:42 CST 2019
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
    
    private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}