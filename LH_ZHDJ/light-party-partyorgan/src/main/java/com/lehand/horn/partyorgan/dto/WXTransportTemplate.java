package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 微信推新消息入参类
 * @author lx
 */
public class WXTransportTemplate {

    private String openid;//目标客户

    private String url;  //跳转链接

    private String templateid;  //消息模板id

    private String user;  //姓名

    private String type;  //消息类型

    private String date;  //日期年月

    private String payable;  //应交党费

    private String actual;  //实交待确认

    private String nowTime;  //当前时间

    private String remark;  //备注信息

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTemplateid() {
        return templateid;
    }

    public void setTemplateid(String templateid) {
        this.templateid = templateid;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPayable() {
        return payable;
    }

    public void setPayable(String payable) {
        this.payable = payable;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
