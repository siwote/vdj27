package com.lehand.horn.partyorgan.constant;

public class HJXXSqlCode {

    /**
     * 分页查询换届信息
     */
    public static String pageHJXX="pageHJXX";
    /**
     * 获取延迟历史信息集合
     */
    public static String listDelayInfo="listDelayInfo";
    /**
     * 获取最新一条换届信息
     */
    public static String getHJXXLimit1="getHJXXLimit1";
    /**
     * 添加换届信息
     */
    public static String insertHjxxInfo="insertHjxxInfo";
    /**
     *删除换届提醒配置
     */
    public static String deleteHjxxRemindConfig="deleteHjxxRemindConfig";
    /**
     * 删除换届提醒时间节点
     */
    public static String deleteHjxxRemindTime="deleteHjxxRemindTime";
    /**
     * 添加换届提醒配置
     */
    public static String insertHjxxRemindConfig="insertHjxxRemindConfig";
    /**
     * 添加换届提醒时间节点
     */
    public static String insertHjxxRemindTime="insertHjxxRemindTime";
    /**
     * 修改换届信息
     */
    public static String updateStatusByHjxxInfo="updateStatusByHjxxInfo";
    /**
     * 添加延期记录
     */
    public static String insertDelayInfo="insertDelayInfo";
    /**
     * 修改换届提醒配置
     */
    public static String updateHjxxRemindConfig="updateHjxxRemindConfig";
    /**
     *获取换届提醒所有时间节点
     */
    public static String listRemindInfo="listRemindInfo";
    /**
     *根据时间节点修改时间节点状态
     */
    public static String updateRemindTime="updateRemindTime";
    /**
     * 统计换届信息
     */
    public static String statisticalHJXX="statisticalHJXX";
    /**
     * 获取换届周期提醒配置
     */
    public static String getHjxxRemindConfig="getHjxxRemindConfig";
    /**
     * 添加换届提醒周期配置日志
     */
    public static String insertRemindConfigLog="insertRemindConfigLog";
    /**
     * 获取换届周期配置日志
     */
    public static String getRemindConfigLog="getRemindConfigLog";
}

