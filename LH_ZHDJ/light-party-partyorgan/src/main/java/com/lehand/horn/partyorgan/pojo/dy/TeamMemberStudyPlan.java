package com.lehand.horn.partyorgan.pojo.dy;

import java.util.List;
import java.util.Map;

public class TeamMemberStudyPlan {

	private Long id;
	private Long compid;
	private String orgcode;
    private String name;
	private String createtime;
    private String files;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private List<Map<String,Object>> listFiles;

    public List<Map<String, Object>> getListFiles() {
        return listFiles;
    }

    public void setListFiles(List<Map<String, Object>> listFiles) {
        this.listFiles = listFiles;
    }
}