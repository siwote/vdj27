package com.lehand.horn.partyorgan.controller.djpp;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.horn.partyorgan.controller.HornBaseController;
import com.lehand.horn.partyorgan.service.djpp.BrandService;

import static com.lehand.components.web.interceptors.BaseInterceptor.getSession;
/**
 * 党建品牌管理
 * @author tangtao
 * @date 2019年9月16日 
 * @description 
 */
@Api(value = "党建品牌管理", tags = { "党建品牌管理" })
@RestController
@RequestMapping("/lhdj/brand")
public class BrandController extends HornBaseController {
	private static final Logger logger = LogManager.getLogger(BrandController.class);
	@Resource
	private BrandService brandService;
	
	/**
	 * 保存党建品牌
	 * @param brandname 品牌名称
	 * @param brandtime 品牌创建时间
	 * @param brandslogan 宣传语
	 * @param brandlogo	logo
	 * @param brandintro 简介
	 * @param brandidea	理念
	 * @param brandmentality 思路
	 * @param measures 举措
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "保存党建品牌", notes = "保存党建品牌", httpMethod = "POST")
	@RequestMapping("/save")
	public Message save(String brandname,String brandtime,String brandslogan,String brandlogo,String brandintro
			,String brandidea,String brandmentality,String measures) {
		Message msg = new Message();
		try {
			int result = brandService.save(brandname,brandtime,brandslogan,brandlogo,brandintro
					,brandidea,brandmentality,measures,getSession());
			if(result>0) {				
				msg.success();
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 * 党建品牌分页
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "党建品牌分页", notes = "党建品牌分页", httpMethod = "POST")
	@RequestMapping("/pageQuery")
	public Message pageQuery(Pager pager) {
		Message msg = new Message();
		try {			
			msg.setData(brandService.pageQuery(pager,getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}		
		return msg;
	}
	
	/**党建品牌详情
	 * @param orgcode
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "党建品牌详情", notes = "党建品牌详情", httpMethod = "POST")
	@RequestMapping("/detail")
	public Message detail(String orgcode) {
		Message msg = new Message();
		try {	
			Pager pager=  new Pager();
			pager.setPageNo(1);
			pager.setPageSize(5);
			msg.setData(brandService.detail(pager,orgcode,getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**党建品牌详情(编辑查询)
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "党建品牌详情(编辑查询)", notes = "党建品牌详情(编辑查询)", httpMethod = "POST")
	@RequestMapping("/brandDetail")
	public Message brandDetail() {
		Message msg = new Message();
		try {	
			Pager pager=  new Pager();
			pager.setPageNo(1);
			pager.setPageSize(5);
			msg.setData(brandService.brandDetail(getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 * 保存新闻详情
	 * @param id 主键id
	 * @param title 新闻标题
	 * @param titlepic 新闻图片
	 * @param publishtime 发布时间
	 * @param content 新闻内容
	 * @return
	 */
	@OpenApi(optflag=1)
    @ApiOperation(value = "保存新闻详情", notes = "保存新闻详情", httpMethod = "POST")
	@RequestMapping("/saveNews")
	public Message saveNews(String id,String title,String titlepic,String publishtime,String content) {
		Message msg = new Message();
		try {			
			int result =  brandService.saveNews(id,title,titlepic,publishtime,content,getSession());
			if(result>0) {
				msg.success();
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=2)
    @ApiOperation(value = "发布新闻", notes = "发布新闻", httpMethod = "POST")
	@RequestMapping("/publishNews")
	public Message updateStatus(String id) {
		Message msg = new Message();
		try {			
			int result =  brandService.updateStatus(id,1,getSession());
			if(result>0) {
				msg.success();
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=2)
    @ApiOperation(value = "取消新闻发布", notes = "取消新闻发布", httpMethod = "POST")
	@RequestMapping("/cancelNews")
	public Message cancelNews(String id) {
		Message msg = new Message();
		try {			
			int result =  brandService.updateStatus(id,0,getSession());
			if(result>0) {
				msg.success();
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=3)
    @ApiOperation(value = "删除新闻", notes = "删除新闻", httpMethod = "POST")
	@RequestMapping("/deleteNews")
	public Message deleteNews(String id) {
		Message msg = new Message();
		try {			
			int result =  brandService.updateStatus(id,-99,getSession());
			if(result>0) {
				msg.success();
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 * 新闻查询页
	 * @param pager
	 * @param likeStr
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "新闻查询页", notes = "新闻查询页", httpMethod = "POST")
	@RequestMapping("/newsPageQuery")
	public Message newsPageQuery(Pager pager,String likeStr) {
		Message msg = new Message();
		try {			
			msg.setData(brandService.newsPageQuery(pager,likeStr,getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	
	/**
	 * 党建新闻详情
	 * @param id
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "党建新闻详情", notes = "党建新闻详情", httpMethod = "POST")
	@RequestMapping("/detailNews")
	public Message newsDetail(String id) {
		Message msg = new Message();
		try {			
			msg.setData(brandService.newsDetail(id,getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 * 点赞功能
	 * @param orgcode
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "点赞功能", notes = "点赞功能", httpMethod = "POST")
	@RequestMapping("/likePost")
	public Message likePost(String orgcode) {
		Message msg = new Message();
		try {
			int result = brandService.postLike(orgcode,getSession());
			if(result>0) {
				msg.success();
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
}
