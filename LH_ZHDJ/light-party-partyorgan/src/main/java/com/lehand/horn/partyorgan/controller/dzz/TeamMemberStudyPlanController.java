package com.lehand.horn.partyorgan.controller.dzz;

import com.alibaba.druid.util.StringUtils;
import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.controller.HornBaseController;
import com.lehand.horn.partyorgan.pojo.dy.TeamMemberStudyPlan;
import com.lehand.horn.partyorgan.service.info.TeamMemberStudyPlanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Api(value = "班子成员学习计划", tags = { "班子成员学习计划" })
@RestController
@RequestMapping("/lhdj/studyplan")
public class TeamMemberStudyPlanController extends HornBaseController {

    private static final Logger logger = LogManager.getLogger(TeamMemberStudyPlanController.class);
    @Resource TeamMemberStudyPlanService teamMemberStudyPlanService;



    @OpenApi(optflag=0)
    @ApiOperation(value = "学习计划新增编辑", notes = "学习计划新增编辑", httpMethod = "POST")
    @RequestMapping("/add")
    public Message add(String name , String files, Long id) {
        Message message = new Message();
        try {
            teamMemberStudyPlanService.add(name,files,id,getSession());
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("学习计划新增/编辑失败");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "学习计划删除", notes = "学习计划删除", httpMethod = "POST")
    @RequestMapping("/remove")
    public Message remove(Long id) {
        Message message = new Message();
        try {
            teamMemberStudyPlanService.remove(id,getSession());
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("学习计划删除失败");
        }
        return message;
    }



    @OpenApi(optflag=0)
    @ApiOperation(value = "学习计划查询", notes = "学习计划查询", httpMethod = "POST")
    @RequestMapping("/getinfo")
    public Message getinfo(Long id) {
        Message message = new Message();
        try {
            TeamMemberStudyPlan data = teamMemberStudyPlanService.getinfo(id,getSession());
            message.setData(data);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("学习计划查询失败");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "学习计划查询", notes = "学习计划查询", httpMethod = "POST")
    @RequestMapping("/list")
    public Message listPager(Pager pager, String name, String starttime, String endtime) {
        Message message = new Message();
        try {
            teamMemberStudyPlanService.listPager(pager,name,starttime,endtime,getSession());
            message.setData(pager);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("学习计划查询失败");
        }
        return message;
    }
}
