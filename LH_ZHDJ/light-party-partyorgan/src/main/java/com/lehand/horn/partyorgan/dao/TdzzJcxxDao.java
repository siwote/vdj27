//package com.lehand.horn.partyorgan.dao;
//
//import java.util.List;
//
//import org.springframework.stereotype.Repository;
//
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzJcxxDzz;
//
///**
// * 奖惩信息
// * @author taogang
// * @version 1.0
// * @date 2019年1月23日, 下午8:41:50
// */
//@Repository
//public class TdzzJcxxDao extends ComBaseDao<TDzzJcxxDzz> {
//
//	/** 奖惩查询 .*/
//	private static final String LIST_BY_ID = "select * from t_dzz_jcxx_dzz where organid = ? order by jcdate desc";
//
//	/**
//	 * 查询奖惩信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public  List<TDzzJcxxDzz> list_jcxx(String organid){
//		return super.list2bean(LIST_BY_ID, TDzzJcxxDzz.class, organid);
//	}
//
//}
