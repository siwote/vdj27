package com.lehand.horn.partyorgan.dto;
/**
 * 微信推新消息参数
 * @author lx
 */
public class TemplateInfo {
    //内容
    private String value;
    //字体颜色
    private String color;

    public TemplateInfo(String value, String color) {
        this.value = value;
        this.color = color;
    }

    public TemplateInfo(){

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
