package com.lehand.horn.partyorgan.pojo.dzz;

public class TDzzHjxxDzz {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.uuid
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String uuid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.organid
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String organid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.organname
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String organname;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.bzversion
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String bzversion;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.hjdate
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String hjdate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.xjtype
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String xjtype;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.xjydhrs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String xjydhrs;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.xjsdhrs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String xjsdhrs;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.jmdate
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String jmdate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.pfzsset
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String pfzsset;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.sjtxzs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String sjtxzs;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.tsexplain
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String tsexplain;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.hjxhcl
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String hjxhcl;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.datasources
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String datasources;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.operator
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String operator;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.operatetime
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String operatetime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.bjbzcsfs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String bjbzcsfs;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.sfdqjc
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String sfdqjc;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.sfxxwz
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private Integer sfxxwz;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.syncstatus
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String syncstatus;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_dzz_hjxx_dzz.synctime
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    private String synctime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.uuid
     *
     * @return the value of t_dzz_hjxx_dzz.uuid
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.uuid
     *
     * @param uuid the value for t_dzz_hjxx_dzz.uuid
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.organid
     *
     * @return the value of t_dzz_hjxx_dzz.organid
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getOrganid() {
        return organid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.organid
     *
     * @param organid the value for t_dzz_hjxx_dzz.organid
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setOrganid(String organid) {
        this.organid = organid == null ? null : organid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.organname
     *
     * @return the value of t_dzz_hjxx_dzz.organname
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getOrganname() {
        return organname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.organname
     *
     * @param organname the value for t_dzz_hjxx_dzz.organname
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setOrganname(String organname) {
        this.organname = organname == null ? null : organname.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.bzversion
     *
     * @return the value of t_dzz_hjxx_dzz.bzversion
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getBzversion() {
        return bzversion;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.bzversion
     *
     * @param bzversion the value for t_dzz_hjxx_dzz.bzversion
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setBzversion(String bzversion) {
        this.bzversion = bzversion == null ? null : bzversion.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.hjdate
     *
     * @return the value of t_dzz_hjxx_dzz.hjdate
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getHjdate() {
        return hjdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.hjdate
     *
     * @param hjdate the value for t_dzz_hjxx_dzz.hjdate
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setHjdate(String hjdate) {
        this.hjdate = hjdate == null ? null : hjdate.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.xjtype
     *
     * @return the value of t_dzz_hjxx_dzz.xjtype
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getXjtype() {
        return xjtype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.xjtype
     *
     * @param xjtype the value for t_dzz_hjxx_dzz.xjtype
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setXjtype(String xjtype) {
        this.xjtype = xjtype == null ? null : xjtype.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.xjydhrs
     *
     * @return the value of t_dzz_hjxx_dzz.xjydhrs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getXjydhrs() {
        return xjydhrs;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.xjydhrs
     *
     * @param xjydhrs the value for t_dzz_hjxx_dzz.xjydhrs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setXjydhrs(String xjydhrs) {
        this.xjydhrs = xjydhrs == null ? null : xjydhrs.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.xjsdhrs
     *
     * @return the value of t_dzz_hjxx_dzz.xjsdhrs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getXjsdhrs() {
        return xjsdhrs;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.xjsdhrs
     *
     * @param xjsdhrs the value for t_dzz_hjxx_dzz.xjsdhrs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setXjsdhrs(String xjsdhrs) {
        this.xjsdhrs = xjsdhrs == null ? null : xjsdhrs.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.jmdate
     *
     * @return the value of t_dzz_hjxx_dzz.jmdate
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getJmdate() {
        return jmdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.jmdate
     *
     * @param jmdate the value for t_dzz_hjxx_dzz.jmdate
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setJmdate(String jmdate) {
        this.jmdate = jmdate == null ? null : jmdate.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.pfzsset
     *
     * @return the value of t_dzz_hjxx_dzz.pfzsset
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getPfzsset() {
        return pfzsset;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.pfzsset
     *
     * @param pfzsset the value for t_dzz_hjxx_dzz.pfzsset
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setPfzsset(String pfzsset) {
        this.pfzsset = pfzsset == null ? null : pfzsset.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.sjtxzs
     *
     * @return the value of t_dzz_hjxx_dzz.sjtxzs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getSjtxzs() {
        return sjtxzs;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.sjtxzs
     *
     * @param sjtxzs the value for t_dzz_hjxx_dzz.sjtxzs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setSjtxzs(String sjtxzs) {
        this.sjtxzs = sjtxzs == null ? null : sjtxzs.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.tsexplain
     *
     * @return the value of t_dzz_hjxx_dzz.tsexplain
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getTsexplain() {
        return tsexplain;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.tsexplain
     *
     * @param tsexplain the value for t_dzz_hjxx_dzz.tsexplain
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setTsexplain(String tsexplain) {
        this.tsexplain = tsexplain == null ? null : tsexplain.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.hjxhcl
     *
     * @return the value of t_dzz_hjxx_dzz.hjxhcl
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getHjxhcl() {
        return hjxhcl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.hjxhcl
     *
     * @param hjxhcl the value for t_dzz_hjxx_dzz.hjxhcl
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setHjxhcl(String hjxhcl) {
        this.hjxhcl = hjxhcl == null ? null : hjxhcl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.datasources
     *
     * @return the value of t_dzz_hjxx_dzz.datasources
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getDatasources() {
        return datasources;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.datasources
     *
     * @param datasources the value for t_dzz_hjxx_dzz.datasources
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setDatasources(String datasources) {
        this.datasources = datasources == null ? null : datasources.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.operator
     *
     * @return the value of t_dzz_hjxx_dzz.operator
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getOperator() {
        return operator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.operator
     *
     * @param operator the value for t_dzz_hjxx_dzz.operator
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.operatetime
     *
     * @return the value of t_dzz_hjxx_dzz.operatetime
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getOperatetime() {
        return operatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.operatetime
     *
     * @param operatetime the value for t_dzz_hjxx_dzz.operatetime
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setOperatetime(String operatetime) {
        this.operatetime = operatetime == null ? null : operatetime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.bjbzcsfs
     *
     * @return the value of t_dzz_hjxx_dzz.bjbzcsfs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getBjbzcsfs() {
        return bjbzcsfs;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.bjbzcsfs
     *
     * @param bjbzcsfs the value for t_dzz_hjxx_dzz.bjbzcsfs
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setBjbzcsfs(String bjbzcsfs) {
        this.bjbzcsfs = bjbzcsfs == null ? null : bjbzcsfs.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.sfdqjc
     *
     * @return the value of t_dzz_hjxx_dzz.sfdqjc
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getSfdqjc() {
        return sfdqjc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.sfdqjc
     *
     * @param sfdqjc the value for t_dzz_hjxx_dzz.sfdqjc
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setSfdqjc(String sfdqjc) {
        this.sfdqjc = sfdqjc == null ? null : sfdqjc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.sfxxwz
     *
     * @return the value of t_dzz_hjxx_dzz.sfxxwz
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public Integer getSfxxwz() {
        return sfxxwz;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.sfxxwz
     *
     * @param sfxxwz the value for t_dzz_hjxx_dzz.sfxxwz
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setSfxxwz(Integer sfxxwz) {
        this.sfxxwz = sfxxwz;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.syncstatus
     *
     * @return the value of t_dzz_hjxx_dzz.syncstatus
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getSyncstatus() {
        return syncstatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.syncstatus
     *
     * @param syncstatus the value for t_dzz_hjxx_dzz.syncstatus
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setSyncstatus(String syncstatus) {
        this.syncstatus = syncstatus == null ? null : syncstatus.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_dzz_hjxx_dzz.synctime
     *
     * @return the value of t_dzz_hjxx_dzz.synctime
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public String getSynctime() {
        return synctime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_dzz_hjxx_dzz.synctime
     *
     * @param synctime the value for t_dzz_hjxx_dzz.synctime
     *
     * @mbg.generated Mon Jun 17 10:10:12 CST 2019
     */
    public void setSynctime(String synctime) {
        this.synctime = synctime == null ? null : synctime.trim();
    }
}