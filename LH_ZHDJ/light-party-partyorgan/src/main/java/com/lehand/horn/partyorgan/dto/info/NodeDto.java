package com.lehand.horn.partyorgan.dto.info;


import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

@ApiModel
public class NodeDto implements Serializable {

    private final static long serialVersionUID = 1L;


    private int flag;//0:用户节点,1:组织节点
    private Long id;
    private String account;//账户
    private String name;//节点名称
    private Long seqno;//节点顺序号
    private int remarks1 = 0;//1表示可点击
    private String dzzqc; //党组织全称
    private String zzlb; //党组织类别
    private Set<NodeDto> childs = new TreeSet<NodeDto>();

    //专门为前端添加的字段
    private boolean isshow;//右侧操作
    private boolean isopen = true;//左侧小箭头
    private boolean ischeck = false;//多选
    private Long pid;
    private Long icon;
    private Integer ratio;//党费上缴比例

    private String orgcode;


    private String onelevel;//0  搜索出来无法点击展开 0
    public String getOnelevel() {
        return onelevel;
    }

    public void setOnelevel(String onelevel) {
        this.onelevel = onelevel;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public boolean isIscheck() {
        return ischeck;
    }

    public void setIscheck(boolean ischeck) {
        this.ischeck = ischeck;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
        if (this.flag==0) {
            this.isopen = false;
        }
    }

    public String getAccount() {
        return account;
    }

    public int getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(int remarks1) {
        this.remarks1 = remarks1;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    public Set<NodeDto> getChilds() {
        return childs;
    }

    public boolean isIsopen() {
        return isopen;
    }

    public void setIsopen(boolean isopen) {
        this.isopen = isopen;
    }

    public void setChilds(Set<NodeDto> childs) {
        this.childs = childs;
    }

    public boolean isIsshow() {
        return isshow;
    }

    public void setIsshow(boolean isshow) {
        this.isshow = isshow;
    }

    public int compare(NodeDto o1, NodeDto o2) {
        return o1.getSeqno().compareTo(o2.getSeqno());
    }

    public int compareTo(NodeDto o) {
        return this.getSeqno().compareTo(o.getSeqno());
    }

    public Long getIcon() {
        return icon;
    }

    public void setIcon(Long icon) {
        this.icon = icon;
    }

    public Integer getRatio() {
        return ratio;
    }

    public void setRatio(Integer ratio) {
        this.ratio = ratio;
    }

    public String getZzlb() {
        return zzlb;
    }

    public void setZzlb(String zzlb) {
        this.zzlb = zzlb;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + flag;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NodeDto other = (NodeDto) obj;
        if (flag != other.flag)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public void addChild(NodeDto node) {
        this.childs.add(node);
    }

    public String getDzzqc() {
        return dzzqc;
    }

    public void setDzzqc(String dzzqc) {
        this.dzzqc = dzzqc;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }
}
