//package com.lehand.horn.partyorgan.dao;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.stereotype.Repository;
//
//import com.lehand.base.common.Pager;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
//
///**
// * 党组织机构DAO层
// * @author taogang
// * @version 1.0
// * @date 2019年1月21日, 下午1:56:04
// */
//@Repository
//public class TdzzInfoSimpleDao extends ComBaseDao<TDzzInfoSimple>{
//
//	/** 根据组织机构id 获取信息 树 .*/
//	private static final String QUERY = "select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc";
//	private static final String QUERY_BY_ID = "select * from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc";
//	private static final String QUERY_LIKESTR = "select * from t_dzz_info_simple where operatetype !=3 and dzzmc like ? and id_path like ?  and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc";
//
//	/** 根据组织结构获取总条数 .*/
//	private static final String COUNT_BY_ID = "select count(1) from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'";
//
//	/** 基础信息查询 .*/
//	private static final String DZZ_BASE = "select distinct a.organid,a.organcode,a.dzzmc,a.zzlb,a.dzzlsgx,"
////			+ "c.dzzsj,c.lxrname,c.lxrphone,c.dzzjlrq,c.address,c.fax,"
//			+ "d.dzzmc as pdzzmc,b.dzzszdwqk,b.xzqh," +
//			"	f.dwname,f.dwxzlb from t_dzz_info_simple a " +
//			"	inner join t_dzz_info b on a.organid = b.organid " +
////			 "	inner join t_dzz_info_bc c on a.organid = c.organid " +
//			"	left join t_dzz_info_simple d on d.organid = a.parentid " +
//			"	left join t_dw_info f on b.szdwid = f.uuid " +
//			"	where a.operatetype != 3 and a.organid = ?";
//
//	/** 单位信息 .*/
//	private static final String DWINFO_BY_ID = "	select distinct c.uuid,a.organid,c.dwname,c.dwxzlb,d.dwlsgx,c.dwjldjczzqk,d.isfrdw,d.dwfzr," +
//			"	d.fzrsfsdy,c.dwcode,d.jjlx,d.qygm,d.sfmykjqy,d.zgzgs,d.zgzgzgrs,d.zgzyjsrys,d.sfjygh, " +
//			"	d.sfzjzz from t_dzz_info_simple a " +
//			"	left join t_dw_info c on a.szdwid = c.uuid " +
//			"	left join t_dw_info_bc d on d.uuid = c.uuid " +
//			"	where a.operatetype != 3 and a.organid = ?";
//
//	private static final String QUERY_BY_CODE = "select * from t_dzz_info_simple where organcode = ?";
//
//	/** 通过编码获取组织机构简称 .*/
//	private static final String FINDJC_BY_CODE = "select remarks6,orgname from pw_organ where compid = ? and orgcode = ?";
//
//
//	/**
//	 * @Title：通过编码获取组织机构简称
//	 * @Description：TODO
//	 * @param ：@param orgCode
//	 * @param ：@param compid
//	 * @param ：@return
//	 * @return ：Map<String,Object>
//	 * @throws
//	 */
//	public Map<String, Object> findjcByCode(Long compid , String orgcode ){
//		return jdbc.get2Map(FINDJC_BY_CODE, compid,orgcode);
//	}
//
//
//	/**
//	 * 根据code取组织机构信息
//	 * @param code
//	 * @return
//	 */
//	public TDzzInfoSimple queryByCode(String code) {
//		return super.get(QUERY_BY_CODE, TDzzInfoSimple.class, code);
//	}
//
//	/**
//	 * 查询自身组织机构
//	 * @param orgId
//	 * @return
//	 * @author taogang
//	 */
//	public List<TDzzInfoSimple> query(String organid){
//		return super.list2bean(QUERY, TDzzInfoSimple.class, organid);
//	}
//
//	/**
//	 * 查询组织机构返回T
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public TDzzInfoSimple queryOrgandId(String organid) {
//		return super.get(QUERY, TDzzInfoSimple.class, organid);
//	}
//
//	/**
//	 * 查询下一级组织机构信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public List<TDzzInfoSimple> queryById(String organid){
//		return super.list2bean(QUERY_BY_ID, TDzzInfoSimple.class, organid);
//	}
//
//	/**
//	 * 模糊查询-组织机构树
//	 * @param likeStr
//	 * @param idPath
//	 * @return
//	 * @author taogang
//	 */
//	public List<TDzzInfoSimple> queryByLikeStr(String likeStr,String idPath){
//		return super.list2bean(QUERY_LIKESTR, TDzzInfoSimple.class, likeStr,idPath);
//	}
//
//	/**
//	 * 分页查询
//	 * @param organid
//	 * @param pager
//	 * @return
//	 * @author taogang
//	 */
//	public List<TDzzInfoSimple> list(String sql,Pager pager,Object...args){
//		return super.pageBeanNotCount(sql, pager, TDzzInfoSimple.class,args);
//	}
//
//	/**
//	 * 分页查询总条数
//	 * @param sql
//	 * @param args
//	 * @return
//	 * @author taogang
//	 */
//	public int count_list(String sql,Object...args) {
//		return super.getInt(sql, args);
//	}
//
//
//	/**
//	 * 根据机构id 获取总条数
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public int countDzz(String organid) {
//		return super.getInt(COUNT_BY_ID, organid);
//	}
//
//	/**
//	 * 查询组织机构基本信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public Map<String, Object> dzz_base(String organid){
//		return jdbc.get2Map(DZZ_BASE, organid);
//	}
//
//	/**
//	 * 所在单位信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public Map<String, Object> dw_info(String organid){
//		return jdbc.get2Map(DWINFO_BY_ID, organid);
//	}
//}
