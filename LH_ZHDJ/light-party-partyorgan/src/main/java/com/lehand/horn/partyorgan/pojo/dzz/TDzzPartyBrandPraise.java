package com.lehand.horn.partyorgan.pojo.dzz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 领航计划(品牌点赞)表
 * @author lx
 */
@ApiModel(value="领航计划(品牌点赞)表",description="领航计划(品牌点赞)表")
public class TDzzPartyBrandPraise implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value="品牌党组织编码",name="orgcode")
  private String orgcode;
  @ApiModelProperty(value="点赞的党组织编码",name="praisecode")
  private String praisecode;
  @ApiModelProperty(value="是否点赞状态（0：点赞，1：未点赞）",name="status")
  private String status;
  @ApiModelProperty(value="品牌logo图片",name="fileid")
  private String fileid;

  @ApiModelProperty(value="领航计划：品牌id",name="brandid")
  private Long brandid;

  public Long getBrandid() {
    return brandid;
  }

  public void setBrandid(Long brandid) {
    this.brandid = brandid;
  }

  public String getFileid() {
    return fileid;
  }

  public void setFileid(String fileid) {
    this.fileid = fileid;
  }

  public String getOrgcode() {
    return orgcode;
  }

  public void setOrgcode(String orgcode) {
    this.orgcode = orgcode;
  }


  public String getPraisecode() {
    return praisecode;
  }

  public void setPraisecode(String praisecode) {
    this.praisecode = praisecode;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}
