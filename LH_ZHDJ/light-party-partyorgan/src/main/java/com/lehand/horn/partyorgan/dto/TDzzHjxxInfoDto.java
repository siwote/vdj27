package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.hjxx.TDzzHjxxInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: TDzzHjxxInfoDto
 * @Description: 党组织换届信息扩展类
 * @Author lx
 * @DateTime 2021-05-10 14:18
 */
@ApiModel(value = "党组织换届信息扩展类",description = "党组织换届信息扩展类")
public class TDzzHjxxInfoDto extends TDzzHjxxInfo {

    private final static long serialVersionUID = 1L;

    /**
     * 延迟原因
     */
    @ApiModelProperty(value = "延迟原因", name = "content")
    private String content;

    /**
     * 组织名称
     */
    @ApiModelProperty(value = "组织名称", name = "orgname")
    private String orgname;
    /**
     * 延迟历史信息
     */
    @ApiModelProperty(value = "延迟信息", name = "delayList")
    List<Map<String,Object>> delayList = new ArrayList<>();

    /**
     * 被提醒人JSON
     */
    @ApiModelProperty(value = "被提醒人JSON(示例：[{\"userid\":\"6855083a17ff418195cad114fc6977dc\"},{\"userid\":\"6855083a17ff418195cad114fc6977dc\"}])", name = "remindpeople")
    private String remindpeople;
    /**
     *提醒周期JSON
     */
    @ApiModelProperty(value = "提醒周期JSON(1：一月份。3：三月份。6：六月份)多选以逗号拼接", name = "reminddate")
    private String reminddate;
    /**
     *提醒时间点
     */
    @ApiModelProperty(value = "提醒时间点", name = "remindtime")
    private String remindtime;
    /**
     *实际换届时间
     */
    @ApiModelProperty(value = "实际换届时间", name = "actualTime")
    private String actualTime;

    @ApiModelProperty(value = "距离届满还有几月", name = "distance")
    private int distance;
    /**
     * 换届周期配置日志
     */
    @ApiModelProperty(value = "换届周期配置日志", name = "remindConfigLog")
    private Map<String,Object> remindConfigLog;

    public Map<String, Object> getRemindConfigLog() {
        return remindConfigLog;
    }

    public void setRemindConfigLog(Map<String, Object> remindConfigLog) {
        this.remindConfigLog = remindConfigLog;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getRemindtime() {
        return remindtime;
    }

    public void setRemindtime(String remindtime) {
        this.remindtime = remindtime;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getRemindpeople() {
        return remindpeople;
    }

    public void setRemindpeople(String remindpeople) {
        this.remindpeople = remindpeople;
    }

    public String getReminddate() {
        return reminddate;
    }

    public void setReminddate(String reminddate) {
        this.reminddate = reminddate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Map<String, Object>> getDelayList() {
        return delayList;
    }

    public void setDelayList(List<Map<String, Object>> delayList) {
        this.delayList = delayList;
    }
}
