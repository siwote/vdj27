package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.PartyTransfer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author code maker
 */
@ApiModel(value="",description="")
public class PartyTransferDto2 extends PartyTransfer {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 附件
	 */
	@ApiModelProperty(value="附件",name="files")
	private List<Map<String,Object>> files;

	@ApiModelProperty(value="组织id",name="orgid")
	private String orgid;

	public List<Map<String, Object>> getFiles() {
		return files;
	}

	public void setFiles(List<Map<String, Object>> files) {
		this.files = files;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}
}
