package com.lehand.horn.partyorgan.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

import com.lehand.horn.partyorgan.constant.MagicConstant;

import java.io.Serializable;

@ApiModel(value = "导出勾选项",description = "导出勾选项")
public class ExportColumnReq implements Serializable   {

    private  static final long serialVersionUID = 1L;

    @ApiModelProperty(value="导出列名",name="title")
    private String title ;

    @ApiModelProperty(value="导出列编码",name="code")
    private String code;

    public ExportColumnReq() {
    }

    public ExportColumnReq(String title, String code) {
        this.title = title;
        this.code = code;
    }

    public String getTitle() {
        return StringUtils.isEmpty(title)? MagicConstant.STR.EMPTY_STR:title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return StringUtils.isEmpty(code)? MagicConstant.STR.EMPTY_STR:code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
