package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.dao.TSecretaryInfoDao;
import com.lehand.horn.partyorgan.dto.DZZExportReq;
import com.lehand.horn.partyorgan.dto.Node;
import com.lehand.horn.partyorgan.dto.SessionDto;
import com.lehand.horn.partyorgan.dto.TDzzAdvancedReq;
import com.lehand.horn.partyorgan.dto.info.ElectionExportReq;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzAdvanced;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzFunding;
import com.lehand.horn.partyorgan.service.GdictItemService;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.horn.partyorgan.service.info.HornOrganizationService;

import com.lehand.horn.partyorgan.service.info.HornOrganizationService2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 党组织控制层
 * @author admin
 *
 */
@Api(value = "党组织信息管理", tags = { "党组织信息管理" })
@RestController
@RequestMapping("/lhdj/dzzorg")
public class HornOrganizationController extends BaseController {
    private static final Logger logger = LogManager.getLogger(HornOrganizationController.class);

    @Resource
    private HornOrganizationService hornOrganizationService;

    @Resource
    private HornOrganizationService2 hornOrganizationService2;

    @Resource
    private BaseDzzTotalService baseDzzTotalService;
    @Resource
    private GdictItemService gdictItemService;

    /**
     * 党组织基本信息
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党组织基本信息(统计)", notes = "党组织基本信息(统计)", httpMethod = "POST")
    @RequestMapping("/dzzInfo")
    public Message dzzInfo(String orgId) {
        Message message = new Message();
        try {
            if(StringUtils.isEmpty(orgId)) {
                message.setMessage("参数错误，组织机构id不能为空");
                return message;
            }
            List<Map<String,Object>> list = baseDzzTotalService.dzzCount(orgId);
            message.setData(list);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党组织基本信息查询出错");
        }
        return message;
    }

    /**
     * 机构树
     * @param organid
     * @param likeStr
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "机构树", notes = "机构树", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "likeStr", value = "查询附加条件", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/tree")
    public Message query(String organid,String likeStr){
        Message message = new Message();
        try {
            Object list = hornOrganizationService.getCurrAndSubListOrg2(getSession(),organid,likeStr, "");
            message.success();
            message.setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("组织机构树查询出错");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "机构树（可选组织类别）", notes = "机构树（可选组织类别）", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "likeStr", value = "查询附加条件", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "zzlbs", value = "需要的组织类别(多个使用英文的逗号隔开)", dataType = "String", paramType = "query", example = "")
    })
    @RequestMapping("/tree2")
    public Message query(String organid,String likeStr, String zzlbs){
        Message message = new Message();
        try {
            Object list = hornOrganizationService.getCurrAndSubListOrg(getSession(),organid,likeStr, zzlbs);
            message.success();
            message.setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("组织机构树查询出错");
        }
        return message;
    }

    /**
     * 列表
     * @param organid
     * @param dwlb
     * @param likeStr
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "dwlb", value = "是否包含下级 （0 是 1 否）", dataType = "Integer", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "likeStr", value = "模糊查询关键字", dataType = "int", paramType = "query", example = "0")
    })
    @RequestMapping("/list")
    public Message list(String organid, Integer dwlb, String likeStr, Pager pager) {
        Message message = new Message();
        try {
            if(StringUtils.isEmpty(organid)) {
                message.setMessage("参数错误，机构id不能为空");
                return message;
            }
            message.setData(new PagerUtils<Object>(hornOrganizationService.getList(pager,organid, dwlb, likeStr)));
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("组织机构分页列表查询出错");
        }
        return message;
    }

    /**
     * 党组织基本信息
     * @param organid
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党组织基本信息", notes = "党组织基本信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/base")
    public Message base(String organid) {
        Message message = new Message();
        try {
            Map<String, Object> map = hornOrganizationService.getBase(organid);
            message.success().setData(map);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("基本信息查询出错");
        }
        return message;
    }

    /**
     * 单位信息
     * @param organid
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "单位信息", notes = "单位信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/dwInfo")
    public Message dwInfo(String organid) {
        Message message = new Message();
        try {
            Map<String, Object> map = hornOrganizationService.getDwInfo(organid);
            message.success().setData(map);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("单位信息查询出错");
        }
        return message;
    }

    /**
     * 换届信息
     * @param organid
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "换届信息", notes = "换届信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/hjxx")
    public Message hjxx(String organid) {
        Message message = new Message();
        try {
            List<Map<String, Object>> list = hornOrganizationService.getHjxxList(organid);
            message.success().setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("换届信息查询出错");
        }
        return message;
    }

    /**
     * 班子成员信息
     * @param organid
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "班子成员信息", notes = "班子成员信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "xm", value = "姓名", dataType = "String", example = "")
    })
    @RequestMapping("/bzcy")
    public Message bzcy(String organid, String xm, String type) {
        Message message = new Message();
        try {
            List<Map<String, Object>> list = hornOrganizationService.getBzcyxxList(organid, xm,type);
            message.success().setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("班子成员查询出错");
        }
        return message;
    }

    /**
     * 奖惩信息
     * @param organid
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "奖惩信息", notes = "奖惩信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/jcxx")
    public Message jcxx(String organid) {
        Message message = new Message();
        try {
            List<Map<String, Object>> list = hornOrganizationService.getJcxx(organid);
            message.success().setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("奖惩信息查询出错");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "导出党组织信息", notes = "导出党组织信息", httpMethod = "POST")
    @RequestMapping("/exportDZZInfo")
    public String dzzInfoExport(HttpServletResponse response, @RequestBody DZZExportReq req ){
        return hornOrganizationService.dzzInfoExport(response, req, getSession());
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "导入党组织信息", notes = "导入党组织信息", httpMethod = "POST")
    @RequestMapping("/importDZZInfo")
    public Message dzzInfoImport(MultipartFile file){
        Message msg = new Message();
        msg.success();
        msg.setData(hornOrganizationService.dzzInfoImport(file, getSession()));
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "导入党组织信息", notes = "导入党组织信息", httpMethod = "POST")
    @RequestMapping("/importDZZInfo2")
    public Message dzzInfoImport(){
        Message msg = new Message();
        msg.success();
        msg.setData(hornOrganizationService2.dzzInfoImport2(null));
        return msg;
    }


    /**
     * 组织关系转接
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "组织关系转接(统计)", notes = "组织关系转接(统计)", httpMethod = "POST")
    @RequestMapping("/zzgxInfo")
    public Message zzgxInfo(String orgId) {
        Message message = new Message();
        try {
            if(com.alibaba.druid.util.StringUtils.isEmpty(orgId)) {
                message.setMessage("参数错误，组织机构id不能为空");
                return message;
            }
            Map<String,Object> map = baseDzzTotalService.getZzgxCount(orgId);
            message.setData(map);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("组织关系转接信息查询出错");
        }
        return message;
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "查询sessiondto", notes = "查询sessiondto", httpMethod = "POST")
    @RequestMapping("/makeSessionDto")
    public Message  makeSessionDto(){
        Message msg  = new Message();
        SessionDto sessionDto = baseDzzTotalService.makeSessionDto(getSession());
        try {
            msg.setData(sessionDto);
            msg.success();
        } catch (Exception e) {
            logger.error(e);
            msg.setMessage("查询sessiondto出错");
        }
        return msg;
    }

    /**
     * 获取指定节点下的所有子节点(含用户、组织)
     * @param organid
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分类授权接口-菜单树", notes = "分类授权接口-菜单树", httpMethod = "POST")
    @RequestMapping("/listAll")
    public Message listAll(Long organid,String username) {
        Message msg = new Message();
        try {
            List<Node> nodes = hornOrganizationService.listAll(getSession(),organid,username, 0);
            msg.success().setData(nodes);
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
        }
        return msg;
    }

    /**
     * 获取指定节点下的所有子节点(含用户、组织)
     * @param organid
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分类授权接口-菜单树", notes = "分类授权接口-菜单树", httpMethod = "POST")
    @RequestMapping("/listAllUnder")
    public Message listAllUnder(Long organid,String username) {
        Message msg = new Message();
        try {
            List<Node> nodes = hornOrganizationService.listAll(getSession(),organid,username, 1);
            msg.success().setData(nodes);
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
        }
        return msg;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "数据授权保存", notes = "数据授权保存", httpMethod = "POST")
    @RequestMapping("/saveAuthClass")
    public Message saveAuthClass(Integer subjecttp,Long subjectid,String classids) {
        Message msg = new Message();
        try {
            if(StringUtils.isEmpty(subjectid)) {
                LehandException.throwException("授权ID不能为空");
            }
            hornOrganizationService.saveAuthClass(subjecttp, subjectid, classids, getSession());
            msg.success();
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
            logger.error("数据授权失败",e);
        }
        return msg;
    }

    /**
     * 数据授权id查询
     * @param subjecttp
     * @param subjectid
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "数据授权id查询", notes = "数据授权id查询", httpMethod = "POST")
    @RequestMapping("/listIds")
    public Message listIds(Integer subjecttp,Long subjectid) {
        Message msg = new Message();
        try {
            List<Map<String,Object>> list = hornOrganizationService.listIds(getSession().getCompid(),subjecttp, subjectid);
            msg.success().setData(list);
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
            logger.error("数据授权查询失败",e);
        }
        return msg;
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "工作台组织概况", notes = "工作台组织概况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "organid", value = "组织机构id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getOrgGeneral")
    public Message getOrgGeneral(String organid){

        Message msg = new Message();
        msg.setData(hornOrganizationService.getOrgGeneral(organid,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "工作台党支部概况", notes = "工作台党支部概况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "organid", value = "组织机构id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getBranchGeneral")
    public Message getBranchGeneral(String organid){
        Message msg = new Message();
        msg.setData(hornOrganizationService.getBranchGeneral(organid,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "工作台活跃度统计", notes = "工作台活跃度统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "统计方式:0按天,1按月", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    @RequestMapping("/getActiveSituation")
    public Message getActiveSituation(int type){
        Message msg = new Message();
        msg.setData(hornOrganizationService.getActiveSituation(type,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "班子成员删除", notes = "班子成员删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "班子成员ID", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delBzcyInfo")
    public Message delBzcyInfo(String userid,String organid) {
        Message msg = new Message();
        try {
            hornOrganizationService.delBzcyInfo(userid,organid, getSession());
            msg.success();
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
            logger.error("数据授权失败",e);
        }
        return msg;
    }


    @OpenApi(optflag=1)
    @ApiOperation(value = "班子成员新增", notes = "班子成员新增", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "班子成员ID", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "username", value = "班子成员名称", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "dnzw", value = "党内职务", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "zwsm", value = "职务说明", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "rzrq", value = "任职日期", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "lzrq", value = "离职日期", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "csrq", value = "出生日期", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "lxdh", value = "联系电话", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "xl", value = "学历", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "idno", value = "身份证号", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "xb", value = "性别", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "organid", value = "登录人机构ID", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/modiBzcyInfo")
    public Message modiBzcyInfo(String userid, String username, String dnzw, String zwsm, String rzrq,
                                String lzrq, String csrq, String lxdh, String xl, String idno, String xb,String organid) {
        Message msg = new Message();
        try {
            hornOrganizationService.modiBzcyInfo(userid,username,dnzw,zwsm,rzrq,lzrq,csrq,lxdh,xl,idno,xb,organid,getSession());
            msg.success();
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
            logger.error("编辑失败",e);
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "班子成员职务", notes = "班子成员职务", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "班子成员职务传 d_dy_dnzw", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getBzcyzw")
    public Message getBzcyzw(String code,String zzlb){
        Message msg = new Message();
        msg.setData(gdictItemService.listItemNew(code,zzlb,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询下级支部", notes = "查询下级支部", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "str", value = "模糊查询", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getUnderBranchList")
    public Message getUnderBranchList(String likeStr){
        Message msg = new Message();
        msg.setData(hornOrganizationService.getUnderBranchList(likeStr,getSession()));
        msg.success();
        return msg;
    }



    @OpenApi(optflag=0)
    @ApiOperation(value = "根据职务查询人名", notes = "根据职务查询人名", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "32位组织机构id", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMemberListByPost")
    public Message getMemberListByPost(String organid){
        Message msg = new Message();
        msg.setData(hornOrganizationService.getMemberListByPost(organid,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "换届统计头部", notes = "换届统计头部", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "32位组织机构id", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getElecInfoByOrg")
    public Message getElecInfoByOrg(String organid){
        Message msg = new Message();
        msg.setData(hornOrganizationService.getElecInfoByOrg(organid,getSession()));
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "换届分页", notes = "换届分页", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "32位组织机构id", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "0:逾期,1正常", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/pageElection")
    public Message pageElection(Pager pager,String organid,String status){
        Message msg = new Message();
        msg.setData(new PagerUtils<Object>(hornOrganizationService.pageElection(pager,organid,status,getSession())));
        msg.success();
        return msg;
    }


    @OpenApi(optflag=0)
    @ApiOperation(value = "换届信息导出", notes = "换届信息导出", httpMethod = "POST")
    @RequestMapping("/exportElectionExcel")
    public String exportElectionExcel(HttpServletResponse response, @RequestBody ElectionExportReq req){
        return hornOrganizationService.exportElectionExcel(response,req,getSession());
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记花名册新增/编辑", notes = "书记花名册新增/编辑", httpMethod = "POST")
    @RequestMapping("/addTSecretaryInfo")
    public Message addTSecretaryInfo(@RequestBody TSecretaryInfoDao tsid ){
        Message msg = new Message();
        hornOrganizationService.addTSecretaryInfo(tsid, getSession());
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记花名册维护分页查询", notes = "书记花名册维护分页查询", httpMethod = "POST")
    @RequestMapping("/pageTSecretaryInfo")
    public Message pageTSecretaryInfo(Pager pager,Integer status ){
        Message msg = new Message();
        hornOrganizationService.pageTSecretaryInfo(pager,status, getSession());
        msg.success().setData(pager);
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记花名册详情", notes = "书记花名册详情", httpMethod = "POST")
    @RequestMapping("/getTSecretaryInfo")
    public Message getTSecretaryInfo(Long id,String userid){
        Message msg = new Message();
        msg.success().setData(hornOrganizationService.getTSecretaryInfo(id, userid, getSession()));
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记花名册删除", notes = "书记花名册删除", httpMethod = "POST")
    @RequestMapping("/delTSecretaryInfo")
    public Message delTSecretaryInfo(Long id){
        Message msg = new Message();
        hornOrganizationService.delTSecretaryInfo(id, getSession());
        msg.success();
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记花名册巡查分页查询", notes = "书记花名册巡查分页查询", httpMethod = "POST")
    @RequestMapping("/pageQueryTSecretaryInfo")
    public Message pageQueryTSecretaryInfo(Pager pager,String organid,String type,String name){
        Message msg = new Message();
        hornOrganizationService.pageQueryTSecretaryInfo(pager,organid,type,name,getSession());
        msg.success().setData(pager);
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党组织机构树", notes = "党组织机构树", httpMethod = "POST")
    @RequestMapping("/orgTrees")
    public Message orgTrees(String organcode){
        Message msg = new Message();
        msg.success().setData(hornOrganizationService.orgTrees(organcode, getSession()));
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "书记花名册导出", notes = "书记花名册导出", httpMethod = "POST")
    @RequestMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response,String organid,String type,String name) {

        /** 第一步，创建一个Workbook，对应一个Excel文件  */
        XSSFWorkbook wb = new XSSFWorkbook();

        /** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
        XSSFSheet sheet = wb.createSheet("书记花名册");

        /** 第三步，设置样式以及字体样式*/
        XSSFCellStyle titleStyle = createTitleCellStyle(wb);
        XSSFCellStyle headerStyle2 = createHeadCellStyle2(wb);
        XSSFCellStyle contentStyle = createContentCellStyle(wb);

        /** 第四步，创建标题 ,合并标题单元格 */
        // 行号
        int rowNum = 0;
        // 创建第一页的第一行，索引从0开始
        XSSFRow row0 = sheet.createRow(rowNum++);
        // 设置行高
        row0.setHeight((short) 800);

        String title = "书记花名册";
        XSSFCell c00 = row0.createCell(0);
        c00.setCellValue(title);
        c00.setCellStyle(titleStyle);
        // 合并单元格，参数依次为起始行，结束行，起始列，结束列 （索引0开始）
        //标题合并单元格操作，9为总列数
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));

        //第四行
        XSSFRow row3 = sheet.createRow(rowNum++);
        row3.setHeight((short) 700);
        String[] row_third = {"姓名", "职务", "党组织名称", "参加书记培训班情况", "", "奖惩情况", "履职考核情况","述职评议情况","工作简历"};
        for (int i = 0; i < row_third.length; i++) {
            XSSFCell tempCell = row3.createCell(i);
            tempCell.setCellValue(row_third[i]);
            tempCell.setCellStyle(headerStyle2);
        }

        XSSFRow row4 = sheet.createRow(rowNum++);
        row3.setHeight((short) 700);
        String[] row_four = {"", "", "", "省国资委以上", "集团公司级", "", "","",""};
        for (int i = 0; i < row_four.length; i++) {
            XSSFCell tempCell = row4.createCell(i);
            tempCell.setCellValue(row_four[i]);
            tempCell.setCellStyle(headerStyle2);
        }

        //合并
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 0, 0));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 1));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 2, 2));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 4));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 5, 5));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 6, 6));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 7, 7));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 8, 8));

        //第五行开始往后
        Pager pager = new Pager();
        pager.setPageSize(10000);
        hornOrganizationService.pageQueryTSecretaryInfo(pager,organid,type,name,getSession());
        List<Map<String, Object>> dataList = (List<Map<String, Object>>) pager.getRows(); //查询出来的数据

        //循环每一行数据
        for (Map<String, Object> excelData : dataList) {
            XSSFRow tempRow = sheet.createRow(rowNum++);
            tempRow.setHeight((short) 700);
            // 循环单元格填入数据
            for (int j = 0; j < 9; j++) {
                XSSFCell tempCell = tempRow.createCell(j);
                tempCell.setCellStyle(contentStyle);
                String tempValue;
                if (j == 0) {
                    tempValue = excelData.get("xm").toString();
                } else if (j == 1) {
                    tempValue = excelData.get("postname").toString();
                } else if (j == 2) {
                    tempValue = excelData.get("dzzmc").toString();
                } else if (j == 3) {
                    tempValue = excelData.get("nationaltrain")==null ? "" : excelData.get("nationaltrain").toString();
                } else if (j == 4) {
                    tempValue = excelData.get("grouptrain")==null ? "" : excelData.get("grouptrain").toString();
                } else if (j == 5) {
                    tempValue = excelData.get("rewards")==null ? "" : excelData.get("rewards").toString();
                } else if(j == 6){
                    tempValue = excelData.get("performduties")==null ? "" : excelData.get("performduties").toString();
                }else if(j == 7){
                    tempValue = excelData.get("report")==null ? "" : excelData.get("report").toString();
                }else if(j == 8){
                    tempValue = excelData.get("resume")==null ? "" : excelData.get("resume").toString();
                }else{
                    tempValue = "";
                }
                tempCell.setCellValue(tempValue);
            }
        }
        sheet.autoSizeColumn((short)1);
        sheet.autoSizeColumn((short)2);
        sheet.setColumnWidth(3,8000);
        sheet.setColumnWidth(4,8000);
        sheet.setColumnWidth(5,8000);
        sheet.setColumnWidth(6,8000);
        sheet.setColumnWidth(7,8000);
        sheet.setColumnWidth(8,8000);

        String fileName = "书记花名册.xlsx";
        OutputStream stream = null;
        try {
            response.setContentType("multipart/form-data");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename="+"书记花名册.xlsx");
            response.setHeader("FileName", "书记花名册.xlsx");
            stream = response.getOutputStream();
            wb.write(stream);// 将数据写出去
        }catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                if(stream != null){
                    stream.flush();
                    stream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @ 创建标题样式
     * @param wb
     * @return
     */
    private XSSFCellStyle createTitleCellStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex()); //背景颜色
        cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        XSSFFont headerFont = (XSSFFont) wb.createFont(); // 创建字体样式
        headerFont.setBold(true); //字体加粗
        headerFont.setFontName("黑体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 14); // 设置字体大小
        cellStyle.setFont(headerFont); // 为标题样式设置字体样式

        return cellStyle;
    }

    /**
     * @ 创建表头样式
     * @param wb
     * @return
     */
    private XSSFCellStyle createHeadCellStyle2(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex()); //背景颜色
        cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        XSSFFont headerFont = (XSSFFont) wb.createFont(); // 创建字体样式
        headerFont.setBold(true); //字体加粗
        headerFont.setFontName("黑体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 12); // 设置字体大小
        cellStyle.setFont(headerFont); // 为标题样式设置字体样式

        return cellStyle;
    }


    /**
     * @ 创建内容样式
     * @param wb
     * @return
     */
    private static XSSFCellStyle createContentCellStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);// 水平居中
        cellStyle.setWrapText(true);// 设置自动换行
        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
        cellStyle.setBorderRight(BorderStyle.THIN); //右边框
        cellStyle.setBorderTop(BorderStyle.THIN); //上边框

        // 生成12号字体
        XSSFFont font = wb.createFont();
        font.setColor((short)8);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setFont(font);

        return cellStyle;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "登录日志统计", notes = "登录日志统计", httpMethod = "POST")
    @RequestMapping("/loginLog")
    public Message loginLog(Pager pager,String startime,String endtime,String organid){
        Message msg = new Message();
        hornOrganizationService.loginLog(pager,startime,endtime,organid,getSession());
        msg.success().setData(pager);
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "登录日志导出", notes = "登录日志导出", httpMethod = "POST")
    @RequestMapping("/exportLoginLog")
    public void exportLoginLog(HttpServletResponse response,String startime,String endtime,String organid) {

        /** 第一步，创建一个Workbook，对应一个Excel文件  */
        XSSFWorkbook wb = new XSSFWorkbook();

        /** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
        XSSFSheet sheet = wb.createSheet("党组织登录日志统计");

        /** 第三步，设置样式以及字体样式*/
        XSSFCellStyle titleStyle = createTitleCellStyle(wb);
        XSSFCellStyle headerStyle2 = createHeadCellStyle2(wb);
        XSSFCellStyle contentStyle = createContentCellStyle(wb);

        /** 第四步，创建标题 ,合并标题单元格 */
        // 行号
        int rowNum = 0;
        // 创建第一页的第一行，索引从0开始
        XSSFRow row0 = sheet.createRow(rowNum++);
        // 设置行高
        row0.setHeight((short) 800);

        String title = "党组织登录日志统计";
        XSSFCell c00 = row0.createCell(0);
        c00.setCellValue(title);
        c00.setCellStyle(titleStyle);
        // 合并单元格，参数依次为起始行，结束行，起始列，结束列 （索引0开始）
        //标题合并单元格操作，9为总列数
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 1));

        //第四行
        XSSFRow row3 = sheet.createRow(rowNum++);
        row3.setHeight((short) 700);
        String[] row_third = {"党组织名称", "登录次数"};
        for (int i = 0; i < row_third.length; i++) {
            XSSFCell tempCell = row3.createCell(i);
            tempCell.setCellValue(row_third[i]);
            tempCell.setCellStyle(headerStyle2);
        }

        //第五行开始往后
        Pager pager = new Pager();
        pager.setPageSize(10000);
        hornOrganizationService.loginLog(pager,startime,endtime,organid,getSession());
        List<Map<String, Object>> dataList = (List<Map<String, Object>>) pager.getRows(); //查询出来的数据

        //循环每一行数据
        for (Map<String, Object> excelData : dataList) {
            XSSFRow tempRow = sheet.createRow(rowNum++);
            tempRow.setHeight((short) 700);
            // 循环单元格填入数据
            for (int j = 0; j < 2; j++) {
                XSSFCell tempCell = tempRow.createCell(j);
                tempCell.setCellStyle(contentStyle);
                String tempValue;
                if (j == 0) {
                    tempValue = excelData.get("organname").toString();
                } else if (j == 1) {
                    tempValue = excelData.get("num").toString();
                }else{
                    tempValue = "";
                }
                tempCell.setCellValue(tempValue);
            }
        }
        //sheet.autoSizeColumn((short)0);
        sheet.setColumnWidth(0,20000);
        sheet.setColumnWidth(1,4000);

        String fileName = "党组织登录日志统计.xlsx";
        OutputStream stream = null;
        try {
            response.setContentType("multipart/form-data");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename="+"党组织登录日志统计.xlsx");
            response.setHeader("FileName", "党组织登录日志统计.xlsx");
            stream = response.getOutputStream();
            wb.write(stream);// 将数据写出去
        }catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                if(stream != null){
                    stream.flush();
                    stream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * 机构树
     * @param organid
     * @param likeStr
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党委会机构树", notes = "党委会机构树", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "组织机构ID", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "likeStr", value = "查询附加条件", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "zzlbs", value = "需要的组织类别(多个使用英文的逗号隔开)", dataType = "String", paramType = "query", example = "")
    })
    @RequestMapping("/tree3")
    public Message tree3(String organid,String likeStr,String zzlbs){
        Message message = new Message();
        try {
            Object list = hornOrganizationService.getCurrAndSubListOrg3(getSession(),organid,likeStr, zzlbs);
            message.success();
            message.setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("组织机构树查询出错");
        }
        return message;
    }
    /**
     * @Description 党组织数据字典下拉
     * @Author lx
     * @Date 2021/1/26 14:11
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "先进基层党组织数据字典下拉", notes = "先进基层党组织数据字典下拉", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/listDictItem")
    public Message listAddSel() {
        Message msg = new Message();
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            //先进基层组织
            map.put("advanced", hornOrganizationService.listDictItem("d_dzz_advanced"));
            msg.setData(map);
            msg.success();
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
        return msg;
    }
    /**
     * @Description 新增先进基层党组织信息
     * @Author lx
     * @Date 2021/1/26 16:15
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询先进基层党组织信息", notes = "分页查询先进基层党组织信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgCode", value = "组织编码code", required = true, dataType = "String"),
            @ApiImplicitParam(name = "startYear", value = "开始年份", required = false, dataType = "String"),
            @ApiImplicitParam(name = "endYear", value = "结束年份", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageType", value = "1:先进基层党组织分页页面，2:党组织tab页面", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "先进基层党组织类型", required = false, dataType = "String")
    })
    @RequestMapping("/pageDzzAdvanced")
    public Message pageDzzAdvanced(Pager pager,String orgCode,String startYear,String endYear,Integer pageType,String type){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.pageDzzAdvanced(pager,orgCode,startYear,endYear,type,getSession(),pageType));
            message.success();
        }catch (Exception e){
            LehandException.throwException("分页查询先进基层党组织信息失败！"+e);
        }
        return message;
    }
    /**
     * @Description 新增先进基层党组织信息
     * @Author lx
     * @Date 2021/1/26 14:12
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "新增先进基层党组织信息", notes = "新增先进基层党组织信息", httpMethod = "POST")
    @RequestMapping("/addDzzAdvanced")
    public Message addDzzAdvanced(TDzzAdvanced bean){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.addDzzAdvanced(bean,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("新增先进基层党组织信息失败！"+e);
        }
        return message;
    }
    /**
     * @Description 删除先进基层党组织信息
     * @Author lx
     * @Date 2021/1/26 14:38
     * @param
     * @return
     **/
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除先进基层党组织信息", notes = "删除先进基层党组织信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "先进基层党组织信息id", required = true, dataType = "String")
    })
    @RequestMapping("/delDzzAdvanced")
    public Message delDzzAdvanced( String id){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.delDzzAdvanced(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("删除先进基层党组织信息失败！"+e);
        }
        return message;
    }

    /**
     * @Description 编辑先进基层党组织信息
     * @Author lx
     * @Date 2021/1/26 14:47
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "编辑先进基层党组织信息", notes = "编辑先进基层党组织信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "先进基层党组织信息id", required = true, dataType = "String")
    })
    @RequestMapping("/updateDzzAdvanced")
    public Message updateDzzAdvanced(TDzzAdvanced bean){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.updateDzzAdvanced(bean,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("编辑先进基层党组织信息失败！"+e);
        }
        return message;
    }
    /**
     * @Description 查看先进基层党组织信息
     * @Author lx
     * @Date 2021/1/26 14:58
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看先进基层党组织信息", notes = "查看先进基层党组织信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "先进基层党组织信息id", required = true, dataType = "String")
    })
    @RequestMapping("/getDzzAdvanced")
    public Message getDzzAdvanced(String id){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.getDzzAdvanced(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("获取先进基层党组织信息失败！");
        }
        return message;
    }
    @OpenApi(optflag=0)
    @ApiOperation(value = "导出先进基层党组织信息", notes = "导出先进基层党组织信息", httpMethod = "POST")
    @RequestMapping("/exportDzzAdvancedInfo")
    public Message exportDzzAdvancedInfo(HttpServletResponse response, @RequestBody TDzzAdvancedReq req) {
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.exportDzzAdvancedInfo(response, req,req.getOrgcode(),req.getStartYear(),req.getEndYear(),req.getType(),getSession()));
            message.success();
        } catch (Exception e) {
            LehandException.throwException("导出先进基层党组织信息失败！"+e);
        }
        return message;
    }

    /**
     * @Description 党建工作经费组织机构树
     * @Author lx
     * @Date 2021/2/23
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "党建工作经费组织机构树", notes = "党建工作经费组织机构树", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "likeStr", value = "关键字", required = false, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "orgCode", value = "组织编码", required = false, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/dzzFundingTree")
    public Message dzzFundingTree(String orgCode,String likeStr){
        Message message = new Message();
        try {
            List<String> listCode = new ArrayList<>();
            message.setData(hornOrganizationService.dzzFundingTree(orgCode,likeStr,getSession(),listCode));
            message.success();
        }catch (Exception e){
            LehandException.throwException("组织机构树查询失败！"+e);
        }
        return message;
    };
    /**
     * @Description 添加党建工作经费
     * @Author lx
     * @Date 2021/2/23
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "添加党建工作经费", notes = "添加党建工作经费", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/addDzzFunding")
    public Message addDzzFunding(@RequestBody TDzzFunding bean){
        Message message = new Message();
        try {
            hornOrganizationService.addDzzFunding(bean,getSession());
            message.success();
        }catch (Exception e){
            LehandException.throwException("新增失败！"+e);
        }
        return message;
    };

    /**
     * @Description 修改党建工作经费
     * @Author lx
     * @Date 2021/2/23
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改党建工作经费", notes = "修改党建工作经费", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/updateDzzFunding")
    public Message updateDzzFunding(@RequestBody  TDzzFunding bean){
        Message message = new Message();
        try {
            hornOrganizationService.updateDzzFunding(bean,getSession());
            message.success();
        }catch (Exception e){
            LehandException.throwException("编辑失败！"+e);
        }
        return message;
    }
    /**
     * @Description 删除党建工作经费
     * @Author lx
     * @Date 2021/2/23
     * @param
     * @return
     **/
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除党建工作经费", notes = "删除党建工作经费", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delDzzFunding")
    public Message delDzzFunding(String id){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.delDzzFunding(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("删除失败！"+e);
        }
        return message;
    }
    /**
     * @Description 查看党建工作经费
     * @Author lx
     * @Date 2021/2/23
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看党建工作经费", notes = "查看党建工作经费", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getDzzFunding")
    public Message getDzzFunding(String id){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.getDzzFunding(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看失败！"+e);
        }
        return message;
    }

    /**
     * @Description 分页党建工作经费
     * @Author lx
     * @Date 2021/2/23
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页党建工作经费", notes = "分页党建工作经费", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "费用类型", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orgCode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/pageDzzFunding")
    public Message pageDzzFunding(Pager pager,String startTime,String endTime,String type,String orgCode){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.pageDzzFunding(pager,startTime,endTime,type,orgCode,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("分页查询失败！"+e);
        }
        return message;
    }
    /**
     * @Description 党建工作经费
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "党建工作经费下拉", notes = "党建工作经费下拉", httpMethod = "POST")
    @RequestMapping("/listdzzFunding")
    public Message listdzzFunding(){
        Message message = new Message();
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            map.put("dzzFunding", hornOrganizationService.listDictItem("t_dzz_funding"));
            message.setData(map);
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看党员优秀信息失败！"+e);
        }
        return message;
    }
    /**
     * @Description 驾驶舱党建工作经费统计
     * @Author lx
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "驾驶舱党建工作经费统计", notes = "驾驶舱党建工作经费统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/dzzFundingStatistica")
    public Message dzzFundingStatistica(String year){
        Message message = new Message();
        try {
            message.setData(hornOrganizationService.dzzFundingStatistica(getSession(),year));
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看失败！"+e);
        }
        return message;
    }
}
