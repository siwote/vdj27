package com.lehand.horn.partyorgan.constant;

public class DjppSqlCfg {
	
	// 通过组织机构查询品牌数
	public final static String countbrandbyorgid="countbrandbyorgid";
	
	// 新增品牌
	public final static String insertbrand = "insertbrand";
	
	//修改品牌
	public final static String updatebrand = "updatebrand";

	public static final String pagequerybrandall = "pagequerybrandall";

	public static final String branddetail = "branddetail";

	public static final String pagequerynews = "pagequerynews";

	public static final String insertnews = "insertnews";

	public static final String updatenews = "updatenews";

	public static final String updatenewsstatus = "updatenewsstatus";

	public static final String pagequerynewsall = "pagequerynewsall";

	public static final String getnewsdetail = "getnewsdetail";

	public static final String getfiledir = "getfiledir";

	public static final String countpostlikerecord = "countpostlikerecord";

	public static final String postlikerecordinsert = "postlikerecordinsert";

	public static final String getpostlikerecordstatus = "getpostlikerecordstatus";

	public static final String postlikerecordupdate = "postlikerecordupdate";

	public static final String getlikepostcountnum = "getlikepostcountnum";

	public static final String getlikepostcount = "getlikepostcount";

	public static final String likepostcountinsert = "likepostcountinsert";

	public static final String likepostcountupdate = "likepostcountupdate";
}
