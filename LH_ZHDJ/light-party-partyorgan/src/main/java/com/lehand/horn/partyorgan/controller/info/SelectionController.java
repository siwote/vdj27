package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.base.common.Session;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.dto.info.NodeDto;
import com.lehand.horn.partyorgan.dto.info.UserDto;
import com.lehand.horn.partyorgan.service.info.SelectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static com.lehand.components.web.interceptors.BaseInterceptor.getSession;

/**
 * 选择器控制层
 *
 * @author admin
 */
@Api(value = "选择器控制层", tags = { "选择器控制层" })
@RestController
@RequestMapping("/lhdj/selection")
public class SelectionController {

    @Resource
    private SelectionService selectionService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "获取指定节点下的所有子节点(含组织，不含用户)", notes = "获取指定节点下的所有子节点(含组织，不含用户)", httpMethod = "POST")
    @RequestMapping("/listOrganChildNode")
    public Message<List<NodeDto> > listOrganChildNode(Long orgid,String organname){
        Message msg = new Message();
        try {
            List<NodeDto>  nodes = selectionService.listOrganChildNode(getSession().getCompid(),orgid,organname);
            msg.success().setData(nodes);
        } catch (Exception e) {
            e.printStackTrace();
            msg.setMessage(e.getMessage());
        }
        return msg;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "获取指定节点下的所有子节点(含组织，不含用户)", notes = "获取指定节点下的所有子节点(含组织，不含用户)", httpMethod = "POST")
    @RequestMapping("/listOrganChildNodeOne")
    public Message<Map<String, Object>> listOrganChildNodeOne(Long orgid,String organname){
        Message msg = new Message();
        try {
            Map<String, Object> nodes = selectionService.listOrganChildNodeOne(getSession());
            msg.success().setData(nodes);
        } catch (Exception e) {
            e.printStackTrace();
            msg.setMessage(e.getMessage());
        }
        return msg;
    }


    /**
     * 成员
     * 获取指定节点下的所有子节点(含用户、组织)
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取指定节点下的所有子节点(含用户、组织)", notes = "获取指定节点下的所有子节点(含用户、组织)", httpMethod = "POST")
    @RequestMapping("/listAllChildNode")
    public Message<NodeDto> listAllChildNode(String username){

        Message msg = new Message();
        try {
            List<UserDto> nodes = selectionService.listAllChildNode(getSession().getCompid(),username);
            msg.success().setData(nodes);
        } catch (Exception e) {
            e.printStackTrace();
            msg.setMessage(e.getMessage());
        }
        return msg;
    }

    /**
     * 成员
     * 获取所用组织用户
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取所用组织用户", notes = "获取所用组织用户", httpMethod = "POST")
    @RequestMapping("/listAllChildNode2")
    public Message<NodeDto> listAllChildNode2(String username,String orgid){

        Message msg = new Message();
        try {
            List<UserDto> nodes = selectionService.listAllChildNode2(getSession(),username,orgid);
            msg.success().setData(nodes);
        } catch (Exception e) {
            e.printStackTrace();
            msg.setMessage(e.getMessage());
        }
        return msg;
    }


    /**
     * 审核人 成员
     *  从角色中获取审核人集合
	 * @return
   */
    @OpenApi(optflag=0)
    @ApiOperation(value = "从角色中获取审核人集合", notes = " 审核人 成员", httpMethod = "POST")
    @RequestMapping("/listRoleMap")
    public Message<List<Map<String,Object>>> listRoleMap(){
        Message msg = new Message();
        try {
            List<Map<String,Object>> nodes = selectionService.listRoleMap(getSession());
            msg.success().setData(nodes);
        } catch (Exception e) {
            e.printStackTrace();
            msg.setMessage(e.getMessage());
        }
        return msg;
    }


    /**
     * 获取指定节点下的所有子节点(含组织，不含用户)
     * type: 当前人员的节点 my， level 1
     * @param orgid
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取指定节点下的所有子节点(含组织，不含用户)", notes = " type: 当前人员的节点 my， level 1", httpMethod = "POST")
    @RequestMapping("/listChildNodeByType")
    public Message<List<NodeDto>>  listChildNodeByType(Long orgid,String organname,String type,String level){
        Message msg = new Message();
        try {

            List<NodeDto> nodes = selectionService.listChildNodeByTypehildNodeByType(getSession(),orgid,organname,type,level);

            msg.success().setData(nodes);
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
        }
        return msg;
    }
    /**
     * 当前组织
     * @param orgid
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "当前组织", notes = "当前组织", httpMethod = "POST")
    @RequestMapping("/getNode")
    public Message<List<NodeDto>>  getNode(Long orgid,String organname,String type,String level){
        Message msg = new Message();
        try {

            List<NodeDto> nodes = selectionService.getNode(getSession(),orgid);

            msg.success().setData(nodes);
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
        }
        return msg;
    }
}
