package com.lehand.horn.partyorgan.pojo.hjxx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: TDzzHjxxRemindTime
 * @Description: 党组织换届提醒时间节点配置表
 * @Author lx
 * @DateTime 2021-05-10 14:18
 */
@ApiModel(value = "党组织换届提醒时间节点配置表", description = "党组织换届提醒时间节点配置表")
public class TDzzHjxxRemindTime implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "compid", name = "compid")
    private long compid;
    @ApiModelProperty(value = "组织id", name = "orgid")
    private String orgid;
    @ApiModelProperty(value = "组织code", name = "orgcode")
    private String orgcode;
    /**
     *换届提醒时间
     */
    @ApiModelProperty(value = "换届提醒时间", name = "remindtime")
    private String remindtime;
    /**
     * 状态(0:未创建,1:正在创建,2:创建完成)
     */
    @ApiModelProperty(value = "状态(0:未创建,1:正在创建,2:创建完成)", name = "status")
    private long status;
    @ApiModelProperty(value = "创建时间", name = "createtime")
    private String createtime;
    @ApiModelProperty(value = "创建人ID", name = "createuserid")
    private long createuserid;

    @ApiModelProperty(value = "距离届满还有几月", name = "distance")
    private int distance;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public long getCompid() {
        return compid;
    }

    public void setCompid(long compid) {
        this.compid = compid;
    }


    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }


    public String getRemindtime() {
        return remindtime;
    }

    public void setRemindtime(String remindtime) {
        this.remindtime = remindtime;
    }


    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }


    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }


    public long getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(long createuserid) {
        this.createuserid = createuserid;
    }

}
