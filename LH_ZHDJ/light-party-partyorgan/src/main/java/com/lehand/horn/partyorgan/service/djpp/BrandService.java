package com.lehand.horn.partyorgan.service.djpp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import org.springframework.beans.factory.annotation.Value;

import com.greenpineyu.fel.common.StringUtils;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.DjppSqlCfg;
import com.lehand.horn.partyorgan.pojo.djpp.BrandBasicInfo;
import com.lehand.horn.partyorgan.pojo.djpp.OrgNews;
import org.springframework.stereotype.Service;

@Service
public class BrandService{
	@Resource private GeneralSqlComponent generalSqlComponent;
	@Value("${jk.manager.down_ip}")
	private String downIp;		
	/**
	 * 保存党建品牌
	 * @param brandname 品牌名称
	 * @param brandtime 品牌创建时间
	 * @param brandslogan 宣传语
	 * @param brandlogo	logo
	 * @param brandintro 简介
	 * @param brandidea	理念
	 * @param brandmentality 思路
	 * @param measures 举措
	 * @return
	 */
	public int save(String brandname, String brandtime, String brandslogan, String brandlogo, String brandintro,
			String brandidea, String brandmentality, String measures, Session session) {
		Map<String,Object> map  = new HashMap<String,Object>(12);
		map.put("brandname", brandname);
		map.put("brandtime", brandtime);
		map.put("brandslogan", brandslogan);
		map.put("brandlogo", brandlogo);
		map.put("brandintro", brandintro);
		map.put("brandidea", brandidea);
		map.put("brandmentality", brandmentality);
		map.put("measures", measures);
		map.put("compid", session.getCompid());
		map.put("orgcode", "0"+session.getCurrorganid());
		// 判断新增还是修改
		int num = generalSqlComponent.query(DjppSqlCfg.countbrandbyorgid, new Object[] {session.getCompid(),"0"+session.getCurrorganid()});
		if(num>0) {//修改处理
			map.put("modifer", session.getUsername());
			map.put("moditime",DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
			return generalSqlComponent.update(DjppSqlCfg.updatebrand, map);
		}
		map.put("creater", session.getUsername());
		map.put("createtime",DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
		return generalSqlComponent.insert(DjppSqlCfg.insertbrand, map);
	}


	@SuppressWarnings("unchecked")
	public Pager pageQuery(Pager pager, Session session) {
		
		pager = generalSqlComponent.pageQuery(DjppSqlCfg.pagequerybrandall, new Object[] {session.getCompid()}, pager);
		List<BrandBasicInfo> list = (List<BrandBasicInfo>) pager.getRows();
		if(list.size()<1) {
			return pager;
		}
		for (BrandBasicInfo brandBasicInfo : list) {
            makeBrandBasicInfo(session, brandBasicInfo);
        }
		pager.setRows(list);
		return pager;
	}

	/**党建品牌详情
	 * @param orgcode
	 * @return
	 */
	public Map<String, Object> detail(Pager pager,String orgcode, Session session) {
		Map<String,Object> map = new HashMap<String,Object>(3);
		BrandBasicInfo brandBasicInfo = generalSqlComponent.query(DjppSqlCfg.branddetail, new Object[] {session.getCompid(),orgcode});		
		if(brandBasicInfo!=null) {
            makeBrandBasicInfo(session, brandBasicInfo);
        }
		map.put("maininfo", brandBasicInfo);
		map.put("poststatus",getpoststatus(orgcode,session));
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("compid", session.getCompid());
		params.put("orgcode", orgcode);
		map.put("newspiclist", getNewsPicList(pager,params));
		map.put("newlist", getNewsList(pager,params));
		return map;
	}

    private void makeBrandBasicInfo(Session session, BrandBasicInfo brandBasicInfo) {
        if (!StringUtils.isEmpty(brandBasicInfo.getBrandlogo())) {
            String dir = generalSqlComponent.query(DjppSqlCfg.getfiledir, new Object[]{brandBasicInfo.getBrandlogo(), session.getCompid()});
            StringBuffer buffer = new StringBuffer();
            buffer.append(downIp);
            buffer.append(dir);
            brandBasicInfo.setBrandlogo(StringUtils.isEmpty(dir) ? "" : buffer.toString());
        }
    }

    private boolean getpoststatus(String orgcode, Session session) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("compid", session.getCompid());
		param.put("articleid", orgcode);
		param.put("businessid", 0);
		param.put("userid", session.getUserid());
		String status = generalSqlComponent.query(DjppSqlCfg.getpostlikerecordstatus, param);
		if(!StringUtils.isEmpty(status)&&!"0".equals(status)) {
			return true;
		}
		return false;
	}

	/**
	 * 新闻图片列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<OrgNews> getNewsPicList(Pager pager,Map<String,Object> params) {
		params.put("pic", "pic");	
		pager = generalSqlComponent.pageQuery(DjppSqlCfg.pagequerynews, params, pager);
		List<OrgNews> list = (List<OrgNews>) pager.getRows();
		if(list.size()<1) {
			return list;
		}
        makeOrgnews(params, list);
//		pager.setRows(list);
		return list;
	}

	/**
	 * 新闻列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<OrgNews> getNewsList(Pager pager,Map<String,Object> params) {
		params.put("pic", null);
		pager = generalSqlComponent.pageQuery(DjppSqlCfg.pagequerynews, params, pager);
		List<OrgNews> list = (List<OrgNews>) pager.getRows();
		if(list.size()<1) {
			return list;
		}
        makeOrgnews(params, list);
//		pager.setRows(list);
		return list;
	}

    private void makeOrgnews(Map<String, Object> params, List<OrgNews> list) {
        for (OrgNews orgNews : list) {
            if (!StringUtils.isEmpty(orgNews.getTitlepic())) {
                String dir = generalSqlComponent.query(DjppSqlCfg.getfiledir, new Object[]{orgNews.getTitlepic(), params.get("compid")});
                StringBuffer buffer = new StringBuffer();
                buffer.append(downIp);
                buffer.append(dir);
                orgNews.setTitlepic(StringUtils.isEmpty(dir) ? "" : buffer.toString());
            }
        }
    }

    /**
	 * 保存新闻详情
	 * @param id 主键id
	 * @param title 新闻标题
	 * @param titlepic 新闻图片
	 * @param publishtime 发布时间
	 * @param content 新闻内容
	 * @return
	 */
	public int saveNews(String id, String title, String titlepic, String publishtime, String content,Session session) {
		Map<String,Object> params = new HashMap<String,Object>(9);
		params.put("id", id);
		params.put("title", title);
		params.put("titlepic", titlepic);
		params.put("publishtime", publishtime);
		params.put("content", content);
		params.put("orgcode", "0"+session.getCurrorganid());
		params.put("compid", session.getCompid());
		if(StringUtils.isEmpty(id)) {
			params.put("creater", session.getLoginAccount());
			params.put("createtime", DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
			return generalSqlComponent.insert(DjppSqlCfg.insertnews, params);
		}
		params.put("modifer", session.getLoginAccount());
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
		return generalSqlComponent.update(DjppSqlCfg.updatenews, params);
	}

	public int updateStatus(String id, int i, Session session) {
		Map<String,Object> param = new HashMap<String,Object>(4);
		param.put("id", id);
		param.put("compid", session.getCompid());
		param.put("status", i);
		param.put("modifer", session.getUsername());
		param.put("moditime", DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
		return generalSqlComponent.update(DjppSqlCfg.updatenewsstatus, param);
	}

	public Pager newsPageQuery(Pager pager, String likeStr, Session session) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("likeStr", likeStr);
		param.put("orgcode", "0"+session.getCurrorganid());
		param.put("compid", session.getCompid());
		return generalSqlComponent.pageQuery(DjppSqlCfg.pagequerynewsall, param, pager);
	}

	
	public OrgNews newsDetail(String id, Session session) {
		OrgNews orgNews = generalSqlComponent.query(DjppSqlCfg.getnewsdetail, new Object[] {id,session.getCompid()});
		if(!StringUtils.isEmpty(orgNews.getTitlepic())) {
			String  dir = generalSqlComponent.query(DjppSqlCfg.getfiledir, new Object[] {orgNews.getTitlepic(),session.getCompid()});
			StringBuffer buffer = new StringBuffer();
			buffer.append(downIp);
			buffer.append(dir);
			orgNews.setUrl(StringUtils.isEmpty(dir)?"":buffer.toString());
    	}
		return orgNews;
	}

	/**
	 * 点赞
	 */
	
	public int postLike(String orgcode, Session session) {
		int count = generalSqlComponent.query(DjppSqlCfg.countpostlikerecord, new Object[] {orgcode,0,session.getUserid(),session.getCompid()});
		int result = 0;
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("compid", session.getCompid());
		param.put("articleid", orgcode);
		param.put("businessid", 0);
		param.put("userid", session.getUserid());
		if(count<1) {
			param.put("poststatus", 1);
			param.put("liketime", DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
			result = generalSqlComponent.insert(DjppSqlCfg.postlikerecordinsert, param);
		}else {
			String status = generalSqlComponent.query(DjppSqlCfg.getpostlikerecordstatus, param);
			param.put("poststatus", Short.parseShort(status)==1?0:1);
			param.put("liketime", DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
			result = generalSqlComponent.update(DjppSqlCfg.postlikerecordupdate, param);
		}
		if(result<1) {
			return result;
		}
		return updatepostCount(session.getCompid(),orgcode);
	}

	private int updatepostCount(Long compid, String orgcode) {
		Map<String,Object> param =  new HashMap<String,Object>();
		param.put("compid", compid);
		param.put("articleid", orgcode);
		param.put("businessid", 0);
		int count = generalSqlComponent.query(DjppSqlCfg.getlikepostcount, param);
		int num = generalSqlComponent.query(DjppSqlCfg.getlikepostcountnum, param);
		
		param.put("postnum", count);
		param.put("updatetime", DateEnum.YYYYMMDDHHMMDD.format(DateEnum.now()));
		if(num<1) {
			return generalSqlComponent.insert(DjppSqlCfg.likepostcountinsert, param);
		}		
		return generalSqlComponent.update(DjppSqlCfg.likepostcountupdate, param);
	}

	
	public BrandBasicInfo brandDetail(Session session) {
		BrandBasicInfo brandBasicInfo = generalSqlComponent.query(DjppSqlCfg.branddetail, new Object[] {session.getCompid(),"0"+session.getCurrorganid()});		
		if(brandBasicInfo==null) {		
			return null;
		}
		if(!StringUtils.isEmpty(brandBasicInfo.getBrandlogo())) {
			String  dir = generalSqlComponent.query(DjppSqlCfg.getfiledir, new Object[] {brandBasicInfo.getBrandlogo(),session.getCompid()});
			StringBuffer buffer = new StringBuffer();
			buffer.append(downIp);
			buffer.append(dir);
			brandBasicInfo.setUrl(StringUtils.isEmpty(dir)?"":buffer.toString());
		}
		return brandBasicInfo;
	}


}
