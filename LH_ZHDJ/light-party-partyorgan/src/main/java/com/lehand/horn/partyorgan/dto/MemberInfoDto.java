package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

import com.lehand.horn.partyorgan.constant.MagicConstant;

import java.util.UUID;

@ApiModel(value = "党员信息返回数据",description = "党员信息返回数据")
public class MemberInfoDto extends MemberBasicDto {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="表单key",name="uuid")
    private  String uuid;

    @ApiModelProperty(value="党员id",name="userid")
    private  String userid;

    @ApiModelProperty(value="人员类别编码",name="dylb")
    private String dylb;

    @ApiModelProperty(value="党员类别名称",name="rylbname")
    private String rylbname;

    @ApiModelProperty(value="性别",name="xb")
    private String xb;

    @ApiModelProperty(value="性别名称",name="xbname")
    private String xbname;

    @ApiModelProperty(value="民族",name="mz")
    private String mz;

    @ApiModelProperty(value="籍贯",name="jg")
    private String jg;

    @ApiModelProperty(value="民族名称",name="mzname")
    private String mzname;

    @ApiModelProperty(value="年龄",name="age")
    private Integer age;

    @ApiModelProperty(value="组织机构名称",name="dzzmc")
    private String dzzmc;

    @ApiModelProperty(value="出生日期",name="csrq")
    private String csrq;

    @ApiModelProperty(value="学历名称",name="xlname")
    private String xlname;

    @ApiModelProperty(value="毕业学校",name="byyx")
    private String byyx;

    @ApiModelProperty(value="学位",name="xwname")
    private String xwname;

    @ApiModelProperty(value="学位编码",name="xw")
    private String xw;

    @ApiModelProperty(value="专业名称",name="zy")
    private String zy;

    @ApiModelProperty(value="转正日期",name="zzsj")
    private String  zzsj;

    @ApiModelProperty(value="入党日期",name="rdsj")
    private String  rdsj;

    @ApiModelProperty(value="是否民工",name="isworkers")
    private String  isworkers;

    @ApiModelProperty(value="联系电话",name="lxdh")
    private String lxdh;

    @ApiModelProperty(value="户籍所在地",name="hjszd")
    private String hjszd;

    @ApiModelProperty(value="工作岗位",name="gzgw")
    private String gzgw;

    @ApiModelProperty(value="工作岗位名称",name="gzgwname")
    private String gzgwname;

    @ApiModelProperty(value="从事专业技术职务",name="jszc")
    private String jszc;

    @ApiModelProperty(value="从事专业技术职务名称",name="jszcname")
    private String jszcname;

    @ApiModelProperty(value="新的社会阶层",name="xshjclx")
    private String xshjclx;

    @ApiModelProperty(value="新的社会阶层名称",name="xshjclxname")
    private String xshjclxname;

    @ApiModelProperty(value="其他联系电话",name="gddh")
    private String gddh;

    @ApiModelProperty(value="联合支部单位名称",name="dwname")
    private String dwname;

    @ApiModelProperty(value="档案所在单位",name="dydaszdw")
    private String dydaszdw;

    @ApiModelProperty(value="现居住地",name="xjzd")
    private String xjzd;

    @ApiModelProperty(value="失去联系情形代码",name="lostreason")
    private String lostreason;

    @ApiModelProperty(value="失去联系情形",name="lostreasonname")
    private String lostreasonname;

    @ApiModelProperty(value="失去联系日期",name="losttime")
    private String losttime;

    @ApiModelProperty(value="是否困难党员", name="povertyParty")
    private String povertyParty;

    @ApiModelProperty(value="是否党员示范岗位", name="demonstrationParty")
    private String demonstrationParty;
    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="组织编码", name="organcode")
    private String organcode;

    public String getOrgancode() {
        return organcode;
    }

    public void setOrgancode(String organcode) {
        this.organcode = organcode;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getPovertyParty() {
        return povertyParty;
    }

    public void setPovertyParty(String povertyParty) {
        this.povertyParty = povertyParty;
    }

    public String getDemonstrationParty() {
        return demonstrationParty;
    }

    public void setDemonstrationParty(String demonstrationParty) {
        this.demonstrationParty = demonstrationParty;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = StringUtils.isEmpty(uuid)? UUID.randomUUID().toString().replaceAll("-",""):uuid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDylb() {
        return  StringUtils.isEmpty(dylb)? MagicConstant.STR.EMPTY_STR:dylb;
    }

    public void setDylb(String dylb) {
        this.dylb = dylb;
    }

    public String getRylbname() {
        return StringUtils.isEmpty(rylbname)? MagicConstant.STR.EMPTY_STR:rylbname;
    }

    public void setRylbname(String rylbname) {
        this.rylbname = rylbname;
    }

    public String getXb() {
        return StringUtils.isEmpty(xb)? MagicConstant.STR.EMPTY_STR:xb;
    }

    public void setXb(String xb) {
        this.xb = xb;
    }

    public String getMz() {
        return StringUtils.isEmpty(mz)? MagicConstant.STR.EMPTY_STR:mz;
    }

    public void setMz(String mz) {
        this.mz = mz;
    }

    public Integer getAge() {
        return StringUtils.isEmpty(age)? 0 :age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDzzmc() {
        return StringUtils.isEmpty(dzzmc)? MagicConstant.STR.EMPTY_STR:dzzmc;
    }

    public void setDzzmc(String dzzmc) {
        this.dzzmc = dzzmc;
    }

    public String getXbname() {
        return StringUtils.isEmpty(xbname)? MagicConstant.STR.EMPTY_STR:xbname;
    }

    public void setXbname(String xbname) {
        this.xbname = xbname;
    }

    public String getMzname() {
        return StringUtils.isEmpty(mzname)? MagicConstant.STR.EMPTY_STR:mzname;
    }

    public void setMzname(String mzname) {
        this.mzname = mzname;
    }

    public String getCsrq() {
        return StringUtils.isEmpty(csrq)? MagicConstant.STR.EMPTY_STR:csrq.replaceAll("00:00:00","");
    }

    public void setCsrq(String csrq) {
        this.csrq = csrq;
    }

    public String getXlname() {
        return StringUtils.isEmpty(xlname)? MagicConstant.STR.EMPTY_STR:xlname;
    }

    public void setXlname(String xlname) {
        this.xlname = xlname;
    }

    public String getXw() {
        return xw;
    }

    public void setXw(String xw) {
        this.xw = xw;
    }

    public String getByyx() {
        return StringUtils.isEmpty(byyx)? MagicConstant.STR.EMPTY_STR:byyx;
    }

    public void setByyx(String byyx) {
        this.byyx = byyx;
    }

    public String getZy() {
        return StringUtils.isEmpty(zy)? MagicConstant.STR.EMPTY_STR:zy;
    }

    public void setZy(String zy) {
        this.zy = zy;
    }

    public String getZzsj() {
        return StringUtils.isEmpty(zzsj)? MagicConstant.STR.EMPTY_STR:zzsj.substring(0,10);
    }

    public void setZzsj(String zzsj) {
        this.zzsj = zzsj;
    }

    public String getIsworkers() {
        return StringUtils.isEmpty(isworkers)? MagicConstant.STR.EMPTY_STR:isworkers;
    }

    public void setIsworkers(String isworkers) {
        this.isworkers = isworkers;
    }

    public String getLxdh() {
        return StringUtils.isEmpty(lxdh)? MagicConstant.STR.EMPTY_STR:lxdh;
    }

    public void setLxdh(String lxdh) {
        this.lxdh = lxdh;
    }

    public String getHjszd() {
        return StringUtils.isEmpty(hjszd)? MagicConstant.STR.EMPTY_STR:hjszd;
    }

    public void setHjszd(String hjszd) {
        this.hjszd = hjszd;
    }

    public String getGzgw() {
        return StringUtils.isEmpty(gzgw)? MagicConstant.STR.EMPTY_STR:gzgw;
    }

    public void setGzgw(String gzgw) {
        this.gzgw = gzgw;
    }

    public String getJszc() {
        return StringUtils.isEmpty(jszc)? MagicConstant.STR.EMPTY_STR:jszc;
    }

    public void setJszc(String jszc) {
        this.jszc = jszc;
    }

    public String getXshjclx() {
        return StringUtils.isEmpty(xshjclx)? MagicConstant.STR.EMPTY_STR:xshjclx;
    }

    public void setXshjclx(String xshjclx) {
        this.xshjclx = xshjclx;
    }

    public String getGddh() {
        return StringUtils.isEmpty(gddh)? MagicConstant.STR.EMPTY_STR:gddh;
    }

    public void setGddh(String gddh) {
        this.gddh = gddh;
    }

    public String getDwname() {
        return dwname;
    }

    public void setDwname(String dwname) {
        this.dwname = dwname;
    }

    public String getDydaszdw() {
        return StringUtils.isEmpty(dydaszdw)? MagicConstant.STR.EMPTY_STR:dydaszdw;
    }

    public void setDydaszdw(String dydaszdw) {
        this.dydaszdw = dydaszdw;
    }

    public String getXjzd() {
        return StringUtils.isEmpty(xjzd)? MagicConstant.STR.EMPTY_STR:xjzd;
    }

    public void setXjzd(String xjzd) {
        this.xjzd = xjzd;
    }

    public String getLostreason() {
        return lostreason;
    }

    public void setLostreason(String lostreason) {
        this.lostreason = lostreason;
    }

    public String getLostreasonname() {
        return lostreasonname;
    }

    public void setLostreasonname(String lostreasonname) {
        this.lostreasonname = lostreasonname;
    }

    public String getLosttime() {
        return StringUtils.isEmpty(losttime)? MagicConstant.STR.EMPTY_STR:losttime.substring(0,10);
    }

    public void setLosttime(String losttime) {
        this.losttime = losttime;
    }

    public String getXwname() {
        return xwname;
    }

    public void setXwname(String xwname) {
        this.xwname = xwname;
    }

    public String getRdsj() {
        return rdsj;
    }

    public void setRdsj(String rdsj) {
        this.rdsj = rdsj;
    }

    public String getGzgwname() {
        return gzgwname;
    }

    public void setGzgwname(String gzgwname) {
        this.gzgwname = gzgwname;
    }

    public String getJszcname() {
        return jszcname;
    }

    public void setJszcname(String jszcname) {
        this.jszcname = jszcname;
    }

    public String getXshjclxname() {
        return xshjclxname;
    }

    public void setXshjclxname(String xshjclxname) {
        this.xshjclxname = xshjclxname;
    }

    public String getJg() {
        return jg;
    }

    public void setJg(String jg) {
        this.jg = jg;
    }
}
