package com.lehand.horn.partyorgan.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.pojo.TreeNode;
import com.lehand.horn.partyorgan.pojo.dict.GDictItem;



/**
 * 参数字典服务层
 * @author taogang
 * @version 1.0
 * @date 2019年1月21日, 下午7:52:52
 */
@Service
public class GdictItemService{
	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	
	
	/**
	 * 参数字典返回list
	 * @param code
	 * @return
	 * @author taogang
	 */
	public List<TreeNode> listItem(String code){
		//查询参数
//	   List<TreeNode> itemList = gdictItemDao.listItem(code);
	   List<TreeNode> itemList = generalSqlComponent.query(SqlCode.getItemListByCode, new Object[] {code});
	   List<TreeNode> tree = makeTree(itemList, -1L);
	   return tree;
	}

	public List<TreeNode> listItem2(String code){
		//查询参数
		List<TreeNode> itemList = generalSqlComponent.query(SqlCode.getItemListByCode2, new Object[] {code});
		return itemList;
	}

    public List<TreeNode> listItemNew(String code,String zzlb, Session session){
	    String codeType = generalSqlComponent.getDbComponent().getSingleColumn("select " +
                        "zzlb from t_dzz_info where organcode=?",String.class,new Object[]{StringUtils.isEmpty(zzlb)
                ? session.getOrgs().get(0).getOrgcode() : zzlb});
        //查询参数
        List<TreeNode> itemList = new ArrayList<>();
        if(!StringUtils.isEmpty(codeType)){
            switch (codeType){
                case "6100000035":
                    itemList= generalSqlComponent.getDbComponent().listBean(
                            "select item_code as value, item_name as lable,parent_code as pid from g_dict_item where code = ? " +
                                    "and parent_code!=-1  and item_code in (3100000009,3200000010,3400000021) order by " +
                                    "item_order asc",
                            TreeNode.class,
                            new Object[] {code});
                    break;
                case "6200000036":
                    itemList=generalSqlComponent.getDbComponent().listBean(
                            "select item_code as value, item_name as lable,parent_code as pid from g_dict_item where code = ? " +
                                    "and parent_code!=-1  and item_code in (4100000013,4200000014,4300000015) order by " +
                                    "item_order asc",
                            TreeNode.class,
                            new Object[] {code});
                    break;
                case "6300000037":
                    itemList=generalSqlComponent.getDbComponent().listBean(
                            "select item_code as value, item_name as lable,parent_code as pid from g_dict_item where code = ? " +
                                    "and parent_code!=-1  and item_code in (5100000017,5200000018,5300000022) order by " +
                                    "item_order asc",
                            TreeNode.class,
                            new Object[] {code});
                    break;
                default:
                    itemList=generalSqlComponent.getDbComponent().listBean(
                            "select item_code as value, item_name as lable,parent_code as pid from g_dict_item where code = ? " +
                                    "and parent_code!=-1  and item_code in (5100000017,5200000018,5300000022) order by item_order asc",
                            TreeNode.class,
                            new Object[] {code});
                    break;

            }
        }
        return itemList;
    }
	
	/**
	 * 递归查询树
	 * @param items
	 * @param pid
	 * @return
	 * @author taogang
	 */
	public List<TreeNode> makeTree(List<TreeNode> items,Long pid){
		//子类
		List<TreeNode> children = items.stream().filter(x -> Long.valueOf(x.getPid())  == pid).collect(Collectors.toList());
		
		//后辈中的非子类
		List<TreeNode> successor = items.stream().filter(x -> Long.valueOf(x.getPid())  != pid).collect(Collectors.toList());
		children.forEach(x ->
         {   
        		 makeTree(successor, Long.valueOf(x.getValue())).forEach(
                		 y -> x.getChildren().add(y)
                 ); 
         }
		);
		return children;
	}
	
	
	/**
	 * 根据编码返回名称
	 * @param code
	 * @param itemCode
	 * @return
	 * @author taogang
	 */
	public String getItemName(String code,String itemCode) {
		String name = "";
		if(!StringUtils.isEmpty(itemCode)) {
			GDictItem item = generalSqlComponent.query(SqlCode.getItemByItemcode, new Object[] {code, itemCode});
			if(null != item) {
				name = item.getItemName();
			}
		}
		return name;
	}

}
