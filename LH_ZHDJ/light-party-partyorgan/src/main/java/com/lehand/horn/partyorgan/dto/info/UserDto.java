package com.lehand.horn.partyorgan.dto.info;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class UserDto  implements Serializable {

    private final static long serialVersionUID = 1L;

    /** 姓名*/
    private String xm;

    /** 同上*/
    private String username;

    /** 同上*/
    private String subjectname;

    /** 用户id*/
    private Long userid;

    /** 同上*/
    private Long id;

    /** 同上*/
    private Long subjectid;

    /** 无用,返回前端使用*/
    private List<Map<String,Object>> childs;


    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSubjectname() {
        return subjectname;
    }

    public void setSubjectname(String subjectname) {
        this.subjectname = subjectname;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Long subjectid) {
        this.subjectid = subjectid;
    }

    public List<Map<String, Object>> getChilds() {
        return childs;
    }

    public void setChilds(List<Map<String, Object>> childs) {
        this.childs = childs;
    }
}
