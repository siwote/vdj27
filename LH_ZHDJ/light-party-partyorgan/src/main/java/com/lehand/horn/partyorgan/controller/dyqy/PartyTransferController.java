package com.lehand.horn.partyorgan.controller.dyqy;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.controller.HornBaseController;
import com.lehand.horn.partyorgan.dto.PartyTransferDto;
import com.lehand.horn.partyorgan.service.PartyTransferPDFService;
import com.lehand.horn.partyorgan.service.dyqy.PartyTransferService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(value = "党员迁移", tags = { "党员迁移" })
@RestController
@RequestMapping("/lhdj/dyqy")
/**
 * @program: huaikuang-dj
 * @description: PartyTransferController
 * @author: zwd
 * @create: 2020-11-25 14:36
 */
public class PartyTransferController  extends HornBaseController {

    @Resource
    PartyTransferService partyTransferService;

    @Resource
    PartyTransferPDFService partyTransferPDFService;
    /**
    * @Description 新建党员迁移
    * @Author zwd
    * @Date 2020/11/25 14:47
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "新建党员迁移", notes = "新建党员迁移", httpMethod = "POST")
    @RequestMapping("/save")
    public Message savePartyTransfer(@RequestBody  PartyTransferDto bean){
        Message message = new Message();
        message.setData(partyTransferService.savePartyTransfer(bean,getSession())).success();
        return message;
    }

    /**
    * @Description 党员迁移审核
    * @Author zwd
    * @Date 2020/11/25 17:27
    * @param
    * @return
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员迁移审核", notes = "党员迁移审核", httpMethod = "POST")
    @RequestMapping("/update")
    public Message updatePartyTransfer(PartyTransferDto bean){
        Message message = new Message();
        message.setData(partyTransferService.updatePartyTransfer(bean,getSession())).success();
        return message;
    }

    /**
    * @Description 重新提交
    * @Author zwd
    * @Date 2020/11/25 18:25
    * @param
    * @return
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "重新提交 status=1", notes = "重新提交 status=1", httpMethod = "POST")
    @RequestMapping("/again")
    public Message againPartyTransfer(PartyTransferDto bean){
        Message message = new Message();
        message.setData(partyTransferService.againPartyTransfer(bean,getSession())).success();
        return message;
    }

    /**
    * @Description 党员迁移分页查询
    * @Author zwd
    * @Date 2020/11/27 10:43
    * @param
    * @return
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员迁移分页查询-转出申请", notes = "党员迁移分页查询-转出申请", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "username", value = "党员名称", required = false, paramType = "query", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "status", value = "审核状态", required = false, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "startdate", value = "审核时间-开始", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "enddate", value = "审核时间-结束", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "1:转出申请，2:转入申请", required = true, paramType = "query", dataType = "int", defaultValue = ""),
//            @ApiImplicitParam(name = "type2", value = "0未审核，1:已审核", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    @RequestMapping("/page")
    public Message partyTransferPage(Pager pager,String username,String startdate,String enddate,Integer type,Integer status){
        Message message = new Message();
        message.setData(partyTransferService.partyTransferPage(pager,username,startdate,enddate,type,status,getSession())).success();
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党员迁移分页查询-转入接收", notes = "党员迁移分页查询-转入接收", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "username", value = "党员名称", required = false, paramType = "query", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "status", value = "审核状态", required = false, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "startdate", value = "审核时间-开始", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "enddate", value = "审核时间-结束", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "1:转出申请，2:转入申请", required = true, paramType = "query", dataType = "int", defaultValue = ""),
//            @ApiImplicitParam(name = "type2", value = "0未审核，1:已审核", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    @RequestMapping("/page2")
    public Message partyTransferPage2(Pager pager,String username,String startdate,String enddate,Integer type){
        Message message = new Message();
        message.setData(partyTransferService.partyTransferPage2(pager,username,startdate,enddate,type,getSession())).success();
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党员迁移分页查询-审核", notes = "党员迁移分页查询-审核", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "username", value = "党员名称", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "审核状态", required = false, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "startdate", value = "审核时间-开始", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "enddate", value = "审核时间-结束", required = false, paramType = "query", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "type", value = "1:转出申请，2:转入申请", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "type2", value = "0未审核，1:已审核", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    @RequestMapping("/page3")
    public Message partyTransferPage3(Pager pager,String username,Integer status,String startdate,String enddate,Integer type2){
        Message message = new Message();
        message.setData(partyTransferService.partyTransferPage3(pager,username,status,startdate,enddate,type2,getSession())).success();
        return message;
    }

    /**
    * @Description 党员迁入未审核数量
    * @Author zwd
    * @Date 2020/11/27 14:13
    * @param
    * @return
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员迁入未审核数量", notes = "党员迁入未审核数量", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "审核状态", required = false, paramType = "query", dataType = "int", defaultValue = ""),
    })
    @RequestMapping("/intonum")
    public Message partyTransferIntoNum(Integer status){
        Message message = new Message();
        message.setData(partyTransferService.partyTransferIntoNum(status,getSession())).success();
        return message;
    }

    /**
    * @Description 党员迁移汇总查询
    * @Author zwd
    * @Date 2020/11/27 14:50
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "汇总查询", notes = "汇总查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "orgcode", value = "组织code", required = false, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "username", value = "党员名称", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "transfertype", value = "转接类型", required = false, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "审核状态", required = false, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "startdate", value = "审核时间-开始", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "enddate", value = "审核时间-结束", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "1:转出申请，2:转入申请", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "organid", value = "组织organid", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/orgnum")
    public Message partyTransferOrgNum(Pager pager,String orgcode,String username,Integer transfertype,Integer status,String startdate,String enddate,Integer type,String organid){
        Message message = new Message();
        message.setData(partyTransferService.partyTransferOrg(pager,orgcode,username,transfertype,status,startdate,enddate,type,getSession(),organid)).success();
        return message;
    }



    /**
    * @Description 删除党员迁移
    * @Author zwd
    * @Date 2020/11/28 15:59
    * @return com.lehand.base.common.Message
    **/
    @RequestMapping("/delete")
    @OpenApi(optflag=0)
    @ApiOperation(value = "删除党员迁移", notes = "删除党员迁移", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    public Message deletePartyTransfer(Integer id ){
        Message message = new Message();
        message.setData(partyTransferService.deletePartyTransfer(id,getSession())).success();
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查看介绍信", notes = "查看介绍信", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getPartyLetter")
    public Message<Object> getPartyLetter(String id){
        Message<Object> msg = new Message<Object>();
        if(StringUtils.isEmpty(id)) {
            LehandException.throwException("id不能传空！");
        }
        try {
            return msg.setData(partyTransferPDFService.getPartyLetter(getSession(),id)).success();
        } catch (Exception e) {
            e.printStackTrace();
            return msg.fail("介绍信打印失败，请稍后重试！"+e);
        }
    }
}