package com.lehand.horn.partyorgan.util;

import com.alibaba.druid.util.StringUtils;
import com.lehand.base.common.Organs;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.dto.UrcOrgan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service(value = "partyUtils")
public class SessionUtils {

    @Resource
    protected GeneralSqlComponent generalSqlComponent;

    public DBComponent db() {
        return generalSqlComponent.getDbComponent();
    }

    /**
     * 根据组织机构编码获取组织机构id
     * @param organCode
     * @return
     */
    public  String  getOrganidByOrganCode(String organCode){
        String organid = db().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{organCode});
        return organid;
    }

    /**
     * 根据组织机构编码获取组织机构类别
     * @param organCode
     * @return
     */
    public  String  getOrgTypeByOrganCode(String organCode){
        String zzlb = db().getSingleColumn("select zzlb  from  t_dzz_info where organcode = ?",String.class,new Object[]{organCode});
        return zzlb;
    }

    /**
     * 获取最高组织机构编码
     * @return
     */
    public  String  getRootOrgCode(){
        String organCode = db().getSingleColumn("select organcode from t_dzz_info_simple where organid = path1",String.class,new Object[]{});
        return organCode;
    }

    /**
     * 根据组织机构编码获取 urc组织机构
     * @param organcode
     * @param compid
     * @return
     */
    public UrcOrgan getUrcOrgByOrgancode(String organcode, Long compid){
        UrcOrgan org  = db().getBean("select * from urc_organization where orgcode = ? and compid=?",
                UrcOrgan.class,new Object[]{organcode,compid});
        return org;
    }

    /**
     * 根据urc的orgid获取 urc组织机构
     * @param orgid
     * @param compid
     * @return
     */
    public UrcOrgan getUrcOrgByOrgid(Long orgid,Long compid){
        UrcOrgan org  = db().getBean("select * from urc_organization where orgid = ? and compid = ?",
                UrcOrgan.class,new Object[]{orgid,compid});
        return org;
    }

    /**
     * 获取secondOrgan属性(上级中的二级党委)
     * @param currorgid
     * @param compid
     * @return
     */
    public Organs getSecondOrgan(Long currorgid ,Long compid){
        UrcOrgan organ = db().getBean("select * from urc_organization where orgid = ? and compid=?",UrcOrgan.class, new Object[] { currorgid, compid });

        Organs org = new Organs();
        //业务表查二级党委id
        String  secondOrganid = db().getSingleColumn("select path2 from t_dzz_info_simple where organcode = ? ",String.class,new Object[]{organ.getOrgcode()});
        if(StringUtils.isEmpty(secondOrganid)){
            return null;
        }
        //查出二级党委code到系统表查询二级党委信息
        String secondOrganCode = db().getSingleColumn("select organcode from t_dzz_info_simple where organid = ? ",String.class,new Object[]{secondOrganid});
        UrcOrgan secondOran = db().getBean("select * from urc_organization where orgcode = ? and compid=?",UrcOrgan.class, new Object[] { secondOrganCode, compid });

        if(secondOran == null){
            return null;
        }
        if(!"6100000035".equals(String.valueOf(secondOran.getOrgtypeid()))){
            return null;
        }

        org.setOrgid(secondOran.getOrgid());
        org.setOrgname(secondOran.getOrgname());
        org.setOrgcode(secondOran.getOrgcode());
        org.setOrgfzdyid(secondOrganid);
        return org;
    }


    /**
     * secondOrgan 组装
     * @param org
     * @return
     */
    public Organs makeSecondOrgan(Organs org){
        if(org == null){
            return org;
        }

        if(StringUtils.isEmpty(org.getOrgcode())){
            return org;
        }

        String organid = getOrganidByOrganCode(org.getOrgcode());
        org.setOrgfzdyid(organid);
        org.setOtid(getOrgTypeByOrganCode(org.getOrgcode()));
        return org;
    }
}
