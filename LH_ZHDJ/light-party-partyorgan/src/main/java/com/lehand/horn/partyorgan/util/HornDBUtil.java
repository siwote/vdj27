package com.lehand.horn.partyorgan.util;

import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;

public class HornDBUtil {

	public static DBComponent getHornDB() {
		return GeneralSqlComponent.use("urc");
	}
}
