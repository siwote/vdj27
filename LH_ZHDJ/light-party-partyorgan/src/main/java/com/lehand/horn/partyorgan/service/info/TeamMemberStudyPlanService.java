package com.lehand.horn.partyorgan.service.info;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.pojo.dy.TeamMemberStudyPlan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TeamMemberStudyPlanService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;

    public void add(String name, String files, Long id,Session session) {
        if(id==null){
            generalSqlComponent.getDbComponent().insert(
                    "insert into team_member_study_plan (name,files,compid,createtime,orgcode) values (?,?,?,?,?)",
                    new Object[]{name,files,session.getCompid(),
                            DateEnum.YYYYMMDD.format(),session.getCurrentOrg().getOrgcode()});
        }else{
            generalSqlComponent.getDbComponent().update(
                    "update team_member_study_plan set name=?,files=? where compid=? and id=?",
                    new Object[]{name,files,session.getCompid(),id});
        }
    }

    public void remove(Long id, Session session) {
        generalSqlComponent.getDbComponent().update(
                "delete from team_member_study_plan where compid=? and id=?",
                new Object[]{session.getCompid(),id});
    }

    public TeamMemberStudyPlan getinfo(Long id, Session session) {
        TeamMemberStudyPlan data = generalSqlComponent.getDbComponent().getBean(
                "select * from team_member_study_plan where compid=? and id=?",
                TeamMemberStudyPlan.class,
                new Object[]{session.getCompid(),id}
        );
        if(data==null){
            LehandException.throwException("学习计划不存在");
        }
        data.setListFiles(listFile(data.getFiles()));
        return data;
    }

    public void listPager(Pager pager, String name, String starttime, String endtime, Session session) {
        StringBuffer sql = new StringBuffer();
        sql.append("select * from team_member_study_plan where compid=? and orgcode=?");
        if(!StringUtils.isEmpty(name)){
            sql.append(" and name like '%"+name+"%'");
        }
        if(!StringUtils.isEmpty(starttime)){
            sql.append(" and left(createtime,7)>='"+starttime+"'");
        }
        if(!StringUtils.isEmpty(endtime)){
            sql.append(" and left(createtime,7)<='"+endtime+"'");
        }
        sql.append(" order by createtime desc");
        generalSqlComponent.getDbComponent().pageBean(pager,sql.toString(),TeamMemberStudyPlan.class,
                new Object[]{session.getCompid(),session.getCurrentOrg().getOrgcode().substring(1)});
        List<TeamMemberStudyPlan> lists = (List<TeamMemberStudyPlan>) pager.getRows();
        lists.forEach(a->{
            a.setListFiles(listFile(a.getFiles()));
        });
    }

    /**
     * 获取附件list集合
     * @param fileids
     * @return
     */
    private List<Map<String, Object>> listFile(String fileids) {
        List<Map<String,Object>> listFile = new ArrayList<>();
        if(!StringUtils.isEmpty(fileids)){
            String[] fileid = fileids.split(",");
            for (String id:fileid) {
                Map<String,Object> m =  generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",1);put("id",id);}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    m.put("fileid",id);
                    m.put("flpath", String.format("%s%s", downIp, dir));
                    m.put("name",m.get("name").toString().substring(0,m.get("name").toString().lastIndexOf(".")));
                    listFile.add(m);
                }
            }
        }
        return  listFile;
    }
}
