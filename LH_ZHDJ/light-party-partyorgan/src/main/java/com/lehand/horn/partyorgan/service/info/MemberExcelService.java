package com.lehand.horn.partyorgan.service.info;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.MemberSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dto.*;
import com.lehand.horn.partyorgan.pojo.SysUser;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
import com.lehand.horn.partyorgan.util.ExcelUtils;
import com.lehand.horn.partyorgan.util.OrgUtil;
import com.lehand.horn.partyorgan.util.SysUtil;
import org.springframework.stereotype.Service;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 导入导出Service
 * @author admin
 *
 */
@Service
public class MemberExcelService {
    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    MemberService memberService;
    @Resource
    PartyCacheComponent partyCacheComponent;
    @Resource
    private DBComponent dBComponent;

    @Resource private OrgUtil orgUtil;
    /**
     * 导出党员信息 数据
     * @param req
     * @param session
     * @return
     */
    public String memberInfoExport(HttpServletResponse response, MemberExportReq req, Session session,String organid,String xm ,String zjhm) {

        List<ExportColumnReq> columns  = req.getExportColumns();

        if(req.getExportColumns().size()<1){
            return  null;
        }
        Pager pager = new Pager();
        pager.setPageNo(1);
        pager.setPageSize(9999);

        //党员信息数据
        List<MemberInfoDto> list = new ArrayList<MemberInfoDto>();

        //校验是否带模糊查询条件(待条件就全局查询)
        if( !StringUtils.isEmpty(xm)|| !StringUtils.isEmpty(zjhm)){
            MemberSearchReq reqBean =new  MemberSearchReq();
            String path = orgUtil.getPath(req.getOrganid());
            reqBean.setPath(path);
            reqBean.setOrganid(organid);
            reqBean.setXm(xm);
            reqBean.setZjhm(zjhm);
            generalSqlComponent.pageQuery(MemberSqlCode.member_page2,req,pager);
            list=(List<MemberInfoDto>)pager.getRows();
            list = memberService.makeList(list);
        }else{
                list = memberService.getMemberInfoList(req,pager);
        }

        if(list.size()<1){
            return null;
        }

        //表头
//		String[] tablesStr = new String[] {};
        List<String> tables = new ArrayList<String>();
        for (ExportColumnReq col: columns
        ) {
            tables.add(col.getTitle());
        }
        String[] tablesStr = tables.toArray(new String[tables.size()]);

        //表名
//        String tableName = String.format("%s党员信息", list.get(0).getDzzmc());
        String tableName = "导出党员信息";
        //内容
        List<Map<String,String>> mapList = getMemberExportInfo(list,columns);
        ExcelUtils.write(response,tablesStr,mapList,tableName);
        return null;
    }

    /**
     * 处理自定义列excel数据
     * @param list
     * @param columns
     * @return
     */
    private List<Map<String, String>> getMemberExportInfo(List<MemberInfoDto> list, List<ExportColumnReq> columns) {
        List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
        for ( MemberInfoDto  info: list) {
            Map<String, String> tempMap = new HashMap<>();
            for (ExportColumnReq col: columns) {
                tempMap.put(col.getTitle(),getInfo(col.getCode(), SysUtil.transBean2Map(info)));
            }
            explist.add(tempMap);
        }
        return explist;
    }

    /**
     * 通过列编码取值
     * @param code
     * @param info
     * @return
     */
    public String getInfo(String code, Map<String, Object> info) {
        String inforStr = StringUtils.isEmpty(info.get(code))?MagicConstant.STR.EMPTY_STR:String.valueOf(info.get(code));
        return "null".equals(inforStr)?MagicConstant.STR.EMPTY_STR:inforStr;
    }

    /**
     * 党员统计信息导出
     * @param response
     * @param req
     * @param session
     * @return
     */
    public Object memberAnalysisInfoExport(HttpServletResponse response, MemberExportReq req, Session session) {

        List<ExportColumnReq> columns  = req.getExportColumns();

        if(req.getExportColumns().size()<1){
            return  null;
        }
        Integer pathNum  = memberService.getPathNum(req.getOrganid());
        if(pathNum<1){
            return  null;
        }
        String path = MagicConstant.STR.PATH_STR+pathNum;
        Map<String,Object> param = new HashMap<String,Object>(3);
        param.put("organid",req.getOrganid());
        param.put("organidx",req.getOrganid());
        param.put("path",path);

        //当前单位统计
        MemberAnalysisDto currAnalysis = generalSqlComponent.query(MemberSqlCode.get_current_analysis,param);
        //直属单位统计
        String childPath = MagicConstant.STR.PATH_STR +(pathNum+1);
        MemberSearchReq childreq = new MemberSearchReq();
        childreq.setPath(childPath);
        childreq.setOrganid(req.getOrganid());
//        List<MemberAnalysisDto> list = generalSqlComponent.query(MemberSqlCode.member_analysis_list,req);

        List<MemberAnalysisDto> list = getchildList(childreq);
        //将当前单位插入数组第一条
        list.add(0,currAnalysis);
        if(list.size()<1){
            return null;
        }

        //表头
//		String[] tablesStr = new String[] {};
        List<String> tables = new ArrayList<String>();
        for (ExportColumnReq col: columns
        ) {
            tables.add(col.getTitle());
        }
        String[] tablesStr = tables.toArray(new String[tables.size()]);

        //表名
        String tableName = String.format("%s党员信息", currAnalysis.getOrganname());

        //内容
        List<Map<String,String>> mapList = getMemberAnalysisExportInfo(list,columns);

        ExcelUtils.write(response,tablesStr,mapList,tableName);

        return null;
    }

    /**
     * 获取下一级党组织统计数据
     * @param childreq
     * @return
     */
    private List<MemberAnalysisDto> getchildList(MemberSearchReq childreq) {
        List<MemberAnalysisDto> list = new ArrayList<MemberAnalysisDto>();

        List<String> orgids = generalSqlComponent.query(SqlCode.getOrgidsByPid,new Object[]{childreq.getOrganid()});
        //查询下一级统计总数
        for (String orgid: orgids) {
            Map<String,Object> params = new HashMap<String,Object>(3);
            params.put("organidx",orgid);
            params.put("organid",orgid);
            params.put("path",childreq.getPath());
            MemberAnalysisDto childAnalysis = generalSqlComponent.query(MemberSqlCode.get_current_analysis,params);
            list.add(childAnalysis);
        }
        return list;
    }

    /**
     * 党员统计信息导出
     * @param list
     * @param columns
     * @return
     */
    private List<Map<String, String>> getMemberAnalysisExportInfo(List<MemberAnalysisDto> list, List<ExportColumnReq> columns) {
        List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
        for ( MemberAnalysisDto  info: list) {
            Map<String, String> tempMap = new HashMap<>();
            for (ExportColumnReq col: columns) {
                tempMap.put(col.getTitle(),getInfo(col.getCode(), SysUtil.transBean2Map(info)));
            }
            explist.add(tempMap);
        }
        return explist;
    }

    /**
     * 导入党员信息
     * @param file
     * @param session
     * @return
     */
    @Transactional
    public String memberInfoImport(MultipartFile file, Session session, String organids,Integer type) {
        try {
            InputStream inputStream = file.getInputStream();
            List<List<String>> list = ExcelUtils.readExcelNew(file.getOriginalFilename(), inputStream);
            if ( null != list && list.size()<=0){
                LehandException.throwException("无数据");
            }
            List<String> listNew = new ArrayList<>();
            list.forEach(a->{
                listNew.add(a.get(1));
            });
            Set<String> setNew = new HashSet<>(listNew);
            if(listNew.size()> setNew.size()){
                LehandException.throwException("党员身份证号码重复");
            }
            if(type==1){//不是增量导入党员
                //校验是否有党组织被删除了或者被合并了
                List<String> listOld =  dBComponent.listSingleColumn("select a.zjhm from t_dy_info  a left join t_dzz_info_simple b on a.organid=b.organid where b.id_path like '%"+organids+"%' and b.operatetype != 3 and b.zzlb != '3100000025' and b.zzlb != '3900000026'",String.class,new Object[]{});
                //generalSqlComponent.query(SqlCode.listTDzzInfos2,new Object[]{"'%"+organids+"%'"});
                String sfzhs = "";
                for(String o :listOld){
                    if(!listNew.contains(o)){
                        //不包含将党员信息置为无效状态使用字段 zfbz=1
                        sfzhs += "'"+o+"'"+",";
                    }
                }
                sfzhs = !StringUtils.isEmpty(sfzhs) ? sfzhs.substring(0,sfzhs.length()-1) : sfzhs;
                Map params = new HashMap();
                params.put("sfzh",sfzhs);
                if(!StringUtils.isEmpty(sfzhs)){
                    //将 t_dy_info 置为作废
                    dBComponent.update("update t_dy_info set delflag=1 where zjhm in ("+sfzhs+")",new Object[]{});
                    //urc_user_account 删除账号
                    dBComponent.update("update urc_user set status=0 where idno in ("+sfzhs+")",new Object[]{});
                    //dBComponent.delete("delete from urc_user_account where account in ("+sfzhs+")",new Object[]{});
                    dBComponent.update("update sys_user set status=0 where acc in ("+sfzhs+")",new Object[]{});
                    dBComponent.update("update t_dzz_bzcyxx set zfbz=1 where zjhm in ("+sfzhs+")",new Object[]{});
                }
            }

            int total = list.size();

            int  insertnum = 0;

            int  updatenum = 0;
            StringBuffer buffer = new StringBuffer("导入结果: ");
            buffer.append("\r\n");
            for (int i = 0; i < total ; i++) {
                int j = 0;
                String orgname = null;
                try {
                    orgname = String.valueOf(list.get(i).get(20));
                } catch (Exception e) {
                    LehandException.throwException("党员组织获取错误");
                }
                //根据当前organids获取党组织类别
                String partyOrgType = generalSqlComponent.query(SqlCode.getDzzlbByOrganIds, new Object[]{organids});
                //6100000035代表党委,6200000036代表总支,6300000037代表支部
                if(!StringUtils.isEmpty(partyOrgType) && partyOrgType.equals("6300000037") && !StringUtils.isEmpty(organids)){
                    String Dzzmc = generalSqlComponent.query(SqlCode.getDzzmcByOrganids,new Object[]{organids});
                    if(!StringUtils.isEmpty(orgname) && !Dzzmc.equals(orgname)){
                        LehandException.throwException("党员组织名称错误");
                    }
                }
                Map map = generalSqlComponent.query(SqlCode.getTDzzInfo,new Object[]{orgname});
                if(map==null){
                    LehandException.throwException("党员组织不存在或党员组织名称匹配错误！");
                }
                String orgcode = map.get("organcode").toString();
                String idcard  = list.get(i).get(1);
                String xm = list.get(i).get(0);
                String csrq = list.get(i).get(3);

                //校验导入数据
                boolean reg = getValidate(i+1,orgcode,idcard,xm,csrq,buffer);

                if(!reg){
                    continue;
                }

                //获取组织机构id,组织机构不存在则不执行导入
                String organid = map.get("organid").toString();
                        //getOrganidByCode(orgcode);
                if(StringUtils.isEmpty(organid)){
                    buffer.append("第"+i+"条数据组织机构数据不存在;");
                    buffer.append("\r\n");
                    continue;
                }

                //判断党员信息是否存在,执行新增或修改操作
                String userid = getUseridByIdcard(idcard);
                if(StringUtils.isEmpty(userid)){//插入
                    userid = insertMemberInfo(list.get(i),organid);
                    insertnum += 1;
                }else{
                    j = updateMemberInfo(list.get(i),userid,organid);
                    updatenum += 1;
                }

                //创建账号必须
                Map<String,Object> orgInfo = generalSqlComponent.query(SqlCode.queryPorgcode ,new HashMap<String,Object>(){{
                    put("organcode",orgcode);
                }});

                if(orgInfo == null){
                    buffer.append("第"+(i-1)+"条数据组织机构数据在管理系统中不存在,无法正确创建账号;");
                    buffer.append("\r\n");
                    continue;
                }
                //更新组织用户和账号
                int k = updateUserAccount(list.get(i),orgInfo.get("orgid"),session,userid,organid,orgcode);
            }
            buffer.append("共"+(total)+"条数据,共导入成功"+(insertnum+updatenum)+"条数据;");
            return buffer.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean getValidate(int i, String orgcode, String idcard, String xm, String csrq,StringBuffer buffer) {

        //判断导入的姓名是否为空
        if(StringUtils.isEmpty(xm)){
            buffer.append("第"+i+"条数据姓名为空;");
            buffer.append("\r\n");
            return false;
        }

        //判断导入的orgcode是否为空
//        if(StringUtils.isEmpty(orgcode)){
//            buffer.append("第"+i+"条数据组织机构编码为空;");
//            buffer.append("\r\n");
//            return false;
//        }

        //判断导入的出生日期格式是否正确yyyy-MM-dd
        if(!StringUtils.isEmpty(csrq)) {
            String regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
            Pattern pattern = Pattern.compile(regex);
            Matcher m = pattern.matcher(csrq);
            boolean dateFlag = m.matches();
            if (!dateFlag) {
               LehandException.throwException("出生日期格式错误,请检查后重新导入!");
            }
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setLenient(false);
            try {
                Date date = formatter.parse(csrq);
                System.out.println(date);
                System.out.print("格式正确!");
            } catch (Exception e) {
                LehandException.throwException("出生日期格式错误,请检查后重新导入!");
            }
        }


        //判断导入的idcard是否为空
        if(StringUtils.isEmpty(idcard)){
            buffer.append("第"+i+"条数据身份证号码为空;");
            buffer.append("\r\n");
            return false;
        }

        //判断身份证格式
        if(!SysUtil.isIDNumber(idcard)){
            buffer.append("第"+i+"条数据身份证号码有误;");
            buffer.append("\r\n");
            return false;
        }

        return true;
    }

    /**
     * 更新用户和账号
     * @param data
     * @param orgid
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public int updateUserAccount(List<String> data, Object orgid, Session session,String userId,String organid,String orgcode) {
        String idno = data.get(1);
        String postname = "";//导入excel中暂时不知道党员的身份，所以这里暂时全部默认为普通党员
        String sex = "男".equals(data.get(2)) ? "1" : "0";
        String rolecode = null;
        String postnames = "";
        int ordernum = 0;
        if("5100000017".equals(postname)){
            postnames = "1";//书记
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_SJ_ROLE;
            ordernum = 0;
        }else if("5100000018".equals(postname)){
            postnames = "2";//副书记
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_FSJ_ROLE;
            ordernum = 1;
        }else if("5300000022".equals(postname)){
            postnames = "3";//委员
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_WY_ROLE;
            ordernum = 2;
        }else{
            postnames = "0";//普通党员
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_ROLE;
            ordernum = 3;
        }

        Integer num = generalSqlComponent.query(SqlCode.countUrcuserByIdno,new Object[]{idno, SysConstants.IDTYPE.ID_CARD});
        if(num>0){//修改
            return updateUser(idno,orgid,sex,data.get(0),data.get(18),session.getCompid());
        }
        //添加党员信息
        Long userid = addUrcUser(data, session, idno, postnames, sex);
        //添加班子成员标信息
        if(!"0".equals(postnames)){
            Map map = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_sexid",data.get(2)});
            addBzcyxx(data, userId, organid, idno, postname, map==null ? "" : map.get("item_code").toString(), ordernum);
        }
        //添加组织关系
        addUserRelation(userid,orgid,session.getCompid(),rolecode);
        //添加账号
        return generalSqlComponent.insert(SqlCode.addUserAccount2,new Object[]{userid,idno, SysConstants.DEFAULTPWD.SIMPLESIX,session.getCompid()});
    }

    /**
     * 添加党员信息
     * @param data
     * @param session
     * @param idno
     * @param postnames
     * @param sex
     * @return
     */
    private Long addUrcUser(List<String> data, Session session, String idno, String postnames, String sex) {
        Map<String,Object> param = new HashMap<String, Object>(6);
        param.put("username",data.get(0));
        param.put("relationid", MagicConstant.STR.EMPTY_STR);
        param.put("avatar",MagicConstant.STR.EMPTY_STR);
        param.put("accflag",0);
        param.put("usertype",0);
        param.put("sex",sex);
        param.put("phone",data.get(18));
        param.put("landlines",MagicConstant.STR.EMPTY_STR);
        param.put("email",MagicConstant.STR.EMPTY_STR);
        param.put("idno",idno);
        param.put("compid",session.getCompid());
        param.put("idtype", 0);
        param.put("postname", postnames);
        param.put("useraddr", MagicConstant.STR.EMPTY_STR);
        param.put("userlable", MagicConstant.STR.EMPTY_STR);
        param.put("seqno", MagicConstant.NUM_STR.NUM_2);
        param.put("createuserid",session.getUserid());
        param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
        param.put("updateuserid", session.getUserid());
        param.put("updatetime", DateEnum.YYYYMMDDHHMMDD.format());
        return generalSqlComponent.insert(SqlCode.insertUrcUser,param);
    }

    /**
     * 添加班子成员
     * @param data
     * @param userId
     * @param organid
     * @param idno
     * @param postname
     * @param sex
     * @param ordernum
     */
    private void addBzcyxx(List<String> data, String userId, String organid, String idno, String postname, String sex, int ordernum) {
        Map<String,Object> param2 = new HashMap<>();
        param2.put("uuid", UUID.randomUUID().toString().replaceAll("-", ""));
        param2.put("userid", userId);
        param2.put("organid", organid);
        param2.put("organname", data.get(20)/*generalSqlComponent.query(SqlCode.queryTDzzBzcyxx,new Object[]{organid})*/);
        param2.put("ordernum", ordernum);
        param2.put("username", data.get(0));
        param2.put("zjhm", idno);
        param2.put("xb", sex);
        param2.put("csrq", data.get(3));
        if(data.get(19).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(19));
            param2.put("lxdh",bd.toPlainString());
        }else{
            param2.put("lxdh", data.get(19));
        }
        Map map = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_xl",data.get(7)});
        param2.put("xl",map==null ? "" : map.get("item_code").toString());
        param2.put("dnzwmc", ""/*partyCacheComponent.getName("d_dy_dnzw",postname)*/);
        param2.put("dnzwname", postname);
        param2.put("dnzwsm", "");
        param2.put("rzdate", DateEnum.YYYYMMDDHHMMDD.format());
        param2.put("sfzr", 1);
        param2.put("syncstatus", "A");
        param2.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
        param2.put("zfbz", 0);
        generalSqlComponent.delete(SqlCode.delTDzzBzcyxx,new Object[]{userId,organid});
        generalSqlComponent.insert(SqlCode.addTDzzBzcyxx,param2);
    }

    /**
     * 添加关联关系
     * @param userid
     * @param orgid
     * @param compid
     * @return
     */
    public int addUserRelation(Object userid, Object orgid, Long compid,String roletype) {
        //插入组织机构关系
        generalSqlComponent.insert(SqlCode.addUserOrgMap2, new HashMap<String, Object>() {
            {
                put("userid", userid);
                put("orgid", orgid);
                put("seqno",MagicConstant.NUM_STR.NUM_0);
                put("compid",compid);
            }
        });
        //插入角色关系
        Map map =generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{compid,roletype});
        try {
            generalSqlComponent.insert(SqlCode.addRoleMap2,new HashMap<String, Object>() {
                {
                    put("sjtype", MagicConstant.NUM_STR.NUM_0);
                    put("sjid", userid);
                    put("roleid", map.get("roleid"));
                    put("orgid",orgid);
                    put("compid",compid);
                }
            });
        } catch (Exception e) {
            System.out.println("用户已存在当前角色");
        }

        return 0;
    }

    /**
     * 删除关联关系
     * @param userid
     * @param compid
     * @return
     */
    public int deleteUserRelation(Object userid, Long compid) {
        int del = generalSqlComponent.delete(SqlCode.removeUserOrgMapByUserid,new Object[]{userid,compid});
        int delrole = generalSqlComponent.delete(SqlCode.removeRoleMapBySubject,new Object[]{0,userid,compid});

        return 0;
    }

    /**
     * 修改账户
     * @param username
     * @param phone
     * @param idno
     * @param orgid
     * @param sex
     * @param compid
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public int updateUser(String idno, Object orgid, String sex, String username, String phone, Long compid) {

        Map<String,Object> map = generalSqlComponent.query(SqlCode.queryUrcUserByIdno,new Object[]{idno});



        int del =  deleteUserRelation(map.get("userid"),compid);

        int ins = addUserRelation(map.get("userid"),orgid,compid,SysConstants.DEFAULTROLEID.PARTY_MEMBER_ROLE);
        return generalSqlComponent.update(SqlCode.updateUrcuserByIdno,new Object[]{username,sex,phone,compid,idno});
    }



    /**
     * 性别数据转换
     * @param s
     * @return
     */
    private String makeSex(String s) {
        if(SysConstants.DYXB.MALE.equals(s)){
            return MagicConstant.NUM_STR.NUM_1;
        }
        if(SysConstants.DYXB.FEMALE.equals(s)){
            return MagicConstant.NUM_STR.NUM_2;
        }
        return MagicConstant.NUM_STR.NUM_0;
    }

    /**
     * 修改党员信息
     * @param data
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public int updateMemberInfo(List<String> data,String userid,String organid) {

        TDyInfo info = new TDyInfo();
        info.setUserid(userid);
        info.setDelflag(MagicConstant.NUM_STR.NUM_0);
        Map map = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_sexid",data.get(2)});
        info.setXb(map==null ? "" : map.get("item_code").toString());
        info.setAge(data.get(6));
        info.setXm(data.get(0));
        info.setCsrq(data.get(3));
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_national",data.get(4)});
        info.setMz(map1==null ? "" : map1.get("item_code").toString());
        info.setJg(data.get(5));
        Map map2 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_xl",data.get(7)});
        info.setXl(map2==null ? "" : map2.get("item_code").toString());
        info.setRdsj(data.get(11));
        info.setZzsj(data.get(12));
        Map map3 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_gzgw",data.get(13)});
        info.setGzgw(map3==null ? "" : map3.get("item_code").toString());
        Map map4 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_rylb",data.get(16)});
        info.setDylb(map4==null ? "" : map4.get("item_code").toString());
        info.setIsworkers("否".equals(data.get(17)) ? "0" : "1");
        if(data.get(18).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(18));
            info.setLxdh(bd.toPlainString());
        }else{
            info.setLxdh(data.get(18));
        }
        boolean zjhm = data.get(1).contains("'");
        if(!zjhm){
            info.setZjhm(data.get(1));
        }else{
            info.setZjhm(data.get(1).substring(1,data.get(1).length()-1));
        }
        info.setDyzt(SysConstants.DYZT.ZC);
        info.setOrganid(organid);
        info.setOperatetype(MagicConstant.NUM_STR.NUM_0);
        info.setSyncstatus("E");
        //学位
        Map map5 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_xw",data.get(8)});
        info.setXwDm(map5==null ? "" : map5.get("item_code").toString());
        info.setByyx(data.get(9));
        info.setZy(data.get(10));
        //从事专业技术职务
        Map map6= generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_jszc",data.get(14)});
        info.setCszyjszwDm(map6==null ? "" : map6.get("item_code").toString());
        //新社会阶层类型
        Map map7 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_shjc",data.get(15)});
        info.setXshjclxDm(map7==null ? "" : map7.get("item_code").toString());
        info.setGddh(data.get(19));
        info.setLhzbdwmc(data.get(21));
        info.setDydaszdw(data.get(22));
        info.setHjszd(data.get(23));
        info.setXjzd(data.get(24));
        info.setLostreason(data.get(25));
        info.setLosttime(data.get(26));
        info.setLosttime(data.get(26));
        info.setXxwzd(data.get(27));
        int result = generalSqlComponent.update(MemberSqlCode.update_t_dy_info,info);
        updateSysuser(data,organid,userid);
        return result;
    }

    private void updateSysuser(List<String> data,String organid,String key){
        SysUser user = new SysUser();
        user.setRoles("role_party_member");
        user.setAcc(data.get(1));
        user.setName(data.get(0));
        user.setOrgId(organid);
        user.setOrgName(data.get(20));
        user.setWorkPhone(data.get(18));
        user.setBaseInfoId(key);
        generalSqlComponent.update(SqlCode.modiSysuser,user);
    }
    /**
     * 新增党员信息
     * @param data
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public String insertMemberInfo(List<String> data,String organid) {
        //党员表主键
        String key = UUID.randomUUID().toString().replace("-","");
        //新增党员信息
        TDyInfoReq info = new TDyInfoReq();
        info.setUserid(key);
        info.setDelflag(MagicConstant.NUM_STR.NUM_0);
        Map map = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_sexid",data.get(2)});
        info.setXb(map==null ? "" : map.get("item_code").toString());
        info.setXm(data.get(0));
        info.setCsrq(data.get(3));
        //判断民族是否包含'如果包含就取后面的字符串
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_national",data.get(4)});
        info.setMz(map1==null ? "" : map1.get("item_code").toString());
        info.setJg(data.get(5));
        Map map2 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_xl",data.get(7)});
        info.setXl(map2==null ? "" : map2.get("item_code").toString());
        //学位
        Map map3 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_xw",data.get(8)});
        info.setXwDm(map3==null ? "" : map3.get("item_code").toString());
        info.setByyx(data.get(9));
        info.setZy(data.get(10));
        info.setRdsj(data.get(11));
        info.setZzsj(data.get(12));
        //工作岗位
        Map map4 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_gzgw",data.get(13)});
        info.setGzgw(map4==null ? "" : map4.get("item_code").toString());
        //从事专业技术职务
        Map map5= generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_jszc",data.get(14)});
        info.setCszyjszwDm(map5==null ? "" : map5.get("item_code").toString());
        //新社会阶层类型
        Map map6 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_shjc",data.get(15)});
        info.setXshjclxDm(map6==null ? "" : map6.get("item_code").toString());
        //人员类别
        Map map7 = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_dy_rylb",data.get(16)});
        info.setDylb(map7==null ? "" : map7.get("item_code").toString());
        info.setIsworkers("否".equals(data.get(17)) ? "0" : "1");
        if(data.get(18).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(18));
            info.setLxdh(bd.toPlainString());
        }else{
            info.setLxdh(data.get(18));
        }
        info.setGddh(data.get(19));
        info.setLhzbdwmc(data.get(21));
        info.setDydaszdw(data.get(22));
        info.setHjszd(data.get(23));
        info.setXjzd(data.get(24));
        info.setLostreason(data.get(25));
        info.setLosttime(data.get(26));
        info.setLosttime(data.get(26));
        info.setXxwzd(data.get(27));
        info.setOrganid(organid);
        boolean zjhm = data.get(1).contains("'");
        if(!zjhm){
            info.setZjhm(data.get(1));
        }else{
            info.setZjhm(data.get(1).substring(1,data.get(1).length()-1));
        }
        info.setDyzt(SysConstants.DYZT.ZC);
        info.setOperatetype(MagicConstant.NUM_STR.NUM_0);
        info.setSyncstatus("E");
        //是否困难党员
        info.setPovertyParty("0");
        //是否党员示范岗位
        info.setDemonstrationParty("0");
        //通过身份证号查询党员是否存在如果存在就更新并将删除标识更新为0 delflag=0
        int result = generalSqlComponent.insert(MemberSqlCode.insert_t_dy_info,info);
        //在play项目人员表中创建数据
        insertSysuser(data,organid,key);
        return key;
    }
    @Transactional(rollbackFor=Exception.class)
    public void insertSysuser(List<String> data, String organid, String key){
        SysUser user = new SysUser();
        user.setId(UUID.randomUUID().toString().replace("-",""));
        user.setPwd("0ffe6471a68f774d03695b1e536c2da5");
        user.setSalt("448d7c19946648e5aad3b9b7b3f7a39a");
        user.setRoles("role_party_member");
        user.setAcc(data.get(1));
        user.setName(data.get(0));
        user.setOrgId(organid);
        user.setOrgName(data.get(20));
        user.setWorkPhone(data.get(18));
        user.setStatus(1);
        user.setBaseInfoId(key);
        user.setCreateTime(DateEnum.now());
        user.setModiTime(DateEnum.now());
        user.setCreater("1");
        user.setModifier("1");
        generalSqlComponent.insert(SqlCode.addSysuser,user);
    }


    /**
     * 根据党员身份证号获取党员id
     * @param idcard
     * @return
     */
    private String getUseridByIdcard(String idcard) {
        String userid = generalSqlComponent.query(MemberSqlCode.get_userid_by_idcard,new Object[]{idcard});
        return userid;
    }

    /**
     * 根据党组织编码获取组织机构id
     * @param orgcode
     * @return
     */
    private String getOrganidByCode(String orgcode) {
        String organid = generalSqlComponent.query(MemberSqlCode.get_organid_by_orgcode,new Object[]{orgcode});
        return organid;
    }

    /**
     * 更新用户和账号
     * @param data
     * @param orgid
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public int updateUserAccount2(List<String> data, Object orgid, Session session,String userId,String organid,String orgcode) {
        String idno = data.get(1);
        String postname = "";//导入excel中暂时不知道党员的身份，所以这里暂时全部默认为普通党员
        String sex = "男".equals(data.get(2)) ? "1" : "0";
        String rolecode = null;
        String postnames = "";
        int ordernum = 0;
        if("5100000017".equals(postname)){
            postnames = "1";//书记
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_SJ_ROLE;
            ordernum = 0;
        }else if("5100000018".equals(postname)){
            postnames = "2";//副书记
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_FSJ_ROLE;
            ordernum = 1;
        }else if("5300000022".equals(postname)){
            postnames = "3";//委员
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_WY_ROLE;
            ordernum = 2;
        }else{
            postnames = "0";//普通党员
            rolecode = SysConstants.DEFAULTROLEID.PARTY_MEMBER_ROLE;
            ordernum = 3;
        }

        Integer num = generalSqlComponent.query(SqlCode.countUrcuserByIdno,new Object[]{idno, SysConstants.IDTYPE.ID_CARD});
        if(num>0){//修改
            return updateUser(idno,orgid,sex,data.get(0),data.get(14),session.getCompid());
        }
        //添加党员信息
        Long userid = addUrcUser2(data, session, idno, postnames, sex);
        //添加班子成员标信息
        if(!"0".equals(postnames)){
            Map map = generalSqlComponent.query(SqlCode.getZzlbCodeAll,new Object[]{"d_sexid",data.get(2)});
            addBzcyxx(data, userId, organid, idno, postname, map==null ? "" : map.get("item_code").toString(), ordernum);
        }
        //添加组织关系
        addUserRelation(userid,orgid,session.getCompid(),rolecode);
        //添加账号
        generalSqlComponent.delete(SqlCode.delUaccountByAccount, new Object[] {idno});
        return generalSqlComponent.insert(SqlCode.addUserAccount2,new Object[]{userid,idno, SysConstants.DEFAULTPWD.SIMPLESIX,session.getCompid()});
    }
    /**
     * 添加党员信息
     * @param data
     * @param session
     * @param idno
     * @param postnames
     * @param sex
     * @return
     */
    private Long addUrcUser2(List<String> data, Session session, String idno, String postnames, String sex) {
        Map<String,Object> param = new HashMap<String, Object>(6);
        param.put("username",data.get(0));
        param.put("relationid", MagicConstant.STR.EMPTY_STR);
        param.put("avatar",MagicConstant.STR.EMPTY_STR);
        param.put("accflag",0);
        param.put("usertype",0);
        param.put("sex",sex);
        param.put("phone",data.get(14));
        param.put("landlines",MagicConstant.STR.EMPTY_STR);
        param.put("email",MagicConstant.STR.EMPTY_STR);
        param.put("idno",idno);
        param.put("compid",session.getCompid());
        param.put("idtype", 0);
        param.put("postname", postnames);
        param.put("useraddr", MagicConstant.STR.EMPTY_STR);
        param.put("userlable", MagicConstant.STR.EMPTY_STR);
        param.put("seqno", MagicConstant.NUM_STR.NUM_2);
        param.put("createuserid",session.getUserid());
        param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
        param.put("updateuserid", session.getUserid());
        param.put("updatetime", DateEnum.YYYYMMDDHHMMDD.format());
        return generalSqlComponent.insert(SqlCode.insertUrcUser,param);
    }
}
