package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.TDyJcxx;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

/**
 * @program: huaikuang-dj
 * @description: TDyJcxxReq
 * @author: zwd
 * @create: 2020-12-04 16:49
 */
public class TDyJcxxReq extends TDyJcxx {

    /**
     * 原始文件集合
     */
    @ApiModelProperty(value="原始文件集合",name="fileList")
    private List<Map<String,Object>> fileList;

    public List<Map<String, Object>> getFileList() {
        return fileList;
    }

    public void setFileList(List<Map<String, Object>> fileList) {
        this.fileList = fileList;
    }
}