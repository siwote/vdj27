//package com.lehand.horn.partyorgan.dao;
//
//import java.util.List;
//import org.springframework.stereotype.Repository;
//
//import com.lehand.horn.partyorgan.pojo.TreeNode;
//import com.lehand.horn.partyorgan.pojo.dict.GDictItem;
//
///**
// * 参数字典DAO
// * @author taogang
// * @version 1.0
// * @date 2019年1月21日, 下午7:44:12
// */
//@Repository
//public class GdictItemDao extends ComBaseDao<GDictItem> {
//	
//	/** 查询sql .*/
//	private static final String QUERY_LIST = "select item_code as value, concat(item_code,' ',item_name) as lable,parent_code as pid from g_dict_item where code = ? order by item_order asc";
//	
//	private static final String QUERY = "select * from g_dict_item where code = ? and item_code = ? order by item_order asc";
//
//	private static final String COUNT_BY_PARENTCODE = "select count(1) from g_dict_item where code = ? and parent_code = ?";
//	
//	/** 获取根节点数据 .*/
//	private static final String ROOT_BY_CODE = "select * from g_dict_item where code = ? and parent_code = -1 order by item_order asc";
//	/** 获取子节点数据 .*/
//	private static final String CHILDREN_BY_PARENTCODE = "select * from g_dict_item where code = ? and parent_code = ? order by item_order asc";
//	
//	/**
//	 * 查询参数字典返回数组
//	 * @param code
//	 * @param itemCode
//	 * @return
//	 * @author taogang
//	 */
//	public List<TreeNode> listItem(String code){
//		return super.list2bean(QUERY_LIST,TreeNode.class,code);
//	}
//	
//	/**
//	 * 获取根节点数据
//	 * @param code
//	 * @return
//	 * @author taogang
//	 */
//	public List<GDictItem> getRootList(String code){
//		return super.list2bean(ROOT_BY_CODE, GDictItem.class, code);
//	}
//	
//	/**
//	 * 查询子节点数据
//	 * @param code
//	 * @param parentCode
//	 * @return
//	 * @author taogang
//	 */
//	public List<GDictItem> getChildrenList(String code,String parentCode){
//		return super.list2bean(CHILDREN_BY_PARENTCODE, GDictItem.class, code,parentCode);
//	}
//	
//	/**
//	 * 判断下一节点数量
//	 * @param parentCode
//	 * @return
//	 * @author taogang
//	 */
//	public int count_sub(String code,String parentCode) {
//		return super.getInt(COUNT_BY_PARENTCODE, code,parentCode);
//	}
//	
//	/**
//	 * 查询参数字典返回对象
//	 * @param code
//	 * @param itemCode
//	 * @return
//	 * @author taogang
//	 */
//	public GDictItem getItem(String code,String itemCode) {
//		return super.get(QUERY, GDictItem.class, code,itemCode);
//	}
//	
//}
