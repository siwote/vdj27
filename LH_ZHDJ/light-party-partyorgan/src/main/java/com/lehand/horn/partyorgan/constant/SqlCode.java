package com.lehand.horn.partyorgan.constant;

public class SqlCode {
    /********************************  党员/党组织基本信息统计 ************************************/

    /**
     * 根据组织机构id 获取信息 树
     * select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryBaseDzzTree = "queryBaseDzzTree";

    public static final String queryBaseDzzTree2 = "queryBaseDzzTree2";

    /**
     * 通过编码获取组织机构简称
     * select remarks6,orgname from pw_organ where compid = ? and orgcode = ?
     */
    public static final String findJCByCode = "findJCByCode";

    /**
     * 班子成员数量
     * select count(*) from t_dzz_bzcyxx a inner join  (select max(rzdate) rzdate,userid ,organid from t_dzz_bzcyxx where  organid = ? group by userid) b
     * on a.rzdate = b.rzdate and a.userid = b.userid and a.organid = b.organid where  a.zfbz = ?
     */
//    public static final String queryHornBZCYCount = "queryHornBZCYCount";
    public static final String queryHornBZCYCount = "select count(*) from t_dzz_bzcyxx a inner join  (select max(rzdate) rzdate,userid ,organid from t_dzz_bzcyxx where  organid = ? group by userid) b on a.rzdate = b.rzdate and a.userid = b.userid and a.organid = b.organid where  a.zfbz = ?";

    /**
     * select b.status,count(1) as value from t_dzz_info_simple a inner join party_dues b on a.organid = b.org_id
     * where a.operatetype != 3 and a.? = ? and year = ? and month = ? group by b.status
     */
    public static final String queryDFInfo = "queryDFInfo";


    /***********************  组织机构补充表业务层  ************************/

    /**
     * select * from t_dzz_info_bc where organid = ?
     */
    public static final String getDzzsjById = "getDzzsjById";

    /**
     * 根据编码获取组织机构信息
     * select * from t_dzz_info_simple where organcode = ?
     */
    public static final String getDzzsjByCode = "getDzzsjByCode";

    /**
     * 通过编码获取组织机构简称
     * select remarks6,orgname from pw_organ where compid = ? and orgcode = ?
     */
    public static final String queryFindJCByCode = "queryFindJCByCode";


    /***********************  组织机构业务层  ************************/

    /**
     * 根据组织机构code 获取信息 树
     * select * from t_dzz_info_simple where organcode = :organid and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimple = "queryTdzzInfoSimple";

    /**
     * 根据组织机构id 获取信息 树
     * select * from t_dzz_info_simple where organid = :organid and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimple2 = "queryTdzzInfoSimple2";

    /**
     * 根据idpath查询所有的组织信息
     * select * from t_dzz_info_simple where id_path like '%${PARAMS.id_path }%' and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimpleByIdpath = "queryTdzzInfoSimpleByIdpath";

    /**
     * 根据idpath查询所有的组织信息
     * select * from t_dzz_info_simple where id_path like '%${PARAMS.id_path }%' and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     * and dzzmc like ?
     * order by organcode asc,organtype desc,ordernum asc
     */
    public static final String queryTdzzInfoSimpleByIdpath2 = "queryTdzzInfoSimpleByIdpath2";

    /**
     * select * from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimpleById = "queryTdzzInfoSimpleById";

    /**
     * 模糊搜索组织机构
     * select * from t_dzz_info_simple where operatetype !=3 and dzzmc like ? and id_path like ?  and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String queryTdzzInfoSimpleLikestr = "queryTdzzInfoSimpleLikestr";

    /**
     * 根据组织结构获取总条数
     * select count(1) from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'
     */
    public static final String queryTdzzInfoSimpleByIdcount = "queryTdzzInfoSimpleByIdcount";


    /**
     * 基础信息查询
     * select distinct a.organid,a.organcode,a.dzzmc,a.zzlb,a.dzzlsgx,d.dzzmc as pdzzmc,b.dzzszdwqk,b.xzqh,f.dwname,f.dwxzlb from t_dzz_info_simple a inner join t_dzz_info b on a.organid = b.organid left join t_dzz_info_simple d on d.organid = a.parentid left join t_dw_info f on b.szdwid = f.uuid where a.operatetype != 3 and a.organid = ?
     */
    public static final String queryTdzzInfoSimpleDzzBase = "queryTdzzInfoSimpleDzzBase";

    /**
     * 单位信息
     * select distinct c.uuid,a.organid,c.dwname,c.dwxzlb,d.dwlsgx,c.dwjldjczzqk,d.isfrdw,d.dwfzr,d.fzrsfsdy,c.dwcode,d.jjlx,d.qygm,d.sfmykjqy,d.zgzgs,d.zgzgzgrs,d.zgzyjsrys,d.sfjygh, d.sfzjzz from t_dzz_info_simple a left join t_dw_info c on a.szdwid = c.uuid left join t_dw_info_bc d on d.uuid = c.uuid where a.operatetype != 3 and a.organid = ?
     */
    public static final String queryTdzzInfoSimpleDwinfoById = "queryTdzzInfoSimpleDwinfoById";

    /**
     * 查询党组织换届信息
     * select * from t_dzz_hjxx_dzz where organid = ?
     */
    public static final String queryTdzzInfoSimpleListById = "queryTdzzInfoSimpleListById";

    /**
     * 查询班子成员
     * select s.uuid,s.organid,s.userid,s.rzdate, s.username xm,zjhm,s.xb,s.csrq,s.xl,s.dnzwname,s.dnzwsm, s.zwlevel,s.lzdate,s.ordernum,s.lxdh from t_dzz_bzcyxx s inner join (select max(rzdate) rzdate,userid,organid from t_dzz_bzcyxx where organid = :organid group by userid ) t on  s.rzdate = t.rzdate and s.userid = t.userid and s.organid = t.organid where  s.zfbz  = 0
     * and xm like '%${PARAMS.xm}%'
     * order by ordernum asc
     */
    public static final String queryTdzzInfoSimpleBZCYList = "queryTdzzInfoSimpleBZCYList";


    /**
     * 奖惩查询
     * select * from t_dzz_jcxx_dzz where organid = ? order by jcdate desc
     */
    public static final String queryTdzzInfoSimpleJCXXList = "queryTdzzInfoSimpleJCXXList";


    /**
     * 查询组织自身和当前层级的组织的列表页
     * select * from t_dzz_info_simple where (organid = :organid or parentid = :organid)
     * <p>
     * and dzzmc like ?
     * order by organcode asc,organtype desc,ordernum asc
     */
    public static final String hornOrgTableList = "hornOrgTableList";

    /**
     * 数据字典项查询
     * select * from g_dict_item
     */
    public static final String queryDcitParams = "queryDcitParams";

    /**
     * 从视图查询包含下级的党组织信息
     * select organid,parentid,organcode,parentcode,ifnull(dzzmc,'') as dzzmc ,ifnull(zzlb,'') as zzlb,ifnull(dzzlsgx,'') as dzzlsgx,ifnull(xzqh,'') as xzqh,ifnull(szdwid,'') as szdwid,ifnull(operatetype,'') as operatetype,ifnull(organtype,'') as organtype,ifnull(ordernum,'') as ordernum,ifnull(lxrname,'') as lxrname,ifnull(lxrphone,'') as lxrphone,ifnull(fax,'') as fax,ifnull(dzzjlrq,'') as dzzjlrq,ifnull(address,'') as address,ifnull(dzzsj,'') as dzzsj,id_path from view_dzz_info where id_path like :idPath and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'
     * and dzzmc like '%${PARAMS.likeStr }%'
     * order by organcode asc,organtype desc,ordernum asc
     */
    public static final String queryTdzzInfoOrgView = "queryTdzzInfoOrgView";

    /**
     * 从视图查询直辖党组织
     * select organid,parentid,organcode,parentcode,ifnull(dzzmc,'') as dzzmc ,ifnull(zzlb,'') as zzlb,ifnull(dzzlsgx,'') as dzzlsgx,ifnull(xzqh,'') as xzqh,ifnull(szdwid,'') as szdwid,ifnull(operatetype,'') as operatetype,ifnull(organtype,'') as organtype,ifnull(ordernum,'') as ordernum,ifnull(lxrname,'') as lxrname,ifnull(lxrphone,'') as lxrphone,ifnull(fax,'') as fax,ifnull(dzzjlrq,'') as dzzjlrq,ifnull(address,'') as address,ifnull(dzzsj,'') as dzzsj,id_path from view_dzz_info where (organid = :organid or parentid = :organid)
     * and dzzmc like '%${PARAMS.likeStr }%'
     * order by organcode asc,organtype desc,ordernum asc
     */
    public static final String queryHornOrgView = "queryHornOrgView";

    /**
     * 党组织信息表
     * INSERT INTO t_dzz_info(organid, parentid, organcode, parentcode, dzzmc, zzlb, dzzlsgx, postcode, fax, address, lxrname, lxrphone, maindwid, szdwid, szdwlb, organtype, deltime, ordernum, creator, updatetime, operorganid, xxwzd, schemaname, delflag, datasources, updator, createtime, dzzjc, operatorname, dzzsj, isfunctional, dzzszdwqk, operatetype, provincecode, xzqh, electdate, expirationdate, gddh, sfxxwz, treecodepath, sumwzd, syncstatus, synctime)
     * VALUES (:organid, :parentid, :organcode, :parentcode, :dzzmc, :zzlb, :dzzlsgx, :postcode, :fax, :address, :lxrname, :lxrphone, :maindwid, :szdwid, :szdwlb, :organtype, :deltime, :ordernum, :creator, :updatetime, :operorganid, :xxwzd, :schemaname, :delflag,:datasources, :updator, :createtime, :dzzjc, :operatorname, :dzzsj, :isfunctional, :dzzszdwqk, :operatetype, :provincecode, :xzqh, :electdate, :expirationdate, :gddh, :sfxxwz, :treecodepath, :sumwzd, :syncstatus, :synctime)
     */
    public static final String saveDZZInfo = "saveDZZInfo";
    public static final String saveDZZInfo2 = "saveDZZInfo2";


    /**
     * update t_dzz_info set parentid=:parentid, parentcode=:parentcode, dzzmc=:dzzmc, zzlb=:zzlb, dzzlsgx=:dzzlsgx, postcode=:postcode, fax=:fax, address=:address, lxrname=:lxrname, lxrphone=:lxrphone,  maindwid=:maindwid,  szdwid=:szdwid,  szdwlb=:szdwlb,  organtype=:organtype,  deltime=:deltime, ordernum= :ordernum,  creator=:creator,  updatetime=:updatetime,  operorganid=:operorganid,  xxwzd=:xxwzd,  schemaname=:schemaname,  delflag=:delflag, datasources=:datasources,  updator=:updator,  createtime=:createtime,  dzzjc=:dzzjc,  operatorname=:operatorname, , dzzsj=:dzzsj,  isfunctional=:isfunctional,  dzzszdwqk=:dzzszdwqk,  operatetype=:operatetype,  provincecode=:provincecode,  xzqh=:xzqh,  electdate=:electdate,  expirationdate=:expirationdate,  gddh=:gddh,  sfxxwz=:sfxxwz,  treecodepath=:treecodepath, sumwzd=:sumwzd, syncstatus=:syncstatus , synctime=:synctime
     */
    public static final String updateDZZInfo = "updateDZZInfo";

    /**
     * 党组织信息表
     * update t_dzz_info set(organid=:organid, parentid=:parentid, parentcode=:parentcode, dzzmc=:dzzmc, zzlb=:zzlb, dzzlsgx=:dzzlsgx, postcode=:postcode, fax=:fax, address=:address, lxrname=:lxrname, lxrphone=:lxrphone, maindwid=:, szdwid=:szdwid, szdwlb=:szdwlb, organtype=:organtype, deltime=:deltime, ordernum=:ordernum, creator=:creator, updatetime=:updatetime, operorganid=:operorganid, xxwzd=:xxwzd, schemaname=:schemaname, delflag=:delflag, datasources=:datasources, updator=:updator, createtime=:createtime, dzzjc=:dzzjc, operatorname=:operatorname, dzzsj=:dzzsj, isfunctional=:isfunctional, dzzszdwqk=:dzzszdwqk, operatetype=:operatetype, provincecode=:provincecode, xzqh=:xzqh, electdate=:electdate, expirationdate=:expirationdate, gddh=:gddh, sfxxwz=:sfxxwz, treecodepath=:treecodepath, sumwzd=:sumwzd, syncstatus=:syncstatus, synctime=:synctime,dxzms=:dxzms) where organcode=:organcode
     */
    public static final String modiDZZInfo = "modiDZZInfo";

    /**
     * 党组织通用表
     * INSERT INTO t_dzz_info_simple(organid, parentid, organcode, parentcode, dzzmc, zzlb, delflag, maindwid, xxwzd, szdwid, organtype, deltime, datasources, schemaname, ordernum, updatetime, dzzlsgx, szdwlb, dzzjc, dzzsj, path1, path2, path3, path4, path5, path6, path7, path8, path9, path10, id_path, insert_date, isfunctional, dzzszdwqk, operatetype, provincecode, xzqh, sfxxwz, treecode, treecodepath)
     * VALUES (:organid, :parentid, :organcode, :parentcode, :dzzmc, :zzlb, :delflag, :maindwid, :xxwzd, :szdwid, :organtype, :deltime, :datasources, :schemaname, :ordernum, :updatetime, :dzzlsgx, :szdwlb, :dzzjc, :dzzsj, :path1, :path2, :path3, :path4, :path5, :path6, :path7, :path8, :path9, :path10, :id_path, :insert_date, :isfunctional, :dzzszdwqk, :operatetype, :provincecode, :xzqh, :sfxxwz, :treecode, :treecodepath);
     */
    public static final String saveDzzsimple = "saveDzzsimple";

    /**
     * 单位信息表
     * INSERT INTO t_dw_info(uuid, dwname, dwxzlb, dwjldjczzqk, isfrdw, dwcode, organid, xxwzd, dworder, datasources, operatetype, createtime, creator, updatetime, updator, gldworganid, treecodepath, sfxxwz, syncstatus, synctime, zfbz)
     * VALUES (:uuid, :dwname, :dwxzlb, :dwjldjczzqk, :isfrdw, :dwcode, :organid, :xxwzd, :dworder, :datasources, :operatetype, :createtime, :creator, :updatetime, :updator, :gldworganid, :treecodepath, :sfxxwz, :syncstatus, :synctime, :zfbz);
     */
    public static final String saveDwInfo = "saveDwInfo";


    /**
     * 党组织扩展表
     * INSERT INTO t_dzz_info_bc(organid, dzzsj, postcode, fax, address, lxrname, lxrphone, dzzjc, dzzsjzjhm, dzzjlrq, kzpyrq, kzdqpybs, cjpydys, kzpthydws, cjptdys, gddh, electdate, expirationdate, bjbzcsfs, sfxxwz, syncstatus, synctime)
     * VALUES (:organid, :dzzsj, :postcode, :fax, :address, :lxrname, :lxrphone, :dzzjc, :dzzsjzjhm, :dzzjlrq, :kzpyrq, :kzdqpybs, :cjpydys, :kzpthydws, :cjptdys, :gddh, :electdate, :expirationdate, :bjbzcsfs, :sfxxwz, :syncstatus, :synctime);
     */
    public static final String saveDzzInfoBc = "saveDzzInfoBc";

    /**
     * 根据组织编码查询信息
     * select * from t_dzz_info where organcode = :organcode
     */
    public static final String queryDzzInfoByOrgancode = "queryDzzInfoByOrgancode";
    public static final String queryDzzInfoByOrgancode2 = "queryDzzInfoByOrgancode2";
    public static final String queryDzzInfoByOrgancode3 = "queryDzzInfoByOrgancode3";




    /**
     * 根据组织编码查询信息
     * select * from t_dzz_info_simple where organcode = :organcode
     */
    public static final String queryDzzInfoSimpleByOrgancode = "queryDzzInfoSimpleByOrgancode";

    /**
     * 更新党组织信息表
     * update t_dzz_info set dzzmc = :dzzmc,zzlb=:zzlb,dzzlsgx=:dzzlsgx,dzzsj=:dzzsj,lxrname=:lxrname,lxrphone=:lxrphone,xzqh=:xzqh,dzzszdwqk=:dzzszdwqk,szdwid=:szdwid,szdwlb=:szdwlb  where  organcode = :organcode
     */
    public static final String updateDzzInfoByOrgancode = "updateDzzInfoByOrgancode";


    /**
     * 更新党组织扩展表
     * update t_dzz_info_bc set dzzsj = :dzzsj,lxrname=:lxrname,lxrphone=:lxrphone,dzzjlrq=:dzzjlrq  where organid = :organid
     */
    public static final String updateDzzInfoBc = "updateDzzInfoBc";


    /**
     * 根据组织ID查询党组织扩展表信息
     * select * from t_dzz_info_bc where organid = :organid
     */
    public static final String queryDzzInfoBcByOranid = "queryDzzInfoBcByOranid";

    /**
     * 更新党组织基本信息路径等
     * update t_dzz_info_simple set dzzmc=:dzzmc,zzlb=:zzlb,dzzlsgx=:dzzlsgx,dzzsj=:dzzsj,path1=:path1,path2=:path2,path3=:path3,path4=:path4,path5=:path5,path6=:path6,path7=:path7,path8=:path8,path9=:path9,path10=:path10 where organcode = :organcode
     */
    public static final String updateDzzInfoSimple = "updateDzzInfoSimple";

    /**
     * 根据idpath和ZZLB查询所有的组织信息
     * select * from t_dzz_info_simple where id_path like :idPath and FIND_IN_SET(zzlb, :zzlb) and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'
     */
    public static final String queryTdzzByIdPathAndZzlb = "queryTdzzByIdPathAndZzlb";

    /**
     * select count(*) from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' AND delflag = 0 order by ordernum+0 asc
     */
    public static String countTdzzInfoSimpleByPid = "countTdzzInfoSimpleByPid";

    /**
     * select organid from t_dzz_info_simple where parentid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static String getOrgidsByPid = "getOrgidsByPid";

    /**
     * 保存党建组织用户信息
     * insert into urc_organization(orgname,sorgname,porgid,orgseqno,compid,orgtypeid,orgcode,type,status,orglevel,orgpath,createuserid,createtime)
     * values(:orgname,:sorgname,:porgid,:orgseqno,:compid,:orgtypeid,:orgcode,:type,:status,:orglevel,:orgpath,:createuserid,:createtime)
     */
    public static final String saveHornOrganization = "saveHornOrganization";

    public static final String saveHornOrganization2 = "saveHornOrganization2";
    public static final String saveHornOrganization3 = "saveHornOrganization3";


    /**
     * update  urc_organization set orgname=:orgname,sorgname=:sorgname,porgid=:porgid,orgseqno=:orgseqno,compid=:compid,orgtypeid=:orgtypeid,type=:type,status=:status,orglevel=:orglevel,orgpath=:orgpath,createuserid=:createuserid,createtime=:createtime where orgcode=:orgcode
     */
    public static final String updateHornOrganization = "updateHornOrganization";
    public static final String updateHornOrganization2 = "updateHornOrganization2";


    public static final String getHornOrganization2 = "getHornOrganization2";

    /**
     * 根据组织编码删除党组织信息
     * delete from t_dzz_info where organcode = :organcode
     */
    public static final String delDzzInfoByCode = "delDzzInfoByCode";

    /**
     * 根据组织编码删除党组织信息表
     * delete from t_dzz_info_simple where organcode = :organcode
     */
    public static final String delDzzInfoSimpleByCode = "delDzzInfoSimpleByCode";

    /**
     * 根据系统组织查询上级组织信息
     * select * from urc_organization where orgcode = :organcode
     */
    public static final String queryPorgcode = "queryPorgcode";
    public static final String queryPorgcode2 = "queryPorgcode2";


    /**
     * 根据组织机构ID删除组织信息
     * delete from urc_organization where orgcode = :organcode
     */
    public static final String delOrganByOrgancode = "delOrganByOrgancode";


    /*********************************************************************** 党员导入********************************************************************/

    /**
     * 根据身份证号查询用户数
     * select count(*) from urc_user where idno = ? and  idtype = ?
     */
    public static String countUrcuserByIdno = "countUrcuserByIdno";

    /**
     * 通过身份证号更新用户信息
     * update urc_user set username = ?,sex = ?,phone = ? where idtype = 0 and compid = ? and idno = ?
     */
    public static String updateUrcuserByIdno = "updateUrcuserByIdno";

    /**
     * 新增账户用户
     * INSERT INTO urc_user (username,relationid, usertype, sex, seqno, avatar, postname, phone, landlines,email, idtype, idno, useraddr,  userlable, compid, createuserid, createtime, updateuserid, updatetime)
     * VALUES ( :username,:relationid,:usertype,:sex,:seqno,:avatar,:postname,:phone,:landlines,:email,:idtype,:idno,:useraddr,:userlable,:compid,:createuserid,:createtime,:updateuserid,:updatetime)
     */
    public static String insertUrcUser = "insertUrcUser";

    /**
     * 新增账号
     * INSERT INTO urc_user_account (account, passwd, userid, acttype, compid, remarks1, remarks2, remarks3, remarks4)
     * VALUES (:account,:passwd,:userid,:acttype,:compid,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static String addUserAccount2 = "addUserAccount2";
    public static String addUserAccount3 = "addUserAccount3";


    /**
     * 根据账号查询用户信息
     * select * from urc_user where idno ?
     */
    public static String queryUrcUserByIdno = "queryUrcUserByIdno";

    /**
     * 根据党组织编码更新用户信息
     * update urc_user set username = ?,sex = ?,phone = ? where idtype = ? and compid = ? and idno = ?
     */
    public static String updateUrcuserByOrgcode = "updateUrcuserByOrgcode";

    /**
     * 根据账户ID删除账户信息
     * delete from urc_user_account where account = ?
     */
    public static String delUaccountByAccount = "delUaccountByAccount";
    public static String delUaccountByAccount2 = "delUaccountByAccount2";


    /**
     * 新增urc_user_org_map表(用户和组织的关系)
     * INSERT INTO urc_user_org_map (userid, orgid, seqno, compid) VALUES (:userid,:orgid,:seqno,:compid)
     */
    public static String addUserOrgMap2 = "addUserOrgMap2";
    public static String addUserOrgMap4 = "addUserOrgMap4";


    /**
     * 删除urc_user_org_map表(用户和组织的关系)
     * DELETE FROM urc_user_org_map WHERE userid = ? and compid = ?
     */
    public static String removeUserOrgMapByUserid = "removeUserOrgMapByUserid";
    public static String removeUserOrgMapByUserid2 = "removeUserOrgMapByUserid2";


    /**
     * 查询系统参数
     * SELECT * FROM sys_param WHERE compid=? and paramkey=?
     */
    public static String getSysParam2 = "getSysParam2";

    /**
     * 根据主体删除角色关联
     * DELETE FROM urc_role_map WHERE sjtype=? and sjid=? and compid=?
     */
    public static String removeRoleMapBySubject = "removeRoleMapBySubject";

    /**
     * 新增角色关系
     * INSERT INTO urc_role_map (sjtype, sjid, roleid, compid,orgid) VALUES (:sjtype,:sjid,:roleid,:compid,:orgid)
     */
    public static String addRoleMap2 = "addRoleMap2";

    /**
     * select * from urc_role where compid=? and rolecode=? limit 1
     */
    public static String getUrcRoleByRoleCode = "getUrcRoleByRoleCode";


    /**--------------------------------交控框架下的党员导入账号处理---------------------------------------------------*/

    /**
     * 根据身份证号查询用户是否存在
     * select count(*) from pw_user where idno = ? and  accflag = ? and compid = ?
     */
    public static final String countPwuserByIdno = "countPwuserByIdno";

    /**
     * 根据组织机构编码查询组织机构信息
     * select * from pw_organ where orgcode = ? and compid = ?
     */
    public static final String getPwOrByOrgcode = "getPwOrByOrgcode";

    /**
     * 新增用户(交控原有)
     * INSERT INTO pw_user (id, username, sex, idno, accflag, postname, nickname, image, admin, cuserid, createtime, uuserid, updatetime, status, compid, remarks1, remarks2, remarks3, remarks4)
     * VALUES (:id,:username,:sex,:idno,:accflag,:postname,:nickname,:image,:admin,:cuserid,:createtime,:uuserid,:updatetime,:status,:compid,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static final String addPwUser = "addPwUser";
    public static final String addPwUser2 = "addPwUser2";

    /**
     * 新增用户组织机构关联(交控原有)
     * INSERT INTO pw_user_organ_map (compid, userid, organid, remarks1, remarks2, remarks3, remarks4) VALUES (:compid, :userid, :organid, :remarks1, :remarks2, :remarks3, :remarks4)
     */
    public static final String addPwUserOrganMap = "addPwUserOrganMap";

    /**
     * 新增角色关联(交控原有)
     * INSERT INTO pw_role_map (compid, subjecttp, subjectid, roleid, remarks1, remarks2, remarks3, remarks4) VALUES (:compid, :subjecttp,:subjectid,:roleid,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static final String addPLM = "addPLM";

    /**
     * 删除角色关联(交控原有)
     * DELETE FROM pw_role_map WHERE compid=? and subjecttp=? and subjectid=?
     */
    public static final String delRoleMap = "delRoleMap";

    /**
     * 删除组织机构关联
     */
    public static final String delPwUserOrgMap = "delPwUserOrgMap";

    /**
     * 添加账号(交控原有)
     * INSERT INTO pw_account (account, passwd, acctypeid, userid, compid, status, remarks1, remarks2, remarks3, remarks4) VALUES (:account,:passwd,:acctypeid,:userid,:compid,:status,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static final String addPwAccount = "addPwAccount";

    /**
     * 根据账号(身份证号)获取党员账号信息(交控原有)
     */
    public static final String getPwUserByAccountAndStatus = "getPwUserByAccountAndStatus";

    /**
     * 根据账号修改账号信息
     * update pw_user set username = ? ,sex = ?  where compid = ? and idno = ?
     */
    public static final String updatePwUserByIdno = "updatePwUserByIdno";

    /**
     * 修改账号电话号码
     * update pw_account set remarks3 = ? where userid = ? and compid = ?
     */
    public static final String updatePwAccountPhone = "updatePwAccountPhone";


    /**--------------------------------交控框架下的党员导入账号处理结束---------------------------------------------------*/

    /*********************************************************************** 党员导入结束********************************************************************/

    /***********************************************************************交控版本迁移开始****************************************************/
    /**
     * 根据类别查询党组织数
     * select count(1) from t_dzz_info_simple where 1 =1  and zzlb =:zzlb and ${PARAMS.path} = :organid
     */
    public static final String getDzzCountByZZLB = "getDzzCountByZZLB";

    /**
     * 查询党小组数
     * select count(1) from t_dzz_info_simple where 1 =1  and organtype = 4 and ${PARAMS.path} = :organid
     */
    public static final String getDxzCountByOrganid = "getDxzCountByOrganid";

    /**
     * 统计班子成员数量
     * select count(*) from t_dzz_bzcyxx a inner join  (select max(rzdate) rzdate,userid ,organid from t_dzz_bzcyxx where  organid = ? group by userid) b
     * on a.rzdate = b.rzdate and a.userid = b.userid and a.organid = b.organid where  a.zfbz = ? and (a.lzdate = '' or a.lzdate is null)
     */
    public static final String getBzcyCountByOrganid = "getBzcyCountByOrganid";

    /**
     * 根据党员类别查询党员数
     * select b.dylb,count(b.userid) as cou from t_dzz_info_simple a inner join t_dy_info b on a.organid = b.organid
     * where b.dyzt =:dyzt   and b.dylb =:dylb and a.${PARAMS.path} = :organid group by b.dylb
     */
    public static final String getDyCountByDYLB = "getDyCountByDYLB";

    /**
     * 统计党员学历数据
     * select t.*,c.item_name from (select b.xl,count(b.userid) as cou from t_dzz_info_simple a inner join t_dy_info b on a.organid = b.organid
     * where b.dyzt = :dyzt and a.${PARAMS.path} = :organid  group by b.xl) t left join g_dict_item c on t.xl = c.item_code   where c.code = :code
     */
    public static final String getXlxxCount = "getXlxxCount";

    /**
     * 组织关系转接数据集合
     * select b.PASSSTATUS as status,count(b.UUID) as val from t_dzz_info_simple a inner join t_transfer_main b on (a.organid = b.FROMORGANID or a.organid = b.TOORGANID)
     * where 1 =1 and a.${PARAMS.path} = :organid and b.TRANSFERSTATUS = 2 and (DATE_FORMAT(b.ENTERDATE,'%m') = :entermonth OR DATE_FORMAT(b.EXITDATE,'%m') = :exitmonth)  group by b.PASSSTATUS
     */
    public static final String getDzzgxzjInfoList = "getDzzgxzjInfoList";

    /**
     * 统计每月党员学习次数
     * select count(1) as cou from t_dzz_info_simple a  inner join t_dy_info b on a.organid = b.organid inner join study_join_plan c on b.userid = c.user_id
     * where 1 = 1 and a.${PARAMS.path} = :organid  and DATE_FORMAT(c.study_start_time,'%Y-%m') = :yearMonth
     */
    public static final String getDyStudyCountByMonth = "getDyStudyCountByMonth";

    /**
     * 按年统计党员累计学时
     * select case when sum(c.actual_time) > 0 then sum(c.actual_time) else 0 end as cou from t_dzz_info_simple a  inner join t_dy_info b on a.organid = b.organid
     * inner join study_join_plan c on b.userid = c.user_id where 1 = 1 and a..${PARAMS.path} = :organid and DATE_FORMAT(c.study_start_time,'%Y') = :studystartyear
     */
    public static final String getDyStudyHourByYear = "getDyStudyHourByYear";

    /**
     * 按年统计党员累计学习次数
     * select count(1) as cou from t_dzz_info_simple a inner join t_dy_info b on a.organid = b.organid inner join study_join_plan c on b.userid = c.user_id
     * where 1 = 1 and a.${PARAMS.path} = :organid  and DATE_FORMAT(c.study_start_time,'%Y') = :studystartyear
     */
    public static final String getDyStudyCountByYear = "getDyStudyCountByYear";

    /**
     * 党费统计
     * select b.status,count(1) as value from t_dzz_info_simple a inner join party_dues b on a.organid = b.org_id
     * where 1 =1 and year = :year and month = :month and a.${PARAMS.path} = :organid  group by b.status
     */
    public static final String getPartyDuesCount = "getPartyDuesCount";

    /**
     * 年龄结构统计
     * //		 * select :str as name,count(1) as value from ( select year(now()) - year(SUBSTRING(b.zjhm,7,8)) as age from t_dzz_info_simple a
     * //		 * inner join t_dy_info b on a.organid = b.organid where b.dyzt = :dyzt and b.dylb in (:zsdy,:ybdy) and a.${PARAMS.path} = :organid
     * //		 *  ) t where t.age >= :startage and t.age < :endage
     * <p>
     * select :str as name ,count(1) as value from view_dy_info where  dyzt = :dyzt and dylb in (:zsdy,:ybdy) and ${PARAMS.path} = :organid and age >= :startage and age < :endage
     */
    public static final String getAgeStructureMap = "getAgeStructureMap";

    /**
     * 根据类编码获取数据字典集合
     * select item_code as value, concat(item_code,' ',item_name) as lable,parent_code as pid from g_dict_item where code = ? order by item_order asc
     */
    public static final String getItemListByCode = "getItemListByCode";

    /**
     * select item_code as value, item_name as lable,parent_code as pid from g_dict_item where code = ? order by item_order asc
     */
    public static final String getItemListByCode2 = "getItemListByCode2";

    /**
     * 根据code和itemcode获取字典数据
     * select * from g_dict_item where code = ? and item_code = ? order by item_order asc
     */
    public static final String getItemByItemcode = "getItemByItemcode";

    /**
     * 获取党组织(查询path使用)
     * select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static final String get_path_num = "get_path_num";

    /***********************************************************************交控版本迁移结束****************************************************/


    /**
     * select 0 flag,u.userid,a.account,u.username name,0 seqno,0 remarks1  from urc_user u left join urc_user_account a on u.userid = a.userid where  a.passwd!='' and u.compid=:compid and u.status=1 and u.accflag = 0
     */
    public static final String listUserNodeByUsername = "listUserNodeByUsername";

    /**
     * select 1 flag,orgid,remarks3 account,orgname name,sorgname, orgseqno,1 remarks1,porgid,orgtypeid icon from urc_organization where compid = ? and orgid=?
     */
    public static final String listOrgan2 = "listOrgan2";

    /**
     * select d.flag,d.userid,d.account,d.name,d.seqno,d.remarks1,d.pid,d.remarks2,d.remarks3,null icon  from(SELECT 0 flag,b.userid,b.account,b.username name,0 seqno,0 remarks1,-1 pid,0 remarks2,b.remarks3 remarks3 FROM urc_user_org_map a right JOIN (select u.status,u.accflag,u.userid,u.username,w.account,u.remarks3,u.compid from urc_user u left join urc_user_account w on u.userid=w.userid where u.compid=:compid and w.compid=:compid) b ON a.userid=b.userid and b.accflag=0 and b.status=1 where a.compid=:compid and a.orgid=:orgid order by remarks2 desc ,remarks3)d union all SELECT 1 flag,orgid,remarks3 account,orgname name,orgseqno,1 remarks1,porgid,0 remarks2,0 remarks3,orgtypeid icon  FROM urc_organization WHERE compid=:compid and porgid=:orgid and status=1 order by orgseqno,remarks2 desc,remarks3
     */
    public static final String listAllChildNode = "listAllChildNode";

    public static final String listAllChildNodeUnder = "listAllChildNodeUnder";

    /**
     * 删除分类授权关系
     * DELETE FROM urc_auth_other_map WHERE compid = ? and subjecttp = ? and subjectid = ?
     */
    public static final String delFelLeiMap = "delFelLeiMap";

    /**
     * SELECT * FROM DL_TYPE WHERE compid=? and ID=?
     */
    public static final String getDlTypeById = "getDlTypeById";

    public static final String getDlTypeByIdParty = "getDlTypeByIdParty";

    /**
     * 保存分类授权数据
     * INSERT INTO urc_auth_other_map (compid, subjecttp, subjectid, module, bizid, cuserid, createtime, remarks1, remarks2, remarks3, remarks4) VALUES (:compid, :subjecttp, :subjectid, :module, :bizid, :cuserid, :createtime, :remarks1, :remarks2, :remarks3, :remarks4)
     */
    public static final String saveFeiLeiAuth = "saveFeiLeiAuth";

    /**
     * 角色查询
     * select bizid classid from urc_auth_other_map where compid = ? and subjecttp = ? and subjectid = ?
     */
    public static final String listFeiLeirole = "listFeiLeirole";

    /**
     * 用户
     * select classid from(SELECT bizid classid FROM urc_auth_other_map where compid=? and subjecttp=1 and subjectid in (SELECT roleid FROM urc_role_map where compid=? and subjecttp=0 and subjectid=?) group by bizid union all SELECT bizid classid FROM urc_auth_other_map where compid=? and subjecttp=0 and subjectid =? ) a group by classid
     */
    public static final String listuserandrole = "listuserandrole";


    /*************************************** 党小组 ****************************************/

    /**
     * 查询可以选择的党小组成员信息
     * select * from view_dy_info where organid = :organid and compid = :compid and userid not in (select memberid from t_party_group_member where compid = :compid)
     */
    public static final String getOptionalMember = "getOptionalMember";

    /**
     * 查询成员是否存在党小组中
     * select * from t_party_group_member where compid = ? and FIND_IN_SET(memberid, ?)
     */
    public static final String checkPartyGroupExists = "checkPartyGroupExists";

    /**
     * 增加党小组基本信息
     * INSERT INTO t_party_group(code, name, orgcode, organid, orgname, createid, createname, createtime, status, type, compid, remarks1, remarks2) VALUES (:code, :name, :orgcode, :organid, :orgname, :createid, :createname, :createtime, :status, :type, :compid, :remarks1, :remarks2)
     */
    public static final String addPartyGroup = "addPartyGroup";

    /**
     * 修改党小组信息
     * update t_party_group set name=:name,status=:status  where id = :id and compid = :compid
     */
    public static final String modifyPartyGroup = "modifyPartyGroup";

    /**
     * 增加党小组成员信息
     * INSERT INTO t_party_group_member( memberid, groupid, membername, orgcode, orgname, groupname, type, idcard, compid) VALUES ( :memberid, :groupid, :membername, :orgcode, :orgname, :groupname, :type, :idcard, :compid)
     */
    public static final String addPartyGroupMember = "addPartyGroupMember";

    /**
     * 删除党小组成员信息
     * delete from t_party_group_member where groupid = ? and  compid = ?
     */
    public static final String delPartyGroupMember = "delPartyGroupMember";

    /**
     * 删除党小组信息
     * delete from t_party_group where id = ? and compid = ?
     */
    public static final String delPartyGroup = "delPartyGroup";

    /**
     * 查询党小组列表
     * select * from t_party_group where orgcode = ? and compid = ?
     */
    public static final String queryPartyGroupList = "queryPartyGroupList";

    /**
     * 查询党小组成员
     * select * from t_party_group_member where groupid = ? and  compid = ?
     * select a.*,b.lxdh,c.dnzwmc from t_party_group_member a
     * left join t_dy_info b on a.memberid = b.userid
     * left join t_party_group d on a.groupid = d.id
     * left join t_dzz_bzcyxx c on a.memberid = c.userid and c.organid = d.organid
     * where a.groupid = ? and  a.compid = ?
     */
    public static final String getPartyGroupMemers = "getPartyGroupMemers";

    /**
     * 查询党小组信息
     * select * from t_party_group where id = ? and compid = ?
     */
    public static final String getPartyGroup = "getPartyGroup";

    /**
     * 查询是否同名的党小组
     * select * from t_party_group where orgcode = ? and name = ? and compid = ?
     */
    public static final String queryExitsGroupName = "queryExitsGroupName";

    /**
     * 统计组织下所有党小组数
     * <p>
     * select count(*) from  t_dzz_info_simple a inner join t_party_group b on a.organid = b.organid and a.organcode = b.orgcode where a.delflag = 0 and  a.${PARAMS.path} = :organid
     */
    public static final String getDxzCountByOrganidnew = "getDxzCountByOrganidnew";

    /**
     * 按性别统计党员数
     * select count(*) from view_dy_info where  dyzt = '1000000003' and dylb in('2000000002' ,'1000000001') and xb = :xb and ${PARAMS.path} = :organid
     */
    public static final String statisticsMemberBySex = "statisticsMemberBySex";
    /**
     * select c.xm, if(postname=1,"书记","党员") as postname,e.phone
     * from urc_organization a
     * left join  urc_user_org_map d on a.orgid=d.orgid
     * left join urc_user e on d.userid=e.userid
     * left join t_dzz_info b on a.orgcode=b.organcode
     * left join t_dy_info c on b.organid=c.organid
     * where a.orgid=:orgId and c.dylb ='1000000001'
     */
    public static final String listTDyInfo = "listTDyInfo";


    /**
     * 党员数
     * <p>
     * select count(*) from t_dy_info where dylb in ('1000000001','2000000002') and dyzt = '1000000003' and organid = ?
     */
    public static final String getMemberCountByOrganid = "getMemberCountByOrganid";

    /**
     * 最新一届换届信息
     * select * from t_dzz_hjxx_dzz where organid = ? order by hjdate desc limit 1
     */
    public static final String getLatestHjxxByOrganid = "getLatestHjxxByOrganid";
    /**
     * select c.xm,d.dnzwname postname,b.dzzmc from t_dzz_info b left join t_dy_info c on b.organid=c.organid  left join t_dzz_bzcyxx d on c.userid=d.userid and c.organid=d.organid where c.zjhm = ?
     */
    public static final String getTDyInfo = "getTDyInfo";

    /**
     * select * from t_dzz_info  where userid=? limit 1
     */
    public static final String getTDyInfo2 = "getTDyInfo2";

    /**
     * insert into dl_file_business (fileid,businessid,businessname,compid,intostatus) values (:fileid,:businessid,:businessname,:compid,:intostatus)
     */
    public static String addDlFileBusiness = "addDlFileBusiness";

    /**
     * update urc_user set jsondata=? where compid=? and userid=?
     */
    public static String modiUrcUser = "modiUrcUser";

    /**
     * select * from dl_file_business where compid=? and businessid=? and businessname=? and intostatus=?
     */
    public static String getDlFileBusiness = "getDlFileBusiness";

    /**
     * update dl_file_business set fileid=:fileid where compid=:compid and businessid=:businessid and businessname=businessname and intostatus=:intostatus
     */
    public static String modiDlFileBusiness = "modiDlFileBusiness";

    /**
     * INSERT INTO urc_user (username,relationid, usertype, sex, seqno, avatar, postname, phone, landlines,email, idtype, idno, useraddr,  userlable, compid, createuserid, createtime, updateuserid, updatetime) VALUES ( :username,:relationid,:usertype,:sex,:seqno,:avatar,:postname,:phone,:landlines,:email,:idtype,:idno,:useraddr,:userlable,:compid,:createuserid,:createtime,:updateuserid,:updatetime)
     */
    public static String insertUrcuser = "insertUrcUser";

    /**
     * SELECT * FROM urc_user WHERE userid =?
     */
    public static String getPwUserByID = "getPwUserByID";
    /**
     * SELECT * FROM urc_user WHERE compid=? and userid =?
     */
    public static String getPwUserByID2 = "getPwUserByID2";

    /**
     * 用户总数(取自系统底层方法)
     * select count(*) from urc_user where compid=:compid AND createtime<=:enddate
     * getSingleColumn java.lang.Integer
     */
    public final static String getUserCountByDate = "getUserCountByDate";

    /**
     * 按天统计活跃用户数
     * select count(*) from (select * from sys_log where accessdate = :accessdate and optflag = 4 group by userid ) t
     */
    public final static String getActiveUserCountByDay = "getActiveUserCountByDay";

    /**
     * 按月统计活跃用户数
     * select count(*) from (select * from sys_log where accessdate >= :monthStart and accessdate <= :monthEnd and optflag = 4 group by userid ) t
     */
    public final static String getActiveUserCountByMonth = "getActiveUserCountByMonth";

    /**
     * select * from urc_user where userid = ? and compid = ?
     */
    public final static String getUserIdcard = "getUserIdcard";

    /**
     * select * from election_remind_config where compid=? and orgcode=? limit 1
     */
    public static String getElectionRemindConfig = "getElectionRemindConfig";

    /**
     * insert into election_remind_config (compid,orgid,orgcode,remindpeople,reminddate,createtime,createuserid,updatetime,updateuserid,remarks1,remarks2,remarks3,remarks4) values (:compid,:orgid,:orgcode,:remindpeople,:reminddate,:createtime,:createuserid,:updatetime,:updateuserid,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static String addElectionRemindConfig = "addElectionRemindConfig";

    /**
     * update election_remind_config set remindpeople=:remindpeople,reminddate=:reminddate,updatetime=:updatetime,updateuserid=:updateuserid where compid=:compid and orgcode=:orgcode
     */
    public static String modiElectionRemindConfig = "modiElectionRemindConfig";

    /**
     * insert into election_remind_time (compid,orgid,orgcode,remindtime,status,createtime,createuserid,remarks1,remarks2,remarks3,remarks4) values (:compid,:orgid,:orgcode,:remindtime,:status,:createtime,:createuserid,:remarks1,:remarks2,:remarks3,:remarks4)
     */
    public static String addElectionRemindTime = "addElectionRemindTime";

    /**
     * delete from election_remind_time where compid=? and orgid=? and status!=2
     */
    public static String delElectionRemindTime = "delElectionRemindTime";

    /**
     * select a.dzzmc,b.hjdate,b.jmdate,c.remindpeople,c.reminddate from t_dzz_info a left join t_dzz_hjxx_dzz b on a.organid=b.organid left join election_remind_config c on a.organcode=c.orgcode where a.organid=?
     */
    public static String queryElectionRemindConfig = "queryElectionRemindConfig";

    /**
     * select * from  election_remind_time compid=? and remindtime<=? and status!=?
     */
    public static String listElectionRemindTime = "listElectionRemindTime";

    /**
     * select * from election_remind_config where compid=? and orgcode=? limit 1
     */
    public static String getConfigPeople = "getConfigPeople";

    /**
     * SELECT b.userid FROM t_dy_info a left join urc_user b on a.zjhm=b.idno where a.userid=?
     */
    public static String getUrcUserid = "getUrcUserid";

    /**
     * update election_remind_time set status=2 where compid=? and orgid=? and remindtime=?
     */
    public static String modiElectionRemindTimeStatus = "modiElectionRemindTimeStatus";

    /**
     * 发送消息(任务模块的发送消息)
     * INSERT INTO tk_task_remind_list (compid, noteid, module, mid, remind, rulecode,rulename, optuserid, optusername, userid, username, isread, ishandle, issend, mesg, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6,remark)
     * VALUES (:compid,:noteid,:module,:mid,:remind,:rulecode,:rulename,:optuserid,:optusername,:userid,:username,:isread,:ishandle,:issend,:mesg,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6,:remark)
     */
    public static String addTTRLByTuihui = "addTTRLByTuihui";

    /**
     * update t_dzz_bzcyxx set zfbz=1 where userid=?
     */
    public static String modiBzcyxx = "modiBzcyxx";

    /**
     * update t_dzz_bzcyxx set dnzwmc=?,dnzwsm=?,rzjc=?,lzdate=? where userid=?
     */
    public static String modiBzcyxx2 = "modiBzcyxx2";

    /**
     * 根据职务类别获取党员姓名
     * select username from t_dzz_bzcyxx where organid = ? and dnzwname = ? and zfbz = 0 order by ordernum asc
     */
    public final static String getBzcyNamesByPost = "getBzcyNamesByPost";

    /**
     * select * from urc_role where compid=? and rolecode=? limit 1
     */
    public static String getUrcRole = "getUrcRole";

    /**
     * INSERT INTO urc_role_map (sjtype, sjid, roleid, compid, orgid, opttime) VALUES (?, ?, ?, ?, ?, ?)
     */
    public static String addUrcRoleMap = "addUrcRoleMap";

    /**
     * delete from urc_role_map where sjtype=? and sjid=? and  roleid=? and compid=?
     */
    public static String removeUrcRoleMap = "removeUrcRoleMap";

    /**
     * 根据状态统计换届数量
     * select count(*) from view_hjxx_info where zfbz = 0 and jmdate <> '' and jmdate is not null and  delayflag = :status and ${PARAMS.path} = :organid
     */
    public final static String getHjxxCount = "getHjxxCount";
    /**
     * 换届信息分页
     * select * from view_hjxx_info where zfbz = 0 and jmdate <> '' and jmdate is not null  and ${PARAMS.path} = :organid
     * <p>
     * and  delayflag = :status
     */
    public final static String pageHjxxInfo = "pageHjxxInfo";

    /**
     * INSERT INTO t_dzz_bzcyxx (uuid, userid, organid, ordernum, username, zjhm, xb, csrq, lxdh, xl, dnzwname, dnzwsm, dnzwmc, rzdate, sfzr,  syncstatus, synctime, zfbz) VALUES (:uuid, :userid, :organid, :ordernum, :username, :zjhm, :xb, :csrq, :lxdh, :xl, :dnzwname, :dnzwsm, :dnzwmc, :rzdate, :sfzr,  :syncstatus, :synctime, :zfbz)
     */
    public static String addTDzzBzcyxx = "addTDzzBzcyxx";

    /**
     * select dzzmc from t_dzz_info where organid=?
     */
    public static String queryTDzzBzcyxx = "queryTDzzBzcyxx";

    /**
     * INSERT INTO t_dzz_bzcyxx (uuid, userid, organid, ordernum, username, zjhm, xb, csrq, lxdh, xl, dnzwname, dnzwsm, dnzwmc, rzdate, sfzr,  syncstatus, synctime, zfbz,rzdate,lzdate,organname) VALUES (:uuid, :userid, :organid, :ordernum, :username, :zjhm, :xb, :csrq, :lxdh, :xl, :dnzwname, :dnzwsm, :dnzwmc, :rzdate, :sfzr,  :syncstatus, :synctime, :zfbz,:rzdate,:lzdate,:organname)
     */
    public static String addTDzzBzcyxx2 = "addTDzzBzcyxx2";


    /**
     * select * from t_dzz_bzcyxx where userid=? and organid=? limit 1
     */
    public static String getBzcyxx = "getBzcyxx";

    /**
     * select * from t_dzz_bzcyxx where organid=? and dnzwname=? limit 1
     */
    public static String getBzcyxx2 = "getBzcyxx2";

    /**
     * delete from t_dzz_bzcyxx where userid=? and organid=?
     */
    public static String delTDzzBzcyxx = "delTDzzBzcyxx";

    /**
     * update urc_organization set remarks3=? where orgid=?
     */
    public static String modiUserOrg = "modiUserOrg";
    public static String modiUserOrg2 = "modiUserOrg2";


    /**
     * select * from t_dzz_info where organcode=? limit 1
     */
    public static String getOrganidByOrgancode = "getOrganidByOrgancode";

    /**
     * select * from t_dy_info where userid=? limit 1
     */
    public static String getIdnoByUserid = "getIdnoByUserid";

    /**
     * select * from t_dzz_bzcyxx where userid=? AND zfbz=0 limit 1
     */
    public static String getDnzwnameByUserid = "getDnzwnameByUserid";

    /**
     * update t_dzz_info set dzzsj=? where organid=?
     */
    public static String modiTDzzInfo = "modiTDzzInfo";

    /**
     * update urc_user set postname=? where idno=?
     */
    public static String modiUrcUserByIdno = "modiUrcUserByIdno";

    /**
     * update t_dzz_info_bc set dzzsj=? where organid=?
     */
    public static String modiTDzzInfoBc = "modiTDzzInfoBc";

    /**
     * update t_dzz_info_simple set dzzsj=? where organid=?
     */
    public static String modiTDzzInfoSimple = "modiTDzzInfoSimple";

    /**
     * delete from t_dzz_hjxx_dzz where organid=:organid
     */
    public static String delDzzBzxxByOrganid = "delDzzBzxxByOrganid";
    /**
     * INSERT INTO t_dzz_hjxx_dzz (organid,organname,hjdate,bjbzcsfs, jmdate,syncstatus,synctime,zfbz) VALUES (:organid,:organname,:hjdate,:bjbzcsfs, :jmdate,:syncstatus,:synctime,:zfbz)
     */
    public static String addDzzBzxxByOrganid = "addDzzBzxxByOrganid";


    //-------------------------------------------数据同步-------------------------------------------//


    /**
     * INSERT INTO urc_organization (orgid, orgname, sorgname, porgid, orgseqno, compid, orgtypeid, orgcode, type, createtime)
     * VALUES (:orgid, :orgname, :orgname, :porgid, 0, 1, :orgtypeid, :orgcode, 0, now());
     */
    public static String addUrcOrganization = "addUrcOrganization";
    /**
     * INSERT INTO urc_user (userid, usertype, username, sex, postname, phone, idno, compid, createtime,accflag,createuserid)
     * VALUES (:userid, 0, :username, :sex, :postname, :phone, :idno, 1, now(),:accflag,:createuserid);
     */
    public static String addUrcUser = "addUrcUser";

    /**
     * update urc_user set usertype=:usertype, username=:username, sex=:sex, postname=:postname, phone=:phone, idno=:idno, compid=:compid, createtime=:createtime,accflag=:accflag ,status=1 where userid=:userid
     */
    public static String updateUrcUser = "updateUrcUser";
    /**
     * INSERT INTO urc_user_org_map (userid, orgid, seqno, compid) VALUES (:userid, :orgid, 0, 1);
     */
    public static String addUserOrgMap3 = "addUserOrgMap3";
    /**
     * INSERT INTO urc_role_map (sjtype, sjid, roleid, compid, orgid, opttime) VALUES (0, :sjid, :roleid, 1, :orgid, now())
     */
    public static String addUserRoleMap = "addUserRoleMap";
    /**
     * INSERT INTO urc_user_account (account, passwd, userid, acttype, compid) VALUES (:account, '3Jvk6N9ujw1fACyEMQ4iqA==', :userid, 0, 1)
     */
    public static String addUrcUserAccount = "addUrcUserAccount";

    /**
     * update urc_user_account set  passwd=:passwd, userid=:userid, acttype=0, compid=:compid where account=:account
     */
    public static String updateUrcUserAccount = "updateUrcUserAccount";
    /**
     * DELETE FROM urc_organization WHERE orgid = :orgid
     */
    public static String delUrcOrganization = "delUrcOrganization";
    /**
     * DELETE FROM urc_user
     */
    public static String delUrcUsers = "delUrcUsers";

    /**
     * select a.* from urc_user a left join urc_user_account b on a.userid=b.userid  where b.account=:account
     */
    public static String getUrcUsers = "getUrcUsers";
    /**
     * DELETE FROM urc_user_org_map WHERE userid = :userid
     */
    public static String delUrcOrgMap = "delUrcOrgMap";
    /**
     * DELETE FROM urc_role_map WHERE  sjid = :sjid
     */
    public static String delUserRoleMap = "delUserRoleMap";
    /**
     * DELETE FROM urc_user_account WHERE userid = :userid
     */
    public static String delUrcUserAccount = "delUrcUserAccount";

    /**
     * select * from urc_user_account where account=:account
     */
    public static String getUrcUserAccount = "getUrcUserAccount";
    /**
     * UPDATE urc_organization SET  remarks3 = :userid WHERE orgid = :orgid
     */
    public static String modiUrcOrganization = "modiUrcOrganization";
    /**
     * select * from t_dzz_info
     */
    public static String listDzzInfo = "listDzzInfo";
    /**
     * select * from sys_user where status=1
     */
    public static String listSysUser = "listSysUser";
    /**
     * select * from t_dzz_info where organid=? limit 1
     */
    public static String getDzzInfo = "getDzzInfo";
    /**
     * select * from t_dy_info where userid=? limit 1
     */
    public static String getDyInfo = "getDyInfo";
    /**
     * select * from t_dzz_bzcyxx where userid=? and sfzr=1 order by ordernum desc limit 1
     */
    public static String getDzzBzcy = "getDzzBzcy";
    /**
     * select a.* from urc_role_map a left join urc_role b on a.roleid=b.roleid where b.rolecode=? and sjid=? and sjtype=0
     */
    public static String getUrcRoleMapByRoleCode = "getUrcRoleMapByRoleCode";
    /**
     * select * from urc_organization where status=1
     */
    public static String listUrcOrgan = "listUrcOrgan";
    /**
     * SELECT * FROM urc_user_account where account like '%B0%' and userid in (select userid from urc_user_org_map where orgid=?) limit 1
     */
    public static String getUrcOrganMap = "getUrcOrganMap";

    //=====================================================书记花名册============================================================

    /**
     * INSERT INTO t_secretary_info (userid, organcode, parttimejob, worktime, partytime, secretarytime, gradschool, nationaltrain, grouptrain, rewards, performduties, report, resume, remarks2, remarks3, remarks4, remarks5)
     * VALUES (:userid, :organcode, :parttimejob, :worktime, :partytime, :secretarytime, :gradschool, :nationaltrain, :grouptrain, :rewards, :performduties, :report, :resume, :remarks2, :remarks3, :remarks4, :remarks5);
     */
    public static String addTSecretaryInfo = "addTSecretaryInfo";
    /**
     * select a.*,b.xm,c.dzzmc,e.item_name postname from t_secretary_info a
     * left join t_dy_info b on a.userid=b.userid
     * left join t_dzz_info c on a.organcode=c.organcode
     * left join t_dzz_bzcyxx d on a.userid=d.userid
     * left join g_dict_item e on d.dnzwname=e.item_code and e.code='d_dy_dnzw'
     * where a.status=? and a.organcode=? order by e.item_code asc
     */
    public static String pageTSecretaryInfo = "pageTSecretaryInfo";
    /**
     * select * from t_secretary_info where userid=? and status=? limit 1
     */
    public static String getTSecretaryInfo = "getTSecretaryInfo";
    /**
     * update t_secretary_info set organcode=:organcode, parttimejob=:parttimejob, worktime=:worktime, partytime=:partytime, secretarytime=:secretarytime, gradschool=:gradschool, nationaltrain=:nationaltrain, grouptrain=:grouptrain, rewards=:rewards, performduties=:performduties, report=:report, resume=:resume where =? and status=1
     */
    public static String modiTSecretaryInfo = "modiTSecretaryInfo";
    /**
     * select a.*,s.uuid,s.organid,s.userid,s.rzdate, s.username xm,zjhm,s.xb,s.csrq,s.xl,s.dnzwname,s.dnzwsm, s.zwlevel,s.lzdate,s.ordernum,s.lxdh from t_secretary_info a left join t_dzz_bzcyxx s on a.userid=s.userid where id=?
     */
    public static String getTSecretaryInfo2 = "getTSecretaryInfo2";
    /**
     * update t_secretary_info set status=? where id=?
     */
    public static String modiTSecretaryInfo2 = "modiTSecretaryInfo2";

    /**
     * path查询
     * select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static String get_meeting_path_num = "get_meeting_path_num";

    public static String listChildOrgan2 = "listChildOrgan2";
    /**
     * select * from urc_organization where orgcode=?
     */
    public static String getOrgan = "getOrgan";
    public static String getOrgan2 = "getOrgan2";
    public static String getOrgan3 = "getOrgan3";


    /**
     * SELECT orgid,sorgname name,orgcode FROM urc_organization where porgid=?
     */
    public static String listChildOrgan3 = "listChildOrgan3";

    /**
     * select b.opt_user_org organid,b.opt_user_org_name organname,count(*) num ,b.opt_time opttime from sys_log_play b left join t_dzz_info_simple a on a.organid=b.opt_user_org
     * where ${PARAMS.path} = :organid and b.fun_id is null
     * and opttime>=:starttime
     * and opttime<=:endtime
     * group by opt_user_org;
     */
    public static String listSysLogPlay = "listSysLogPlay";

    public static String listSysLogPlay2 = "listSysLogPlay2";
    /**
     * SELECT * FROM g_dict_item where code = 'd_dzz_zzlb' and item_name=? limit 1
     */
    public static String getZzlbCode = "getZzlbCode";

    /**
     * SELECT * FROM g_dict_item where code = 'd_dw_xzlb' and item_name=? limit 1
     */
    public static String getZzlbCode2 = "getZzlbCode2";

    /**
     * SELECT * FROM g_dict_item where code = 'd_dw_lsgx' and item_name=? limit 1
     */
    public static String getZzlbCode3 = "getZzlbCode3";

    /**
     * SELECT * FROM g_dict_item where code = 'd_dw_jljczz' and item_name=? limit 1
     */
    public static String getZzlbCode4 = "getZzlbCode4";

    /**
     * select * from t_dzz_info where dxzms = ? limit 1
     */
    public static String getTDzzInfo = "getTDzzInfo";

    public static String getTDzzInfo2 = "getTDzzInfo2";

    /**
     * SELECT * FROM g_dict_item where code = ? and item_name=? limit 1
     */
    public static String getZzlbCodeAll = "getZzlbCodeAll";

    /**
     * INSERT INTO sys_user ( id, acc, pwd, name, org_id, org_name, type, work_phone, roles, remark, status, order_id, base_info_id, create_time, modi_time, salt, creater, modifier) VALUES ( :id :acc, :pwd, :name, :orgId, :orgName, :type, :workPhone, :roles, :remark, :status, :orderId, :baseInfoId, :createTime, :modiTime, :salt, :creater, :modifier)
     */
    public static String addSysuser = "addSysuser";

    /**
     * update sys_user set acc=:acc,  name=:name, org_id=:orgId, org_name=:orgName, work_phone=:workPhone, roles=:roles where base_info_id=:baseInfoId
     */
    public static String modiSysuser = "modiSysuser";

    /**
     * update sys_user set  name=:name, org_id=:orgId, org_name=:orgName, work_phone=:workPhone, roles=:roles where acc=:acc
     */
    public static String modiSysuser2 = "modiSysuser2";

    /**
     * select dxzms from t_dzz_info where (organcode = ? or parentcode =?) and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public static String listTDzzInfos = "listTDzzInfos";

    public static String listTDzzInfos3 = "listTDzzInfos3";

    /**
     * update t_dzz_info set operatetype=3 where dxzms in (:orgname)
     */
    public static String modiTDzzinfos = "modiTDzzinfos";

    /**
     * update t_dzz_info_simple set operatetype=3 where dxzms in (:orgname)
     */
    public static String modiTDzzSimpleinfos = "modiTDzzSimpleinfos";

    /**
     * select  GROUP_CONCAT('''',organid,'''') organid from t_dzz_info where dxzms in (:orgname)
     */
    public static String getOrganidBydxzms = "getOrganidBydxzms";

    /**
     * select * from t_dzz_info_simple where organid=?
     */
    public static String getTDzzInfoimpl = "getTDzzInfoimpl";

    /**
     * select * from t_dzz_info_simple where organcode=?
     */
    public static String geTTDzzInfoimplBycode = "geTTDzzInfoimplBycode";

    /**
     * update t_dzz_info_simple set organid=:organid, parentid=:parentid, parentcode=:parentcode, dzzmc=:dzzmc, zzlb=:zzlb, dzzlsgx=:dzzlsgx, postcode=:postcode, fax=:fax, address=:address, lxrname=:lxrname, lxrphone=:lxrphone, maindwid=:, szdwid=:szdwid, szdwlb=:szdwlb, organtype=:organtype, deltime=:deltime, ordernum=:ordernum, creator=:creator, updatetime=:updatetime, operorganid=:operorganid, xxwzd=:xxwzd, schemaname=:schemaname, delflag=:delflag, datasources=:datasources, updator=:updator, createtime=:createtime, dzzjc=:dzzjc, operatorname=:operatorname, dzzsj=:dzzsj, isfunctional=:isfunctional, dzzszdwqk=:dzzszdwqk, operatetype=:operatetype, provincecode=:provincecode, xzqh=:xzqh, electdate=:electdate, expirationdate=:expirationdate, gddh=:gddh, sfxxwz=:sfxxwz, treecodepath=:treecodepath, sumwzd=:sumwzd, syncstatus=:syncstatus, synctime=:synctime where organid=:organid
     */
    public static String modiDZZimpl = "modiDZZimpl";

    /**
     * update urc_organization set status=0 where orgname in (:orgname)
     */
    public static String modiUrcOrganizations = "modiUrcOrganizations";


    public static String modiUrcOrganizations2 = "modiUrcOrganizations2";

    public static String modiUrcOrganizations3 = "modiUrcOrganizations3";
    public static String modiUrcOrganizations4 = "modiUrcOrganizations4";


    /**
     * delete from urc_user_account where account in ()
     */
    public static String removeUrcAccount = "removeUrcAccount";


    public static String getOrganidBydxzms2 = "getOrganidBydxzms2";

    //delete from sys_user where acc in ()
    public static String removeSysuser = "removeSysuser";

    /**
     * select * from sys_user where acc=? limit 1
     */
    public static String getSysuser = "getSysuser";

    /**
     * select * from t_dzz_info_simple where id_path like :idPath and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026'
     */
    public static String listTDzzInfos2 = "listTDzzInfos2";
    /**
     * select * from t_dzz_info where organcode=?
     */
    public static String geTTDzzInfoBycode = "geTTDzzInfoBycode";
    public static String updateDZZsimpleInfo = "updateDZZsimpleInfo";


    /**
     * 根据Organids获取党组织名称
     *select dzzmc as partyOrgName from t_dzz_info where organid = ?;
     */
    public static String getDzzmcByOrganids = "getDzzmcByOrganids";

    /**
     * 根据Organids获取党组织类别
     *select zzlb as PartyOrgType from t_dzz_info where organid = ?
     */
    public static String getDzzlbByOrganIds = "getDzzlbByOrganIds";

    /**
     * SELECT * FROM urc_user  where compid=:compid  and status=1
     * and userid in (select userid from urc_user_org_map where orgid=:orgid and compid=:compid)
     * and username like '%${PARAMS.username}%'
     * 获取所有用户
     */
    public static String getUrcUsersList = "getUrcUsersList";

    /**
     * 新建党员迁移
     * insert into party_transfer set transfertype=:transfertype,userid=:userid,username=:username,outorgcode=:outorgcode,outorgname=:outorgname
     * ,infoorgcode=:infoorgcode,infoorgname=:infoorgname,status=1,outtime=:outtime,outremark=:outremark,outdworgcode=:outdworgcode
     */
    public static String addPartyTransfer = "addPartyTransfer";

    /**
     * 党员迁移日志
     * insert into  party_transfer_log set transferid=:transferid,userid=:userid,username=:username,orgcode=:orgcode,orgname=:orgname,status=:status,times=:times,remark=:remark,message=:message
     */
    public static String addPartyTransferLog = "addPartyTransferLog";

    /**
     * 党员迁移审核
     * update party_transfer set status=:status,remark3=:remark3
     * ,backremark=:remark3
     * ,outexaminetime=:outexaminetime,ourexamineremark=:remark3
     * ,infoexaminetime=:infoexaminetime,infoexamineremark=:remark3
     * ,infooktime=:infooktime,infookexamineremark=:remark3
     */
    public static String updatePartyTransfer = "updatePartyTransfer";

    /**
     * 删除党小组成员信息
     * delete  from t_party_group_member where memberid =?
     */
    public static String delPartyGroupMember2 = "delPartyGroupMember2";

    /**
     * 删除党费基数
     * delete  from h_dy_jfjs where userid =? and compid=?
     */
    public static String deleteDfjs = "deleteDfjs";

    /**
     * 删除用户
     * delete from  sys_user where deleteSysUser = ?
     */
    public static String deleteSysUser = "deleteSysUser";

    /**
     * 修改用户状态
     * update sys_user set status =? where base_info_id = ?
     */
    public static String updateSysUser = "updateSysUser";

    /**
     * 修改党员删除状态
     * update t_dy_info set delflag = ? where userid = ?
     */
    public static String updateTDyInfo = "updateTDyInfo";

    /**
     * 修改账户状态
     * update urc_user set status where userid = ?
     */
    public static String updateUrcUserStatus = "updateUrcUserStatus";

    public static String updateUrcUserStatus3 = "updateUrcUserStatus3";



    /**
     * 修改所属组织
     * update h_dy_jfjs set organid where userid= ? and compid = ?
     */
    public static String updateDfjsOrgid = "updateDfjsOrgid";

    /**
     * 修改党员所属机构
     * update t_dy_info set  organid =? where  userid = ?
     */
    public static String updateTDyInfoOrganid = "updateTDyInfoOrganid";

    /**
     * 修改用户党组织信息
     * update sys_user set order_id = ?,org_name = ? where base_info_id = ?
     */
    public static String updateSysUserOrgid = "updateSysUserOrgid";

    /**
     * 根据userid查询
     * select * from sys_user where base_info_id =?
     */
    public static String getSysUserList = "getSysUserList";

    /**
     * 查询未完成并有效的申请
     * select * from party_transfer where userid = ? and status2 = 1 and status!=9
     */
    public static String getPartyTransferUser = "getPartyTransferUser";

    /**
     * select * from party_transfer where compid =:compid and status2 = 1
     * and outorgcode=:outorgcode
     * and status =:status
     * and username=:username
     * and outtime between :outtimestart and :outtimeend
     */
    public static String getPartyTransfer = "getPartyTransfer";

    /**
     * select * from party_transfer where compid =:compid and status2 = 1
     * and outorgcode=:outorgcode
     * and status >:status
     * and username=:username
     * and outtime between :outtimestart and :outtimeend
     */
    public static String getPartyTransfer2 = "getPartyTransfer2";

    public static String getPartyTransfer3= "getPartyTransfer3";
    public static String getPartyTransfer4= "getPartyTransfer4";
    public static String getPartyTransfer6= "getPartyTransfer6";
    public static String getPartyTransfer7 = "getPartyTransfer7";

    /**
     * 根据审批状态获取数量
     * select count(*) from  party_transfer where compid =:compid and status2 = 1 and status=:status
     */
    public static String getPartyTransferNum = "getPartyTransferNum";

    /**
     * 查询是否是AB账号
     * select * from  urc_user_account where userid = :userid
     *  and (account = (select CONCAT("A",orgcode) from urc_organization where orgid = (select orgid from urc_user_org_map where userid = :userid))
     *   or account = (select CONCAT("B",orgcode) from urc_organization where orgid = (select orgid from urc_user_org_map where userid =:userid)))
     */
    public static String getABaccountList = "getABaccountList";

    /**
     * 查询是不是党支部数据
     * select * from t_dzz_bzcyxx where userid = (select userid from t_dy_info where zjhm = (select idno from urc_user where userid =:userid))
     * and organid = (select organid from t_dzz_info where organcode = :organcode) and zfbz =0 and dnzwname = '5100000017'
     */
    public static String getdzbsj = "getdzbsj";

    /**
     * 查询最大序号
     * select ifnull(max(number),0) from  party_transfer where years=:years and compid =:compid
     */
    public static String getPartyTransferYearsNum = "getPartyTransferYearsNum";

    /**
     * 插入待办事项
     * insert into party_transfer set category=:category,name=:name,path=:path,state=:state,userid=:userid
     * ,username=:username,time=:time,compid=:compid,bizid=:bizid,orgid=:orgid,deleted=:deleted
     */
    public static String addTodoItem = "addTodoItem";

    /**
     * 删除党员迁移
     * update party_transfer set status2 =1 where id =? and compid =?
     */
    public static String updatePartyTransferStatus2 = "updatePartyTransferStatus2";

    /**
     * 修改待办删除状态
     * update todo_item set deleted = 1 where category=? and bizid = ? and deleted = 0
     */
    public static String updateTodoItemDelete = "updateTodoItemDelete";
    public static String updateTodoItemDelete2 = "updateTodoItemDelete2";
    public static String updateTodoItemDelete3 = "updateTodoItemDelete3";

    /**
     * 根据userId查询党员电话和身份证
     * select lxdh,zjhm from t_dy_info where userid = ?
     */
    public static String queryPhoneByUserId = "queryPhoneByUserId";


    public static String getGDictItem = "getGDictItem";

    /**
     * 根据orgid查询
     *   select * from t_dzz_info_simple where organid = ?
     */
    public static String getTDzzInfoSimpleOrgid = "getTDzzInfoSimpleOrgid";

    /**
     * 新增短息发送
     * insert into sms_send (compid, usermark, phone, status, content, createtime, delflag) VALUES (:compid, :usermark,:phone,:status,:content,:createtime, :delflag)
     *
     */
    public static String addSmsSend = "addSmsSend";

    /**
     * 单位信息表
     *  INSERT INTO t_dw_info(uuid, dwname, dwxzlb,dwjldjczzqk,isfrdw,dwcode, organid,dworder,createtime,syncstatus, zfbz) VALUES (:uuid, :dwname, :dwxzlb,:dwjldjczzqk,:isfrdw,:dwcode, :organid,:dworder,:createtime,:syncstatus, 0)
     */
    public static String saveDwInfo2 = "saveDwInfo2";

    /**
     * 添加单位补充表
     * insert into t_dw_info_bc(uuid,dwlsgx,isfrdw,dwfzr,fzrsfsdy,jjlx,qygm,sfmykjqy,zgzgs,zgzgzgrs,zgzyjsrys,sfjygh,sfzjzz,syncstatus,zfbz) values (:uuid,:dwlsgx,:isfrdw,:dwfzr,:fzrsfsdy,:jjlx,:qygm,:sfmykjqy,:zgzgs,:zgzgzgrs,:zgzyjsrys,:sfjygh,:sfzjzz,'A',0)
     */
    public static String addTDwInfoBc = "addTDwInfoBc";

    /**
     * 查询支部书记
     * select * from t_dzz_bzcyxx where userid =? and organid = ? and zfbz !=1 and dnzwname = '5100000017'
     */
    public static String getTDzzBzcyxx = "getTDzzBzcyxx";

    /**
     * 党组织书记置空
     * update t_dzz_info_bc set dzzsj=null where organid
     */
    public static String updateTDzzInfoBcDzzsj = "updateTDzzInfoBcDzzsj";

    /**
     * 根据orgid查询单位信息
     * select * from t_dw_info where organid = ?
     */
    public static String getTDwInfo = "getTDwInfo";

    /**
     * 修改单位信息
     * update t_dw_info set dwname=:dwcode,dwxzlb=:dwxzlb,dwjldjczzqk=:dwjldjczzqk,isfrdw=:isfrdw,=:dwcode,=:dworder   where uuid=:uuid
     */
    public static String updateTDwInfo = "updateTDwInfo";

    /**
     * updateTDwInfoBc
     * update t_dw_info_bc set dwlsgx=:dwlsgx,isfrdw=:isfrdw,dwfzr=:dwfzr,fzrsfsdy=:fzrsfsdy,jjlx=:jjlx,qygm=:qygm
     * ,sfmykjqy=:sfmykjqy,zgzgs=:zgzgs,zgzgzgrs=:zgzgzgrs,zgzyjsrys=:zgzyjsrys,sfjygh=:sfjygh,sfzjzz=:sfzjzz
     * where uuid=:uuid
     */
    public static String updateTDwInfoBc = "updateTDwInfoBc";

    /**
     * 修改党组织表
     * update t_dzz_info set  dzzmc=:dzzmc,zzlb=:zzlb,dzzlsgx=:dzzlsgx,fax=:fax,lxrname=:lxrname,lxrphone=:lxrphone
     * ,ordernum=:ordernum,updatetime=:updatetime,operorganid=:operorganid,updator=:updator,dzzjc=:dzzjc
     * ,operatorname=:operatorname,dzzszdwqk=:dzzszdwqk,xzqh=:xzqh,expirationdate=:expirationdate where organid = :organid
     */
    public static String updateTDzzInfo = "updateTDzzInfo";

    /**
     * 修改t_dzz_info_simple
     * update t_dzz_info_simple dzzmc=:dzzmc,zzlb=:zzlb,ordernum=:ordernum,updatetime=:updatetime,dzzlsgx=:dzzlsgx
     * ,dzzszdwqk=:dzzszdwqk,xzqh=:xzqh    where organid = :organid
     */
    public static String updateTDzzSimple = "updateTDzzSimple";

    /**
     * 修改党组织补充表
     * update t_dzz_info_bc set fax=:fax,lxrname=:lxrname,lxrphone=:lxrphone,expirationdate=:expirationdate  where organid=:organid
     */
    public static String updateTDzzInfoBc = "updateTDzzInfoBc";

    /**
     * 修改urc_organization表
     * update urc_organization set orgname=:orgname,sorgname=:sorgname,orgseqno=:orgseqno,orgtypeid=:orgtypeid
     * ,updateuserid=:updateuserid,updatetime=:updatetime where orgcode = :orgcode
     */
    public static String updateUrcOrganization = "updateUrcOrganization";


    /**
     * 修改届满日期
     * update t_dzz_hjxx_dzz set jmdate=:jmdate,operator=:operator,operatetime=:operatetime where organid=:organid
     */
    public static String updateTDzzHjxxDzz = "updateTDzzHjxxDzz";

    /**
     * 修改账号联系方式
     * update urc_user set status=:status,phone=:phone where idno=:idno
     */
    public static String updateUrcUserPhone = "updateUrcUserPhone";

    /**
     * 修改状态
     * update urc_user set status = ? where idno = ?
     */
    public static String updateUrcUserStatus2 = "updateUrcUserStatus2";

    /**
     * 修改作废标志
     * update t_dzz_hjxx_dzz set zfbz =? where organid = ?
     */
    public static String updateTDzzHjxxDzzZfbz = "updateTDzzHjxxDzzZfbz";

    /**
     * 修改作废标志
     * update t_dw_info_bc set zfbz = ? where uuid = ?
     */
    public static String updateTDwInfoBcStatus = "updateTDwInfoBcStatus";

    /**
     * 修改作废标志
     * update t_dw_info set zfbz = ? where organid = ?
     */
    public static String updateTDwInfoZfbz = "updateTDwInfoZfbz";

    /**
     * 修改删除标志
     * update t_dzz_info_simple set delflag = ? where organid = ?
     */
    public static String updateTDzzInfoSimpleDeflag = "updateTDzzInfoSimpleDeflag";

    /**
     * 修改删除标志
     * update t_dzz_info set delflag = ? where organid = ?
     */
    public static String updateTDzzInfoDeflag = "updateTDzzInfoDeflag";

    /**
     * 查询党员
     * select *  from  t_dy_info where organid = ? and (delflag = 0 or delflag is null)
     */
    public static String getTDyInfoList3 = "getTDyInfoList3";
    /**
     * 查询党组织转接信息
     * select * from party_transfer where id = ?
     */
    public static String getPartyTransferByID = "select * from party_transfer where id = ?";

    /**
     * 根据userid查询党员基本信息
     * SELECT * FROM `t_dy_info`  WHERE userid =?;
     */
    public static String getTDYInfo = "SELECT * FROM `t_dy_info`  WHERE userid =?";

    /**
     * 根据年份，月份排序 查询党员已经缴费信息最新一条
     * SELECT * FROM `h_dy_jfinfo` WHERE `userid` = ？ AND `actual` <> '0' ORDER BY `year`,`month` DESC
     */
    public static String getHDyIfInfoByUserId = "SELECT * FROM `h_dy_jfinfo` WHERE `userid` = ? AND organid = ? AND `actual` <> '0' ORDER BY `year`,`month` DESC LIMIT 1";

    /**
     * 根据组织编码查询组织书记信息
     * SELECT a.* FROM `t_dzz_bzcyxx` a,t_dzz_info b WHERE a.organid=b.organid AND b.organcode = ? AND a.dnzwmc = ?;
     */
    public static String getDZZBZCYXXBYCode = "SELECT a.* FROM `t_dzz_bzcyxx` a,t_dzz_info b WHERE a.organid=b.organid AND zfbz = 0 AND b.organcode = ? AND a.dnzwmc = ?";
    /**
     * 新增文件<P/>
     * insert into dl_file (compid,name,dir,size,type,usertor,remark,createtime,remark1,remark2,remark3,remark4,remark5,remark6)
     * values (:compid,:name,:dir,:size,:type,:usertor,:remark,:createtime,:remark1,:remark2,:remark3,:remark4,:remark5,:remark6)
     */
    public final static String addDlFile="addDlFile";

    /**
     *insert into t_dy_info set userid = :userid,organid = :organid,xm = :xm,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg
     * ,rdsj = :rdsj,zzsj = :zzsj,dylb = :dylb,isworkers= :isworkers,lxdh = :lxdh,operatetype = :operatetype
     * ,syncstatus = :syncstatus,xb = :xb, gzgw = :gzgw,delflag = :delflag,zjhm =:zjhm,dyzt = :dyzt
     */
    public final static String insert_t_dy_info="insert_t_dy_info";

    /**
     * 新增党员补充表
     * insert into t_dy_info_bc (userid,xw,byyx,zy,jszc,xshjclx,dydaszdw,hjdz_qhnxxdz,syncstatus)
     * values (:userid,:xw,:byyx,:zy,:jszc,:xshjclx,:dydaszdw,:hjdz_qhnxxdz,:syncstatus)
     */
    public final static String addTDyInfoBc="addTDyInfoBc";

    /**
     * 添加党籍和党组织关系
     * insert into t_dy_info_dzz_dj (userid,firstdzz,developUser,zzqk,partyage,correcting,otherpartyorg,joinotherpartyorgdate,outotherpartyorgdate)
     * values (:userid,:firstdzz,:developUser,:zzqk,:partyage,:correcting,:otherpartyorg,:joinotherpartyorgdate,:outotherpartyorgdate)
     */
    public final static String addDjDzz="addDjDzz";


    /**
     * 添加奖惩信息
     * insert into t_dy_jcxx (uuid,userid,jgid,pzjgjb,jcpzjg,pzrq,cxrq,jcmc,jcyy,jcsm,operator,operatetime,sjly,jcqdyswj
     * ,syncstatus,synctime,zfbz,rewardspunishments) values (:uuid,:userid,:jgid,:pzjgjb,:jcpzjg,:pzrq,:cxrq,:jcmc,:jcyy,:jcsm,:operator,:operatetime,:sjly,:jcqdyswj,:syncstatus,:synctime,:zfbz,:rewardspunishments)
     */
    public final static String addTDyJcxx="addTDyJcxx";

    /**
     * 查询党籍和党组织关系
     * select * from t_dy_info_dzz_dj where userid = ?
     */
    public final static String getTDyInfoDzzDj="getTDyInfoDzzDj";

    /**
     * 查询奖惩信息
     * select * from t_dy_jcxx where userid = ? and rewardspunishments = ? and zfbz = 0
     */
    public final static String getTDyJcxx="getTDyJcxx";


    /**
     * 修改党员信息
     * update t_dy_info set xm = :xm,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg,rdsj = :rdsj,zzsj = :zzsj,dylb = :dylb
     * ,isworkers= :isworkers,lxdh = :lxdh,operatetype = 2 xb = :xb, gzgw = :gzgw,zjhm =:zjhm where userid=:userid
     */
    public final static String updateTDyInfo2="updateTDyInfo2";

    /**
     * 修改党员补充表信息
     * update t_dy_info_bc set xw=:xw,byyx=:byyx,zy=:zy,jszc=:jszc,xshjclx=:xshjclx,dydaszdw=:dydaszdw
     * ,hjdz_qhnxxdz=:hjdz_qhnxxdz where userid
     */
    public final static String updateTDyInfoBc="updateTDyInfoBc";

    /**
     * 修改党员信息
     * update sys_user  set   name=:name, work_phone=:work_phone,  remark=:remark, modi_time=modi_time,
     * modifier=:modifier where id =:id
     */
    public final static String upadteSysUser="upadteSysUser";

    /**
     * 修改党籍和党组织关系
     * update t_dy_info_dzz_dj set firstdzz=:firstdzz,developUser=:developUser,zzqk=:zzqk,partyage=:partyage
     * ,correcting=:correcting,otherpartyorg=:otherpartyorg,joinotherpartyorgdate=:joinotherpartyorgdate,outotherpartyorgdate=:outotherpartyorgdate where userid=:userid
     */
    public final static String updateDjDzz="updateDjDzz";

    /**
     * 修改奖惩信息
     * update t_dy_jcxx set jgid=:jgid,pzjgjb=:pzjgjb,jcpzjg=:jcpzjg,pzrq=:pzrq,cxrq=:cxrq,jcmc=:jcmc,jcyy=:jcyy,
     * jcsm=:jcsm,operator=:operator,operatetime=:operatetime,jcqdyswj=:jcqdyswj where uuid=:uuid
     */
    public final static String updateDyJcxx="updateDyJcxx";


    public final static String FZDY_BC_DELETE="FZDY_BC_DELETE";


    /**
     * 删除党籍与党组织关系
     * delete from t_dy_info_dzz_dj where userid = ?
     */
    public final static String delTDyInfoDzzDj="delTDyInfoDzzDj";

    /**
     * 删除奖惩信息
     * update t_dy_jcxx set zfbz = ? and where uuid = ?
     */
    public final static String updateTDyJcxx="updateTDyJcxx";

    /**
     * 获取处分处置原因
     * select * from g_dict_item where  code = ? and parent_code = ? order by  item_code
     */
    public final static String getGDictItem2="getGDictItem2";

    /**
     * 获取处分处置原因
     * select * from g_dict_item where  code = ? and item_code = ? order by  item_code
     */
    public final static String getGDictItemByItemCode="select * from g_dict_item where  code = ? and item_code = ? ";

    /**
     * 获取民族信息
     * SELECT * FROM `g_dict_item` WHERE `item_code` = ？ AND `code` = 'd_national'
     */
    public final static String getMZInfo="SELECT * FROM `g_dict_item` WHERE `item_code` =? AND `code` = 'd_national' ";

    /**
     * 根据证件号码查询
     * select * from t_dy_info where zjhm = ?
     */
    public final static String getTDyInfoZjhm3="getTDyInfoZjhm3";

    /**
     * 根据证件号码查询
     * select * from t_dy_info where zjhm = ?  and userid <> ?
     */
    public final static String getTDyInfoZjhm4="select * from t_dy_info where zjhm = ? and userid <> ?";

    /**
     * 获取附件
     * select * from dl_file where id=:id and compid=:compid
     */
    public final static String getPrintFile = "getPrintFile";

    /**
     * 根据组织code获取组织organId
     * SELECT * FROM t_dzz_info_simple WHERE organcode = ?
     */
    public final static String getDZZInfoSimpleByCode = "SELECT * FROM t_dzz_info_simple WHERE organcode = ? ";

    /**
     * 根据组织organid查询党组织
     * select organcode from t_dzz_info_simple where 1 =1  and ${PARAMS.path}=:organid
     */
    public static final String getDzzByOrganId = "getDzzByOrganId";

    /**
     * 根据上级机构查询
     * select * from t_dzz_info where parentid = ? and (delflag !=1 or delflag is  null)
     */
    public static final String getTDzzInfoParentid = "getTDzzInfoParentid";

    public static final String getPhoneByOrgCode = "getPhoneByOrgCode";

    /**
     * 根据t_dzz_info organid查询
     * select * from urc_organization where orgcode = (select organcode from t_dzz_info where organid = ? )
     */
    public static final String getorg = "getorg";

    /**
     * 删除权限
     * delete from urc_role_map where sjid = ? and orgid = ? and roleid = ? and compid = ?
     */
    public static final String delUrcUserMap = "delUrcUserMap";


    /**
     * 获取党员补充表信息
     * SELECT * FROM `t_dy_info_bc` WHERE `userid` = ?
     */
    public final static String getTDyInfoBc="SELECT count(1) FROM t_dy_info_bc WHERE userid = ? ";

    /**
     * 获取OA账号
     *select * from view_user_account where remarks3 = ?
     */
    public static final String getAccountByRemarks3 = "getAccountByRemarks3";

    /**
     * 根据oa账号获取用户信息
     * select * from view_user_account where remarks3 = ?;
     */
    public static final String getLoginUserByAccount = "getLoginUserByAccount";


    /**
     * 根据oa账号获取用户信息
     * select * from view_user_account where oaaccount = ?;
     */
    public static final String getLoginUserByOAAccount = "getLoginUserByOAAccount";

    /**
     * 查询单位补充表
     * select * from t_dw_info_bc where uuid = ?
     */
    public static final String getTDwInfoBc = "getTDwInfoBc";

    /**
     * 查询党组织
     * select * from t_dzz_info_simple where parentid = ? and (operatetype != 3 or operatetype is null)
     * and zzlb != '3100000025' and zzlb != '3900000026'  and (delflag!=1 or delflag is null) order by ordernum asc,organcode asc
     */
    public static final String getTDzzInfoSimpleTree2 = "getTDzzInfoSimpleTree2";
    public static final String getTDzzInfoSimpleTree = "getTDzzInfoSimpleTree";
    public static final String getTDzzInfoSimpleTree3 = "getTDzzInfoSimpleTree3";

    /**
     * 根据组织查询角色
     * select * from urc_role_map where sjtype = ? and roleid = ? and orgid = ?
     */
    public static final String getRoleOrg = "getRoleOrg";


    /**
     * 党委转接审核列表页
     * select * from party_transfer where compid =:compid and status2 = 1 and outdworgcode =:orgcode and transfertype=3
     * and status in (${PARAMS.status})
     * and username=:username
     * and outtime between :outtimestart and :outtimeend
     */
    public static String getPartyTransferByDW= "getPartyTransferByDW";

    /**
     * 运维导入但党组织数据
     * select * from tem_t_dzz
     */
    public static String getTemTDzz= "getTemTDzz";

    public static String updatePUUsername= "updatePUUsername";
    public static String updatePUUsername2= "updatePUUsername2";

    public static String getItemByItemcode2= "getItemByItemcode2";

    /**
     * INSERT INTO t_dy_info_simple (userid,organid,xm,updatetime,xxwzd,rdsj,delflag,sfzld,xb,mz,dyzt,dylb,zjhm,sortnum,
     * idcardmult,insert_date,idcardvalidity,operatetype,xl,csrq,lkzgzzrq,hjdz_ssxqdm,hjdz_qhnxxdz,sfgarz,iscontact,tsqkrdfs,treecodepath,sync_status,anchor
     * )
     * VALUES(:userid,:organid,:xm,:updatetime,:xxwzd,:rdsj,:delflag,:sfzld,:xb,:mz,:dyzt,:dylb,:zjhm,:sortnum,
     * :idcardmult,:insertDate,:idcardvalidity,:operatetype,:xl,:csrq,:zzsj,:lkzgzzrq,:djdyzt,:hjdzSsxqdm,:hjdzGhnxxdz,:sfgarz,:iscontact,:tsqkrdfs,:treecodepath,:syncStatus,:anchor)
     */
    public static String addTDyInfoSimple ="addTDyInfoSimple";
    /**
     * UPDATE t_dy_info_simple SET organid=:organid,xm=:xm,updatetime=:updatetime,xxwzd=:xxwzd,
     * rdsj=:rdsj,delflag=:delflag,sfzld=:sfzld,xb=:xb,mz=:mz,dyzt=:dyzt,dylb=:dylb,zjhm=:zjhm,
     * sortnum=:sortnum,idcardmult=:idcardmult,insert_date=:insertDate,idcardvalidity=:idcardvalidity,
     * operatetype=:operatetype,xl=:xl,csrq=:csrq,zzsj=:zzsj,lkzgzzrq=:lkzgzzrq,djdyzt=:djdyzt,hjdz_ssxqdm=:hjdzSsxqdm,
     * hjdz_qhnxxdz=:hjdzGhnxxdz,sfgarz=:sfgarz,iscontact=:iscontact,tsqkrdfs=:tsqkrdfs,treecodepath=:treecodepath,
     * sync_status=:syncStatus,anchor=:anchorWHERE (userid = :userid);
     */
    public static String updateTDyInfoSimple="updateTDyInfoSimple";
    /**
     * select * from t_dzz_info_simple where organcode =:organcode  and (operatetype != 3 or operatetype is null) and zzlb = \'6100000035\'
     * and (delflag!=1 or delflag is null) order by ordernum asc,organcode asc
     */
    public static String getTDzzInfoSimpleTree4="getTDzzInfoSimpleTree4";
    /**
     * select * from t_dzz_info_simple where parentcode =:organcode  and (operatetype != 3 or operatetype is null)
     * and zzlb=\'6100000035\'  and (delflag!=1 or delflag is null) order by ordernum asc,organcode asc
     */
    public static String getTDzzInfoSimpleTree5 = "getTDzzInfoSimpleTree5";
    /**
     * 添加先进基层党组织
     * INSERT INTO t_dzz_advanced (orgcode, orgname, type, year, createtime, createuserid,remark, fileid)
     * VALUES (:orgcode, :orgname, :type, :year, :createtime, :createuserid, :remark, :fileid);
     */
    public static String saveDzzAdvanced ="saveDzzAdvanced";
    /**
     * 修改先进基层党组织
     * UPDATE t_dzz_advanced SET type=:type, year=:year,updatetime=:updatetime, updateuserid=:updateuserid, remark=:remark, fileid=:fileid WHERE (id=:id)
     */
    public static String updateDzzAdvanced ="updateDzzAdvanced";
    /**
     * 查看先进基层党组织信息
     * SELECT * FROM t_dzz_advanced WHERE id=?
     */
    public static String getDzzAdvanced="getDzzAdvanced";
    /**
     * 分页查询先进基层党组织信息
     *select * from t_dzz_advanced WHERE 1=1
     * and orgcode in (${PARAMS.orgcode})
     * and year between :startYear and :endYear
     */
    public static String pageDzzAdvanced ="pageDzzAdvanced";
    /**
     * 分页查询先进基层党组织信息
     *select * from t_dzz_advanced WHERE 1=1
     * and orgcode in (${PARAMS.orgcode})
     * and year between :startYear and :endYear
     * and type = ${PARAMS.type}
     * order by year desc
     */
    public static String listDzzAdvanced = "listDzzAdvanced";

    /**
     * 新增优秀党员
     * INSERT INTO t_dy_info_good
     * (userid, orgcode, organname, xm, year, type, creator, createtime,remark, fileids)
     * VALUES
     * (:userid, :orgcode, :organname, :xm, :year, :type, :creator, :createtime,:remark, :fileids)
     */
    public static String insertTDyInfoGood="insertTDyInfoGood";

    /**
     *修改优秀党员
     *UPDATE t_dy_info_good SET userid=:userid, xm=:xm, year=:year, updator=:updator, updatetime=:updatetime, remark=:remark, fileids=:fileids WHERE (id=:id)
     */
    public static String updateTDyIndoGood="updateTDyIndoGood";
    /**
     * 删除优秀党员
     * DELETE FROM t_dy_info_good WHERE id=?
     */
    public static String delTDyIndoGood="delTDyIndoGood";
    /**
     * 查看优秀党员
     * SELECT * FROM t_dy_info_good WHERE id =?
     */
    public static String getTDyIndoGood="getTDyIndoGood";
    /**
     * 分页查询优秀党员信息
     * SELECT * FROM t_dy_info_good WHERE 1=1
     * and orgcode in (${PARAMS.orgcode})
     * and year between :startYear and :endYear
     * and type =:type
     * and xm =:xm
     * order by year desc
     */
    public static String pageDyInfoGood="pageDyInfoGood";

    /**
     * 根据userid，组织编码查询党员优秀信息
     * SELECT * FROM t_dy_info_good WHERE userid=:userid and orgcode=:orgcode order by year desc
     */
    public static String listDyInfoGood="listDyInfoGood";
    /**
     * 添加党组织人员信息子模块
     *INSERT INTO t_dy_info_child_module (orgcode, organname, type, xm, xb, zzmm, csrq, mz, jg, csdz, hyzk, zjhm, jrqtdpsj, zcjb, gwzl, gwmc, qrzxl, qrzbyyx, qrzbyyxzy, qrzxw, zzjyxl, zzjybyyx, zzjybyyxzy, zzjyxw, zjxy, creator, createtime)
     * VALUES (:orgcode, :organname, :type, :xm, :xb, :zzmm, :csrq, :mz, :jg, :csdz, :hyzk, :zjhm, :jrqtdpsj, :zcjb, :gwzl, :gwmc, :qrzxl, :qrzbyyx, :qrzbyyxzy, :qrzxw, :zzjyxl, :zzjybyyx, :zzjybyyxzy, :zzjyxw, :zjxy, :creator, :createtime) ;
     */
    public static String insertTDyInfoChildModule = "insertTDyInfoChildModule";
    /**
     * 编辑党组织人员信息子模块
     * UPDATE t_dy_info_child_module SET  xm=:xm, xb=:xb, zzmm=:zzmm, csrq=:csrq, mz=:mz, jg=:jg, csdz=:csdz,
     * hyzk=:hyzk, zjhm=:zjhm, jrqtdpsj=:jrqtdpsj, zcjb=:zcjb, gwzl=:gwzl, gwmc=:gwmc, qrzxl=:qrzxl, qrzbyyx=:qrzbyyx,
     * qrzbyyxzy=:qrzbyyxzy, qrzxw=:qrzxw, zzjyxl=:zzjyxl, zzjybyyx=:zzjybyyx, zzjybyyxzy=:zzjybyyxzy, zzjyxw=:zzjyxw, zjxy=:zjxy, updator=:updator, updatetime=:updatetime WHERE (id=:id);
     */
    public static String updateDyInfoChildModule ="updateDyInfoChildModule";
    /**
     * 删除党组织人员信息子模块
     * DELETE FROM t_dy_info_child_module WHERE id=?
     */
    public static String delDyInfoChildModule="delDyInfoChildModule";
    /**
     * 查看党组织人员信息子模块
     * SELECT * FROM t_dy_info_child_module WHERE id =?
     */
    public static String getDyInfoChildModule="getDyInfoChildModule";
    /**
     * 分页查询党组织人员信息子模块
     * SELECT * FROM t_dy_info_good WHERE 1=1
     * and orgcode in (${PARAMS.orgcode})
     * and createtime between :startTime and :endTime
     * and type =:type
     * and xm =:xm
     * and zzmm =:zzmm
     * order by year desc
     */
    public static String pageDyInfoChildModule="pageDyInfoChildModule";
    /**
     * 查询党组织人员信息子模块列表
     * SELECT * FROM t_dy_info_good WHERE 1=1
     * and orgcode in (${PARAMS.orgcode})
     * and createtime between :startTime and :endTime
     * and type =:type
     * and xm =:xm
     * and zzmm =:zzmm
     * order by year desc
     */
    public static String listTDyInfoChildModuleReq="listTDyInfoChildModuleReq";
    /**
     * 党建工作经费组织机构树
     * select * from t_dzz_info_simple where 1=1 and (operatetype != 3 or operatetype is null) and zzlb != \'3100000025\' and zzlb != \'3900000026\' and zzlb = \'6100000035\'  and (delflag!=1 or delflag is null)
     * and organcode = :organcode
     * and dzzmc like \'%${PARAMS.likeStr }%\'
     *
     */
    public static String dzzFundingTree="dzzFundingTree";
    /**
     * 查询二级党委
     * select * from t_dzz_info_simple where parentcode =? and zzlb = \'6100000035\'
     * and (operatetype != 3 or operatetype is null) and zzlb != \'3100000025\' and zzlb != \'3900000026\'
     * and (delflag!=1 or delflag is null) order by ordernum asc,organcode asc
     */
    public static String listTDzzInfoSimple="listTDzzInfoSimple";
    /**
     * 添加党建工作经费
     *INSERT INTO t_dzz_funding (orgcode, orgname, type, uselines, usetime, createtime, createuserid,remark, fileid)
     * VALUES (:orgcode, :orgname, :type, :uselines, :usetime, :createtime, :createuserid,:remark, :fileid);
     */
    public static String addDzzFunding="addDzzFunding";
    /**
     * 修改党建工作经费
     *UPDATE t_dzz_funding SET type=:type, uselines=:uselines, usetime=:usetime, updatetime=:updatetime, updateuserid=:updateuserid, remark=:remark, fileid=:fileid WHERE (id=:id);
     */
    public static String updateDzzFunding="updateDzzFunding";
    /**
     * 删除党建工作经费
     *DELETE FROM t_dzz_funding WHERE id=?;
     */
    public static String delDzzFunding ="delDzzFunding";
    /**
     * 查看党建工作经费
     * SELECT * FROM `t_dzz_funding` WHERE id=?;
     */
    public static String getDzzFunding="getDzzFunding";
    /**
     * 分页党建工作经费
     *SELECT * FROM `t_dzz_funding` WHERE 1=1
     * and orgcode in (${PARAMS.orgcode})
     * and usetime between :startTime and :endTime
     * and type =:type
     * order by createtime desc
     */
    public static String pageDzzFunding="pageDzzFunding";
    /**
     * 统计活动阵地
     * select count(1) from t_dzz_info_simple where operatetype !=3   and (delflag!=1 or delflag is null) and activitypositions=1
     * and ${PARAMS.path} = :organid
     */
    public static String getDzzCountByActivity="getDzzCountByActivity";
    /**
     * 党建工作经费统计
     * SELECT SUM(uselines) uselines FROM `t_dzz_funding` WHERE DATE_FORMAT(usetime,\'%Y-%m\')=:time AND FIND_IN_SET(orgcode,:orgcode)
     */
    public static String dzzFundingStatistica="dzzFundingStatistica";
    /**
     * 党员党龄统计
     * select count(*) count from t_dy_info where dyzt = '1000000003' and dylb in ('1000000001', '2000000002') and (delflag != 1 or delflag is null)
     * and TimeStampDiff(YEAR,rdsj,zzsj) > :start and TimeStampDiff(YEAR,rdsj,zzsj) <= :end
     * and organid in ('a544d1ce4000464c88da687438e1048c')
     */
    public static String partyAgeInfo="partyAgeInfo";
    /**
     * 根据角色名称查询角色信息
     */
    public static String getRoleBYRoleName="select * from urc_role where compid = 1 and rolename = ? and roletype != 9 limit 1";
    /**
     * 查询当前组织下的党小组下拉列表
     * SELECT id,name FROM `t_party_group` where orgcode = ? and compid = ?
     */
    public final static String getPartyGroupSel = "getPartyGroupSel";
}

