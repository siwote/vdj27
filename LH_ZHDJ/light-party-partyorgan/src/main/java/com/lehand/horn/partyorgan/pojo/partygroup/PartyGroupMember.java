package com.lehand.horn.partyorgan.pojo.partygroup;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="党小组成员表", description="党小组成员表")
public class PartyGroupMember implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="自增长主键", name="id")
    private Long id;

    @ApiModelProperty(value="党员ID", name="memberid")
    private String memberid;

    @ApiModelProperty(value="党小组ID", name="groupid")
    private Long groupid;

    @ApiModelProperty(value="党员名称", name="membername")
    private String membername;

    @ApiModelProperty(value="组织编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="组织名称", name="orgname")
    private String orgname;

    @ApiModelProperty(value="党小组名称", name="groupname")
    private String groupname;

    @ApiModelProperty(value="小组内职位（0 成员 ； 1 组长）", name="type")
    private Integer type;

    @ApiModelProperty(value="身份证号", name="idcard")
    private String idcard;

    @ApiModelProperty(value="租户ID", name="compid")
    private Long compid;

    @ApiModelProperty(value="联系电话", name="lxdh")
    private String lxdh;

    @ApiModelProperty(value="职务名称", name="dnzwmc")
    private String dnzwmc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public Long getGroupid() {
        return groupid;
    }

    public void setGroupid(Long groupid) {
        this.groupid = groupid;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getLxdh() {
        return lxdh;
    }

    public void setLxdh(String lxdh) {
        this.lxdh = lxdh;
    }

    public String getDnzwmc() {
        return dnzwmc;
    }

    public void setDnzwmc(String dnzwmc) {
        this.dnzwmc = dnzwmc;
    }
}