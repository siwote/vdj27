package com.lehand.horn.partyorgan.controller.dzz;

import java.util.List;
import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.horn.partyorgan.controller.HornBaseController;
import com.lehand.horn.partyorgan.pojo.TreeNode;
import com.lehand.horn.partyorgan.service.GdictItemService;

/**
 * 公共数据字典控制层
 * @author taogang
 * @version 1.0
 * @date 2019年1月22日, 上午8:53:35
 */
@Api(value = "公共数据字典控制层", tags = { "公共数据字典控制层" })
@RestController
@RequestMapping("/lhdj/dict")
public class GdictItemController extends HornBaseController{
	
	private static final Logger logger = LogManager.getLogger(GdictItemController.class);
	
	@Resource
	private GdictItemService gdictItemService;
	/**
	 * 查询下来框字典列表
	 * @param code
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "查询下来框字典列表", notes = "查询下来框字典列表", httpMethod = "POST")
	@RequestMapping("/item")
	public Message dict(String code) {
		Message message = new Message();
		//判断code 是否为空
		if(StringUtils.isEmpty(code)) {
			message.setMessage("参数错误，code编码不能为空");
			return message;
		}
		try {
			List<TreeNode> list = gdictItemService.listItem(code);
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("参数字典查询出错");
		}
		return message;
	}
	
}
