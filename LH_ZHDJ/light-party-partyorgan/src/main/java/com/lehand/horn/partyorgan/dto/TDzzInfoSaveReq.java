package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dzz.TDwInfo;
import io.swagger.annotations.ApiModelProperty;

/**
 * @program: huaikuang-dj
 * @description: TDzzInfoSaveReq
 * @author: zwd
 * @create: 2020-12-01 11:00
 */
public class TDzzInfoSaveReq {
    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="党组织基本信息", name="tdzzinfo")
    private TDzzInfoSimpleReq tdzzinfo;

    @ApiModelProperty(value="所在单位", name="tdwinfo")
    private TDwInfoReq tdwinfo;

    public TDzzInfoSimpleReq getTdzzinfo() {
        return tdzzinfo;
    }

    public void setTdzzinfo(TDzzInfoSimpleReq tdzzinfo) {
        this.tdzzinfo = tdzzinfo;
    }

    public TDwInfoReq getTdwinfo() {
        return tdwinfo;
    }

    public void setTdwinfo(TDwInfoReq tdwinfo) {
        this.tdwinfo = tdwinfo;
    }
}