package com.lehand.horn.partyorgan.service.jobservice;


import com.alibaba.fastjson.JSON;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class AddElectionRemindService {
    //记录日志
	private static final Logger logger = LogManager.getLogger(AddElectionRemindService.class);
	//是否正在创建任务
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);
	//线程池
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	@Transactional(rollbackFor=Exception.class)
	public void addElectionRemind() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			List<Map<String,Object>> lists = generalSqlComponent.query(SqlCode.listElectionRemindTime,new Object[]{1, DateEnum.YYYYMMDDHHMMDD.format(),2});
			logger.info("定时创建换届提醒开始...");
			for (final Map<String,Object> map : lists) {
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						try {
							//查询所有接收人的集合
							Map data = generalSqlComponent.query(SqlCode.getConfigPeople,new Object[]{1,map.get("orgcode")});
							List<Map> lists = JSON.parseArray(data.get("remindpeople").toString(),Map.class);
							if(CollectionUtils.isEmpty(lists)){
								return;
							}
							Map param = getStringObjectMap(Integer.valueOf(map.get("remarks1").toString()));
							for (Map<String,Object> list : lists) {
								//通过32位userid查询urc_user表中对应的userid
								Map map = generalSqlComponent.query(SqlCode.getUrcUserid,new Object[]{list.get("userid")});
								if(map != null && map.get("userid") != null){
									param.put("userid",map.get("userid"));
									param.put("username",list.get("xm"));
									generalSqlComponent.insert(SqlCode.addTTRLByTuihui, param);
								}
							}
							modiElectionRemindTimeStatus(1L,map.get("orgid").toString(),map.get("remindtime").toString());
						} catch (Exception e) {
							logger.error("创建会议待办异常",e);
						}
					}
				});
			}
			logger.info("定时创建会议待办结束...");
		} catch (Exception e) {
			logger.error("定时创建会议待办异常",e);
		} finally {
			CREATE.set(false);
		}
	}

	/**
	 * 组装消息参数
	 * @param date
	 * @return
	 */
	private Map getStringObjectMap(Integer date) {
		Map param = new HashMap();
		param.put("compid",1L);
		param.put("noteid",-1L);
		param.put("module",100);//模块(0:任务发布,1:任务反馈)
		param.put("mid",-1L);//对应模块的ID
		param.put("remind",DateEnum.YYYYMMDDHHMMDD.format(new Date()));//提醒日期
		param.put("rulecode","ELECTION_REMIND");//编码
		param.put("rulename","换届提醒");
		param.put("optuserid",0L);
		param.put("optusername","系统消息");
		param.put("isread",0);//
		param.put("ishandle",0);//
		param.put("issend",0);//是否已提醒(0:未提醒,1:已提醒)
		param.put("mesg","班子任期即将届满（剩余"+date+"个月），请及时换届");//消息
		param.put("remarks1",1);//备注1
		param.put("remarks2",0);//备注2
		param.put("remarks3","换届提醒");//备注3
		param.put("remarks4","");//备注3
		param.put("remarks5","");//备注3
		param.put("remarks6","");//备注3
		param.put("remark","");
		return param;
	}

	/**
	 * 更新时间状态为创建完成
	 * @param compid
	 * @param orgid
	 * @param remindtime
	 */
	private void modiElectionRemindTimeStatus(Long compid,String orgid,String remindtime) {
		generalSqlComponent.update(SqlCode.modiElectionRemindTimeStatus,new Object[]{compid,orgid,remindtime});
	}



	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
