package com.lehand.horn.partyorgan.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * 拼接业务树类
 * @author taogang
 * @version 1.0
 * @date 2019年1月22日, 下午5:18:00
 */
public class TreeNode {
	
	private String lable;
	
	private String value;
	
	private String pid;
	
	private List<TreeNode> children = new ArrayList<>();
	  
//    public TreeNode(String lable, String value, String pid) {
//        this.lable = lable;
//        this.value = value;
//        this.pid = pid;
//    }

	public String getLable() {
		return lable;
	}

	public void setLable(String lable) {
		this.lable = lable;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

}
