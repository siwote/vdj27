package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.TDyInfoGood;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 优秀共产党员与优秀党务工作者表扩展类
 * @author lx
 */
@ApiModel(value="优秀共产党员与优秀党务工作者表扩展类",description="优秀共产党员与优秀党务工作者表扩展类")
public class TDyInfoGoodReq extends TDyInfoGood {


    @ApiModelProperty(value="附件", name="attachList")
    private List<Map<String,Object>> attachList;

    @ApiModelProperty(value="导出列名数据",name="exportColumns")
    private List<ExportColumnReq> exportColumns;

    @ApiModelProperty(value="开始年份",name="startYear")
    private String startYear;

    @ApiModelProperty(value="结束年份",name="endYear")
    private String endYear;

    public List<ExportColumnReq> getExportColumns() {
        return exportColumns;
    }

    public void setExportColumns(List<ExportColumnReq> exportColumns) {
        this.exportColumns = exportColumns;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public List<Map<String, Object>> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<Map<String, Object>> attachList) {
        this.attachList = attachList;
    }
}
