package com.lehand.horn.partyorgan.service.info;

import com.lehand.base.exception.LehandException;
import com.lehand.horn.partyorgan.constant.Constant;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

@Service
public class HornExcelService {

    /**
     * 根据编码下载Excel模板
     * @param code
     * @return
     */
    public String getExcelTemplate(String code, HttpServletResponse response) {
        if(StringUtils.isEmpty(code)) {
            LehandException.throwException("文件编码不能为空，请重试！");
        }

        ClassPathResource classPathResource = new ClassPathResource(String.format("exceltemplates/%s", getFileName(code)));
        ByteArrayOutputStream  baos = null;
        try {
            InputStream inputStream =classPathResource.getInputStream();
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int num = inputStream.read(buffer);
            while (num != -1) {
                baos.write(buffer, 0, num);
                num = inputStream.read(buffer);
            }
            baos.flush();
            inputStream.close();
            response.reset();
            //八进制输出流
            response.setContentType("application/octet-stream;charset=UTF-8");
            //这后面可以设置导出Excel的名称
            response.setHeader("Content-disposition", String.format("attachment;filename=%s", URLEncoder.encode(getFileName(code),"UTF-8")));
            response.flushBuffer();
            ServletOutputStream out = response.getOutputStream();
            out.write(baos.toByteArray());
            out.flush();
            out.close();
        } catch (IOException e) {
            LehandException.throwException("文件不存在！");
            e.printStackTrace();
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    /**
     * 通过编码获取文件
     * @param code
     * @return
     */
    public String getFileName(String code) {
        switch (code) {
            case "DZZ_EXCEL_IMPORT":
                return Constant.ExcelTemplate.DZZ_EXCEL_IMPORT;
            case "DY_EXCEL_IMPORT":
                return Constant.ExcelTemplate.DY_EXCEL_IMPORT;
            case "DF_EXCEL_IMPORT":
                return Constant.ExcelTemplate.DF_EXCEL_IMPORT;
            default:
                LehandException.throwException("编码不存在，请重试！");
        }
        return "000000";
    }

}
