package com.lehand.horn.partyorgan.service.hjxx;


import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.HJXXSqlCode;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.TDzzHjxxInfoDto;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfo;
import com.lehand.horn.partyorgan.pojo.hjxx.TDzzHjxxRemindConfig;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.module.common.feign.UserRoleOrg;
import com.lehand.module.urc.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrgTransitionService {

    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    private RoleService roleService;
    @Resource
    private BaseDzzTotalService baseDzzTotalService;

    public Pager page(Session session, Pager pager, String orgcode) throws ParseException {
        Map<Object, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", orgcode);
        generalSqlComponent.pageQuery(HJXXSqlCode.pageHJXX, paramMap, pager);
        List<TDzzHjxxInfoDto> listHJXX = (List<TDzzHjxxInfoDto>) pager.getRows();
        if(listHJXX.size()<0){
            return pager;
        }
        int size = listHJXX.size();
        int count=1;
        for(int i=0;i<size;i++){
            count++;
            TDzzHjxxInfoDto dto = listHJXX.get(i);
            //延期
            if (2 == dto.getStatus()) {
                //获取延期历史信息
                dto.setDelayList(generalSqlComponent.query(HJXXSqlCode.listDelayInfo, new Object[]{dto.getId()}));
            }
            //获取换届周期配置日志
            Map<String,Object> remindConfigLog=generalSqlComponent.query(HJXXSqlCode.getRemindConfigLog, new Object[]{dto.getId()});
            dto.setRemindConfigLog(remindConfigLog);
            //党组织名称
            dto.setOrgname(getTDzzInfoByOrgCode(dto.getOrgcode()).getDzzmc());
            //判断最新一条换届状态
            if(0==i){

                //当前状态
                long status = dto.getStatus();

                //当前届满时间
                String plandate = dto.getPlandate();
                long planTime = DateEnum.YYYYMMDDHHMMDD.parse(plandate).getTime();
                //当前时间戳
                long nowTime = System.currentTimeMillis();
                //状态 正常 到期未换届  延期
                if(1==status){
                    if(planTime<nowTime){
                        dto.setStatus(3);//到期未换届
                    }
                    //修改换届状态
                    generalSqlComponent.update(HJXXSqlCode.updateStatusByHjxxInfo,dto);
                }
            }
            //添加实际的换届时间
            if(i>0){
                TDzzHjxxInfoDto nextDto = listHJXX.get(i - 1);
                dto.setActualTime(nextDto==null?null:nextDto.getActualdate());
            }else{
                dto.setActualTime(null);
            }
        }

        return pager;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean modify(TDzzHjxxInfoDto dto, Session session) throws Exception {
        //校验必填参数 start
        checkParam(dto);
        //end
        //正常
        //查看是否已经存在换届记录  查询最新一届记录
        TDzzHjxxInfoDto hjxxInfoDto= generalSqlComponent.query(HJXXSqlCode.getHJXXLimit1,new Object[]{dto.getOrgcode()});
        //创建时间和创建人
        dto.setCreateuserid(String.valueOf(session.getUserid()));
        dto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        //更新时间和更新人
        dto.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dto.setUpdateuserid(String.valueOf(session.getUserid()));
        //是否已经有存在换届记录
        if(hjxxInfoDto==null){
            //添加换届提醒配置
            generalSqlComponent.insert(HJXXSqlCode.insertHjxxRemindConfig,dto);
            //添加换届相关信息及配置
            addHJXX(dto);
            return true;
        }
        long oldId = hjxxInfoDto.getId();//上一届id
        //当前换届日期
        String actualdate = dto.getActualdate();
        long actualTime = DateEnum.YYYYMMDDHHMMDD.parse(actualdate).getTime();
        //当前届满时间
        String plandate = dto.getPlandate();
        long planTime = DateEnum.YYYYMMDDHHMMDD.parse(plandate).getTime();
        //上一届换届日期
        String oldActualdate = dto.getActualdate();
        long oldActualTime = DateEnum.YYYYMMDDHHMMDD.parse(oldActualdate).getTime();
        //上一届届满时间
        String oldPlandate = hjxxInfoDto.getPlandate();
        long oldPlanTime = DateEnum.YYYYMMDDHHMMDD.parse(oldPlandate).getTime();
        //检验时间
        if(actualTime>planTime){
            LehandException.throwException("换届日期不能大于届满日期");
        }
        if(actualTime>oldActualTime){
            LehandException.throwException("当前换届日期不能上一届换届日期");
        }
        //上一届状态
        long oldStatus = hjxxInfoDto.getStatus();
        Map<String, Object> delayInfoMap =new HashMap<>();//上一届最新一次延期记录
        if(2==oldStatus){//上一届延期
            List<Map<String,Object>> listDelayInfo = generalSqlComponent.query(HJXXSqlCode.listDelayInfo, new Object[]{dto.getId()});
            if(listDelayInfo.size()<1){
                LehandException.throwException("上一届延期记录不在");
            }
            delayInfoMap = listDelayInfo.get(0);
        }
        //正常
        if(1==dto.getStatus()){
            //修改换届提醒配置
            generalSqlComponent.update(HJXXSqlCode.updateHjxxRemindConfig,dto);
            TDzzHjxxInfoDto remindConfig = generalSqlComponent.query(HJXXSqlCode.getHjxxRemindConfig, new Object[]{dto.getOrgcode()});
            if(remindConfig==null){
                LehandException.throwException("上一届换届提醒周期没有配置");
            }
            //添加换届提醒周期配置日志
            Map<Object, Object> paramMap = new HashMap<>();
            paramMap.put("hjxxid",hjxxInfoDto.getId());
            paramMap.put("remindpeople",remindConfig.getRemindpeople());
            paramMap.put("reminddate",remindConfig.getReminddate());
            generalSqlComponent.insert(HJXXSqlCode.insertRemindConfigLog,paramMap);
            //添加换届相关信息及配置
            addHJXX(dto);
            if(delayInfoMap.size()<1 ){//上一届没有延期
                //在时间之内  正常
                if(actualTime>oldActualTime&&actualTime<oldPlanTime){
                    //修改上一届为 正常
                    hjxxInfoDto.setStatus(1);
                }
                //在时间之后  到期未换届
                if(actualTime>oldPlanTime){
                    //修改上一届为 到期未换届
                    hjxxInfoDto.setStatus(3);
                }
            }else{
                //最新一条延期记录届满时间
                String oldDelaydate = String.valueOf(delayInfoMap.get("delaydate"));
                long oldDelaydateTime = DateEnum.YYYYMMDDHHMMDD.parse(oldDelaydate).getTime();
                //在时间之后  到期未换届
                if(actualTime>oldDelaydateTime){
                    //修改上一届为 到期未换届
                    hjxxInfoDto.setStatus(3);
                }
            }
        }
        //延期 添加延期记录
        if(2==dto.getStatus()){
            dto.setId(oldId);
            //添加延期记录
            generalSqlComponent.insert(HJXXSqlCode.insertDelayInfo,dto);
            //删除之前提醒时间节点
            generalSqlComponent.delete(HJXXSqlCode.deleteHjxxRemindTime,dto);
            //添加换届提醒时间点
            addHjxxRemindTime(dto);
            //修改上一届换届状态
            hjxxInfoDto.setStatus(dto.getStatus());
        }
        //修改上一届换届状态
        generalSqlComponent.update(HJXXSqlCode.updateStatusByHjxxInfo,hjxxInfoDto);
        return true;
    }

    /**
     *添加换届相关信息及配置
     * @param dto
     */
    private void addHJXX(TDzzHjxxInfoDto dto) throws ParseException {
        //添加换届提醒时间点
        addHjxxRemindTime(dto);
        //插入换届信息
        generalSqlComponent.insert(HJXXSqlCode.insertHjxxInfo,dto);
    }

    /**
     * 添加换届时间节点
     * @param dto
     */
    private void addHjxxRemindTime(TDzzHjxxInfoDto dto) throws ParseException {
        //添加换届提醒时间点
        String[] date = dto.getReminddate().split(",");//换届周期
        String remindtime = "";
        for (String s : date) {
            remindtime = getPrevMonthDate(DateEnum.YYYYMMDDHHMMDD.parse(dto.getPlandate()),Integer.valueOf(s));
            if(!StringUtils.isEmpty(remindtime)){
                dto.setRemindtime(remindtime+ " 09:00:00");
                dto.setDistance(Integer.parseInt(s));
                generalSqlComponent.insert(HJXXSqlCode.insertHjxxRemindTime,dto);
            }
        }
    }

    public String getPrevMonthDate(Date date, int n) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - n);
        return DateEnum.YYYYMMDD.parse(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()))
                .before(DateEnum.YYYYMMDD.parse(DateEnum.YYYYMMDD.format())) ?
                "" : new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }
    /**
     * 获取党组织信息
     * @param orgcode
     * @return
     */
    private TDzzInfo getTDzzInfoByOrgCode(String orgcode) {
        Map<String, Object> map = new HashMap<>();
        map.put("organcode",orgcode);
        TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
        if(tDzzInfo==null){
            LehandException.throwException("不存在当前党组织");
        }
        return tDzzInfo;
    }

    /**
     * 校验必填参数
     * @param dto
     */
    private void checkParam(TDzzHjxxInfoDto dto) {
        if (StringUtils.isEmpty(dto.getPlandate())) {
            LehandException.throwException("届满时间不能为空！");
        }
        if (StringUtils.isEmpty(dto.getActualdate())) {
            LehandException.throwException("换届时间不能为空！");
        }
        if (StringUtils.isEmpty(dto.getOrgid())) {
            LehandException.throwException("组织id不能为空！");
        }
        if (StringUtils.isEmpty(dto.getOrgcode())) {
            LehandException.throwException("组织编码code不能为空！");
        }
        if (StringUtils.isEmpty(dto.getStatus())) {
            LehandException.throwException("状态参数不能为空！");
        }
        if (!StringUtils.isEmpty(dto.getReminddate()) || !StringUtils.isEmpty(dto.getRemindpeople())) {
            if (StringUtils.isEmpty(dto.getReminddate()) || StringUtils.isEmpty(dto.getRemindpeople())) {
                LehandException.throwException("提醒人或提醒周期不能为空！");
            }
        }
        if (2 == dto.getStatus() && StringUtils.isEmpty(dto.getContent())) {
            LehandException.throwException("延迟原因不能为空！");
        }
    }

    public TDzzHjxxInfoDto getLatestHJXXInfo(String orgcode, Session session) {
        //查看是否已经存在换届记录  查询最新一届记录
        TDzzHjxxInfoDto hjxxInfoDto= generalSqlComponent.query(HJXXSqlCode.getHJXXLimit1,new Object[]{orgcode});
        if(hjxxInfoDto==null){
            return null;
        }
        TDzzHjxxInfoDto remindConfig = generalSqlComponent.query(HJXXSqlCode.getHjxxRemindConfig, new Object[]{orgcode});
        if(remindConfig==null){
           LehandException.throwException("换届提醒周期没有配置");
        }
        hjxxInfoDto.setRemindpeople(remindConfig.getRemindpeople());
        hjxxInfoDto.setReminddate(remindConfig.getReminddate());
        //获取最新延迟记录
        hjxxInfoDto.setDelayList(generalSqlComponent.query(HJXXSqlCode.listDelayInfo, new Object[]{hjxxInfoDto.getId()}));
        return hjxxInfoDto;
    }

    /**
     *
     * @param orgcode 组织编码
     * @param rolename 角色名称
     * @param type 1：查询当前组织角色。2：查询上级组织用户
     * @return
     */
    public List<UserRoleOrg> pageRole(String orgcode, String rolename,int type) {
        Map<String,Object> orgInfo = generalSqlComponent.query(SqlCode.queryPorgcode ,new HashMap<String,Object>(){{
            put("organcode", orgcode);
        }});
        if(orgInfo==null){
            LehandException.throwException(orgcode+"组织不存在！");
        }
        long orgid = (long) orgInfo.get("orgid");
        if(2==type){
            orgid=(long) orgInfo.get("porgid");
            if(0==orgid){
                return null;
            }
        }
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap(SqlCode.getRoleBYRoleName, new Object[]{rolename});
        if(map==null){
            LehandException.throwException(rolename+"角色不存在！");
        }
        //获取组织用户信息
        List<UserRoleOrg> roleUserListByCodeAndOrgid = roleService.getRoleUserListByCodeAndOrgid(1L, orgid, String.valueOf(map.get("rolecode")));
        return roleUserListByCodeAndOrgid;
    }

    public void update(TDzzHjxxInfoDto dto, Session session) throws ParseException {
        //校验必填参数
        if(StringUtils.isEmpty(dto.getId())){
            LehandException.throwException("id不能为空！");
        }
        if(StringUtils.isEmpty(dto.getRemindpeople())){
            LehandException.throwException("提醒人不能为空！");
        }if(StringUtils.isEmpty(dto.getReminddate())){
            LehandException.throwException("提醒周期不能为空！");
        }
        //创建时间和创建人
        dto.setCreateuserid(String.valueOf(session.getUserid()));
        dto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());

        dto.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dto.setUpdateuserid(String.valueOf(session.getUserid()));
        //删除之前提醒时间节点
        generalSqlComponent.delete(HJXXSqlCode.deleteHjxxRemindTime,dto);
        //添加换届提醒时间点
        addHjxxRemindTime(dto);
        //修改换届周期配置
        generalSqlComponent.update(HJXXSqlCode.updateHjxxRemindConfig,dto);
    }

    public List<Map<String,Object>> statisticalHJXX(String orgid, Session session) {
        if(StringUtils.isEmpty(orgid)){
            orgid = generalSqlComponent.getDbComponent().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{session.getCurrentOrg().getOrgcode()});
        }
        if(StringUtils.isEmpty(orgid)){
            LehandException.throwException("必传参数组织id不能为空");
        }
        String path = baseDzzTotalService.getPath(orgid);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("path",path);
        paramMap.put("orgid",orgid);
        List<Map<String,Object>> list = generalSqlComponent.query(HJXXSqlCode.statisticalHJXX,paramMap);
        Map<String, Object> m1 = new HashMap<>();
        m1.put("name","正常");
        m1.put("value",0);
        Map<String, Object> m2 = new HashMap<>();
        m2.put("name","延期换届");
        m2.put("value",0);
        Map<String, Object> m3 = new HashMap<>();
        m3.put("name","到期未换届");
        m3.put("value",0);
        if(list!=null&&list.size()>0){
            for (Map<String,Object> map:list) {
                String status = String.valueOf(map.get("status"));
                Object count = map.get("count");
                if(MagicConstant.NUM_STR.NUM_1.equals(status)){
                    m1.put("value",count);
                }
                if(MagicConstant.NUM_STR.NUM_2.equals(status)){
                    m2.put("value",count);
                }
                if(MagicConstant.NUM_STR.NUM_3.equals(status)){
                    m3.put("value",count);
                }
            }
        }
        List<Map<String,Object>> resultList = new ArrayList<>();
        resultList.add(m1);
        resultList.add(m2);
        resultList.add(m3);
        return resultList;
    }
}
