package com.lehand.horn.partyorgan.service.dyqy;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.BeanUtil;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.document.lib.service.FileDelService;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.Constant;
import com.lehand.horn.partyorgan.constant.HJXXSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dto.PartyTransferDto;
import com.lehand.horn.partyorgan.dto.PartyTransferDto2;
import com.lehand.horn.partyorgan.dto.TDzzHjxxInfoDto;
import com.lehand.horn.partyorgan.pojo.SysUser;
import com.lehand.horn.partyorgan.pojo.dy.PartyTransferLog;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
import com.lehand.horn.partyorgan.service.DataSynchronizationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @program: huaikuang-dj
 * @description: PartyTransferService
 * @author: zwd
 * @create: 2020-11-25 15:16
 */
@Service
public class PartyTransferService {
    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    DataSynchronizationService dataSynchronizationService;
    @Resource
    FileDelService fileDelService;

    @Resource
    private DFSComponent dFSComponent;
    @Resource
    private PartyCacheComponent partyCacheComponent;

    @Value("${upload-path}")
    private String uploadTempPath;

    //预备党员
    @Value("${jk.organ.yubeidangyuan}")
    private String yubeidangyuan;
    //正式党员
    @Value("${jk.organ.zhengshidangyuan}")
    private String zhengshidangyuan;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;
    /**
    * @Description 创建党员迁移
    * @Author zwd
    * @Date 2020/11/27 10:31
    * @param
    * @return
    **/
    @Transactional(rollbackFor = Exception.class)
    public Boolean savePartyTransfer(PartyTransferDto bean, Session session){
        //验证  该党员未走完迁移流程不可再申请
        Map<String,Object>  data = generalSqlComponent.query(SqlCode.getPartyTransferUser,new Object[]{bean.getUserid()});
        if(data!=null){
            LehandException.throwException("当前党员正在迁移中");
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        bean.setOutorgname(bean.getOutorgname());
        bean.setOutorgcode(bean.getOutorgcode());
        bean.setOuttime(sdf.format(new Date()));
        bean.setCompid(session.getCompid());
        bean.setStatus(1);
        String years = sdf.format(new Date()).substring(0,4);
        bean.setYears(years);
        //查询党员
        Map<String,Object>  dy = generalSqlComponent.query(SqlCode.getTDyInfo2,new Object[]{bean.getUserid()});
        bean.setIdcode(dy.get("zjhm").toString());
        Integer number = generalSqlComponent.query(SqlCode.getPartyTransferYearsNum,new Object[]{years,session.getCompid()});
        bean.setNumber(number+1);
        //迁出的最近的党委
        Map<String, Object> parentPartyOrganCode = getParentPartyOrganCode(bean.getOutorgcode());
        bean.setOutdworgcode(parentPartyOrganCode==null?null:(String) parentPartyOrganCode.get("organcode"));
        //初始化党员迁移数据
        Long id = generalSqlComponent.insert(SqlCode.addPartyTransfer,bean);
        bean.setId(Integer.valueOf(id.toString()));

        //生成党员迁移日志
        partyTransferLog(bean,session,-1,null,"创建党员迁移");
        partyTransferLog(bean,session,-1,null,"待迁出党支部书记审核");
        //添加待办  迁出党支部书记
        addTodoItem(session, sdf, id,session.getCurrentOrg().getOrgcode(),bean.getUsername()+"(转出)",bean.getStatus(),"/affairsParty/orgApprove");
        return true;
    }

    private void addTodoItem(Session session, SimpleDateFormat sdf, Long id,String orgcode,String name,Integer node,String path) {
        Map<String,Object> todoItem = new HashMap<>(14);
        todoItem.put("category",10);
        todoItem.put("name",name);
        todoItem.put("path",path);
        todoItem.put("state",0);
        todoItem.put("userid",null);
        todoItem.put("username",null);
        todoItem.put("time", sdf.format(new Date()));
        todoItem.put("compid", session.getCompid());
        todoItem.put("bizid", id);
        todoItem.put("orgid", orgcode);
        todoItem.put("deleted",0);
        todoItem.put("node",node);
        generalSqlComponent.query(SqlCode.addTodoItem,todoItem);
    }

    /**
    * @Description 党员迁移审核
    * @Author zwd
    * @Date 2020/11/27 10:32
    * @param
    * @return
    **/
    @Transactional(rollbackFor = Exception.class)
    public Boolean updatePartyTransfer(PartyTransferDto bean, Session session){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap(SqlCode.getPartyTransferByID, new Object[]{bean.getId()});
        if(map!=null){
            String s = String.valueOf(map.get("status"));
            if("0".equals(s)||"9".equals(s)){
                LehandException.throwException("当前党员已经被其他角色审核，请勿重新操作！");
            }
        }
        if(bean.getStatus()==0){//驳回
            if(bean.getRemark1()==1){
                bean.setOutexaminetime(sdf.format(new Date()));
            }
            if(bean.getRemark1()==2){
                bean.setInfoexaminetime(sdf.format(new Date()));
            }
            if(bean.getRemark1()==4){
                bean.setOutdwexaminetime(sdf.format(new Date()));
            }
            if(1==bean.getJtdworgcode()){//一级集团党委直接确认
                bean.setJtdworgcode(1);
                bean.setJtdwexaminetime(sdf.format(new Date()));
                bean.setJtdwremark(bean.getJtdwremark());
            }
            partyTransferLog(bean,session,bean.getStatus(),null,"党员迁移申请被驳回");
            //删除待办
            generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
            //添加待办  迁出党支部书记
            addTodoItem(session, sdf, Long.valueOf(bean.getId()),bean.getOutorgcode(),bean.getUsername()+"(退回)",bean.getStatus(),"/affairsParty/orgTransfer");
        }else if(bean.getStatus()==2){//迁出审核
            if(bean.getTransfertype()==3){
                //最近的党委
                if(StringUtils.isEmpty(bean.getOutdworgcode())){
                    bean.setStatus(9);
                    bean.setOutexaminetime(sdf.format(new Date()));
                    partyTransferLog(bean,session,bean.getStatus(),null,"迁出书记审核");
                    partyTransferLog(bean,session,bean.getStatus(),null,"最近的上级党委不存在已存在,完成");
                    //迁出集团外 到此结束
                    //处理党小组
                    generalSqlComponent.delete(SqlCode.delPartyGroupMember2,new Object[]{bean.getUserid()});
                    //处理班子成员
                    Map<String,Object> data = generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{bean.getUserid()});
                    //查询是否支部书记
                    Map<String,Object> zbsjdata = generalSqlComponent.query(SqlCode.getTDzzBzcyxx,new Object[]{bean.getUserid(),data.get("organid")});
                    if(zbsjdata!=null){
                        generalSqlComponent.update(SqlCode.updateTDzzInfoBcDzzsj,new Object[]{data.get("organid")});
                    }
                    generalSqlComponent.delete(SqlCode.delTDzzBzcyxx,new Object[]{bean.getUserid(),data.get("organid")});
                    //处理党费基数
                    generalSqlComponent.delete(SqlCode.deleteDfjs,new Object[]{bean.getUserid(),session.getCompid()});
                    //同步党组织信息   修改sys_user t_dy_info， urc_user状态
                    //修改sys_user
                    generalSqlComponent.update(SqlCode.updateSysUser,new Object[]{0,bean.getUserid()});
                    //修改t_dy_info
                    generalSqlComponent.update(SqlCode.updateTDyInfo,new Object[]{1,bean.getUserid()});
                    //修改urc_user
                    Map<String,Object> dy = generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{bean.getUserid()});
                    Map<String,Object> user = generalSqlComponent.query(SqlCode.queryUrcUserByIdno,new Object[]{dy.get("zjhm")});
                    generalSqlComponent.update(SqlCode.updateUrcUserStatus,new Object[]{0,user.get("userid")});

                    bean.setOutexaminetime(sdf.format(new Date()));
                    partyTransferLog(bean,session,bean.getStatus(),null,"迁出党委审核");
                    partyTransferLog(bean,session,bean.getStatus(),null,"完成");
                    //删除待办
                    generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
                }else {
                    //待党委审核
                    bean.setStatus(4);
                    bean.setOutexaminetime(sdf.format(new Date()));
                    partyTransferLog(bean,session,bean.getStatus(),null,"迁出党支部书记审核");
                    partyTransferLog(bean,session,bean.getStatus(),null,"待迁出党委审核");
                    //删除待办
                    generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
                    //迁出集团外 生成党委待办
                    addTodoItem(session, sdf, Long.valueOf(bean.getId()), String.valueOf(bean.getOutdworgcode()),bean.getUsername()+"(转出)",bean.getStatus(),"/affairsParty/orgApprove");
                }
            }else{
                bean.setOutexaminetime(sdf.format(new Date()));
                partyTransferLog(bean,session,bean.getStatus(),null,"迁出党支部书记审核");
                partyTransferLog(bean,session,bean.getStatus(),null,"待迁入党支部书记审核");
                //删除待办
                generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
                //添加待办  迁出党支部书记
                addTodoItem(session, sdf, Long.valueOf(bean.getId()),bean.getInfoorgcode(),bean.getUsername()+"(接收)",bean.getStatus(),"/affairsParty/orgApprove");
            }
        }else if(bean.getStatus()==3){//迁入审核
            bean.setInfoexaminetime(sdf.format(new Date()));
            partyTransferLog(bean,session,bean.getStatus(),null,"迁入党支部书记审核");
            partyTransferLog(bean,session,bean.getStatus(),null,"待迁入党支部确认");
            //删除待办
            generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
            //添加待办  迁出党支部书记
            addTodoItem(session, sdf, Long.valueOf(bean.getId()),bean.getInfoorgcode(),bean.getUsername()+"(确认)",bean.getStatus(),"/affairsParty/orgTransfer");
        }else if(bean.getStatus()==5){//集团外转出党委审批
            if(bean.getTransfertype()==3){
                bean.setStatus(9);
                bean.setOutdwexaminetime(sdf.format(new Date()));
                partyTransferLog(bean,session,bean.getStatus(),null,"迁出党委审核");
                partyTransferLog(bean,session,bean.getStatus(),null,"完成");
                //迁出集团外 到此结束
                //处理党小组
                generalSqlComponent.delete(SqlCode.delPartyGroupMember2,new Object[]{bean.getUserid()});
                //处理班子成员
                Map<String,Object> data = generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{bean.getUserid()});
                //查询是否支部书记
                Map<String,Object> zbsjdata = generalSqlComponent.query(SqlCode.getTDzzBzcyxx,new Object[]{bean.getUserid(),data.get("organid")});
                if(zbsjdata!=null){
                    generalSqlComponent.update(SqlCode.updateTDzzInfoBcDzzsj,new Object[]{data.get("organid")});
                }
                generalSqlComponent.delete(SqlCode.delTDzzBzcyxx,new Object[]{bean.getUserid(),data.get("organid")});
                //处理党费基数
                generalSqlComponent.delete(SqlCode.deleteDfjs,new Object[]{bean.getUserid(),session.getCompid()});
                //同步党组织信息   修改sys_user t_dy_info， urc_user状态
                //修改sys_user
                generalSqlComponent.update(SqlCode.updateSysUser,new Object[]{0,bean.getUserid()});
                //修改t_dy_info
                generalSqlComponent.update(SqlCode.updateTDyInfo,new Object[]{1,bean.getUserid()});
                //修改urc_user
                Map<String,Object> dy = generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{bean.getUserid()});
                Map<String,Object> user = generalSqlComponent.query(SqlCode.queryUrcUserByIdno,new Object[]{dy.get("zjhm")});
                if(user!=null){
                    generalSqlComponent.update(SqlCode.updateUrcUserStatus,new Object[]{0,user.get("userid")});
                }

                bean.setOutexaminetime(sdf.format(new Date()));
                partyTransferLog(bean,session,bean.getStatus(),null,"迁出党委审核");
                partyTransferLog(bean,session,bean.getStatus(),null,"完成");
                //删除待办
                generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
            }
        }
        else if(bean.getStatus()==9){//迁入确认
            //处理党小组
            generalSqlComponent.delete(SqlCode.delPartyGroupMember2,new Object[]{bean.getUserid()});
            //处理班子成员
            Map<String,Object> data = generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{bean.getUserid()});
            //查询是否支部书记
            Map<String,Object> zbsjdata = generalSqlComponent.query(SqlCode.getTDzzBzcyxx,new Object[]{bean.getUserid(),data.get("organid")});
            if(zbsjdata!=null){
                generalSqlComponent.update(SqlCode.updateTDzzInfoBcDzzsj,new Object[]{data.get("organid")});

            }
            generalSqlComponent.delete(SqlCode.delTDzzBzcyxx,new Object[]{bean.getUserid(),data.get("organid")});

            //处理党费基数 迁移到新组织下
            //查询迁入组织
            Map<String,Object> dzzdata = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode,new Object[]{bean.getInfoorgcode()});
            //修改党费所属组织
            generalSqlComponent.update(SqlCode.updateDfjsOrgid,new Object[]{dzzdata.get("organid"),bean.getUserid(),session.getCompid()});
            //同步党组织信息
            //修改t_dy_info
            generalSqlComponent.update(SqlCode.updateTDyInfoOrganid,new Object[]{dzzdata.get("organid"),bean.getUserid()});
            //修改sys_user
            generalSqlComponent.update(SqlCode.updateSysUserOrgid,new Object[]{dzzdata.get("ordernum"),bean.getInfoorgname(),bean.getUserid()});
            //组织内迁移时
            if(bean.getTransfertype()==2){
                SysUser singleColumn = generalSqlComponent.getDbComponent().getBean("SELECT * FROM `sys_user` WHERE acc =?", SysUser.class, new Object[]{bean.getIdcode()});
                if(singleColumn!=null){
                    //sys_user
                    SysUser user = new SysUser();
                    user.setId(singleColumn.getId());
                    user.setAcc(bean.getIdcode());
                    user.setName(bean.getUsername());
                    user.setWorkPhone(singleColumn.getWorkPhone());
                    user.setBaseInfoId(bean.getUserid());
                    user.setModiTime(new Date());
                    user.setStatus(1);
                    user.setOrgId(bean.getInfoorgcode());
                    user.setOrgName(bean.getInfoorgname());
                    user.setModifier(session.getUserid().toString());
                    generalSqlComponent.update(SqlCode.upadteSysUser,user);
                }
                Map<String,Object> mapUrcOrg = generalSqlComponent.getDbComponent().getMap("select * from urc_organization where compid=? and orgcode=?", new Object[] {session.getCompid(),bean.getInfoorgcode()});
                Map<String, Object> mapUrcUser = generalSqlComponent.getDbComponent().getMap("select * from urc_user where idno =?", new Object[]{data.get("zjhm")});
                Map<String,Object> outMapUrcOrg = generalSqlComponent.getDbComponent().getMap("select * from urc_organization where compid=? and orgcode=?", new Object[] {session.getCompid(),bean.getOutorgcode()});
                if(outMapUrcOrg!=null&&mapUrcUser!=null){
                    generalSqlComponent.getDbComponent().update("DELETE FROM urc_role_map WHERE roleid='7' AND orgid=? AND sjid=?",new Object[]{outMapUrcOrg.get("orgid"),mapUrcUser.get("userid")});//删除书记角色
                }
                if(mapUrcOrg!=null&&mapUrcUser!=null){
                    String urcUserid = String.valueOf(mapUrcUser.get("userid"));
                    //修改组织用户信息
                    generalSqlComponent.getDbComponent().update("UPDATE urc_user_org_map SET orgid=? WHERE userid=?",new Object[]{mapUrcOrg.get("orgid"),urcUserid});
//                    generalSqlComponent.getDbComponent().update("UPDATE urc_role_map SET orgid=? WHERE sjid=?",new Object[]{mapUrcOrg.get("orgid"),urcUserid});
                    //处理换届提醒
                    TDzzHjxxInfoDto remindConfig = generalSqlComponent.query(HJXXSqlCode.getHjxxRemindConfig, new Object[]{bean.getOutorgcode()});
                    if(remindConfig!=null){
                        List<Map> lists = JSON.parseArray(remindConfig.getRemindpeople(),Map.class);
                        for(int i=0;i<lists.size();i++){
                            String name = String.valueOf(lists.get(i).get("name"));
                            String userid = String.valueOf(lists.get(i).get("userid"));
                            if(urcUserid.equals(userid)){
                                lists.remove(i);
                            }
                        }
                        remindConfig.setRemindpeople(JSON.toJSONString(lists));
                        generalSqlComponent.update(HJXXSqlCode.updateHjxxRemindConfig,remindConfig);
                    }
                }
            }
            //查询sys_user
            List<Map<String,Object>> listmap = generalSqlComponent.query(SqlCode.getSysUserList,new Object[]{bean.getUserid()});
            //同步数据
            dataSynchronizationService.syncUserInfos2(listmap);

            bean.setInfooktime(sdf.format(new Date()));
            if(org.springframework.util.StringUtils.isEmpty(bean.getJtdworgcode())){
                bean.setJtdworgcode(0);
            }
            if(1==bean.getJtdworgcode()){//一级集团党委直接确认
                bean.setJtdworgcode(1);
                bean.setJtdwexaminetime(sdf.format(new Date()));
                bean.setJtdwremark(bean.getJtdwremark());
                partyTransferLog(bean,session,bean.getStatus(),null,"一级集团党委直接迁移");
                partyTransferLog(bean,session,bean.getStatus(),null,"完成");
            }else{
                partyTransferLog(bean,session,bean.getStatus(),null,"迁入党支部确认");
                partyTransferLog(bean,session,bean.getStatus(),null,"完成");
            }
            //删除待办
            generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
        }
        bean.setCompid(session.getCompid());
        generalSqlComponent.update(SqlCode.updatePartyTransfer,bean);
        return null;
    }

    /**
     * 获取上级最近一个党委
     * @param outorgcode
     * @return
     */
    private Map<String, Object> getParentPartyOrganCode(String outorgcode) {
        //父机构编码
        String parentcode = generalSqlComponent.getDbComponent().getSingleColumn("select parentcode from t_dzz_info where organcode=? ",String.class,new Object[]{outorgcode});
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from t_dzz_info where organcode=? ", new Object[]{parentcode});
        if(map==null){
            return null;
        }
        if(SysConstants.ZZLB.DW.equals(map.get("zzlb"))){
            return map;
        }else {
            return getParentPartyOrganCode(parentcode);
        }
    }

    /**
    * @Description 重新提交
    * @Author zwd
    * @Date 2020/11/27 10:32
    * @param
    * @return
    **/
    @Transactional(rollbackFor = Exception.class)
    public Boolean againPartyTransfer(PartyTransferDto bean, Session session){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        bean.setOuttime(sdf.format(new Date()));
        bean.setCompid(session.getCompid());
        generalSqlComponent.update(SqlCode.updatePartyTransfer,bean);
        //生成党员迁移日志
        partyTransferLog(bean,session,-1,null,"重新提交党员迁移");
        partyTransferLog(bean,session,-1,null,"待迁出党支部书记审核");
        //删除待办
        generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,bean.getId()});
        //添加待办  迁出党支部书记
        addTodoItem(session, sdf, Long.valueOf(bean.getId()),bean.getOutorgcode(),bean.getUsername()+"(转出)",bean.getStatus(),"/affairsParty/orgApprove");
        return null;
    }

    /**
    * @Description 分页查询
    * @Author zwd
    * @Date 2020/11/28 9:05
    * @param
    * @return
    **/
    public Pager partyTransferPage(Pager pager, String username,  String startdate, String enddate
            ,Integer type,Integer status,Session session){
        Map<String,Object> parms = new HashMap<String,Object>(4);
        parms.put("outorgcode",session.getCurrentOrg().getOrgcode());
//        if(type==1){
//            parms.put("outorgcode",session.getCurrentOrg().getOrgcode());
//        }else if(type==2){
//            parms.put("infoorgcode",session.getCurrentOrg().getOrgcode());
//        }
        if(!StringUtils.isEmpty(startdate)&&!StringUtils.isEmpty(enddate)){
            parms.put("outtimestart",startdate+" 00:00:00");
            parms.put("outtimeend",enddate+" 23:59:59");
        }
        parms.put("username",username);
        parms.put("compid",session.getCompid());

        parms.put("status",status);
        if(status!=null&&status==1){
            parms.put("status","1,4");
        }
        //是否AB账号
        Map<String,Object> parmsab = new HashMap<String,Object>(1);
        parmsab.put("userid",session.getUserid());
        List<Map<String,Object>> ABlist = generalSqlComponent.query(SqlCode.getABaccountList,parmsab);

        //是否支部书记
        Map<String,Object> parmszbsj = new HashMap<String,Object>(2);
        parmszbsj.put("userid",session.getUserid());
        parmszbsj.put("organcode",session.getCurrentOrg().getOrgcode());

        Map<String,Object> mapzbsj = generalSqlComponent.query(SqlCode.getdzbsj,parmszbsj);
        System.out.println("*****************************************************");
        if(ABlist.size()>0||mapzbsj!=null){
            System.out.println("*****************************************************"+parms);
            generalSqlComponent.pageQuery(SqlCode.getPartyTransfer,parms,pager);
//            if(type2==0){
//                generalSqlComponent.pageQuery(SqlCode.getPartyTransfer,parms,pager);
//            }else{
//                generalSqlComponent.pageQuery(SqlCode.getPartyTransfer2,parms,pager);
//            }
        }
//        //当前登录是否是党委
//        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from t_dzz_info where organcode=? and `zzlb` = '6100000035'", new Object[]{session.getCurrentOrg().getOrgcode()});
//        if(map!=null){
//            parms.put("outorgcode","");
//            parms.put("outdworgcode",session.getCurrentOrg().getOrgcode());
//            generalSqlComponent.pageQuery(SqlCode.getPartyTransfer,parms,pager);
//        }

        return processingdata(pager,session);

    }

    public Pager partyTransferPage2(Pager pager, String username,  String startdate, String enddate
            ,Integer type,Session session){
        Map<String,Object> parms = new HashMap<String,Object>(4);
        parms.put("infoorgcode",session.getCurrentOrg().getOrgcode());
        parms.put("status",3);
//        if(type==1){
//            parms.put("outorgcode",session.getCurrentOrg().getOrgcode());
//        }else if(type==2){
//            parms.put("infoorgcode",session.getCurrentOrg().getOrgcode());
//        }
        if(!StringUtils.isEmpty(startdate)&&!StringUtils.isEmpty(enddate)){
            parms.put("outtimestart",startdate+" 00:00:00");
            parms.put("outtimeend",enddate+" 23:59:59");
        }
        parms.put("username",username);
        parms.put("compid",session.getCompid());

        //是否AB账号
        Map<String,Object> parmsab = new HashMap<String,Object>(1);
        parmsab.put("userid",session.getUserid());
        List<Map<String,Object>> ABlist = generalSqlComponent.query(SqlCode.getABaccountList,parmsab);

        //是否支部书记
        Map<String,Object> parmszbsj = new HashMap<String,Object>(2);
        parmszbsj.put("userid",session.getUserid());
        parmszbsj.put("organcode",session.getCurrentOrg().getOrgcode());

        Map<String,Object> mapzbsj = generalSqlComponent.query(SqlCode.getdzbsj,parmszbsj);
        if(ABlist.size()>0||mapzbsj!=null){
            generalSqlComponent.pageQuery(SqlCode.getPartyTransfer6,parms,pager);
//            if(type2==0){
//                generalSqlComponent.pageQuery(SqlCode.getPartyTransfer,parms,pager);
//            }else{
//                generalSqlComponent.pageQuery(SqlCode.getPartyTransfer2,parms,pager);
//            }
        }
        return processingdata(pager,session);

    }

    public Pager partyTransferPage3(Pager pager, String username, Integer status, String startdate, String enddate
            ,Integer type2,Session session){
        Map<String,Object> parms = new HashMap<String,Object>(4);
        parms.put("orgcode",session.getCurrentOrg().getOrgcode());
        parms.put("status1",1);
        parms.put("status2",2);
        if(!StringUtils.isEmpty(startdate)&&!StringUtils.isEmpty(enddate)){
            parms.put("outtimestart",startdate+" 00:00:00");
            parms.put("outtimeend",enddate+" 23:59:59");
        }
        parms.put("username",username);
        parms.put("compid",session.getCompid());

        //是否AB账号
        Map<String,Object> parmsab = new HashMap<String,Object>(1);
        parmsab.put("userid",session.getUserid());
        List<Map<String,Object>> ABlist = generalSqlComponent.query(SqlCode.getABaccountList,parmsab);

        //是否支部书记
        Map<String,Object> parmszbsj = new HashMap<String,Object>(2);
        parmszbsj.put("userid",session.getUserid());
        parmszbsj.put("organcode",session.getCurrentOrg().getOrgcode());
        Map<String,Object> mapzbsj = generalSqlComponent.query(SqlCode.getdzbsj,parmszbsj);
        if(mapzbsj!=null){
            if(type2==0){
                generalSqlComponent.pageQuery(SqlCode.getPartyTransfer3,parms,pager);
            }else{
                generalSqlComponent.pageQuery(SqlCode.getPartyTransfer4,parms,pager);
            }
        }
        //当前登录是否是党委
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from t_dzz_info where organcode=? and `zzlb` = '6100000035'", new Object[]{session.getCurrentOrg().getOrgcode()});
        if(map!=null){
            String parentcode = String.valueOf(map.get("parentcode"));
            boolean b = "0".equals(parentcode);
            if(b){//是一级集团党委
                parms.put("transfertype","2,3");
            }else{
                parms.put("transfertype","3");
            }
            if(type2==0){
                //待审核
                if(b){
                    parms.put("status","1,2,3");
                    parms.put("orgcode","");
                }else{
                    parms.put("status",4);
                }
                generalSqlComponent.pageQuery(SqlCode.getPartyTransferByDW,parms,pager);
            }else{
                //已审核
                if(b){
                    parms.put("jtdworgcode",1);
                    parms.put("orgcode","");
                }
                parms.put("status","0,9");
                generalSqlComponent.pageQuery(SqlCode.getPartyTransferByDW,parms,pager);
            }
        }

        return processingdata(pager,session);

    }

    /**
    * @Description  迁入未审核数量
    * @Author zwd
    * @Date 2020/11/27 14:25
    * @param 
    * @return 
    **/
    public Integer partyTransferIntoNum(Integer status,Session session){
        Map<String,Object> parms = new HashMap<String,Object>(2);
        parms.put("status",status);
        parms.put("compid",session.getCompid());
        parms.put("infoorgcode",session.getCurrentOrg().getOrgcode());
        Integer num = generalSqlComponent.query(SqlCode.getPartyTransferNum,parms);
        return num;
    }

    /**
    * @Description 党员迁移汇总查询
    * @Author zwd
    * @Date 2020/11/27 15:20
    * @param 
    * @return 
    **/
    public Pager partyTransferOrg(Pager pager,String orgcode, String username, Integer transfertype,Integer status, String startdate, String enddate,Integer type,Session session,String organid){
        Map<String,Object> parms = new HashMap<String,Object>(2);

        String path = getPath(organid);
        List<String> listCode= generalSqlComponent.query(SqlCode.getDzzByOrganId,new HashMap(){{put("path",path);put("organid",organid);}});

        if(!StringUtils.isEmpty(startdate)&&!StringUtils.isEmpty(enddate)){
            parms.put("outtimestart",startdate+" 00:00:00");
            parms.put("outtimeend",enddate+" 23:59:59");
        }
        parms.put("username",username);
        parms.put("status",status);
        if(status== Constant.INT_ONE){
            parms.put("status","");
            parms.put("outstatus","1,4");
        }
        parms.put("type",type);
        parms.put("transfertype",transfertype);
//        if(type==1){
//            parms.put("outorgcode",orgcode);
//
//        }else{
//            parms.put("infoorgcode",orgcode);
//        }

        if(type==1){
            parms.put("outorgcodes",StringUtils.strip(listCode.toString(),"[]"));
        }else{
            parms.put("infoorgcodes",StringUtils.strip(listCode.toString(),"[]"));
        }
        parms.put("compid",session.getCompid());
        generalSqlComponent.pageQuery(SqlCode.getPartyTransfer7,parms,pager);
        return processingdata(pager,session);
    }

    public Pager processingdata(Pager pager,Session session){
        List<PartyTransferDto> list = (List<PartyTransferDto>) pager.getRows();
        if(list==null){
            return pager;
        }
        List<PartyTransferDto2> list2 = new ArrayList<PartyTransferDto2>(list.size());
        for(PartyTransferDto li:list){
            PartyTransferDto2 party = new PartyTransferDto2();
//            party.setId(li.getId());
//            party.setTransfertype(li.getTransfertype());
//            party.setUserid(li.getUserid());
//            party.setUsername(li.getUsername());
//            party.setOutorgcode(li.getOutorgcode());
//            party.setOutorgname(li.getOutorgname());
//            party.setInfoorgcode(li.getInfoorgcode());
//            party.setInfoorgname(li.getInfoorgname());
//            party.setStatus(li.getStatus());
//            party.setStatus2(li.getStatus2());
//            party.setOuttime(li.getOuttime());
//            party.setBackremark(li.getBackremark());
//            party.setOutexaminetime(li.getOutexaminetime());
//            party.setOurexamineremark(li.getOurexamineremark());
//            party.setInfoexaminetime(li.getInfoexaminetime());
//            party.setInfoexamineremark(li.getInfoexamineremark());
//            party.setInfooktime(li.getInfooktime());
//            party.setInfookexamineremark(li.getInfookexamineremark());
//            party.setOutremark(li.getOutremark());
//            party.setRemark1(li.getRemark1());
//            party.setRemark2(li.getRemark2());
//            party.setRemark3(li.getRemark3());
//            party.setRemark4(li.getRemark4());
//            party.setCompid(li.getCompid());
//            party.setIdcode(li.getIdcode());
            BeanUtil.copy(li, party);

            Map<String,Object> orgpatms = new HashMap<String,Object>(1);
            orgpatms.put("organcode",li.getInfoorgcode());
            Map<String,Object> orgmap =  generalSqlComponent.query(SqlCode.queryPorgcode,orgpatms);
            party.setOrgid(orgmap==null?null:orgmap.get("orgid").toString());
            party.setFiles(fileDelService.getFiles(li.getFiles(),session));
            list2.add(party);
        }
        pager.setRows(list2);
        return pager;
    }

    /**
    * @Description 创建日志
    * @Author zwd
    * @Date 2020/11/27 10:33
    * @param 
    * @return 
    **/
    public void partyTransferLog(PartyTransferDto bean, Session session,Integer status,String remark,String message){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        PartyTransferLog log = new PartyTransferLog();
        log.setTransferid(bean.getId());
        log.setUserid(session.getUserid().toString());
        log.setUsername(session.getUsername());
        log.setOrgcode(session.getCurrentOrg().getOrgcode());
        log.setOrgname(session.getCurrentOrg().getOrgname());
        log.setStatus(status);
        log.setTimes(sdf.format(new Date()));
        log.setRemark(remark);
        log.setMessage(message);
        generalSqlComponent.insert(SqlCode.addPartyTransferLog,log);
    }

    public Boolean deletePartyTransfer(Integer id,Session session){
        //删除待办
        generalSqlComponent.update(SqlCode.updateTodoItemDelete,new Object[]{10,id});
        generalSqlComponent.update(SqlCode.updatePartyTransferStatus2,new Object[]{id,session.getCompid()});
        return true;
    }
    /**
     * 获取组织机构Path
     * @param orgId
     * @return
     * @author taogang
     */
    public String getPath(String orgId) {
        String path = "";
        if(!StringUtils.isEmpty(orgId)) {

            TDzzInfoSimple org =generalSqlComponent.query(SqlCode.get_path_num,new Object[]{orgId});
            if(null != org) {
                String[] oarr = org.getIdPath().split("/");
                path = "path"+(oarr.length-1);
            }
        }
        return path;
    }
}