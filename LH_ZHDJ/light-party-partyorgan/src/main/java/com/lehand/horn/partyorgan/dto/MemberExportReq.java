package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "党员导出请求参数",description = "党员导出请求参数")
public class MemberExportReq extends MemberSearchReq {
    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="导出列名数据",name="exportColumns")
    private List<ExportColumnReq> exportColumns;

    public List<ExportColumnReq> getExportColumns() {
        return exportColumns;
    }

    public void setExportColumns(List<ExportColumnReq> exportColumns) {
        this.exportColumns = exportColumns;
    }
}
