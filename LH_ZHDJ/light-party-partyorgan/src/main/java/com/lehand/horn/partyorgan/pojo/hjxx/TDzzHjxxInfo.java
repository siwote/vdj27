package com.lehand.horn.partyorgan.pojo.hjxx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: TDzzHjxxInfo
 * @Description: 党组织换届信息
 * @Author lx
 * @DateTime 2021-05-10 14:18
 */
@ApiModel(value = "党组织换届信息类", description = "党组织换届信息类")
public class TDzzHjxxInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "自增长id", name = "id")
    private long id;
    @ApiModelProperty(value = "党组织ID", name = "orgid")
    private String orgid;
    @ApiModelProperty(value = "机构CODE", name = "orgcode")
    private String orgcode;
    /**
     * 任期届满日期
     */
    @ApiModelProperty(value = "任期届满日期", name = "plandate")
    private String plandate;
    /**
     * 实际换届日期
     */
    @ApiModelProperty(value = "实际换届日期", name = "actualdate")
    private String actualdate;
    /**
     * 1:正常。2:延迟
     */
    @ApiModelProperty(value = "1:正常。2:延迟。3:到期未换届", name = "status")
    private long status;
    @ApiModelProperty(value = "操作人", name = "createuserid")
    private String createuserid;
    @ApiModelProperty(value = "操作时间", name = "createtime")
    private String createtime;
    @ApiModelProperty(value = "更新人", name = "updateuserid")
    private String updateuserid;
    @ApiModelProperty(value = "更新操作时间", name = "updatetime")
    private String updatetime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getPlandate() {
        return plandate;
    }

    public void setPlandate(String plandate) {
        this.plandate = plandate;
    }

    public String getActualdate() {
        return actualdate;
    }

    public void setActualdate(String actualdate) {
        this.actualdate = actualdate;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(String createuserid) {
        this.createuserid = createuserid;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(String updateuserid) {
        this.updateuserid = updateuserid;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }
}
