package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 
 * @author code maker
 */
@ApiModel(value="",description="")
public class PartyTransferLog implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 主键
	 */
	@ApiModelProperty(value="主键",name="id")
	private Integer id;
	
	/**
	 * 迁移表id
	 */
	@ApiModelProperty(value="迁移表id",name="transferid")
	private Integer transferid;
	
	/**
	 * 操作人
	 */
	@ApiModelProperty(value="操作人",name="userid")
	private String userid;
	
	/**
	 * 操作人名
	 */
	@ApiModelProperty(value="操作人名",name="username")
	private String username;
	
	/**
	 * 操作组织code
	 */
	@ApiModelProperty(value="操作组织code",name="orgcode")
	private String orgcode;
	
	/**
	 * 操作组织名
	 */
	@ApiModelProperty(value="操作组织名",name="orgname")
	private String orgname;
	
	/**
	 * 和党员迁移表状态关联      -1：申请(创建)，0：驳回，1迁出待审核，2：迁入待审核，3：迁入待确认,9：通过
	 */
	@ApiModelProperty(value="和党员迁移表状态关联      -1：申请(创建)，0：驳回，1迁出待审核，2：迁入待审核，3：迁入待确认,9：通过",name="status")
	private Integer status;
	
	/**
	 * 操作时间
	 */
	@ApiModelProperty(value="操作时间",name="times")
	private String times;
	
	/**
	 * 操作结果备注（审核结果备注）
	 */
	@ApiModelProperty(value="操作结果备注（审核结果备注）",name="remark")
	private String remark;
	
	/**
	 * 操作说明
	 */
	@ApiModelProperty(value="操作说明",name="message")
	private String message;
	

    /**
     * setter for id
     * @param id
     */
	public void setId(Integer id) {
		this.id = id;
	}

    /**
     * getter for id
     */
	public Integer getId() {
		return id;
	}

    /**
     * setter for transferid
     * @param transferid
     */
	public void setTransferid(Integer transferid) {
		this.transferid = transferid;
	}

    /**
     * getter for transferid
     */
	public Integer getTransferid() {
		return transferid;
	}

    /**
     * setter for userid
     * @param userid
     */
	public void setUserid(String userid) {
		this.userid = userid;
	}

    /**
     * getter for userid
     */
	public String getUserid() {
		return userid;
	}

    /**
     * setter for username
     * @param username
     */
	public void setUsername(String username) {
		this.username = username;
	}

    /**
     * getter for username
     */
	public String getUsername() {
		return username;
	}

    /**
     * setter for orgcode
     * @param orgcode
     */
	public void setOrgcode(String orgcode) {
		this.orgcode = orgcode;
	}

    /**
     * getter for orgcode
     */
	public String getOrgcode() {
		return orgcode;
	}

    /**
     * setter for orgname
     * @param orgname
     */
	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

    /**
     * getter for orgname
     */
	public String getOrgname() {
		return orgname;
	}

    /**
     * setter for status
     * @param status
     */
	public void setStatus(Integer status) {
		this.status = status;
	}

    /**
     * getter for status
     */
	public Integer getStatus() {
		return status;
	}

    /**
     * setter for times
     * @param times
     */
	public void setTimes(String times) {
		this.times = times;
	}

    /**
     * getter for times
     */
	public String getTimes() {
		return times;
	}

    /**
     * setter for remark
     * @param remark
     */
	public void setRemark(String remark) {
		this.remark = remark;
	}

    /**
     * getter for remark
     */
	public String getRemark() {
		return remark;
	}

    /**
     * setter for message
     * @param message
     */
	public void setMessage(String message) {
		this.message = message;
	}

    /**
     * getter for message
     */
	public String getMessage() {
		return message;
	}

    /**
     * PartyTransferLogEntity.toString()
     */
    @Override
    public String toString() {
        return "PartyTransferLogEntity{" +
               "id='" + id + '\'' +
               ", transferid='" + transferid + '\'' +
               ", userid='" + userid + '\'' +
               ", username='" + username + '\'' +
               ", orgcode='" + orgcode + '\'' +
               ", orgname='" + orgname + '\'' +
               ", status='" + status + '\'' +
               ", times='" + times + '\'' +
               ", remark='" + remark + '\'' +
               ", message='" + message + '\'' +
               '}';
    }

}
