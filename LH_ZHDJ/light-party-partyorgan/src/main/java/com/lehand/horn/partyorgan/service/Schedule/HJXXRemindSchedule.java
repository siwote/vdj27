package com.lehand.horn.partyorgan.service.Schedule;


import com.alibaba.fastjson.JSON;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.HJXXSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.TDzzHjxxInfoDto;
import com.lehand.horn.partyorgan.dto.UrcUser;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfo;
import com.lehand.horn.partyorgan.service.hjxx.OrgTransitionService;
import com.lehand.module.common.feign.UserRoleOrg;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class HJXXRemindSchedule {
    //记录日志
    private static final Logger logger = LogManager.getLogger(HJXXRemindSchedule.class);
    //是否正在创建任务
    private static final AtomicBoolean CREATE = new AtomicBoolean(false);
    //线程池
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private OrgTransitionService transitionService;

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Transactional(rollbackFor = Exception.class)
    public void addElectionRemind() {
        try {
            synchronized (CREATE) {
                if (CREATE.get()) {
                    logger.info("之前任务正在执行,本次不处理");
                    return;
                }
                CREATE.set(true);
            }
            logger.info("定时创建换届提醒开始...");

            //所有换届提醒节点
            List<TDzzHjxxInfoDto> listHJXX = generalSqlComponent.query(HJXXSqlCode.listRemindInfo, new Object[]{});
            for (TDzzHjxxInfoDto dto : listHJXX) {
                String orgcode = dto.getOrgcode();
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            List<String> userids = new ArrayList<>();
                            //查询所有接收人的集合
                            List<Map> lists = JSON.parseArray(dto.getRemindpeople(), Map.class);
                            if (CollectionUtils.isEmpty(lists)) {
                                return;
                            }
                            lists.stream().forEach(a -> {
                                userids.add(String.valueOf(a.get("userid")));
                            });
                            //查询换届提醒人角色下上级组织用户
                            List<UserRoleOrg> hjList = transitionService.pageRole(orgcode, "换届提醒人", 2);
                            //存在取换届提醒人用户，不存在取书记下用户
                            if (hjList != null && hjList.size() > 0) {
                                hjList.stream().forEach(a -> {
                                    userids.add(String.valueOf(a.getUserid()));
                                });
                            } else {
                                //查询书记角色下上级组织用户
                                List<UserRoleOrg> sjList = transitionService.pageRole(orgcode, "书记", 2);
                                if (sjList != null && sjList.size() > 0) {
                                    sjList.stream().forEach(a -> {
                                        userids.add(String.valueOf(a.getUserid()));
                                    });
                                }
                            }
                            //配置参数
                            Map param = getStringObjectMap(orgcode,dto.getDistance());
                            for (String userid : userids) {
                                Map<String,Object> user = generalSqlComponent.query(SqlCode.getPwUserByID2, new Object[]{1,userid});
                                param.put("userid", user.get("userid"));
                                    param.put("username",user.get("username"));
                                    generalSqlComponent.insert(SqlCode.addTTRLByTuihui,param);
                            }
                            modiRemindTimeStatus(orgcode,dto.getRemindtime(), 2);
                        } catch (Exception e) {
                            logger.error("定时创建换届提醒异常", e);
                        }
                    }
                });
            }
            logger.info("定时创建换届提醒结束...");
        } catch (Exception e) {
            logger.error("定时创建换届提醒异常", e);
        } finally {
            CREATE.set(false);
        }
    }

    /**
     * 修改提醒状态
     * @param orgcode 组织编码
     * @param remindtime 时间节点
     * @param status 状态
     */
    private void modiRemindTimeStatus(String orgcode, String remindtime, int status) {
        generalSqlComponent.update(HJXXSqlCode.updateRemindTime,new Object[]{status,remindtime,orgcode});
    }

    /**
     * 组装消息参数
     *
     * @param date
     * @return
     */
    private Map getStringObjectMap(String orgcode,Integer date) {
        Map param = new HashMap();
        param.put("compid", 1L);
        param.put("noteid", -1L);
        param.put("module", 100);//模块(0:任务发布,1:任务反馈)
        param.put("mid", -1L);//对应模块的ID
        param.put("remind", DateEnum.YYYYMMDDHHMMDD.format(new Date()));//提醒日期
        param.put("rulecode", "ELECTION_REMIND");//编码
        param.put("rulename", "换届提醒");
        param.put("optuserid", 0L);
        param.put("optusername", "系统消息");
        param.put("isread", 0);//
        param.put("ishandle", 0);//
        param.put("issend", 0);//是否已提醒(0:未提醒,1:已提醒)
        param.put("mesg",getTDzzInfoByOrgCode(orgcode).getDzzmc()+ "即将届满（剩余" + date + "个月），请及时换届");//消息
        param.put("remarks1", 1);//备注1
        param.put("remarks2", 0);//备注2
        param.put("remarks3", "换届提醒");//备注3
        param.put("remarks4", "");//备注3
        param.put("remarks5", "");//备注3
        param.put("remarks6", "");//备注3
        param.put("remark", "");
        return param;
    }

    /**
     * 获取党组织信息
     * @param orgcode
     * @return
     */
    private TDzzInfo getTDzzInfoByOrgCode(String orgcode) {
        Map<String, Object> map = new HashMap<>();
        map.put("organcode",orgcode);
        TDzzInfo tDzzInfo = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, map);
        if(tDzzInfo==null){
            LehandException.throwException("不存在当前党组织");
        }
        return tDzzInfo;
    }

    public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
        return threadPoolTaskExecutor;
    }

    public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.threadPoolTaskExecutor = threadPoolTaskExecutor;
    }

}
