package com.lehand.horn.partyorgan.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.cache.CacheComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PartyCacheComponent {
    private static final Logger logger = LogManager.getLogger(PartyCacheComponent.class);

    public static final String DCITITEMCACHE = "DCITITEMCACHE";

    @Value("${session-timeout}")
    private int sessionTimeOut;

    @Value("${verification-timeout}")
    private int verificationTimeOut;

    @Resource
    private CacheComponent cacheComponent;

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 加载数据字典内容
     */
    public void getAllDictItem() {
        logger.info("加载字典数据缓存...");
        //查询字典信息
        List<Map<String, Object>> list =generalSqlComponent.query(SqlCode.queryDcitParams, new Object[] {});
        Map<String, String> dictItemMap = new HashMap<>(list.size());

        //将查询结果放置缓存集合中
        for(Map<String, Object> map : list) {
            dictItemMap.put(getKey(map.get("code").toString(),map.get("item_code").toString()), map.get("item_name").toString());
        }

        cacheComponent.set(DCITITEMCACHE, JSON.toJSONString(dictItemMap));
    }

    /**
     * 生成组合Key
     * @param code
     * @param itemCode
     * @return
     */
    private String getKey(String code,String itemCode) {
        return code+ StringConstant.UNDERLINE+itemCode;
    }

    /**
     * 获取键值内容
     * @param code
     * @param itemCode
     * @return
     */
    public String getName(String code,Object itemCode) {
        if (null==itemCode) {
            return StringConstant.EMPTY;
        }

        String cache = cacheComponent.get(DCITITEMCACHE);
        if(StringUtils.isEmpty(cache)) {
            logger.info("重新加载字典缓存数据...");
            this.getAllDictItem();
            cache = cacheComponent.get(DCITITEMCACHE);
        }

        JSONObject object = (JSONObject) JSONObject.parse(cache);
        String name = (String) object.get(getKey(code,itemCode.toString()));
        return name==null?StringConstant.EMPTY:name;
    }
}
