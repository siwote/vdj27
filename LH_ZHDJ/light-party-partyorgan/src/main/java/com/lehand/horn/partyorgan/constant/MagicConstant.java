package com.lehand.horn.partyorgan.constant;

public class MagicConstant {

    /**
     * 字符串常量
     */
    public static interface STR {
        //空字符串
        final String  EMPTY_STR = "";

        final String  EMPTY_STRS = "无";

        final String  PATH_STR = "path";
    }

    /**
     * 数字格式字符串
     */
    public static interface NUM_STR {
        final String  NUM_0 = "0";
        final String NUM_1 = "1";
        final String NUM_2 = "2";
        final String NUM_3 = "3";
    }
}
