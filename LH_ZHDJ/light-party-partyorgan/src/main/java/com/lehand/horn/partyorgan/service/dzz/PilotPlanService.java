package com.lehand.horn.partyorgan.service.dzz;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.PilotPlanSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dto.TDzzPartyBrandDynamicReq;
import com.lehand.horn.partyorgan.dto.TDzzPartyBrandReq;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrand;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandDynamic;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandPraise;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PilotPlanService {

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")
    private String downIp;

    @Resource
    GeneralSqlComponent generalSqlComponent;

    /**
     * 验证必要参数
     * @param bean
     */
    private void isEmpty(TDzzPartyBrand bean) {
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("品牌组织编码不能为空！");
        }
//        if(StringUtils.isEmpty(bean.getFileid())){
//            LehandException.throwException("品牌logo不能为空！");
//        }
        if(StringUtils.isEmpty(bean.getConcept())){
            LehandException.throwException("品牌理念不能为空！");
        }
        if(StringUtils.isEmpty(bean.getSlogan())){
            LehandException.throwException("宣传语不能为空！");
        }
        if(StringUtils.isEmpty(bean.getIntroduce())){
            LehandException.throwException("品牌介绍不能为空！");
        }
        if(StringUtils.isEmpty(bean.getTrainofthought())){
            LehandException.throwException("品牌思路不能为空！");
        }

    }

    /**
     * 检查品牌类型
     * @param type
     */
    private String checkType(String type) {
//        1:培育品牌、2:示范品牌、3:推优上级品牌
        if(SysConstants.BRAND.ONE.equals(type)){
            return "培育品牌";
        }
        if(SysConstants.BRAND.TWO.equals(type)){
            return "示范品牌";
        }
        if(SysConstants.BRAND.THREE.equals(type)){
            return "推优上级品牌";
        }
        return "培育品牌";
    }
    /**
     * 获取附件
     * @param id
     * @return
     */
    private Map<String, Object> getFile(String id) {
        Map<String,Object> m =  generalSqlComponent.query(SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",1);put("id",id);}});
        if(m!=null){
            String dir = (String) m.get("dir");
            m.put("fileid",id);
            m.put("flpath", String.format("%s%s", downIp, dir));
            m.put("name",m.get("name").toString().substring(0,m.get("name").toString().indexOf(".")));
        }
        return  m;
    }
    @Transactional(rollbackFor = Exception.class)
    public void addTDzzPartyBrand(TDzzPartyBrand bean, Session session) {
        //验证必要参数
        isEmpty(bean);
        Map<String,Object> dzz = generalSqlComponent.getDbComponent().getMap(SqlCode.getDZZInfoSimpleByCode,new Object[] {bean.getOrgcode()});
        if(dzz==null){
            LehandException.throwException("党组织信息为空！");
        }
        String parentcode = String.valueOf(dzz.get("parentcode"));
        bean.setAuditorgcode(parentcode);
        if("0".equals(bean.getAuditorgcode())){//是一级党委
            bean.setLevel(2);//公司部级
        }else{
//            Map<String,Object> parentDzz = generalSqlComponent.getDbComponent().getMap(SqlCode.getDZZInfoSimpleByCode,new Object[] {parentcode});
//            if(parentDzz==null){
//                LehandException.throwException("党组织信息为空！");
//            }
//            if("0".equals(parentDzz.get("parentcode"))){//是二级党委
                bean.setLevel(1);//事业部级
//            }else{
//                bean.setLevel(0);//无级
//            }
        }
        bean.setCreator(session.getUserid().toString());
        generalSqlComponent.insert(PilotPlanSqlCode.insertTDzzPartyBrand,bean);
    }
    @Transactional(rollbackFor = Exception.class)
    public void updateTDzzPartyBrand(TDzzPartyBrand bean, Session session) {
        //验证必要参数
        isEmpty(bean);
        if(StringUtils.isEmpty(bean.getId())){
            LehandException.throwException("id不能为空！！");
        }
        if(StringUtils.isEmpty(bean.getLevel())){
            bean.setLevel(1);
        }
        bean.setUpdator(session.getUserid().toString());
        bean.setUpdatetime(DateEnum.YYYYMMDD.format());
        generalSqlComponent.update(PilotPlanSqlCode.updateTDzzPartyBrand,bean);
    }
    @Transactional(rollbackFor = Exception.class)
    public void delTDzzPartyBrand(String id, Session session) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("品牌组织id不能为空！");
        }
        generalSqlComponent.delete(PilotPlanSqlCode.delTDzzPartyBrand,new Object[]{id});
        generalSqlComponent.getDbComponent().delete("DELETE FROM t_dzz_party_brand_dynamic WHERE brandid=?",new Object[]{id});
        generalSqlComponent.getDbComponent().delete("DELETE FROM t_dzz_party_brand_praise WHERE brandid=?",new Object[]{id});
    }
    public TDzzPartyBrandReq getTDzzPartyBrand(String id,String orgcode,Session session) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("品牌组织id不能为空！");
        }
        if(StringUtils.isEmpty(orgcode)){
            LehandException.throwException("品牌组织编码不能为空！");
        }
        TDzzPartyBrandReq brandReq = generalSqlComponent.query(PilotPlanSqlCode.getTDzzPartyBrand,new Object[]{id});
        if(brandReq==null){
            LehandException.throwException("品牌不存在！！！");
        }
        //品牌类型名称
        brandReq.setTypeName(checkType(brandReq.getType()));
        //上传图片logo
        brandReq.setFile(getFile(brandReq.getFileid()));
        //查询党组织
        Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{orgcode});
        brandReq.setOrgName(tDzzInfo!=null?String.valueOf(tDzzInfo.get("dxzms")):"");
        //当前登录组织是否点赞
        TDzzPartyBrandPraise listBrandPraise=generalSqlComponent.query(PilotPlanSqlCode.getBrandPraiseByPraisecode,new Object[]{orgcode,session.getCurrentOrg().getOrgcode(),id});
        brandReq.setIsPraise(listBrandPraise!=null?Integer.valueOf(listBrandPraise.getStatus()):1);
        return brandReq;
    }

    public Pager pageTDzzPartyBrand(Pager pager, String type,String orgCodeLike,String level,String status,String name,String myOrLower,Session session) {
        Map<String, Object> paramMap = new HashMap<>();//传参sql集合

        String orgCode = session.getCurrentOrg().getOrgcode();//当前组织code
        List<String> listOrgCode = new ArrayList<>();
        Map<String,Object> dzz = generalSqlComponent.getDbComponent().getMap(SqlCode.getDZZInfoSimpleByCode,new Object[] {orgCode});
        if(dzz==null){
            LehandException.throwException("党组织信息为空！");
        }
        if(!StringUtils.isEmpty(level)){
            if(level.equals("1")){//事业部级tab页只能展示当前账号所在的二级党委及二级党委以下的所有数据
                String organcode =null;
                if(!StringUtils.isEmpty(dzz.get("path2"))){
                    Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("SELECT * FROM t_dzz_info_simple WHERE organid = ?", new Object[]{dzz.get("path2")});
                    if(map!=null){
                        organcode = String.valueOf(map.get("organcode"));//二级党委orgcode
                    }
                }else {
                    organcode = String.valueOf(orgCode);//一级党委orgcode
                }
                listOrgCode.add(organcode);
                getOrgCode(organcode,listOrgCode);
                paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));

            }
        }

        paramMap.put("type",type);
        paramMap.put("level",level);
        paramMap.put("status",status);
        paramMap.put("name",name);
        //我的当前组织所有品牌
        if("1".equals(myOrLower)){
            paramMap.put("orgcode","'"+orgCode+"'");
        }
        //下级展示审核组织是当前组织
        if("2".equals(myOrLower)){
            String parentcode = String.valueOf(dzz.get("parentcode"));
            boolean b1 = StringUtils.isEmpty(dzz.get("path3"));
            boolean b2 = !StringUtils.isEmpty(dzz.get("path2"));
            if(b1&&b2){//当前组织是二级党委
                //二级党委展示的是需要二级党委审核的和一级党委审核的
                listOrgCode.add(orgCode);
                listOrgCode.add(parentcode);
            }else {
                listOrgCode.add(orgCode);
            }
            paramMap.put("auditorgcode",org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
            if(!StringUtils.isEmpty(orgCodeLike)){//筛选条件不存在
                paramMap.put("orgcode", orgCodeLike);
            }
            paramMap.put("myOrLower",myOrLower);
        }
        paramMap.put("name",name);
        generalSqlComponent.pageQuery(PilotPlanSqlCode.pageTDzzPartyBrand,paramMap,pager);
        List<Map<String, Object>> listBrandReq = (List<Map<String, Object>>) pager.getRows();
        for (Map<String, Object> map: listBrandReq) {
            //品牌类型名称
            map.put("typeName",checkType(String.valueOf(map.get("type"))));
            //上传图片logo
            map.put("file",getFile(String.valueOf(map.get("fileid"))));
            //查询党组织
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{map.get("orgcode")});
            map.put("orgName",tDzzInfo==null?"":String.valueOf(tDzzInfo.get("dxzms")));
            //当前登录组织是否点赞
            TDzzPartyBrandPraise listBrandPraise=generalSqlComponent.query(PilotPlanSqlCode.getBrandPraiseByPraisecode,new Object[]{map.get("orgcode"),session.getCurrentOrg().getOrgcode(),map.get("id")});
            map.put("isPraise",listBrandPraise!=null?Integer.valueOf(listBrandPraise.getStatus()):1);

        }
        return pager;
    }
    @Transactional(rollbackFor = Exception.class)
    public void addBrandDynamic(TDzzPartyBrandDynamic bean, Session session) {
        //验证必要参数
        if(StringUtils.isEmpty(bean.getBrandid())){
            LehandException.throwException("关联组织品牌id不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("组织编码不能为空！");
        }
        if(StringUtils.isEmpty(bean.getTitle())){
            LehandException.throwException("动态标题不能为空！");
        }
        bean.setCreator(session.getUserid().toString());
        generalSqlComponent.insert(PilotPlanSqlCode.insertBrandDynamic,bean);
    }
    @Transactional(rollbackFor = Exception.class)
    public void updateBrandDynamic(TDzzPartyBrandDynamic bean, Session session) {
        //验证必要参数
        if(StringUtils.isEmpty(bean.getBrandid())){
            LehandException.throwException("关联组织品牌id不能为空！");
        }
        if(StringUtils.isEmpty(bean.getId())){
            LehandException.throwException("单个品牌动态不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("组织编码不能为空！");
        }
        if(StringUtils.isEmpty(bean.getTitle())){
            LehandException.throwException("动态标题不能为空！");
        }
        bean.setUpdator(session.getUserid().toString());
        bean.setUpdatetime(DateEnum.YYYYMMDD.format());
        generalSqlComponent.update(PilotPlanSqlCode.updateBrandDynamic,bean);
    }

    public TDzzPartyBrandDynamicReq getBrandDynamic(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("单个品牌动态不能为空！");
        }
        TDzzPartyBrandDynamicReq brandDynamicReq = generalSqlComponent.query(PilotPlanSqlCode.getBrandDynamic,new Object[]{id});
        //品牌类型名称
        //上传图片logo
        brandDynamicReq.setFile(getFile(brandDynamicReq.getFileid()));
        //查询党组织
        Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{brandDynamicReq.getOrgcode()});
        brandDynamicReq.setOrgName(tDzzInfo==null?"":String.valueOf(tDzzInfo.get("dxzms")));
        return brandDynamicReq;
    }

    public Pager pageBrandDynamic(Pager pager, String orgcode,String status,String title,String name,String brandid) {
        if(StringUtils.isEmpty(orgcode)){
            LehandException.throwException("组织编码不能为空！");
        }
//        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("SELECT * FROM t_dzz_party_brand WHERE name=?", new Object[]{name});
        List<Map<String, Object>> mapList = generalSqlComponent.getDbComponent().listMap("SELECT * FROM t_dzz_party_brand WHERE name=?", new Object[]{name});
        List<String> listid = new ArrayList<>();
        mapList.stream().forEach(e->{
            listid.add(String.valueOf(e.get("id")));
        });
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode",orgcode);
        paramMap.put("status",status);
        paramMap.put("title",title);
        if(!StringUtils.isEmpty(name)){
            paramMap.put("brandid",org.apache.commons.lang.StringUtils.strip(listid.toString(),"[]"));
        }
        if(!StringUtils.isEmpty(brandid)){
            paramMap.put("brandid",brandid);
        }
        generalSqlComponent.pageQuery(PilotPlanSqlCode.pageBrandDynamic,paramMap,pager);
        List<TDzzPartyBrandDynamicReq> listBrandReq = (List<TDzzPartyBrandDynamicReq>) pager.getRows();
        for (TDzzPartyBrandDynamicReq brandDynamicReq: listBrandReq) {
            //上传图片logo
            brandDynamicReq.setFile(getFile(String.valueOf(brandDynamicReq.getFileid())));
            //查询党组织
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{brandDynamicReq.getOrgcode()});
            brandDynamicReq.setOrgName(tDzzInfo==null?"":String.valueOf(tDzzInfo.get("dxzms")));

            //品牌名称
            TDzzPartyBrandReq brandReq = generalSqlComponent.query(PilotPlanSqlCode.getTDzzPartyBrand, new Object[]{brandDynamicReq.getBrandid()});
            brandDynamicReq.setBrandName(brandReq.getName());
        }
        return pager;
    }
    @Transactional(rollbackFor = Exception.class)
    public void delBrandDynamic(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("单个品牌动态不能为空！");
        }
        generalSqlComponent.delete(PilotPlanSqlCode.delBrandDynamic,new Object[]{id});
    }

    public Long getBrandPraise(String orgcode,String id) {
        if(StringUtils.isEmpty(orgcode)){
            LehandException.throwException("品牌组织编码不能为空！");
        }
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("品牌id不能为空！");
        }
        List<TDzzPartyBrandPraise> listBrandPraise=generalSqlComponent.query(PilotPlanSqlCode.getBrandPraise,new Object[]{orgcode,id});
        return Long.valueOf(listBrandPraise!=null?listBrandPraise.size():0);
    }
    @Transactional(rollbackFor = Exception.class)
    public Map<String,Object> modifyBrandPraise(TDzzPartyBrandPraise bean, Session session) {
        Map<String, Object> resultMap = new HashMap<>();
        if(StringUtils.isEmpty(bean.getBrandid())){
            LehandException.throwException("品牌id不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("品牌组织编码不能为空！");
        }
        if(StringUtils.isEmpty(bean.getPraisecode())){
            LehandException.throwException("点赞的组织编码不能为空！");
        }
        TDzzPartyBrandPraise listBrandPraise=generalSqlComponent.query(PilotPlanSqlCode.getBrandPraiseByPraisecode,new Object[]{bean.getOrgcode(),bean.getPraisecode(),bean.getBrandid()});
        //是否有点赞记录
        if(listBrandPraise!=null){
            //点赞改为没点赞,没点赞改为点赞
            String status="0".equals(listBrandPraise.getStatus())?"1":"0";
            //点赞取消点赞,没有点赞加点赞
            generalSqlComponent.update(PilotPlanSqlCode.updateBrandPraise,new Object[]{status,bean.getOrgcode(),bean.getPraisecode(),bean.getBrandid()});
            //当前是否点赞
            resultMap.put("isPraise",status);
        }else {
            Map<Object, Object> map = new HashMap<>();
            map.put("orgcode",bean.getOrgcode());
            map.put("praisecode",bean.getPraisecode());
            map.put("status",0);
            map.put("brandid",bean.getBrandid());
            generalSqlComponent.insert(PilotPlanSqlCode.insertBrandPraise, map);
            resultMap.put("isPraise","0");
        }
        resultMap.put("count",getBrandPraise(bean.getOrgcode(), String.valueOf(bean.getBrandid())));
        return resultMap;
    }

    public List<Map<String,Object>> pilotPlanStatistical(String orgcode, Session session) {
        if(StringUtils.isEmpty(orgcode)){
            orgcode=session.getCurrentOrg().getOrgcode();
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgcode);
        getOrgCode(orgcode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
//        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));//统计当前组织及以下
        paramMap.put("orgcode","");//统计所有
        List<Map<String,Object>> list = generalSqlComponent.query(PilotPlanSqlCode.listPartyBrandGroupByType,paramMap);
        Map<String, Object> m1 = new HashMap<>();
        m1.put("name","培育库");
        m1.put("value",0);
        Map<String, Object> m2 = new HashMap<>();
        m2.put("name","示范库");
        m2.put("value",0);
        Map<String, Object> m3 = new HashMap<>();
        m3.put("name","推优库");
        m3.put("value",0);
        if(list!=null&&list.size()>0){
            for (Map<String,Object> map: list) {
                String type = String.valueOf(map.get("type"));
                Object count = map.get("count");

                if(SysConstants.BRAND.ONE.equals(type)){
                    m1.put("value",count);
                }
                if(SysConstants.BRAND.TWO.equals(type)){
                    m2.put("value",count);
                }
                if(SysConstants.BRAND.THREE.equals(type)){
                    m3.put("value",count);
                }
            }
        }
        List<Map<String,Object>> resultList = new ArrayList<>();
        resultList.add(m1);
        resultList.add(m2);
        resultList.add(m3);
        return resultList;
    }
    /**
     * 获取当前组织下所有组织编码
     * @param organcode
     * @return
     */
    private void getOrgCode(String organcode,List<String> listOrgCode) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        List<Map<String,Object>> list  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree2, paramMap);
        for(Map<String,Object> li:list){
            listOrgCode.add("'"+li.get("organcode").toString()+"'");
            getOrgCode(li.get("organcode").toString(),listOrgCode);
        }
    }


    public Object getStatusByOrgCode(String orgcode,Integer type) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from t_dzz_info_simple where organcode = ?", new Object[]{orgcode});
        if(map==null){
            LehandException.throwException("当前组织不存在！！");
        }
        //组织类别
        String zzlb = String.valueOf(map.get("zzlb"));
        //组织处于第几个级别
        int level=1;
        for (int i=1;i<=10;i++){
            String param="path"+ i;
            if(StringUtils.isEmpty(map.get(param))){
                level=i-1;
                break;
            }
        }
        if(2==type){
            return level;
        }
        resultList = switchStatus(level,zzlb);

        return resultList;
    }

    private List<Map<String, Object>> switchStatus(int level, String zzlb) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        //状态(0:草稿，1:上级待审核，2:上级通过，3:上级驳回，4:二级党委待审，5:二级党委通过，6:二级党委驳回，7:一级党委待审，8:一级党委通过，9:一级党委驳回，默认0)
        resultList.add(getStatus().get(0)) ;//草稿
        switch (level){
            case 1:
                //0,8
                resultList.add(getStatus().get(8)) ;//一级党委通过
                break;
            case 2:
                //0,5,7,8,9
                resultList.add(getStatus().get(5)) ;//二级党委通过
                resultList.add(getStatus().get(7)) ;//一级党委待审
                resultList.add(getStatus().get(8)) ;//一级党委通过
                resultList.add(getStatus().get(9)) ;//一级党委驳回
                break;
            case 3:
                //0,4,5,6,7,8,9
                resultList.add(getStatus().get(4)) ;//二级党委待审
                resultList.add(getStatus().get(5)) ;//二级党委通过
                resultList.add(getStatus().get(6)) ;//二级党委驳回
                resultList.add(getStatus().get(7)) ;//一级党委待审
                resultList.add(getStatus().get(8)) ;//一级党委通过
                resultList.add(getStatus().get(9)) ;//一级党委驳回
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                //0,1,2,3,4,5,6,7,8,9
                resultList.add(getStatus().get(1)) ;//上级党委待审
                resultList.add(getStatus().get(2)) ;//上级党委通过
                resultList.add(getStatus().get(3)) ;//上级党委驳回
                resultList.add(getStatus().get(4)) ;//二级党委待审
                resultList.add(getStatus().get(5)) ;//二级党委通过
                resultList.add(getStatus().get(6)) ;//二级党委驳回
                resultList.add(getStatus().get(7)) ;//一级党委待审
                resultList.add(getStatus().get(8)) ;//一级党委通过
                resultList.add(getStatus().get(9)) ;//一级党委驳回
                break;
        }

        return resultList;
    }

    private List<Map<String, Object>> getStatus() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        //状态(0:草稿，1:上级待审核，2:上级通过，3:上级驳回，4:二级党委待审，5:二级党委通过，6:二级党委驳回，7:一级党委待审，8:一级党委通过，9:一级党委驳回，默认0)
        Map<String, Object> m0 = new HashMap<>();
        m0.put("status",0);
        m0.put("name","草稿");
        Map<String, Object> m1 = new HashMap<>();
        m1.put("status",1);
        m1.put("name","上级待审核");
        Map<String, Object> m2 = new HashMap<>();
        m2.put("status",2);
        m2.put("name","上级通过");
        Map<String, Object> m3 = new HashMap<>();
        m3.put("status",3);
        m3.put("name","上级驳回");
        Map<String, Object> m4 = new HashMap<>();
        m4.put("status",4);
        m4.put("name","二级党委待审");
        Map<String, Object> m5 = new HashMap<>();
        m5.put("status",5);
        m5.put("name","二级党委通过");
        Map<String, Object> m6 = new HashMap<>();
        m6.put("status",6);
        m6.put("name","二级党委驳回");
        Map<String, Object> m7 = new HashMap<>();
        m7.put("status",7);
        m7.put("name","一级党委待审");
        Map<String, Object> m8 = new HashMap<>();
        m8.put("status",8);
        m8.put("name","一级党委通过");
        Map<String, Object> m9 = new HashMap<>();
        m9.put("status",9);
        m9.put("name","一级党委驳回");
        resultList.add(m0);
        resultList.add(m1);
        resultList.add(m2);
        resultList.add(m3);
        resultList.add(m4);
        resultList.add(m5);
        resultList.add(m6);
        resultList.add(m7);
        resultList.add(m8);
        resultList.add(m9);
        return resultList;
    }
    @Transactional(rollbackFor = Exception.class)
    public void reportAuditPartyBrand(String id,String orgcode,String remake,Integer type, Session session) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id不能为空！！");
        }
        if(StringUtils.isEmpty(orgcode)){
            LehandException.throwException("品牌组织编码code不能为空！！");
        }
        if(StringUtils.isEmpty(type)){
            LehandException.throwException("按钮类型不能为空！！");
        }
        if(4==type){//驳回状态
            if(StringUtils.isEmpty(remake)){
                LehandException.throwException("驳回意见不能为空！！");
            }
        }
        //查看单个品牌信息
        TDzzPartyBrandReq brandReq = generalSqlComponent.query(PilotPlanSqlCode.getTDzzPartyBrand,new Object[]{id});
        if(brandReq==null){
            LehandException.throwException("品牌不存在！！");
        }
        Map<String,Object> dzz = generalSqlComponent.getDbComponent().getMap(SqlCode.getDZZInfoSimpleByCode,new Object[] {orgcode});
        if(dzz==null){
            LehandException.throwException("党组织信息为空！");
        }
        TDzzPartyBrand partyBrand = new TDzzPartyBrand();
        //上级组织编码
        String parentcode = String.valueOf(dzz.get("parentcode"));
        //1：提交/2：上报/3：通过:4：驳回
        if(1==type){//提交按钮
            //上级组织编码
            //获取当前组织状态
            List<Map<String, Object>> statusByOrgCode = (List<Map<String, Object>>) getStatusByOrgCode(orgcode,1);
            //上一级状态  提交都是当前草稿状态,提交设置成草稿的上一级状态
            Map<String, Object> map = statusByOrgCode.get(1);
            partyBrand.setStatus((Integer) map.get("status"));
            //需要审核的组织设置成父类orgcode
            partyBrand.setAuditorgcode(parentcode);
            //提交还是当前状态
            partyBrand.setLevel(brandReq.getLevel());
            if(5==partyBrand.getStatus()){//是二级党委提交直接通过,级别为事业部级
                partyBrand.setLevel(1);//事业部级
                partyBrand.setAuditorgcode(orgcode);
            }else if(8==partyBrand.getStatus()){//是一级党委提交直接通过,级别为公司部级
                partyBrand.setLevel(2);//公司部级
            }
        }
        if(2==type){//上报按钮
            //上报都是当前通过状态,上报设置+2到上面待审核状态
            partyBrand.setStatus(brandReq.getStatus()+2);
            //需要审核的组织设置成父类orgcode,一级党委不存在上报
            partyBrand.setAuditorgcode(parentcode);
            //上报还是之前通过状态的级别
            partyBrand.setLevel(brandReq.getLevel());
        }
        if(3==type){//通过按钮
            //通过之前都是待审核状态,+1到上面通过状态
            partyBrand.setStatus(brandReq.getStatus()+1);
            //需要审核的组织设置原通过组织orgcode
            partyBrand.setAuditorgcode(orgcode);
            //上报还是之前通过状态的级别
            partyBrand.setLevel(brandReq.getLevel());
            if(5==partyBrand.getStatus()){
                partyBrand.setLevel(1);//事业部级
            }
            if(8==partyBrand.getStatus()){
                partyBrand.setLevel(2);//公司部级
            }
        }
        if(4==type){//驳回按钮
            //驳回之前都是待审核状态,+2到上面驳回状态
            partyBrand.setStatus(brandReq.getStatus()+2);
            //需要审核的组织设置原通过组织orgcode
            partyBrand.setAuditorgcode(orgcode);
            partyBrand.setLevel(brandReq.getLevel());
            partyBrand.setRemark(remake);
        }
        partyBrand.setId(brandReq.getId());
        partyBrand.setUpdator(session.getUserid().toString());
        partyBrand.setUpdatetime(DateEnum.YYYYMMDD.format());
        generalSqlComponent.update(PilotPlanSqlCode.updateBrandStatus,partyBrand);
    }

    public void updateDynamicStatus(String id, String status, Session session) {
        TDzzPartyBrandDynamic bean=new TDzzPartyBrandDynamic();
        bean.setId(Long.valueOf(id));
        bean.setStatus(status);
        bean.setUpdator(session.getUserid().toString());
        bean.setUpdatetime(DateEnum.YYYYMMDD.format());
        generalSqlComponent.update(PilotPlanSqlCode.updateDynamicStatus,bean);
    }
}
