package com.lehand.horn.partyorgan.service.info;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.PartyGroupDto;
import com.lehand.horn.partyorgan.pojo.partygroup.PartyGroup;
import com.lehand.horn.partyorgan.pojo.partygroup.PartyGroupMember;
import com.lehand.horn.partyorgan.util.DcitCacheServiceUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PartyGroupService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private DcitCacheServiceUtil dcitCacheServiceUtil;

    /**
     * 查询可选择的党小组成员
     * @return
     */
    public List<Map<String, Object>> getOptionalMember(String organid, Long groupid, Session session, String xm) {
        Map<String, Object> param = new HashMap<>();
        param.put("compid", session.getCompid());
        param.put("organid", organid);
        param.put("groupid", groupid);
        param.put("xm", xm);

        List<Map<String, Object>> list = generalSqlComponent.query(SqlCode.getOptionalMember, param);
        for(Map<String, Object> map : list) {
            if(StringUtils.isEmpty(map.get("dnzwname"))) {
                map.put("dnzwmc", "党员");
            } else {
                map.put("dnzwmc", dcitCacheServiceUtil.getName("d_dy_dnzw", map.get("dnzwname")));
            }
        }
        return list;
    }

    /**
     * 新增党小组信息
     * @param partyGroupDto
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public PartyGroupDto addPartyGroup(PartyGroupDto partyGroupDto, Session session) {
        //校验
        if(StringUtils.isEmpty(partyGroupDto.getName()) || StringUtils.isEmpty(partyGroupDto.getOrgcode())) {
            LehandException.throwException("提交的参数信息不完整，请重试!");
        }
        if(partyGroupDto.getPartyGroupMemberList() == null || partyGroupDto.getPartyGroupMemberList().size() < 0) {
            LehandException.throwException("请先选择党小组成员！");
        }
        partyGroupDto.setCompid(session.getCompid());
        partyGroupDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        partyGroupDto.setCreateid(String.valueOf(session.getUserid()));
        partyGroupDto.setCreatename(session.getUsername());

        //校验同一党支部下不能存在同名的党小组名称
        boolean flag = checkIsExistsName(partyGroupDto.getOrgcode(), partyGroupDto.getName(), session);
        if(flag) {
            LehandException.throwException("同一党支部下不可以出现同名的党小组名称！");
        }

        //校验党小组成员信息
        checkPartyGroup(partyGroupDto.getPartyGroupMemberList(), session, -1L);

        //保存党小组基本信息
        Long id = generalSqlComponent.insert(SqlCode.addPartyGroup, partyGroupDto);
        partyGroupDto.setId(id);
        //保存党小组成员信息
        for(PartyGroupMember partyGroupMember : partyGroupDto.getPartyGroupMemberList()) {
            partyGroupMember.setCompid(session.getCompid());
            partyGroupMember.setGroupid(id);
            partyGroupMember.setGroupname(partyGroupDto.getName());
            generalSqlComponent.insert(SqlCode.addPartyGroupMember, partyGroupMember);
        }
        return partyGroupDto;
    }

    /**
     * 修改党小组信息
     * @param partyGroupDto
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public PartyGroupDto modifyPartyGroup(PartyGroupDto partyGroupDto, Session session) {
        //校验
        if(StringUtils.isEmpty(partyGroupDto.getName()) || StringUtils.isEmpty(partyGroupDto.getOrgcode()) || StringUtils.isEmpty(partyGroupDto.getId())) {
            LehandException.throwException("提交的参数信息不完整，请重试!");
        }
        if(partyGroupDto.getPartyGroupMemberList() == null || partyGroupDto.getPartyGroupMemberList().size() < 0) {
            LehandException.throwException("请先选择党小组成员！");
        }
        partyGroupDto.setCompid(session.getCompid());

        //查询被修改的党小组是否存在
        PartyGroupDto groupDto = generalSqlComponent.query(SqlCode.getPartyGroup, new Object[] {partyGroupDto.getId(), session.getCompid()});
        if(groupDto == null) {
            LehandException.throwException("党小组信息不存在，请刷新后重试！");
        }

        //校验同一党支部下不能存在同名的党小组名称
        boolean flag = checkIsExistsName(partyGroupDto.getOrgcode(), partyGroupDto.getName(), session);
        if(flag && !groupDto.getName().equals(partyGroupDto.getName())) {
            LehandException.throwException("同一党支部下不可以出现同名的党小组名称！");
        }

        //校验党小组成员信息
        checkPartyGroup(partyGroupDto.getPartyGroupMemberList(), session, partyGroupDto.getId());

        //修改党小组信息
        generalSqlComponent.update(SqlCode.modifyPartyGroup, partyGroupDto);

        //党小组成员删除后添加
        generalSqlComponent.delete(SqlCode.delPartyGroupMember, new Object[] {partyGroupDto.getId(), session.getCompid()});
        //保存党小组成员信息
        for(PartyGroupMember partyGroupMember : partyGroupDto.getPartyGroupMemberList()) {
            partyGroupMember.setCompid(session.getCompid());
            partyGroupMember.setGroupid(partyGroupDto.getId());
            generalSqlComponent.insert(SqlCode.addPartyGroupMember, partyGroupMember);
        }
        return partyGroupDto;
    }

    /**
     * 校验党小组成员是否存在，党小组组长是否存在（有且仅一位）
     * @param partyGroupMembers
     * @param groupid 需要排除的小组成员
     * @param session
     */
    public void checkPartyGroup(List<PartyGroupMember> partyGroupMembers, Session session, Long groupid) {
        if(partyGroupMembers == null) {
            LehandException.throwException("党小组成员集合为空！");
        }

        StringBuffer midsBuffer = new StringBuffer("");//党小组成员集合
        int n = 0;//党小组组长个数
        for(PartyGroupMember partyGroupMember : partyGroupMembers) {
            if("1".equals(String.valueOf(partyGroupMember.getType()))) {
                n++;
            }
            midsBuffer.append(partyGroupMember.getMemberid()).append(",");
        }
        //校验是否存在党小组组长
        if(n != 1) {
            LehandException.throwException("当前党小组未设置组长或设置多个，请检查后添加！");
        }
        //删除最后一个多余的逗号
        if((midsBuffer.lastIndexOf(",") + 1) == midsBuffer.toString().length()) {
            midsBuffer.deleteCharAt(midsBuffer.length() -1);
        }

        //查询成员是否存在其他党小组中
        List<PartyGroupMember> memberList = generalSqlComponent.query(SqlCode.checkPartyGroupExists, new Object[] {session.getCompid(), midsBuffer.toString(), groupid});
        if(memberList.size() > 0) {
            StringBuffer existMembers = new StringBuffer("");
            for(PartyGroupMember partyGroupMember : memberList) {
                existMembers.append(partyGroupMember.getMembername()).append(",");
            }
            LehandException.throwException(String.format("已经存在的成员列表：%s，请重新选择！", existMembers.toString()));
        }
    }


    /**
     * 分页查询党小组列表
     * @param orgcode
     * @param pager
     * @param session
     * @return
     */
    public Pager queryPartyGroupList(String orgcode, Pager pager, Session session) {
        if(StringUtils.isEmpty(orgcode)) {
            LehandException.throwException("组织机构编码不能为空！");
        }
        Pager page = generalSqlComponent.pageQuery(SqlCode.queryPartyGroupList,
                new Object[] {orgcode, session.getCompid()}, pager);
        PagerUtils<PartyGroupDto> pagerUtils = new PagerUtils<PartyGroupDto>(page);
        for(PartyGroupDto partyGroupDto : pagerUtils.getRows()) {
            List<PartyGroupMember> partyGroupMemberList = generalSqlComponent.query(SqlCode.getPartyGroupMemers,
                    new Object[] {partyGroupDto.getId(), session.getCompid()});
            partyGroupDto.setPartyGroupMemberList(partyGroupMemberList);
        }
        return page;
    }

    /**
     * 查询党小组信息
     * @param id
     * @param session
     * @return
     */
    public PartyGroupDto getPartyGroup(Long id, Session session) {
        if(StringUtils.isEmpty(id)) {
            LehandException.throwException("党小组ID不能为空！");
        }
        PartyGroupDto partyGroupDto = generalSqlComponent.query(SqlCode.getPartyGroup, new Object[] {id, session.getCompid()});
        if(partyGroupDto == null) {
            LehandException.throwException("党小组信息不存在，请刷新后重试！");
        }
        List<PartyGroupMember> partyGroupMemberList = generalSqlComponent.query(SqlCode.getPartyGroupMemers,
                new Object[] {id, session.getCompid()});
        partyGroupDto.setPartyGroupMemberList(partyGroupMemberList);
        return partyGroupDto;
    }

    /**
     * 删除党小组信息和关联的成员信息
     * @param id
     * @param session
     */
    public void delPartyGroup(Long id, Session session) {
        PartyGroupDto partyGroupDto = generalSqlComponent.query(SqlCode.getPartyGroup, new Object[] {id, session.getCompid()});
        if(partyGroupDto == null) {
            LehandException.throwException("党小组信息不存在，请刷新后重试！");
        }
        //删除成员
        generalSqlComponent.delete(SqlCode.delPartyGroupMember, new Object[] {id, session.getCompid()});
        //删除党小组信息
        generalSqlComponent.delete(SqlCode.delPartyGroup, new Object[] {id, session.getCompid()});
    }

    /**
     * 查询党小组名称是否重复
     * @param orgcode
     * @param name
     * @param session
     * @return
     */
    public boolean checkIsExistsName(String orgcode, String name, Session session) {
        PartyGroup partyGroup = generalSqlComponent.query(SqlCode.queryExitsGroupName, new Object[] {orgcode, name, session.getCompid()});
        if(partyGroup == null) {
            return false;
        }
        return true;
    }

}
