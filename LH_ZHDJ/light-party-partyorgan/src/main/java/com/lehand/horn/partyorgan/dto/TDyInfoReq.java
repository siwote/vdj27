package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
import io.swagger.annotations.ApiModelProperty;

/**
 * @program: huaikuang-dj
 * @description: TDyInfoReq
 * @author: zwd
 * @create: 2020-12-04 15:08
 */
public class TDyInfoReq extends TDyInfo {

    @ApiModelProperty(value="学位", name="xw")
    private String  xw;

    @ApiModelProperty(value="毕业院校", name="byyx")
    private String  byyx;

    @ApiModelProperty(value="专业", name="zy")
    private String  zy;

    @ApiModelProperty(value="从事专业技术职务", name="jszc")
    private String  jszc;

    @ApiModelProperty(value="新社会阶层类型", name="xshjclx")
    private String  xshjclx;


    @ApiModelProperty(value="入党介绍人", name="rdjsr")
    private String rdjsr;

    @ApiModelProperty(value="其他党团", name="qtdt")
    private String qtdt;

    @ApiModelProperty(value="加入其他党团日期", name="jrqtdtrq")
    private String jrqtdtrq;

    @ApiModelProperty(value="离开其他党团日期", name="lkqtdtrq")
    private String lkqtdtrq;

    @ApiModelProperty(value="户籍地址区划内详细地址", name="hjdzQhnxxdz")
    private String  hjdzQhnxxdz;

    @ApiModelProperty(value="户籍地址省市县区代码", name="hjdzSsxqdm")
    private String  hjdzSsxqdm;

    @ApiModelProperty(value="0 本地新增,1 本地修改,-1 本地删除,9 同步", name="syncStatus")
    private String  syncStatus;

    @ApiModelProperty(value="是否公安认证", name="sfgarz")
    private String sfgarz;

    @ApiModelProperty(value="该党员是否失去联系。1-是；0-否", name="iscontact")
    private String iscontact;

    @ApiModelProperty(value="同步时间", name="anchor")
    private String anchor;

    @ApiModelProperty(value="原来的身份证号码", name="oldZJHM")
    private String oldZJHM;

    public String getOldZJHM() {
        return oldZJHM;
    }

    public void setOldZJHM(String oldZJHM) {
        this.oldZJHM = oldZJHM;
    }

    public String getAnchor() {
        return anchor;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    @Override
    public String getIscontact() {
        return iscontact;
    }

    @Override
    public void setIscontact(String iscontact) {
        this.iscontact = iscontact;
    }

    public String getSfgarz() {
        return sfgarz;
    }

    public void setSfgarz(String sfgarz) {
        this.sfgarz = sfgarz;
    }

    public String getHjdzSsxqdm() {
        return hjdzSsxqdm;
    }

    public void setHjdzSsxqdm(String hjdzSsxqdm) {
        this.hjdzSsxqdm = hjdzSsxqdm;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getHjdzQhnxxdz() {
        return hjdzQhnxxdz;
    }

    public void setHjdzQhnxxdz(String hjdzQhnxxdz) {
        this.hjdzQhnxxdz = hjdzQhnxxdz;
    }

    public String getQtdt() {
        return qtdt;
    }

    public void setQtdt(String qtdt) {
        this.qtdt = qtdt;
    }

    public String getJrqtdtrq() {
        return jrqtdtrq;
    }

    public void setJrqtdtrq(String jrqtdtrq) {
        this.jrqtdtrq = jrqtdtrq;
    }

    public String getLkqtdtrq() {
        return lkqtdtrq;
    }

    public void setLkqtdtrq(String lkqtdtrq) {
        this.lkqtdtrq = lkqtdtrq;
    }

    public String getRdjsr() {
        return rdjsr;
    }

    public void setRdjsr(String rdjsr) {
        this.rdjsr = rdjsr;
    }

    @ApiModelProperty(value="党员档案所在单位", name="dydaszdw")
    private String  dydaszdw;

    @ApiModelProperty(value="户籍所在地", name="hjdz_qhnxxdz")
    private String  hjdz_qhnxxdz;

    public String getXw() {
        return xw;
    }

    public void setXw(String xw) {
        this.xw = xw;
    }

    public String getByyx() {
        return byyx;
    }

    public void setByyx(String byyx) {
        this.byyx = byyx;
    }

    public String getZy() {
        return zy;
    }

    public void setZy(String zy) {
        this.zy = zy;
    }



    public String getJszc() {
        return jszc;
    }

    public void setJszc(String jszc) {
        this.jszc = jszc;
    }

    public String getXshjclx() {
        return xshjclx;
    }

    public void setXshjclx(String xshjclx) {
        this.xshjclx = xshjclx;
    }



    public String getDydaszdw() {
        return dydaszdw;
    }

    public void setDydaszdw(String dydaszdw) {
        this.dydaszdw = dydaszdw;
    }

    public String getHjdz_qhnxxdz() {
        return hjdz_qhnxxdz;
    }

    public void setHjdz_qhnxxdz(String hjdz_qhnxxdz) {
        this.hjdz_qhnxxdz = hjdz_qhnxxdz;
    }

    @ApiModelProperty(value="是否困难党员", name="povertyParty")
    private String povertyParty;

    @ApiModelProperty(value="是否党员示范岗位", name="demonstrationParty")
    private String demonstrationParty;

    public String getPovertyParty() {
        return povertyParty;
    }

    public void setPovertyParty(String povertyParty) {
        this.povertyParty = povertyParty;
    }

    public String getDemonstrationParty() {
        return demonstrationParty;
    }

    public void setDemonstrationParty(String demonstrationParty) {
        this.demonstrationParty = demonstrationParty;
    }
}