package com.lehand.horn.partyorgan.constant;


/**
 * 系统常量
 * @author taogang
 * @version 1.0
 * @date 2019年2月21日, 下午2:04:57
 */
public class SysConstants {
	
	public static interface SystemModule{
		final String horn_dev_member = "devmember5";
	}
	
	/**
	 * 基层党支部组织类型
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:07:25
	 */
	public static interface ZZLB {
		/** 党委 .*/
		final String DW = "6100000035";
		/** 党总支 .*/
		final String DZZ = "6200000036";
		/** 党支部 .*/
		final String DZB = "6300000037";
		/** 联合党支部 .*/
		final String LHDZB = "6400000038";
		/** 党委书记 */
		final String DWSJ = "3100000009";
		/** 党总之书记 */
		final String DZZSJ = "4100000013";
		/** 党支部书记 */
		final String DZBSJ = "5100000017";
		/** 党委副书记 */
		final String DWFSJ = "3200000010";
		/** 党总支副书记 */
		final String DZZFSJ = "4200000014";
		/** 党支部副书记 */
		final String DZBFSJ = "5200000018";
		/** 党委委员 */
		final String DWWY = "3400000021";
		/** 党总支委员 */
		final String DZZWY = "4300000015";
		/** 党支部委员 */
		final String DZBWY = "5300000022";

	}
	
	/**
	 * 党组织类别
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:10:07
	 */
	public static interface ORGTYPE {
		/** 支部 .*/
		final Integer DZB = 1;
		/** 总支.*/
		final Integer DZZ = 2;
		/** 党委 .*/
		final Integer DW = 3;
		/** 党小组 .*/
		final Integer DXZ = 4;
	}
	
	/**
	 * 是否可用 E 可用  D 不可用
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:07:00
	 */
	public static interface YnStr {
		/** 可用 .*/
		final String E = "E";
		/** 不可用 .*/
		final String D = "D";
	}
	
	/**
	 * 党员类别
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月21日, 下午2:35:21
	 */
	public static interface DYLB {
		/** 正式党员 .*/
//		final Integer ZSDY = 1;
		final String ZSDY = "1000000001";
		/** 预备党员 .*/
//		final Integer YBDY = 2;
		final String YBDY = "2000000002";
		/** 发展对象 .*/
//		final Integer FZDX = 3;
		final String FZDX =  "3000000003";
		/** 积极分子 .*/
//		final Integer JJFZ = 4;
		final String JJFZ = "4000000004";
		/** 申请人 .*/
//		final Integer SQR = 5;
		final String SQR = "5000000005";
	}
	
	/**
	 * 党员状态
	 * @author tangtao
	 * @date 2019年8月1日 
	 * @description 
	 */
	public static interface DYZT{
		
		/** 已跨省（系统）转出*/
		final String XTZC = "5000000001";
		
		/** 正常*/
		final String ZC = "1000000003";
		
		/** 已死亡*/
		final String YSW = "2000000004";
		
		/** 已出党*/
		final String YCD = "3000000005";
		
		/** 已停止党籍*/
		final String YTZDJ = "4000000006";
		
		/** 已删除*/
		final String YSC = "6000000006";
	}
	
	/**
	 * 组织关系转接
	 * @author taogang
	 * @version 1.0
	 * @date 2019年2月25日, 上午10:03:36
	 */
	public static interface ZZGX{
		/** 转入 .*/
		final Integer ZR = 2;
		/** 转出 .*/
		final Integer ZC = 1;
	}
	
	public static interface ORGID{
		final String orgId = "64BB6D7CEB6F4400B19990A9FB089AC6";
	}

	/**
	 * 党员性别
	 */
	public static interface DYXB{

		/**男*/
		final String MALE = "1000000003";

		/**女*/
		final String FEMALE = "2000000004";

		/**未说明的*/
		final String UNDEFIND = "9000000001";

		/**未知的*/
		final String UNKNOWN = "0000000002";
	}

	/**
	 *默认密码
	 */
	public static interface DEFAULTPWD {
		final String SIMPLESIX = "3Jvk6N9ujw1fACyEMQ4iqA==";
	}

	/**
	 *默认密码  交控
	 */
	public static interface DEFAULTPWD2 {
		final String SIMPLESIX = "cWFWbDaIX6aZPT/xcWPTCw==";
	}

	/**
	 * 用户证件类型
	 */
	public static interface IDTYPE{

		/**身份证*/
		final int ID_CARD = 0;

		/**护照*/
		final int PASSPROT = 1;

		/**组织机构*/
		final int ORG_CODE = 2;

	}

	/**
	 * 会议类型编码
	 */
	public static interface MTYPE{

		/** 组织生活总体*/
		final String OLM_CODE = "OLM_CODE";

		/** 三会一课*/
		final String TMOC = "TMOC";

		/** 支部委员会*/
		final String  BRANCH_COMMITTEE = "branch_committee";

		/**支部党员大会*/
		final String BRANCH_PARTY_CONGRESS = "branch_party_congress";

		/** 党小组会*/
		final String PARTY_GROUP_MEETING = "party_group_meeting";

		/**上党课 */
		final String PARTY_LECTURE = "party_lecture";
	}

	/**
	 * 会议状态
	 */
	public static interface  MSTATUS{

		/** 删除 */
		final int deleted = 0;

		/** 保存 */
		final int saved = 1;

		/** 提交 */
		final int submitted = 2;

		/** 撤销 */
		final int cancled = 3;
	}

	/**
	 * 会议达标状态
	 */
	public static interface MSTANDARD{

		/** 未达标 */
		final int UNREACHED = 0;

		/** 已达标 */
		final int REACHED = 1;

		/** 无要求*/
		final int NO_REQUIREMENT = 2;
	}

	/**
	 * 会议召开要求
	 */
	public static interface mrequirementtype{

		/** 每月 */
		final String permon = "permon";

		/** 没季度 */
		final String perseason = "perseason";
	}


	/**
	 * 默认角色id
	 */
	public static interface DEFAULTROLEID{

		/** 党员*/
		final  String PARTY_MEMBER_ROLE = "PARTYMEMBER";

		/** 书记*/
		final  String PARTY_MEMBER_SJ_ROLE = "PARTYSECRETARY";

		/** 副书记*/
		final  String PARTY_MEMBER_FSJ_ROLE = "PARTYVICESECRETARY";

		/** 委员*/
		final  String PARTY_MEMBER_WY_ROLE = "PARTYCOMMITMEMBER";

		/** 党支部 */
		final  String PARTY_BRANCH_ROLE = "PARTYBRANCH";

		/** 党总支 */
		final  String GENERAL_PARTY_BRANCH_ROLE = "PARTYSUPPORT";

		/** 党委 */
		final  String PARTY_COMMITTEE_ROLE = "PARTYCOMMIT";

		/** 系统管理员 */
		final  String ADMIN = "ADMIN";

		/** 普通用户 */
		final  String PUBLIC = "PUBLIC";

		/** 分级管理员 */
		final  String SUBAMIND = "SUBAMIND";
	}
	
	/**
	 * 	任职状态
	 * @author admin
	 *
	 */
	public static interface RZZT{
		/**在任*/
		final String ZR = "0";
		/**离任*/
		final String LR = "1";
	}
	/**
	 * 	党建品牌
	 * @author lx
	 * 1:培育品牌、2:示范品牌、3:推优上级品牌
	 */
	public static interface BRAND{
		/**培育品牌*/
		final String ONE = "1";
		/**示范品牌*/
		final String TWO = "2";
		/**推优上级品牌*/
		final String THREE = "3";
	}
}
