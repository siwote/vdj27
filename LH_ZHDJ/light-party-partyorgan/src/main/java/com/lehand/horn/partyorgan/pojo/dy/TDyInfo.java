package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 党员信息表
 * @author code maker
 */
@ApiModel(value="党员信息表",description="党员信息表")
public class TDyInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 党员ID
	 */
	@ApiModelProperty(value="党员ID",name="userid")
	private String userid;
	
	/**
	 * 所在党支部代码
	 */
	@ApiModelProperty(value="所在党支部代码",name="organid")
	private String organid;
	
	/**
	 * 姓名
	 */
	@ApiModelProperty(value="姓名",name="xm")
	private String xm;
	
	/**
	 * 性别
	 */
	@ApiModelProperty(value="性别",name="xb")
	private String xb;
	
	/**
	 * 民族
	 */
	@ApiModelProperty(value="民族",name="mz")
	private String mz;
	
	/**
	 * 籍贯
	 */
	@ApiModelProperty(value="籍贯",name="jg")
	private String jg;
	
	/**
	 * 学历
	 */
	@ApiModelProperty(value="学历",name="xl")
	private String xl;
	
	/**
	 * 出生日期
	 */
	@ApiModelProperty(value="出生日期",name="csrq")
	private String csrq;
	
	/**
	 * 年龄
	 */
	@ApiModelProperty(value="年龄",name="age")
	private String age;
	
	/**
	 * 移动电话
	 */
	@ApiModelProperty(value="移动电话",name="lxdh")
	private String lxdh;
	
	/**
	 * 公民身份号码
	 */
	@ApiModelProperty(value="公民身份号码",name="zjhm")
	private String zjhm;
	
	/**
	 * 工作岗位
	 */
	@ApiModelProperty(value="工作岗位",name="gzgw")
	private String gzgw;
	
	/**
	 * 加入党组织日期
	 */
	@ApiModelProperty(value="加入党组织日期",name="rdsj")
	private String rdsj;
	
	/**
	 * 转为正式党员日期
	 */
	@ApiModelProperty(value="转为正式党员日期",name="zzsj")
	private String zzsj;
	
	/**
	 * 人员状态
	 */
	@ApiModelProperty(value="人员状态",name="dyzt")
	private String dyzt;
	
	/**
	 * 人员类别
	 */
	@ApiModelProperty(value="人员类别",name="dylb")
	private String dylb;
	
	/**
	 * 是否台湾省籍
	 */
	@ApiModelProperty(value="是否台湾省籍",name="sftwj")
	private String sftwj;
	
	/**
	 * 党级修改时间
	 */
	@ApiModelProperty(value="党级修改时间",name="djxgsj")
	private String djxgsj;
	
	/**
	 * 是否失联
	 */
	@ApiModelProperty(value="是否失联",name="iscontact")
	private String iscontact;
	
	/**
	 * 是否在流动
	 */
	@ApiModelProperty(value="是否在流动",name="sfzld")
	private String sfzld;

	/**
	 * 是否农民工
	 */
	@ApiModelProperty(value="是否农民工",name="isworkers")
	private String isworkers;
	
	/**
	 * 信息完整度
	 */
	@ApiModelProperty(value="信息完整度",name="xxwzd")
	private String xxwzd;
	
	/**
	 * 更新操作人
	 */
	@ApiModelProperty(value="更新操作人",name="updator")
	private String updator;
	
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value="更新时间",name="updatetime")
	private String updatetime;
	
	/**
	 * 删除标记
	 */
	@ApiModelProperty(value="删除标记",name="delflag")
	private String delflag;
	
	/**
	 * 数据来源
	 */
	@ApiModelProperty(value="数据来源",name="sjly")
	private String sjly;
	
	/**
	 * 失联原因
	 */
	@ApiModelProperty(value="失联原因",name="lostreason")
	private String lostreason;
	
	/**
	 * 失联时间
	 */
	@ApiModelProperty(value="失联时间",name="losttime")
	private String losttime;
	
	/**
	 * 人员排序
	 */
	@ApiModelProperty(value="人员排序",name="sortnum")
	private String sortnum;
	
	/**
	 * 操作人名称
	 */
	@ApiModelProperty(value="操作人名称",name="operatorname")
	private String operatorname;
	
	/**
	 * 外出联系
	 */
	@ApiModelProperty(value="外出联系",name="wclx")
	private String wclx;
	
	/**
	 * 身份证重复标识
	 */
	@ApiModelProperty(value="身份证重复标识",name="idcardmult")
	private String idcardmult;
	
	/**
	 * 党籍党员状态
	 */
	@ApiModelProperty(value="党籍党员状态",name="djdyzt")
	private String djdyzt;
	
	/**
	 * 离开中共组织日期
	 */
	@ApiModelProperty(value="离开中共组织日期",name="lkzgzzrq")
	private String lkzgzzrq;
	
	/**
	 * 其他联系电话
	 */
	@ApiModelProperty(value="其他联系电话",name="gddh")
	private String gddh;
	
	/**
	 * 党员表状态
	 */
	@ApiModelProperty(value="党员表状态",name="dytablestate")
	private String dytablestate;
	
	/**
	 * 首次录入时间
	 */
	@ApiModelProperty(value="首次录入时间",name="createtime")
	private String createtime;
	
	/**
	 * 身份证号有效性校验
	 */
	@ApiModelProperty(value="身份证号有效性校验",name="idcardvalidity")
	private String idcardvalidity;
	
	/**
	 * 操作类型（1-首次录入；2-更新（编辑）；3-更新（删除））
	 */
	@ApiModelProperty(value="操作类型（1-首次录入；2-更新（编辑）；3-更新（删除））",name="operatetype")
	private String operatetype;
	
	/**
	 * 首次录入操作人
	 */
	@ApiModelProperty(value="首次录入操作人",name="creator")
	private String creator;
	
	/**
	 * 该人现居住的地址。
	 */
	@ApiModelProperty(value="该人现居住的地址。",name="xjzd")
	private String xjzd;
	
	/**
	 * 进入当前信息库类别
	 */
	@ApiModelProperty(value="进入当前信息库类别",name="jrdqxxklb")
	private String jrdqxxklb;
	
	/**
	 * 离开当前信息库类别
	 */
	@ApiModelProperty(value="离开当前信息库类别",name="lkdqxxklb")
	private String lkdqxxklb;
	
	/**
	 * 出党原因
	 */
	@ApiModelProperty(value="出党原因",name="cdreason")
	private String cdreason;
	
	/**
	 * 党组织三位一体层级代码
	 */
	@ApiModelProperty(value="党组织三位一体层级代码",name="treecodepath")
	private String treecodepath;
	
	/**
	 * 特殊情况入党，6、直接入党；7，第一线入党；8、追认为党员
	 */
	@ApiModelProperty(value="特殊情况入党，6、直接入党；7，第一线入党；8、追认为党员",name="tsqkrdfs")
	private String tsqkrdfs;
	
	/**
	 * A本地新增,E 本地修改,D 本地删除,Q 同步
	 */
	@ApiModelProperty(value="A本地新增,E 本地修改,D 本地删除,Q 同步",name="syncstatus")
	private String syncstatus;
	
	/**
	 * 同步的时间
	 */
	@ApiModelProperty(value="同步的时间",name="synctime")
	private String synctime;
	
	/**
	 * 党小组id
	 */
	@ApiModelProperty(value="党小组id",name="teamId")
	private String teamId;
	
	/**
	 * 党小组名称
	 */
	@ApiModelProperty(value="党小组名称",name="teamName")
	private String teamName;
	
	/**
	 * 是否选中
	 */
	@ApiModelProperty(value="是否选中",name="doesShow")
	private String doesShow;
	
	/**
	 * 是否组长
	 */
	@ApiModelProperty(value="是否组长",name="isGroup")
	private String isGroup;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="sfgarzDm")
	private String sfgarzDm;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="lhzbdwmc")
	private String lhzbdwmc;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="lrzzId")
	private String lrzzId;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="czzzId")
	private String czzzId;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="xxwzdyc")
	private String xxwzdyc;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="dyycxx")
	private String dyycxx;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="dljzz")
	private String dljzz;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="dydaszdw")
	private String dydaszdw;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="cszyjszwDm")
	private String cszyjszwDm;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="xshjclxDm")
	private String xshjclxDm;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="xwDm")
	private String xwDm;

	/**
	 *
	 */
	@ApiModelProperty(value="",name="byyx")
	private String byyx;

	/**
	 *
	 */
	@ApiModelProperty(value="",name="zy")
	private String zy;

	/**
	 *
	 */
	@ApiModelProperty(value="",name="hjszd")
	private String hjszd;

	/**
	 *
	 */
	@ApiModelProperty(value="",name="yxqkDm")
	private String yxqkDm;



	/**
     * setter for userid
     * @param userid
     */
	public void setUserid(String userid) {
		this.userid = userid;
	}

    /**
     * getter for userid
     */
	public String getUserid() {
		return userid;
	}

    /**
     * setter for organid
     * @param organid
     */
	public void setOrganid(String organid) {
		this.organid = organid;
	}

    /**
     * getter for organid
     */
	public String getOrganid() {
		return organid;
	}

    /**
     * setter for xm
     * @param xm
     */
	public void setXm(String xm) {
		this.xm = xm;
	}

    /**
     * getter for xm
     */
	public String getXm() {
		return xm;
	}

    /**
     * setter for xb
     * @param xb
     */
	public void setXb(String xb) {
		this.xb = xb;
	}

    /**
     * getter for xb
     */
	public String getXb() {
		return xb;
	}

    /**
     * setter for mz
     * @param mz
     */
	public void setMz(String mz) {
		this.mz = mz;
	}

    /**
     * getter for mz
     */
	public String getMz() {
		return mz;
	}

    /**
     * setter for jg
     * @param jg
     */
	public void setJg(String jg) {
		this.jg = jg;
	}

    /**
     * getter for jg
     */
	public String getJg() {
		return jg;
	}

    /**
     * setter for xl
     * @param xl
     */
	public void setXl(String xl) {
		this.xl = xl;
	}

    /**
     * getter for xl
     */
	public String getXl() {
		return xl;
	}

    /**
     * setter for csrq
     * @param csrq
     */
	public void setCsrq(String csrq) {
		this.csrq = csrq;
	}

    /**
     * getter for csrq
     */
	public String getCsrq() {
		return csrq;
	}

    /**
     * setter for age
     * @param age
     */
	public void setAge(String age) {
		this.age = age;
	}

    /**
     * getter for age
     */
	public String getAge() {
		return age;
	}

    /**
     * setter for lxdh
     * @param lxdh
     */
	public void setLxdh(String lxdh) {
		this.lxdh = lxdh;
	}

    /**
     * getter for lxdh
     */
	public String getLxdh() {
		return lxdh;
	}

    /**
     * setter for zjhm
     * @param zjhm
     */
	public void setZjhm(String zjhm) {
		this.zjhm = zjhm;
	}

    /**
     * getter for zjhm
     */
	public String getZjhm() {
		return zjhm;
	}

    /**
     * setter for gzgw
     * @param gzgw
     */
	public void setGzgw(String gzgw) {
		this.gzgw = gzgw;
	}

    /**
     * getter for gzgw
     */
	public String getGzgw() {
		return gzgw;
	}

    /**
     * setter for rdsj
     * @param rdsj
     */
	public void setRdsj(String rdsj) {
		this.rdsj = rdsj;
	}

    /**
     * getter for rdsj
     */
	public String getRdsj() {
		return rdsj;
	}

    /**
     * setter for zzsj
     * @param zzsj
     */
	public void setZzsj(String zzsj) {
		this.zzsj = zzsj;
	}

    /**
     * getter for zzsj
     */
	public String getZzsj() {
		return zzsj;
	}

    /**
     * setter for dyzt
     * @param dyzt
     */
	public void setDyzt(String dyzt) {
		this.dyzt = dyzt;
	}

    /**
     * getter for dyzt
     */
	public String getDyzt() {
		return dyzt;
	}

    /**
     * setter for dylb
     * @param dylb
     */
	public void setDylb(String dylb) {
		this.dylb = dylb;
	}

    /**
     * getter for dylb
     */
	public String getDylb() {
		return dylb;
	}

    /**
     * setter for sftwj
     * @param sftwj
     */
	public void setSftwj(String sftwj) {
		this.sftwj = sftwj;
	}

    /**
     * getter for sftwj
     */
	public String getSftwj() {
		return sftwj;
	}

    /**
     * setter for djxgsj
     * @param djxgsj
     */
	public void setDjxgsj(String djxgsj) {
		this.djxgsj = djxgsj;
	}

    /**
     * getter for djxgsj
     */
	public String getDjxgsj() {
		return djxgsj;
	}

    /**
     * setter for iscontact
     * @param iscontact
     */
	public void setIscontact(String iscontact) {
		this.iscontact = iscontact;
	}

    /**
     * getter for iscontact
     */
	public String getIscontact() {
		return iscontact;
	}

    /**
     * setter for sfzld
     * @param sfzld
     */
	public void setSfzld(String sfzld) {
		this.sfzld = sfzld;
	}

    /**
     * getter for sfzld
     */
	public String getSfzld() {
		return sfzld;
	}

    /**
     * setter for isworkers
     * @param isworkers
     */
	public void setIsworkers(String isworkers) {
		this.isworkers = isworkers;
	}

    /**
     * getter for isworkers
     */
	public String getIsworkers() {
		return isworkers;
	}

    /**
     * setter for xxwzd
     * @param xxwzd
     */
	public void setXxwzd(String xxwzd) {
		this.xxwzd = xxwzd;
	}

    /**
     * getter for xxwzd
     */
	public String getXxwzd() {
		return xxwzd;
	}

    /**
     * setter for updator
     * @param updator
     */
	public void setUpdator(String updator) {
		this.updator = updator;
	}

    /**
     * getter for updator
     */
	public String getUpdator() {
		return updator;
	}

    /**
     * setter for updatetime
     * @param updatetime
     */
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

    /**
     * getter for updatetime
     */
	public String getUpdatetime() {
		return updatetime;
	}

    /**
     * setter for delflag
     * @param delflag
     */
	public void setDelflag(String delflag) {
		this.delflag = delflag;
	}

    /**
     * getter for delflag
     */
	public String getDelflag() {
		return delflag;
	}

    /**
     * setter for sjly
     * @param sjly
     */
	public void setSjly(String sjly) {
		this.sjly = sjly;
	}

    /**
     * getter for sjly
     */
	public String getSjly() {
		return sjly;
	}

    /**
     * setter for lostreason
     * @param lostreason
     */
	public void setLostreason(String lostreason) {
		this.lostreason = lostreason;
	}

    /**
     * getter for lostreason
     */
	public String getLostreason() {
		return lostreason;
	}

    /**
     * setter for losttime
     * @param losttime
     */
	public void setLosttime(String losttime) {
		this.losttime = losttime;
	}

    /**
     * getter for losttime
     */
	public String getLosttime() {
		return losttime;
	}

    /**
     * setter for sortnum
     * @param sortnum
     */
	public void setSortnum(String sortnum) {
		this.sortnum = sortnum;
	}

    /**
     * getter for sortnum
     */
	public String getSortnum() {
		return sortnum;
	}

    /**
     * setter for operatorname
     * @param operatorname
     */
	public void setOperatorname(String operatorname) {
		this.operatorname = operatorname;
	}

    /**
     * getter for operatorname
     */
	public String getOperatorname() {
		return operatorname;
	}

    /**
     * setter for wclx
     * @param wclx
     */
	public void setWclx(String wclx) {
		this.wclx = wclx;
	}

    /**
     * getter for wclx
     */
	public String getWclx() {
		return wclx;
	}

    /**
     * setter for idcardmult
     * @param idcardmult
     */
	public void setIdcardmult(String idcardmult) {
		this.idcardmult = idcardmult;
	}

    /**
     * getter for idcardmult
     */
	public String getIdcardmult() {
		return idcardmult;
	}

    /**
     * setter for djdyzt
     * @param djdyzt
     */
	public void setDjdyzt(String djdyzt) {
		this.djdyzt = djdyzt;
	}

    /**
     * getter for djdyzt
     */
	public String getDjdyzt() {
		return djdyzt;
	}

    /**
     * setter for lkzgzzrq
     * @param lkzgzzrq
     */
	public void setLkzgzzrq(String lkzgzzrq) {
		this.lkzgzzrq = lkzgzzrq;
	}

    /**
     * getter for lkzgzzrq
     */
	public String getLkzgzzrq() {
		return lkzgzzrq;
	}

    /**
     * setter for gddh
     * @param gddh
     */
	public void setGddh(String gddh) {
		this.gddh = gddh;
	}

    /**
     * getter for gddh
     */
	public String getGddh() {
		return gddh;
	}

    /**
     * setter for dytablestate
     * @param dytablestate
     */
	public void setDytablestate(String dytablestate) {
		this.dytablestate = dytablestate;
	}

    /**
     * getter for dytablestate
     */
	public String getDytablestate() {
		return dytablestate;
	}

    /**
     * setter for createtime
     * @param createtime
     */
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

    /**
     * getter for createtime
     */
	public String getCreatetime() {
		return createtime;
	}

    /**
     * setter for idcardvalidity
     * @param idcardvalidity
     */
	public void setIdcardvalidity(String idcardvalidity) {
		this.idcardvalidity = idcardvalidity;
	}

    /**
     * getter for idcardvalidity
     */
	public String getIdcardvalidity() {
		return idcardvalidity;
	}

    /**
     * setter for operatetype
     * @param operatetype
     */
	public void setOperatetype(String operatetype) {
		this.operatetype = operatetype;
	}

    /**
     * getter for operatetype
     */
	public String getOperatetype() {
		return operatetype;
	}

    /**
     * setter for creator
     * @param creator
     */
	public void setCreator(String creator) {
		this.creator = creator;
	}

    /**
     * getter for creator
     */
	public String getCreator() {
		return creator;
	}

    /**
     * setter for xjzd
     * @param xjzd
     */
	public void setXjzd(String xjzd) {
		this.xjzd = xjzd;
	}

    /**
     * getter for xjzd
     */
	public String getXjzd() {
		return xjzd;
	}

    /**
     * setter for jrdqxxklb
     * @param jrdqxxklb
     */
	public void setJrdqxxklb(String jrdqxxklb) {
		this.jrdqxxklb = jrdqxxklb;
	}

    /**
     * getter for jrdqxxklb
     */
	public String getJrdqxxklb() {
		return jrdqxxklb;
	}

    /**
     * setter for lkdqxxklb
     * @param lkdqxxklb
     */
	public void setLkdqxxklb(String lkdqxxklb) {
		this.lkdqxxklb = lkdqxxklb;
	}

    /**
     * getter for lkdqxxklb
     */
	public String getLkdqxxklb() {
		return lkdqxxklb;
	}

    /**
     * setter for cdreason
     * @param cdreason
     */
	public void setCdreason(String cdreason) {
		this.cdreason = cdreason;
	}

    /**
     * getter for cdreason
     */
	public String getCdreason() {
		return cdreason;
	}

    /**
     * setter for treecodepath
     * @param treecodepath
     */
	public void setTreecodepath(String treecodepath) {
		this.treecodepath = treecodepath;
	}

    /**
     * getter for treecodepath
     */
	public String getTreecodepath() {
		return treecodepath;
	}

    /**
     * setter for tsqkrdfs
     * @param tsqkrdfs
     */
	public void setTsqkrdfs(String tsqkrdfs) {
		this.tsqkrdfs = tsqkrdfs;
	}

    /**
     * getter for tsqkrdfs
     */
	public String getTsqkrdfs() {
		return tsqkrdfs;
	}

    /**
     * setter for syncstatus
     * @param syncstatus
     */
	public void setSyncstatus(String syncstatus) {
		this.syncstatus = syncstatus;
	}

    /**
     * getter for syncstatus
     */
	public String getSyncstatus() {
		return syncstatus;
	}

    /**
     * setter for synctime
     * @param synctime
     */
	public void setSynctime(String synctime) {
		this.synctime = synctime;
	}

    /**
     * getter for synctime
     */
	public String getSynctime() {
		return synctime;
	}

    /**
     * setter for teamId
     * @param teamId
     */
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

    /**
     * getter for teamId
     */
	public String getTeamId() {
		return teamId;
	}

    /**
     * setter for teamName
     * @param teamName
     */
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

    /**
     * getter for teamName
     */
	public String getTeamName() {
		return teamName;
	}

    /**
     * setter for doesShow
     * @param doesShow
     */
	public void setDoesShow(String doesShow) {
		this.doesShow = doesShow;
	}

    /**
     * getter for doesShow
     */
	public String getDoesShow() {
		return doesShow;
	}

    /**
     * setter for isGroup
     * @param isGroup
     */
	public void setIsGroup(String isGroup) {
		this.isGroup = isGroup;
	}

    /**
     * getter for isGroup
     */
	public String getIsGroup() {
		return isGroup;
	}

    /**
     * setter for sfgarzDm
     * @param sfgarzDm
     */
	public void setSfgarzDm(String sfgarzDm) {
		this.sfgarzDm = sfgarzDm;
	}

    /**
     * getter for sfgarzDm
     */
	public String getSfgarzDm() {
		return sfgarzDm;
	}

    /**
     * setter for lhzbdwmc
     * @param lhzbdwmc
     */
	public void setLhzbdwmc(String lhzbdwmc) {
		this.lhzbdwmc = lhzbdwmc;
	}

    /**
     * getter for lhzbdwmc
     */
	public String getLhzbdwmc() {
		return lhzbdwmc;
	}

    /**
     * setter for lrzzId
     * @param lrzzId
     */
	public void setLrzzId(String lrzzId) {
		this.lrzzId = lrzzId;
	}

    /**
     * getter for lrzzId
     */
	public String getLrzzId() {
		return lrzzId;
	}

    /**
     * setter for czzzId
     * @param czzzId
     */
	public void setCzzzId(String czzzId) {
		this.czzzId = czzzId;
	}

    /**
     * getter for czzzId
     */
	public String getCzzzId() {
		return czzzId;
	}

    /**
     * setter for xxwzdyc
     * @param xxwzdyc
     */
	public void setXxwzdyc(String xxwzdyc) {
		this.xxwzdyc = xxwzdyc;
	}

    /**
     * getter for xxwzdyc
     */
	public String getXxwzdyc() {
		return xxwzdyc;
	}

    /**
     * setter for dyycxx
     * @param dyycxx
     */
	public void setDyycxx(String dyycxx) {
		this.dyycxx = dyycxx;
	}

    /**
     * getter for dyycxx
     */
	public String getDyycxx() {
		return dyycxx;
	}

    /**
     * setter for dljzz
     * @param dljzz
     */
	public void setDljzz(String dljzz) {
		this.dljzz = dljzz;
	}

    /**
     * getter for dljzz
     */
	public String getDljzz() {
		return dljzz;
	}

    /**
     * setter for dydaszdw
     * @param dydaszdw
     */
	public void setDydaszdw(String dydaszdw) {
		this.dydaszdw = dydaszdw;
	}

    /**
     * getter for dydaszdw
     */
	public String getDydaszdw() {
		return dydaszdw;
	}

    /**
     * setter for cszyjszwDm
     * @param cszyjszwDm
     */
	public void setCszyjszwDm(String cszyjszwDm) {
		this.cszyjszwDm = cszyjszwDm;
	}

    /**
     * getter for cszyjszwDm
     */
	public String getCszyjszwDm() {
		return cszyjszwDm;
	}

    /**
     * setter for xshjclxDm
     * @param xshjclxDm
     */
	public void setXshjclxDm(String xshjclxDm) {
		this.xshjclxDm = xshjclxDm;
	}

    /**
     * getter for xshjclxDm
     */
	public String getXshjclxDm() {
		return xshjclxDm;
	}

    /**
     * setter for xwDm
     * @param xwDm
     */
	public void setXwDm(String xwDm) {
		this.xwDm = xwDm;
	}

    /**
     * getter for xwDm
     */
	public String getXwDm() {
		return xwDm;
	}

    /**
     * setter for byyx
     * @param byyx
     */
	public void setByyx(String byyx) {
		this.byyx = byyx;
	}

    /**
     * getter for byyx
     */
	public String getByyx() {
		return byyx;
	}

    /**
     * setter for zy
     * @param zy
     */
	public void setZy(String zy) {
		this.zy = zy;
	}

    /**
     * getter for zy
     */
	public String getZy() {
		return zy;
	}

    /**
     * setter for hjszd
     * @param hjszd
     */
	public void setHjszd(String hjszd) {
		this.hjszd = hjszd;
	}

    /**
     * getter for hjszd
     */
	public String getHjszd() {
		return hjszd;
	}

    /**
     * setter for yxqkDm
     * @param yxqkDm
     */
	public void setYxqkDm(String yxqkDm) {
		this.yxqkDm = yxqkDm;
	}

    /**
     * getter for yxqkDm
     */
	public String getYxqkDm() {
		return yxqkDm;
	}

	@ApiModelProperty(value="党内职务", name="dnzwname")
	private String dnzwname;
	@ApiModelProperty(value="入党时所在支部", name="rdszdzbmc")
	private String rdszdzbmc;
	@ApiModelProperty(value="转正情况", name="zzqk")
	private String zzqk;
	@ApiModelProperty(value="党龄", name="partyAge")
	private String partyAge;
	@ApiModelProperty(value="党籍校正值", name="correcting")
	private String correcting;

	public String getDnzwname() {
		return dnzwname;
	}

	public void setDnzwname(String dnzwname) {
		this.dnzwname = dnzwname;
	}

	public String getRdszdzbmc() {
		return rdszdzbmc;
	}

	public void setRdszdzbmc(String rdszdzbmc) {
		this.rdszdzbmc = rdszdzbmc;
	}

	public String getZzqk() {
		return zzqk;
	}

	public void setZzqk(String zzqk) {
		this.zzqk = zzqk;
	}

	public String getPartyAge() {
		return partyAge;
	}

	public void setPartyAge(String partyAge) {
		this.partyAge = partyAge;
	}

	public String getCorrecting() {
		return correcting;
	}

	public void setCorrecting(String correcting) {
		this.correcting = correcting;
	}

	/**
     * TDyInfoEntity.toString()
     */
    @Override
    public String toString() {
        return "TDyInfoEntity{" +
               "userid='" + userid + '\'' +
               ", organid='" + organid + '\'' +
               ", xm='" + xm + '\'' +
               ", xb='" + xb + '\'' +
               ", mz='" + mz + '\'' +
               ", jg='" + jg + '\'' +
               ", xl='" + xl + '\'' +
               ", csrq='" + csrq + '\'' +
               ", age='" + age + '\'' +
               ", lxdh='" + lxdh + '\'' +
               ", zjhm='" + zjhm + '\'' +
               ", gzgw='" + gzgw + '\'' +
               ", rdsj='" + rdsj + '\'' +
               ", zzsj='" + zzsj + '\'' +
               ", dyzt='" + dyzt + '\'' +
               ", dylb='" + dylb + '\'' +
               ", sftwj='" + sftwj + '\'' +
               ", djxgsj='" + djxgsj + '\'' +
               ", iscontact='" + iscontact + '\'' +
               ", sfzld='" + sfzld + '\'' +
               ", isworkers='" + isworkers + '\'' +
               ", xxwzd='" + xxwzd + '\'' +
               ", updator='" + updator + '\'' +
               ", updatetime='" + updatetime + '\'' +
               ", delflag='" + delflag + '\'' +
               ", sjly='" + sjly + '\'' +
               ", lostreason='" + lostreason + '\'' +
               ", losttime='" + losttime + '\'' +
               ", sortnum='" + sortnum + '\'' +
               ", operatorname='" + operatorname + '\'' +
               ", wclx='" + wclx + '\'' +
               ", idcardmult='" + idcardmult + '\'' +
               ", djdyzt='" + djdyzt + '\'' +
               ", lkzgzzrq='" + lkzgzzrq + '\'' +
               ", gddh='" + gddh + '\'' +
               ", dytablestate='" + dytablestate + '\'' +
               ", createtime='" + createtime + '\'' +
               ", idcardvalidity='" + idcardvalidity + '\'' +
               ", operatetype='" + operatetype + '\'' +
               ", creator='" + creator + '\'' +
               ", xjzd='" + xjzd + '\'' +
               ", jrdqxxklb='" + jrdqxxklb + '\'' +
               ", lkdqxxklb='" + lkdqxxklb + '\'' +
               ", cdreason='" + cdreason + '\'' +
               ", treecodepath='" + treecodepath + '\'' +
               ", tsqkrdfs='" + tsqkrdfs + '\'' +
               ", syncstatus='" + syncstatus + '\'' +
               ", synctime='" + synctime + '\'' +
               ", teamId='" + teamId + '\'' +
               ", teamName='" + teamName + '\'' +
               ", doesShow='" + doesShow + '\'' +
               ", isGroup='" + isGroup + '\'' +
               ", sfgarzDm='" + sfgarzDm + '\'' +
               ", lhzbdwmc='" + lhzbdwmc + '\'' +
               ", lrzzId='" + lrzzId + '\'' +
               ", czzzId='" + czzzId + '\'' +
               ", xxwzdyc='" + xxwzdyc + '\'' +
               ", dyycxx='" + dyycxx + '\'' +
               ", dljzz='" + dljzz + '\'' +
               ", dydaszdw='" + dydaszdw + '\'' +
               ", cszyjszwDm='" + cszyjszwDm + '\'' +
               ", xshjclxDm='" + xshjclxDm + '\'' +
               ", xwDm='" + xwDm + '\'' +
               ", byyx='" + byyx + '\'' +
               ", zy='" + zy + '\'' +
               ", hjszd='" + hjszd + '\'' +
               ", yxqkDm='" + yxqkDm + '\'' +
               '}';
    }

}
