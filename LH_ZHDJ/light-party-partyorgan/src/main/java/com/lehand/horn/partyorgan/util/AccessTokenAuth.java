package com.lehand.horn.partyorgan.util;

import com.lehand.base.constant.DateEnum;
import com.lehand.components.bridge.info.AccessToken;

import java.text.ParseException;

public class AccessTokenAuth extends AccessToken {

    public long pulltime;

    public long getPulltime() {
        return pulltime;
    }

    public void setPulltime(long pulltime) {
        this.pulltime = pulltime;
    }

    //如果记录时间+时效值expires_in <当前时间，则失效
    public boolean isDisabled() {
        if((this.pulltime + getExpires_in()) < DateEnum.now().getTime()/1000){
            return true;
        }
        return false;
    }
}
