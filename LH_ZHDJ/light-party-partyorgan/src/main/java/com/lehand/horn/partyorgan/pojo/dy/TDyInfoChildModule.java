package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 党组织人员信息子模块表
 * @author lx
 */
@ApiModel(value="党组织人员信息子模块表",description="党组织人员信息子模块表")
public class TDyInfoChildModule implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value="id", name="id")
  private long id;
  @ApiModelProperty(value="党组织id", name="orgcode")
  private String orgcode;
  @ApiModelProperty(value="党组织名称", name="organname")
  private String organname;
  @ApiModelProperty(value="类型（1:党代表/2:统战对象/3:统战代表人士/4:人大代表/5:政协委员）", name="type")
  private String type;
  @ApiModelProperty(value="姓名", name="xm")
  private String xm;
  @ApiModelProperty(value="性别", name="xb")
  private String xb;
  @ApiModelProperty(value="政治面貌", name="zzmm")
  private String zzmm;
  @ApiModelProperty(value="出生日期", name="csrq")
  private String csrq;
  @ApiModelProperty(value="民族", name="mz")
  private String mz;
  @ApiModelProperty(value="籍贯", name="jg")
  private String jg;
  @ApiModelProperty(value="出生地址", name="csdz")
  private String csdz;
  @ApiModelProperty(value="婚姻状况", name="hyzk")
  private String hyzk;
  @ApiModelProperty(value="公民身份号码", name="zjhm")
  private String zjhm;
  @ApiModelProperty(value="加入其他党派时间", name="jrqtdpsj")
  private String jrqtdpsj;
  @ApiModelProperty(value="职称级别", name="zcjb")
  private String zcjb;
  @ApiModelProperty(value="岗位职类", name="gwzl")
  private String gwzl;
  @ApiModelProperty(value="岗位名称（现任职务）", name="gwmc")
  private String gwmc;
  @ApiModelProperty(value="全日制学历", name="qrzxl")
  private String qrzxl;
  @ApiModelProperty(value="全日制毕业院校", name="qrzbyyx")
  private String qrzbyyx;
  @ApiModelProperty(value="全日制毕业院校专业", name="qrzbyyxzy")
  private String qrzbyyxzy;
  @ApiModelProperty(value="全日制学位", name="qrzxw")
  private String qrzxw;
  @ApiModelProperty(value="在职教育学历", name="zzjyxl")
  private String zzjyxl;
  @ApiModelProperty(value="在职教育毕业院校", name="zzjybyyx")
  private String zzjybyyx;
  @ApiModelProperty(value="在职教育毕业院校专业", name="zzjybyyxzy")
  private String zzjybyyxzy;
  @ApiModelProperty(value="在职教育学位", name="zzjyxw")
  private String zzjyxw;
  @ApiModelProperty(value="宗教信仰", name="zjxy")
  private String zjxy;
  @ApiModelProperty(value="首次录入操作人", name="creator")
  private String creator;
  @ApiModelProperty(value="首次录入时间", name="createtime")
  private String createtime;
  @ApiModelProperty(value="更新人", name="updator")
  private String updator;
  @ApiModelProperty(value="更新时间", name="updatetime")
  private String updatetime;
    @ApiModelProperty(value="是否是党员(默认否)", name="partymember")
    private String partymember;


    public String getPartymember() {
        return partymember;
    }

    public void setPartymember(String partymember) {
        this.partymember = partymember;
    }

    public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getOrgcode() {
    return orgcode;
  }

  public void setOrgcode(String orgcode) {
    this.orgcode = orgcode;
  }

  public String getOrganname() {
    return organname;
  }

  public void setOrganname(String organname) {
    this.organname = organname;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public String getXm() {
    return xm;
  }

  public void setXm(String xm) {
    this.xm = xm;
  }


  public String getXb() {
    return xb;
  }

  public void setXb(String xb) {
    this.xb = xb;
  }


  public String getZzmm() {
    return zzmm;
  }

  public void setZzmm(String zzmm) {
    this.zzmm = zzmm;
  }


  public String getCsrq() {
    return csrq;
  }

  public void setCsrq(String csrq) {
    this.csrq = csrq;
  }


  public String getMz() {
    return mz;
  }

  public void setMz(String mz) {
    this.mz = mz;
  }


  public String getJg() {
    return jg;
  }

  public void setJg(String jg) {
    this.jg = jg;
  }


  public String getCsdz() {
    return csdz;
  }

  public void setCsdz(String csdz) {
    this.csdz = csdz;
  }


  public String getHyzk() {
    return hyzk;
  }

  public void setHyzk(String hyzk) {
    this.hyzk = hyzk;
  }


  public String getZjhm() {
    return zjhm;
  }

  public void setZjhm(String zjhm) {
    this.zjhm = zjhm;
  }


  public String getJrqtdpsj() {
    return jrqtdpsj;
  }

  public void setJrqtdpsj(String jrqtdpsj) {
    this.jrqtdpsj = jrqtdpsj;
  }


  public String getZcjb() {
    return zcjb;
  }

  public void setZcjb(String zcjb) {
    this.zcjb = zcjb;
  }


  public String getGwzl() {
    return gwzl;
  }

  public void setGwzl(String gwzl) {
    this.gwzl = gwzl;
  }


  public String getGwmc() {
    return gwmc;
  }

  public void setGwmc(String gwmc) {
    this.gwmc = gwmc;
  }


  public String getQrzxl() {
    return qrzxl;
  }

  public void setQrzxl(String qrzxl) {
    this.qrzxl = qrzxl;
  }


  public String getQrzbyyx() {
    return qrzbyyx;
  }

  public void setQrzbyyx(String qrzbyyx) {
    this.qrzbyyx = qrzbyyx;
  }


  public String getQrzbyyxzy() {
    return qrzbyyxzy;
  }

  public void setQrzbyyxzy(String qrzbyyxzy) {
    this.qrzbyyxzy = qrzbyyxzy;
  }


  public String getQrzxw() {
    return qrzxw;
  }

  public void setQrzxw(String qrzxw) {
    this.qrzxw = qrzxw;
  }


  public String getZzjyxl() {
    return zzjyxl;
  }

  public void setZzjyxl(String zzjyxl) {
    this.zzjyxl = zzjyxl;
  }


  public String getZzjybyyx() {
    return zzjybyyx;
  }

  public void setZzjybyyx(String zzjybyyx) {
    this.zzjybyyx = zzjybyyx;
  }


  public String getZzjybyyxzy() {
    return zzjybyyxzy;
  }

  public void setZzjybyyxzy(String zzjybyyxzy) {
    this.zzjybyyxzy = zzjybyyxzy;
  }


  public String getZzjyxw() {
    return zzjyxw;
  }

  public void setZzjyxw(String zzjyxw) {
    this.zzjyxw = zzjyxw;
  }


  public String getZjxy() {
    return zjxy;
  }

  public void setZjxy(String zjxy) {
    this.zjxy = zjxy;
  }


  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }


  public String getCreatetime() {
    return createtime;
  }

  public void setCreatetime(String createtime) {
    this.createtime = createtime;
  }


  public String getUpdator() {
    return updator;
  }

  public void setUpdator(String updator) {
    this.updator = updator;
  }


  public String getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(String updatetime) {
    this.updatetime = updatetime;
  }

}
