//package com.lehand.horn.partyorgan.dao;
//
//import org.springframework.stereotype.Repository;
//
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoBc;
//
//
//
///**
// * 组织信息补充表DAO
// * @author taogang
// * @version 1.0
// * @date 2019年1月21日, 下午8:13:52
// */
//@Repository
//public class TdzzInfoBcDao extends ComBaseDao<TDzzInfoBc> {
//
//	private static final String QUERY_BY_ID = "select * from t_dzz_info_bc where organid = ?";
//
//	/**
//	 * 返回党组织补充表对象
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public TDzzInfoBc getInfoById(String organid) {
//		return super.get(QUERY_BY_ID, TDzzInfoBc.class, organid);
//	}
//}
