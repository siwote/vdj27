package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dzz.TDwInfo;
import io.swagger.annotations.ApiModelProperty;

/**
 * @program: huaikuang-dj
 * @description: TDwInfoReq
 * @author: zwd
 * @create: 2020-12-02 10:55
 */
public class TDwInfoReq extends TDwInfo {

    @ApiModelProperty(value="单位隶属关系", name="dwlsgx")
    private String dwlsgx;

    @ApiModelProperty(value="法人代表（单位负责人）", name="dwfzr")
    private String dwfzr;

    @ApiModelProperty(value="是否党员", name="fzrsfsdy")
    private String fzrsfsdy;


    @ApiModelProperty(value="企业控制（控股）情况", name="jjlx")
    private String jjlx;


    @ApiModelProperty(value="企业规模", name="qygm")
    private String qygm;

    @ApiModelProperty(value="民营科技企业标识", name="sfmykjqy")
    private String sfmykjqy;

    @ApiModelProperty(value="在岗职工数", name="zgzgs")
    private String zgzgs;

    @ApiModelProperty(value="在岗职工中工人数", name="zgzgzgrs")
    private String zgzgzgrs;


    @ApiModelProperty(value="在岗专业技术人员数", name="zgzyjsrys")
    private String zgzyjsrys;

    @ApiModelProperty(value="是否建有工会", name="sfjygh")
    private String sfjygh;

    @ApiModelProperty(value="中介组织标识", name="sfzjzz")
    private String sfzjzz;

    public String getDwlsgx() {
        return dwlsgx;
    }

    public void setDwlsgx(String dwlsgx) {
        this.dwlsgx = dwlsgx;
    }

    public String getDwfzr() {
        return dwfzr;
    }

    public void setDwfzr(String dwfzr) {
        this.dwfzr = dwfzr;
    }

    public String getFzrsfsdy() {
        return fzrsfsdy;
    }

    public void setFzrsfsdy(String fzrsfsdy) {
        this.fzrsfsdy = fzrsfsdy;
    }

    public String getJjlx() {
        return jjlx;
    }

    public void setJjlx(String jjlx) {
        this.jjlx = jjlx;
    }

    public String getQygm() {
        return qygm;
    }

    public void setQygm(String qygm) {
        this.qygm = qygm;
    }

    public String getSfmykjqy() {
        return sfmykjqy;
    }

    public void setSfmykjqy(String sfmykjqy) {
        this.sfmykjqy = sfmykjqy;
    }

    public String getZgzgs() {
        return zgzgs;
    }

    public void setZgzgs(String zgzgs) {
        this.zgzgs = zgzgs;
    }

    public String getZgzgzgrs() {
        return zgzgzgrs;
    }

    public void setZgzgzgrs(String zgzgzgrs) {
        this.zgzgzgrs = zgzgzgrs;
    }

    public String getZgzyjsrys() {
        return zgzyjsrys;
    }

    public void setZgzyjsrys(String zgzyjsrys) {
        this.zgzyjsrys = zgzyjsrys;
    }

    public String getSfjygh() {
        return sfjygh;
    }

    public void setSfjygh(String sfjygh) {
        this.sfjygh = sfjygh;
    }

    public String getSfzjzz() {
        return sfzjzz;
    }

    public void setSfzjzz(String sfzjzz) {
        this.sfzjzz = sfzjzz;
    }
}