package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "党组织导出表单",description = "党组织导出表单")
public class DZZExportReq {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="组织机构ID",name="organid")
    private String organid;

    @ApiModelProperty(value="模糊查询关键字",name="likeStr")
    private String likeStr;

    @ApiModelProperty(value="是否包含下级（0 是 1 否）",name="dwlb")
    private Integer dwlb;

    @ApiModelProperty(value="页大小",name="pageSize")
    private Integer pageSize;

    @ApiModelProperty(value="页码",name="pageNo")
    private Integer pageNo;

    @ApiModelProperty(value="导出列名数据",name="exportColumns")
    private List<ExportColumnReq> exportColumns;

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }

    public String getLikeStr() {
        return likeStr;
    }

    public void setLikeStr(String likeStr) {
        this.likeStr = likeStr;
    }

    public Integer getDwlb() {
        return dwlb;
    }

    public void setDwlb(Integer dwlb) {
        this.dwlb = dwlb;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public List<ExportColumnReq> getExportColumns() {
        return exportColumns;
    }

    public void setExportColumns(List<ExportColumnReq> exportColumns) {
        this.exportColumns = exportColumns;
    }
}
