package com.lehand.horn.partyorgan.pojo.dzz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 领航计划(党建品牌)表
 * @author lx
 */
@ApiModel(value="领航计划(党建品牌)表",description="领航计划(党建品牌)表")
public class TDzzPartyBrand implements Serializable {

  private static final long serialVersionUID = 1L;
  @ApiModelProperty(value="id",name="id")
  private Long id;
  @ApiModelProperty(value="党组织编码",name="orgcode")
  private String orgcode;
  @ApiModelProperty(value="需要审核的党组织编码",name="auditorgcode")
  private String auditorgcode;
  @ApiModelProperty(value="品牌名称",name="name")
  private String name;
  @ApiModelProperty(value="品牌类型(1:培育品牌、2:示范品牌、3:推优上级品牌，默认培育品牌)",name="type")
  private String type;
  @ApiModelProperty(value="品牌级别（0：无，1：事业部级品牌，2：公司级品牌，默认0）",name="level")
  private int level;
  @ApiModelProperty(value="状态(0:草稿，1:上级待审，2:上级通过，3:上级驳回，4:二级党委待审，5:二级党委通过，6:二级党委驳回，7:一级党委待审，8:一级党委通过，9:一级党委驳回，默认0)",name="status")
  private int status;
  @ApiModelProperty(value="排序",name="sort")
  private Integer sort;
  @ApiModelProperty(value="品牌logo图片",name="fileid")
  private String fileid;
  @ApiModelProperty(value="宣传语",name="slogan")
  private String slogan;
  @ApiModelProperty(value="品牌介绍",name="introduce")
  private String introduce;
  @ApiModelProperty(value="品牌理念",name="concept")
  private String concept;
  @ApiModelProperty(value="品牌思路",name="trainofthought")
  private String trainofthought;
  @ApiModelProperty(value="品牌措施",name="measures")
  private String measures;
  @ApiModelProperty(value="首次录入操作人",name="creator")
  private String creator;
  @ApiModelProperty(value="首次录入时间",name="createtime")
  private String createtime;
  @ApiModelProperty(value="更新操作人",name="updator")
  private String updator;
  @ApiModelProperty(value="更新操作时间",name="updatetime")
  private String updatetime;
  @ApiModelProperty(value="驳回意见",name="remark")
  private String remark;

  public Integer getSort() {
    return sort;
  }

  public String getAuditorgcode() {
    return auditorgcode;
  }

  public void setAuditorgcode(String auditorgcode) {
    this.auditorgcode = auditorgcode;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOrgcode() {
    return orgcode;
  }

  public void setOrgcode(String orgcode) {
    this.orgcode = orgcode;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public String getFileid() {
    return fileid;
  }

  public void setFileid(String fileid) {
    this.fileid = fileid;
  }


  public String getSlogan() {
    return slogan;
  }

  public void setSlogan(String slogan) {
    this.slogan = slogan;
  }


  public String getIntroduce() {
    return introduce;
  }

  public void setIntroduce(String introduce) {
    this.introduce = introduce;
  }


  public String getConcept() {
    return concept;
  }

  public void setConcept(String concept) {
    this.concept = concept;
  }


  public String getTrainofthought() {
    return trainofthought;
  }

  public void setTrainofthought(String trainofthought) {
    this.trainofthought = trainofthought;
  }


  public String getMeasures() {
    return measures;
  }

  public void setMeasures(String measures) {
    this.measures = measures;
  }


  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }


  public String getCreatetime() {
    return createtime;
  }

  public void setCreatetime(String createtime) {
    this.createtime = createtime;
  }


  public String getUpdator() {
    return updator;
  }

  public void setUpdator(String updator) {
    this.updator = updator;
  }


  public String getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(String updatetime) {
    this.updatetime = updatetime;
  }

}
