//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lehand.horn.partyorgan.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.CryptogramUtil.AESPlus;
import com.lehand.components.web.ann.OpenApi;import com.lehand.horn.partyorgan.service.NewLoginService;
import com.lehand.module.urc.base.UrcBaseController;
import com.lehand.module.urc.cache.UrcCacheComponent;
import com.lehand.module.urc.config.SessionConfig;
import com.lehand.module.urc.dto.LoginRequest;
import com.lehand.module.urc.pojo.UserAccountView;
import com.lehand.module.urc.service.DingdingService;
import com.lehand.module.urc.service.UserService;
import com.lehand.module.urc.utils.Base64Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(
    value = "新登录认证",
    tags = {"新登录认证"}
)
@RestController
@RequestMapping({"/urc/new/authentication"})
public class NewLoginController extends UrcBaseController {

    @Resource
    private UrcCacheComponent urcCacheComponent;

    @Resource
    private DingdingService dingdingService;
    @Resource
    private SessionConfig sessionConfig;
    @Resource
    private UserService userService;
    @Resource
    private NewLoginService newLoginService;

    @OpenApi(optflag = 0)
    @ApiOperation(value = "发送短信验证码", notes = "发送短信验证码", httpMethod = "POST"
    )
    @RequestMapping({"/sendSMSCode"})
    public Message sendSMSCode(@ApiIgnore LoginRequest req) {
        Message msg = new Message();

        String account = req.getAccount();

        if (StringUtils.isEmpty(account)) {
            LehandException.throwException("账户不能为空!");
        }
        UserAccountView user = this.userService.getUserByAccount(account);
        if (null == user) {
            LehandException.throwException("用户名不存在");
        }
        newLoginService.createSMS(user);

        if (0 == user.getStatus()) {
            LehandException.throwException("该用户已被禁用!");
        }
        msg.success();
        return msg;
    }

    @OpenApi(optflag = 4)
    @ApiOperation(value = "账号登录", notes = "账号登录", httpMethod = "POST"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "0:账号密码登陆,1:小程序,2:企业微信,3:手机号,4:钉钉免登录", dataType = "string", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "channel", value = "登录渠道,PC:PC端, H5:H5端, APP:移动端", dataType = "string", paramType = "query", example = "PC"),
            @ApiImplicitParam(name = "account", value = "账户", dataType = "string", paramType = "query", example = "admin"),
            @ApiImplicitParam(name = "passwd", value = "密码", dataType = "string", paramType = "query", example = "admin"),
            @ApiImplicitParam(name = "captcha", value = "验证码", dataType = "string", paramType = "query", example = "xxxx"),
            @ApiImplicitParam(name = "token", value = "token令牌", dataType = "string", paramType = "query", example = "xxxx"
    )})
    @RequestMapping({"/login"})
    public Message login(@ApiIgnore LoginRequest req, HttpServletRequest request, HttpServletResponse response) {
        Message msg = new Message();
        req.validate();
        String account = req.getAccount();
        UserAccountView user = null;
        if (req.getType() == 0) {
            user = this.getUserViewByAccount(account, req.getPasswd());

            if (!req.getChannel().equals(LoginRequest.H5) && !StringUtils.isEmpty(req.getCaptcha())) {
                this.newLoginService.checkCaptcha(req,user);
            }

        } else if (req.getType() == 4) {
            if (req.getChannel().equals(LoginRequest.PC)) {
                //user = this.dingdingService.getDingUserAccountView(req.getToken());
                this.newLoginService.checkCaptcha(req,user);
            } else if (req.getChannel().equals(LoginRequest.H5)) {
                //user = this.dingdingService.getDingUserAccountView(req.getToken());
                this.newLoginService.checkCaptcha(req,user);
            } else {
                LehandException.throwException("当前登录渠道不存在");
            }

            req.setAccount(user.getAccount());
        } else {
            LehandException.throwException("当前登录方式不存在");
        }

        if (0 == user.getStatus()) {
            LehandException.throwException("该用户已被禁用!");
        }

        Session session = this.sessionConfig.createSession(req, request, user);
        msg.success().setData(session).setLog(user.getUsername() + "登录成功!");
        return msg;
    }

    private UserAccountView getUserViewByAccount(String account, String password) {
        int num = 0;
        /*int pwdCheckErrorNum = this.urcProperties.getPwdCheckErrorNum();
        if (-1 != pwdCheckErrorNum) {
            num = this.urcCacheComponent.getPwdErrorNum(account);
            if (num >= pwdCheckErrorNum) {
                LehandException.throwException("密码错误次数超过" + pwdCheckErrorNum + "次,将锁定" + this.urcProperties.getPwdCheckErrorTimeout() / 60L + "分钟");
            }
        }*/

        UserAccountView user = this.userService.getUserByAccount(account);
        if (null == user) {
            LehandException.throwException("用户名或密码错误");
        }

        String pwd = "";

        try {
            pwd = AESPlus.encrypt(Base64Utils.encodeToString(Base64Utils.decode(password)));
        } catch (Exception var8) {
            LehandException.throwException("检查密码错误");
        }

        /*if (!user.getPasswd().equals(pwd)) {
            if (-1 != pwdCheckErrorNum) {
                ++num;
                this.urcCacheComponent.setPwdErrorNum(account, num, this.urcProperties.getPwdCheckErrorTimeoutForMS());
            }

            LehandException.throwException("用户名或密码错误");
        }*/

        return user;
    }
}
