package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dzz.TDzzAdvanced;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzFunding;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

/**
 * @Description 党建工作经费信息表
 * @Author lx
 * @Date 2021/2/23
 **/
@ApiModel(value = "党建工作经费信息表扩展", description = "党建工作经费信息表扩展")
public class TDzzFundingReq extends TDzzFunding {

    @ApiModelProperty(value="附件", name="meetingAttachList")
    private List<Map<String,Object>> attachList;

    @ApiModelProperty(value="导出列名数据",name="exportColumns")
    private List<ExportColumnReq> exportColumns;

    @ApiModelProperty(value="开始时间",name="startTime")
    private String startTime;

    @ApiModelProperty(value="结束时间",name="endTime")
    private String endTime;

    public List<Map<String, Object>> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<Map<String, Object>> attachList) {
        this.attachList = attachList;
    }

    public List<ExportColumnReq> getExportColumns() {
        return exportColumns;
    }

    public void setExportColumns(List<ExportColumnReq> exportColumns) {
        this.exportColumns = exportColumns;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}