package com.lehand.horn.partyorgan.service.info;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.MemberSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dto.TDyInfoReq;
import com.lehand.horn.partyorgan.util.SysUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 导入导出Service
 * @author admin
 *
 */
@Service
public class MemberExcelService2 {
    @Resource
    GeneralSqlComponent generalSqlComponent;
    @Resource
    MemberService memberService;
    @Resource
    PartyCacheComponent partyCacheComponent;
    @Resource
    private DBComponent dBComponent;

    @Transactional
    public String memberInfoImport() {
        try {
            List<Map<String, Object>> newListData = dBComponent.listMap("SELECT * FROM `t_dy_import_data`", new Object[]{});
            if ( null != newListData && newListData.size()<=0){
                LehandException.throwException("无数据");
            }
            List<String> listNewIdCard = new ArrayList<>();
            newListData.forEach(a->{
                listNewIdCard.add(a.get("身份证号码").toString());
            });
            Set<String> setNewIdCard = new HashSet<>(listNewIdCard);
            if(listNewIdCard.size()> setNewIdCard.size()){
                LehandException.throwException("党员身份证号码重复");
            }
//            //校验是否有党员被删除了
//            List<String> listOld = dBComponent.listSingleColumn("select a.zjhm from t_dy_info  where a.zjhm in '(" + setNewIdCard.toString() + ")' and b.operatetype != 3 and b.zzlb != '3100000025' and b.zzlb != '3900000026'", String.class, new Object[]{});
////            List<String> listOld =  dBComponent.listSingleColumn("select a.zjhm from t_dy_info  a left join t_dzz_info_simple b on a.organid=b.organid where b.id_path like '%"+organids+"%' and b.operatetype != 3 and b.zzlb != '3100000025' and b.zzlb != '3900000026'",String.class,new Object[]{});
//            String sfzhs = "";
//            for(String o :listOld){
//                if(!setNewIdCard.contains(o)){
//                    //不包含将党员信息置为无效状态使用字段 zfbz=1
//                    sfzhs += "'"+o+"'"+",";
//                }
//            }
//            sfzhs = !StringUtils.isEmpty(sfzhs) ? sfzhs.substring(0,sfzhs.length()-1) : sfzhs;
//            Map params = new HashMap();
//            params.put("sfzh",sfzhs);
//            if(!StringUtils.isEmpty(sfzhs)){
//                //将 t_dy_info 置为作废
//                dBComponent.update("update t_dy_info set delflag=1 where zjhm in ("+sfzhs+")",new Object[]{});
////                //urc_user_account 删除账号
////                dBComponent.update("update urc_user set status=0 where idno in ("+sfzhs+")",new Object[]{});
//                //dBComponent.delete("delete from urc_user_account where account in ("+sfzhs+")",new Object[]{});
////                dBComponent.update("update sys_user set status=0 where acc in ("+sfzhs+")",new Object[]{});
////                dBComponent.update("update t_dzz_bzcyxx set zfbz=1 where zjhm in ("+sfzhs+")",new Object[]{});
//            }

            int total = setNewIdCard.size();

            int  insertnum = 0;

            int  updatenum = 0;
            StringBuffer buffer = new StringBuffer("导入结果: ");
            buffer.append("\r\n");
            for (int i = 0; i < total ; i++) {
                int j = 0;

                String orgname = null;
                try {
                    orgname = String.valueOf(newListData.get(i).get("所在党支部"));
                } catch (Exception e) {
                    LehandException.throwException("党员所在党支部获取错误");
                }
                Map<String, Object> map = dBComponent.getMap("select * from t_dzz_info where dzzmc = ? limit 1", new Object[]{orgname});
                if(map==null){
                    LehandException.throwException("党员组织不存在或党员组织名称匹配错误！");
                }
                //获取组织机构id,组织机构不存在则不执行导入
                String organid = map.get("organid").toString();
                String orgcode = map.get("organcode").toString();
                if(StringUtils.isEmpty(orgcode)){
                    buffer.append("第"+i+"条数据所在党支部组织机构数据不存在;");
                    buffer.append("\r\n");
                    continue;
                }
                String idcard  = String.valueOf(newListData.get(i).get("身份证号码"));
                String xm = String.valueOf(newListData.get(i).get("姓名"));
                String csrq = String.valueOf(newListData.get(i).get("出生日期"));

                //校验导入数据
                boolean reg = getValidate(i,orgcode,idcard,xm,csrq,buffer);

                if(!reg){
                    continue;
                }

                //判断党员信息是否存在,执行新增或修改操作
                String userid = dBComponent.getSingleColumn("select userid from t_dy_info  where zjhm = ? and dyzt = '1000000003'",String.class,new Object[]{idcard});
                if(StringUtils.isEmpty(userid)){//插入
                    insertMemberInfo(newListData.get(i),organid);
                    insertnum += 1;
                }else{
                    updateMemberInfo(newListData.get(i),userid,organid);
                    updatenum += 1;
                }
            }
            buffer.append("共"+(total)+"条数据,共导入成功"+(insertnum+updatenum)+"条数据;");
            return buffer.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean getValidate(int i, String orgcode, String idcard, String xm, String csrq,StringBuffer buffer) {

        //判断导入的姓名是否为空
        if(StringUtils.isEmpty(xm)){
            buffer.append("第"+i+"条数据姓名为空;");
            buffer.append("\r\n");
            return false;
        }

        //判断导入的出生日期格式是否正确yyyy-MM-dd
        if(!StringUtils.isEmpty(csrq)) {
            String regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
            Pattern pattern = Pattern.compile(regex);
            Matcher m = pattern.matcher(csrq);
            boolean dateFlag = m.matches();
            if (!dateFlag) {
               LehandException.throwException("出生日期格式错误,请检查后重新导入!");
            }
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setLenient(false);
            try {
                Date date = formatter.parse(csrq);
                System.out.println(date);
                System.out.print("格式正确!");
            } catch (Exception e) {
                LehandException.throwException("出生日期格式错误,请检查后重新导入!");
            }
        }

        //判断导入的idcard是否为空
        if(StringUtils.isEmpty(idcard)){
            buffer.append("第"+i+"条数据身份证号码为空;");
            buffer.append("\r\n");
            return false;
        }

        //判断身份证格式
        if(!SysUtil.isIDNumber(idcard)){
            buffer.append("第"+i+"条数据身份证号码有误;");
            buffer.append("\r\n");
            return false;
        }

        return true;
    }

    /**
     * 修改党员信息
     * @param data
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public int updateMemberInfo(Map<String,Object> data,String userid,String organid) {
        TDyInfoReq info = new TDyInfoReq();

        info.setUserid(userid);

        info.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        //党员是否失效
        info.setDelflag(MagicConstant.NUM_STR.NUM_0);
        //所在支部编码
        info.setOrganid(organid);
        //姓名
        info.setXm(String.valueOf(data.get("姓名")));
        //身份证号码
        info.setZjhm(String.valueOf(data.get("身份证号码")));
        //出生日期
        info.setCsrq(String.valueOf(data.get("出生日期")));
        //性别
        Map<String,Object> map=getGDictItem("d_sexid", String.valueOf(data.get("性别")));
        info.setXb(map==null ? "" : map.get("item_code").toString());
        //民族
        Map<String,Object> map1=getGDictItem("d_national", String.valueOf(data.get("民族")));
        info.setMz(map1==null ? "" : map1.get("item_code").toString());
        //籍贯
        info.setJg(String.valueOf(data.get("籍贯")));
        //年龄
        info.setAge(String.valueOf(data.get("年龄")));
        //学历
        Map<String,Object> map2=getGDictItem("d_dy_xl", String.valueOf(data.get("学历")));
        info.setXl(map2==null ? "" : map2.get("item_code").toString());
        //学位
        Map<String,Object> map3=getGDictItem("d_dy_xw", String.valueOf(data.get("学位")));
        info.setXw(map3==null ? "" : map3.get("item_code").toString());
        //毕业院校
        info.setByyx(String.valueOf(data.get("毕业院校")));
        //专业
        info.setZy(String.valueOf(data.get("专业")));
        //入党日期
        info.setRdsj(String.valueOf(data.get("入党日期")));
        //转正日期
        info.setZzsj(String.valueOf(data.get("转正日期")));
        //工作岗位
        Map<String,Object> map4=getGDictItem("d_dy_gzgw", String.valueOf(data.get("工作岗位")));
        info.setGzgw(map4==null ? "" : map4.get("item_code").toString());
        //从事专业技术职务
        Map<String,Object> map5=getGDictItem("d_dy_jszc", String.valueOf(data.get("从事专业技术职务")));
        info.setCszyjszwDm(map5==null ? "" : map5.get("item_code").toString());
        //新社会阶层类型
        Map<String,Object> map6=getGDictItem("d_dy_shjc", String.valueOf(data.get("新社会阶层类型")));
        info.setXshjclxDm(map6==null ? "" : map6.get("item_code").toString());
        //人员类别
        Map<String,Object> map7=getGDictItem("d_dy_rylb", String.valueOf(data.get("人员类别")));
        info.setDylb(map7==null ? "" : map7.get("item_code").toString());
        //是否农民工
        info.setIsworkers("否".equals(String.valueOf(data.get("是否农民工"))) ? "0" : "1");
        //手机号码
        if(String.valueOf(data.get("手机号码")).contains(".")){
            BigDecimal bd = new BigDecimal(String.valueOf(data.get("手机号码")));
            info.setLxdh(bd.toPlainString());
        }else{
            info.setLxdh(String.valueOf(data.get("手机号码")));
        }
        //联系电话
        info.setGddh(String.valueOf(data.get("联系电话")));
        //联合支部单位名称
        info.setLhzbdwmc(String.valueOf(data.get("联合支部单位名称")));
        //档案所在单位
        info.setDydaszdw(String.valueOf(data.get("档案所在单位")));
        //户籍所在地
        info.setHjdzQhnxxdz(String.valueOf(data.get("户籍所在地")));
        //现居住地
        info.setXjzd(String.valueOf(data.get("现居住地")));
        //失去联系情形
        info.setLostreason(String.valueOf(data.get("失去联系情形")));
        //失去联系日期
        info.setLosttime(String.valueOf(data.get("失去联系日期")));
        //信息完整度
        info.setXxwzd(String.valueOf(data.get("信息完整度")));
        //党员状态
        info.setDyzt(SysConstants.DYZT.ZC);
        //操作类型
        info.setOperatetype(MagicConstant.NUM_STR.NUM_0);
        //同步状态
        info.setSyncstatus("A");
        //t_dy_info
        int result = generalSqlComponent.update(MemberSqlCode.update_t_dy_info,info);
        //t_dy_info_bc
        generalSqlComponent.update(SqlCode.updateTDyInfoBc,info);
        //t_dy_info_simple
        //性别 1000000003 男 2000000004女
        String xb = info.getXb();
        if(SysConstants.DYXB.MALE.equals(xb)){
            info.setXb("1");
        }else if(SysConstants.DYXB.FEMALE.equals(xb)){
            info.setXb("2");
        }
        //民族
        String mz = info.getMz().substring(0,2);
        info.setMz(mz!=null?mz:null);
        //党员状态
        String dyzt = info.getDyzt().substring(0,1);
        info.setDyzt(dyzt!=null?dyzt:null);
        //学历
        String xl = info.getXl().substring(0,info.getXl().indexOf("0"));
        info.setXl(xl!=null?xl:null);
        //党员类别
        String dylb = info.getDylb().substring(0,1);
        info.setDylb(dylb!=null?dylb:null);
        generalSqlComponent.update(SqlCode.updateTDyInfoSimple,info);
        return result;
    }

    /**
     * 新增党员信息
     * @param data
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public String insertMemberInfo(Map<String,Object> data,String organid) {
        //党员表主键
        String key = UUID.randomUUID().toString().replace("-","");
        //新增党员信息
        TDyInfoReq info = new TDyInfoReq();
        String formatDate = DateEnum.YYYYMMDDHHMMDD.format();
        info.setUserid(key);
        info.setCreatetime(formatDate);
        //党员排序号
        info.setSortnum("1");
        //身份证重复标识
        info.setIdcardmult("0");
        //同步时间
        info.setAnchor(formatDate);
        //党员是否失效
        info.setDelflag(MagicConstant.NUM_STR.NUM_0);
        //所在支部编码
        info.setOrganid(organid);
        //姓名
        info.setXm(String.valueOf(data.get("姓名")));
        //身份证号码
        info.setZjhm(String.valueOf(data.get("身份证号码")));
        //出生日期
        info.setCsrq(String.valueOf(data.get("出生日期")));
        //性别 1000000003 男 2000000004女
        Map<String,Object> map=getGDictItem("d_sexid", String.valueOf(data.get("性别")));
        info.setXb(map==null ? "" : map.get("item_code").toString());
        //民族
        Map<String,Object> map1=getGDictItem("d_national", String.valueOf(data.get("民族")));
        info.setMz(map1==null ? "" : map1.get("item_code").toString());
        //籍贯
        info.setJg(String.valueOf(data.get("籍贯")));
        //年龄
        info.setAge(String.valueOf(data.get("年龄")));
        //学历
        Map<String,Object> map2=getGDictItem("d_dy_xl", String.valueOf(data.get("学历")));
        info.setXl(map2==null ? "" : map2.get("item_code").toString());
        //学位
        Map<String,Object> map3=getGDictItem("d_dy_xw", String.valueOf(data.get("学位")));
        info.setXw(map3==null ? "" : map3.get("item_code").toString());
        //毕业院校
        info.setByyx(String.valueOf(data.get("毕业院校")));
        //专业
        info.setZy(String.valueOf(data.get("专业")));
        //入党日期
        info.setRdsj(String.valueOf(data.get("入党日期")));
        //转正日期
        info.setZzsj(String.valueOf(data.get("转正日期")));
        //工作岗位
        Map<String,Object> map4=getGDictItem("d_dy_gzgw", String.valueOf(data.get("工作岗位")));
        info.setGzgw(map4==null ? "" : map4.get("item_code").toString());
        //从事专业技术职务
        Map<String,Object> map5=getGDictItem("d_dy_jszc", String.valueOf(data.get("从事专业技术职务")));
        info.setJszc(map5==null ? "" : map5.get("item_code").toString());
        //新社会阶层类型
        Map<String,Object> map6=getGDictItem("d_dy_shjc", String.valueOf(data.get("新社会阶层类型")));
        info.setXshjclx(map6==null ? "" : map6.get("item_code").toString());
        //人员类别
        Map<String,Object> map7=getGDictItem("d_dy_rylb", String.valueOf(data.get("人员类别")));
        info.setDylb(map7==null ? "" : map7.get("item_code").toString());
        //是否农民工
        info.setIsworkers("否".equals(String.valueOf(data.get("是否农民工"))) ? "0" : "1");
        //手机号码
        if(String.valueOf(data.get("手机号码")).contains(".")){
            BigDecimal bd = new BigDecimal(String.valueOf(data.get("手机号码")));
            info.setLxdh(bd.toPlainString());
        }else{
            info.setLxdh(String.valueOf(data.get("手机号码")));
        }
        //联系电话
        info.setGddh(String.valueOf(data.get("联系电话")));
        //联合支部单位名称
        info.setLhzbdwmc(String.valueOf(data.get("联合支部单位名称")));
        //档案所在单位
        info.setDydaszdw(String.valueOf(data.get("档案所在单位")));
        //户籍所在地
        info.setHjdzQhnxxdz(String.valueOf(data.get("户籍所在地")));
        //现居住地
        info.setXjzd(String.valueOf(data.get("现居住地")));
        //失去联系情形
        info.setLostreason(String.valueOf(data.get("失去联系情形")));
        //失去联系日期
        String Losttime = String.valueOf(data.get("失去联系日期"));
        info.setLosttime(Losttime);
        //该党员是否失去联系。1-是；0-否
        if(StringUtils.isEmpty(Losttime)){
            info.setIscontact("1");
        }else{
            info.setIscontact("0");

        }
        //信息完整度
        info.setXxwzd(String.valueOf(data.get("信息完整度")));
        //党员状态
        info.setDyzt(SysConstants.DYZT.ZC);
        //操作类型
        info.setOperatetype(MagicConstant.NUM_STR.NUM_0);
        //同步状态
        info.setSyncstatus("A");
        //是否困难党员
        info.setPovertyParty("0");
        //是否党员示范岗位
        info.setDemonstrationParty("0");
        //t_dy_info
        generalSqlComponent.insert(MemberSqlCode.insert_t_dy_info,info);
        //t_dy_info_bc
        generalSqlComponent.insert(SqlCode.addTDyInfoBc,info);
        //t_dy_info_simple
        //性别 1000000003 男 2000000004女
        String xb = info.getXb();
        if(SysConstants.DYXB.MALE.equals(xb)){
            info.setXb("1");
        }else if(SysConstants.DYXB.FEMALE.equals(xb)){
            info.setXb("2");
        }
        //民族
        String mz = info.getMz().substring(0,2);
        info.setMz(mz);
        //党员状态
        String dyzt = info.getDyzt().substring(0,1);
        info.setDyzt(dyzt);
        //学历
        String xl = info.getXl().substring(0,info.getXl().indexOf("0"));
        info.setXl(xl);
        //党员类别
        String dylb = info.getDylb().substring(0,1);
        info.setDylb(dylb);
        generalSqlComponent.insert(SqlCode.addTDyInfoSimple,info);
        return key;
    }

    private Map<String, Object> getGDictItem(String code, String itemName) {
        Map<String, Object> map = dBComponent.getMap("SELECT * FROM g_dict_item where code = ? and item_name=? limit 1",new Object[]{code,itemName});
        return map;
    }

}
