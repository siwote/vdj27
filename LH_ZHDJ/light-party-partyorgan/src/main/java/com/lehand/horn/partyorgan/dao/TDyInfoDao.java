//package com.lehand.horn.partyorgan.dao;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.stereotype.Repository;
//
//import com.lehand.base.common.Pager;
//import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
//
//
//
///**
// * 党员信息DAO层
// * @author zouwendong
// * @date 2019年1月22日 上午10:20:11
// * @version V1.0
// */
//@Repository
//public class TDyInfoDao extends ComBaseDao<TDyInfo>{
//
//	/**
//	 * 查询党员
//	 */
//	private final static String DY_TYPE = "select * from t_dy_info where userid = ?";
//
//	/**
//	 *
//	 * @Description:  获取本机构党员
//	 * @param organid
//	 * @return
//	 * @return List<TDyInfo>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月22日 上午10:31:00
//	 */
//	public List<TDyInfo> query(String sql,Pager pager,Object...args){
//		return super.pageBeanNotCount(sql, pager,TDyInfo.class, args);
//	}
//
//	/**
//	 *
//	 * @Description:  获取本机构党员 总条数
//	 * @param organid
//	 * @return
//	 * @return int
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月22日 上午10:33:10
//	 */
//	public int queryCount(String sql,Object...args) {
//		return super.getInt(sql, args);
//
//	}
//
//	/**
//	 *
//	 * @Description:查询党员基本信息
//	 * @param sql
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月22日 下午7:18:01
//	 */
//	public List<Map<String, Object>> queryEssential(String sql,String userId){
//		return super.list2map(sql, userId);
//	}
//
//	/**
//	 *
//	 * @Description: 根据userId查询
//	 * @param sql
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月22日 下午8:16:58
//	 */
//	public List<Map<String, Object>> queryParty(String sql,String userId){
//		return super.list2map(sql, userId);
//	}
//
//	public List<Map<String, Object>> querylcdy(String sql,String userId,String organid){
//		return super.list2map(sql, organid,userId);
//
//	}
//
//	public TDyInfo queryDy(String userid) {
//		return super.get(DY_TYPE, TDyInfo.class, userid);
//	}
//}
