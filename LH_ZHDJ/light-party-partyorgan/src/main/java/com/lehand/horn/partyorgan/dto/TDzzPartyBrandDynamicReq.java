package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrand;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandDynamic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * 领航计划(品牌动态)表表扩展类
 * @author lx
 */
@ApiModel(value="领航计划(品牌动态)表表扩展类",description="领航计划(品牌动态)表表扩展类")
public class TDzzPartyBrandDynamicReq extends TDzzPartyBrandDynamic {

    @ApiModelProperty(value="品牌logo图片",name="file")
    private Map<String,Object> file;

    @ApiModelProperty(value="党组织名称",name="orgname")
    private String orgName;

    @ApiModelProperty(value="品牌名称",name="brandName")
    private String brandName;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Map<String, Object> getFile() {
        return file;
    }

    public void setFile(Map<String, Object> file) {
        this.file = file;
    }


}