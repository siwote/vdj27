package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 
 * @author code maker
 */
@ApiModel(value="",description="")
public class PartyTransfer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="id")
	private Integer id;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="compid")
	private Long compid;
	
	/**
	 * 迁移类型 1：组织外迁入，2：组织内迁移，3迁出组织外
	 */
	@ApiModelProperty(value="迁移类型 1：组织外迁入，2：组织内迁移，3迁出组织外",name="transfertype",required=true)
	private Integer transfertype;
	
	/**
	 * 党员id
	 */
	@ApiModelProperty(value="党员id",name="userid",required=true)
	private String userid;
	
	/**
	 * 党员姓名
	 */
	@ApiModelProperty(value="党员姓名",name="username",required=true)
	private String username;
	
	/**
	 * 身份证号码
	 */
	@ApiModelProperty(value="身份证号码",name="idcode")
	private String idcode;
	
	/**
	 * 迁出党组织code
	 */
	@ApiModelProperty(value="迁出党组织code",name="outorgcode")
	private String outorgcode;
	
	/**
	 * 迁出党组织名称
	 */
	@ApiModelProperty(value="迁出党组织名称",name="outorgname")
	private String outorgname;
	
	/**
	 * 迁入党组织code
	 */
	@ApiModelProperty(value="迁入党组织code",name="infoorgcode",required=true)
	private String infoorgcode;
	
	/**
	 * 迁入党组织名称
	 */
	@ApiModelProperty(value="迁入党组织名称",name="infoorgname",required=true)
	private String infoorgname;
	
	/**
	 * 0：驳回，1迁出待审核，2：迁入待审核，3：迁入待确认,9：通过
	 */
	@ApiModelProperty(value="0：驳回，1迁出审核中，2：迁入待审核，3：迁入待确认,9：通过",name="status",required=true)
	private Integer status;
	
	/**
	 * 1：有效：-99:无效
	 */
	@ApiModelProperty(value="1：有效：-99:无效",name="status2")
	private Integer status2;
	
	/**
	 * 迁出时间
	 */
	@ApiModelProperty(value="迁出时间",name="outtime")
	private String outtime;
	
	/**
	 * 驳回备注
	 */
	@ApiModelProperty(value="驳回备注",name="backremark")
	private String backremark;
	
	/**
	 * 迁出审核时间
	 */
	@ApiModelProperty(value="迁出审核时间",name="outexaminetime")
	private String outexaminetime;
	
	/**
	 * 迁出审核备注
	 */
	@ApiModelProperty(value="迁出审核备注",name="ourexamineremark")
	private String ourexamineremark;
	
	/**
	 * 迁入审核时间
	 */
	@ApiModelProperty(value="迁入审核时间",name="infoexaminetime")
	private String infoexaminetime;
	
	/**
	 * 迁入审核备注
	 */
	@ApiModelProperty(value="迁入审核备注",name="infoexamineremark")
	private String infoexamineremark;
	
	/**
	 * 迁入确认时间
	 */
	@ApiModelProperty(value="迁入确认时间",name="infooktime")
	private String infooktime;
	
	/**
	 * 迁入确认备注
	 */
	@ApiModelProperty(value="迁入确认备注",name="infookexamineremark")
	private String infookexamineremark;
	
	/**
	 * 迁出备注
	 */
	@ApiModelProperty(value="迁出备注",name="outremark")
	private String outremark;
	
	/**
	 * 备用字段
	 */
	@ApiModelProperty(value="备用字段",name="remark1")
	private Integer remark1;
	
	/**
	 * 备用字段2
	 */
	@ApiModelProperty(value="备用字段2",name="remark2")
	private Integer remark2;
	
	/**
	 * 备用字段3
	 */
	@ApiModelProperty(value="备用字段3",name="remark3")
	private String remark3;
	
	/**
	 * 备用字段4
	 */
	@ApiModelProperty(value="备用字段4",name="remark4")
	private String remark4;

	@ApiModelProperty(value="年份",name="years")
	private String years;

	@ApiModelProperty(value="序号",name="number")
	private Integer number;

	@ApiModelProperty(value="迁出所在支部书记userid",name="outsecretary")
	private String outsecretary;

	@ApiModelProperty(value="迁入所在支部书记userid",name="infosecretary")
	private String infosecretary;

	/**
	 * 迁出党组织党委code
	 */
	@ApiModelProperty(value="迁出党组织党委code",name="outdworgcode")
	private String outdworgcode;

	/**
	 * 迁出党委审核时间
	 */
	@ApiModelProperty(value="迁出党委审核时间",name="outdwexaminetime")
	private String outdwexaminetime;

	/**
	 * 迁出党委审核意见
	 */
	@ApiModelProperty(value="迁出党委审核意见",name="remark5")
	private String remark5;
    /**
     * 一级集团党委审核（0：否。1：是）
     */
    @ApiModelProperty(value="集团党委审核（0：否。1：是）",name="jtdworgcode")
    private Integer jtdworgcode;

    /**
     * 一级集团党委审核时间
     */
    @ApiModelProperty(value="集团党委审核时间",name="jtdwexaminetime")
    private String jtdwexaminetime;

    /**
     * 一级集团党委审核意见
     */
    @ApiModelProperty(value="一级集团党委审核意见",name="jtdwremark")
    private String jtdwremark;

    public Integer getJtdworgcode() {
        return jtdworgcode;
    }

    public void setJtdworgcode(Integer jtdworgcode) {
        this.jtdworgcode = jtdworgcode;
    }

    public String getJtdwexaminetime() {
        return jtdwexaminetime;
    }

    public void setJtdwexaminetime(String jtdwexaminetime) {
        this.jtdwexaminetime = jtdwexaminetime;
    }

    public String getJtdwremark() {
        return jtdwremark;
    }

    public void setJtdwremark(String jtdwremark) {
        this.jtdwremark = jtdwremark;
    }

    public String getOutdwexaminetime() {
		return outdwexaminetime;
	}

	public void setOutdwexaminetime(String outdwexaminetime) {
		this.outdwexaminetime = outdwexaminetime;
	}

	public String getRemark5() {
		return remark5;
	}

	public void setRemark5(String remark5) {
		this.remark5 = remark5;
	}

	public String getOutdworgcode() {
		return outdworgcode;
	}

	public void setOutdworgcode(String outdworgcode) {
		this.outdworgcode = outdworgcode;
	}

	public String getOutsecretary() {
		return outsecretary;
	}

	public void setOutsecretary(String outsecretary) {
		this.outsecretary = outsecretary;
	}

	public String getInfosecretary() {
		return infosecretary;
	}

	public void setInfosecretary(String infosecretary) {
		this.infosecretary = infosecretary;
	}

	/**
     * setter for id
     * @param id
     */
	public void setId(Integer id) {
		this.id = id;
	}

    /**
     * getter for id
     */
	public Integer getId() {
		return id;
	}

    /**
     * setter for compid
     * @param compid
     */
	public void setCompid(Long compid) {
		this.compid = compid;
	}

    /**
     * getter for compid
     */
	public Long getCompid() {
		return compid;
	}

    /**
     * setter for transfertype
     * @param transfertype
     */
	public void setTransfertype(Integer transfertype) {
		this.transfertype = transfertype;
	}

    /**
     * getter for transfertype
     */
	public Integer getTransfertype() {
		return transfertype;
	}

    /**
     * setter for userid
     * @param userid
     */
	public void setUserid(String userid) {
		this.userid = userid;
	}

    /**
     * getter for userid
     */
	public String getUserid() {
		return userid;
	}

    /**
     * setter for username
     * @param username
     */
	public void setUsername(String username) {
		this.username = username;
	}

    /**
     * getter for username
     */
	public String getUsername() {
		return username;
	}

    /**
     * setter for idcode
     * @param idcode
     */
	public void setIdcode(String idcode) {
		this.idcode = idcode;
	}

    /**
     * getter for idcode
     */
	public String getIdcode() {
		return idcode;
	}

    /**
     * setter for outorgcode
     * @param outorgcode
     */
	public void setOutorgcode(String outorgcode) {
		this.outorgcode = outorgcode;
	}

    /**
     * getter for outorgcode
     */
	public String getOutorgcode() {
		return outorgcode;
	}

    /**
     * setter for outorgname
     * @param outorgname
     */
	public void setOutorgname(String outorgname) {
		this.outorgname = outorgname;
	}

    /**
     * getter for outorgname
     */
	public String getOutorgname() {
		return outorgname;
	}

    /**
     * setter for infoorgcode
     * @param infoorgcode
     */
	public void setInfoorgcode(String infoorgcode) {
		this.infoorgcode = infoorgcode;
	}

    /**
     * getter for infoorgcode
     */
	public String getInfoorgcode() {
		return infoorgcode;
	}

    /**
     * setter for infoorgname
     * @param infoorgname
     */
	public void setInfoorgname(String infoorgname) {
		this.infoorgname = infoorgname;
	}

    /**
     * getter for infoorgname
     */
	public String getInfoorgname() {
		return infoorgname;
	}

    /**
     * setter for status
     * @param status
     */
	public void setStatus(Integer status) {
		this.status = status;
	}

    /**
     * getter for status
     */
	public Integer getStatus() {
		return status;
	}

    /**
     * setter for status2
     * @param status2
     */
	public void setStatus2(Integer status2) {
		this.status2 = status2;
	}

    /**
     * getter for status2
     */
	public Integer getStatus2() {
		return status2;
	}

    /**
     * setter for outtime
     * @param outtime
     */
	public void setOuttime(String outtime) {
		this.outtime = outtime;
	}

    /**
     * getter for outtime
     */
	public String getOuttime() {
		return outtime;
	}

    /**
     * setter for backremark
     * @param backremark
     */
	public void setBackremark(String backremark) {
		this.backremark = backremark;
	}

    /**
     * getter for backremark
     */
	public String getBackremark() {
		return backremark;
	}

    /**
     * setter for outexaminetime
     * @param outexaminetime
     */
	public void setOutexaminetime(String outexaminetime) {
		this.outexaminetime = outexaminetime;
	}

    /**
     * getter for outexaminetime
     */
	public String getOutexaminetime() {
		return outexaminetime;
	}

    /**
     * setter for ourexamineremark
     * @param ourexamineremark
     */
	public void setOurexamineremark(String ourexamineremark) {
		this.ourexamineremark = ourexamineremark;
	}

    /**
     * getter for ourexamineremark
     */
	public String getOurexamineremark() {
		return ourexamineremark;
	}

    /**
     * setter for infoexaminetime
     * @param infoexaminetime
     */
	public void setInfoexaminetime(String infoexaminetime) {
		this.infoexaminetime = infoexaminetime;
	}

    /**
     * getter for infoexaminetime
     */
	public String getInfoexaminetime() {
		return infoexaminetime;
	}

    /**
     * setter for infoexamineremark
     * @param infoexamineremark
     */
	public void setInfoexamineremark(String infoexamineremark) {
		this.infoexamineremark = infoexamineremark;
	}

    /**
     * getter for infoexamineremark
     */
	public String getInfoexamineremark() {
		return infoexamineremark;
	}

    /**
     * setter for infooktime
     * @param infooktime
     */
	public void setInfooktime(String infooktime) {
		this.infooktime = infooktime;
	}

    /**
     * getter for infooktime
     */
	public String getInfooktime() {
		return infooktime;
	}

    /**
     * setter for infookexamineremark
     * @param infookexamineremark
     */
	public void setInfookexamineremark(String infookexamineremark) {
		this.infookexamineremark = infookexamineremark;
	}

    /**
     * getter for infookexamineremark
     */
	public String getInfookexamineremark() {
		return infookexamineremark;
	}

    /**
     * setter for outremark
     * @param outremark
     */
	public void setOutremark(String outremark) {
		this.outremark = outremark;
	}

    /**
     * getter for outremark
     */
	public String getOutremark() {
		return outremark;
	}

    /**
     * setter for remark1
     * @param remark1
     */
	public void setRemark1(Integer remark1) {
		this.remark1 = remark1;
	}

    /**
     * getter for remark1
     */
	public Integer getRemark1() {
		return remark1;
	}

    /**
     * setter for remark2
     * @param remark2
     */
	public void setRemark2(Integer remark2) {
		this.remark2 = remark2;
	}

    /**
     * getter for remark2
     */
	public Integer getRemark2() {
		return remark2;
	}

    /**
     * setter for remark3
     * @param remark3
     */
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

    /**
     * getter for remark3
     */
	public String getRemark3() {
		return remark3;
	}

    /**
     * setter for remark4
     * @param remark4
     */
	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}

    /**
     * getter for remark4
     */
	public String getRemark4() {
		return remark4;
	}


	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
     * PartyTransferEntity.toString()
     */
    @Override
    public String toString() {
        return "PartyTransferEntity{" +
               "id='" + id + '\'' +
               ", compid='" + compid + '\'' +
               ", transfertype='" + transfertype + '\'' +
               ", userid='" + userid + '\'' +
               ", username='" + username + '\'' +
               ", idcode='" + idcode + '\'' +
               ", outorgcode='" + outorgcode + '\'' +
               ", outorgname='" + outorgname + '\'' +
               ", infoorgcode='" + infoorgcode + '\'' +
               ", infoorgname='" + infoorgname + '\'' +
               ", status='" + status + '\'' +
               ", status2='" + status2 + '\'' +
               ", outtime='" + outtime + '\'' +
               ", backremark='" + backremark + '\'' +
               ", outexaminetime='" + outexaminetime + '\'' +
               ", ourexamineremark='" + ourexamineremark + '\'' +
               ", infoexaminetime='" + infoexaminetime + '\'' +
               ", infoexamineremark='" + infoexamineremark + '\'' +
               ", infooktime='" + infooktime + '\'' +
               ", infookexamineremark='" + infookexamineremark + '\'' +
               ", outremark='" + outremark + '\'' +
               ", remark1='" + remark1 + '\'' +
               ", remark2='" + remark2 + '\'' +
               ", remark3='" + remark3 + '\'' +
               ", remark4='" + remark4 + '\'' +
               '}';
    }

}
