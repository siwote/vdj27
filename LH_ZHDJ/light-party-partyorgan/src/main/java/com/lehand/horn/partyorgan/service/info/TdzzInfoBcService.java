package com.lehand.horn.partyorgan.service.info;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoBc;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * 组织机构补充表业务层
 * @author taogang
 * @version 1.0
 * @date 2019年1月21日, 下午8:32:38
 */
@Service
public class TdzzInfoBcService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	/**
	 * 根据
	 * @param organid
	 * @return
	 * @author taogang
	 */
	public String getDzzsj(String organid) {
		String dzzsj = "";
		TDzzInfoBc infobc = generalSqlComponent.query(SqlCode.getDzzsjById, new Object[] {organid});
		if(null != infobc) {
			dzzsj = infobc.getDzzsj();
		}
		return dzzsj;
	}
	
	
	/**
	 * 根据session获取组织机构id
	 * @param session
	 * @return
	 */
//	public String getOrgId(Session session) {
//		String orgId = "";
//		List<OrgDto> orgList = session.getOrgs();
//		if(orgList.size() > 0 ) {
//			orgId = "0" + orgList.get(0).getOrgid().toString();
//		}
//		TDzzInfoSimple org = generalSqlComponent.query(SqlCode.getDzzsjByCode, new Object[] {orgId});
//		if(org != null) {
//			orgId = org.getOrganid();
//		}
//		return orgId;
//	}
	
	/**
	 * 
	 * @Title：获取党组织简称
	 * @Description：TODO
	 * @param ：@param orgId
	 * @param ：@return 
	 * @return ：String 
	 * @throws
	 */
	public String getDzzjc(String orgcode,Long compid) {
		String dzzjc = null;
		Map<String, Object> dzz = generalSqlComponent.query(SqlCode.queryFindJCByCode, new Object[]{compid,orgcode});
		if(null != dzz) {
			dzzjc = StringUtils.isEmpty(dzz.get("remarks6")) ? dzz.get("orgname").toString() : dzz.get("remarks6").toString() ;
		}
		return dzzjc;
	}

}
