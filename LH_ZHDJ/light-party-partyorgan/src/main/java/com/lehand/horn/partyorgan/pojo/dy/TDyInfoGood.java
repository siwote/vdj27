package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 优秀共产党员与优秀党务工作者表
 * @author lx
 */
@ApiModel(value="优秀共产党员与优秀党务工作者表",description="优秀共产党员与优秀党务工作者表")
public class TDyInfoGood implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value="id", name="id")
  private long id;
  @ApiModelProperty(value="党员userid", name="userid")
  private String userid;
  @ApiModelProperty(value="党组织编码", name="orgcode")
  private String orgcode;
  @ApiModelProperty(value="党组织名称", name="organname")
  private String organname;
  @ApiModelProperty(value="姓名", name="xm")
  private String xm;
  @ApiModelProperty(value="年度", name="year")
  private String year;
  @ApiModelProperty(value="优秀类型（0:优秀共产党员,1:优秀党务工作者,2:党代表）", name="type")
  private String type;
  @ApiModelProperty(value="创建人", name="creator")
  private String creator;
  @ApiModelProperty(value="创建时间", name="createtime")
  private String createtime;
  @ApiModelProperty(value="更新人", name="updator")
  private String updator;
  @ApiModelProperty(value="更新时间", name="updatetime")
  private String updatetime;
  @ApiModelProperty(value="备注", name="remark")
  private String remark;
  @ApiModelProperty(value="附件id，以“，”拼接", name="fileids")
  private String fileids;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getOrgcode() {
    return orgcode;
  }

  public void setOrgcode(String orgcode) {
    this.orgcode = orgcode;
  }

  public String getOrganname() {
    return organname;
  }

  public void setOrganname(String organname) {
    this.organname = organname;
  }


  public String getXm() {
    return xm;
  }

  public void setXm(String xm) {
    this.xm = xm;
  }


  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }


  public String getCreatetime() {
    return createtime;
  }

  public void setCreatetime(String createtime) {
    this.createtime = createtime;
  }


  public String getUpdator() {
    return updator;
  }

  public void setUpdator(String updator) {
    this.updator = updator;
  }


  public String getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(String updatetime) {
    this.updatetime = updatetime;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public String getFileids() {
    return fileids;
  }

  public void setFileids(String fileids) {
    this.fileids = fileids;
  }

}
