package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.TDyInfoChildModule;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 党组织人员信息子模块表扩展类
 * @author lx
 */
@ApiModel(value="党组织人员信息子模块表扩展类",description="党组织人员信息子模块表扩展类")
public class TDyInfoChildModuleReq extends TDyInfoChildModule {

    @ApiModelProperty(value="导出列名数据",name="exportColumns")
    private List<ExportColumnReq> exportColumns;

    @ApiModelProperty(value="开始时间",name="startTime")
    private String startTime;

    @ApiModelProperty(value="结束时间",name="endTime")
    private String endTime;

    public List<ExportColumnReq> getExportColumns() {
        return exportColumns;
    }

    public void setExportColumns(List<ExportColumnReq> exportColumns) {
        this.exportColumns = exportColumns;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
