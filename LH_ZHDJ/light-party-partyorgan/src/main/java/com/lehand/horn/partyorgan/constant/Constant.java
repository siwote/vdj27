package com.lehand.horn.partyorgan.constant;

public class Constant {



	/**
	 * Excel导入模板名称
	 */
	public static class ExcelTemplate {
		/**
		 * 党组织导入模板名称
		 */
		public static final String DZZ_EXCEL_IMPORT = "党组织导入模板.xlsx";

		/**
		 * 党员导入模板
		 */
		public static final String DY_EXCEL_IMPORT =  "党员导入模板.xlsx";


		/**
		 * 党费收交导入模板
		 */
		public static final String DF_EXCEL_IMPORT =  "党费收交导入模板.xlsx";
	}

	/**
	 * 微信状态
	 */
	public static class WXSTATUS {
		/**
		 * 消息类型
		 */
		public static final String EVENT = "event";
		/**
		 * 用户已关注
		 */
		public static final String EVENT_SCAN = "SCAN";
		/**
		 * 用户关注
		 */
		public static final String EVENT_SUBSCRIBE = "subscribe";
	}


	/**
	 * 换届短信提醒
	 */
	public static final String CHANGE_TERM_REMIND = "5";

	//hj
//    public static final String APP_ID = "wx0e0a6134108bf8df";
//    public static final String APP_SECRET = "de28f06cf07f263b11dc47cb05157ca6";
	//lx
	public static final String APP_ID = "wx6e95e9238dce7688";
	public static final String APP_SECRET = "30b0615ad1e3336b00d9b2ab063b3399";
	//正式公众号
//	public static final String APP_ID = "wxc0b258c1a83c8a22";
//	public static final String APP_SECRET = "a0f4bf9eaccd973937cb6d7ef76214d8";
	/**
	 * 短信验证码
	 */
	public static final String SMS_CODE = "8";
	/**
	 * 常量
	 */
	public static final Integer INT_ONE = 1;
}
