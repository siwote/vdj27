package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "党员信息查询传参",description = "党员信息查询传参")
public class MemberSearchReq extends MemberBasicDto {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="年龄起始查询条件",name="startage")
    private Integer startage;

    @ApiModelProperty(value="年龄结束询条件",name="endage")
    private Integer endage;

    @ApiModelProperty(value="党员类别",name="dylb")
    private String dylb;

    @ApiModelProperty(value="查询方式",name="type")
    private String type;

    @ApiModelProperty(hidden = true)
    private String path;

    @ApiModelProperty(hidden = true)
    private String parentid;

    public Integer getStartage() {
        return startage;
    }

    public void setStartage(Integer startage) {
        this.startage = startage;
    }

    public Integer getEndage() {
        return endage;
    }

    public void setEndage(Integer endage) {
        this.endage = endage;
    }

    public String getDylb() {
        return dylb;
    }

    public void setDylb(String dylb) {
        this.dylb = dylb;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }
}
