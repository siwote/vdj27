package com.lehand.horn.partyorgan.service.dzz;


import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.constant.SysConstants.DYLB;
import com.lehand.horn.partyorgan.constant.SysConstants.DYZT;
import com.lehand.horn.partyorgan.constant.SysConstants.RZZT;
import com.lehand.horn.partyorgan.dto.SessionDto;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
import com.lehand.horn.partyorgan.util.SessionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.*;


/**
 * 党员/党组织基本信息统计
 * @author taogang
 * @version 1.0
 * @date 2019年2月21日, 上午11:52:45
 */
@Service
public class BaseDzzTotalService{

	@Resource
    GeneralSqlComponent generalSqlComponent;
	@Resource
	SessionUtils partyUtils;

	@Resource
	private PartyCacheComponent partyCacheComponent;

	public static final String TDYDNZW = "d_dy_dnzw";
	//保留两位小数
	private DecimalFormat df = new DecimalFormat("0.00");
	
	/**
	 * 获取组织机构Path
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public String getPath(String orgId) {
		 String path = "";
		 if(!StringUtils.isEmpty(orgId)) {

			TDzzInfoSimple org =generalSqlComponent.query(SqlCode.get_path_num,new Object[]{orgId});
			if(null != org) {
				String[] oarr = org.getIdPath().split("/");
			    path = "path"+(oarr.length-1); 
			}
		 }
		 return path;
	}
	
	/**
	 * 获取
	 * @param organid
	 * @return
	 * @author taogang
	 */
	public List<Map<String, Object>> dzzCount(String organid){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Long count = 0L;  		//党组织总数
		Long dwCount = 0L;  	//党委总数
		Long dzzCount = 0L; 	//党总支总数
		Long dzbCount = 0L; 	//党支部总数
		Long lhdzbCount = 0L;   //联合党支部总数
		Long dxzCount = 0L;     //党小组总数
		Long bzcyCount = 0L;    //班子成员数
		Long activityCount = 0L;    //活动阵地
		//党委,党总支，党支部，联合党支部
		String path = getPath(organid);
		dwCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
				new HashMap(){{put("zzlb",SysConstants.ZZLB.DW);put("path",path);put("organid",organid);}});
        dzzCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb",SysConstants.ZZLB.DZZ);put("path",path);put("organid",organid);}});
        dzbCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb",SysConstants.ZZLB.DZB);put("path",path);put("organid",organid);}});
        lhdzbCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb",SysConstants.ZZLB.LHDZB);put("path",path);put("organid",organid);}});

		activityCount = generalSqlComponent.query(SqlCode.getDzzCountByActivity,
				new HashMap(){{put("path",path);put("organid",organid);}});
		//党小组
//		dxzCount = generalSqlComponent.query(SqlCode.getDxzCountByOrganid, new HashMap(){{put("path",path);put("organid",organid);}});
		dxzCount = generalSqlComponent.query(SqlCode.getDxzCountByOrganidnew, new HashMap(){{put("path",path);put("organid",organid);}});
		//班子成员
		bzcyCount = generalSqlComponent.query(SqlCode.getBzcyCountByOrganid, new Object[] {organid,RZZT.ZR});
		//党组织总数
//		System.out.println(organid+"************************"+path);
//		System.out.println("************************"+dwCount);
//		System.out.println("************************"+dzzCount);
//		System.out.println("************************"+dzbCount);
//		System.out.println("************************"+lhdzbCount);
		count = dwCount + dzzCount + dzbCount + lhdzbCount;
		//list赋值
		list.add(getMap("党组织", count));
		list.add(getMap("党委数", dwCount));
		list.add(getMap("党总支数", dzzCount));
		list.add(getMap("班子成员数", bzcyCount));
//		list.add(getMap("党支部数", dzbCount));
//		list.add(getMap("联合党支部数", lhdzbCount));
		list.add(getMap("党支部数", dzbCount+lhdzbCount));
		list.add(getMap("党小组数", dxzCount));
		list.add(getMap("活动阵地", activityCount));
		return list;
	}
	
	/**
	 * 获取map
	 * @param name
	 * @param value
	 * @return
	 * @author taogang
	 */
	public Map<String, Object> getMap(String name,Long value){
		Map<String, Object> bean = new HashMap<>();
		bean.put("name",name);
		bean.put("value", value);
		return bean;
	}
	
	/**
	 * 党员信息
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public List<Map<String, Object>> dyCount(String orgId,Session session){
		List<Map<String, Object>> list = new ArrayList<>();
		//获取sql
		Long dyCount = 0L;
        if(StringUtils.isEmpty(orgId)){
            orgId = generalSqlComponent.getDbComponent().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{session.getCurrentOrg().getOrgcode()});
        }
        if(StringUtils.isEmpty(orgId)) {
            LehandException.throwException("参数错误，组织机构id不能为空");
        }
		String path = getPath(orgId);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("dyzt", DYZT.ZC);
		paramMap.put("dylb", SysConstants.DYLB.ZSDY);
		paramMap.put("path", path);
        paramMap.put("organid", orgId);
		Map<String, Object> zsdy = generalSqlComponent.query(SqlCode.getDyCountByDYLB, paramMap);
		if(null != zsdy) {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "正式党员数");
			bean.put("value", zsdy.get("cou"));
			dyCount += Long.valueOf((long) zsdy.get("cou"));
			list.add(bean);
		}else {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "正式党员数");
			bean.put("value", 0);
			list.add(bean);
		}
		
		//预备党员
        paramMap.put("dylb", SysConstants.DYLB.YBDY);
		Map<String, Object> ybdy = generalSqlComponent.query(SqlCode.getDyCountByDYLB, paramMap);
		if(null != ybdy) {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "预备党员数");
			bean.put("value", ybdy.get("cou"));
			dyCount += Long.valueOf((long) ybdy.get("cou"));
			list.add(bean);
		}else {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "预备党员数");
			bean.put("value", 0);
			list.add(bean);
		}
		
		//发展对象
        paramMap.put("dylb", SysConstants.DYLB.FZDX);
		Map<String, Object> fzdx = generalSqlComponent.query(SqlCode.getDyCountByDYLB, paramMap);
		if(null != fzdx) {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "发展对象数");
			bean.put("value", fzdx.get("cou"));
			list.add(bean);
		}else {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "发展对象数");
			bean.put("value", 0);
			list.add(bean);
		}
		
		//积极份子
        paramMap.put("dylb", SysConstants.DYLB.JJFZ);
		Map<String, Object> jjfz = generalSqlComponent.query(SqlCode.getDyCountByDYLB, paramMap);
		if(null != jjfz) {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "积极分子数");
			bean.put("value", jjfz.get("cou"));
			list.add(bean);
		}else {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "积极分子数");
			bean.put("value", 0);
			list.add(bean);
		}
		
		//入党申请人
        paramMap.put("dylb", SysConstants.DYLB.SQR);
		Map<String, Object> rdsq = generalSqlComponent.query(SqlCode.getDyCountByDYLB, paramMap);
		if(null != rdsq) {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "入党申请数");
			bean.put("value", rdsq.get("cou"));
			list.add(bean);
		}else {
			Map<String, Object> bean = new HashMap<>(2);
			bean.put("name", "入党申请数");
			bean.put("value", 0);
			list.add(bean);
		}
		list.add(0,getMap("党员数", dyCount));
		return list;
	}
	
	
	/**
	 * 学历信息
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public List<Map<String, Object>> xlCount(String orgId){
		List<Map<String, Object>> list = new ArrayList<>();
		
		String path = getPath(orgId);
		//执行sql
//		List<Map<String, Object>> xlList = baseDzzDao.getCountList2MapXl(sql, orgId);
        List<Map<String, Object>> xlList = generalSqlComponent.query(SqlCode.getXlxxCount,
                new HashMap(){{put("dyzt",DYZT.ZC);put("path",path);put("organid",orgId);put("code","d_dy_xl");}});
		if(xlList.size() > 0) {
			Map<String, Object> bean = null;
			for (Map<String, Object> map : xlList) {
				bean = new HashMap<>();
				bean.put("name", map.get("item_name"));
				bean.put("value", map.get("cou"));
				list.add(bean);
			}
		}
		return list;
	}
	
	/**
	 * 年龄结构
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public List<Map<String, Object>> getAgeCount(String orgId){
		List<Map<String, Object>> list = new ArrayList<>();
		//定义sql
		String path = getPath(orgId);
//		list = baseDzzDao.list2map(sql, orgId,orgId,orgId,orgId,orgId);
		//小于30岁
		Map<String,Object> map1 =  generalSqlComponent.query(SqlCode.getAgeStructureMap,
                new HashMap(){{put("str","30岁以下");put("dyzt",DYZT.ZC);put("zsdy",DYLB.ZSDY);put("ybdy",DYLB.YBDY);
                    put("path",path);put("organid",orgId);put("startage",0);put("endage",30);}});
		list.add(map1);
		//30岁到35岁
		Map<String,Object> map2 =  generalSqlComponent.query(SqlCode.getAgeStructureMap,
                new HashMap(){{put("str","30岁到35岁");put("dyzt",DYZT.ZC);put("zsdy",DYLB.ZSDY);put("ybdy",DYLB.YBDY);
                    put("path",path);put("organid",orgId);put("startage",30);put("endage",35);}});
		list.add(map2);
		//35岁到40岁
		Map<String,Object> map3 =  generalSqlComponent.query(SqlCode.getAgeStructureMap,
                new HashMap(){{put("str","35岁到40岁");put("dyzt",DYZT.ZC);put("zsdy",DYLB.ZSDY);put("ybdy",DYLB.YBDY);
                    put("path",path);put("organid",orgId);put("startage",35);put("endage",40);}});
		list.add(map3);
		//40岁到45岁

		Map<String,Object> map4 =  generalSqlComponent.query(SqlCode.getAgeStructureMap,
                new HashMap(){{put("str","40岁到45岁");put("dyzt",DYZT.ZC);put("zsdy",DYLB.ZSDY);put("ybdy",DYLB.YBDY);
                    put("path",path);put("organid",orgId);put("startage",40);put("endage",45);}});
		list.add(map4);
		//45岁到60岁

		Map<String,Object> map5 =  generalSqlComponent.query(SqlCode.getAgeStructureMap,
                new HashMap(){{put("str","45岁到60岁");put("dyzt",DYZT.ZC);put("zsdy",DYLB.ZSDY);put("ybdy",DYLB.YBDY);
                    put("path",path);put("organid",orgId);put("startage",45);put("endage",60);}});
		list.add(map5);
		//60岁以上

		Map<String,Object> map6 =  generalSqlComponent.query(SqlCode.getAgeStructureMap,
                new HashMap(){{put("str","60岁以上");put("dyzt",DYZT.ZC);put("zsdy",DYLB.ZSDY);put("ybdy",DYLB.YBDY);
                    put("path",path);put("organid",orgId);put("startage",60);put("endage",150);}});
		list.add(map6);
		return list;
	}
	
	/**
	 * 组织关系转接统计
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public Map<String, Object> getZzgxCount(String orgId){
		List<Map<String, Object>> list = new ArrayList<>();
		//获取当前年
		Calendar cale = Calendar.getInstance();
		int month = cale.get(Calendar.MONTH) + 1;
		String monValue = "";
//		String sql = "";
		//X月份轴
		List<String> xAxis = new ArrayList<>();
		//转入数量
		List<Long> zrData = new ArrayList<>();
		//转出数量
		List<Long> zcData = new ArrayList<>();
		for (int i = 1;i <= month ; i++ ) {
			if(i < 10) {
				monValue = '0' + String.valueOf(i);
			}else {
				monValue = String.valueOf(i);
			}
			xAxis.add(i + "月");
			String path = getPath(orgId);
            Map<String,Object> param = new HashMap<String,Object>(4);
            param.put("path",path);
            param.put("organid",orgId);
            param.put("entermonth",monValue);
            param.put("exitmonth",monValue);
            List<Map<String, Object>> zzgxList = generalSqlComponent.query(SqlCode.getDzzgxzjInfoList,param);
			if(zzgxList.size() == 1) {
				for (Map<String, Object> map : zzgxList) {
					//转入
					if(SysConstants.ZZGX.ZR == Integer.valueOf(map.get("status").toString())) {
						zrData.add(Long.valueOf(map.get("val").toString()));
					}else {
						zrData.add(0L);
					}
					
					//转出
					if(SysConstants.ZZGX.ZC == Integer.valueOf(map.get("status").toString())) {
						zcData.add(Long.valueOf(map.get("val").toString()));
					}else {
						zcData.add(0L);
					}
				}
			}else if(zzgxList.size() == 2) {
				for (Map<String, Object> map : zzgxList) {
					//转入
					if(SysConstants.ZZGX.ZR == Integer.valueOf(map.get("status").toString())) {
						zrData.add(Long.valueOf(map.get("val").toString()));
					}
					
					//转出
					if(SysConstants.ZZGX.ZC == Integer.valueOf(map.get("status").toString())) {
						zcData.add(Long.valueOf(map.get("val").toString()));
					}
				}
			}else {
				zrData.add(0L);
				zcData.add(0L);
			}
		}
		//转入
		Map<String, Object> bean = new HashMap<>();
		bean.put("name", "转入数量");
		bean.put("data", zrData);
		list.add(bean);
		//转出
		Map<String, Object> bean2 = new HashMap<>();
		bean2.put("name", "转出数量");
		bean2.put("data", zcData);
		list.add(bean2);
		
		Map<String, Object> row = new HashMap<>();
		row.put("xAxis", xAxis);
		row.put("data", list);
		return row;
	}
	
	/**
	 * 学习教育统计
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public Map<String, Object> getStudyCount(String orgId){
		Map<String, Object> bean = new HashMap<>();
		//获取当前年
		Calendar cale = Calendar.getInstance();
		int month = cale.get(Calendar.MONTH) + 1;
		int year = cale.get(Calendar.YEAR);
		//X月份轴
		List<String> xAxis = new ArrayList<>();
		//Y 数值
		List<Integer> yData = new ArrayList<>();
		String monValue = "";
		//月份遍历
		for (int i = 1; i <= month; i++) {
			if(i < 10) {
				monValue = '0' + String.valueOf(i);
			}else {
				monValue = String.valueOf(i);
			}
			xAxis.add(i + "月");		
			String path = getPath(orgId);
			// 统计每月党员学习次数
			Integer count = generalSqlComponent.query(SqlCode.getDyStudyCountByMonth,new Object[] {path,orgId,year + "-" + monValue});
			yData.add(count);
		}
		
		
		//累计学时
		String path = getPath(orgId);
		
		//今年，去年累计学时
		double thisYear = generalSqlComponent.query(SqlCode.getDyStudyHourByYear,new Object[] {path,orgId,year});
		double lastYear = generalSqlComponent.query(SqlCode.getDyStudyHourByYear,new Object[] {path,orgId,year-1});
		//同比去年
		double studyTime = 0.0;
		if(lastYear > 0) {
			studyTime = (thisYear - lastYear) / lastYear * 100;
		}else {
			if(thisYear > 0) {
				studyTime = 100;
			}
		}
		
		//今年，去年累计学习数
		double thisYearNum = generalSqlComponent.query(SqlCode.getDyStudyCountByYear,new Object[] {path,orgId,year});
		double lastYearNum = generalSqlComponent.query(SqlCode.getDyStudyCountByYear,new Object[] {path,orgId,year-1});
		//同比去年
		double studyCount = 0.0;
		if(lastYearNum > 0) {
			studyCount = (thisYearNum - lastYearNum) / lastYearNum * 100;
		}else {
			if(thisYearNum > 0) {
				studyCount = 100;
			}
		}
		
		//Map赋值
		bean.put("xAxis", xAxis);
		bean.put("data", yData);
		bean.put("studyTime", thisYear);
		bean.put("timeBl", df.format(studyTime));
		bean.put("studyCount", thisYearNum);
		bean.put("countBl", df.format(studyCount));
		return bean;
	}
	
	/**
	 * 党费统计
	 * @param orgId
	 * @return
	 * @author taogang
	 */
	public List<Map<String, Object>> getPartyDuesCount(String orgId){
		//定义数组
		List<Map<String, Object>> list = new ArrayList<>();
		
		//获取当前年
		Calendar cale = Calendar.getInstance();
		int year = cale.get(Calendar.YEAR);
		//当前月
		int month = cale.get(Calendar.MONTH) + 1;
		
		String path = getPath(orgId);
		
		//获取党费信息
        List<Map<String, Object>> costList = generalSqlComponent.query(SqlCode.getPartyDuesCount,
                new HashMap(){{put("path",path);put("orgId",orgId);put(year,year);put(month,month);}});
		if(costList.size() > 0 ) {
			Map<String, Object> bean = null;
			for (Map<String, Object> map : costList) {
				bean = new HashMap<>();
				if(0 == Integer.valueOf(map.get("status").toString())) {
					bean.put("name", "未缴党费人数");
					bean.put("value", map.get("value"));
				}
				if(1 == Integer.valueOf(map.get("status").toString())) {
					bean.put("name", "已缴党费人数");
					bean.put("value", map.get("value"));
				}
				list.add(bean);
			}
		}else {
			//未交党费
			Map<String, Object> bean = new HashMap<>();
			bean.put("name", "未缴党费");
			bean.put("value", 0);
			list.add(bean);
			
			//已交党费
			Map<String, Object> bean2 = new HashMap<>();
			bean2.put("name", "已缴党费");
			bean2.put("value", 0);
			list.add(bean2);
		}
		
		return list;
	}
	
	/**
	 * 获取统计学习
	 * @param organid
	 * @param type  1 统计每月党员学习次数， 2 统计累计学时  3 统计每年学习次数
	 * @return
	 * @author taogang
	 */
	public String getStudySql(String organid,Integer type) {
		StringBuffer sql = new StringBuffer();
		String path = getPath(organid);
		switch (type) {
			case 1:
				sql.append(" select count(1) as cou from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" inner join study_join_plan c on b.userid = c.user_id ");
				sql.append(" where 1 = 1 and a."+ path +" = ? ");
				sql.append(" and DATE_FORMAT(c.study_start_time,'%Y-%m') = ? ");
				break;
			case 2:
				sql.append(" select case when sum(c.actual_time) > 0 then sum(c.actual_time) else 0 end as cou from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" inner join study_join_plan c on b.userid = c.user_id ");
				sql.append(" where 1 = 1 and a."+ path +" = ? ");
				sql.append(" and DATE_FORMAT(c.study_start_time,'%Y') = ? ");
				break;
			case 3:
				sql.append(" select count(1) as cou from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" inner join study_join_plan c on b.userid = c.user_id ");
				sql.append(" where 1 = 1 and a."+ path +" = ? ");
				sql.append(" and DATE_FORMAT(c.study_start_time,'%Y') = ? ");
				break;
			default:
				break;
		}
		return sql.toString();
	}
	
	/**
	 * 获取组织关系转接SQL 
	 * @param organid
	 * @param month
	 * @return
	 * @author taogang
	 */
	public String getZZgxSql(String organid,String month) {
		//定义sql
		StringBuffer sql = new StringBuffer();
		String path = getPath(organid);
		sql.append(" select b.PASSSTATUS as status,count(b.UUID) as val from t_dzz_info_simple a ");
		sql.append(" inner join t_transfer_main b on (a.organid = b.FROMORGANID or a.organid = b.TOORGANID) ");
		sql.append(" where 1 =1 and a."+ path +" = ? and b.TRANSFERSTATUS = 2 ");
		sql.append(" and (DATE_FORMAT(b.ENTERDATE,'%m') = ? OR DATE_FORMAT(b.EXITDATE,'%m') = ?)");
		//--当前年，暂注销				sql.append(" and DATE_FORMAT(b.ENTERDATE,'%Y') = DATE_FORMAT(now(),'%Y') and DATE_FORMAT(b.EXITDATE,'%Y') = DATE_FORMAT(now(),'%Y') ");
		sql.append(" group by b.PASSSTATUS ");
		return sql.toString();
	}
	
	
	/**
	 * 获取sql
	 * @param organid
	 * @param type  0 党组织  1 党小组  2 党员  3 学历  4 年龄  
	 * @return
	 * @author taogang
	 */
	public String getSQL(String organid,Integer type) {
		//定义sql
		StringBuffer sql = new StringBuffer();
		String path = getPath(organid);
		switch (type) {
			case 0:
				sql.append("select count(1) from t_dzz_info_simple ");
				sql.append(" where 1 =1 and " + path + "=? and zzlb = ?");
				break;
			case 1:
				sql.append("select count(1) from t_dzz_info_simple ");
				sql.append(" where 1 =1 and " + path + "=? and organtype = 4");
				break;
			case 2:
				sql.append("select b.dylb,count(b.userid) as cou from t_dzz_info_simple a");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and a." + path + "=?  and dylb=?");
				sql.append(" group by b.dylb");
				break;
			case 3:
				sql.append(" select t.*,c.item_name from ( ");
				sql.append(" select b.xl,count(b.userid) as cou from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and a."+ path +" = ? ");
				sql.append(" group by b.xl ");
				sql.append(" ) t ");
				sql.append(" left join g_dict_item c on t.xl = c.item_code ");
				sql.append(" where c.code = 'd_dy_xl' ");
				break;
			case 4:
				sql.append(" select '30岁以下' as name,count(1) as value from ( ");
				sql.append(" select year(now()) - year(SUBSTRING(b.zjhm,7,8)) as age from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and b.dylb in('"+DYLB.ZSDY+"','"+DYLB.YBDY+"')and b.dyzt = '"+DYZT.ZC+"' and a."+ path +" = ? ");
				sql.append(" ) t ");
				sql.append(" where t.age < 30 ");
				sql.append(" union all ");
				sql.append(" select '30岁-35岁' as name,count(1) as value from ( ");
				sql.append(" select year(now()) - year(SUBSTRING(b.zjhm,7,8)) as age from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and b.dylb in ('"+DYLB.ZSDY+"','"+DYLB.YBDY+"') and b.dyzt = '"+DYZT.ZC+"' and a."+ path +" = ? ");
				sql.append(" ) t ");
				sql.append(" where t.age >= 30 and t.age <= 35 ");
				sql.append(" union all ");
				sql.append(" select '36岁-45岁' as name,count(1) as value from ( ");
				sql.append(" select year(now()) - year(SUBSTRING(b.zjhm,7,8)) as age from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and b.dylb in ('"+DYLB.ZSDY+"','"+DYLB.YBDY+"') and b.dyzt = '"+DYZT.ZC+"' and a."+ path +" = ? ");
				sql.append(" ) t ");
				sql.append(" where t.age >= 36 and t.age <= 45 ");
				sql.append(" union all ");
				sql.append(" select '46岁-59岁' as name,count(1) as value from ( ");
				sql.append(" select year(now()) - year(SUBSTRING(b.zjhm,7,8)) as age from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and b.dylb in ('"+DYLB.ZSDY+"','"+DYLB.YBDY+"') and b.dyzt = '"+DYZT.ZC+"' and a."+ path +" = ? ");
				sql.append(" ) t ");
				sql.append(" where t.age >= 46 and t.age <= 59 ");
				sql.append(" union all ");
				sql.append(" select '60岁以上' as name,count(1) as value from ( ");
				sql.append(" select year(now()) - year(SUBSTRING(b.zjhm,7,8)) as age from t_dzz_info_simple a ");
				sql.append(" inner join t_dy_info b on a.organid = b.organid ");
				sql.append(" where b.dyzt = '"+DYZT.ZC+"' and b.dylb in ('"+DYLB.ZSDY+"','"+DYLB.YBDY+"') and b.dyzt = '"+DYZT.ZC+"' and a."+ path +" = ? ");
				sql.append(" ) t ");
				sql.append(" where t.age >= 60 ");
				break;
			default:
				break;
		}
		return sql.toString();
	}

	/**
	 * 处理session
	 * @param session
	 * @return
	 */
	public SessionDto makeSessionDto(Session session){
		SessionDto dto = new SessionDto();
		BeanUtils.copyProperties(session,dto) ;
		//String organid = partyUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		String zzlb = partyUtils.getOrgTypeByOrganCode(session.getCurrentOrg().getOrgcode());
		if(!StringUtils.isEmpty(zzlb)){
			Long typeid = Long.valueOf(zzlb);
			dto.getCurrentOrg().setOrgtypeid(typeid);
		}
		Map<String,Object> map = generalSqlComponent.query(SqlCode.getUserIdcard,new Object[]{session.getUserid(),session.getCompid()});
		dto.setIdcard(String.valueOf(map.get("idno")));
		String orgid = dto.getCurrentOrg().getOrgid().toString();
        String organid = partyUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		dto.setCustom(organid);
		dto.setPost(String.valueOf(map.get("postname")));
        dto.setSecondOrgan(partyUtils.getSecondOrgan(session.getCurrorganid(),session.getCompid()));
        //覆盖老session重新赋值
		dto.setAccflag(Integer.valueOf(map.get("accflag").toString()));
		return dto;
	}

	/**
	 * 党支部下所有党员的基本信息
	 * @param orgId
	 * @return
	 * @author pantao
	 */
    public List<Map<String, Object>> partyBaseInfos(String orgId,String type) {
		Map<String, Object> param = new HashMap<>();
		param.put("orgId",orgId);
		param.put("type",type);
		List<Map<String, Object>> lists= generalSqlComponent.query(SqlCode.listTDyInfo,param);
		lists.forEach(a->{
			if(StringUtils.isEmpty(a.get("postname"))){
				a.put("postname","普通党员");
			}else{
				a.put("postname",partyCacheComponent.getName(TDYDNZW,a.get("postname")));
			}
		});
		return lists;
    }

	/**
	 * 党员的基本信息
	 * @param idNumber
	 * @return
	 * @author pantao
	 */
	public Map<String, Object> partyBaseInfo(String idNumber,Session session) {

		SessionDto dto = makeSessionDto(session);
		Integer accflag = dto.getAccflag();

		if(accflag==0){
			String zjhm = dto.getIdcard();
			return getBaseInfoByMember(zjhm);
		}

		return getBaseInfoByParty(session);
	}

	/**
	 * 组织账号登录
	 * @param session
	 * @return
	 */
	private Map<String, Object> getBaseInfoByParty(Session session) {
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("xm",session.getUsername());
		map.put("dzzmc",session.getCurrentOrg().getOrgname());
		map.put("postname","组织账号");
		return map;
	}

	/**
	 * 人员账号登录
	 * @param idNumber
	 * @return
	 */
	private Map<String, Object> getBaseInfoByMember(String idNumber) {
		Map<String, Object> map = generalSqlComponent.query(SqlCode.getTDyInfo,new Object[]{idNumber});
		if(map==null){
			LehandException.throwException("党员不存在！");
		}
		if(StringUtils.isEmpty(map.get("postname"))){
			map.put("postname","普通党员");
		}else{
			map.put("postname",partyCacheComponent.getName(TDYDNZW,map.get("postname")));
		}
		return map;
	}


	/**
	 * 党党员的签名
	 * @param idNumber
	 * @param userId
	 * @param fileId
	 * @return
	 * @author pantao
	 */
	@Transactional(rollbackFor = Exception.class)
	public void partySign(Session session, String idNumber, String userId, String fileId) {
//		//保存党员和附件的关联关系 businessid 为党员的userid; businessname为20; intostatus为6;
//		Map<String, Object> param = getStringObjectMap(userId, fileId);
//		//先查询下党员附件关联表中是否存在，不存在就新增，反之修改
//		Map<String, Object> dfb = generalSqlComponent.query(SqlCode.getDlFileBusiness,new Object[]{session.getCompid(),userId,20,6});
//		if(dfb==null){
//			generalSqlComponent.insert(SqlCode.addDlFileBusiness,param);
//		}else{
//			generalSqlComponent.update(SqlCode.modiDlFileBusiness,param);
//		}
//		//更新urc_user表中的jsondata字段为{memberid:"xxx",userid:xxx,fileid:"xxx"}
//		//先查询党员的ID
//		Map<String, Object> party = generalSqlComponent.query(SqlCode.getTDyInfo,new Object[]{idNumber});
//		if(party==null){
//			LehandException.throwException("党员不存在！");
//		}
//		//拼接json数据
//		String json = getString(userId, fileId, party);
		//更新urc_user
        if("null".equals(fileId)){
            fileId="";
        }
		generalSqlComponent.update(SqlCode.modiUrcUser,new Object[]{fileId,session.getCompid(),userId});
	}
	/**
	 * 党员的基本信息
	 * @param userId
	 * @return
	 * @author pantao
	 */
	public Map<String, Object> getPartySign(Session session,String userId) {
		return generalSqlComponent.query(SqlCode.getPwUserByID2,new Object[]{session.getCompid(),userId});
	}
	/**
	 * 获取当前组织下所有组织编码
	 * @param organcode
	 * @return
	 */
	private void getOrgCode(String organcode,List<String> listOrganId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("organcode", organcode);
		List<Map<String,Object>> list  = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getTDzzInfoSimpleTree2, paramMap);
		for(Map<String,Object> li:list){
			listOrganId.add("'"+li.get("organid").toString()+"'");
			getOrgCode(li.get("organcode").toString(),listOrganId);
		}
	}
	public List<Map<String, Object>> partyAgeInfo(String orgcode, String organid) {
		List<Map<String, Object>> result = new ArrayList<>();
		List<String> listOrganId = new ArrayList<>();
		//获取当前组织编码及其下所有组织编码
		listOrganId.add("'"+organid+"'");
		getOrgCode(orgcode,listOrganId);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("organid", org.apache.commons.lang.StringUtils.strip(listOrganId.toString(),"[]"));

		List<Map<String, Object>> partyAgeList = new ArrayList<>();
		Map<String, Object> m1 = new HashMap<>();
		m1.put("str","小于等于5年");
		m1.put("start",-1);
		m1.put("end",10);
		Map<String, Object> m2 = new HashMap<>();
		m2.put("str","大于10年小于等于20年");
		m2.put("start",10);
		m2.put("end",20);
		Map<String, Object> m3 = new HashMap<>();
		m3.put("str","大于20年小于等于30年");
		m3.put("start",20);
		m3.put("end",30);
		Map<String, Object> m4 = new HashMap<>();
		m4.put("str","大于30年小于等于40年");
		m4.put("start",30);
		m4.put("end",40);
		Map<String, Object> m5 = new HashMap<>();
		m5.put("str","大于40年");
		m5.put("start",40);
		m5.put("end",100);
		partyAgeList.add(m1);
		partyAgeList.add(m2);
		partyAgeList.add(m3);
		partyAgeList.add(m4);
		partyAgeList.add(m5);

		for (Map<String, Object> map: partyAgeList) {
			paramMap.putAll(map);
			Map<String, Object> data = generalSqlComponent.query(SqlCode.partyAgeInfo, paramMap);
			result.add(data);
		}

		return result;
	}

	/**
	 * 参数拼接
	 * @param userId
	 * @param fileId
	 * @param party
	 * @return
	 */
//	private String getString(String userId, String fileId, Map<String, Object> party) {
//		Map<String, Object> map = new HashMap<>();
//		map.put("memberid",party.get("userid"));
//		map.put("userid",userId);
//		map.put("fileid",fileId);
//		return JSON.toJSONString(map);
//	}

	/**
	 * 条件组装
	 * @param userId
	 * @param fileId
	 * @return
	 */
//	private Map<String, Object> getStringObjectMap(String userId, String fileId) {
//		Map<String, Object> param = new HashMap<>();
//		param.put("fileid",fileId);
//		param.put("businessid",userId);
//		param.put("businessname",20);
//		param.put("compid",1);
//		param.put("intostatus",6);
//		return param;
//	}
}
