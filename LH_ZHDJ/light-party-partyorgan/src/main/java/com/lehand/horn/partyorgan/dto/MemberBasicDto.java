package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "党员信息基本数据",description = "党员信息基本数据")
public class MemberBasicDto  implements Serializable {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty(value="党员姓名",name="xm")
    private String xm;

    @ApiModelProperty(value="证件号码",name="zjhm")
    private String zjhm;

    @ApiModelProperty(value="学历",name="xl")
    private String xl;

    @ApiModelProperty(value="组织机构id",name="organid",required = true)
    private String organid;

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getZjhm() {
        return zjhm;
    }

    public void setZjhm(String zjhm) {
        this.zjhm = zjhm;
    }

    public String getXl() {
        return xl;
    }

    public void setXl(String xl) {
        this.xl = xl;
    }

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }
}
