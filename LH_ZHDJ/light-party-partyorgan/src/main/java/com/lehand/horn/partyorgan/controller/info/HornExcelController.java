package com.lehand.horn.partyorgan.controller.info;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.service.info.HornExcelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@Api(value = "Excel公共操作接口", tags = { "Excel公共操作接口" })
@RestController
@RequestMapping("/lhdj/excel")
public class HornExcelController extends BaseController {

    @Resource
    private HornExcelService hornExcelService;

    /**
     *
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "通过编码获取Excel模板", notes = "通过编码获取Excel模板", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "编码", dataType = "String", paramType = "query", example = "DZZ_EXCEL_IMPORT")
    })
    @RequestMapping("/getExcelTemplate")
    public String getExcelTemplate(String code, HttpServletResponse response) {
        return hornExcelService.getExcelTemplate(code, response);
    }

}
