package com.lehand.horn.partyorgan.service.info;

import com.alibaba.fastjson.JSONArray;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.HJXXSqlCode;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dao.TSecretaryInfoDao;
import com.lehand.horn.partyorgan.dto.*;
import com.lehand.horn.partyorgan.dto.info.ElectionExportReq;
import com.lehand.horn.partyorgan.pojo.DlType;
import com.lehand.horn.partyorgan.pojo.SysUser;
import com.lehand.horn.partyorgan.pojo.UrcAuthOtherMap;
import com.lehand.horn.partyorgan.pojo.dzz.*;
import com.lehand.horn.partyorgan.service.DataSynchronizationService;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.horn.partyorgan.util.ExcelUtils;
import com.lehand.horn.partyorgan.util.OrgUtil;
import com.lehand.horn.partyorgan.util.SysUtil;
import com.lehand.module.urc.cache.CacheService;
import com.lehand.module.urc.pojo.UserAccountView;
import com.lehand.module.urc.service.UserOrgService;
import com.lehand.module.urc.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.lehand.horn.partyorgan.util.HornDBUtil.getHornDB;

/**
 *	 组织机构业务层
 * @author admin
 *
 */
@Service
public class HornOrganizationService {

    @Resource
    private MemberExcelService memberExcelService;

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private PartyCacheComponent partyCacheComponent;

    @Resource
    private TdzzInfoBcService tdzzInfoBcService;

    @Resource
    private BaseDzzTotalService baseDzzTotalService;

    @Resource
    private CacheService cacheService;

    @Resource private OrgUtil orgUtil;

    @Resource
    private PartyGroupService partyGroupService;
    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;

    @Resource
    private DataSynchronizationService dataSynchronizationService;

    @Resource
    private UserOrgService userOrgService;

    @Resource
    private UserService userService;

    //党委code
    @Value("${jk.organ.dangwei}")
    private String dangwei;

    //党总支部code
    @Value("${jk.organ.dangzongzhi}")
    private String dangzongzhi;

    //党总支部code
    @Value("${jk.organ.dangzhibu}")
    private String dangzhibu;

    @Value("${organ.organ-pid}")
    private String rootNodeCode;

    @Resource private DBComponent dBComponent;
    /**
     * 查询当前组织和当前组织下的组织数据
     * @param session
     * @param organid
     * @param likeStr
     * @return
     */
    public Object getCurrAndSubListOrg(Session session, String organid, String likeStr, String zzlbs) {
        Map<String, Object> paramMap = new HashMap<>();
        if(StringUtils.isEmpty(organid)) {
            organid = session.getCurrentOrg().getOrgcode();
        }
        paramMap.put("organid", organid);
        //查询 id_path
        TDzzInfoSimple tDzzInfoSimple = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple, paramMap);
        //查询下级组织
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(tDzzInfoSimple.getIdPath()).append("%");
        paramMap.put("idPath", stringBuffer.toString());
        paramMap.put("likeStr", likeStr);
        paramMap.put("zzlb", zzlbs);
        List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        if(StringUtils.isEmpty(zzlbs)) {
            records = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleByIdpath, paramMap);
        } else {
            records = generalSqlComponent.query(SqlCode.queryTdzzByIdPathAndZzlb, paramMap);
        }
        //模糊查询返回数据列表
        if(!StringUtils.isEmpty(likeStr)) {
            return addTreeNode(records);
        }
        return treeUtils(tDzzInfoSimple, records);
    }

    public Object getCurrAndSubListOrg2(Session session, String organcode, String likeStr, String zzlbs) {

        if(StringUtils.isEmpty(organcode)) {
            organcode = session.getCurrentOrg().getOrgcode();
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        paramMap.put("likeStr", likeStr);

        if(!StringUtils.isEmpty(likeStr)){
            return getCurrAndSubListOrg(session, organcode, likeStr, zzlbs);
        }
        Map<String,Object> data  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree3, paramMap);
        if(data==null){
            return data;
        }
        data.put("key", data.get("organid"));
        data.put("title", data.get("dzzmc"));
        data.put("slots", getSlots(data.get("zzlb").toString()));
        data.put("children", getMaps(session, data.get("organcode").toString(), likeStr, zzlbs));
        return data;
    }

    public Object getCurrAndSubListOrg3(Session session, String organcode, String likeStr, String zzlbs) {

        if(StringUtils.isEmpty(organcode)) {
            organcode = session.getCurrentOrg().getOrgcode();
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        paramMap.put("zzlb",zzlbs);
        if(!StringUtils.isEmpty(likeStr)){
            return getCurrAndSubListOrg(session, organcode, likeStr, zzlbs);
        }
        Map<String,Object> data  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree4, paramMap);
        if(data==null){
            return data;
        }
        data.put("key", data.get("organid"));
        data.put("title", data.get("dzzmc"));
        data.put("slots", getSlots(data.get("zzlb").toString()));
        data.put("children", getMaps2(session, data.get("organcode").toString(), likeStr, zzlbs));
        return data;
    }
    private List<Map<String, Object>> getMaps2(Session session, String organcode, String likeStr, String zzlbs) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        paramMap.put("zzlb",zzlbs);
        List<Map<String,Object>> list  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree5, paramMap);
        for(Map<String,Object> li:list){
            li.put("key", li.get("organid"));
            li.put("title", li.get("dzzmc"));
            li.put("slots", getSlots(li.get("zzlb").toString()));
            li.put("children", getMaps2(session, li.get("organcode").toString(), likeStr, zzlbs));
        }
        return list;
    }
    private List<Map<String, Object>> getMaps(Session session, String organcode, String likeStr, String zzlbs) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        List<Map<String,Object>> list  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree2, paramMap);
        for(Map<String,Object> li:list){
            li.put("key", li.get("organid"));
            li.put("title", li.get("dzzmc"));
            li.put("slots", getSlots(li.get("zzlb").toString()));
            li.put("children", getMaps(session, li.get("organcode").toString(), likeStr, zzlbs));
        }
        return list;
    }

    /**
     * 增加每个树节点信息
     * @param records
     * @return
     */
    public List<Map<String, Object>> addTreeNode(List<Map<String, Object>> records) {
        for (int i = 0, l = records.size(); i < l; i++) {
            Map<String, Object> record = records.get(i);
            record.put("key", record.get("organid"));
            record.put("title", record.get("dzzmc"));
            record.put("slots", getSlots(record.get("zzlb").toString()));
        }
        return records;
    }

    /**
     * 党组织树形化输出
     * @param tDzzInfoSimple
     * @param records
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> treeUtils(TDzzInfoSimple tDzzInfoSimple, List<Map<String, Object>> records) {
        Map<String, Map<String, Object>> tree = new HashMap<>();
        Map<String, Object> head = new HashMap<>();
        head.put("title", tDzzInfoSimple.getDzzmc());
        head.put("key", tDzzInfoSimple.getOrganid());
        head.put("slots", getSlots(tDzzInfoSimple.getZzlb()));
        head.put("organcode", tDzzInfoSimple.getOrgancode());
        head.put("organid", tDzzInfoSimple.getOrganid());
        head.put("parentcode", tDzzInfoSimple.getParentcode());
        head.put("children", new ArrayList<Map<String, Object>>());
        head.put("zzlb", tDzzInfoSimple.getZzlb());
        tree.put(tDzzInfoSimple.getOrgancode(), head);

        for (int i = 0, l = records.size(); i < l; i++) {
            Map<String, Object> record = records.get(i);
            String parentcode = record.get("parentcode").toString();
            record.put("children", new ArrayList<Map<String, Object>>());
            if (tree.containsKey(parentcode)) {
                List<Map<String, Object>> children = (List<Map<String, Object>>) tree.get(parentcode).get("children");
                record.put("key", record.get("organid"));
                record.put("title", record.get("dzzmc"));
                record.put("slots", getSlots(record.get("zzlb").toString()));
                children.add(record);
                tree.put(record.get("organcode").toString(), record);
            }
        }
        return tree.get(tDzzInfoSimple.getOrgancode());
    }

    //返回组织树节点的类别
    public Map<String, Object> getSlots(String zzlb) {
        Map<String, Object> map = new HashMap<>();
        map.put("icon", zzlb);
        return map;
    }

    /**
     * 查询组织机构树
     * @param organid 组织机构id
     * @return
     * @author taogang
     */
    public List<Map<String, Object>> getListByOrganId(Session session, String organid, String likeStr){
        //定义list 数组
        List<Map<String, Object>> list = new ArrayList<>();
        //查询党组织信息集合
        List<TDzzInfoSimple> dzzList = new ArrayList<>();
        //判断组织机构id是否为空
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organid", organid);
        if(StringUtils.isEmpty(organid)) {
            paramMap.put("organid", session.getCurrentOrg().getOrgcode());
            dzzList = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple,paramMap);
        }else {
            dzzList = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleLikestr, paramMap);
        }
        //模糊搜索
        if(!StringUtils.isEmpty(likeStr)){
            paramMap.put("likeStr", likeStr);
            TDzzInfoSimple tdzz = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple, paramMap);
            paramMap.put("likeStr", tdzz.getIdPath());
            dzzList = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleLikestr,paramMap);
            if(null == dzzList) {
                dzzList = new ArrayList<>();
            }
        }

        if(dzzList.size() > 0 ) {
            Map<String, Object> bean = null;
            for (TDzzInfoSimple tDzzInfoSimple : dzzList) {
                bean = new HashMap<>();
                bean.put("organid", tDzzInfoSimple.getOrganid());
                bean.put("dzzmc", tDzzInfoSimple.getDzzmc());
                bean.put("dzzqc", tDzzInfoSimple.getDzzmc());
                bean.put("orgtype", tDzzInfoSimple.getOrgantype());
                bean.put("icon", tDzzInfoSimple.getZzlb());
                bean.put("parentid", tDzzInfoSimple.getParentid());
                bean.put("isSubordinate", isSubordinate(tDzzInfoSimple.getOrganid()));
                bean.put("isOpen", "true");
                list.add(bean);
            }
        }
        return list;
    }

    /**
     * 是否包含下级
     * @param organid 组织机构id
     * @return
     * @author taogang
     */
    public Integer isSubordinate(String organid) {
        return (Integer)generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleByIdcount, new Object[] {organid}) > 0 ? 1 : 0 ;
    }

    /**
     * 组织机构列表查询
     * @param organid  	组织机构id
     * @param dwlb  	单位类别
     * @param likeStr 	dzzmc 模糊查询
     * @return
     * @author taogang
     */
    public Pager getList(Pager pager, String organid, Integer dwlb, String likeStr){
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organid", organid);
        paramMap.put("likeStr", likeStr);
        Pager page = new Pager();
        if(dwlb == 0) {//dwlb是否包含下级 （0 是 1 否）
            //查询 id_path
            TDzzInfoSimple tDzzInfoSimple = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple2, paramMap);
            //查询下级组织
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(tDzzInfoSimple.getIdPath()).append("%");
            paramMap.put("idPath", stringBuffer.toString());
            page = generalSqlComponent.pageQuery(SqlCode.queryTdzzInfoSimpleByIdpath2, paramMap, pager);
        } else if(dwlb == 1) {
            page = generalSqlComponent.pageQuery(SqlCode.hornOrgTableList, paramMap, pager);
        }

        List<TDzzInfoSimple> dzzList = new PagerUtils<TDzzInfoSimple>(page).getRows();
        if(dzzList.size() > 0) {
            Map<String, Object> bean = null;
            for(TDzzInfoSimple row : dzzList) {
                bean = new HashMap<>();
                bean.put("organid", row.getOrganid());
                bean.put("dzzmc", row.getDzzmc());
                bean.put("organcode", row.getOrgancode());
                bean.put("zzlb", partyCacheComponent.getName("d_dzz_zzlb", row.getZzlb()));
                bean.put("dwlb", partyCacheComponent.getName("d_dw_xzlb", row.getSzdwlb()));
                bean.put("dzzsj",  tdzzInfoBcService.getDzzsj(row.getOrganid()));
                bean.put("dzzlsgx",  partyCacheComponent.getName("d_dzz_lsgx",  row.getDzzlsgx()));
                bean.put("xxwzd", row.getXxwzd());
                bean.put("organtype", row.getOrgantype());
                bean.put("ordernum",row.getOrdernum());
                list.add(bean);
            }
        }

        if(page != null) {
            page.setRows(list);
        }
        return page;
    }

    /**
     * 统计列表条数
     * @param organid
     * @param dwlb
     * @param likeStr
     * @return
     * @author taogang
     */
    public int countDzz(String organid,String dwlb,String likeStr) {
        int count = 0;
        //定义查询条件
        StringBuffer sql = new StringBuffer();
        List<String> params = new ArrayList<String>(4);
        params.add(organid);
        params.add(organid);
        sql.append("select count(1) from t_dzz_info_simple where (organid = ? or parentid = ?)");
        if(!StringUtils.isEmpty(dwlb)) {
            sql.append(" and szdwlb = ?");
            params.add(dwlb);
        }
        if(!StringUtils.isEmpty(likeStr)) {
            likeStr = "%" + likeStr + "%";
            sql.append(" and dzzmc like ?");
            params.add(likeStr);
        }
        count = getHornDB().getSingleColumn(sql.toString(),int.class, params.toArray());
        return count;
    }

    /**
     * 获取基础信息基本信息
     * @param organid
     * @return
     * @author taogang
     */
    public Map<String, Object> getBase(String organid){
        Map<String, Object> bean = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleDzzBase,  new Object[] {organid});
        if(bean == null) {
            return bean;
        }
        bean.put("zzlbName",partyCacheComponent.getName("d_dzz_zzlb",bean.get("zzlb")));
        bean.put("dzzlsgx",partyCacheComponent.getName("d_dzz_lsgx",bean.get("dzzlsgx"))==""?bean.get("dzzlsgx"):partyCacheComponent.getName("d_dzz_lsgx",bean.get("dzzlsgx")));
        bean.put("dwxzlb",partyCacheComponent.getName("d_dw_xzlb",bean.get("dwxzlb"))==""?bean.get("dwxzlb"):partyCacheComponent.getName("d_dw_xzlb",bean.get("dwxzlb")));
        bean.put("dzzszdwqk",partyCacheComponent.getName("d_dzz_dwqk",bean.get("dzzszdwqk"))==""?bean.get("dzzszdwqk"):partyCacheComponent.getName("d_dzz_dwqk",bean.get("dzzszdwqk")));
        bean.put("xzqh",partyCacheComponent.getName("d_dzz_xzqh",bean.get("xzqh"))==""?bean.get("xzqh"):partyCacheComponent.getName("d_dzz_xzqh",bean.get("xzqh")));
        bean.put("dzzlsgx", partyCacheComponent.getName("d_dzz_dwqk", bean.get("dzzlsgx"))==""?bean.get("dzzlsgx"):partyCacheComponent.getName("d_dzz_dwqk", bean.get("dzzlsgx")));
        bean.put("dwxzlb",partyCacheComponent.getName("d_dw_xzlb", bean.get("dwxzlb"))==""?bean.get("dwxzlb"):partyCacheComponent.getName("d_dw_xzlb", bean.get("dwxzlb")));
        TDzzHjxxInfoDto hjxxInfoDto= generalSqlComponent.query(HJXXSqlCode.getHJXXLimit1,new Object[]{bean.get("organcode")});
        bean.put("dzzjmrq",hjxxInfoDto==null?"":hjxxInfoDto.getPlandate());
        return bean;
    }

    /**
     * 单位所在信息
     * @param organid
     * @return
     * @author taogang
     */
    public Map<String, Object> getDwInfo(String organid){
        Map<String, Object> bean = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleDwinfoById, new Object[] {organid});
        if(null != bean) {
            bean.put("dwlsgx",partyCacheComponent.getName("d_dw_lsgx", bean.get("dwlsgx"))==""?bean.get("dwlsgx"):partyCacheComponent.getName("d_dw_lsgx", bean.get("dwlsgx")));
            bean.put("dwxzlb",partyCacheComponent.getName("d_dw_xzlb", bean.get("dwxzlb"))==""?bean.get("dwxzlb"):partyCacheComponent.getName("d_dw_xzlb", bean.get("dwxzlb")));
            bean.put("dwjldjczzqk",partyCacheComponent.getName("d_dw_jljczz", bean.get("dwjldjczzqk"))==""?bean.get("dwjldjczzqk"):partyCacheComponent.getName("d_dw_jljczz", bean.get("dwjldjczzqk")));
            bean.put("jjlx",partyCacheComponent.getName("d_dw_jjlx", bean.get("jjlx"))==""?bean.get("jjlx"):partyCacheComponent.getName("d_dw_jjlx", bean.get("jjlx")));
            bean.put("qygm",partyCacheComponent.getName("d_dw_qygm", bean.get("qygm"))==""?bean.get("qygm"):partyCacheComponent.getName("d_dw_qygm", bean.get("qygm")));
//            bean.put("dwlsgx", partyCacheComponent.getName("d_dw_lsgx", bean.get("dwlsgx")));
        }
        return bean;
    }

    /**
     * 换届信息
     * @param organid
     * @return
     * @author taogang
     */
    public List<Map<String, Object>> getHjxxList(String organid){
        //定义map
        List<Map<String, Object>> list = new ArrayList<>();
        //换届信息结果集
        List<TDzzHjxxDzz> hjxxList = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleListById, new Object[] {organid});
        if(hjxxList.size() > 0 ) {
            Map<String, Object> bean = null;
            for (TDzzHjxxDzz row : hjxxList) {
                bean = new HashMap<>();
                bean.put("uuid", row.getUuid());
                bean.put("bzversion", row.getBzversion());
                bean.put("bjbzcsfs", row.getBjbzcsfs());
                bean.put("bjbzcsfsName", partyCacheComponent.getName("d_dzz_xjfs", row.getBjbzcsfs()));
                bean.put("hjdate", row.getHjdate());
                bean.put("jmdate", row.getJmdate());
                list.add(bean);
            }
        }
        return list;
    }

    /**
     * 班子成员
     * @param organid
     * @param type 有值即书记花名册选人
     * @return
     * @author taogang
     */
    public List<Map<String, Object>> getBzcyxxList(String organid, String xm,String type) throws Exception{
        //定义list数组
        Map<String, Object> paramMap = new HashMap<>();
        if(!organid.isEmpty() && organid.length()<32){
            Map map = generalSqlComponent.query(SqlCode.getOrganidByOrgancode,new Object[]{organid});
            paramMap.put("organid", map.get("organid"));
        }else{
            paramMap.put("organid", organid);
        }
        paramMap.put("xm", xm);
        if(!StringUtils.isEmpty(type)){
            paramMap.put("type", type);
        }else{
            paramMap.put("type", "");
        }
        List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        List<Map<String, Object>> list =generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleBZCYList, paramMap);
        if(list.size() < 0) {
            return result;
        }
        for (Map<String, Object> map : list) {
            map.put("xlName", partyCacheComponent.getName("d_dy_xl",map.get("xl")));
            map.put("dnzwnameName", partyCacheComponent.getName("d_dy_dnzw", map.get("dnzwname")));
            map.put("zwlevelName", partyCacheComponent.getName("d_dzz_zwjb", map.get("zwlevel")));
            if(!StringUtils.isEmpty(type)){
                Map data = generalSqlComponent.query(SqlCode.getTSecretaryInfo, new Object[]{map.get("userid"),1});
                if(data==null){
                    //不禁用 可选
                    map.put("flag","able");
                }else{
                    //禁用 不可选
                    map.put("flag","disable");
                }
            }
            map.put("ryfs", "从系统中选择");
            String lzdateStr = String.valueOf(map.get("lzdate"));
            if(!StringUtils.isEmpty(map.get("lzdate"))&&DateEnum.YYYYMMDDHHMM.parse(lzdateStr).before(DateEnum.YYYYMMDDHHMM.parse(DateEnum.YYYYMMDDHHMM.format()))) {
                continue;
            }

            result.add(map);
        }
        return result;
    }

    /**
     * 获取奖惩信息
     * @param organid
     * @return
     * @author taogang
     */
    public List<Map<String, Object>> getJcxx(String organid){
        //定义数组
        List<Map<String, Object>> list = new ArrayList<>();
        List<TDzzJcxxDzz> jcxxList = generalSqlComponent.query(SqlCode.queryTdzzInfoSimpleJCXXList, new Object[] {organid});
        if(jcxxList.size() > 0 ) {
            Map<String, Object> bean = null;
            for (TDzzJcxxDzz row : jcxxList) {
                bean = new HashMap<>();
                bean.put("uuid", row.getUuid());
                bean.put("jcname", row.getJcname());
                bean.put("jcdate", row.getJcdate());
                bean.put("jcreason", row.getJcreason());
                bean.put("pzjcdzz", row.getPzjcdzz());
                bean.put("jcnameName", partyCacheComponent.getName("d_dzz_jcmc", row.getJcname()));
                list.add(bean);
            }
        }
        return list;
    }
    private List<ExportColumnReq> getExportColumns() {
        List<ExportColumnReq> columns =new ArrayList<>();
        //党组织代码	党组织名称	组织类别	属地关系	党组织书记	党组织联系人	联系电话	建立党组织日期	通讯地址	传真号码	党组织所在单位情况	党组织所在行政区划	单位名称	单位隶属关系	单位性质类别	单位建立党组织情况	法人单位标识	法人代表（单位负责人）	是否党员	社会统一信用代码	企业控制（控股）情况	企业规模	民营科技企业标识	社会服务机构类别	事业单位类别	在岗职工数（人）	在岗职工中工人数（人）	在岗专业技术人员数（人）	是否建有工会	中介组织标识	户数（户）	人口（人）	上级党组织编码
        ExportColumnReq organcode = new ExportColumnReq("党组织代码","organcode");
        columns.add(organcode);
        ExportColumnReq dzzmc = new ExportColumnReq("党组织名称","dzzmc");
        columns.add(dzzmc);
        ExportColumnReq zzlb = new ExportColumnReq("党组织类别","zzlb");
        columns.add(zzlb);
        ExportColumnReq dzzlsgx = new ExportColumnReq("属地关系","dzzlsgx");
        columns.add(dzzlsgx);
        ExportColumnReq dzzsj = new ExportColumnReq("党组织书记","dzzsj");
        columns.add(dzzsj);
        ExportColumnReq lxrname = new ExportColumnReq("党组织联系人","lxrname");
        columns.add(lxrname);
        ExportColumnReq lxrphone = new ExportColumnReq("联系电话","lxrphone");
        columns.add(lxrphone);
        ExportColumnReq dzzjlrq = new ExportColumnReq("建立党组织日期","dzzjlrq");
        columns.add(dzzjlrq);
        ExportColumnReq address = new ExportColumnReq("通讯地址","address");
        columns.add(address);
        ExportColumnReq fax = new ExportColumnReq("传真号码","fax");
        columns.add(fax);
        ExportColumnReq dzzszdwqk = new ExportColumnReq("党组织所在单位情况","dzzszdwqk");
        columns.add(dzzszdwqk);
        ExportColumnReq xzqh = new ExportColumnReq("党组织所在行政区划","xzqh");
        columns.add(xzqh);
        ExportColumnReq dwname = new ExportColumnReq("单位名称","dwname");
        columns.add(dwname);
        ExportColumnReq dwlsgx = new ExportColumnReq("单位隶属关系","dwlsgx");
        columns.add(dwlsgx);
        ExportColumnReq dwxzlb = new ExportColumnReq("单位性质类别","dwxzlb");
        columns.add(dwxzlb);
        ExportColumnReq dwjldjczzqk = new ExportColumnReq("单位建立党组织情况","dwjldjczzqk");
        columns.add(dwjldjczzqk);
        ExportColumnReq isfrdw = new ExportColumnReq("法人单位标识","isfrdw");
        columns.add(isfrdw);
        ExportColumnReq dwfzr = new ExportColumnReq("法人代表（单位负责人）","dwfzr");
        columns.add(dwfzr);
        ExportColumnReq fzrsfsdy = new ExportColumnReq("单位负责人是否为党员","fzrsfsdy");
        columns.add(fzrsfsdy);
        ExportColumnReq dwcode = new ExportColumnReq("社会统一信用代码","dwcode");
        columns.add(dwcode);
        ExportColumnReq jjlx = new ExportColumnReq("企业控制（控股）情况","jjlx");
        columns.add(jjlx);
        ExportColumnReq qygm = new ExportColumnReq("企业规模","qygm");
        columns.add(qygm);
        ExportColumnReq sfmykjqy = new ExportColumnReq("民营科技企业标识","sfmykjqy");
        columns.add(sfmykjqy);
        ExportColumnReq shfwjghy = new ExportColumnReq("社会服务机构类别","shfwjghy");
        columns.add(shfwjghy);
        ExportColumnReq sydwlx = new ExportColumnReq("事业单位类别","sydwlx");
        columns.add(sydwlx);
        ExportColumnReq zgzgs = new ExportColumnReq("在岗职工数（人）","zgzgs");
        columns.add(zgzgs);
        ExportColumnReq zgzgzgrs = new ExportColumnReq("在岗职工中工人数（人）","zgzgzgrs");
        columns.add(zgzgzgrs);
        ExportColumnReq zgzyjsrys = new ExportColumnReq("在岗专业技术人员数（人）","zgzyjsrys");
        columns.add(zgzyjsrys);
        ExportColumnReq sfjygh = new ExportColumnReq("是否建有工会","sfjygh");
        columns.add(sfjygh);
        ExportColumnReq sfzjzz = new ExportColumnReq("中介组织标识","sfzjzz");
        columns.add(sfzjzz);
        ExportColumnReq hs = new ExportColumnReq("户数（户）","hs");
        columns.add(hs);
        ExportColumnReq czrk = new ExportColumnReq("人口（人）","czrk");
        columns.add(czrk);
        ExportColumnReq parentcode = new ExportColumnReq("上级党组织编码","parentcode");
        columns.add(parentcode);
        return columns;
    }

    /**
     * 导出党组织信息
     * @param response
     * @param req
     * @param session
     */
    public String dzzInfoExport(HttpServletResponse response, DZZExportReq req , Session session) {
//        List<ExportColumnReq> columns  = req.getExportColumns();
        List<ExportColumnReq> columns  = getExportColumns();//需要导出的字段
        if(columns.size()<1){
            LehandException.throwException("请先选择导出的字段！");
        }
        //查询导出的党组织信息
        List<Map<String, Object>> list = getDZZList(req, session);

        //表头
        List<String> tables = new ArrayList<String>();
        for (ExportColumnReq col: columns) {
            tables.add(col.getTitle());
        }
        String[] tablesStr = tables.toArray(new String[tables.size()]);
        //表名
        String tableName = "党组织信息";
        //内容
        List<Map<String,String>> mapList =  getDzzExportInfo(list,columns);
        ExcelUtils.write(response,tablesStr,mapList,tableName);
        return null;
    }


    /**
     * 查询导出的党组织信息
     * @return
     */
    public List<Map<String, Object>> getDZZList(DZZExportReq req , Session session) {
        Map<String, Object> paramMap = new HashMap<>();
        if(StringUtils.isEmpty(req.getOrganid())) {
            paramMap.put("organid", session.getCurrentOrg().getOrgcode());
            TDzzInfoSimple tDzzInfoSimple = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple, paramMap);
            paramMap.put("organid", tDzzInfoSimple.getOrganid());
        } else {
            paramMap.put("organid", req.getOrganid());
        }
        paramMap.put("likeStr", req.getLikeStr());
        List<Map<String, Object>> list = null;
        if(req.getDwlb() == 0) {//dwlb是否包含下级 （0 是 1 否）
            //查询 id_path
            TDzzInfoSimple tDzzInfoSimple = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple2, paramMap);
            //查询下级组织
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(tDzzInfoSimple.getIdPath()).append("%");
            paramMap.put("idPath", stringBuffer.toString());
            list = generalSqlComponent.query(SqlCode.queryTdzzInfoOrgView, paramMap);
        } else if(req.getDwlb() == 1) {
            list = generalSqlComponent.query(SqlCode.queryHornOrgView, paramMap);
        }

        for(Map<String, Object> map : list) {
            map.put("dzzlsgx", StringUtils.isEmpty(partyCacheComponent.getName("d_dzz_dwqk", map.get("dzzlsgx")))?map.get("dzzlsgx"):null);
            map.put("dzzszdwqk", StringUtils.isEmpty(partyCacheComponent.getName("d_dzz_dwqk", map.get("dzzszdwqk")))?map.get("dzzszdwqk"):null);
            map.put("zzlb", partyCacheComponent.getName("d_dzz_zzlb", map.get("zzlb")));
            map.put("xzqh", StringUtils.isEmpty(partyCacheComponent.getName("d_dzz_xzqh", map.get("xzqh")))?map.get("xzqh"):null);
            map.put("dwxzlb", StringUtils.isEmpty(partyCacheComponent.getName("d_dw_xzlb", map.get("dwxzlb")))?map.get("dwxzlb"):null);
            if(StringUtils.isEmpty(map.get("dzzjlrq"))) {
                map.put("dzzjlrq", map.get("dzzjlrq").toString().replace("00:00:00",""));
            }
            map.put("isfrdw","1".equals(map.get("isfrdw"))?"是":"否");
            map.put("fzrsfsdy","1".equals(map.get("fzrsfsdy"))?"是":"否");
            map.put("sfmykjqy","1".equals(map.get("sfmykjqy"))?"是":"否");
            map.put("sfjygh","1".equals(map.get("sfjygh"))?"是":"否");
            map.put("sfzjzz","1".equals(map.get("sfzjzz"))?"是":"否");
        }

        return list;
    }


    /**
     * 加载导出的项目
     * @param list
     * @param columns
     * @return
     */
    public List<Map<String,String>> getDzzExportInfo(List<Map<String, Object>> list, List<ExportColumnReq> columns) {
        List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
        for ( Map<String, Object> info: list) {
            Map<String, String> tempMap = new HashMap<>();
            for (ExportColumnReq col: columns) {
                tempMap.put(col.getTitle(), memberExcelService.getInfo(col.getCode(), info));
            }
            explist.add(tempMap);
        }
        return explist;
    }


    /**
     * 导入党组织信息
     * @param file
     * @param session
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public String dzzInfoImport(MultipartFile file, Session session) {
        //TODO 待优化结构
        int total = 0;
        int success = 0;
        int failed = 0;
        StringBuffer retMsg = new StringBuffer("");
        List<String> errorMsg = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            List<List<String>> list = ExcelUtils.readExcelNew(file.getOriginalFilename(), inputStream);
            if(list.size() <= 0) {
                LehandException.throwException("没有读取到正确的党组织导入信息！");
            }

            List<String> listNew = new ArrayList<>();
            list.forEach(a->{
                listNew.add(a.get(0));
            });
            //校验是否有党组织被删除了或者被合并了
            Map<String, Object> paramMap2 = new HashMap<>();
            paramMap2.put("organcode",list.get(0).get(0));
            TDzzInfo pold = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, paramMap2);
            String orgnames = "";
            if(pold!=null){
                List<String> listOld = generalSqlComponent.query(SqlCode.listTDzzInfos3,new Object[]{"%"+pold.getOrganid()+"%"});
                for(String o :listOld){
                    if(!listNew.contains(o)){
                        //不包含将党组织信息置为无效状态使用字段 operatetype=3
                        orgnames += "'"+o+"'"+",";
                    }
                }
            }
            orgnames = !StringUtils.isEmpty(orgnames) ? orgnames.substring(0,orgnames.length()-1) : orgnames;
            Map params = new HashMap();
            params.put("orgname",orgnames);
            if(!StringUtils.isEmpty(orgnames)){
                //t_dzz_info 置为无效
                generalSqlComponent.update(SqlCode.modiTDzzinfos,params);
                //t_dzz_info_simple 置为无效
                Map map = generalSqlComponent.query(SqlCode.getOrganidBydxzms,params);
                generalSqlComponent.update(SqlCode.modiTDzzSimpleinfos,map);
                //urc_organization 置为无效
                generalSqlComponent.update(SqlCode.modiUrcOrganizations,params);
                //urc_user_account 删除账号
                Map map2 = generalSqlComponent.query(SqlCode.getOrganidBydxzms2,params);
                String organcode = String.valueOf(map2.get("organcode"));
                String[] organcodes = organcode.split(",");
                String orgcodes = "";
                for (String s : organcodes) {
                    orgcodes += "'B"+s+"','A"+s+"',";
                }
                orgcodes = orgcodes.substring(0,orgcodes.length()-1);
                dBComponent.update("update sys_user set status=0 where acc in ("+orgcodes+")",new Object[]{});
                String userids = generalSqlComponent.query(SqlCode.modiUrcOrganizations2,params);
                dBComponent.update("update urc_user set status=0 where userid in ("+userids+")",new Object[]{});
            }

            total = list.size() ;
            //从第三行导入党组织信息
            Map<String, Object> paramMap = new HashMap<>();
            for(int i = 0 ; i < list.size(); i++) {
                try {
                    List<String> dzz = list.get(i);
                    String organid = UUID.randomUUID().toString().replace("-","");
                    if(StringUtils.isEmpty(dzz.get(0).trim()) || StringUtils.isEmpty(dzz.get(32).trim())) {
                        failed++;
                        errorMsg.add(String.format("第%d行 党组织编码或者上级党组织编码为空，跳过;",(i+2)));
                        continue;
                    }

                    paramMap.put("organcode", dzz.get(32).trim());
                    TDzzInfo pold2 = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, paramMap);
                    if(!"0.0".equals(dzz.get(32).trim())){
                        if(pold2 == null) {
                            failed++;
                            errorMsg.add(String.format("第%d行 党组织编码或者上级党组织编码记录不存在，跳过;",(i+2)));
                            continue;
                        }
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        if(dzz.size()>7&&!StringUtils.isEmpty(dzz.get(7))){
                            dzz.set(7,sdf.format(sdf.parse(dzz.get(7))));
                        }
//                        if(dzz.size()>33&&!StringUtils.isEmpty(dzz.get(33))){
//                            dzz.set(33,sdf.format(sdf.parse(dzz.get(33))));
//                        }
//                        if(dzz.size()>34&&!StringUtils.isEmpty(dzz.get(34))){
//                            dzz.set(34,sdf.format(sdf.parse(dzz.get(34))));
//                        }
                    } catch (Exception e) {
                        failed++;
                        errorMsg.add(String.format("第%s行 存在时间格式错误，请检查",(i+2)));
                        continue;
                    }
                    try {
                        Map data = generalSqlComponent.query(SqlCode.getTDzzInfo2,new Object[]{dzz.get(0).trim()});
                        if(data!=null){
                            organid = String.valueOf(data.get("organid"));
                        }

                        //保存导入党组织信息
                        saveDzz(dzz, pold, organid, session, pold2);
                        success++;
                    } catch (Exception e) {
                        errorMsg.add(String.format("第%s行 数据导入异常,请检查后重试",(i+2)));
                        failed++;
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    failed++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //同步机构简称
//        dataSynchronizationService.synchronize();
        new Thread(()-> cacheService.refCache()).start();
        if(errorMsg.size() > 0) {
            //异步刷新缓存
            return String.format("%s \r\n 党组织数据导入成功%d条，失败%d条。", errorMsg.toString(), success,failed);
        } else {
            //异步刷新缓存
            return String.format("党组织数据导入成功%d条，失败%d条。", success,failed);
//            return null;
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void saveDzz(List<String> dzz, TDzzInfo pold, String organid, Session session, TDzzInfo parent) {
        //保存单位信息
        String dwUuids = saveDZZDWInfo(dzz, organid);
        //保存导入的党组织的主表信息
        saveMainDZZInfo(dzz, organid, dwUuids, pold, parent);
        //保存换届信息配置表
//        saveDZZHjxxDzz(dzz, organid);
        //党组织路径等信息配置表
        saveDzzInfoSimple(dzz, organid, pold, parent);
        //党组织补充表
        saveDZZBCInfo(dzz, organid);
        //保存党组织到系统组织机构表
        Long orgId = saveUrcOrg(dzz, session);
        //新建党组织账户
        addUrcUserAndAccount(dzz, session, orgId, organid);

    }

    private void insertSysuser(List<String> data,String organid){
        SysUser user = new SysUser();
        user.setId(UUID.randomUUID().toString().replace("-",""));
        user.setPwd("0ffe6471a68f774d03695b1e536c2da5");
        user.setSalt("448d7c19946648e5aad3b9b7b3f7a39a");
        Map map = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{data.get(2)});
        if(map!=null && "6100000035".equals(String.valueOf(map.get("item_code")))){
            user.setRoles("role_party_committee");
        }else if(map!=null && "6200000036".equals(String.valueOf(map.get("item_code")))){
            user.setRoles("role_general_party_branch");
        }else{
            user.setRoles("role_party_branch");
        }
        user.setAcc("B"+ data.get(0));
        user.setType("SYS");
        user.setName(data.get(1));
        user.setOrgId(organid);
        user.setOrgName(data.get(1));
        if(data.get(6).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(6));
            user.setWorkPhone(bd.toPlainString());// 7
        }else{
            user.setWorkPhone(data.get(6));
        }
        user.setStatus(1);
        user.setBaseInfoId(null);
        user.setCreateTime(DateEnum.now());
        user.setModiTime(DateEnum.now());
        user.setCreater("1");
        user.setModifier("1");
        Map map2 = generalSqlComponent.query(SqlCode.getSysuser,new Object[]{user.getAcc()});
        if(map2==null){
            generalSqlComponent.insert(SqlCode.addSysuser,user);
        }
//        user.setAcc("A"+ data.get(0));
//        user.setId(UUID.randomUUID().toString().replace("-",""));
//        Map map3 = generalSqlComponent.query(SqlCode.getSysuser,new Object[]{user.getAcc()});
//        if(map3==null){
//            generalSqlComponent.insert(SqlCode.addSysuser,user);
//        }
    }

    private void updateSysuser(List<String> data,String organid){
        SysUser user = new SysUser();
        Map map = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{data.get(2)});
        if(map!=null && "6100000035".equals(String.valueOf(map.get("item_code")))){
            user.setRoles("role_party_committee");
        }else if(map!=null && "6200000036".equals(String.valueOf(map.get("item_code")))){
            user.setRoles("role_general_party_branch");
        }else{
            user.setRoles("role_party_branch");
        }
        user.setAcc("B"+data.get(0));
        user.setName(data.get(1));
        user.setOrgId(organid);
        user.setOrgName(data.get(1));
        if(data.get(6).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(6));
            user.setWorkPhone(bd.toPlainString());// 7
        }else{
            user.setWorkPhone(data.get(6));
        }
        user.setBaseInfoId(null);
        generalSqlComponent.update(SqlCode.modiSysuser2,user);
//        user.setAcc("A"+data.get(0));
//        generalSqlComponent.update(SqlCode.modiSysuser2,user);
    }


    /**
     * 增加党组织在系统模块的用户和账号信息
     * @param dzz
     */
    @Transactional(rollbackFor=Exception.class)
    public void addUrcUserAndAccount(List<String> dzz, Session session, Long orgId,String organid) {
        String idno = dzz.get(0).trim();
        Map<String, Object> oldUser = generalSqlComponent.query(SqlCode.queryUrcUserByIdno, new Object[] {idno});
        Long userid = 0L;
        Map<String,Object> param = new HashMap<String, Object>(6);
        param.put("username",dzz.get(1));
        param.put("sex",0);
        if(dzz.get(6).contains(".")){
            BigDecimal bd = new BigDecimal(dzz.get(6));
            param.put("phone",bd.toPlainString());
        }else{
            param.put("phone",dzz.get(6));
        }
        param.put("idno",idno);
        param.put("compid",session.getCompid());
        param.put("idtype", 2);
        param.put("relationid", "");
        param.put("usertype", 0);
        param.put("seqno", 0);
        param.put("avatar", "");
        param.put("postname", "");
        param.put("landlines", "");
        param.put("email", "");
        param.put("useraddr", "");
        param.put("userlable", "");
        param.put("createuserid", 1);
        param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        param.put("updateuserid", null);
        param.put("updatetime", "");
        param.put("accflag", 1);

        if(oldUser == null){//修改
            userid = generalSqlComponent.insert(SqlCode.insertUrcuser,param);
            insertSysuser(dzz,organid);
        } else {
            userid = Long.valueOf(oldUser.get("userid").toString());
            generalSqlComponent.update(SqlCode.updateUrcuserByOrgcode,new Object[]{dzz.get(1),0,param.get("phone"), 2,session.getCompid(),idno});
            updateSysuser(dzz,organid);
        }

        //绑定用户和组织的关系
        param.put("userid",userid);
        param.put("orgid",orgId);
        param.put("seqno",orgId);
        param.put("compid",session.getCompid());
        generalSqlComponent.delete(SqlCode.removeUserOrgMapByUserid, new Object[] {userid, session.getCompid()});
        generalSqlComponent.insert(SqlCode.addUserOrgMap2, param);
        //更新机构表remarks3字段为用户ID
        generalSqlComponent.update(SqlCode.modiUserOrg, new Object[]{userid,orgId});

        //更新账户表
        generalSqlComponent.delete(SqlCode.delUaccountByAccount, new Object[] {String.format("B%s",idno)});
        generalSqlComponent.insert(SqlCode.addUserAccount2,new Object[]{userid,String.format("B%s",idno), SysConstants.DEFAULTPWD.SIMPLESIX,session.getCompid()});
//        generalSqlComponent.delete(SqlCode.delUaccountByAccount, new Object[] {String.format("A%s",idno)});
//        generalSqlComponent.insert(SqlCode.addUserAccount2,new Object[]{userid,String.format("A%s",idno), SysConstants.DEFAULTPWD.SIMPLESIX,session.getCompid()});

        //给组织账号添加相应的菜单权限
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
        String orgcode = map1!=null ? map1.get("item_code").toString() : "";
        Map map = null;
        if(dangwei.equals(orgcode)){
            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.PARTY_COMMITTEE_ROLE});
        }else if(dangzongzhi.equals(orgcode)){
            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.GENERAL_PARTY_BRANCH_ROLE});
        }else{
            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.PARTY_BRANCH_ROLE});
        }
        generalSqlComponent.delete(SqlCode.removeUrcRoleMap,new Object[]{0,userid,map.get("roleid"),session.getCompid()});
        generalSqlComponent.insert(SqlCode.addUrcRoleMap,new Object[]{0,userid,map.get("roleid"),session.getCompid(),orgId,DateEnum.YYYYMMDDHHMMDD.format()});
    }

    /**
     * 保存导入的党组织的主表信息
     * @param dzz
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveMainDZZInfo(List<String> dzz, String organid, String dwUuids, TDzzInfo pold,TDzzInfo parent) {
        //查询是否存在党组织
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", dzz.get(0).trim());
        //generalSqlComponent.delete(SqlCode.delDzzInfoByCode, paramMap);

        TDzzInfo tDzzInfo = new TDzzInfo();
        tDzzInfo.setDelflag("0");
        tDzzInfo.setOrganid(organid);
        tDzzInfo.setParentid(parent==null ? "0" : parent.getOrganid());//查询返回父ID
        boolean oc = dzz.get(0).trim().contains("'");
        if(!oc){
            tDzzInfo.setOrgancode(dzz.get(0).trim());
        }else{
            tDzzInfo.setOrgancode(dzz.get(0).trim().substring(1,dzz.get(0).trim().length()-1));
        }
        tDzzInfo.setDzzmc(dzz.get(1));
        tDzzInfo.setDxzms(dzz.get(1));
        tDzzInfo.setOperatetype("0");
        //组织类别
        Map map = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
        tDzzInfo.setZzlb(map!=null ? map.get("item_code").toString() : "");
        //单位隶属关系
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode3,new Object[]{dzz.get(3)});
//        tDzzInfo.setDzzlsgx(map1!=null ? map1.get("item_code").toString() : "");
        tDzzInfo.setDzzlsgx(dzz.get(3));
//        tDzzInfo.setDzzsj(dzz.get(4));
        tDzzInfo.setDzzsj("");
        tDzzInfo.setLxrname(dzz.get(5));
        if(dzz.get(6).contains(".")){
            BigDecimal bd = new BigDecimal(dzz.get(6));
            tDzzInfo.setLxrphone(bd.toPlainString());// 7
        }else{
            tDzzInfo.setLxrphone(dzz.get(6));// 7
        }

        tDzzInfo.setXzqh(dzz.get(11)); //行政区号
        //单位建立党组织情况
        Map map2 = generalSqlComponent.query(SqlCode.getZzlbCode4,new Object[]{dzz.get(15)});
//        tDzzInfo.setDzzszdwqk(map1!=null ? map1.get("item_code").toString() : "");
        tDzzInfo.setDzzszdwqk(dzz.get(15));
        tDzzInfo.setSzdwid(dwUuids);//生成单位后返回
        //单位性质类别
//        Map map3 = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{dzz.get(14)});
//        tDzzInfo.setSzdwlb(map3!=null ? map3.get("item_code").toString() : "");
        tDzzInfo.setSzdwlb(dzz.get(14));
        boolean pc = dzz.get(32).trim().contains("'");
        if(!pc){
            tDzzInfo.setParentcode(dzz.get(32).trim());
        }else{
            tDzzInfo.setParentcode(dzz.get(32).trim().substring(1,dzz.get(32).trim().length()-1));
        }
        //在主表中保存信息
        Map map4 = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{dzz.get(0).trim()});
        if(map4==null){
            generalSqlComponent.insert(SqlCode.saveDZZInfo, tDzzInfo);
        }else{
            generalSqlComponent.update(SqlCode.updateDZZInfo, tDzzInfo);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveDZZHjxxDzz(List<String> dzz, String organid) {
        Map param = new HashMap();
        String uuid = UUID.randomUUID().toString().replace("-","");
        param.put("uuid",uuid);
        param.put("organid",organid);
        param.put("organname",dzz.get(1));
        param.put("hjdate",!StringUtils.isEmpty(dzz.get(33)) ? dzz.get(33) + " 00:00:00": "");
        param.put("jmdate",!StringUtils.isEmpty(dzz.get(34)) ? dzz.get(34) + " 00:00:00": "");
        param.put("bjbzcsfs","1000000001");
        param.put("syncstatus","A");
        param.put("synctime",DateEnum.YYYYMMDDHHMMDD.format());
        param.put("zfbz",0);
        if(!StringUtils.isEmpty(dzz.get(34))){//届满日期不为空
            generalSqlComponent.delete(SqlCode.delDzzBzxxByOrganid, param);
            generalSqlComponent.insert(SqlCode.addDzzBzxxByOrganid, param);
        }
    }

    /**
     * 党组织信息保存
     * @param dzz
     * @param organid
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveDzzInfoSimple(List<String> dzz, String organid, TDzzInfo p, TDzzInfo parent) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", dzz.get(0).trim());
        //generalSqlComponent.delete(SqlCode.delDzzInfoSimpleByCode, paramMap);

        TDzzInfoSimple pold = generalSqlComponent.query(SqlCode.getDzzsjByCode, new Object[] {dzz.get(32).trim()});
        String idPath = pold == null ? "/"+organid+"/" : String.format("%s%s/", pold.getIdPath(), organid);

        TDzzInfoSimple tDzzInfoSimple = new TDzzInfoSimple();
        tDzzInfoSimple.setDelflag("0");
        tDzzInfoSimple.setOrgancode(dzz.get(0).trim());
        tDzzInfoSimple.setOrganid(organid);
        tDzzInfoSimple.setDzzmc(dzz.get(1));
        tDzzInfoSimple.setParentid(pold==null ? "0" : pold.getOrganid());
        //组织类别
        Map map = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
        tDzzInfoSimple.setZzlb(map!=null ? map.get("item_code").toString() : "");
        tDzzInfoSimple.setOperatetype("0");
        //单位隶属关系
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode3,new Object[]{dzz.get(3)});
        tDzzInfoSimple.setDzzlsgx(map1!=null ? map1.get("item_code").toString() : "");
//        tDzzInfoSimple.setDzzlsgx(dzz.get(3));
//        tDzzInfoSimple.setDzzsj(dzz.get(4));
        tDzzInfoSimple.setDzzsj("");
        tDzzInfoSimple.setParentcode(dzz.get(32).trim());
        tDzzInfoSimple.setIdPath(idPath);

        //设置路径
        if(pold !=null && pold.getPath1() != null) {
            tDzzInfoSimple.setPath1(pold.getPath1());
            if(pold.getOrganid() != pold.getPath1() && pold.getPath2() != null) {
                tDzzInfoSimple.setPath2(pold.getPath2());
                if(pold.getOrganid() != pold.getPath2() && pold.getPath3() != null) {
                    tDzzInfoSimple.setPath3(pold.getPath3());
                    if(pold.getOrganid() != pold.getPath3() && pold.getPath4() != null) {
                        tDzzInfoSimple.setPath4(pold.getPath4());
                        if(pold.getOrganid() != pold.getPath4() && pold.getPath5() != null) {
                            tDzzInfoSimple.setPath5(pold.getPath5());
                            if(pold.getOrganid() != pold.getPath5() && pold.getPath6() != null) {
                                tDzzInfoSimple.setPath6(pold.getPath6());
                                if(pold.getOrganid() != pold.getPath6() && pold.getPath7() != null) {
                                    tDzzInfoSimple.setPath7(pold.getPath7());
                                    if(pold.getOrganid() != pold.getPath7() && pold.getPath8() != null) {
                                        tDzzInfoSimple.setPath8(pold.getPath8());
                                        if(pold.getOrganid() != pold.getPath8() && pold.getPath9() != null) {
                                            tDzzInfoSimple.setPath9(pold.getPath9());
                                            if(pold.getOrganid() != pold.getPath9() && pold.getPath10() != null) {
                                                tDzzInfoSimple.setPath10(pold.getPath10());
                                            } else {
                                                tDzzInfoSimple.setPath10(organid);
                                            }
                                        } else {
                                            tDzzInfoSimple.setPath9(organid);
                                        }
                                    } else {
                                        tDzzInfoSimple.setPath8(organid);
                                    }
                                } else {
                                    tDzzInfoSimple.setPath7(organid);
                                }
                            } else {
                                tDzzInfoSimple.setPath6(organid);
                            }
                        } else {
                            tDzzInfoSimple.setPath5(organid);
                        }
                    } else {
                        tDzzInfoSimple.setPath4(organid);
                    }
                } else {
                    tDzzInfoSimple.setPath3(organid);
                }
            } else {
                tDzzInfoSimple.setPath2(organid);
            }

        }else{
            tDzzInfoSimple.setPath1(organid);
        }
        tDzzInfoSimple.setInsertDate(new Date());
        //在主表中保存信息
        Map map4 = generalSqlComponent.query(SqlCode.geTTDzzInfoimplBycode, new Object[]{dzz.get(0).trim()});
        if(map4==null){
            generalSqlComponent.insert(SqlCode.saveDzzsimple, tDzzInfoSimple);
        }else{
            generalSqlComponent.update(SqlCode.updateDZZsimpleInfo, tDzzInfoSimple);
        }
    }

    /**
     * 保存党组织补充信息
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveDZZBCInfo(List<String> list, String organid) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organid", organid);
        TDzzInfoBc old = generalSqlComponent.query(SqlCode.queryDzzInfoBcByOranid, paramMap);

        TDzzInfoBc tDzzInfoBc = new TDzzInfoBc();
        tDzzInfoBc.setOrganid(organid);
        tDzzInfoBc.setDzzsj("");
        tDzzInfoBc.setLxrname(list.get(5));
        tDzzInfoBc.setLxrphone(list.get(6));
        tDzzInfoBc.setDzzjlrq(list.get(7));
        tDzzInfoBc.setAddress(list.get(8));
        tDzzInfoBc.setFax(list.get(9));
        if(old == null) {
            generalSqlComponent.insert(SqlCode.saveDzzInfoBc, tDzzInfoBc);
        } else {
            generalSqlComponent.update(SqlCode.updateDzzInfoBc, tDzzInfoBc);
        }
    }

    /**
     * 保存党组织单位信息
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public String saveDZZDWInfo(List<String> list, String organid) {
        StringBuffer stringBuffer = new StringBuffer("");
        //单位名称
        if(StringUtils.isEmpty(list.get(1))) {
            return "";
        }
        String[] array = list.get(10).toString().split(",");
//        if(array.length > 0) {
//            for(int i = 0 ; i < array.length; i++) {
//                String uuid = UUID.randomUUID().toString().replace("-","");
//                stringBuffer.append(uuid).append(",");
//                TDwInfo tDwInfo = new TDwInfo();
//                tDwInfo.setOrganid(organid);
//                tDwInfo.setUuid(uuid);
//             //   tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMM.format());
//                tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
//                tDwInfo.setDwname(array[i]);
//                //组织性质类别
//                Map map = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{list.get(14)});
//                tDwInfo.setDwxzlb(map!=null ? map.get("item_code").toString() : "");
//                tDwInfo.setOperatetype("0");
//                generalSqlComponent.insert(SqlCode.saveDwInfo, tDwInfo);
//            }
//        }

        Map<String,Object> dwmap = generalSqlComponent.query(SqlCode.getTDwInfo,new Object[]{organid});
        if(dwmap==null){
            if(!StringUtils.isEmpty(list.get(10).toString())){
                String uuid = UUID.randomUUID().toString().replace("-","");
                stringBuffer.append(uuid).append(",");
                TDwInfo tDwInfo = new TDwInfo();
                tDwInfo.setOrganid(organid);
                tDwInfo.setUuid(uuid);
                //   tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMM.format());
                tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
                tDwInfo.setDwname(list.get(12));
                tDwInfo.setDwlsgx(list.get(13));
                //组织性质类别
                Map map = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{list.get(14)});
                tDwInfo.setDwxzlb(map!=null ? map.get("item_code").toString() : "");
                tDwInfo.setDwjldjczzqk(list.get(15));
                tDwInfo.setIsfrdw("是".equals(list.get(16).toString())?"1":"0");
                tDwInfo.setDwfzr(list.get(17));
                tDwInfo.setFzrsfsdy("是".equals(list.get(18).toString())?"1":"0");
                tDwInfo.setDwcode(list.get(19));
                tDwInfo.setJjlx(list.get(20));
                tDwInfo.setQygm(list.get(21));
                tDwInfo.setSfmykjqy("是".equals(list.get(22).toString())?"1":"0");
                tDwInfo.setZgzgs(list.get(25));
                tDwInfo.setZgzgzgrs(list.get(26));
                tDwInfo.setZgzyjsrys(list.get(27));
                tDwInfo.setSfjygh("是".equals(list.get(28).toString())?"1":"0");
                tDwInfo.setSfzjzz("是".equals(list.get(28).toString())?"1":"0");
                tDwInfo.setOperatetype("0");
                tDwInfo.setZfbz("0");
                tDwInfo.setSyncstatus("A");
                tDwInfo.setOperatetype("0");
                generalSqlComponent.insert(SqlCode.saveDwInfo2, tDwInfo);
                generalSqlComponent.insert(SqlCode.addTDwInfoBc, tDwInfo);
                stringBuffer.append(tDwInfo.getUuid());
            }
        }else{
            if(!StringUtils.isEmpty(list.get(10).toString())){
                TDwInfo tDwInfo = new TDwInfo();
                tDwInfo.setOrganid(organid);
                //   tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMM.format());
                tDwInfo.setUuid(dwmap.get("uuid").toString());
                tDwInfo.setDwname(list.get(12));
                tDwInfo.setDwlsgx(list.get(13));
                //组织性质类别
                Map map = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{list.get(14)});
                tDwInfo.setDwxzlb(map!=null ? map.get("item_code").toString() : "");
                tDwInfo.setDwjldjczzqk(list.get(15));
                tDwInfo.setIsfrdw("是".equals(list.get(16).toString())?"1":"0");
                tDwInfo.setDwfzr(list.get(17));
                tDwInfo.setFzrsfsdy("是".equals(list.get(18).toString())?"1":"0");
                tDwInfo.setDwcode(list.get(19));
                tDwInfo.setJjlx(list.get(20));
                tDwInfo.setQygm(list.get(21));
                tDwInfo.setSfmykjqy("是".equals(list.get(22).toString())?"1":"0");
                tDwInfo.setZgzgs(list.get(25));
                tDwInfo.setZgzgzgrs(list.get(26));
                tDwInfo.setZgzyjsrys(list.get(27));
                tDwInfo.setSfjygh("是".equals(list.get(28).toString())?"1":"0");
                tDwInfo.setSfzjzz("是".equals(list.get(28).toString())?"1":"0");
                generalSqlComponent.update(SqlCode.updateTDwInfo, tDwInfo);
                generalSqlComponent.update(SqlCode.updateTDwInfoBc, tDwInfo);
            }
        }



        return stringBuffer.toString();
    }

    /**
     * 保存的党组织到系统组织机构中
     * @param list
     * @param session
     */
    @Transactional(rollbackFor = Exception.class)
    public Long saveUrcOrg(List<String> list,Session session) {
        Map<String, Object> map = new HashMap<>();
        map.put("organcode", list.get(32));
        Map<String, Object> retMap = null;
        if(!"0.0".equals(list.get(32))){
            retMap = generalSqlComponent.query(SqlCode.queryPorgcode, map);
            if(retMap == null) {
                LehandException.throwException("上级党组织编码不存在，导入失败");
                return null;
            }
        }
        map.put("organcode", list.get(0));
        //generalSqlComponent.delete(SqlCode.delOrganByOrgancode, map);
        map.put("orgid", list.get(0).substring(1));
        map.put("orgcode", list.get(0));
        map.put("orgname", list.get(1));
        map.put("sorgname", list.get(1));
        map.put("porgid", retMap == null ? 0 : retMap.get("orgid"));
        map.put("orgseqno", 0L);
        map.put("compid", 1L);
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{list.get(2)});
        map.put("orgtypeid", map1!=null ? map1.get("item_code").toString() : "");
        map.put("type", 0);
        map.put("status", 1);
        map.put("orglevel", retMap==null?0:((int) retMap.get("orglevel")+1));
        map.put("createuserid", session.getUserid());
        map.put("createtime", DateEnum.parseDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
        map.put("remarks3","");
        Map map2 = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{list.get(0)});
        if(map2==null){
            map.put("orgpath", "");
            generalSqlComponent.insert(SqlCode.saveHornOrganization2, map);
            Map map4 = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{list.get(0)});
            String orgpath= String.valueOf(retMap.get("orgpath"));
            orgpath=orgpath+ map4.get("orgid")+"_";
            map.put("orgpath",orgpath);
            generalSqlComponent.update(SqlCode.updateHornOrganization, map);
            return Long.valueOf(map4.get("orgid").toString());
        }else{
            String orgpath= String.valueOf(retMap.get("orgpath"));
            orgpath=orgpath+ map2.get("orgid")+"_";
            map.put("orgpath",orgpath);
            generalSqlComponent.update(SqlCode.updateHornOrganization, map);
            return Long.valueOf(map2.get("orgid").toString());
        }

    }


    /**
     * @ 人员选择器中的成员接口
     */
    public List<Node> listAll(Session session, Long pid, String username, int type) {
        Long compid = session.getCompid();
        Long selfOrgid = session.getCurrorganid();
        if (!StringUtils.isEmpty(username)) {
            Map<String,Object> param = new HashMap<String,Object>(2);
            param.put("compid", compid);
            param.put("username", username);
            return generalSqlComponent.query(SqlCode.listUserNodeByUsername, param);
        }
        if(StringUtils.isEmpty(pid)) {
            return generalSqlComponent.query(SqlCode.listOrgan2, new Object[] {compid,selfOrgid});
            //generalSqlComponent.query(SqlCode.listOrgan, new Object[] {selfOrgid,compid,compid,selfOrgid});
        }else {
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("compid", compid);
            map.put("orgid", pid);
            if(type == 0) {
                return generalSqlComponent.query(SqlCode.listAllChildNode, map);
            } else {
                //不包含自己本级组织和相关信息
                return generalSqlComponent.query(SqlCode.listAllChildNodeUnder, map);
            }
        }
    }


    public int deleteBySubject(Long compid,Integer subjecttp, Long subjectid) {
        return generalSqlComponent.delete(SqlCode.delFelLeiMap, new Object[] {compid,subjecttp,subjectid});
    }

    /**
     * 数据授权
     */
    @Transactional(rollbackFor=Exception.class)
    public void saveAuthClass(Integer subjecttp,Long subjectid,String classids,Session session) throws Exception{
        //首先删除数据
        deleteBySubject(session.getCompid(),subjecttp, subjectid);
        //如果仅仅是修改就不管他
        if(!StringUtils.isEmpty(classids)){
            String[] newsubs = classids.split(",");
            if(newsubs!=null && newsubs.length>0) {
                for (String id : newsubs) {
                    DlType tkTaskClass = generalSqlComponent.query(SqlCode.getDlTypeByIdParty, new Object[] {session.getCompid(),Long.valueOf(id)});
                    String pid = "";
                    String orderid = "";
                    if(tkTaskClass!=null) {
                        pid=tkTaskClass.getPid().toString();
                        orderid = tkTaskClass.getOrderid().toString();
                    }
                    insertPwAuthOtherMap(session.getCompid(),subjecttp,subjectid,session.getUserid(),Long.valueOf(id),pid,orderid);
                }
            }
        }
    }

    public void insertPwAuthOtherMap(Long compid,Integer subjecttp, Long subjectid, Long userid, Long id,String pid,String orderid) {
        UrcAuthOtherMap pwAuthOtherMap = new UrcAuthOtherMap();
        pwAuthOtherMap.setCompid(compid);
        pwAuthOtherMap.setBizid(id);
        pwAuthOtherMap.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        pwAuthOtherMap.setCuserid(userid);
        pwAuthOtherMap.setModule(0);
        pwAuthOtherMap.setRemarks1(0);
        pwAuthOtherMap.setRemarks2(0);
        pwAuthOtherMap.setRemarks3(pid);
        pwAuthOtherMap.setRemarks4(orderid);
        pwAuthOtherMap.setSubjectid(subjectid);
        pwAuthOtherMap.setSubjecttp(subjecttp);
        generalSqlComponent.insert(SqlCode.saveFeiLeiAuth, pwAuthOtherMap);
    }

    /**
     * 查询已被授权的id集合
     */
    public List<Map<String,Object>> listIds(Long compid,Integer subjecttp,Long subjectid){
        if(subjecttp == 1) {//角色查询
            return generalSqlComponent.query(SqlCode.listFeiLeirole, new Object[] {compid,subjecttp,subjectid});
        }else {//用户
            return generalSqlComponent.query(SqlCode.listuserandrole, new Object[] {compid,compid,subjectid,compid,subjectid});
        }
    }

    /**
     * 工作台组织概况
     * @param organid
     * @param session
     * @return
     */
    public Map<String,Object> getOrgGeneral(String organid, Session session) {
        Map<String,Object> map = new HashMap<String,Object>();

        if(StringUtils.isEmpty(organid)){
           organid = generalSqlComponent.getDbComponent().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{session.getCurrentOrg().getOrgcode()});
        }
        if(StringUtils.isEmpty(organid)){
            LehandException.throwException("必传参数组织id不能为空");
        }
        String path = baseDzzTotalService.getPath(organid);

        map.put("dzzMap",getDzzCountMap(organid,path,session));

        map.put("ageMap",getAgeMap(organid));

        map.put("sexMap",getSexList(path,organid,session));

        return map;
    }

    /**
     * 性别分布饼状图
     * @param path
     * @param organid
     * @param session
     * @return
     */
    private List<Map<String,Object>> getSexList(String path, String organid, Session session) {

        List<Map<String,Object>> sexList = new ArrayList<Map<String,Object>>(2);

        //党员性别统计
        Long maleCount = generalSqlComponent.query(SqlCode.statisticsMemberBySex,new HashMap(){{put("path",path);put("organid",organid);put("xb", SysConstants.DYXB.MALE);}});
        Long femaleCount = generalSqlComponent.query(SqlCode.statisticsMemberBySex,new HashMap(){{put("path",path);put("organid",organid);put("xb", SysConstants.DYXB.FEMALE);}});

        //男性党员
        Map<String,Object> maleMap = new HashMap(){{put("name","男性党员");put("value",maleCount);}};
        sexList.add(maleMap);

        //女性党员
        Map<String,Object> femaleMap = new HashMap(){{put("name","女性党员");put("value",femaleCount);}};
        sexList.add(femaleMap);
        return sexList;
    }

    /**
     * 年龄分布的柱状图
     * @param organid
     * @return
     */
    private Map<String, Object> getAgeMap(String organid) {

        //党员年龄统计
        List<Map<String,Object>> ageList = baseDzzTotalService.getAgeCount(organid);

        //年龄数据对象
        Map<String,Object> ageMap = new HashMap<String,Object>(2);
        List<String> nameList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();

        for (Map<String,Object> age:ageList) {
            nameList.add(String.valueOf(age.get("name")));
            valueList.add(String.valueOf(age.get("value")));
        }
        ageMap.put("nameList",nameList);
        ageMap.put("valueList",valueList);

        return ageMap;
    }

    /**
     * 各类型组织个数统计
     * @param organid
     * @param path
     * @param session
     * @return
     */
    private Map<String, Object> getDzzCountMap(String organid,String path, Session session) {

        //各类型组织个数统计
        Long count = 0L;  		//党组织总数
        Long dwCount = 0L;  	//党委总数
        Long dzzCount = 0L; 	//党总支总数
        Long dzbCount = 0L; 	//党支部总数
        Long lhdzbCount = 0L;   //联合党支部总数
        Long dxzCount = 0L;     //党小组总数

        dwCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb", SysConstants.ZZLB.DW);put("path",path);put("organid",organid);}});
        dzzCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb",SysConstants.ZZLB.DZZ);put("path",path);put("organid",organid);}});
        dzbCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb",SysConstants.ZZLB.DZB);put("path",path);put("organid",organid);}});
        lhdzbCount = generalSqlComponent.query(SqlCode.getDzzCountByZZLB,
                new HashMap(){{put("zzlb",SysConstants.ZZLB.LHDZB);put("path",path);put("organid",organid);}});
        //党小组
        dxzCount = generalSqlComponent.query(SqlCode.getDxzCountByOrganidnew, new HashMap(){{put("path",path);put("organid",organid);}});

        //党组织总数
        count = dwCount + dzzCount + dzbCount + lhdzbCount;

        //组织机构数统计对象
        Map<String,Object> dzzMap = new HashMap<String,Object>(5);

        dzzMap.put("count",count);
        dzzMap.put("dwCount",dwCount);
        dzzMap.put("dzzCount",dzzCount);
        dzzMap.put("dzbCount",dzbCount+lhdzbCount);
        dzzMap.put("dxzCount",dxzCount);

        return dzzMap;
    }

    public Map<String,Object> getBranchGeneral(String organid, Session session) {
        Map<String,Object> map = new HashMap<String,Object>(6);

        //党员人数
        Long mcount = generalSqlComponent.query(SqlCode.getMemberCountByOrganid,new Object[]{organid});
        map.put("mcount",mcount);

        //支部书记
        TDzzInfoBc infoBc = generalSqlComponent.query(SqlCode.getDzzsjById,new Object[]{organid});
        map.put("secretary",infoBc ==null?MagicConstant.STR.EMPTY_STR:infoBc.getDzzsj());
        //党组织成立时间
        map.put("creationdate",infoBc ==null?MagicConstant.STR.EMPTY_STR:infoBc.getDzzjlrq());

        //班子成员数
        Long tmcount = generalSqlComponent.query(SqlCode.getBzcyCountByOrganid,new Object[]{organid,0});
        map.put("tmcount",tmcount);

        //上届换届时间
        Map<String,Object> hjxx = generalSqlComponent.query(SqlCode.getLatestHjxxByOrganid,new Object[]{organid});
        map.put("hjdate",hjxx==null?"":hjxx.get("hjdate"));

        //班子任期
        Object hjdate = hjxx==null? MagicConstant.STR.EMPTY_STR:hjxx.get("hjdate");
        Object jmdate = hjxx==null? MagicConstant.STR.EMPTY_STR:hjxx.get("jmdate");
        if(StringUtils.isEmpty(hjdate)||StringUtils.isEmpty(jmdate)){
            map.put("term",MagicConstant.STR.EMPTY_STR);
            return map;
        }
        //计算任期
        String hjyear = String.valueOf(hjdate).substring(0,4);
        String jmyear = String.valueOf(jmdate).substring(0,4);

        int term = Integer.parseInt(jmyear)-Integer.parseInt(hjyear);
        map.put("term",term);

        return map;
    }

    public Map<String,Object> getActiveSituation(int type,Session session) {
        Map<String,Object> map = new HashMap<String,Object>(3);
        Date date = new Date();
        //总用户数
        Integer totaluserCounts = generalSqlComponent.query(
                SqlCode.getUserCountByDate,  new HashMap<String, Object>(){
                    {
                        put("compid", session.getCompid());
                        put("enddate", date);
                    }});

        //活跃用户数
        String days = DateEnum.parseDate(date,"YYYY-MM-dd");
        String months = DateEnum.parseDate(date,"YYYY-MM");
        Integer activeCount = 0;

        //按天统计
        if(type == 0){
            activeCount =  generalSqlComponent.query(SqlCode.getActiveUserCountByDay, new HashMap<String, Object>(){
                {
                    put("compid", session.getCompid());
                    put("accessdate", days);
                }});
        }

        //按月统计
        if(type == 1){
            String monthStart = months+"-00";
            String monthEnd = months +"-31";
            activeCount = generalSqlComponent.query(SqlCode.getActiveUserCountByMonth,new HashMap<String, Object>(){
                {
                    put("compid", session.getCompid());
                    put("monthStart", monthStart);
                    put("monthEnd", monthEnd);
                }});
        }

        Integer unactiveCount = totaluserCounts - activeCount;
        map.put("totaluserCounts",totaluserCounts);
        map.put("activeCount",activeCount);
        map.put("unactiveCount",unactiveCount);

        return map;
    }

    /**
     * 删除班子成员
     * @param userid
     * @param session
     */
    @Transactional(rollbackFor=Exception.class)
    public void delBzcyInfo(String userid, String organid,Session session) {
        //先查询作废的是不是书记如果是则执行下面程序
        Map mapBzcy = getTDzzBzcyxx(userid,organid);
        Map mapDzz = getTDzzInfo2(organid);
        generalSqlComponent.update(SqlCode.modiBzcyxx,new Object[]{userid,mapDzz.get("organid")});
        Map mapDy = getTDyInfo(userid);
        Map mapBzcy2 = getTDzzBzcyxx(userid,organid);
        if(mapBzcy2==null){//避免同一个人担任不同组织的书记，然后一个组织书记职务被撤销时不会影响urc_user表中的职位
            generalSqlComponent.update(SqlCode.modiUrcUserByIdno,new Object[]{0,mapDy.get("zjhm")});
        }
        if(SysConstants.ZZLB.DWSJ.equals(mapBzcy.get("dnzwname").toString())
                ||SysConstants.ZZLB.DZZSJ.equals(mapBzcy.get("dnzwname").toString())
                ||SysConstants.ZZLB.DZBSJ.equals(mapBzcy.get("dnzwname").toString())){
            //t_dzz_info表，dzzsj字段；
            generalSqlComponent.update(SqlCode.modiTDzzInfo,new Object[]{"",mapDzz.get("organid")});
            //t_dzz_info_bc表，dzzsj字段；
            generalSqlComponent.update(SqlCode.modiTDzzInfoBc,new Object[]{"",mapDzz.get("organid")});
            //t_dzz_info_simple表，dzzsj字段；
            generalSqlComponent.update(SqlCode.modiTDzzInfoSimple,new Object[]{"",mapDzz.get("organid")});
            //删除角色权限
            delUrcUserRole(mapDy.get("zjhm").toString(), mapDzz.get("organid").toString(),session);
        }
    }

    private Map getTDzzInfo(Session session) {
        return generalSqlComponent.query(SqlCode.getOrganidByOrgancode,new Object[]{session.getCurrentOrg().getOrgcode()});
    }
    private Map getTDzzInfo2(String organid) {
        return generalSqlComponent.query(SqlCode.getDzzInfo,new Object[]{organid});
    }


    private Map getTDyInfo(String userid) {
        return generalSqlComponent.query(SqlCode.getIdnoByUserid,new Object[]{userid});
    }

    private Map getTDzzBzcyxx(String userid,String organid) {
        return generalSqlComponent.query(SqlCode.getDnzwnameByUserid,new Object[]{userid,organid});
    }
    /**
     * 修改班子成员信息
     * @param userid
     * @param dnzw
     * @param zwsm
     * @param rzrq
     * @param lzrq
     */
    @Transactional(rollbackFor=Exception.class)
    public void modiBzcyInfo(String userid,String username,String dnzw, String zwsm, String rzrq,String lzrq,
                             String csrq,String lxdh,String xl,String idno,String xb,
                             String organid,Session session) throws ParseException {
        //先查询班子成员表中是否存在，不存在就新增，存在就修改
        Map map = generalSqlComponent.query(SqlCode.getBzcyxx,new Object[]{userid,organid});
        //书记职务只能有一个
        String strs = "2100000005,3100000009,4100000013,5100000017,7100000028,6100000033";
        if(strs.contains(dnzw)){
            //判断当前组织是否已经有了该书记的职务
            Map map2 = generalSqlComponent.query(SqlCode.getBzcyxx2,new Object[]{organid,dnzw});
            if(map2!=null && !userid.equals(map2.get("userid").toString())){
                LehandException.throwException("当前党组织班子成员中已经有了"+partyCacheComponent.getName("d_dy_dnzw",dnzw));
            }
        }
        if(map==null){
            addBzcyxx(username,userid,organid,idno,dnzw,xb,csrq,lxdh,xl,zwsm,rzrq,lzrq,session.getCurrentOrg().getOrgname());
            //添加到书记角色
            addRoleMap(idno, organid, dnzw,session);
        }else{
            //判断修改前职务是不是书记  党委书记，党总支书记 党支部书记
            updateRoleMap(idno, organid, dnzw,map,session);
            int ordernum = getOrdernum(dnzw);
            generalSqlComponent.update(SqlCode.modiBzcyxx2,new Object[]{dnzw,partyCacheComponent.getName("d_dy_dnzw",dnzw),zwsm,rzrq,lzrq,ordernum,userid,organid});
        }
        modiRelationTableInfos(username, dnzw, idno, organid, session,map);


    }

    private void modiRelationTableInfos(String username, String dnzw, String idno, String organid, Session session,Map map) {
        //更新党组织表中的机构书记字段
        Map map2 = getTDzzInfo2(organid);
        boolean zzlb = dangwei.equals(map2.get("zzlb").toString());
        boolean zzlb1 = dangzongzhi.equals(map2.get("zzlb").toString());
        boolean zzlb2 = dangzhibu.equals(map2.get("zzlb").toString());
        //判断修改前职务是不是书记  党委书记，党总支书记 党支部书记
        String strs2 = "3100000009,4100000013,5100000017";
        if(map!=null&&strs2.contains(map.get("dnzwname").toString())){
            if((SysConstants.ZZLB.DWSJ.equals(dnzw) && zzlb)||(SysConstants.ZZLB.DZZSJ.equals(dnzw) && zzlb1)||(SysConstants.ZZLB.DZBSJ.equals(dnzw) && zzlb2)){
            }else{
                //修改之前是书记  修改之后不是书记 删除urc_role_map
                //t_dzz_info表，dzzsj字段；
                generalSqlComponent.update(SqlCode.modiTDzzInfo,new Object[]{"",organid});
                //urc_user表，postname字段
                generalSqlComponent.update(SqlCode.modiUrcUserByIdno,new Object[]{0,idno});
                //t_dzz_info_bc表，dzzsj字段；
                generalSqlComponent.update(SqlCode.modiTDzzInfoBc,new Object[]{"",organid});
                //t_dzz_info_simple表，dzzsj字段；
                generalSqlComponent.update(SqlCode.modiTDzzInfoSimple,new Object[]{"",organid});
            }
        }
        if((SysConstants.ZZLB.DWSJ.equals(dnzw) && zzlb)
            ||(SysConstants.ZZLB.DZZSJ.equals(dnzw) && zzlb1)
            ||(SysConstants.ZZLB.DZBSJ.equals(dnzw) && zzlb2)){
            //t_dzz_info表，dzzsj字段；
            generalSqlComponent.update(SqlCode.modiTDzzInfo,new Object[]{username,organid});
            //urc_user表，postname字段
            generalSqlComponent.update(SqlCode.modiUrcUserByIdno,new Object[]{1,idno});
            //t_dzz_info_bc表，dzzsj字段；
            generalSqlComponent.update(SqlCode.modiTDzzInfoBc,new Object[]{username,organid});
            //t_dzz_info_simple表，dzzsj字段；
            generalSqlComponent.update(SqlCode.modiTDzzInfoSimple,new Object[]{username,organid});

        }else if((SysConstants.ZZLB.DWFSJ.equals(dnzw) && zzlb)
                ||(SysConstants.ZZLB.DZZFSJ.equals(dnzw) && zzlb1)
                ||(SysConstants.ZZLB.DZBFSJ.equals(dnzw) && zzlb2)){
            generalSqlComponent.update(SqlCode.modiUrcUserByIdno,new Object[]{2,idno});
        }else if((SysConstants.ZZLB.DWWY.equals(dnzw) && zzlb)
                ||(SysConstants.ZZLB.DZZWY.equals(dnzw) && zzlb1)
                ||(SysConstants.ZZLB.DZBWY.equals(dnzw) && zzlb2)){
            generalSqlComponent.update(SqlCode.modiUrcUserByIdno,new Object[]{3,idno});
        }
    }

    private void addRoleMap(String idno, String organid,String dnzw,Session session) {
        //查询书记角色是否存在
        Map<String,Object> user2 = generalSqlComponent.query(SqlCode.getRoleOrg,new Object[]{0,7,organid});
        if(user2==null){
            delUrcUserRole(idno, organid,session);
        }
        Map map2 = getTDzzInfo2(organid);
        boolean zzlb = dangwei.equals(map2.get("zzlb").toString());
        boolean zzlb1 = dangzongzhi.equals(map2.get("zzlb").toString());
        boolean zzlb2 = dangzhibu.equals(map2.get("zzlb").toString());
        if((SysConstants.ZZLB.DWSJ.equals(dnzw) && zzlb)||(SysConstants.ZZLB.DZZSJ.equals(dnzw) && zzlb1)||(SysConstants.ZZLB.DZBSJ.equals(dnzw) && zzlb2)){
            Map<String,Object> user = generalSqlComponent.query(SqlCode.queryUrcUserByIdno,new Object[]{idno});
            Map<String,Object>  userorg = generalSqlComponent.query(SqlCode.getorg,new Object[]{organid});
            generalSqlComponent.insert(SqlCode.addUrcRoleMap,new Object[]{0,user.get("userid"),7, session.getCompid(),userorg.get("orgid"),new Date()});
        }

    }

    private void updateRoleMap(String idno, String organid,String dnzw,Map map,Session session) {
        //查询班子成员
        //判断修改前职务是不是书记  党委书记，党总支书记 党支部书记
        String strs2 = "3100000009,4100000013,5100000017";
        if(strs2.contains(map.get("dnzwname").toString())){
            Map map2 = getTDzzInfo2(organid);
            boolean zzlb = dangwei.equals(map2.get("zzlb").toString());
            boolean zzlb1 = dangzongzhi.equals(map2.get("zzlb").toString());
            boolean zzlb2 = dangzhibu.equals(map2.get("zzlb").toString());
            if((SysConstants.ZZLB.DWSJ.equals(dnzw) && zzlb)||(SysConstants.ZZLB.DZZSJ.equals(dnzw) && zzlb1)||(SysConstants.ZZLB.DZBSJ.equals(dnzw) && zzlb2)){
            }else{
                //修改之前是书记  修改之后不是书记 删除urc_role_map
                delUrcUserRole(idno, organid,session);
            }
        }else{
            //修改之前不是书记  添加到书记角色
            addRoleMap(idno, organid, dnzw,session);
        }


    }

    private void delUrcUserRole(String idno, String organid,Session session) {
        Map<String,Object> user = generalSqlComponent.query(SqlCode.queryUrcUserByIdno,new Object[]{idno});
        Map<String,Object>  userorg = generalSqlComponent.query(SqlCode.getorg,new Object[]{organid});
        generalSqlComponent.delete(SqlCode.delUrcUserMap,new Object[]{user.get("userid"),userorg.get("orgid"),7,session.getCompid()});
    }

    private void addBzcyxx(String username, String userId, String organid, String idno, String postname,
                           String sex, String csrq,String lxdh,String xl,String dnzwsm,String rzrq,String lzrq,
                            String orgname) throws ParseException {
        int ordernum = getOrdernum(postname);
        Map<String,Object> param2 = new HashMap<>();
        param2.put("uuid", UUID.randomUUID().toString().replaceAll("-", ""));
        param2.put("userid", userId);
        param2.put("organid", organid);
        param2.put("ordernum", ordernum);
        param2.put("username", username);
        param2.put("zjhm", idno);
        param2.put("xb", sex);
        param2.put("csrq", csrq);
        param2.put("lxdh", lxdh);
        param2.put("xl", xl);
        param2.put("rzdate", rzrq);
        param2.put("lzdate", lzrq);
        param2.put("dnzwname", postname);
        param2.put("dnzwsm", dnzwsm);
        param2.put("dnzwmc", partyCacheComponent.getName("d_dy_dnzw",postname));
        param2.put("sfzr", 1);
        param2.put("organname", orgname);
        param2.put("syncstatus", "A");
        param2.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
        param2.put("zfbz", 0);
        generalSqlComponent.insert(SqlCode.addTDzzBzcyxx2,param2);
    }

    private int getOrdernum(String postname) {
        int ordernum = 0;
        if(SysConstants.ZZLB.DZBSJ.equals(postname) || SysConstants.ZZLB.DWSJ.equals(postname) || SysConstants.ZZLB.DZZSJ.equals(postname)){
            ordernum = 0;
        }else if(SysConstants.ZZLB.DZBFSJ.equals(postname) || SysConstants.ZZLB.DWFSJ.equals(postname) || SysConstants.ZZLB.DZZFSJ.equals(postname)){
            ordernum = 1;
        }else if(SysConstants.ZZLB.DZBWY.equals(postname) || SysConstants.ZZLB.DZZWY.equals(postname) || SysConstants.ZZLB.DWWY.equals(postname)){
            ordernum = 2;
        }else{
            ordernum = 3;
        }
        return ordernum;
    }

    /**
     * 下级支部列表
     * @param likeStr
     * @param session
     * @return
     */
    public List<Map<String,Object>> getUnderBranchList(String likeStr,Session session) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organid", session.getCurrentOrg().getOrgcode());
        //查询 id_path
        TDzzInfoSimple tDzzInfoSimple = generalSqlComponent.query(SqlCode.queryTdzzInfoSimple, paramMap);
        //查询下级组织
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(tDzzInfoSimple.getIdPath()).append("%");
        paramMap.put("idPath", stringBuffer.toString());
        paramMap.put("zzlb", SysConstants.ZZLB.DZB);
        paramMap.put("likeStr", likeStr);
        List<Map<String, Object>> records = generalSqlComponent.query(SqlCode.queryTdzzByIdPathAndZzlb, paramMap);
        return records;
    }

    /**
     * 工作巡查支部情况
     * @param organid
     * @param session
     * @return
     */
    public Map<String,Object> getMemberListByPost(String organid,Session session) {
        Map<String,Object>  result = new HashMap<String,Object>(3);

        //书记
        Map<String,Object> secretary = new HashMap<String,Object>(3);
        secretary.put("postname","支部书记");
        secretary.put("post","5100000017");
        secretary.put("names",getMemberNamesByPost(organid,"5100000017"));
        result.put("secretary",secretary);

        //副书记
        Map<String,Object> dsecretary = new HashMap<String,Object>(3);
        dsecretary.put("postname","支部副书记");
        dsecretary.put("post","5200000018");
        dsecretary.put("names",getMemberNamesByPost(organid,"5200000018"));
        result.put("dsecretary",dsecretary);

        //委员
        Map<String,Object> cm = new HashMap<String,Object>(3);
        cm.put("postname","党支部委员");
        cm.put("post","5300000022");
        cm.put("names",getMemberNamesByPost(organid,"5300000022"));
        result.put("cm",cm);
        return result;
    }

    /**
     * 获取党内职务
     * @param organid
     * @param post
     * @return
     */
    private String getMemberNamesByPost(String organid, String post) {
        List<String> list = generalSqlComponent.query(SqlCode.getBzcyNamesByPost,new Object[]{organid,post});
        if(list.size()<1){
            return MagicConstant.STR.EMPTY_STR;
        }
        StringBuffer buffer = new StringBuffer();
        int k = list.size();
        for(int i= 0;i<k;i++){
            buffer.append(list.get(i));
            if(i==k-1){
                continue;
            }
            buffer.append(";");
        }
        return buffer.toString();
    }

    /**
     * 换届信息,头部
     * @param organid
     * @param session
     * @return
     */
    public Map<String,Object> getElecInfoByOrg(String organid, Session session) {
        Map<String,Object> result = new HashMap<String,Object>();
        String path = baseDzzTotalService.getPath(organid);
        long normalnum = getElectionNum(MagicConstant.NUM_STR.NUM_1,organid,path);
        long delaynum = getElectionNum(MagicConstant.NUM_STR.NUM_0,organid,path);
        Map<String,Object> normal = new HashMap<String,Object>();
        normal.put("status",1);
        normal.put("num",normalnum);
        normal.put("name","正常换届");
        Map<String,Object> delay = new HashMap<String,Object>();
        delay.put("status",0);
        delay.put("num",delaynum);
        delay.put("name","逾期换届");
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        list.add(normal);
        list.add(delay);
        result.put("list",list);
        return result;
    }

    /**
     * 换届数量
     * @param status
     * @param organid
     * @return
     */
    private long getElectionNum(String status, String organid,String path) {
        long count = generalSqlComponent.query(SqlCode.getHjxxCount,new HashMap(){{put("status",status);put("organid",organid);put("path",path);}});
        return count;
    }

    public Pager pageElection(Pager pager,String organid, String status,Session session) {
        String path = baseDzzTotalService.getPath(organid);
        return generalSqlComponent.pageQuery(SqlCode.pageHjxxInfo,new HashMap(){{put("status",status);put("organid",organid);put("path",path);}},pager);
    }

    /**
     * 换届统计导出
     * req
     * @param session
     */
    public String exportElectionExcel(HttpServletResponse response, ElectionExportReq req, Session session){
        Pager pager = new Pager();
        pager.setPageNo(1);
        pager.setPageSize(9999);
        pager = pageElection(pager,req.getOrganid(),req.getStatus(),session);
        List<Map<String,Object>> list = (List<Map<String, Object>>) pager.getRows();
        List<Map<String,String>> data = new ArrayList<Map<String,String>>();
        if(list.size()>0){
            for (Map<String,Object> map:list) {
                Map<String,String> rs = new LinkedHashMap<String,String>(3);
                rs.put("组织名称",String.valueOf(map.get("organname")));
                rs.put("班子当选日期",String.valueOf(map.get("hjdate")).replace("00:00:00",""));
                rs.put("本届届满日期",String.valueOf(map.get("jmdate")).replace("00:00:00",""));
                rs.put("换届状态",String.valueOf(map.get("delayflag")).equals(MagicConstant.NUM_STR.NUM_1)?"正常":"逾期");
                data.add(rs);
            }
        }
        ExcelUtils.write(response,req.getTablesStr(),data,session.getCurrentOrg().getOrgname()+"下级组织换届信息");
        return null;
    }

    /**
     * 书记花名册新增/编辑
     * @param tsid
     */
    public void addTSecretaryInfo(TSecretaryInfoDao tsid, Session session) {
        tsid.setOrgancode(session.getCurrentOrg().getOrgcode());
        //Map<String,Object> data = generalSqlComponent.query(SqlCode.getTSecretaryInfo,new Object[]{tsid.getUserid()});
        if(tsid.getId()==null){
            generalSqlComponent.insert(SqlCode.addTSecretaryInfo,tsid);
        }else{
            generalSqlComponent.update(SqlCode.modiTSecretaryInfo,tsid);
        }
    }

    /**
     * 书记花名册查询
     * @param pager
     * @param status
     */
    public void pageTSecretaryInfo(Pager pager, Integer status, Session session) {
        Map param = new HashMap();
        param.put("status",status);
        param.put("organcode",session.getCurrentOrg().getOrgcode());
        Map map = generalSqlComponent.query(SqlCode.getOrganidByOrgancode,new Object[]{param.get("organcode")});
        param.put("organid",map.get("organid"));
        if(dangwei.equals(session.getCurrentOrg().getOrgtypeid().toString())){
            param.put("type",1);
        }else if(dangzongzhi.equals(session.getCurrentOrg().getOrgtypeid().toString())){
            param.put("type",2);
        }else{
            param.put("type",3);
        }
        generalSqlComponent.pageQuery(SqlCode.pageTSecretaryInfo,param,pager);
    }

    /**
     * 书记花名册详情
     * @param session
     * @return
     */
    public Map<String,Object> getTSecretaryInfo(Long id,String userid,Session session) {
        //Map map1 = generalSqlComponent.query(SqlCode.getOrganidByOrgancode,new Object[]{session.getCurrentOrg().getOrgcode()});
        Map map = generalSqlComponent.query(SqlCode.getTSecretaryInfo2,new Object[]{/*map1.get("organid"),*/id,userid});
        map.put("xlName", partyCacheComponent.getName("d_dy_xl",map.get("xl")));
        map.put("dnzwnameName", partyCacheComponent.getName("d_dy_dnzw", map.get("dnzwname")));
        map.put("zwlevelName", partyCacheComponent.getName("d_dzz_zwjb", map.get("zwlevel")));
        return map;
    }

    /**
     * 书记花名册删除
     * @param session
     */
    public void delTSecretaryInfo(Long id, Session session) {
        generalSqlComponent.update(SqlCode.modiTSecretaryInfo2,new Object[]{0,id});
    }

    public void pageQueryTSecretaryInfo(Pager pager, String organid, String type,String name, Session session) {
        Map param = new HashMap();
        String path = orgUtil.getPath(organid);
        param.put("path",path);
        param.put("organid",organid);
        if("all".equals(type)){
            param.put("type",1);
        }else if("dw".equals(type)){
            param.put("type",2);
        }else if("dzz".equals(type)){
            param.put("type",3);
        }else{
            param.put("type",4);
        }
        param.put("name",name);
        generalSqlComponent.pageQuery(SqlCode.listChildOrgan2,param,pager);

    }

    /**
     * 组织机构书展示
     * @param organcode
     * @param session
     * @return
     */
    public Map<String,Object> orgTrees(String organcode, Session session) {
        if(StringUtils.isEmpty(organcode)){ organcode = rootNodeCode;}
        Map<String,Object> map = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{organcode});
        List<Map<String,Object>> list = generalSqlComponent.query(SqlCode.listChildOrgan3,new Object[]{map.get("orgid")});
        Map<String,Object> data = new HashMap<>(2);
        data.put("name",map.get("sorgname"));
        data.put("children",list);
        return data;
    }

    /**
     * 登录日志
     * @param pager
     * @param startime
     * @param endtime
     * @param session
     */
    public void loginLog(Pager pager, String startime, String endtime,String organid, Session session) {
        Map param = new HashMap();
        String path = orgUtil.getPath(organid);
        param.put("path",path);
        param.put("organid",organid);
        param.put("startime",startime);
        param.put("endtime",endtime);
        generalSqlComponent.pageQuery(SqlCode.listSysLogPlay2,param,pager);
    }


    @Transactional(rollbackFor = Exception.class)
    public boolean addTDzzinfo(TDzzInfoSaveReq bean, Session session){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //党组织信息
        TDzzInfoSimpleReq tdzzinfo = bean.getTdzzinfo();
        //单位信息
        TDwInfoReq tdwinfo = bean.getTdwinfo();

//        检查orgcode是否已存在
        if (vericode(tdzzinfo.getOrgancode())!=null){
            LehandException.throwException("党组织"+tdzzinfo.getOrgancode()+"编码已存在!!");
        }

        tdzzinfo.setOrganid(UUID.randomUUID().toString().replaceAll("-", ""));
        tdzzinfo.setCreator(session.getUserid().toString());
        tdzzinfo.setOrgantype(null);
        tdzzinfo.setOrdernum(tdzzinfo.getOrdernum()+"00");
        tdzzinfo.setXxwzd(null);
        tdzzinfo.setIsfunctional(null);
        tdzzinfo.setOperatetype("0");
        tdzzinfo.setSfxxwz(null);
        tdzzinfo.setProvincecode(tdzzinfo.getOrgancode().substring(0,3));
        tdzzinfo.setDzzsjzjhm(null);
        tdzzinfo.setKzpyrq(null);
        tdzzinfo.setKzdqpybs(null);
        tdzzinfo.setCjpydys(null);
        tdzzinfo.setKzpthydws(null);
        tdzzinfo.setCjptdys(null);
        tdzzinfo.setBjbzcsfs(null);
        tdzzinfo.setSfxxwz(null);
        tdzzinfo.setActivitypositions(bean.getTdzzinfo().getActivitypositions());
        tdzzinfo.setExpirationdate(!StringUtils.isEmpty(tdzzinfo.getExpirationdate())?tdzzinfo.getExpirationdate().substring(0,10)+" 00:00:00":tdzzinfo.getExpirationdate());
        //查询urc_organization 创建人所在组织
        Map<String,Object> organization = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{session.getCurrentOrg().getOrgcode()});
        //查询父组织t_dzz_info_simple
        Map<String,Object> pidtdzzinfo = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleOrgid,new Object[]{tdzzinfo.getParentid()});

        String maindwid = null;
        if(tdwinfo!=null){
            //插入t_dw_info
            maindwid = addTDwInfo(session,tdwinfo,tdzzinfo,sdf);
            tdzzinfo.setMaindwid(maindwid);
            //插入t_dw_info_bc
            tdwinfo.setUuid(maindwid);
            addTDwInfoBc(session,tdwinfo,tdzzinfo,sdf);
        }

        //插入 t_dzz_info
        addTDzzinfo(session, sdf, tdzzinfo, organization);
        if(pidtdzzinfo!=null){
            //插入 t_dzz_info_simple
            addTDzzinfoSimple(session, sdf, tdzzinfo, organization,pidtdzzinfo);
        }
        //插入t_dzz_info_bc
        addTDzzinfoBc(session, sdf, tdzzinfo, organization);
        //插入urc_user
        long userid = addUrcUser(session, sdf, tdzzinfo);
        //插入urc_organization
        tdzzinfo.setRemarks3(String.valueOf(userid));
        long oranizationid = addUrcOrganization(session, sdf, tdzzinfo,pidtdzzinfo);
        // 插入t_dzz_hjxx_dzz
        addTDzzHjxxDzz(session, sdf, tdzzinfo);

        //插入urc_user_account
        addUrcUserAccount(tdzzinfo,userid);
        //插入urc_user_org_map
        addUrcUserOrgMap(tdzzinfo,session,userid,oranizationid);



        //给组织账号添加相应的菜单权限
//        Map map12 = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
//        String orgcode2 = map1!=null ? map1.get("item_code").toString() : "";
        Map map = null;
        if(dangwei.equals(tdzzinfo.getZzlb())){
            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.PARTY_COMMITTEE_ROLE});
        }else if(dangzongzhi.equals(tdzzinfo.getZzlb())){
            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.GENERAL_PARTY_BRANCH_ROLE});
        }else{
            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.PARTY_BRANCH_ROLE});
        }
        generalSqlComponent.delete(SqlCode.removeUrcRoleMap,new Object[]{0,userid,map.get("roleid"),session.getCompid()});
        generalSqlComponent.insert(SqlCode.addUrcRoleMap,new Object[]{0,userid,map.get("roleid"),session.getCompid(),oranizationid,DateEnum.YYYYMMDDHHMMDD.format()});


        new Thread(()-> cacheService.refCache()).start();
        return true;
    }

    private TDzzInfoSimple vericode(String orgcode) {
        TDzzInfoSimple  dzzdata = generalSqlComponent.query(SqlCode.getDzzsjByCode,new Object[]{orgcode});
        if(dzzdata!=null){
//            LehandException.throwException("编码："+ orgcode+"组织已存在");
            return dzzdata;
        }
        return null;
    }

    private void addTDzzinfo(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String, Object> organization) {
        tdzzinfo.setOperorganid(organization.get("orgid").toString());
        tdzzinfo.setUpdator(session.getUserid().toString());
        tdzzinfo.setCreatetime(sdf.format(new Date()));
        tdzzinfo.setOperatorname(organization.get("orgname").toString());
        tdzzinfo.setSyncstatus("A");
        tdzzinfo.setDelflag("0");
        generalSqlComponent.insert(SqlCode.saveDZZInfo, tdzzinfo);
    }
    private void addTDzzinfoSimple(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String, Object> organization, Map<String,Object> pidtdzzinfo) {
        tdzzinfo.setIdPath(pidtdzzinfo.get("id_path")+tdzzinfo.getOrganid()+"/");
        String[] path =tdzzinfo.getIdPath().split("/");
        Integer num = path.length-1;
        tdzzinfo.setPath1(num<1?null:(num==1?tdzzinfo.getOrganid():pidtdzzinfo.get("path1").toString()));
        tdzzinfo.setPath2(num<2?null:(num==2?tdzzinfo.getOrganid():pidtdzzinfo.get("path2").toString()));
        tdzzinfo.setPath3(num<3?null:(num==3?tdzzinfo.getOrganid():pidtdzzinfo.get("path3").toString()));
        tdzzinfo.setPath4(num<4?null:(num==4?tdzzinfo.getOrganid():pidtdzzinfo.get("path4").toString()));
        tdzzinfo.setPath5(num<5?null:(num==5?tdzzinfo.getOrganid():pidtdzzinfo.get("path5").toString()));
        tdzzinfo.setPath6(num<6?null:(num==6?tdzzinfo.getOrganid():pidtdzzinfo.get("path6").toString()));
        tdzzinfo.setPath7(num<7?null:(num==7?tdzzinfo.getOrganid():pidtdzzinfo.get("path7").toString()));
        tdzzinfo.setPath8(num<8?null:(num==8?tdzzinfo.getOrganid():pidtdzzinfo.get("path8").toString()));
        tdzzinfo.setPath9(num<9?null:(num==9?tdzzinfo.getOrganid():pidtdzzinfo.get("path9").toString()));
        tdzzinfo.setPath10(num<10?null:(num==10?tdzzinfo.getOrganid():pidtdzzinfo.get("path10").toString()));
        tdzzinfo.setInsertDate(new Date());
        generalSqlComponent.insert(SqlCode.saveDzzsimple, tdzzinfo);
    }
    private void addTDzzinfoBc(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String, Object> organization) {
        tdzzinfo.setDzzsj("");
        generalSqlComponent.insert(SqlCode.saveDzzInfoBc, tdzzinfo);
    }

    private Long addUrcOrganization(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String,Object> pidtdzzinfo) {
        String[] path =tdzzinfo.getIdPath().split("/");
        Integer num = path.length-1;
        UrcOrgan organ = new UrcOrgan();
        organ.setOrgname(tdzzinfo.getDxzms());
        organ.setSorgname(tdzzinfo.getDzzmc());
        organ.setPorgid(null);
        Map<String,Object> parms = new HashMap<String,Object>(1);
        Map<String,Object> piddata = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{tdzzinfo.getParentcode()});
        organ.setPorgid(Long.valueOf(piddata.get("orgid").toString()));
        organ.setOrgseqno(Long.valueOf(tdzzinfo.getOrdernum()));
        organ.setOrgtypeid(Long.valueOf(tdzzinfo.getZzlb()));
        organ.setOrgcode(tdzzinfo.getOrgancode());
        organ.setStatus(1);
        organ.setType(0);
        organ.setOrglevel(String.valueOf(num));
        organ.setOrgpath("");
        organ.setCreateuserid(String.valueOf(session.getUserid()));
        organ.setCreatetime(sdf.format(new Date()));
        organ.setCompid(session.getCompid());
        organ.setRemarks3(tdzzinfo.getRemarks3());
        Long id = generalSqlComponent.insert(SqlCode.saveHornOrganization2, organ);
        organ.setOrgpath(String.valueOf(piddata.get("orgpath"))+id+"_");
        generalSqlComponent.update(SqlCode.updateHornOrganization, organ);
        return id;
    }
    private void addTDzzHjxxDzz(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo) {
        Map param = new HashMap();
        String uuid = UUID.randomUUID().toString().replace("-","");
        param.put("uuid",uuid);
        param.put("organid",tdzzinfo.getOrganid());
        param.put("organname",tdzzinfo.getDzzmc());
        param.put("hjdate","");
        param.put("jmdate",tdzzinfo.getExpirationdate());
        param.put("bjbzcsfs","1000000001");
        param.put("syncstatus","A");
        param.put("synctime",DateEnum.YYYYMMDDHHMMDD.format());
        param.put("zfbz",0);
        generalSqlComponent.insert(SqlCode.addDzzBzxxByOrganid, param);
    }

    private Long addUrcUser(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo) {
        UrcUser user = new UrcUser();
        user.setUsername(tdzzinfo.getDzzmc());
        user.setSex(0);
        user.setPhone(tdzzinfo.getLxrphone());
        user.setIdno(tdzzinfo.getOrgancode());
        user.setAccflag(1);
        user.setPostname("");
        user.setCreateuserid(String.valueOf(session.getUserid()));
        user.setCreatetime(sdf.format(new Date()));
        user.setCompid(session.getCompid());
        Long userid = generalSqlComponent.insert(SqlCode.addUrcUser, user);
        return userid;
    }

    private void addUrcUserAccount(TDzzInfoSimpleReq tdzzinfo,Long userid) {
        Map param = new HashMap();
//        Map param2 = new HashMap();
        param.put("account","B"+tdzzinfo.getOrgancode());
        param.put("userid",userid);
//        param2.put("account","A"+tdzzinfo.getOrgancode());
//        param2.put("userid",userid);
        generalSqlComponent.insert(SqlCode.addUrcUserAccount, param);
//        generalSqlComponent.insert(SqlCode.addUrcUserAccount, param2);
    }
    private void addUrcUserOrgMap(TDzzInfoSimpleReq tdzzinfo,Session session ,Long userid,Long orgid) {
        Map param = new HashMap();
        param.put("userid",userid);
        param.put("orgid",orgid);
        param.put("seqno",tdzzinfo.getOrdernum());
        param.put("compid",session.getCompid());
        generalSqlComponent.insert(SqlCode.addUserOrgMap2, param);
    }

    private String addTDwInfo(Session session,TDwInfoReq tdwinfo,TDzzInfoSimpleReq tdzzinfo,SimpleDateFormat sdf) {
        String uuid = UUID.randomUUID().toString().replace("-","");
        tdwinfo.setUuid(uuid);
        tdwinfo.setOrganid(tdzzinfo.getOrganid());
        tdwinfo.setDworder(tdzzinfo.getOrdernum());
        tdwinfo.setCreatetime(sdf.format(new Date()));
        tdwinfo.setSyncstatus("A");
        generalSqlComponent.insert(SqlCode.saveDwInfo2, tdwinfo);
        return uuid;

    }

    private void addTDwInfoBc(Session session,TDwInfoReq tdwinfo,TDzzInfoSimpleReq tdzzinfo,SimpleDateFormat sdf) {
        generalSqlComponent.insert(SqlCode.addTDwInfoBc, tdwinfo);
    }


    public List<Map<String,Object>> getDzzlsgx(String code,String parent_code,Session session){
        Map<String,Object> parms = new HashMap<String,Object>(2);
        parms.put("code",code);
        parms.put("parent_code",parent_code);
        List<Map<String,Object>> list = generalSqlComponent.query(SqlCode.getGDictItem,parms);
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean updateTDzzinfo(TDzzInfoSaveReq bean, Session session){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //党组织信息
        TDzzInfoSimpleReq tdzzinfo = bean.getTdzzinfo();
        //单位信息
        TDwInfoReq tdwinfo = bean.getTdwinfo();

        if(tdzzinfo!=null){
            if(StringUtils.isEmpty(tdzzinfo.getOrdernum())){
                LehandException.throwException("排序不能为空！！！");
            }
        }
        //查询urc_organization 创建人所在组织
        Map<String,Object> organization = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{session.getCurrentOrg().getOrgcode()});
        //查询父组织t_dzz_info_simple
        Map<String,Object> pidtdzzinfo = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleOrgid,new Object[]{tdzzinfo.getParentid()});



        //处理单位数据
        String maindwid = null;
        if(tdwinfo!=null){
            //查询单位信息是否已存在
            Map<String,Object> dwmap = generalSqlComponent.query(SqlCode.getTDwInfo,new Object[]{tdzzinfo.getOrganid()});
            if(dwmap==null){//未存在 调新增方法
                //插入t_dw_info
                maindwid = addTDwInfo(session,tdwinfo,tdzzinfo,sdf);
                tdzzinfo.setMaindwid(maindwid);
                //插入t_dw_info_bc
                tdwinfo.setUuid(maindwid);
                addTDwInfoBc(session,tdwinfo,tdzzinfo,sdf);
            }else{//存在 写修改方法
                tdwinfo.setUuid(dwmap.get("uuid").toString());
                //修改t_dw_info表
                updateTDwInfo(session,tdwinfo,tdzzinfo,sdf);
                Map<String,Object> dwmapbc = generalSqlComponent.query(SqlCode.getTDwInfoBc,new Object[]{dwmap.get("uuid")});
                if(dwmapbc==null){
                    addTDwInfoBc(session,tdwinfo,tdzzinfo,sdf);
                }else{
                    //修改t_dw_info_bc
                    updateTDwInfoBc(session,tdwinfo,tdzzinfo,sdf);
                }

            }
        }

        if(tdzzinfo!=null){
            tdzzinfo.setExpirationdate(!StringUtils.isEmpty(tdzzinfo.getExpirationdate())?tdzzinfo.getExpirationdate().substring(0,10)+" 00:00:00":tdzzinfo.getExpirationdate());
            //修改 t_dzz_info
            updateTDzzinfo(session, sdf, tdzzinfo, organization);
            if(pidtdzzinfo!=null){
                //修改 t_dzz_info_simple
                updateTDzzinfoSimple(session, sdf, tdzzinfo, organization,pidtdzzinfo);
            }
            //修改t_dzz_info_bc
            updateTDzzinfoBc(session, sdf, tdzzinfo, organization);
            //修改urc_organization
            updateUrcOrganization(session, sdf, tdzzinfo,pidtdzzinfo);
            //保存换届信息配置表
//            Map<String,Object> hjxx = generalSqlComponent.query(SqlCode.getLatestHjxxByOrganid,new Object[]{tdzzinfo.getOrganid()});
//            if(hjxx==null){
//                // 插入t_dzz_hjxx_dzz
//                addTDzzHjxxDzz(session, sdf, tdzzinfo);
//            }else{
//                // 修改t_dzz_hjxx_dzz
//                updateTDzzHjxxDzz(session, sdf, tdzzinfo);
//            }

            //修改urc_user
            updateUrcUser(session, sdf, tdzzinfo);
        }
        return true;
    }

    public void updateTDwInfo(Session session,TDwInfoReq tdwinfo,TDzzInfoSimpleReq tdzzinfo,SimpleDateFormat sdf){
        tdwinfo.setDworder(tdzzinfo.getOrdernum());
        generalSqlComponent.update(SqlCode.updateTDwInfo, tdwinfo);
    }

    private void updateTDwInfoBc(Session session,TDwInfoReq tdwinfo,TDzzInfoSimpleReq tdzzinfo,SimpleDateFormat sdf) {
        generalSqlComponent.update(SqlCode.updateTDwInfoBc, tdwinfo);
    }

    private void updateTDzzinfo(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String, Object> organization) {
        tdzzinfo.setOperorganid(organization.get("orgid").toString());
        tdzzinfo.setUpdator(session.getUserid().toString());
        tdzzinfo.setOperatorname(organization.get("orgname").toString());
        tdzzinfo.setUpdatetime(sdf.format(new Date()));
        tdzzinfo.setDxzms(tdzzinfo.getDxzms());
        tdzzinfo.setDelflag("0");
        generalSqlComponent.update(SqlCode.updateTDzzInfo, tdzzinfo);
    }

    private void updateTDzzinfoSimple(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String, Object> organization, Map<String,Object> pidtdzzinfo) {
        tdzzinfo.setIdPath(pidtdzzinfo.get("id_path")+tdzzinfo.getOrganid()+"/");
        String[] path =tdzzinfo.getIdPath().split("/");
        Integer num = path.length-1;
        tdzzinfo.setPath1(num<1?null:(num==1?tdzzinfo.getOrganid():pidtdzzinfo.get("path1").toString()));
        tdzzinfo.setPath2(num<2?null:(num==2?tdzzinfo.getOrganid():pidtdzzinfo.get("path2").toString()));
        tdzzinfo.setPath3(num<3?null:(num==3?tdzzinfo.getOrganid():pidtdzzinfo.get("path3").toString()));
        tdzzinfo.setPath4(num<4?null:(num==4?tdzzinfo.getOrganid():pidtdzzinfo.get("path4").toString()));
        tdzzinfo.setPath5(num<5?null:(num==5?tdzzinfo.getOrganid():pidtdzzinfo.get("path5").toString()));
        tdzzinfo.setPath6(num<6?null:(num==6?tdzzinfo.getOrganid():pidtdzzinfo.get("path6").toString()));
        tdzzinfo.setPath7(num<7?null:(num==7?tdzzinfo.getOrganid():pidtdzzinfo.get("path7").toString()));
        tdzzinfo.setPath8(num<8?null:(num==8?tdzzinfo.getOrganid():pidtdzzinfo.get("path8").toString()));
        tdzzinfo.setPath9(num<9?null:(num==9?tdzzinfo.getOrganid():pidtdzzinfo.get("path9").toString()));
        tdzzinfo.setPath10(num<10?null:(num==10?tdzzinfo.getOrganid():pidtdzzinfo.get("path10").toString()));
        tdzzinfo.setDelflag("0");
        generalSqlComponent.update(SqlCode.updateTDzzSimple, tdzzinfo);
    }


    private void updateTDzzinfoBc(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String, Object> organization) {
        generalSqlComponent.update(SqlCode.updateTDzzInfoBc, tdzzinfo);
    }

    private void updateUrcOrganization(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo, Map<String,Object> pidtdzzinfo) {
        Map<String,Object> orgdata = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{tdzzinfo.getOrgancode()});
        Map<String,Object> piddata = generalSqlComponent.query(SqlCode.getOrgan,new Object[]{tdzzinfo.getParentcode()});
        Long pidOrgid = Long.valueOf(piddata.get("orgid").toString());//上级Orgid
        Integer pidorglevel = (Integer) piddata.get("orglevel");//上级orglevel
        String pidorgpath = String.valueOf(piddata.get("orgpath"));//上级orgpath
        Object orgid = orgdata.get("orgid");//当前组织id
        int num = pidorglevel+1;//当前组织level
        UrcOrgan organ = new UrcOrgan();
        organ.setOrgcode(tdzzinfo.getOrgancode());
        organ.setOrgname(tdzzinfo.getDxzms());
        organ.setSorgname(tdzzinfo.getDzzmc());
        organ.setOrgseqno(Long.valueOf(tdzzinfo.getOrdernum()));
        organ.setOrgtypeid(Long.valueOf(tdzzinfo.getZzlb()));
        organ.setUpdateuserid(session.getUserid().toString());
        organ.setUpdatetime(sdf.format(new Date()));
        organ.setStatus(1);
        //修改父级，路径
        organ.setOrgpath(pidorgpath+orgid+"_");
        organ.setOrglevel(num+"");
        organ.setPorgid(pidOrgid);
        generalSqlComponent.update(SqlCode.updateUrcOrganization, organ);
    }

    private void updateTDzzHjxxDzz(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo) {
        Map param = new HashMap();
        param.put("organid",tdzzinfo.getOrganid());
        param.put("jmdate",tdzzinfo.getExpirationdate());
        param.put("operator",session.getUserid());
        param.put("operatetime",sdf.format(new Date()));
        generalSqlComponent.insert(SqlCode.updateTDzzHjxxDzz, param);
    }

    private void updateUrcUser(Session session, SimpleDateFormat sdf, TDzzInfoSimpleReq tdzzinfo) {
        UrcUser oldUrcUser = generalSqlComponent.getDbComponent().getBean("SELECT userid FROM `urc_user` WHERE idno =?", UrcUser.class, new Object[]{tdzzinfo.getOrgancode()});
        if(oldUrcUser!=null){
            UrcUser user = new UrcUser();
            user.setUserid(oldUrcUser.getUserid());
            user.setPhone(tdzzinfo.getLxrphone());
            user.setIdno(tdzzinfo.getOrgancode());
            user.setStatus("1");
            user.setUsername(tdzzinfo.getDzzmc());
            generalSqlComponent.update(SqlCode.updateUrcUserPhone, user);
        }
    }

    public Boolean deleteTDzzInfo(String organid,String organcode,Session session){
        //判断是否有党员   有党员不允许删除
        List<Map<String,Object>> dylist = generalSqlComponent.query(SqlCode.getTDyInfoList3,new Object[]{organid});
        if(dylist.size()>0){
            LehandException.throwException("该组织下存在党员信息，无法删除！");
            return false;
        }
        //判断是否有子机构 有子机构不允许删除
        List<Map<String,Object>> dzzlist = generalSqlComponent.query(SqlCode.getTDzzInfoParentid,new Object[]{organid});
        if(dzzlist.size()>0){
            LehandException.throwException("该组织存在下级组织，无法删除！");
            return false;
        }
        //urc_user_org_map 不需处理
        //urc_user_account 不需处理
        //urc_user 修改status 0
        Map<String,Object> orgpatms = new HashMap<String,Object>(1);
        orgpatms.put("organcode",organcode);
        Map<String,Object> orgmap =  generalSqlComponent.query(SqlCode.queryPorgcode,orgpatms);
        if(orgmap==null){
            LehandException.throwException("urc组织不能为空！");
        }
        generalSqlComponent.update(SqlCode.updateUrcUserStatus,new Object[]{0,orgmap.get("userid")});
//        generalSqlComponent.update(SqlCode.updateUrcUserStatus2,new Object[]{0,organcode});
        // t_dzz_hjxx_dzz 修改zfbz 1
        generalSqlComponent.update(SqlCode.updateTDzzHjxxDzzZfbz,new Object[]{1,organid});
        //urc_organization 修改status 0
        Map<String,Object> parms = new HashMap<String,Object>(1);
        parms.put("orgcode",organcode);
        parms.put("compid",session.getCompid());
        generalSqlComponent.update(SqlCode.modiUrcOrganizations3,parms);
        //查询党组织信息
        Map<String,Object> dwmap = generalSqlComponent.query(SqlCode.getTDwInfo,new Object[]{organid});
        if(dwmap!=null){
            //t_dw_info_bc  修改zfbz 1
            generalSqlComponent.update(SqlCode.updateTDwInfoBcStatus,new Object[]{1,dwmap.get("uuid")});
            //t_dw_info   修改zfbz 1
            generalSqlComponent.update(SqlCode.updateTDwInfoZfbz,new Object[]{1,organid});
        }

        //t_dzz_info_bc  不需处理
        //t_dzz_info_simple  修改 delflag 1
        generalSqlComponent.update(SqlCode.updateTDzzInfoSimpleDeflag,new Object[]{1,organid});
        // t_dzz_info   修改 delflag 1
        generalSqlComponent.update(SqlCode.updateTDzzInfoDeflag,new Object[]{1,organid});
        //删除党小组
        List<Map<String, Object>> partyGroups = generalSqlComponent.query(SqlCode.getPartyGroupSel, new Object[]{organcode, session.getCompid()});
        for (Map<String, Object> map:partyGroups) {
            partyGroupService.delPartyGroup((Long) map.get("id"), session);
        }
        //禁用当前组织下用户信息
        Pager pager = new Pager();
        pager.setPageNo(1);
        pager.setPageSize(999);
        pager.setSort("seqno desc");
        pager.setUseSort(true);
        String datacode= "SUBADMIN";
        String useSearch= "0";
        Long orgid = (Long) orgmap.get("orgid");
        UserAccountView user =new UserAccountView();
        user.setOrgid(orgid);
        List<UserAccountView> list = userOrgService.getUserListForPagerNew(datacode,orgid, user, session, pager,useSearch).getRows();
        for (UserAccountView userAccountView:list) {
            userService.prohibitUser(userAccountView.getUserid(), session);
        }
        //刷新缓存组织信息
        new Thread(()-> cacheService.refCache()).start();
        return true;
    }

    public TDzzInfoSimple checkCode(String organcode){
        TDzzInfoSimple vericode = vericode(organcode);
//        if(vericode!=null){
//            LehandException.throwException("编码："+ organcode+"组织已存在");
//        }
        return vericode;
    }

    public List<Map<String, Object>> listDictItem(String code) {
        List<Map<String,Object>> list = generalSqlComponent.getDbComponent().listMap("SELECT item_name name,item_code id  FROM `g_dict_item` WHERE code = ?", new Object[] {code});
        return list;
    }

    /**
     * 新增先进基层党组织信息
     * @param bean
     * @param session
     * @return
     */
    public boolean addDzzAdvanced(TDzzAdvanced bean, Session session) {
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("党组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgname())){
            LehandException.throwException("党组织名称参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getYear())){
            LehandException.throwException("年份参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getType())){
            LehandException.throwException("先进基层类型参数不能为空！");
        }
        bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        bean.setCreateuserid(session.getUserid());
        generalSqlComponent.insert(SqlCode.saveDzzAdvanced,bean);
        return true;
    }

    /**
     * 删除先进基层党组织信息
     * @param id
     * @return
     */
    public boolean delDzzAdvanced(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }
        generalSqlComponent.getDbComponent().delete("delete from t_dzz_advanced where id=?",new Object[]{id});
        return true;
    }

    /**
     * 编辑先进基层党组织信息
     * @param bean
     * @return
     */
    public boolean updateDzzAdvanced(TDzzAdvanced bean, Session session) {
        if(StringUtils.isEmpty(bean.getId())){
            LehandException.throwException("id参数不能为空！");
        }
        bean.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        bean.setUpdateuserid(session.getUserid());
        generalSqlComponent.update(SqlCode.updateDzzAdvanced,bean);
        return true;
    }

    /**
     *查看先进基层党组织信息
     * @param id
     * @return
     */
    public TDzzAdvancedReq getDzzAdvanced(String id) {
        TDzzAdvancedReq tDzzAdvancedReq = generalSqlComponent.query(SqlCode.getDzzAdvanced,new Object[]{id});
        if(!StringUtils.isEmpty(tDzzAdvancedReq.getFileid())){
            List<Map<String,Object>> listFile = listFile(tDzzAdvancedReq.getFileid());
            tDzzAdvancedReq.setAttachList(listFile);
        }
        return  tDzzAdvancedReq;
    }

    /**
     * 获取附件list集合
     * @param fileids
     * @return
     */
    private List<Map<String, Object>> listFile(String fileids) {
        List<Map<String,Object>> listFile = new ArrayList<>();
        if(!StringUtils.isEmpty(fileids)){
            String[] fileid = fileids.split(",");
            for (String id:fileid) {
                Map<String,Object> m =  generalSqlComponent.query(SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",1);put("id",id);}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    m.put("fileid",id);
                    m.put("flpath", String.format("%s%s", downIp, dir));
                    m.put("name",m.get("name").toString().substring(0,m.get("name").toString().lastIndexOf(".")));
                    listFile.add(m);
                }
            }
        }
        return  listFile;
    }


    /**
     *分页查询先进基层党组织信息
     * @param pager
     * @param orgCode  组织编码
     * @param startYear  开始年份
     * @param endYear    结束年份
     * @param type    先进基层党组织信息类型
     * @param session
     * @return
     */
    public Pager pageDzzAdvanced(Pager pager, String orgCode, String startYear, String endYear, String type, Session session,Integer pageType) {
        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
        if(pageType==1){
            paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        }else{
            paramMap.put("orgcode", orgCode);
        }
        paramMap.put("startYear",startYear);
        paramMap.put("endYear",endYear);
        paramMap.put("type",type);
        pager=generalSqlComponent.pageQuery(SqlCode.pageDzzAdvanced,paramMap,pager);
        List<TDzzAdvancedReq> dataList = (List<TDzzAdvancedReq>) pager.getRows();
        for (TDzzAdvancedReq  tDzzAdvancedReq: dataList) {
            List<Map<String,Object>> listFile = listFile(tDzzAdvancedReq.getFileid());
            tDzzAdvancedReq.setAttachList(listFile);
        }
        return pager ;
    }

    /**
     * 获取当前组织下所有组织编码
     * @param organcode
     * @return
     */
    private void getOrgCode(String organcode,List<String> listOrgCode) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        List<Map<String,Object>> list  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree2, paramMap);
        for(Map<String,Object> li:list){
            listOrgCode.add(li.get("organcode").toString());
            getOrgCode(li.get("organcode").toString(),listOrgCode);
        }
    }

    public Object exportDzzAdvancedInfo(HttpServletResponse response, TDzzAdvancedReq req, String orgCode, String startYear, String endYear, String type,Session session) {
        //表格标题
        if(req.getExportColumns().size()<1){
            return  null;
        }
        List<ExportColumnReq> columns  = req.getExportColumns();

        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        paramMap.put("sort",1);
        paramMap.put("startYear",startYear);
        paramMap.put("endYear",endYear);
        paramMap.put("type",type);
        //获取导出内容
        List<TDzzAdvancedReq> dataList = generalSqlComponent.query(SqlCode.listDzzAdvanced, paramMap);

        //表头
        List<String> tables = new ArrayList<String>();
        for (ExportColumnReq col: columns
        ) {
            tables.add(col.getTitle());
        }
        String[] tablesStr = tables.toArray(new String[tables.size()]);
        //表名
        String tableName = String.format("%s先进基层党组织信息", session.getUsername());

        //内容
        List<Map<String,String>> mapList = getMemberAnalysisExportInfo(dataList,columns);

        ExcelUtils.write(response,tablesStr,mapList,tableName);
        return null;
    }

    /**
     * 先进基层党组织信息导出
     * @param list
     * @param columns
     * @return
     */
    private List<Map<String, String>> getMemberAnalysisExportInfo(List<TDzzAdvancedReq> list, List<ExportColumnReq> columns) {
        List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
        for ( TDzzAdvancedReq  info: list) {
            Map<String, String> tempMap = new HashMap<>();
            for (ExportColumnReq col: columns) {
                tempMap.put(col.getTitle(),getInfo(col.getCode(), SysUtil.transBean2Map(info)));
            }
            explist.add(tempMap);
        }
        return explist;
    }
    /**
     * 通过列编码取值
     * @param code
     * @param info
     * @return
     */
    public String getInfo(String code, Map<String, Object> info) {
        String inforStr = StringUtils.isEmpty(info.get(code)) ? MagicConstant.STR.EMPTY_STR : String.valueOf(info.get(code));
        return "null".equals(inforStr) ? MagicConstant.STR.EMPTY_STR : inforStr;
    }


    public Object dzzFundingTree(String orgCode, String likeStr, Session session, List<String> listOrgCode) {
        if(StringUtils.isEmpty(orgCode)){
            orgCode = session.getCurrentOrg().getOrgcode();
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", orgCode);
        Map<String, Object> data = generalSqlComponent.getDbComponent().getMap("select * from t_dzz_info_simple where organcode = ?", new Object[]{orgCode});
        if(data==null){
            return null;
        }
        //是否是集团党委
        if(!SysConstants.ZZLB.DW.equals(data.get("zzlb"))){
            return null;
        }
        //组织关键字查询
        if(!StringUtils.isEmpty(likeStr)){
            paramMap.put("likeStr", likeStr);
            List<Map<String,Object>> listMap = generalSqlComponent.query(SqlCode.dzzFundingTree, paramMap);
            for (Map<String,Object> map:
            listMap) {
                data.put("title", map.get("dzzmc"));
                data.put("key", map.get("organid"));
                data.put("slots", getSlots(map.get("zzlb").toString()));
                data.put("children", "");
                UrcOrgan urcOrgan = getUrcOrgByOrgancode(orgCode);
                data.put("orgid", urcOrgan!=null?urcOrgan.getOrgid():null);
            }
            return listMap;
        }
        String parentcode = String.valueOf(data.get("parentcode"));
        listOrgCode.add(orgCode);
        data.put("title",data.get("dzzmc"));
        data.put("key", data.get("organid"));
        data.put("slots", getSlots(data.get("zzlb").toString()));
        UrcOrgan urcOrgan = getUrcOrgByOrgancode(orgCode);
        data.put("orgid", urcOrgan!=null?urcOrgan.getOrgid():null);
        //是否是集团党委
        if("0".equals(parentcode)){
            //二级党委
            List<Map<String,Object>> listMap = generalSqlComponent.query(SqlCode.listTDzzInfoSimple, new Object[]{orgCode});
            for (Map<String,Object> map: listMap) {
                //所有组织编码
                listOrgCode.add(String.valueOf(map.get("organcode")));
                map.put("title",map.get("dzzmc"));
                UrcOrgan urcOrgan1 = getUrcOrgByOrgancode(String.valueOf(map.get("organcode")));
                map.put("orgid", urcOrgan1!=null?urcOrgan1.getOrgid():null);
                map.put("key", map.get("organid"));
                map.put("slots", getSlots(map.get("zzlb").toString()));
                map.put("children", "");
            }
            data.put("children", listMap);
        }else{
            return data.get("path3")==null?data:null;
        }
        return data;

    }
    /**
     * 根据组织机构编码获取 urc组织机构
     * @param organcode
     * @return
     */
    public UrcOrgan getUrcOrgByOrgancode(String organcode){
        UrcOrgan org  = generalSqlComponent.getDbComponent().getBean("select * from urc_organization where orgcode = ? ",
                UrcOrgan.class,new Object[]{organcode});
        return org;
    }
    public boolean addDzzFunding(TDzzFunding bean, Session session) {
        //判断必传参数
        isNotEmpty(bean);
        bean.setCreateuserid(session.getUserid().toString());
        bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        generalSqlComponent.insert(SqlCode.addDzzFunding,bean);
        return true;
    }

    private void isNotEmpty(TDzzFunding bean) {
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgname())){
            LehandException.throwException("组织名称参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getType())){
            LehandException.throwException("类型参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getUsetime())){
            LehandException.throwException("使用日期不能为空！");
        }
        if(StringUtils.isEmpty(bean.getUselines())){
            LehandException.throwException("使用金额参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getRemark())){
            LehandException.throwException("事项说明参数不能为空！");
        }
    }

    public boolean updateDzzFunding(TDzzFunding bean, Session session) {

        if(StringUtils.isEmpty(bean.getId())){
            LehandException.throwException("id参数不能为空！");
        }
        //判断必创参数
        isNotEmpty(bean);

        bean.setUpdateuserid(session.getUserid().toString());
        bean.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        generalSqlComponent.update(SqlCode.updateDzzFunding,bean);
        return true;
    }
    public boolean delDzzFunding(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }
        generalSqlComponent.delete(SqlCode.delDzzFunding,new Object[]{id});

        return true;
    }

    public TDzzFundingReq getDzzFunding(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }
        TDzzFundingReq tDzzFundingReq = generalSqlComponent.query(SqlCode.getDzzFunding,new Object[]{id});

        //查询党组织
        Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDzzFundingReq.getOrgcode()});
        tDzzFundingReq.setOrgname(String.valueOf(tDzzInfo.get("dzzmc")));

        tDzzFundingReq.setAttachList(listFile(tDzzFundingReq.getFileid()));
        return tDzzFundingReq;
    }

    public Pager pageDzzFunding(Pager pager, String startTime, String endTime, String type, String orgCode, Session session) {
        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
//        dzzFundingTree(orgCode, "", session,listOrgCode);
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));

        //开始-结束年份
        paramMap.put("startTime",startTime);
        paramMap.put("endTime",endTime);
        //优秀党员类型
        paramMap.put("type",type);
        pager=generalSqlComponent.pageQuery(SqlCode.pageDzzFunding,paramMap,pager);
        List<TDzzFundingReq> datas = (List<TDzzFundingReq>) pager.getRows();
        for (TDzzFundingReq tDzzFundingReq: datas) {
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDzzFundingReq.getOrgcode()});
            tDzzFundingReq.setOrgname(String.valueOf(tDzzInfo.get("dzzmc")));
        }

        return pager ;
    }

    public List<Map<String, Object>> dzzFundingStatistica(Session session,String year) {

        List<Map<String, Object>> result = new ArrayList<>();
        String orgCode = session.getCurrentOrg().getOrgcode();
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        String[] str={"01","02","03","04","05","06","07","08","09","10","11","12"};

        for (String month: str) {
            paramMap.put("time",year+"-"+month);
            Map<String, Object> data = generalSqlComponent.query(SqlCode.dzzFundingStatistica, paramMap);
            data.put("uselines",data.get("uselines")!=null?data.get("uselines"):0);


            data.put("month",month);
            result.add(data);
        }
        return result;
    }
}
