//package com.lehand.horn.partyorgan.service.dzz;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.Resource;
//import org.springframework.util.StringUtils;
//import com.lehand.base.common.Pager;
//import com.lehand.horn.partyorgan.dao.TdzzBzcyxxDao;
//import com.lehand.horn.partyorgan.dao.TdzzHjxxDao;
//import com.lehand.horn.partyorgan.dao.TdzzInfoSimpleDao;
//import com.lehand.horn.partyorgan.dao.TdzzJcxxDao;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzHjxxDzz;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzJcxxDzz;
//import com.lehand.horn.partyorgan.service.GdictItemService;
//
//
///**
// * 组织机构业务层
// * @author taogang
// * @version 1.0
// * @date 2019年1月21日, 下午2:06:32
// */
//public class TdzzInfoSimpleService{
//
//	@Resource
//	private TdzzInfoSimpleDao tdzzInfoSimpleDao;
//
//	@Resource
//	private GdictItemService gdictItemService;
//
//	@Resource
//	private TdzzInfoBcService tdzzInfoBcService;
//
//	@Resource
//	private TdzzHjxxDao tdzzHjxxDao;
//
//	@Resource
//	private TdzzBzcyxxDao tdzzBzcyxxDao;
//
//	@Resource
//	private TdzzJcxxDao tdzzJcxxDao;
//
//	/**
//	 * 查询组织机构树
//	 * @param organid 组织机构id
//	 * @return
//	 * @author taogang
//	 */
//	public List<Map<String, Object>> getListByOrganId(Session session,String organid,String likeStr){
//		//定义list 数组
//		List<Map<String, Object>> list = new ArrayList<>();
//		//查询党组织信息集合
//		List<TDzzInfoSimple> dzzList = new ArrayList<>();
//		//判断组织机构id是否为空
//		if(StringUtils.isEmpty(organid)) {
//			organid = tdzzInfoBcService.getOrgId(session);
//			dzzList = tdzzInfoSimpleDao.query(organid);
//		}else {
//			dzzList = tdzzInfoSimpleDao.queryById(organid);
//		}
//		//模糊搜索
//		if(!StringUtils.isEmpty(likeStr)){
//			likeStr = "%" + likeStr + "%";
//			TDzzInfoSimple tdzz = tdzzInfoSimpleDao.queryOrgandId(organid);
//			dzzList = tdzzInfoSimpleDao.queryByLikeStr(likeStr, tdzz.getIdPath() + "%");
//			if(null == dzzList) {
//				dzzList = new ArrayList<>();
//			}
//		}
//
//		if(dzzList.size() > 0 ) {
//			Map<String, Object> bean = null;
//			for (TDzzInfoSimple tDzzInfoSimple : dzzList) {
//				bean = new HashMap<>();
//				bean.put("organid", tDzzInfoSimple.getOrganid());
//				bean.put("dzzmc", tdzzInfoBcService.getDzzjc(tDzzInfoSimple.getOrgancode(), session.getCompid()));
//				bean.put("dzzqc", tDzzInfoSimple.getDzzmc());
//				bean.put("orgtype", tDzzInfoSimple.getOrgantype());
//				bean.put("icon", tDzzInfoSimple.getZzlb());
//				bean.put("parentid", tDzzInfoSimple.getParentid());
//				bean.put("isSubordinate", isSubordinate(tDzzInfoSimple.getOrganid()));
//				bean.put("isOpen", "true");
//				list.add(bean);
//			}
//		}
//		return list;
//	}
//
//	/**
//	 * 是否包含下级
//	 * @param organid 组织机构id
//	 * @return
//	 * @author taogang
//	 */
//	public Integer isSubordinate(String organid) {
//		return tdzzInfoSimpleDao.countDzz(organid) > 0 ? 1 : 0 ;
//	}
//
//	/**
//	 * 组织机构列表查询
//	 * @param organid  	组织机构id
//	 * @param dwlb  	单位类别
//	 * @param likeStr 	dzzmc 模糊查询
//	 * @return
//	 * @author taogang
//	 */
//	public List<Map<String, Object>> getList(Pager pager,String organid,String dwlb,String likeStr){
//		//定义list
//		List<Map<String, Object>> list = new ArrayList<>();
//		//定义查询条件
//		StringBuffer whereSql = new StringBuffer();
//		List<String> params = new ArrayList<String>(4);
//		params.add(organid);
//		params.add(organid);
//		whereSql.append("select * from t_dzz_info_simple where (organid = ? or parentid = ?)");
//		if(!StringUtils.isEmpty(dwlb)) {
//			whereSql.append(" and szdwlb = ?");
//			params.add(dwlb);
//		}
//		if(!StringUtils.isEmpty(likeStr)) {
//			likeStr = "%" + likeStr + "%";
//			whereSql.append(" and dzzmc like ?");
//			params.add(likeStr);
//		}
//		whereSql.append(" order by organcode asc,organtype desc,ordernum asc");
//		List<TDzzInfoSimple> dzzList = tdzzInfoSimpleDao.list(whereSql.toString(), pager,params.toArray());
//		if(dzzList.size() > 0) {
//			Map<String, Object> bean = null;
//			for (TDzzInfoSimple row : dzzList) {
//				bean = new HashMap<>();
//				bean.put("organid", row.getOrganid());
//				bean.put("dzzmc", row.getDzzmc());
//				bean.put("organcode", row.getOrgancode());
//				bean.put("zzlb", gdictItemService.getItemName("d_dzz_zzlb", row.getZzlb()));
//				bean.put("dwlb", gdictItemService.getItemName("d_dw_xzlb", row.getSzdwlb()));
//				bean.put("dzzsj", tdzzInfoBcService.getDzzsj(row.getOrganid()));
//				bean.put("dzzlsgx", gdictItemService.getItemName("d_dzz_lsgx", row.getDzzlsgx()));
//				bean.put("xxwzd", row.getXxwzd());
//				bean.put("organtype", row.getOrgantype());
//				list.add(bean);
//			}
//		}
//		return list;
//	}
//
//	/**
//	 * 统计列表条数
//	 * @param organid
//	 * @param dwlb
//	 * @param likeStr
//	 * @return
//	 * @author taogang
//	 */
//	public int countDzz(String organid,String dwlb,String likeStr) {
//		int count = 0;
//		//定义查询条件
//		StringBuffer sql = new StringBuffer();
//		List<String> params = new ArrayList<String>(4);
//		params.add(organid);
//		params.add(organid);
//		sql.append("select count(1) from t_dzz_info_simple where (organid = ? or parentid = ?)");
//		if(!StringUtils.isEmpty(dwlb)) {
//			sql.append(" and szdwlb = ?");
//			params.add(dwlb);
//		}
//		if(!StringUtils.isEmpty(likeStr)) {
//			likeStr = "%" + likeStr + "%";
//			sql.append(" and dzzmc like ?");
//			params.add(likeStr);
//		}
//		count = tdzzInfoSimpleDao.count_list(sql.toString(), params.toArray());
//		return count;
//	}
//
//	/**
//	 * 获取基础信息基本信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public Map<String, Object> getBase(String organid){
//		Map<String, Object> bean = tdzzInfoSimpleDao.dzz_base(organid);
//		bean.put("zzlbName", gdictItemService.getItemName("d_dzz_zzlb", bean.get("zzlb") != null ? bean.get("zzlb").toString() : null));
//		bean.put("dzzlsgxName", gdictItemService.getItemName("d_dzz_lsgx", bean.get("dzzlsgx") != null ? bean.get("dzzlsgx").toString() : null));
//		bean.put("dwxzlbName", gdictItemService.getItemName("d_dw_xzlb", bean.get("dwxzlb") != null ? bean.get("dwxzlb").toString() : null));
//		bean.put("dzzszdwqkName", gdictItemService.getItemName("d_dzz_dwqk", bean.get("dzzszdwqk") != null ? bean.get("dzzszdwqk").toString() : null));
//		bean.put("xzqhName", gdictItemService.getItemName("d_dzz_xzqh", bean.get("xzqh") != null ? bean.get("xzqh").toString() : null));
//		return bean;
//	}
//
//	/**
//	 * 单位所在信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public Map<String, Object> getDwInfo(String organid){
//		Map<String, Object> bean = tdzzInfoSimpleDao.dw_info(organid);
//		if(null != bean) {
//			bean.put("dwlsgxName", gdictItemService.getItemName("d_dw_lsgx", bean.get("dwlsgx") != null ? bean.get("dwlsgx").toString() : null));
//			bean.put("dwxzlbName", gdictItemService.getItemName("d_dw_xzlb", bean.get("dwxzlb") != null ? bean.get("dwxzlb").toString() : null));
//			bean.put("dwjldjczzqkName", gdictItemService.getItemName("d_dw_jljczz", bean.get("dwjldjczzqk") != null ? bean.get("dwjldjczzqk").toString() : null));
//			bean.put("jjlxName", gdictItemService.getItemName("d_dw_jjlx", bean.get("jjlx") != null ? bean.get("jjlx").toString() : null));
//			bean.put("qygmName", gdictItemService.getItemName("d_dw_qygm", bean.get("qygm") != null ? bean.get("qygm").toString() : null));
//		}
//		return bean;
//	}
//
//	/**
//	 * 换届信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public List<Map<String, Object>> getHjxxList(String organid){
//		//定义map
//		List<Map<String, Object>> list = new ArrayList<>();
//		//换届信息结果集
//		List<TDzzHjxxDzz> hjxxList = tdzzHjxxDao.ListHjxx(organid);
//		if(hjxxList.size() > 0 ) {
//			Map<String, Object> bean = null;
//			for (TDzzHjxxDzz row : hjxxList) {
//				bean = new HashMap<>();
//				bean.put("uuid", row.getUuid());
//				bean.put("bzversion", row.getBzversion());
//				bean.put("bjbzcsfs", row.getBjbzcsfs());
//				bean.put("bjbzcsfsName", gdictItemService.getItemName("d_dzz_xjfs", row.getBjbzcsfs()));
//				bean.put("hjdate", row.getHjdate());
//				bean.put("jmdate", row.getJmdate());
//				list.add(bean);
//			}
//		}
//		return list;
//	}
//
//	/**
//	 * 班子成员
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public List<Map<String, Object>> getBzcyxxList(String organid){
//		//定义list数组
//		List<Map<String, Object>> list = tdzzBzcyxxDao.ListBzcyxx(organid);
//		if(list.size() > 0) {
//			for (Map<String, Object> map : list) {
//				map.put("xlName", gdictItemService.getItemName("d_dy_xl", map.get("xl") != null ? map.get("xl").toString() : null));
//				map.put("dnzwnameName", gdictItemService.getItemName("d_dy_dnzw", map.get("dnzwname") != null ? map.get("dnzwname").toString() : null));
//				map.put("zwlevelName", gdictItemService.getItemName("d_dy_zwjb", map.get("zwlevel") != null ? map.get("zwlevel").toString() : null));
//				map.put("ryfs", "从系统中选择");
//			}
//		}
//		return list;
//	}
//
//	/**
//	 * 获取奖惩信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public List<Map<String, Object>> getJcxx(String organid){
//		//定义数组
//		List<Map<String, Object>> list = new ArrayList<>();
//		List<TDzzJcxxDzz> jcxxList = tdzzJcxxDao.list_jcxx(organid);
//		if(jcxxList.size() > 0 ) {
//			Map<String, Object> bean = null;
//			for (TDzzJcxxDzz row : jcxxList) {
//				bean = new HashMap<>();
//				bean.put("uuid", row.getUuid());
//				bean.put("jcname", row.getJcname());
//				bean.put("jcdate", row.getJcdate());
//				bean.put("jcreason", row.getJcreason());
//				bean.put("pzjcdzz", row.getPzjcdzz());
//				bean.put("jcnameName", gdictItemService.getItemName("d_dzz_jcmc",row.getJcname()));
//				list.add(bean);
//			}
//		}
//		return list;
//	}
//
//}
