package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.service.info.ElectionConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Map;

/**
 * 换届提醒配置控制层
 *
 * @author pantao
 */
@Api(value = "换届提醒配置控制层-已经作废", tags = { "换届提醒配置控制层-已经作废" })
@RestController
@RequestMapping("/lhdj/election")
public class ElectionConfigController extends BaseController{

    @Resource
    private ElectionConfigService electionConfigService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "换届提醒配置查询", notes = "换届提醒配置查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organid", value = "机构的32位ID", required = true, dataType = "String", defaultValue = "")
    })
    @RequestMapping("/queryElectionConfig")
    public Message queryElectionConfig(String organid) {
        Message msg = new Message();
        msg.setData(electionConfigService.queryElectionConfig(organid,getSession()));
        return msg.success();
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "换届提醒配置保存", notes = "换届提醒配置保存", httpMethod = "POST")
    @RequestMapping("/saveElectionConfig")
    public Message saveElectionConfig(@RequestBody Map<String,Object> configParam) throws ParseException {
        Message msg = new Message();
        electionConfigService.saveElectionConfig(configParam, getSession());
        return msg.success();
    }
}
