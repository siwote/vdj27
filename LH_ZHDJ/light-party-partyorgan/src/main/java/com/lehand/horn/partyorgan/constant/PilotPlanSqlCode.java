package com.lehand.horn.partyorgan.constant;

public class PilotPlanSqlCode {

    /**
     * 插入党建品牌
     *INSERT INTO t_dzz_party_brand (orgcode, name, type, fileid, slogan, introduce, concept, trainofthought, measures, creator, createtime,auditorgcode,level)
     *  VALUES (:orgcode, :name, :type, :fileid, :slogan, :introduce, :concept, :trainofthought, :measures, :creator, :createtime,:auditorgcode,:level);
     */
    public static String insertTDzzPartyBrand="insertTDzzPartyBrand";
    /**
     * 编辑党建品牌
     *UPDATE t_dzz_party_brand SET  name=:name, type=:type, fileid=:fileid, slogan=:slogan, introduce=:introduce,
     * concept=:concept, trainofthought=:trainofthought, measures=:measures,updator=:updator, updatetime=:updatetime,level=:level,sort=:sort WHERE id=:id;
     */
    public static String updateTDzzPartyBrand="updateTDzzPartyBrand";
    /**
     * 查看党建品牌信息
     SELECT * FROM t_dzz_party_brand WHERE id=?;
     */
    public static String getTDzzPartyBrand="getTDzzPartyBrand";
    /**
     * 分页查询党建品牌
     *SELECT a.*,COUNT(b.orgcode) count FROM t_dzz_party_brand a LEFT JOIN t_dzz_party_brand_praise b ON a.id = b.brandid AND b.STATUS = 0 WHERE 1=1
     * AND a.type =:type
     * and level=:level
     * !empty(PARAMS.myOrLower)', 'and a.status !=:type
     * and a.status =:status
     * and a.name=:name
     * and a.auditorgcode=:auditorgcode
     * GROUP BY
     * 	a.id
     * ORDER BY
     * 	a.sort IS NULL,
     * 	a.sort ASC,
     * 	COUNT(b.brandid) DESC,
     * 	a.updatetime DESC
     */
    public static String pageTDzzPartyBrand="pageTDzzPartyBrand";
    /**
     * 插入品牌动态
     *INSERT INTO t_dzz_party_brand_dynamic (orgcode, title, fileid, status, content, creator, createtime,brandid)
     * VALUES (:orgcode, :title, :fileid, :status, :content, :creator, :createtime,:brandid)
     */
    public static String insertBrandDynamic="insertBrandDynamic";
    /**
     * 修改品牌动态
     * UPDATE t_dzz_party_brand_dynamic SET title=:title, fileid=:fileid, status=:status,
     * content=:content, updator=:updator, updatetime=:updatetime
     * WHERE id=:id
     */
    public static String updateBrandDynamic="updateBrandDynamic";
    /**
     * 查看单个品牌动态
     * SELECT * FROM t_dzz_party_brand_dynamic WHERE id=?
     */
    public static String getBrandDynamic="getBrandDynamic";
    /**
     * 分页查询品牌动态
     *SELECT id, orgcode, title, fileid, status,creator, createtime, updator,updatetime,brandid
     *  FROM t_dzz_party_brand_dynamic WHERE orgcode=:orgcode
     * !empty(PARAMS.title)   and title like '%${PARAMS.title}%'
     * !empty(PARAMS.brandid) and brandid=:brandid
     * !empty(PARAMS.status)  and status=:status
     *  ORDER BY updatetime DESC,createtime DESC
     */
    public static String pageBrandDynamic="pageBrandDynamic";
    /**
     * 删除单个品牌动态
     *DELETE FROM t_dzz_party_brand_dynamic WHERE id=?
     */
    public static String delBrandDynamic="delBrandDynamic";
    /**
     * 查看单个品牌点赞数
     *SELECT * FROM t_dzz_party_brand_praise WHERE status=0 AND orgcode=? and brandid=?;
     */
    public static String getBrandPraise="getBrandPraise";
    /**
     * 根据点赞组织查看单个品牌点赞信息
     *SELECT * FROM t_dzz_party_brand_praise WHERE orgcode=? AND praisecode=? and brandid=?;
     */
    public static String getBrandPraiseByPraisecode="getBrandPraiseByPraisecode";
    /**
     *根据点赞组织修改是否点赞
     * UPDATE t_dzz_party_brand_praise SET status=? WHERE orgcode=? AND praisecode=? and brandid=?;
     */
    public static String updateBrandPraise="updateBrandPraise";
    /**
     *根据点赞组织插入品牌点赞
     * INSERT INTO t_dzz_party_brand_praise (orgcode, praisecode, status,brandid) VALUES (:orgcode, :praisecode, :status,:brandid);
     */
    public static String insertBrandPraise="insertBrandPraise";
    /**
     * 获取当前组织及组织下所有品牌数量
     *SELECT count(*) count,type FROM t_dzz_party_brand WHERE 1=1
     * !empty(PARAMS.orgcode)  and orgcode in (${PARAMS.orgcode})
     *GROUP BY type
     */
    public static String listPartyBrandGroupByType="listPartyBrandGroupByType";
    /**
     * 删除单个组织品牌
     * DELETE FROM t_dzz_party_brand WHERE id=?
     */
    public static String delTDzzPartyBrand ="delTDzzPartyBrand";
    /**
     * (提交/上报/通过/驳回)品牌
     *UPDATE t_dzz_party_brand SET auditorgcode=:auditorgcode,level=:level, status=:status, remark=:remark WHERE id=:id;
     */
    public static String updateBrandStatus="updateBrandStatus";
    /**
     * 发布或撤销发布品牌动态
     *UPDATE t_dzz_party_brand_dynamic SET status=:status,updator=:updator, updatetime=:updatetime WHERE id=:id
     */
    public static String updateDynamicStatus="updateDynamicStatus";
}

