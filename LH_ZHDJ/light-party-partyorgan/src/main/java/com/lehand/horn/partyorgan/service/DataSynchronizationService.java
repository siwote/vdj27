package com.lehand.horn.partyorgan.service;

import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据同步Service
 */
@Service
public class DataSynchronizationService {

    private static final Logger logger = LogManager.getLogger(DataSynchronizationService.class);

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 数据同步
     */
    @Transactional(rollbackFor=Exception.class)
    public void synchronizationData() {
        //同步组织信息
        syncUrcOrgan();
        //同步用户相关信息
        syncUserInfos();
        //更新机构的userid
        modiOrgUserid();
    }
    @Transactional(rollbackFor=Exception.class)
    public void modiOrgUserid() {
        List<Map<String,Object>> organs = generalSqlComponent.query(SqlCode.listUrcOrgan,new Object[]{});
        if(organs==null){
            return;
        }
        for (Map<String, Object> organ : organs) {
            Map map = generalSqlComponent.query(SqlCode.getUrcOrganMap,new Object[]{organ.get("orgid")});
            if(map!=null){
                modiUrcOrganization(new HashMap<String, Object>(){{put("userid",map.get("userid"));put("orgid", organ.get("orgid"));}});
            }
        }
    }

    @Transactional(rollbackFor=Exception.class)
    public void syncUserInfos() {
        List<Map<String,Object>> dyLists = generalSqlComponent.query(SqlCode.listSysUser,new Object[]{});
        if(dyLists==null || dyLists.size()<=0){
            return;
        }
        syncUserInfos2(dyLists);
    }

    public void syncUserInfos2(List<Map<String, Object>> dyLists) {
        for (Map<String, Object> map : dyLists) {
            Map<String,Object> dzz =  generalSqlComponent.query(SqlCode.getDzzInfo,new Object[]{map.get("org_id")});
            Map<String,Object> dy =  generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{map.get("base_info_id")});
            Map<String,Object> bzcy =  generalSqlComponent.query(SqlCode.getDzzBzcy,new Object[]{map.get("base_info_id")});
            if(dzz==null){
                logger.info("用户("+map.get("name")+")的机构信息为空，机构ID="+map.get("org_id"));
            }else{
                Map<String,Object> param = new HashMap<>();
                param.put("account",map.get("acc"));
                final String organcode = dzz.get("organcode").toString().substring(1);

                Map<String,Object> orgInfo = generalSqlComponent.query(SqlCode.queryPorgcode ,new HashMap<String,Object>(){{
                    put("organcode", dzz.get("organcode"));
                }});
                String orgid = String.valueOf(orgInfo.get("orgid"));
                Map<String,Object> user = getUrcUser(param);
                Long userid = null;
                //添加用户信息
                if(user==null){
                    userid =  addUrcUser(getParamUser(map, dy, bzcy));
                }else{
                    Map<String,Object> data = getParamUser(map, dy, bzcy);
                    userid = Long.valueOf(user.get("userid").toString());
                    data.put("userid",userid);
                    updateUrcUser(data);
                }
                param.put("userid",userid);

                //添加账号信息
                Map<String,Object> account = getUrcUserAccount(param);
                if(account==null){
                    addUrcUserAccount(param);
                }else{
                    account.put("userid",userid);
                    updateUrcUserAccount(account);
                }
                //添加用户机构关系
                param.put("orgid",orgid);
                delUrcOrgMap(param);
                addUserOrgMap(param);
                //添加角色关系
                String roleids = map.get("roles")==null ? "" : map.get("roles").toString();
                addRoleMap(userid.toString(), roleids, orgid);
            }
        }
    }

    @Transactional(rollbackFor=Exception.class)
    public void syncUrcOrgan() {
        //查询组织信息 t_dzz_info
        List<Map<String,Object>> dzzLists = generalSqlComponent.query(SqlCode.listDzzInfo,new Object[]{});
        if(dzzLists==null || dzzLists.size()<=0 ){
            return;
        }
        for (Map<String, Object> map : dzzLists) {
            //添加组织信息
            final String userid = map.get("organcode").toString().substring(1);
            delUrcOrganization(new HashMap<String, Object>(){{put("orgid", userid);}});
            addUrcOrganization(getParamOrg(map));
        }
    }

    /**  老角色                        新角色
     * role_party_company 集团     ADMIN      1
     * role_party_secretary	书记 PARTYSECRETARY   7
     * role_party_vicesecretary	副书记  PARTYVICESECRETARY  8
     * role_party_commitmember	委员  PARTYCOMMITMEMBER  9
     * role_party_member	党员  PARTYMEMBER  10
     * role_party_committee，role_party_threecommittee	党委  PARTYCOMMIT  4
     * role_general_party_branch	党总支  PARTYSUPPORT  5
     * role_party_branch	党支部  PARTYBRANCH  6
     */
    private void addRoleMap(String userid, String roleids, String orgid) {
       if(!StringUtils.isEmpty(roleids)){
           if(roleids.contains("role_party_company")){
               Map param = getRoleMap("ADMIN",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",1);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_party_secretary")){
               Map param = getRoleMap("PARTYSECRETARY",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",7);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_party_vicesecretary")){
               Map param = getRoleMap("PARTYVICESECRETARY",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",8);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_party_commitmember")){
               Map param = getRoleMap("PARTYCOMMITMEMBER",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",9);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_party_member")){
               Map param = getRoleMap("PARTYMEMBER",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",10);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_party_committee") || roleids.contains("role_party_threecommittee")){
               Map param = getRoleMap("PARTYCOMMIT",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",4);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_general_party_branch")){
               Map param = getRoleMap("PARTYSUPPORT",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",5);put("orgid",orgid);}});
               }
           }
           if(roleids.contains("role_party_branch")){
               Map param = getRoleMap("PARTYBRANCH",userid);
               if(param==null){
                   addUserRoleMap(new HashMap<String,Object>(){{put("sjid",userid);put("roleid",6);put("orgid",orgid);}});
               }
           }
       }
    }

    private Map getRoleMap(String code,String userid) {
        Map param = generalSqlComponent.query(SqlCode.getUrcRoleMapByRoleCode,new Object[]{code,userid});
        return param;
    }


    private HashMap<String, Object> getParamOrg(Map<String, Object> map) {
        return new HashMap<String, Object>(){
            {
                put("orgid", map.get("organcode").toString().substring(1));
                put("orgname", map.get("dzzmc"));
                put("porgid", map.get("parentcode"));
                put("orgtypeid", map.get("zzlb"));
                put("orgcode", map.get("organcode"));
            }
        };
    }

    private Map<String, Object> getParamUser(Map<String, Object> map, Map<String, Object> dy, Map<String, Object> bzcy) {
        Map<String, Object> data = new HashMap<>();
        data.put("status",map.get("status"));
        data.put("usertype",0);
        data.put("username", map.get("name"));
        data.put("createuserid",1);
        if (dy == null) {
            data.put("sex", 0);
            data.put("idno", "");
        } else {
            /**
             * 0000000002	未知的性别
             * 1000000003	男
             * 2000000004	女
             */
            if (dy == null) {
                data.put("sex", 0);
                data.put("idno", "");
            } else {
                if ("1000000003".equals(dy.get("xb").toString())) {
                    data.put("sex", 1);
                } else if ("2000000004".equals(dy.get("xb").toString())) {
                    data.put("sex", 2);
                } else {
                    data.put("sex", 0);
                }
                data.put("idno", dy.get("zjhm"));
            }
        }
        data.put("phone", map.get("work_phone") == null ? "无" : map.get("work_phone"));
        if (map.get("type")!=null && map.get("type").toString().equals("SYS")) {
            data.put("accflag", 1);
        } else {
            data.put("accflag", 0);
        }
        if (bzcy == null) {
            data.put("postname", 0);
        } else {
            String dnzwname = bzcy.get("dnzwmc").toString();
            if ("5100000017".equals(dnzwname)
                    || "3100000009".equals(dnzwname)
                    || "4100000013".equals(dnzwname)) {
                data.put("postname", 1);
            } else if ("5100000018".equals(dnzwname)
                    || "4200000014".equals(dnzwname)
                    || "3200000010".equals(dnzwname)) {
                data.put("postname", 2);
            } else if ("5300000022".equals(dnzwname)
                    || "4300000015".equals(dnzwname)
                    || "3400000021".equals(dnzwname)) {
                data.put("postname", 3);
            } else {
                data.put("postname", 0);
            }
        }

        return data;
    }

    /**
     * 添加组织信息
     * @param param
     */
    private void addUrcOrganization(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.addUrcOrganization,param);
    }

    /**
     * 添加用户信息
     * @param param
     */
    private Long addUrcUser(Map<String,Object> param) {
        return generalSqlComponent.insert(SqlCode.addUrcUser,param);
    }

    private void updateUrcUser(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.updateUrcUser,param);
    }

    /**
     * 添加用户账号信息
     * @param param
     */
    private void addUrcUserAccount(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.addUrcUserAccount,param);
    }

    private void updateUrcUserAccount(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.updateUrcUserAccount,param);
    }

    /**
     * 添加用户机构关系
     * @param param
     */
    private void addUserOrgMap(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.addUserOrgMap3,param);
    }

    /**
     * 添加用户角色关系
     * @param param
     */
    private void addUserRoleMap(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.addUserRoleMap,param);
    }

    /**
     * 修改组织信息
     * @param param
     */
    private void delUrcOrganization(Map<String,Object> param) {
        generalSqlComponent.update(SqlCode.delUrcOrganization,param);
    }

    /**
     * 修改组织信息
     * @param param
     */
    private void modiUrcOrganization(Map<String,Object> param) {
        generalSqlComponent.update(SqlCode.modiUrcOrganization,param);
    }

    /**
     * 修改用户信息
     * @param param
     */
    private void delUrcUser(Map<String,Object> param) {
        generalSqlComponent.update(SqlCode.delUrcUsers,param);
    }

    private Map<String,Object> getUrcUser(Map<String,Object> param) {
        return generalSqlComponent.update(SqlCode.getUrcUsers,param);
    }

    /**
     * 修改用户账号信息
     * @param param
     */
    private void delUrcUserAccount(Map<String,Object> param) {
        generalSqlComponent.insert(SqlCode.delUrcUserAccount,param);
    }

    private Map<String,Object> getUrcUserAccount(Map<String,Object> param) {
        return generalSqlComponent.query(SqlCode.getUrcUserAccount,param);
    }


    /**
     * 修改用户机构关系
     * @param param
     */
    private void delUrcOrgMap(Map<String,Object> param) {
        generalSqlComponent.update(SqlCode.delUrcOrgMap,param);
    }

    /**
     * 修改用户角色关系
     * @param param
     */
    private void delUserRoleMap(Map<String,Object> param) {
        generalSqlComponent.update(SqlCode.delUserRoleMap,param);
    }


    @Resource private DBComponent sqlfgDBComponent;

    /**
     * 同步组织机构简称
     */
    @Transactional(rollbackFor = Exception.class)
    public void synchronize() {
        //查询机构和用户信息
        List<Map<String,Object>> lists1 = sqlfgDBComponent.listMap("SELECT orgid,substring(orgname,13) orgname,sorgname FROM urc_organization WHERE orgname LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists1!=null && lists1.size()>0) {
            for (Map map : lists1) {
                sqlfgDBComponent.update("UPDATE urc_organization SET sorgname = ?  WHERE orgid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("orgid")});
            }
        }

        List<Map<String,Object>> lists11 = sqlfgDBComponent.listMap("SELECT orgid,substring(orgname,7) orgname,sorgname FROM urc_organization WHERE orgname LIKE '%中共淮北矿业%' and orgname not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists11!=null && lists11.size()>0) {
            for (Map map : lists11) {
                sqlfgDBComponent.update("UPDATE urc_organization SET sorgname = ?  WHERE orgid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("orgid")});
            }
        }
        List<Map<String,Object>> lists2 = sqlfgDBComponent.listMap("SELECT orgid,substring(orgname,5)  orgname,sorgname  FROM urc_organization WHERE orgname NOT LIKE '%中共淮北矿业%' AND orgname LIKE '%中共淮北%' and orgname not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists2!=null && lists2.size()>0) {
            for (Map map : lists2) {
                sqlfgDBComponent.update("UPDATE urc_organization SET sorgname = ?  WHERE orgid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("orgid")});
            }
        }
        List<Map<String,Object>> lists3 = sqlfgDBComponent.listMap("SELECT orgid,substring(orgname,3) orgname,sorgname FROM urc_organization WHERE orgname LIKE '%中共%' AND orgname NOT LIKE '%中共淮北矿业%' AND orgname NOT LIKE '%中共淮北%' and orgname not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists3!=null && lists3.size()>0) {
            for (Map map : lists3) {
                sqlfgDBComponent.update("UPDATE urc_organization SET sorgname = ?  WHERE orgid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("orgid")});
            }
        }
        sqlfgDBComponent.update("UPDATE t_dzz_info a ,urc_organization b SET a.dxzms=b.orgname where a.organcode=b.orgcode", new Object[] {});
        List<Map<String,Object>> lists4 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,13) orgname FROM t_dzz_info WHERE dzzmc LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists4!=null && lists4.size()>0) {
            for (Map map : lists4) {
                sqlfgDBComponent.update("UPDATE t_dzz_info SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
        List<Map<String,Object>> lists14 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,7) orgname FROM t_dzz_info WHERE dzzmc LIKE '%中共淮北矿业%' and dzzmc not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists14!=null && lists14.size()>0) {
            for (Map map : lists14) {
                sqlfgDBComponent.update("UPDATE t_dzz_info SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
        List<Map<String,Object>> lists5 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,5) orgname FROM t_dzz_info WHERE dzzmc NOT LIKE '%中共淮北矿业%' AND dzzmc LIKE '%中共淮北%' and dzzmc not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists5!=null && lists5.size()>0) {
            for (Map map : lists5) {
                sqlfgDBComponent.update("UPDATE t_dzz_info SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
        List<Map<String,Object>> lists6 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,3) orgname FROM t_dzz_info WHERE dzzmc LIKE '%中共%' AND dzzmc NOT LIKE '%中共淮北矿业%' AND dzzmc NOT LIKE '%中共淮北%' and dzzmc not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists6!=null && lists6.size()>0) {
            for (Map map : lists6) {
                sqlfgDBComponent.update("UPDATE t_dzz_info SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }

        List<Map<String,Object>> lists7 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,13) orgname FROM t_dzz_info_simple WHERE dzzmc LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists7!=null && lists7.size()>0) {
            for (Map map : lists7) {
                sqlfgDBComponent.update("UPDATE t_dzz_info_simple SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
        List<Map<String,Object>> lists17 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,7) orgname FROM t_dzz_info_simple WHERE dzzmc LIKE '%中共淮北矿业%' and dzzmc not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists17!=null && lists17.size()>0) {
            for (Map map : lists17) {
                sqlfgDBComponent.update("UPDATE t_dzz_info_simple SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
        List<Map<String,Object>> lists8 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,5) orgname FROM t_dzz_info_simple WHERE dzzmc NOT LIKE '%中共淮北矿业%' AND dzzmc LIKE '%中共淮北%' and dzzmc not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists8!=null && lists8.size()>0) {
            for (Map map : lists8) {
                sqlfgDBComponent.update("UPDATE t_dzz_info_simple SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
        List<Map<String,Object>> lists9 = sqlfgDBComponent.listMap("SELECT organid, substring(dzzmc,3) orgname FROM t_dzz_info_simple WHERE dzzmc LIKE '%中共%' AND dzzmc NOT LIKE '%中共淮北矿业%' AND dzzmc NOT LIKE '%中共淮北%' and dzzmc not LIKE '%中共淮北矿业股份有限公司%'", new Object[] {});
        if(lists9!=null && lists9.size()>0) {
            for (Map map : lists9) {
                sqlfgDBComponent.update("UPDATE t_dzz_info_simple SET dzzmc=?  WHERE organid = ?", new Object[] {map.get("orgname").toString().substring(0,map.get("orgname").toString().length()-3),map.get("organid")});
            }
        }
    }
}
