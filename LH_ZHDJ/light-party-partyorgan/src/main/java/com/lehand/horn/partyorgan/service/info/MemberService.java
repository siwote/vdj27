package com.lehand.horn.partyorgan.service.info;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.HJXXSqlCode;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.MemberSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.*;
import com.lehand.horn.partyorgan.pojo.SysUser;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfoBc;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfoChildModule;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfoDzzDj;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfoGood;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
import com.lehand.horn.partyorgan.service.DataSynchronizationService;
import com.lehand.horn.partyorgan.util.ExcelUtils;
import com.lehand.horn.partyorgan.util.OrgUtil;
import com.lehand.horn.partyorgan.util.SysUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 党员信息业务层
 * @author admin
 *
 */
@Service
public class MemberService {


    @Resource
    GeneralSqlComponent generalSqlComponent;

    @Resource
    private DBComponent sqlfgDBComponent;
    @Resource
    private PartyCacheComponent partyCacheComponent;

    @Resource
    DataSynchronizationService dataSynchronizationService;

    @Resource
    MemberExcelService memberExcelService;

    @Resource private OrgUtil orgUtil;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")
    private String downIp;

    private String sql = "SELECT * FROM g_dict_item where code = ? and item_name=? limit 1";
    /**
     * 分页查询
     */
    @SuppressWarnings("unchecked")
	public Pager query(Session session, MemberSearchReq req, Pager pager){

        //校验是否带模糊查询条件(待条件就全局查询)
        if(!StringUtils.isEmpty(req.getXm()) || !StringUtils.isEmpty(req.getZjhm())
                || !StringUtils.isEmpty(req.getXl()) || !StringUtils.isEmpty(req.getDylb())
                || !StringUtils.isEmpty(req.getStartage()) || !StringUtils.isEmpty(req.getEndage()) ){
            String path = orgUtil.getPath(req.getOrganid());
            req.setPath(path);
            generalSqlComponent.pageQuery(MemberSqlCode.member_page2,req,pager);
        }else{
            if(StringUtils.isEmpty(req.getType())){
                pager = generalSqlComponent.pageQuery(MemberSqlCode.member_page,req,pager);
                List<MemberInfoDto> list = (List<MemberInfoDto>)pager.getRows();
                list = makeList(list);
                pager.setRows(list);
                return pager;
            }
            Integer pathNum  = getPathNum(req.getOrganid());
            if(pathNum<1){
                return pager;
            }
            String path = MagicConstant.STR.PATH_STR+pathNum;
            req.setPath(path);
            pager = generalSqlComponent.pageQuery(MemberSqlCode.member_page_childs,req,pager);
        }
        List<MemberInfoDto> list = (List<MemberInfoDto>)pager.getRows();
        list = makeList(list);
        pager.setRows(list);
        return pager ;

    }

    /**
     * 获取党员信息列表
     * @param req
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<MemberInfoDto> getMemberInfoList(MemberSearchReq req,Pager pager) {
        if(StringUtils.isEmpty(req.getType())) {
            pager = generalSqlComponent.pageQuery(MemberSqlCode.member_page, req, pager);
        }else{
            Integer pathNum  = getPathNum(req.getOrganid());
            if(pathNum<1){
                pathNum = 1;
            }
            String path = MagicConstant.STR.PATH_STR+pathNum;
            req.setPath(path);
            pager =  generalSqlComponent.pageQuery(MemberSqlCode.member_page_childs,req,pager);
        }
        List<MemberInfoDto> list = (List<MemberInfoDto>)pager.getRows();
        if(list.size()<1){
            return list;
        }

        list = makeList(list);

        return list;
    }

    /**
     * 处理编码名称转换
     * @param list
     * @return
     */
    public List<MemberInfoDto> makeList(List<MemberInfoDto> list) {
        for (MemberInfoDto dto: list) {
            dto.setUuid(dto.getUserid());
            dto.setMzname(String.valueOf(partyCacheComponent.getName("d_national",dto.getMz())));
//			dto.setAge(SysUtil.getAgeFromBirthTime(dto.getCsrq()));
            dto.setXbname(String.valueOf(partyCacheComponent.getName("d_sexid",dto.getXb())));
            dto.setRylbname(String.valueOf(partyCacheComponent.getName("d_dy_rylb",dto.getDylb())));
            dto.setLostreasonname(String.valueOf(partyCacheComponent.getName("d_dy_cgyzzlx",dto.getLostreason())));
            dto.setXlname(String.valueOf(partyCacheComponent.getName("d_dy_xl",dto.getXl())));
            if (StringUtils.isEmpty(partyCacheComponent.getName("d_dy_gzgw", dto.getGzgw()))) {
                dto.setGzgwname(dto.getGzgw());
            } else {
                dto.setGzgwname(partyCacheComponent.getName("d_dy_gzgw",dto.getGzgw()));
            }
            dto.setXwname(partyCacheComponent.getName("d_dy_xw",dto.getXw()));
            dto.setJszcname(partyCacheComponent.getName("d_dy_jszc",dto.getJszc()));
            dto.setXshjclxname(partyCacheComponent.getName("d_dy_shjc",dto.getXshjclx()));
            dto.setIsworkers("0".equals(dto.getIsworkers()) ? "否" : "是");
            dto.setPovertyParty("0".equals(dto.getPovertyParty()) ? "否" : "是");
            dto.setDemonstrationParty("0".equals(dto.getDemonstrationParty()) ? "否" : "是");
//            if(MagicConstant.STR.EMPTY_STRS.equals(String.valueOf(dto.getCsrq()))||StringUtils.isEmpty(dto.getCsrq())){
//                dto.setAge(0);
//            }else{
//                dto.setAge(SysUtil.getAgeFromBirthTime(dto.getCsrq()));
//            }
            //年龄
            if(dto.getAge()<=0){
                if(MagicConstant.STR.EMPTY_STRS.equals(String.valueOf(dto.getCsrq()))||StringUtils.isEmpty(dto.getCsrq())){
                    dto.setAge(0);
                }else{
                    dto.setAge(SysUtil.getAgeFromBirthTime(String.valueOf(dto.getCsrq())));
                }
            }
        }
        return list;
    }

    /**
     * 党员统计页面
     * @param session
     * @param req
     * @param pager
     * @return
     */
    public Pager queryAnalysis(Session session, MemberSearchReq req, Pager pager){
        Integer pathNum  = getPathNum(req.getOrganid());
        if(pathNum<1){
            return pager;
        }
        String path = MagicConstant.STR.PATH_STR+pathNum;

        //当前单位统计
        Map<String,Object> param = new HashMap<String,Object>(3);
        param.put("path",path);
        param.put("organidx",req.getOrganid());
        param.put("organid",req.getOrganid());
        MemberAnalysisDto currAnalysis = generalSqlComponent.query(MemberSqlCode.get_current_analysis,param);

        List<MemberAnalysisDto> result =new ArrayList<MemberAnalysisDto>();

        int count = generalSqlComponent.query(SqlCode.countTdzzInfoSimpleByPid,new Object[]{req.getOrganid()});

        List<String> orgids = generalSqlComponent.query(SqlCode.getOrgidsByPid,new Object[]{req.getOrganid()});
        if(orgids.size()<1){
            pager.setRows(result);
            return pager;
        }


        //第一页时
        int firstPageSize = pager.getPageSize()-1;

        //不是第一页时
        int startRowNo =  firstPageSize + (pager.getPageNo()-2)*pager.getPageSize();
        int endRowNo = startRowNo + pager.getPageSize();

        if(startRowNo>count){
            startRowNo = count;
            endRowNo = startRowNo;
        }
        if(endRowNo>count){
            endRowNo = count;
        }

        List<String> organids = new ArrayList<String>();
        if(pager.getPageNo()==1){//第一页,插入统计总数
            organids = orgids.subList(0, firstPageSize>count?count:firstPageSize);
            result.add(0,currAnalysis);
        }else{
            organids =  orgids.subList(startRowNo,endRowNo);
        }

        if(organids.size()<1){
            pager.setRows(result);
            return pager;
        }

        //查询下一级统计总数
        for (String orgid: organids) {
            Map<String,Object> params = new HashMap<String,Object>(3);
            params.put("organidx",orgid);
            params.put("organid",orgid);
            String childPath = MagicConstant.STR.PATH_STR+(pathNum+1);
            params.put("path",childPath);
            MemberAnalysisDto childAnalysis = generalSqlComponent.query(MemberSqlCode.get_current_analysis,params);
            result.add(childAnalysis);
        }
        count +=1 ;
        //将当前单位插入数组第一条
        pager.setRows(result);
        pager.setTotalRows(count);
        int i = count % pager.getPageSize();
        int v = count / pager.getPageSize();
        pager.setTotalPages(i>0 ? v+1 : v);

        return pager;
    }



    /**
     *
     * @Description: 获取党员基本信息
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月22日 下午7:16:44
     */
    public List<Map<String,Object>> queryEssential(String userId){

        List<Map<String,Object>> data = generalSqlComponent.query(MemberSqlCode.get_base,new Object[]{userId});
        if(data.size()<1){
            return data;
        }

        for(Map<String,Object> map :data) {


            map.put("mzName",partyCacheComponent.getName("d_national",map.get("mz")));
            //年龄
            if(StringUtils.isEmpty(map.get("age"))){
                if(MagicConstant.STR.EMPTY_STRS.equals(String.valueOf(map.get("csrq")))||StringUtils.isEmpty(map.get("csrq"))){
                    map.put("age",MagicConstant.STR.EMPTY_STR);
                }else{
                    map.put("age",String.valueOf(SysUtil.getAgeFromBirthTime(String.valueOf(map.get("csrq")))));
                }
            }else{
                map.put("age",map.get("age"));
            }
            //党龄
            if(MagicConstant.STR.EMPTY_STRS.equals(String.valueOf(map.get("zzsj")))||StringUtils.isEmpty(map.get("zzsj"))){
                map.put("partyAge",MagicConstant.STR.EMPTY_STR);
            }else{
                map.put("partyAge",String.valueOf(SysUtil.getAgeFromBirthTime(String.valueOf(map.get("zzsj")))));
            }

            map.put("xlName",partyCacheComponent.getName("d_dy_xl",map.get("xl")));

            map.put("xwName",partyCacheComponent.getName("d_dy_xw",map.get("xwDm")));

            map.put("dnzwsm",partyCacheComponent.getName("d_dy_dnzw",map.get("dnzwname")));
            String name = partyCacheComponent.getName("d_dy_gzgw", map.get("gzgw"));
            map.put("gzgw",StringUtils.isEmpty(name)?map.get("gzgw"):name);

            map.put("jszcName",partyCacheComponent.getName("d_dy_jszc",map.get("jszc")));

            map.put("xshjclx",String.valueOf(map.get("xshjclx")).trim());

            map.put("xshjclxName",partyCacheComponent.getName("d_dy_shjc",map.get("xshjclx")));

            map.put("cszyjszwName",partyCacheComponent.getName("d_dy_jszc",map.get("cszyjszw_dm")));

            map.put("dylbName",partyCacheComponent.getName("d_dy_rylb",map.get("dylb")));

            map.put("xb",partyCacheComponent.getName("d_sexid",map.get("xb")));


        }
        return data;
    }

    /**
     *
     * @Description:  查询党籍和党组织
     * @param userId
     * @param organid
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月22日 下午8:05:04
     */
    public List<Map<String,Object>> queryPartyMembershipOrg(String userId,String organid){
        /**
         * 定义list数组
         */
        List<Map<String,Object>> list = new ArrayList<>();

        list = generalSqlComponent.query(MemberSqlCode.get_membership, new Object[]{userId});
        if(list.size()>0) {
            for (Map<String, Object> map : list) {
                map.put("partyAge", StringUtils.isEmpty(map.get("zzsj")) ? map.get("zzsj") : SysUtil.getAgeFromBirthTime(String.valueOf(map.get("zzsj"))));
                map.put("rdszdzbmc", map.get("rdszdzbmc") == null ? "" : map.get("rdszdzbmc"));
                map.put("rdjsr", map.get("rdjsr") == null ? "" : map.get("rdjsr"));
                map.put("zzqk", map.get("zzqk") == null ? "" : map.get("zzqk") + " " + partyCacheComponent.getName("d_fzdy_scjg" , map.get("zzqk")));
                map.put("qtdt", map.get("qtdt") == null ? map.get("qtdt") : map.get("qtdt")+ " " + partyCacheComponent.getName("d_dy_qtdt" , map.get("qtdt")));
            }
        }
        /**
         * 流入党组织
         */
        List<Map<String,Object>> listLrdy = new ArrayList<>();
        Map<String,Object> mapLrdy = new HashMap<>();


        listLrdy = generalSqlComponent.query(MemberSqlCode.get_membership_in, new Object[]{userId,organid});

        list.add(listLrdy.size()>0? listLrdy.get(0):mapLrdy);
        /**
         * 流出党组织
         */
        List<Map<String,Object>> listLcdy = new ArrayList<>();
        Map<String,Object> mapLcdy = new HashMap<>();

        listLcdy = generalSqlComponent.query(MemberSqlCode.get_membership_out, new Object[]{userId,organid});
        list.add(listLcdy.size()>0? listLcdy.get(0):mapLcdy);
        return list;
    }

    /**
     *
     * @Description: 职务
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月23日 下午8:50:51
     */
    public Map<String,Object> queryPost(String userId){
        /**
         * 定义list数组
         */
        Map<String,Object> result = new HashMap<String,Object>();

        List<Map<String,Object>> zwlist = new ArrayList<>();

        List<Map<String,Object>> lzzlist = new ArrayList<>();

        List<Map<String,Object>> list = generalSqlComponent.query(MemberSqlCode.get_post, new Object[]{userId});

        if(list.size()<1){
            return result;
        }
        for(Map<String,Object> map:list) {
            Map<String,Object> zwmap = new HashMap<String,Object>();

            zwmap.put("dnzwname",partyCacheComponent.getName("d_dy_dnzw" ,map.get("dnzwname")));
            zwmap.put("zwlevel",partyCacheComponent.getName("d_dzz_zwjb" , map.get("zwlevel")));
            zwmap.put("rzdate",  map.get("rzdate"));
            zwmap.put("lzdate",  map.get("lzdate"));
            zwmap.put("organname",  map.get("organname"));
            zwmap.put("dnzwsm",  map.get("dnzwsm"));
            zwmap.put("sfzr",  "是");

            zwlist.add(zwmap);
            Map<String,Object> lzzmap = new HashMap<String,Object>();
            lzzmap.put("ldbywyzzyy", partyCacheComponent.getName("d_dy_cdyy",map.get("ldbywyzzyy")));
            lzzmap.put("ldbywyzzrq", map.get("ldbywyzzrq"));
            lzzmap.put("ldbywyjmrq", map.get("ldbywyjmrq"));
            lzzmap.put("ldbywylb",partyCacheComponent.getName("d_dy_ldbywy",map.get("ldbywylb")));
            lzzmap.put("ksrq", map.get("ksrq"));
            lzzmap.put("sfzr", MagicConstant.NUM_STR.NUM_1.equals(map.get("sfzr"))?"是":"否");
            lzzmap.put("jc", map.get("jc"));
            lzzlist.add(lzzmap);
        }
        result.put("zwlist",zwlist);
        result.put("lzzlist",lzzlist);
        return result;
    }

    /**
     *
     * @Description: 奖惩信息
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月24日 上午9:12:06
     */
    public List<Map<String,Object>> queryJcxx(String userId){
        /**
         * 定义list数组
         */
        List<Map<String,Object>> list = new ArrayList<>();

        list =generalSqlComponent.query(MemberSqlCode.get_rewards, new Object[]{userId});
        if(list.size()<1){
            return list;
        }
        for(Map<String,Object> map:list) {
            map.put("jcmc", partyCacheComponent.getName("d_dy_jcmc" , map.get("jcmc")));
            map.put("pzrq",String.valueOf(map.get("pzrq")).replace("00:00:00", MagicConstant.STR.EMPTY_STR));
        }
        return list;

    }

    /**
     *
     * @Description: 困难情况
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月24日 上午9:29:32
     */
    public List<Map<String,Object>> queryknqk(String userId){
        /**
         * 定义list数组
         */
        List<Map<String,Object>> list = new ArrayList<>();
        list = generalSqlComponent.query(MemberSqlCode.get_difficulty, new Object[]{userId});

        if(list.size()<1){
            return list;
        }
        for(Map<String,Object> map:list) {
            map.put("shknlx",  partyCacheComponent.getName("d_dy_shkn", map.get("shknlx")));
            map.put("sfxsczzzshbz",  "1".equals(map.get("sfxsczzzshbz"))? "是":("0".equals(map.get("sfxsczzzshbz"))? "否":null));
            map.put("fxbz",  "1".equals(map.get("fxbz"))? "是":("0".equals(map.get("fxbz"))? "否":null));
            map.put("jkzk",  partyCacheComponent.getName("d_dy_jkzk" , map.get("jkzk")));
        }
        return list;
    }

    /**
     *
     * @Description: 出国出境
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月24日 上午9:55:41
     */
    public List<Map<String,Object>> queryCgcj(String userId){
        /**
         * 定义list数组
         */
        List<Map<String,Object>> list = new ArrayList<>();

        list =generalSqlComponent.query(MemberSqlCode.get_go_abroad, new Object[]{userId});
        if(list.size()<1){
            return list;
        }
        for(Map<String,Object> map:list) {
            map.put("szgjmc",  partyCacheComponent.getName("d_dy_country" , map.get("szgjmc")));
            map.put("cgyy",  partyCacheComponent.getName("d_dy_cgyy" , map.get("cgyy")));
            map.put("ydzzlxqk",  partyCacheComponent.getName("d_dy_cgyzzlx" , map.get("ydzzlxqk")));
            map.put("djclfs",  partyCacheComponent.getName("d_dy_djcl" , map.get("djclfs")));
            map.put("hgqk",  partyCacheComponent.getName("d_dy_hgqk" , map.get("hgqk")));
            map.put("hfzzshqk",  partyCacheComponent.getName("d_dy_hfzzsh" , map.get("hfzzshqk")));
            map.put("cgrq",String.valueOf(map.get("cgrq")).replace("00:00:00", MagicConstant.STR.EMPTY_STR));
            map.put("hgrq",String.valueOf(map.get("hgrq")).replace("00:00:00", MagicConstant.STR.EMPTY_STR));
        }
        return list;
    }

    /**
     *
     * @Description:  入党信息
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月24日 上午11:38:26
     */
    public List<Map<String,Object>> queryRdxx(String userId){
        /**
         * 定义list数组
         */
        List<Map<String,Object>> list = new ArrayList<>();

        list = generalSqlComponent.query(MemberSqlCode.get_join_party,new Object[]{userId});

        if(list.size()<1){
            return list;
        }
        for(Map<String,Object> map:list) {
            map.put("fzsxl",  partyCacheComponent.getName("d_dy_xl" , map.get("fzsxl")));
            map.put("fzsgzgw",  partyCacheComponent.getName("d_dy_gzgw" , map.get("fzsgzgw")));
            map.put("fzsjszw", partyCacheComponent.getName("d_dy_jszc" , map.get("fzsjszw")));
            map.put("fzsyxqk",  partyCacheComponent.getName("d_dy_yxqk" , map.get("fzsyxqk")));
            map.put("fzsxshjc",  partyCacheComponent.getName("d_dy_shjc" , map.get("fzsxshjc")));
            map.put("dhtljg",  "1".equals(map.get("dhtljg"))? "通过": "不通过");
            map.put("rdlx", partyCacheComponent.getName("d_dy_jrzz" , map.get("rdlx")));
            map.put("zzqk",  partyCacheComponent.getName("d_dy_zzqk" , map.get("zzqk")));
            map.put("ycybqsj",  partyCacheComponent.getName("d_dy_ycybqsj" , map.get("ycybqsj")));

        }
        return list;
    }

    /**
     *
     * @Description:流动党员
     * @param userId
     * @return
     * @return List<Map<String,Object>>
     * @throws
     * @author zouwendong
     * @date:   2019年1月24日 下午9:38:04
     */
    public List<Map<String,Object>> queryLddy(String userId){
        /**
         * 定义list数组
         */
        List<Map<String,Object>> list = new ArrayList<>();

        list =generalSqlComponent.query(MemberSqlCode.get_flows, new Object[]{userId});
        for(Map<String,Object> map:list) {
            map.put("wcdd",  partyCacheComponent.getName("d_dzz_xzqh" , map.get("wcdd")));
            map.put("wclx",  partyCacheComponent.getName("d_dy_wclx" , map.get("wclx")));
            map.put("ldzt",  partyCacheComponent.getName("d_dy_ldzt" , map.get("ldzt")));
            map.put("sqlxqx",  partyCacheComponent.getName("d_dy_sljtqx" , map.get("sqlxqx")));
            map.put("lxqk",  partyCacheComponent.getName("d_dy_cgyzzlx" , map.get("lxqk")));
            map.put("wcrq",String.valueOf(map.get("wcrq")).replace("00:00:00", MagicConstant.STR.EMPTY_STR));
            map.put("fhrq",String.valueOf(map.get("fhrq")).replace("00:00:00", MagicConstant.STR.EMPTY_STR));
        }
        return list;
    }


    /**
     * 获取组织机构Path
     * @param orgId
     * @return
     * @author taogang
     */
    public Integer getPathNum(String orgId) {
        Integer num = 0;
        if(!StringUtils.isEmpty(orgId)) {
            TDzzInfoSimple org =generalSqlComponent.query(MemberSqlCode.get_path_num,new Object[]{orgId});
            if(null != org) {
                String[] oarr = org.getIdPath().split("/");
                num = oarr.length-1;
            }
        }
        return num;
    }

    /**
     * 根据code获取编码
     * @param code
     * @return
     */
    public List<Map<String,Object>> getDictItemByCode(String code) {
        List<Map<String,Object>> list = new ArrayList<>();
        list =  generalSqlComponent.query(MemberSqlCode.get_item_by_code,new Object[]{code});
        Map<String,Object> map =  new HashMap<String,Object>(1);
        map.put("id", MagicConstant.STR.EMPTY_STR);
        map.put("name","全部");
        list.add(0,map);
        return list;
    }

    public Pager pageQuery(Session session,String organid,String dylb,String xm,String zjhm,String xl,String startage,String endage, Pager pager){
        Map<String,Object> parms = new HashMap<String,Object>();
        parms.put("organid",organid);
        parms.put("dylb",dylb);
        parms.put("xm",xm);
        parms.put("zjhm",zjhm);
        parms.put("xl",xl);
        parms.put("startage",startage);
        parms.put("endage",endage);

        pager = generalSqlComponent.pageQuery(MemberSqlCode.getTDyInfoList2,parms,pager);
        List<TDyInfo> list= (List<TDyInfo>) pager.getRows();
        for(TDyInfo li: list){
            li.setMz(partyCacheComponent.getName("d_national",li.getMz()));
            li.setAge(partyCacheComponent.getName("d_national",li.getMz()));
            if(MagicConstant.STR.EMPTY_STRS.equals(String.valueOf(li.getAge()))||StringUtils.isEmpty(li.getAge())){
                li.setAge(MagicConstant.STR.EMPTY_STR);
            }
            li.setXl(partyCacheComponent.getName("d_dy_xl",li.getXl()));


//            map.put("xwName",partyCacheComponent.getName("d_dy_xw",map.get("xw")));
//
//
//            map.put("dnzwsm",partyCacheComponent.getName("d_dy_dnzw",map.get("dnzwname")));
//
//
//            map.put("gzgwName",partyCacheComponent.getName("d_dy_gzgw",map.get("gzgw")));
//
//            map.put("jszcName",partyCacheComponent.getName("d_dy_jszc",map.get("jszc")));
//
//
//            map.put("xshjclxName",partyCacheComponent.getName("d_dy_shjc",map.get("xshjclx")));
//
//            map.put("dylbName",partyCacheComponent.getName("d_dy_rylb",map.get("dylb")));
//
//            map.put("xb",partyCacheComponent.getName("d_sexid",map.get("xb")));
        }
        return pager;
    }

    public TDyInfoDzzDj getDyInfoDzzDj(String userid,Session session){
        return generalSqlComponent.query(SqlCode.getTDyInfoDzzDj,new Object[]{userid});
    }

    public Pager getTDyJcxx(Pager pager,String userid,Integer rewardspunishments,Session session){
        generalSqlComponent.pageQuery(SqlCode.getTDyJcxx,new Object[]{userid,rewardspunishments},pager);
        List<Map<String,Object>> rows = (List<Map<String, Object>>) pager.getRows();
        for (Map<String,Object> map: rows) {
            //奖惩原因
            String jcyy = (String) map.get("jcyy");
            StringBuffer sb = new StringBuffer();
            if(!StringUtils.isEmpty(jcyy)){
                String[] split = jcyy.split(",");
                for (String ItemCode: split) {
                    Map<String,Object> m =  generalSqlComponent.getDbComponent().getMap(SqlCode.getGDictItemByItemCode, new Object[]{"d_dy_jcyy", ItemCode});
                    if(m!=null){
                        sb.append(m.get("item_name")).append(",");
                    }
                }
                map.put("jcyyName",sb.toString().substring(0, sb.toString().length() - 1));
            }
            //附件
            String jcqdyswj = (String) map.get("jcqdyswj");
            List<Map<String, Object>> listFile = new ArrayList<>();
            if(jcqdyswj!=null){
                String[] split = jcqdyswj.split(",");
                for (String fileId: split) {
                    Map<String,Object> m =  generalSqlComponent.query(SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",session.getCompid());put("id",fileId);}});
                    if(m!=null){
                        listFile.add(m);
                    }
                }
            }
            map.put("listFile",listFile);
        }

        return pager;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean addTDyInfo(TDyInfoReq bean,Session session){
        bean.setUserid(UUID.randomUUID().toString().replaceAll("-", ""));
        bean.setOperatetype("1");
        bean.setSyncstatus("A");
        bean.setDelflag("0");
        bean.setDyzt("1000000003");
        String zjhm = bean.getZjhm();
        Map<String,Object> data = generalSqlComponent.query(SqlCode.getTDyInfoZjhm3,new Object[]{zjhm});
        if(data!=null){
            LehandException.throwException("身份证号："+ zjhm+"党员已存在");
            return false;
        }
        //t_dy_info
        generalSqlComponent.insert(SqlCode.insert_t_dy_info,bean);
        TDyInfoBc dybc = new TDyInfoBc();
        dybc.setUserid(bean.getUserid());
        dybc.setXw(bean.getXw());
        dybc.setByyx(bean.getByyx());
        dybc.setZy(bean.getZy());
        dybc.setJszc(bean.getJszc());
        dybc.setXshjclx(bean.getXshjclx());
        dybc.setDydaszdw(bean.getDydaszdw());
        dybc.setXjzd(bean.getXjzd());
        dybc.setHjdzQhnxxdz(bean.getHjdz_qhnxxdz());
        dybc.setSyncstatus("A");
        dybc.setRdjsr(bean.getRdjsr());
        dybc.setQtdt(bean.getQtdt());
        dybc.setJrqtdtrq(bean.getJrqtdtrq());
        dybc.setLkqtdtrq(bean.getLkqtdtrq());
        //t_dy_info_bc
        generalSqlComponent.insert(SqlCode.addTDyInfoBc,dybc);

        //sys_user
        Map<String,Object> dzzinfo= generalSqlComponent.query(SqlCode.getDzzInfo,new Object[]{bean.getOrganid()});
        SysUser user = new SysUser();
        user.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        user.setAcc(bean.getZjhm());
        user.setPwd("0ffe6471a68f774d03695b1e536c2da5");
        user.setSalt("448d7c19946648e5aad3b9b7b3f7a39a");
        user.setRoles("role_party_member");
        user.setName(bean.getXm());
        user.setOrgId(bean.getOrganid());
        user.setOrgName(dzzinfo.get("dzzmc").toString());
        user.setType("PUB");
        user.setWorkPhone(bean.getLxdh());
        user.setStatus(1);
        user.setOrderId(100l);
        user.setBaseInfoId(bean.getUserid());
        user.setCreateTime(new Date());
        user.setModiTime(new Date());
        user.setCreater(session.getUserid().toString());
        user.setModifier(session.getUserid().toString());
        generalSqlComponent.insert(SqlCode.addSysuser,user);
        //查询sys_user
        List<Map<String,Object>> listmap = generalSqlComponent.query(SqlCode.getSysUserList,new Object[]{bean.getUserid()});
        //同步数据
        dataSynchronizationService.syncUserInfos2(listmap);
        return true;
    }

    public Boolean addTDyInfoDzzDj(TDyInfoDzzDjReq bean,Session session){
        generalSqlComponent.insert(SqlCode.addDjDzz,bean);
        return true;
    }

    public Boolean addTDyJcxx(TDyJcxxReq bean,Session session){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        bean.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
        bean.setOperator(session.getUserid().toString());
        bean.setOperatetime(sdf.format(new Date()));
        bean.setSyncstatus("A");
        bean.setSynctime(sdf.format(new Date()));
        bean.setZfbz("0");
        generalSqlComponent.insert(SqlCode.addTDyJcxx,bean);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean updateTDyInfo(TDyInfoReq bean,Session session){
        //身份证验证
        String zjhm = bean.getZjhm();
        String userid = bean.getUserid();
        Map<String,Object> data =  generalSqlComponent.getDbComponent().getMap(SqlCode.getTDyInfoZjhm4,new Object[]{zjhm,userid});
        if(data!=null){
            LehandException.throwException("身份证号："+ zjhm+"党员已存在");
            return false;
        }
        //t_dy_info
        bean.setDyzt("1000000003");
        bean.setDelflag("0");
        generalSqlComponent.update(SqlCode.updateTDyInfo2,bean);
        TDyInfoBc dybc = new TDyInfoBc();
        dybc.setUserid(bean.getUserid());
        dybc.setXw(bean.getXw());
        dybc.setByyx(bean.getByyx());
        dybc.setZy(bean.getZy());
        dybc.setJszc(bean.getJszc());
        dybc.setXshjclx(bean.getXshjclx());
        dybc.setDydaszdw(bean.getDydaszdw());
        dybc.setXjzd(bean.getXjzd());
        dybc.setHjdzQhnxxdz(bean.getHjdz_qhnxxdz());
        dybc.setRdjsr(bean.getRdjsr());
        dybc.setQtdt(bean.getQtdt());
        dybc.setJrqtdtrq(bean.getJrqtdtrq());
        dybc.setLkqtdtrq(bean.getLkqtdtrq());
        //t_dy_info_bc
        Integer count = generalSqlComponent.getDbComponent().getSingleColumn(SqlCode.getTDyInfoBc, Integer.class, new Object[]{bean.getUserid()});
        if(count<1){
            generalSqlComponent.insert(SqlCode.addTDyInfoBc,dybc);
        }
        generalSqlComponent.update(SqlCode.updateTDyInfoBc,dybc);
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("SELECT * FROM t_dzz_info WHERE organid=?", new Object[]{bean.getOrganid()});
        //sys_user
        SysUser singleColumn = generalSqlComponent.getDbComponent().getBean("SELECT * FROM `sys_user` WHERE acc =?", SysUser.class, new Object[]{bean.getOldZJHM()});
        if(singleColumn!=null){
            SysUser user = new SysUser();
            user.setId(singleColumn.getId());
            user.setAcc(bean.getZjhm());
            user.setName(bean.getXm());
            user.setWorkPhone(bean.getLxdh());
            user.setBaseInfoId(bean.getUserid());
            user.setModiTime(new Date());
            user.setStatus(1);
            user.setOrgId(bean.getOrganid());
            user.setOrgName(map!=null?String.valueOf(map.get("dxzms")):"");
            user.setModifier(session.getUserid().toString());
            generalSqlComponent.update(SqlCode.upadteSysUser,user);
        }
        //urc_user
        UrcUser oldUrcUser = generalSqlComponent.getDbComponent().getBean("SELECT * FROM `urc_user` WHERE idno =?", UrcUser.class, new Object[]{bean.getOldZJHM()});
        if(oldUrcUser!=null){
            UrcUser urcUser = new UrcUser();
            urcUser.setUserid(oldUrcUser.getUserid());
            urcUser.setPhone(bean.getLxdh());
            urcUser.setIdno(bean.getZjhm());
            urcUser.setStatus("1");
            generalSqlComponent.update(SqlCode.updateUrcUserPhone, urcUser);
        }
        //查询sys_user
        List<Map<String,Object>> listmap = generalSqlComponent.query(SqlCode.getSysUserList,new Object[]{bean.getUserid()});
        //同步数据
        dataSynchronizationService.syncUserInfos2(listmap);
        return true;
    }

    public Boolean updateTDyInfoDzzDj(TDyInfoDzzDjReq bean,Session session){
        generalSqlComponent.update(SqlCode.updateDjDzz,bean);
        return true;
    }


    public Boolean updateTDyJcxx(TDyJcxxReq bean,Session session){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        bean.setOperator(session.getUserid().toString());
        bean.setOperatetime(sdf.format(new Date()));
        generalSqlComponent.update(SqlCode.updateDyJcxx,bean);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean delTDyInfo(String userid,String zjhm,Session session){

//        String orgcode=session.getCurrentOrg().getOrgcode();
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("SELECT b.organid,b.organcode FROM t_dy_info a ,t_dzz_info b WHERE a.organid =b.organid AND a.userid=?", new Object[]{userid});
        if(map==null){
            LehandException.throwException("该党员或党员党组织不存在！！");
        }
        String orgcode= String.valueOf(map.get("organcode"));
        //t_dy_info
        generalSqlComponent.update(SqlCode.updateTDyInfo,new Object[]{1,userid});

        //t_dy_info_bc
        generalSqlComponent.update(SqlCode.FZDY_BC_DELETE,new Object[]{"D",userid});


        //修改sys_user
        generalSqlComponent.update(SqlCode.updateSysUser,new Object[]{0,userid});

        //修改urc_user
        generalSqlComponent.update(SqlCode.updateUrcUserStatus2,new Object[]{0,zjhm});

        //修改 t_dzz_bzcyxx
        generalSqlComponent.getDbComponent().update("update t_dzz_bzcyxx set zfbz=1 where userid=?",new Object[]{userid});

        //删除党小组里成员
        generalSqlComponent.getDbComponent().delete("delete from t_party_group_member where idcard =?", new Object[]{zjhm});
        //删除缴费记录
        //年
        int year = LocalDate.now().getYear();
        //月
        int month = LocalDate.now().getMonthValue();
        String organid = generalSqlComponent.getDbComponent().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{orgcode});
        //党组织是否上报党费
        Map<String,Object> hdzz =  generalSqlComponent.getDbComponent().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?", new Object[] {session.getCompid(),organid,year,month});
        if(hdzz!=null){
            Integer apstatus = (Integer) hdzz.get("apstatus");
            if(apstatus==0||apstatus==3){
                generalSqlComponent.getDbComponent().delete("delete from h_dy_dues where years =? and months = ? and usercard =?",new Object[]{year,month,zjhm});
            }
        }
        Map<String,Object> mapUrcOrg = generalSqlComponent.getDbComponent().getMap("select * from urc_organization where compid=? and orgcode=?", new Object[] {session.getCompid(),orgcode});
        Map<String, Object> mapUrcUser = generalSqlComponent.getDbComponent().getMap("select * from urc_user where idno =?", new Object[]{zjhm});
        if(mapUrcOrg!=null&&mapUrcUser!=null){
            generalSqlComponent.getDbComponent().update("DELETE FROM urc_role_map WHERE roleid='7' AND orgid=? AND sjid=?",new Object[]{mapUrcOrg.get("orgid"),mapUrcUser.get("userid")});//删除书记角色
        }
        //处理换届提醒
        delHJXXRemindConfig(mapUrcUser,zjhm,orgcode);
        return true;
    }

    private void delHJXXRemindConfig(Map<String, Object> mapUrcUser,String zjhm, String orgcode) {
        //处理换届提醒
        TDzzHjxxInfoDto remindConfig = generalSqlComponent.query(HJXXSqlCode.getHjxxRemindConfig, new Object[]{orgcode});
        if(remindConfig!=null&&mapUrcUser!=null){
            String urcUserid = String.valueOf(mapUrcUser.get("userid"));
            List<Map> lists = JSON.parseArray(remindConfig.getRemindpeople(),Map.class);
            for(int i=0;i<lists.size();i++){
                String userid = String.valueOf(lists.get(i).get("userid"));
                if(urcUserid.equals(userid)){
                    lists.remove(i);
                }
            }
            remindConfig.setRemindpeople(JSON.toJSONString(lists));
            generalSqlComponent.update(HJXXSqlCode.updateHjxxRemindConfig,remindConfig);
        }
    }

    public Boolean delTDyInfoDzzDj(String userid,Session session){
        generalSqlComponent.delete(SqlCode.delTDyInfoDzzDj,new Object[]{userid});
        return true;
    }

    public Boolean delTDyJcxx(String uuid,Session session){
        generalSqlComponent.update(SqlCode.updateTDyJcxx,new Object[]{1,uuid});
        return true;
    }

    public List<Map<String,Object>> getccxx(Session session){
        List<Map<String,Object>> data = generalSqlComponent.query(SqlCode.getGDictItem2,new Object[]{"d_dy_jcyy","-1"});
        for(Map<String,Object> li:data){
            List<Map<String,Object>> children = generalSqlComponent.query(SqlCode.getGDictItem2,new Object[]{"d_dy_jcyy",li.get("item_code")});
            li.put("children",children);
        }
        return data;
    }


    public Object checkZjhm(String zjhm){
        Map<String,Object> data = generalSqlComponent.query(SqlCode.getTDyInfoZjhm3,new Object[]{zjhm});
        if(data!=null){
            if("1".equals(data.get("delflag"))){
                return data;
            }
            LehandException.throwException("身份证号："+ zjhm+"党员已存在");
            return false;
        }
        return true;
    }

    /**
     * 新增优秀党员
     * @param bean
     * @param session
     * @return
     */
    public TDyInfoGood addDyInfoGood(TDyInfoGood bean, Session session) {
        //判断必创参数
        isNotEmpty(bean);
        bean.setCreator(session.getUserid().toString());
        bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        generalSqlComponent.insert(SqlCode.insertTDyInfoGood,bean);
        return bean;
    }

    /**
     * 修改优秀党员
     * @param bean
     * @param session
     * @return
     */
    public boolean updateDyInfoGood(TDyInfoGood bean, Session session) {
        if(StringUtils.isEmpty(bean.getId())){
            LehandException.throwException("id参数不能为空！");
        }
        //判断必创参数
        isNotEmpty(bean);

        bean.setUpdator(session.getUserid().toString());
        bean.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        generalSqlComponent.update(SqlCode.updateTDyIndoGood,bean);
        return true;
    }

    /**
     * 判断优秀党员必传参数
     * @param bean
     */
    private void isNotEmpty(TDyInfoGood bean) {
        if(StringUtils.isEmpty(bean.getUserid())){
            LehandException.throwException("党员userid参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getXm())){
            LehandException.throwException("党员名称参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrganname())){
            LehandException.throwException("组织名称参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getType())){
            LehandException.throwException("优秀类型参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getYear())){
            LehandException.throwException("年份参数不能为空！");
        }
    }

    /**
     * 删除优秀党员
     * @param id
     * @return
     */
    public boolean delDyInfoGood(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }
        generalSqlComponent.delete(SqlCode.delTDyIndoGood,new Object[]{id});
        return true;
    }

    public TDyInfoGoodReq getDyInfoGood(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }

        TDyInfoGoodReq tDyInfoGoodReq = generalSqlComponent.query(SqlCode.getTDyIndoGood,new Object[]{id});
        if(!StringUtils.isEmpty(tDyInfoGoodReq.getFileids())){
            List<Map<String,Object>> listFile = listFile(tDyInfoGoodReq.getFileids());
            tDyInfoGoodReq.setAttachList(listFile);
        }
        //查询党员
        Map<String,Object>  dy = generalSqlComponent.query(SqlCode.getTDyInfo2,new Object[]{tDyInfoGoodReq.getUserid()});
        tDyInfoGoodReq.setXm(String.valueOf(dy.get("xm")));
        //查询党组织
        Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoGoodReq.getOrgcode()});
        tDyInfoGoodReq.setOrganname(String.valueOf(tDzzInfo.get("dzzmc")));
        return tDyInfoGoodReq;
    }

    public Pager pageDyInfoGood(Pager pager, String startYear, String endYear, String xm, String type, String orgCode) {
        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(type)){
            LehandException.throwException("优秀类型不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        //开始-结束年份
        paramMap.put("startYear",startYear);
        paramMap.put("endYear",endYear);
        //姓名
        paramMap.put("xm",xm);
        //优秀党员类型
        paramMap.put("type",type);
        pager=generalSqlComponent.pageQuery(SqlCode.pageDyInfoGood,paramMap,pager);
        List<TDyInfoGoodReq> dataList = (List<TDyInfoGoodReq>) pager.getRows();
        for (TDyInfoGoodReq  tDyInfoGoodReq: dataList) {
            List<Map<String,Object>> listFile = listFile(tDyInfoGoodReq.getFileids());
            tDyInfoGoodReq.setAttachList(listFile);

            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoGoodReq.getOrgcode()});
            tDyInfoGoodReq.setOrganname(String.valueOf(tDzzInfo.get("dzzmc")));
        }
        return pager ;
    }
    /**
     * 获取当前组织下所有组织编码
     * @param organcode
     * @return
     */
    private void getOrgCode(String organcode,List<String> listOrgCode) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", organcode);
        List<Map<String,Object>> list  = generalSqlComponent.query(SqlCode.getTDzzInfoSimpleTree2, paramMap);
        for(Map<String,Object> li:list){
            listOrgCode.add(li.get("organcode").toString());
            getOrgCode(li.get("organcode").toString(),listOrgCode);
        }
    }
    /**
     * 获取附件list集合
     * @param fileids
     * @return
     */
    private List<Map<String, Object>> listFile(String fileids) {
        List<Map<String,Object>> listFile = new ArrayList<>();
        if(!StringUtils.isEmpty(fileids)){
            String[] fileid = fileids.split(",");
            for (String id:fileid) {
                Map<String,Object> m =  generalSqlComponent.query(SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",1);put("id",id);}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    m.put("fileid",id);
                    m.put("flpath", String.format("%s%s", downIp, dir));
                    m.put("name",m.get("name").toString().substring(0,m.get("name").toString().lastIndexOf(".")));
                    listFile.add(m);
                }
            }
        }
        return  listFile;
    }

    public List<TDyInfoGoodReq> listDyInfoGood(String userid,String type) {
        if(StringUtils.isEmpty(userid)){
            LehandException.throwException("党员id参数不能为空！");
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("userid", userid);
        paramMap.put("type", type);
        List<TDyInfoGoodReq> dataList = generalSqlComponent.query(SqlCode.listDyInfoGood,paramMap);
        for (TDyInfoGoodReq  tDyInfoGoodReq: dataList) {
            List<Map<String,Object>> listFile = listFile(tDyInfoGoodReq.getFileids());
            tDyInfoGoodReq.setAttachList(listFile);
            //查询党员
            Map<String,Object>  dy = generalSqlComponent.query(SqlCode.getTDyInfo2,new Object[]{userid});
            tDyInfoGoodReq.setXm(String.valueOf(dy.get("xm")));
            //查询党组织
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoGoodReq.getOrgcode()});
            tDyInfoGoodReq.setOrganname(String.valueOf(tDzzInfo.get("dzzmc")));
        }
        return dataList;
    }

    public Object exportDyInfoGood(HttpServletResponse response, TDyInfoGoodReq req, String orgCode, String startYear, String endYear, String type, String xm, Session session) {
        //表格标题
        if(req.getExportColumns().size()<1){
            return  null;
        }
        List<ExportColumnReq> columns  = req.getExportColumns();

        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(type)){
            LehandException.throwException("优秀党员类型不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        //开始-结束年份
        paramMap.put("startYear",startYear);
        paramMap.put("endYear",endYear);
        //姓名
        paramMap.put("xm",xm);
        //优秀党员类型
        paramMap.put("type",type);
        //获取导出内容
        List<TDyInfoGoodReq> dataList = generalSqlComponent.query(SqlCode.listDyInfoGood, paramMap);
        for (TDyInfoGoodReq tDyInfoGoodReq: dataList) {
            if("0".equals(tDyInfoGoodReq.getType())){
                tDyInfoGoodReq.setType("优秀共产党员");
            }
            if("1".equals(tDyInfoGoodReq.getType())){
                tDyInfoGoodReq.setType("优秀党务工作者");
            }
            if("2".equals(tDyInfoGoodReq.getType())){
                tDyInfoGoodReq.setType("党代表");
            }
            //查询党员
            Map<String,Object>  dy = generalSqlComponent.query(SqlCode.getTDyInfo2,new Object[]{tDyInfoGoodReq.getUserid()});
            tDyInfoGoodReq.setXm(dy==null?null:String.valueOf(dy.get("xm")));
            //查询党组织
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoGoodReq.getOrgcode()});
            tDyInfoGoodReq.setOrganname(tDzzInfo==null?null:String.valueOf(tDzzInfo.get("dzzmc")));
        }

        //表头
        List<String> tables = new ArrayList<String>();
        for (ExportColumnReq col: columns
        ) {
            tables.add(col.getTitle());
        }
        String[] tablesStr = tables.toArray(new String[tables.size()]);
        //表名
        String tableName = String.format("%s"+("0".equals(type)?"优秀共产党员":"优秀党务工作者"), session.getUsername());

        //内容
        List<Map<String,String>> mapList = getMemberAnalysisExportInfo(dataList,columns);

        ExcelUtils.write(response,tablesStr,mapList,tableName);
        return null;
    }
    /**
     * 优秀党员信息导出
     * @param list
     * @param columns
     * @return
     */
    private List<Map<String, String>> getMemberAnalysisExportInfo(List<TDyInfoGoodReq> list, List<ExportColumnReq> columns) {
        List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
        for ( TDyInfoGoodReq  info: list) {
            Map<String, String> tempMap = new HashMap<>();
            for (ExportColumnReq col: columns) {
                tempMap.put(col.getTitle(),getInfo(col.getCode(), SysUtil.transBean2Map(info)));
            }
            explist.add(tempMap);
        }
        return explist;
    }
    /**
     * 通过列编码取值
     * @param code
     * @param info
     * @return
     */
    public String getInfo(String code, Map<String, Object> info) {
        String inforStr = StringUtils.isEmpty(info.get(code)) ? MagicConstant.STR.EMPTY_STR : String.valueOf(info.get(code));
        return "null".equals(inforStr) ? MagicConstant.STR.EMPTY_STR : inforStr;
    }

    public boolean addDyInfoChildModule(TDyInfoChildModule bean, Session session) {
        //判断必传参数
        isNotEmpty(bean);
        bean.setCreator(session.getUserid().toString());
        bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        generalSqlComponent.insert(SqlCode.insertTDyInfoChildModule,bean);
        return true;
    }
    /**
     * 判断优秀党员必传参数
     * @param bean
     */
    private void isNotEmpty(TDyInfoChildModule bean) {

        if(StringUtils.isEmpty(bean.getXm())){
            LehandException.throwException("党员名称参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getXb())){
            LehandException.throwException("党员性别参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrgcode())){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getOrganname())){
            LehandException.throwException("组织名称参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getType())){
            LehandException.throwException("类型参数不能为空！");
        }
        if(StringUtils.isEmpty(bean.getZzmm())){
            LehandException.throwException("政治面貌参数不能为空！");
        }

    }

    public boolean updateDyInfoChildModule(TDyInfoChildModule bean, Session session) {
        if(StringUtils.isEmpty(bean.getId())){
            LehandException.throwException("id参数不能为空！");
        }
        //判断必创参数
        isNotEmpty(bean);

        bean.setUpdator(session.getUserid().toString());
        bean.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        generalSqlComponent.update(SqlCode.updateDyInfoChildModule,bean);
        return true;
    }

    public boolean delDyInfoChildModule(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }
        generalSqlComponent.delete(SqlCode.delDyInfoChildModule,new Object[]{id});

        return true;
    }

    public TDyInfoChildModuleReq getDyInfoChildModule(String id) {
        if(StringUtils.isEmpty(id)){
            LehandException.throwException("id参数不能为空！");
        }
        TDyInfoChildModuleReq tDyInfoChildModuleReq = generalSqlComponent.query(SqlCode.getDyInfoChildModule,new Object[]{id});
        //查询党组织
        Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoChildModuleReq.getOrgcode()});
        tDyInfoChildModuleReq.setOrganname(String.valueOf(tDzzInfo.get("dzzmc")));
        return tDyInfoChildModuleReq;
    }

    public Pager pageDyInfoChildModule(Pager pager, String startTime, String endTime, String xm, String zzmm, String type, String orgCode) {
        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(type)){
            LehandException.throwException("类型不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        //开始-结束年份
        paramMap.put("startTime",startTime);
        paramMap.put("endTime",endTime);
        //姓名
        paramMap.put("xm",xm);
        //姓名
        paramMap.put("zzmm",zzmm);
        //优秀党员类型
        paramMap.put("type",type);
        pager=generalSqlComponent.pageQuery(SqlCode.pageDyInfoChildModule,paramMap,pager);
        List<TDyInfoChildModuleReq> datas = (List<TDyInfoChildModuleReq>) pager.getRows();
        for (TDyInfoChildModuleReq tDyInfoChildModuleReq: datas) {
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoChildModuleReq.getOrgcode()});
            tDyInfoChildModuleReq.setOrganname(String.valueOf(tDzzInfo.get("dzzmc")));
        }

        return pager ;
    }

    public Object exportDyInfoChildModule(HttpServletResponse response, TDyInfoChildModuleReq req, String orgCode, String startTime, String endTime, String type, String xm, String zzmm, Session session) {
        //表格标题
        if(req.getExportColumns().size()<1){
            return  null;
        }
        List<ExportColumnReq> columns  = req.getExportColumns();

        if(StringUtils.isEmpty(orgCode)){
            LehandException.throwException("组织编码参数不能为空！");
        }
        if(StringUtils.isEmpty(type)){
            LehandException.throwException("类型不能为空！");
        }
        List<String> listOrgCode = new ArrayList<>();
        //获取当前组织编码及其下所有组织编码
        listOrgCode.add(orgCode);
        getOrgCode(orgCode,listOrgCode);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));
        //开始-结束年份
        paramMap.put("startTime",startTime);
        paramMap.put("endTime",endTime);
        //姓名
        paramMap.put("xm",xm);
        //姓名
        paramMap.put("zzmm",zzmm);
        //优秀党员类型
        paramMap.put("type",type);
        //获取导出内容
        List<TDyInfoChildModuleReq> dataList = generalSqlComponent.query(SqlCode.listTDyInfoChildModuleReq, paramMap);
        for (TDyInfoChildModuleReq tDyInfoChildModuleReq: dataList) {
            Map<String,Object> tDzzInfo = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{tDyInfoChildModuleReq.getOrgcode()});
            tDyInfoChildModuleReq.setOrganname(String.valueOf(tDzzInfo.get("dzzmc")));
        }
        //表头
        List<String> tables = new ArrayList<String>();
        for (ExportColumnReq col: columns
        ) {
            tables.add(col.getTitle());
        }
        String[] tablesStr = tables.toArray(new String[tables.size()]);
        //表名
        String tableName = String.format("%s党组织人员信息子模块", session.getUsername());

        //内容
        List<Map<String,String>> mapList = getChildModuleExportInfo(dataList,columns);

        ExcelUtils.write(response,tablesStr,mapList,tableName);
        return null;
    }
    /**
     * 党组织人员信息子模块导出
     * @param list
     * @param columns
     * @return
     */
    private List<Map<String, String>> getChildModuleExportInfo(List<TDyInfoChildModuleReq> list, List<ExportColumnReq> columns) {
        List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
        for ( TDyInfoChildModuleReq  info: list) {
            Map<String, String> tempMap = new HashMap<>();
            for (ExportColumnReq col: columns) {
                tempMap.put(col.getTitle(),getInfo(col.getCode(), SysUtil.transBean2Map(info)));
            }
            explist.add(tempMap);
        }
        return explist;
    }

    /**
     * 导入党员信息
     * @param file
     * @param session
     * @return
     */
    @Transactional
    public String memberInfoImportNew(MultipartFile file, Session session, String organids,Integer type) {
        try {
            InputStream inputStream = file.getInputStream();
            List<List<String>> list = ExcelUtils.readExcelNew(file.getOriginalFilename(), inputStream);
            if ( null != list && list.size()<=0){
                LehandException.throwException("无数据");
            }
            List<String> listNew = new ArrayList<>();
            list.forEach(a->{
                listNew.add(a.get(1));
            });
            Set<String> setNew = new HashSet<>(listNew);
            if(listNew.size()> setNew.size()){
                LehandException.throwException("党员身份证号码重复");
            }
            if(type==1){//不是增量导入党员
                //校验是否有党组织被删除了或者被合并了
                List<String> listOld =  sqlfgDBComponent.listSingleColumn("select a.zjhm from t_dy_info  a left join t_dzz_info_simple b on a.organid=b.organid where b.id_path like '%"+organids+"%' and b.operatetype != 3 and b.zzlb != '3100000025' and b.zzlb != '3900000026'",String.class,new Object[]{});
                //generalSqlComponent.query(SqlCode.listTDzzInfos2,new Object[]{"'%"+organids+"%'"});
                String sfzhs = "";
                for(String o :listOld){
                    if(!listNew.contains(o)){
                        //不包含将党员信息置为无效状态使用字段 zfbz=1
                        sfzhs += "'"+o+"'"+",";
                    }
                }
                sfzhs = !StringUtils.isEmpty(sfzhs) ? sfzhs.substring(0,sfzhs.length()-1) : sfzhs;
                Map params = new HashMap();
                params.put("sfzh",sfzhs);
                if(!StringUtils.isEmpty(sfzhs)){
                    //将 t_dy_info 置为作废
                    sqlfgDBComponent.update("update t_dy_info set delflag=1 where zjhm in ("+sfzhs+")",new Object[]{});
                    //urc_user_account 删除账号
                    sqlfgDBComponent.update("update urc_user set status=0 where idno in ("+sfzhs+")",new Object[]{});
                    //dBComponent.delete("delete from urc_user_account where account in ("+sfzhs+")",new Object[]{});
                    sqlfgDBComponent.update("update sys_user set status=0 where acc in ("+sfzhs+")",new Object[]{});
                    sqlfgDBComponent.update("update t_dzz_bzcyxx set zfbz=1 where zjhm in ("+sfzhs+")",new Object[]{});
                }
            }
            int total = list.size();

            int  insertnum = 0;

            int  updatenum = 0;
            StringBuffer buffer = new StringBuffer("导入结果: ");
            buffer.append("\r\n");
            for (int i = 0; i < total ; i++) {
                String idcard  = list.get(i).get(1);
                String xm = list.get(i).get(0);
                String csrq = list.get(i).get(3);
                String rdrq = list.get(i).get(7);
                String zzrq = list.get(i).get(8);
                String rylb = list.get(i).get(12);
                String orgname = null;
                try {
                    orgname = String.valueOf(list.get(i).get(19));
                } catch (Exception e) {
                    buffer.append("第"+(i+2)+"条党员组织获取错误");
                    buffer.append("\r\n");
                    continue;
                }
                Map map = generalSqlComponent.query(SqlCode.getTDzzInfo,new Object[]{orgname});
                if(map==null){
                    buffer.append("第"+(i+2)+"条党员组织不存在或党员组织名称匹配错误！");
                    buffer.append("\r\n");
                    continue;
                }
                String organcode = String.valueOf(map.get("organcode"));
                String organid= String.valueOf(map.get("organid"));
                //校验导入数据
                if(idcard.length()==18 && idcard.substring(17).matches("[a-zA-Z]+")) {
                    idcard = idcard.substring(0,17)+idcard.substring(17).toUpperCase();
                }

                boolean reg = getValidate(i+2,idcard,xm,csrq,buffer,rdrq,zzrq,rylb);

                if(!reg){
                    continue;
                }

                //判断党员信息是否存在,执行新增或修改操作
                String userid = getUseridByIdcard(idcard);
                if(StringUtils.isEmpty(userid)){//插入
                    userid = insertMemberInfo(list.get(i),organid,
                            session.getCurrentOrg().getOrgname(),
                            idcard);
                    insertSysuser(list.get(i),organid,
                            userid,session);
                    insertnum += 1;
                }else{
                    updateMemberInfo(list.get(i),userid,organid,
                            session.getCurrentOrg().getOrgname(),
                            idcard);
                    updateSysuser(list.get(i),organid,userid,session);
                    updatenum += 1;
                }
                //创建账号必须
                Map<String,Object> orgInfo = generalSqlComponent.query(SqlCode.queryPorgcode ,new HashMap<String,Object>(){{
                    put("organcode",organcode);
                }});

                if(orgInfo == null){
                    buffer.append("第"+(i+2)+"条数据组织机构数据在管理系统中不存在,无法正确创建账号;");
                    buffer.append("\r\n");
                    continue;
                }
                //更新组织用户和账号
                int k = memberExcelService.updateUserAccount2(list.get(i),
                        orgInfo.get("orgid"),session,userid,organid,session.getCurrentOrg().getOrgcode());
            }
            buffer.append("共"+(total)+"条数据,共导入成功"+(insertnum+updatenum)+"条数据;");
            return buffer.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private boolean getValidate(int i,String idcard, String xm, String csrq,StringBuffer buffer,String rdrq,String zzrq,String rylb) {

        //判断导入的姓名是否为空
        if(StringUtils.isEmpty(xm)){
            buffer.append("第"+i+"条数据姓名为空;");
            buffer.append("\r\n");
            return false;
        }

        //判断导入的出生日期格式是否正确yyyy-MM-dd
        if(!StringUtils.isEmpty(csrq)) {
            String regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
            Pattern pattern = Pattern.compile(regex);
            Matcher m = pattern.matcher(csrq);
            boolean dateFlag = m.matches();
            if (!dateFlag) {
                LehandException.throwException("出生日期格式错误,请检查后重新导入!");
            }
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setLenient(false);
            try {
                Date date = formatter.parse(csrq);
                System.out.println(date);
                System.out.print("格式正确!");
            } catch (Exception e) {
                LehandException.throwException("出生日期格式错误,请检查后重新导入!");
            }
        }


        //判断导入的idcard是否为空
        if(StringUtils.isEmpty(idcard)){
            buffer.append("第"+i+"条数据身份证号码为空;");
            buffer.append("\r\n");
            return false;
        }

        //判断身份证格式
        if(!SysUtil.isIDNumber(idcard)){
            buffer.append("第"+i+"条数据身份证号码有误;");
            buffer.append("\r\n");
            return false;
        }

        if(StringUtils.isEmpty(rdrq)){
            buffer.append("第"+i+"条数据入党日期为空;");
            buffer.append("\r\n");
            return false;
        }

//        if(StringUtils.isEmpty(zzrq)){
//            buffer.append("第"+i+"条数据转正日期为空;");
//            buffer.append("\r\n");
//            return false;
//        }

        if(StringUtils.isEmpty(rylb)){
            buffer.append("第"+i+"条数据人员类别为空;");
            buffer.append("\r\n");
            return false;
        }

        return true;
    }
    /**
     * 根据党员身份证号获取党员id
     * @param idcard
     * @return
     */
    @SuppressWarnings("rawtypes")
    private String getUseridByIdcard(String idcard) {
        Map map = sqlfgDBComponent.getMap("select userid from t_dy_info  " +
                "where zjhm = ? and dyzt = '1000000003'",new Object[]{idcard});
        return map==null ? "" : String.valueOf(map.get("userid"));
    }
    /**
     * 新增党员信息
     * @param data
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Transactional(rollbackFor=Exception.class)
    public String insertMemberInfo(List<String> data,String organid,String orgname,String idcard) {
        //党员表主键
        String key = UUID.randomUUID().toString().replace("-","");
        //新增党员信息
        TDyInfo info = new TDyInfo();
        info.setUserid(key);
        info.setDelflag("0");
        info.setXm(data.get(0));
        boolean zjhm = idcard.contains("'");
        if(!zjhm){
            info.setZjhm(idcard);
        }else{
            info.setZjhm(idcard.substring(1,data.get(1).length()-1));
        }
        Map map = sqlfgDBComponent.getMap(sql,new Object[]{"d_sexid",data.get(2)});
        info.setXb(map==null ? "" : map.get("item_code").toString());
        info.setCsrq(data.get(3));
        //判断民族是否包含'如果包含就取后面的字符串
        Map map1 =  sqlfgDBComponent.getMap(sql,new Object[]{"d_national",data.get(4)});
        info.setMz(map1==null ? "" : map1.get("item_code").toString());
        Map map2 =  sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_xl",data.get(5)});
        info.setXl(map2==null ? "" : map2.get("item_code").toString());
        //学位
        Map map3 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_xw",data.get(6)});
        info.setXwDm(map3==null ? "" : map3.get("item_code").toString());
        info.setRdsj(data.get(7));
        info.setZzsj(data.get(8));
        //工作岗位
        //Map map4 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_gzgw",data.get(9)});
        info.setGzgw(data.get(9)==null ? "" : data.get(9));
        //从事专业技术职务
        Map map5= sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_jszc",data.get(10)});
        info.setCszyjszwDm(map5==null ? "" : map5.get("item_code").toString());
        //新社会阶层类型
        //Map map6 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_shjc",data.get(11)});
        info.setXshjclxDm(data.get(11)==null ? "" : data.get(11));
        //人员类别
        Map map7 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_rylb",data.get(12)});
        info.setDylb(map7==null ? "" : map7.get("item_code").toString());
        info.setIsworkers("否".equals(data.get(13)) ? "0" : "1");
        if(data.get(14).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(14));
            info.setLxdh(bd.toPlainString());
        }else{
            info.setLxdh(data.get(14));
        }
        info.setLhzbdwmc(data.get(15));
        info.setHjszd(data.get(16));
        info.setXjzd(data.get(17));
        info.setXxwzd(data.get(18));
        info.setOrganid(organid);
        info.setDyzt("1000000003");
        info.setOperatetype("0");
        info.setSyncstatus("E");
        //通过身份证号查询党员是否存在如果存在就更新并将删除标识更新为0 delflag=0
        sqlfgDBComponent.insert("insert into t_dy_info set \r\n" +
                "userid = :userid,organid = :organid,xm = :xm,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg,\r\n" +
                "rdsj = :rdsj,zzsj = :zzsj,dylb = :dylb,isworkers= :isworkers,lxdh = :lxdh,operatetype = :operatetype ,age=:age,\r\n" +
                "syncstatus = :syncstatus,xb = :xb, gzgw = :gzgw,delflag = :delflag,zjhm = :zjhm,dyzt = :dyzt,gddh = :gddh,zy = :zy,\r\n" +
                "xw_dm = :xwDm,byyx = :byyx,hjszd = :hjszd,xjzd = :xjzd,dydaszdw = :dydaszdw,lhzbdwmc = :lhzbdwmc,cszyjszw_dm = :cszyjszwDm,xshjclx_dm = :xshjclxDm",info);
        //
        TDyInfoBc dybc = new TDyInfoBc();
        dybc.setUserid(key);
        dybc.setXw(info.getXwDm());
        dybc.setByyx(info.getByyx());
        dybc.setZy(info.getZy());
        dybc.setDydaszdw(info.getDydaszdw());
        dybc.setXjzd(info.getXjzd());
        dybc.setSyncstatus("A");
        dybc.setHjszd(data.get(16));
        // t_dy_info_bc
        sqlfgDBComponent.insert(
                "insert into t_dy_info_bc (userid,xw,byyx,zy,jszc,xshjclx,dydaszdw,hjdz_qhnxxdz,syncstatus,"
                        + "xjzd,rdjsr,qtdt,jrqtdtrq,lkqtdtrq," +
                        "hjszd) values (:userid,:xw,:byyx,:zy,:jszc,:xshjclx," +
                        ":dydaszdw,:hjdzQhnxxdz,"
                        + ":syncstatus,:xjzd,:rdjsr,:qtdt,:jrqtdtrq," +
                        ":lkqtdtrq,:hjszd)",
                dybc);
        return key;
    }
    public void insertSysuser(List<String> data, String organid, String key,
                              Session session){
        SysUser user = new SysUser();
        user.setId(UUID.randomUUID().toString().replace("-",""));
        user.setPwd("0ffe6471a68f774d03695b1e536c2da5");
        user.setSalt("448d7c19946648e5aad3b9b7b3f7a39a");
        user.setRoles("role_party_member");
        user.setAcc(data.get(1));
        user.setName(data.get(0));
        user.setOrgId(organid);
        user.setOrgName(session.getCurrentOrg().getOrgname());
        user.setWorkPhone(data.get(14));
        user.setStatus(1);
        user.setBaseInfoId(key);
        user.setCreateTime(DateEnum.now());
        user.setModiTime(DateEnum.now());
        user.setCreater("1");
        user.setModifier("1");
        generalSqlComponent.insert(SqlCode.addSysuser,user);
    }
    /**
     * 修改党员信息
     * @param data
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Transactional(rollbackFor=Exception.class)
    public int updateMemberInfo(List<String> data,String userid,String organid, String orgname,String idcard) {

        TDyInfo info = new TDyInfo();
        info.setOrganid(organid);
        info.setUserid(userid);
        info.setDelflag("0");
        info.setXm(data.get(0));
        boolean zjhm = idcard.contains("'");
        if(!zjhm){
            info.setZjhm(idcard);
        }else{
            info.setZjhm(idcard.substring(1,idcard.length()-1));
        }
        Map map = sqlfgDBComponent.getMap(sql,new Object[]{"d_sexid",data.get(2)});
        info.setXb(map==null ? "" : map.get("item_code").toString());
        info.setCsrq(data.get(3));
        Map map1 = sqlfgDBComponent.getMap(sql,new Object[]{"d_national",data.get(4)});
        info.setMz(map1==null ? "" : map1.get("item_code").toString());
        Map map2 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_xl",data.get(5)});
        info.setXl(map2==null ? "" : map2.get("item_code").toString());
        //学位
        Map map5 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_xw",data.get(6)});
        info.setXwDm(map5==null ? "" : map5.get("item_code").toString());
        info.setRdsj(data.get(7));
        info.setZzsj(data.get(8));
        //Map map3 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_gzgw",data.get(9)});
        info.setGzgw(data.get(9)==null ? "" : data.get(9));
        //从事专业技术职务
        Map map6= sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_jszc",data.get(10)});
        info.setCszyjszwDm(map6==null ? "" : map6.get("item_code").toString());
        //新社会阶层类型
        //Map map7 =sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_shjc",data.get(11)});
        info.setXshjclxDm(data.get(11)==null ? "" : data.get(11));
        Map map4 = sqlfgDBComponent.getMap(sql,new Object[]{"d_dy_rylb",data.get(12)});
        info.setDylb(map4==null ? "" : map4.get("item_code").toString());
        info.setIsworkers("否".equals(data.get(13)) ? "0" : "1");
        if(data.get(14).contains(".")){
            BigDecimal bd = new BigDecimal(data.get(14));
            info.setLxdh(bd.toPlainString());
        }else{
            info.setLxdh(data.get(14));
        }

        info.setLhzbdwmc(data.get(15));
        info.setHjszd(data.get(16));
        info.setXjzd(data.get(17));
        info.setXxwzd(data.get(18));
        info.setDyzt("1000000003");
        info.setOrganid(organid);
        info.setOperatetype("0");
        info.setSyncstatus("E");
        int result = sqlfgDBComponent.update("update t_dy_info set organid = " +
                ":organid,xm = :xm,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg,\r\n" +
                "rdsj = :rdsj,zzsj = :zzsj,dylb = :dylb,isworkers= :isworkers,lxdh = :lxdh,operatetype = :operatetype ,age=:age,\r\n" +
                "syncstatus = :syncstatus,xb = :xb, gzgw = :gzgw,delflag = :delflag,zjhm = :zjhm,dyzt = :dyzt,gddh = :gddh,zy = :zy,\r\n" +
                "xw_dm = :xwDm,byyx = :byyx,hjszd = :hjszd,xjzd = :xjzd,dydaszdw = :dydaszdw,lhzbdwmc = :lhzbdwmc,cszyjszw_dm = :cszyjszwDm,xshjclx_dm = :xshjclxDm where userid = :userid",info);
        return result;
    }
    public void updateSysuser(List<String> data,String organid,String key, Session session){
        SysUser user = new SysUser();
        user.setRoles("role_party_member");
        user.setAcc(data.get(1));
        user.setName(data.get(0));
        user.setOrgId(organid);
        user.setOrgName(session.getCurrentOrg().getOrgname());
        user.setWorkPhone(data.get(14));
        user.setBaseInfoId(key);
        generalSqlComponent.update(SqlCode.modiSysuser,user);
    }

}
