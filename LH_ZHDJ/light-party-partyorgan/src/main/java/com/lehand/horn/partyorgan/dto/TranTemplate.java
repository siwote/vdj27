package com.lehand.horn.partyorgan.dto;

import java.util.Map;
/**
 * 微信推新消息模板类
 * @author lx
 */
public class TranTemplate {
    //模板信息
    Map<String , TemplateInfo> data;
    //模板ID
    String template_id;
    //接收人ID
    String touser;
    //颜色
    String topcolor;
    //跳转url
    String url;

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, TemplateInfo> getData() {
        return data;
    }

    public void setData(Map<String, TemplateInfo> data) {
        this.data = data;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }
}
