package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="短信发送实体类", description="短信发送实体类")
public class SmsSendDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="自增长主键", name="id")
    private Long id;

    @ApiModelProperty(value="租户id", name="compid")
    private Long compid;

    @ApiModelProperty(value = "用户标识", name = "usermark")
    private String usermark;

    @ApiModelProperty(value = "电话号码", name = "phone")
    private String phone;

    @ApiModelProperty(value = "短信发送状态/1.已发送/0.未发送", name = "status")
    private Integer status;

    @ApiModelProperty(value = "发送内容", name = "content")
    private String content;

    @ApiModelProperty(value="业务id", name="businessid")
    private Long businessid;

    @ApiModelProperty(value="删除状态/1.删除/0未删除", name="delflag")
    private Integer delflag;

    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="更新时间", name="updatetime")
    private String updatetime;

    public SmsSendDto() {
    }

    public SmsSendDto(Long id, Long compid, String usermark, String phone, Integer status, String content, Long businessid, Integer delflag, String createtime, String updatetime) {
        this.id = id;
        this.compid = compid;
        this.usermark = usermark;
        this.phone = phone;
        this.status = status;
        this.content = content;
        this.businessid = businessid;
        this.delflag = delflag;
        this.createtime = createtime;
        this.updatetime = updatetime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getUsermark() {
        return usermark;
    }

    public void setUsermark(String usermark) {
        this.usermark = usermark;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getBusinessid() {
        return businessid;
    }

    public void setBusinessid(Long businessid) {
        this.businessid = businessid;
    }

    public Integer getDelflag() {
        return delflag;
    }

    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }
}
