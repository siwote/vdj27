package com.lehand.horn.partyorgan.pojo.partygroup;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="党小组信息表", description="党小组信息表")
public class PartyGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="自增长主键", name="id")
    private Long id;

    @ApiModelProperty(value="党小组编码", name="code")
    private String code;

    @ApiModelProperty(value="党小组名称", name="name")
    private String name;

    @ApiModelProperty(value="组织编码", name="orgcode")
    private String orgcode;

    @ApiModelProperty(value="组织机构ID(党组织ID)", name="organid")
    private String organid;

    @ApiModelProperty(value="组织名称", name="orgname")
    private String orgname;

    @ApiModelProperty(value="创建人ID", name="createid")
    private String createid;

    @ApiModelProperty(value="创建人名称", name="createname")
    private String createname;

    @ApiModelProperty(value="创建时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="状态 （ 0 正常  1 取消  -1 删除 ）", name="status")
    private Integer status;

    @ApiModelProperty(value="类别（0 默认值 ）", name="type")
    private Integer type;

    @ApiModelProperty(value="租户ID", name="compid")
    private Long compid;

    @ApiModelProperty(value="备注1", name="remarks1")
    private String remarks1;

    @ApiModelProperty(value="备注2", name="remarks2")
    private String remarks2;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getCreateid() {
        return createid;
    }

    public void setCreateid(String createid) {
        this.createid = createid;
    }

    public String getCreatename() {
        return createname;
    }

    public void setCreatename(String createname) {
        this.createname = createname;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public String getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(String remarks1) {
        this.remarks1 = remarks1;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }


}