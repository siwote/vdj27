package com.lehand.horn.partyorgan.controller.dzz;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.util.StringUtils;
import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.horn.partyorgan.controller.HornBaseController;
import com.lehand.horn.partyorgan.dto.MemberSearchReq;
import com.lehand.horn.partyorgan.service.info.MemberService;

@Api(value = "党员-前端改造后该当前类作废", tags = { "党员-前端改造后该当前类作废" })
@RestController
@RequestMapping("/lhdj/dy")
//TODO 前端改造后该当前类作废
public class TDyInfoController extends HornBaseController{

	private static final Logger logger = LogManager.getLogger(TDyInfoController.class);

	@Resource
	private MemberService memberService;

	@OpenApi(optflag=0)
	@ApiOperation(value = "详情", notes = "详情", httpMethod = "POST")
	@RequestMapping("/info")
	public Message query(String xm,String zjhm,String dylb,String organid,Pager pager) {
		Message message = new Message();
		try {
			/**
			 * 分页查询
			 */
//			pager = tdyInfoService.query(getSession(),xm,zjhm,dylb,organid,pager);
			
			MemberSearchReq req = new MemberSearchReq();
			req.setXm(xm);
			req.setZjhm(zjhm);
			req.setDylb(dylb);
			req.setOrganid(organid);
			pager = memberService.query(getSession(),req,pager);
		    message.setData(pager);
		    message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("党员查询出错");
		}

		return message;

	}

	/**
	 *
	 * @Description:获取党员基本信息
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月22日 下午7:17:13
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取党员基本信息", notes = "获取党员基本信息", httpMethod = "POST")
	@RequestMapping("/essen")
	public Message queryEssential(String userId) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryEssential(userId);
			List<Map<String,Object>> list = memberService.queryEssential(userId);
		    message.setData(list);
		    message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("党员基本信息查询出错");
		}
		return message;
	}


	/**
	 *
	 * @Description:  查询党籍和党组织
	 * @param userId
	 * @param organid
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月22日 下午8:04:38
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询党籍和党组织", notes = "查询党籍和党组织", httpMethod = "POST")
	@RequestMapping("/party")
	public Message queryPartyMembershipOrg(String userId,String organid) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
			if(StringUtils.isEmpty(organid)) {
				message.setMessage("参数错误，党组织id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryPartyMembershipOrg(userId,organid);
			List<Map<String,Object>> list = memberService.queryPartyMembershipOrg(userId,organid);
		    message.setData(list);
		    message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("党籍和组织关系查询出错");
		}

		return message;

	}

	/**
	 *
	 * @Description: 查询党员职务
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月22日 下午8:39:29
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询党员职务", notes = "查询党员职务", httpMethod = "POST")
	@RequestMapping("/post")
	public Message queryPost(String userId) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryPost(userId);
			
			Map<String,Object> map = memberService.queryPost(userId);			
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			list.add(map);
		    message.setData(list);
		    message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("职务查询出错");
		}
		return message;
	}

	/**
	 *
	 * @Description: 查询奖惩信息
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月24日 上午8:59:38
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询奖惩信息", notes = "查询奖惩信息", httpMethod = "POST")
	@RequestMapping("/jcxx")
	public Message queryJcxx(String userId) {
		Message message =new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryJcxx(userId);
			List<Map<String,Object>> list = memberService.queryJcxx(userId);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("奖惩信息查询错误");
		}
		return message;
	}

	/**
	 *
	 * @Description:   困难情况
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月24日 上午9:28:07
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "困难情况", notes = "困难情况", httpMethod = "POST")
	@RequestMapping("/knqk")
	public Message queryKnqk(String userId) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryknqk(userId);
			List<Map<String,Object>> list = memberService.queryknqk(userId);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("困难情况查询错误");
		}
		return message;
	}

	/**
	 *
	 * @Description:  出国出境
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月24日 上午9:54:09
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "出国出境", notes = "出国出境", httpMethod = "POST")
	@RequestMapping("/cgcj")
	public Message queryCgcj(String userId) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryCgcj(userId);
			List<Map<String,Object>> list = memberService.queryCgcj(userId);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("出国出境查询错误");
		}
		return message;
	}

	/**
	 *
	 * @Description:  入党信息
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月24日 上午11:37:26
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "入党信息", notes = "入党信息", httpMethod = "POST")
	@RequestMapping("/rdxx")
	public Message queryRdxx(String userId) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryRdxx(userId);
			List<Map<String,Object>> list = memberService.queryRdxx(userId);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("入党信息查询错误");
		}
		return message;
	}

	/**
	 *
	 * @Description:  流动党员
	 * @param userId
	 * @return
	 * @return Message
	 * @throws
	 * @author zouwendong
	 * @date:   2019年1月24日 下午6:16:12
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "流动党员", notes = "流动党员", httpMethod = "POST")
	@RequestMapping("/lddy")
	public Message queryLddy(String userId) {
		Message message = new Message();
		try {
			if(StringUtils.isEmpty(userId)) {
				message.setMessage("参数错误，党员id不能为空");
				return message;
			}
//			List<Map<String,Object>> list = tdyInfoService.queryLddy(userId);
			List<Map<String,Object>> list = memberService.queryLddy(userId);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("入党信息查询错误");
		}
		return message;
	}
}
