package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.dto.*;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfoChildModule;
import com.lehand.horn.partyorgan.pojo.dy.TDyInfoGood;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.horn.partyorgan.service.info.HornOrganizationService;
import com.lehand.horn.partyorgan.service.info.MemberExcelService;
import com.lehand.horn.partyorgan.service.info.MemberExcelService2;
import com.lehand.horn.partyorgan.service.info.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.lehand.components.web.interceptors.BaseInterceptor.getSession;

/**
 * 党员控制层
 *
 * @author admin
 */
@Api(value = "党员信息管理", tags = { "党员信息管理" })
@RestController
@RequestMapping("/lhdj/member")
public class MemberController {

    private static final Logger logger = LogManager.getLogger(MemberController.class);

    @Resource
    private MemberService memberService;

    @Resource
    private MemberExcelService memberExcelService;

    @Resource
    private MemberExcelService2 memberExcelService2;

    @Resource
    private BaseDzzTotalService baseDzzTotalService;

    @Resource
    private HornOrganizationService hornOrganizationService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "党员信息分页查询", notes = "党员信息分页查询", httpMethod = "POST")
    @RequestMapping("/pageInfo")
    public Message pageQuery(String organid,String dylb,String xm,String zjhm,String xl,Integer startage,Integer endage, Pager pager) {
        Message message = new Message();
        MemberSearchReq req = new MemberSearchReq();
        req.setOrganid(organid);
        req.setDylb(dylb);
        req.setXm(xm);
        req.setZjhm(zjhm);
        req.setXl(xl);
        req.setStartage(startage);
        req.setEndage(endage);
        try {
            /**
             * 分页查询
             */
            message.setData(new PagerUtils<MemberInfoDto>(
                    memberService.query(getSession(),req, pager)));
//            message.setData(memberService.pageQuery(getSession(), organid, dylb,xm,zjhm,xl,startage,endage, pager));
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党员查询出错" + e);
        }

        return message;

    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党员统计信息分页查询", notes = "党员信息分页查询", httpMethod = "POST")
    @RequestMapping("/pageAnalysisinfo")
    public Message pageQueryAnalysis(MemberSearchReq req, Pager pager) {
        Message message = new Message();
        try {
            /**
             * 分页查询
             */
            message.success();
            message.setData(new PagerUtils<MemberAnalysisDto>(
                    memberService.queryAnalysis(getSession(), req, pager)));

        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党员统计查询出错" + e);
        }

        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "获取党员基本信息", notes = "获取党员基本信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getBase")
    public Message queryEssential(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryEssential(userId);
            message.success();
            message.setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党员基本信息查询出错");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询党籍和党组织", notes = "查询党籍和党组织", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "organid", value = "组织机构id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getMembership")
    public Message queryPartyMembershipOrg(String userId, String organid) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            if (StringUtils.isEmpty(organid)) {
                message.setMessage("参数错误，党组织id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryPartyMembershipOrg(userId, organid);
            message.success();
            message.setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党籍和组织关系查询出错");
        }

        return message;

    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询党员职务", notes = "查询党员职务", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getPost")
    public Message queryPost(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            Map<String, Object> result = memberService.queryPost(userId);
            message.success();
            message.setData(result);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("职务查询出错");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询奖惩信息", notes = "查询奖惩信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getRewards")
    public Message queryJcxx(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryJcxx(userId);
            message.success();
            message.setData(list);

        } catch (Exception e) {
            logger.error(e);
            message.setMessage("奖惩信息查询错误");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "困难情况", notes = "困难情况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getDifficulty")
    public Message queryKnqk(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryknqk(userId);
            message.success();
            message.setData(list);

        } catch (Exception e) {
            logger.error(e);
            message.setMessage("困难情况查询错误");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "出国出境", notes = "出国出境", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getGoAbroad")
    public Message queryCgcj(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryCgcj(userId);
            message.success();
            message.setData(list);

        } catch (Exception e) {
            logger.error(e);
            message.setMessage("出国出境查询错误");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "入党信息", notes = "入党信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getJoinParty")
    public Message queryRdxx(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryRdxx(userId);
            message.success();
            message.setData(list);

        } catch (Exception e) {
            logger.error(e);
            message.setMessage("入党信息查询错误");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "流动党员", notes = "流动党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getFlows")
    public Message queryLddy(String userId) {
        Message message = new Message();
        try {
            if (StringUtils.isEmpty(userId)) {
                message.setMessage("参数错误，党员id不能为空");
                return message;
            }
            List<Map<String, Object>> list = memberService.queryLddy(userId);
            message.success();
            message.setData(list);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("流动党员查询错误");
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "导出党员信息", notes = "导出党员信息", httpMethod = "POST")
    @RequestMapping("/exportMemberInfo")
    public Message memberInfoExport(HttpServletResponse response, @RequestBody MemberExportReq req,String organid,String xm ,String zjhm) {
        Message message = new Message();
        try {
            message.success();
            message.setData(memberExcelService.memberInfoExport(response, req, getSession(),organid,xm ,zjhm));
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("导出党员信息失败" + e);
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "导出党员统计信息", notes = "导出党员统计信息", httpMethod = "POST")
    @RequestMapping("/exportAnalysisMemberInfo")
    public Message memberAnalysisInfoExport(HttpServletResponse response, @RequestBody MemberExportReq req) {
        Message message = new Message();
        try {
            message.success();
            message.setData(memberExcelService.memberAnalysisInfoExport(response, req, getSession()));
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("导出党员统计信息失败" + e);
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党员信息导入", notes = "党员信息导入", httpMethod = "POST")
    @RequestMapping(value = "/memberInfoImport", method = RequestMethod.POST)
    public Message memberInfoImport(MultipartFile file,String organids) {
        Message message = new Message();
        try {
            message.success();
            message.setData(memberExcelService.memberInfoImport(file, getSession(),organids,1));
        } catch (LehandException e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员信息增量导入", notes = "党员信息增量导入", httpMethod = "POST")
    @RequestMapping(value = "/incrementalMemberInfoImport", method = RequestMethod.POST)
    public Message incrementalMemberInfoImport(MultipartFile file,String organids) {
        Message message = new Message();
        try {
            message.success();
            message.setData(memberExcelService.memberInfoImport(file, getSession(),organids,2));
        } catch (LehandException e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }
    /**
     * Description: 党员导入
     * @author lx
     * @date 2021/04/27
     * @param file
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "新模板党员信息导入", notes = "新模板党员信息导入", httpMethod = "POST")
    @RequestMapping(value = "/memberInfoImportNew", method = RequestMethod.POST)
    public Message memberInfoImportNew(MultipartFile file,String organids) {
        Message message = new Message();
        try {
            message.success();
            message.setData(memberService.memberInfoImportNew(file,
                    getSession(),organids,1));
        } catch (LehandException e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }
    /**
     * Description: 党员导入
     * @author lx
     * @date 2021/04/27
     * @param file
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "新模板党员信息增量导入", notes = "新模板党员信息增量导入", httpMethod = "POST")
    @RequestMapping(value = "/memberInfoImportNewAdd", method = RequestMethod.POST)
    public Message memberInfoImportNewAdd(MultipartFile file,String organids) {
        Message message = new Message();
        try {
            message.success();
            message.setData(memberService.memberInfoImportNew(file,
                    getSession(),organids,2));
        } catch (LehandException e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }
    @OpenApi(optflag=0)
    @ApiOperation(value = "数据字典下拉框", notes = "数据字典下拉框", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "code", value = "编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/dictItemSel")
    public Message dictItemSel(String code) {
        Message message = new Message();
        message.success();
        message.setData(memberService.getDictItemByCode(code));
        return message;
    }

    /**
     * 党员基本信息
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员基本信息(统计)", notes = "党员基本信息(统计)", httpMethod = "POST")
    @RequestMapping("/dyInfo")
    public Message dyInfo(String orgId) {
        Message message = new Message();
        try {

            List<Map<String,Object>> list = baseDzzTotalService.dyCount(orgId,getSession());
            message.setData(list);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党员基本信息查询出错");
        }
        return message;
    }

    /**
     * 学历信息
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "学历信息(统计)", notes = "学历信息(统计)", httpMethod = "POST")
    @RequestMapping("/xlInfo")
    public Message xlInfo(String orgId) {
        Message message = new Message();
        try {
            if(StringUtils.isEmpty(orgId)) {
                message.setMessage("参数错误，组织机构id不能为空");
                return message;
            }
            List<Map<String,Object>> list = baseDzzTotalService.xlCount(orgId);
            message.setData(list);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("学历信息查询出错");
        }
        return message;
    }

    /**
     * 年龄分布
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "年龄分布(统计)", notes = "年龄分布(统计)", httpMethod = "POST")
    @RequestMapping("/ageInfo")
    public Message ageInfo(String orgId) {
        Message message = new Message();
        try {
            if(StringUtils.isEmpty(orgId)) {
                message.setMessage("参数错误，组织机构id不能为空");
                return message;
            }
            List<Map<String,Object>> list = baseDzzTotalService.getAgeCount(orgId);
            message.setData(list);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("年龄分布信息查询出错");
        }
        return message;
    }

    /**
     * 教育学习统计
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "教育学习统计(统计)", notes = "教育学习统计(统计)", httpMethod = "POST")
    @RequestMapping("/studyInfo")
    public Message studyInfo(String orgId) {
        Message message = new Message();
        try {
            if(com.alibaba.druid.util.StringUtils.isEmpty(orgId)) {
                message.setMessage("参数错误，组织机构id不能为空");
                return message;
            }
            Map<String,Object> map = baseDzzTotalService.getStudyCount(orgId);
            message.setData(map);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("教育学习统计信息查询出错");
        }
        return message;
    }

    /**
     * 党费缴纳情况
     * @param orgId
     * @return
     * @author taogang
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党费缴纳情况(统计)", notes = "党费缴纳情况(统计)", httpMethod = "POST")
    @RequestMapping("/costInfo")
    public Message costInfo(String orgId) {
        Message message = new Message();
        try {
            if(com.alibaba.druid.util.StringUtils.isEmpty(orgId)) {
                message.setMessage("参数错误，组织机构id不能为空");
                return message;
            }
            List<Map<String,Object>> list = baseDzzTotalService.getPartyDuesCount(orgId);
            message.setData(list);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("党费统计信息查询出错");
        }
        return message;
    }

    /**
     * 党支部下所有党员的基本信息
     * @param orgId
     * @param type
     * @return
     * @author pantao
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党支部下所有党员的基本信息(移动端)", notes = "党支部下所有党员的基本信息(移动端)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "组织机构ID", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "type", value = "查询类型（默认传空字符串，查询更多传all）", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/partyBaseInfos")
    public Message partyBaseInfos(String orgId,String type) {
        Message message = new Message();
        try {
            List<Map<String, Object>> data = baseDzzTotalService.partyBaseInfos(orgId,type);
            message.success().setData(data);
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
            message.setMessage("基本信息查询出错");
        }
        return message;
    }

    /**
     * 党员的基本信息
     * @param idNumber
     * @return
     * @author pantao
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员的基本信息(移动端)", notes = "党员的基本信息(移动端)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idNumber", value = "党员的身份证号", dataType = "String", paramType = "query", example = "0")

    })
    @RequestMapping("/partyBaseInfo")
    public Message partyBaseInfo(String idNumber) {
        Message message = new Message();
        try {
            Map<String, Object> data = baseDzzTotalService.partyBaseInfo(idNumber,getSession());
            message.success().setData(data);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("基本信息查询出错");
        }
        return message;
    }

    /**
     * 党党员的签名
     * @param idNumber
     * @param userId
     * @return
     * @author pantao
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员签名(移动端)", notes = "党员签名(移动端)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idNumber", value = "党员的身份证号", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "userId", value = "党员的USERID）", dataType = "String", paramType = "query", example = "0"),
            @ApiImplicitParam(name = "fileId", value = "签名附件ID）", dataType = "String", paramType = "query", example = "0")
    })
    @RequestMapping("/partySign")
    public Message partySign(String idNumber,String userId,String fileId) {
        Message message = new Message();
        try {
            baseDzzTotalService.partySign(getSession(),idNumber,userId,fileId);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("基本信息查询出错");
        }
        return message;
    }

    /**
     * 党员的基本信息
     * @param userId
     * @return
     * @author pantao
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员签名查询(移动端)", notes = "党员签名查询(移动端)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "党员的USERID）", dataType = "String", paramType = "query", example = "0")

    })
    @RequestMapping("/getPartySign")
    public Message getPartySign(String userId) {
        Message message = new Message();
        try {
            Map<String, Object> data = baseDzzTotalService.getPartySign(getSession(),userId);
            message.success().setData(data);
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("基本信息查询出错");
        }
        return message;
    }

    /**
     * @Description 查询党籍和党组织关系
     * @Author zwd
     * @Date 2020/12/7 10:34
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询党籍和党组织关系", notes = "查询党籍和党组织关系", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/getdj")
    public Message getDyInfoDzzDj(String userid){
        Message message = new Message();
        message.setData(memberService.getDyInfoDzzDj(userid,getSession()));
        message.success();
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询奖惩信息(包括详情)", notes = "查询奖惩信息(包括详情)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "rewardspunishments", value = "1:奖 2：惩", required = true, paramType = "query", dataType = "Integer", defaultValue = "")
    })
    @RequestMapping("/getjcxx")
    public Message getTDyJcxx(Pager pager,String userid,Integer rewardspunishments){
        Message message = new Message();
        message.setData(memberService.getTDyJcxx(pager,userid,rewardspunishments,getSession()));
        message.success();
        return message;
    }

    /**
    * @Description 添加党员
    * @Author zwd
    * @Date 2020/12/4 15:05
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "添加党员", notes = "添加党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/adddy")
    public Message addTDyInfo(@RequestBody  TDyInfoReq bean){
        Message message = new Message();
        message.setData(memberService.addTDyInfo(bean,getSession()));
        message.success();
        return message;
    };

    /**
    * @Description 党籍和党组织关系
    * @Author zwd
    * @Date 2020/12/7 9:44
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "党籍和党组织关系", notes = "党籍和党组织关系", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/adddj")
    public Message addTDyInfoDzzDj(@RequestBody TDyInfoDzzDjReq bean){
        Message message = new Message();
        message.setData(memberService.addTDyInfoDzzDj(bean,getSession()));
        message.success();
        return message;
    }

    /**
    * @Description 添加奖惩信息
    * @Author zwd
    * @Date 2020/12/7 9:55
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "添加奖惩信息", notes = "添加奖惩信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/addjcxx")
    public Message addTDyJcxx(@RequestBody TDyJcxxReq bean){
        Message message = new Message();
        message.setData(memberService.addTDyJcxx(bean,getSession()));
        message.success();
        return message;
    }


    /**
    * @Description 修改党员
    * @Author zwd
    * @Date 2020/12/7 10:54
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改党员", notes = "修改党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/updatedy")
    public Message updateTDyInfo(@RequestBody  TDyInfoReq bean){
        Message message = new Message();
        message.setData(memberService.updateTDyInfo(bean,getSession()));
        message.success();
        return message;
    };

    /**
    * @Description 修改党籍和党组织关系
    * @Author zwd
    * @Date 2020/12/7 11:20
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改党籍和党组织关系", notes = "修改党籍和党组织关系", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/updatedj")
    public Message updateTDyInfoDzzDj(@RequestBody TDyInfoDzzDjReq bean){
        Message message = new Message();
        message.setData(memberService.updateTDyInfoDzzDj(bean,getSession()));
        message.success();
        return message;
    }

    /**
    * @Description 修改添加奖惩信息
    * @Author zwd
    * @Date 2020/12/7 11:32
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改添加奖惩信息", notes = "修改添加奖惩信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/updatejcxx")
    public Message updateTDyJcxx(@RequestBody TDyJcxxReq bean){
        Message message = new Message();
        message.setData(memberService.updateTDyJcxx(bean,getSession()));
        message.success();
        return message;
    }


    /**
    * @Description //TODO
    * @Author zwd
    * @Date 2020/12/7 11:48
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "删除党员", notes = "删除党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/deldy")
    public Message delTDyInfo(String userid,String zjhm){
        Message message = new Message();
        message.setData(memberService.delTDyInfo(userid,zjhm,getSession()));
        message.success();
        return message;
    }

    /**
    * @Description 删除党籍和党组织关系
    * @Author zwd
    * @Date 2020/12/7 13:41
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "删除党籍和党组织关系", notes = "删除党籍和党组织关系", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/deldj")
    public Message delTDyInfoDzzDj(String suerid){
        Message message = new Message();
        message.setData(memberService.delTDyInfoDzzDj(suerid,getSession()));
        message.success();
        return message;
    }


    /**
    * @Description 删除添加奖惩信息
    * @Author zwd
    * @Date 2020/12/7 14:02
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "删除添加奖惩信息", notes = "删除添加奖惩信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "uuid", value = "uuid", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/deljcxx")
    public Message delTDyJcxx(String uuid){
        Message message = new Message();
        message.setData(memberService.delTDyJcxx(uuid,getSession()));
        message.success();
        return message;
    }

    /**
    * @Description 查询处分处置原因
    * @Author zwd
    * @Date 2020/12/7 15:21
    * @param 
    * @return 
    **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询处分处置原因", notes = "查询处分处置原因", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/getccxx")
    public Message getccxx(){
        Message message = new Message();
        message.setData(memberService.getccxx(getSession()));
        message.success();
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "身份证校验", notes = "身份证校验", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/checkzjhm")
    public Message checkZjhm(String zjhm){
        Message message = new Message();
        message.setData(memberService.checkZjhm(zjhm));
        message.success();
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "党员信息导入", notes = "党员信息导入", httpMethod = "POST")
    @RequestMapping(value = "/memberInfoImport2")
    public Message memberInfoImport2() {
        Message message = new Message();
        try {
            message.setData(memberExcelService2.memberInfoImport());
            message.success();
        } catch (LehandException e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            message.setMessage(e.getMessage());
        }
        return message;
    }

    /**
     * @Description 添加优秀党员
     * @Author lx
     * @Date 2021/2/5
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "添加优秀党员", notes = "添加优秀党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/addDyInfoGood")
    public Message addDyInfoGood(@RequestBody TDyInfoGood bean){
        Message message = new Message();
        try {
            message.setData(memberService.addDyInfoGood(bean,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("新增失败！"+e);
        }
        return message;
    };

    /**
     * @Description 修改优秀党员
     * @Author lx
     * @Date 2021/2/5
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改优秀党员", notes = "修改优秀党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/updateDyInfoGood")
    public Message updateDyInfoGood(@RequestBody  TDyInfoGood bean){
        Message message = new Message();
        try {
            message.setData(memberService.updateDyInfoGood(bean,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("编辑失败！"+e);
        }
        return message;
    }
    /**
     * @Description 删除优秀党员
     * @Author lx
     * @Date 2021/2/5
     * @param
     * @return
     **/
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除优秀党员", notes = "删除优秀党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delDyInfoGood")
    public Message delDyInfoGood(String id){
        Message message = new Message();
        try {
            message.setData(memberService.delDyInfoGood(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("删除失败！"+e);
        }
        return message;
    }
    /**
     * @Description 查看优秀党员
     * @Author lx
     * @Date 2021/2/5
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看优秀党员", notes = "查看优秀党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getDyInfoGood")
    public Message getDyInfoGood(String id){
        Message message = new Message();
        try {
            message.setData(memberService.getDyInfoGood(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看失败！"+e);
        }
        return message;
    }

    /**
     * @Description 查看党员优秀信息
     * @Author lx
     * @Date 2021/2/20
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看党员优秀信息", notes = "查看党员优秀信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "userid", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "优秀党员类型（优秀共产党员与优秀党务工作者）", required = false, dataType = "String")
    })
    @RequestMapping("/listDyInfoGood")
    public Message listDyInfoGood(String userid,String type){
        Message message = new Message();
        try {
            message.setData(memberService.listDyInfoGood(userid,type));
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看党员优秀信息失败！"+e);
        }
        return message;
    }
    /**
     * @Description 分页查询优秀党员
     * @Author lx
     * @Date 2021/2/20
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询优秀党员", notes = "分页查询优秀党员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "startYear", value = "开始年份", required = false, dataType = "String"),
            @ApiImplicitParam(name = "endYear", value = "结束年份", required = false, dataType = "String"),
            @ApiImplicitParam(name = "xm", value = "姓名", required = false, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "优秀党员类型（优秀共产党员与优秀党务工作者）", required = false, dataType = "String"),
            @ApiImplicitParam(name = "orgCode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/pageDyInfoGood")
    public Message pageDyInfoGood(Pager pager,String startYear,String endYear,String xm,String type,String orgCode){
        Message message = new Message();
        try {
            message.setData(memberService.pageDyInfoGood(pager,startYear,endYear,xm,type,orgCode));
            message.success();
        }catch (Exception e){
            LehandException.throwException("分页查询失败！"+e);
        }
        return message;
    }
    /**
     * @Description 导出查询优秀党员
     * @Author lx
     * @Date 2021/2/20
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "导出查询优秀党员", notes = "导出查询优秀党员", httpMethod = "POST")
    @RequestMapping("/exportDyInfoGood")
    public Message exportDyInfoGood(HttpServletResponse response, @RequestBody TDyInfoGoodReq req) {
        Message message = new Message();
        try {
            message.setData(memberService.exportDyInfoGood(response,req,req.getOrgcode(),req.getStartYear(),req.getEndYear(),req.getType(),req.getXm(),getSession()));
            message.success();
        } catch (Exception e) {
            LehandException.throwException("导出先进基层党组织信息失败！"+e);
        }
        return message;
    }

    /**
     * @Description 党组织人员信息子模块
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "政治面貌下拉", notes = "政治面貌下拉", httpMethod = "POST")
    @RequestMapping("/listPoliticalLandscape")
    public Message listPoliticalLandscape(){
        Message message = new Message();
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            map.put("politicalLandscape", hornOrganizationService.listDictItem("t_dy_zzmm"));
            message.setData(map);
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看党员优秀信息失败！"+e);
        }
        return message;
    }


    /**
     * @Description 添加党组织人员信息子模块
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "添加党组织人员信息子模块", notes = "添加党组织人员信息子模块", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping("/addDyInfoChildModule")
    public Message addDyInfoChildModule(@RequestBody TDyInfoChildModule bean){
        Message message = new Message();
        try {
            message.setData(memberService.addDyInfoChildModule(bean,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("新增失败！"+e);
        }
        return message;
    };

    /**
     * @Description 修改党组织人员信息子模块
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改党组织人员信息子模块", notes = "修改党组织人员信息子模块", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/updateDyInfoChildModule")
    public Message updateDyInfoChildModule(@RequestBody  TDyInfoChildModule bean){
        Message message = new Message();
        try {
            message.setData(memberService.updateDyInfoChildModule(bean,getSession()));
            message.success();
        }catch (Exception e){
            LehandException.throwException("编辑失败！"+e);
        }
        return message;
    }
    /**
     * @Description 删除党组织人员信息子模块
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除党组织人员信息子模块", notes = "删除党组织人员信息子模块", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delDyInfoChildModule")
    public Message delDyInfoChildModule(String id){
        Message message = new Message();
        try {
            message.setData(memberService.delDyInfoChildModule(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("删除失败！"+e);
        }
        return message;
    }
    /**
     * @Description 查看党组织人员信息子模块
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看党组织人员信息子模块", notes = "查看党组织人员信息子模块", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getDyInfoChildModule")
    public Message getDyInfoChildModule(String id){
        Message message = new Message();
        try {
            message.setData(memberService.getDyInfoChildModule(id));
            message.success();
        }catch (Exception e){
            LehandException.throwException("查看失败！"+e);
        }
        return message;
    }

    /**
     * @Description 分页党组织人员信息子模块
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页党组织人员信息子模块", notes = "分页党组织人员信息子模块", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "String"),
            @ApiImplicitParam(name = "xm", value = "姓名", required = false, dataType = "String"),
            @ApiImplicitParam(name = "zzmm", value = "政治面貌", required = false, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "优秀党员类型（优秀共产党员与优秀党务工作者）", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orgCode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/pageDyInfoChildModule")
    public Message pageDyInfoChildModule(Pager pager,String startTime,String endTime,String xm,String zzmm,String type,String orgCode){
        Message message = new Message();
        try {
            message.setData(memberService.pageDyInfoChildModule(pager,startTime,endTime,xm,zzmm,type,orgCode));
            message.success();
        }catch (Exception e){
            LehandException.throwException("分页查询失败！"+e);
        }
        return message;
    }
    /**
     * @Description 导出查询优秀党员
     * @Author lx
     * @Date 2021/2/22
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "导出党组织人员信息子模块", notes = "导出党组织人员信息子模块", httpMethod = "POST")
    @RequestMapping("/exportDyInfoChildModule")
    public Message exportDyInfoChildModule(HttpServletResponse response, @RequestBody TDyInfoChildModuleReq req) {
        Message message = new Message();
        try {
            message.setData(memberService.exportDyInfoChildModule(response,req,req.getOrgcode(),req.getStartTime(),req.getEndTime(),req.getType(),req.getXm(),req.getZzmm(),getSession()));
            message.success();
        } catch (Exception e) {
            LehandException.throwException("导出先进基层党组织信息失败！"+e);
        }
        return message;
    }
    /**
     * 驾驶舱党员党龄统计
     * @author lx
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "驾驶舱党员党龄统计", notes = "驾驶舱党员党龄统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "organid", value = "组织id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/partyAgeInfo")
    public Message partyAgeInfo(String orgcode,String organid) {
        Message message = new Message();
        try {

            LocalDate localDate = LocalDate.now();

            LocalDate parse = LocalDate.parse("2011-12-07");

            Period between = Period.between(parse,localDate);

            List<Map<String,Object>> list = baseDzzTotalService.partyAgeInfo(orgcode,organid);
            message.setData(list);
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("年龄分布信息查询出错");
        }
        return message;
    }
}
