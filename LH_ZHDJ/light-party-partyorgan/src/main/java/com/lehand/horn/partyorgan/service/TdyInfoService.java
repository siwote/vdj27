//package com.lehand.horn.partyorgan.service;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.Resource;
//
//import org.springframework.util.StringUtils;
//
//import com.lehand.base.common.Pager;
//import com.lehand.components.db.GeneralSqlComponent;
//import com.lehand.horn.partyorgan.constant.SysConstants.DYLB;
//import com.lehand.horn.partyorgan.constant.SysConstants.DYZT;
//import com.lehand.horn.partyorgan.pojo.dy.TDyInfo;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
//import com.lehand.horn.partyorgan.service.dzz.TdzzInfoBcService;
//import com.lehand.horn.partyorgan.util.SysUtil;
//
///**
// * 党员信息业务层
// * @author zouwendong
// * @date 2019年1月22日 上午10:34:22
// * @version V1.0
// */
//public class TdyInfoService{
//
//	@Resource
//	GeneralSqlComponent generalSqlComponent;
//
//	@Resource
//	private GdictItemService gdictItemService;
//
//	@Resource
//	private TdzzInfoSimpleDao tdzzInfoSimpleDao;
//
//	@Resource
//	private TdzzInfoBcService tdzzInfoBcService;
//
//	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//
//
//
//	/**
//	 * 分页查询
//	 */
//	public Pager query(Session session,String xm,String zjhm,String dylb,String organid,Pager pager){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		/**
//		 * 定义党员信息集合
//		 */
//		List<TDyInfo> dyList = new ArrayList<>();
//		if(StringUtils.isEmpty(organid)) {
//			organid = tdzzInfoBcService.getOrgId(session);
//		}
//		/**
//		 * 定义查询条件
//		 */
//		StringBuffer sql = new StringBuffer();
//		List<String> params = new ArrayList<String>(4);
//		sql.append("select * from t_dy_info where organid = ? and operatetype != 3 and dyzt = ?");
//		params.add(organid);
//		params.add(DYZT.ZC);
//		if(!StringUtils.isEmpty(xm)) {
//			sql.append(" and CONCAT(xm,zjhm) like ?");
//			params.add("%"+xm+"%");
//		}
////		if(!StringUtils.isEmpty(zjhm)) {
////			sql.append(" and zjhm = ?");
////			params.add(zjhm);
////		}
//		if(!StringUtils.isEmpty(dylb)) {
//			sql.append(" and dylb = ?");
//			params.add(dylb);
//		}else {
//			sql.append(" and ( dylb = '"+DYLB.ZSDY+"' or dylb='"+DYLB.YBDY+"' )");
//		}
//		sql.append("order by sortnum");
//		dyList = tDyInfoDao.query(sql.toString(),pager,params.toArray());
//		if(dyList.size()>0) {
//			Map<String, Object> bean = null;
//			for(TDyInfo tdy:dyList) {
//				bean  = new HashMap<>();
//				bean.put("userId", tdy.getUserid());
//				bean.put("xm", tdy.getXm());
//				bean.put("zjhm", tdy.getZjhm());
//				bean.put("xb", tdy.getXb());
//				bean.put("mz", gdictItemService.getItemName("d_national", tdy.getMz()));
//				bean.put("age", SysUtil.getAgeFromBirthTime(tdy.getCsrq()));
//				bean.put("dylb", gdictItemService.getItemName("d_dy_rylb", tdy.getDylb()));
//				bean.put("organid", tdy.getOrganid());
////				bean.put("operatorname", tdy.getOperatorname());
//				bean.put("operatorname", getOrgName(tdy.getOrganid()));
//				bean.put("xxwzd", tdy.getXxwzd());
//				bean.put("csrq", tdy.getCsrq());
//				bean.put("xl", tdy.getXl());
//				bean.put("lxdh", tdy.getLxdh());
//				bean.put("flag", false);
//				list.add(bean);
//			}
//		}
//		int count  = tDyInfoDao.queryCount("select count(*) from ( "+sql.toString()+") as b",params.toArray());
//		pager.setRows(list);
//		pager.setTotalRows(count);
//		int i = count % pager.getPageSize();
//	    int v = count / pager.getPageSize();
//	    pager.setTotalPages(i>0 ? v+1 : v);
//		return pager;
//
//	}
//
//
//	private String getOrgName(String organid) {
//		String sql = "SELECT * FROM `t_dzz_info` WHERE organid = ?";
//		TDzzInfoSimple info = tdzzInfoSimpleDao.get(sql, TDzzInfoSimple.class, new Object[] {organid});
//		return info.getDzzmc();
//	}
//
//
//	/**
//	 *
//	 * @Description: 获取党员基本信息
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月22日 下午7:16:44
//	 */
////	public List<Map<String,Object>> queryEssential(String userId){
////		/**
////		 * 定义list数组
////		 */
////		List<Map<String,Object>> list = new ArrayList<>();
////
////		/**
////		 * 定义查询条件
////		 */
////		StringBuffer sql = new StringBuffer();
////		sql.append("select  a.userid,a.xm,a.xb,a.zjhm,a.csrq,a.mz,a.jg,a.xl,b.xw,b.byyx,b.zy,a.rdsj,a.zzsj,c.dnzwname,a.gzgw,b.jszc");
////		sql.append(",b.xshjclx,a.dylb,b.isworkers,a.lxdh,a.gddh,a.operatorname,b.dwmc,b.dydaszdw,b.hjdz_qhnxxdz,b.xjzd");
////		sql.append("  from t_dy_info a INNER JOIN t_dy_info_bc b ON b.userid = a.userid");
////		sql.append("  LEFT JOIN t_dzz_bzcyxx c ON c.userid = a.userid and c.sfzr = 1 and c.organid = a.organid WHERE a.userid = ?");
////		List<String> params = new ArrayList<String>(1);
////		params.add(userId);
////		list = tDyInfoDao.queryEssential(sql.toString(), userId);
////		for(Map<String,Object> map :list) {
////			map.put("mz", map.get("mz"));
////			map.put("mzName", map.get("mz")==null? map.get("mz"):gdictItemService.getItemName("d_national", map.get("mz").toString()));
////			map.put("age", SysUtil.getAgeFromBirthTime(map.get("csrq").toString()));
////			map.put("xl",  map.get("xl"));
////			map.put("xlName",  map.get("xl")==null?map.get("xl"):gdictItemService.getItemName("d_dy_xl", map.get("xl").toString()));
////			map.put("xw",  map.get("xw"));
////			map.put("xwName",  map.get("xw")==null?map.get("xw"):gdictItemService.getItemName("d_dy_xw", map.get("xw").toString()));
////			map.put("dnzwname",  map.get("dnzwname"));
////			map.put("dnzwnameName",  map.get("dnzwname")==null?map.get("dnzwname"):gdictItemService.getItemName("d_dy_dnzw", map.get("dnzwname").toString()));
////			map.put("gzgw",  map.get("gzgw"));
////			map.put("gzgwName",  map.get("gzgw")==null?map.get("gzgw"):gdictItemService.getItemName("d_dy_gzgw", map.get("gzgw").toString()));
////			map.put("jszc",  map.get("jszc"));
////			map.put("jszcName",  map.get("jszc")==null?map.get("jszc"):gdictItemService.getItemName("d_dy_jszc", map.get("jszc").toString()));
////			map.put("xshjclx",  map.get("xshjclx"));
////			map.put("xshjclxName",  map.get("xshjclx")==null?map.get("xshjclx"):gdictItemService.getItemName("d_dy_shjc", map.get("xshjclx").toString()));
////			map.put("dylb",  map.get("dylb"));
////			map.put("dylbName",  map.get("dylb")==null?map.get("dylb"):gdictItemService.getItemName("d_dzz_zrdzzqk", map.get("dylb").toString()));
//////			map.put("xb", "1".equals(map.get("xb"))? "男":("2".equals(map.get("xb"))? "女":"未知"));
////			map.put("xb", map.get("xb")==null?map.get("xb"):gdictItemService.getItemName("d_sexid", map.get("xb").toString()));
////		}
////		return list;
////	}
//
//	/**
//	 *
//	 * @Description:  查询党籍和党组织
//	 * @param userId
//	 * @param organid
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月22日 下午8:05:04
//	 */
//	public List<Map<String,Object>> queryPartyMembershipOrg(String userId,String organid){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer partySql = new StringBuffer();
//		partySql.append("select a.rdsj,a.zzsj,a.operatorname,b.rdszdzbmc,b.rdjsr,b.zzqk,c.qtdt,c.jrqtdtrq,c.lkqtdtrq from t_dy_info a LEFT JOIN t_dy_fzshxx b ON b.userid = a.userid ");
//		partySql.append(" INNER JOIN t_dy_info_bc c ON c.userid = a.userid where a.userid = ?");
//		list = tDyInfoDao.queryParty(partySql.toString(), userId);
//		for(Map<String,Object> map :list) {
//			map.put("partyAge",  StringUtils.isEmpty(map.get("zzsj"))? map.get("zzsj"):SysUtil.getAgeFromBirthTime(map.get("zzsj").toString()));
//			map.put("rdszdzbmc",  map.get("rdszdzbmc")==null?"由【入党信息】同步":map.get("rdszdzbmc"));
//			map.put("rdjsr",  map.get("rdjsr")==null?"由【入党信息】同步":map.get("rdjsr"));
//			map.put("zzqk",  map.get("zzqk")==null?"由【入党信息】同步":map.get("zzqk").toString()+" "+gdictItemService.getItemName("d_fzdy_scjg", map.get("zzqk").toString()));
//			map.put("qtdt",  map.get("qtdt")==null? map.get("qtdt"):map.get("qtdt").toString()+" "+gdictItemService.getItemName("d_dy_qtdt", map.get("qtdt").toString()));
//		}
//		/**
//		 * 流入党组织
//		 */
//		StringBuffer lrdySql = new StringBuffer();
//		List<Map<String,Object>> listLrdy = new ArrayList<>();
//		Map<String,Object> mapLrdy = new HashMap<>();
//		lrdySql.append("select b.dzzmc,a.lrdzzid,a.ldtype,a.adminname from t_dy_lrdy a");
//		lrdySql.append(" INNER JOIN t_dzz_info_simple b on b.organid = a.lrdzzid where a.organid = ? and a.userid = ?");
//		listLrdy = tDyInfoDao.querylcdy(lrdySql.toString(), userId,organid);
//		list.add(listLrdy.size()>0? listLrdy.get(0):mapLrdy);
//		/**
//		 * 流出党组织
//		 */
//		StringBuffer lcdySql = new StringBuffer();
//		List<Map<String,Object>> listLcdy = new ArrayList<>();
//		Map<String,Object> mapLcdy = new HashMap<>();
//		lcdySql.append("select b.dzzmc,a.organid,a.ldtype,a.blryname from t_dy_lcdy a");
//		lcdySql.append(" INNER JOIN t_dzz_info_simple b on b.organid = a.organid where a.organid = ? and a.userid = ?");
//		listLcdy = tDyInfoDao.querylcdy(lcdySql.toString(), userId,organid);
//		list.add(listLcdy.size()>0? listLcdy.get(0):mapLcdy);
//		return list;
//	}
//
//
//	/**
//	 *
//	 * @Description: 职务
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月23日 下午8:50:51
//	 */
//	public List<Map<String,Object>> queryPost(String userId){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.organname,a.dnzwname,a.rzdate,a.lzdate,b.ddbqk,b.ksrq,b.dqrq,b.jc,a.zwlevel,a.dnzwsm,a.sfzr,b.ldbywyzzyy,b.ldbywyzzrq from t_dzz_bzcyxx a ");
//		sql.append(" LEFT  JOIN t_dy_ddb b on b.userid = a.userid and b.sfzr = 1 where a.userid = ?  ");
//		sql.append("and a.sfzr = 1 ");
//		list = tDyInfoDao.queryParty(sql.toString(), userId);
//		for(Map<String,Object> map:list) {
//			map.put("dnzwname",  map.get("dnzwname")==null? map.get("dnzwname"):gdictItemService.getItemName("d_dy_dnzw", map.get("dnzwname").toString()));
//			map.put("zwlevel",  map.get("zwlevel")==null? map.get("zwlevel"):gdictItemService.getItemName("d_dzz_zwjb", map.get("zwlevel").toString()));
//			map.put("ldbywyzzyy",  map.get("ldbywyzzyy")==null? map.get("ldbywyzzyy"):gdictItemService.getItemName("d_dy_cdyy", map.get("ldbywyzzyy").toString()));
////			map.put("sfzr",  "1".equals(map.get("sfzr"))? "是":("0".equals(map.get("sfzr"))? "否":null));
//			map.put("sfzr",  "是");
//		}
//		return list;
//	}
//
//	/**
//	 *
//	 * @Description: 奖惩信息
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月24日 上午9:12:06
//	 */
//	public List<Map<String,Object>> queryJcxx(String userId){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append("select userid,jcmc,jcsm,jcpzjg,pzrq,cxrq from t_dy_jcxx where userid = ?");
//		list = tDyInfoDao.queryParty(sql.toString(), userId);
//		for(Map<String,Object> map:list) {
//			map.put("jcmc",  map.get("jcmc")==null? map.get("jcmc"):gdictItemService.getItemName("d_dy_jcmc", map.get("jcmc").toString()));
//		}
//		return list;
//
//	}
//
//	/**
//	 *
//	 * @Description: 困难情况
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月24日 上午9:29:32
//	 */
//	public List<Map<String,Object>> queryknqk(String userId){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append("select userid,shknlx,sfxsczzzshbz,fxbz,jkzk,knqkbc from t_dy_knqk where userid = ?");
//		list = tDyInfoDao.queryParty(sql.toString(), userId);
//		for(Map<String,Object> map:list) {
//			map.put("shknlx",  map.get("shknlx")==null? map.get("shknlx"):gdictItemService.getItemName("d_dy_shkn", map.get("shknlx").toString()));
//			map.put("sfxsczzzshbz",  "1".equals(map.get("sfxsczzzshbz"))? "是":("0".equals(map.get("sfxsczzzshbz"))? "否":null));
//			map.put("fxbz",  "1".equals(map.get("fxbz"))? "是":("0".equals(map.get("fxbz"))? "否":null));
//			map.put("jkzk",  map.get("jkzk")==null? map.get("jkzk"):gdictItemService.getItemName("d_dy_jkzk", map.get("jkzk").toString()));
//		}
//		return list;
//	}
//
//	/**
//	 *
//	 * @Description: 出国出境
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月24日 上午9:55:41
//	 */
//	public List<Map<String,Object>> queryCgcj(String userId){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append("select userid,szgjmc,cgyy,cgrq,hgrq,ydzzlxqk,djclfs,sqbldjsj,hgqk,hfzzshqk,pzzhfzzshrq,cgbz from t_dy_cgcj where userid = ?");
//		list = tDyInfoDao.queryParty(sql.toString(), userId);
//		for(Map<String,Object> map:list) {
//			map.put("szgjmc",  map.get("szgjmc")==null? map.get("szgjmc"):gdictItemService.getItemName("d_dy_country", map.get("szgjmc").toString()));
//			map.put("cgyy",  map.get("cgyy")==null? map.get("cgyy"):gdictItemService.getItemName("d_dy_cgyy", map.get("cgyy").toString()));
//			map.put("ydzzlxqk",  map.get("ydzzlxqk")==null? map.get("ydzzlxqk"):gdictItemService.getItemName("d_dy_cgyzzlx", map.get("ydzzlxqk").toString()));
//			map.put("djclfs",  map.get("djclfs")==null? map.get("djclfs"):gdictItemService.getItemName("d_dy_djcl", map.get("djclfs").toString()));
//			map.put("hgqk",  map.get("hgqk")==null? map.get("hgqk"):gdictItemService.getItemName("d_dy_hgqk", map.get("hgqk").toString()));
//			map.put("hfzzshqk",  map.get("hfzzshqk")==null? map.get("hfzzshqk"):gdictItemService.getItemName("d_dy_hfzzsh", map.get("hfzzshqk").toString()));
//		}
//		return list;
//	}
//
//	/**
//	 *
//	 * @Description:  入党信息
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月24日 上午11:38:26
//	 */
//	public List<Map<String,Object>> queryRdxx(String userId){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.fzsxl,a.fzsgzgw,a.fzsjszw,a.fzsyxqk,a.fzsxshjc,b.rdsqri,b.lwrdjjfzrq,b.pylxr,b.lwfzdxrq,b.rdjsr, ");
//		sql.append(" b.yssj,c.rdsj,b.dhtljg,a.rdszdzbmc,b.sjzzthrq,b.fzdythr,b.sjsprq,a.rdlx,b.zzsqrq,a.zbdhtlrq,a.zzqk,c.zzsj,a.ycybqsj,a.zzsjsprq ");
//		sql.append(" from t_dy_info c left join t_dy_pyfz b on b.userid = c.userid left join t_dy_fzshxx a on a.userid = c.userid where c.userid = ?");
//		list = tDyInfoDao.queryParty(sql.toString(), userId);
//		for(Map<String,Object> map:list) {
//			map.put("fzsxl",  map.get("fzsxl")==null? map.get("fzsxl"):gdictItemService.getItemName("d_dy_country", map.get("fzsxl").toString()));
//			map.put("fzsgzgw",  map.get("fzsgzgw")==null? map.get("fzsgzgw"):gdictItemService.getItemName("d_dy_gzgw", map.get("fzsgzgw").toString()));
//			map.put("fzsjszw",  map.get("fzsjszw")==null? map.get("fzsjszw"):gdictItemService.getItemName("d_dy_jszc", map.get("fzsjszw").toString()));
//			map.put("fzsyxqk",  map.get("fzsyxqk")==null? map.get("fzsyxqk"):gdictItemService.getItemName("d_dy_yxqk", map.get("fzsyxqk").toString()));
//			map.put("fzsxshjc",  map.get("fzsxshjc")==null? map.get("fzsxshjc"):gdictItemService.getItemName("d_dy_shjc", map.get("fzsxshjc").toString()));
//			map.put("dhtljg",  "1".equals(map.get("fxbz"))? "通过":("0".equals(map.get("fxbz"))? "不通过":null));
//			map.put("rdlx",  map.get("rdlx")==null? map.get("rdlx"):gdictItemService.getItemName("d_dy_jrzz", map.get("rdlx").toString()));
//			map.put("zzqk",  map.get("zzqk")==null? map.get("zzqk"):gdictItemService.getItemName("d_dy_zzqk", map.get("zzqk").toString()));
//			map.put("ycybqsj",  map.get("ycybqsj")==null? map.get("ycybqsj"):gdictItemService.getItemName("d_dy_ycybqsj", map.get("ycybqsj").toString()));
//
//		}
//		return list;
//	}
//
//	/**
//	 *
//	 * @Description:流动党员
//	 * @param userId
//	 * @return
//	 * @return List<Map<String,Object>>
//	 * @throws
//	 * @author zouwendong
//	 * @date:   2019年1月24日 下午9:38:04
//	 */
//	public List<Map<String,Object>> queryLddy(String userId){
//		/**
//		 * 定义list数组
//		 */
//		List<Map<String,Object>> list = new ArrayList<>();
//		StringBuffer sql = new StringBuffer();
//		sql.append("select wclx,wcdd,wcyy,wcrq,wcddbcsm,ldzt,lcdzbmc,lrdzbmc,lcdzbsj,lrdzbsj,lclxdh,lrlxdh,lxqk,fhrq,sqlxqx,sqlxrq from t_dy_lddy where userid = ?");
//		list = tDyInfoDao.queryParty(sql.toString(), userId);
//		for(Map<String,Object> map:list) {
//			map.put("wcdd",  map.get("wcdd")==null? map.get("wcdd"):gdictItemService.getItemName("d_dzz_xzqh", map.get("wcdd").toString()));
//			map.put("wclx",  map.get("wclx")==null? map.get("wclx"):gdictItemService.getItemName("d_dy_wclx", map.get("wclx").toString()));
//			map.put("ldzt",  map.get("ldzt")==null? map.get("ldzt"):gdictItemService.getItemName("d_dy_ldzt", map.get("ldzt").toString()));
//			map.put("sqlxqx",  map.get("sqlxqx")==null? map.get("sqlxqx"):gdictItemService.getItemName("d_dy_sljtqx", map.get("sqlxqx").toString()));
//			map.put("lxqk",  map.get("lxqk")==null? map.get("lxqk"):gdictItemService.getItemName("d_dy_cgyzzlx", map.get("lxqk").toString()));
//		}
//		return list;
//	}
//}
