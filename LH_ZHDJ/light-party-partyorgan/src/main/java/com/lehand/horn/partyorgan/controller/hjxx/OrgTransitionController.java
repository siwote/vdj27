package com.lehand.horn.partyorgan.controller.hjxx;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.dto.PartyGroupDto;
import com.lehand.horn.partyorgan.dto.TDzzHjxxInfoDto;
import com.lehand.horn.partyorgan.pojo.hjxx.TDzzHjxxInfo;
import com.lehand.horn.partyorgan.service.hjxx.OrgTransitionService;
import com.lehand.horn.partyorgan.service.info.PartyGroupService;
import com.lehand.module.urc.controller.RoleController;
import com.lehand.module.urc.pojo.Role;
import com.lehand.module.urc.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: OrgTransitionController
 * @Description: 组织换届控制层
 * @Author lx
 * @DateTime 2021-05-10 14:00
 */
@Api(value = "组织换届控制层", tags = { "组织换届控制层" })
@RestController
@RequestMapping("/lhdj/transition")
public class OrgTransitionController extends BaseController {

    @Resource
    private OrgTransitionService transitionService;

    /**
     * Description:分页查询组织换届信息
     * @param orgcode
     * @return
     * @Author lx
     * @DateTime 2021-05-10 15:00
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询组织换届信息", notes = "分页查询组织换届信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织编码", dataType = "String",required = true,  defaultValue = "")
    })
    @RequestMapping("/page")
    public Message page(String orgcode,Pager pager) {
        Message message = new Message();
        try {
            pager = transitionService.page(getSession(),pager,orgcode);
            message.setData(pager);
            message.success();
        } catch (Exception e) {
            message.setMessage("查询出错"+e.getMessage());
        }
        return message;
    }

    /**
     * Description:添加换届信息
     * @param dto
     * @return
     * @Author lx
     * @DateTime 2021-05-10 15:00
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "换届维护", notes = "换届维护", httpMethod = "POST")
    @RequestMapping("/add")
    public Message add(@RequestBody TDzzHjxxInfoDto dto) {
        Message msg = new Message();
        try {
            transitionService.modify(dto, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("添加失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("添加失败");
        }
        return msg;
    }
    /**
     * Description:查看最新一条换届信息
     * @param orgcode
     * @return
     * @Author lx
     * @DateTime 2021-05-11 15:20
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看最新一条换届信息", notes = "查看最新一条换届信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织编码", dataType = "String",required = true,  defaultValue = "")
    })
    @RequestMapping("/getLatestHJXXInfo")
    public Message getNowHJXXInfo(String orgcode) {
        Message msg = new Message();
        try {
            msg.setData(transitionService.getLatestHJXXInfo(orgcode, getSession()));
            msg.success();
        } catch (LehandException e) {
            msg.fail("获取失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("获取失败");
        }
        return msg;
    }
    /**
     * Description:分页根据角色名称和组织编码查询组织用户
     * @param
     * @return
     * @Author lx
     * @DateTime 2021-05-10 15:00
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页根据角色名称和组织编码查询组织用户", notes = "分页根据角色名称和组织编码查询组织用户", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织编码", dataType = "String",required = true,  defaultValue = ""),
            @ApiImplicitParam(name = "rolename", value = "角色名称", dataType = "String",required = true,  defaultValue = "")
    })
    @RequestMapping("/pageRole")
    public Message pageRole(String orgcode,String rolename) {
        Message message = new Message();
        try {
            message.setData(transitionService.pageRole(orgcode, rolename,1));
            message.success();
        } catch (Exception e) {
            message.setMessage("查询出错"+e.getMessage());
        }
        return message;
    }
    /**
     * Description:修改换届提醒配置
     * @param dto
     * @return
     * @Author lx
     * @DateTime 2021-05-12 11:40
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "修改换届提醒配置", notes = "修改换届提醒配置", httpMethod = "POST")
    @RequestMapping("/update")
    public Message update(@RequestBody TDzzHjxxInfoDto dto) {
        Message msg = new Message();
        try {
            transitionService.update(dto, getSession());
            msg.success();
        } catch (LehandException e) {
            msg.fail("添加失败" + e.getMessage());
        } catch (Exception e) {
            msg.fail("添加失败");
        }
        return msg;
    }
    /**
     * Description:工作台统计数据
     * @param
     * @return
     * @Author lx
     * @DateTime 2021-05-12 15:15
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "工作台统计数据", notes = "工作台统计数据", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgid", value = "组织id", dataType = "String",required = true,  defaultValue = "")
    })
    @RequestMapping("/statisticalHJXX")
    public Message statisticalHJXX(String orgid) {
        Message message = new Message();
        try {
            message.setData(transitionService.statisticalHJXX(orgid,getSession()));
            message.success();
        } catch (Exception e) {
            message.setMessage("查询出错"+e.getMessage());
        }
        return message;
    }
}
