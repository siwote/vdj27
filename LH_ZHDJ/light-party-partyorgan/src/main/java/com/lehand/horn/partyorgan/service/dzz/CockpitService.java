package com.lehand.horn.partyorgan.service.dzz;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.PilotPlanSqlCode;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dto.TDzzPartyBrandDynamicReq;
import com.lehand.horn.partyorgan.dto.TDzzPartyBrandReq;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrand;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandDynamic;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandPraise;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CockpitService {

    @Resource
    GeneralSqlComponent generalSqlComponent;

    public List<Map<String,Object>> mapInfo(String szsf, Session session) {
        //是否是一级地图
        if(StringUtils.isEmpty(szsf)){
            return generalSqlComponent.getDbComponent().listMap("SELECT COUNT(1) count,bc.dzzszsf name FROM t_dzz_info a,t_dzz_info_bc bc where a.organid=bc.organid  GROUP BY bc.dzzszsf",new Object[]{});
        }else{
            return generalSqlComponent.getDbComponent().listMap("SELECT COUNT(1) count,bc.dzzszsq name FROM t_dzz_info a,t_dzz_info_bc bc where a.organid=bc.organid  AND bc.dzzszsf =? GROUP BY dzzszsq",new Object[]{szsf});
        }
    }
}
