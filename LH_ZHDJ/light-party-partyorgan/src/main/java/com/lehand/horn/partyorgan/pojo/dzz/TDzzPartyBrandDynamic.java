package com.lehand.horn.partyorgan.pojo.dzz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 领航计划(品牌动态)表
 * @author lx
 */
@ApiModel(value="领航计划(品牌动态)表",description="领航计划(品牌动态)表")
public class TDzzPartyBrandDynamic implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "自增长主键", name = "id")
  private Long id;
  @ApiModelProperty(value="品牌党组织编码",name="orgcode")
  private String orgcode;
  @ApiModelProperty(value="动态标题",name="title")
  private String title;
  @ApiModelProperty(value="品牌logo图片",name="fileid")
  private String fileid;
  @ApiModelProperty(value="状态（0：未发布，1：已发布,默认0）",name="status")
  private String status;
  @ApiModelProperty(value="品牌内容",name="content")
  private String content;
  @ApiModelProperty(value="首次录入操作人",name="creator")
  private String creator;
  @ApiModelProperty(value="首次录入时间",name="createtime")
  private String createtime;
  @ApiModelProperty(value="更新操作人",name="updator")
  private String updator;
  @ApiModelProperty(value="更新操作时间",name="updatetime")
  private String updatetime;

  @ApiModelProperty(value="领航计划：品牌id",name="brandid")
  private Long brandid;

  public Long getBrandid() {
    return brandid;
  }

  public void setBrandid(Long brandid) {
    this.brandid = brandid;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOrgcode() {
    return orgcode;
  }

  public void setOrgcode(String orgcode) {
    this.orgcode = orgcode;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }


  public String getFileid() {
    return fileid;
  }

  public void setFileid(String fileid) {
    this.fileid = fileid;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }


  public String getCreatetime() {
    return createtime;
  }

  public void setCreatetime(String createtime) {
    this.createtime = createtime;
  }


  public String getUpdator() {
    return updator;
  }

  public void setUpdator(String updator) {
    this.updator = updator;
  }


  public String getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(String updatetime) {
    this.updatetime = updatetime;
  }

}
