package com.lehand.horn.partyorgan.service;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.interceptors.DefaultInterceptor;
import com.lehand.module.urc.dto.SystemLogRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

@Service
public class NewLogService extends DefaultInterceptor {
    @Resource
    protected GeneralSqlComponent generalSqlComponent;

    private static int optflag_4 = 4;

    public Map<String, Object> getUsages(SystemLogRequest param, final Session session) {
        HashMap loginMap = new HashMap();

        try {
            final Date startDate = DateEnum.YYYYMMDD.parse(param.getStartdate());
            final Date endDate = DateEnum.YYYYMMDD.parse(param.getEnddate());

            List userTop;

            final List<Map<String, Object>> orgPies = (List)this.generalSqlComponent.query("getNewSystemLogOrgForCount", new HashMap<String, Object>() {
                {
                    this.put("optflag", NewLogService.optflag_4);
                    this.put("compid", session.getCompid());
                    this.put("startdate", DateEnum.YYYYMMDD.format(startDate));
                    this.put("enddate", DateEnum.YYYYMMDD.format(endDate));
                }
            });
            final List<String> xorgPie = new ArrayList();
            Iterator var20 = orgPies.iterator();

            while(var20.hasNext()) {
                Map<String, Object> orgPie = (Map)var20.next();
                xorgPie.add((String)orgPie.get("name"));
            }

            loginMap.put("loginOrgPie", new HashMap<String, Object>() {
                {
                    this.put("x", xorgPie);
                    this.put("data", orgPies);
                }
            });
            userTop = (List)this.generalSqlComponent.query("getNewSystemLogUserForCount", new HashMap<String, Object>() {
                {
                    this.put("optflag", NewLogService.optflag_4);
                    this.put("isLimit", true);
                    this.put("compid", session.getCompid());
                    this.put("startdate", DateEnum.YYYYMMDD.format(startDate));
                    this.put("enddate", DateEnum.YYYYMMDD.format(endDate));
                }
            });
            final List<String> usernames = new ArrayList();
            final List<Long> topdata = new ArrayList();
            Iterator var15 = userTop.iterator();

            while(var15.hasNext()) {
                Map<String, Object> top = (Map)var15.next();
                usernames.add((String)top.get("username"));
                topdata.add((Long)top.get("value"));
            }

            loginMap.put("loginTop", new HashMap<String, Object>() {
                {
                    this.put("x", usernames);
                    this.put("data", topdata);
                }
            });
        } catch (ParseException var17) {
            LehandException.throwException("查询使用统计错误", var17);
        }

        return loginMap;
    }
}
