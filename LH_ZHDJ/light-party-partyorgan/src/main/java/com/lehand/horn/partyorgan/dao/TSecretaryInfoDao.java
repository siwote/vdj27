package com.lehand.horn.partyorgan.dao;


public class TSecretaryInfoDao {
  private Long id;
  private String userid;
  private String organcode;
  private String parttimejob;
  private String worktime;
  private String partytime;
  private String secretarytime;
  private String gradschool;
  private String nationaltrain;
  private String grouptrain;
  private String rewards;
  private String performduties;
  private String report;
  private String resume;
  private Integer status;
  private Integer remarks2;
  private Integer remarks3;
  private String remarks4;
  private String remarks5;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }


  public String getOrgancode() {
    return organcode;
  }

  public void setOrgancode(String organcode) {
    this.organcode = organcode;
  }


  public String getParttimejob() {
    return parttimejob;
  }

  public void setParttimejob(String parttimejob) {
    this.parttimejob = parttimejob;
  }


  public String getWorktime() {
    return worktime;
  }

  public void setWorktime(String worktime) {
    this.worktime = worktime;
  }


  public String getPartytime() {
    return partytime;
  }

  public void setPartytime(String partytime) {
    this.partytime = partytime;
  }


  public String getSecretarytime() {
    return secretarytime;
  }

  public void setSecretarytime(String secretarytime) {
    this.secretarytime = secretarytime;
  }


  public String getGradschool() {
    return gradschool;
  }

  public void setGradschool(String gradschool) {
    this.gradschool = gradschool;
  }


  public String getNationaltrain() {
    return nationaltrain;
  }

  public void setNationaltrain(String nationaltrain) {
    this.nationaltrain = nationaltrain;
  }


  public String getGrouptrain() {
    return grouptrain;
  }

  public void setGrouptrain(String grouptrain) {
    this.grouptrain = grouptrain;
  }


  public String getRewards() {
    return rewards;
  }

  public void setRewards(String rewards) {
    this.rewards = rewards;
  }


  public String getPerformduties() {
    return performduties;
  }

  public void setPerformduties(String performduties) {
    this.performduties = performduties;
  }


  public String getReport() {
    return report;
  }

  public void setReport(String report) {
    this.report = report;
  }


  public String getResume() {
    return resume;
  }

  public void setResume(String resume) {
    this.resume = resume;
  }

  public long getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public long getRemarks2() {
    return remarks2;
  }

  public void setRemarks2(Integer remarks2) {
    this.remarks2 = remarks2;
  }


  public long getRemarks3() {
    return remarks3;
  }

  public void setRemarks3(Integer remarks3) {
    this.remarks3 = remarks3;
  }


  public String getRemarks4() {
    return remarks4;
  }

  public void setRemarks4(String remarks4) {
    this.remarks4 = remarks4;
  }


  public String getRemarks5() {
    return remarks5;
  }

  public void setRemarks5(String remarks5) {
    this.remarks5 = remarks5;
  }

}
