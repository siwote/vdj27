package com.lehand.horn.partyorgan.constant;

public class MemberSqlCode {

    /**
     * 党员列表
     *  select * from view_dy_info a where a.dyzt = '1000000003'
     *  and a.dylb <> '3000000003' and a.dylb <> '4000000004' and a.dylb <> '5000000005'
     *  and a.organid = :organid
     *  and a.age >= :startage
     *  and a.age<= :endage
     *  and a.dylb = :dylb
     *  and a.xl = :xl
     *  and a.xm like : xm
     *  and a.zjhm like :zjhm
     *
     */
    public final static String member_page = "member_page";

    /**
     * 党员列表(从非党支部部分查询)
     *  select * from view_dy_info  where dyzt = '1000000003'
     *  and dylb <> '3000000003' and dylb <> '4000000004' and dylb <> '5000000005'
     *  and :path = :organid
     *  and age >= :startage
     *  and age<= :endage
     *  and dylb = :dylb
     *  and xl = :xl
     *  and xm like : xm
     *  and zjhm like :zjhm
     *
     */
    public static String member_page_childs = "member_page_childs";


    /**
     *
     * select count(*)  from  view_dy_info a where a.dyzt = '1000000003'
     * and a.dylb <> '3000000003' and a.dylb <> '4000000004' and a.dylb <> '5000000005'
     * and a.organid = :organid
     *  and a.age >= :startage
     *  and a.age<= :endage
     *  and a.dylb = :dylb
     *  and a.xl = :xl
     *  and a.xm like : xm
     *  and a.zjhm like :zjhm
     * 党员列表查询总数
     */
    public final static String member_page_count = "member_page_count";


    /**
     * 下级党组织统计信息
     *  select count(if(t.dylb = '1000000001' , t.dylb, null)) storagecount ,
     * count(if(t.dylb = '1000000002' , t.dylb, null))  probationarycount ,
     * count(if(t.xb = '2000000004' , t.xb, null))  femalecount ,
     * count(if(t.mz <> '0100000037' , t.mz, null))  ethniccount ,
     * count(if(t.xl <> '8000000019' and xl<> '7000000018' and xl<> '6000000017' and xl<> '5000000016' and xl<> '4000000015', t.mz, null)) collegecount
     *  from ( select   a.dylb,a.xb,a.xl,a.mz,a.organid,b.parentid  from  t_dy_info a left join t_dzz_info_simple b on a.organid = b.organid  and b.parentid = :parentid and :path = b.organid) t group by t.organid
     */
    public final static String member_analysis_list = "member_analysis_list";

    /**
     * 下级党组织统计信息总条数
     * select count(*) from ( select   a.dylb,a.xb,a.xl,a.mz,a.organid,b.parentid  from  t_dy_info a left join t_dzz_info_simple b on a.organid = b.organid  and b.parentid = :parentid and :path =  b.organid group by a.organid) t
     */
    public final static String member_analysis_list_count= "member_analysis_list_count";

    /**
     * path查询
     * select * from t_dzz_info_simple where organid = ? and operatetype != 3 and zzlb != '3100000025' and zzlb != '3900000026' order by ordernum+0 asc
     */
    public final static String get_path_num ="get_path_num";

    /**
     * 本单位统计信息
     *  select count(if(t.dylb = '1000000001' , t.dylb, null)) storagecount ,
     * count(if(t.dylb = '1000000002' , t.dylb, null))  probationarycount ,
     * count(if(t.xb = '2000000004' , t.xb, null))  femalecount ,
     * count(if(t.mz <> '0100000037' , t.mz, null))  ethniccount ,
     * count(if(t.xl <> '8000000019' and xl<> '7000000018' and xl<> '6000000017' and xl<> '5000000016' and xl<> '4000000015', t.mz, null)) collegecount
     *  from ( select   a.dylb,a.xb,a.xl,a.mz,a.organid,b.parentid  from  t_dy_info a inner join t_dzz_info_simple b on a.organid = b.organid  and :path = :organid) t
     */
    public final static String get_current_analysis = "get_current_analysis";

    /**
     * 入党信息
     *  select a.fzsxl,a.fzsgzgw,a.fzsjszw,a.fzsyxqk,a.fzsxshjc,b.rdsqri,b.lwrdjjfzrq,b.pylxr,b.lwfzdxrq,b.rdjsr,
     *  b.yssj,c.rdsj,b.dhtljg,a.rdszdzbmc,b.sjzzthrq,b.fzdythr,b.sjsprq,a.rdlx,b.zzsqrq,a.zbdhtlrq,a.zzqk,c.zzsj,a.ycybqsj,a.zzsjsprq
     *  from t_dy_info c left join t_dy_pyfz b on b.userid = c.userid left join t_dy_fzshxx a on a.userid = c.userid where c.userid = ?
     */
    public final static String get_join_party = "get_join_party";

    /**
     * 流动信息
     * select wclx,wcdd,wcyy,wcrq,wcddbcsm,ldzt,lcdzbmc,lrdzbmc,lcdzbsj,lrdzbsj,lclxdh,lrlxdh,lxqk,fhrq,sqlxqx,sqlxrq from t_dy_lddy where userid = ?
     */
    public final static String get_flows = "get_flows";

    /**
     * 出国境信息
     * select userid,szgjmc,cgyy,cgrq,hgrq,ydzzlxqk,djclfs,sqbldjsj,hgqk,hfzzshqk,pzzhfzzshrq,cgbz from t_dy_cgcj where userid = ?
     */
    public  final static String get_go_abroad = "get_go_abroad";

    /**
     * 困难情况
     * select userid,shknlx,sfxsczzzshbz,fxbz,jkzk,knqkbc from t_dy_knqk where userid = ?
     */
    public final static String get_difficulty = "get_difficulty";


    /**
     * 奖惩信息
     * select userid,jcmc,jcsm,jcpzjg,pzrq,cxrq from t_dy_jcxx where userid = ?
     */
    public final static String get_rewards = "get_rewards";

    /**
     * 查询党员职务
     * select a.organname,a.dnzwname,a.rzdate,a.lzdate,b.ddbqk,b.ksrq,b.dqrq,b.jc,a.zwlevel,a.dnzwsm,a.sfzr,b.ldbywyzzyy,b.ldbywyzzrq,b.ldbywyjmrq,b.ldbywylb from t_dzz_bzcyxx a
     *  LEFT  JOIN t_dy_ddb b on b.userid = a.userid and  where a.userid = ?
     */
    public final static String get_post = "get_post";

    /**
     * 查询党籍和党组织
     *   select a.rdsj,a.zzsj,a.operatorname,b.rdszdzbmc,b.rdjsr,b.zzqk,c.qtdt,c.jrqtdtrq,c.lkqtdtrq,d.dzzmc from t_dy_info a LEFT JOIN t_dy_fzshxx b ON b.userid = a.userid left join t_dzz_info d on a.organid = d.organid 
     * INNER JOIN t_dy_info_bc c ON c.userid = a.userid where a.userid = ?
     */
    public final static String get_membership = "get_membership";

    /**
     * 流入党组织
     * select b.dzzmc,a.lrdzzid,a.ldtype,a.adminname from t_dy_lrdy a INNER JOIN t_dzz_info_simple b on b.organid = a.lrdzzid where a.organid = ? and a.userid = ?
     *
     */
    public final static String get_membership_in = "get_membership_in";

    /**
     * 流出党组织
     * select b.dzzmc,a.organid,a.ldtype,a.blryname from t_dy_lcdy a INNER JOIN t_dzz_info_simple b on b.organid = a.organid where a.organid = ? and a.userid = ?
     */
    public final static String get_membership_out = "get_membership_out";

    /**
     * 党员基本信息
     * select  a.userid,a.xm,a.xb,a.zjhm,a.csrq,a.mz,a.jg,a.xl,b.xw,b.byyx,b.zy,a.rdsj,a.zzsj,c.dnzwname,a.gzgw,b.jszc
     *  ,b.xshjclx,a.dylb,b.isworkers,a.lxdh,a.gddh,a.operatorname,b.dwmc dwname,b.dydaszdw,b.hjdz_qhnxxdz,b.xjzd
     *    from t_dy_info a left JOIN t_dy_info_bc b ON b.userid = a.userid
     *    LEFT JOIN t_dzz_bzcyxx c ON c.userid = a.userid  and c.organid = a.organid WHERE a.userid = ?
     */
    public final static String get_base = "get_base";

    /**
     * 根据身份证号获取userid
     *  select userid from t_dy_info  where zjhm = ? and dyzt = '1000000003'
     */
    public static String get_userid_by_idcard = "get_userid_by_idcard";

    /**
     * 根据组织机构编码获取organid
     * select organid from t_dzz_info_simple where  zfbz = 0 and orgcode = ?
     */
    public static String get_organid_by_orgcode  ="get_organid_by_orgcode";

    /**
     * 插入党员数据
     * insert into t_dy_info set organid = :organid,xm = :xm,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg,rdsj = :rdsj,dylb = :dylb,isworkers= :isworkers
     * ,lxdh = :lxdh,operatetype = :operatetype ,syncstatus = :syncstatus,delflag = :delflag,zjhm = :zjhm,xb = :xb, gzgw = :gzgw
     */
    public static String insert_t_dy_info = "insert_t_dy_info";

    /**
     *修改党员数据
     * update t_dy_info set organid = :organid,xm = :xm,xb = :xb,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg,age = :age,rdsj = :rdsj,zzsj = :zzsj,dylb = :dylb,isworkers= :isworkers,lxdh = :lxdh,operatetype = :operatetype ,syncstatus = :syncstatus,delflag = :delflag,syncstatus = :syncstatus where userid = :userid;
     */
    public static String update_t_dy_info = "update_t_dy_info";

    /**
     * 根据code获取字典数据
     * select item_code  id,item_name name from g_dict_item where code  = ?
     */
    public static String get_item_by_code = "get_item_by_code";
    /**
     *  select * from view_dy_info a where a.dyzt = '1000000003'  and a.dylb <> '3000000003' and a.dylb <> '4000000004' and a.dylb <> '5000000005' and  ${PARAMS.path} = :organid
     */
    public final static String member_page2 = "member_page2";

    /**
     * select * from t_dy_info where zjhm=?
     */
    public static String getTdyinfos = "getTdyinfos";

    /**
     * update t_dy_info set organid = :organid,xm = :xm,xl = :xl,csrq = :csrq,mz = :mz,jg = :jg,rdsj = :rdsj,zzsj = :zzsj,dylb = :dylb,isworkers= :isworkers,lxdh = :lxdh,operatetype = :operatetype ,syncstatus = :syncstatus,delflag = :delflag,syncstatus = :syncstatus where userid = :userid
     */
    public static String updateTdyinfos = "updateTdyinfos";

    /**
     * 获取党员信息
     * select * from t_dy_info where organid =:organid and (dylb ='1000000001' or dylb = '2000000002')  and (delflag!=1 or delflag is null)
     * and xm like :xm and zjhm like :zjhm and dylb = :dylb and xl=:xl and age between :a and :b
     */
    public static String getTDyInfoList2 = "getTDyInfoList2";
}
