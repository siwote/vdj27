package com.lehand.horn.partyorgan.pojo;

import com.lehand.base.constant.DateEnum;

import java.util.Date;

/**
 * 系统用户
 * @author
 */
public class SysUser  {

	public String id;

	/**
	 * 账户
	 */
	public String acc;

	/**
	 * 密码
	 */
	public String pwd;

	/**
	 * 姓名
	 */
	public String name;

	/**
	 * 部门ID
	 */
	public String orgId;

	/**
	 * 部门名称
	 */
	public String orgName;

	/**
	 * 类型：SYS系统管理员
	 */
	public String type;

	/**
	 * 角色
	 */
	public String roles;

	/**
	 * 备注
	 */
	public String remark;

	/**
	 * 状态
	 */
	public Integer status = 1;

	/**
	 * 排序号
	 */
	public Long orderId = 1L;

	/**
	 * 基础信息id
	 */
	public String baseInfoId;

	/**
	 * 联系方式
	 */
	public String workPhone;

	/**
	 * 盐
	 */
	public String salt;

	/**
	 * 创建人
	 */
	public String creater;

	/**
	 * 创建时间
	 */
	public Date createTime = DateEnum.now();

	/**
	 * 修改人
	 */
	public String modifier;

	/**
	 * 修改时间
	 */
	public Date modiTime = DateEnum.now();
	
	/**
	 * 移动设备ID
	 */
	public String clientId ;
	
	/**
	 * ios_token
	 */
	public String iosToken;
	
	/**
	 * 设备类型
	 */
	public String devType;
	
	/**
	 * 行政组织机构ID
	 */
	public String executiveOrgId;
	
	/**
	 * OA关联的账号
	 */
	public String oaUserAcc;
	
	/**
	 * 企业微信账号
	 */
	public String wxUserId;
	
	public String newPWD;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getBaseInfoId() {
		return baseInfoId;
	}

	public void setBaseInfoId(String baseInfoId) {
		this.baseInfoId = baseInfoId;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModiTime() {
		return modiTime;
	}

	public void setModiTime(Date modiTime) {
		this.modiTime = modiTime;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getIosToken() {
		return iosToken;
	}

	public void setIosToken(String iosToken) {
		this.iosToken = iosToken;
	}

	public String getDevType() {
		return devType;
	}

	public void setDevType(String devType) {
		this.devType = devType;
	}

	public String getExecutiveOrgId() {
		return executiveOrgId;
	}

	public void setExecutiveOrgId(String executiveOrgId) {
		this.executiveOrgId = executiveOrgId;
	}

	public String getOaUserAcc() {
		return oaUserAcc;
	}

	public void setOaUserAcc(String oaUserAcc) {
		this.oaUserAcc = oaUserAcc;
	}

	public String getWxUserId() {
		return wxUserId;
	}

	public void setWxUserId(String wxUserId) {
		this.wxUserId = wxUserId;
	}

	public String getNewPWD() {
		return newPWD;
	}

	public void setNewPWD(String newPWD) {
		this.newPWD = newPWD;
	}
}
