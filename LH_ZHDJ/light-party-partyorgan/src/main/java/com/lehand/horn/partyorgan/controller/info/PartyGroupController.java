package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.dto.PartyGroupDto;
import com.lehand.horn.partyorgan.service.info.PartyGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(value = "党小组", tags = { "提供党小组的信息管理" })
@RestController
@RequestMapping("/lhdj/partygroup")
public class PartyGroupController extends BaseController {

    @Resource
    private PartyGroupService partyGroupService;

    @OpenApi(optflag=0)
    @ApiOperation(value = "增加党小组", notes = "增加党小组", httpMethod = "POST")
    @RequestMapping("/addPartyGroup")
    public Message<PartyGroupDto> addPartyGroup(@RequestBody PartyGroupDto partyGroupDto) {
        Message<PartyGroupDto> msg = new Message<PartyGroupDto>();
        return msg.setData(partyGroupService.addPartyGroup(partyGroupDto, getSession())).success();
    }

    @OpenApi(optflag=2)
    @ApiOperation(value = "修改党小组信息", notes = "修改党小组信息", httpMethod = "POST")
    @RequestMapping("/modifyPartyGroup")
    public Message<PartyGroupDto> modifyPartyGroup(@RequestBody PartyGroupDto partyGroupDto) {
        Message<PartyGroupDto> msg = new Message<PartyGroupDto>();
        return msg.setData(partyGroupService.modifyPartyGroup(partyGroupDto, getSession())).success();
    }

    @OpenApi(optflag=1)
    @ApiOperation(value = "查询党小组信息列表", notes = "查询党小组信息列表", httpMethod = "POST")
    @RequestMapping("/queryPartyGroupList")
    public Message<Pager> queryPartyGroupList(String orgcode, Pager pager) {
        Message<Pager> msg = new Message<Pager>();
        return msg.setData(partyGroupService.queryPartyGroupList(orgcode, pager, getSession())).success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询党小组信息", notes = "查询党小组信息", httpMethod = "POST")
    @RequestMapping("/getPartyGroup")
    public Message<PartyGroupDto> getPartyGroup(Long id) {
        Message<PartyGroupDto> msg = new Message<PartyGroupDto>();
        return msg.setData(partyGroupService.getPartyGroup(id, getSession())).success();
    }

    @OpenApi(optflag=3)
    @ApiOperation(value = "删除党小组", notes = "删除党小组", httpMethod = "POST")
    @RequestMapping("/delPartyGroup")
    public Message<Object> delPartyGroup(Long id) {
        Message<Object> msg = new Message<Object>();
        partyGroupService.delPartyGroup(id, getSession());
        return msg.success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "查询可选择的党小组成员", notes = "查询可选择的党小组成员", httpMethod = "POST")
    @RequestMapping("/getOptionalMember")
    public Message<Object> getOptionalMember(String organid, Long groupid, String xm) {
        Message<Object> msg = new Message<Object>();
        return msg.setData(partyGroupService.getOptionalMember(organid, groupid, getSession(), xm)).success();
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "校验党小组同名", notes = "校验党小组同名", httpMethod = "POST")
    @RequestMapping("/checkIsExistsName")
    public Message<Object> checkIsExistsName(String orgcode, String name) {
        Message<Object> msg = new Message<Object>();
        return msg.setData(partyGroupService.checkIsExistsName(orgcode, name, getSession())).success();
    }

}
