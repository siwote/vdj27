//package com.lehand.horn.partyorgan.service.dzz;
//
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import com.lehand.horn.partyorgan.dao.TdzzInfoSimpleDao;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoBc;
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
//
//
///**
// * 组织机构补充表业务层
// * @author taogang
// * @version 1.0
// * @date 2019年1月21日, 下午8:32:38
// */
//@Service
//public class TdzzInfoBcService {
//
//	@Resource
//	private TdzzInfoBcDao tdzzInfoBcDao;
//
//	@Resource
//	private TdzzInfoSimpleDao tdzzInfoSimpleDao;
//
//	/**
//	 * 根据
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public String getDzzsj(String organid) {
//		String dzzsj = "";
//		TDzzInfoBc infobc = tdzzInfoBcDao.getInfoById(organid);
//		if(null != infobc) {
//			dzzsj = infobc.getDzzsj();
//		}
//		return dzzsj;
//	}
//
//
//	/**
//	 * 根据session获取组织机构id
//	 * @param session
//	 * @return
//	 */
//	public String getOrgId(Session session) {
//		String orgId = "";
//		List<Organ> orgList = session.getOrganids();
//		if(orgList.size() > 0 ) {
//			orgId = "0" + orgList.get(0).getOrgid().toString();
//		}
//		TDzzInfoSimple org = tdzzInfoSimpleDao.queryByCode(orgId);
//		if(org != null) {
//			orgId = org.getOrganid();
//		}
//		return orgId;
//	}
//
//	/**
//	 *
//	 * @Title：获取党组织简称
//	 * @Description：TODO
//	 * @param ：@param orgId
//	 * @param ：@return
//	 * @return ：String
//	 * @throws
//	 */
//	public String getDzzjc(String orgcode,Long compid) {
//		String dzzjc = null;
//		Map<String, Object> dzz = tdzzInfoSimpleDao.findjcByCode(compid,orgcode);
//		if(null != dzz) {
//			dzzjc = StringUtils.isEmpty(dzz.get("remarks6")) ? dzz.get("orgname").toString() : dzz.get("remarks6").toString() ;
//		}
//		return dzzjc;
//	}
//
//}
