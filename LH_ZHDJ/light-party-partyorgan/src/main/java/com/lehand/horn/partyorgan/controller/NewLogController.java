
package com.lehand.horn.partyorgan.controller;

import com.lehand.base.common.Message;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.service.NewLogService;
import com.lehand.module.urc.base.UrcBaseController;
import com.lehand.module.urc.dto.SystemLogRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(
    value = "新日志管理",
    tags = {"新日志管理"}
)
@RestController
@RequestMapping({"/urc/new/log"})
public class NewLogController extends UrcBaseController {
    @Resource
    private NewLogService newLogService;

    @OpenApi(
        optflag = 0
    )
    @ApiOperation(
        value = "使用统计",
        notes = "使用统计",
        httpMethod = "POST"
    )
    @RequestMapping(
        value = {"/usage"},
        method = {RequestMethod.POST}
    )
    public Message<Object> usage(SystemLogRequest param) {
        Message<Object> msg = new Message(Message.SUCCESS);
        msg.setData(this.newLogService.getUsages(param, this.getSession()));
        return msg;
    }
}
