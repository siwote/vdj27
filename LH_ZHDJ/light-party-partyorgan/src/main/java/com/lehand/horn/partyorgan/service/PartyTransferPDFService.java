package com.lehand.horn.partyorgan.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.horn.partyorgan.util.SysUtil;
import com.lehand.document.lib.pojo.DlFile;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import sun.misc.BASE64Decoder;
import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @program: huaikuang-dj
 * @description: PartyTransferService
 * @author: zwd
 * @create: 2020-11-25 15:16
 */
@Service
public class PartyTransferPDFService {
    @Resource
    GeneralSqlComponent generalSqlComponent;

    @Resource
    private BaseDzzTotalService baseDzzTotalService;
    @Resource
    private DFSComponent dFSComponent;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;

    @Value("${upload-path}")
    private String uploadTempPath;

    /**
     * A4的宽度
     */
    public static final int PAGE_A4_IMAGE_WIDTH = 500;

    /**
     * A4的高度
     */
    public static final int PAGE_A4_IMAGE_HEIGHT = 375;
    /**
     * 签名的宽度
     */
    public static final int PAGE_SIGN_IMAGE_WIDTH = 50;

    /**
     * 签名的高度
     */
    public static final int PAGE_SIGN_IMAGE_HEIGHT = 30;

    //预备党员
    @Value("${jk.organ.yubeidangyuan}")
    private String yubeidangyuan;
    //正式党员
    @Value("${jk.organ.zhengshidangyuan}")
    private String zhengshidangyuan;

    public Map<String,Object> getPartyLetter(Session session,String id) throws Exception{
        Map<String,Object> result = new HashMap<>();
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap(SqlCode.getPartyTransferByID, new Object[]{id});
        if(map!=null){
            Map<String, Object> dataMap = new HashMap<>();
            //党员id
            String userId = (String) map.get("userid");
            if(!(userId!=null)){
                LehandException.throwException("党员参数为空！");
            }

            Map<String,Object> dyData = generalSqlComponent.getDbComponent().getMap(SqlCode.getTDYInfo, new Object[]{userId});
            if(dyData.size()<1){
                LehandException.throwException("党员基本信息为空！");
            }

            //人员类别，(正式党员,预备党员）
            //人员类别
            String dylb =(String)dyData.get("dylb");
            dataMap.put("dylb",dylb);
            //性别
            /**
             * 0000000002	未知的性别
             * 1000000003	男
             * 2000000004	女
             */
            if ("1000000003".equals((String)dyData.get("xb").toString())) {
                dataMap.put("sex", "男");
            } else if ("2000000004".equals((String)dyData.get("xb").toString())) {
                dataMap.put("sex", "女");
            } else {
                dataMap.put("sex", "");
            }
            //迁出组织编码
            String outOrgCode = (String) map.get("outorgcode");
            //民族
            Map<String,Object> mzInfo = generalSqlComponent.getDbComponent().getMap(SqlCode.getMZInfo,new Object[] {(String)dyData.get("mz")});
//            List<Map<String, Object>> mzInfo = generalSqlComponent.getDbComponent().listMap(SqlCode.getMZInfo, new Object[]{(String) dyData.get("mz")});
            String mz = "";
            if(mzInfo!=null){
                mz=(String)mzInfo.get("item_name");
            }
            dataMap.put("mz",mz);

            String dylbName="";
            if(yubeidangyuan.equals(dylb)){
                dylbName="预备";
            }
            if(zhengshidangyuan.equals(dylb)){
                dylbName="正式";
            }
            dataMap.put("dylbName",dylbName);
            //党员电话号码
            String phone = (String) dyData.get("lxdh");
            dataMap.put("phone",phone==null?"":phone);
            //党费
            Map<String,Object> dzz = generalSqlComponent.getDbComponent().getMap(SqlCode.getDZZInfoSimpleByCode,new Object[] {outOrgCode});
            if(dzz==null){
                LehandException.throwException("党组织信息为空！");
            }
            String organid= (String) dzz.get("organid");
            Map<String,Object> dfInfo = generalSqlComponent.getDbComponent().getMap(SqlCode.getHDyIfInfoByUserId,new Object[] {userId,organid});
            Integer actualYear =0;
            Integer actualMonth=0;
            if(dfInfo!=null&&dfInfo.size()>0){
                //已交党费
                //年份
                actualYear=(Integer)dfInfo.get("year");
                //月份
                actualMonth=(Integer)dfInfo.get("month");
            }
            dataMap.put("actualYear",actualYear);
            dataMap.put("actualMonth",actualMonth);
            //党员名称
            String userName = (String) map.get("username");
            dataMap.put("userName",userName);
            //党员身份证号码
            String idCode = (String) map.get("idcode");
            String age = "";
            if(!StringUtils.isEmpty(dyData.get("age"))){
                age = String.valueOf(dyData.get("age"));
            }else if(!StringUtils.isEmpty(dyData.get("csrq"))){
                age = String.valueOf(SysUtil.getAgeFromBirthTime(String.valueOf(dyData.get("csrq"))));
            }
            dataMap.put("idCode",idCode);
            dataMap.put("age",age);
            //迁出支部书记信息
            //迁出支部书记userid
            String outSecretaryUserId = (String) map.get("outsecretary");
            Map<String, Object> outSJInfo = baseDzzTotalService.getPartySign(session, outSecretaryUserId);
            //迁出支部书记姓名
            String outSjName ="";
            //迁出支部书记电话
            String outSjPhone ="";
            //迁出支部书记签名（base64）
            String outSjSign ="";
            if(outSJInfo!=null&&outSJInfo.size()>0){
                //迁出支部书记姓名
                outSjName = (String) outSJInfo.get("username");
                //迁出支部书记电话
                outSjPhone = (String) outSJInfo.get("phone");
                //迁出支部书记签名（base64）
                outSjSign = (String) outSJInfo.get("jsondata");

            }
//            Map<String,Object> outSJInfo = getSJInfo(outOrgCode);
//            if(outSJInfo!=null&&outSJInfo.size()>0){
//                //迁出支部书记姓名
//                outSjName = (String) outSJInfo.get("username");
//                //迁出支部书记电话
//                outSjPhone = (String) outSJInfo.get("lxdh");
//                //迁出支部书记签名（base64）
//                Map<String,Object> user = getSJSign((String) outSJInfo.get("zjhm"));
//                if(user.size()>0){
//                    outSjSign = (String) user.get("jsondata");
//                }
//            }
            dataMap.put("outSjName",outSjName);
            dataMap.put("outSjPhone",outSjPhone);
            dataMap.put("outSjSign",outSjSign);
            //迁出组织名称
            String outOrgName = (String) map.get("outorgname");
            dataMap.put("outOrgName",outOrgName);
            //迁入组织编码(允许空)
            String intoOrgCode = (String) map.get("infoorgcode");
            //迁入支部书记信息
            String intoSjName ="";
            //迁入支部书记电话
            String intoSjPhone ="";
            //迁入书记签名(base64)
            String intoSjSign ="";
            //迁入支部书记userid
            String infoSecretaryUserId = (String) map.get("infosecretary");
            if(!org.apache.commons.lang.StringUtils.isEmpty(infoSecretaryUserId)){
                Map<String, Object> intoSJInfo = baseDzzTotalService.getPartySign(session, infoSecretaryUserId);
                if(intoSJInfo!=null&&intoSJInfo.size()>0){
                    //迁入支部书记姓名
                    intoSjName = (String) intoSJInfo.get("username");
                    //迁入支部书记电话
                    intoSjPhone = (String) intoSJInfo.get("phone");
                    //迁入书记签名(base64)
                    intoSjSign = (String) intoSJInfo.get("jsondata");

                }
            }
//            if(intoOrgCode!=null){
//                Map<String,Object> intoSJInfo = getSJInfo(intoOrgCode);
//                if(intoSJInfo!=null&&intoSJInfo.size()>0){
//                    //迁入支部书记姓名
//                    intoSjName = (String) intoSJInfo.get("username");
//                    //迁入支部书记电话
//                    intoSjPhone = (String) intoSJInfo.get("lxdh");
//                    //迁入书记签名(base64)
//                    Map<String,Object> user = getSJSign((String) intoSJInfo.get("zjhm"));
//                    if(user.size()>0){
//                        intoSjSign = (String) user.get("jsondata");
//                    }
//                }
//            }
            dataMap.put("intoSjName",intoSjName);
            dataMap.put("intoSjPhone",intoSjPhone);
            dataMap.put("intoSjSign",intoSjSign);
            //迁入组织名称(允许空)
            String intoOrgName = (String) map.get("infoorgname");
            dataMap.put("intoOrgName",intoOrgName);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sim1 = new SimpleDateFormat("yyyy");
            SimpleDateFormat sim2 = new SimpleDateFormat("MM");
            SimpleDateFormat sim3 = new SimpleDateFormat("dd");
            //序号
            Integer number = (Integer) map.get("number");
            //迁出时间
            String outTime = (String) map.get("outtime");
            String outTimeYear = "";
            String outTimeMonth = "";
            String outTimeDay = "";
            if(outTime!=null){
                Date parse = sdf.parse(outTime);
                outTimeYear = sim1.format(parse);
                outTimeMonth = sim2.format(parse);
               outTimeDay = sim3.format(parse);
            }
            dataMap.put("outTimeYear",outTimeYear);
            dataMap.put("outTimeMonth",outTimeMonth);
            dataMap.put("outTimeDay",outTimeDay);
            //迁出审核时间
            String outExamineTime = (String) map.get("outexaminetime");
            String outExamineTimeYear = "";
            String outExamineTimeMonth = "";
            String outExamineTimeDay = "";
            if(outExamineTime!=null){
                Date parse1 = sdf.parse(outExamineTime);
                outExamineTimeYear = sim1.format(parse1);
                outExamineTimeMonth = sim2.format(parse1);
                outExamineTimeDay = sim3.format(parse1);
            }
            dataMap.put("outExamineTimeYear",outExamineTimeYear);
            dataMap.put("outExamineTimeMonth",outExamineTimeMonth);
            dataMap.put("outExamineTimeDay",outExamineTimeDay);
            //迁入审核时间(允许空)
            String infoExamineTime = (String) map.get("infoexaminetime");
            String infoExamineTimeYear="";
            String infoExamineTimeMonth ="";
            String infoExamineTimeDay ="";
            if(infoExamineTime!=null){
                Date parse1 = sdf.parse(infoExamineTime);
                infoExamineTimeYear = sim1.format(parse1);
                infoExamineTimeMonth = sim2.format(parse1);
                infoExamineTimeDay = sim3.format(parse1);
            }
            //序号 迁出时间年+序号
            String n=number.toString().length()>1?"0"+number:"00"+number;
            dataMap.put("number",outTimeYear+n);
            dataMap.put("infoExamineTimeYear",infoExamineTimeYear);
            dataMap.put("infoExamineTimeMonth",infoExamineTimeMonth);
            dataMap.put("infoExamineTimeDay",infoExamineTimeDay);
            //经办人
            String agent ="";
            //经办人电话
            String agentPhone ="";
            //序号
            dataMap.put("agent",agent);
            dataMap.put("agentPhone",agentPhone);
            String pdfFilePath = createPDFFile(dataMap);
            result = getPdfPreview("介绍信", pdfFilePath, session, 4);
        }
        return result;
    }

    public String createPDFFile(Map<String,Object> data) throws Exception{
        BaseFont bfCN = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        //标题样式
        Font titlefont = new Font(bfCN, 30, Font.BOLD);
        //正常样式
        Font normalFont=new Font(bfCN,13f);
        //加粗样式
        Font boldFont=new Font(bfCN,25f);
        Font[] f={titlefont,normalFont,boldFont};

        String uuid = UUID.randomUUID().toString();
        String fullFilePath = String.format("%s/%s.pdf", uploadTempPath, uuid);
        //实例化文档以及属性
        Document document = new Document(PageSize.A4,-50,-50,0,0);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fullFilePath));
        document.open();
        document.newPage();
        writer.setPageEmpty(false);
        //第一联
        String title="中国共产党党员组织关系介绍信";
        document.add(getPdfTitle(title,titlefont));
        PdfPTable firstTable = getFirstTable(data, f, 200f);
        document.add(firstTable);

        //第二联
        String title2="中国共产党党员组织关系介绍信";
        document.add(getPdfTitle(title2,titlefont));
        PdfPTable firstTable2 = getFirstTable2(data, f, 300f);
        document.add(firstTable2);

        //第一联
        String title3="中国共产党党员组织关系介绍信回执信";
        document.add(getPdfTitle(title3,titlefont));
        PdfPTable firstTable3 = getFirstTable3(data, f, 100f);

        document.add(firstTable3);
        document.close();
        return fullFilePath;
    }

    /**
     * 返回DFS路径的文件信息
     * @param fileName
     * @param fullFilePath
     * @param session
     * @return
     * @throws Exception
     */
    public Map<String,Object> getPdfPreview(String fileName, String fullFilePath, Session session, int type) throws Exception {
        File file = new File(fullFilePath);
        if(!file.exists()) {
            LehandException.throwException("没有可以打印的记录！");
        }
        String name = file.getName();
        String filetype=name.substring(name.lastIndexOf(".")+1).toLowerCase();
        String dfsFilePath = dFSComponent.upload(fullFilePath);

        DlFile dlFile=new DlFile();
        //路径
        dlFile.setDir(dfsFilePath);
        //文件名称
        dlFile.setName(String.format("%s.pdf", fileName));
        //文件大小
        dlFile.setSize(file.length());
        //文件类型
        dlFile.setType(filetype);
        String remark= StringConstant.EMPTY;
        dlFile.setRemark(remark);
        dlFile.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        dlFile.setCompid(session.getCompid());
        dlFile.setUsertor(session.getUserid());
        dlFile.setRemark1(0);
        dlFile.setRemark2(0);
        dlFile.setRemark3(StringConstant.EMPTY);
        dlFile.setRemark4(StringConstant.EMPTY);
        dlFile.setRemark5(StringConstant.EMPTY);
        dlFile.setRemark6(StringConstant.EMPTY);
        Long fileId = generalSqlComponent.insert(SqlCode.addDlFile, dlFile);
        dlFile.setId(fileId);
        FileUtils.forceDeleteOnExit(file);

        //返回的数据集合
        Map<String,Object> result=new HashMap<String,Object>(8);
        //id
        result.put("id", dlFile.getId());
        String dir=dlFile.getDir();
        //上传到服务器后的地址
        result.put("dir", dir);
        //名称，包含后缀名
        result.put("name", dlFile.getName());
        //大小
        result.put("size", dlFile.getSize());
        //类型
        result.put("type", dlFile.getType());
        result.put("url",downIp+dir);
        //前端
        result.put("checked", false);
        //前端
        result.put("disabled", false);
        result.put("intostatus", type);
        return result;
    }
    /**
     * 根据组织编码获取书记基本信息
     * @param code
     * @return
     */
    public Map<String,Object> getSJInfo(String code){
        return generalSqlComponent.getDbComponent().getMap(SqlCode.getDZZBZCYXXBYCode,new Object[] {code,"党支部书记"});
    }
    /**
     * 根据身份证号码获取书记签名
     * @param idno
     * @return
     */
    public Map<String,Object> getSJSign(String idno){
        return generalSqlComponent.query(SqlCode.queryUrcUserByIdno,new Object[]{idno});
    }

    /**
     * 普通单元格
     * @param value  单元格内容
     * @param font 字体
     * @param height 单元格最小高度
     * @return
     */
    public static PdfPCell getPdfCellNoBorder(String value,Font font,float height){
        Paragraph p=new Paragraph(value,font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        PdfPCell cellN = new PdfPCell(p);
        cellN.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        cellN.setMinimumHeight(height);
        cellN.setBorderWidthBottom(2f);
        cellN.setBorderWidthTop(2f);
        cellN.setHorizontalAlignment(Element.ALIGN_CENTER); //水平居中
        cellN.setVerticalAlignment(Element.ALIGN_MIDDLE); //垂直居中
        return cellN;
    }

    /**
     * 第一联
     * @param data 数据
     * @param font 样式
     * @param height 高度
     * @return
     */
    public PdfPTable getFirstTable(Map<String,Object> data,Font[] font,float height) throws  Exception{
        float[] f={50f,300f,50f};
//        float[] floats = {500f};
        String a1="党\n员\n介\n绍\n信\n存\n根";
        String a3="第\n一\n联";
        PdfPTable table=new PdfPTable(f);
        table.addCell(getNormalPdfCell(a1, font[2],height));
        table.addCell(addPdfSecondColumn(data, font[1]));
        table.addCell(getNormalPdfCell(a3, font[1],height));
        return table;
    }

    /**
     * 第一联，第二列
     * @param data 数据
     * @param fontChinese11Normal  样式
     * @return
     */
    public PdfPTable addPdfSecondColumn(Map<String,Object> data, Font fontChinese11Normal) throws Exception{
        float[] width={300f};
        PdfPTable pdfPTable=new PdfPTable(width);

        Paragraph p=new Paragraph("第"+(String) data.get("number")+"号",fontChinese11Normal);

        PdfPCell cell1 = new PdfPCell(p);
        cell1.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell1.setMinimumHeight(10f);
        cell1.setBorder(0);
        pdfPTable.addCell(cell1);

        addPdfRow2Cell8(data,fontChinese11Normal,pdfPTable);

        Paragraph p2=new Paragraph(data.get("outTimeYear")+"年"+data.get("outTimeMonth")+"月"+data.get("outTimeDay")+"日",fontChinese11Normal);
        PdfPCell cell2 = new PdfPCell(p2);
        cell2.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell2.setMinimumHeight(10f);
        cell2.setBorder(0);
        pdfPTable.addCell(cell2);

        return pdfPTable;
    }
    /**
     * 第一联，第二列 ,第二行
     * @param data 数据
     * @param fontChinese11Normal  样式
     * @return
     */
    public PdfPTable addPdfRow2Cell8(Map<String, Object> data, Font fontChinese11Normal,PdfPTable pdfPTable) throws Exception{
        BaseFont bfCN = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font underlineFont = new Font(bfCN, 13, Font.UNDERLINE);//下划线
        pdfPTable.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
        pdfPTable.getDefaultCell().setBorderWidth(0);

        Paragraph p5=new Paragraph();
        p5.setFirstLineIndent(30f);//设置首行缩进
        p5.setAlignment(Paragraph.ALIGN_RIGHT);
        p5.setIndentationLeft(12); //设置左缩进
        p5.setIndentationRight(12); //设置右缩进
        p5.setLeading(20f); //行间距

        getChunkCell(p5,(String) data.get("userName"),underlineFont);
        getChunkCell(p5,"同志系中共(",fontChinese11Normal);
        getChunkCell(p5,(String) data.get("dylbName")==""?"____":(String)data.get("dylbName"),fontChinese11Normal);
        getChunkCell(p5,")党员，组织关系由",fontChinese11Normal);
        getChunkCell(p5,(String) data.get("outOrgName"),underlineFont);
        getChunkCell(p5,"转到",fontChinese11Normal);
        getChunkCell(p5,(String) data.get("intoOrgName")==""?"________":(String) data.get("intoOrgName"),data.get("intoOrgName")==""?fontChinese11Normal:underlineFont);
        getChunkCell(p5,"。",fontChinese11Normal);

        pdfPTable.addCell(p5);
        return  pdfPTable;
    }


    /**
     * 第二联
     * @param data 数据
     * @param font 样式
     * @param height 高度
     * @return
     */
    public PdfPTable getFirstTable2(Map<String,Object> data,Font[] font,float height) throws  Exception{
        float[] f={400f,50f};
        String a2="第\n二\n联";
        PdfPTable table=new PdfPTable(f);
        table.addCell(addPdf2OneColumn(data, font[1]));
        table.addCell(getNormalPdfCell(a2, font[1],height));
        return table;
    }

    /**
     * 第二联，第一列
     * @param data 数据
     * @param fontChinese11Normal  样式
     * @return
     */
    public PdfPTable addPdf2OneColumn(Map<String,Object> data, Font fontChinese11Normal) throws Exception{
        BaseFont bfCN = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font underlineFont = new Font(bfCN, 13, Font.UNDERLINE);//下划线
        float[] width={300f};
        PdfPTable pdfPTable=new PdfPTable(width);
        pdfPTable.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
        pdfPTable.getDefaultCell().setBorderWidth(0);

        Paragraph p=new Paragraph("第"+(String) data.get("number")+"号",fontChinese11Normal);
        PdfPCell cell1 = new PdfPCell(p);
        cell1.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell1.setMinimumHeight(10f);
        cell1.setBorder(0);
        pdfPTable.addCell(cell1);

        Paragraph p3=new Paragraph((String) data.get("intoOrgName")==""?"________:":(String) data.get("intoOrgName")+":",(String) data.get("intoOrgName")==""?fontChinese11Normal:underlineFont);
        PdfPCell cell3 = new PdfPCell(p3);
        cell3.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell3.setMinimumHeight(10f);
        cell3.setBorder(0);
        pdfPTable.addCell(cell3);

        //内容
        Paragraph p4=new Paragraph();
        p4.setFirstLineIndent(30f);//设置首行缩进
        p4.setAlignment(Paragraph.ALIGN_RIGHT);
        p4.setIndentationLeft(12); //设置左缩进
        p4.setIndentationRight(12); //设置右缩进
        p4.setLeading(20f); //行间距

        getChunkCell(p4,(String) data.get("userName"),underlineFont);
        getChunkCell(p4,"同志（"+(String) data.get("sex")+"）,",fontChinese11Normal);
        getChunkCell(p4,(String) data.get("age")==""?"___":(String) data.get("age"),(String) data.get("age")==""?fontChinese11Normal:underlineFont);
        getChunkCell(p4,"岁,",fontChinese11Normal);
        getChunkCell(p4,(String) data.get("mz")==""?"___":(String) data.get("mz"),(String) data.get("mz")==""?fontChinese11Normal:underlineFont);
        getChunkCell(p4,",系中共（",fontChinese11Normal);
        getChunkCell(p4,data.get("dylbName")==""?"____":data.get("dylbName")+"）党员,身份证号码",fontChinese11Normal);
        getChunkCell(p4, (String) data.get("idCode"),underlineFont);
        getChunkCell(p4, ",由",fontChinese11Normal);
        getChunkCell(p4, (String) data.get("outOrgName"),underlineFont);
        getChunkCell(p4, "去",fontChinese11Normal);
        getChunkCell(p4, (String) data.get("intoOrgName")==""?"________":(String) data.get("intoOrgName"),data.get("intoOrgName")==""?fontChinese11Normal:underlineFont);
        getChunkCell(p4, ",请转接组织关系.该同志党费已经交到",fontChinese11Normal);
        Integer actualYear = (Integer)data.get("actualYear");
        Integer actualMonth = (Integer) data.get("actualMonth");
        getChunkCell(p4, actualYear==0?"________":actualYear+"",actualYear==0?fontChinese11Normal:underlineFont);
        getChunkCell(p4, "年",fontChinese11Normal);
        getChunkCell(p4, actualMonth==0?"________":actualMonth+"",actualMonth==0?fontChinese11Normal:underlineFont);
        getChunkCell(p4, "月.",fontChinese11Normal);
        pdfPTable.addCell(p4);
        //有效期
        Paragraph p5=new Paragraph("（有效期 30 天）",fontChinese11Normal);
        PdfPCell cell5 = new PdfPCell(p5);
        cell5.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell5.setMinimumHeight(10f);
        cell5.setBorder(0);
        pdfPTable.addCell(cell5);
        //书记签名
        Paragraph p6=new Paragraph(1);
        PdfPCell cell6 = new PdfPCell(p6);
        if(!StringUtils.isEmpty(data.get("outSjSign"))){
            Image outSjSign = Image.getInstance(new URL(String.format("file:////%s", createBase64Image((String) data.get("outSjSign")))));
            getImageLogo(outSjSign);
            cell6.addElement(outSjSign);
        }else {
            getChunkCell(p6,(String) data.get("outSjName"),fontChinese11Normal);
        }
        cell6.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell6.setMinimumHeight(10f);
        cell6.setBorder(0);
        pdfPTable.addCell(cell6);

        //书记审核时间
        String outExamineTimeYear = (String) data.get("outExamineTimeYear");
        String outExamineTimeMonth = (String) data.get("outExamineTimeMonth");
        String outExamineTimeDay = (String) data.get("outExamineTimeDay");

        Paragraph p7=new Paragraph();
        PdfPCell cell7 = new PdfPCell(p7);
        getChunkCell(p7,outExamineTimeYear==""?"___年":outExamineTimeYear+"年",fontChinese11Normal);
        getChunkCell(p7,outExamineTimeMonth==""?"___月":outExamineTimeMonth+"月",fontChinese11Normal);
        getChunkCell(p7,outExamineTimeDay==""?"___日":outExamineTimeDay+"日",fontChinese11Normal);
        cell7.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell7.setMinimumHeight(10f);
        cell7.setBorder(0);
        pdfPTable.addCell(cell7);
        //党员联系电话
        Paragraph p8=new Paragraph("党员联系电话:"+data.get("phone"),fontChinese11Normal);
        PdfPCell cell8 = new PdfPCell(p8);
        cell8.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell8.setMinimumHeight(10f);
        cell8.setBorder(0);
        pdfPTable.addCell(cell8);
        //党员原所在党支部书记联系电话
        Paragraph p9=new Paragraph("党员原所在党支部书记联系电话:"+data.get("outSjPhone"),fontChinese11Normal);
        PdfPCell cell9 = new PdfPCell(p9);
        cell9.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell9.setMinimumHeight(10f);
        cell9.setBorder(0);
        pdfPTable.addCell(cell9);

        return pdfPTable;
    }

    /**
     * 第三联
     * @param data 数据
     * @param font 样式
     * @param height 高度
     * @return
     */
    public PdfPTable getFirstTable3(Map<String,Object> data,Font[] font,float height) throws  Exception{
        float[] f={400f,50f};
        String a2="第\n三\n联";
        PdfPTable table=new PdfPTable(f);
        table.addCell(addPdf3OneColumn(data, font[1]));
        table.addCell(getNormalPdfCell(a2, font[1],height));
        return table;
    }
    /**
     * 第三联，第一列
     * @param data 数据
     * @param fontChinese11Normal  样式
     * @return
     */
    public PdfPTable addPdf3OneColumn(Map<String,Object> data, Font fontChinese11Normal) throws Exception{
        BaseFont bfCN = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font underlineFont = new Font(bfCN, 13, Font.UNDERLINE);//下划线
        float[] width={300f};
        PdfPTable pdfPTable=new PdfPTable(width);
        pdfPTable.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
        pdfPTable.getDefaultCell().setBorderWidth(0);

        Paragraph p=new Paragraph("第"+(String) data.get("number")+"号",fontChinese11Normal);
        PdfPCell cell1 = new PdfPCell(p);
        cell1.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell1.setMinimumHeight(10f);
        cell1.setBorder(0);
        pdfPTable.addCell(cell1);

        Paragraph p3=new Paragraph((String) data.get("outOrgName")+":",underlineFont);
        PdfPCell cell3 = new PdfPCell(p3);
        cell3.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell3.setMinimumHeight(10f);
        cell3.setBorder(0);
        pdfPTable.addCell(cell3);

        //内容
        Paragraph p4=new Paragraph();
        p4.setFirstLineIndent(30);//设置首行缩进
        p4.setAlignment(Paragraph.ALIGN_RIGHT);
        p4.setIndentationLeft(12); //设置左缩进
        p4.setIndentationRight(12); //设置右缩进
        p4.setLeading(20f); //行间距

        getChunkCell(p4,(String) data.get("userName"),underlineFont);
        getChunkCell(p4,"同志的党员组织关系已转达我处,特此回复。",fontChinese11Normal);
        pdfPTable.addCell(p4);
        //书记签名
        Paragraph p6=new Paragraph(1);
        PdfPCell cell6 = new PdfPCell(p6);
        if(!StringUtils.isEmpty(data.get("intoSjSign"))){
            Image intoSjSign = Image.getInstance(new URL(String.format("file:////%s", createBase64Image((String) data.get("intoSjSign")))));
            getImageLogo(intoSjSign);
            cell6.addElement(intoSjSign);
        }else {
            getChunkCell(p6,(String) data.get("intoSjName"),fontChinese11Normal);
        }
        cell6.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell6.setMinimumHeight(10f);
        cell6.setBorder(0);
        pdfPTable.addCell(cell6);

        //书记审核时间
        String infoExamineTimeYear = (String) data.get("infoExamineTimeYear");
        String infoExamineTimeMonth = (String) data.get("infoExamineTimeMonth");
        String infoExamineTimeDay = (String) data.get("infoExamineTimeDay");

        Paragraph p7=new Paragraph();
        PdfPCell cell7 = new PdfPCell(p7);
        getChunkCell(p7,infoExamineTimeYear==""?"___年":infoExamineTimeYear+"年",fontChinese11Normal);
        getChunkCell(p7,infoExamineTimeMonth==""?"___月":infoExamineTimeMonth+"月",fontChinese11Normal);
        getChunkCell(p7,infoExamineTimeDay==""?"___日":infoExamineTimeDay+"日",fontChinese11Normal);
        cell7.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell7.setMinimumHeight(10f);
        cell7.setBorder(0);
        pdfPTable.addCell(cell7);
        //党员联系电话
        Paragraph p8=new Paragraph();
        p8.setFirstLineIndent(30f);//设置首行缩进
        p8.setAlignment(Paragraph.ALIGN_CENTER);
        p8.setIndentationLeft(12); //设置左缩进
        p8.setIndentationRight(12); //设置右缩进
        p8.setLeading(20f); //行间距

        getChunkCell(p8,"经办人：____________",fontChinese11Normal);
        getChunkCell(p8,"联系电话：____________",fontChinese11Normal);
        pdfPTable.addCell(p8);

        return pdfPTable;
    }
    /**
     * 生成base64码
     * @param jsonData
     * @return
     */
    private String createBase64Image(String jsonData) {
        String uuid = "sign_"+UUID.randomUUID().toString();
        File file = new File(String.format("%s%s%s.png", uploadTempPath, File.separator, uuid));
        byte[] b;
        try {
            b = new BASE64Decoder().decodeBuffer(jsonData.replace("data:image/png;base64,", ""));
            for (int i = 0; i < b.length; i++) {
                if(b[i]<0) {
                    b[i]+=256;
                }
            }
            OutputStream out = new FileOutputStream(file.getAbsoluteFile());
            out.write(b);
            out.flush();
            out.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return file.getAbsolutePath();
        }
    }

    /**
     * 图片样式设置
     * 设置图片居中，超过固定宽度后，对图片进行比例缩小
     * @param image
     */
    public void getImageLogo(Image image) {
        float realWidth = image.getScaledWidth();
        float realHeight = image.getScaledHeight();
        image.setAlignment(Image.ALIGN_RIGHT);//设置靠左
        //宽度大于A4显示宽度，高度
        if(realWidth > PAGE_A4_IMAGE_WIDTH || realHeight > PAGE_A4_IMAGE_HEIGHT) {
            //宽度超过预期显示宽度
            if(realWidth > PAGE_A4_IMAGE_WIDTH) {
                image.scaleAbsoluteWidth(PAGE_SIGN_IMAGE_WIDTH);
                image.scaleAbsoluteHeight((realHeight * PAGE_SIGN_IMAGE_WIDTH) / realWidth);
                return; //这里返回，是宽度进行缩放后，不再进行高度的缩放
            }
            //高度超过预期显示高度
            if(realHeight > PAGE_A4_IMAGE_HEIGHT) {
                image.scaleAbsoluteHeight(PAGE_SIGN_IMAGE_HEIGHT);
                image.scaleAbsoluteWidth((realWidth * PAGE_SIGN_IMAGE_HEIGHT) / realHeight);
            }
        }
    }
    private void getChunkCell(Paragraph paragraph, String value, Font font) {
        Chunk c = new Chunk(value, font);
        c.setLineHeight(30);
        c.setWordSpacing(10);
        paragraph.add(c);
    }

    /**
     * 普通单元格
     * @param value  单元格内容
     * @param font 字体
     * @param height 单元格最小高度
     * @return
     */
    public static PdfPCell getNormalPdfCellR(String value,Font font,float height){
        Paragraph p=new Paragraph(value,font);
        PdfPCell cellN = new PdfPCell(p);
        cellN.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        return cellN;
    }
    /**
     * 普通单元格
     * @param value  单元格内容
     * @param font 字体
     * @param height 单元格最小高度
     * @return
     */
    public static PdfPCell getNormalPdfCell(String value,Font font,float height){
        Paragraph p=new Paragraph(value,font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        PdfPCell cellN = new PdfPCell(p);
        cellN.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        cellN.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        cellN.setMinimumHeight(height);
        return cellN;
    }
    /**
     * 获取标题
     * @param value  标题内容
     * @param font  字体样式
     * @return
     */
    public static PdfPTable getPdfTitle(String value, Font font){
        float[] width={200f};
        PdfPTable table=new PdfPTable(width);
        Paragraph content=new Paragraph(value,font);
        PdfPCell cellN = new PdfPCell(content);
        cellN.setPadding(10f);
        cellN.setBorder(0);
        cellN.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        cellN.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        cellN.setMinimumHeight(30f);
        table.addCell(cellN);
        return table;
    }
}