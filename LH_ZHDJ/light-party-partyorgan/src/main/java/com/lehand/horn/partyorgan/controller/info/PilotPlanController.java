package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrand;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandDynamic;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzPartyBrandPraise;
import com.lehand.horn.partyorgan.service.dzz.PilotPlanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 领航计划控制层
 * @author lx
 *
 */
@Api(value = "领航计划", tags = { "领航计划" })
@RestController
@RequestMapping("/lhdj/pilotPlan")
public class PilotPlanController extends BaseController {
    private static final Logger logger = LogManager.getLogger(PilotPlanController.class);
    @Resource
    private PilotPlanService pilotPlanService;
    /**
     * @Description 添加党建品牌
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "添加党建品牌", notes = "添加党建品牌", httpMethod = "POST")
    @RequestMapping("/addTDzzPartyBrand")
    public Message addTDzzPartyBrand(@RequestBody TDzzPartyBrand bean){
        Message message = new Message();
        try {
            pilotPlanService.addTDzzPartyBrand(bean,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("添加失败！"+e.getMessage());
        }
        return message;
    };

    /**
     * @Description 修改党建品牌
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改党建品牌", notes = "修改党建品牌", httpMethod = "POST")
    @RequestMapping("/updateTDzzPartyBrand")
    public Message updateTDzzPartyBrand(@RequestBody  TDzzPartyBrand bean){
        Message message = new Message();
        try {
            pilotPlanService.updateTDzzPartyBrand(bean,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("编辑失败！"+e.getMessage());
        }
        return message;
    }

    /**
     * @Description (提交/上报/通过/驳回)品牌
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "(提交/上报/通过/驳回)品牌", notes = "(提交/上报/通过/驳回)品牌", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "品牌组织id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "orgcode", value = "当前组织编码", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "remake", value = "驳回原因", required = false, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "type", value = "(1：提交/2：上报/3：通过:4：驳回)品牌", required = true, paramType = "query", dataType = "Integer", defaultValue = "")
    })
    @RequestMapping("/reportAuditPartyBrand")
    public Message reportAuditPartyBrand(String id,String orgcode,String remake,Integer type){
        Message message = new Message();
        try {
            pilotPlanService.reportAuditPartyBrand(id,orgcode,remake,type,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("编辑失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 分页查询党建品牌
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询党建品牌", notes = "分页查询党建品牌", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "关键字搜索（品牌名称）", required = false, dataType = "String"),
            @ApiImplicitParam(name = "orgcode", value = "条件筛选组织code", required = false, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "品牌类型(1:培育品牌、2:示范品牌、3:推优上级品牌，全部不传参)", required = false, dataType = "String"),
            @ApiImplicitParam(name = "level", value = "品牌级别（0：无，1：事业部级品牌，2：公司级品牌，全部不传参）", required = false, dataType = "String"),
            @ApiImplicitParam(name = "status", value = "状态(0:草稿，1:上级待审，2:上级通过，3:上级驳回，4:二级党委待审，5:二级党委通过，6:二级党委驳回，7:一级党委待审，8:一级党委通过，9:一级党委驳回，全部不传参)", required = false, dataType = "String"),
            @ApiImplicitParam(name = "myOrLower", value = "我的或者是下级（1：我的，2：下级，默认不传参）", required = false, dataType = "String")
    })
    @RequestMapping("/pageTDzzPartyBrand")
    public Message pageTDzzPartyBrand(Pager pager,String type,String orgcode,String level,String status,String name,String myOrLower){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.pageTDzzPartyBrand(pager,type,orgcode,level,status,name,myOrLower,getSession()));
            message.success();
        }catch (Exception e){
            message.setMessage("分页查询失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 删除党建品牌
     * @Author lx
     * @Date 2021/3/29
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "删除党建品牌", notes = "删除党建品牌", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "品牌组织id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delTDzzPartyBrand")
    public Message delTDzzPartyBrand(String id){
        Message message = new Message();
        try {
            pilotPlanService.delTDzzPartyBrand(id,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("编辑失败！"+e.getMessage());
        }
        return message;
    }

    /**
     * @Description 查看党建品牌
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看党建品牌", notes = "查看党建品牌", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "品牌组织编码", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "品牌组织id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getTDzzPartyBrand")
    public Message getTDzzPartyBrand(String orgcode,String id){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.getTDzzPartyBrand(id,orgcode,getSession()));
            message.success();
        }catch (Exception e){
            message.setMessage("查看失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 添加品牌动态
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "添加品牌动态", notes = "添加品牌动态", httpMethod = "POST")
    @RequestMapping("/addBrandDynamic")
    public Message addBrandDynamic(@RequestBody TDzzPartyBrandDynamic bean){
        Message message = new Message();
        try {
            pilotPlanService.addBrandDynamic(bean,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("添加失败！"+e.getMessage());
        }
        return message;
    };

    /**
     * @Description 修改品牌动态
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改品牌动态", notes = "修改品牌动态", httpMethod = "POST")
    @RequestMapping("/updateBrandDynamic")
    public Message updateBrandDynamic(@RequestBody  TDzzPartyBrandDynamic bean){
        Message message = new Message();
        try {
            pilotPlanService.updateBrandDynamic(bean,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("编辑失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description (发布,撤销发布)品牌动态
     * @Author lx
     * @Date 2021/3/31
     * @param
     * @return
     **/
    @OpenApi(optflag=2)
    @ApiOperation(value = "(发布,撤销发布)品牌动态", notes = "(发布,撤销发布)品牌动态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "单个品牌动态id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "0：未发布(撤销)，1：已发布(发布)", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/updateDynamicStatus")
    public Message updateDynamicStatus(String id,String status){
        Message message = new Message();
        try {
            pilotPlanService.updateDynamicStatus(id,status,getSession());
            message.success();
        }catch (Exception e){
            message.setMessage("编辑失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 查看单个品牌动态
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看单个品牌动态", notes = "查看单个品牌动态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "单个品牌动态id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getBrandDynamic")
    public Message getBrandDynamic(String id){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.getBrandDynamic(id));
            message.success();
        }catch (Exception e){
            message.setMessage("查看失败！"+e.getMessage());
        }
        return message;
    }

    /**
     * @Description 删除单个品牌动态
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "删除单个品牌动态", notes = "删除单个品牌动态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "单个品牌动态id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/delBrandDynamic")
    public Message delBrandDynamic(String id){
        Message message = new Message();
        try {
            pilotPlanService.delBrandDynamic(id);
            message.success();
        }catch (Exception e){
            message.setMessage("删除失败！"+e.getMessage());
        }
        return message;
    }

    /**
     * @Description 分页查询品牌动态
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询品牌动态", notes = "分页查询品牌动态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织编码code", required = true, dataType = "String"),
            @ApiImplicitParam(name = "name", value = "(品牌名称)条件筛选", required = false, dataType = "String"),
            @ApiImplicitParam(name = "status", value = "状态（0：未发布，1：已发布,空或null：查所有）", required = false, dataType = "String"),
            @ApiImplicitParam(name = "title", value = "筛选条件标题", required = false, dataType = "String"),
            @ApiImplicitParam(name = "brandid", value = "品牌id", required = false, dataType = "String")
    })
    @RequestMapping("/pageBrandDynamic")
    public Message pageBrandDynamic(Pager pager,String orgcode,String name,String status,String title,String brandid){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.pageBrandDynamic(pager,orgcode,status,title,name,brandid));
            message.success();
        }catch (Exception e){
            message.setMessage("分页查询失败！"+e.getMessage());
        }
        return message;
    }

    /**
     * @Description 查看单个品牌点赞数量
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看单个品牌点赞数量", notes = "查看单个品牌点赞数量", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "品牌组织编码", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "id", value = "品牌id", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/getBrandPraise")
    public Message getBrandPraise(String orgcode,String id){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.getBrandPraise(orgcode,id));
            message.success();
        }catch (Exception e){
            message.setMessage("查看失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 添加(撤销)品牌点赞
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "添加(撤销)品牌点赞", notes = "添加(撤销)品牌点赞", httpMethod = "POST")
    @RequestMapping("/modifyBrandPraise")
    public Message modifyBrandPraise(@RequestBody TDzzPartyBrandPraise bean){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.modifyBrandPraise(bean,getSession()));
            message.success();
        }catch (Exception e){
            message.setMessage("添加失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 获取当前组织状态
     * @Author lx
     * @Date 2021/3/9
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "获取当前组织状态", notes = "获取当前组织状态", httpMethod = "POST")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "orgcode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    )
    @RequestMapping("/getStatusByOrgCode")
    public Message getStatusByOrgCode(String orgcode){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.getStatusByOrgCode(orgcode,1));
            message.success();
        }catch (LehandException e){
            message.setMessage("获取失败！"+e.getMessage());
        }catch (Exception e){
            message.setMessage("获取失败！"+e.getMessage());
        }
        return message;
    }
    /**
     * @Description 获取当前组织级别
     * @Author lx
     * @Date 2021/4/1
     * @param
     * @return
     **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "获取当前组织级别", notes = "获取当前组织级别", httpMethod = "POST")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "orgcode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    )
    @RequestMapping("/getLevelByOrgCode")
    public Message getLevelByOrgCode(String orgcode){
        Message message = new Message();
        try {
            message.setData(pilotPlanService.getStatusByOrgCode(orgcode,2));
            message.success();
        }catch (LehandException e){
            message.setMessage("获取失败！"+e.getMessage());
        }catch (Exception e){
            message.setMessage("获取失败！"+e.getMessage());
        }
        return message;
    }
}
