package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
import com.lehand.horn.partyorgan.pojo.partygroup.PartyGroupMember;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @program: huaikuang-dj
 * @description: TDzzInfoSimpleReq
 * @author: zwd
 * @create: 2020-12-01 08:47
 */
public class TDzzInfoSimpleReq extends TDzzInfoSimple {

    @ApiModelProperty(value="邮编", name="postcode")
    private String postcode;

    @ApiModelProperty(value="传真", name="fax")
    private String fax;

    @ApiModelProperty(value="地址", name="address")
    private String address;

    @ApiModelProperty(value="联系人", name="lxrname")
    private String lxrname;

    @ApiModelProperty(value="联系电话", name="lxrphone")
    private String lxrphone;

    @ApiModelProperty(value="首次录入操作人", name="creator")
    private String creator;

    @ApiModelProperty(value="办理机构(办理人所在的organid)", name="operorganid")
    private String operorganid;

    @ApiModelProperty(value="更新操作人", name="updator")
    private String updator;

    @ApiModelProperty(value="首次录入时间", name="createtime")
    private String createtime;

    @ApiModelProperty(value="办理人组织名称", name="operatorname")
    private String operatorname;

    @ApiModelProperty(value="本届班子当选日期", name="electdate")
    private String electdate;

    @ApiModelProperty(value="本届班子任期届满日期", name="expirationdate")
    private String expirationdate;

    @ApiModelProperty(value="固定电话", name="gddh")
    private String gddh;

    @ApiModelProperty(value="党组织信息完整度；以分母；分子表示", name="sumwzd")
    private String sumwzd;

    @ApiModelProperty(value="同步状态  A 新增 D 删除 E 修改 Q 同步", name="syncstatus")
    private String syncstatus;

    @ApiModelProperty(value="同步时间", name="synctime")
    private String synctime;

    @ApiModelProperty(value="党组织全称", name="dxzms")
    private String dxzms;

    @ApiModelProperty(value="党组织书记公民身份号码", name="dzzsjzjhm")
    private String dzzsjzjhm;

    @ApiModelProperty(value="党组织建立日期", name="dzzjlrq")
    private String dzzjlrq;

    @ApiModelProperty(value="本年度开展党员民主评议日期", name="kzpyrq")
    private String kzpyrq;

    @ApiModelProperty(value="该党组织本年度是否已开展党员评议的基层党组织领导班子。1-是；0-否。", name="kzdqpybs")
    private String kzdqpybs;

    @ApiModelProperty(value="参加党员民主评议的党员人数", name="cjpydys")
    private String cjpydys;

    @ApiModelProperty(value="本年度开展党员旁听党委会议的基层党委数量。", name="kzpthydws")
    private String kzpthydws;

    @ApiModelProperty(value="本年度参加旁听党委会议的党员人数", name="cjptdys")
    private String cjptdys;

    @ApiModelProperty(value="本届班子产生方式", name="bjbzcsfs")
    private String bjbzcsfs;

    @ApiModelProperty(value="信息完整度标识", name="sfxxwz")
    private String sfxxwz;

    @ApiModelProperty(value="urc_user表userid", name="remarks3")
    private String remarks3;

    @ApiModelProperty(value="是否是活动阵地；0否，1是",name="activitypositions")
    private String activitypositions;


    @ApiModelProperty(value="党组织所在省份", name="dzzszsf")
    private String dzzszsf;

    @ApiModelProperty(value="党组织所在市区", name="dzzszsq")
    private String dzzszsq;

    public String getDzzszsf() {
        return dzzszsf;
    }

    public void setDzzszsf(String dzzszsf) {
        this.dzzszsf = dzzszsf;
    }

    public String getDzzszsq() {
        return dzzszsq;
    }

    public void setDzzszsq(String dzzszsq) {
        this.dzzszsq = dzzszsq;
    }

    public String getActivitypositions() {
        return activitypositions;
    }

    public void setActivitypositions(String activitypositions) {
        this.activitypositions = activitypositions;
    }



    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLxrname() {
        return lxrname;
    }

    public void setLxrname(String lxrname) {
        this.lxrname = lxrname;
    }

    public String getLxrphone() {
        return lxrphone;
    }

    public void setLxrphone(String lxrphone) {
        this.lxrphone = lxrphone;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getOperorganid() {
        return operorganid;
    }

    public void setOperorganid(String operorganid) {
        this.operorganid = operorganid;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getOperatorname() {
        return operatorname;
    }

    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname;
    }

    public String getElectdate() {
        return electdate;
    }

    public void setElectdate(String electdate) {
        this.electdate = electdate;
    }

    public String getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        this.expirationdate = expirationdate;
    }

    public String getGddh() {
        return gddh;
    }

    public void setGddh(String gddh) {
        this.gddh = gddh;
    }

    public String getSumwzd() {
        return sumwzd;
    }

    public void setSumwzd(String sumwzd) {
        this.sumwzd = sumwzd;
    }

    public String getSyncstatus() {
        return syncstatus;
    }

    public void setSyncstatus(String syncstatus) {
        this.syncstatus = syncstatus;
    }

    public String getSynctime() {
        return synctime;
    }

    public void setSynctime(String synctime) {
        this.synctime = synctime;
    }

    public String getDxzms() {
        return dxzms;
    }

    public void setDxzms(String dxzms) {
        this.dxzms = dxzms;
    }

    public String getDzzsjzjhm() {
        return dzzsjzjhm;
    }

    public void setDzzsjzjhm(String dzzsjzjhm) {
        this.dzzsjzjhm = dzzsjzjhm;
    }

    public String getDzzjlrq() {
        return dzzjlrq;
    }

    public void setDzzjlrq(String dzzjlrq) {
        this.dzzjlrq = dzzjlrq;
    }

    public String getKzpyrq() {
        return kzpyrq;
    }

    public void setKzpyrq(String kzpyrq) {
        this.kzpyrq = kzpyrq;
    }

    public String getKzdqpybs() {
        return kzdqpybs;
    }

    public void setKzdqpybs(String kzdqpybs) {
        this.kzdqpybs = kzdqpybs;
    }

    public String getCjpydys() {
        return cjpydys;
    }

    public void setCjpydys(String cjpydys) {
        this.cjpydys = cjpydys;
    }

    public String getKzpthydws() {
        return kzpthydws;
    }

    public void setKzpthydws(String kzpthydws) {
        this.kzpthydws = kzpthydws;
    }

    public String getCjptdys() {
        return cjptdys;
    }

    public void setCjptdys(String cjptdys) {
        this.cjptdys = cjptdys;
    }

    public String getBjbzcsfs() {
        return bjbzcsfs;
    }

    public void setBjbzcsfs(String bjbzcsfs) {
        this.bjbzcsfs = bjbzcsfs;
    }

    @Override
    public String getSfxxwz() {
        return sfxxwz;
    }

    @Override
    public void setSfxxwz(String sfxxwz) {
        this.sfxxwz = sfxxwz;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }
}