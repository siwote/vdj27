package com.lehand.horn.partyorgan.controller.dzz;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.dto.TDzzInfoSaveReq;
import com.lehand.horn.partyorgan.dto.TDzzInfoSimpleReq;
import com.lehand.horn.partyorgan.service.info.HornOrganizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.horn.partyorgan.controller.HornBaseController;



/**
 * 组织机构控制层
 * @author taogang
 * @version 1.0
 * @date 2019年1月21日, 下午2:30:51
 */
@Api(value = "党组织-前端改造后该当前类作废", tags = { "党组织-前端改造后该当前类作废" })
@RestController
@RequestMapping("/lhdj/dzz")
public class TdzzInfoSimpleController extends HornBaseController{

	private static final Logger logger = LogManager.getLogger(TdzzInfoSimpleController.class);

	@Resource
	private HornOrganizationService tdzzInfoSimpleService;

	/**
	 * 机构树
	 * @param organid
	 * @param likeStr
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "机构树", notes = "机构树", httpMethod = "POST")
	@RequestMapping("/tree")
	public Message query(String organid,String likeStr){
		Message message = new Message();
		try {
			List<Map<String, Object>> list = tdzzInfoSimpleService.getListByOrganId(getSession(),organid,likeStr);
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("组织机构树查询出错");
		}
		return message;
	}

//	/**
//	 * 列表
//	 * @param organid
//	 * @param dwlb
//	 * @param likeStr
//	 * @return
//	 * @author taogang
//	 */
//	@RequestMapping("/list")
//	public Message list(String organid,String dwlb,String likeStr,Pager pager) {
//		Message message = new Message();
//		try {
//			if(StringUtils.isEmpty(organid)) {
//				message.setMessage("参数错误，机构id不能为空");
//				return message;
//			}
//			//分页查询
//			int count = tdzzInfoSimpleService.countDzz(organid, dwlb, likeStr);
//			List<Map<String, Object>> list = tdzzInfoSimpleService.getList(pager,organid, dwlb, likeStr);
//			pager.setRows(list);
//			pager.setTotalRows(count);
//			int i = count % pager.getPageSize();
//		    int v = count / pager.getPageSize();
//		    pager.setTotalPages(i>0 ? v+1 : v);
//		    message.setData(pager);
//		    message.success();
//		} catch (Exception e) {
//			logger.error(e);
//			message.setMessage("组织机构分页列表查询出错");
//		}
//		return message;
//	}

	/**
	 * 党组织基本信息
	 * @param organid
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "党组织基本信息", notes = "党组织基本信息", httpMethod = "POST")
	@RequestMapping("/base")
	public Message base(String organid) {
		Message message = new Message();
		try {
			Map<String, Object> map = tdzzInfoSimpleService.getBase(organid);
			message.success().setData(map);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("基本信息查询出错");
		}
		return message;
	}

	/**
	 * 单位信息
	 * @param organid
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "单位信息", notes = "单位信息", httpMethod = "POST")
	@RequestMapping("/dwInfo")
	public Message dwInfo(String organid) {
		Message message = new Message();
		try {
			Map<String, Object> map = tdzzInfoSimpleService.getDwInfo(organid);
			message.success().setData(map);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("单位信息查询出错");
		}
		return message;
	}

	/**
	 * 换届信息
	 * @param organid
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "换届信息", notes = "换届信息", httpMethod = "POST")
	@RequestMapping("/hjxx")
	public Message hjxx(String organid) {
		Message message = new Message();
		try {
			List<Map<String, Object>> list = tdzzInfoSimpleService.getHjxxList(organid);
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("换届信息查询出错");
		}
		return message;
	}

	/**
	 * 班子成员信息
	 * @param organid
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "班子成员信息", notes = "班子成员信息", httpMethod = "POST")
	@RequestMapping("/bzcy")
	public Message bzcy(String organid) {
		Message message = new Message();
		try {
			List<Map<String, Object>> list = tdzzInfoSimpleService.getBzcyxxList(organid, MagicConstant.STR.EMPTY_STR,"");
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("班子成员查询出错");
		}
		return message;
	}

	/**
	 * 奖惩信息
	 * @param organid
	 * @return
	 * @author taogang
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "奖惩信息", notes = "奖惩信息", httpMethod = "POST")
	@RequestMapping("/jcxx")
	public Message jcxx(String organid) {
		Message message = new Message();
		try {
			List<Map<String, Object>> list = tdzzInfoSimpleService.getJcxx(organid);
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("奖惩信息查询出错");
		}
		return message;
	}


	/**
	* @Description 新增党组织
	* @Author zwd
	* @Date 2020/12/1 8:55
	* @param
	* @return
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "新增党组织", notes = "新增党组织", httpMethod = "POST")
	@RequestMapping("/adddzz")
	public Message addTDzzinfo(@RequestBody TDzzInfoSaveReq bean){
		Message message = new Message();
		message.setData(tdzzInfoSimpleService.addTDzzinfo(bean,getSession()));
		message.success();
		return message;
	}

	/**
	* @Description 下拉选择
	* @Author zwd
	* @Date 2020/12/1 11:30
	* @param
	* @return
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "下拉选择-作废", notes = "下拉选择-作废", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "code", value = "党组织属地关系：d_dzz_lsgx，党组织类别：d_dzz_zzlb,党组织所在行政区划：d_dzz_xzqh，党组织所在单位情况：d_dzz_dwqk，" +
					"单位性质类别：d_dw_xzlb，单位隶属关系：d_dw_lsgx,查询企业控制(控股)情况:d_dw_jjlx,企业规模：d_dw_qygm",
					required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "parent_code", value = "父级code，查党组织类别时传6000000034,查询企业控制(控股)情况传-1，查询企业规模时传-1", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/dzzlsgx")
	public Message getDzzlsgx(String code,String parent_code){
		Message message = new Message();
		message.setData(tdzzInfoSimpleService.getDzzlsgx(code,parent_code,getSession()));
		message.success();
		return message;
	}

	/**
	* @Description  修改党组织信息
	* @Author zwd
	* @Date 2020/12/3 17:56
	* @param 
	* @return 
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "修改党组织", notes = "修改党组织", httpMethod = "POST")
	@RequestMapping("/updatedzz")
	public Message updateTDzzinfo(@RequestBody TDzzInfoSaveReq bean){
		Message message = new Message();
		message.setData(tdzzInfoSimpleService.updateTDzzinfo(bean,getSession()));
		message.success();
		return message;
	}

	/**
	* @Description 删除党组织
	* @Author zwd
	* @Date 2020/12/4 11:29
	* @param 
	* @return 
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "删除党组织", notes = "新增党组织", httpMethod = "POST")
	@RequestMapping("/deldzz")
	public Message deleteTDzzInfo(String organid,String organcode){
		Message message = new Message();
		message.setData(tdzzInfoSimpleService.deleteTDzzInfo(organid,organcode,getSession()));
		message.success();
		return message;
	}

	/**
	* @Description /检验organcode是否存在
	* @Author zwd
	* @Date 2020/12/8 9:32
	* @param 
	* @return 
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "检验organcode是否存在", notes = "检验organcode是否存在", httpMethod = "POST")
	@RequestMapping("/checkcode")
	public Message checkCode(String organcode){
		Message message = new Message();
		message.setData(tdzzInfoSimpleService.checkCode(organcode));
		message.success();
		return message;
	}
}
