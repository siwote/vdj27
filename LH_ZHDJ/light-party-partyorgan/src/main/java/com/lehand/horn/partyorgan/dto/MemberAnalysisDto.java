package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "党员统计信息",description = "党员统计信息")
public class MemberAnalysisDto implements Serializable {

    private  static final long serialVersionUID = 1L;

    @ApiModelProperty(value="表单key",name="uuid")
    private String uuid ;

    @ApiModelProperty(value="组织机构id",name="organid")
    private String organid ;

    @ApiModelProperty(value="组织机构名称",name="organname")
    private String organname ;

    @ApiModelProperty(value="入库党员数",name="storagecount")
    private String  storagecount;

    @ApiModelProperty(value="预备党员数",name="probationarycount")
    private String  probationarycount;

    @ApiModelProperty(value="女性党员数",name="femalecount")
    private String  femalecount;

    @ApiModelProperty(value="大专及以上党员数",name="collegecount")
    private String  collegecount;

    @ApiModelProperty(value="少数民族党员数",name="ethniccount")
    private String  ethniccount;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }

    public String getOrganname() {
        return organname;
    }

    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public String getStoragecount() {
        return storagecount;
    }

    public void setStoragecount(String storagecount) {
        this.storagecount = storagecount;
    }

    public String getProbationarycount() {
        return probationarycount;
    }

    public void setProbationarycount(String probationarycount) {
        this.probationarycount = probationarycount;
    }

    public String getFemalecount() {
        return femalecount;
    }

    public void setFemalecount(String femalecount) {
        this.femalecount = femalecount;
    }

    public String getCollegecount() {
        return collegecount;
    }

    public void setCollegecount(String collegecount) {
        this.collegecount = collegecount;
    }

    public String getEthniccount() {
        return ethniccount;
    }

    public void setEthniccount(String ethniccount) {
        this.ethniccount = ethniccount;
    }
}
