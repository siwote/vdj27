package com.lehand.horn.partyorgan.pojo.dzz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 党建工作经费信息表
 * @author lx
 */
@ApiModel(value="党建工作经费信息表",description="党建工作经费信息表")
public class TDzzFunding implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "自增长主键", name = "id")
  private Long id;

  @ApiModelProperty(value = "组织机构名称", name = "orgname")
  private String orgname;

  @ApiModelProperty(value = "组织机构编码", name = "orgcode")
  private String orgcode;

  @ApiModelProperty(value = "费用类型", name = "type")
  private String type;

  @ApiModelProperty(value = "使用金额", name = "uselines")
  private Double uselines;

  @ApiModelProperty(value = "使用日期", name = "usetime")
  private String usetime;

  @ApiModelProperty(value = "创建时间", name = "createtime")
  private String createtime;

  @ApiModelProperty(value = "创建人ID", name = "createuserid")
  private String createuserid;

  @ApiModelProperty(value = "修改时间", name = "updatetime")
  private String updatetime;

  @ApiModelProperty(value = "修改人ID", name = "updateuserid")
  private String updateuserid;

  @ApiModelProperty(value = "事项说明", name = "remark")
  private String remark;

  @ApiModelProperty(value = "附件id,以','拼接", name = "fileid")
  private String fileid;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOrgname() {
    return orgname;
  }

  public void setOrgname(String orgname) {
    this.orgname = orgname;
  }

  public String getOrgcode() {
    return orgcode;
  }

  public void setOrgcode(String orgcode) {
    this.orgcode = orgcode;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Double getUselines() {
    return uselines;
  }

  public void setUselines(Double uselines) {
    this.uselines = uselines;
  }

  public String getUsetime() {
    return usetime;
  }

  public void setUsetime(String usetime) {
    this.usetime = usetime;
  }

  public String getCreatetime() {
    return createtime;
  }

  public void setCreatetime(String createtime) {
    this.createtime = createtime;
  }



  public String getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(String updatetime) {
    this.updatetime = updatetime;
  }

  public String getCreateuserid() {
    return createuserid;
  }

  public void setCreateuserid(String createuserid) {
    this.createuserid = createuserid;
  }

  public String getUpdateuserid() {
    return updateuserid;
  }

  public void setUpdateuserid(String updateuserid) {
    this.updateuserid = updateuserid;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getFileid() {
    return fileid;
  }

  public void setFileid(String fileid) {
    this.fileid = fileid;
  }
}
