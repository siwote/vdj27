package com.lehand.horn.partyorgan.pojo.hjxx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: TDzzHjxxRemindConfig
 * @Description: 党组织换届提醒配置
 * @Author lx
 * @DateTime 2021-05-10 14:18
 */
@ApiModel(value = "党组织换届提醒配置", description = "党组织换届提醒配置")
public class TDzzHjxxRemindConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "compid", name = "compid")
    private long compid;
    @ApiModelProperty(value = "组织id", name = "orgid")
    private String orgid;
    @ApiModelProperty(value = "组织code", name = "orgcode")
    private String orgcode;
    /**
     * 被提醒人JSON
     */
    @ApiModelProperty(value = "被提醒人JSON(示例：[{\"userid\":\"6855083a17ff418195cad114fc6977dc\"},{\"userid\":\"6855083a17ff418195cad114fc6977dc\"}])", name = "remindpeople")
    private String remindpeople;
    /**
     *提醒周期JSON
     */
    @ApiModelProperty(value = "提醒周期JSON(1：一月份。3：三月份。6：六月份)多选以逗号拼接", name = "reminddate")
    private String reminddate;
    @ApiModelProperty(value = "创建时间", name = "createtime")
    private String createtime;
    @ApiModelProperty(value = "创建人ID", name = "createuserid")
    private long createuserid;
    @ApiModelProperty(value = "修改时间", name = "updatetime")
    private String updatetime;
    @ApiModelProperty(value = "修改人ID", name = "updateuserid")
    private long updateuserid;



    public long getCompid() {
        return compid;
    }

    public void setCompid(long compid) {
        this.compid = compid;
    }


    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }


    public String getRemindpeople() {
        return remindpeople;
    }

    public void setRemindpeople(String remindpeople) {
        this.remindpeople = remindpeople;
    }


    public String getReminddate() {
        return reminddate;
    }

    public void setReminddate(String reminddate) {
        this.reminddate = reminddate;
    }


    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }


    public long getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(long createuserid) {
        this.createuserid = createuserid;
    }


    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }


    public long getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(long updateuserid) {
        this.updateuserid = updateuserid;
    }


}
