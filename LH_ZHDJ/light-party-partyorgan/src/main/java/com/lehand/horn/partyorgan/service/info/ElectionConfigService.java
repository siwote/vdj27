package com.lehand.horn.partyorgan.service.info;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.Constant;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.SmsSendDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ElectionConfigService {
    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * Save the election reminder configuration
     * @param session
     */
    @Transactional(rollbackFor=Exception.class)
    public void saveElectionConfig(Map<String,Object> configParam,Session session) throws ParseException {
        Map param = getConfigParam(JSON.toJSONString(configParam.get("remindpeople")), configParam.get("reminddate").toString(), session);
        Map data = generalSqlComponent.query(SqlCode.getElectionRemindConfig,new Object[]{session.getCompid(),session.getCurrentOrg().getOrgcode()});
        String[] date = configParam.get("reminddate").toString().split(",");
        if(data==null){
            generalSqlComponent.insert(SqlCode.addElectionRemindConfig, param);
        }else{
            param.put("updatetime",DateEnum.YYYYMMDDHHMMDD.format());
            param.put("updateuserid",session.getUserid());
            generalSqlComponent.update(SqlCode.modiElectionRemindConfig, param);
            //删除没有扫描到的的时间点
            generalSqlComponent.delete(SqlCode.delElectionRemindTime, new Object[]{session.getCompid(),session.getCurrentOrg().getOrgid()});
        }
        //这里已经前端做了判断，这个以防万一后端在加个判断
        if(StringUtils.isEmpty(configParam.get("electiondate"))){
            LehandException.throwException("党组织下次换届时间缺失，请先维护下次换届时间！");
        }
        //插入有效的时间点
        String remindtime = "";
        for (String s : date) {
            remindtime = getPrevMonthDate(DateEnum.YYYYMMDDHHMMDD.parse(configParam.get("electiondate").toString()),Integer.valueOf(s));
            if(!StringUtils.isEmpty(remindtime)){
                addElectionRemindTime(session, remindtime,Integer.valueOf(s));
            }
        }
        String msg = "【智慧党建】: 您好,请于" + remindtime + "前完成"+configParam.get("dzzmc").toString()+"的换届准备!";
        //换届短信提醒
        //获取提醒人的userId
        List<Map<String,Object>> listConfigParam=(List<Map<String,Object>>)configParam.get("remindpeople");
        if(listConfigParam != null) {
            for (Map<String, Object> listConfigParamMap : listConfigParam) {
                Iterator<Map.Entry<String, Object>> iterator = listConfigParamMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, Object> entry = iterator.next();
                    String userId = (String) entry.getValue();
                    Map<String, Object> map = generalSqlComponent.query(SqlCode.queryPhoneByUserId, new Object[]{userId});
                    if (map == null || StringUtils.isEmpty(map.get("lxdh"))) {
                        continue;
                    }
                    String idCard = map.get("zjhm").toString();
                    String phone = map.get("lxdh").toString();
                    Map<String, Object> user = generalSqlComponent.query(SqlCode.queryUrcUserByIdno, new Object[]{idCard});
                    if (user == null) {
                        continue;
                    }
                    SmsSendDto smsSendDto = new SmsSendDto();
                    smsSendDto.setCompid(session.getCompid());
                    smsSendDto.setUsermark(user.get("userid").toString());
                    smsSendDto.setPhone(phone);
                    smsSendDto.setContent(msg);
                    smsSendDto.setBusinessid(Long.parseLong(Constant.CHANGE_TERM_REMIND));
                    smsSendDto.setStatus(0);
                    smsSendDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
                    smsSendDto.setDelflag(0);
                    //插入短信发送内容
                    generalSqlComponent.insert(SqlCode.addSmsSend, smsSendDto);
                }
            }
        }

    }

    /**
     * Add reminder time
     * @param session
     * @param remindtime
     * @param date
     */
    private void addElectionRemindTime(Session session, String remindtime,Integer date) {
        generalSqlComponent.insert(SqlCode.addElectionRemindTime, getTParam(remindtime+ " 09:00:00",date,session));
    }

    /**
     * Calculate the corresponding time points of a few months before and after a certain date
     * @param date
     * @param n
     * @return
     */
    public String getPrevMonthDate(Date date, int n) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - n);
        return DateEnum.YYYYMMDD.parse(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()))
                .before(DateEnum.YYYYMMDD.parse(DateEnum.YYYYMMDD.format())) ?
                "" : new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    /**
     * Splice table election_remind_config parameters
     * @param remindpeople
     * @param reminddate
     * @param session
     * @return
     */
    private Map getConfigParam(String remindpeople, String reminddate, Session session) {
        Map param = new HashMap();
        param.put("compid",session.getCompid());
        param.put("orgid",session.getCurrentOrg().getOrgid());
        param.put("orgcode",session.getCurrentOrg().getOrgcode());
        param.put("remindpeople",remindpeople);
        param.put("reminddate",reminddate);
        param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
        param.put("createuserid",session.getUserid());
        param.put("updatetime","");
        param.put("updateuserid",null);
        param.put("remarks1",0);
        param.put("remarks2",0);
        param.put("remarks3","");
        param.put("remarks4","");
        return param;
    }


    /**
     * Splice table election_remind_time parameters
     * @param remindtime
     * @param session
     * @param date
     * @return
     */
    private Map getTParam(String remindtime,Integer date, Session session) {
        Map param = new HashMap();
        param.put("compid",session.getCompid());
        param.put("orgid",session.getCurrentOrg().getOrgid());
        param.put("orgcode",session.getCurrentOrg().getOrgcode());
        param.put("remindtime",remindtime);
        param.put("status",0);
        param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
        param.put("createuserid",session.getUserid());
        param.put("remarks1",date);
        param.put("remarks2",0);
        param.put("remarks3","");
        param.put("remarks4","");
        return param;
    }

    /**
     * election reminder configuration query
     * @param session
     * @param organid
     * @return
     */
    public Map queryElectionConfig(String organid,Session session) {
        return generalSqlComponent.query(SqlCode.queryElectionRemindConfig,new Object[]{organid});
    }
}
