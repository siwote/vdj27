package com.lehand.horn.partyorgan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "党员信息返回数据",description = "党员信息返回数据")
public class MemberBaseInfoDto  extends MemberBasicDto {

    private  static final long serialVersionUID = 1L;

    @ApiModelProperty(value="民族",name="mz")
    private String mz;

    @ApiModelProperty(value="性别",name="xb")
    private String xb;

    @ApiModelProperty(value="出生日期",name="csrq")
    private String csrq;

    @ApiModelProperty(value="民族名称",name="mzName")
    private String mzName;

    @ApiModelProperty(value="民族名称",name="mzName")
    private String xm;

    @ApiModelProperty(value="年龄",name="age")
    private String age;

    @ApiModelProperty(value="毕业院校",name="byyx")
    private String byyx;

    @ApiModelProperty(value="专业",name="byyx")
    private String zy;


    @ApiModelProperty(value="学历名称",name="xlName")
    private String xlName;

    @ApiModelProperty(value="学位",name="xw")
    private String xw;

    @ApiModelProperty(value="学位名称",name="xwName")
    private String xwName;

    @ApiModelProperty(value="党内职务",name="dnzwname")
    private String dnzwname;

    @ApiModelProperty(value="党内职务名",name="dnzwnameName")
    private String dnzwnameName;

    @ApiModelProperty(value="工作岗位",name="gzgw")
    private String gzgw;

    @ApiModelProperty(value="工作岗位名称",name="gzgwName")
    private String gzgwName;

    @ApiModelProperty(value="技术职称",name="jszc")
    private String jszc;

    @ApiModelProperty(value="技术职称名称",name="jszcName")
    private String jszcName;

    @ApiModelProperty(value="新的社会阶层",name="xshjclx")
    private String xshjclx;

    @ApiModelProperty(value="新的社会阶层名称",name="xshjclxName")
    private String xshjclxName;

    @ApiModelProperty(value="党员类别",name="dylb")
    private String dylb;

    @ApiModelProperty(value="党员类别名称",name="dylbName")
    private String dylbName;

    public String getMz() {
        return mz;
    }

    public void setMz(String mz) {
        this.mz = mz;
    }

    public String getMzName() {
        return mzName;
    }

    public void setMzName(String mzName) {
        this.mzName = mzName;
    }

    public String getXb() {
        return xb;
    }

    public void setXb(String xb) {
        this.xb = xb;
    }

    public String getCsrq() {
        return csrq;
    }

    public void setCsrq(String csrq) {
        this.csrq = csrq;
    }

    @Override
    public String getXm() {
        return xm;
    }

    @Override
    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getByyx() {
        return byyx;
    }

    public void setByyx(String byyx) {
        this.byyx = byyx;
    }

    public String getZy() {
        return zy;
    }

    public void setZy(String zy) {
        this.zy = zy;
    }

    public String getXlName() {
        return xlName;
    }

    public void setXlName(String xlName) {
        this.xlName = xlName;
    }

    public String getXw() {
        return xw;
    }

    public void setXw(String xw) {
        this.xw = xw;
    }

    public String getXwName() {
        return xwName;
    }

    public void setXwName(String xwName) {
        this.xwName = xwName;
    }

    public String getDnzwname() {
        return dnzwname;
    }

    public void setDnzwname(String dnzwname) {
        this.dnzwname = dnzwname;
    }

    public String getDnzwnameName() {
        return dnzwnameName;
    }

    public void setDnzwnameName(String dnzwnameName) {
        this.dnzwnameName = dnzwnameName;
    }

    public String getGzgw() {
        return gzgw;
    }

    public void setGzgw(String gzgw) {
        this.gzgw = gzgw;
    }

    public String getGzgwName() {
        return gzgwName;
    }

    public void setGzgwName(String gzgwName) {
        this.gzgwName = gzgwName;
    }

    public String getJszc() {
        return jszc;
    }

    public void setJszc(String jszc) {
        this.jszc = jszc;
    }

    public String getJszcName() {
        return jszcName;
    }

    public void setJszcName(String jszcName) {
        this.jszcName = jszcName;
    }

    public String getXshjclx() {
        return xshjclx;
    }

    public void setXshjclx(String xshjclx) {
        this.xshjclx = xshjclx;
    }

    public String getXshjclxName() {
        return xshjclxName;
    }

    public void setXshjclxName(String xshjclxName) {
        this.xshjclxName = xshjclxName;
    }

    public String getDylb() {
        return dylb;
    }

    public void setDylb(String dylb) {
        this.dylb = dylb;
    }

    public String getDylbName() {
        return dylbName;
    }

    public void setDylbName(String dylbName) {
        this.dylbName = dylbName;
    }
}
