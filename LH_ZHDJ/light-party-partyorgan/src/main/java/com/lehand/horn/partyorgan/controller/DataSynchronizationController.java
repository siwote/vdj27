package com.lehand.horn.partyorgan.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.dto.MemberSearchReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lehand.horn.partyorgan.service.DataSynchronizationService;

import javax.annotation.Resource;

@Api(value = "数据同步", tags = { "数据同步" })
@RestController
@RequestMapping("/lhdj/datasynchronization")
public class DataSynchronizationController extends HornBaseController{

    private static final Logger logger = LogManager.getLogger(DataSynchronizationController.class);

    @Resource private DataSynchronizationService dataSynchronizationSeivice;

    @OpenApi(optflag=0)
    @ApiOperation(value = "数据同步", notes = "数据同步", httpMethod = "POST")
    @RequestMapping("/synchronization")
    public Message synchronization() {
        Message message = new Message();
        try {
            dataSynchronizationSeivice.synchronizationData();
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("数据同步出错" + e);
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "同步机构", notes = "同步机构", httpMethod = "POST")
    @RequestMapping("/one")
    public Message one() {
        Message message = new Message();
        try {
            dataSynchronizationSeivice.syncUrcOrgan();
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("机构同步出错" + e);
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "同步用户信息", notes = "同步用户信息", httpMethod = "POST")
    @RequestMapping("/two")
    public Message two() {
        Message message = new Message();
        try {
            dataSynchronizationSeivice.syncUserInfos();
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("用户信息同步出错" + e);
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "更新机构Userid", notes = "更新机构Userid", httpMethod = "POST")
    @RequestMapping("/three")
    public Message three() {
        Message message = new Message();
        try {
            dataSynchronizationSeivice.modiOrgUserid();
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("更新机构同步出错" + e);
        }
        return message;
    }

    @OpenApi(optflag=0)
    @ApiOperation(value = "同步机构简称", notes = "同步机构简称", httpMethod = "POST")
    @RequestMapping("/four")
    public Message four() {
        Message message = new Message();
        try {
            dataSynchronizationSeivice.synchronize();
            message.success();
        } catch (Exception e) {
            logger.error(e);
            message.setMessage("机构简称同步出错" + e);
        }
        return message;
    }
}
