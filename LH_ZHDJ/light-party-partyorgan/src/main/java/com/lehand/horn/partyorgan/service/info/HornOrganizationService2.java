package com.lehand.horn.partyorgan.service.info;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.component.PartyCacheComponent;
import com.lehand.horn.partyorgan.constant.MagicConstant;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dao.TSecretaryInfoDao;
import com.lehand.horn.partyorgan.dto.*;
import com.lehand.horn.partyorgan.dto.info.ElectionExportReq;
import com.lehand.horn.partyorgan.pojo.DlType;
import com.lehand.horn.partyorgan.pojo.SysUser;
import com.lehand.horn.partyorgan.pojo.UrcAuthOtherMap;
import com.lehand.horn.partyorgan.pojo.dzz.*;
import com.lehand.horn.partyorgan.service.DataSynchronizationService;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.horn.partyorgan.util.ExcelUtils;
import com.lehand.horn.partyorgan.util.OrgUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.lehand.horn.partyorgan.util.HornDBUtil.getHornDB;

/**
 *	 组织机构业务层   交控导入党组织 运维使用（使用时去除各种定时器）
 * @author admin
 *
 */
@Service
public class HornOrganizationService2 {


    @Resource
    private GeneralSqlComponent generalSqlComponent;




    @Transactional(rollbackFor=Exception.class)
    public String dzzInfoImport2(Session session) {
        //TODO 待优化结构
        int total = 0;
        int success = 0;
        int failed = 0;
        StringBuffer retMsg = new StringBuffer("");
        List<String> errorMsg = new ArrayList<>();
        List<List<String>> list = new ArrayList<>();

        List<Map<String,String>> data2= generalSqlComponent.query(SqlCode.getTemTDzz,new Object[]{});
        for(Map<String,String> li:data2){
            List<String> list2 = new ArrayList<String>(35);
            list2.add(li.get("党组织代码"));
            list2.add(li.get("党组织名称"));
            list2.add(li.get("组织类别"));
            list2.add(li.get("属地关系"));
            list2.add(li.get("党组织书记"));
            list2.add(li.get("党组织联系人"));
            list2.add(li.get("联系电话"));
            list2.add(li.get("建立党组织日期"));
            list2.add(li.get("通讯地址"));
            list2.add(li.get("传真号码"));
            list2.add(li.get("党组织所在单位情况"));
            list2.add(li.get("党组织所在行政区划"));
            list2.add(li.get("单位名称"));
            list2.add(li.get("单位隶属关系"));
            list2.add(li.get("单位性质类别"));
            list2.add(li.get("单位建立党组织情况"));
            list2.add(li.get("法人单位标识"));
            list2.add(li.get("法人代表（单位负责人）"));
            list2.add(li.get("是否党员"));
            list2.add(li.get("社会统一信用代码"));
            list2.add(li.get("企业控制（控股）情况"));
            list2.add(li.get("企业规模"));
            list2.add(li.get("民营科技企业标识"));
            list2.add(li.get("社会服务机构类别"));
            list2.add(li.get("事业单位类别"));
            list2.add(li.get("在岗职工数（人）"));
            list2.add(li.get("在岗职工中工人数（人）"));
            list2.add(li.get("在岗专业技术人员数（人）"));
            list2.add(li.get("是否建有工会"));
            list2.add(li.get("中介组织标识"));
            list2.add(li.get("户数（户）"));
            list2.add(li.get("人口（人）"));
            list2.add(li.get("上级党组织编码"));
            list2.add(li.get("换届日期"));
            list2.add(li.get("届满日期"));
            list.add(list2);

        }
        if(list.size() <= 0) {
            LehandException.throwException("没有读取到正确的党组织导入信息！");
        }
        List<String> listNew = new ArrayList<>();
        list.forEach(a->{
            listNew.add(a.get(0));
        });
        //校验是否有党组织被删除了或者被合并了
        Map<String, Object> paramMap2 = new HashMap<>();
        paramMap2.put("organcode",list.get(0).get(0));
        TDzzInfo pold = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, paramMap2);
        String orgnames = "";
        if(pold!=null){
            List<String> listOld = generalSqlComponent.query(SqlCode.listTDzzInfos3,new Object[]{"%"+pold.getOrganid()+"%"});
            for(String o :listOld){
                if(!listNew.contains(o)){
                    //不包含将党组织信息置为无效状态使用字段 operatetype=3
                    orgnames += "'"+o+"'"+",";
                }
            }
        }
        orgnames = !StringUtils.isEmpty(orgnames) ? orgnames.substring(0,orgnames.length()-1) : orgnames;
        Map params = new HashMap();
        params.put("orgname",orgnames);
        if(!StringUtils.isEmpty(orgnames)){
            //t_dzz_info 置为无效
            generalSqlComponent.update(SqlCode.modiTDzzinfos,params);
            //t_dzz_info_simple 置为无效
            Map map = generalSqlComponent.query(SqlCode.getOrganidBydxzms,params);
            generalSqlComponent.update(SqlCode.modiTDzzSimpleinfos,map);
            //urc_organization 置为无效
            generalSqlComponent.update(SqlCode.modiUrcOrganizations4,params);
            //urc_user_account 删除账号
//            Map map2 = generalSqlComponent.query(SqlCode.getOrganidBydxzms2,params);
//            String organcode = String.valueOf(map2.get("organcode"));
//            String[] organcodes = organcode.split(",");
//            String orgcodes = "";
//            for (String s : organcodes) {
//                orgcodes += "'B"+s+"','A"+s+"',";
//            }
//            orgcodes = orgcodes.substring(0,orgcodes.length()-1);
//            dBComponent.update("update sys_user set status=0 where acc in ("+orgcodes+")",new Object[]{});
//            String userids = generalSqlComponent.query(SqlCode.modiUrcOrganizations2,params);
//            dBComponent.update("update urc_user set status=0 where userid in ("+userids+")",new Object[]{});
        }
        total = list.size() ;
        //从第三行导入党组织信息
        Map<String, Object> paramMap = new HashMap<>();
        for(int i = 0 ; i < list.size(); i++) {
            try {
                List<String> dzz = list.get(i);
                String organid = UUID.randomUUID().toString().replace("-","");
                if(StringUtils.isEmpty(dzz.get(0).trim()) || StringUtils.isEmpty(dzz.get(32).trim())) {
                    failed++;
                    errorMsg.add(String.format("第%d行 党组织编码或者上级党组织编码为空，跳过;",(i+2)));
                    continue;
                }

                paramMap.put("organcode", dzz.get(32).trim());
                TDzzInfo pold2 = generalSqlComponent.query(SqlCode.queryDzzInfoByOrgancode, paramMap);
                if(!"0".equals(dzz.get(32).trim())){
                    if(pold2 == null) {
                        failed++;
                        errorMsg.add(String.format("第%d行 党组织编码或者上级党组织编码记录不存在，跳过;",(i+2)));
                        continue;
                    }
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    if(dzz.size()>7&&!StringUtils.isEmpty(dzz.get(7))){
                        dzz.set(7,sdf.format(sdf.parse(dzz.get(7))));
                    }
                    if(dzz.size()>33&&!StringUtils.isEmpty(dzz.get(33))){
                        dzz.set(33,sdf.format(sdf.parse(dzz.get(33))));
                    }
                    if(dzz.size()>34&&!StringUtils.isEmpty(dzz.get(34))){
                        dzz.set(34,sdf.format(sdf.parse(dzz.get(34))));
                    }
                } catch (Exception e) {
                    failed++;
                    errorMsg.add(String.format("第%s行 存在时间格式错误，请检查",(i+2)));
                    continue;
                }
                try {
                    Map data = generalSqlComponent.query(SqlCode.getTDzzInfo2,new Object[]{dzz.get(0).trim()});
                    if(data!=null){
                        organid = String.valueOf(data.get("organid"));
                    }
                    //保存导入党组织信息
                    saveDzz2(dzz, pold, organid, session, pold2);
                    success++;
                } catch (Exception e) {
                    errorMsg.add(String.format("第%s行 数据导入异常,请检查后重试",(i+2)));
                    failed++;
                    e.printStackTrace();
                }
            } catch (Exception e) {
                errorMsg.add(String.format("第%s行 数据导入异常,请检查后重试",(i+2)));
                failed++;
            }
        }
        //同步机构简称
//        dataSynchronizationService.synchronize();
        if(errorMsg.size() > 0) {
            return String.format("%s \r\n 党组织数据导入成功%d条，失败%d条。", errorMsg.toString(), success,failed);
        } else {
//            return String.format("党组织数据导入成功%d条，失败%d条。", success,failed);
            return null;
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void saveDzz2(List<String> dzz, TDzzInfo pold, String organid, Session session, TDzzInfo parent) {
        //保存单位信息
        String dwUuids = saveDZZDWInfo(dzz, organid);
        //保存导入的党组织的主表信息
        saveMainDZZInfo(dzz, organid, dwUuids, pold, parent);
        //保存换届信息配置表
//        saveDZZHjxxDzz(dzz, organid);
        //党组织路径等信息配置表
        saveDzzInfoSimple(dzz, organid, pold, parent);
        //党组织补充表
        saveDZZBCInfo(dzz, organid);
        //保存党组织到系统组织机构表
        Long orgId = saveUrcOrg2(dzz, session);
        //新建党组织账户
        addUrcUserAndAccount2(dzz, session, orgId, organid);

    }





    /**
     * 增加党组织在系统模块的用户和账号信息
     * @param dzz
     */
    @Transactional(rollbackFor=Exception.class)
    public void addUrcUserAndAccount2(List<String> dzz, Session session, Long orgId,String organid) {
        String idno = dzz.get(0).trim();

        Long userid = 0L;
        Map<String,Object> param = new HashMap<String, Object>(6);
        param.put("username",dzz.get(1));
        param.put("sex",0);
        param.put("idno",idno);
        param.put("postname", "");
        param.put("accflag", 1);
        param.put("nickname", "");
        param.put("image", "");
        param.put("admin", 1);
        param.put("cuserid", "");
        param.put("createtime", DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        param.put("uuserid", "");
        param.put("updatetime", "");
        param.put("uuserid", DateEnum.YYYYMMDDHHMMDD.format(new Date()));
        param.put("status", 1);
        param.put("compid",session==null?1:session.getCompid());
        param.put("remarks1", 0);
        param.put("remarks2", 0);
        param.put("remarks3", "");
        param.put("remarks4", "");
        Map<String,Object> map2 = generalSqlComponent.query(SqlCode.getOrgan3,new Object[]{dzz.get(0)});
        if(map2 == null){//修改
            userid = generalSqlComponent.insert(SqlCode.addPwUser2,param);
//            insertSysuser(dzz,organid);
        } else {
            userid = Long.valueOf(map2.get("remarks3").toString());
            generalSqlComponent.update(SqlCode.updatePUUsername2,new Object[]{dzz.get(1),userid});
//            updateSysuser(dzz,organid);
        }

        //绑定用户和组织的关系
        param.put("userid",userid);
        param.put("orgid",orgId);
        param.put("seqno",orgId);
        param.put("compid",session==null?1:session.getCompid());
        generalSqlComponent.delete(SqlCode.removeUserOrgMapByUserid2, new Object[] {userid, session==null?1:session.getCompid()});
        generalSqlComponent.insert(SqlCode.addUserOrgMap4, param);
        //更新机构表remarks3字段为用户ID
        generalSqlComponent.update(SqlCode.modiUserOrg2, new Object[]{userid,orgId});

        //更新账户表
        generalSqlComponent.delete(SqlCode.delUaccountByAccount2, new Object[] {String.format("B%s",idno)});
        generalSqlComponent.insert(SqlCode.addUserAccount3,new Object[]{userid,String.format("B%s",idno), SysConstants.DEFAULTPWD2.SIMPLESIX,session==null?1:session.getCompid()});
//        generalSqlComponent.delete(SqlCode.delUaccountByAccount2, new Object[] {String.format("A%s",idno)});
//        generalSqlComponent.insert(SqlCode.addUserAccount3,new Object[]{userid,String.format("A%s",idno), SysConstants.DEFAULTPWD2.SIMPLESIX,session==null?1:session.getCompid()});

//        //给组织账号添加相应的菜单权限
//        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
//        String orgcode = map1!=null ? map1.get("item_code").toString() : "";
//        Map map = null;
//        if(dangwei.equals(orgcode)){
//            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.PARTY_COMMITTEE_ROLE});
//        }else if(dangzongzhi.equals(orgcode)){
//            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.GENERAL_PARTY_BRANCH_ROLE});
//        }else{
//            map = generalSqlComponent.query(SqlCode.getUrcRole,new Object[]{session.getCompid(),SysConstants.DEFAULTROLEID.PARTY_BRANCH_ROLE});
//        }
//        generalSqlComponent.delete(SqlCode.removeUrcRoleMap,new Object[]{0,userid,map.get("roleid"),session.getCompid()});
//        generalSqlComponent.insert(SqlCode.addUrcRoleMap,new Object[]{0,userid,map.get("roleid"),session.getCompid(),orgId,DateEnum.YYYYMMDDHHMMDD.format()});
    }

    /**
     * 保存导入的党组织的主表信息
     * @param dzz
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveMainDZZInfo(List<String> dzz, String organid, String dwUuids, TDzzInfo pold,TDzzInfo parent) {
        //查询是否存在党组织
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", dzz.get(0).trim());
        //generalSqlComponent.delete(SqlCode.delDzzInfoByCode, paramMap);

        TDzzInfo tDzzInfo = new TDzzInfo();
        tDzzInfo.setDelflag("0");
        tDzzInfo.setOrganid(organid);
        tDzzInfo.setParentid(parent==null ? "0" : parent.getOrganid());//查询返回父ID
        boolean oc = dzz.get(0).trim().contains("'");
        if(!oc){
            tDzzInfo.setOrgancode(dzz.get(0).trim());
        }else{
            tDzzInfo.setOrgancode(dzz.get(0).trim().substring(1,dzz.get(0).trim().length()-1));
        }
        tDzzInfo.setDzzmc(dzz.get(1));
        tDzzInfo.setDxzms(dzz.get(1));
        tDzzInfo.setOperatetype("0");
        //组织类别
        Map map = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
        tDzzInfo.setZzlb(map!=null ? map.get("item_code").toString() : "");
        //单位隶属关系
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode3,new Object[]{dzz.get(13)});
//        tDzzInfo.setDzzlsgx(map1!=null ? map1.get("item_code").toString() : "");
        tDzzInfo.setDzzlsgx(dzz.get(13));
//        tDzzInfo.setDzzsj(dzz.get(4));
        tDzzInfo.setDzzsj("");
        tDzzInfo.setLxrname(dzz.get(5));
        if(dzz.get(6).contains(".")){
            BigDecimal bd = new BigDecimal(dzz.get(6));
            tDzzInfo.setLxrphone(bd.toPlainString());// 7
        }else{
            tDzzInfo.setLxrphone(dzz.get(6));// 7
        }

        tDzzInfo.setXzqh(dzz.get(11)); //行政区号
        //单位建立党组织情况
        Map map2 = generalSqlComponent.query(SqlCode.getZzlbCode4,new Object[]{dzz.get(15)});
//        tDzzInfo.setDzzszdwqk(map1!=null ? map1.get("item_code").toString() : "");
        tDzzInfo.setDzzszdwqk(dzz.get(15));
        tDzzInfo.setSzdwid(dwUuids);//生成单位后返回
        //单位性质类别
//        Map map3 = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{dzz.get(14)});
//        tDzzInfo.setSzdwlb(map3!=null ? map3.get("item_code").toString() : "");
        tDzzInfo.setSzdwlb(dzz.get(14));
        boolean pc = dzz.get(32).trim().contains("'");
        if(!pc){
            tDzzInfo.setParentcode(dzz.get(32).trim());
        }else{
            tDzzInfo.setParentcode(dzz.get(32).trim().substring(1,dzz.get(32).trim().length()-1));
        }
        //在主表中保存信息
        Map map4 = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{dzz.get(0).trim()});
        if(map4==null){
            generalSqlComponent.insert(SqlCode.saveDZZInfo2, tDzzInfo);
        }else{
            generalSqlComponent.update(SqlCode.updateDZZInfo, tDzzInfo);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveDZZHjxxDzz(List<String> dzz, String organid) {
        Map param = new HashMap();
        String uuid = UUID.randomUUID().toString().replace("-","");
        param.put("uuid",uuid);
        param.put("organid",organid);
        param.put("organname",dzz.get(1));
        param.put("hjdate",!StringUtils.isEmpty(dzz.get(33)) ? dzz.get(33) + " 00:00:00": "");
        param.put("jmdate",!StringUtils.isEmpty(dzz.get(34)) ? dzz.get(34) + " 00:00:00": "");
        param.put("bjbzcsfs","1000000001");
        param.put("syncstatus","A");
        param.put("synctime",DateEnum.YYYYMMDDHHMMDD.format());
        param.put("zfbz",0);
        if(!StringUtils.isEmpty(dzz.get(34))){//届满日期不为空
            generalSqlComponent.delete(SqlCode.delDzzBzxxByOrganid, param);
            generalSqlComponent.insert(SqlCode.addDzzBzxxByOrganid, param);
        }
    }

    /**
     * 党组织信息保存
     * @param dzz
     * @param organid
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveDzzInfoSimple(List<String> dzz, String organid, TDzzInfo p, TDzzInfo parent) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organcode", dzz.get(0).trim());
        //generalSqlComponent.delete(SqlCode.delDzzInfoSimpleByCode, paramMap);

        TDzzInfoSimple pold = generalSqlComponent.query(SqlCode.getDzzsjByCode, new Object[] {dzz.get(32).trim()});
        String idPath = pold == null ? "/"+organid+"/" : String.format("%s%s/", pold.getIdPath(), organid);

        TDzzInfoSimple tDzzInfoSimple = new TDzzInfoSimple();
        tDzzInfoSimple.setDelflag("0");
        tDzzInfoSimple.setOrgancode(dzz.get(0).trim());
        tDzzInfoSimple.setOrganid(organid);
        tDzzInfoSimple.setDzzmc(dzz.get(1));
        tDzzInfoSimple.setParentid(pold==null ? "0" : pold.getOrganid());
        //组织类别
        Map map = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{dzz.get(2)});
        tDzzInfoSimple.setZzlb(map!=null ? map.get("item_code").toString() : "");
        tDzzInfoSimple.setOperatetype("0");
        //单位隶属关系
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode3,new Object[]{dzz.get(13)});
//        tDzzInfoSimple.setDzzlsgx(map1!=null ? map1.get("item_code").toString() : "");
        tDzzInfoSimple.setDzzlsgx(dzz.get(13));
//        tDzzInfoSimple.setDzzsj(dzz.get(4));
        tDzzInfoSimple.setDzzsj("");
        tDzzInfoSimple.setParentcode(dzz.get(32).trim());
        tDzzInfoSimple.setIdPath(idPath);

        //设置路径
        if(pold !=null && pold.getPath1() != null) {
            tDzzInfoSimple.setPath1(pold.getPath1());
            if(pold.getOrganid() != pold.getPath1() && pold.getPath2() != null) {
                tDzzInfoSimple.setPath2(pold.getPath2());
                if(pold.getOrganid() != pold.getPath2() && pold.getPath3() != null) {
                    tDzzInfoSimple.setPath3(pold.getPath3());
                    if(pold.getOrganid() != pold.getPath3() && pold.getPath4() != null) {
                        tDzzInfoSimple.setPath4(pold.getPath4());
                        if(pold.getOrganid() != pold.getPath4() && pold.getPath5() != null) {
                            tDzzInfoSimple.setPath5(pold.getPath5());
                            if(pold.getOrganid() != pold.getPath5() && pold.getPath6() != null) {
                                tDzzInfoSimple.setPath6(pold.getPath6());
                                if(pold.getOrganid() != pold.getPath6() && pold.getPath7() != null) {
                                    tDzzInfoSimple.setPath7(pold.getPath7());
                                    if(pold.getOrganid() != pold.getPath7() && pold.getPath8() != null) {
                                        tDzzInfoSimple.setPath8(pold.getPath8());
                                        if(pold.getOrganid() != pold.getPath8() && pold.getPath9() != null) {
                                            tDzzInfoSimple.setPath9(pold.getPath9());
                                            if(pold.getOrganid() != pold.getPath9() && pold.getPath10() != null) {
                                                tDzzInfoSimple.setPath10(pold.getPath10());
                                            } else {
                                                tDzzInfoSimple.setPath10(organid);
                                            }
                                        } else {
                                            tDzzInfoSimple.setPath9(organid);
                                        }
                                    } else {
                                        tDzzInfoSimple.setPath8(organid);
                                    }
                                } else {
                                    tDzzInfoSimple.setPath7(organid);
                                }
                            } else {
                                tDzzInfoSimple.setPath6(organid);
                            }
                        } else {
                            tDzzInfoSimple.setPath5(organid);
                        }
                    } else {
                        tDzzInfoSimple.setPath4(organid);
                    }
                } else {
                    tDzzInfoSimple.setPath3(organid);
                }
            } else {
                tDzzInfoSimple.setPath2(organid);
            }

        }else{
            tDzzInfoSimple.setPath1(organid);
        }
        tDzzInfoSimple.setInsertDate(new Date());
        //在主表中保存信息
        Map map4 = generalSqlComponent.query(SqlCode.geTTDzzInfoimplBycode, new Object[]{dzz.get(0).trim()});
        if(map4==null){
            generalSqlComponent.insert(SqlCode.saveDzzsimple, tDzzInfoSimple);
        }else{
            generalSqlComponent.update(SqlCode.updateDZZsimpleInfo, tDzzInfoSimple);
        }
    }

    /**
     * 保存党组织补充信息
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveDZZBCInfo(List<String> list, String organid) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("organid", organid);
        TDzzInfoBc old = generalSqlComponent.query(SqlCode.queryDzzInfoBcByOranid, paramMap);

        TDzzInfoBc tDzzInfoBc = new TDzzInfoBc();
        tDzzInfoBc.setOrganid(organid);
        tDzzInfoBc.setDzzsj(list.get(4));
        tDzzInfoBc.setLxrname(list.get(5));
        tDzzInfoBc.setLxrphone(list.get(6));
        tDzzInfoBc.setDzzjlrq(list.get(7));
        if(old == null) {
            generalSqlComponent.insert(SqlCode.saveDzzInfoBc, tDzzInfoBc);
        } else {
            generalSqlComponent.update(SqlCode.updateDzzInfoBc, tDzzInfoBc);
        }
    }

    /**
     * 保存党组织单位信息
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public String saveDZZDWInfo(List<String> list, String organid) {
        StringBuffer stringBuffer = new StringBuffer("");
        //单位名称
        if(StringUtils.isEmpty(list.get(1))) {
            return "";
        }
        String[] array = list.get(10).toString().split(",");
//        if(array.length > 0) {
//            for(int i = 0 ; i < array.length; i++) {
//                String uuid = UUID.randomUUID().toString().replace("-","");
//                stringBuffer.append(uuid).append(",");
//                TDwInfo tDwInfo = new TDwInfo();
//                tDwInfo.setOrganid(organid);
//                tDwInfo.setUuid(uuid);
//             //   tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMM.format());
//                tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
//                tDwInfo.setDwname(array[i]);
//                //组织性质类别
//                Map map = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{list.get(14)});
//                tDwInfo.setDwxzlb(map!=null ? map.get("item_code").toString() : "");
//                tDwInfo.setOperatetype("0");
//                generalSqlComponent.insert(SqlCode.saveDwInfo, tDwInfo);
//            }
//        }

        Map<String,Object> dwmap = generalSqlComponent.query(SqlCode.getTDwInfo,new Object[]{organid});
        if(dwmap==null){
            if(!StringUtils.isEmpty(list.get(10).toString())){
                String uuid = UUID.randomUUID().toString().replace("-","");
                stringBuffer.append(uuid).append(",");
                TDwInfo tDwInfo = new TDwInfo();
                tDwInfo.setOrganid(organid);
                tDwInfo.setUuid(uuid);
                //   tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMM.format());
                tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
                tDwInfo.setDwname(list.get(12));
                Map item = generalSqlComponent.query(SqlCode.getItemByItemcode2,new Object[]{"d_dw_lsgx",list.get(13)});
                tDwInfo.setDwlsgx(item.get("item_code").toString());
                //组织性质类别
                Map map = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{list.get(14)});
                tDwInfo.setDwxzlb(map!=null ? map.get("item_code").toString() : "");
                tDwInfo.setDwjldjczzqk(list.get(15));
                tDwInfo.setIsfrdw("是".equals(list.get(16).toString())?"1":"0");
                tDwInfo.setDwfzr(list.get(17));
                tDwInfo.setFzrsfsdy("是".equals(list.get(18).toString())?"1":"0");
                tDwInfo.setDwcode(list.get(19));
                tDwInfo.setJjlx(list.get(20));
                tDwInfo.setQygm(list.get(21));
                tDwInfo.setSfmykjqy("是".equals(list.get(22).toString())?"1":"0");
                tDwInfo.setZgzgs(list.get(25));
                tDwInfo.setZgzgzgrs(list.get(26));
                tDwInfo.setZgzyjsrys(list.get(27));
                tDwInfo.setSfjygh("是".equals(list.get(28).toString())?"1":"0");
                tDwInfo.setSfzjzz("是".equals(list.get(28).toString())?"1":"0");
                tDwInfo.setOperatetype("0");
                tDwInfo.setZfbz("0");
                tDwInfo.setSyncstatus("A");
                tDwInfo.setOperatetype("0");
                generalSqlComponent.insert(SqlCode.saveDwInfo2, tDwInfo);
                generalSqlComponent.insert(SqlCode.addTDwInfoBc, tDwInfo);
                stringBuffer.append(tDwInfo.getUuid());
            }
        }else{
            if(!StringUtils.isEmpty(list.get(10).toString())){
                TDwInfo tDwInfo = new TDwInfo();
                tDwInfo.setOrganid(organid);
                //   tDwInfo.setCreatetime(DateEnum.YYYYMMDDHHMM.format());
                tDwInfo.setUuid(dwmap.get("uuid").toString());
                tDwInfo.setDwname(list.get(12));
                tDwInfo.setDwlsgx(list.get(13));
                //组织性质类别
                Map map = generalSqlComponent.query(SqlCode.getZzlbCode2,new Object[]{list.get(14)});
                tDwInfo.setDwxzlb(map!=null ? map.get("item_code").toString() : "");
                tDwInfo.setDwjldjczzqk(list.get(15));
                tDwInfo.setIsfrdw("是".equals(list.get(16).toString())?"1":"0");
                tDwInfo.setDwfzr(list.get(17));
                tDwInfo.setFzrsfsdy("是".equals(list.get(18).toString())?"1":"0");
                tDwInfo.setDwcode(list.get(19));
                tDwInfo.setJjlx(list.get(20));
                tDwInfo.setQygm(list.get(21));
                tDwInfo.setSfmykjqy("是".equals(list.get(22).toString())?"1":"0");
                tDwInfo.setZgzgs(list.get(25));
                tDwInfo.setZgzgzgrs(list.get(26));
                tDwInfo.setZgzyjsrys(list.get(27));
                tDwInfo.setSfjygh("是".equals(list.get(28).toString())?"1":"0");
                tDwInfo.setSfzjzz("是".equals(list.get(28).toString())?"1":"0");
                generalSqlComponent.update(SqlCode.updateTDwInfo, tDwInfo);
                generalSqlComponent.update(SqlCode.updateTDwInfoBc, tDwInfo);
            }
        }



        return stringBuffer.toString();
    }

    /**
     * 保存的党组织到系统组织机构中
     * @param list
     * @param session
     */
    @Transactional(rollbackFor = Exception.class)
    public Long saveUrcOrg2(List<String> list,Session session) {
        Map<String, Object> map = new HashMap<>();
        map.put("organcode", list.get(32));
        Map<String, Object> retMap = null;
        if(!"0".equals(list.get(32))){
            retMap = generalSqlComponent.query(SqlCode.queryPorgcode2, map);
            if(retMap == null) {
                LehandException.throwException("上级党组织编码不存在，导入失败");
                return null;
            }
        }
        map.put("id", list.get(0));
        Map map1 = generalSqlComponent.query(SqlCode.getZzlbCode,new Object[]{list.get(2)});
        map.put("otid", map1!=null ? map1.get("item_code").toString() : "");
        map.put("orgcode", list.get(0));
        //generalSqlComponent.delete(SqlCode.delOrganByOrgancode, map);
//        map.put("orgid", list.get(0).substring(1));
        map.put("orgname", list.get(1));
        map.put("orgdesc", "");
        map.put("seqno", 900);
        map.put("cuserid", "");
        map.put("createtime", DateEnum.parseDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
        map.put("uuserid", "");
        map.put("updatetime", DateEnum.parseDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
        map.put("status", 1);
        map.put("compid", 1);
        map.put("remarks1", 0);
        map.put("remarks2", 100);
        map.put("remarks3", "");
        map.put("remarks4", "");
        map.put("remarks5", "");
        map.put("remarks6", list.get(1));
        map.put("pid", list.get(32));
        Map map2 = generalSqlComponent.query(SqlCode.getOrgan2,new Object[]{list.get(0)});
        if(map2==null){
            generalSqlComponent.insert(SqlCode.saveHornOrganization3, map);
            return Long.valueOf(map.get("id").toString());
        }else{
            generalSqlComponent.update(SqlCode.updateHornOrganization2, map);
            return Long.valueOf(map2.get("id").toString());
        }
    }




}
