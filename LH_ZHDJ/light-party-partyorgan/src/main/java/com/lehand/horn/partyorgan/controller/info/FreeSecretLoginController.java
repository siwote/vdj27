package com.lehand.horn.partyorgan.controller.info;

import com.alibaba.fastjson.JSONObject;
import com.lehand.base.common.Message;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.module.urc.config.SessionConfig;
import com.lehand.module.urc.dto.LoginRequest;
import com.lehand.module.urc.pojo.UserAccountView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


/**
 * @author hj
 */
@RestController
@RequestMapping(value = "/power/freeLogin")
@Api(value = "电建OA免登录", tags = { "电建OA免登录" })
public class FreeSecretLoginController {
    private static final Logger logger = LogManager.getLogger(FreeSecretLoginController.class);

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private SessionConfig sessionConfig;

    @OpenApi(optflag = 4)
    @ApiOperation( value = "OA免登录",notes = "OA免登录",httpMethod = "POST")
    @ApiImplicitParams({ @ApiImplicitParam(name = "param", value = "参数", dataType = "string", paramType = "query", example = "admin")})
    @RequestMapping({"/loginFree"})
    public Message<Object> loginToken(@ApiIgnore LoginRequest req, HttpServletRequest request,String param) {
        Message<Object> msg = new Message(Message.LOGIN);
        logger.info("获取到参数:"+param);
        if(StringUtils.isEmpty(param)){
            msg = msg.fail("OA账户登录失败");
            return msg;
        }

        String desEncryptParam= null;
        String oaaccount = null;
        try {
            //对传进来的param进行解密,获取到oaaccount和timestamp
            desEncryptParam = AESPlus.desEncrypt(param);

            JSONObject jsonObject = JSONObject.parseObject(desEncryptParam);
            oaaccount = jsonObject.getString("oaaccount");
            String timestamp = jsonObject.getString("timeStamp");
            long timeStamp = Long.parseLong(timestamp);
            long currentTime = System.currentTimeMillis();
            long minute =(currentTime - timeStamp)/(1000*60);
            //当前系统时间与解密后获取到的时间戳相减获得的时间大于15分钟,则当前链接失效,登录失败
            if(minute > 15){
                return msg.fail("本次登录超时");
            }
            logger.info("解析后参数: " + jsonObject.toString());
        } catch (Exception e) {
            return msg.fail("OA账户登录失败");
        }
        UserAccountView user = null;
        try {
            //根据oaaccount查询当前账号
//            user =generalSqlComponent.query(SqlCode.getLoginUserByAccount,new Object[]{oaaccount});
            user =generalSqlComponent.query(SqlCode.getLoginUserByOAAccount,new Object[]{oaaccount});

        } catch (Exception e) {
            logger.error("查询用户异常: " + e.getMessage());
            return msg.fail("OA账户登录失败");
        }
        logger.info("获取用户信息: " + JSONObject.toJSONString(user));
        if (null == user) {
            msg = msg.fail("OA账户登录失败");
            return msg;
        }
        //判断账户是否禁用
        if (0 == user.getStatus()) {
            return msg.fail("该用户已被禁用!");
        }
        req.setAccount(user.getAccount());
        Session session = this.sessionConfig.createSession(req, request, user);
        return msg.success().setData(session).setLog(user.getUsername() + "登录成功!");
    }


    @OpenApi(optflag = 4)
    @ApiOperation( value = "账号校验",notes = "账号校验",httpMethod = "POST")
    @ApiImplicitParams({ @ApiImplicitParam(name = "oaaccount", value = "OA账号", dataType = "string", paramType = "query", example = "admin"),
            @ApiImplicitParam(name = "userid", value = "被修改用户userid", dataType = "Long", paramType = "query", example = "123456")})
    @RequestMapping({"/freeLoginValidate"})
    public Message<Object> FreeLoginValidate(String oaaccount,Long userid) {
        Message<Object> msg = new Message(Message.LOGIN);
        if(StringUtils.isEmpty(oaaccount)){
            return msg.fail("账号为空");
        }
        Map<String,Object> OAAccount = new HashMap<>(16);
        try {
            OAAccount = generalSqlComponent.query(SqlCode.getAccountByRemarks3, new Object[]{oaaccount});
            if(OAAccount!=null && !OAAccount.get("userid").toString().equals(userid.toString())){
                return msg.setMessage("账号已绑定");
            }else {
                return msg.setMessage("账号未绑定");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return msg;
    }



    public static final class AESPlus {
        private static final String IV = "lehandoaaccount1";
        private static final String KEY = "lehandoa12345678";

        public AESPlus() {
        }

        public static String encrypt(String data) throws Exception {
            return encrypt(data, "lehandoa12345678", "lehandoaaccount1");
        }

        public static String desEncrypt(String data) throws Exception {
            return desEncrypt(data, "lehandoa12345678", "lehandoaaccount1");
        }

        //加密
        public static String encrypt(String data, String key, String iv) throws Exception {
            try {
                Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
                int blockSize = cipher.getBlockSize();
                byte[] dataBytes = data.getBytes();
                int plaintextLength = dataBytes.length;
                if (plaintextLength % blockSize != 0) {
                    plaintextLength += blockSize - plaintextLength % blockSize;
                }

                byte[] plaintext = new byte[plaintextLength];
                System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
                SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
                IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
                cipher.init(1, keyspec, ivspec);
                byte[] encrypted = cipher.doFinal(plaintext);
                return Base64.encodeBase64String(encrypted);
            } catch (Exception var11) {
                var11.printStackTrace();
                return null;
            }
        }

        //解密
        public static String desEncrypt(String data, String key, String iv) throws Exception {
            try {
                byte[] encrypted1 = Base64.decodeBase64(data);
                Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
                SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
                IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
                cipher.init(2, keyspec, ivspec);
                byte[] original = cipher.doFinal(encrypted1);
                String originalString = new String(original);
                return originalString;
            } catch (Exception var9) {
                var9.printStackTrace();
                return null;
            }
        }
    }

}
