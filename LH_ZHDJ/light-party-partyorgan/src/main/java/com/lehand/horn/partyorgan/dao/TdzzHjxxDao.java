//package com.lehand.horn.partyorgan.dao;
//
//import java.util.List;
//
//import org.springframework.stereotype.Repository;
//
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzHjxxDzz;
//
///**
// * 换届信息
// * @author taogang
// * @version 1.0
// * @date 2019年1月23日, 下午4:59:35
// */
//@Repository
//public class TdzzHjxxDao extends ComBaseDao<TDzzHjxxDzz>{
//
//	/**查询党组织换届信息 .*/
//	private static final String LIST_BY_ID = "select * from t_dzz_hjxx_dzz where organid = ?";
//
//	/**
//	 * 查询换届信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public List<TDzzHjxxDzz> ListHjxx(String organid) {
//		return super.list2bean(LIST_BY_ID, TDzzHjxxDzz.class, organid);
//	}
//
//}
