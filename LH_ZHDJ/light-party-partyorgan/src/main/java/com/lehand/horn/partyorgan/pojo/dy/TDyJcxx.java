package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 党员奖惩信息表
 * @author code maker
 */
@ApiModel(value="党员奖惩信息表",description="党员奖惩信息表")
public class TDyJcxx implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 记录ID
	 */
	@ApiModelProperty(value="记录ID",name="uuid")
	private String uuid;
	
	/**
	 * 人员ID
	 */
	@ApiModelProperty(value="人员ID",name="userid")
	private String userid;
	
	/**
	 * 批准机关id
	 */
	@ApiModelProperty(value="批准机关id",name="jgid")
	private String jgid;
	
	/**
	 * 批准机关级别
	 */
	@ApiModelProperty(value="批准机关级别",name="pzjgjb")
	private String pzjgjb;
	
	/**
	 * 奖惩批准机关
	 */
	@ApiModelProperty(value="奖惩批准机关",name="jcpzjg")
	private String jcpzjg;
	
	/**
	 * 奖惩批准日期
	 */
	@ApiModelProperty(value="奖惩批准日期",name="pzrq")
	private String pzrq;
	
	/**
	 * 奖惩撤销日期
	 */
	@ApiModelProperty(value="奖惩撤销日期",name="cxrq")
	private String cxrq;
	
	/**
	 * 奖惩代码
	 */
	@ApiModelProperty(value="奖惩代码",name="jcmc")
	private String jcmc;
	
	/**
	 * 奖惩原因
	 */
	@ApiModelProperty(value="奖惩原因",name="jcyy")
	private String jcyy;
	
	/**
	 * 奖惩说明
	 */
	@ApiModelProperty(value="奖惩说明",name="jcsm")
	private String jcsm;
	
	/**
	 * 操作人
	 */
	@ApiModelProperty(value="操作人",name="operator")
	private String operator;
	
	/**
	 * 操作时间
	 */
	@ApiModelProperty(value="操作时间",name="operatetime")
	private String operatetime;
	
	/**
	 * 数据来源
	 */
	@ApiModelProperty(value="数据来源",name="sjly")
	private String sjly;
	
	/**
	 * 奖惩决定原始文件
	 */
	@ApiModelProperty(value="奖惩决定原始文件",name="jcqdyswj")
	private String jcqdyswj;
	
	/**
	 * A本地新增,E 本地修改,D 本地删除,Q 同步
	 */
	@ApiModelProperty(value="A本地新增,E 本地修改,D 本地删除,Q 同步",name="syncstatus")
	private String syncstatus;
	
	/**
	 * 同步的时间
	 */
	@ApiModelProperty(value="同步的时间",name="synctime")
	private String synctime;
	
	/**
	 * 作废标志||1是0否
	 */
	@ApiModelProperty(value="作废标志||1是0否",name="zfbz")
	private String zfbz;
	
	/**
	 * 奖惩标记 1：奖 2：惩
	 */
	@ApiModelProperty(value="奖惩标记 1：奖 2：惩",name="rewardspunishments")
	private Integer rewardspunishments;
	

    /**
     * setter for uuid
     * @param uuid
     */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

    /**
     * getter for uuid
     */
	public String getUuid() {
		return uuid;
	}

    /**
     * setter for userid
     * @param userid
     */
	public void setUserid(String userid) {
		this.userid = userid;
	}

    /**
     * getter for userid
     */
	public String getUserid() {
		return userid;
	}

    /**
     * setter for jgid
     * @param jgid
     */
	public void setJgid(String jgid) {
		this.jgid = jgid;
	}

    /**
     * getter for jgid
     */
	public String getJgid() {
		return jgid;
	}

    /**
     * setter for pzjgjb
     * @param pzjgjb
     */
	public void setPzjgjb(String pzjgjb) {
		this.pzjgjb = pzjgjb;
	}

    /**
     * getter for pzjgjb
     */
	public String getPzjgjb() {
		return pzjgjb;
	}

    /**
     * setter for jcpzjg
     * @param jcpzjg
     */
	public void setJcpzjg(String jcpzjg) {
		this.jcpzjg = jcpzjg;
	}

    /**
     * getter for jcpzjg
     */
	public String getJcpzjg() {
		return jcpzjg;
	}

    /**
     * setter for pzrq
     * @param pzrq
     */
	public void setPzrq(String pzrq) {
		this.pzrq = pzrq;
	}

    /**
     * getter for pzrq
     */
	public String getPzrq() {
		return pzrq;
	}

    /**
     * setter for cxrq
     * @param cxrq
     */
	public void setCxrq(String cxrq) {
		this.cxrq = cxrq;
	}

    /**
     * getter for cxrq
     */
	public String getCxrq() {
		return cxrq;
	}

    /**
     * setter for jcmc
     * @param jcmc
     */
	public void setJcmc(String jcmc) {
		this.jcmc = jcmc;
	}

    /**
     * getter for jcmc
     */
	public String getJcmc() {
		return jcmc;
	}

    /**
     * setter for jcyy
     * @param jcyy
     */
	public void setJcyy(String jcyy) {
		this.jcyy = jcyy;
	}

    /**
     * getter for jcyy
     */
	public String getJcyy() {
		return jcyy;
	}

    /**
     * setter for jcsm
     * @param jcsm
     */
	public void setJcsm(String jcsm) {
		this.jcsm = jcsm;
	}

    /**
     * getter for jcsm
     */
	public String getJcsm() {
		return jcsm;
	}

    /**
     * setter for operator
     * @param operator
     */
	public void setOperator(String operator) {
		this.operator = operator;
	}

    /**
     * getter for operator
     */
	public String getOperator() {
		return operator;
	}

    /**
     * setter for operatetime
     * @param operatetime
     */
	public void setOperatetime(String operatetime) {
		this.operatetime = operatetime;
	}

    /**
     * getter for operatetime
     */
	public String getOperatetime() {
		return operatetime;
	}

    /**
     * setter for sjly
     * @param sjly
     */
	public void setSjly(String sjly) {
		this.sjly = sjly;
	}

    /**
     * getter for sjly
     */
	public String getSjly() {
		return sjly;
	}

    /**
     * setter for jcqdyswj
     * @param jcqdyswj
     */
	public void setJcqdyswj(String jcqdyswj) {
		this.jcqdyswj = jcqdyswj;
	}

    /**
     * getter for jcqdyswj
     */
	public String getJcqdyswj() {
		return jcqdyswj;
	}

    /**
     * setter for syncstatus
     * @param syncstatus
     */
	public void setSyncstatus(String syncstatus) {
		this.syncstatus = syncstatus;
	}

    /**
     * getter for syncstatus
     */
	public String getSyncstatus() {
		return syncstatus;
	}

    /**
     * setter for synctime
     * @param synctime
     */
	public void setSynctime(String synctime) {
		this.synctime = synctime;
	}

    /**
     * getter for synctime
     */
	public String getSynctime() {
		return synctime;
	}

    /**
     * setter for zfbz
     * @param zfbz
     */
	public void setZfbz(String zfbz) {
		this.zfbz = zfbz;
	}

    /**
     * getter for zfbz
     */
	public String getZfbz() {
		return zfbz;
	}

    /**
     * setter for rewardspunishments
     * @param rewardspunishments
     */
	public void setRewardspunishments(Integer rewardspunishments) {
		this.rewardspunishments = rewardspunishments;
	}

    /**
     * getter for rewardspunishments
     */
	public Integer getRewardspunishments() {
		return rewardspunishments;
	}

    /**
     * TDyJcxxEntity.toString()
     */
    @Override
    public String toString() {
        return "TDyJcxxEntity{" +
               "uuid='" + uuid + '\'' +
               ", userid='" + userid + '\'' +
               ", jgid='" + jgid + '\'' +
               ", pzjgjb='" + pzjgjb + '\'' +
               ", jcpzjg='" + jcpzjg + '\'' +
               ", pzrq='" + pzrq + '\'' +
               ", cxrq='" + cxrq + '\'' +
               ", jcmc='" + jcmc + '\'' +
               ", jcyy='" + jcyy + '\'' +
               ", jcsm='" + jcsm + '\'' +
               ", operator='" + operator + '\'' +
               ", operatetime='" + operatetime + '\'' +
               ", sjly='" + sjly + '\'' +
               ", jcqdyswj='" + jcqdyswj + '\'' +
               ", syncstatus='" + syncstatus + '\'' +
               ", synctime='" + synctime + '\'' +
               ", zfbz='" + zfbz + '\'' +
               ", rewardspunishments='" + rewardspunishments + '\'' +
               '}';
    }

}
