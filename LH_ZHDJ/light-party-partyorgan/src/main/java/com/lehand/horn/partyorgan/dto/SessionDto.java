package com.lehand.horn.partyorgan.dto;

import com.lehand.base.common.Session;

public class SessionDto extends Session {

    /** t_dzz_info表中的 organid字段 ,党费管理中使用*/
    private String custom;

    /** 组织类别*/
    private String zzlb;

    /** 身份证号*/
    private String idcard;

    /** 职务信息*/
    private String post;

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }


    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
}
