package com.lehand.horn.partyorgan.pojo.dy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * @author code maker
 */
@ApiModel(value="",description="")
public class TDyInfoDzzDj implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	@ApiModelProperty(value="",name="id")
	private Integer id;
	
	/**
	 * t_dy_info表userid
	 */
	@ApiModelProperty(value="t_dy_info表userid",name="userid")
	private String userid;
	
	/**
	 * 入党时所在党支部
	 */
	@ApiModelProperty(value="入党时所在党支部",name="firstdzz")
	private String firstdzz;
	
	/**
	 * 入党介绍人
	 */
	@ApiModelProperty(value="入党介绍人",name="developuser")
	private String developuser;
	
	/**
	 * 转正情况
	 */
	@ApiModelProperty(value="转正情况",name="zzqk")
	private String zzqk;
	
	/**
	 * 党龄
	 */
	@ApiModelProperty(value="党龄",name="partyage")
	private Long partyage;
	
	/**
	 * 党籍校正值
	 */
	@ApiModelProperty(value="党籍校正值",name="correcting")
	private String correcting;
	
	/**
	 * 其他党团
	 */
	@ApiModelProperty(value="其他党团",name="otherpartyorg")
	private String otherpartyorg;
	
	/**
	 * 加入其他党团时间
	 */
	@ApiModelProperty(value="加入其他党团时间",name="joinotherpartyorgdate")
	private Date joinotherpartyorgdate;
	
	/**
	 * 离开其他党团时间
	 */
	@ApiModelProperty(value="离开其他党团时间",name="outotherpartyorgdate")
	private Date outotherpartyorgdate;
	

    /**
     * setter for id
     * @param id
     */
	public void setId(Integer id) {
		this.id = id;
	}

    /**
     * getter for id
     */
	public Integer getId() {
		return id;
	}

    /**
     * setter for userid
     * @param userid
     */
	public void setUserid(String userid) {
		this.userid = userid;
	}

    /**
     * getter for userid
     */
	public String getUserid() {
		return userid;
	}

    /**
     * setter for firstdzz
     * @param firstdzz
     */
	public void setFirstdzz(String firstdzz) {
		this.firstdzz = firstdzz;
	}

    /**
     * getter for firstdzz
     */
	public String getFirstdzz() {
		return firstdzz;
	}

    /**
     * setter for developuser
     * @param developuser
     */
	public void setDevelopuser(String developuser) {
		this.developuser = developuser;
	}

    /**
     * getter for developuser
     */
	public String getDevelopuser() {
		return developuser;
	}

    /**
     * setter for zzqk
     * @param zzqk
     */
	public void setZzqk(String zzqk) {
		this.zzqk = zzqk;
	}

    /**
     * getter for zzqk
     */
	public String getZzqk() {
		return zzqk;
	}

    /**
     * setter for partyage
     * @param partyage
     */
	public void setPartyage(Long partyage) {
		this.partyage = partyage;
	}

    /**
     * getter for partyage
     */
	public Long getPartyage() {
		return partyage;
	}

    /**
     * setter for correcting
     * @param correcting
     */
	public void setCorrecting(String correcting) {
		this.correcting = correcting;
	}

    /**
     * getter for correcting
     */
	public String getCorrecting() {
		return correcting;
	}

    /**
     * setter for otherpartyorg
     * @param otherpartyorg
     */
	public void setOtherpartyorg(String otherpartyorg) {
		this.otherpartyorg = otherpartyorg;
	}

    /**
     * getter for otherpartyorg
     */
	public String getOtherpartyorg() {
		return otherpartyorg;
	}

    /**
     * setter for joinotherpartyorgdate
     * @param joinotherpartyorgdate
     */
	public void setJoinotherpartyorgdate(Date joinotherpartyorgdate) {
		this.joinotherpartyorgdate = joinotherpartyorgdate;
	}

    /**
     * getter for joinotherpartyorgdate
     */
	public Date getJoinotherpartyorgdate() {
		return joinotherpartyorgdate;
	}

    /**
     * setter for outotherpartyorgdate
     * @param outotherpartyorgdate
     */
	public void setOutotherpartyorgdate(Date outotherpartyorgdate) {
		this.outotherpartyorgdate = outotherpartyorgdate;
	}

    /**
     * getter for outotherpartyorgdate
     */
	public Date getOutotherpartyorgdate() {
		return outotherpartyorgdate;
	}

    /**
     * TDyInfoDzzDjEntity.toString()
     */
    @Override
    public String toString() {
        return "TDyInfoDzzDjEntity{" +
               "id='" + id + '\'' +
               ", userid='" + userid + '\'' +
               ", firstdzz='" + firstdzz + '\'' +
               ", developuser='" + developuser + '\'' +
               ", zzqk='" + zzqk + '\'' +
               ", partyage='" + partyage + '\'' +
               ", correcting='" + correcting + '\'' +
               ", otherpartyorg='" + otherpartyorg + '\'' +
               ", joinotherpartyorgdate='" + joinotherpartyorgdate + '\'' +
               ", outotherpartyorgdate='" + outotherpartyorgdate + '\'' +
               '}';
    }

}
