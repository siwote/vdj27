package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dzz.TDzzAdvanced;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Description 先进基层党组织
 * @Author lx
 * @Date 2021/1/26 14:20
 **/
@ApiModel(value = "先进基层党组织扩展", description = "先进基层党组织扩展")
public class TDzzAdvancedReq extends TDzzAdvanced {

    @ApiModelProperty(value="附件", name="meetingAttachList")
    private List<Map<String,Object>> attachList;

    @ApiModelProperty(value="导出列名数据",name="exportColumns")
    private List<ExportColumnReq> exportColumns;

    @ApiModelProperty(value="开始年份",name="startYear")
    private String startYear;

    @ApiModelProperty(value="结束年份",name="endYear")
    private String endYear;

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public List<ExportColumnReq> getExportColumns() {
        return exportColumns;
    }

    public void setExportColumns(List<ExportColumnReq> exportColumns) {
        this.exportColumns = exportColumns;
    }
    public List<Map<String, Object>> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<Map<String, Object>> attachList) {
        this.attachList = attachList;
    }
}