package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.partygroup.PartyGroup;
import com.lehand.horn.partyorgan.pojo.partygroup.PartyGroupMember;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value="党小组信息表", description="党小组信息表")
public class PartyGroupDto extends PartyGroup {

    @ApiModelProperty(value="成员列表", name="partyGroupMemberList")
    private List<PartyGroupMember> partyGroupMemberList;

    public List<PartyGroupMember> getPartyGroupMemberList() {
        return partyGroupMemberList;
    }

    public void setPartyGroupMemberList(List<PartyGroupMember> partyGroupMemberList) {
        this.partyGroupMemberList = partyGroupMemberList;
    }
}
