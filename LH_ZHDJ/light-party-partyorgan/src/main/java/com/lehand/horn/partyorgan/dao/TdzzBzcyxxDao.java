//package com.lehand.horn.partyorgan.dao;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.stereotype.Repository;
//
//import com.lehand.horn.partyorgan.pojo.dzz.TDzzBzcyxx;
//
//
//
///**
// * 班子成员信息
// * @author taogang
// * @version 1.0
// * @date 2019年1月23日, 下午4:59:20
// */
//@Repository
//public class TdzzBzcyxxDao extends ComBaseDao<TDzzBzcyxx>{
//	/** 查询班子成员 .*/
//	private static final String LIST_BY_ID = "  select s.uuid,s.organid,s.userid,s.rzdate, s.username xm,zjhm,s.xb,s.csrq,s.xl,s.dnzwname,s.dnzwsm," +
//			"	  s.zwlevel,s.lzdate,s.ordernum from t_dzz_bzcyxx s" +
//			"	 inner join (select max(rzdate) rzdate,userid,organid from t_dzz_bzcyxx where organid = ? group by userid ) t" +
//			"	 on  s.rzdate = t.rzdate and s.userid = t.userid and s.organid = t.organid" +
//			"	 where  s.zfbz  = 0  order by ordernum asc ";
//
//
//	/**
//	 * 查询班子成员信息
//	 * @param organid
//	 * @return
//	 * @author taogang
//	 */
//	public List<Map<String, Object>> ListBzcyxx(String organid){
//		return super.list2map(LIST_BY_ID, organid);
//	}
//}
