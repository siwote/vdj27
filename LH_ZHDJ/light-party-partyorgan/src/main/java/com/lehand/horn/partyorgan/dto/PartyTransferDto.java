package com.lehand.horn.partyorgan.dto;

import com.lehand.horn.partyorgan.pojo.dy.PartyTransfer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author code maker
 */
@ApiModel(value="",description="")
public class PartyTransferDto extends PartyTransfer {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 附件
	 */
	@ApiModelProperty(value="附件",name="files")
	private String files;

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}
}
