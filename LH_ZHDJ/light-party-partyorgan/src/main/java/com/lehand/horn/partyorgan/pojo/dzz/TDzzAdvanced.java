package com.lehand.horn.partyorgan.pojo.dzz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * @Description 先进基层党组织
 * @Author lx
 * @Date 2021/1/26 14:20
 **/
@ApiModel(value = "先进基层党组织", description = "先进基层党组织")
public class TDzzAdvanced implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增长主键", name = "id")
    private Long id;

    @ApiModelProperty(value = "组织机构名称", name = "orgname")
    private String orgname;

    @ApiModelProperty(value = "组织机构编码", name = "orgcode")
    private String orgcode;

    @ApiModelProperty(value = "先进基层党组织类型", name = "type")
    private String type;

    @ApiModelProperty(value = "年份", name = "year")
    private Integer year;

    @ApiModelProperty(value = "创建时间", name = "createtime")
    private String createtime;

    @ApiModelProperty(value = "创建人ID", name = "createuserid")
    private Long createuserid;

    @ApiModelProperty(value = "修改时间", name = "updatetime")
    private String updatetime;

    @ApiModelProperty(value = "修改人ID", name = "updateuserid")
    private Long updateuserid;

    @ApiModelProperty(value = "备注", name = "remark")
    private String remark;

    @ApiModelProperty(value = "附件id,以','拼接", name = "fileid")
    private String fileid;

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Long getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(Long createuserid) {
        this.createuserid = createuserid;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Long getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(Long updateuserid) {
        this.updateuserid = updateuserid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}