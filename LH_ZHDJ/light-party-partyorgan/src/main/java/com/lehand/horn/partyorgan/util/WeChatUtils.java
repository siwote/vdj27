package com.lehand.horn.partyorgan.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.cache.CacheComponent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.URL;
import java.util.List;


public class WeChatUtils {

    private static Log log=LogFactory.getLog(WeChatUtils.class);
    //绑定跳转url

    private static AccessTokenAuth token;
    //消息模本id
    public static String template_id="7klVrv-tzF5n2vLZLwRMvq93pqIIHqzyuBf16-daC3M";

    /**
     * 确认党费跳转页面
     */
    //wch
//    public static String confirm_dues_url = "http://10.1.50.175:8086/h5/pages/confirm/dues?year=YEAR&month=MONTH&actual=ACTUAL&idno=IDNO&orgid=ORGID&name=NAME&basefee=BASEFEE&rate=RATE&payable=PAYABLE";
    //测试环境
//    public static String confirm_dues_url = "http://192.168.60.234:16799/h5/pages/confirm/dues?year=YEAR&month=MONTH&actual=ACTUAL&idno=IDNO&orgid=ORGID&name=NAME&basefee=BASEFEE&rate=RATE&payable=PAYABLE";
    //正式环境
    public static String confirm_dues_url = "http://dj.jac.com.cn/h5/pages/confirm/dues?year=YEAR&month=MONTH&actual=ACTUAL&idno=IDNO&orgid=ORGID&name=NAME&basefee=BASEFEE&rate=RATE&payable=PAYABLE";
    //lx
    private static String appid = "wx6e95e9238dce7688";
    private static String appsecret = "30b0615ad1e3336b00d9b2ab063b3399";
    //正式公众号
//    private static String appid = "wxc0b258c1a83c8a22";
//    private static String appsecret = "a0f4bf9eaccd973937cb6d7ef76214d8";


    //获取access_token
    public static String access_token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
    //刷新access_token
    public static String refresh_token = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN";
    //发送党费(带模板id)
    public static String send_dues_message_template = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
    //发送党费
    public static String send_dues_message = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
    @Resource
    private static CacheComponent cacheComponent;

    /**
     * 获取凭证
     * @return

     */
    public static String getToken() {
        if (token==null) {
            token = new AccessTokenAuth();
            String requestUrl = access_token_url.replace("APPID", appid).replace("APPSECRET", appsecret);
            JSONObject jsonObject = httpRequest(requestUrl, "GET", null);
            // 如果请求成功
            if (null != jsonObject) {
                try {
                    token.setAccess_token(jsonObject.getString("access_token"));
                    token.setExpires_in(jsonObject.getInteger("expires_in"));
                    token.setPulltime(DateEnum.now().getTime()/1000);
                    String jsonStr = JSON.toJSONString(token);
                    if (cacheComponent!=null) {
                        cacheComponent.set(appid,jsonStr);
                    }

                } catch (Exception e) {
                    token = null;
                    // 获取token失败
                    log.info("获取token失败 errcode:{"+ jsonObject.getInteger("errcode") + "} errmsg:{"+ jsonObject.getString("errmsg") + "}");
                    throw new RuntimeException("获取token失败");
                }
            }
        }
        if(token.isDisabled()){
            getRefreshToken(token);
        }
        return token.getAccess_token();
    }


    /**刷新高级接口的token
     * @param token
     */
    public static void getRefreshToken(AccessTokenAuth token){
        String url=refresh_token.replace("APPID", appid).replace("REFRESH_TOKEN", token.getAccess_token());
        JSONObject jsonObject =httpRequest(url, "GET", null);
        if(!jsonObject.containsKey("errcode")){
            token.setAccess_token(jsonObject.getString("access_token"));
            token.setExpires_in(jsonObject.getInteger("expires_in"));
            token.setPulltime(DateEnum.now().getTime()/1000);
            cacheComponent.set(appid,JSON.toJSONString(token));
        }else{
            token.setErrcode(jsonObject.getString("errcode"));
            token.setErrmsg(jsonObject.getString("errmsg"));
        }
    }

    /**发送请求
     * @param requestUrl
     * @param requestMethod
     * @param outputStr
     * @return
     */
    public static JSONObject httpRequest(String requestUrl, String requestMethod, String outputStr) {
        JSONObject jsonObject = null;
        StringBuffer buffer = new StringBuffer();
        try {
            // 创建SSLContext对象，并使用我们指定的信任管理器初始化
            TrustManager[] tm = { new MyX509TrustManager() };
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new java.security.SecureRandom());
            // 从上述SSLContext对象中得到SSLSocketFactory对象
            SSLSocketFactory ssf = sslContext.getSocketFactory();

            // 获取链接
            URL url = new URL(requestUrl);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            // 证书
            conn.setSSLSocketFactory(ssf);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            // 设置请求方式（GET/POST）
            conn.setRequestMethod(requestMethod);
            if ("GET".equalsIgnoreCase(requestMethod)){
                conn.connect();
            }

            // 当有数据需要提交时
            if (null != outputStr && !"".equals(outputStr)) {
                OutputStream outputStream = conn.getOutputStream();
                // 注意编码格式，防止中文乱码
                outputStream.write(outputStr.getBytes("UTF-8"));// 写进去
                outputStream.close();
            }
            log.info("<<<提交请求方式"+requestMethod+",url:"+requestUrl+outputStr!=null?("\r提交数据："+outputStr):"");
            // 将返回的输入流转换成字符串
            InputStream inputStream = conn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(	inputStreamReader);

            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            // 释放资源
            inputStream.close();
            inputStream = null;
            conn.disconnect();
            log.info(">>>>>>>响应数据："+buffer);
            jsonObject = JSONObject.parseObject(buffer.toString());
            log.info(jsonObject.toString());
        } catch (ConnectException ce) {
            log.info("Weixin server connection timed out.");
        } catch (Exception e) {
            // log.error("https request error:{}", e);
        }
        return jsonObject;
    }

}
