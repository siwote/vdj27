package com.lehand.horn.partyorgan.service.info;

import com.aspose.slides.p2cbca448.and;
import com.lehand.base.common.Session;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.UrcOrgan;
import com.lehand.horn.partyorgan.dto.info.NodeDto;
import com.lehand.horn.partyorgan.dto.info.UserDto;
import com.lehand.horn.partyorgan.util.SessionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SelectionService {
    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    SessionUtils partyUtils;

    private static final String FIND_ALL_NODE    =   "SELECT 1 flag,orgid id,b.userid account,orgname dzzqc,sorgname name, orgseqno,1 remarks1,porgid pid,orgtypeid zzlb,a.remarks2 ratio,orgtypeid  icon,orgtypeid zzlb,orgcode  FROM urc_organization a left join urc_user b on b.idno = a.orgcode";

    private final static String listOrganRootNodeSql = FIND_ALL_NODE +" WHERE a.compid=? and a.porgid=? and a.status=1 order by orgseqno  asc,orgcode asc";



    //以下3个是小程序接口需要的参数
    private boolean flag = false;

    private static List<UrcOrgan> childPwOrgan=new ArrayList<UrcOrgan>();

    private boolean flagUser = false;

    public DBComponent db() {
        return generalSqlComponent.getDbComponent();
    }

    /**
     * 获取组织机构
     * @param session
     * @return
     */
    public Map<String, Object> listOrganChildNodeOne(Session session) {
        Long compid = session.getCompid();
        Long organid = session.getCurrorganid();

        return db().getMap(
                FIND_ALL_NODE+"  a left join t_dzz_info b on a.orgcode = b.organcode WHERE a.compid = ? and a.orgid = ? and a.status=1 order by a.orgseqno  asc,a.orgcode asc",
                new Object[] {compid,organid});
    }


    /**
     * 成员
     * 获取指定节点下的所有子节点(含用户、组织)
     * @return
     */
    public List<UserDto> listAllChildNode(Long compid, String username) {

        String sql = "SELECT username xm,username subjectname,username ,userid id, userid , userid subjectid FROM urc_user " +
                "where accflag=0 and userid!=1 ";

        String userids = db().getSingleColumn("SELECT group_concat(userid) userids FROM urc_user  " +
                "where compid=? and accflag=0 and status=1 and usertype=0", String.class, new Object[] {compid});
        Map<String,Object> param = new HashMap<String,Object>(2);
        param.put("id", userids);

        if(StringUtils.isEmpty(username)){
            sql += " and userid in ("+userids+")";
            return db().listBean(sql, UserDto.class,"");
        }
        param.put("username", username);
        return db().listBean(SqlCode.getUrcUsersList, UserDto.class,param);
    }

    /**
    * @Description 获取所有组织用户
    * @Author zwd
    * @Date 2020/11/23 9:15
    * @param 
    * @return 
    **/
    public List<UserDto> listAllChildNode2(Session session, String username,String orgid) {

        Map<String,Object> param = new HashMap<String,Object>(3);
        param.put("compid", session.getCompid());
        param.put("orgid", orgid);
        param.put("username", username);
        List<UserDto> list = generalSqlComponent.query(SqlCode.getUrcUsersList,param);
        return list;
    }

    public List<Map<String, Object>> listRoleMap(Session session) {

        Long compid = session.getCompid();
//        return db().listMap(
//                "select a.remarks2 type, c.id, c.username from pw_role_map a " +
//                        "left join pw_user_organ_map d on a.subjectid=d.userid and a.compid=d.compid " +
//                        "left join pw_role b on a.roleid=b.id and a.compid=b.compid left join " +
//                        "pw_user c on a.subjectid=c.id and a.compid=c.compid where a.roleid in (select id from pw_role" +
//                        " where compid=? and remarks2 in (1,2)) and a.compid=? and d.organid=?",
//                new Object[]{compid, compid,session.getCurrorganid()});
        return db().listMap("select a.sjid id,c.username, b.remarks2 type from urc_role_map a" +
                " inner join urc_role b on a.roleid = b.roleid  and a.compid = b.compid and b.remarks2 in(1,2) " +
                "left join urc_user c on a.sjid = c.userid and a.compid = b.compid " +
                "left join urc_user_org_map d on a.compid =d.compid and a.sjid = d.userid " +
                " where a.compid = ? and d.orgid = ?",
                new Object[]{compid,session.getCurrentOrg().getOrgid()});
    }


    /**
     * 获取指定节点下的所有子节点(含组织，不含用户)
     * type: 当前人员的节点 my， level 1
     * @param orgid
     * @return
     */
    public List<NodeDto> listChildNodeByTypehildNodeByType(Session session, Long orgid, String organname, String type, String level) {
        if("my".equals(type)&&!"admin".equals(session.getLoginAccount())){
            if(orgid==0 && !CollectionUtils.isEmpty(session.getOrgs())&& StringUtils.isEmpty(organname)){
                return db().listBean(FIND_ALL_NODE +" WHERE a.compid=? and a.orgid=? and a.status=1 order by orgseqno  asc,orgcode asc",
                        NodeDto.class,new Object[]{session.getCompid(), session.getCurrentOrg().getOrgid()});
            }else{
                return listCurrentChildNode(session,organname,level);
            }
        }else{
            return listOrganChildNode(session.getCompid(),orgid,organname);
        }
    }


    private List<NodeDto> listCurrentChildNode(Session session,String organname,String level){
        Long compid = session.getCompid();
        Long myOrgid = session.getCurrentOrg().getOrgid();
        if (!StringUtils.isEmpty(organname)) {
            StringBuffer orgids = new StringBuffer();
            orgids.append(myOrgid);
            if(com.alibaba.druid.util.StringUtils.equals(level,"1")){
                List<NodeDto> childNodes = db().listBean(listOrganRootNodeSql, NodeDto.class, new Object[]{compid, myOrgid});
                for (NodeDto childNode : childNodes) {
                    orgids.append(",").append(childNode.getId());
                }
            }else{
                List<UrcOrgan> childOrgans = getChildOrgan(compid, myOrgid);
                for (UrcOrgan organ : childOrgans) {
                    if(organ.getOrgid()!=null){
                        orgids.append(",").append(organ.getOrgid());
                    }
                }
            }

//            String Sql = FIND_ALL_NODE +" WHERE status=1 and compid=:compid and orgid in (:id) ";
            String Sql = FIND_ALL_NODE +" WHERE a.status=1 and a.compid=:compid and FIND_IN_SET(a.orgid,:id) ";

            HashMap<String, Object> params = new HashMap<String,Object>();
            params.put("compid",compid);
            params.put("id", orgids);
            if(!StringUtils.isEmpty(organname)) {
                Sql += "and sorgname like '%"+organname+"%'  order by orgseqno  asc,orgcode asc";
            };

            List<NodeDto> nodes = db().listBean(Sql,NodeDto.class,params);
            if(com.alibaba.druid.util.StringUtils.equals(level,"1")){
                for (NodeDto node : nodes) {
                    node.setOnelevel("0");
                }
            }
            return nodes;

        }
        String rootCode = partyUtils.getRootOrgCode();
        Long rootOrgId = partyUtils.getUrcOrgByOrgancode(rootCode,compid).getOrgid();
        if (rootOrgId.equals(myOrgid)) {
            return  db().listBean(listOrganRootNodeSql, NodeDto.class, new Object[]{compid, myOrgid});
        }
        return db().listBean(listOrganRootNodeSql, NodeDto.class, new Object[]{compid, myOrgid});
    }


    private List<UrcOrgan> getChildOrgan(Long compid, Long myOrgid) {
        //-- 首先通过id查找第一级子节点，找到后将结果add到结果集中
        List<UrcOrgan> list = db().listBean("select * from urc_organization where compid=? and status=1 and  porgid = ?", UrcOrgan.class, new Object[]{compid, myOrgid});;
        //-- 然后将第一级子节点的id变成以逗号隔开的字符串
        String ids =db().getSingleColumn("select group_concat(orgid) id from urc_organization where compid=? and status=1 and porgid = ?",String.class, new Object[]{compid, myOrgid});
        if(list != null && list.size()>0) {
            childPwOrgan = list;
            flag = true;
            data(compid,flag,ids);
        }
        return childPwOrgan;
    }

    private void data(Long compid,boolean flag,String ids){
        if (flag) {
            //-- 其次将上一步得到的字符串进行下一步查询
            List<UrcOrgan> list2 = db().listBean("select*  from urc_organization where compid=? and status=1 and porgid in (?)",UrcOrgan.class, new Object[]{compid, ids});
            String ids2 = db().getSingleColumn("select group_concat(orgid) id from urc_organization where compid=? and status=1 and porgid in (?)",String.class, new Object[]{compid, ids});
            if(list2 != null && list2.size()>0) {
                for (UrcOrgan organ : list2) {
                    childPwOrgan.add(organ);
                }
                data(compid,flag,ids2);
            }else {
                flag=false;
            }
        }
    }

    /**
     *
     * @param compid
     * @param orgid
     * @param organname
     * @return
     */
    public List<NodeDto>   listOrganChildNode(Long compid,Long orgid,String organname){

        String rootCode = partyUtils.getRootOrgCode();
        Long rootOrgId = partyUtils.getUrcOrgByOrgancode(rootCode,compid).getOrgid();

        if(orgid==0) {
            orgid=rootOrgId;
        }
        if (!StringUtils.isEmpty(organname)) {

            StringBuilder sb = new StringBuilder(FIND_ALL_NODE);
            sb.append(" WHERE a.compid=? and a.status=1 and a.orgname like '%" + organname + "%' order by orgseqno asc,orgcode asc");
            return db().listBean(sb.toString(), NodeDto.class, new Object[]{compid});
        }
            return db().listBean(listOrganRootNodeSql, NodeDto.class, new Object[]{compid,orgid});
    }
    /**
     * @param orgid
     * @return
     */
    public List<NodeDto> getNode(Session session, Long orgid) {
       return db().listBean(FIND_ALL_NODE +" WHERE a.compid=? and a.orgid=? and a.status=1 order by orgseqno  asc,orgcode asc",
                        NodeDto.class,new Object[]{session.getCompid(), orgid});
    }
}
