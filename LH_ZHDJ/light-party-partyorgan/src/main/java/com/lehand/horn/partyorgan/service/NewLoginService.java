package com.lehand.horn.partyorgan.service;


import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.cache.CacheComponent;
import com.lehand.horn.partyorgan.constant.Constant;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.horn.partyorgan.dto.SmsSendDto;
import com.lehand.module.common.base.CommonBaseService;
import com.lehand.module.urc.dto.LoginRequest;
import com.lehand.module.urc.pojo.UserAccountView;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.Random;

@Service
public class NewLoginService extends CommonBaseService{

	@Resource
	private CacheComponent cacheComponent;

	private final Long TIME_OUT = Long.valueOf(5*60*1000);
	/**
	 * 生成验证码并发送短信
	 * @param user 登录用户
	 */
	@Transactional(rollbackFor = Exception.class)
    public void createSMS(UserAccountView user){
			try {
				String value = getFourRandom();
				String userid = String.valueOf(user.getUserid());
				//删除之前验证码
				String key = "SMSCode-" + user.getUserid();
				this.cacheComponent.delete(key);
				//设置失效时间
				createCaptcha(userid,value);

				String phone = user.getPhone();
				if (StringUtils.isEmpty(phone)) {
					LehandException.throwException("该账号手机号未设置，请先设置手机号！！！");
				}
				String msg = "【智慧党建】: 您登陆智慧党建平台的短信验证码为："+value+",验证码有效时长为5分钟，请注意保管。";
				SmsSendDto smsSendDto = new SmsSendDto();
				smsSendDto.setCompid(user.getCompid());
				smsSendDto.setUsermark(userid);
				smsSendDto.setPhone(phone);
				smsSendDto.setContent(msg);
				smsSendDto.setBusinessid(Long.valueOf(Constant.SMS_CODE));
				smsSendDto.setStatus(0);
				smsSendDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
				smsSendDto.setDelflag(0);
				//插入短信发送内容
				generalSqlComponent.insert(SqlCode.addSmsSend, smsSendDto);

			}catch (Exception e){
				LehandException.throwException("该账号手机号未设置，请先设置手机号！！！");
			}
		}
	public void checkCaptcha(LoginRequest req,UserAccountView user) {
		if (org.springframework.util.StringUtils.isEmpty(req.getCaptcha())) {
			LehandException.throwException("验证码不能为空!");
		}
		String key = "SMSCode-" + user.getUserid();
		Object v = this.cacheComponent.get(key);
		if (null == v) {
			LehandException.throwException("验证码已失效!");
		}
		if (!req.getCaptcha().toLowerCase().equals(String.valueOf(v).toLowerCase())) {
			LehandException.throwException("验证码错误!");
		}
		//删掉验证码
		this.cacheComponent.delete(key);

	}
	/**
	 *设置失效时间
	 * @param value
	 * @throws IOException
	 */
	public void createCaptcha(String userid,String value) throws IOException {
		String key = "SMSCode-" + userid;
		this.cacheComponent.set(key, value, TIME_OUT);
	}
    /**
	 * 产生4位随机数(0000-9999)
     * @return 4位随机数
     */
    public static String getFourRandom() {
        return StringUtils.leftPad(new Random().nextInt(10000) + "", 4, "0");
    }
}
