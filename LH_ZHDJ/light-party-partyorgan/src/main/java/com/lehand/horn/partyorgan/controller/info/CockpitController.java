package com.lehand.horn.partyorgan.controller.info;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.exception.LehandException;
import com.lehand.components.cache.CacheComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.horn.partyorgan.dao.TSecretaryInfoDao;
import com.lehand.horn.partyorgan.dto.DZZExportReq;
import com.lehand.horn.partyorgan.dto.Node;
import com.lehand.horn.partyorgan.dto.SessionDto;
import com.lehand.horn.partyorgan.dto.TDzzAdvancedReq;
import com.lehand.horn.partyorgan.dto.info.ElectionExportReq;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzAdvanced;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzFunding;
import com.lehand.horn.partyorgan.service.GdictItemService;
import com.lehand.horn.partyorgan.service.dzz.BaseDzzTotalService;
import com.lehand.horn.partyorgan.service.dzz.CockpitService;
import com.lehand.horn.partyorgan.service.dzz.PilotPlanService;
import com.lehand.horn.partyorgan.service.info.HornOrganizationService;
import com.lehand.horn.partyorgan.service.info.HornOrganizationService2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 驾驶舱控制层
 * @author admin
 *
 */
@Api(value = "驾驶舱", tags = { "驾驶舱" })
@RestController
@RequestMapping("/lhdj/cockpit")
public class CockpitController extends BaseController {
    private static final Logger logger = LogManager.getLogger(CockpitController.class);

    @Resource
    private PilotPlanService pilotPlanService;

    @Resource
    private CockpitService cockpitService;
    /**
     * Description: 驾驶舱领航计划统计
     * @author lx
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "驾驶舱领航计划统计", notes = "驾驶舱领航计划统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgcode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/pilotPlanStatistical")
    public Message pilotPlanStatistical(String orgcode) {
        Message message = new Message();
        try {
            message.setData(pilotPlanService.pilotPlanStatistical(orgcode,getSession()));
            message.success();
        } catch (Exception e) {
            LehandException.throwException("驾驶舱领航计划统计"+e);
        }
        return message;
    }

    /**
     * Description: 中心地图统计
     * @author lx
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "中心地图统计", notes = "中心地图统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "szsf", value = "所在省份(不传：省份地图，传参：市区地图)", required = false, paramType = "query", dataType = "String", defaultValue = "")
    })
    @RequestMapping("/mapInfo")
    public Message mapInfo(String szsf) {
        Message message = new Message();
        try {
            message.setData(cockpitService.mapInfo(szsf,getSession()));
            message.success();
        } catch (Exception e) {
            LehandException.throwException("驾驶舱领航计划统计"+e);
        }
        return message;
    }
}
