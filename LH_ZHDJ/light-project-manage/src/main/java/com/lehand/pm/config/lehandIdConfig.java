package com.lehand.pm.config;

import com.lehand.id.core.SnowflakeLongIdGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class lehandIdConfig {

    @Value("${pm.id.dataCenterId}")
    private Long dataCenterId;

    @Value("${pm.id.machineId}")
    private Long machineId;

    @Bean
    public SnowflakeLongIdGenerator snowflakeLongIdGenerator() {
        return new SnowflakeLongIdGenerator(dataCenterId,machineId);
    }

}
