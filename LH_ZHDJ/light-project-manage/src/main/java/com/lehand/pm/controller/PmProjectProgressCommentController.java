package com.lehand.pm.controller;


import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.pm.dto.PmProjectProgressCommentDto;
import com.lehand.pm.service.PmProjectProgressCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author code maker
 */
@Api(value = "项目进度管理评论", tags = {"项目进度管理评论"})
@RestController
@RequestMapping("/horn/pmProjectProgressComment")
public class PmProjectProgressCommentController extends BaseController {

    @Resource
    private PmProjectProgressCommentService pmProjectProgressCommentService;

    /**
     * 分页查询
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message<Pager> page(Pager pager, PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        Message<Pager> message = new Message<Pager>();

        return message.setData(pmProjectProgressCommentService.pagePmProjectProgressComment(getSession(), pmProjectProgressCommentDto, pager)).success();
    }

    /**
     * 列表
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "列表查询", notes = "列表查询", httpMethod = "POST")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Message<List<PmProjectProgressCommentDto>> list(PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        Message<List<PmProjectProgressCommentDto>> message = new Message<List<PmProjectProgressCommentDto>>();
        return message.setData(
                pmProjectProgressCommentService.listPmProjectProgressComment(getSession(), pmProjectProgressCommentDto)).success();
    }


    /**
     * 新增
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message<Long> add(PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        Message<Long> message = new Message<Long>();

        return message.setData(
                pmProjectProgressCommentService.addPmProjectProgressComment(getSession(), pmProjectProgressCommentDto)).success();
    }


    /**
     * 查看详情
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message<PmProjectProgressCommentDto> get(PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        Message<PmProjectProgressCommentDto> message = new Message<PmProjectProgressCommentDto>();
        PmProjectProgressCommentDto result = pmProjectProgressCommentService.getPmProjectProgressCommentById(getSession().getCompid(), pmProjectProgressCommentDto.getId());

        return message.setData(result).success();
    }

    /**
     * 修改
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message<Integer> modify(PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        Message<Integer> message = new Message<Integer>();

        return message.setData(
                pmProjectProgressCommentService.modifyPmProjectProgressComment(getSession(), pmProjectProgressCommentDto)).success();
    }

    /**
     * 根据id删除，真删除
     */
    @OpenApi(optflag = 3)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message<Integer> remove(PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        Message<Integer> message = new Message<Integer>();
        return message.setData(
                pmProjectProgressCommentService.removePmProjectProgressComment(getSession(), pmProjectProgressCommentDto)).success();
    }
}
