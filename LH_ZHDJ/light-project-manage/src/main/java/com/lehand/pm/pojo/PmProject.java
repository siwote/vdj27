package com.lehand.pm.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProject implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
    @ApiModelProperty(value = "唯一标识", name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id", name = "compid")
    private Long compid;

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称", name = "name")
    private String name;

    /**
     * 项目介绍
     */
    @ApiModelProperty(value = "项目介绍", name = "introduction")
    private String introduction;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间 yyyy-MM-dd HH:MM:SS", name = "starttime")
    private String starttime;

    /**
     * 项目联系人名字
     */
    @ApiModelProperty(value = "截止时间 yyyy-MM-dd HH:MM:SS", name = "endtime")
    private String endtime;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "项目创建时间 yyyy-MM-dd HH:MM:SS", name = "createtime")
    private String createtime;


    /**
     *
     */
    @ApiModelProperty(value = "项目联系人名字", name = "contactsname")
    private String contactsname;

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式", name = "contactsphone")
    private String contactsphone;

    /**
     *
     */
    @ApiModelProperty(value = "项目状态 ： 0  草稿  1 已提交  2 已结项", name = "state")
    private Integer state;
    @ApiModelProperty(value = "附件文件id集合  用逗号分隔", name = "attachmentids")
    private String  attachmentids;
    @ApiModelProperty(value = "项目结项结果状态 ： 0  项目待完  1 项目完成", name = "resultstate")
    private  Integer resultstate;
    @ApiModelProperty(value = "结项理由", name = "closereason")
    private String closereason;
    @ApiModelProperty(value = "项目类型 ： 1 公司级项目   2 部门级项目", name = "type")
    private  int type;
    /**
     * 项目状态 ： 0  草稿  1 已提交  2 已结项
     */
    @ApiModelProperty(value = "逻辑删除 ： 0  正常  1 删除", name = "delflag")
    private Integer delflag;

    /**
     * 预留字段1
     */
    @ApiModelProperty(value = "预留字段1", name = "remarks1")
    private Long remarks1;

    /**
     * 预留字段2
     */
    @ApiModelProperty(value = "预留字段2", name = "remarks2")
    private Integer remarks2;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks3")
    private String remarks3;

    /**
     * 预留字段4
     */
    @ApiModelProperty(value = "保存创造项目的账号组织信息", name = "remarks4")
    private String remarks4;

    @ApiModelProperty(value = "附件文件信息", name = "attachList")
    private List<Map<String, Object>> attachList;

    /**
     * setter for id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for id
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for compid
     *
     * @param compid
     */
    public void setCompid(Long compid) {
        this.compid = compid;
    }

    /**
     * getter for compid
     */
    public Long getCompid() {
        return compid;
    }

    /**
     * setter for name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter for name
     */
    public String getName() {
        return name;
    }

    /**
     * setter for introduction
     *
     * @param introduction
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * getter for introduction
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * setter for starttime
     *
     * @param starttime
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     * getter for starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * setter for endtime
     *
     * @param endtime
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     * getter for endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     * setter for contactsname
     *
     * @param contactsname
     */
    public void setContactsname(String contactsname) {
        this.contactsname = contactsname;
    }

    /**
     * getter for contactsname
     */
    public String getContactsname() {
        return contactsname;
    }

    /**
     * setter for contactsphone
     *
     * @param contactsphone
     */
    public void setContactsphone(String contactsphone) {
        this.contactsphone = contactsphone;
    }

    /**
     * getter for contactsphone
     */
    public String getContactsphone() {
        return contactsphone;
    }

    /**
     * setter for state
     *
     * @param state
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * getter for state
     */
    public Integer getState() {
        return state;
    }

    /**
     * setter for delflag
     *
     * @param delflag
     */
    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }

    /**
     * getter for delflag
     */
    public Integer getDelflag() {
        return delflag;
    }

    /**
     * setter for remarks1
     *
     * @param remarks1
     */
    public void setRemarks1(Long remarks1) {
        this.remarks1 = remarks1;
    }

    /**
     * getter for remarks1
     */
    public Long getRemarks1() {
        return remarks1;
    }

    /**
     * setter for remarks2
     *
     * @param remarks2
     */
    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    /**
     * getter for remarks2
     */
    public Integer getRemarks2() {
        return remarks2;
    }

    /**
     * setter for remarks3
     *
     * @param remarks3
     */
    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    /**
     * getter for remarks3
     */
    public String getRemarks3() {
        return remarks3;
    }

    /**
     * setter for remarks4
     *
     * @param remarks4
     */
    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    /**
     * getter for remarks4
     */
    public String getRemarks4() {
        return remarks4;
    }

    public String getAttachmentids() {
        return attachmentids;
    }

    public void setAttachmentids(String attachmentids) {
        this.attachmentids = attachmentids;
    }

    public Integer getResultstate() {
        return resultstate;
    }

    public void setResultstate(Integer resultstate) {
        this.resultstate = resultstate;
    }

    public String getClosereason() {
        return closereason;
    }

    public void setClosereason(String closereason) {
        this.closereason = closereason;
    }

    /**
     * PmProjectEntity.toString()
     */
    @Override
    public String toString() {
        return "PmProjectEntity{" +
                "id='" + id + '\'' +
                ", compid='" + compid + '\'' +
                ", name='" + name + '\'' +
                ", introduction='" + introduction + '\'' +
                ", starttime='" + starttime + '\'' +
                ", endtime='" + endtime + '\'' +
                ", contactsname='" + contactsname + '\'' +
                ", contactsphone='" + contactsphone + '\'' +
                ", state='" + state + '\'' +
                ", delflag='" + delflag + '\'' +
                ", remarks1='" + remarks1 + '\'' +
                ", remarks2='" + remarks2 + '\'' +
                ", remarks3='" + remarks3 + '\'' +
                ", remarks4='" + remarks4 + '\'' +
                '}';
    }

    public List<Map<String, Object>> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<Map<String, Object>> attachList) {
        this.attachList = attachList;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
