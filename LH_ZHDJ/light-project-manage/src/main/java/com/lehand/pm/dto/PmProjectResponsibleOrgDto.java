package com.lehand.pm.dto;

import com.lehand.pm.pojo.PmProjectResponsibleOrg;
import io.swagger.annotations.ApiModel;

/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectResponsibleOrgDto extends PmProjectResponsibleOrg {

    private static final long serialVersionUID = 1L;

}
