package com.lehand.pm.constant;

public class SqlCode {
    /*
    项目添加
     */
    public static final String addPmProject = "addPmProject";
    public static final String delPmProjectById = "delPmProjectById";
    public static final String modifyPmProjectById = "modifyPmProjectById";
    public static final String getPmProjectById = "getPmProjectById";
    public static final String listPmProject = "listPmProject";
    public static final String pagePmProject = "pagePmProject";
    //项目进度汇报评论
    public static final String addPmProjectProgressComment = "addPmProjectProgressComment";
    public static final String delPmProjectProgressCommentById = "delPmProjectProgressCommentById";
    public static final String modifyPmProjectProgressCommentById = "modifyPmProjectProgressCommentById";
    public static final String getPmProjectProgressCommentById = "getPmProjectProgressCommentById";
    public static final String listPmProjectProgressComment = "listPmProjectProgressComment";
    public static final String pagePmProjectProgressComment = "pagePmProjectProgressComment";
    //项目进度汇报
    public static final String addPmProjectProgressReport = "addPmProjectProgressReport";
    public static final String delPmProjectProgressReportById = "delPmProjectProgressReportById";
    public static final String modifyPmProjectProgressReportById = "modifyPmProjectProgressReportById";
    public static final String getPmProjectProgressReportById = "getPmProjectProgressReportById";
    public static final String listPmProjectProgressReport = "listPmProjectProgressReport";
    public static final String pagePmProjectProgressReport = "pagePmProjectProgressReport";

    //项目负责组织
    public static final String addPmProjectResponsibleOrg = "addPmProjectResponsibleOrg";
    public static final String delPmProjectResponsibleOrgById = "delPmProjectResponsibleOrgById";
    public static final String modifyPmProjectResponsibleOrgById = "modifyPmProjectResponsibleOrgById";
    public static final String getPmProjectResponsibleOrgById = "getPmProjectResponsibleOrgById";
    public static final String listPmProjectResponsibleOrg = "listPmProjectResponsibleOrg";
    public static final String pagePmProjectResponsibleOrg = "pagePmProjectResponsibleOrg";
    public static final String listByProjectId = "listByProjectId";


    /**
     * 获取附件
     * select * from dl_file where id=:id and compid=:compid
     */
    public static final String getPrintFile = "getPrintFile";

    /**
     * 根据状态获取所有攻坚项目
     */
    public static String crucialProjectStatistical="crucialProjectStatistical";
    public static String listPmProjectNew="listPmProjectNew";
}
