package com.lehand.pm.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.id.core.SnowflakeLongIdGenerator;
import com.lehand.pm.constant.SqlCode;
import com.lehand.pm.dto.PmProjectProgressCommentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author code maker
 */
@Service("pmProjectProgressCommentService")
public class PmProjectProgressCommentService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;
    @Autowired
    private SnowflakeLongIdGenerator snowflakeLongIdGenerator;
    /**
     * 分页查询
     *
     * @param session
     * @param pmProjectProgressCommentDto
     * @return
     */
    public Pager pagePmProjectProgressComment(Session session, PmProjectProgressCommentDto pmProjectProgressCommentDto, Pager pager) {
        pmProjectProgressCommentDto.setCompid(session.getCompid());
        return generalSqlComponent.pageQuery(SqlCode.pagePmProjectProgressComment, pmProjectProgressCommentDto, pager);
    }

    /**
     * 查询列表
     *
     * @param session
     * @param pmProjectProgressCommentDto
     * @return
     */
    public List<PmProjectProgressCommentDto> listPmProjectProgressComment(Session session, PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        pmProjectProgressCommentDto.setCompid(session.getCompid());
        return generalSqlComponent.query(SqlCode.listPmProjectProgressComment, pmProjectProgressCommentDto);
    }

    /**
     * 根据id查询
     *
     * @param id     主键
     * @param compid 租户id
     */
    public PmProjectProgressCommentDto getPmProjectProgressCommentById(Long compid, Long id) {
        return generalSqlComponent.query(SqlCode.getPmProjectProgressCommentById, new Object[]{compid, id});
    }

    /**
     * 新增
     *
     * @param pmProjectProgressCommentDto
     * @return
     */
    public Long addPmProjectProgressComment(Session session, PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        pmProjectProgressCommentDto.setCompid(session.getCompid());
        if (pmProjectProgressCommentDto.getCompid() == null) {
            LehandException.throwException("新增的对象compid为空。");
        }
        pmProjectProgressCommentDto.setId(snowflakeLongIdGenerator.generate().value());
        pmProjectProgressCommentDto.setCreatetime(DateEnum.parseDate(DateEnum.now(),"yyyy-MM-dd HH:mm:ss"));
        if(session.getCurrentOrg()!=null){
            pmProjectProgressCommentDto.setOrgid(session.getCurrentOrg().getOrgid());
            pmProjectProgressCommentDto.setOrgname(session.getCurrentOrg().getOrgname());
            pmProjectProgressCommentDto.setSorgname(session.getCurrentOrg().getSorgname());
        }
        generalSqlComponent.insert(SqlCode.addPmProjectProgressComment, pmProjectProgressCommentDto);

        return pmProjectProgressCommentDto.getId();
    }

    /**
     * 根据主键修改
     *
     * @param session
     * @param pmProjectProgressCommentDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int modifyPmProjectProgressComment(Session session, PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        pmProjectProgressCommentDto.setCompid(session.getCompid());
        if(session.getCurrentOrg()!=null){
            pmProjectProgressCommentDto.setOrgid(session.getCurrentOrg().getOrgid());
            pmProjectProgressCommentDto.setOrgname(session.getCurrentOrg().getOrgname());
            pmProjectProgressCommentDto.setSorgname(session.getCurrentOrg().getSorgname());
        }
        return generalSqlComponent.update(SqlCode.modifyPmProjectProgressCommentById, pmProjectProgressCommentDto);
    }

    /**
     * 通过主键物理删除
     *
     * @param session
     * @param pmProjectProgressCommentDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int removePmProjectProgressComment(Session session, PmProjectProgressCommentDto pmProjectProgressCommentDto) {
        if (pmProjectProgressCommentDto.getId() == null) {
            LehandException.throwException("id为空，不允许删除所有数据");
        }
        pmProjectProgressCommentDto.setCompid(session.getCompid());
        return generalSqlComponent.delete(SqlCode.delPmProjectProgressCommentById, pmProjectProgressCommentDto);
    }
}
