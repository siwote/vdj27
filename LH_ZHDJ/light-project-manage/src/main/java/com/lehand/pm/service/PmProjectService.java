package com.lehand.pm.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.id.core.SnowflakeLongIdGenerator;
import com.lehand.module.urc.service.OrganizationService;
import com.lehand.pm.constant.SqlCode;
import com.lehand.pm.dto.PmProjectDto;
import com.lehand.pm.dto.PmProjectResponsibleOrgDto;
import com.lehand.pm.utils.ListUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author code maker
 */
@Service("pmProjectService")
public class PmProjectService {
    @Resource
    private SnowflakeLongIdGenerator snowflakeLongIdGenerator;
	@Resource
    private GeneralSqlComponent generalSqlComponent;
    @Autowired
	private PmProjectResponsibleOrgService pmProjectResponsibleOrgService;
    @Autowired
    private FileService fileService;
    @Resource
    private OrganizationService organizationService;
	
	/**
     * 分页查询
     * @param session
	 * @return
	 */
    public Pager pagePmProject(Session session, Pager pager, PmProjectDto pmProjectDto) {
        if(pmProjectDto ==null){
            pmProjectDto=new PmProjectDto();
        }
        pmProjectDto.setCompid(session.getCompid());
        if(session.getCurrentOrg()!=null){
            pmProjectDto.setRemarks4(session.getCurrentOrg().getOrgid()+"");
        }

        return generalSqlComponent.pageQuery(SqlCode.pagePmProject,pmProjectDto,pager);
    }
    
     /**
     * 查询列表
     * @param session
     * @param pmProjectDto
	 * @return
	 */
    public Pager listPmProject(Session session,PmProjectDto pmProjectDto,Pager pager) {
        pmProjectDto.setCompid(session.getCompid());
        return generalSqlComponent.pageQuery(SqlCode.listPmProject,pmProjectDto,pager);
    }

    public List<Long> listPmProjectIds(Session session) {
        return generalSqlComponent.query(SqlCode.listPmProjectNew,new Object[]{session.getCompid(),
                session.getCurrentOrg().getOrgid()});
    }
    
    /**
     * 根据id查询
     * @param id 主键
     * @param compid 租户id
     */
	public PmProjectDto getPmProjectById(Long compid,Long id) {
        PmProjectDto result=  generalSqlComponent.query(SqlCode.getPmProjectById,new Object[]{compid,id});
        if(result !=null){
            List<Map<String, Object>> attachList=fileService.listFile(compid,result.getAttachmentids());
            result.setAttachList(attachList);
        }
        return result;
    }
	
    /**
     * 新增
     * @param pmProjectDto
	 * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer addPmProject(Session session, PmProjectDto pmProjectDto) {
    	pmProjectDto.setCompid(session.getCompid());
        if(pmProjectDto.getCompid() == null){
            LehandException.throwException("新增的对象compid为空。");
        }
//        pmProjectDto.setType(2);
//        if(session.getCurrentOrg()!=null){
//            pmProjectDto.setRemarks4(session.getCurrentOrg().getOrgid()+"");
//            if(session.getCurrentOrg().getOrgpath() !=null && session.getCurrentOrg().getOrgpath().split("_").length==1){
//                pmProjectDto.setType(1);
//            }
//        }
        pmProjectDto.setId(snowflakeLongIdGenerator.generate().value());
        pmProjectDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        pmProjectDto.setDelflag(0);
        pmProjectDto.setRemarks1(session.getUserid());
        pmProjectDto.setRemarks4(session.getCurrentOrg().getOrgid().toString());
        pmProjectDto.setRemarks3(organizationService.getOrg(session.getCurrentOrg().getOrgid(),session.getCompid()).getOrgpath());
        List<PmProjectResponsibleOrgDto> list=pmProjectDto.getRespOrgDtoList();
        if(false ==ListUtil.isEmpty(list)){
            list.stream().forEach(each->each.setProjectid(pmProjectDto.getId()));
            list.stream().forEach(each->pmProjectResponsibleOrgService.addPmProjectResponsibleOrg(session,each));
        }
        return generalSqlComponent.insert(SqlCode.addPmProject,pmProjectDto);
    }

    /**
     * 根据主键修改
     * @param session
     * @param pmProjectDto
	 * @return
	 */
    @Transactional(rollbackFor = Exception.class)
    public int modifyPmProject(Session session ,PmProjectDto pmProjectDto) {
        pmProjectDto.setCompid(session.getCompid());
        List<PmProjectResponsibleOrgDto> list=pmProjectDto.getRespOrgDtoList();
        if(false ==ListUtil.isEmpty(list)){
            list.stream().forEach(each->each.setProjectid(pmProjectDto.getId()));
            list.stream().forEach(each->pmProjectResponsibleOrgService.addPmProjectResponsibleOrg(session,each));
        }
        return generalSqlComponent.update(SqlCode.modifyPmProjectById,pmProjectDto);
    }

    /**
     * 通过主键物理删除
     * @param session
     * @param pmProjectDto
	 * @return
	 */
    @Transactional(rollbackFor = Exception.class)
    public int removePmProject(Session session ,PmProjectDto pmProjectDto) {
        if(pmProjectDto.getId()==null ){
            LehandException.throwException("id为空，不允许删除所有数据");
        }
        pmProjectDto.setCompid(session.getCompid());
        return generalSqlComponent.delete(SqlCode.delPmProjectById,pmProjectDto);
    }

    public List<Map<String, Object>> crucialProjectStatistical(Session session) {
        if(StringUtils.isEmpty(session.getCurrentOrg().getOrgid())){
            LehandException.throwException("session组织id不能为空");
        }
        Long orgid = session.getCurrentOrg().getOrgid();
        //获取urc组织信息
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("SELECT * FROM urc_organization WHERE orgid =? LIMIT 1", new Object[]{orgid});
        if(map==null){
            LehandException.throwException("urc组织不存在");
        }
        String orgpath = String.valueOf(map.get("orgpath"));
        //根据orgpath获取相关组织
        List<Map<String, Object>> list = generalSqlComponent.getDbComponent().listMap("SELECT * FROM urc_organization WHERE orgpath LIKE '%"+orgpath+"%' ", new Object[]{});
        List<String> orgCodeList = new ArrayList<>();
        for (Map<String, Object> m:list) {
            orgCodeList.add(String.valueOf(m.get("orgid")));
        }
        //获取所有组织编码
        Map<String, Object> paramMap = new HashMap<>();
//        paramMap.put("orgid", org.apache.commons.lang.StringUtils.strip(orgCodeList.toString(),"[]"));//统计当前组织及以下
        paramMap.put("orgid", "");//统计所有
        //获取所有攻坚项目
        List<Map<String,Object>> list1 = generalSqlComponent.query(SqlCode.crucialProjectStatistical,paramMap);
        //公司级
        int a1=0;
        int b1=0;
        //事业部级
        int a2=0;
        int b2=0;
        if(list1!=null&&list1.size()>0){
            for (Map<String,Object> m:list1) {
                String type = String.valueOf(m.get("type"));
                String state = String.valueOf(m.get("state"));
                int count = new Long((Long) m.get("count")).intValue();
                if("1".equals(type)){//公司级
                    if("2".equals(state)){//已经结项
                        a1=a1+count;
                    }else{
                        b1=b1+count;
                    }
                }else{//事业部级
                    if("2".equals(state)){//已经结项
                        a2=a2+count;
                    }else{
                        b2=b2+count;
                    }
                }
            }
        }
        List<Map<String,Object>> resultList = new ArrayList<>();
        Map<String, Object> m1 = new HashMap<>();
        m1.put("count",a1+b1);
        m1.put("success",a1);
        m1.put("name","公司级");
        Map<String, Object> m2 = new HashMap<>();
        m2.put("count",a2+b2);
        m2.put("success",a2);
        m2.put("name","事业部级");
        resultList.add(m1);
        resultList.add(m2);

        return resultList;
    }
}
