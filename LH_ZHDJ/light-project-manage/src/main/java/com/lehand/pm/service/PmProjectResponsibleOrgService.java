package com.lehand.pm.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.id.core.SnowflakeLongIdGenerator;
import com.lehand.pm.constant.SqlCode;
import com.lehand.pm.dto.PmProjectResponsibleOrgDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * 
 * @author code maker
 */
@Service("pmProjectResponsibleOrgService")
public class PmProjectResponsibleOrgService {

	@Resource
    private GeneralSqlComponent generalSqlComponent;
	@Resource
    private SnowflakeLongIdGenerator snowflakeLongIdGenerator;
	/**
     * 分页查询
     * @param session
     * @param pmProjectResponsibleOrgDto
	 * @return
	 */
    public Pager pagePmProjectResponsibleOrg(Session session, PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto, Pager pager) {
        pmProjectResponsibleOrgDto.setCompid(session.getCompid());
        return generalSqlComponent.pageQuery(SqlCode.pagePmProjectResponsibleOrg,pmProjectResponsibleOrgDto,pager);
    }
    
     /**
     * 查询列表
     * @param session
     * @param pmProjectResponsibleOrgDto
	 * @return
	 */
    public List<PmProjectResponsibleOrgDto> listPmProjectResponsibleOrg(Session session, PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        pmProjectResponsibleOrgDto.setCompid(session.getCompid());
        return generalSqlComponent.query(SqlCode.listPmProjectResponsibleOrg,pmProjectResponsibleOrgDto);
    }
    
    /**
     * 根据id查询
     * @param id 主键
     * @param compid 租户id
     */
	public PmProjectResponsibleOrgDto getPmProjectResponsibleOrgById(Long compid,Long id) {
        return generalSqlComponent.query(SqlCode.getPmProjectResponsibleOrgById,new Object[]{compid,id});
    }
	
    /**
     * 新增
     * @param pmProjectResponsibleOrgDto
	 * @return
     */
    public Integer addPmProjectResponsibleOrg(Session session, PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
    	pmProjectResponsibleOrgDto.setCompid(session.getCompid());
        if(pmProjectResponsibleOrgDto.getCompid() == null){
            LehandException.throwException("新增的对象compid为空。");
        }
        if(pmProjectResponsibleOrgDto.getId()==null){
            pmProjectResponsibleOrgDto.setId(snowflakeLongIdGenerator.generate().value());
        }
        return generalSqlComponent.insert(SqlCode.addPmProjectResponsibleOrg,pmProjectResponsibleOrgDto);
    }

    /**
     * 根据主键修改
     * @param session
     * @param pmProjectResponsibleOrgDto
	 * @return
	 */
    @Transactional(rollbackFor = Exception.class)
    public int modifyPmProjectResponsibleOrg(Session session ,PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        pmProjectResponsibleOrgDto.setCompid(session.getCompid());
        return generalSqlComponent.update(SqlCode.modifyPmProjectResponsibleOrgById,pmProjectResponsibleOrgDto);
    }

    /**
     * 通过主键物理删除
     * @param session
     * @param pmProjectResponsibleOrgDto
	 * @return
	 */
    @Transactional(rollbackFor = Exception.class)
    public int removePmProjectResponsibleOrg(Session session ,PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        if(pmProjectResponsibleOrgDto.getId()==null ){
            LehandException.throwException("id为空，不允许删除所有数据");
        }
        pmProjectResponsibleOrgDto.setCompid(session.getCompid());
        return generalSqlComponent.delete(SqlCode.delPmProjectResponsibleOrgById,pmProjectResponsibleOrgDto);
    }

    /**
     * 查询列表
     * @param session
     * @param pmProjectResponsibleOrgDto
     * @return
     */
    public List<PmProjectResponsibleOrgDto> listByProjectId(Session session,PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        pmProjectResponsibleOrgDto.setCompid(session.getCompid());
        return generalSqlComponent.query(SqlCode.listByProjectId,pmProjectResponsibleOrgDto);
    }

}
