package com.lehand.pm.dto;

import com.lehand.pm.pojo.PmProjectProgressReport;
import io.swagger.annotations.ApiModel;

/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectProgressReportDto extends PmProjectProgressReport {

    private static final long serialVersionUID = 1L;

}
