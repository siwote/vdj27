package com.lehand.pm.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectProgressComment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 项目进度汇报id
     */
    @ApiModelProperty(value = "项目进度汇报id", name = "projectreportid")
    private Long projectreportid;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id", name = "compid")
    private Long compid;

    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容", name = "comment")
    private String comment;

    /**
     * 评论人组织id
     */
    @ApiModelProperty(value = "评论人组织id", name = "orgid")
    private Long orgid;

    /**
     * 评论人组织全称
     */
    @ApiModelProperty(value = "评论人组织全称", name = "orgname")
    private String orgname;

    /**
     * 组织简称
     */
    @ApiModelProperty(value = "组织简称", name = "sorgname")
    private String sorgname;

    /**
     * 汇报创建时间
     */
    @ApiModelProperty(value = "汇报创建时间", name = "createtime")
    private String createtime;

    /**
     * 预留字段1
     */
    @ApiModelProperty(value = "预留字段1", name = "remarks1")
    private Integer remarks1;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks2")
    private Integer remarks2;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks3")
    private String remarks3;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks4")
    private String remarks4;


    /**
     * setter for id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for id
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for projectreportid
     *
     * @param projectreportid
     */
    public void setProjectreportid(Long projectreportid) {
        this.projectreportid = projectreportid;
    }

    /**
     * getter for projectreportid
     */
    public Long getProjectreportid() {
        return projectreportid;
    }

    /**
     * setter for compid
     *
     * @param compid
     */
    public void setCompid(Long compid) {
        this.compid = compid;
    }

    /**
     * getter for compid
     */
    public Long getCompid() {
        return compid;
    }

    /**
     * setter for comment
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * getter for comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * setter for orgid
     *
     * @param orgid
     */
    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    /**
     * getter for orgid
     */
    public Long getOrgid() {
        return orgid;
    }

    /**
     * setter for orgname
     *
     * @param orgname
     */
    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    /**
     * getter for orgname
     */
    public String getOrgname() {
        return orgname;
    }

    /**
     * setter for sorgname
     *
     * @param sorgname
     */
    public void setSorgname(String sorgname) {
        this.sorgname = sorgname;
    }

    /**
     * getter for sorgname
     */
    public String getSorgname() {
        return sorgname;
    }

    /**
     * setter for createtime
     *
     * @param createtime
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    /**
     * getter for createtime
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * setter for remarks1
     *
     * @param remarks1
     */
    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    /**
     * getter for remarks1
     */
    public Integer getRemarks1() {
        return remarks1;
    }

    /**
     * setter for remarks2
     *
     * @param remarks2
     */
    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    /**
     * getter for remarks2
     */
    public Integer getRemarks2() {
        return remarks2;
    }

    /**
     * setter for remarks3
     *
     * @param remarks3
     */
    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    /**
     * getter for remarks3
     */
    public String getRemarks3() {
        return remarks3;
    }

    /**
     * setter for remarks4
     *
     * @param remarks4
     */
    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    /**
     * getter for remarks4
     */
    public String getRemarks4() {
        return remarks4;
    }

    /**
     * PmProjectProgressCommentEntity.toString()
     */
    @Override
    public String toString() {
        return "PmProjectProgressCommentEntity{" +
                "id='" + id + '\'' +
                ", projectreportid='" + projectreportid + '\'' +
                ", compid='" + compid + '\'' +
                ", comment='" + comment + '\'' +
                ", orgid='" + orgid + '\'' +
                ", orgname='" + orgname + '\'' +
                ", sorgname='" + sorgname + '\'' +
                ", createtime='" + createtime + '\'' +
                ", remarks1='" + remarks1 + '\'' +
                ", remarks2='" + remarks2 + '\'' +
                ", remarks3='" + remarks3 + '\'' +
                ", remarks4='" + remarks4 + '\'' +
                '}';
    }

}
