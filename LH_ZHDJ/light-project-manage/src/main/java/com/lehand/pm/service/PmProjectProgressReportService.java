package com.lehand.pm.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.id.core.SnowflakeLongIdGenerator;
import com.lehand.pm.constant.SqlCode;
import com.lehand.pm.dto.PmProjectProgressReportDto;
import com.lehand.pm.utils.ListUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @author code maker
 */
@Service("pmProjectProgressReportService")
public class PmProjectProgressReportService {

    @Resource
    private GeneralSqlComponent generalSqlComponent;
    @Autowired
    private SnowflakeLongIdGenerator snowflakeLongIdGenerator;
    @Autowired
    private FileService fileService;

    /**
     * 分页查询
     *
     * @param session
     * @param pmProjectProgressReportDto
     * @return
     */
    public Pager pagePmProjectProgressReport(Session session, PmProjectProgressReportDto pmProjectProgressReportDto, Pager pager) {
        pmProjectProgressReportDto.setCompid(session.getCompid());
        return generalSqlComponent.pageQuery(SqlCode.pagePmProjectProgressReport, pmProjectProgressReportDto, pager);
    }

    /**
     * 查询列表
     *
     * @param session
     * @param pmProjectProgressReportDto
     * @return
     */
    public List<PmProjectProgressReportDto> listPmProjectProgressReport(Session session, PmProjectProgressReportDto pmProjectProgressReportDto) {
        pmProjectProgressReportDto.setCompid(session.getCompid());
        List<PmProjectProgressReportDto> list= generalSqlComponent.query(SqlCode.listPmProjectProgressReport, pmProjectProgressReportDto);
        if(false==ListUtil.isEmpty(list)){
            list.stream().forEach((PmProjectProgressReportDto each)->{
                List<Map<String, Object>>   attachList= fileService.listFile(session.getCompid(),each.getAttachmentids());
                each.setAttachList(attachList);
            });
        }
        return list;
    }

    /**
     * 根据id查询
     *
     * @param id     主键
     * @param compid 租户id
     */
    public PmProjectProgressReportDto getPmProjectProgressReportById(Long compid, Long id) {
        PmProjectProgressReportDto pmProjectProgressReportDto=generalSqlComponent.query(SqlCode.getPmProjectProgressReportById, new Object[]{compid, id});
        if(pmProjectProgressReportDto !=null){
            List<Map<String, Object>>   attachList= fileService.listFile(compid,pmProjectProgressReportDto.getAttachmentids());
            pmProjectProgressReportDto.setAttachList(attachList);
        }
        return pmProjectProgressReportDto;
    }

    /**
     * 新增
     *
     * @param pmProjectProgressReportDto
     * @return
     */
    public Long addPmProjectProgressReport(Session session, PmProjectProgressReportDto pmProjectProgressReportDto) {
        pmProjectProgressReportDto.setCompid(session.getCompid());
        if (pmProjectProgressReportDto.getCompid() == null) {
            LehandException.throwException("新增的对象compid为空。");
        }
        pmProjectProgressReportDto.setId(snowflakeLongIdGenerator.generate().value());
        pmProjectProgressReportDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        if(session.getCurrentOrg()!=null){
            pmProjectProgressReportDto.setOrgid(session.getCurrentOrg().getOrgid());
            pmProjectProgressReportDto.setOrgname(session.getCurrentOrg().getOrgname());
            pmProjectProgressReportDto.setSorgname(session.getCurrentOrg().getSorgname());
        }

        generalSqlComponent.insert(SqlCode.addPmProjectProgressReport, pmProjectProgressReportDto);
        return pmProjectProgressReportDto.getId();
    }

    /**
     * 根据主键修改
     *
     * @param session
     * @param pmProjectProgressReportDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int modifyPmProjectProgressReport(Session session, PmProjectProgressReportDto pmProjectProgressReportDto) {
        pmProjectProgressReportDto.setCompid(session.getCompid());

        pmProjectProgressReportDto.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        if(session.getCurrentOrg()!=null){
            pmProjectProgressReportDto.setOrgid(session.getCurrentOrg().getOrgid());
            pmProjectProgressReportDto.setOrgname(session.getCurrentOrg().getOrgname());
            pmProjectProgressReportDto.setSorgname(session.getCurrentOrg().getSorgname());
        }
        return generalSqlComponent.update(SqlCode.modifyPmProjectProgressReportById, pmProjectProgressReportDto);
    }

    /**
     * 通过主键物理删除
     *
     * @param session
     * @param pmProjectProgressReportDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int removePmProjectProgressReport(Session session, PmProjectProgressReportDto pmProjectProgressReportDto) {
        if (pmProjectProgressReportDto.getId() == null) {
            LehandException.throwException("id为空，不允许删除所有数据");
        }
        pmProjectProgressReportDto.setCompid(session.getCompid());
        return generalSqlComponent.delete(SqlCode.delPmProjectProgressReportById, pmProjectProgressReportDto);
    }


}
