package com.lehand.pm.dto;

import com.lehand.pm.pojo.PmProjectProgressComment;
import io.swagger.annotations.ApiModel;

/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectProgressCommentDto extends PmProjectProgressComment {

    private static final long serialVersionUID = 1L;

}
