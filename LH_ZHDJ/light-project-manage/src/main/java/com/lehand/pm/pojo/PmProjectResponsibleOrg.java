package com.lehand.pm.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectResponsibleOrg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 项目id
     */
    @ApiModelProperty(value = "项目id", name = "projectid")
    private Long projectid;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id", name = "compid")
    private Long compid;

    /**
     * 负责组织id
     */
    @ApiModelProperty(value = "负责组织id", name = "orgid")
    private Long orgid;

    /**
     * 负责组织的全称
     */
    @ApiModelProperty(value = "负责组织的全称", name = "orgname")
    private String orgname;

    /**
     * 负责组织简称
     */
    @ApiModelProperty(value = "负责组织简称", name = "sorgname")
    private String sorgname;

    /**
     * 组织路径
     */
    @ApiModelProperty(value = "组织路径", name = "orgpath")
    private String orgpath;

    /**
     * 组织标签
     */
    @ApiModelProperty(value = "组织标签", name = "orglable")
    private String orglable;

    /**
     * 组织编码
     */
    @ApiModelProperty(value = "组织编码", name = "orgcode")
    private String orgcode;

    /**
     * 预留字段1
     */
    @ApiModelProperty(value = "预留字段1", name = "remarks1")
    private Integer remarks1;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks2")
    private Integer remarks2;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks3")
    private String remarks3;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks4")
    private String remarks4;


    /**
     * setter for id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for id
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for projectid
     *
     * @param projectid
     */
    public void setProjectid(Long projectid) {
        this.projectid = projectid;
    }

    /**
     * getter for projectid
     */
    public Long getProjectid() {
        return projectid;
    }

    /**
     * setter for compid
     *
     * @param compid
     */
    public void setCompid(Long compid) {
        this.compid = compid;
    }

    /**
     * getter for compid
     */
    public Long getCompid() {
        return compid;
    }

    /**
     * setter for orgid
     *
     * @param orgid
     */
    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    /**
     * getter for orgid
     */
    public Long getOrgid() {
        return orgid;
    }

    /**
     * setter for orgname
     *
     * @param orgname
     */
    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    /**
     * getter for orgname
     */
    public String getOrgname() {
        return orgname;
    }

    /**
     * setter for sorgname
     *
     * @param sorgname
     */
    public void setSorgname(String sorgname) {
        this.sorgname = sorgname;
    }

    /**
     * getter for sorgname
     */
    public String getSorgname() {
        return sorgname;
    }

    /**
     * setter for orgpath
     *
     * @param orgpath
     */
    public void setOrgpath(String orgpath) {
        this.orgpath = orgpath;
    }

    /**
     * getter for orgpath
     */
    public String getOrgpath() {
        return orgpath;
    }

    /**
     * setter for orglable
     *
     * @param orglable
     */
    public void setOrglable(String orglable) {
        this.orglable = orglable;
    }

    /**
     * getter for orglable
     */
    public String getOrglable() {
        return orglable;
    }

    /**
     * setter for orgcode
     *
     * @param orgcode
     */
    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    /**
     * getter for orgcode
     */
    public String getOrgcode() {
        return orgcode;
    }

    /**
     * setter for remarks1
     *
     * @param remarks1
     */
    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    /**
     * getter for remarks1
     */
    public Integer getRemarks1() {
        return remarks1;
    }

    /**
     * setter for remarks2
     *
     * @param remarks2
     */
    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    /**
     * getter for remarks2
     */
    public Integer getRemarks2() {
        return remarks2;
    }

    /**
     * setter for remarks3
     *
     * @param remarks3
     */
    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    /**
     * getter for remarks3
     */
    public String getRemarks3() {
        return remarks3;
    }

    /**
     * setter for remarks4
     *
     * @param remarks4
     */
    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    /**
     * getter for remarks4
     */
    public String getRemarks4() {
        return remarks4;
    }

    /**
     * PmProjectResponsibleOrgEntity.toString()
     */
    @Override
    public String toString() {
        return "PmProjectResponsibleOrgEntity{" +
                "id='" + id + '\'' +
                ", projectid='" + projectid + '\'' +
                ", compid='" + compid + '\'' +
                ", orgid='" + orgid + '\'' +
                ", orgname='" + orgname + '\'' +
                ", sorgname='" + sorgname + '\'' +
                ", orgpath='" + orgpath + '\'' +
                ", orglable='" + orglable + '\'' +
                ", orgcode='" + orgcode + '\'' +
                ", remarks1='" + remarks1 + '\'' +
                ", remarks2='" + remarks2 + '\'' +
                ", remarks3='" + remarks3 + '\'' +
                ", remarks4='" + remarks4 + '\'' +
                '}';
    }

}
