package com.lehand.pm.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectProgressReport implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ApiModelProperty(value = "项目进度汇报id", name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 项目id
     */
    @ApiModelProperty(value = "项目id", name = "projectid")
    private Long projectid;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id", name = "compid")
    private Long compid;

    /**
     *
     */
    @ApiModelProperty(value = "进度说明", name = "reportcontent")
    private String reportcontent;

    /**
     * 进度汇报组织id
     */
    @ApiModelProperty(value = "进度汇报组织id", name = "orgid")
    private Long orgid;

    /**
     * 组织全称
     */
    @ApiModelProperty(value = "组织全称", name = "orgname")
    private String orgname;

    @ApiModelProperty(value = "附件文件id集合  用逗号分隔", name = "attachmentids")
    private String attachmentids;
    /**
     * 组织简称
     */
    @ApiModelProperty(value = "组织简称", name = "sorgname")
    private String sorgname;

    /**
     * 汇报创建时间
     */
    @ApiModelProperty(value = "汇报创建时间", name = "createtime")
    private String createtime;

    /**
     * 汇报修改时间
     */
    @ApiModelProperty(value = "汇报修改时间", name = "updatetime")
    private String updatetime;

    @ApiModelProperty(value = "附件文件信息", name = "attachList")
    private List<Map<String, Object>> attachList;

    /**
     * 预留字段1
     */
    @ApiModelProperty(value = "预留字段1", name = "remarks1")
    private Integer remarks1;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks2")
    private Integer remarks2;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks3")
    private String remarks3;

    /**
     *
     */
    @ApiModelProperty(value = "", name = "remarks4")
    private String remarks4;


    /**
     * setter for id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for id
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for projectid
     *
     * @param projectid
     */
    public void setProjectid(Long projectid) {
        this.projectid = projectid;
    }

    /**
     * getter for projectid
     */
    public Long getProjectid() {
        return projectid;
    }

    /**
     * setter for compid
     *
     * @param compid
     */
    public void setCompid(Long compid) {
        this.compid = compid;
    }

    /**
     * getter for compid
     */
    public Long getCompid() {
        return compid;
    }

    /**
     * setter for reportcontent
     *
     * @param reportcontent
     */
    public void setReportcontent(String reportcontent) {
        this.reportcontent = reportcontent;
    }

    /**
     * getter for reportcontent
     */
    public String getReportcontent() {
        return reportcontent;
    }

    /**
     * setter for orgid
     *
     * @param orgid
     */
    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    /**
     * getter for orgid
     */
    public Long getOrgid() {
        return orgid;
    }

    /**
     * setter for orgname
     *
     * @param orgname
     */
    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    /**
     * getter for orgname
     */
    public String getOrgname() {
        return orgname;
    }

    /**
     * setter for sorgname
     *
     * @param sorgname
     */
    public void setSorgname(String sorgname) {
        this.sorgname = sorgname;
    }

    /**
     * getter for sorgname
     */
    public String getSorgname() {
        return sorgname;
    }

    /**
     * setter for createtime
     *
     * @param createtime
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    /**
     * getter for createtime
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * setter for updatetime
     *
     * @param updatetime
     */
    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    /**
     * getter for updatetime
     */
    public String getUpdatetime() {
        return updatetime;
    }

    /**
     * setter for remarks1
     *
     * @param remarks1
     */
    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    /**
     * getter for remarks1
     */
    public Integer getRemarks1() {
        return remarks1;
    }

    /**
     * setter for remarks2
     *
     * @param remarks2
     */
    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    /**
     * getter for remarks2
     */
    public Integer getRemarks2() {
        return remarks2;
    }

    /**
     * setter for remarks3
     *
     * @param remarks3
     */
    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    /**
     * getter for remarks3
     */
    public String getRemarks3() {
        return remarks3;
    }

    /**
     * setter for remarks4
     *
     * @param remarks4
     */
    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    /**
     * getter for remarks4
     */
    public String getRemarks4() {
        return remarks4;
    }

    /**
     * PmProjectProgressReportEntity.toString()
     */
    @Override
    public String toString() {
        return "PmProjectProgressReportEntity{" +
                "id='" + id + '\'' +
                ", projectid='" + projectid + '\'' +
                ", compid='" + compid + '\'' +
                ", reportcontent='" + reportcontent + '\'' +
                ", orgid='" + orgid + '\'' +
                ", orgname='" + orgname + '\'' +
                ", sorgname='" + sorgname + '\'' +
                ", createtime='" + createtime + '\'' +
                ", updatetime='" + updatetime + '\'' +
                ", remarks1='" + remarks1 + '\'' +
                ", remarks2='" + remarks2 + '\'' +
                ", remarks3='" + remarks3 + '\'' +
                ", remarks4='" + remarks4 + '\'' +
                '}';
    }

    public String getAttachmentids() {
        return attachmentids;
    }

    public void setAttachmentids(String attachmentids) {
        this.attachmentids = attachmentids;
    }

    public List<Map<String, Object>> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<Map<String, Object>> attachList) {
        this.attachList = attachList;
    }
}
