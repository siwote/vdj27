package com.lehand.pm.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.pm.dto.PmProjectDto;
import com.lehand.pm.dto.PmProjectResponsibleOrgDto;
import com.lehand.pm.service.PmProjectResponsibleOrgService;
import com.lehand.pm.service.PmProjectService;
import com.lehand.pm.utils.ListUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author code maker
 */
@Api(value = "项目进度管理", tags = {"项目进度管理接口"})
@RestController
@RequestMapping("/horn/pmProject")
public class PmProjectController extends BaseController {

    @Autowired
    private PmProjectService pmProjectService;
    @Autowired
    private PmProjectResponsibleOrgService pmProjectResponsibleOrgService;

    /**
     * 分页查询
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "分页查询我的项目", notes = "分页查询我的项目", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
    })
    @RequestMapping(value = "/my", method = RequestMethod.GET)
    public Message<Pager> page(Pager pager, PmProjectDto pmProjectDto) {
        Message<Pager> message = new Message<Pager>();
        return message.setData(pmProjectService.pagePmProject(getSession(),pager,pmProjectDto)).success();
    }

    /**
     * 列表
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "我相关的", notes = "我相关的", httpMethod = "POST")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Message<Pager> list(Pager pager,PmProjectDto pmProjectDto) {
        Message<Pager> message = new Message<Pager>();

        if(pmProjectDto==null){
            pmProjectDto=new PmProjectDto();
        }
//        List<PmProjectDto> result=new ArrayList<>();
//        PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto=new PmProjectResponsibleOrgDto();
//        pmProjectResponsibleOrgDto.setCompid(getSession().getCompid());
//        pmProjectResponsibleOrgDto.setOrgid(getSession().getCurrentOrg().getOrgid());
//        List<PmProjectResponsibleOrgDto> list= pmProjectResponsibleOrgService.listPmProjectResponsibleOrg(getSession(), pmProjectResponsibleOrgDto);
//
//        if(false==ListUtil.isEmpty(list)){
//            List<Long> idList = list.stream().map(PmProjectResponsibleOrgDto::getProjectid).collect(Collectors.toList());
        List<Long> idList = pmProjectService.listPmProjectIds(getSession());
        if(!ListUtil.isEmpty(idList)){
            pmProjectDto.setCompid(getSession().getCompid());
            pmProjectDto.setRemarks1(getSession().getUserid());
            pmProjectDto.setIdList(idList);
            pager= pmProjectService.listPmProject(getSession(),pmProjectDto,pager);
        }
//            result= (List<PmProjectDto>) pager.getRows();
//        }
//        if(false==ListUtil.isEmpty(result)){
//            result.stream().forEach((PmProjectDto each)->{
//                list.stream().forEach((PmProjectResponsibleOrgDto a)->{
//                     if(each.getId().equals(a.getProjectid())){
//                         each.getRespOrgDtoList().add(a);
//                     }
//                });
//            });
//        }
        return message.setData(pager).success();
    }


    /**
     * 新增
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message<Integer> add(@RequestBody PmProjectDto pmProjectDto) {
        Message<Integer> message = new Message<Integer>();
        if (pmProjectDto == null) {
            return message.fail("新增对象不能为空！");
        }
        return message.setData(
                pmProjectService.addPmProject(getSession(), pmProjectDto)).success();
    }


    /**
     * 查看详情
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message<PmProjectDto> get(PmProjectDto pmProjectDto) {
        Message<PmProjectDto> message = new Message<PmProjectDto>();
        PmProjectDto result = pmProjectService.getPmProjectById(getSession().getCompid(), pmProjectDto.getId());
        if(result !=null){
            PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto=new PmProjectResponsibleOrgDto();
            pmProjectResponsibleOrgDto.setProjectid(result.getId());
            List<PmProjectResponsibleOrgDto> list=  pmProjectResponsibleOrgService.listByProjectId(getSession(),pmProjectResponsibleOrgDto);
            result.setRespOrgDtoList(list);
        }
        return message.setData(result).success();
    }

    /**
     * 修改
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message<Integer> modify(@RequestBody PmProjectDto pmProjectDto) {
        Message<Integer> message = new Message<Integer>();
        if(pmProjectDto==null){
            return message.fail("参数不能为空！");
        }
        PmProjectResponsibleOrgDto orgDto=new PmProjectResponsibleOrgDto();
        orgDto.setProjectid(pmProjectDto.getId());
        List<PmProjectResponsibleOrgDto> list =pmProjectResponsibleOrgService.listByProjectId(getSession(),orgDto);
        if(false ==ListUtil.isEmpty(list)){
            list.stream().forEach(each->{
                pmProjectResponsibleOrgService.removePmProjectResponsibleOrg(getSession(),each);
            });
        }
        return message.setData(
                pmProjectService.modifyPmProject(getSession(), pmProjectDto)).success();
    }

    /**
     * 根据id删除，真删除
     */
    @OpenApi(optflag = 3)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message<Integer> remove(PmProjectDto pmProjectDto) {
        Message<Integer> message = new Message<Integer>();

        PmProjectResponsibleOrgDto orgDto=new PmProjectResponsibleOrgDto();
        orgDto.setProjectid(pmProjectDto.getId());
        List<PmProjectResponsibleOrgDto> list =pmProjectResponsibleOrgService.listByProjectId(getSession(),orgDto);

        if(false ==ListUtil.isEmpty(list)){
            list.stream().forEach(each->{
                pmProjectResponsibleOrgService.removePmProjectResponsibleOrg(getSession(),each);
            });
        }
        return message.setData(
                pmProjectService.removePmProject(getSession(), pmProjectDto)).success();
    }
    /**
     * Description: 党组织攻坚项目统计
     * @author lx
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党组织攻坚项目统计", notes = "党组织攻坚项目统计", httpMethod = "POST")
    @RequestMapping("/crucialProjectStatistical")
    public Message crucialProjectStatistical() {
        Message message = new Message();
        try {
            List<Map<String,Object>> list =pmProjectService.crucialProjectStatistical(getSession());
            message.setData(list);
            message.success();
        } catch (Exception e) {
            LehandException.throwException("党组织攻坚项目统计"+e);
        }
        return message;
    }
}
