package com.lehand.pm.dto;

import com.lehand.pm.pojo.PmProject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author code maker
 */
@ApiModel(value = "", description = "")
public class PmProjectDto extends PmProject {

    private static final long serialVersionUID = 1L;
    //项目负责组织
    @ApiModelProperty(value = "查询搜索", name = "queryKey")
    private String queryString;

    @ApiModelProperty(value = "项目负责组织", name = "respOrgDtoList")
    private List<PmProjectResponsibleOrgDto> respOrgDtoList;

    private List<Long> idList;

    @ApiModelProperty(value = "组织编码", name = "orgcode")
    private String orgcode;

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public List<PmProjectResponsibleOrgDto> getRespOrgDtoList() {
        if(respOrgDtoList==null){
            respOrgDtoList=new ArrayList<>();
        }
        return respOrgDtoList;
    }

    public void setRespOrgDtoList(List<PmProjectResponsibleOrgDto> respOrgDtoList) {
        this.respOrgDtoList = respOrgDtoList;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public List<Long> getIdList() {
        return idList;
    }

    public void setIdList(List<Long> idList) {
        this.idList = idList;
    }
}
