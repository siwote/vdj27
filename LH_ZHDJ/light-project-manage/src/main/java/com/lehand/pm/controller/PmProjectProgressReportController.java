package com.lehand.pm.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.pm.dto.PmProjectProgressReportDto;
import com.lehand.pm.service.PmProjectProgressReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author code maker
 */
@Api(value = "项目进度汇报", tags = {"项目进度汇报"})
@RestController
@RequestMapping("/horn/pmProjectProgressReport")
public class PmProjectProgressReportController extends BaseController {

    @Resource
    private PmProjectProgressReportService pmProjectProgressReportService;

    /**
     * 分页查询
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message<Pager> page(Pager pager, PmProjectProgressReportDto pmProjectProgressReportDto) {
        Message<Pager> message = new Message<Pager>();

        return message.setData(pmProjectProgressReportService.pagePmProjectProgressReport(getSession(), pmProjectProgressReportDto, pager)).success();
    }

    /**
     * 列表
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "列表查询", notes = "列表查询", httpMethod = "POST")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Message<List<PmProjectProgressReportDto>> list(PmProjectProgressReportDto pmProjectProgressReportDto) {
        Message<List<PmProjectProgressReportDto>> message = new Message<List<PmProjectProgressReportDto>>();
        return message.setData(
                pmProjectProgressReportService.listPmProjectProgressReport(getSession(), pmProjectProgressReportDto)).success();
    }


    /**
     * 新增
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message<Long> add(PmProjectProgressReportDto pmProjectProgressReportDto) {
        Message<Long> message = new Message<Long>();
        return message.setData(
                pmProjectProgressReportService.addPmProjectProgressReport(getSession(), pmProjectProgressReportDto)).success();
    }


    /**
     * 查看详情
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message<PmProjectProgressReportDto> get(PmProjectProgressReportDto pmProjectProgressReportDto) {
        Message<PmProjectProgressReportDto> message = new Message<PmProjectProgressReportDto>();
        PmProjectProgressReportDto result = pmProjectProgressReportService.getPmProjectProgressReportById(getSession().getCompid(), pmProjectProgressReportDto.getId());

        return message.setData(result).success();
    }

    /**
     * 修改
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message<Integer> modify(PmProjectProgressReportDto pmProjectProgressReportDto) {
        Message<Integer> message = new Message<Integer>();

        return message.setData(
                pmProjectProgressReportService.modifyPmProjectProgressReport(getSession(), pmProjectProgressReportDto)).success();
    }

    /**
     * 根据id删除，真删除
     */
    @OpenApi(optflag = 3)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message<Integer> remove(PmProjectProgressReportDto pmProjectProgressReportDto) {
        Message<Integer> message = new Message<Integer>();
        return message.setData(
                pmProjectProgressReportService.removePmProjectProgressReport(getSession(), pmProjectProgressReportDto)).success();
    }
}
