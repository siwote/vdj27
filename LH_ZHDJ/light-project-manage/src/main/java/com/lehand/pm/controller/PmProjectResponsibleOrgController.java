package com.lehand.pm.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.pm.dto.PmProjectResponsibleOrgDto;
import com.lehand.pm.service.PmProjectResponsibleOrgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author code maker
 */
@Api(value = "项目进度负责组织", tags = {"项目进度负责组织"})
@RestController
@RequestMapping("/horn/pmProjectResponsibleOrg")
public class PmProjectResponsibleOrgController extends BaseController {

    @Resource
    private PmProjectResponsibleOrgService pmProjectResponsibleOrgService;

    /**
     * 分页查询
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Message<Pager> page(Pager pager, PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        Message<Pager> message = new Message<Pager>();

        return message.setData(pmProjectResponsibleOrgService.pagePmProjectResponsibleOrg(getSession(), pmProjectResponsibleOrgDto, pager)).success();
    }

    /**
     * 列表
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "列表查询", notes = "列表查询", httpMethod = "POST")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Message<List<PmProjectResponsibleOrgDto>> list(PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        Message<List<PmProjectResponsibleOrgDto>> message = new Message<List<PmProjectResponsibleOrgDto>>();
        return message.setData(
                pmProjectResponsibleOrgService.listPmProjectResponsibleOrg(getSession(), pmProjectResponsibleOrgDto)).success();
    }


    /**
     * 新增
     */
    @OpenApi(optflag = 1)
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "POST")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Message<Integer> add(PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        Message<Integer> message = new Message<>();

        return message.setData(
                pmProjectResponsibleOrgService.addPmProjectResponsibleOrg(getSession(), pmProjectResponsibleOrgDto)).success();
    }


    /**
     * 查看详情
     */
    @OpenApi(optflag = 0)
    @ApiOperation(value = "查看详情", notes = "查看详情", httpMethod = "POST")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Message<PmProjectResponsibleOrgDto> get(PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        Message<PmProjectResponsibleOrgDto> message = new Message<PmProjectResponsibleOrgDto>();
        PmProjectResponsibleOrgDto result = pmProjectResponsibleOrgService.getPmProjectResponsibleOrgById(getSession().getCompid(), pmProjectResponsibleOrgDto.getId());

        return message.setData(result).success();
    }

    /**
     * 修改
     */
    @OpenApi(optflag = 2)
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public Message<Integer> modify(PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        Message<Integer> message = new Message<Integer>();

        return message.setData(
                pmProjectResponsibleOrgService.modifyPmProjectResponsibleOrg(getSession(), pmProjectResponsibleOrgDto)).success();
    }

    /**
     * 根据id删除，真删除
     */
    @OpenApi(optflag = 3)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Message<Integer> remove(PmProjectResponsibleOrgDto pmProjectResponsibleOrgDto) {
        Message<Integer> message = new Message<Integer>();
        return message.setData(
                pmProjectResponsibleOrgService.removePmProjectResponsibleOrg(getSession(), pmProjectResponsibleOrgDto)).success();
    }
}
