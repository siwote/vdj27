package com.lehand.developing.party.service.fzdy.handle;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.developing.party.common.constant.Constant;
import com.lehand.developing.party.service.DevelopingMesgService;
import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDzzsyxxStep;

/**
 * Click OK to enter the next stage button operation related method implementation
 * @author pantao
 *
 */
@Service
public class FzdyBzxxHandle extends BaseStep {

    @Resource
    private SessionUtils sessionUtils;
	
	//党委code
	@Value("${jk.organ.dangwei}")
	private String dangwei;
	
	//党总支部code
	@Value("${jk.organ.dangzongzhi}")
	private String dangzongzhi;
	
	//入党积极分子党支部审批
	@Resource
	private TFzdyDzzsyxxStep tFzdyDzzsyxxStep;
	
	@Resource
	private DevelopingMesgService developingMesgService;
	
	//person_id 
	//type
    @Override
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		params.put("compid", session.getCompid());
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where type=:type and person_id=:person_id and compid=:compid", params);
		return map;
	}
	
	
	/**
	 * New step information 新增步骤信息
	 * type 下一阶段id
	 * person_id
	 * fatherCode机构标识
	 * currtype 当前阶段id
	 */
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session, String fatherCode, String currtype) {
		params.put("compid", session.getCompid());
		params.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("uneditable", 0);//默认可编辑
		params.put("sfipen", 1);//默认不开启标识
		int num = 0;
		//开始判断上级节点是党委还是党总支
		if(fatherCode.equals(dangwei)) {//是党委
			if ("203".equals(currtype) || "213".equals(currtype) || "402".equals(currtype) || "407".equals(currtype) || "513".equals(currtype)) {
				//新增党总支的步骤信息(这里暂时先不添加党总支的步骤信息，如果要添加就要确保党总支和党委的步骤创建时间不一样)
//				params.put("uneditable", 1);
//				num+=db().insert("INSERT INTO fzdy_bzxx (compid, type, uneditable, person_id, sfipen, createtime,moditime)"  
//						+ "VALUES (:compid,:type,:uneditable,:person_id,:sfipen,:createtime,:moditime)", params);
				
				//找到当前阶段的下二阶段的信息（即党委审批的步骤信息）
				Map<String, Object> map2 = db().getMap("select * from fzdy_type where compid=:compid "
						+ "and id>:type and pid!=0 order by id limit 1", params);
				
				params.put("type", map2.get("id"));
				params.put("uneditable", 0);
				num+=db().insert("INSERT INTO fzdy_bzxx (compid, type, uneditable, person_id, sfipen, createtime)"  
						+ "VALUES (:compid,:type,:uneditable,:person_id,:sfipen,:createtime)", params);
			}else {
				num += db().insert("INSERT INTO fzdy_bzxx (compid, type, uneditable, person_id, sfipen, createtime)"  
						+ "VALUES (:compid,:type,:uneditable,:person_id,:sfipen,:createtime)", params);
			}
		}else {//党总支
			//新增
			 num += db().insert("INSERT INTO fzdy_bzxx (compid, type, uneditable, person_id, sfipen, createtime)"  
					+ "VALUES (:compid,:type,:uneditable,:person_id,:sfipen,:createtime)", params);
		}
		//更新其他步骤为不可编辑
		update1(params, session);
		return num;
	}
	
	
	/**
	 * Update the edit status of other steps to not editable 将其他步骤的编辑状态更新为不可编辑
	 * @param params (type person_id) 
	 * @param session
	 * @return
	 */
	public int update1(Map<String, Object> params, Session session) {
		params.put("uneditable", 1);
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("compid", session.getCompid());
		int num = db().update("UPDATE fzdy_bzxx SET uneditable=:uneditable,  moditime=:moditime WHERE compid=:compid "
				+ "and person_id=:person_id and type!=:type", params);
		return num;
	}
	
	
	/**
	 * Update a stage to an editable state 更新某个阶段为可编辑状态
	 * @param params (type person_id) 
	 * @param session
	 * @return
	 */
	public int update2(Map<String, Object> params, Session session) {
		params.put("uneditable", 0);
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("compid", session.getCompid());
		int num = db().update("UPDATE fzdy_bzxx SET uneditable=:uneditable,  moditime=:moditime WHERE compid=:compid "
				+ "and person_id=:person_id and type=:type", params);
		return num;
	}
	
	/**
	 * Update whether the opening flag of a certain stage is on 更新某个阶段的是否开启标识 为开启
	 * @param params (type person_id) 
	 * @param session
	 * @return
	 */
	public int update3(Map<String, Object> params, Session session) {
		params.put("sfipen", 0);
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("compid", session.getCompid());
		int num = db().update("UPDATE fzdy_bzxx SET sfipen=:sfipen,  moditime=:moditime WHERE compid=:compid "
				+ "and person_id=:person_id and type=:type", params);
		return num;
	}
	
	/**
	 * Click to enter the next stage  点击进入下一阶段
	 * @param params (type person_id) 
	 * @param session
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public int nextStage(Map<String, Object> params, Session session) {
		//当前阶段信息
		Map<String, Object> map = get(params, session);
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("compid", session.getCompid());
		param.put("type", map.get("type"));
		//找到当前阶段的下一阶段的信息
		Map<String, Object> map2 = db().getMap("select * from fzdy_type where compid=:compid "
				+ "and id>:type and pid!=0 and status=1 order by id limit 1", param);
		if(map2==null && "518".equals(map.get("type").toString())) {
		    int num = db().update("UPDATE fzdy_bzxx SET uneditable=1,  moditime=? WHERE compid=? "
					+ "and person_id=? and type=?", new Object[] {DateEnum.YYYYMMDDHHMMDD.format(),session.getCompid(),params.get("person_id"),518});
		   return num;
		}
		//下一阶段的id放入map中
		params.put("type", map2.get("id"));
		//这里要区分是党支部进来的还是审核人进来的
		String fatherCode = "";
		String audit_id = "";
		String audit_name = "";
		int num = 0;
        String orgtypeId = sessionUtils.getOrgTypeByOrganCode(session.getCurrentOrg().getOrgcode());
		if(dangzongzhi.equals(orgtypeId) || dangwei.equals(orgtypeId)) {//即审核人操作
//			fatherCode = session.getOrganCode();
            fatherCode = orgtypeId;
			audit_id = session.getUserid().toString();
			audit_name = session.getUsername();
			//新增下一步的步骤信息
			num = save(params, session, fatherCode, map.get("type").toString());
		}else {//党支部自己点击进入下一阶段
			//当前登录人的父级机构
			Map<String,Object> fatherOrgan = getFatherOrgan(session.getCompid(),session.getCurrorganid());
			//判断是党总支还是党委
//			fatherCode = fatherOrgan.get("otid").toString();
			fatherCode = fatherOrgan.get("orgtypeid").toString();
			//审核人id
			audit_id = fatherOrgan.get("remarks3").toString();
			//审核人名称
			audit_name = fatherOrgan.get("orgname").toString();
			//新增下一步的步骤信息
			num = save(params, session, fatherCode, map.get("type").toString());
			
			/**==============================================上面是插入步骤的信息======下面是特殊步骤后续逻辑处理===============================================*/	
			String person_id =params.get("person_id").toString();
			String code = map.get("type").toString();
			if ("203".equals(code) || "213".equals(code) || "301".equals(code) || "402".equals(code) || "407".equals(code)
					|| "513".equals(code)) {
				Map<String,Object> paramMap = commontParameter(session, code, person_id, fatherCode, audit_id, audit_name);
				
				//这里需要注意的是几个特殊的阶段  
				Map<String,Object> params2 = new HashMap<String,Object>();
				params2.put("compid", session.getCompid());
				params2.put("person_id", params.get("person_id"));
				switch (code) {
				//第5步203：上级党委备案信息（点击确认进入下一步后需要进行插入审核数据操作）
				case "203":
					//插入审核的信息
					commont(paramMap, Constant.ONE, fatherCode, params2, "205", "204", session);
					break;
					//第15步213：上报上级党委备案信息（点击确认进入下一步后需要进行插入审核数据操作）	
				case "213":
					commont(paramMap, Constant.TWO, fatherCode, params2, "215", "214", session);
					break;
					//第18步301：确定入党介绍人（点击确认进入下一步后需要进行插入审核数据操作）	
				case "301":
					if(!dangwei.equals(fatherCode)) {//审核人不是党委
						Map<String,Object> fatherOrgan302 = getFatherOrgan(session.getCompid(),Long.valueOf(fatherOrgan.get("orgid").toString()));
						//判断是党总支还是党委
						fatherCode = fatherOrgan302.get("orgtypeid").toString();
						//审核人id
						audit_id = fatherOrgan302.get("remarks3").toString();
						//审核人名称
						audit_name = fatherOrgan302.get("orgname").toString();
						
						paramMap = commontParameter(session, code, person_id, fatherCode, audit_id, audit_name);
						
					}
					paramMap.put("type", Constant.THREE);//这一步比较特殊必须直接是党委审核
					//审核人信息插入审核表
					generalSqlComponent.insert(SqlCode.insertBzxx, paramMap);
					params2.put("type", "302");
					update2(params2, session);
					update1(params2, session);
					break;
					//第25步402：确定入党介绍人（点击确认进入下一步后需要进行插入审核数据操作）	
				case "402":
					commont(paramMap, Constant.FOUR,fatherCode, params2, "404", "403", session);
					break;
					//第30步407：上报上级党组织审批（点击确认进入下一步后需要进行插入审核数据操作）	
				case "407":
					commont(paramMap, Constant.FIVE, fatherCode, params2, "409", "408", session);
					break;
					//第43步513：上报上级党组织审批（点击确认进入下一步后需要进行插入审核数据操作）	
				case "513":
					commont(paramMap, Constant.SIX, fatherCode, params2, "515", "514", session);
					break;
				default:
					break;
				}
			}
		}
		
		return num;
	}
	
	/**
	 * Interface implementation into a specific stage  进入特定阶段
	 * @param params (node_id,modi_node_id,modi_reason,person_id)
	 * @param session
	 * @return
	 */
	public void enterSpecificStage(Map<String, Object> params, Session session) {
		//点击修改按钮进入特定的节点进行信息的修改（当前节点 node_id,申请修改的节点 modi_node_id,申请原因 modi_reason,人员 person_id）
		//当前登录人的父级机构
		Map<String,Object> fatherOrgan = getFatherOrgan(session.getCompid(),session.getCurrorganid());
		//判断是党总支还是党委
		String fatherCode = fatherOrgan.get("orgtypeid").toString();
		//父级机构编码
		String audit_id = fatherOrgan.get("remarks3").toString();
		//父级机构名称
		String audit_name = fatherOrgan.get("orgname").toString();
		//当前节点id
		String code = params.get("node_id").toString();
		//修改节点id
		String modicode = params.get("modi_node_id").toString();
		//人员id
		String person_id = params.get("person_id").toString();
		
		Map<String, Object> paramMap = commontParameter(session, code, person_id, fatherCode, audit_id, audit_name);
		// 修改阶段名称
		Map<String, Object> fatherStep = getFatherStep(session, modicode);
		String fatherStepName = fatherStep.get("name").toString();
		String fatherStepid = fatherStep.get("id").toString();
		// 修改节点名称
		Map<String, Object> step = getStep(session, modicode);
		String stepName = step.get("name").toString();
		
		paramMap.put("type", Constant.SEVEN);
		paramMap.put("modi_stage_id", fatherStepid);
		paramMap.put("modi_stage_name", fatherStepName);
		paramMap.put("modi_node_id", modicode);
		paramMap.put("modi_node_name", stepName);
		paramMap.put("modi_reason", params.get("modi_reason"));
		
		// 审核人信息插入审核表
		generalSqlComponent.insert(SqlCode.insertBzxx, paramMap);
		
		//创建审核人的待办信息 TODO
		insertMyToDoList(4, Constant.SEVEN,paramMap.get("person_name")+"《"+paramMap.get("stage_name")+"》"+"的审批。", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), paramMap.get("audit_id").toString());
		
		//修改fzdy_bzxx中的uneditable字段为2已申请并且将是否开启字段更新为0开启
		db().update("update fzdy_bzxx set uneditable=2,sfipen=0 where compid=? and person_id=? and type=?", new Object[] {session.getCompid(),person_id,modicode});
	
	}
	
	
	public void insertMyToDoList(int module,String busid,String content,String remind,String optuserid,String userid) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("module", module);
		paramMap.put("busid", busid);
		paramMap.put("content", content);
		paramMap.put("remind", remind);
		paramMap.put("optuserid", optuserid);
		paramMap.put("userid", userid);
		paramMap.put("isread", 0);
		paramMap.put("ishandle", 0);
		generalSqlComponent.getDbComponent().insert("INSERT INTO my_to_do_list (module, busid, content, remind, optuserid, userid, isread, ishandle) VALUES (:module, :busid, :content, :remind, :optuserid, :userid, :isread, :ishandle)", paramMap);
	}
	
	
	/**
	 * Get public parameter method
	 * @param session
	 * @param code
	 * @param person_id
	 * @param fatherCode
	 * @return
	 */
	public Map<String, Object> commontParameter(Session session, String code, String person_id, String fatherCode,
			String audit_id, String audit_name) {
		// 当前阶段名称
		Map<String, Object> fatherStep = getFatherStep(session, code);
		String fatherStepName = fatherStep.get("name").toString();
		String fatherStepid = fatherStep.get("id").toString();

		// 当前节点名称
		Map<String, Object> step = getStep(session, code);
		String stepName = step.get("name").toString();
		String stepId = step.get("id").toString();

		// 当前组织机构
		Map<String, Object> organ = getOrgan(session);
		String orgname = organ.get("orgname").toString();
		String orgid = organ.get("orgid").toString();
		Long compid = session.getCompid();
		// 当前准党员的基本信息
		Map<String, Object> user = getUser(compid,person_id);
		String person_name = user.get("xm").toString();
		String idcard = user.get("zjhm").toString();
		String sex = user.get("xb").toString();//1000000003 男 2000000004女

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("compid", compid);
		paramMap.put("audit_id", audit_id);
		paramMap.put("audit_name", audit_name);
		paramMap.put("audit_organ_code", fatherCode);
		paramMap.put("person_id", person_id);
		paramMap.put("person_name", person_name);
		paramMap.put("person_idcard", idcard);
		paramMap.put("person_sex", sex);
		paramMap.put("stage_id", fatherStepid);
		paramMap.put("stage_name", fatherStepName);
		paramMap.put("node_id", stepId);
		paramMap.put("node_name", stepName);
		paramMap.put("commit_time", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap.put("commit_orgid", orgid);
		paramMap.put("commit_orgname", orgname);
		paramMap.put("status", 0);
		paramMap.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
		
		if(paramMap.get("modi_stage_id")==null) {
			paramMap.put("modi_stage_id", Constant.ENPTY);
		}
		if(paramMap.get("modi_stage_name")==null) {
			paramMap.put("modi_stage_name", Constant.ENPTY);
		}
		if(paramMap.get("modi_node_id")==null) {
			paramMap.put("modi_node_id", Constant.ENPTY);
		}
		if(paramMap.get("modi_node_name")==null) {
			paramMap.put("modi_node_name", Constant.ENPTY);
		}
		if(paramMap.get("modi_reason")==null) {
			paramMap.put("modi_reason", Constant.ENPTY);
		}
		return paramMap;
	}
	
	
	/**
	 * Public use method
	 * @param paramMap
	 * @param typeone
	 * @param fatherCode
	 * @param params2
	 * @param typetwo
	 * @param typethree
	 * @param session
	 */
	public void commont(Map<String, Object> paramMap, String typeone,String fatherCode,
			Map<String, Object> params2, String typetwo, String typethree, Session session) {
		paramMap.put("type", typeone);
		
		// 审核人信息插入审核表
		generalSqlComponent.insert(SqlCode.insertBzxx, paramMap);
		// 判断上级是党委还是党总支
		if (dangwei.equals(fatherCode)) {// 下级是党委
			// 即直接跳到第7步党委的审核页面
			params2.put("type", typetwo);
		} else {// 下级是总党支
				// 即直接跳到第6步总党支的审核页面
			params2.put("type", typethree);
		}
		update2(params2, session);
		update1(params2, session);
		

		//创建审核人的待办信息 TODO
		insertMyToDoList(4, typeone,paramMap.get("person_name")+"《"+paramMap.get("stage_name")+"》"+"的审批。", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), paramMap.get("audit_id").toString());
	}
	
	
	/**
	 * Get the parent organization of the current registrant organization
	 * @return
	 */
	public Map<String, Object> getFatherOrgan(Long compid,Long orgid) {
		return db().getMap(
				"select * from urc_organization  where compid=? and orgid =(SELECT porgid FROM urc_organization where compid=? and orgid=?)",
				new Object[] {compid,compid,orgid});
	}
	
	
	/**
	 * Find the step object by id
	 * @param session
	 * @param type
	 * @return
	 */
	public Map<String,Object> getStep(Session session,String type){
		return db().getMap("select * from fzdy_type  where compid=? and id=?", new Object[] {session.getCompid(),type});
	}
	
	
	/**
	 * Find the parent step object by id
	 * @param session
	 * @param type
	 * @return
	 */
	public Map<String, Object> getFatherStep(Session session, String type) {
		Long compid = session.getCompid();
		return db().getMap(
				"select * from fzdy_type  where compid=? and id= (select pid from fzdy_type  where compid=? and id=?)",
				new Object[] { compid, compid, type });
	}
	
	/**
	 * Get the current registrant organization
	 * @param session
	 * @return
	 */
	public Map<String,Object> getOrgan(Session session){
		Long compid = session.getCompid();
		Long orgid = session.getCurrentOrg().getOrgid();
		return db().getMap("select * from urc_organization where compid=? and orgid=? ", new Object[] {compid,orgid});
	}
	
	/**
	 * Get current party member information
	 * @return
	 */
	public Map<String,Object> getUser(Long compid, String person_id){
		return db().getMap("select * from t_dy_info where userid=? ", new Object[] {person_id});
	
	}


}
