package com.lehand.developing.party.dto;

import java.io.Serializable;

public class UrcOrgan implements Serializable {
    /** 自增长id*/
    private Long orgid;

    /** 组织类型*/
    private Long orgtypeid;

    /** 组织编码*/
    private String orgcode;

    /** 组织名称*/
    private String orgname;

    /** 简称*/
    private String sorgname;

    /** 组织排序号*/
    private Long orgseqno;

    /** 创建人*/
    private String createuserid;

    /** 创建时间*/
    private String createtime;

    /** 更新人*/
    private String updateuserid;

    /** 更新时间*/
    private String updatetime;

    /** 状态*/
    private Integer status;

    /** 所属租户*/
    private Long compid;

    private Integer remarks1;

    private Integer remarks2;

    /** 组织的userid (当前任务创建人)*/
    private String remarks3;

    private String remarks4;

    private String remarks5;

    private String remarks6;

    /** 父级组织ID(0时视为顶层组织)*/
    private Long porgid;

    /** t_dzz_info表组织机构id */
    private String orgfzdyid;


    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public Long getOrgtypeid() {
        return orgtypeid;
    }

    public void setOrgtypeid(Long orgtypeid) {
        this.orgtypeid = orgtypeid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getSorgname() {
        return sorgname;
    }

    public void setSorgname(String sorgname) {
        this.sorgname = sorgname;
    }

    public Long getOrgseqno() {
        return orgseqno;
    }

    public void setOrgseqno(Long orgseqno) {
        this.orgseqno = orgseqno;
    }

    public String getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(String createuserid) {
        this.createuserid = createuserid;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(String updateuserid) {
        this.updateuserid = updateuserid;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Integer getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    public Integer getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    public String getRemarks4() {
        return remarks4;
    }

    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    public String getRemarks5() {
        return remarks5;
    }

    public void setRemarks5(String remarks5) {
        this.remarks5 = remarks5;
    }

    public String getRemarks6() {
        return remarks6;
    }

    public void setRemarks6(String remarks6) {
        this.remarks6 = remarks6;
    }

    public Long getPorgid() {
        return porgid;
    }

    public void setPorgid(Long porgid) {
        this.porgid = porgid;
    }

    public String getOrgfzdyid() {
        return orgfzdyid;
    }

    public void setOrgfzdyid(String orgfzdyid) {
        this.orgfzdyid = orgfzdyid;
    }

}
