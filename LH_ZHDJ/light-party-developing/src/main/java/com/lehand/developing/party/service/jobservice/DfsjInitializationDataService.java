package com.lehand.developing.party.service.jobservice;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.service.dfgl.dfsjgl.DfglSjblHandle;
import com.lehand.developing.party.service.dfgl.gzjfgl.DfglGzjfHandle;
import com.lehand.developing.party.utils.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class DfsjInitializationDataService {

	private static final Logger logger = LogManager.getLogger(DfsjInitializationDataService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	//党支部code
	@Value("${jk.organ.dangzhibu}")
	private String dangzhibu;
	
	//党支部code
	@Value("${jk.organ.lianhedangzhibu}")
	private String lianhedangzhibu;

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	@Resource
	protected DfglGzjfHandle dfglGzjfHandle;

	@Resource
	protected DfglSjblHandle dfglSjblHandle;
	
	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	

	public void initializationData() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			//查询个支部党费基数设置
			List<Map<String,Object>> list = generalSqlComponent.query(SqlCode.getDfjsList,new Object[]{});
			List<String> data = null;
			//年份
			String years = DateEnum.YYYY.format();
			//月份
			int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
			for(Map<String,Object> li:list){
				data = new ArrayList<String>();
				data.add(li.get("username").toString());
				data.add(li.get("zjhm").toString());
				data.add(li.get("basefee").toString());
				//删除个人党费明细
				dfglGzjfHandle.deleteHDyDues(years,months,li.get("organid").toString(),data.get(1).toString());
				//插入个人党费明细
				dfglGzjfHandle.insertHDyDues2(years,months,data,li.get("organid").toString(),Double.valueOf(StringUtils.isEmpty(data.get(2))?"0":data.get(2)));
				//插入每月缴费信息表
				dfglSjblHandle.hDYJfInfo(Double.valueOf(StringUtils.isEmpty(data.get(2))?"0":data.get(2)),li.get("userid").toString(),data.get(0),li.get("organid").toString(),String.valueOf(NumberUtils.num1)
						,null,null,null,1l,Integer.valueOf(years),months);
			}

			logger.info("定时生成各个党支部的党费收缴任务结束...");
		} catch (Exception e) {
			logger.error("定时生成各个党支部的党费收缴任务异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	

	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
	
}
