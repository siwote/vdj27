package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdySbdwspStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_sbdwsp where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		makeFiles(map,session);
		return map;
	}
	
	//tzbrrq
	//zbdhxbrq
	//bsjdwsprq 
	//person_id
	//file
	//flag 是否是反写 是传1 否传0 (flag 为0时 传bsjdwsprq person_id flag)(flag 为1时 传 tzbrrq zbdhxbrq person_id flag)
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		int flag = Integer.valueOf(params.get("flag").toString());
		int num = 0;
		if(flag==0) {
			params.put("reportp_id", UUID.randomUUID().toString().replaceAll("-", ""));
			params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
			params.put("created_by", session.getUserid());
			params.put("delete_flag", "0");
			params.put("syncstatus", "A");
			params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
			if(params.get("tzbrrq")==null) {
				params.put("tzbrrq", Constant.ENPTY);
			}
			if(params.get("zbdhxbrq")==null) {
				params.put("zbdhxbrq", Constant.ENPTY);
			}
			num = db().delete("UPDATE t_fzdy_sbdwsp SET syncstatus='D' WHERE  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
			
			num += db().insert("INSERT INTO t_fzdy_sbdwsp (compid, reportp_id, bsjdwsprq, tzbrrq, zbdhxbrq, created_by, create_date, delete_flag, person_id, syncstatus, synctime, file)" + 
					" VALUES (:compid,:reportp_id,:bsjdwsprq,:tzbrrq,:zbdhxbrq,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
			Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
			if(map!=null && map.get("sfipen").toString().equals("0")) {
				db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
			}
			saveFile(params);
			return num;
		}else {
			params.put("updated_by", session.getUserid());
			num = db().update("UPDATE t_fzdy_sbdwsp SET tzbrrq=:tzbrrq,zbdhxbrq=:zbdhxbrq,updated_by=:updated_by WHERE  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
			return num;
		}
		
	}
}
