//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lehand.developing.party.common.constant;

public class FormSqlCode {
    public static String insertForm = "insertForm";
    public static String pageQueryForm = "pageQueryForm";
    public static String updateStatusByid = "updateStatusByid";
    public static String deleteFormById = "deleteFormById";
    public static String getFormById = "getFormById";
    public static String getFormByBusId = "getFormByBusId";
    public static String updateForm = "updateForm";

    public FormSqlCode() {
    }
}
