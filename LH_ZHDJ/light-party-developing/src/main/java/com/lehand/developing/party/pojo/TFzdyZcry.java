package com.lehand.developing.party.pojo;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description= "返回响应数据")
public class TFzdyZcry {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.transfer_id
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "主键id")
	private String transferId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.person_id
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "人员id")
	private String personId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.created_by
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "创建人")
	private String createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.updated_by
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "修改人")
	private String updatedBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.create_date
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "创建日期")
	private String createDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.last_update_date
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "最近一次修改日期")
	private String lastUpdateDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.version
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	private BigDecimal version;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.delete_flag
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "删除标识")
	private BigDecimal deleteFlag;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.szdzb
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "转出人所在支部")
	private String szdzb;
	
	@ApiModelProperty(value = "转出支部")
	private String dx_uuid;
	
	public String getDx_uuid() {
		return dx_uuid;
	}

	public void setDx_uuid(String dx_uuid) {
		this.dx_uuid = dx_uuid;
	}   

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.sync_status
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "A本地新增，E 本地修改，D 本地删除 Q 同步")
	private String syncStatus;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.synctime
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "同步时间")
	private String synctime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.zrdzz
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "转入党组织")
	private String zrdzz;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.zcrq
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "转出时间")
	private String zcrq;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.zrrq
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "转入时间")
	private String zrrq;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.zcstatus
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "主键id")
	private Integer zcstatus;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column t_fzdy_zcry.remark
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	@ApiModelProperty(value = "审核备注")
	private String remark;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.transfer_id
	 * @return  the value of t_fzdy_zcry.transfer_id
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getTransferId() {
		return transferId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.transfer_id
	 * @param transferId  the value for t_fzdy_zcry.transfer_id
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setTransferId(String transferId) {
		this.transferId = transferId == null ? null : transferId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.person_id
	 * @return  the value of t_fzdy_zcry.person_id
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.person_id
	 * @param personId  the value for t_fzdy_zcry.person_id
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setPersonId(String personId) {
		this.personId = personId == null ? null : personId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.created_by
	 * @return  the value of t_fzdy_zcry.created_by
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.created_by
	 * @param createdBy  the value for t_fzdy_zcry.created_by
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy == null ? null : createdBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.updated_by
	 * @return  the value of t_fzdy_zcry.updated_by
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.updated_by
	 * @param updatedBy  the value for t_fzdy_zcry.updated_by
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy == null ? null : updatedBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.create_date
	 * @return  the value of t_fzdy_zcry.create_date
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.create_date
	 * @param createDate  the value for t_fzdy_zcry.create_date
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate == null ? null : createDate.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.last_update_date
	 * @return  the value of t_fzdy_zcry.last_update_date
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.last_update_date
	 * @param lastUpdateDate  the value for t_fzdy_zcry.last_update_date
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate == null ? null : lastUpdateDate.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.version
	 * @return  the value of t_fzdy_zcry.version
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public BigDecimal getVersion() {
		return version;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.version
	 * @param version  the value for t_fzdy_zcry.version
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setVersion(BigDecimal version) {
		this.version = version;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.delete_flag
	 * @return  the value of t_fzdy_zcry.delete_flag
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public BigDecimal getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.delete_flag
	 * @param deleteFlag  the value for t_fzdy_zcry.delete_flag
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setDeleteFlag(BigDecimal deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.szdzb
	 * @return  the value of t_fzdy_zcry.szdzb
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getSzdzb() {
		return szdzb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.szdzb
	 * @param szdzb  the value for t_fzdy_zcry.szdzb
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setSzdzb(String szdzb) {
		this.szdzb = szdzb == null ? null : szdzb.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.sync_status
	 * @return  the value of t_fzdy_zcry.sync_status
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getSyncStatus() {
		return syncStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.sync_status
	 * @param syncStatus  the value for t_fzdy_zcry.sync_status
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus == null ? null : syncStatus.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.synctime
	 * @return  the value of t_fzdy_zcry.synctime
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getSynctime() {
		return synctime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.synctime
	 * @param synctime  the value for t_fzdy_zcry.synctime
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setSynctime(String synctime) {
		this.synctime = synctime == null ? null : synctime.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.zrdzz
	 * @return  the value of t_fzdy_zcry.zrdzz
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getZrdzz() {
		return zrdzz;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.zrdzz
	 * @param zrdzz  the value for t_fzdy_zcry.zrdzz
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setZrdzz(String zrdzz) {
		this.zrdzz = zrdzz == null ? null : zrdzz.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.zcrq
	 * @return  the value of t_fzdy_zcry.zcrq
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getZcrq() {
		return zcrq;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.zcrq
	 * @param zcrq  the value for t_fzdy_zcry.zcrq
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setZcrq(String zcrq) {
		this.zcrq = zcrq == null ? null : zcrq.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.zrrq
	 * @return  the value of t_fzdy_zcry.zrrq
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getZrrq() {
		return zrrq;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.zrrq
	 * @param zrrq  the value for t_fzdy_zcry.zrrq
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setZrrq(String zrrq) {
		this.zrrq = zrrq == null ? null : zrrq.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.zcstatus
	 * @return  the value of t_fzdy_zcry.zcstatus
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public Integer getZcstatus() {
		return zcstatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.zcstatus
	 * @param zcstatus  the value for t_fzdy_zcry.zcstatus
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setZcstatus(Integer zcstatus) {
		this.zcstatus = zcstatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column t_fzdy_zcry.remark
	 * @return  the value of t_fzdy_zcry.remark
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column t_fzdy_zcry.remark
	 * @param remark  the value for t_fzdy_zcry.remark
	 * @mbg.generated  Mon Jul 22 16:56:00 CST 2019
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
}