package com.lehand.developing.party.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "党员党费实交扩展类",description = "党员党费实交扩展类")
public class PeisonActualDto {
    @ApiModelProperty(value="月份",name="month")
    String month;
    @ApiModelProperty(value="年份",name="year")
    String year;
    @ApiModelProperty(value="实交党费",name="actual")
    String actual;
    @ApiModelProperty(value="需要确认党费党员id",name="peisonid")
    String peisonid;
    @ApiModelProperty(value="应交",name="payable")
    String payable;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getPeisonid() {
        return peisonid;
    }

    public void setPeisonid(String peisonid) {
        this.peisonid = peisonid;
    }

    public String getPayable() {
        return payable;
    }

    public void setPayable(String payable) {
        this.payable = payable;
    }
}
