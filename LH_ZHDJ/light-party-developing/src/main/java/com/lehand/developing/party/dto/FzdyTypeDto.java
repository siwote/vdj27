package com.lehand.developing.party.dto;

import java.util.ArrayList;
import java.util.List;

public class FzdyTypeDto {
	
    private Long id;			// id
    private String label;		// 名称
    private String sftip;
    private List<FzdyTypeDto> children = new ArrayList<FzdyTypeDto>();	//子节点
    private Long pid;			//父节点
    private Boolean isedit;		// 前端需要的
    private Boolean isshow;		// 前端需要的
private Integer orderid;		// 前端需要的
    private Boolean checked;	// 前端需要的
    private String icon;		// 前端需要的
    private Boolean disabled;	// 不可点击的判断，true不可点击，false可点击
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<FzdyTypeDto> getChildren() {
		return children;
	}
	public void setChildren(List<FzdyTypeDto> children) {
		this.children = children;
	}
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public Boolean getIsedit() {
		return isedit;
	}
	public void setIsedit(Boolean isedit) {
		this.isedit = isedit;
	}
	public Boolean getIsshow() {
		return isshow;
	}
	public void setIsshow(Boolean isshow) {
		this.isshow = isshow;
	}
	public Integer getOrderid() {
		return orderid;
	}
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}
	public Boolean getChecked() {
		return checked;
	}
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public String getSftip() {
		return sftip;
	}
	public void setSftip(String sftip) {
		this.sftip = sftip;
	}
	

}
