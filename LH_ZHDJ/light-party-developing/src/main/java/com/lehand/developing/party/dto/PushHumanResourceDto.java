package com.lehand.developing.party.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 人力资源推送数据类
 * @author lx
 */
@ApiModel(value="人力资源推送数据类",description="人力资源推送数据类")
public class PushHumanResourceDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="身份证号码", name="idCard")
	private String idCard;

	@ApiModelProperty(value="姓名", name="name")
	private String name;

	@ApiModelProperty(value="缴费基数", name="basefee")
	private Double basefee;

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getBasefee() {
		return basefee;
	}

	public void setBasefee(Double basefee) {
		this.basefee = basefee;
	}
}
