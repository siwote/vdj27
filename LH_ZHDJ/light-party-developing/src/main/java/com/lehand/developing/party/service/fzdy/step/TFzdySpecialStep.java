package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;


@Service
public class TFzdySpecialStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from fzdy_special  where person_id=:person_id and stepid=:step limit 1", params);
//		if (map != null) {
//			String fileids = String.valueOf(map.get("fileids"));
//			List<Map<String, Object>> files = new ArrayList<Map<String, Object>>(0);
//			if (!StringUtils.isEmpty(fileids)) {
//				files = db().listMap("select * from dl_file where compid=? and id in (" + fileids + ")",
//						new Object[] {session.getCompid()});
//			}
//			map.put("files", files);
//		}
		makeFiles(map,session);
		return map;
	}
	
	

	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("stepid", params.get("step"));
		params.put("fileids", params.get("file"));
		db().delete("delete from fzdy_special where  person_id=:person_id and stepid=:step", params);
		int num = db().insert("INSERT INTO fzdy_special (person_id, stepid, fileids) VALUES (:person_id, :stepid, :fileids)", params);
		return num;
	}
}
