package com.lehand.developing.party.controller.schedule;

import com.lehand.base.common.Message;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.service.dfgl.dfsjgl.PushHumanResourceHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Description: 定时发送微信党员党费消息
 * @author lx
 * @date 2021/3/29
 * @return
 */
public class PushWeChatSchedule {
	private static final Logger logger = LogManager.getLogger(PushWeChatSchedule.class);

	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	@Resource
	PushHumanResourceHandle pushHumanResourceHandle;

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}




	public void pushWeChat() {
		Message message = new Message();
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			pushHumanResourceHandle.pushWechat();
			logger.info("定时发送微信党员党费消息结束");
		} catch (LehandException e) {
			logger.error(e.getMessage());
		}catch (Exception e) {
			logger.error("测试发送微信党员党费消息出错！！"+e.getMessage());
		}finally {
			CREATE.set(false);
		}
	}
}
