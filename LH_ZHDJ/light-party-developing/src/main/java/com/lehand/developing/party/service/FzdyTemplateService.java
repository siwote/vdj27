package com.lehand.developing.party.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.developing.party.common.sql.CommSqlCfgCode;
import com.lehand.developing.party.pojo.DlFile;
import com.lehand.developing.party.pojo.DlFileBusiness;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.pojo.FzdyMbpz;

/**
 * @ClassName:：FzdyTemplateBusinessImpl 
 * @Description： 模板配置实现层
 * @author ：taogang  
 * @date ：2019年7月8日 下午4:13:28 
 *
 */
@Service
public class FzdyTemplateService {
	
	@Resource private GeneralSqlComponent generalSqlComponent;
	
	@Resource private DFSComponent dFSComponent;

	/**
	 * 新增模板
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean addTemplate(FzdyMbpz info,Long fileid, Session session) {
		//保存模板信息
		info.setCompid(session.getCompid());
		info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setStatus("1");
		info.setCreator(session.getUserid());
//		info.setOrgid(session.getOrganids().get(0).getOrgid());
//		info.setOrgname(session.getOrganids().get(0).getOrgname());
		info.setOrgid(session.getCurrentOrg().getOrgid());
		info.setOrgname(session.getCurrentOrg().getOrgname());
		info.setOrderid(10);
		Long mbId = generalSqlComponent.insert(SqlCode.addTemplate, info);
		//保存文件关联信息
		boolean flag = saveFileBusiness(mbId, fileid, session);
		if(!flag) {
			return false;
		}
		return true;
	}
	
	/**
	 * @Title：saveFileBusiness 
	 * @Description：保存文件关联表
	 * @param ：@param mbId
	 * @param ：@param fileid
	 * @param ：@param session
	 * @param ：@return 
	 * @return ：boolean 
	 * @throws
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean saveFileBusiness(Long mbId,Long fileid,Session session) {
		//删除文件关联
		delFileBusiness(fileid, mbId, 5L, session.getCompid());
		//新增关联信息
		DlFileBusiness business = new DlFileBusiness();
		business.setCompid(session.getCompid());
		business.setFileid(fileid);
		business.setBusinessid(mbId);
		business.setBusinessname(Short.valueOf("5"));
		business.setIntostatus(Short.valueOf("0"));
		generalSqlComponent.insert(CommSqlCfgCode.addDlFileBusiness, business);
		return true;
	}
	
	/**
	 * 修改模板
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean updateTemplate(FzdyMbpz info,Long fileid, Session session) {
		//查询模板信息
		FzdyMbpz oldInfo = queryTempById(info.getId(), session.getCompid());
		if(null == oldInfo) {
			return false;
		}
		oldInfo = updateMbpz(oldInfo, info, session);
		generalSqlComponent.update(SqlCode.updateTempLate, oldInfo);
		//修改文件关联表
		boolean flag = saveFileBusiness(info.getId(), fileid, session);
		if(!flag) {
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @Title：delFileBusiness 
	 * @Description：删除文件关联表
	 * @param ：@param fileid
	 * @param ：@param bussid
	 * @param ：@param bussname
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：boolean 
	 * @throws
	 */
	public boolean delFileBusiness(Long fileid,Long bussid,Long bussname,Long compid) {
		generalSqlComponent.delete(SqlCode.delFileBusinessByMb, new Object[] {fileid,bussid,bussname,compid});
		return true;
	}
	
	/**
	 * 
	 * @Title：updateMbpz 
	 * @Description：模板配置表数据替换
	 * @param ：@param oldInfo
	 * @param ：@param info
	 * @param ：@return 
	 * @return ：FzdyMbpz 
	 * @throws
	 */
	private FzdyMbpz updateMbpz(FzdyMbpz oldInfo,FzdyMbpz info,Session session) {
		oldInfo.setTypeid(info.getTypeid());
		oldInfo.setModitor(session.getUserid());
		oldInfo.setModitime(DateEnum.YYYYMMDDHHMMDD.format());
//		info.setOrgid(session.getOrganids().get(0).getOrgid());
//		info.setOrgname(session.getOrganids().get(0).getOrgname());
		info.setOrgid(session.getCurrentOrg().getOrgid());
		info.setOrgname(session.getCurrentOrg().getOrgname());
		return oldInfo;
	}
	
	/**
	 * 删除模板
	 * @throws Exception 
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean delTemplate(Long id,Long fileid, Session session) throws Exception {
		//定义参数map
		Map<String, Object> parm = new HashMap<String, Object>(2);
		parm.put("id", fileid);
		parm.put("compid", session.getCompid());
		//查询文件信息
		DlFile file = generalSqlComponent.query(CommSqlCfgCode.getFilesDep,parm);
		if(null != file) {
			  synchronized (dFSComponent) {
		            //直接删除文件
		            dFSComponent.delete(file.getDir());
		            //删除文件的表数据
		           generalSqlComponent.delete(CommSqlCfgCode.deleteFile, parm);
		        }
		}
		//删除文件关联信息
		delFileBusiness(fileid, id, 5L, session.getCompid());
		//删除模板信息
		generalSqlComponent.delete(SqlCode.delTemplate, new Object[] {id,session.getCompid()});
		return true;
	}
	
	/**
	 * 模板列表
	 */
	
	public Pager queryTemplateByTypeId(Pager pager,Long typeid, Session session) {
		return generalSqlComponent.pageQuery(SqlCode.queryTemplateByTypeId, new Object[] {session.getCompid(),typeid,5}, pager);
	}
	
	/**
	 * 
	 * @Title：queryTempById 
	 * @Description：根据id查询模板信息
	 * @param ：@param id
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：FzdyMbpz 
	 * @throws
	 */
	private FzdyMbpz queryTempById(Long id,Long compid) {
		return generalSqlComponent.query(SqlCode.queryTempById, new Object[] {id,compid});
	}

}
