package com.lehand.developing.party.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @program:
 * @description: FzdyPartyLink
 * @author: zwd
 * @create: 2020-10-31 14:54
 */
public class FzdyPartyLink implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ApiModelProperty(value="",name="id")
    private String id;

    /**
     *
     */
    @ApiModelProperty(value="",name="userid",required=true)
    private String userid;

    /**
     * 当前环节编码
     */
    @ApiModelProperty(value="当前环节编码",name="type",required=true)
    private Long type;

    /**
     * 入党申请人：申请时间
     */
    @ApiModelProperty(value="入党申请人：申请时间",name="applyTime")
    private String applyTime;

    /**
     * 入党申请人:谈话地点
     */
    @ApiModelProperty(value="入党申请人:谈话地点",name="personPlace")
    private String personPlace;

    /**
     * 入党申请人:谈话时间
     */
    @ApiModelProperty(value="入党申请人:谈话时间",name="personTime")
    private String personTime;

    public String getPersonPlace() {
        return personPlace;
    }

    public void setPersonPlace(String personPlace) {
        this.personPlace = personPlace;
    }

    public String getPersonTime() {
        return personTime;
    }

    public void setPersonTime(String personTime) {
        this.personTime = personTime;
    }

    //    /**
//     * 入党申请人：谈话接收人
//     */
//    @ApiModelProperty(value="入党申请人：谈话接收人",name="applyTalkUserId")
//    private String applyTalkUserId;

//    /**
//     * 入党申请人：谈话接收人名
//     */
//    @ApiModelProperty(value="入党申请人：谈话接收人名",name="applyTalkUserName")
//    private String applyTalkUserName;



    /**
     * 入党积极分子：列为入党积极分子
     */
    @ApiModelProperty(value="入党积极分子：列为入党积极分子",name="activistTime")
    private String activistTime;

//    /**
//     * 入党积极分子：指定培养人
//     */
//    @ApiModelProperty(value="入党积极分子：指定培养人",name="activistFosterUserId")
//    private String activistFosterUserId;
//
//    /**
//     * 入党积极分子：指定培养人名
//     */
//    @ApiModelProperty(value="入党积极分子：指定培养人名",name="activistFosterUserName")
//    private String activistFosterUserName;



    /**
     * 发展对象：集中发展对象时间
     */
    @ApiModelProperty(value="发展对象：集中发展对象时间",name="developTime")
    private String developTime;

    /**
     * 发展对象：集中培训时间
     */
    @ApiModelProperty(value="发展对象：集中培训时间",name="developCultivateTime")
    private String developCultivateTime;

//    /**
//     * 发展对象：入党介绍人
//     */
//    @ApiModelProperty(value="发展对象：入党介绍人",name="developUserId")
//    private String developUserId;
//
//    /**
//     * 发展对象：入党介绍人名
//     */
//    @ApiModelProperty(value="发展对象：入党介绍人名",name="developUserName")
//    private String developUserName;



    /**
     * 预备党员：确定预备党员时间
     */
    @ApiModelProperty(value="预备党员：确定预备党员时间",name="readyOkTime")
    private String readyOkTime;

    /**
     * 预备党员：上级党委审批时间
     */
    @ApiModelProperty(value="预备党员：上级党委审批时间",name="readyApprovalTime")
    private String readyApprovalTime;

    /**
     * 预备党员：志愿书编号
     */
    @ApiModelProperty(value="预备党员：志愿书编号",name="readyCode")
    private String readyCode;
    /**
     * 预备党员:谈话人
     */
    @ApiModelProperty(value="预备党员:谈话人",name="speakers")
    private String speakers;

    /**
     * 预备党员:谈话时间
     */
    @ApiModelProperty(value="预备党员:谈话时间",name="speakersTime")
    private String speakersTime;


    /**
     * 正式党员：预备转正时间
     */
    @ApiModelProperty(value="正式党员：预备转正时间",name="formalTime")
    private String formalTime;

    /**
     * 正式党员：上级党委审批时间
     */
    @ApiModelProperty(value="正式党员：上级党委审批时间",name="formalApprovalTime")
    private String formalApprovalTime;

    /**
     * 正式党员:是否延迟预备期（是/否）
     */
    @ApiModelProperty(value="正式党员:是否延迟预备期（是/否）",name="delay")
    private String delay;

    /**
     * 正式党员:材料归档（是/否）
     */
    @ApiModelProperty(value="正式党员:材料归档（是/否）",name="materialArchive")
    private String materialArchive;

    public String getDelay() {
        return delay;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }

    public String getMaterialArchive() {
        return materialArchive;
    }

    public void setMaterialArchive(String materialArchive) {
        this.materialArchive = materialArchive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

//    public String getApplyTalkUserId() {
//        return applyTalkUserId;
//    }
//
//    public void setApplyTalkUserId(String applyTalkUserId) {
//        this.applyTalkUserId = applyTalkUserId;
//    }
//
//    public String getApplyTalkUserName() {
//        return applyTalkUserName;
//    }
//
//    public void setApplyTalkUserName(String applyTalkUserName) {
//        this.applyTalkUserName = applyTalkUserName;
//    }

    public String getActivistTime() {
        return activistTime;
    }

    public void setActivistTime(String activistTime) {
        this.activistTime = activistTime;
    }

//    public String getActivistFosterUserId() {
//        return activistFosterUserId;
//    }
//
//    public void setActivistFosterUserId(String activistFosterUserId) {
//        this.activistFosterUserId = activistFosterUserId;
//    }
//
//    public String getActivistFosterUserName() {
//        return activistFosterUserName;
//    }
//
//    public void setActivistFosterUserName(String activistFosterUserName) {
//        this.activistFosterUserName = activistFosterUserName;
//    }

    public String getDevelopTime() {
        return developTime;
    }

    public void setDevelopTime(String developTime) {
        this.developTime = developTime;
    }

    public String getDevelopCultivateTime() {
        return developCultivateTime;
    }

    public void setDevelopCultivateTime(String developCultivateTime) {
        this.developCultivateTime = developCultivateTime;
    }

//    public String getDevelopUserId() {
//        return developUserId;
//    }
//
//    public void setDevelopUserId(String developUserId) {
//        this.developUserId = developUserId;
//    }
//
//    public String getDevelopUserName() {
//        return developUserName;
//    }
//
//    public void setDevelopUserName(String developUserName) {
//        this.developUserName = developUserName;
//    }

    public String getReadyOkTime() {
        return readyOkTime;
    }

    public void setReadyOkTime(String readyOkTime) {
        this.readyOkTime = readyOkTime;
    }

    public String getReadyApprovalTime() {
        return readyApprovalTime;
    }

    public void setReadyApprovalTime(String readyApprovalTime) {
        this.readyApprovalTime = readyApprovalTime;
    }

    public String getReadyCode() {
        return readyCode;
    }

    public void setReadyCode(String readyCode) {
        this.readyCode = readyCode;
    }

    public String getFormalTime() {
        return formalTime;
    }

    public void setFormalTime(String formalTime) {
        this.formalTime = formalTime;
    }

    public String getFormalApprovalTime() {
        return formalApprovalTime;
    }

    public void setFormalApprovalTime(String formalApprovalTime) {
        this.formalApprovalTime = formalApprovalTime;
    }

    public String getSpeakers() {
        return speakers;
    }

    public void setSpeakers(String speakers) {
        this.speakers = speakers;
    }

    public String getSpeakersTime() {
        return speakersTime;
    }

    public void setSpeakersTime(String speakersTime) {
        this.speakersTime = speakersTime;
    }

    /**
     * FzdyPartyLinkEntity.toString()
     */
    @Override
    public String toString() {
        return "FzdyPartyLinkEntity{" +
                "id='" + id + '\'' +
                ", userid='" + userid + '\'' +
                ", type='" + type + '\'' +
                ", applyTime='" + applyTime + '\'' +
//                ", applyTalkUserId='" + applyTalkUserId + '\'' +
//                ", applyTalkUserName='" + applyTalkUserName + '\'' +
                ", activistTime='" + activistTime + '\'' +
//                ", activistFosterUserId='" + activistFosterUserId + '\'' +
//                ", activistFosterUserName='" + activistFosterUserName + '\'' +
                ", developTime='" + developTime + '\'' +
                ", developCultivateTime='" + developCultivateTime + '\'' +
//                ", developUserId='" + developUserId + '\'' +
//                ", developUserName='" + developUserName + '\'' +
                ", readyOkTime='" + readyOkTime + '\'' +
                ", readyApprovalTime='" + readyApprovalTime + '\'' +
                ", readyCode='" + readyCode + '\'' +
                ", formalTime='" + formalTime + '\'' +
                ", formalApprovalTime='" + formalApprovalTime + '\'' +
                '}';
    }
}