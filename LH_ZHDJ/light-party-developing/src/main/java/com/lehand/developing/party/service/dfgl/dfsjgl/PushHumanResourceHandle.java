package com.lehand.developing.party.service.dfgl.dfsjgl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.constant.Constant;
import com.lehand.developing.party.common.sql.PushHumanSqlCode;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.dto.PushHumanResourceDto;
import com.lehand.developing.party.dto.PushHumanResourceReq;
import com.lehand.developing.party.utils.NewsPush;
import com.lehand.horn.partyorgan.dto.SendDuesMessage;
import com.lehand.horn.partyorgan.dto.TemplateInfo;
import com.lehand.horn.partyorgan.dto.TranTemplate;
import com.lehand.horn.partyorgan.dto.WXTransportTemplate;
import com.lehand.horn.partyorgan.util.AccessTokenAuth;
import com.lehand.horn.partyorgan.util.WeChatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.tools.ant.launch.Locator.encodeURI;


/**
 * Description: 推送人力资源处理
 * @author lx
 *
 */
@Service
public class PushHumanResourceHandle {
	private static final Logger logger = LogManager.getLogger(PushHumanResourceHandle.class);
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}

	protected DecimalFormat dFormat = new DecimalFormat("0.00");

	private static AccessTokenAuth accessTokenAuth;


	@Transactional(rollbackFor = Exception.class)
	public void pushBasefee(PushHumanResourceReq req) {
		//当前时间
		String adjusttime = DateEnum.YYYYMMDDHHMMDD.format();
		LocalDateTime date =LocalDateTime.now();
		int year = date.getYear();
		int month = date.getMonthValue();
		//保存推送日志
		Map<String,Object> paramMap =new HashMap<>();
		paramMap.put("record", JSON.toJSONString(req.getRequest().toArray()));
		paramMap.put("time",adjusttime);
		generalSqlComponent.getDbComponent().insert("INSERT INTO push_basefee_log (record, times) VALUES (:record, :time)",paramMap);
		logger.info("人力推送"+JSON.toJSONString(req.getRequest().toArray()));
		//获取所有党员身份证号码和组织id
		List<Map<String,Object>> listTDyInfo = generalSqlComponent.query(PushHumanSqlCode.listZJHMByTDyInfo, new Object[]{});
		for (Map<String,Object> map: listTDyInfo) {
			String zjhm = String.valueOf(map.get("zjhm"));
			String organid = String.valueOf(map.get("organid"));
			String userid = String.valueOf(map.get("userid"));
			String xm = String.valueOf(map.get("xm"));
			for (PushHumanResourceDto pushHumanResourceDto: req.getRequest()) {
				if(zjhm.equals(pushHumanResourceDto.getIdCard())){
					logger.info("人力推送"+pushHumanResourceDto.getName());
					Double basefee = pushHumanResourceDto.getBasefee();
					//人力推送synchronous 设置为1
					Object[] args = new Object[] {basefee,adjusttime,"人力推送",1,1,userid};
					Map<String, Object> jfjsMap = db().getMap("select * from h_dy_jfjs where compid=? and userid=?", new Object[]{1, userid});
					if(jfjsMap!=null&&jfjsMap.size()>0){
						db().update("update h_dy_jfjs set basefee=?,adjusttime=?,ajusername=?,synchronous=? where compid=? and userid=?", args);
					}else{
						Map<String,Object> parms = new HashMap<String,Object>();
						parms.put("userid", userid);
						parms.put("username",xm);
						parms.put("organid", organid);
						parms.put("basefee",  basefee);
						parms.put("compid", 1);
						parms.put("adjusttime", adjusttime);
						parms.put("synchronous", 1);
						db().insert("insert h_dy_jfjs set userid=:userid,username=:username,organid=:organid,basefee=:basefee,compid=:compid,adjusttime=:adjusttime,synchronous=:synchronous",parms);
					}

					//党组织是否上报党费
					Map<String,Object> hdzz = db().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?", new Object[] {1,organid,year,month});
					if(hdzz!=null){
						Integer apstatus = (Integer) hdzz.get("apstatus");
						if(apstatus==0||apstatus==3){
							//比例查询
							hDYJfInfo(basefee, userid, xm, organid, 1, adjusttime, null, null, 1, year, month);

							//查询是否有当月费用
							Map dues = db().getMap("SELECT * FROM h_dy_dues where years=? and months=? and usercard=?",new Object[]{year,month,zjhm});
							List<String> data = new ArrayList<>();
							data.add(xm);
							data.add(zjhm);
							if(dues!=null){
								//修改个人党费明细 覆盖之前
								updateHDyDues(year,month,data,organid,basefee);
							}else{
								//插入个人党费明细
								insertHDyDues(year,month,data,organid,basefee);
							}
						}
					}
				}
			}
		}
	}
	@Transactional(rollbackFor = Exception.class)
	public void deleteHDyDues(Integer years,Integer months,String organcode,String usercard) {
		db().delete("delete from h_dy_dues where organcode = ? and years =? and months = ? and usercard = ?",new Object[]{organcode,years,months,usercard});
	}
	@Transactional(rollbackFor = Exception.class)
	public void insertHDyDues(Integer years,Integer months,List<String> list, String organid,Double basefee) {
		//month月份是否在其他支部生成h_dy_dues数据，如果生成了则不能在本支部生成
		Map<String,Object> map2 = db().getMap("select * from h_dy_dues where years = ? and months = ? and usercard = ? and organcode!=?",new Object[]{years,months,list.get(1),organid});
		if(map2==null){
			Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
			Double payable = Double.valueOf(dFormat.format(basefee*rate/100));
			db().insert("INSERT INTO h_dy_dues set years=?, months=?, usercard=?, username=?, organcode=?, wage=?, deductionone=?, " +
							"deductiontwo=?, deductionthree=?, deductionfour=?, deductionfive=?, deductionsix=?, deductionseven=?, " +
							"basefee=?, rate=?, payable=?, status=?, remarks=?",
					new Object[]{years,months,list.get(1),list.get(0),organid,null,null,
							null,null,null,null,null,0,
							basefee,rate,payable,1,null});
		}
	}
	@Transactional(rollbackFor = Exception.class)
	public void updateHDyDues(Integer years,Integer months,List<String> list, String organid,Double basefee) {

		Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
		Double payable = Double.valueOf(dFormat.format(basefee*rate/100));
		db().update("update h_dy_dues set actual=?,basefee=?,payable = ?,rate=? where organcode = ? and years =? and months = ? and usercard = ?"
				,new Object[]{null,basefee,payable,rate,organid,years,months,list.get(1)});

	}
	public Map<String, Object> getRate(Double dues) {
		Map<String,Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id DESC limit 1", new Object[] {1,dues});

		if(jssz==null){
			jssz.put("ratio",0);
		}
		return jssz;
	}
	@Transactional(rollbackFor = Exception.class)
	public void hDYJfInfo(Double basefee, String userid, String username, String organid, Integer onpost, String adjusttime, String ajuserid, String ajusername, Integer compid, int year, int month) {
//		Map<String,Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and min<=? and max>? limit 1", new Object[] {compid, basefee, basefee});
		Map<String, Object> jssz =db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[] {compid, basefee});
		if(jssz==null) {
			LehandException.throwException("党费缴纳比例未设置，请先设置再操作！");
		}
		Map<String, Object> params = getParams(basefee, userid, username, organid, onpost, adjusttime, ajuserid,
				ajusername, compid, year, month, jssz.get("ratio").toString());
		Map<String,Object> map = db().getMap("select * from h_dy_jfinfo where compid=? and userid=? and year=? and month=?", new Object[] {compid, userid, year, month});
		//month月份是否在其他支部生成h_dy_jfinfo数据，如果生成了则不能在本支部生成
		Map<String,Object> map2 = db().getMap("select * from h_dy_jfinfo where userid = ? and month =? and year = ? and organid !=? and compid =?", new Object[] {userid,month, year,organid, compid});
		if(map2==null){
			if(map==null) {//新增
				db().insert("INSERT INTO h_dy_jfinfo (userid, year, month, basefee, rate, payable, actual, status, remark, onpost, organid, username, ajuserid, ajusername, adjusttime, compid) VALUES (:userid, :year, :month, :basefee, :rate, :payable, :actual, :status, :remark, :onpost, :organid, :username, :ajuserid, :ajusername, :adjusttime, :compid)", params);
			}else {//修改
				db().update("update h_dy_jfinfo set basefee=:basefee,rate=:rate,payable=:payable,actual=:actual,onpost=:onpost,ajuserid=:ajuserid,ajusername=:ajusername,adjusttime=:adjusttime where compid=:compid and userid=:userid and year=:year and month=:month", params);
			}
		}
	}
	public Map<String, Object> getParams(Double basefee, String userid, String username, String organid, Integer onpost,
										 String adjusttime, String ajuserid, String ajusername, Integer compid, int year, int month,
										 String jssz) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("userid", userid);
		params.put("year", year);
		params.put("month", month);
		params.put("basefee", basefee);

		Double ratio = Double.valueOf(jssz);
		params.put("rate", ratio);

		DecimalFormat df=new DecimalFormat("0.00");
		Double yjdf = basefee*ratio/100;
		String yjdf2 = df.format(yjdf);
		params.put("payable", yjdf2);

		params.put("actual", 0);
		params.put("status", 1);
		params.put("remark", Constant.ENPTY);
		params.put("onpost", onpost);
		params.put("organid", organid);
		params.put("username", username);
		params.put("ajuserid", ajuserid);
		params.put("ajusername", ajusername);
		params.put("adjusttime", adjusttime);
		params.put("compid", compid);
		if("0".equals(onpost)) {//不在岗
			params.put("basefee", 0);
			params.put("rate", 0);
			params.put("payable", basefee);
		}
		return params;
	}

	public Object pushWeChatTemplate(WXTransportTemplate bean) {
		//获得令牌
		String	accessToken = WeChatUtils.getToken();

//		//替换token
//		String url=WeChatUtils.send_dues_message.replace("ACCESS_TOKEN", accessToken);
//		//消息文本类
//		SendDuesMessage sendDuesMessage = new SendDuesMessage();
//		//设置消息的类型
//		sendDuesMessage.setMsgtype("text");
//		//设置要发送的openid集合
//		sendDuesMessage.setTouser(openids.get(0));
//		//创建集合
//		Map<String,Object> map=new HashMap<>();
//		//设置发送内容
//		StringBuffer sb =new StringBuffer();
//		sb.append("姓名："+name+"\n");
//		sb.append("日期："+year+"年"+month+"月党费"+"\n");
//		sb.append("应交党费："+payable+"\n");
//		sb.append("实交党费："+actual+"\n");
//		sb.append("你有一条党费消息待确认："+WeChatUtils.confirm_dues);
//		map.put("content",sb.toString());
//		sendDuesMessage.setText(map);
//		//将测试消息对象转成json
//		String jsonMessage = JSONObject.toJSONString(sendDuesMessage);
//		//调用接口进行发送
//		JSONObject jsonObject = WeChatUtils.httpRequest(url, "POST", jsonMessage);


		//替换token
		String url=WeChatUtils.send_dues_message_template.replace("ACCESS_TOKEN", accessToken);
		//发送模板消息开始
		TranTemplate tranTemplate = new TranTemplate();
		//设置接收消息微信ID
		tranTemplate.setTouser(bean.getOpenid());
		//设置模板ID
		tranTemplate.setTemplate_id(bean.getTemplateid());
		//设置url
		tranTemplate.setUrl(bean.getUrl());
		//设置颜色
		tranTemplate.setTopcolor("#FF0000");
		//给模板的内容赋值
		Map<String , TemplateInfo> dataMap = new HashMap<>();
		//姓名
		TemplateInfo userBean = new TemplateInfo(bean.getUser(),"#173177");
		dataMap.put("user",userBean);
		//年月
		TemplateInfo dateBean = new TemplateInfo(bean.getDate(),"#173177");
		dataMap.put("date",dateBean);
		//类型
		TemplateInfo typeBean = new TemplateInfo(bean.getType(),"#173177");
		dataMap.put("type",typeBean);
		//应交
		TemplateInfo payableBean = new TemplateInfo(bean.getPayable(),"#173177");
		dataMap.put("payable",payableBean);
		//实交
		TemplateInfo actualBean = new TemplateInfo(bean.getActual(),"#DC143C");
		dataMap.put("actual",actualBean);
		//当前时间
		TemplateInfo deadTimeBean = new TemplateInfo(bean.getNowTime(),"#173177");
		dataMap.put("nowTime",deadTimeBean);
		//备注
		TemplateInfo remarkBean = new TemplateInfo(bean.getRemark(),"#DC143C");
		dataMap.put("remark",remarkBean);

		tranTemplate.setData(dataMap);
		//将测试消息对象转成json
		String jsonMessage = JSONObject.toJSONString(tranTemplate);
		//调用接口进行发送
		JSONObject jsonObject = WeChatUtils.httpRequest(url, "POST", jsonMessage);

		Integer errcode = jsonObject.getInteger("errcode");
		if (errcode != 0){
			logger.error("党员：【"+bean.getUser()+"】发消息失败 errcode:{" + jsonObject.getInteger("errcode")+"} " +
					"errmsg:{"+jsonObject.getString("errmsg")+"} ");
		}
		return null;
	}
	public void pushWechat(){
		//年
		int year = LocalDate.now().getYear();
		//月
		int month = LocalDate.now().getMonthValue();
		//查询个支部党费基数设置
		List<Map<String,Object>> dataList = generalSqlComponent.query(PushHumanSqlCode.getDfjsOpenidList,new Object[]{});
		for (Map<String,Object> data:dataList) {
			//微信openid
			String openid = String.valueOf(data.get("openid"));
			//党员姓名
			String username = String.valueOf(data.get("username"));
			//党员缴费基数
			String basefee = String.valueOf(data.get("basefee"));
			//身份证号码
			String zjhm = String.valueOf(data.get("zjhm"));
			//组织编码id
			String organid = String.valueOf(data.get("organid"));
			if(!StringUtils.isEmpty(basefee)&&openid!=null){
				//党费
				Map<String, Object> dataMap = getPayable(Double.valueOf(basefee));
				if(dataMap==null){
					continue;
				}
				WXTransportTemplate template=new WXTransportTemplate();
				//微信用户openid
				template.setOpenid(openid);
				//微信消息模板id
				template.setTemplateid(WeChatUtils.template_id);
				//用户姓名
				template.setUser(username);
				//消息类型
				template.setType("每月党费确认");
				//党费日期
				template.setDate(year+"年"+month+"月党费");
				//应交党费
				template.setPayable(String.valueOf(dataMap.get("payable")));
				//实交党费
				template.setActual(String.valueOf(dataMap.get("payable")));
				//当前时间
				template.setNowTime(DateEnum.YYYYMMDDHHMMDD.format());
				//跳转url
				String url = WeChatUtils.confirm_dues_url.replace("YEAR", year + "").replace("MONTH", month + "").replace("ACTUAL", template.getActual()).replace("IDNO", zjhm).replace("ORGID", organid).replace("NAME", username);
				url=url.replace("BASEFEE", basefee).replace("RATE", String.valueOf(dataMap.get("ratio"))).replace("PAYABLE", template.getPayable());
				template.setUrl(url);
				//提示消息备注
				template.setRemark("请及时确认！！");
				pushWeChatTemplate(template);
			}
		}
	}

	private Map<String, Object> getPayable(Double basefee) {

		Map<String, Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[]{1, basefee});
		if (jssz == null) {
			logger.error("党费缴纳比例未设置，请先设置再操作！");
			return null;
		}
		Map<String, Object> map = new HashMap<>();
		//缴费比例
		Double ratio = Double.valueOf(String.valueOf(jssz.get("ratio")));

		Double yjdf = basefee*ratio/100;
		String payable = dFormat.format(yjdf);
		map.put("payable",payable);
		map.put("ratio",ratio);
		return map;
	}

	public Object getConfirmDues() {
		//年
		int year = LocalDate.now().getYear();
		//月
		int month = LocalDate.now().getMonthValue();
		if(1==month){
			year=year-1;
			month=12;
		}else{
			month=month-1;
		}
		Object[] param = {year,month};
		List<Map<String, Object>> maps = generalSqlComponent.getDbComponent().listMap("SELECT basefee ,payable ,actual, rate, years year,months month,usercard idCard,username name FROM  h_dy_dues WHERE (actual is not null) AND years =? AND months=?  ", param);
		if (maps!=null&&maps.size()>0) {
			db().update("UPDATE h_dy_dues SET  push=? WHERE (actual is not null) AND years =? and months = ? ",new Object[]{1,year,month});
		}
		return maps;
	}

	public void confirm(String year,String month,String actual,String idno,String orgid,String name,String basefee,String rate,String payable) {

		Map<String,Object> map = db().getMap("select * from  h_dy_dues WHERE years =? and months = ? and usercard = ?",new Object[]{year,month,idno});

		if(map!=null){
			if(!StringUtils.isEmpty(map.get("actual"))){
				LehandException.throwException("党费已确认！！");
			}
			db().update("UPDATE h_dy_dues SET  actual = ?, status = ? WHERE years =? and months = ? and usercard =?",new Object[]{actual,1,year,month,idno});
		}else{
			db().insert("INSERT INTO h_dy_dues set years=?, months=?, usercard=?, username=?, organcode=?, wage=?, deductionone=?, " +
							"deductiontwo=?, deductionthree=?, deductionfour=?, deductionfive=?, deductionsix=?, deductionseven=?, " +
							"basefee=?, rate=?, payable=?,actual=?, status=?, remarks=?",
					new Object[]{year,month,idno,name,orgid,null,null,
							null,null,null,null,null,0,
							basefee,rate,payable,actual,null});
		}
	}

	public Map<String,Object> get(String year, String month, String idno) {
		Map<String,Object> map = db().getMap("select * from  h_dy_dues WHERE years =? and months = ? and usercard = ?",new Object[]{year,month,idno});
//		if(map!=null){
//			if(!StringUtils.isEmpty(map.get("actual"))){
//				LehandException.throwException("党费已确认！！");
//			}
//		}
		return map;
	}
}
