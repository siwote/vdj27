package com.lehand.developing.party.controller;

import java.util.List;
import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lehand.base.common.Message;
import com.lehand.developing.party.dto.FzdyTypeDto;
import com.lehand.developing.party.pojo.FzdyType;
import com.lehand.developing.party.service.FzdyTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @ClassName:：FzdyTypeController 
 * @Description： 发展党员步骤分类控制层
 * @author ：taogang  
 * @date ：2019年7月8日 上午10:00:23 
 *
 */
@Api(value = "步骤分类服务", tags = { "步骤分类服务" })
@RestController
@RequestMapping("/developing/type")
public class FzdyTypeController extends FzdyBaseController{
	
	private static final Logger logger = LogManager.getLogger(FzdyTypeController.class);
	
	@Resource private FzdyTypeService fdyTypeService;

	@OpenApi(optflag=1)
	@ApiOperation(value = "保存步骤分类", notes = "保存步骤分类", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "步骤id/修改时必传", required = false, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "name", value = "步骤名称", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "pid", value = "父节点，父节点为0", required = false, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "seqno", value = "排序号", required = false, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "sftip", value = "是否提醒节点", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/saveFzdyType")
	public Message saveFzdyType(@ApiIgnore FzdyType fzdyType) {
		Message message = null;
		try {
			boolean success = false;
			if(null == fzdyType.getId()) {
				success = fdyTypeService.addFzdyType(fzdyType, getSession());
			}else {
				success = fdyTypeService.updateFzdyType(fzdyType, getSession());
			}
			//判断是否成功
			if(!success) {
				message = Message.FAIL;
				message.setMessage("步骤分类保存失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("步骤分类保存成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=3)
	@ApiOperation(value = "删除步骤分类", notes = "删除步骤分类", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "步骤id", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/delFzdyType")
	public Message delFzdyType(Long id) {
		Message message = null;
		try {
			if(null == id) {
				message = Message.FAIL;
				message.setMessage("步骤id不可为空");
			}
			boolean success = fdyTypeService.delFzdyType(id, getSession());
			if(!success) {
				message = Message.FAIL;
				message.setMessage("当前节点存在子节点，删除失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("步骤分类删除成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "查询步骤分类Tree", notes = "查询步骤分类Tree", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "showType", value = "查询类型(all 全部，1 提醒节点  )", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "likeStr", value = "名称模糊查询", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/queryFzdyType")
	public Message queryFzdyType(String showType,String likeStr) {
		Message message = null;
		try {
			List<FzdyTypeDto> result = fdyTypeService.queryFzdyType(showType, likeStr, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("查询成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "根据id查询步骤分类信息", notes = "根据id查询步骤分类信息", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "步骤id", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/queryFzdyTypeById")
	public Message queryFzdyTypeById(Long id) {
		Message message = null;
		try {
			if(null == id) {
				message = Message.FAIL;
				message.setMessage("步骤id不可为空");
			}
			FzdyType result = fdyTypeService.queryFzdyTypeById(id, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("根据id查询步骤分类信息成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

}
