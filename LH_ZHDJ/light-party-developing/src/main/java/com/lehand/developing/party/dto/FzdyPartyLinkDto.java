package com.lehand.developing.party.dto;


import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.pojo.FzdyPartyLink;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @program: huaikuang-dj
 * @description: FzdyPartyLinkDto
 * @author: zwd
 * @create: 2020-11-02 09:20
 */
@ApiModel(value = "党员发展5环节表",description = "党员发展5环节表")
public class FzdyPartyLinkDto extends FzdyPartyLink {

    /*@Resource
    FileDelService fileDelService;*/

    protected GeneralSqlComponent generalSqlComponent;

    /**
     * 入党申请人：申请人附件
     */
    @ApiModelProperty(value="入党申请人：申请人附件",name="applyFiles")
    private List<Map<String,Object>> applyFiles;

    /**
     * 入党积极分子：积极分子附件
     */
    @ApiModelProperty(value="入党积极分子：积极分子附件",name="activistFiles")
    private List<Map<String,Object>> activistFiles;

    /**
     * 发展对象：发展对象附件
     */
    @ApiModelProperty(value="发展对象：发展对象附件",name="developFiles")
    private List<Map<String,Object>> developFiles;


    /**
     * 预备党员：政审材料
     */
    @ApiModelProperty(value="预备党员：政审材料",name="readyPoliticalReviewFiles")
    private List<Map<String,Object>> readyPoliticalReviewFiles;

    /**
     * 预备党员：预备党员附件
     */
    @ApiModelProperty(value="预备党员：预备党员附件",name="readyFiles")
    private List<Map<String,Object>> readyFiles;

    /**
     * 正式党员：转正附件
     */
    @ApiModelProperty(value="正式党员：转正附件",name="formalfiles")
    private List<Map<String,Object>> formalfiles;

    public List<Map<String, Object>> getApplyFiles() {
        return applyFiles;
    }

    public void setApplyFiles(List<Map<String, Object>> applyFiles) {
        this.applyFiles = applyFiles;
    }

    public List<Map<String, Object>> getActivistFiles() {
        return activistFiles;
    }

    public void setActivistFiles(List<Map<String, Object>> activistFiles) {
        this.activistFiles = activistFiles;
    }

    public List<Map<String, Object>> getDevelopFiles() {
        return developFiles;
    }

    public void setDevelopFiles(List<Map<String, Object>> developFiles) {
        this.developFiles = developFiles;
    }

    public List<Map<String, Object>> getReadyPoliticalReviewFiles() {
        return readyPoliticalReviewFiles;
    }

    public void setReadyPoliticalReviewFiles(List<Map<String, Object>> readyPoliticalReviewFiles) {
        this.readyPoliticalReviewFiles = readyPoliticalReviewFiles;
    }

    public List<Map<String, Object>> getReadyFiles() {
        return readyFiles;
    }

    public void setReadyFiles(List<Map<String, Object>> readyFiles) {
        this.readyFiles = readyFiles;
    }

    public List<Map<String, Object>> getFormalfiles() {
        return formalfiles;
    }

    public void setFormalfiles(List<Map<String, Object>> formalfiles) {
        this.formalfiles = formalfiles;
    }
}