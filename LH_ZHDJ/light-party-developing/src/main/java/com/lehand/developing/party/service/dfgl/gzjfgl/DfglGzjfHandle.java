package com.lehand.developing.party.service.dfgl.gzjfgl;

import java.awt.*;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Message;
import com.lehand.base.common.Organs;
import com.lehand.base.common.Session;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.utils.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.constant.Constant;
import com.lehand.developing.party.service.dfgl.dfsygl.DfglDfsyHandle;
import org.springframework.web.multipart.MultipartFile;

import static org.apache.tools.ant.launch.Locator.encodeURI;


/**
 * Description: 党费基数上交比率配置
 * @author pantao
 *
 */
@Service
public class DfglGzjfHandle {
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	@Resource
	protected SessionUtils sessionUtils;

	@Resource private DfglDfsyHandle dfglDfsyHandle;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}

	protected DecimalFormat dFormat = new DecimalFormat("0.0");
	
	@Value("${jk.manager.down_ip}")
	private String down_ip;

	@Value("${jk.organ.dangwei}")
	private String dangwei;

	@Value("${jk.organ.dangzongzhi}")
	private String dangzongzhi;

	@Value("${wx_service_url}")
	private String url;


	/**======================================淮矿党费收交开始============================================*/

	/**
	 * 党员党费信息导入
	 * @param file
	 * @param session
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public Message partyInfoImport(MultipartFile file, Session session,Message message) {
		try {
			InputStream inputStream = file.getInputStream();
			List<List<String>> list = ExcelUtils.readExcelNew(file.getOriginalFilename(), inputStream, NumberUtils.num3);
			if (list.size()<1){
				LehandException.throwException("无数据");
				return null;
			}
			int total = list.size();
			int  insertnum = 0;
			int  updatenum = 0;
			StringBuffer buffer = new StringBuffer("导入结果: ");
			buffer.append("\r\n");
			//年份
			String years = DateEnum.YYYY.format();
			//月份
			int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
			int num = 0;
			int num1 = 0;
			for (int i = 0; i < total ; i++) {
				int h = 0;int j = 0;

				if(list.get(0).size()>list.get(i).size()){
					num++;
					buffer.append("第"+(i+1)+"条数据不完整;");
					buffer.append("\r\n");
				}
				//用户名称
				String username = list.get(i).get(0);
				//用户身份证号
				String usercard  = list.get(i).get(1);

				//校验导入数据
				num1 = getValidate(i+1,usercard,username,buffer,num1);

				if(num1>0){
					continue;
				}
				//获取组织机构id,组织机构不存在则不执行导入
				Map<String,Object> map = getUseridOrgcodeByIdcard(usercard,username);
				if(map==null){
					int n = i+1;
					num++;
					buffer.append("第"+n+"条数据党员不存在;");
					buffer.append("\r\n");
					continue;
				}
				String userid = String.valueOf(map.get("userid"));
				String organcode = String.valueOf(map.get("organcode"));
				if(StringUtils.isEmpty(organcode)){
					int n = i+1;
					num++;
					buffer.append("第"+n+"条数据组织机构数据不存在;");
					buffer.append("\r\n");
					continue;
				}
				//查询数据是否已经插入
				Map dues = getHDyDues(years,months,usercard);
				if(dues==null){//插入
					insertHDyDues(years,months,list.get(i),organcode);
					insertnum += 1;
				}else{
					updateHDyDues(years,months,list.get(i),organcode);
					updatenum += 1;
				}
				//发送消息
				String content = "您的"+years+"年"+months+"月党费已生成，应交金额为"+getPayable(years,months,list.get(i),organcode)+"元，请及时缴费";
				NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+"'"+usercard+"'"+"&content="+content+"&sendAcc="+"'"+session.getLoginAccount()+"'")));
			}
			//更新组织党费
			modifyOrganInfos(session.getCurrentOrg().getOrgcode(),years,months);
			buffer.append("共"+(total)+"条数据,共导入成功"+(insertnum+updatenum)+"条数据;");
			if(num>0 || num1>0){
				message.fail("999999",buffer.toString());
			}else{
				message.success();
			}
			return message;

		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}

	private Map<String,Object> getHDyDues(String years,Integer months,String usercard) {
		return db().getMap("SELECT * FROM h_dy_dues where years=? and months=? and usercard=?",new Object[]{years,months,usercard});
	}


	private void updateHDyDues(String years,Integer months,List<String> list, String organcode) {
		Double basefee = Double.valueOf(
				dFormat.format(Double.valueOf(list.get(2))-Double.valueOf(list.get(3))-Double.valueOf(list.get(4))
						-Double.valueOf(list.get(5))-Double.valueOf(list.get(6))-Double.valueOf(list.get(7))
						-Double.valueOf(list.get(8))-Double.valueOf(list.get(9))
				));
		Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
		Double payable = Double.valueOf(dFormat.format(basefee*rate/100));
		db().update("UPDATE h_dy_dues SET username = ?, organcode = ?, wage = ?, deductionone = ?, deductiontwo = ?, " +
						"deductionthree = ?,deductionfour = ?, deductionfive = ?, deductionsix = ?, deductionseven = ?, " +
						"basefee = ?, rate = ?, payable = ?, remarks = ? WHERE years = ? and months = ? and usercard = ?",
				new Object[]{list.get(0),organcode,list.get(2),list.get(3),
						list.get(4),list.get(5),list.get(6),list.get(7),list.get(8),list.get(9),
						basefee,rate,payable,list.get(10),years,months,list.get(1)});
	}

	public void updateHDyDues2(String years,Integer months, String organcode,String usercard,Double actual,String remark) {

		db().update("update h_dy_dues set actual = ?,remarks=? where organcode = ? and years =? and months = ? and usercard = ?"
				,new Object[]{actual,remark,organcode,years,months,usercard});
	}

	public void insertHDyDues(String years,Integer months,List<String> list, String organcode) {
		Double basefee = Double.valueOf(
				dFormat.format(Double.valueOf(list.get(2))-Double.valueOf(list.get(3))-Double.valueOf(list.get(4))
								-Double.valueOf(list.get(5))-Double.valueOf(list.get(6))-Double.valueOf(list.get(7))
								-Double.valueOf(list.get(8))-Double.valueOf(list.get(9))
				));
		Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
		Double payable = Double.valueOf(dFormat.format(basefee*rate/100));
		db().insert("INSERT INTO h_dy_dues set years=?, months=?, usercard=?, username=?, organcode=?, wage=?, deductionone=?, " +
						"deductiontwo=?, deductionthree=?, deductionfour=?, deductionfive=?, deductionsix=?, deductionseven=?, " +
						"basefee=?, rate=?, payable=?, status=?, remarks=?",
				new Object[]{years,months,list.get(1),list.get(0),organcode,list.get(2),list.get(3),
						list.get(4),list.get(5),list.get(6),list.get(7),list.get(8),list.get(9),
						basefee,rate,payable,1,list.get(10)});
	}
	public void insertHDyDues2(String years,Integer months,List<String> list, String organcode,Double basefee) {
		//month月份是否在其他支部生成h_dy_dues数据，如果生成了则不能在本支部生成
		Map<String,Object> map2 = db().getMap("select * from h_dy_dues where years = ? and months = ? and usercard = ? and organcode!=?",new Object[]{years,months,list.get(1),organcode});
		if(map2==null){
			Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
			Double payable = Double.valueOf(dFormat.format(basefee*rate/100));
			db().insert("INSERT INTO h_dy_dues set years=?, months=?, usercard=?, username=?, organcode=?, wage=?, deductionone=?, " +
							"deductiontwo=?, deductionthree=?, deductionfour=?, deductionfive=?, deductionsix=?, deductionseven=?, " +
							"basefee=?, rate=?, payable=?, status=?, remarks=?",
					new Object[]{years,months,list.get(1),list.get(0),organcode,null,null,
							null,null,null,null,null,0,
							basefee,rate,payable,1,null});
		}
	}
	public void updateHDyDues3(String years,Integer months,List<String> list, String organcode,Double basefee) {

		Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
		Double payable = Double.valueOf(dFormat.format(basefee*rate/100));
		db().update("update h_dy_dues set basefee=?,payable = ?,rate=? where organcode = ? and years =? and months = ? and usercard = ?"
				,new Object[]{basefee,payable,rate,organcode,years,months,list.get(1)});

	}
	public void deleteHDyDues(String years,Integer months,String organcode,String usercard) {
		db().delete("delete from h_dy_dues where organcode = ? and years =? and months = ? and usercard = ?",new Object[]{organcode,years,months,usercard});
	}

	public void deleteHDyDues2(String years,Integer months,String organcode,String usercard) {
		db().delete("delete from h_dy_dues where organcode = ? and years =? and months = ? and usercard =?",new Object[]{organcode,years,months,usercard});
	}
	public Map<String,Object> getHDyDues(String years,String months,String organcode,String usercard) {
		return db().getMap("select * from h_dy_dues where years = ? and months = ? and usercard = ? and organcode=?",new Object[]{years,months,usercard,organcode});
	}
	private Double getPayable(String years,Integer months,List<String> list, String organcode) {
		Double basefee = Double.valueOf(
				dFormat.format(Double.valueOf(list.get(2))-Double.valueOf(list.get(3))-Double.valueOf(list.get(4))
						-Double.valueOf(list.get(5))-Double.valueOf(list.get(6))-Double.valueOf(list.get(7))
						-Double.valueOf(list.get(8))-Double.valueOf(list.get(9))
				));
		Double rate = Double.valueOf(getRate(basefee).get("ratio").toString());
		return Double.valueOf(dFormat.format(basefee*rate/100));
	}

	private int getValidate(int i, String idcard, String xm, StringBuffer buffer,Integer num) {
		//判断导入的姓名是否为空
		if(StringUtils.isEmpty(xm)){
			buffer.append("第"+i+"条数据姓名为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		//判断导入的idcard是否为空
		if(StringUtils.isEmpty(idcard)){
			buffer.append("第"+i+"条数据身份证号码为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		//判断身份证格式
//		if(!SysUtil.isIDNumber(idcard)){
//			buffer.append("第"+i+"条数据身份证号码有误;");
//			buffer.append("\r\n");
//			num++;
//			return num;
//		}
		return num;
	}
	/**
	 * 根据党员身份证号获取党员id
	 * @param idcard
	 * @return
	 */
	private Map<String, Object> getUseridOrgcodeByIdcard(String idcard,String xm) {
		Map<String, Object> map = db().getMap("select a.userid,b.organcode from t_dy_info a left join t_dzz_info b on a.organid=b.organid where zjhm = ? and xm = ? and dyzt = '1000000003' and a.delflag=0", new Object[] {idcard,xm});
		return map;
	}

	public Map<String, Object> getRate(Double dues) {
//		Map<String,Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and min<=? and max>? limit 1", new Object[] {1,dues,dues});
		Map<String, Object> jssz =db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[] {1, dues});
		if(jssz==null){
			jssz.put("ratio",0);
		}
		return jssz;
	}


	/**
	 * 确认
	 * @param infos
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void confirm(String infos, Session session) throws Exception {
		List<Map> datas = JSON.parseArray(infos,Map.class);
		//年份
		String years = DateEnum.YYYY.format();
		//月份
		int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
		String usercard = "";
		for (Map data : datas) {
			Map map = db().getMap("select * from  h_dy_dues WHERE years =? and months = ? and usercard = ?",new Object[]{years,months,data.get("usercard")});
			String content = "您的"+years+"年"+months+"月党费已交，实交金额为"+map.get("payable")+"元";
			NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+"'"+data.get("usercard")+"'"+"&content="+content+"&sendAcc="+"'"+session.getLoginAccount()+"'")));
			usercard += "'"+data.get("usercard").toString()+"'"+",";
		}
		usercard = usercard.substring(0,usercard.length()-1);
		db().update("UPDATE h_dy_dues SET  actual = payable, status = ? WHERE years =? and months = ? and usercard in ("+usercard+")",new Object[]{2,years,months});
		modifyOrganInfos(session.getCurrentOrg().getOrgcode(),years,months);
	}

	/**
	 * 党费信息编辑
	 * @param usercard
	 * @param actual
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void modify(String usercard, Double actual, Session session) throws Exception {
		//年份
		String years = DateEnum.YYYY.format();
		//月份
		int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
		db().update("UPDATE h_dy_dues SET  actual = ? WHERE years =? and months = ? and usercard = ?",new Object[]{actual,years,months,usercard});
		modifyOrganInfos(session.getCurrentOrg().getOrgcode(),years,months);
		String content = "您的"+years+"年"+months+"月党费已交，实交金额为"+actual+"元";
		NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+"'"+usercard+"'"+"&content="+content+"&sendAcc="+"'"+session.getLoginAccount()+"'")));
	}

	/**
	 * 党费信息去除
	 * @param usercard
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void removeInfo(String usercard, Session session) {
		db().delete("delete from h_dy_dues where usercard=?",new Object[]{usercard});
		//年份
		String years = DateEnum.YYYY.format();
		//月份
		int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
		modifyOrganInfos(session.getCurrentOrg().getOrgcode(),years,months);
	}

	/**
	 * 党费收交分页查询
	 * @param session
	 * @param pager
	 * @param months
	 * @param years
	 */
	public void infoPage(Session session, Pager pager, Integer months, String years,String username,String organcode) {
		if(StringUtils.isEmpty(username)) {
			db().pageMap(pager, "SELECT *,false flag  FROM h_dy_dues where years=? and months=? and organcode=? ", new Object[] {years,months,organcode});
		}else {
			db().pageMap(pager, "SELECT *,false flag FROM h_dy_dues where years=? and months=? and organcode=? and username like "+" '%"+username+"%' ", new Object[] {years,months,organcode});
		}
	}


	private void modifyOrganInfos(String organcode,String years,Integer months) {
		Map sum = db().getMap("select sum(payable) payable,sum(actual) actual from h_dy_dues where years=? and months=? and organcode=?",new Object[]{years,months,organcode});
		List<Map<String,Object>> lists = db().listMap("select * from h_dy_dues where years=? and months=? and organcode=?",new Object[]{years,months,organcode});
		//应收党费
		String receivable = sum.get("payable")==null ? "0" : dFormat.format(Double.valueOf(sum.get("payable").toString()));
		//实收党费
		String actualin = sum.get("actual")==null ? "0" : sum.get("actual").toString();
		//应交人数
		Integer duenum = db().getSingleColumn("select count(*) from t_dy_info a left join t_dzz_info b on a.organid=b.organid where b.organcode=? and dyzt='1000000003' and dylb in ('1000000001','2000000002')"
				,Integer.class,new Object[]{organcode});
		//实交人数
		Integer realnum = 0;
		//多交人数
		Integer morenum=0;
		//少交人数
		Integer lessnum=0;
		//免交人数
		Integer freenum = 0;
		//大额党费党费
		Integer bignum = 0;
		for (Map<String, Object> list : lists) {
			if(Integer.valueOf(list.get("status").toString())==2){
				//实交 （状态为2的）
				realnum++;
				//免交（状态为2已交并且实交金额为0的）
				if(Double.valueOf(list.get("actual").toString())==0.0){
					freenum++;
				}
				//多交 （状态为2并且实交金额大于应交金额的）
				if(Double.valueOf(list.get("actual").toString())>Double.valueOf(list.get("payable").toString())){
					morenum++;
				}
				//少交 （状态为2并且实交金额小于应交金额并且实交金额不为0的）
				if(Double.valueOf(list.get("actual").toString())<Double.valueOf(list.get("payable").toString())
						&& Double.valueOf(list.get("actual").toString())!=0.0){
					lessnum++;
				}
			}
			if(list.get("actual")!=null && Double.valueOf(list.get("actual").toString())>1000.0){
				bignum++;
			}
		}
		//未交人数（党支部人数减去实交人数）
		Integer unpaidnum = duenum-realnum;
		//父机构编码
		String porgcode = db().getSingleColumn("select parentcode from t_dzz_info where organcode=?",String.class,new Object[]{organcode});
		Map map = db().getMap("select * from h_dzz_duse where years=? and months=? and organcode = ?",new Object[]{years,months,organcode});
		if(map==null){
			db().insert("INSERT INTO h_dzz_duse set organcode=?, years=?, months=?, porgcode=?, receivable=?, actualin=?, duenum=?, realnum=?, morenum=?, lessnum=?, unpaidnum=?, freenum=?,remarks1=?"
					,new Object[]{organcode,years,months,porgcode,receivable,actualin,duenum,realnum,morenum,lessnum,unpaidnum,freenum,bignum});
		}else{
			db().update("UPDATE h_dzz_duse SET  porgcode=?, receivable=?, actualin=?, duenum=?, realnum=?, morenum=?, lessnum=?, unpaidnum=?, freenum=?,remarks1=? WHERE years =? and months = ? and organcode = ?"
					,new Object[]{porgcode,receivable,actualin,duenum,realnum,morenum,lessnum,unpaidnum,freenum,bignum,years,months,organcode});
		}
	}


	/**
	 * 党费巡查
	 * @param session
	 * @param pager
	 * @param months
	 * @param years
	 */
	public void patrolPage(Session session, Pager pager, Integer months, String years,String organcode,String username) {
		//判断组织类型（党委，党总支/党支部）
		Map map = db().getMap("select * from t_dzz_info where organcode=?",new Object[]{organcode});
		if(map!=null){
			//6200000035党委 6200000036党总支
			String type = map.get("zzlb").toString();
			if(dangwei.equals(type) || dangzongzhi.equals(type)){
				db().pageMap(pager,"select b.orgname,a.receivable,a.actualin,a.duenum,a.realnum,a.morenum,a.lessnum,a.unpaidnum,a.freenum from h_dzz_duse a left join urc_organization b on a.organcode=b.orgcode where years=? and months=? and (porgcode=? or organcode=?)",new Object[]{years,months,organcode,organcode});
				List<Map> lists = (List<Map>) pager.getRows();
				for (Map list : lists) {
					list.put("receivable",dFormat.format(Double.valueOf(list.get("receivable").toString())));
					list.put("actualin",dFormat.format(Double.valueOf(list.get("actualin").toString())));
				}
			}else{
				infoPage(session,pager,months,years,username,organcode);
			}
		}
	}

	/**
	 * 党组织党费汇总
	 * @param session
	 * @param months
	 * @param years
	 * @param organcode
	 * @return
	 */
	public Map<String,Object> summary(Session session, Integer months, String years, String organcode) {
		Map<String,Object> map = db().getMap("select ifnull(sum(receivable),0) receivable,ifnull(sum(actualin),0) actualin,ifnull(sum(duenum),0) duenum," +
						"ifnull(sum(realnum),0) realnum,ifnull(sum(morenum),0) morenum,ifnull(sum(lessnum),0) lessnum,ifnull(sum(unpaidnum),0) unpaidnum " +
						",ifnull(sum(freenum),0) freenum,ifnull(sum(remarks1),0) bignum from h_dzz_duse where years=? and months=? and (organcode=? or porgcode=?)"
				,new Object[]{years,months,organcode,organcode});
		map.put("receivable",dFormat.format(Double.valueOf(map.get("receivable").toString())));
		map.put("actualin",dFormat.format(Double.valueOf(map.get("actualin").toString())));
		return map;
	}

	/**
	 * 党费汇总人数详情分页查询
	 * @param session
	 * @param pager
	 * @param months
	 * @param years
	 * @param organcode
	 * @param type
	 */
	public void patrolDetailsPage(Session session, Pager pager, Integer months, String years, String organcode, Integer type) {
		String commentSql = "SELECT a.*,false flag,b.dzzmc organname  FROM h_dy_dues a left join t_dzz_info b on a.organcode=b.organcode where a.years=? and a.months=? and (b.parentcode=? or b.organcode=?)";
		final Object[] args = {years, months, organcode, organcode};
		switch (type){
			//应交人数
			case 1:
				db().pageMap(pager, commentSql, args);
				break;
			//实交人数
			case 2:
				db().pageMap(pager, commentSql + " and a.status=2", args);
				break;
			//多交人数
			case 3:
				db().pageMap(pager, commentSql + " and a.status=2 and a.actual>a.payable", args);
				break;
			//少交人数
			case 4:
				db().pageMap(pager, commentSql + " and a.status=2 and a.actual<a.payable", args);
				break;
			//未交人数
			case 5:
				db().pageMap(pager, commentSql + " and a.status=1", args);
				break;
			//免交人数
			case 6:
				db().pageMap(pager, commentSql + " and a.status=2 and a.actual=0.0", args);
				break;
			//大额人数
			case 7:
				db().pageMap(pager, commentSql + " and a.status=2 and a.actual>1000.0", args);
				break;
			default:
				break;
		}
	}


	/**
	 * 移动端我的党费查询
	 * @param session
	 * @param years
	 * @param usercard
	 * @return
	 */
	public List<Map<String,Object>> myPartyFee(Session session, String years, String usercard) {
		return db().listMap("select months,payable,actual from  h_dy_dues where years=? and usercard=?",new Object[]{years,usercard});
	}

	/**======================================淮矿党费收交结束============================================*/
	
	/** ===============================================工作经费设置======================================================*/

	/**
	 * Description: 查询当前机构下分类设置
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param session
	 * @param year
	 * @return
	 */
	public List<Map<String, Object>> list(Session session, Integer year) {
//		String orgid = session.getCustom();
		String orgid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Long compid= session.getCompid();
		List<Map<String, Object>> listMap = db().listMap("select a.*,b.clname, 'false' flag  from h_gzjf_set a left join h_df_class b on a.classid=b.id and a.compid=b.compid where a.compid=? and a.years=? and a.orgid=?", new Object[] {compid,year,orgid});
		return listMap;
	}


	@Transactional(rollbackFor = Exception.class)
	public void insertHGzjfSet(Session session, Integer year, String orgid, String classids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("compid", session.getCompid());
		paramMap.put("years", year);
		paramMap.put("orgid", orgid);
		paramMap.put("listamount", 0);
		paramMap.put("balance", 0);
		paramMap.put("amountspent", 0);
		paramMap.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap.put("status", 1);
		if(StringUtils.isEmpty(classids)) {
			List<Map<String, Object>> listMap2 = db().listMap("select * from h_df_class where compid=? and type=1 and status=1", new Object[] {session.getCompid()});
			if(listMap2==null || listMap2.size()<=0) {
				LehandException.throwException("请先设置工作经费分类");
			}
			for (Map<String, Object> map : listMap2) {
				String classid = map.get("id").toString();
				int pid = Integer.valueOf(map.get("pid").toString());
				if(pid==0) {//父节点，看看是否存在子节点
					Map<String,Object> map1 = db().getMap("select * from h_df_class where compid=? and pid=? and type=1 and status=1 limit 1", new Object[] {session.getCompid(),classid});
					if(map1==null) {
						paramMap.put("classid", classid);
						db().insert("insert into h_gzjf_set (compid, years, orgid, classid, listamount, balance, amountspent, moditime, status) values (:compid, :years, :orgid, :classid, :listamount, :balance, :amountspent, :moditime, :status)", paramMap);
					}
				}else {//子节点直接插入
					paramMap.put("classid", classid);
					db().insert("insert into h_gzjf_set (compid, years, orgid, classid, listamount, balance, amountspent, moditime, status) values (:compid, :years, :orgid, :classid, :listamount, :balance, :amountspent, :moditime, :status)", paramMap);
				}
			}
		}else {
			paramMap.put("classid", classids);
			Map<String,Object> map2 = db().getMap("select * from h_gzjf_set where years=? and orgid=? and classid=?", new Object[] {year,orgid,classids});
			if(map2==null) {
				db().insert("insert into h_gzjf_set (compid, years, orgid, classid, listamount, balance, amountspent, moditime, status) values (:compid, :years, :orgid, :classid, :listamount, :balance, :amountspent, :moditime, :status)", paramMap);
			}
		}
	}
	
	
	/**
	 * Description: 初始保存预算总金额
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param session
	 * @param year
	 * @param totlebudget
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveSummary(Session session, Integer year, String totlebudget) {
		Long compid = session.getCompid();
//		String orgid = session.getCustom();
		String orgid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Map<String,Object> map = db().getMap("select * from h_gzjf_summary where compid=? and years=? and orgid=?", new Object[] {compid,year,orgid});
		Map<String,Object> paramMap = new HashMap<String,Object>();	
		paramMap.put("compid", compid);
		paramMap.put("years", year);
		paramMap.put("orgid", orgid);
		paramMap.put("totlebudget", totlebudget);
		paramMap.put("listamount", 0);
		paramMap.put("balance", totlebudget);
		paramMap.put("amountspent", 0);
		paramMap.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		if(map==null){
			db().insert("insert into h_gzjf_summary (compid, years, orgid, totlebudget, listamount, balance, amountspent, moditime) values (:compid, :years, :orgid, :totlebudget, :listamount, :balance, :amountspent, :moditime)", paramMap);
		}else {
			Double oldamountspent = Double.valueOf(map.get("amountspent").toString());//已花费总金额
			Double newtotlebudget = Double.valueOf(totlebudget);
			if(oldamountspent.compareTo(newtotlebudget)==1) {//最新的预算总额小于已花费总额
				LehandException.throwException("预算总额小于已花费总额");
			}else {//最新预算总金额大于等于已花费总金额
				//列取金额总和
				Map<String,Object> sumlistamount = db().getMap("select sum(listamount) listamount from h_gzjf_set where compid=? and years=? and orgid=?", new Object[] {compid,year,orgid});
			    Double listamount = Double.valueOf(sumlistamount.get("listamount").toString());
			    if(newtotlebudget.compareTo(listamount)==-1) {//判断是否小于列取金额的总和(如果小于那么要修改相应的列取金额)
			    	LehandException.throwException("预算总额小于列取金额总和，请先修改相应的列取金额");
			    }
			}
			DecimalFormat df = new DecimalFormat("0.00");
			String balance = df.format(newtotlebudget-oldamountspent);
			paramMap.put("balance", balance);
			db().update("update h_gzjf_summary set totlebudget=:totlebudget,balance=:balance,moditime=:moditime where compid=:compid and years=:years and orgid=:orgid", paramMap);
		}
	}
	

	
	/**
	 * Description: 工作经费汇总
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param session
	 * @param year
	 * @return
	 */
	public Map<String,Object> getSummary(Session session, Integer year){
		return db().getMap("select * from h_gzjf_summary where compid=? and years=? and orgid=?",
				new Object[] {session.getCompid(),year,sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode())});
	}



	/**
	 * Description: 设置列取金额
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param session
	 * @param year
	 */
	@Transactional(rollbackFor = Exception.class)
	public void setListamount(Session session, Integer year, Integer classid,String listamount){
		Long compid = session.getCompid();
//		String orgid = session.getCustom();
		String orgid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		//查询该分类对象的已花费金额是否为0
		Map<String,Object> map = db().getMap("select * from h_gzjf_set where compid=? and orgid=? and years=? and classid=? limit 1", new Object[] {compid,orgid,year,classid});
		if(map==null) {
			LehandException.throwException("分类数据查询出错");
		}
		Double amountspent = Double.valueOf(map.get("amountspent").toString());//已花费的费用
		Double oldlistamount = Double.valueOf(map.get("listamount").toString());//列取的费用
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("listamount", listamount);
		paramMap.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap.put("status", 1);
		paramMap.put("compid", compid);
		paramMap.put("years", year);
		paramMap.put("orgid", orgid);
		paramMap.put("classid", classid);
		if(amountspent.compareTo(0.0)==0 || amountspent==null) {
			paramMap.put("balance", listamount);
			paramMap.put("amountspent", 0);
			db().update("update h_gzjf_set set listamount=:listamount,balance=:balance,amountspent=:amountspent,moditime=:moditime,status=:status where compid=:compid and years=:years and orgid=:orgid and classid=:classid", paramMap);
		}else {
			//判断列取费用是否小于已花费费用
			if(Double.valueOf(listamount).compareTo(amountspent)<0) {
				LehandException.throwException("列取金额小于已花费金额");
			}
			Map<String,Object> sum = db().getMap("select sum(listamount) listamount from h_gzjf_set where compid=? and years=? and orgid=?", new Object[] {compid,year,orgid});
			Double sumlistamount = Double.valueOf(sum.get("listamount").toString())+Double.valueOf(listamount)-oldlistamount;
			Map<String,Object> summary = getSummary(session, year);
			if(sumlistamount.compareTo(Double.valueOf(summary.get("totlebudget").toString()))>0) {
				LehandException.throwException("列取金额总和大于总预算");
			}
			Double balance = Double.valueOf(listamount)-Double.valueOf(map.get("amountspent").toString());
			NumberFormat  nf=new  DecimalFormat( "0.0 ");
			balance = Double.parseDouble(nf.format(balance));
			paramMap.put("balance", balance);
			paramMap.put("amountspent", map.get("amountspent"));
			db().update("update h_gzjf_set set listamount=:listamount,balance=:balance,amountspent=:amountspent,moditime=:moditime,status=:status where compid=:compid and years=:years and orgid=:orgid and classid=:classid", paramMap);
		}
		
		
		
	}



	/**
	 * Description: 年度结算
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param session
	 * @param year
	 */
	@Transactional(rollbackFor = Exception.class)
	public void settlement(Session session, Integer year) {
		Long compid = session.getCompid();
//		String orgid = session.getCustom();
		String orgcode = session.getCurrentOrg().getOrgcode();
		String orgid = sessionUtils.getOrganidByOrganCode(orgcode);
		if(StringUtils.isEmpty(orgid)){
			LehandException.throwException("组织机构不存在!");
		}
		//防重复点击
		Map<String,Object> map1 = db().getMap("select * from h_gzjf_set where years=? and orgid=? limit 1", new Object[] {year,orgid});
		if(map1!=null && map1.get("status").toString().equals("0")) {
			LehandException.throwException(year+"年度已经结算过了，请勿重复点击");
		}
		//将本年度的全部更新为status=0,然后新增下一年度的数据
		db().update("update h_gzjf_set set status=0 where compid=? and years=? and orgid=?", new Object[] {compid,year,orgid});
		Map<String,Object> map = db().getMap("select * from h_gzjf_set where years=? and orgid=? limit 1", new Object[] {year+1,orgid});
		if(map==null) {
			insertHGzjfSet(session, year+1, orgid,Constant.ENPTY);
		}
	}

	/** ===============================================工作经费申请======================================================*/	

	/**
	 * Description: 工作经费申请保存
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param feetype
	 * @param appfee
	 * @param appdesc
	 * @param appid
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveWorkFundingApplication(Session session, Integer feetype, String appfee, String appdesc,String appid) {
		int appflag = 1;
		Long userid = session.getUserid();
//		String organid = session.getCustom();
		String organid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		String apptime = DateEnum.YYYYMMDDHHMMDD.format();
		int status = 0;
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("appflag", appflag);
		paramMap.put("feetype", feetype);
		paramMap.put("userid", userid);
		paramMap.put("organid", organid);
		paramMap.put("appfee", appfee);
		paramMap.put("appdesc", appdesc);
		paramMap.put("apptime", apptime);
		paramMap.put("status", status);
//		paramMap.put("tagorgid", session.getSecondOrgan().getOrgfzdyid());
		Organs organs = sessionUtils.getSecondOrgan(session.getCurrorganid(),session.getCompid());
		if(organs == null){
			LehandException.throwException("不存在该组织的二级党委!");
		}
		paramMap.put("tagorgid",organs.getOrgfzdyid() );
		paramMap.put("apptype", 0);
		if(StringUtils.isEmpty(appid)) {
			Long id = db().insertReturnId("insert into h_dzz_dfsysq (appflag,feetype, userid, organid, appfee, appdesc, apptime, status,tagorgid,apptype) values (:appflag, :feetype, :userid, :organid, :appfee, :appdesc, :apptime, :status, :tagorgid, :apptype)", paramMap,null);
			/**================================================创建审核信息逐级审核直到二级党组织==============================================================*/
			Organs upOrgan = sessionUtils.getUpOrganByOrgid(session.getCurrentOrg().getPorgid(),session.getCompid());

			dfglDfsyHandle.insertMyToDoList(6, id.toString(),"《"+appdesc+"》", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), upOrgan.getUserid().toString());
			addUpOrganAuditInfo(session, id, 1, 0);
		}else {
			paramMap.put("appid", appid);
			db().update("update h_dzz_dfsysq set feetype=:feetype,appfee=:appfee,appdesc=:appdesc,apptime=:apptime where appid=:appid", paramMap);
			Map<String,Object> map = db().getMap("select * from h_dzz_dfsysq where appid=?", new Object[] {appid});
			if(map!=null && map.get("status").toString().equals("2")) {
				dfglDfsyHandle.insertMyToDoList(6, appid,"《"+appdesc+"》", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), session.getUpOrgan().getUserid().toString());
				addUpOrganAuditInfo(session, Long.valueOf(appid), 1, 0);
			}
		}
	}

	
	
	/**
	 * Description: 工作/报销 经费申请审批
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param id
	 * @param classid(工作经费申请审批时传空串，报销经费审批时传最终的分类id)
	 */
	@Transactional(rollbackFor = Exception.class)
	public void auditWorkFundingApplication(Session session, Long id, int result, String appmesg, String classid) {
		String organid  = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		db().update("update h_dzz_sprz set result=?,appmesg=?,remarks3=? where id=?", new Object[] {result,appmesg,DateEnum.YYYYMMDDHHMMDD.format(),id});
		
		/**================================================创建审核信息逐级审核直到二级党组织==============================================================*/
		
		Map<String,Object> map = db().getMap("select * from h_dzz_sprz where id=?", new Object[] {id});
		//二级党组织信息
//		Organs secondOrgan = session.getSecondOrgan();
		Organs secondOrgan = sessionUtils.getSecondOrgan(session.getCurrorganid(),session.getCompid());
		if(!StringUtils.isEmpty(classid) && organid.equals(secondOrgan.getOrgfzdyid())) {//只有报销申请的二级党委审核时可以修改分类，其他的都不可以修改
			db().update("update h_dzz_dfsysq set feetype=? where appid=?", new Object[] {classid,map.get("appid")});
		}
		if(map==null) {
			LehandException.throwException("记录不存在");
		}
		if(result==2) {//拒绝
			db().update("update h_dzz_dfsysq set status=2 where appid=?", new Object[] {map.get("appid")});
			return;
		}
		Map<String,Object> map1 = db().getMap("select * from h_dzz_dfsysq where appid=?", new Object[] {map.get("appid")});
		if(map1==null) {
			LehandException.throwException("工作经费申请已变更，请返回。");
		}
		//判断当前组织是否是二级党组织
		if(secondOrgan.getOrgid()!=null && !organid.equals(secondOrgan.getOrgfzdyid())) {//不是二级党组织
			//上级党组织信息
//			Organs upOrgan = session.getUpOrgan();
			Organs upOrgan = sessionUtils.getUpOrganByOrgid(session.getCurrentOrg().getPorgid(),session.getCompid());
			//待办
			dfglDfsyHandle.insertMyToDoList(6, map.get("appid").toString(),"《"+map1.get("appdesc")+"》", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), upOrgan.getUserid().toString());
			Map<String,Object> paramMap2 = new HashMap<String,Object>();
			paramMap2.put("type", 1);
			paramMap2.put("appid", map.get("appid"));
			paramMap2.put("auditorid", upOrgan.getUserid());
			paramMap2.put("apptime", DateEnum.YYYYMMDDHHMMDD.format());
			paramMap2.put("userid", session.getUserid());
			paramMap2.put("organid", organid);
			paramMap2.put("result", 0);
			paramMap2.put("remarks1", map1.get("apptype"));
			db().insert("insert into h_dzz_sprz (type, appid, auditorid, apptime, userid, organid, result, remarks1) values (:type, :appid, :auditorid, :apptime, :userid, :organid, :result, :remarks1)", paramMap2);
		}else {//二级党组织通过后更新申请表的状态为通过
			if(!StringUtils.isEmpty(classid)) {//二级党委审核通过报销申请后将申请状态修改成完成
				db().update("update h_dzz_dfsysq set status=3 where appid=?", new Object[] {map.get("appid")});
				//2019-09-20 添加
				//二级党委审核通过后需要根据分类去扣除相应的费用
				Double sumfee = Double.valueOf(map1.get("sumfee").toString());
				SimpleDateFormat sim1 = new SimpleDateFormat("yyyy"); 
				int year = Integer.valueOf(sim1.format(DateEnum.now()));
				Map<String,Object> map2 = db().getMap("select * from h_gzjf_set where classid=? and orgid=? and years=? and status=1", new Object[] {classid,organid,year});
				if(map2==null) {
					LehandException.throwException(year+"年度工作经费未配置或者已失效");
				}
				//已花费经额
				Double amountspent = Double.valueOf(map2.get("amountspent").toString());
				//剩余经额
				Double balance = Double.valueOf(map2.get("balance").toString());
				DecimalFormat df = new DecimalFormat("0.00");
				//计算分类中花费经额和剩余经额
				String amountspent1 = df.format(amountspent+sumfee);
				String balance1 = df.format(balance-sumfee);
				db().update("update h_gzjf_set set amountspent=?,balance=? where classid=? and orgid=? and years=? and status=1", new Object[] {amountspent1,balance1,classid,organid,year});
				//还需更新工作经费汇总表的已花费金额和剩余金额
				Map<String,Object> map3 = db().getMap("select * from h_gzjf_summary where orgid=? and years=?", new Object[] {organid,year});
				//剩余
				Double shengyujine = Double.valueOf(map3.get("balance").toString());
				//已花费金额
				Double yihuafeijine = Double.valueOf(map3.get("amountspent").toString());
				
				Double yihuafeijine1 = Double.valueOf(df.format(yihuafeijine+sumfee));
				Double shengyujine1 = Double.valueOf(df.format(shengyujine-sumfee));
				db().update("update h_gzjf_summary set amountspent=?,balance=? where orgid=? and years=? ", new Object[] {yihuafeijine1,shengyujine1,organid,year});
				
			}else {//二级党委审核通过党费申请后将申请状态改成通过
				db().update("update h_dzz_dfsysq set status=1 where appid=?", new Object[] {map.get("appid")});
			}
		}
	}

	
	/**
	 * Description: 获取申请审核的记录
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param id
	 * @return
	 */
	public Map<String, Object> getWorkFundingApplication(Session session, Long id) {
		Map<String, Object> map = db().getMap("select a.*,b.clname from h_dzz_dfsysq a left join h_df_class b on a.feetype=b.id where a.appid=?", new Object[] {id});
		if(map==null) {
			LehandException.throwException("记录不存在");
		}
		String remarks3 = String.valueOf(map.get("remarks3"));
		if(!StringUtils.isEmpty(remarks3)) {
			List<Map<String, Object>> map3 = db().listMap("select * from dl_file where id in ("+remarks3+")", new Object[] {});
			if(map3!=null && map3.size()>0) {
				for (Map<String, Object> map2 : map3) {
					map2.put("dir", down_ip+map2.get("dir"));
				}
			}
			map.put("remarks3", map3);
		}
		String remarks4 =  String.valueOf(map.get("remarks4"));
		if(!StringUtils.isEmpty(remarks4)) {
			List<Map<String, Object>> map4 = db().listMap("select * from dl_file where id in ("+remarks4+")", new Object[] {});
			if(map4!=null && map4.size()>0) {
				for (Map<String, Object> map2 : map4) {
					map2.put("dir", down_ip+map2.get("dir"));
				}
			}
			map.put("remarks4", map4);
		}
		return map;
	}

	
	
	/**
	 * Description: 报销申请保存
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param flag 新增传0 修改传1
	 * @param appid  申请id
	 * @param sumfee 报销金额
	 * @param feelist 金额明细
	 * @param remarks3  发票附件
	 * @param remarks4 申请单附件
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveReimbursementFundingApplication(Session session, int flag,
			String appid, String sumfee, String feelist, String remarks3, String remarks4) {
		String apptime = DateEnum.YYYYMMDDHHMMDD.format();
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("appid", appid);
		paramMap.put("apptime", apptime);
		paramMap.put("apptype", 1);
		paramMap.put("sumfee", sumfee);
		paramMap.put("feelist", feelist);
		paramMap.put("remarks3", remarks3);
		paramMap.put("remarks4", remarks4);
		paramMap.put("status", 0);
		if(flag==0) {
			db().update("update h_dzz_dfsysq set status=:status, sumfee=:sumfee,feelist=:feelist,remarks3=:remarks3,remarks4=:remarks4,apptype=:apptype,apptime=:apptime where appid=:appid", paramMap);
			/**================================================创建审核信息逐级审核直到二级党组织==============================================================*/
			addUpOrganAuditInfo(session, Long.valueOf(appid), 1, 1);
		}else {
			Map<String,Object> map = db().getMap("select * from h_dzz_dfsysq where appid=?", new Object[] {appid});
			db().update("update h_dzz_dfsysq set status=:status, sumfee=:sumfee,feelist=:feelist,remarks3=:remarks3,remarks4=:remarks4,apptime=:apptime where appid=:appid", paramMap);
			if(map.get("status").toString().equals("2")) {//驳回状态重新编辑
				addUpOrganAuditInfo(session, Long.valueOf(appid), 1, 1);
			}
		}
		
	}

	/**
	 * Description: 新增上级党组织审核信息
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param id
	 */
	@Transactional(rollbackFor = Exception.class)
	public void addUpOrganAuditInfo(Session session, Long id, int type, int remarks1) {
		//上级党组织信息
//		Organs upOrgan = session.getUpOrgan();
		Organs upOrgan = sessionUtils.getUpOrganByOrgid(session.getCurrentOrg().getPorgid(),session.getCompid());
		Map<String,Object> paramMap2 = new HashMap<String,Object>();
		paramMap2.put("type", type);
		paramMap2.put("appid", id);
		paramMap2.put("auditorid", upOrgan.getUserid());
		paramMap2.put("apptime", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap2.put("userid", session.getUserid());
//		paramMap2.put("organid", session.getCustom());
		paramMap2.put("organid", sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()));
		paramMap2.put("result", 0);
		paramMap2.put("remarks1", remarks1);
		db().insert("insert into h_dzz_sprz (type, appid, auditorid, apptime, userid, organid, result,remarks1) values (:type, :appid, :auditorid, :apptime, :userid, :organid, :result, :remarks1)", paramMap2);
	}
	
	
	
	/**
	 * Description: 二级党委报销审核时点击分类显示该分类金额的具体信息
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param classid
	 * @return
	 */
	public Map<String, Object> getClassInfo(Session session, Long classid) {
		SimpleDateFormat sim = new SimpleDateFormat("yyyy");
		String year = sim.format(DateEnum.now());
		return db().getMap("select * from h_gzjf_set where compid=? and years=? and orgid=? and classid=? ",
				new Object[] {session.getCompid(),year,sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()),classid});
	}

	
	
	/**
	 * Description: 获取审批日志
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param appid
	 * @param type 工作经费传1 党费传0
	 * @return
	 */
	public List<Map<String, Object>> listApprovalLog(Session session, Long appid, Integer type) {
//		return db().listMap("select a.remarks3 apptime,a.result,a.appmesg,a.auditorid,a.remarks1,b.orgname from h_dzz_sprz a left join pw_organ b on a.auditorid=b.remarks3 where a.appid=? and a.type=? and a.result!=0 order by a.apptime desc", new Object[] {appid,type});
		return db().listMap("select a.remarks3 apptime,a.result,a.appmesg,a.auditorid,a.remarks1,b.orgname from h_dzz_sprz a left join urc_organization b on a.auditorid=b.remarks3 where a.appid=? and a.type=? and a.result!=0 order by a.apptime desc", new Object[] {appid,type});
	}

	
	
	/**
	 * Description: 工作经费申请分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param pager
	 * @param name
	 * @return
	 */
	public Pager applicationPage(Session session, Pager pager, int type, String name) {
		if(StringUtils.isEmpty(name)) {
			db().pageMap(pager, "select a.*,b.clname from h_dzz_dfsysq a left join h_df_class b on a.feetype = b.id where a.userid=? and a.appflag=? and a.remarks1!=1 order by a.apptime desc", new Object[] {session.getUserid(),type});
		}else {
			if(type==1) {
				db().pageMap(pager, "select a.*,b.clname from h_dzz_dfsysq a left join h_df_class b on a.feetype = b.id where a.userid=? and a.appflag=? and a.remarks1!=1 and a.appdesc like '%"+name+"%' order by a.apptime desc", new Object[] {session.getUserid(),type});
			}else {
				db().pageMap(pager, "select a.*,b.clname from h_dzz_dfsysq a left join h_df_class b on a.feetype = b.id where a.userid=? and a.appflag=? and a.remarks1!=1 and a.title like '%"+name+"%' order by a.apptime desc", new Object[] {session.getUserid(),type});
			}
		}
		return pager;
	}

	
	
	/**
	 * Description: 工作经费审批分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param pager
	 * @param type
	 * @param name
	 * @param status
	 * @return
	 */
	public Pager auditPage(Session session, Pager pager, int type, String name, int status) {
//		Map<String,Object> map = db().getMap("select remarks3 from pw_organ where compid=? and id=?", new Object[] {session.getCompid(),session.getCurrorganid()});
		Map<String,Object> map = db().getMap("select remarks3 from urc_organization where compid=? and orgid = ?", new Object[] {session.getCompid(),session.getCurrorganid()});
		Object userid = map.get("remarks3");
		if(StringUtils.isEmpty(name)) {
			if(status==0) {
				db().pageMap(pager, "SELECT a.id,a.result,a.apptime submittime,a.remarks1 panduan, b.*,c.dzzmc orgname FROM h_dzz_sprz a left join  h_dzz_dfsysq b on a.appid=b.appid left join t_dzz_info c on b.organid=c.organid where a.auditorid=? and a.type=? and a.result=?  order by a.apptime desc", new Object[] {userid,type,status});
			}else {
				db().pageMap(pager, "SELECT a.id,a.result,a.apptime submittime,a.remarks1 panduan, b.*,c.dzzmc orgname FROM h_dzz_sprz a left join  h_dzz_dfsysq b on a.appid=b.appid left join t_dzz_info c on b.organid=c.organid where a.auditorid=? and a.type=? and a.result in (1,2)  order by a.apptime desc", new Object[] {userid,type});
			}
		}else {
			if(type==1) {
				if(status==0) {
					db().pageMap(pager, "SELECT a.id,a.result,a.apptime submittime,a.remarks1 panduan, b.*,c.dzzmc orgname FROM h_dzz_sprz a left join  h_dzz_dfsysq b on a.appid=b.appid left join t_dzz_info c on b.organid=c.organid where a.auditorid=? and a.type=? and a.result=? and b.appdesc like '%"+name+"%' order by a.apptime desc", new Object[] {userid,type,status});
				}else {
					db().pageMap(pager, "SELECT a.id,a.result,a.apptime submittime,a.remarks1 panduan, b.*,c.dzzmc orgname FROM h_dzz_sprz a left join  h_dzz_dfsysq b on a.appid=b.appid left join t_dzz_info c on b.organid=c.organid where a.auditorid=? and a.type=? and a.result in (1,2) and b.appdesc like '%"+name+"%' order by a.apptime desc", new Object[] {userid,type});
				}
			}else {
				if(status==0) {
					db().pageMap(pager, "SELECT a.id,a.result,a.apptime submittime,a.remarks1 panduan, b.*,c.dzzmc orgname FROM h_dzz_sprz a left join  h_dzz_dfsysq b on a.appid=b.appid left join t_dzz_info c on b.organid=c.organid where a.auditorid=? and a.type=? and a.result=? and b.title like '%"+name+"%' order by a.apptime desc", new Object[] {userid,type,status});
				}else {
					db().pageMap(pager, "SELECT a.id,a.result,a.apptime submittime,a.remarks1 panduan, b.*,c.dzzmc orgname FROM h_dzz_sprz a left join  h_dzz_dfsysq b on a.appid=b.appid left join t_dzz_info c on b.organid=c.organid where a.auditorid=? and a.type=? and a.result in (1,2) and b.title like '%"+name+"%' order by a.apptime desc", new Object[] {userid,type});
				}
			}
		}
		return pager;
	}

	/**==================================================费用管理一二级党组织专用=============================================================*/	
	
	/**
	 * Description: 一二级机构专用申请保存 无需审核就登记数据用
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param appflag 0党费申请，1工作经费申请
	 * @param feetype 分类id
	 * @param appfee  申请金额
	 * @param appdesc  事项
	 * @param appid   申请的id新增传空
	 * @param retain  自留费用拨款额度     只有党费申请时才有工作经费申请时传空串
	 * @param allocate 下拨党费拨款额度  只有党费申请时才有工作经费申请时传空串
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveApplication(Session session, int appflag, int feetype, String appfee, String appdesc, String appid,
			String retain, String allocate) {
		
		Long userid = session.getUserid();
//		String organid = session.getCustom();
		String organid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		String apptime = DateEnum.YYYYMMDDHHMMDD.format();
		int status = 3;
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("appflag", appflag);
		paramMap.put("feetype", feetype);
		paramMap.put("userid", userid);
		paramMap.put("organid", organid);
		paramMap.put("appfee", appfee);
		paramMap.put("appdesc", appdesc);
		paramMap.put("apptime", apptime);
		paramMap.put("status", status);
		Organs organs = sessionUtils.getSecondOrgan(session.getCurrorganid(), session.getCompid());
        if(organs == null) {
            LehandException.throwException("当前组织没有二级党委！");
        }
		paramMap.put("tagorgid", organs.getOrgfzdyid());
		paramMap.put("apptype", 0);
		paramMap.put("retain", retain);
		paramMap.put("allocate", allocate);
		paramMap.put("remarks1", 1);
		db().insert("insert into h_dzz_dfsysq (appflag,feetype, userid, organid, appfee, appdesc, apptime, status,tagorgid,apptype,retain,allocate,remarks1) values (:appflag, :feetype, :userid, :organid, :appfee, :appdesc, :apptime, :status, :tagorgid, :apptype, :retain, :allocate, :remarks1)", paramMap);
	
		if(appflag==0) {
			//走账成功后更新自己单位的余额
			DecimalFormat df = new DecimalFormat("0.00");
			Map<String,Object> map3 = db().getMap("select * from h_dzz_fundfee where organid=? ", new Object[] {organid});
			Double allocate2 = Double.valueOf(map3.get("allocate").toString());
			Double selffee = Double.valueOf(map3.get("selffee").toString());
			String allocate3 = df.format(allocate2-Double.valueOf(allocate));
			String selffee2 = df.format(selffee-Double.valueOf(retain));
			db().update("update h_dzz_fundfee set allocate=?,selffee=? where organid=?", new Object[] {allocate3,selffee2,organid});
		}
	}
	
	
	/**
	 * Description: 各机构自己花钱自己走账列表查询
	 * @author PT  
	 * @date 2019年9月11日 
	 * @param session
	 * @param pager
	 * @param name
	 * @return
	 */
	public Pager page(Session session, Pager pager,String name) {
		if(StringUtils.isEmpty(name)) {
			db().pageMap(pager, "select a.*,b.clname from h_dzz_dfsysq a left join h_df_class b on a.feetype = b.id where a.userid=? and a.remarks1=1 order by a.apptime desc", new Object[] {session.getUserid()});
		}else {
			db().pageMap(pager, "select a.*,b.clname from h_dzz_dfsysq a left join h_df_class b on a.feetype = b.id where a.userid=? and a.remarks1=1 and a.appdesc like '"+name+"' order by a.apptime desc", new Object[] {session.getUserid()});
		}
		return pager;
	}


}
