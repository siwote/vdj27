package com.lehand.developing.party.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.developing.party.pojo.FzdyRemindrule;
import com.lehand.developing.party.pojo.MsgTemplate;
import com.lehand.developing.party.service.FzdyRemindRuleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(value = "提醒规则服务", tags = { "提醒规则服务" })
@RestController
@RequestMapping("/developing/remindrule")
public class FzdyRemindRuleController extends FzdyBaseController {
	
	private static final Logger logger = LogManager.getLogger(FzdyRemindRuleController.class);
	
	@Resource FzdyRemindRuleService fzdyRemindRuleService;

	@OpenApi(optflag=1)
	@ApiOperation(value = "保存消息模板", notes = "保存消息模板", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "tlcode", value = "模板编码", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "tlname", value = "模板名称", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "cond", value = "条件", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "channel", value = "消息发送渠道", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "msgtpl", value = "消息模板", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "msgurltpl", value = "消息链接地址模板", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "status", value = "状态 0 关 1 开", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/saveMsgTemp")
	public Message saveMsgTemp(@ApiIgnore MsgTemplate info) {
		Message message = null;
		try {
			boolean success = false;
			if(StringUtils.isEmpty(info.getTlcode())) {
				success = fzdyRemindRuleService.addMsgTemp(info, getSession());
			}else {
				success = fzdyRemindRuleService.updateMsgTemp(info, getSession());
			}
			
			if(!success) {
				message = Message.FAIL;
				message.setMessage("消息模板保存失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("保存成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=3)
	@ApiOperation(value = "删除消息模板", notes = "删除消息模板", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "tlcode", value = "模板编码", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/delMsgTemplate")
	public Message delMsgTemplate(String tlcode) {
		Message message = null;
		try {
			if(StringUtils.isEmpty(tlcode)) {
				message = Message.FAIL;
				message.setMessage("消息模板编码不可为空");
			}
			Boolean success = fzdyRemindRuleService.delMsgTemp(tlcode, 1, getSession());
			if(!success) {
				message = Message.FAIL;
				message.setMessage("消息模板删除失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("删除成功");
			
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "分页查询消息模板列表", notes = "分页查询消息模板列表", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "pageNo", value = "当前页码", required = true, paramType = "query", dataType = "Long", defaultValue = "1"),
		@ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "Long", defaultValue = "10"),
		@ApiImplicitParam(name = "sort", value = "排序字段", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "direction", value = "排序方式  asc  desc", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/pageMsgTemplate")
	public Message queryPageMsgTemplate(@ApiIgnore Pager pager) {
		Message message = null;
		try {
			Pager result = fzdyRemindRuleService.queryPageMsgTemp(pager, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("分页查询成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "查询提醒点下拉框值", notes = "查询提醒点下拉框值", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤编码", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/queryFzdyDict")
	public Message queryFzdyDict(Long typeid) {
		Message message = null;
		try {
			List<Map<String, Object>> result = fzdyRemindRuleService.getDictList(typeid, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("查询成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	@OpenApi(optflag=0)
	@ApiOperation(value = "分页查询提醒规则列表", notes = "分页查询提醒规则列表", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "pageNo", value = "当前页码", required = true, paramType = "query", dataType = "Long", defaultValue = "1"),
		@ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "Long", defaultValue = "10"),
		@ApiImplicitParam(name = "sort", value = "排序字段", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "direction", value = "排序方式  asc  desc", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤编码", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "status", value = "状态  1 常规 2 逾期提醒", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/pageRemindRule")
	public Message queryRemindRule(@ApiIgnore Pager pager,Long typeid,Integer status) {
		Message message = null;
		try {
			Pager result = fzdyRemindRuleService.queryRemind(pager, typeid, status, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("分页查询提醒规则成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "查询规则详情", notes = "查询规则详情", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "提醒规则id", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/queryRuleById")
	public Message queryRemindRuleById(Long id) {
		Message message = null;
		try {
			Map<String, Object> result = fzdyRemindRuleService.queryRuleById(id, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("查询提醒规则详情成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=3)
	@ApiOperation(value = "删除提醒规则", notes = "删除提醒规则", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "提醒规则id", required = true, paramType = "query", dataType = "Long", defaultValue = "")
	})
	@RequestMapping("/delRemindRuleById")
	public Message delRemindRuleById(Long id) {
		Message message = null;
		try {
			if(null == id) {
				message = Message.FAIL;
				message.setMessage("规则id不可为空");
				return message;
			}
			boolean success = fzdyRemindRuleService.delRemindRule(id, getSession());
			//判断是否成功
			if(!success) {
				message = Message.FAIL;
				message.setMessage("提醒规则删除失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("删除提醒规则成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=1)
	@ApiOperation(value = "保存提醒规则", notes = "保存提醒规则", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "提醒规则id", required = false, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤分类编码", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "remindtype", value = "1 常规提醒  2 逾期提醒", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "starttstage", value = "提醒开始节点编码，下拉框中id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "enddate", value = "提醒结束时间", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "unit", value = "单位 Y 年 M 月 D 日", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "tempid", value = "消息模板tlcode", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "dates", value = "提醒点JSON数据[{}]，逾期提醒传递空", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "remark3", value = "逾期提醒传递时间点", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/saveRemindRule")
	public Message saveRemindRule(@ApiIgnore FzdyRemindrule rule,String dates) {
		Message message = null;
		try {
			boolean success = false;
			if(StringUtils.isEmpty(rule.getId())) {
				success = fzdyRemindRuleService.addRemindRule(rule, dates, getSession());
			}else {
				success = fzdyRemindRuleService.updateRemindRule(rule, dates, getSession());
			}
			
			//判断是否成功
			if(!success) {
				message = Message.FAIL;
				message.setMessage("提醒规则保存失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("保存成功");
			
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
}
