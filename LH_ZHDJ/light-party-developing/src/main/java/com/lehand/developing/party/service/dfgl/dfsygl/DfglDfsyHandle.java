package com.lehand.developing.party.service.dfgl.dfsygl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Organs;
import com.lehand.base.common.Session;
import com.lehand.developing.party.utils.SessionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.service.dfgl.gzjfgl.DfglGzjfHandle;
@Service
public class DfglDfsyHandle {

	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	@Resource
	protected SessionUtils sessionUtils;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	@Resource private DfglGzjfHandle dfglGzjfHandle;
	
	

	/**
	 * Description: 费用类别查询
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> list(Session session, Integer type) {
		Long compid = session.getCompid();
		// 返回一个List<Map<String, Object>>
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		// 返回一个TkTaskClass对象的集合
		List<Map<String, Object>> rows = db().listMap(" select * from h_df_class where compid=? and status=? and type=? order by pid asc,seqno desc",new Object[] {compid,1,type});
		// 对上面的rows集合进行数据分析处理
		Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(rows.size());
		// 对集合数据进行遍历处理
		for (Map<String, Object> row : rows) {
			Long id = Long.valueOf(row.get("id").toString());
			Long pid = Long.valueOf(row.get("pid").toString());
			// 返回的数据里面共有的信息（id和name）
			Map<String, Object> line = new HashMap<String, Object>(3);
			line.put("id", id);
			line.put("label", row.get("clname"));
			line.put("pid", pid);
			line.put("isshow", false);
			line.put("isedit", false);
			line.put("clickFlag",false);
			//判断是否是根节点
			if (pid <= 0) {//是
				// 父节点的话再将下面的子集put进去
				line.put("children", new ArrayList<Map<String, Object>>());
				// 再将集合添加到结果集中
				result.add(line);
			} else {//不是
				if (map.get(pid)!=null) {//有
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					childrens.add(line);
				}else {//没有
					result.add(line);
				}
			}
			map.put(id, line);
		}
		return result;
	}




	/**
	 * Description:分类新增/修改
	 * 
	 * @author PT
	 * @date 2019年9月3日
	 * @param session
	 * @param type 分类类型(0:党费,1:工作经费)
	 * @param pid
	 * @param name
	 * @param id 新增传空串 修改传id
	 */
	@Transactional(rollbackFor = Exception.class)
	public void save(Session session, int type, int pid, String name, String id) {
		if(StringUtils.isEmpty(name)) {
			LehandException.throwException("分类名称不能为空");
		}
		Long compid = session.getCompid();
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("compid", compid);
		paramMap.put("type", type);
		paramMap.put("flag", 0);
		paramMap.put("clname", name);
		paramMap.put("pid", pid);
		paramMap.put("status", 1);
		paramMap.put("seqno", System.currentTimeMillis());
		if(StringUtils.isEmpty(id)) {//新增
			if(pid!=0) {//子节点，判断父节点是否被使用
				Map<String,Object> map = db().getMap("select * from h_dzz_dfsysq where feetype=? limit 1", new Object[] {pid});
				if(map!=null) {
					LehandException.throwException("该分类已被使用，不可以添加子节点");
				}
				if(type==1) {//工作经费
					Map<String,Object> map1 = db().getMap("select * from h_gzjf_set where classid=? and compid=? limit 1", new Object[] {pid,compid});
					if(map1!=null) {
						LehandException.throwException("该分类已被使用，不可以添加子节点");
					}
				}
			}
			db().insert("insert into h_df_class (compid, type, flag, clname, pid, seqno, status) values (:compid, :type, :flag, :clname, :pid, :seqno, :status)", paramMap);
		}else {
			paramMap.put("id", id);
			db().update("update h_df_class set clname=:clname where compid=:compid and id=:id", paramMap);
		}
	}




	/**
	 * Description: 分类删除
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param session
	 * @param id
	 */
	@Transactional(rollbackFor = Exception.class)
	public void delete(Session session, String id) {
		//首先查询该分类是否被使用
		Map<String,Object> map = db().getMap("select * from h_dzz_dfsysq where feetype=? limit 1", new Object[] {id});
		Map<String,Object> map1 = db().getMap("select * from h_gzjf_set where classid=? limit 1", new Object[] {id});
		if(map!=null || map1!=null) {
			LehandException.throwException("该分类已被使用不允许被删除。");
		}
		db().delete("delete from h_df_class where compid=? and id=?", new Object[] {session.getCompid(),id});
	}
	
	
	/**
	 * 任务分类拖拽  
	 * 			如果flag=1 那么 srcpid = tagpid 将tag下面的节点序号+2（不包括src本身）并且讲src的序号改为tag序号-1
	 *          如果flag=2 那么srcpid = tagpid 将tag上面的节点序号-2（不包括src本身）并且将src的序号改为tag序号+1
	 * @author pantao
	 * @date 2018年11月7日上午10:37:25
	 *
	 * @param srcid 拖动组织
	 * @param tagid 拖动到哪个组织
	 * @param flag  1:上面,2:下面
	 */
	@Transactional(rollbackFor = Exception.class)
	public void move(Long srcid, Long tagid, int flag,Long compid) throws LehandException {
		Map<String,Object> tag = db().getMap("select seqno,pid from h_df_class where compid=? and id=?", new Object[] {compid,tagid});// 目标父节点
		int tagPid = Integer.valueOf(tag.get("pid").toString());
		Long num = null;
		num = System.currentTimeMillis();
		Object[] args = new Object[] {compid,srcid,tagPid,tag.get("seqno")};
		if (flag == 1) {
			db().update("update h_df_class set seqno = seqno-2 where compid=? and id != ? and pid = ? and seqno <= ?", args);
			num = Long.valueOf(tag.get("seqno").toString()) + 1;
		} else {
			db().update("update h_df_class set seqno = seqno+2  where compid=? and id != ? and pid = ? and seqno >= ?",args);
			num = Long.valueOf(tag.get("seqno").toString()) - 1;
		}
		db().update("UPDATE h_df_class SET  pid = ? , seqno = ?  WHERE compid=? and id = ?", new Object[] {tagPid,num,compid,srcid});
	}


	/**======================================================党费使用申请===============================================================*/


	/**
	 * Description: 党费使用申请保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param session
	 * @param title
	 * @param feetype
	 * @param appfee
	 * @param appdesc
	 * @param appid
	 * @param json  关联申请单ID列表(appid和title数组的JSON字符串)
	 */
	@Transactional(rollbackFor = Exception.class)
	public void savePartyApplication(Session session, String title,Integer feetype, String appfee, String appdesc,String appid,String json) {
		int appflag = 0;
		Long userid = session.getUserid();
//		String organid = session.getCustom();
		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		String apptime = DateEnum.YYYYMMDDHHMMDD.format();
		int status = 0;
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("appflag", appflag);
		paramMap.put("feetype", feetype);
		paramMap.put("userid", userid);
		paramMap.put("organid", organid);
		paramMap.put("appfee", appfee);
		paramMap.put("appdesc", appdesc);
		paramMap.put("apptime", apptime);
		paramMap.put("status", status);
        Organs organs = sessionUtils.getSecondOrgan(session.getCurrorganid(), session.getCompid());
        if(organs == null) {
            LehandException.throwException("当前组织没有二级党委！");
        }
		paramMap.put("tagorgid", organs.getOrgfzdyid());
		paramMap.put("apptype", 0);
		paramMap.put("title", title);
		paramMap.put("appids", json);
        Organs upOrgans = sessionUtils.getUpOrganByOrgid(session.getCurrentOrg().getPorgid(),session.getCompid());
		if(StringUtils.isEmpty(appid)) {
			Long id = db().insertReturnId("insert into h_dzz_dfsysq (appflag,feetype, userid, organid, appfee, appdesc, apptime, status,tagorgid,apptype,title,appids) values (:appflag, :feetype, :userid, :organid, :appfee, :appdesc, :apptime, :status, :tagorgid, :apptype, :title, :appids)", paramMap,null);
			insertMyToDoList(5, id.toString(),"《"+title+"》", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), upOrgans.getUserid());
			dfglGzjfHandle.addUpOrganAuditInfo(session, id, 0, 0);
		}else {
			paramMap.put("appid", appid);
			db().update("update h_dzz_dfsysq set title=:title,feetype=:feetype,appfee=:appfee,appdesc=:appdesc,apptime=:apptime,appids=:appids where appid=:appid", paramMap);
			Map<String,Object> map = db().getMap("select * from h_dzz_dfsysq where appid=?", new Object[] {appid});
			if(map!=null && map.get("status").toString().equals("2")) {
				dfglGzjfHandle.addUpOrganAuditInfo(session, Long.valueOf(appid), 0, 0);
				insertMyToDoList(5, appid,"《"+title+"》", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), upOrgans.getUserid());
			}
		}
		
			
		
	}

	
	/**
	 * Description: 创建待办消息
	 * @author PT  
	 * @date 2019年9月10日 
	 * @param module
	 * @param content
	 * @param remind
	 * @param optuserid
	 * @param userid
	 */
	public void insertMyToDoList(int module,String busid,String content,String remind,String optuserid,String userid) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("module", module);
		paramMap.put("busid", busid);
		paramMap.put("content", content);
		paramMap.put("remind", remind);
		paramMap.put("optuserid", optuserid);
		paramMap.put("userid", userid);
		paramMap.put("isread", 0);
		paramMap.put("ishandle", 0);
		generalSqlComponent.getDbComponent().insert("INSERT INTO my_to_do_list (module, busid, content, remind, optuserid, userid, isread, ishandle) VALUES (:module, :busid, :content, :remind, :optuserid, :userid, :isread, :ishandle)", paramMap);
	}



	/**
	 * Description: 保存申请单时关联的申请单列表数据源
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param session
	 */
	public List<Map<String,Object>> listPartyFeeApplicationForm(Session session) {
		Map<String,Object> map = db().getMap("select remarks3 from urc_organization where compid=? and orgid=?", new Object[] {session.getCompid(),session.getCurrorganid()});
		Object userid = map.get("remarks3");
		return db().listMap("select a.appid,b.title from h_dzz_sprz a left join h_dzz_dfsysq b on a.appid=b.appid where a.auditorid=? and a.type=0 group by a.appid,b.title", new Object[] {userid});
	}



	/**
	 * Description: 党费申请/报销申请审核
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param plaad
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public void auditPartyFeeApplication(Session session, PartyFeeApplicationAuditDto plaad) {
		int flag = plaad.getFlag();
		Long id = plaad.getId();
		String appmesg = plaad.getAppmesg();
		int result = plaad.getResult();
		Map<String,Object> map = db().getMap("select a.appid,b.apptype,a.organid from h_dzz_sprz a left join h_dzz_dfsysq b on a.appid=b.appid where a.id=?", new Object[] {id});
		int apptype = Integer.valueOf(map.get("apptype").toString());//判断是党费申请审核还是报销审核
		//党费申请审核
		if(apptype==0) {
			if(result==1) {//通过
				if(flag==0) {//党费申请报销
					db().update("update h_dzz_sprz set result=?,appmesg=?,flag=?,remarks3=? where id=?", new Object[] {result,appmesg,flag,DateEnum.YYYYMMDDHHMMDD.format(),id});
					db().update("update h_dzz_dfsysq set status=1 where appid=?", new Object[] {map.get("appid")});
					
				}else {//党费申请下拨
					auditPassUpdate(session, plaad,  map);
					db().update("update h_dzz_dfsysq set status=3 where appid=?", new Object[] {map.get("appid")});
				}
			}else {//驳回
				db().update("update h_dzz_sprz set result=?,appmesg=?,remarks3=? where id=?", new Object[] {result,appmesg,DateEnum.YYYYMMDDHHMMDD.format(),id});
				db().update("update h_dzz_dfsysq set status=2 where appid=?", new Object[] {map.get("appid")});
			}
		}
		//党费报销审核
		else {
			if(result==1) {//通过
				auditPassUpdate(session, plaad, map);
				db().update("update h_dzz_dfsysq set status=3 where appid=?", new Object[] {map.get("appid")});
			}else {
				db().update("update h_dzz_sprz set result=?,appmesg=?,remarks3=? where id=?", new Object[] {result,appmesg,DateEnum.YYYYMMDDHHMMDD.format(),id});
				db().update("update h_dzz_dfsysq set status=2 where appid=?", new Object[] {map.get("appid")});
			}
		}
	}


	private void auditPassUpdate(Session session, PartyFeeApplicationAuditDto plaad,Map<String, Object> map) {
		int flag = plaad.getFlag();
		Long id = plaad.getId();
		String appmesg = plaad.getAppmesg();
		int result = plaad.getResult();
		Double retain = plaad.getRetain();//自留费用拨款额度
		Double allocate = plaad.getAllocate();//下拨党费拨款额度
		
		int feetype = plaad.getFeetype();//费用类别id
		db().update("update h_dzz_sprz set result=?,appmesg=?,flag=?,retain=?,allocate=?,remarks3=? where id=?", new Object[] {result,appmesg,flag,retain,allocate,DateEnum.YYYYMMDDHHMMDD.format(),id});
		
		//向下拨表中插入数据
		Map<String,Object> map1 = db().getMap("select * from h_dzz_sprz where id=?", new Object[] {id});
		String orgid = map1.get("organid").toString();
		Map<String,Object> map2 = db().getMap("select * from t_dzz_info where organid=?", new Object[] {orgid});
		String orgname = map2.get("dzzmc").toString();
		saveLowerPartyFee(session, orgid, orgname, retain, allocate, 0);//审核下拨
		//更新相应党组织的党费余额
		//当前组织余额减少 
		DecimalFormat df=new DecimalFormat("0.00");
		updateHDzzFundfee(session, orgid, retain, allocate, df);
		//更新申请表的分类id
		db().update("update h_dzz_dfsysq set feetype=?,apptime=? where appid=?", new Object[] {feetype,DateEnum.YYYYMMDDHHMMDD.format(),map.get("appid")});
	}




	/**
	 * Description: 下拨党费列表分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param session
	 * @param pager
	 * @param name
	 * @param time
	 * @return
	 */
	public Pager lowerPartyFeePage(Session session, Pager pager, String name, String time) {
		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		String[] times = null;
		String time1 = "";
		String time2 = "";
		if(!StringUtils.isEmpty(time)) {
			times = time.split(",");
			time1 = times[0];
			time2 = times[1];
		}
		if(StringUtils.isEmpty(name) && StringUtils.isEmpty(time)) {//不带模糊查询
			db().pageMap(pager, "select * from h_appropriation where sorgid=? order by createtime desc", new Object[] {organid});
		}else if(!StringUtils.isEmpty(name) && !StringUtils.isEmpty(time)) {
			db().pageMap(pager, "select * from h_appropriation where sorgid=? and createtime>=? and createtime<=? and torgname like '%"+name+"%' order by createtime desc", new Object[] {organid,time1,time2});
		}else if(!StringUtils.isEmpty(name) && StringUtils.isEmpty(time)) {
			db().pageMap(pager, "select * from h_appropriation where sorgid=? and torgname like '%"+name+"%' order by createtime desc", new Object[] {organid});
		}else if(StringUtils.isEmpty(name) && !StringUtils.isEmpty(time)) {
			db().pageMap(pager, "select * from h_appropriation where sorgid=? and createtime>=? and createtime<=? order by createtime desc", new Object[] {organid,time1,time2});
		}
		return pager;
	}




	/**
	 * Description: 下拨党费保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param session
	 * @param orgid
	 * @param orgname
	 * @param retain
	 * @param allocate
	 */
	public void saveLowerPartyFee(Session session, String orgid, String orgname, Double retain, Double allocate, int apttype) {
		DecimalFormat df=new DecimalFormat("0.00");
		if(retain == null) {
			retain = 0.0;
		}
		if(allocate == null) {
			allocate = 0.0;
		}
		String amount = df.format(retain+allocate);
//		String sorgid = session.getCustom();
		String sorgid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Map<String,Object> map = db().getMap("select * from urc_organization where compid=? and orgid=?", new Object[] {session.getCompid(),session.getCurrorganid()});
		String sorgname = map.get("orgname").toString();
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("sorgid", sorgid);
		paramMap.put("sorgname", sorgname);
		paramMap.put("torgid", orgid);
		paramMap.put("torgname", orgname);
		paramMap.put("amount", amount);
		paramMap.put("retain", retain);
		paramMap.put("allocate", allocate);
		paramMap.put("wkfunds", 0);
		paramMap.put("apttype", apttype);
		paramMap.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
		db().insert("INSERT INTO h_appropriation (sorgid, sorgname, torgid, torgname, amount, retain, allocate, wkfunds, apttype, createtime) VALUES (:sorgid, :sorgname, :torgid, :torgname, :amount, :retain, :allocate, :wkfunds, :apttype, :createtime)", paramMap);
	    if(apttype==1) {
	    	//主动下拨完成需要更新相关党组织总金额
	    	updateHDzzFundfee(session, orgid, retain, allocate, df);
	    }
	}
	
	
	/**
	 * Description: 下拨党费列表汇总 
	 * @author PT  
	 * @date 2019年9月10日 
	 * @param session
	 * @return
	 */
	public Map<String, Object> getSummary(Session session) {
		Map<String,Object> map = new HashMap<String,Object>();
		Double allocate1 = 0.0;
		Double retain1 = 0.0;
		DecimalFormat df=new DecimalFormat("0.00");
		//自己账户原有的钱
		Map<String,Object> map4 = db().getMap("select allocate,selffee from h_dzz_fundfee where organid=?",
				new Object[] {sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode())});
		allocate1 = Double.valueOf(map4.get("allocate").toString());
		retain1 = Double.valueOf(map4.get("selffee").toString());
//		Double allocate2 = 0.0;
//		Double retain2 = 0.0;
//		//我下拨给下级的下拨经费和自留经费（我支出的钱）
//		Map<String,Object> map2 = db().getMap("select sum(allocate) allocate,sum(retain) retain from h_appropriation where sorgid=?", new Object[] {session.getCustom()});
//		if(map2.get("allocate")!=null) {
//			allocate2 = Double.valueOf(map2.get("allocate").toString());
//		}
//		if(map2.get("retain")!=null) {
//			retain2 = Double.valueOf(map2.get("retain").toString());
//		}
//		Double allocate3 =Double.valueOf( df.format(allocate1-allocate2));
//		Double retain3 =Double.valueOf( df.format(retain1-retain2));
		Double allocate3 =Double.valueOf( df.format(allocate1));
		Double retain3 =Double.valueOf( df.format(retain1));
		String amount = df.format(allocate3+retain3);
		map.put("amount", amount);
		map.put("allocate", allocate3);
		map.put("retain", retain3);
		return map;
	}




	private void updateHDzzFundfee(Session session, String orgid, Double retain, Double allocate, DecimalFormat df) {
		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		if(retain == null) {
			retain = 0.0;
		}
		if(allocate == null) {
			allocate = 0.0;
		}
		//当前组织余额减少 
//		Map<String,Object> map3 = db().getMap("select * from h_dzz_fundfee where organid=? ", new Object[] {session.getCustom()});
		Map<String,Object> map3 = db().getMap("select * from h_dzz_fundfee where organid=? ", new Object[] {organid});
		Double allocate2 = Double.valueOf(map3.get("allocate").toString());
		Double selffee = Double.valueOf(map3.get("selffee").toString());
		String allocate3 = df.format(allocate2-allocate);
		String selffee2 = df.format(selffee-retain);
//		db().update("update h_dzz_fundfee set allocate=?,selffee=? where organid=?", new Object[] {allocate3,selffee2,session.getCustom()});
		db().update("update h_dzz_fundfee set allocate=?,selffee=? where organid=?", new Object[] {allocate3,selffee2,organid});
		//被下拨组织余额增加了
		Map<String,Object> map4 = db().getMap("select * from h_dzz_fundfee where organid=?", new Object[] {orgid});
		if(map4==null) {
			db().insert("insert into h_dzz_fundfee (organid,allocate) values (?,?)", new Object[] {orgid,df.format(retain+allocate)});
		}else {
			Double allocate5 = Double.valueOf(df.format(map4.get("allocate")));
			String allocate4 = df.format(retain+allocate+allocate5);
			db().update("update h_dzz_fundfee set allocate=? where organid=?", new Object[] {allocate4,orgid});
		}
	}




	/**
	 * Description: 党费报销申请保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param session
	 * @param title
	 * @param flag
	 * @param appid
	 * @param sumfee
	 * @param feelist
	 * @param remarks3
	 * @param remarks4
	 */
	public void saveReimbursementFundingApplication(Session session, String title, Integer flag, String appid,
			String sumfee, String feelist, String remarks3, String remarks4) {
		String apptime = DateEnum.YYYYMMDDHHMMDD.format();
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("title", title);
		paramMap.put("appid", appid);
		paramMap.put("apptime", apptime);
		paramMap.put("apptype", 1);
		paramMap.put("sumfee", sumfee);
		paramMap.put("feelist", feelist);
		paramMap.put("remarks3", remarks3);
		paramMap.put("remarks4", remarks4);
		paramMap.put("status", 0);
		if(flag==0) {
			db().update("update h_dzz_dfsysq set status=:status, title=:title,sumfee=:sumfee,feelist=:feelist,remarks3=:remarks3,remarks4=:remarks4,apptype=:apptype,apptime=:apptime where appid=:appid", paramMap);
			dfglGzjfHandle.addUpOrganAuditInfo(session, Long.valueOf(appid), 0, 1);
		}else {
			db().update("update h_dzz_dfsysq set status=:status, sumfee=:sumfee,feelist=:feelist,remarks3=:remarks3,remarks4=:remarks4,apptime=:apptime where appid=:appid", paramMap);
		}
	}
	
	
	
	/**
	 * Description: 党组织获取下拨党费余额以及自留党费余额
	 * @author PT  
	 * @date 2019年9月6日 
	 * @param session
	 * @return
	 */
	public Map<String, Object> getBalance(Session session) {
		return db().getMap("select selffee ziliu,allocate xiabo from h_dzz_fundfee where organid=?",
				new Object[] {sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode())});
	}

	
	
	

	/**===================================================党费信息汇总======================================================================*/
	
	
	/**
	 * Description: 党支部新增利息
	 * @author PT  
	 * @date 2019年9月6日 
	 * @param session
	 * @param addinterest
	 */
	public void saveInterest(Session session,String addinterest) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		String years = sdf.format(DateEnum.now());
		SimpleDateFormat sdf2 = new SimpleDateFormat("MM");
		String months = sdf2.format(DateEnum.now());
//		Object custom = session.getCustom();
		Object custom = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Map<String,Object> map2 = db().getMap("select * from h_dzz_fundfee where organid=?", new Object[] {custom});
		DecimalFormat df=new DecimalFormat("0.00");
		Double valueOf = Double.valueOf(map2.get("selffee").toString());
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("years", years);
		paramMap.put("months", months);
		paramMap.put("orgid", custom);
		paramMap.put("createtime", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap.put("addinterest", addinterest);
		Map<String,Object> map = db().getMap("select * from h_df_interest where orgid=? order by id desc limit 1", new Object[] {custom});
		if(map==null) {
			paramMap.put("id", 1);
		}else {
			paramMap.put("id", Long.valueOf(map.get("id").toString())+1);
		}
		db().insert("insert into h_df_interest (id, years, months, orgid, createtime, addinterest) values (:id, :years, :months, :orgid, :createtime, :addinterest)", paramMap);
	
		//每次新增完利息都需要更新该组织余额  更新自留余额
		String allocate = df.format(valueOf+Double.valueOf(addinterest));
		db().update("update h_dzz_fundfee set selffee = ? where organid = ?", new Object[] {allocate,custom});
	}


	/**
	 * Description: 新增修改账户信息
	 * @author PT  
	 * @date 2019年9月12日 
	 * @param session
	 * @param accountname
	 * @param balancecard
	 * @param handaddress
	 */
    public void saveSum(Session session, String accountname, String balancecard, String handaddress) {
//		int num = db().update("update h_dzz_account set accountname=?,balancecard=?,handaddress=? where orgid=?", new Object[] {accountname,balancecard,handaddress,session.getCustom()});
		int num = db().update("update h_dzz_account set accountname=?,balancecard=?,handaddress=? where orgid=?",
				new Object[] {accountname,balancecard,handaddress,sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode())});
		if(num<=0) {
			db().insert("insert into h_dzz_account (orgid,accountname,balancecard,handaddress) values (?,?,?,?)",
					new Object[] {sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()),accountname,balancecard,handaddress});
		}
	}
	
    
    
	    
	/**
	 * Description: 获取账户信息
	 * @author PT  
	 * @date 2019年9月16日 
	 * @param session
	 * @return
	 */
	public Map<String,Object> getSum(Session session) {
		return db().getMap("select * from h_dzz_account where orgid=? limit 1",
				new Object[] {sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode())});
	}
    
    
    /**
 
     * Description: 查询现有党费
     * @author PT  
     * @date 2019年9月12日 
     * @param session
     * @return
     */
    public String getFee(Session session) {
    	Map<String,Object> map2 = db().getMap("select * from h_dzz_fundfee where organid=?",
				new Object[] {sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode())});
		DecimalFormat df=new DecimalFormat("0.00");
		Double valueOf = Double.valueOf(map2.get("selffee").toString());
    	String balance = df.format(Double.valueOf(map2.get("retain").toString())+Double.valueOf(map2.get("allocate").toString())+valueOf);
    	return balance;
    }
	


	/**
	 * Description: 利息列表查询
	 * @author PT  
	 * @date 2019年9月6日 
	 * @param session
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	public List<Map<String, Object>> listInterest(Session session, String starttime, String endtime) {

		String organid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		List<Map<String, Object>> listMap = null;
		if(StringUtils.isEmpty(starttime) && StringUtils.isEmpty(endtime)) {
			listMap = db().listMap("select * from  h_df_interest where orgid =?", new Object[] {organid});
		}else if(!StringUtils.isEmpty(starttime) && !StringUtils.isEmpty(endtime)) {
			starttime = starttime+" 00:00:00";
			endtime = endtime+" 23:59:59";
			listMap = db().listMap("select * from  h_df_interest where orgid =? and createtime >=? and createtime<=?", new Object[] {organid,starttime,endtime});
		}else if(StringUtils.isEmpty(starttime) && !StringUtils.isEmpty(endtime)) {
			endtime = endtime+" 23:59:59";
			listMap = db().listMap("select * from  h_df_interest where orgid =? and createtime<=?", new Object[] {organid,endtime});
		}else if(!StringUtils.isEmpty(starttime) && StringUtils.isEmpty(endtime)) {
			starttime = starttime+" 00:00:00";
			listMap = db().listMap("select * from  h_df_interest where orgid =? and createtime >=? ", new Object[] {organid,starttime});
		}
		return listMap;
	}



	/**
	 * Description: 党费汇总列表查询
	 * @author PT  
	 * @date 2019年9月6日 
	 * @param session
	 * @param orgid
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	public List<Map<String, Object>> listPartyFeeSummary(Session session, String orgid, String starttime, String endtime) {
		List<Map<String,Object>> listMaps = new ArrayList<Map<String,Object>>();
		String startyear = "";
		int startmonth = 0;
		String endyear = "";
		int endmonth = 0;
		if(!StringUtils.isEmpty(starttime)) {
			startyear = starttime.substring(0, 4);
			startmonth = Integer.valueOf(starttime.substring(5, 7));
		}
		if(!StringUtils.isEmpty(endtime)) {
			endyear = starttime.substring(0, 4);
			endmonth = Integer.valueOf(starttime.substring(5, 7));
		}
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("startyear", startyear);
		paramMap.put("startmonth", startmonth);
		paramMap.put("endyear", endyear);
		paramMap.put("startyear", startyear);
		paramMap.put("endmonth", endmonth);
		paramMap.put("starttime", starttime);
		paramMap.put("endtime", endtime);
		List<Map<String,Object>> listMap = db().listMap("SELECT organid FROM t_dzz_info where organid=? or parentid=? order by organcode asc", new Object[] {orgid,orgid});
		if(listMap!=null && listMap.size()>=0) {
			for (Map<String, Object> map : listMap) {
				Object organid = map.get("organid");
				paramMap.put("organid", organid);
				Map<String,Object> data = new HashMap<String,Object>();
				if(!StringUtils.isEmpty(starttime) && !StringUtils.isEmpty(endtime)) {//都不为空
					data = db().getMap("select a.organid,b.dzzmc organname,c.one,d.two,e.three,f.four,g.five,h.six,j.seven from h_dzz_fundfee a left join t_dzz_info b on a.organid=b.organid " + 
							"left join (select sum(retain+allocate+selffee) one,organid from h_dzz_fundfee where organid=:organid) c on a.organid=c.organid " + 
							"left join (SELECT sum(actualin) two,organid FROM h_dzz_jfmx where organid=:organid and year>=:startyear and year<=:endyear and month>=:startmonth and month<=:endmonth ) d on a.organid=d.organid " + 
							"left join (SELECT sum(actualout) seven,organid FROM h_dzz_jfmx where organid=:organid and apstatus=2 and year>=:startyear and year<=:endyear and month>=:startmonth and month<=:endmonth) j on a.organid=j.organid " + 
							"left join (SELECT sum(addinterest) four,orgid FROM h_df_interest where orgid=:organid and createtime>=:starttime and createtime<=:endtime) f on a.organid=f.orgid " + 
							"left join (SELECT sum(amount) three,torgid FROM h_appropriation where torgid=:organid and createtime>=:starttime and createtime<=:endtime) e on a.organid=e.torgid " + 
							"left join (SELECT sum(amount) five,sorgid FROM h_appropriation where sorgid=:organid and apttype=0 and createtime>=:starttime and createtime<=:endtime) g on a.organid=g.sorgid " + 
							"left join (SELECT sum(amount) six,sorgid FROM h_appropriation where sorgid=:organid and apttype=1 and createtime>=:starttime and createtime<=:endtime) h on a.organid=h.sorgid " + 
							"where a.organid=:organid", paramMap);
				}else if(StringUtils.isEmpty(starttime) && StringUtils.isEmpty(endtime)){//都为空
					data = db().getMap("select a.organid,b.dzzmc organname,c.one,d.two,e.three,f.four,g.five,h.six,j.seven from h_dzz_fundfee a left join t_dzz_info b on a.organid=b.organid " + 
							"left join (select sum(retain+allocate+selffee) one,organid from h_dzz_fundfee where organid=:organid) c on a.organid=c.organid " + 
							"left join (SELECT sum(actualin) two,organid FROM h_dzz_jfmx where organid=:organid ) d on a.organid=d.organid " + 
							"left join (SELECT sum(actualout) seven,organid FROM h_dzz_jfmx where organid=:organid and apstatus=2) j on a.organid=j.organid " + 
							"left join (SELECT sum(addinterest) four,orgid FROM h_df_interest where orgid=:organid ) f on a.organid=f.orgid " + 
							"left join (SELECT sum(amount) three,torgid FROM h_appropriation where torgid=:organid ) e on a.organid=e.torgid " + 
							"left join (SELECT sum(amount) five,sorgid FROM h_appropriation where sorgid=:organid and apttype=0 ) g on a.organid=g.sorgid " + 
							"left join (SELECT sum(amount) six,sorgid FROM h_appropriation where sorgid=:organid and apttype=1 ) h on a.organid=h.sorgid " + 
							"where a.organid=:organid", paramMap);
				}else if(!StringUtils.isEmpty(starttime) && StringUtils.isEmpty(endtime)) {//开始时间不为空结束时间为空
					data = db().getMap("select a.organid,b.dzzmc organname,c.one,d.two,e.three,f.four,g.five,h.six,j.seven from h_dzz_fundfee a left join t_dzz_info b on a.organid=b.organid " + 
							"left join (select sum(retain+allocate+selffee) one,organid from h_dzz_fundfee where organid=:organid) c on a.organid=c.organid " + 
							"left join (SELECT sum(actualin) two,organid FROM h_dzz_jfmx where organid=:organid and year>=:startyear and month>=:startmonth  ) d on a.organid=d.organid " + 
							"left join (SELECT sum(actualout) seven,organid FROM h_dzz_jfmx where organid=:organid and apstatus=2 and year>=:startyear and month>=:startmonth) j on a.organid=j.organid " + 
							"left join (SELECT sum(addinterest) four,orgid FROM h_df_interest where orgid=:organid and createtime>=:starttime ) f on a.organid=f.orgid " + 
							"left join (SELECT sum(amount) three,torgid FROM h_appropriation where torgid=:organid and createtime>=:starttime ) e on a.organid=e.torgid " + 
							"left join (SELECT sum(amount) five,sorgid FROM h_appropriation where sorgid=:organid and apttype=0 and createtime>=:starttime ) g on a.organid=g.sorgid " + 
							"left join (SELECT sum(amount) six,sorgid FROM h_appropriation where sorgid=:organid and apttype=1 and createtime>=:starttime ) h on a.organid=h.sorgid " + 
							"where a.organid=:organid", paramMap);
				}else if(StringUtils.isEmpty(starttime) && !StringUtils.isEmpty(endtime)) {//开始时间为空结束时间不为空
					data = db().getMap("select a.organid,b.dzzmc organname,c.one,d.two,e.three,f.four,g.five,h.six,j.seven from h_dzz_fundfee a left join t_dzz_info b on a.organid=b.organid " + 
							"left join (select sum(retain+allocate+selffee) one,organid from h_dzz_fundfee where organid=:organid) c on a.organid=c.organid " + 
							"left join (SELECT sum(actualin) two,organid FROM h_dzz_jfmx where organid=:organid  and year<=:endyear  and month<=:endmonth ) d on a.organid=d.organid " + 
							"left join (SELECT sum(actualout) seven,organid FROM h_dzz_jfmx where organid=:organid and apstatus=2 and year<=:endyear  and month<=:endmonth) j on a.organid=j.organid " + 
							"left join (SELECT sum(addinterest) four,orgid FROM h_df_interest where orgid=:organid and createtime<=:endtime) f on a.organid=f.orgid " + 
							"left join (SELECT sum(amount) three,torgid FROM h_appropriation where torgid=:organid  and createtime<=:endtime) e on a.organid=e.torgid " + 
							"left join (SELECT sum(amount) five,sorgid FROM h_appropriation where sorgid=:organid and apttype=0 and createtime<=:endtime) g on a.organid=g.sorgid " + 
							"left join (SELECT sum(amount) six,sorgid FROM h_appropriation where sorgid=:organid and apttype=1 and createtime<=:endtime) h on a.organid=h.sorgid " + 
							"where a.organid=:organid", paramMap);
				}
				listMaps.add(data);
			}
		}
		return listMaps;
	}








	




	


	
}
