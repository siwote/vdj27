package com.lehand.developing.party.service.dfgl.dfsygl;

public class PartyFeeApplicationAuditDto {

	private int result;//审批结果1通过2拒绝
	
	private String appmesg;//审批备注
	
	private Long id;//审核列表数据id
	
	private int flag;//报销拨款(0:报销,1:拨款) 如果为0下面三个就不用传
	
	private Double retain;//自留费用拨款额度
	
	private Double allocate;//下拨党费拨款额度
	
	private int feetype;//费用类别id
	

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getAppmesg() {
		return appmesg;
	}

	public void setAppmesg(String appmesg) {
		this.appmesg = appmesg;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public Double getRetain() {
		return retain;
	}

	public void setRetain(Double retain) {
		this.retain = retain;
	}

	public Double getAllocate() {
		return allocate;
	}

	public void setAllocate(Double allocate) {
		this.allocate = allocate;
	}

	public int getFeetype() {
		return feetype;
	}

	public void setFeetype(int feetype) {
		this.feetype = feetype;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
