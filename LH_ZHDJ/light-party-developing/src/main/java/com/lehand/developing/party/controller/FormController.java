//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lehand.developing.party.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.controller.BaseController;
import com.lehand.developing.party.dto.Condition;
import com.lehand.developing.party.pojo.FormInfo;
import com.lehand.developing.party.service.FormComponent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Api("自定义表单服务")
@RestController
@RequestMapping({"/lhweb/form"})
public class FormController extends BaseController {
    private static final Logger logger = LogManager.getLogger(FormController.class);
    public static final String BASE_URL = "/lhweb/form";
    @Resource
    private FormComponent formComponent;

    public FormController() {
    }

    @ApiOperation(
        value = "分页查询自定义表单",
        notes = "分页查询自定义表单",
        httpMethod = "POST"
    )
    @ApiImplicitParams({@ApiImplicitParam(
    name = "sessionid",
    value = "系统sessinid",
    required = true,
    paramType = "header",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "pageNo",
    value = "当前页码",
    required = true,
    paramType = "query",
    dataType = "Long",
    defaultValue = "1"
), @ApiImplicitParam(
    name = "pageSize",
    value = "每页显示条数",
    required = true,
    paramType = "query",
    dataType = "Long",
    defaultValue = "10"
), @ApiImplicitParam(
    name = "sort",
    value = "排序字段",
    required = false,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "bussid",
    value = "业务id",
    required = false,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "formname",
    value = "表单名称",
    required = false,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "starttime",
    value = "开始时间",
    required = false,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "endtime",
    value = "结束时间",
    required = false,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
)})
    @RequestMapping({"/pageForm"})
    public Message<Object> pageForm(@ApiIgnore Pager pager, @ApiIgnore Condition condition) {
        Message message = null;

        try {
            Pager result = this.formComponent.pageForm(pager, condition, this.getSession().getCompid());
            message = Message.SUCCESS;
            message.setData(result);
            message.setMessage("查询自定义表单成功");
            return message;
        } catch (Exception var5) {
            logger.error("查询自定义表单出错", var5);
            return Message.FAIL;
        }
    }

    @ApiOperation(
        value = "保存自定义表单",
        notes = "保存自定义表单",
        httpMethod = "POST"
    )
    @ApiImplicitParams({@ApiImplicitParam(
    name = "sessionid",
    value = "系统sessinid",
    required = true,
    paramType = "header",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "id",
    value = "表单id",
    required = false,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "bussid",
    value = "业务id",
    required = true,
    paramType = "query",
    dataType = "Long",
    defaultValue = ""
), @ApiImplicitParam(
    name = "formname",
    value = "表单名称",
    required = true,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "jsonstr",
    value = "表单json结果集",
    required = true,
    paramType = "query",
    dataType = "String",
    defaultValue = ""
)})
    @RequestMapping({"/saveForm"})
    public Message<Object> saveForm(@ApiIgnore FormInfo info) {
        Message message = Message.FAIL;

        try {
            if (info.getId() != null) {
                this.formComponent.updateForm(info, this.getSession().getCompid());
            } else {
                info.setUserid(this.getSession().getUserid().toString());
                info.setUsername(this.getSession().getUsername());
                this.formComponent.addForm(info, this.getSession().getCompid());
            }

            message.success().setData("");
            message.setMessage("自定义表单保存成功");
        } catch (Exception var4) {
            logger.error("保存自定义表单出错", var4);
        }

        return message;
    }

    @ApiOperation(
        value = "删除表单",
        notes = "删除表单",
        httpMethod = "POST"
    )
    @ApiImplicitParams({@ApiImplicitParam(
    name = "sessionid",
    value = "系统sessinid",
    required = true,
    paramType = "header",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "id",
    value = "自定义表单id",
    required = true,
    paramType = "query",
    dataType = "Long",
    defaultValue = ""
)})
    @RequestMapping({"/delForm"})
    public Message<Object> delForm(Long id) {
        Message message = Message.FAIL;

        try {
            if (StringUtils.isEmpty(id)) {
                message.setMessage("表单编码id不可为空");
                return message;
            }

            this.formComponent.deleteFormById(id, this.getSession().getCompid());
            message.success().setMessage("自定义表单删除成功");
        } catch (Exception var4) {
            logger.error("删除自定义表单出错", var4);
            message.setMessage("删除自定义表单出错");
        }

        return message;
    }

    @ApiOperation(
        value = "根据自定义表单id查询详情",
        notes = "根据自定义表单id查询详情",
        httpMethod = "POST"
    )
    @ApiImplicitParams({@ApiImplicitParam(
    name = "sessionid",
    value = "系统sessinid",
    required = true,
    paramType = "header",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "id",
    value = "自定义表单id",
    required = true,
    paramType = "query",
    dataType = "Long",
    defaultValue = ""
)})
    @RequestMapping({"/getFormById"})
    public Message<Object> getFormInfoById(Long id) {
        Message message = Message.FAIL;

        try {
            if (StringUtils.isEmpty(id)) {
                message.setMessage("表单编码id不可为空");
                return message;
            }

            FormInfo info = this.formComponent.getFormById(id, this.getSession().getCompid());
            message.success().setData(info);
            message.setMessage("查询自定义表单成功");
        } catch (Exception var4) {
            logger.error("查询自定义表单出错", var4);
            message.setMessage("根据id查询自定义表单出错");
        }

        return message;
    }

    @ApiOperation(
        value = "根据业务id查询详情",
        notes = "根据业务id查询详情",
        httpMethod = "POST"
    )
    @ApiImplicitParams({@ApiImplicitParam(
    name = "sessionid",
    value = "系统sessinid",
    required = true,
    paramType = "header",
    dataType = "String",
    defaultValue = ""
), @ApiImplicitParam(
    name = "bussid",
    value = "业务id",
    required = true,
    paramType = "query",
    dataType = "Long",
    defaultValue = ""
)})
    @RequestMapping({"/getFormByBusId"})
    public Message<Object> getFormInfoByBusId(Long bussid) {
        Message message = Message.FAIL;

        try {
            if (StringUtils.isEmpty(bussid)) {
                message.setMessage("业务id不可为空");
                return message;
            }

            FormInfo info = this.formComponent.getFormByBusId(bussid, this.getSession().getCompid());
            message.success().setData(info);
            message.setMessage("查询自定义表单成功");
        } catch (Exception var4) {
            logger.error("查询自定义表单出错", var4);
            message.setMessage("根据业务id查询自定义表单出错");
        }

        return message;
    }
}
