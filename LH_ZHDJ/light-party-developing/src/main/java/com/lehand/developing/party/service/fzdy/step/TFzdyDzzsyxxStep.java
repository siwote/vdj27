package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.service.fzdy.handle.FzdyBzxxHandle;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyDzzsyxxStep extends BaseStep {
	
	@Resource private FzdyBzxxHandle fzdyBzxxHandle;

	//person_id 
	//type
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_dzzsyxx where type=:type and person_id=:person_id and syncstatus='A' and compid=:compid", params);
		makeFiles(map,session);
		return map;
	}
	


	//type
	//sydate
	//syyj
	//syjg （通过传1，不通过传-1）
	//bsjdwdate
	//	person_id
	//	file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("keep_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("bsjdwdate")==null) {
			params.put("bsjdwdate", Constant.ENPTY);
		}
		
		int num = db().delete("UPDATE t_fzdy_dzzsyxx SET syncstatus='D' WHERE type=:type and person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_dzzsyxx (compid, keep_id, type, bsjdwdate, sydate, syyj, syjg, created_by, create_date, delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:keep_id,:type,:bsjdwdate,:sydate,:syyj,:syjg,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		saveFile(params);
		
		//插入数据完成后就得更新下审核表对应数据的状态
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("compid", session.getCompid());
		paramMap.put("status", params.get("syjg"));
		paramMap.put("person_id", params.get("person_id"));
		paramMap.put("audit_id", session.getUserid());
		paramMap.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap.put("id", params.get("id"));
		db().update("update fzdy_dzzsp set status=:status,moditime=:moditime where compid=:compid and person_id=:person_id and audit_id=:audit_id and id=:id", paramMap);
		//如果是不通过那么就得更新步骤表直接返回到第5，15，26步
		String type = params.get("type").toString();
		String dzzspType = "";
		if("-1".equals(params.get("syjg").toString())) {
			Map<String,Object> params2 = new HashMap<String,Object>();
			params2.put("person_id", params.get("person_id"));
			switch (type) {
			case "204":
				params2.put("type", "203");
				dzzspType = Constant.ONE;
				break;
			case "214":
				params2.put("type", "213");
				dzzspType = Constant.TWO;
				break;
			case "403":
				params2.put("type", "402");
				dzzspType = Constant.FOUR;
				break;
			case "408":
				params2.put("type", "407");
				dzzspType = Constant.FIVE;
				break;
			case "514":
				params2.put("type", "513");		
				dzzspType = Constant.SIX;
				break;
			default:
				break;
			}
			//删除当前步骤的fzdy_bzxx表信息
			db().delete("delete from fzdy_bzxx where person_id = ? and type = ?", new Object[] {params.get("person_id"),type});
			//更新某个阶段为可编辑状态
			fzdyBzxxHandle.update2(params2, session);
			//将其他步骤的编辑状态更新为不可编辑
			fzdyBzxxHandle.update1(params2, session);
		}else {//总党支审核通过
			//寻找下一级审核人（党委）并向审核表中插入相应记录
			//当前登录人的父级机构
			Map<String,Object> fatherOrgan = fzdyBzxxHandle.getFatherOrgan(session.getCompid(),session.getCurrorganid());
			//判断是党总支还是党委
			String fatherCode = fatherOrgan.get("orgtypeid").toString();
			//审核人id
			String audit_id = fatherOrgan.get("remarks3").toString();
			// 审核人名称
			String audit_name = fatherOrgan.get("orgname").toString();
			Map<String, Object> paramMap2 = fzdyBzxxHandle.commontParameter(session, params.get("step").toString(),
					params.get("person_id").toString(), fatherCode, audit_id, audit_name);
			switch (type) {
			case "204":
				dzzspType = Constant.ONE;
				break;
			case "214":
				dzzspType = Constant.TWO;
				break;
			case "403":
				dzzspType = Constant.FOUR;
				break;
			case "408":
				dzzspType = Constant.FIVE;
				break;
			case "514":
				dzzspType = Constant.SIX;
				break;
			default:
				break;
			}
			
			fzdyBzxxHandle.commont(paramMap2, dzzspType, fatherCode, params, "205", "204",
					session);
			//审核通过后就自动进入下一步
			params.put("type", type);
			fzdyBzxxHandle.nextStage(params, session);
		}
		
		//更新审核人的待办为已办
		db().update("update my_to_do_list set ishandle=1 where module=4 and userid=? and busid=?", new Object[] {session.getUserid(),dzzspType});
	
		
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		return num;
	}
	
}
