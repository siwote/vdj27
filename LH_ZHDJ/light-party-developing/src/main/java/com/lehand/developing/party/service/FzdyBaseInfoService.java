package com.lehand.developing.party.service;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.lehand.base.common.Session;
import com.lehand.developing.party.dto.*;
import com.lehand.developing.party.pojo.FzdyPartyLink;
import com.lehand.developing.party.utils.ExcelUtils;
import com.lehand.developing.party.utils.NumberUtils;
import com.lehand.developing.party.utils.SessionUtils;
import com.lehand.document.lib.service.FileDelService;
import com.lehand.horn.partyorgan.service.DataSynchronizationService;
import com.lehand.horn.partyorgan.service.info.MemberExcelService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.pojo.TFzdyZcry;

@Service
public class FzdyBaseInfoService{

	@Resource private GeneralSqlComponent generalSqlComponent;
	@Resource private SessionUtils sessionUtils;
	@Resource
	MemberExcelService memberExcelService;
	@Resource
	DataSynchronizationService dataSynchronizationService;
	@Resource
    FileDelService fileDelService;

	@Value("${jk.organ.shenqingren}")
	private String shenqingren;
	@Value("${jk.organ.rudangjijifenzi}")
	private String rudangjijifenzi;
	@Value("${jk.organ.fazhanduixiang}")
	private String fazhanduixiang;
	@Value("${jk.organ.yubeidangyuan}")
	private String yubeidangyuan;
	@Value("${jk.organ.zhengshidangyuan}")
	private String zhengshidangyuan;
	/**
	 *	发展党员管理列表
	 */
	public Pager queryPageFzdy(Pager pager, String type, String nameStr, String codeStr, Session session) {
		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("type", type);
		params.put("nameStr", nameStr);
		params.put("codeStr", codeStr);
		params.put("organid",sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()));
		params.put("compid", session.getCompid());
		return generalSqlComponent.pageQuery(SqlCode.FZDY_PAGE, params, pager); 
	}
	
	/**
	 *发展党员基本信息保存
	 */
	@Transactional(rollbackFor = Exception.class)
	public int save(FzdyBaseInfoDto info,Session session) {
		info.setOrganid(sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()));
		//验证当前身份证号是否已存在
//		//是新增
//		if(StringUtils.isEmpty(info.getUserid())){
//			Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from  t_dy_info where zjhm = ? limit 1", new Object[]{info.getZjhm()});
//			if(map!=null){
//				LehandException.throwException("该身份证号码的党员已存在");
//			}
//			return insertInfo(info,session);
//		}
//		return updateInfo(info,session);

		List<Map<String,Object>> dyinfo = generalSqlComponent.query(SqlCode.getDyInfoZjhm,new Object[]{info.getZjhm(),info.getUserid()});
		if(dyinfo.size()>0){
			LehandException.throwException("该身份证号码的党员已存在");
			return 0;
		}else{
			if(StringUtils.isEmpty(info.getUserid())) {
				return insertInfo(info,session);
			}
			return updateInfo(info,session);
		}


	}
	
	

	/**
	 * 发展党员基本信息 修改
	 * @param info
	 * @param session 
	 * @return
	 */
//	@Transactional(rollbackFor = Exception.class)
	private int updateInfo(FzdyBaseInfoDto info, Session session) {
		info.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
		int num =  generalSqlComponent.update(SqlCode.FZDY_UPDATE, info);
//		//头像附件id保存到48步中的附件特殊表中fzdy_special
//		if(StringUtils.isEmpty(info.getPhotourl())) {
//			generalSqlComponent.getDbComponent().delete("delete from fzdy_special where person_id=? and stepid=1000", new Object[] {info.getUserid()});
//		}else {
//			generalSqlComponent.getDbComponent().update("update fzdy_special set fileids = ? where person_id=? and stepid=1000", new Object[] {info.getPhotourl(),info.getUserid()});
//			
//		}
		if(num<1) {
			LehandException.throwException("修改基本信息失败");
			return 0;
		}
		int rows = generalSqlComponent.update(SqlCode.FZDY_BC_UPDATE, info);
		if(rows<0) {
			LehandException.throwException("修改补充信息失败");
			return 0;
		}
		return rows;
	}

	/**
	 * 发展党员基本信息 新增
	 * @param info
	 * @param session 
	 * @return
	 */
//	@Transactional(rollbackFor = Exception.class)
	private int insertInfo(FzdyBaseInfoDto info, Session session) {
		String userid = UUID.randomUUID().toString().replace("-", "");
		info.setUserid(userid);	
		info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		int num =	generalSqlComponent.insert(SqlCode.FZDY_INSERT, info);
		//头像附件id保存到48步中的附件特殊表中fzdy_special
//		if(StringUtils.isEmpty(info.getPhotourl())) {
//			generalSqlComponent.getDbComponent().insert("insert into fzdy_special (person_id,stepid,fileids) values (?,?,?)", new Object[] {userid,1000,info.getPhotourl()});
//		}
		if(num < 1) {
			LehandException.throwException("保存基础信息失败");
			return 0;
		}
		int rows  = generalSqlComponent.insert(SqlCode.FZDY_BC_INSERT,info);
		if(rows<1) {
			LehandException.throwException("保存补充信息失败");
			return 0;
		}
		int flag = generalSqlComponent.insert(SqlCode.FZDY_GL_INSERT,new Object[] {session.getCompid(),userid});

		if(rows<1) {
			LehandException.throwException("保存管理信息失败");
			return 0;
		}
		//向5环节表插入初始数据
		Map<String,String> parms = new HashMap<String,String>(1);
		parms.put("id",UUID.randomUUID().toString().replace("-", ""));
		parms.put("userid",userid);
		int num2 = generalSqlComponent.insert(SqlCode.fzdy_party_link,parms);
		if(num2<1) {
			LehandException.throwException("初始化发展党员环节表失败");
			return 0;
		}
		return flag;
	}

	/**
	 *作废发展党员信息 :根据userid修改该条数据的操作状态operatetype
	 */
	public int delFzdy(int status, String userid) {
		if(status == 3) {
			return deleteOpt(status,userid);
		}
		return recoverOpt(status,userid);
	}

	/**
	 * 恢复作废
	 * @param status
	 * @param userid
	 * @return
	 */
	private int recoverOpt(int status, String userid) {
//		int nums = generalSqlComponent.query(SqlCode.FZDY_RECOVER_CHECK,new Object[] {userid});
//		if(nums>0) {
//			return 0;
//		}
		return deleteOpt(status,userid);
	}

	/**
	 * 直接修改状态
	 * @param status
	 * @param userid
	 * @return
	 */
	private int deleteOpt(int status, String userid) {
		int num = generalSqlComponent.update(SqlCode.FZDY_DELETE, new Object[] {status,userid});
		if(num<1) {
			LehandException.throwException("操作失败");
			return 0;
		}
		return num;
//		int rows = generalSqlComponent.update(SqlCode.FZDY_BC_DELETE, new Object[] {status,userid});
//		if(rows<1) {
//			LehandException.throwException("操作失败");
//			return 0;
//		}
//		return rows;
	}

	/**
	 * 发展党员信息详情
	 */
	public FzdyBaseInfoDto queryFzdyById(String userid) {
		return generalSqlComponent.query(SqlCode.FZDY_DETAIL, new Object[] {userid});
	}

	/**
	 * 发展党员转出
	 */
	@Transactional(rollbackFor = Exception.class)
	public int fzdyZzgxzj(String userid,int type,String organid,Session session) {
		Map<String,Object> map = generalSqlComponent.getDbComponent().getMap("select b.organid from urc_organization a left join t_dzz_info b on a.orgcode=b.organcode where a.orgid=?", new Object[] {organid});
		TFzdyZcry cry = new TFzdyZcry();
		cry.setPersonId(userid);
		cry.setSzdzb(sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()));
		if(map!=null) {
			cry.setZrdzz(map.get("organid").toString());
		}else {
			cry.setZrdzz("");
		}
		cry.setZcrq(DateEnum.YYYYMMDDHHMMDD.format());
		cry.setCreateDate(DateEnum.YYYYMMDDHHMMDD.format());
		cry.setCreatedBy(session.getLoginAccount());
		cry.setSyncStatus("A");
		if(type == 1) {//集团内转接
			cry.setZcstatus(1);
			generalSqlComponent.update(SqlCode.FZDY_DELETE, new Object[] {4,userid});
			//,zrdzz = :zrdzz,zcrq = :zcrq,zrrq = :zrrq,zcstatus = :zcstatus 2019-09-11 去除sql中的
			return generalSqlComponent.insert(SqlCode.FZDY_ROLL_OUT, cry);

		}
		//转出集团
		cry.setZcstatus(2);
		int num = generalSqlComponent.insert(SqlCode.FZDY_ROLL_OUT, cry);
		if(num<1) {
			LehandException.throwException("转出失败");
			return 0;
		}
		generalSqlComponent.update(SqlCode.FZDY_DELETE, new Object[] {5,userid});
		return num;
//		int row = generalSqlComponent.update(SqlCode.FZDY_BC_DELETE, new Object[] {4,userid});
//		if(row<1) {
//			LehandException.throwException("转出失败");
//			return 0;
//		}
//		return  row;
	}


	/**
	 *发展党员接收
	 */
    @Transactional(rollbackFor = Exception.class)
	public int receiveFzdy(String userid,int status,String remark,Session session) {
		TFzdyZcry cry = new TFzdyZcry();
		cry.setPersonId(userid);
		cry.setLastUpdateDate(DateEnum.YYYYMMDDHHMMDD.format());
		cry.setSyncStatus("E");
		cry.setZcstatus(status);
		cry.setUpdatedBy(session.getUsername());
		cry.setRemark(remark);
		cry.setZrrq(DateEnum.YYYYMMDDHHMMDD.format());
		cry.setDx_uuid(sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()));
		//UPDATE t_fzdy_zcry SET sync_status = :syncStatus,updated_by = :updatedBy,last_update_date = :lastUpdateDate  WHERE person_id = :personId and dx_uuid=:dx_uuid
		int num =  generalSqlComponent.update(SqlCode.FZDY_SHIFT_TO, cry);
		if(status!= 1) {
			//update t_dy_info set operatetype = ? where userid = ?
			generalSqlComponent.update(SqlCode.FZDY_DELETE, new Object[] {2,userid});
			return num;
		}
		int rows =  updateTDyInfo(cry);
		if(rows == 0) {			
			LehandException.throwException("转入失败");
		}
		generalSqlComponent.update(SqlCode.FZDY_DELETE, new Object[] {2,userid});
		return rows;
	}

	/**
	 * 转接成功,更改党员组织机构
	 * @param cry
	 * @return
	 */
//	@Transactional(rollbackFor = Exception.class)
	private int updateTDyInfo(TFzdyZcry cry)  {
		Map<String,Object> zcr =  generalSqlComponent.getDbComponent().getMap("SELECT * FROM t_fzdy_zcry WHERE person_id = ? ORDER BY insert_date desc LIMIT 1", new Object[] {cry.getPersonId()});
		if(StringUtils.isEmpty(zcr.get("dx_uuid"))) {
			return 0;
		}
		//UPDATE t_dy_info SET organid = ?,operatetype = 2,updatetime = ?,operatorname = ? WHERE userid = ?
		return generalSqlComponent.update(SqlCode.FZDY_UPDATE_ORGANID, new Object[] {zcr.get("dx_uuid"),DateEnum.YYYYMMDDHHMMDD.format(),cry.getUpdatedBy(),cry.getPersonId()}) ;
	}

	/**
	 *	发展党员组织关系转接列表
	 */
	public Pager queryZzgxAccept(Pager pager, String type, String nameStr, String codeStr, Session session) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("type", type);
		params.put("nameStr", nameStr);
		params.put("codeStr", codeStr);
		params.put("organid", sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()));
		params.put("compid", session.getCompid());
		return generalSqlComponent.pageQuery(SqlCode.FZDY_SHIFT_TO_PAGE, params, pager); 
	}

	/**
	 *党员类别下拉
	 */
	public List<Map<String, Object>> listDylbSel(Session session,String code) {
		List<Map<String,Object>> list = generalSqlComponent.query(SqlCode.FZDY_DICT_DYLB, new Object[] {code});
		return list;
	}

	public FzdyPartyLinkDto4 getFzdyPartyLink(String userid,Session session){

		FzdyPartyLinkDto2 fzdy2 = generalSqlComponent.query(SqlCode.getFzdyPartyLink,new Object[] {userid} );
		FzdyPartyLinkDto4 fzdy = new FzdyPartyLinkDto4();
		fzdy.setId(fzdy2.getId());
		fzdy.setUserid(fzdy2.getUserid());
		fzdy.setType(fzdy2.getType());
		fzdy.setApplyTime(fzdy2.getApplyTime());
		fzdy.setApplyTalkUser(generalSqlComponent.query(SqlCode.getFzdyPartyLinkSubjectList,new Object[]{fzdy2.getId(),NumberUtils.num100}));
//		fzdy.setApplyTalkUserId(fzdy2.getApplyTalkUserId());
//		fzdy.setApplyTalkUserName(fzdy2.getApplyTalkUserName());
		fzdy.setApplyFiles(fileDelService.getFiles(fzdy2.getApplyFiles(),session));
		fzdy.setPersonPlace(fzdy2.getPersonPlace());//入党申请人谈话地点
		fzdy.setPersonTime(fzdy2.getPersonTime());//入党申请人谈话时间

		fzdy.setActivistTime(fzdy2.getActivistTime());
		fzdy.setActivistFosterUser(generalSqlComponent.query(SqlCode.getFzdyPartyLinkSubjectList,new Object[]{fzdy2.getId(),NumberUtils.num200}));
//		fzdy.setActivistFosterUserId(fzdy2.getActivistFosterUserId());
//		fzdy.setActivistFosterUserName(fzdy2.getActivistFosterUserName());
		fzdy.setActivistFiles(fileDelService.getFiles(fzdy2.getActivistFiles(),session));

		fzdy.setDevelopTime(fzdy2.getDevelopTime());
		fzdy.setDevelopCultivateTime(fzdy2.getDevelopCultivateTime());
		fzdy.setDevelopUser(generalSqlComponent.query(SqlCode.getFzdyPartyLinkSubjectList,new Object[]{fzdy2.getId(),NumberUtils.num300}));
//		fzdy.setDevelopUserId(fzdy2.getDevelopUserId());
//		fzdy.setDevelopUserName(fzdy2.getDevelopUserName());
		fzdy.setDevelopFiles(fileDelService.getFiles(fzdy2.getDevelopFiles(),session));

		fzdy.setReadyOkTime(fzdy2.getReadyOkTime());
		fzdy.setReadyApprovalTime(fzdy2.getReadyApprovalTime());
		fzdy.setReadyCode(fzdy2.getReadyCode());
		fzdy.setReadyFiles(fileDelService.getFiles(fzdy2.getReadyFiles(),session));
		fzdy.setReadyPoliticalReviewFiles(fileDelService.getFiles(fzdy2.getReadyPoliticalReviewFiles(),session));
		fzdy.setSpeakers(fzdy2.getSpeakers());//预备党员谈话人
		fzdy.setSpeakersTime(fzdy2.getSpeakersTime());//预备党员谈话时间

		fzdy.setFormalTime(fzdy2.getFormalTime());
		fzdy.setFormalApprovalTime(fzdy2.getFormalApprovalTime());
		fzdy.setFormalfiles(fileDelService.getFiles(fzdy2.getFormalFiles(),session));
		fzdy.setDelay(fzdy2.getDelay());//正式党员：是否延期预备党员
		fzdy.setMaterialArchive(fzdy2.getMaterialArchive());//正式党员：材料是否归档
		return fzdy;
	}
	@Transactional(rollbackFor=Exception.class)
	public int updateFzdyPartyLink(FzdyPartyLinkDto3 bean, String saveType, Session session){

		try {

			if(bean.getType()== NumberUtils.num100){
				generalSqlComponent.update(SqlCode.updateDylb,new Object[]{shenqingren,bean.getUserid()});
			}
			if(bean.getType()== NumberUtils.num200){
				generalSqlComponent.update(SqlCode.updateDylb,new Object[]{rudangjijifenzi,bean.getUserid()});
			}
			if(bean.getType()== NumberUtils.num300){
				generalSqlComponent.update(SqlCode.updateDylb,new Object[]{fazhanduixiang,bean.getUserid()});
			}
			if(bean.getType()== NumberUtils.num400){
				generalSqlComponent.update(SqlCode.updateDylb,new Object[]{yubeidangyuan,bean.getUserid()});
			}
			if(bean.getType()== NumberUtils.num500){
				generalSqlComponent.update(SqlCode.updateDylb,new Object[]{zhengshidangyuan,bean.getUserid()});
				//点击完成
				if(bean.getSavetype().equals(NumberUtils.NEXT)){
					//同步党员数据
					//想sys_user表插入数据
					addSysUser(bean,session);
				}
			}
			int i = 0;
			if(bean.getType()== NumberUtils.num600){
				bean.setType(Long.valueOf(NumberUtils.num500));
				i = generalSqlComponent.update(SqlCode.updaeFzdyPartyLink,bean);
			}else{
				i = generalSqlComponent.update(SqlCode.updaeFzdyPartyLink,bean);
			}

			if(bean.getApplyTalkUser().size()>0){
				delFzdyPartyLinkSubject(bean.getId(),String.valueOf(NumberUtils.num100));
				for(Map<String,Object> li:bean.getApplyTalkUser()){
					addFzdyPartyLinkSubject(bean.getId(),String.valueOf(NumberUtils.num100),li.get("userid").toString(),li.get("username").toString());
				}
			}
			if(bean.getActivistFosterUser().size()>0){
				delFzdyPartyLinkSubject(bean.getId(),String.valueOf(NumberUtils.num200));
				for(Map<String,Object> li:bean.getActivistFosterUser()){
					addFzdyPartyLinkSubject(bean.getId(),String.valueOf(NumberUtils.num200),li.get("userid").toString(),li.get("username").toString());
				}
			}
			if(bean.getDevelopUser().size()>0){
				delFzdyPartyLinkSubject(bean.getId(),String.valueOf(NumberUtils.num300));
				for(Map<String,Object> li:bean.getDevelopUser()){
					addFzdyPartyLinkSubject(bean.getId(),String.valueOf(NumberUtils.num300),li.get("userid").toString(),li.get("username").toString());
				}
			}

			return i;
		} catch (Exception e) {
			LehandException.throwException("数据保存错误");
			return 0;
		}

	}

	public String exportFzdy(HttpServletResponse response,String orgid,String stageid,String name,String idcard, Session session){
		Map<String,String> parms = new HashMap<String,String>();
		parms.put("organid",orgid);
		parms.put("nameStr",name);
		parms.put("codeStr",idcard);
		parms.put("type",stageid);
		List<Map<String,Object>>  list = generalSqlComponent.query(SqlCode.getFzdyList,parms);
		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("姓名");
		tables.add("性别");
		tables.add("身份证号");
		tables.add("民族");
		tables.add("全日制学历");
		tables.add("年龄");
		tables.add("籍贯");
		tables.add("毕业时间");
		tables.add("毕业院校及专业");
		tables.add("学位");
		tables.add("最高学历");
		tables.add("工作时间");
		tables.add("工作岗位");
		tables.add("技术职称");
		tables.add("联系电话");
		tables.add("家庭住址");

		List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(Map<String,Object> li:list){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("姓名",li.get("xm")==null?"":li.get("xm").toString());
			tempMap.put("性别",li.get("xb")==null?"":li.get("xb").toString());
			tempMap.put("身份证号",li.get("zjhm")==null?"":li.get("zjhm").toString());
			tempMap.put("民族",li.get("mz")==null?"":li.get("mz").toString());
			tempMap.put("全日制学历",li.get("xl")==null?"":li.get("xl").toString());
			tempMap.put("年龄",li.get("age")==null?"":li.get("age").toString());
			tempMap.put("籍贯",li.get("jg")==null?"":li.get("jg").toString());
			tempMap.put("毕业时间",li.get("bysj")==null?"":li.get("bysj").toString());
			tempMap.put("毕业院校及专业",li.get("byyx")==null?"":li.get("byyx").toString());
			tempMap.put("学位",li.get("xw")==null?"":li.get("xw").toString());
			tempMap.put("最高学历",li.get("zgxl")==null?"":li.get("zgxl").toString());
			tempMap.put("工作时间",li.get("cjgzrq")==null?"":li.get("cjgzrq").toString());
			tempMap.put("工作岗位",li.get("gzgw")==null?"":li.get("gzgw").toString());
			tempMap.put("技术职称",li.get("jszc")==null?"":li.get("jszc").toString());
			tempMap.put("联系电话",li.get("lxdh")==null?"":li.get("lxdh").toString());
			tempMap.put("家庭住址",li.get("xjzd")==null?"":li.get("xjzd").toString());
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "发展党员信息";
		ExcelUtils.write(response,tablesStr,explist,tableName);

		return null;
	}

	/**
	* @Description  保存发展党员5环节相关人员
	* @Author zwd
	* @Date 2020/11/17 13:43
	* @param 
	* @return 
	**/
	public String  addFzdyPartyLinkSubject(String linkid,String type,String userid,String username){
		//插入相关人员
		generalSqlComponent.insert(SqlCode.addFzdyPartyLinkSubject,new Object[]{linkid,type,userid,username});
		return null;
	}
	public String  delFzdyPartyLinkSubject(String linkid,String type){

		//删除相关人员
		generalSqlComponent.delete(SqlCode.delFzdyPartyLinkSubject,new Object[]{linkid,type});
		return null;
	}

	public String addSysUser(FzdyPartyLinkDto3 bean,Session session){
		//查询t_dy_info表
		Map<String,Object> dyinfo= generalSqlComponent.query(SqlCode.getDyInfo,new Object[]{bean.getUserid()});
		Map<String,Object> dzzinfo= generalSqlComponent.query(SqlCode.getDzzInfo,new Object[]{dyinfo.get("organid")});
		//查询t_dzz_info表
		List<String> list = new ArrayList<String>();
		list.add(dyinfo.get("xm").toString());//0
		list.add(dyinfo.get("zjhm").toString());//1
		list.add("无用信息");//2
		list.add("无用信息");//3
		list.add("无用信息");//4
		list.add("无用信息");//5
		list.add("无用信息");//6
		list.add("无用信息");//7
		list.add("无用信息");//8
		list.add("无用信息");//9
		list.add("无用信息");//10
		list.add("无用信息");//11
		list.add("无用信息");//12
		list.add("无用信息");//13
		list.add("无用信息");//14
		list.add("无用信息");//15
		list.add("无用信息");//16
		list.add("无用信息");//17
		list.add(null);//18
		list.add("无用信息");//19
		list.add(dzzinfo.get("dzzmc").toString());//20
		memberExcelService.insertSysuser(list,dyinfo.get("organid").toString(),bean.getUserid());
		//查询sys_user表
		List<Map<String,Object>> listmap = generalSqlComponent.query(SqlCode.getSysUserList,new Object[]{bean.getUserid()});
		dataSynchronizationService.syncUserInfos2(listmap);
		return null;
	}
	public void addHisLink(FzdyPartyLinkDto3 bean, Session session) {
		generalSqlComponent.getDbComponent().insert("INSERT INTO fzdy_party_link_his (userid, formalApprovalTime, delay, materialArchive, formalFiles) VALUES (:userid, :formalApprovalTime, :delay, :materialArchive, :formalFiles);",bean);
	}

	public Pager pageHisLink(Pager pager, Session session,String userid) {
		Map<String, Object> map = new HashMap<>();
		map.put("userid",userid);
		generalSqlComponent.pageQuery(SqlCode.pageHisLink,map,pager);
		List<Map<String,Object>> rows = (List<Map<String, Object>>) pager.getRows();

		for (Map<String,Object> m: rows) {
			List<Map<String, Object>> listFiles = fileDelService.getFiles(String.valueOf(m.get("formalFiles")), session);
			m.put("listFiles",listFiles);
		}
		return pager;
	}
}
