package com.lehand.developing.party.service.fzdy.step;

import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.developing.party.service.fzdy.BaseStep;


@Service
public class TFzdyWsybdyStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_wsybdy where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		return map;
	}
	


	//rdsj
	//dxzmc
	//tzbrrq
	//person_id
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("perfect_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		
		int num = db().delete("UPDATE t_fzdy_wsybdy SET syncstatus='D' WHERE person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_wsybdy (compid, perfect_id, rdsj, dxzmc, tzbrrq, created_by, create_date, delete_flag, person_id, syncstatus, synctime)" + 
				" VALUES (:compid,:perfect_id,:rdsj,:dxzmc,:tzbrrq,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime)", params);
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		
		saveFile(params);
		return num;
	}
}
