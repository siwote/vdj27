package com.lehand.developing.party.service.fzdy.handle;

import java.util.HashMap;
import java.util.Map;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.developing.party.common.sql.SqlCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.base.common.Pager;
import com.lehand.developing.party.common.constant.Constant;
import com.lehand.developing.party.service.fzdy.BaseStep;


/**
 * Audit related method implementation 
 * @author pantao
 *
 */
@Service
public class FzdyDzzspHandle extends BaseStep {
	
	/**
	 * description：Review page list query
	 * @param :type (分类编码：a入党积极分子审批 onecode  ；b发展对象审批 twocode ；c政治审查 threecode ；
     *                     d预备党员预审 fourcode ； e预备党员接收审批 fivecode ； 
     *                     f 预备党员转正审批 sixcode ; g信息修改审批 sevencode)
     *        status 状态  0未审核  2 已审核（1通过 -1驳回）
	 */
	public Pager page(Pager pager, Map<String, Object> params, Session session){
		 Map<String, Object> param = new HashMap<String, Object>();
		 param.put("compid", session.getCompid());
		 param.put("audit_id", session.getUserid());
		 param.put("type", params.get("type"));
		 String status = params.get("status").toString();
		 if("0".equals(status)) {
			 param.put("status", params.get("status"));
			 db().pageMap(pager, "select * from fzdy_dzzsp where compid=:compid and audit_id=:audit_id and type=:type and status=:status", param);
		 }else {
			 db().pageMap(pager, "select * from fzdy_dzzsp where compid=:compid and audit_id=:audit_id and type=:type and status!=0", param); 
		 }
		return pager;
	}

	
	
	/**
	 * Description:Realization of the Party's General Branch and the Party Committee's Approval Interface
	 * @author PT  
	 * @date 2019年8月13日 
	 * @param params id status(通过传1 驳回传-1)
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void audit(Map<String, Object> params, Session session) {
		params.put("compid", session.getCompid());
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		db().update("update fzdy_dzzsp set status=:status,moditime=:moditime where compid=:compid and id=:id", params);
	}



	/**
	 * Description: 判断每步是否可以编辑
	 * @author PT  
	 * @date 2019年8月23日 
	 * @param params (person_id,type)
	 * @param session
	 */
	public Map<String,Object> getMap(Map<String, Object> params, Session session) {
		params.put("compid", session.getCompid());
		return db().getMap("select * from fzdy_bzxx where compid=:compid and type=:type and person_id=:person_id", params);
	}



	/**
	 * Description: 申请修改信息审核
	 * @author PT  
	 * @date 2019年8月31日 
	 * @param params(person_id id)
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void auditBaseMesg(Map<String, Object> params, Session session) {
		params.put("compid", session.getCompid());
		params.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		String person_id = params.get("person_id").toString();
		db().update("update fzdy_dzzsp set status=:status,moditime=:moditime where compid=:compid and id=:id", params);
		int status = Integer.valueOf(params.get("status").toString());
		if(status==1) {//审核通过
			//更新fzdy_bzxx中的uneditable为0可编辑状态
			db().update("update fzdy_bzxx set uneditable=0 where compid=? and person_id=? and type=?", new Object[] {session.getCompid(),person_id,params.get("type")});
		}else {//不通过
			db().update("update fzdy_bzxx set uneditable=1 where compid=? and person_id=? and type=?", new Object[] {session.getCompid(),person_id,params.get("type")});
		}
		
		//更新审核人的待办为已办
		db().update("update my_to_do_list set ishandle=1 where module=4 and userid=? and busid=?", new Object[] {session.getUserid(),Constant.SEVEN});
	}



	/**
	 * Description:发展党员巡查列表分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param session
	 * @param pager
	 * @param stageid
	 * @param name
	 * @param idcard
	 * @return
	 */
	public Pager page2(Session session, Pager pager, Integer stageid, String name, String idcard,String orgid) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("type", "");
		params.put("nameStr", name);
		params.put("codeStr", idcard);
		params.put("organid",orgid );
		params.put("stageid",stageid );
		params.put("compid", session.getCompid());
		return generalSqlComponent.pageQuery(SqlCode.FZDY_PAGE2, params, pager);
	}

}
