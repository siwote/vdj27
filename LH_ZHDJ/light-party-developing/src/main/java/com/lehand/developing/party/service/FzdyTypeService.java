package com.lehand.developing.party.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.dto.FzdyTypeDto;
import com.lehand.developing.party.pojo.FzdyType;

/**
 * @ClassName:：FzdyTypeBusinessImpl 
 * @Description： 步骤分类实现层
 * @author ：taogang  
 * @date ：2019年7月8日 上午10:01:18 
 *
 */
@Service
public class FzdyTypeService {
	
	@Resource private GeneralSqlComponent generalSqlComponent;
	
	/**
	 * 新增步骤分类
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean addFzdyType(FzdyType type, Session session) {
		type.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		type.setStatus(1);
		type.setCompid(session.getCompid());
		type.setCreator(session.getUserid());
//		type.setOrgid(session.getOrganids().get(0).getOrgid());
//		type.setOrgname(session.getOrganids().get(0).getOrgname());
		type.setOrgid(session.getCurrentOrg().getOrgid());
		type.setOrgname(session.getCurrentOrg().getOrgname());
		//添加排序号
		Integer seqNo = generalSqlComponent.insert(SqlCode.querySeqNo, new Object[] {type.getPid(),session.getCompid()});
		if(null != seqNo) {
			type.setSeqno(seqNo + 10);
		}else {
			type.setSeqno(10);
		}
		Long typeId = generalSqlComponent.insert(SqlCode.addFzdyType, type);
		if(null == typeId) {
			return false;
		}
		return true;
	}
	
	/**
	 * 修改步骤分类
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean updateFzdyType(FzdyType type, Session session) {
		Long id = type.getId();
		FzdyType oldInfo = queryFzdyTypeById(id, session);
		oldInfo = updateFzdyInfo(oldInfo, type);
		oldInfo.setCompid(session.getCompid());
		oldInfo.setModitor(session.getUserid());
//		oldInfo.setOrgid(session.getOrganids().get(0).getOrgid());
//		oldInfo.setOrgname(session.getOrganids().get(0).getOrgname());
		oldInfo.setOrgid(session.getCurrentOrg().getOrgid());
		oldInfo.setOrgname(session.getCurrentOrg().getOrgname());
		generalSqlComponent.update(SqlCode.updateFzdyType, oldInfo);
		return true;
	}
	
	private FzdyType updateFzdyInfo(FzdyType oldInfo,FzdyType type) {
		oldInfo.setModitime(DateEnum.YYYYMMDDHHMMDD.format());
		oldInfo.setName(type.getName());
		oldInfo.setPid(type.getPid());
		oldInfo.setSeqno(type.getSeqno());
		oldInfo.setSftip(type.getSftip());
		return oldInfo;
	}
	
	/*
	 * <p>Title：delFzdyType</p> 
	 * <p>Description：删除分类 </p> 
	 * @param id
	 * @param session
	 * @return 
	 * @see com.lehand.developing.party.common.business.FzdyTypeBusiness#delFzdyType(java.lang.Long, com.lehand.components.web.common.Session)
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean delFzdyType(Long id, Session session) {
		//当前记录
		FzdyType info = queryFzdyTypeById(id, session);
		if(null == info) {
			return false;
		}
		Integer count = generalSqlComponent.query(SqlCode.queryFzdyTypeCountByPid, new Object[] {id,session.getCompid()});
		if(count > 0) {
			return false;
		}
		generalSqlComponent.update(SqlCode.delFzdyType, new Object[] {id,session.getCompid()});
		return true;
	}

	
	public List<FzdyTypeDto> queryFzdyType(String showType, String likeStr, Session session) {
		//定义参数
		Map<String, Object> params = new HashMap<String, Object>(3);
		params.put("compid", session.getCompid());
		
		if(!StringUtils.isEmpty(likeStr)) {
			params.put("likeStr", likeStr);
		}
		
		if("1".equals(showType)) {
			params.put("showType", showType);
		}
		//查询步骤分类集合
		List<FzdyTypeDto> list = new ArrayList<FzdyTypeDto>();
		List<FzdyTypeDto> typeList = generalSqlComponent.query(SqlCode.queryFzdyType, params);
		if(typeList.size() > 0) {
//			if("1".equals(showType) || !StringUtils.isEmpty(likeStr)) {
//				FzdyTypeDto dto = null;
//				for (FzdyTypeDto fzdyTypeDto : typeList) {
//					list.add(fzdyTypeDto);
//					dto = generalSqlComponent.query(SqlCode.queryFzdyTypeByPid, new Object[] {fzdyTypeDto.getPid(),session.getCompid()});
//					if(null != dto) {
//						list.add(dto);
//					}
//				}
//				//去重
//				list = removeDuplicate(list);
//			}else {
//				list = typeList;
//			}
			List<FzdyTypeDto> tree = markTree(typeList, 0L);
			return tree;
		}
		return null;
	}
	
	/**
	 * @Title：removeDuplicate 
	 * @Description：list 去重
	 * @param ：@param list
	 * @param ：@return 
	 * @return ：List<FzdyTypeDto> 
	 * @throws
	 */
	public List<FzdyTypeDto> removeDuplicate(List<FzdyTypeDto> list){
		for (int i = 0; i< list.size() -1; i++) {
			 for (int j = list.size()-1; j > i; j--) {
				if(list.get(j).getId().equals(list.get(i).getId())) {
					list.remove(j);
				}
			 }
		}
		return list;
	}
	
	/**
	 * 
	 * @Title：markTree 
	 * @Description：递归树
	 * @param ：@param items
	 * @param ：@param pid
	 * @param ：@return 
	 * @return ：List<FzdyTypeDto> 
	 * @throws
	 */
	public List<FzdyTypeDto> markTree(List<FzdyTypeDto> items,Long pid){
		 // 子类
        List<FzdyTypeDto> children = items.stream().filter(x -> x.getPid().equals(pid)).collect(Collectors.toList());
        // 后辈中的非子类
        List<FzdyTypeDto> successor = items.stream().filter(x -> !x.getPid().equals(pid)).collect(Collectors.toList());
        children.forEach(x -> {
        	if(x.getPid() == 0) {
        		x.setDisabled(true);
        	}else {
        		x.setDisabled(false);
        	}
            markTree(successor, x.getId()).forEach(y -> x.getChildren().add(y));
        });
        
        return children;
	}
	
	/**
	 * 根据id查询步骤分类信息
	 */
	
	public FzdyType queryFzdyTypeById(Long id, Session session) {
		return generalSqlComponent.query(SqlCode.queryFzdyTypeById, new Object[] {id,session.getCompid()});
	}
}
