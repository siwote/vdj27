package com.lehand.developing.party.controller;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.developing.party.pojo.FzdyMbpz;
import com.lehand.developing.party.service.FzdyTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @ClassName:：FzdyTemplateController 
 * @Description： 模板配置控制层
 * @author ：taogang  
 * @date ：2019年7月8日 下午2:47:17 
 *
 */
@Api(value = "模板服务", tags = { "模板配置控制层" })
@RestController
@RequestMapping("/developing/temp")
public class FzdyTemplateController extends FzdyBaseController {
	
	private static final Logger logger = LogManager.getLogger(FzdyTemplateController.class);
	
	@Resource private FzdyTemplateService fzdyTemplateService;

	@OpenApi(optflag=0)
	@ApiOperation(value = "分页查询模板列表", notes = "分页查询模板列表", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "pageNo", value = "当前页码", required = true, paramType = "query", dataType = "Long", defaultValue = "1"),
		@ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "Long", defaultValue = "10"),
		@ApiImplicitParam(name = "sort", value = "排序字段", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "direction", value = "排序方式  asc  desc", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤分类id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/pageTemplate")
	public Message pageTemplate(@ApiIgnore Pager pager,Long typeid) {
		Message message = null;
		try {
			if(null == typeid) {
				message = Message.FAIL;
				message.setMessage("步骤编码不可为空");
				return message;
			}
			
			Pager result = fzdyTemplateService.queryTemplateByTypeId(pager, typeid, getSession());
			message = Message.SUCCESS;
			message.setData(result);
			message.setMessage("查询模板列表成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}

	@OpenApi(optflag=1)
	@ApiOperation(value = "保存模板", notes = "保存模板", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤分类id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "fileids", value = "文件id集合，以英文;分割", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/saveTemplate")
	public Message saveTemplate(@ApiIgnore FzdyMbpz info,String fileids) {
		Message message = null;
		try {
			boolean success = false;
			String[] filesLisgt = fileids.split(";");
			for (String fileid : filesLisgt) {
				success = fzdyTemplateService.addTemplate(info, Long.valueOf(fileid), getSession());
			}
//			if(null == info.getId()) {
//				success = fzdyTemplateService.addTemplate(info, fileid, getSession());
//			}else {
//				success = fzdyTemplateService.updateTemplate(info, fileid, getSession());
//			}
			//判断是否成功
			if(!success) {
				message = Message.FAIL;
				message.setMessage("模板保存失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("保存模板成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=3)
	@ApiOperation(value = "删除模板", notes = "删除模板", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "模板id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "fileid", value = "文件id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/delTemplate")
	public Message delTemplate(Long id,Long fileid) {
		Message message = null;
		try {
			if(null == id) {
				message = Message.FAIL;
				message.setMessage("模板id不可为空");
				return message;
			}
			if(null == fileid) {
				message = Message.FAIL;
				message.setMessage("文件id不可为空");
				return message;
			}
			fzdyTemplateService.delTemplate(id, fileid, getSession());
			message = Message.SUCCESS;
			message.setMessage("删除模板成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	

}
