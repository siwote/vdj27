package com.lehand.developing.party.common.sql;

/**
 * 
 * @ClassName:：SqlCode 
 * @Description：数据库sql语句静态常量
 * @author ：taogang  
 * @date ：2019年7月8日 上午10:04:38 
 *
 */
public class SqlCode {
	
	//*********************************************步骤分类sql***********************************************************
	
	/**
	 * 新增步骤分类
	 * INSERT INTO fzdy_type (compid, id, name, pid, seqno, status, sftip, createtime, moditime, creator, moditor, orgid, orgname, remark, remark1, remark2, remark3, remark4, remark5, remark6) VALUES(:compid, :id, :name, :pid, :seqno, :status, :sftip, :createtime, :moditime, :creator, :moditor, :orgid, :orgname, :remark, :remark1, :remark2, :remark3, :remark4, :remark5, :remark6)
	 */
	public final static String addFzdyType = "addFzdyType";
	  
	/**
	 * 修改步骤分类  
	 * UPDATE fzdy_type SET name=:name, pid=:pid, seqno=:seqno, status=:status, sftip=:sftip, createtime=:createtime, moditime=:moditime, creator=:creator, moditor=:moditor, orgid=:orgid, orgname=:orgname, remark=:remark, remark1=:remark1, remark2=:remark2, remark3=:remark3, remark4=:remark4, remark5=:remark5, remark6=:remark6 WHERE id=:id and compid=:compid
	 */
	public final static String updateFzdyType = "updateFzdyType";
	
	/**
	 * 删除步骤分类
	 * update fzdy_type set status = -99 where id = ? and compid = ?
	 */
	public final static String delFzdyType = "delFzdyType";
	
	/**
	 * 查询步骤分类tree
	 * select id,pid,name as label,false as isedit,false as isshow,false as checked,seqno as orderid,"folder" as icon,true as disabled,sftip from fzdy_type where status=1 and compid = '%${PARAMS.compid}%' and name like '%${PARAMS.name}%'
	 */
	public final static String queryFzdyType= "queryFzdyType";
	
	/**
	 * 根据pid查询所有父节点
	 * select id,pid,name as label,false as isedit,false as isshow,false as checked,seqno as orderid,"folder" as icon,true as disabled,sftip from fzdy_type where status=1 and id=? and compid=?
	 */
	public final static String queryFzdyTypeByPid = "queryFzdyTypeByPid";
	
	/**
	 * 根据id查询父节点下是否有数据
	 * select count(1) from fzdy_type where status = 1 and pid=? and compid = ? 
	 */
	public final static String queryFzdyTypeCountByPid = "queryFzdyTypeCountByPid";
	
	/**
	 * 根据id查询步骤分类信息
	 * select * from fzdy_type where status = 1 and id = ? and compid = ?
	 */
	public final static String queryFzdyTypeById = "queryFzdyTypeById";
	
	/**
	  *  查询分类排序号
	 * select seqno from fzdy_type where pid = ? and compid = ? order by createtime desc limit 1
	 */
	public final static String querySeqNo = "querySeqNo";
	
	
	//**********************************************模板配置sql**********************************************************
	
	/**
	 * 新增模板
	 * INSERT INTO fzdy_mbpz (compid, id, typeid, orderid, status, createtime, creator, orgid, orgname, remark, remark1, remark2, remark3, remark4) VALUES(:compid, :id, :typeid, :orderid, :status, :createtime, :creator, :orgid, :orgname, :remark, :remark1, :remark2, :remark3, :remark4)
	 */
	public final static String addTemplate = "addTemplate";
	
	/**
	 * 修改模板
	 * UPDATE fzdy_mbpz SET typeid=:typeid, orderid=:orderid, status=:status, moditime=:moditime, moditor=:moditor, orgid=:orgid, orgname=:orgname, remark=:remark, remark1=:remark1, remark2=:remark2, remark3=:remark3, remark4=:remark4 WHERE id=:id and compid=:compid
	 */
	public final static String updateTempLate = "updateTempLate";
	
	/**
	 * 删除模板
	 * delete from fzdy_mbpz where id = ? and compid = ?
	 */
	public final static String delTemplate = "delTempLate";
	
	/**
	 * 查询模板列表
	 * select m.id,b.fileid,c.name,c.dir,c.viewfilepath from fzdy_mbpz m inner join dl_file_business b on m.id = b.businessid and m.compid = b.compid inner join dl_file c on b.fileid = c.id and c.compid = b.compid where m.compid = ? and m.typeid = ? and b.businessname = ?
	 */
	public final static String queryTemplateByTypeId = "queryTemplateByTypeId";
	
	/**
	 * 根据id查询模板信息
	 * select * from fzdy_mbpz where id = ? and compid = ?
	 */
	public final static String queryTempById = "queryTempById";
	
	/**
	 * 删除文件关联表
	 * delete from dl_file_business where fileid = ? and businessid = ? and businessname = ? and compid = ?
	 */
	public final static String delFileBusinessByMb = "delFileBusinessByMb";
	
	
	//********************************************要求配置************************************************************************
	
	/**
	 * 新增要求配置
	 * INSERT INTO fzdy_workflow (compid, id, typeid, sfflow, workflow, orderid, status, createtime, creator, orgid, orgname, remark, remark1, remark2, remark3, remark4) VALUES(:compid, :id, :typeid, :sfflow, :workflow, :orderid, :status, :createtime, :creator, :orgid, :orgname, :remark, :remark1, :remark2, :remark3, :remark4)
	 */
	public final static String addStandard = "addStandard";
	
	/**
	 * 修改要求配置
	 * UPDATE fzdy_workflow SET typeid=:typeid, sfflow=:sfflow, workflow=:workflow, moditime=:moditime, moditor=:moditor, orgid=:orgid, orgname=:orgname, remark=:remark, remark1=:remark1, remark2=:remark2, remark3=:remark3, remark4=:remark4 WHERE id=:id and compid=:compid
	 */
	public final static String updateStandard= "updateStandard";
	
	/**
	 * 根据分类编码查询要求配置信息
	 * select id,typeid,sfflow as flag,workflow as content from fzdy_workflow where typeid=:typeid and compid=:compid and sfflow=:flag
	 */
	public final static String queryStandardByTypeId = "queryStandardByTypeId";
	
	/**
	 * 根据要求配置id查询要求配置
	 * select * from fzdy_workflow where id = ? and compid = ?
	 */
	public final static String queryStandardById = "queryStandardById";
	
	//********************************************书写规范************************************************************************
	
	/**
	 * 新增书写规范
	 * INSERT INTO fzdy_writenorm (compid, id, typeid, sfnorm, writenorm, orderid, status, createtime, creator, orgid, orgname, remark, remark1, remark2, remark3, remark4) VALUES(:compid, :id, :typeid, :sfnorm, :writenorm, :orderid, :status, :createtime, :creator, :orgid, :orgname, :remark, :remark1, :remark2, :remark3, :remark4)
	 */
	public final static String addAsk = "addAsk";
	
	/**
	 * 修改书写规范
	 * UPDATE fzdy_writenorm SET typeid=:typeid, sfnorm=:sfnorm, writenorm=:writenorm, status=:status, moditime=:moditime, moditor=:moditor, orgid=:orgid, orgname=:orgname, remark=:remark, remark1=:remark1, remark2=:remark2, remark3=:remark3, remark4=:remark4 WHERE id=:id and compid=:compid
	 */
	public final static String updateAsk = "updateAsk";
	
	/**
	 * 根据步骤编码查询书写规范
	 * select id,typeid,sfnorm as flag,writenorm as content from fzdy_writenorm where typeid=:typeid and compid=:compid and sfnorm=:flag
	 */
	public final static String queryAskByTypeId = "queryAskByTypeId";
	
	/**
	 * 根据书写规范id查询书写规范信息
	 * select * from fzdy_writenorm where id = ? and compid = ?
	 */
	public final static String queryAskById = "queryAskById";
	
	//****************************************************消息模板*************************************************************
	
	/**
	 * 新增消息模板
	 * INSERT INTO msg_template (compid, tlcode, seqno, tlname, cond, channel, msgtpl, msgurltpl, status, remarks1, remarks2, remarks3, remarks4) VALUES(:compid, :tlcode, :seqno, :tlname, :cond, :channel, :msgtpl, :msgurltpl, :status, :remarks1, :remarks2, :remarks3, :remarks4)
	 */
	public final static String addMsgTemp = "addMsgTemp";
	
	/**
	 * 修改消息模板
	 * UPDATE msg_template SET tlname=:tlname, cond=:cond, channel=:channel, msgtpl=:msgtpl, msgurltpl=:msgurltpl, status=:status, remarks1=:remarks1, remarks2=:remarks2, remarks3=:remarks3, remarks4=:remarks4 WHERE compid=:compid AND tlcode=:tlcode
	 */
	public final static String updateMsgTemp = "updateMsgTemp";
	
	/**
	 * 删除消息模板
	 * delete from msg_template where tlcode = ? and compid = ? and remarks1 = ?
	 */
	public final static String delMsgTemp = "delMsgTemp";
	
	/**
	  * 分页查询消息模板
	 * select * from msg_template where compid = ? and remarks1 = ?
	 */
	public final static String queryPageMsgTemp = "queryPageMsgTemp";
	
	/**
	 * 根据消息模板code查询消息模板
	 * select * from msg_template where tlcode = ? and compid = ? and remarks1 = ?
	 */
	public final static String queryMsgTempByCode = "queryMsgTempByCode";
	
	/**
	  *  查询分类排序号
	 * select max(seqno) from msg_template where compid = ? and remarks1 = ? 
	 */
	public final static String queryMsgTempSeqNo = "queryMsgTempSeqNo";
	
	
	//**********************************************************提醒规则***************************************************************
	
	/**
	 * 添加提醒时间节点
	 * INSERT INTO fzdy_remindrule_date (compid, id, ruleid, txdate, txunit, time, remark1, remark2, remark3, remark4) VALUES(:compid, :id, :ruleid, :txdate, :txunit, :time, :remark1, :remark2, :remark3, :remark4)
	 */
	public final static String addRemindRuleDate = "addRemindRuleDate";
	
	/**
	 * 删除时间节点
	 * delete from fzdy_remindrule_date where ruleid = ? and compid = ?
	 */
	public final static String delRemindRuleDate = "delRemindRuleDate";
	
	/**
	 * 根据提醒规则id查询时间节点
	 * select * from fzdy_remindrule_date where ruleid = ? and compid = ?
	 */
	public final static String queryRemindRuleDateByRuleId = "queryRemindRuleDateByRuleId";
	
	/**
	 * 根据提醒规则id查询提醒点次数
	 * select count(id) from fzdy_remindrule_date where ruleid = ? and compid =?
	 */
	public final static String queryDateCountByRuleId = "queryDateCountByRuleId";
	
	/**
	 * 分页查询提醒规则列表
	 * select r.id,d.name,r.enddate,r.remark3,r.unit,t.msgtpl from fzdy_remindrule r inner join fzdy_dict d on r.starttstage = d.id inner join msg_template t on r.tempid = t.tlcode where r.typeid = ? and r.remindtype = ? and r.compid = ?
	 */
	public final static String queryPageRuleByTypeId = "queryPageRuleByTypeId";
	
	/**
	 * 根据提醒规则id查询规则详情
	 * select * from fzdy_remindrule where id = ? and compid = ?
	 */
	public final static String queryRemindRuleById = "queryRemindRuleById";
	
	/**
	 * 删除提醒规则
	 * delete from fzdy_remindrule where id = ? and compid= ?
	 */
	public final static String delRemindRuleById = "delRemindRuleById";
	
	/**
	 * 新增提醒规则
	 * INSERT INTO fzdy_remindrule (compid, id, typeid, remindtype, status, starttstage, enddate, unit, tempid, creator, remark, remark1, remark2, remark3, remark4, remark5, remark6) VALUES(:compid, :id, :typeid, :remindtype, :status, :starttstage, :enddate, :unit, :tempid, :creator, :remark, :remark1, :remark2, :remark3, :remark4, :remark5, :remark6);
	 */
	public final static String addRemindRule = "addRemindRule";
	
	/**
	 * 修改提醒规则
	 * UPDATE fzdy_remindrule SET typeid=:typeid, remindtype=:remindtype, status=:status, starttstage=:starttstage, enddate=:enddate, unit=:unit, tempid=:tempid, moditor=:moditor, remark=:remark, remark1=:remark1, remark2=:remark2, remark3=:remark3, remark4=:remark4, remark5=:remark5, remark6=:remark6 WHERE id=:id and compid=:compid
	 */
	public final static String updateRemindRule = "updateRemindRule";
	
	/**
	 * 根据步骤编码id查询下拉框值
	 * select * from fzdy_dict where compid = ? and typeid = ?
	 */
	public final static String getFzdyDictByTypeid = "getFzdyDictByTypeid";
	
	
	//**********************************************************发展党员管理***************************************************************
	
	/**
	 * 发展党员管理 分页
	 * SELECT v.xm,v.xb,c.type,(c.type - 1) nextType FROM  (SELECT a.compid,a.person_id,b.type FROM (SELECT max(createtime) createtime ,person_id ,compid FROM fzdy_bzxx GROUP BY person_id) a LEFT JOIN fzdy_bzxx b ON a.createtime = b.createtime and a.person_id = b.person_id and a.compid = b.compid) c LEFT JOIN  t_dy_info v  ON v.userid = c.person_id 	WHERE 1 = 1 
	 */
	public final static String FZDY_PAGE  = "FZDY_PAGE";
		
	/**
	 * 发展党员管理,新增
	 * INSERT INTO t_dy_info SET userid =  :userid ,organid =:organid ,xm = :xm,xb = :xb,mz = :mz,xl = :xl,csrq = :csrq,zjhm = :zjhm,lxdh = :lxdh,jg = :jg,xjzd = :xjzd,dylb =:dylb
	 */
	public final static String FZDY_INSERT  = "FZDY_INSERT";
	
	
	/**
	 * 发展党员管理表新增
	 * INSERT INTO fzdy_bzxx SET compid = ? ,person_id =? ,type = 100,sfipen = 1,createtime = sysdate()
	 */
	public final static String FZDY_GL_INSERT = "FZDY_GL_INSERT";
	
	/**
	 * 发展党员管理 补充,新增
	 * INSERT INTO t_dy_info_bc SET userid = :userid,dydaszw = :dydaszw,photourl = :photourl
	 */
	public final static String FZDY_BC_INSERT  = "FZDY_BC_INSERT";
	
	/**
	 *	发展党员管理,修改
	 *	update t_dy_info SET xm = :xm,xb = :xb,mz = :mz,xl = :xl,csrq = :csrq,zjhm = :zjhm,lxdh = :lxdh,jg = :jg,xjzd = :xjzd WHERE userid = :userid
	 */
	public final static String FZDY_UPDATE  = "FZDY_UPDATE";
	
	/**
	 * 发展党员管理 补充,修改
	 * update t_dy_info_bc SET dydaszdw = :dydaszdw,photourl = :photourl WHERE userid = :userid 
	 */
	public final static String FZDY_BC_UPDATE = "FZDY_BC_UPDATE";
	
	/**
	 * 发展党员详细信息
	 * SELECT (SELECT dzzmc FROM t_dzz_info WHERE organid = a.organid ) dzzmc ,a.userid,a.xm,a.xb,a.mz,a.csrq,a.xl,a.zjhm, a.organid,a.xjzd,b.photourl ,b.dydaszdw FROM t_dy_info a LEFT JOIN t_dy_info_bc b ON a.userid = b.userid WHERE a.userid = ?
	 */
	public final static String FZDY_DETAIL = "FZDY_DETAIL";
	
	/**
	 * 发展党员 删除/恢复
	 * update t_dy_info set sync_status = ? where userid = ?
	 */
	public final static String FZDY_DELETE = "FZDY_DELETE";
	
	/**
	 * 发展党员 补充 ,删除/恢复
	 * update t_dy_info_bc set sync_status = ? where userid = ?
	 */
	public final static String FZDY_BC_DELETE = "FZDY_BC_DELETE";
	
	/**
	 * 恢复前判断是否已存在
	 * SELECT COUNT(*) FROM t_dy_info WHERE dylb <> 1 AND sync_status = 1 AND zjhm = (SELECT zjhm FROM t_dy_info WHERE userid = ?)
	 */
	public final static String FZDY_RECOVER_CHECK = "FZDY_RECOVER_CHECK";
	
	/**
	 * 发展党员转出
	 * INSERT INTO t_fzdy_zcry SET transfer_id = REPLACE(uuid(), '-', ''),person_id = :personId,created_by = :createdBy,szdzb = :szdzb,sync_status = :syncStatus,zrdzz = :zrdzz,zcrq = :zcrq,zrrq = :zrrq,zcstatus = :zcstatus
	 */
	public final static String FZDY_ROLL_OUT = "FZDY_ROLL_OUT";
	
	
	/**
	 * 发展党员转入接受
	 * UPDATE t_fzdy_zcry SET zcstatus = :zcstatus ,sync_status = :syncStatus,updated_by = :updatedBy,last_update_date = :lastUpdateDate ,zrrq = :zrrq WHERE person_id = :personId
	 */
	public final static String FZDY_SHIFT_TO = "FZDY_SHIFT_TO";
	
	
	/**
	 * 发展党员接收列表
	 *  太长 省略 （老SQL备份）
	 *  SELECT
	v.userid,
	v.xb,
	v.xm,
	c.type,
	(c.type - 1) nextType
FROM
	(
		SELECT
			a.person_id,
			b.type,
			b.compid
		FROM
			t_fzdy_zcry a
		LEFT JOIN (
			SELECT
				d.compid,
				d.person_id,
				e.type
			FROM
				(
					SELECT
						max(createtime) createtime,
						person_id,
						compid
					FROM
						fzdy_bzxx
					GROUP BY
						person_id
				) d
			LEFT JOIN fzdy_bzxx e ON d.createtime = e.createtime
			AND d.person_id = e.person_id
			AND d.compid = e.compid
		) b ON a.person_id = b.person_id
		AND a.zcstatus = 1
	) c
LEFT JOIN (
	SELECT
		xm,
		xb,
		operatetype,
		userid
	FROM
		t_dy_info 
	WHERE
				organid = :organid
) v ON c.person_id = v.userid
WHERE
	v.operatetype <> 4

	 */
	public final static String FZDY_SHIFT_TO_PAGE = "FZDY_SHIFT_TO_PAGE";
	
	/**
	 * 发展党员转接 记录详情
	 * SELECT * FROM t_fzdy_zcry WHERE person_id = ? ORDER BY create_date desc LIMIT 1
	 */
	public final static String FZDY_SHIFT_TO_DETAIL = "FZDY_SHIFT_TO_DETAIL";
	
	/**
	 * 修改党员组织机构
	 */
	public final static String FZDY_UPDATE_ORGANID = "FZDY_UPDATE_ORGANID";
	
	
	/**
	 * 党员类别(取自字典表)
	 * SELECT item_name name,item_code id  FROM `g_dict_item` WHERE code = 'd_dy_rylb'
	 */
	public final static String FZDY_DICT_DYLB = "FZDY_DICT_DYLB";

	public static final String FZDY_PAGE2 = "FZDY_PAGE2";

	/**
	 *  INSERT INTO fzdy_dzzsp (compid, type, audit_id, audit_name,audit_organ_code, person_id, person_name, 
	 *  person_idcard, person_sex, stage_id, stage_name, node_id, node_name, commit_time, commit_orgid, commit_orgname, 
	 *	modi_stage_id,modi_stage_name,modi_node_id, modi_node_name, modi_reason, status, createtime)  
	 *	VALUES (:compid,:type,:audit_id,:audit_name,:audit_organ_code,:person_id,:person_name,:person_idcard,
	 *	:person_sex,:stage_id,:stage_name,:node_id,:node_name,:commit_time,:commit_orgid,:commit_orgname,:modi_stage_id,
	 *	:modi_stage_name,:modi_stage_id,:modi_node_name,:modi_reason,:status,:createtime
	 */
	public static String insertBzxx = "insertBzxx";

	public static String addSmsVerif = "addSmsVerif";  
	
	public static final String getUserByAccount = "getUserByAccount";

	/**
	 * 查询党支部书记
	 * select * from t_dzz_bzcyxx where  dnzwname = '5100000017' and zfbz !=0   and lzdate>synctime  and  organid=:organid  order by rzdate desc limit 1
	 */
	public static final String getZbsj = "getZbsj";

	/**
	 * 向发展党员5环节表插入初始数据
	 *insert into fzdy_party_link SET id = :id ,userid =:userid ,paty =100
	 */
	public static final String fzdy_party_link = "fzdy_party_link";

	/**
	 * 获取发展党员5环节数据
	 */
	public static final String getFzdyPartyLink = "getFzdyPartyLink";

	/**
	 * 修改发展党员5环节
	 * update fzdy_party_link SET type =:type,applyTime=:applyTime,applyTalkUserId=:applyTalkUserId,applyTalkUserName=:applyTalkUserName,applyFiles=:activistFiles
	 * ,activistTime=:activistTime,activistFosterUserId=:activistFosterUserId,activistFosterUserName=:activistFosterUserName,activistFiles=:activistFiles
	 * ,developTime=:developTime,developCultivateTime=:developCultivateTime,developUserId=:developUserId,developUserName=:developUserName,developFiles=:developFiles
	 * ,readyOkTime=:readyOkTime,readyApprovalTime=:readyApprovalTime,readyCode=:readyCode,readyPoliticalReviewFiles=:readyPoliticalReviewFiles,readyFiles=:readyFiles
	 * ,formalTime=:formalTime,formalApprovalTime=:formalApprovalTime,formalFiles=:formalFiles where userid = ?
	 */
	public static final String updaeFzdyPartyLink = "updaeFzdyPartyLink";

	/**
	 * 修改党员类别
	 */
	public static final String updateDylb = "updateDylb";

	/**
	 * 获取导出发展党员信息
	 * select a.userid,a.xm,(SELECT item_name FROM g_dict_item where item_code = a.xb and code = 'd_sexid') as xb,a.zjhm,(SELECT item_name FROM g_dict_item where item_code = a.mz and code = 'd_national') asmz
	 * ,(SELECT item_name FROM g_dict_item where item_code = a.xl and code = 'd_dy_xl') as xl,a.age,a.jg,b.bysj,b.byyx,b.xw,b.zgxl,b.cjgzrq,a.gzgw,b.jszc,a.lxdh,a.xjzd from t_dy_info a
	 * left join t_dy_info_bc b on b.userid = a.userid
	 * where a.organid = ? and dylb = ?
	 */
	public static final String getFzdyList = "getFzdyList";

	/**
	 *根据组织id和党员类别查询
	 * select * from t_dy_info where dylb = ? and organid = ? and delflag!=1
	 */
	public static final String getTDyInfoList = "getTDyInfoList";

	/**
	 * select a.xm,a.zjhm,b.basefee from  t_dy_info a left join h_dy_jfjs b on b.userid= a.userid where a.organid = ? and delflag!=1 and (dylb=? or dylb=?) and b.years=? and b.months=?
	 * 缴费基数导出查询
	 */
	public static final String getdfjsdc = "getdfjsdc";

	/**
	 * 根据组织id和 证件号码查询
	 * select * from t_dy_info where organid =? and zjhm = ? and delflag!=1
	 */
	public static final String getTDyInfoZjhm = "getTDyInfoZjhm";

	/**
	 *
	 * select * from h_dy_jfjs  a left join t_dy_info b on b.userid = a.userid where b.organid =? and b.zjhm = ? and years=? and months = ? and b.delflag!=1
	 */
	public static final String getHDyJfjs = "getHDyJfjs";

	/**
	 * insert h_dy_jfjs set userid=:userid,username=:username,organid=:organid,basefee=:basefee,nopost=:nopost,years=:years,months=:months,compid=:compid
	 * 插入党费基数
	 */
	public static final String insertDfjs = "insertDfjs";

	/**
	 * 修改党费基数
	 * update h_dy_jfjs set basefee=:basefee,nopost=:nopost where userid =:userid and compid=:compid
	 */
	public static final String updateDfjs = "updateDfjs";

	/**
	 * 根据身份账号查询党员
	 *  select * from  t_dy_info where zjhm = ?  and delflag !=1
	 */
	public static final String getDyInfoZjhm = "getDyInfoZjhm";

	/**
	 * 查询党费有没有上报
	 * select * from h_dzz_jfmx where organid = ? and (apstatus = 1 or apstatus = 2 or apstatus = 4) and year = ? and month = ?
	 */
	public static final String getDfjjDr = "getDfjjDr";


	/**
	 * 根据组织id和 证件号码查询正式和预备党员
	 * select * from  t_dy_info where zjhm = ? and organid = ? and (dylb = '1000000001' or dylb ='2000000002')
	 */
	public static final String getTDyInfoZjhm2 = "getTDyInfoZjhm2";

	/**
	 * 插入党员发展5换届相关人员
	 * insert into fzdy_party_link_subject set linkid=:linkid,types=:type,userid=:userid,username=:username
	 */
	public static final String addFzdyPartyLinkSubject = "addFzdyPartyLinkSubject";

	/**
	 * 删除发展党员各环节相关人员
	 * delete fzdy_party_link_subject where linkid=:linkid and types =:type
	 */
	public static final String delFzdyPartyLinkSubject = "delFzdyPartyLinkSubject";

	/**
	 * 获取发展党员各环节相关人员
	 * select * from  fzdy_party_link_subject where linkid = ? and types = ?
	 */
	public static final String getFzdyPartyLinkSubjectList = "getFzdyPartyLinkSubjectList";

	public static final String getDyInfo = "getDyInfo";

	public static final String getDzzInfo = "getDzzInfo";

	/**
	 * 根据userid查询
	 * select * from sys_user where base_info_id =?
	 */
	public static final String getSysUserList = "getSysUserList";


	/**
	 * 获取党费基数
	 * select a.username,b.zjhm,a.basefee,b.organid,a.userid from h_dy_jfjs a
	 * left join t_dy_info b on b.userid = a.userid where compid = 1
	 */
	public static final String getDfjsList = "getDfjsList";

	/**
	 * 删除党费基数
	 * delete  from h_dy_jfjs where organid =? and compid=?
	 */
	public static final String deleteDfjs2 = "deleteDfjs2";

	/**
	 *  select * from t_dzz_info where   (delflag = 0 or delflag is null)
	 * 查询所有未删除的党组织
	 */
	public static final String getAllTDzzInfo = "getAllTDzzInfo";
	/**
	 * 党费统计
	 *SELECT SUM(payable) payable,SUM(actual),COUNT(*) actual FROM h_dy_jfinfo  WHERE month=:month AND year =:year AND organid in (:organid)
	 */
    public static String duesstatistical="duesstatistical";
	/**
	 * 分页获取真实党员延期预备历史
	 * SELECT * FROM fzdy_party_link_his where userid =:userid ORDER BY id DESC
	 */
	public static String pageHisLink="pageHisLink";
}
