package com.lehand.developing.party.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @program: huaikuang-dj
 * @description: FzdyPartyLinkSubject
 * @author: zwd
 * @create: 2020-11-16 16:54
 */
public class FzdyPartyLinkSubject implements Serializable  {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="",name="linkid")
    private String linkid;

    @ApiModelProperty(value="",name="types")
    private String types;

    @ApiModelProperty(value="",name="userid")
    private String userid;

    @ApiModelProperty(value="",name="username")
    private String username;

    public String getLinkid() {
        return linkid;
    }

    public void setLinkid(String linkid) {
        this.linkid = linkid;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}