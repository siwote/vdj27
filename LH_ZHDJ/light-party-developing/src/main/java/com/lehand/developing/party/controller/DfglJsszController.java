package com.lehand.developing.party.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.components.web.util.ServletUtil;
import com.lehand.developing.party.dto.PeisonActualDto;
import com.lehand.developing.party.service.dfgl.dfsjgl.DfglJsszHandle;
import com.lehand.developing.party.service.dfgl.dfsjgl.DfglSjblHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

//import com.lehand.components.db.DBComponent;

@Api(value = "党费管理", tags = { "党费管理" })
@RestController
@RequestMapping("/developing/dfgl")
public class DfglJsszController extends BaseController{
	
	@Resource DfglJsszHandle dfglJsszHandle;
	
	@Resource DfglSjblHandle dfglSjblHandle;

	//党支部code
	@Value("${jk.organ.dangzhibu}")
	private String dangzhibu;
	
	//党支部code
	@Value("${jk.organ.lianhedangzhibu}")
	private String lianhedangzhibu;
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	/**
	 * Description: 党费基数配置查询
	 * @author PT  
	 * @date 2019年8月30日 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费基数配置查询", notes = "党费基数配置查询", httpMethod = "POST")
	@RequestMapping("/list")
	public Message list() {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = dfglSjblHandle.list(getSession());
		    message.setData(list);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("机构下党员集合分页查询出错");
		}
		return message;
		
	}
	
	
	/**
	 * Description: 党费基数配置保存
	 * @author PT  
	 * @date 2019年8月30日 
	 * @param request
	 * @param response
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "党费基数配置保存", notes = "党费基数配置保存", httpMethod = "POST")
	@RequestMapping("/jsszsave")
	public Message jsszsave(HttpServletRequest request, HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			Map<String, Object> params = ServletUtil.params2map(request);
			dfglJsszHandle.save(params, getSession());
			msg.success();
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description: 党费基数配置删除
	 * @author PT  
	 * @date 2019年8月30日 
	 * @param request
	 * @param response
	 * @return
	 */
	@OpenApi(optflag=3)
	@ApiOperation(value = "党费基数配置删除", notes = "党费基数配置删除", httpMethod = "POST")
	@RequestMapping("/delete")
	public Message delete(HttpServletRequest request, HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			//Map<String, Object> params = ServletUtil.params2map(request);
			//dfglJsszHandle.delete(params, getSession());
			msg.success();
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	/**
	 * Description: 党费上缴比例设置
	 * @author PT  
	 * @date 2019年8月19日 
	 * @param request
	 * @param response
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "党费上缴比例设置", notes = "党费上缴比例设置", httpMethod = "POST")
	@RequestMapping("/sjblsave")
	public Message sjblsave(HttpServletRequest request, HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			//Map<String, Object> params = ServletUtil.params2map(request);
			//dfglSjblHandle.save(params, getSession());
			msg.success();
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description: 给各个机构进行调整基数授权
	 * @author PT  
	 * @date 2019年8月21日 
	 * @param :params (sqsave 1授权 0取消授权  organid)
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "给各个机构进行调整基数授权", notes = "给各个机构进行调整基数授权（(sqsave 1授权 0取消授权  organid)）", httpMethod = "POST")
	@RequestMapping("/sqsave")
	public Message sqsave(HttpServletRequest request, HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			//Map<String, Object> params = ServletUtil.params2map(request);
			//dfglSjblHandle.sqsave(params, getSession());
			msg.success();
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * 获取指定节点下的所有子节点(含组织，不含用户)
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "获取指定节点下的所有子节点(含组织，不含用户)", notes = "获取指定节点下的所有子节点(含组织，不含用户)", httpMethod = "POST")
	@RequestMapping("/listOrganChildNodeOne")
	public Message listOrganChildNodeOne(/*String orgname*/) {
		Message msg = new Message();
		try {
			Map<String,Object> nodes = dfglSjblHandle.listOrganChildNodeOne(getSession()/*,orgname*/);
			msg.success().setData(nodes);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	/**
	 * 获取指定节点下的所有子节点(含组织，不含用户)
	 * @param organid
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "获取指定节点下的所有子节点(含组织，不含用户)", notes = "获取指定节点下的所有子节点(含组织，不含用户)", httpMethod = "POST")
	@RequestMapping("/listOrganChildNodeTwo")
	public Message listOrganChildNodeTwo(Long organid/*,String orgname*/) {
		Message msg = new Message();
		try {
			List<Map<String,Object>> nodes = dfglSjblHandle.listOrganChildNodeTwo(getSession().getCompid(),organid/*,orgname*/);
			msg.success().setData(nodes);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	/**
	 * 根据组织机构编码获取改组织党费管理党费基数授权状态
	 * @param orgcode
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "根据组织机构编码获取改组织党费管理党费基数授权状态", notes = "根据组织机构编码获取改组织党费管理党费基数授权状态", httpMethod = "POST")
	@RequestMapping("/getGrandStatusByOrgCode")
	public Message getGrandStatusByOrgCode(String orgcode) {
		Message msg = new Message();
		try {			
			msg.setData(dfglSjblHandle.getGrandStatusByOrgCode(orgcode,getSession()));
			msg.success();
			msg.setMessage("查询成功");
		} catch (Exception e) {
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
    
	/**
	 * Description: 机构下党员集合分页查询
	 * @author PT  
	 * @date 2019年8月21日 
	 * @param xm
	 * @param organid
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "机构下党员集合分页查询", notes = "机构下党员集合分页查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(String xm,String organid,Pager pager) {
		Message message = new Message();
		try {
			//先新增
			dfglSjblHandle.insert(getSession(),organid);
			//后分页查询
			pager = dfglSjblHandle.page(getSession(),xm,organid,pager);
		    message.setData(pager);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("机构下党员集合分页查询出错");
		}
		return message;
		
	}
	
	
	/**
	 * Description: 调整党费基数
	 * @author PT  
	 * @date 2019年8月22日 
	 * @param basefee
	 * @param userid
	 * @param organid
	 * @param onpost
	 * @return
	 */
	@OpenApi(optflag=2)
    @ApiOperation(value = "调整党费基数", notes = "调整党费基数", httpMethod = "POST")
	@RequestMapping("/update")
	public Message update(Double basefee,String userid,String username,String organid,String onpost) {
		Message message = new Message();
		try {
			dfglSjblHandle.update(getSession(),basefee,userid,username,organid,onpost);
		    message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("调整党费基数出错");
		}
		return message;
		
	}
	
	
	/**
	 * Description: 党费上报分页查询
	 * @author PT  
	 * @date 2019年8月22日 
	 * @param month
	 * @param year
	 * @param xm
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "党费上报分页查询", notes = "党费上报分页查询", httpMethod = "POST")
	@RequestMapping("/reportpage")
	public Message reportPage(String organid,String month,String year,String xm,Pager pager) {//之所以多加organid是为了跳转用
		Message message = new Message();
		if(organid==null || "".equals(organid.trim())){
			message.setMessage("参数不能为空");
			return message;
		}
		try {
			Long compid = getSession().getCompid();
			Map<String,Object> map3 = db().getMap("select remarks2,orgtypeid zzlb from urc_organization where compid=? and orgid=? and status = 1", new Object[] {compid,getSession().getCurrorganid()});
			if(!dangzhibu.equals(map3.get("zzlb").toString()) && !lianhedangzhibu.equals(map3.get("zzlb").toString())) {//不是党支部
//				dfglSjblHandle.initialization(getSession(),year,month);
			}else {
//
			}
			pager = dfglSjblHandle.reportPage(getSession(),organid,month,year,xm,pager);
		    message.setData(pager);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费上报分页查询出错");
		}
		return message;
		
	}

	/**
	 * 保存经费设置
	 * @param organid
	 * @param month
	 * @param year
	 * @param compid
	 * @param list
	 */
	void saveHjfinfo(String organid, String month, String year, Long compid, List<Map<String, Object>> list) {
		for (Map<String, Object> map : list) {
//			Map<String, Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and min<=? and max>? limit 1", new Object[]{compid, Double.valueOf(map.get("basefee").toString()), Double.valueOf(map.get("basefee").toString())});
			Map<String, Object> jssz =db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[] {compid, map.get("basefee")});
			if (jssz == null) {
				LehandException.throwException("党费缴纳比例未设置，请先设置再操作！");
			}
			if (map.get("onpost") != null) {
				Map<String, Object> params = dfglSjblHandle.getParams(Double.valueOf(map.get("basefee").toString()), map.get("userid").toString(), map.get("username").toString(), organid, map.get("onpost").toString(), map.get("adjusttime")==null? null:map.get("adjusttime").toString(), map.get("ajuserid")==null? null:Long.valueOf(map.get("ajuserid").toString()), map.get("ajusername")==null? null:map.get("ajusername").toString(), compid, Integer.valueOf(year), Integer.valueOf(month), jssz.get("ratio").toString());
				db().insert("INSERT INTO h_dy_jfinfo (userid, year, month, basefee, rate, payable, actual, status, remark, onpost, organid, username, ajuserid, ajusername, adjusttime, compid) VALUES (:userid, :year, :month, :basefee, :rate, :payable, :actual, :status, :remark, :onpost, :organid, :username, :ajuserid, :ajusername, :adjusttime, :compid)", params);
			}
		}
	}

	@OpenApi(optflag=0)
    @ApiOperation(value = "党费汇总查询", notes = "党费汇总查询", httpMethod = "POST")
	@RequestMapping("/getbean")
	public Message getBean(Integer month,Integer year) {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglSjblHandle.getBean(getSession(),year,month);
		    message.setData(data);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费汇总查询出错");
		}
		return message;
	}
	
	/**
	 * Description: 上报党费列表头部汇总
	 * @author PT  
	 * @date 2019年8月23日 
	 * @param month
	 * @param year
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "上报党费列表头部汇总", notes = "上报党费列表头部汇总", httpMethod = "POST")
	@RequestMapping("/reporttop")
	public Message reportTop(String month,String year) {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglSjblHandle.reportTop(getSession(),month,year);
		    message.setData(data);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费汇总查询出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 党支部更新每个党员的实交党费
	 * @author PT  
	 * @date 2019年8月23日 
	 * @param month
	 * @param year
	 * @param actual
	 * @param payable 应交党费
	 * @return
	 *
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党支部更新每个党员的实交党费", notes = "党支部更新每个党员的实交党费", httpMethod = "POST")
	@RequestMapping("/modiactual")
	public Message modiActual(String month,String year,String actual,String personid,String remark,String payable) {
		Message message = new Message();
		try {
			dfglSjblHandle.modiActual(getSession(),month,year,actual,personid,remark,payable);
		    message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费更新出错");
		}
		return message;
	}

    /**
     * Description: 党支部批量更新党员的实交党费
     * @author lx
     * @date 2021-04-18
     * @return
     *
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党支部批量更新每个党员的实交党费", notes = "党支部批量更新每个党员的实交党费", httpMethod = "POST")
    @RequestMapping("/batchModiactual")
    public Message batchModiactual(@RequestBody List<PeisonActualDto> peisonActualDtos){
        Message message = new Message();
        try {
            dfglSjblHandle.batchModiactual(getSession(),peisonActualDtos);
            message.success();
        } catch (LehandException e) {
            message.setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            message.setMessage("党费更新出错");
        }
        return message;
    }
	/**
	 * Description: 党支部上报党费
	 * @author PT  
	 * @date 2019年8月23日 
	 * @param month
	 * @param year
	 * @param otherfee
	 * @param remark
	 * @param actualout
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党支部上报党费", notes = "党支部上报党费", httpMethod = "POST")
	@RequestMapping("/report")
	public Message report(String month,String year,Double otherfee,String remark,String actualout,String file) {
		Message message = new Message();
		try {
			//如果是党支部上报则首先需要更新h_dzz_jfmx表中的实收党费和实付党费(因为党支部可以修改实收金额)
			if(dangzhibu.equals(getSession().getCurrentOrg().getOrgcode()) || lianhedangzhibu.equals(getSession().getCurrentOrg().getOrgcode())){
				dfglSjblHandle.updateActualout(getSession(),month,year,actualout);
			}
			dfglSjblHandle.report(getSession(),month,year,otherfee,remark,actualout,file);
		    message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费上报出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 上报党费审核
	 * @author PT  
	 * @date 2019年8月26日 
	 * @param month
	 * @param year
	 * @param organid
	 * @param remark
	 * @param status
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "上报党费审核", notes = "上报党费审核", httpMethod = "POST")
	@RequestMapping("/audit")
	public Message audit(String month,String year,String organid,String remark,int status) {
		Message message = new Message();
		try {
			dfglSjblHandle.audit(getSession(),month,year,organid,remark,status);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费审核出错");
		}
		return message;
	}

	/**
	* @Description 党费基数模板下载
	* @Author zwd
	* @Date 2020/11/4 9:15
	* @param
	* @return
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费基数模板下载", notes = "党费基数模板下载", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orgid", value = "支部id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/exportJsMo")
	public Message  exportJsModel(HttpServletResponse response,@RequestBody Map<String,Object> map){
    	Message message = new Message();
    	message.setData(dfglSjblHandle.exportJsModel(response,map.get("orgid").toString(),getSession())).success();
    	return message;
	}

	/**
	* @Description 党费基数导出
	* @Author zwd
	* @Date 2020/11/4 9:38
	* @param
	* @return
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费基数导出", notes = "党费基数导出", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orgid", value = "支部id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/exportJs")
	public Message  exportJs(HttpServletResponse response,@RequestBody Map<String,Object> map){

		Message message = new Message();
		try {
			message.setData(dfglSjblHandle.exportJs(response,map.get("orgid").toString(),getSession())).success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
		}

		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "党费基数导入", notes = "党费基数导入", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "months", value = "月份", required = true, paramType = "query", dataType = "Integer", defaultValue = "")
	})
	@RequestMapping("/ImportJs")
	public Message importJs(HttpServletResponse response,MultipartFile file,String orgid,Integer months) {
		Message message = new Message();
		try {
			message = dfglSjblHandle.importJs(response,file, getSession(),message,orgid,months);
		} catch (Exception e) {
			message.setMessage("导入失败！！");
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "党费收缴管理导出", notes = "党费收缴管理导出", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orgid", value = "支部id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "month", value = "月份", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "xm", value = "姓名", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/dfexport")
	public Message dfExport(HttpServletResponse response,@RequestBody Map<String,Object> map){
		Message message = new Message();
		try {
			message.setData(dfglSjblHandle.dfExport(response,map.get("orgid").toString(),map.get("year").toString(),map.get("month").toString(),map.get("xm").toString(),getSession()));
		} catch (Exception e) {
			message.setMessage(e.getMessage());
		}
		return message;
	}

	/**
	 * @Description 党费基数模板下载
	 * @Author zwd
	 * @Date 2020/11/4 9:15
	 * @param
	 * @return
	 **/
	@OpenApi(optflag=0)
	@ApiOperation(value = "当前所有党费基数模板下载", notes = "当前所有党费基数模板下载", httpMethod = "POST")
	@RequestMapping("/exportJsMoAll")
	public Message  exportJsMoAll(HttpServletResponse response){
		Message message = new Message();
		message.setData(dfglSjblHandle.exportJsModelAll(response,getSession())).success();
		return message;
	}

	/**
	 * @Description 党费基数导出
	 * @Author zwd
	 * @Date 2020/11/4 9:38
	 * @param
	 * @return
	 **/
	@OpenApi(optflag=0)
	@ApiOperation(value = "当前所有党费基数导出", notes = "当前所有党费基数导出", httpMethod = "POST")
	@RequestMapping("/exportJsAll")
	public Message  exportJsAll(HttpServletResponse response){

		Message message = new Message();
		try {
			message.setData(dfglSjblHandle.exportJsAll(response,getSession())).success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
		}

		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "当前所有党费基数导入", notes = "当前所有党费基数导入", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "months", value = "月份", required = true, paramType = "query", dataType = "Integer", defaultValue = "")
	})
	@RequestMapping("/importJsALL")
	public Message importJsALL(HttpServletResponse response,MultipartFile file,Integer months) {
		Message message = new Message();
		try {
			message = dfglSjblHandle.importJsAll(response,file, getSession(),message,months);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
		}
		return message;
	}
	/**
	 * Description: 驾驶舱党员党费统计
	 * @author lx
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "驾驶舱党员党费统计", notes = "驾驶舱党员党费统计", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orgcode", value = "组织编码", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "organid", value = "组织id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/duesStatistical")
	public Message duesStatistical(String orgcode,String organid,String year) {
		Message message = new Message();
		try {
			message.setData(dfglSjblHandle.duesStatistical(getSession(),orgcode,organid,year));
			message.success();
		} catch (Exception e) {
			LehandException.throwException("驾驶舱党员党费统计"+e);
		}
		return message;
	}
    /**
     * Description: 查询维护党费列表页
     * @author lx
     * @date 2201/04/27
     * @param pager
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询维护党费查询列表页", notes = "查询维护党费查询列表页", httpMethod = "POST")
    @RequestMapping("/pageMaintainDues")
    public Message pageMaintainDues(String organid,Pager pager) {
        Message message = new Message();
        if(organid==null || "".equals(organid.trim())){
            message.setMessage("参数不能为空");
            return message;
        }
        try {
            //先新增
            dfglSjblHandle.insert(getSession(),organid);

            pager = dfglSjblHandle.pageMaintainDues(getSession(),organid,pager);
            message.setData(pager);
            message.success();
        } catch (Exception e) {
            e.printStackTrace();
            message.setMessage("维护党费分页查询出错");
        }
        return message;
    }

    /**
     * Description: 修改维护党费
     * @author lx
     * @date 2201/04/28
     * @param organid
     * @param userid
     * @param zjhm
     * @param payable
     * @return
     */
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改维护党费", notes = "修改维护党费", httpMethod = "POST")
    @RequestMapping("/modifyMaintainDues")
    public Message modifyMaintainDues(String organid,String userid,String zjhm,String payable) {
        Message message = new Message();
        if(organid==null || "".equals(organid.trim())){
            message.setMessage("参数不能为空");
            return message;
        }
        try {
            dfglSjblHandle.modifyMaintainDues(getSession(),organid,userid,zjhm,payable);
            message.success();
        } catch (Exception e) {
            e.printStackTrace();
            message.setMessage("修改失败"+e.getMessage());
        }
        return message;
    }
}
