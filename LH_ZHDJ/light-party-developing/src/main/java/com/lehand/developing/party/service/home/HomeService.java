package com.lehand.developing.party.service.home;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.developing.party.utils.SessionUtils;
import org.springframework.stereotype.Service;

import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;

@Service
public class HomeService {

	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	@Resource
	protected SessionUtils sessionUtils;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}

	/**
	 * Description:党组织各时间查询
	 * @author PT  
	 * @date 2019年9月9日 
	 * @param session t_dzz_info_bc 和 t_dzz_hjxx_dzz 表
	 * @return
	 */
	public Map<String, Object> get(Session session) {
//		String orgid = session.getCustom();
		String orgid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		return db().getMap("select b.dzzjlrq,a.hjdate,a.jmdate from t_dzz_hjxx_dzz a left join t_dzz_info_bc b on a.organid=b.organid where a.organid=? order by bzversion desc limit 1", new Object[] {orgid});
	}

	
	/**
	 * Description: 待办列表
	 * @author PT  
	 * @date 2019年9月11日 
	 * @param session
	 * @return
	 */
	public List<Map<String, Object>> list(Session session) {
		return db().listMap("select * from my_to_do_list where userid=? and ishandle=0 order by remind desc limit 0,4", new Object[] {session.getUserid()});
	}

	/**
	 * Description: 更新已读标识
	 * @author PT  
	 * @date 2019年9月11日 
	 * @param id
	 */
	public void modi(Long id) {
		db().update("update my_to_do_list set isread=1 where id=?", new Object[] {id});
	}

}
