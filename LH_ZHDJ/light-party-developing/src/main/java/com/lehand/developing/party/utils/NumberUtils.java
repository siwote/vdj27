package com.lehand.developing.party.utils;

/**
 * @program: huaikuang-dj
 * @description: NumberUtils
 * @author: zwd
 * @create: 2020-11-02 15:25
 */
public class NumberUtils {
    public static final int num0 = 0;
    public static final int num1 = 1;
    public static final int num2 = 2;
    public static final int num3 = 3;
    public static final int num100 = 100;
    public static final int num200 = 200;
    public static final int num300 = 300;
    public static final int num400 = 400;
    public static final int num500 = 500;
    public static final int num600 = 600;

    public static final int num1000000001 = 1000000001;

    public static final Long num5000000005 = 5000000005l;

    public static final String SAVE = "save";

    public static final String NEXT = "next";


}