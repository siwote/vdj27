package com.lehand.developing.party.dto;

import com.lehand.developing.party.pojo.FzdyPartyLink;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

/**
 * @program: huaikuang-dj
 * @description: FzdyPartyLinkDto2
 * @author: zwd
 * @create: 2020-11-02 14:36
 */
@ApiModel(value = "党员发展5环节表",description = "党员发展5环节表")
public class FzdyPartyLinkDto2  extends FzdyPartyLink {
    /**
     * 入党申请人：申请人附件
     */
    @ApiModelProperty(value="入党申请人：申请人附件",name="applyFiles")
    private String applyFiles;

    /**
     * 入党积极分子：积极分子附件
     */
    @ApiModelProperty(value="入党积极分子：积极分子附件",name="activistFiles")
    private String activistFiles;

    /**
     * 发展对象：发展对象附件
     */
    @ApiModelProperty(value="发展对象：发展对象附件",name="developFiles")
    private String developFiles;


    /**
     * 预备党员：政审材料
     */
    @ApiModelProperty(value="预备党员：政审材料",name="readyPoliticalReviewFiles")
    private String readyPoliticalReviewFiles;

    /**
     * 预备党员：预备党员附件
     */
    @ApiModelProperty(value="预备党员：预备党员附件",name="readyFiles")
    private String readyFiles;

    /**
     * 正式党员：转正附件
     */
    @ApiModelProperty(value="正式党员：转正附件",name="formalFiles")
    private String formalFiles;

    public String getApplyFiles() {
        return applyFiles;
    }

    public void setApplyFiles(String applyFiles) {
        this.applyFiles = applyFiles;
    }

    public String getActivistFiles() {
        return activistFiles;
    }

    public void setActivistFiles(String activistFiles) {
        this.activistFiles = activistFiles;
    }

    public String getDevelopFiles() {
        return developFiles;
    }

    public void setDevelopFiles(String developFiles) {
        this.developFiles = developFiles;
    }

    public String getReadyPoliticalReviewFiles() {
        return readyPoliticalReviewFiles;
    }

    public void setReadyPoliticalReviewFiles(String readyPoliticalReviewFiles) {
        this.readyPoliticalReviewFiles = readyPoliticalReviewFiles;
    }

    public String getReadyFiles() {
        return readyFiles;
    }

    public void setReadyFiles(String readyFiles) {
        this.readyFiles = readyFiles;
    }

    public String getFormalFiles() {
        return formalFiles;
    }

    public void setFormalFiles(String formalFiles) {
        this.formalFiles = formalFiles;
    }
}