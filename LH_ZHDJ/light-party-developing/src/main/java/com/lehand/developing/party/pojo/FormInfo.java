package com.lehand.developing.party.pojo;

public class FormInfo {
    private Long id;
    private Long bussid;
    private String formname;
    private Integer status;
    private String userid;
    private String username;
    private String createtime;
    private String updatetime;
    private Long compid;
    private Integer remarks1;
    private Integer remarks2;
    private String remarks3;
    private String remarks4;
    private String remarks5;
    private String remarks6;
    private String jsonstr;

    public FormInfo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBussid() {
        return this.bussid;
    }

    public void setBussid(Long bussid) {
        this.bussid = bussid;
    }

    public String getFormname() {
        return this.formname;
    }

    public void setFormname(String formname) {
        this.formname = formname == null ? null : formname.trim();
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getCreatetime() {
        return this.createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    public String getUpdatetime() {
        return this.updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime == null ? null : updatetime.trim();
    }

    public Long getCompid() {
        return this.compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Integer getRemarks1() {
        return this.remarks1;
    }

    public void setRemarks1(Integer remarks1) {
        this.remarks1 = remarks1;
    }

    public Integer getRemarks2() {
        return this.remarks2;
    }

    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return this.remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3 == null ? null : remarks3.trim();
    }

    public String getRemarks4() {
        return this.remarks4;
    }

    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4 == null ? null : remarks4.trim();
    }

    public String getRemarks5() {
        return this.remarks5;
    }

    public void setRemarks5(String remarks5) {
        this.remarks5 = remarks5 == null ? null : remarks5.trim();
    }

    public String getRemarks6() {
        return this.remarks6;
    }

    public void setRemarks6(String remarks6) {
        this.remarks6 = remarks6 == null ? null : remarks6.trim();
    }

    public String getJsonstr() {
        return this.jsonstr;
    }

    public void setJsonstr(String jsonstr) {
        this.jsonstr = jsonstr == null ? null : jsonstr.trim();
    }
}
