package com.lehand.developing.party.controller;

import com.lehand.base.common.Message;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.developing.party.dto.PushHumanResourceReq;
import com.lehand.developing.party.service.dfgl.dfsjgl.PushHumanResourceHandle;
import com.lehand.horn.partyorgan.dto.WXTransportTemplate;
import com.lehand.horn.partyorgan.util.WeChatUtils;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;



@Api(value = "人力资源推送管理", tags = { "人力资源推送管理" })
@RestController
@RequestMapping("/developing/basefee")
public class HumanResourceController extends BaseController{

	@Resource
	PushHumanResourceHandle pushHumanResourceHandle;


	/**
	 * Description: 人力推送党员党费缴纳基数
	 * @author lx
	 * @date 2021/3/10
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "人力推送党员党费缴纳基数", notes = "人力推送党员党费缴纳基数", httpMethod = "POST")
	@RequestMapping("/pushBasefee")
	public Message pushBasefee(@RequestBody PushHumanResourceReq req) {
		Message message = new Message();
		try {
			pushHumanResourceHandle.pushBasefee(req);
//			pushHumanResourceHandle.pushWechat();
			message.setMessage("推送党员党费缴纳基数成功");
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("推送党员党费缴纳基数出错");
		}
		return message;
	}
	/**
	 * Description: 测试人力推送党员党费缴纳基数
	 * @author lx
	 * @date 2021/3/10
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "测试人力推送党员党费缴纳基数", notes = "测试 人力推送党员党费缴纳基数", httpMethod = "POST")
	@RequestMapping("/testPushBasefee")
	public Message testPushBasefee(@RequestBody PushHumanResourceReq req) {
		Message message = new Message();
		try {
			pushHumanResourceHandle.pushBasefee(req);
			message.setMessage("测试推送党员党费缴纳基数成功");
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("测试推送党员党费缴纳基数出错");
		}
		return message;
	}
	/**
	 * Description: 测试发送微信党员党费消息
	 * @author lx
	 * @date 2021/3/22
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "测试发送微信党员党费消息", notes = "测试发送微信党员党费消息", httpMethod = "POST")
	@RequestMapping("/testPushWeChat")
	public Message pushWeChat() {
		Message message = new Message();
		try {
//			WXTransportTemplate bean = new WXTransportTemplate();
//			bean.setOpenid("opyz56vqfmL02hQowPbv58ecwhqo");
//			bean.setTemplateid(WeChatUtils.template_id);
//			bean.setUrl(WeChatUtils.confirm_dues_url);
//			bean.setType("每月党费确认");
//			bean.setDate("2021年3月党费");
//			bean.setUser("李响");
//			bean.setPayable("8888.88 元");
//			bean.setActual("88.88 元");
//			bean.setNowTime(DateEnum.YYYYMMDDHHMMDD.format());
//			bean.setRemark("请及时确认！！");
//			message.setData(pushHumanResourceHandle.pushWeChatTemplate(bean));
			pushHumanResourceHandle.pushWechat();
			message.setMessage("测试发送微信党员党费消息成功！！");
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("测试发送微信党员党费消息出错！！");
		}
		return message;
	}
	/**
	 * Description: 人力资源获取已确认党费信息
	 * @author lx
	 * @date 2021/3/24
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "人力资源获取已确认党费信息", notes = "人力资源获取已确认党费信息", httpMethod = "POST")
	@RequestMapping("/getConfirmDues")
	public Message getConfirmDues() {
		Message message = new Message();
		try {
			message.setData(pushHumanResourceHandle.getConfirmDues());
			message.setMessage("人力资源获取已确认党费信息成功！！");
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("人力资源获取已确认党费信息出错！！");
		}
		return message;
	}
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费信息是否确认", notes = "党费信息是否确认", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "month", value = "月份", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "idno", value = "身份证号码", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping(value = "/get", method = RequestMethod.POST)
	public Message get(String year,String month,String idno) {
		Message message = new Message();
		try {
			message.setData(pushHumanResourceHandle.get(year, month, idno));
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费信息确认失败");
		}
		return message;
	}
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费信息确认", notes = "党费信息确认", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "year", value = "年份", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "month", value = "月份", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "actual", value = "实交党费", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "idno", value = "身份证号码", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "orgid", value = "组织id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "name", value = "姓名", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "basefee", value = "缴费基数", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "rate", value = "缴费比率", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "payable", value = "应交党费", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	public Message confirm(String year,String month,String actual,String idno,String orgid,String name,String basefee,String rate,String payable) {
		Message message = new Message();
		try {
			pushHumanResourceHandle.confirm(year, month, actual, idno,orgid,name,basefee,rate,payable);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费信息确认失败");
		}
		return message;
	}
}
