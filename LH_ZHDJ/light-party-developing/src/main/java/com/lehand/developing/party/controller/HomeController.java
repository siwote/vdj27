package com.lehand.developing.party.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.components.web.controller.BaseController;
import com.lehand.developing.party.service.home.HomeService;


@RestController
@Api(value = "发展党员模块首页信息", tags = { "发展党员首页信息" })
@RequestMapping("/developing/home")
public class HomeController extends BaseController{
	
	@Resource HomeService homeSerivce;

	@OpenApi(optflag=0)
	@ApiOperation(value = "首页党组织各时间查询", notes = "首页党组织各时间查询", httpMethod = "POST")
	@RequestMapping("/get")
	public Message get() {
		Message message = new Message();
		try {
			Map<String,Object> data = homeSerivce.get(getSession());
		    message.setData(data);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费设置查询出错");
		}
		return message;
	}
	

	@OpenApi(optflag=0)
	@ApiOperation(value = "首页待办列表", notes = "首页待办列表", httpMethod = "POST")
	@RequestMapping("/list")
	public Message list() {
		Message message = new Message();
		try {
			List<Map<String,Object>> data = homeSerivce.list(getSession());
		    message.setData(data);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费设置查询出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 更新已读标识
	 * @author PT  
	 * @date 2019年9月11日 
	 * @param id
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "更新已读标识", notes = "更新已读标识", httpMethod = "POST")
	@RequestMapping("/modi")
	public Message modi(Long id) {
		Message message = new Message();
		try {
			homeSerivce.modi(id);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费设置查询出错");
		}
		return message;
	}
	
}
