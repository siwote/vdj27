package com.lehand.developing.party.dto;

import com.lehand.developing.party.pojo.FzdyPartyLink;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @program: huaikuang-dj
 * @description: FzdyPartyLinkDto3
 * @author: zwd
 * @create: 2020-11-17 14:06
 */
@ApiModel(value = "党员发展5环节表",description = "党员发展5环节表")
public class FzdyPartyLinkDto3 extends FzdyPartyLink {


    /**
     * 入党申请人：谈话接收人
     */
    @ApiModelProperty(value="入党申请人：谈话接收人",name="applyTalkUserId")
    private List<Map<String,Object>> applyTalkUser;



    /**
     * 入党积极分子：指定培养人
     */
    @ApiModelProperty(value="入党积极分子：指定培养人",name="activistFosterUserId")
    private List<Map<String,Object>> activistFosterUser;



    /**
     * 发展对象：入党介绍人
     */
    @ApiModelProperty(value="发展对象：入党介绍人",name="developUserId")
    private List<Map<String,Object>> developUser;



    /**
     * 入党申请人：申请人附件
     */
    @ApiModelProperty(value="入党申请人：申请人附件",name="applyFiles")
    private String applyFiles;

    /**
     * 入党积极分子：积极分子附件
     */
    @ApiModelProperty(value="入党积极分子：积极分子附件",name="activistFiles")
    private String activistFiles;

    /**
     * 发展对象：发展对象附件
     */
    @ApiModelProperty(value="发展对象：发展对象附件",name="developFiles")
    private String developFiles;


    /**
     * 预备党员：政审材料
     */
    @ApiModelProperty(value="预备党员：政审材料",name="readyPoliticalReviewFiles")
    private String readyPoliticalReviewFiles;

    /**
     * 预备党员：预备党员附件
     */
    @ApiModelProperty(value="预备党员：预备党员附件",name="readyFiles")
    private String readyFiles;

    /**
     * 正式党员：转正附件
     */
    @ApiModelProperty(value="正式党员：转正附件",name="formalFiles")
    private String formalFiles;


    @ApiModelProperty(value="保存方式 sava 保存 next 保存并下一步",name="formalFiles")
    private String savetype;


    public List<Map<String, Object>> getApplyTalkUser() {
        return applyTalkUser;
    }

    public void setApplyTalkUser(List<Map<String, Object>> applyTalkUser) {
        this.applyTalkUser = applyTalkUser;
    }



    public List<Map<String, Object>> getActivistFosterUser() {
        return activistFosterUser;
    }

    public void setActivistFosterUser(List<Map<String, Object>> activistFosterUser) {
        this.activistFosterUser = activistFosterUser;
    }

    public List<Map<String, Object>> getDevelopUser() {
        return developUser;
    }

    public void setDevelopUser(List<Map<String, Object>> developUser) {
        this.developUser = developUser;
    }



    public String getApplyFiles() {
        return applyFiles;
    }

    public void setApplyFiles(String applyFiles) {
        this.applyFiles = applyFiles;
    }

    public String getActivistFiles() {
        return activistFiles;
    }

    public void setActivistFiles(String activistFiles) {
        this.activistFiles = activistFiles;
    }

    public String getDevelopFiles() {
        return developFiles;
    }

    public void setDevelopFiles(String developFiles) {
        this.developFiles = developFiles;
    }

    public String getReadyPoliticalReviewFiles() {
        return readyPoliticalReviewFiles;
    }

    public void setReadyPoliticalReviewFiles(String readyPoliticalReviewFiles) {
        this.readyPoliticalReviewFiles = readyPoliticalReviewFiles;
    }

    public String getReadyFiles() {
        return readyFiles;
    }

    public void setReadyFiles(String readyFiles) {
        this.readyFiles = readyFiles;
    }

    public String getFormalFiles() {
        return formalFiles;
    }

    public void setFormalFiles(String formalFiles) {
        this.formalFiles = formalFiles;
    }

    public String getSavetype() {
        return savetype;
    }

    public void setSavetype(String savetype) {
        this.savetype = savetype;
    }
}