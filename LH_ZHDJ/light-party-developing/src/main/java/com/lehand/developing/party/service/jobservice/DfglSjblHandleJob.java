package com.lehand.developing.party.service.jobservice;

import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.service.dfgl.dfsjgl.DfglSjblHandle;
import com.lehand.developing.party.utils.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @program: huaikuang-dj
 * @description: DfglSjblHandleJob
 * @author: zwd
 * @create: 2020-12-28 15:29
 */
public class DfglSjblHandleJob {
    private static final Logger logger = LogManager.getLogger(DfsjInitializationDataService.class);

    private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    DfglSjblHandle dfglSjblHandle;

    @Resource
    protected GeneralSqlComponent generalSqlComponent;

    public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
        return threadPoolTaskExecutor;
    }

    public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.threadPoolTaskExecutor = threadPoolTaskExecutor;
    }

    public void dfglSjblHandData() {
        try {
            synchronized (CREATE) {
                if (CREATE.get()) {
                    logger.info("之前任务正在执行,本次不处理");
                    return;
                }
                CREATE.set(true);
            }
            //查询所有未删除的党组织
            List<Map<String,Object>> list = generalSqlComponent.query(SqlCode.getAllTDzzInfo,new Object[]{});
            List<String> data = null;
            //年份
            String years = DateEnum.YYYY.format();
            //月份
            int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
            for(Map<String,Object> li:list){
                dfglSjblHandle.initialization(li.get("organid").toString(),years,String.valueOf(months));
            }

            logger.info("定时初始化党费审核与上报数据结束");
        } catch (Exception e) {
            logger.error("定时初始化党费审核与上报数据异常",e);
        } finally {
            CREATE.set(false);
        }
    }

}