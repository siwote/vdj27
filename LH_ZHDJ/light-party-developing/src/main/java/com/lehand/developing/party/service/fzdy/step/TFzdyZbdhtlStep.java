package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyZbdhtlStep extends BaseStep {

	//正式党员code
		@Value("${jk.organ.zhengshidangyuan}")
		private String zhengshidangyuan;
		//预备党员code
		@Value("${jk.organ.yubeidangyuan}")
		private String yubeidangyuan;
	
	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {

		String organid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		Map<String, Object> map = db().getMap("select * from t_fzdy_zbdhtl where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		makeFiles(map,session);
		if (map != null) {
			String fileids = String.valueOf(map.get("file"));
			List<Map<String, Object>> files = new ArrayList<Map<String, Object>>(0);
			if (!StringUtils.isEmpty(fileids)) {
				files = db().listMap("select * from dl_file where compid=? and id in (" + fileids + ")",
						new Object[] {session.getCompid()});
			}
			map.put("files", files);
		}else {
			map = new HashMap<String, Object>();
		}
		//机构下正式党员加预备党员的数量
		Map<String,Object> map1 = db().getMap("select count(*) num from t_dy_info where organid = ? and operatetype != 3 and (dylb=? or dylb=?)", new Object[] {organid,zhengshidangyuan,yubeidangyuan});
	    int num1 = Integer.valueOf(map1.get("num").toString());
	    //机构下正式党员的数量
	    Map<String,Object> map2 = db().getMap("select count(*) num from t_dy_info where organid = ? and operatetype != 3 and dylb=? ", new Object[] {organid,zhengshidangyuan});
	    int num2 = Integer.valueOf(map2.get("num").toString());
	    map.put("zbdhydrs", num1);
	    map.put("zbdhybjqdhrs", num2);
		return map;
	}
	


	
	//zbdhtlrq
	//zbdhydhrs
	//zbdhybjqdhrs
	//zbdhsdhrs
	//zbdhzcps
	//zbdhhqzcps
	//zbdhfdps
	//zbdhhqfdps
	//zbdhqqps
	//zbdhhqqqps
	//zbdhtljg
	//person_id
	//file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("generaldiscuss_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("zbdhdpjg")==null) {
			params.put("zbdhdpjg", Constant.ENPTY);
		}
		if(params.get("zbdhyqjzrq")==null) {
			params.put("zbdhyqjzrq", Constant.ENPTY);
		}
		
		int num = db().delete("UPDATE t_fzdy_zbdhtl SET syncstatus='D' WHERE person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_zbdhtl (compid, generaldiscuss_id, zbdhtlrq, zbdhydhrs, zbdhybjqdhrs, zbdhsdhrs, zbdhzcps, zbdhhqzcps, zbdhfdps, zbdhhqfdps, zbdhqqps, zbdhhqqqps, zbdhdpjg, zbdhtljg, zbdhyqjzrq, created_by,create_date, delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:generaldiscuss_id,:zbdhtlrq,:zbdhydhrs,:zbdhybjqdhrs,:zbdhsdhrs,:zbdhzcps,:zbdhhqzcps,:zbdhfdps,:zbdhhqfdps,:zbdhqqps,:zbdhhqqqps,:zbdhdpjg,:zbdhtljg,:zbdhyqjzrq,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
	
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		
		saveFile(params);
		
		return num;
	}
}
