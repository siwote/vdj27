package com.lehand.developing.party.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.pojo.FzdyRemindrule;
import com.lehand.developing.party.pojo.FzdyRemindruleDate;
import com.lehand.developing.party.pojo.MsgTemplate;

/**
 * @ClassName:：FzdyRemindRuleBusinessImpl 
 * @Description： 发展党员提醒规则实现类
 * @author ：taogang  
 * @date ：2019年7月10日 下午3:30:41 
 *
 */
@Service
public class FzdyRemindRuleService {
	
	@Resource private GeneralSqlComponent generalSqlComponent;
	
	/**
	 * 保存消息模板
	 */  
	
	@Transactional(rollbackFor=Exception.class)
	public boolean addMsgTemp(MsgTemplate info, Session session) {
		info.setTlcode(String.valueOf(System.currentTimeMillis()));
		info.setCompid(Integer.valueOf(session.getCompid().toString()));
		//发展党员使用默认为1
		info.setRemarks1(1);
		Integer seqNo = maxSeqNo(session.getCompid(), 1);
		if(null == seqNo) {
			info.setSeqno(1);
		}else {
			info.setSeqno(seqNo + 1);
		}
		info.setMsgurltpl(info.getMsgurltpl() == null ? "" : info.getMsgurltpl());
		info.setRemarks2(info.getRemarks2() == null ? 0 :info.getRemarks2());
		info.setRemarks3(info.getRemarks3() == null ? "" :info.getRemarks3());
		info.setRemarks4(info.getRemarks4() == null ? "" :info.getRemarks4());
		Integer result = generalSqlComponent.insert(SqlCode.addMsgTemp, info);
		return result > 0 ? true : false;
	}
	
	/**
	 * @Title：maxSeqNo 
	 * @Description：查询最大排序号
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：Integer 
	 * @throws
	 */
	public Integer maxSeqNo(Long compid,Integer fzdy) {
		return generalSqlComponent.query(SqlCode.queryMsgTempSeqNo, new Object[] {compid,fzdy});
	}
	
	/**
	 * 修改消息模板
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean updateMsgTemp(MsgTemplate info, Session session) {
		//查询历史记录
		MsgTemplate oldInfo = queryMsgTempByCode(info.getTlcode(), 1, session);
		oldInfo.setTlname(info.getTlname());
		oldInfo.setCond(info.getCond());
		oldInfo.setChannel(info.getChannel());
		oldInfo.setMsgtpl(info.getMsgtpl());
		oldInfo.setMsgurltpl(info.getMsgurltpl());
		oldInfo.setStatus(info.getStatus());
		Integer result = generalSqlComponent.update(SqlCode.updateMsgTemp, oldInfo);
		return result > 0 ? true : false;
	}
	
	/**
	 * 删除消息模板
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean delMsgTemp(String tlcode,Integer fzdy,Session session) {
		Integer result = generalSqlComponent.delete(SqlCode.delMsgTemp, new Object[]{tlcode,session.getCompid(),fzdy});
		return result > 0 ? true : false;
	}
	
	/**
	  * 分页查询消息模板
	 */
	
	public Pager queryPageMsgTemp(Pager pager, Session session) {
		return generalSqlComponent.pageQuery(SqlCode.queryPageMsgTemp, new Object[] {session.getCompid(),1}, pager);
	}
	
	/**
	 * 根据消息模板编码查询消息模板
	 */
	
	public MsgTemplate queryMsgTempByCode(String tlcode,Integer fzdy, Session session) {
		return generalSqlComponent.query(SqlCode.queryMsgTempByCode, new Object[] {tlcode,session.getCompid(),fzdy});
	}
	
	/**
	 * 根据规则id删除全部提醒点信息
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean delRuleDateById(Long ruleid, Session session) {
		Integer result = generalSqlComponent.delete(SqlCode.delRemindRuleDate, new Object[] {ruleid,session.getCompid()});
		return result > 0 ? true : false;
	}
	
	/**
	 * 删除提醒规则
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean delRemindRule(Long id, Session session) {
		//删除提醒点信息
		delRuleDateById(id, session);
		Integer result = generalSqlComponent.delete(SqlCode.delRemindRuleById, new Object[] {id,session.getCompid()});
		if(result > 0) {
			return true;
		}
		return false;
	}
	
	/**
	  * 查询提醒规则分页列表
	 */
	@SuppressWarnings("unchecked")
	
	public Pager queryRemind(Pager pager, Long typeid,Integer status,Session session) {
		Pager result = generalSqlComponent.pageQuery(SqlCode.queryPageRuleByTypeId, new Object[] {typeid,status,session.getCompid()}, pager);
		if(status == 1) {
			List<Map<String, Object>> rows = (List<Map<String, Object>>) result.getRows();
			for (Map<String, Object> map : rows) {
				map.put("txnum", queryDateCount((long)map.get("id"), session.getCompid()));
			}
		}
		return result;
	}
	
	/**
	 * @Title：queryDateCount 
	 * @Description：根据规则id查询提醒点次数
	 * @param ：@param ruleid
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：Long 
	 * @throws
	 */
	public Long queryDateCount(Long ruleid,Long compid) {
		return generalSqlComponent.query(SqlCode.queryDateCountByRuleId, new Object[] {ruleid,compid});
	}
	
	/**
	 * 查询提醒规则详情
	 */
	
	public Map<String, Object> queryRuleById(Long id, Session session) {
		Map<String, Object> bean = new HashMap<String, Object>(3);
		//查询自身提醒规则信息
		FzdyRemindrule ruleInfo = queryFzdyRemindRuleById(id, session.getCompid());
		if(null != ruleInfo) {
			//查询提醒节点信息
			List<FzdyRemindruleDate> dateList = generalSqlComponent.query(SqlCode.queryRemindRuleDateByRuleId, new Object[] {ruleInfo.getId(),session.getCompid()});
			//查询消息模板信息
			MsgTemplate msgTemp = queryMsgTempByCode(ruleInfo.getTempid(), 1, session);
			bean.put("ruleInfo", ruleInfo);
			bean.put("dates", dateList);
			bean.put("msgTemp", msgTemp);
		}
		return bean;
	}
	
	/**
	 * @Title：queryFzdyRemindRuleById 
	 * @Description：根据规则id查询提醒规则信息
	 * @param ：@param id
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：FzdyRemindrule 
	 * @throws
	 */
	public FzdyRemindrule queryFzdyRemindRuleById(Long id,Long compid) {
		return generalSqlComponent.query(SqlCode.queryRemindRuleById, new Object[] {id,compid});
	}
	
	/**
	 * 新增提醒规则
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean addRemindRule(FzdyRemindrule ruleInfo, String dates, Session session) {
		// 新增提醒规则
		ruleInfo.setCompid(session.getCompid());
		ruleInfo.setCreator(session.getUserid());
		ruleInfo.setStatus(1);
		Long ruleId = generalSqlComponent.insert(SqlCode.addRemindRule, ruleInfo);
		if(null != ruleId) {
			saveFzdyRuleDate(ruleId, dates, session);
			return true;
		}
		return false;
	}
	
	/**
	 * 修改提醒规则
	 */
	
	@Transactional(rollbackFor=Exception.class)
	public boolean updateRemindRule(FzdyRemindrule ruleInfo, String dates, Session session) {
		//根据id查询提醒规则
		FzdyRemindrule oldInfo = queryFzdyRemindRuleById(ruleInfo.getId(), session.getCompid());
		oldInfo.setTypeid(ruleInfo.getTypeid());
		oldInfo.setRemindtype(ruleInfo.getRemindtype());
		oldInfo.setStarttstage(ruleInfo.getStarttstage());
		oldInfo.setEnddate(ruleInfo.getEnddate());
		oldInfo.setUnit(ruleInfo.getUnit());
		oldInfo.setTempid(ruleInfo.getTempid());
		oldInfo.setModitor(session.getUserid());
		oldInfo.setRemark3(ruleInfo.getRemark3());
		Integer result = generalSqlComponent.update(SqlCode.updateRemindRule, oldInfo);
		if(result > 0 ) {
			saveFzdyRuleDate(ruleInfo.getId(), dates, session);
			return true;
		}
		return false;
	}
	
	/**
	 * @Title：saveFzdyRuleDate 
	 * @Description：保存提醒时间节点
	 * @param ：@param ruleid
	 * @param ：@param dates
	 * @param ：@param session
	 * @param ：@return 
	 * @return ：boolean 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor=Exception.class)
	public boolean saveFzdyRuleDate(Long ruleid,String dates,Session session) {
		//删除时间节点
		delRuleDateById(ruleid, session);
		if(!StringUtils.isEmpty(dates)) {
			List<Map<String, Object>> dateList = JSON.parseObject(dates, ArrayList.class);
			if(dateList.size() > 0 ) {
				FzdyRemindruleDate fzdyRemindruleDate = null;
				for (Map<String, Object> map : dateList) {
					fzdyRemindruleDate = new FzdyRemindruleDate();
					fzdyRemindruleDate.setTxdate(map.get("txdate").toString());
					fzdyRemindruleDate.setTxunit(map.get("txunit").toString());
					fzdyRemindruleDate.setTime(map.get("time").toString());
					fzdyRemindruleDate.setCompid(session.getCompid());
					fzdyRemindruleDate.setRuleid(ruleid);
					generalSqlComponent.insert(SqlCode.addRemindRuleDate, fzdyRemindruleDate);
				}
			}
		}
		return true;
	}
	
	
	/**
	 * 查询下拉框数据
	 */
	public List<Map<String, Object>> getDictList(Long typeid,Session session){
		return generalSqlComponent.query(SqlCode.getFzdyDictByTypeid, new Object[] {session.getCompid(),typeid});
	}

}
