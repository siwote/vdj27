package com.lehand.developing.party.service.fzdy.step;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.developing.party.service.fzdy.BaseStep;


@Service
public class TFzdyClgdStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_clgd  where person_id=:person_id and compid=:compid limit 1", params);
		if(map==null) {
			map = new HashMap<String,Object>();
		}
		Map<String, Object> map1 = db().getMap("select * from t_fzdy_clsq where person_id=:person_id and compid=:compid order by create_date desc limit 1", params);
		map.put("lqclrq", map1.get("lqclrq"));
		map.put("lqr", map1.get("lqr"));
		Map<String, Object> map2 = db().getMap("select * from t_fzdy_sbdwsp where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		map.put("zbdhxbrq", map2.get("zbdhxbrq"));
		map.put("tzbrrq", map2.get("tzbrrq"));
		return map;
	}
	
	
	//clgdlx
	//dabgd
	//person_id
	//file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("clgd_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		
		int num = db().insert("INSERT INTO t_fzdy_clgd (compid, clgd_id, clgdlx, dabgd, created_by, create_date, delete_flag, person_id, file)" + 
				" VALUES (:compid,:clgd_id,:clgdlx,:dabgd,:created_by,:create_date,:delete_flag,:person_id,:file)", params);
		
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		
		saveFile(params);
		
		return num;
	}
}
