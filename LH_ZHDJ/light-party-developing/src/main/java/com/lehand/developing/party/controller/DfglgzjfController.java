package com.lehand.developing.party.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.lehand.base.common.Session;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.developing.party.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.awt.Color;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.service.dfgl.gzjfgl.DfglGzjfHandle;
import org.springframework.web.multipart.MultipartFile;


@Api(value = "工作经费", tags = { "工作经费" })
@RestController
@RequestMapping("/developing/gzjf")
public class DfglgzjfController extends BaseController {
	
	@Resource DfglGzjfHandle dfglGzjfHandle;
	
	//党支部code
	@Value("${jk.organ.dangzhibu}")
	private String dangzhibu;

	@Value("${jk.organ.dangwei}")
	private String dangwei;

	@Value("${jk.organ.dangzongzhi}")
	private String dangzongzhi;
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	@Resource
	protected SessionUtils sessionUtils;


	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
/**======================================淮矿党费收交开始============================================*/
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费信息导入", notes = "党费信息导入", httpMethod = "POST")
	@RequestMapping(value = "/partyInfoImport", method = RequestMethod.POST)
	public Message partyInfoImport(MultipartFile file) {
		Message message = new Message();
		try {
			message = dfglGzjfHandle.partyInfoImport(file, getSession(),message);
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("导入党费信息失败");
		}
		return message;
	}


	@OpenApi(optflag=0)
	@ApiOperation(value = "党费信息确认", notes = "党费信息确认", httpMethod = "POST")
	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	public Message confirm(String infos) {
		Message message = new Message();
		try {
			dfglGzjfHandle.confirm(infos, getSession());
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费信息确认失败");
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "党费信息编辑", notes = "党费信息编辑", httpMethod = "POST")
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public Message modify(String usercard,Double actual) {
		Message message = new Message();
		try {
			dfglGzjfHandle.modify(usercard, actual, getSession());
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费信息编辑失败");
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "党费信息去除", notes = "党费信息去除", httpMethod = "POST")
	@RequestMapping(value = "/removeInfo", method = RequestMethod.POST)
	public Message removeInfo(String usercard) {
		Message message = new Message();
		try {
			dfglGzjfHandle.removeInfo(usercard, getSession());
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费信息编辑失败");
		}
		return message;
	}


	@OpenApi(optflag=0)
	@ApiOperation(value = "党费收交分页查询", notes = "党费收交分页查询", httpMethod = "POST")
	@RequestMapping("/infoPage")
	public Message infoPage(Pager pager,Integer months,String years,String username) {
		Message message = new Message();
		try {
			dfglGzjfHandle.infoPage(getSession(),pager,months,years,username,getSession().getCurrentOrg().getOrgcode());
			message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费收交分页查询出错");
		}
		return message;
	}


	@OpenApi(optflag=0)
	@ApiOperation(value = "党费巡查分页查询", notes = "党费巡查分页查询", httpMethod = "POST")
	@RequestMapping("/patrolPage")
	public Message patrolPage(Pager pager,Integer months,String years,String organcode,String username) {
		Message message = new Message();
		try {
			dfglGzjfHandle.patrolPage(getSession(),pager,months,years,organcode,username);
			message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费巡查分页查询出错");
		}
		return message;
	}


	@OpenApi(optflag=0)
	@ApiOperation(value = "党费汇总", notes = "党费汇总", httpMethod = "POST")
	@RequestMapping("/summary")
	public Message summary(Integer months,String years,String organcode) {
		Message message = new Message();
		try {
			message.success().setData(dfglGzjfHandle.summary(getSession(),months,years,organcode));;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费收交分页查询出错");
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "党费汇总人数详情分页查询", notes = "党费汇总人数详情分页查询", httpMethod = "POST")
	@RequestMapping("/patrolDetailsPage")
	public Message patrolDetailsPage(Pager pager,Integer months,String years,String organcode,Integer type) {
		Message message = new Message();
		try {
			dfglGzjfHandle.patrolDetailsPage(getSession(),pager,months,years,organcode,type);
			message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费巡查分页查询出错");
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "党费导出", notes = "党费导出", httpMethod = "POST")
	@RequestMapping("/excel")
	public void excel(HttpServletResponse response, Integer months, String years, String organcode) {
		Map map = db().getMap("select * from t_dzz_info where organcode=?",new Object[]{organcode});
		if(map!=null){
			String type = map.get("zzlb").toString();
			if(dangwei.equals(type) || dangzongzhi.equals(type)){
				exportExcel(response,months,years,organcode,map.get("dzzmc").toString());
			}else{
				exportExcel2(response,months,years,organcode,map.get("dzzmc").toString());
			}
		}
	}

	/**
	 * 上级党组织党费导出
	 * @param response
	 * @param months
	 * @param years
	 * @param organcode
	 * @param organname
	 */
	public void exportExcel(HttpServletResponse response, Integer months, String years, String organcode,String organname) {

		/** 第一步，创建一个Workbook，对应一个Excel文件  */
		XSSFWorkbook wb = new XSSFWorkbook();

		/** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
		XSSFSheet sheet = wb.createSheet(organname + "党费收交情况");

		/** 第三步，设置样式以及字体样式*/
		XSSFCellStyle titleStyle = createTitleCellStyle(wb);
		XSSFCellStyle headerStyle = createHeadCellStyle(wb);
		XSSFCellStyle headerStyle2 = createHeadCellStyle2(wb);
		XSSFCellStyle contentStyle = createContentCellStyle(wb);

		/** 第四步，创建标题 ,合并标题单元格 */
		// 行号
		int rowNum = 0;
		// 创建第一页的第一行，索引从0开始
		XSSFRow row0 = sheet.createRow(rowNum++);
		// 设置行高
		row0.setHeight((short) 800);

		String title = organname + "党费收交情况";
		XSSFCell c00 = row0.createCell(0);
		c00.setCellValue(title);
		c00.setCellStyle(titleStyle);
		// 合并单元格，参数依次为起始行，结束行，起始列，结束列 （索引0开始）
		//标题合并单元格操作，9为总列数
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));
		//汇总数据源
		Map<String,Object> summary = dfglGzjfHandle.summary(getSession(), months, years, organcode);
		// 第二行
		XSSFRow row1 = sheet.createRow(rowNum++);
		row1.setHeight((short) 700);
		String[] row_first = {"应收党费：", "", "实收党费：", "", "应交人数：", "", "实交人数：","",""};
		for (int i = 0; i < row_first.length; i++) {
			XSSFCell tempCell = row1.createCell(i);
			tempCell.setCellStyle(headerStyle);
			if (i == 0) {
				tempCell.setCellValue(row_first[i] + summary.get("receivable")+"元");
			} else if (i == 2) {
				tempCell.setCellValue(row_first[i] + summary.get("actualin")+"元");
			} else if(i==4) {
				tempCell.setCellValue(row_first[i] + summary.get("duenum")+"人");
			}else if(i==6) {
				tempCell.setCellValue(row_first[i] + summary.get("realnum")+"人");
			}else{
				tempCell.setCellValue(row_first[i]);
			}
		}

		// 合并
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 1));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 4, 5));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 6, 8));

		//第三行
		XSSFRow row2 = sheet.createRow(rowNum++);
		row2.setHeight((short) 700);
		String[] row_second = {"多交人数：", "", "少交人数：", "", "未交人数：", "", "免交人数：","",""};
		for (int i = 0; i < row_second.length; i++) {
			XSSFCell tempCell = row2.createCell(i);
			tempCell.setCellStyle(headerStyle);
			if (i == 0) {
				tempCell.setCellValue(row_second[i] + summary.get("morenum")+"人");
			} else if (i == 2) {
				tempCell.setCellValue(row_second[i] + summary.get("lessnum")+"人");
			} else if(i==4) {
				tempCell.setCellValue(row_second[i] + summary.get("unpaidnum")+"人");
			}else if(i==6) {
				tempCell.setCellValue(row_second[i] + summary.get("freenum")+"人");
			}else{
				tempCell.setCellValue(row_second[i]);
			}
		}

		// 合并
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 1));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 4, 5));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 6, 8));

		//第四行
		XSSFRow row3 = sheet.createRow(rowNum++);
		row3.setHeight((short) 700);
		String[] row_third = {"党组织名称", "应收党费", "实收党费", "应交人数", "实交人数", "多交人数", "少交人数","未交人数","免交人数"};
		for (int i = 0; i < row_third.length; i++) {
			XSSFCell tempCell = row3.createCell(i);
			tempCell.setCellValue(row_third[i]);
			tempCell.setCellStyle(headerStyle2);
		}

		//第五行开始往后
		Pager pager = new Pager();
		pager.setPageSize(10000);
		dfglGzjfHandle.patrolPage(getSession(), pager, months, years,organcode,"");
		List<Map<String, Object>> dataList = (List<Map<String, Object>>) pager.getRows(); //查询出来的数据

		//循环每一行数据
		for (Map<String, Object> excelData : dataList) {
			XSSFRow tempRow = sheet.createRow(rowNum++);
			tempRow.setHeight((short) 700);
			// 循环单元格填入数据
			for (int j = 0; j < 9; j++) {
				XSSFCell tempCell = tempRow.createCell(j);
				tempCell.setCellStyle(contentStyle);
				String tempValue;
				if (j == 0) {
					tempValue = excelData.get("orgname").toString();
				} else if (j == 1) {
					tempValue = excelData.get("receivable").toString();
				} else if (j == 2) {
					tempValue = excelData.get("actualin").toString();
				} else if (j == 3) {
					tempValue = excelData.get("duenum").toString();
				} else if (j == 4) {
					tempValue = excelData.get("realnum").toString();
				} else if (j == 5) {
					tempValue = excelData.get("morenum").toString();
				} else if(j == 6){
					tempValue = excelData.get("lessnum").toString();
				}else if(j == 7){
					tempValue = excelData.get("unpaidnum").toString();
				}else if(j == 8){
					tempValue = excelData.get("freenum").toString();
				}else{
					tempValue = "";
				}
				tempCell.setCellValue(tempValue);
			}
		}
		sheet.autoSizeColumn((short)0); //调整宽度
		sheet.autoSizeColumn((short)1);
		sheet.autoSizeColumn((short)2);
		sheet.autoSizeColumn((short)3);
		sheet.autoSizeColumn((short)4);
		sheet.autoSizeColumn((short)5);
		sheet.autoSizeColumn((short)6);
		sheet.autoSizeColumn((short)7);
		sheet.autoSizeColumn((short)8);
		String fileName = organname + "党费收交情况汇总表.xls";
		OutputStream stream = null;
		try {
			response.setHeader("Content-disposition", "attachment;filename="+"党费收交情况汇总表.xlsx");
			response.setHeader("FileName", "党费收交情况汇总表.xlsx");
			stream = response.getOutputStream();
			wb.write(stream);// 将数据写出去
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(stream != null){
					stream.flush();
					stream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 党支部党费导出
	 * @param response
	 * @param months
	 * @param years
	 * @param organcode
	 * @param organname
	 */
	public void exportExcel2(HttpServletResponse response,Integer months, String years, String organcode, String organname) {

		/** 第一步，创建一个Workbook，对应一个Excel文件  */
		XSSFWorkbook wb = new XSSFWorkbook();

		/** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
		XSSFSheet sheet = wb.createSheet(organname + "党费收交情况");

		/** 第三步，设置样式以及字体样式*/
		XSSFCellStyle titleStyle = createTitleCellStyle(wb);
		XSSFCellStyle headerStyle = createHeadCellStyle(wb);
		XSSFCellStyle headerStyle2 = createHeadCellStyle2(wb);
		XSSFCellStyle contentStyle = createContentCellStyle(wb);

		/** 第四步，创建标题 ,合并标题单元格 */
		// 行号
		int rowNum = 0;
		// 创建第一页的第一行，索引从0开始
		XSSFRow row0 = sheet.createRow(rowNum++);
		// 设置行高
		row0.setHeight((short) 800);

		String title = organname + "党费收交情况";
		XSSFCell c00 = row0.createCell(0);
		c00.setCellValue(title);
		c00.setCellStyle(titleStyle);
		// 合并单元格，参数依次为起始行，结束行，起始列，结束列 （索引0开始）
		//标题合并单元格操作，9为总列数
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 15));

		//汇总数据源
		Map<String,Object> summary = dfglGzjfHandle.summary(getSession(), months, years, organcode);
		// 第二行
		XSSFRow row1 = sheet.createRow(rowNum++);
		row1.setHeight((short) 700);
		String[] row_first = {"应收党费：", "", "","实收党费：", "", "", "应交人数：", "", "", "实交人数：", "", "", "大额党费人数：","","",""};
		for (int i = 0; i < row_first.length; i++) {
			XSSFCell tempCell = row1.createCell(i);
			tempCell.setCellStyle(headerStyle);
			if (i == 0) {
				tempCell.setCellValue(row_first[i] + summary.get("receivable")+"元");
			} else if (i == 3) {
				tempCell.setCellValue(row_first[i] + summary.get("actualin")+"元");
			} else if(i==6) {
				tempCell.setCellValue(row_first[i] + summary.get("duenum")+"人");
			}else if(i==9) {
				tempCell.setCellValue(row_first[i] + summary.get("realnum")+"人");
			}else if(i==12) {
				tempCell.setCellValue(row_first[i] + summary.get("bignum")+"人");
			} else{
				tempCell.setCellValue(row_first[i]);
			}
		}

		// 合并
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 2));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 5));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 6, 8));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 9, 11));
		sheet.addMergedRegion(new CellRangeAddress(1, 1, 12, 15));

		//第三行
		XSSFRow row2 = sheet.createRow(rowNum++);
		row2.setHeight((short) 500);
		String[] row_second = {"多交人数：", "","","少交人数：", "","", "未交人数：","", "","免交人数：", "","", "","","",""};
		for (int i = 0; i < row_second.length; i++) {
			XSSFCell tempCell = row2.createCell(i);
			tempCell.setCellStyle(headerStyle);
			if (i == 0) {
				tempCell.setCellValue(row_second[i] + summary.get("morenum")+"人");
			} else if (i == 3) {
				tempCell.setCellValue(row_second[i] + summary.get("lessnum")+"人");
			} else if(i==6) {
				tempCell.setCellValue(row_second[i] + summary.get("unpaidnum")+"人");
			}else if(i==9) {
				tempCell.setCellValue(row_second[i] + summary.get("freenum")+"人");
			}else{
				tempCell.setCellValue(row_second[i]);
			}
		}

		// 合并
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 2));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 3, 5));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 6, 8));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 9, 11));
		sheet.addMergedRegion(new CellRangeAddress(2, 2, 12, 15));

		//第四行
		XSSFRow row3 = sheet.createRow(rowNum++);
		row3.setHeight((short) 500);
		String[] row_third = {"姓名", "身份证号", "应得工资", "扣减项目", "", "", "","","","","计费基数","应交比例","应交金额","实交金额","备注","状态"};
		for (int i = 0; i < row_third.length; i++) {
			XSSFCell tempCell = row3.createCell(i);
			tempCell.setCellValue(row_third[i]);
			tempCell.setCellStyle(headerStyle2);
		}

		XSSFRow row4 = sheet.createRow(rowNum++);
		row3.setHeight((short) 700);
		String[] row_four = {"", "", "", "税", "险", "金", "津补贴","费","安全工资","其他","","","","","",""};
		for (int i = 0; i < row_four.length; i++) {
			XSSFCell tempCell = row4.createCell(i);
			tempCell.setCellValue(row_four[i]);
			tempCell.setCellStyle(headerStyle2);
		}

		//合并
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 0, 0));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 1, 1));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 2, 2));
		sheet.addMergedRegion(new CellRangeAddress(3, 3, 3, 9));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 10, 10));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 11, 11));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 12, 12));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 13, 13));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 14, 14));
		sheet.addMergedRegion(new CellRangeAddress(3, 4, 15, 15));

		//第五行开始往后
		Pager pager = new Pager();
		pager.setPageSize(10000);
		dfglGzjfHandle.patrolPage(getSession(), pager, months, years,organcode,"");
		List<Map<String, Object>> dataList = (List<Map<String, Object>>) pager.getRows(); //查询出来的数据

		//循环每一行数据
		for (Map<String, Object> excelData : dataList) {
			XSSFRow tempRow = sheet.createRow(rowNum++);
			tempRow.setHeight((short) 500);
			// 循环单元格填入数据
			for (int j = 0; j < 16; j++) {
				XSSFCell tempCell = tempRow.createCell(j);
				tempCell.setCellStyle(contentStyle);
				String tempValue;
				if (j == 0) {
					tempValue = excelData.get("username").toString();
				} else if (j == 1) {
					tempValue = excelData.get("usercard").toString();
				} else if (j == 2) {
					tempValue = excelData.get("wage").toString();
				} else if (j == 3) {
					tempValue = excelData.get("deductionone").toString();
				} else if (j == 4) {
					tempValue = excelData.get("deductiontwo").toString();
				} else if (j == 5) {
					tempValue = excelData.get("deductionthree").toString();
				} else if(j == 6){
					tempValue = excelData.get("deductionfour").toString();
				}else if(j == 7){
					tempValue = excelData.get("deductionfive").toString();
				}else if(j == 8){
					tempValue = excelData.get("deductionsix").toString();
				}else if(j == 9){
					tempValue = excelData.get("deductionseven").toString();
				}else if(j == 10){
					tempValue = excelData.get("basefee").toString();
				}else if(j == 11){
					tempValue = excelData.get("rate").toString();
				}else if(j == 12){
					tempValue = excelData.get("payable").toString();
				}else if(j == 13){
					tempValue = excelData.get("actual")==null ? "" : excelData.get("actual").toString();
				}else if(j == 14){
					tempValue = excelData.get("remarks").toString();
				}else if(j == 15){
					tempValue = "1".equals(excelData.get("status").toString()) ? "未交" : "已交";
				}else{
					tempValue = "";
				}
				tempCell.setCellValue(tempValue);
			}
		}
		sheet.autoSizeColumn((short)1);
		sheet.autoSizeColumn((short)2);
		sheet.autoSizeColumn((short)8);
		sheet.setColumnWidth(10,4000);
		sheet.setColumnWidth(11,4000);
		sheet.setColumnWidth(12,4000);
		sheet.setColumnWidth(13,4000);
		sheet.setColumnWidth(14,4000);
		sheet.setColumnWidth(15,2000);

		String fileName = organname + "党费收交情况汇总表.xlsx";
		OutputStream stream = null;
		try {
			response.setContentType("multipart/form-data");
			response.setCharacterEncoding("utf-8");
			response.setHeader("Content-disposition", "attachment;filename="+"党费收交情况汇总表.xlsx");
			response.setHeader("FileName", "党费收交情况汇总表.xlsx");
			stream = response.getOutputStream();
			wb.write(stream);// 将数据写出去
		}catch (Exception e) {
			e.printStackTrace();
		} finally{
			try {
				if(stream != null){
					stream.flush();
					stream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @ 创建标题样式
	 * @param wb
	 * @return
	 */
	private XSSFCellStyle createTitleCellStyle(XSSFWorkbook wb) {
		XSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setWrapText(true);// 设置自动换行
		cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex()); //背景颜色
		cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
		cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
		cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
		cellStyle.setBorderRight(BorderStyle.THIN); //右边框
		cellStyle.setBorderTop(BorderStyle.THIN); //上边框

		XSSFFont headerFont = (XSSFFont) wb.createFont(); // 创建字体样式
		headerFont.setBold(true); //字体加粗
		headerFont.setFontName("黑体"); // 设置字体类型
		headerFont.setFontHeightInPoints((short) 14); // 设置字体大小
		cellStyle.setFont(headerFont); // 为标题样式设置字体样式

		return cellStyle;
	}

	private XSSFCellStyle createHeadCellStyle2(XSSFWorkbook wb) {
		XSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setWrapText(true);// 设置自动换行
		cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex()); //背景颜色
		cellStyle.setAlignment(HorizontalAlignment.CENTER); //水平居中
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
		cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
		cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
		cellStyle.setBorderRight(BorderStyle.THIN); //右边框
		cellStyle.setBorderTop(BorderStyle.THIN); //上边框

		XSSFFont headerFont = (XSSFFont) wb.createFont(); // 创建字体样式
		headerFont.setBold(true); //字体加粗
		headerFont.setFontName("黑体"); // 设置字体类型
		headerFont.setFontHeightInPoints((short) 12); // 设置字体大小
		cellStyle.setFont(headerFont); // 为标题样式设置字体样式

		return cellStyle;
	}

	/**
	 * @ 创建表头样式
	 * @param wb
	 * @return
	 */
	private static XSSFCellStyle createHeadCellStyle(XSSFWorkbook wb) {
		XSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setWrapText(true);// 设置自动换行
		cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyle.setAlignment(HorizontalAlignment.LEFT); //水平居中
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER); //垂直对齐
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.index);
		cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
		cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
		cellStyle.setBorderRight(BorderStyle.THIN); //右边框
		cellStyle.setBorderTop(BorderStyle.THIN); //上边框

		XSSFFont headerFont = wb.createFont(); // 创建字体样式
		headerFont.setBold(true); //字体加粗
		headerFont.setFontName("黑体"); // 设置字体类型
		headerFont.setFontHeightInPoints((short) 12); // 设置字体大小
		cellStyle.setFont(headerFont); // 为标题样式设置字体样式

		return cellStyle;
	}


	/**
	 * @ 创建内容样式
	 * @param wb
	 * @return
	 */
	private static XSSFCellStyle createContentCellStyle(XSSFWorkbook wb) {
		XSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
		cellStyle.setAlignment(HorizontalAlignment.CENTER);// 水平居中
		cellStyle.setWrapText(true);// 设置自动换行
		cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
		cellStyle.setBorderLeft(BorderStyle.THIN); //左边框
		cellStyle.setBorderRight(BorderStyle.THIN); //右边框
		cellStyle.setBorderTop(BorderStyle.THIN); //上边框

		// 生成12号字体
		XSSFFont font = wb.createFont();
		font.setColor((short)8);
		font.setFontHeightInPoints((short) 12);
		cellStyle.setFont(font);

		return cellStyle;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "我的党费", notes = "我的党费", httpMethod = "POST")
	@RequestMapping("/myPartyFee")
	public Message myPartyFee(String years,String usercard) {
		Message message = new Message();
		try {
			message.success().setData(dfglGzjfHandle.myPartyFee(getSession(),years,usercard));;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费收交分页查询出错");
		}
		return message;
	}




/**======================================淮矿党费收交结束============================================*/


	
/** ===============================================工作经费设置======================================================*/

	/**
	 * Description:工作经费设置查询
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param year
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "工作经费设置查询", notes = "工作经费设置查询", httpMethod = "POST")
	@RequestMapping("/list")
	public Message list(Integer year) {
		Message message = new Message();
		try {
//			String orgid = getSession().getCustom();
			String orgcode = getSession().getCurrentOrg().getOrgcode();
			String orgid = sessionUtils.getOrganidByOrganCode(orgcode);
			Long compid= getSession().getCompid();
			String classid = "";
			List<Map<String, Object>> listMap = db().listMap("select a.*,b.clname from h_gzjf_set a left join h_df_class b on a.classid=b.id and a.compid=b.compid where a.compid=? and a.years=? and a.orgid=?", new Object[] {compid,year,orgid});
			if(listMap.size()<=0 || listMap==null) {//没有查询到
				dfglGzjfHandle.insertHGzjfSet(getSession(), year, orgid, classid);
			}else {
				List<Map<String, Object>> listMap2 = db().listMap("select * from  h_df_class  where compid=? and type=1 and status=1", new Object[] {compid});
				if(listMap.size()>0 && listMap2!=null) {
					for (Map<String, Object> map : listMap2) {
						classid = map.get("id").toString();
						Map<String,Object> map2 = db().getMap("select * from h_gzjf_set where years=? and orgid=? and classid=?", new Object[] {year,orgid,classid});
						if(map2==null) {//设置表中没有该分类
							int pid = Integer.valueOf(map.get("pid").toString());
							if(pid==0) {//父节点，看看是否存在子节点
								Map<String,Object> map1 = db().getMap("select * from h_df_class where compid=? and pid=? and type=1 and status=1 limit 1", new Object[] {getSession().getCompid(),classid});
								if(map1==null) {
									dfglGzjfHandle.insertHGzjfSet(getSession(), year, orgid,classid);
								}
							}else {//子节点
								dfglGzjfHandle.insertHGzjfSet(getSession(), year, orgid,classid);
							}
						}
					}
				}
			}
			List<Map<String,Object>> list = dfglGzjfHandle.list(getSession(),year);
		    message.setData(list);
		    message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费设置查询出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 工作经费预算总额保存
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param year
	 * @param totlebudget
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "工作经费预算总额保存", notes = "工作经费预算总额保存", httpMethod = "POST")
	@RequestMapping("/savesummary")
	public Message saveSummary(Integer year, String totlebudget) {
		Message message = new Message();
		try {
			dfglGzjfHandle.saveSummary(getSession(),year,totlebudget);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费预算总额保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 工作经费汇总（如果为null那就显示0）
	 * @author PT  
	 * @date 2019年9月2日
	 * @param year
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "工作经费汇总（如果为null那就显示0）", notes = "工作经费汇总（如果为null那就显示0）", httpMethod = "POST")
	@RequestMapping("/getsummary")
	public Message getSummary(Integer year) {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglGzjfHandle.getSummary(getSession(),year);
		    message.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费汇总查询出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 设置列取金额
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param year
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "设置列取金额", notes = "设置列取金额", httpMethod = "POST")
	@RequestMapping("/setlistamount")
	public Message setListamount(Integer year,Integer classid,String listamount) {
		Message message = new Message();
		try {
			dfglGzjfHandle.setListamount(getSession(),year,classid,listamount);
		    message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费汇总查询出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 年度结算
	 * @author PT  
	 * @date 2019年9月2日 
	 * @param year
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "年度结算", notes = "年度结算", httpMethod = "POST")
	@RequestMapping("/settlement")
	public Message settlement(Integer year) {
		Message message = new Message();
		try {
			dfglGzjfHandle.settlement(getSession(),year);
		    message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费汇总查询出错");
		}
		return message;
	}
	
	
	/** ===============================================工作经费申请======================================================*/	
	
	
	/**
	 * Description: 工作经费申请保存
	 * @author PT  
	 * @date 2019年9月3日
	 * @param feetype
	 * @param appfee
	 * @param appdesc
	 * @param appid
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "工作经费申请保存", notes = "工作经费申请保存", httpMethod = "POST")
	@RequestMapping("/saveworkfundingapplication")
	public Message saveWorkFundingApplication(Integer feetype, String appfee, String appdesc,String appid) {
		Message message = new Message();
		try {
			dfglGzjfHandle.saveWorkFundingApplication(getSession(),feetype,appfee,appdesc,appid);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage(e.getMessage());
		}
		return message;
	}
	
	
	/**
	 * Description: 工作/报销 经费申请审批
	 * @author PT  
	 * @date 2019年9月3日
	 * @param id
	 * @param classid(工作经费申请审批时传空串，报销经费审批时传最终的分类id)
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "工作/报销 经费申请审批", notes = "工作/报销 经费申请审批", httpMethod = "POST")
	@RequestMapping("/auditworkfundingapplication")
	public Message auditWorkFundingApplication(Long id,int result,String appmesg,String classid) {
		Message message = new Message();
		try {
			dfglGzjfHandle.auditWorkFundingApplication(getSession(),id,result,appmesg,classid);
		    message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 获取申请审核的记录
	 * @author PT  
	 * @date 2019年9月3日
	 * @param id
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取申请审核的记录", notes = "获取申请审核的记录", httpMethod = "POST")
	@RequestMapping("/getworkfundingapplication")
	public Message getWorkFundingApplication(Long id) {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglGzjfHandle.getWorkFundingApplication(getSession(),id);
		    message.success().setData(data);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 报销申请保存
	 * @author PT  
	 * @date 2019年9月3日
	 * @param flag 新增传0 修改传1
	 * @param appid  申请id
	 * @param sumfee 报销金额
	 * @param feelist 金额明细
	 * @param remarks3  发票附件
	 * @param remarks4 申请单附件
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "报销申请保存", notes = "报销申请保存", httpMethod = "POST")
	@RequestMapping("/savereimbursementfundingapplication")
	public Message saveReimbursementFundingApplication(Integer flag, String appid,
			String sumfee, String feelist, String remarks3, String remarks4) {
		Message message = new Message();
		try {
			dfglGzjfHandle.saveReimbursementFundingApplication(getSession(), flag, appid, sumfee,
					feelist, remarks3, remarks4);
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 二级党委报销审核时点击分类显示该分类金额的具体信息
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param classid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "二级党委报销审核时点击分类显示该分类金额的具体信息", notes = "二级党委报销审核时点击分类显示该分类金额的具体信息", httpMethod = "POST")
	@RequestMapping("/getclassinfo")
	public Message getClassInfo(Long classid) {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglGzjfHandle.getClassInfo(getSession(),classid);
		    message.success().setData(data);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 获取审批日志
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param appid
	 * @param type
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取审批日志", notes = "获取审批日志", httpMethod = "POST")
	@RequestMapping("/listapprovallog")
	public Message listApprovalLog(Long appid,Integer type) {
		Message message = new Message();
		try {
			List<Map<String,Object>> data = dfglGzjfHandle.listApprovalLog(getSession(),appid,type);
		    message.success().setData(data);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 工作经费申请分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param pager
	 * @param name
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "工作经费申请分页查询", notes = "工作经费申请分页查询", httpMethod = "POST")
	@RequestMapping("/applicationpage")
	public Message applicationPage(Pager pager,Integer type,String name) {
		Message message = new Message();
		try {
			pager = dfglGzjfHandle.applicationPage(getSession(),pager,type,name);
		    message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 工作经费审批分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param pager
	 * @param type
	 * @param name
	 * @param status
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "工作经费审批分页查询", notes = "工作经费审批分页查询", httpMethod = "POST")
	@RequestMapping("/auditpage")
	public Message auditPage(Pager pager,int type,String name,int status) {
		Message message = new Message();
		try {
			pager = dfglGzjfHandle.auditPage(getSession(),pager,type,name,status);
		    message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
/**==================================================费用管理一二级党组织专用=============================================================*/	
	
	/**
	 * Description: 一二级机构专用申请保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param appflag 0党费申请，1工作经费申请
	 * @param feetype 分类id
	 * @param appfee  申请金额
	 * @param appdesc  事项
	 * @param appid   申请的id新增传空
	 * @param retain  自留费用拨款额度     只有党费申请时才有工作经费申请时传空串
	 * @param allocate 下拨党费拨款额度  只有党费申请时才有工作经费申请时传空串
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "一二级机构专用申请保存", notes = "一二级机构专用申请保存", httpMethod = "POST")
	@RequestMapping("/saveapplication")
	public Message saveApplication(int appflag, int feetype, String appfee, String appdesc,String appid,String retain, String allocate) {
		Message message = new Message();
		try {
			dfglGzjfHandle.saveApplication(getSession(),appflag,feetype,appfee,appdesc,appid,retain,allocate);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 各机构自己花钱自己走账列表查询
	 * @author PT  
	 * @date 2019年9月11日 
	 * @param pager
	 * @param name
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "各机构自己花钱自己走账列表查询", notes = "各机构自己花钱自己走账列表查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(Pager pager,String name) {
		Message message = new Message();
		try {
			pager = dfglGzjfHandle.page(getSession(),pager,name);
		    message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
}
