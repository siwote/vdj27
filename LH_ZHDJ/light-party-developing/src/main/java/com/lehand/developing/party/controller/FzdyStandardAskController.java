package com.lehand.developing.party.controller;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.developing.party.service.FzdyStandardAskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api(value = "要求，书写规范配置服务", tags = { "要求，书写规范配置服务" })
@RestController
@RequestMapping("/developing/steup")
public class FzdyStandardAskController extends FzdyBaseController {
	
	private static final Logger logger = LogManager.getLogger(FzdyStandardAskController.class);
	
	@Resource FzdyStandardAskService fzdyStandardAskService;

	@OpenApi(optflag=0)
	@ApiOperation(value = "查询要求/书写配置信息", notes = "查询要求/书写配置信息", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "tag", value = "标签 1 要求/2 书写规范", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤分类id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "flag", value = "是否有要求/书写", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/queryStandardAsk")
	public Message queryStandardAskByTypeId(Long tag,Long typeid,String flag) {
		Message message = null;
		try {
			if(null == tag) {
				message = Message.FAIL;
				message.setMessage("标签tag不可为空");
				return message;
			}
			
			Object obj = null;
			if(tag == 1) {
				obj = fzdyStandardAskService.queryStandardByTypeId(typeid, flag, getSession());
			}
			if(tag == 2) {
				obj = fzdyStandardAskService.queryAskByTypeId(typeid, flag, getSession());
			}
			message = Message.SUCCESS;
			message.setData(obj);
			message.setMessage("查询成功");
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=1)
	@ApiOperation(value = "保存要求/书写配置信息", notes = "保存要求/书写配置信息", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "tag", value = "标签 1 要求/2 书写规范", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "typeid", value = "步骤分类id", required = true, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "id", value = "要求/书写id,修改时必传", required = false, paramType = "query", dataType = "Long", defaultValue = ""),
		@ApiImplicitParam(name = "flag", value = "是否有要求/书写", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "content", value = "富文本编辑器内容", required = true, paramType = "query", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "remark", value = "周期", required = false, paramType = "query", dataType = "String", defaultValue = "")

	})
	@RequestMapping("/saveStandardAsk")
	public Message saveStandardAsk(Long tag,Long typeid,Long id,String flag,String content,String remark) {
		Message message = null;
		try {
			
			if(null == tag) {
				message = Message.FAIL;
				message.setMessage("标签tag不可为空");
				return message;
			}
			Boolean success = false;
			//要求配置
			if(tag == 1) {
				if(!StringUtils.isEmpty(id) && id > 0 ) {
					success = fzdyStandardAskService.updateStandard(typeid, id, flag, content,remark, getSession());
				}else {
					success = fzdyStandardAskService.addStandard(typeid, flag, content,remark, getSession());
				}
			}
			//书写规范
			if(tag == 2) {
				if(!StringUtils.isEmpty(id) && id > 0) {
					success = fzdyStandardAskService.updateAsk(typeid, id, flag, content, getSession());
				}else {
					success = fzdyStandardAskService.addAsk(typeid, flag, content, getSession());
				}
			}
			
			//判断是否成功
			if(!success) {
				message = Message.FAIL;
				message.setMessage("保存失败");
				return message;
			}
			message = Message.SUCCESS;
			message.setData("");
			message.setMessage("保存成功");
			
		} catch (Exception e) {
			message = Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return  message;
	}

}
