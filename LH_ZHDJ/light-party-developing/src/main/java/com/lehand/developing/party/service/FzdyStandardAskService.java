package com.lehand.developing.party.service;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.pojo.FzdyWorkflow;
import com.lehand.developing.party.pojo.FzdyWritenorm;

/**
 * @ClassName:：FzdyStandardAskBusinessImpl
 * @Description： 发展党员要求配置/书写规范配置实现类
 * @author ：taogang
 * @date ：2019年7月10日 上午9:18:30
 *
 */
@Service
public class FzdyStandardAskService {
	
	@Resource private GeneralSqlComponent generalSqlComponent;


	
	@Transactional(rollbackFor=Exception.class)
	public boolean addStandard(Long typeid,String flag, String content, String remark,Session session) {
		FzdyWorkflow info = new FzdyWorkflow();
		info.setSfflow(flag);
		info.setWorkflow(content);
		info.setOrderid(1);
		info.setTypeid(typeid);
		info.setStatus("1");
		info.setCompid(session.getCompid());
		info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setCreator(session.getUserid());
//		info.setOrgid(session.getOrganids().get(0).getOrgid());
//		info.setOrgname(session.getOrganids().get(0).getOrgname());
        //周期
        info.setRemark(remark);
		info.setOrgid(session.getCurrentOrg().getOrgid());
		info.setOrgname(session.getCurrentOrg().getOrgname());
		Integer success = generalSqlComponent.insert(SqlCode.addStandard, info);
		if(success > 0) {
			return true;
		}
		return false;
	}

	
	@Transactional(rollbackFor=Exception.class)
	public boolean updateStandard(Long typeid,Long id, String flag, String content,String remark, Session session) {
		//查询要求配置
		FzdyWorkflow info = queryStandardById(id, session.getCompid());
		if(null == info) {
			return false;
		}
		info.setSfflow(flag);
		info.setWorkflow(content);
		info.setTypeid(typeid);
		info.setModitime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setModitor(session.getUserid());
//		info.setOrgid(session.getOrganids().get(0).getOrgid());
//		info.setOrgname(session.getOrganids().get(0).getOrgname());
        //周期
        info.setRemark(remark);
		info.setOrgid(session.getCurrentOrg().getOrgid());
		info.setOrgname(session.getCurrentOrg().getOrgname());
		Integer success = generalSqlComponent.update(SqlCode.updateStandard, info);
		if(success > 0) {
			return true;
		}
		return false;
	}

	
	@Transactional(rollbackFor=Exception.class)
	public boolean addAsk(Long typeid,String flag, String content, Session session) {
		FzdyWritenorm info = new FzdyWritenorm();
		info.setSfnorm(flag);
		info.setWritenorm(content);
		info.setOrderid(1);
		info.setTypeid(typeid);
		info.setStatus("1");
		info.setCompid(session.getCompid());
		info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setCreator(session.getUserid());
//		info.setOrgid(session.getOrganids().get(0).getOrgid());
//		info.setOrgname(session.getOrganids().get(0).getOrgname());
		info.setOrgid(session.getCurrentOrg().getOrgid());
		info.setOrgname(session.getCurrentOrg().getOrgname());
		Integer success = generalSqlComponent.insert(SqlCode.addAsk, info);
		if(success > 0) {
			return true;
		}
		return false;
	}

	
	@Transactional(rollbackFor=Exception.class)
	public boolean updateAsk(Long typeid,Long id, String flag, String content, Session session) {
		//查询书写规范信息
		FzdyWritenorm  info = queryAskById(id, session.getCompid());
		if(null == info) {
			return false;
		}
		info.setSfnorm(flag);
		info.setWritenorm(content);
		info.setTypeid(typeid);
		info.setModitime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setModitor(session.getUserid());
//		info.setOrgid(session.getOrganids().get(0).getOrgid());
//		info.setOrgname(session.getOrganids().get(0).getOrgname());
		info.setOrgid(session.getCurrentOrg().getOrgid());
		info.setOrgname(session.getCurrentOrg().getOrgname());
		Integer success = generalSqlComponent.update(SqlCode.updateAsk, info);
		if(success > 0) {
			return true;
		}
		return false;
	}

	
	public Map<String, Object> queryStandardByTypeId(Long typeid, String flag, Session session) {
		//定义参数
		Map<String, Object> param = new HashMap<String, Object>(2);
		param.put("compid", session.getCompid());
		param.put("typeid", typeid);
		if(!StringUtils.isEmpty(flag)) {
			param.put("flag", flag);
		}
		return generalSqlComponent.query(SqlCode.queryStandardByTypeId, param);
	}

	
	public Map<String, Object> queryAskByTypeId(Long typeid, String flag, Session session) {
		//定义参数
		Map<String, Object> param = new HashMap<String, Object>(2);
		param.put("compid", session.getCompid());
		param.put("typeid", typeid);
		if(!StringUtils.isEmpty(flag)) {
			param.put("flag", flag);
		}
		return generalSqlComponent.query(SqlCode.queryAskByTypeId, param);
	}
	
	/**
	 * 
	 * @Title：queryStandardById 
	 * @Description：根据id查询要求配置
	 * @param ：@param id
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：FzdyWorkflow 
	 * @throws
	 */
	public FzdyWorkflow queryStandardById(Long id,Long compid) {
		return generalSqlComponent.query(SqlCode.queryStandardById, new Object[] {id,compid});
	}
	
	/**
	 * 
	 * @Title：queryAskById 
	 * @Description：根据id查询书写规范配置
	 * @param ：@param id
	 * @param ：@param compid
	 * @param ：@return 
	 * @return ：FzdyWritenorm 
	 * @throws
	 */
	public FzdyWritenorm queryAskById(Long id,Long compid) {
		return generalSqlComponent.query(SqlCode.queryAskById, new Object[] {id,compid});
	}

}
