package com.lehand.developing.party.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 * 人力资源推送数据扩展类
 * @author lx
 */
@ApiModel(value="人力资源推送数据扩展类",description="人力资源推送数据扩展类")
public class PushHumanResourceReq {

	@ApiModelProperty(value="身份证号码", name="datas")
	private List<PushHumanResourceDto> request;

	public List<PushHumanResourceDto> getRequest() {
		return request;
	}

	public void setRequest(List<PushHumanResourceDto> request) {
		this.request = request;
	}
}
