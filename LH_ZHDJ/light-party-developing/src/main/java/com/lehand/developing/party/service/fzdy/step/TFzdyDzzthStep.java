package com.lehand.developing.party.service.fzdy.step;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.developing.party.service.fzdy.BaseStep;


@Service
public class TFzdyDzzthStep extends BaseStep {
	
	
	//person_id 
	@SuppressWarnings("unchecked")
	public Pager page(Pager pager, Map<String, Object> params, Session session) {
	    db().pageMap(pager, "select * from t_fzdy_dzzth where person_id=:person_id and syncstatus='A' and compid=:compid order by thsj asc", params);
	    //找到最早谈话的记录将其谈话时间与提交申请书时间进行比较，谈话时间比提交申请书时间延后一个月算正常，超过一个月算逾期
	    Map<String, Object> map = db().getMap("select * from t_fzdy_rdsqs where person_id=:person_id and syncstatus='A' and compid=:compid limit 1", params);
	    List< Map<String, Object>> list = (List<Map<String, Object>>) pager.getRows();
	    try {
	    	if(list!=null && list.size()>0) {
	    		long rdsqtime = 0L;
	    		long zzthtime = DateEnum.YYYYMMDDHHMMDD.parse(list.get(0).get("thsj").toString()).getTime();
	    		if(map!=null) {
	    			rdsqtime = DateEnum.YYYYMMDDHHMMDD.parse(map.get("rq").toString()).getTime();
	    		}else {
	    			rdsqtime = zzthtime;
	    		}
	    		long daytime = 30L*24*3600*1000;
	    		for (int i = 0; i < list.size(); i++) {
	    			list.get(i).put("status", 1);//正常状态
	    		}
	    		if(zzthtime>rdsqtime+daytime) {
	    			list.get(0).put("status", 2);//逾期状态
	    		}
	    	}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    pager.setRows(list);
		return pager;
	}
	

	
	//talk_id
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_dzzth where talk_id=:talk_id", params);
		String fileids = String.valueOf(map.get("file"));
		List<Map<String, Object>> files = new ArrayList<Map<String,Object>>(0);
		if(!StringUtils.isEmpty(fileids)) {
			files = db().listMap("select * from dl_file where compid=? and id in ("+fileids+")", new Object[] {session.getCompid()});
		}
		map.put("files", files);
		return map;
	}
	

	//	thsj
	//	thr
	//	thrname
	//	thrdnzw
	//	thnr
	//  person_id
	//  file
	//  flags 新增传1 修改传0
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		int flags = Integer.valueOf(params.get("flags").toString());
		if(flags==1) {
			params.put("talk_id", UUID.randomUUID().toString().replaceAll("-", ""));
		}
		params.put("created_by", session.getUserid());
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		int num = 0;
		if(flags==0) {
			 num = db().delete("UPDATE t_fzdy_dzzth SET syncstatus='D' WHERE talk_id=:talk_id and  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		}
		 params.put("talk_id", UUID.randomUUID().toString().replaceAll("-", ""));
		 num += db().insert("INSERT INTO t_fzdy_dzzth (compid, talk_id, thsj, thr, thrname, thrdnzw, thnr, created_by, create_date,  delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:talk_id,:thsj,:thr,:thrname,:thrdnzw,:thnr,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		 Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
			if(map!=null && map.get("sfipen").toString().equals("0")) {
				db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		 }
		List<Map<String,Object>> data = db().listMap("select file from t_fzdy_dzzth where person_id=? and syncstatus='A'",  new Object[] {params.get("person_id")});	
		String fileids="";
		if(data!=null && data.size()>0) {
			for (Map<String, Object> map2 : data) {
				fileids += map2.get("file")+",";
			}
		}
		if(StringUtils.isEmpty(params.get("file"))) {
			fileids = fileids.substring(0,fileids.length()-1);
		}else {
			fileids = fileids+params.get("file");
		}
		params.put("file", fileids);
		saveFile(params);
			
		 return num;
	}
}
