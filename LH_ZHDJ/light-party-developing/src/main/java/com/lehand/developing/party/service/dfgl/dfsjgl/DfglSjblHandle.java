package com.lehand.developing.party.service.dfgl.dfsjgl;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.constant.Constant;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.dto.PeisonActualDto;
import com.lehand.developing.party.pojo.TDyInfo;
import com.lehand.developing.party.service.dfgl.gzjfgl.DfglGzjfHandle;
import com.lehand.developing.party.utils.ExcelUtils;
import com.lehand.developing.party.utils.NumberUtils;
import com.lehand.developing.party.utils.SessionUtils;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfoSimple;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;


/**
 * Description: 党费上交比例设置
 * @author pantao
 *
 */
@Service
public class DfglSjblHandle {

	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	@Resource
	protected SessionUtils sessionUtils;

	/*@Resource
	private DocumentManagerService documentManagerBusiness;*/

	@Resource
	protected DfglGzjfHandle dfglGzjfHandle;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}


	//党支部code
	@Value("${jk.organ.dangzhibu}")
	private String dangzhibu;
	//党支部code
	@Value("${jk.organ.lianhedangzhibu}")
	private String lianhedangzhibu;
	//党总之code
	@Value("${jk.organ.dangzongzhi}")
	private String dangzongzhi;
	//正式党员code
	@Value("${jk.organ.zhengshidangyuan}")
	private String zhengshidangyuan;
	//预备党员code
	@Value("${jk.organ.yubeidangyuan}")
	private String yubeidangyuan;


	@Value("${jk.manager.down_ip}")
	private String down_ip;

	@Value("${spring.http.multipart.location}")private String location;

	/**
	 * Description: 党费上交比例设置（比例必须是整数，前台判断条件是icon=6100000035并且pid!=34190000001满足条件的可以设置）
	 * @author PT
	 * @date 2019年8月14日
	 * @param params
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void save(Map<String, Object> params, Session session) {
		Long compid = session.getCompid();
		params.put("compid", compid);
//		db().update("update pw_organ set remarks2=:ratio where compid=:compid and id=:organid", params);
		db().update("update urc_organization set remarks2=:ratio where compid=:compid and orgid=:organid", params);

	}

	/**
	 * Description: 给各个机构进行调整基数授权
	 * @author PT
	 * @date 2019年8月21日
	 * @param params (sqsave 1授权 0取消授权  organid)
	 * @param session
	 */
	@Transactional(rollbackFor = Exception.class)
	public void sqsave(Map<String, Object> params, Session session) {
		Long compid = session.getCompid();
		params.put("compid", compid);
//		db().update("update pw_organ set remarks4=:sqsave where compid=:compid and id=:organid", params);
		db().update("update urc_organization set remarks4=:sqsave where compid=:compid and orgid=:organid", params);
	}


	/**
	 * Description: 获取指定节点下的所有子节点(含组织，不含用户)
	 * @author PT
	 * @date 2019年8月21日
	 * @param compid
	 * @param pid
	 * @return
	 */
	public List<Map<String,Object>> listOrganChildNodeTwo(Long compid,Long pid/*,String name*/) {
		//if(StringUtils.isEmpty(name)) {
//			return db().listMap("SELECT 1 flag,id,remarks3 account,orgname dzzqc,remarks6 name, seqno,1 remarks1,pid,otid icon,remarks2 ratio,remarks4 sqsave,b.organid,a.orgcode organcode FROM pw_organ a left join t_dzz_info_simple b on a.orgcode = b.organcode WHERE a.compid=? and a.pid=? and a.status=1 order by a.seqno", new Object[] {compid,pid});
			return db().listMap("SELECT 1 flag,orgid,orgid id,remarks3 account,orgname dzzqc,sorgname name, orgseqno,1 remarks1,porgid,zzlb,remarks2 ratio,remarks4 sqsave,b.organid,a.orgcode organcode FROM urc_organization a left join t_dzz_info b on a.orgcode = b.organcode WHERE a.compid=? and a.porgid=? and a.status=1 order by a.orgseqno", new Object[] {compid,pid});
//		}else {
//			return db().listMap("SELECT 1 flag,id,remarks3 account,orgname dzzqc,remarks6 name, seqno,1 remarks1,pid,otid icon,remarks2 ratio,remarks4 sqsave,b.organid,a.orgcode organcode FROM pw_organ a left join t_dzz_info_simple b on a.orgcode = b.organcode WHERE a.compid=? and a.pid=? and a.status=1 and a.orgname like'%"+name+"%' order by a.seqno", new Object[] {compid,pid});
//		}
	}


	/**
	 * Description: 获取当前机构信息
	 * @author PT
	 * @date 2019年8月21日
	 * @param session
	 * @return
	 */
	public Map<String,Object> listOrganChildNodeOne(Session session/*,String name*/) {
		Long compid = session.getCompid();
		Long organid = session.getCurrorganid();
//		if(StringUtils.isEmpty(name)) {
//			return db().getMap("SELECT 1 flag,id,remarks3 account,orgname dzzqc,remarks6 name, seqno,1 remarks1,pid,otid icon,remarks2 ratio,remarks4 sqsave,b.organid,a.orgcode organcode FROM pw_organ a left join t_dzz_info_simple b on a.orgcode = b.organcode WHERE a.compid=? and a.id=? and a.status=1 order by a.seqno", new Object[] {compid,organid});
			return db().getMap("SELECT 1 flag,orgid,orgid id,remarks3 account,orgname dzzqc,sorgname name, orgseqno,1 remarks1,porgid,zzlb,remarks2 ratio,remarks4 sqsave,b.organid,a.orgcode organcode FROM urc_organization  a left join t_dzz_info b on a.orgcode = b.organcode WHERE a.compid = ? and a.orgid = ? and a.status=1 order by a.orgseqno", new Object[] {compid,organid});
//		}else {
//			return db().getMap("SELECT 1 flag,id,remarks3 account,orgname dzzqc,remarks6 name, seqno,1 remarks1,pid,otid icon,remarks2 ratio,remarks4 sqsave,b.organid,a.orgcode organcode FROM pw_organ a left join t_dzz_info_simple b on a.orgcode = b.organcode WHERE a.compid=? and a.id=? and a.status=1 and a.orgname like'%"+name+"%' order by a.seqno", new Object[] {compid,organid});
//		}
	}


	/**
	 * Description: 机构下党员集合分页查询
	 * @author PT
	 * @date 2019年8月21日
	 * @param xm
	 * @param organid
	 * @param pager
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager page(Session session, String xm, String organid, Pager pager) {
		Long compid = session.getCompid();
		if(StringUtils.isEmpty(organid)) {
			organid = session.getCurrorganid().toString();
		}
		if(StringUtils.isEmpty(xm)) {
			db().pageMap(pager, "select a.xm,a.userid,0 bl,0 yjdf,b.basefee jnjs,b.onpost from t_dy_info a left join h_dy_jfjs b on a.userid = b.userid where a.organid = ? and (a.operatetype != 3 OR a.operatetype is NULL) and (dylb=? or dylb=?) and (a.delflag!=1 or a.delflag is null)", new Object[] {organid,zhengshidangyuan,yubeidangyuan});
		}else {
			db().pageMap(pager, "select a.xm,a.userid,0 bl,0 yjdf,b.basefee jnjs,b.onpost from t_dy_info a left join h_dy_jfjs b on a.userid = b.userid where a.organid = ? and (a.operatetype != 3 OR a.operatetype is NULL) and (dylb=? or dylb=?) and a.xm like '%"+xm+"%' and (a.delflag!=1 or a.delflag is null)", new Object[] {organid,zhengshidangyuan,yubeidangyuan});
		}
		List<Map<String,Object>> lists = (List<Map<String, Object>>) pager.getRows();
		for (Map<String, Object> map : lists) {
			if(map.get("onpost")!=null && "0".equals(map.get("onpost").toString())) {
				map.put("bl", 0);
				map.put("yjdf", map.get("jnjs"));
				map.put("jnjs", 0);
			}else {
				Double jnjs = map.get("jnjs")==null? 0:Double.valueOf(map.get("jnjs").toString());//基数
//				Map<String,Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and min<=? and max>? limit 1", new Object[] {compid,jnjs,jnjs});
				Map<String, Object> jssz =db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[] {compid, jnjs});
				if(jssz==null) {
					LehandException.throwException("党费缴纳比例未设置，请先设置再操作！");
				}
				Double ratio = Double.valueOf(jssz.get("ratio").toString());//比例
				map.put("bl", ratio);
				DecimalFormat df=new DecimalFormat("0.00");
				Double yjdf = jnjs*ratio/100;
				String yjdf2 = df.format(yjdf);
				map.put("yjdf", yjdf2);
			}
		}
		pager.setRows(lists);
		return pager;
	}


	/**
	 * Description: 插入基数数据
	 * @author PT
	 * @date 2019年8月21日
	 * @param session
	 * @param organid
	 */
	@Transactional(rollbackFor = Exception.class)
	public void insert(Session session, String organid) {
		Long compid = session.getCompid();
		//机构下党员的数量
		Map<String,Object> map1 = db().getMap("select count(*) as num from t_dy_info where organid = ? and (operatetype != 3 or operatetype is null) and (dylb = ? or dylb = ?) and (delflag != 1 or delflag is null)", new Object[] {organid,zhengshidangyuan,yubeidangyuan});
	    int num1 = Integer.valueOf(map1.get("num").toString());
		//已经产生的当年当月下各党员的基数设置数量
	    Map<String,Object> map2 = db().getMap("select count(*) num from h_dy_jfjs where compid=? and organid=?", new Object[] {compid,organid});
	    int num2 = Integer.valueOf(map2.get("num").toString());
	    if(num1>num2) {
			Object[] args = new Object[] {organid,compid,organid};
			Object[] args2 = new Object[] {organid,compid,organid,zhengshidangyuan,yubeidangyuan};
			db().insert("INSERT INTO h_dy_jfjs (userid,username,organid,basefee,compid) SELECT a.userid,a.xm,?,0,? FROM  t_dy_info a WHERE organid=? and NOT EXISTS (SELECT 1 FROM h_dy_jfjs b WHERE a.userid = b.userid)", args);
			db().insert("INSERT INTO h_dy_jfinfo (userid,username,organid,basefee,status,year,month,compid) SELECT a.userid,a.xm,?,0,1,YEAR(NOW()),MONTH(NOW()),? FROM  t_dy_info a WHERE organid=? and operatetype != 3 and (dylb=? or dylb=?) and syncstatus='Q' and NOT EXISTS (SELECT 1 FROM h_dy_jfinfo b WHERE a.userid = b.userid)", args2);
		}else if(num1<num2) {
			db().delete("DELETE a FROM h_dy_jfjs a WHERE NOT EXISTS (SELECT 1 FROM t_dy_info b WHERE a.userid = b.userid)", new Object[] {});
			db().delete("DELETE a FROM h_dy_jfinfo a WHERE NOT EXISTS (SELECT 1 FROM t_dy_info b WHERE a.userid = b.userid)", new Object[] {});
		}
	}

	/**
	 * Description:调整党费基数
	 * @author PT
	 * @date 2019年8月22日
	 * @param session
	 * @param basefee 基数 (在岗传基数不在岗传应交党费)
	 * @param userid 党员userid
	 * @param onpost 是否在岗(0:不在岗,1:在岗)
	 */
	@Transactional(rollbackFor = Exception.class)
	public void update(Session session, Double basefee, String userid,String username, String organid, String onpost) {
		String adjusttime = DateEnum.YYYYMMDDHHMMDD.format();
		Long ajuserid = session.getUserid();
		String ajusername = session.getUsername();
		Long compid = session.getCompid();
		Object[] args = new Object[] {onpost,basefee,adjusttime,ajuserid,ajusername,compid,userid};
		Map<String, Object> map = db().getMap("select * from h_dy_jfjs where compid=? and userid=?", new Object[]{compid, userid});
		if(map!=null&&map.size()>0){
			//是否是人力资源推送
			long synchronous = (long) map.get("synchronous");
			if(1==synchronous){
				LehandException.throwException("人力资源推送数据不能修改");
			}
			db().update("update h_dy_jfjs set onpost=?,basefee=?,adjusttime=?,ajuserid=?,ajusername=? where compid=? and userid=?", args);
		}else{
			Map<String,Object> parms1 = new HashMap<String,Object>();
			parms1.put("userid", userid);
			parms1.put("username", username);
			parms1.put("organid", organid);
			parms1.put("basefee",  basefee);
			parms1.put("compid", compid);
			parms1.put("adjusttime", adjusttime);
//			`userid`, `username`, `organid`, `basefee`, `onpost`, `adjusttime`, `ajuserid`, `ajusername`, `compid`
			db().insert("insert h_dy_jfjs set userid=:userid,username=:username,organid=:organid,basefee=:basefee,compid=:compid,adjusttime=:adjusttime",parms1);
		}

		SimpleDateFormat sim1 = new SimpleDateFormat("yyyy");
		int year = Integer.valueOf(sim1.format(DateEnum.now()));
		SimpleDateFormat sim2 = new SimpleDateFormat("MM");
		int month = Integer.valueOf(sim2.format(DateEnum.now()));
		//党组织是否上报党费
		Map<String,Object> hdzz = db().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?", new Object[] {compid,organid,year,month});
		if(hdzz!=null){
			Integer apstatus = (Integer) hdzz.get("apstatus");
			if(apstatus==0||apstatus==3){
				//比例查询
				hDYJfInfo(basefee, userid, username, organid, onpost, adjusttime, ajuserid, ajusername, compid, year, month);

				//查询是否有当月费用
				String usercard = db().getSingleColumn("SELECT zjhm FROM t_dy_info  WHERE userid =?", String.class, new Object[]{userid});
                Map dues = getHDyDues(year,month,usercard);
				List<String> data = new ArrayList<>();
				data.add(username);
				data.add(usercard);
				if(dues!=null&&dues.size()>0){
					//删除个人党费明细
					dfglGzjfHandle.updateHDyDues3(String.valueOf(year),month,data,organid,basefee);
				}else{
					//插入个人党费明细
					dfglGzjfHandle.insertHDyDues2(String.valueOf(year),month,data,organid,basefee);
				}
			}
		}
	}
	private Map<String,Object> getHDyDues(Integer years,Integer months,String usercard) {
		return db().getMap("SELECT * FROM h_dy_dues where years=? and months=? and usercard=?",new Object[]{years,months,usercard});

	}
	public void hDYJfInfo(Double basefee, String userid, String username, String organid, String onpost, String adjusttime, Long ajuserid, String ajusername, Long compid, int year, int month) {
//		Map<String,Object> jssz = db().getMap("select * from t_dfgl_jssz where compid=? and min<=? and max>? limit 1", new Object[] {compid, basefee, basefee});
		Map<String, Object> jssz =db().getMap("select * from t_dfgl_jssz where compid=? and (? BETWEEN min AND max) ORDER BY id asc limit 1", new Object[] {compid, basefee});
		if(jssz==null) {
			LehandException.throwException("党费缴纳比例未设置，请先设置再操作！");
		}
		Map<String, Object> params = getParams(basefee, userid, username, organid, onpost, adjusttime, ajuserid,
				ajusername, compid, year, month, jssz.get("ratio").toString());
		Map<String,Object> map = db().getMap("select * from h_dy_jfinfo where compid=? and userid=? and year=? and month=?", new Object[] {compid, userid, year, month,});
		//month月份是否在其他支部生成h_dy_jfinfo数据，如果生成了则不能在本支部生成
		Map<String,Object> map2 = db().getMap("select * from h_dy_jfinfo where userid = ? and month =? and year = ? and organid !=? and compid =?", new Object[] {userid,month, year,organid, compid,});
		if(map2==null){
			if(map==null) {//新增
				db().insert("INSERT INTO h_dy_jfinfo (userid, year, month, basefee, rate, payable, actual, status, remark, onpost, organid, username, ajuserid, ajusername, adjusttime, compid) VALUES (:userid, :year, :month, :basefee, :rate, :payable, :actual, :status, :remark, :onpost, :organid, :username, :ajuserid, :ajusername, :adjusttime, :compid)", params);
			}else {//修改
				db().update("update h_dy_jfinfo set basefee=:basefee,rate=:rate,payable=:payable,actual=:actual,onpost=:onpost,ajuserid=:ajuserid,ajusername=:ajusername,adjusttime=:adjusttime where compid=:compid and userid=:userid and year=:year and month=:month", params);
			}
		}

	}

	public Map<String, Object> getParams(Double basefee, String userid, String username, String organid, String onpost,
			String adjusttime, Long ajuserid, String ajusername, Long compid, int year, int month,
			String jssz) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("userid", userid);
		params.put("year", year);
		params.put("month", month);
		params.put("basefee", basefee);

		Double ratio = Double.valueOf(jssz);
		params.put("rate", ratio);

	    DecimalFormat df=new DecimalFormat("0.00");
	    Double yjdf = basefee*ratio/100;
	    String yjdf2 = df.format(yjdf);
		params.put("payable", yjdf2);

		params.put("actual", 0);
		params.put("status", 1);
		params.put("remark", Constant.ENPTY);
		params.put("onpost", onpost);
		params.put("organid", organid);
		params.put("username", username);
		params.put("ajuserid", ajuserid);
		params.put("ajusername", ajusername);
		params.put("adjusttime", adjusttime);
		params.put("compid", compid);
		if("0".equals(onpost)) {//不在岗
			params.put("basefee", 0);
			params.put("rate", 0);
			params.put("payable", basefee);
		}
		return params;
	}


	/**
	 * Description: 党费上报分页查询
	 * @author PT
	 * @date 2019年8月22日
	 * @param month
	 * @param year
	 * @param organid
	 * @param xm
	 * @param pager
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager reportPage(Session session, String organid, String month, String year, String xm, Pager pager) {
		Long compid = session.getCompid();
		Map<String,Object> map3 = db().getMap("select * from t_dzz_info where organid=? and (delflag !=1 or delflag is null)", new Object[] {organid});
		if(map3==null){
			return pager;
		}
		if(dangzhibu.equals(map3.get("zzlb").toString()) || lianhedangzhibu.equals(map3.get("zzlb").toString())) {//党支部
			if(StringUtils.isEmpty(xm)) {
//				db().pageMap(pager, "select a.*,1 icon,b.zjhm idCard from h_dy_jfinfo a left join t_dy_info b on b.userid = a.userid where a.compid=? and a.year=? and a.month=? and a.organid=? and a.status=1 and (b.dylb = '1000000001' or b.dylb ='2000000002') and (b.delflag!=1 or b.delflag is null)", new Object[] {compid,year,month,organid});
				db().pageMap(pager, "select b.userid,a.remarks,a.years year,a.months month,a.basefee,a.rate,a.payable,a.actual,a.status,a.organcode organid,a.username,1 icon,b.zjhm idCard from h_dy_dues a left join t_dy_info b on b.zjhm = a.usercard where  a.years=? and a.months=? and a.organcode=? and a.status=1 ", new Object[] {year,month,organid});
			}else {
//				db().pageMap(pager, "select a.*,1 icon ,b.zjhm idCard from h_dy_jfinfo a  left join t_dy_info b on b.userid = a.userid  where a.compid=? and a.year=? and a.month=? and a.organid=? and a.status=1 and a.username like '%"+xm+"%' and (b.dylb = '1000000001' or b.dylb ='2000000002') and (b.delflag!=1 or b.delflag is null)", new Object[] {compid,year,month,organid});
				db().pageMap(pager, "select b.userid,a.remarks,a.years year,a.months month,a.basefee,a.rate,a.payable,a.actual,a.status,a.organcode organid,a.username,1 icon,b.zjhm idCard from h_dy_dues a left join t_dy_info b on b.zjhm = a.usercard where  a.years=? and a.months=? and a.organcode=? and a.status=1 and a.username like '%"+xm+"%' ", new Object[] {year,month,organid});
			}
		}else {//党总支及以上组织
			if(StringUtils.isEmpty(xm)) {
					db().pageMap(pager, "SELECT a.*,b.dzzmc orgname,c.apstatus status,c.remark,c.apmesg FROM h_dzz_jfinfo a left join h_dzz_jfmx c on a.compid=c.compid and a.organid=c.organid and a.year=c.year and a.month=c.month left join t_dzz_info b on a.organid=b.organid  where  a.porgid=? and a.year=? and a.month=? ", new Object[] {organid,year,month});
			}else {
				db().pageMap(pager, "SELECT a.*,b.dzzmc orgname,c.apstatus status,c.remark,c.apmesg FROM h_dzz_jfinfo a left join h_dzz_jfmx c on a.compid=c.compid and a.organid=c.organid and a.year=c.year and a.month=c.month left join t_dzz_info b on a.organid=b.organid  where  a.porgid=? and a.year=? and a.month=? and b.dzzmc like '%"+xm+"%'  ", new Object[] {organid,year,month});
			}
			List<Map<String,Object>> listMap = (List<Map<String, Object>>) pager.getRows();
			if(listMap!=null && listMap.size()>0) {
				for (Map<String, Object> map : listMap) {
					Map<String,Object> map1 = db().getMap("select * from dl_file where id in (select files from h_dzz_files where organid=? and years=? and months=?)", new Object[] {map.get("organid"),year,month});
					if(map1!=null) {
						map1.put("dir", down_ip+map1.get("dir"));
					}
					map.put("file", map1);
				}
			}
			pager.setRows(listMap);
		}
		return pager;
	}


	public Map<String,Object> getBean(Session session,Integer year,Integer month){
//		String organid = session.getCustom();
		String organid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		return db().getMap("SELECT a.*,b.dzzmc orgname,c.apstatus status FROM h_dzz_jfinfo a left join h_dzz_jfmx c on a.compid=c.compid and a.organid=c.organid and a.year=c.year and a.month=c.month left join t_dzz_info b on a.organid=b.organid where a.porgid=? and a.year=? and a.month=? ", new Object[] {organid,year,month});
	}

	/**
	 * Description: 上报党费列表头部汇总
	 * @author PT
	 * @date 2019年8月23日
	 * @param month
	 * @param year
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> reportTop(Session session, String month, String year) {
		String organid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Long compid = session.getCompid();
		Map<String,Object> map = db().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?", new Object[] {compid,organid,year,month});
		//查询当前组织的上级组织机构id
		Map<String,Object> map2 = db().getMap("select parentid from t_dzz_info where organid=?", new Object[] {organid});
		String porgid = map2.get("parentid").toString();
		//关于应付党费,实付党费,自留费用,上缴比例 的算法需要根据具体的机构类型来决定
		//规则：
		//    上交比例 查询 pw_organ remark2 字段
		//    上交比例 查询 urc_organization remark2 字段
		//    应付党费=应收党费*上交比例
		//    实付党费=实收党费*上交比例
		//    自留费用=实收党费*(1-上交比例)
		//自留基数  党支部就是实收党费,
		//          总党支就是所有直属下级机构的自留基数总和,
		//          以此类推党委就是所有直属下级的自留基数总和
//		Map<String,Object> map3 = db().getMap("select remarks2,otid from pw_organ where compid=? and id=?", new Object[] {compid,session.getCurrorganid()});
		Map<String,Object> map3 = db().getMap("select remarks2,orgtypeid zzlb from urc_organization where compid=? and orgid =?", new Object[] {compid,session.getCurrentOrg().getOrgid()});
		//上交比例
		Double rate = Double.valueOf(map3.get("remarks2").toString());
		//统计该机构下党员应交党费,实交党费总和
		//应收总和
		Double receivable = 0.0;
		//实收总和
		Double actualin =  0.0;
		//自留基数
		Double retainbase = 0.0;
		//其他费用
		Double otherfee = 0.0;
		//应收组织数或用户数
		int recenum = 0;
		//实收组织数或用户数
		int actualnum = 0;
		 DecimalFormat df=new DecimalFormat("0.00");
//		if(dangzhibu.equals(map3.get("otid").toString()) || lianhedangzhibu.equals(map3.get("otid").toString())) {//党支部
		if(dangzhibu.equals(map3.get("zzlb").toString()) || lianhedangzhibu.equals(map3.get("zzlb").toString())) {//党支部
//			Map<String,Object> sum =db().getMap("select sum(a.payable) payable,sum(a.actual) actual from h_dy_jfinfo   a left join t_dy_info b on b.userid= a.userid where a.compid=? and a.year=? and a.month=? and a.organid=? and a.status=1 and (b.delflag != 1 or b.delflag = null)", new Object[] {compid,year,month,organid});
			Map<String,Object> sum =db().getMap("select sum(a.payable) payable,sum(a.actual) actual from h_dy_dues a left join t_dy_info b on b.zjhm = a.usercard  where  a.years=? and a.months=? and a.organcode=? and a.status=1", new Object[] {year,month,organid});
			if(map!=null) {
				otherfee=Double.valueOf(map.get("otherfee").toString());
			}
			if(sum!=null) {
				if(sum.get("payable")!=null) {
					receivable = Double.valueOf(df.format(sum.get("payable")));
				}
				if(sum.get("actual")!=null) {
					actualin =  Double.valueOf(df.format(sum.get("actual")));
				}
			}
			retainbase = actualin;
			Map<String,Object> map6 = db().getMap("select count(*) usernum from t_dy_info where organid=? and (dylb=? or dylb=?)", new Object[] {organid,zhengshidangyuan,yubeidangyuan});
			if(map6!=null) {
				recenum = Integer.valueOf(map6.get("usernum").toString());
			}
			Map<String,Object> map8 = db().getMap("select count(*) usernum from h_dy_jfinfo where compid=? and organid=? and actual>0", new Object[] {compid,organid});
			if(map8!=null) {
				actualnum = Integer.valueOf(map8.get("usernum").toString());
			}
		}else {//其他上级组织
			//Map<String,Object> sum =db().getMap("select sum(payable) payable,sum(actualout) actual from h_dzz_jfinfo  where compid=? and year=? and month=? and porgid=?", new Object[] {compid,year,month,organid});
			//应上报党费
			Map<String,Object> sum =db().getMap("select sum(receivable) receivable,sum(otherfee) otherfee from h_dzz_jfmx  where compid=? and year=? and month=? and porgid=?", new Object[] {compid,year,month,organid});
			//实收党费要审核通过后才可以
			Map<String,Object> sum1 =db().getMap("select sum(actualout) actual from h_dzz_jfmx  where compid=? and year=? and month=? and porgid=? and apstatus=2", new Object[] {compid,year,month,organid});
			if(sum!=null) {
				if(sum.get("receivable")!=null) {
					receivable = Double.valueOf(df.format(sum.get("receivable")));
					otherfee=Double.valueOf(sum.get("otherfee").toString());
				}
			}
			if(sum1!=null) {
				if(sum1.get("actual")!=null) {
					actualin =  Double.valueOf(df.format(sum1.get("actual")));
				}
			}
//			Map<String,Object> map4 = db().getMap("SELECT group_concat('''',organid,'''') organids FROM t_dzz_info where parentid=?", new Object[] {organid});
//			Map<String,Object> map5 = db().getMap("SELECT sum(retainbase) retainbase FROM h_dzz_jfmx where organid in ("+map4.get("organids")+")", new Object[] {});
			Map<String,Object> map5 = db().getMap("SELECT sum(retainbase) retainbase FROM h_dzz_jfmx where organid in (SELECT group_concat('''',organid,'''') organids FROM t_dzz_info where parentid=?)", new Object[] {organid});
			System.out.println(receivable+"********************************************************"+organid);
			if(map5!=null) {
				if(map5.get("retainbase")!=null) {
					retainbase = Double.valueOf(df.format(map5.get("retainbase")));
				}
			}

			Map<String,Object> map7 = db().getMap("SELECT count(*) orgnum FROM t_dzz_info where parentid=?", new Object[] {organid});
			if(map7!=null) {
				if(map7.get("orgnum")!=null) {
					recenum = Integer.valueOf(map7.get("orgnum").toString());
				}
			}

			Map<String,Object> map9 = db().getMap("SELECT count(*) orgnum FROM h_dzz_jfinfo where compid=? and porgid=?", new Object[] {compid,organid});
			if(map!=null) {
				if(map9.get("orgnum")!=null) {
					actualnum = Integer.valueOf(map9.get("orgnum").toString());
				}
			}
		}
		if(map==null) {//没有查询到就要新增
			Map<String,Object> dataMap = insertHDzzJfmx(month, year, organid, compid, receivable, actualin, porgid, rate.toString(), 0.0, 0.0, 0.0,
					retainbase, recenum, actualnum,Constant.ENPTY,0,Constant.ENPTY,0D);
			return dataMap;
		}
		Double ratenew = Double.valueOf(map.get("rate").toString());
		//应付党费
		Double payable = receivable;
		//实付党费
//		Double actualout = Double.valueOf(map.get("actualout").toString());
		//新要求  实上报党费等于实收党费
		Double actualout = actualin;
		//自留费用
		Double retain = Double.valueOf(map.get("retain").toString());
		String remarks = "";
		if(map.get("remark")!=null) {
			remarks = map.get("remark").toString();
		}

		Map<String,Object> dataMap = updateHDzzJfmx(month, year, organid, compid, receivable, actualin, porgid, ratenew.toString(), payable, actualout, retain,
				retainbase, recenum, actualnum,remarks,Integer.valueOf(map.get("apstatus").toString()),map.get("apmesg").toString(),otherfee);

		Map<String,Object> map1 = db().getMap("select * from dl_file where id in (select files from h_dzz_files where organid=? and years=? and months=?)", new Object[] {organid,year,month});
		if(map1!=null) {
			map1.put("dir", down_ip+map1.get("dir"));
		}

		dataMap.put("file", map1);
	    return dataMap;
	}


	/**
	*organid	         组织ID
	*year	         年份
	*month	         月份
	*porgid	        上级组织ID
	*receivable 应收党费
	*actualin   实收党费
	*otherfee   实收其他费用
	*payable	         应付党费=应收党费*上交比例
	*actualout  实付党费=实收党费*上交比例
	*retainbase 自留基数:党支部就是实收党费, 总党支就是所有直属下级机构的自留基数总和,以此类推党委就是所有直属下级的自留基数总和
	*retain	         自留费用=实收党费*(1-上交比例)
//	*rate	         上交比例 查询 pw_organ remark2 字段
//	*rate	         上交比例 查询 urc_organization remark2 字段
	*remark	         备注
	*apstatus   审批状态(0:待上报,1:审批中,2:上级已审批通过,3:上级已退回,4:集团审批完成)
	*apmesg	         审批意见
	*recenum	         应收组织数或用户数
	*actualnum  实收组织数或用户数
	*compid	        租户id
	*/
	private Map<String,Object> insertHDzzJfmx(String month, String year, String organid, Long compid, Double receivable,
			Double actualin, String porgid, String rate, Double payable, Double actualout, Double retain,
			Double retainbase, int recenum, int actualnum,String remark,int apstatus,String apmesg,Double otherfee) {
		Map<String, Object> paramMap = paramMap(month, year, organid, compid, receivable, actualin, porgid, rate,
				payable, actualout, retain, retainbase, recenum, actualnum,remark,apstatus,apmesg,otherfee);
		db().insert("INSERT INTO h_dzz_jfmx (organid, year, month, porgid, receivable, actualin, otherfee, payable, actualout, retainbase, retain, rate, remark, apstatus, apmesg, recenum, actualnum, compid) VALUES (:organid,:year,:month,:porgid,:receivable,:actualin,:otherfee,:payable,:actualout,:retainbase,:retain,:rate,:remark,:apstatus,:apmesg,:recenum,:actualnum,:compid)", paramMap);
		return paramMap;
	}

	private Map<String, Object> paramMap(String month, String year, String organid, Long compid, Double receivable,
			Double actualin, String porgid, String rate, Double payable, Double actualout, Double retain,
			Double retainbase, int recenum, int actualnum,String remark,int apstatus,String apmesg,Double otherfee) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("organid", organid);
		paramMap.put("year", year);
		paramMap.put("month", month);
		paramMap.put("porgid", porgid);
		paramMap.put("receivable", receivable);
		paramMap.put("actualin", actualin);
		paramMap.put("otherfee", otherfee);
		paramMap.put("payable", payable);
		paramMap.put("actualout", actualout);
		paramMap.put("retainbase", retainbase);
		paramMap.put("retain", retain);
		paramMap.put("rate", rate);
		paramMap.put("remark", remark);
		paramMap.put("apstatus", apstatus);
		paramMap.put("apmesg", apmesg);
		paramMap.put("recenum", recenum);
		paramMap.put("actualnum", actualnum);
		paramMap.put("compid", compid);
		return paramMap;
	}


	private Map<String,Object> updateHDzzJfmx(String month, String year, String organid, Long compid, Double receivable,
			Double actualin, String porgid, String rate, Double payable, Double actualout, Double retain,
			Double retainbase, int recenum, int actualnum,String remark,int apstatus,String apmesg,Double otherfee) {
		Map<String, Object> paramMap = paramMap(month, year, organid, compid, receivable, actualin, porgid, rate,
				payable, actualout, retain, retainbase, recenum, actualnum,remark,apstatus,apmesg,otherfee);
		db().update("UPDATE h_dzz_jfmx SET porgid =:porgid, receivable =:receivable, actualin = :actualin, otherfee = :otherfee, payable = :payable, actualout = :actualout, retainbase = :retainbase, retain = :retain, rate = :rate, remark = :remark, apstatus = :apstatus, apmesg = :apmesg, recenum = :recenum, actualnum = :actualnum WHERE organid = :organid and year = :year and month = :month and compid = :compid", paramMap);
		return paramMap;
	}


	/**
	 * Description: 党支部更新每个党员的实交党费   TODO 后续这里要将每个党员缴纳钱数存入统计表中以便统计
	 * @author PT
	 * @date 2019年8月23日
	 * @param month
	 * @param year
	 * @param actual
	 * @param peisonid
	 * @param payable 应交党费
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public void modiActual(Session session, String month, String year, String actual, String peisonid,String remark,String payable) {
        //获取生份证号
        Map<String,Object> user = db().getMap("select * from t_dy_info where userid =?",new Object[]{peisonid});
        if(user==null){
            LehandException.throwException("该党员不存在！！！");
        }
        String zjhm = String.valueOf(user.get("zjhm"));
        Map<String,Object> map1 = db().getMap("select * from h_dy_dues where usercard=? and years=? and months=?",
				new Object[] {zjhm,year,month});
        if(map1==null || map1.get("basefee")==null) {
            LehandException.throwException("缴纳管理记录不存在！！！");
        }
		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		Map<String, Object> hDyDues = dfglGzjfHandle.getHDyDues(year, month, String.valueOf(user.get("organid")),zjhm);
		if(hDyDues!=null&&hDyDues.size()>0){
			int push = (int) hDyDues.get("push");
			if(1==push){
				LehandException.throwException("已经推送到人力系统,不能修改！！");
			}
		}
		//更新前先查询是否已经有了实交的钱
		Double actual2 = map1.get("actual")==null?0:Double.valueOf(map1.get("actual").toString());
		db().update("update h_dy_jfinfo set actual=?,remark=?,payable=? where compid=? and userid=? and year=? and month=?", new Object[] {actual,remark,payable,session.getCompid(),peisonid,year,month});
		//更新完成后还需要更新h_dzz_fundfee中的自留党费和下拨党费总额 这里更新自留党费总额
//		Map<String,Object> map = db().getMap("select * from h_dzz_fundfee where  organid=?", new Object[] {session.getCustom()});
		Map<String,Object> map = db().getMap("select * from h_dzz_fundfee where  organid=?", new Object[] {organid});
		if(map==null) {//没有查询到
//			db().insert("insert into h_dzz_fundfee (organid,retain,allocate) values (?,?,?)", new Object[] {session.getCustom(),actual,0});
			db().insert("insert into h_dzz_fundfee (organid,retain,allocate) values (?,?,?)", new Object[] {organid,actual,0});
		}else {
			Double retain = Double.valueOf(map.get("retain").toString());
			Double actual1 = Double.valueOf(actual);
			DecimalFormat df=new DecimalFormat("0.00");
			String newretain = df.format(retain-actual2+actual1);//直接点确定时原实交的钱为0元,点击修改在保存时原实交的钱就已经有了所以给总金额更新差额就可以了
//			db().update("update h_dzz_fundfee set retain=? where organid=?",new Object[] {newretain,session.getCustom()});
			db().update("update h_dzz_fundfee set retain=? where organid=?",new Object[] {newretain,organid});

		}

		dfglGzjfHandle.updateHDyDues2(year,Integer.valueOf(month),organid,user.get("zjhm").toString(),Double.valueOf(actual),remark);
	}


	/**
	 * Description: 党支部上报党费  TODO 后续这里要将每个党员缴纳钱数存入统计表中以便统计
	 * @author PT
	 * @date 2019年8月23日
	 * @param month
	 * @param year
	 * @param otherfee
	 * @param remark
	 * @param actualout 实上报党费（党支部和党总支不可以更改只有党委及以上可以修改金额）
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public void report(Session session, String month, String year, Double otherfee, String remark, String actualout, String file) {

		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		//首先判断上报是否存在附件
		if(!StringUtils.isEmpty(file)) {//存在
			//先删除旧的附件
//			db().delete("delete from h_dzz_files where organid=? and years=? and months=?", new Object[] {session.getCustom(),year,month});
			db().delete("delete from h_dzz_files where organid=? and years=? and months=?", new Object[] {organid,year,month});
			//将附件存入h_dzz_files表
//			db().insert("insert into h_dzz_files (organid,years,months,files) values (?,?,?,?)", new Object[] {session.getCustom(),year,month,file});
			db().insert("insert into h_dzz_files (organid,years,months,files) values (?,?,?,?)", new Object[] {organid,year,month,file});
		}else {//不存在
			//删除旧的附件
			db().delete("delete from h_dzz_files where organid=? and years=? and months=?", new Object[] {organid,year,month});
			db().delete("delete from h_dzz_files where organid=? and years=? and months=?", new Object[] {organid,year,month});
		}
		//更新汇总表h_dzz_jfmx
//		String organid = session.getCustom();
		Long compid = session.getCompid();
		if(otherfee==null) {
			otherfee = 0.0;
		}
		Map<String, Object> map = db().getMap("select * from h_dzz_jfmx where organid=? and year=? and month=? and compid=?", new Object[] {organid,year,month,compid});
		Double shishou = Double.valueOf(map.get("actualin").toString());
		//Double yingfu =  Double.valueOf(map.get("payable").toString());
		//Double selffee = Double.valueOf(map.get("retain").toString());
		if(dangzhibu.equals(session.getOrganCode()) || lianhedangzhibu.equals(session.getOrganCode()) || dangzongzhi.equals(session.getOrganCode())) {//没有修改实付金额
			db().update("UPDATE h_dzz_jfmx SET otherfee = ?, remark = ? , apstatus=1 WHERE organid = ? and year = ? and month = ? and compid=?", new Object[] {otherfee,remark,organid,year,month,compid});
			//往h_dzz_jfinfo插入数据
			Map<String, Object> map2 = db().getMap("select * from h_dzz_jfinfo where compid=? and organid=? and year=? and month=?", new Object [] {compid,organid,year,month});
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("organid", organid);
			paramMap.put("year", year);
			paramMap.put("month", month);
			paramMap.put("porgid", map.get("porgid"));
			paramMap.put("receivable", map.get("receivable"));
			paramMap.put("actualin", map.get("actualin"));
			paramMap.put("otherfee", otherfee);
			paramMap.put("payable", map.get("payable"));
			paramMap.put("actualout", actualout);
			paramMap.put("retain", 0);
			paramMap.put("retainbase", map.get("retainbase"));
			paramMap.put("allocate", 0);
			paramMap.put("wkfunds", DateEnum.YYYYMMDDHHMMDD.format());
			paramMap.put("compid", compid);
			if(map2==null) {
				insertHDzzJfinfo(paramMap);

				String itemname = session.getCurrentOrg().getOrgname() + "上报党费（" + month + "月）";
				String bizid = organid + "," + year + "," + month;
				addTodoItem(Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES, itemname, bizid, Todo.FunctionURL.PARTY_DUES.getPath(), session);
			}else {
				updateHDzzJfinfo(paramMap);
				String itemname = session.getCurrentOrg().getOrgname() + "上报党费（" + month + "月）";
				String bizid = organid + "," + year + "," + month;
				addTodoItem(Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES, itemname, bizid, Todo.FunctionURL.PARTY_DUES.getPath(), session);
			}
		}else {//修改了实付金额（重新计算上交比例）
			DecimalFormat df=new DecimalFormat("0.00");
			Double rate =shishou==0?0:Double.valueOf(df.format(100* Double.valueOf(actualout)/shishou));
			Double retain = Double.valueOf(df.format(shishou-Double.valueOf(actualout)));
			db().update("UPDATE h_dzz_jfmx SET otherfee = ?, remark = ?,actualout=?,retain=?,rate=?, apstatus=1 WHERE organid = ? and year = ? and month = ? and compid=?", new Object[] {otherfee,remark,actualout,retain,rate,organid,year,month,compid});
			if(retain<0) {
				LehandException.throwException("实际上报值不可以大于实际收缴值");
			}
//			else if(retain>0) {//存在自留党费
//				//更新完h_dzz_jfmx还需要更新h_dzz_fundfee表中的selefee自留费用
//				Map<String,Object> pmap = db().getMap("select * from h_dzz_fundfee where  organid=?",new Object[] {session.getCustom()});
//				if(pmap==null) {//没有查询到
//					db().insert("insert into h_dzz_fundfee (organid,selffee) values (?,?)", new Object[] {session.getCustom(),retain});
//				}else {
//					Double pretain = Double.valueOf(pmap.get("retain").toString());
//					String pnewretain = df.format(pretain-selffee+retain);
//					db().update("update h_dzz_fundfee set retain=? where organid=?",new Object[] {pnewretain,session.getCustom()});
//				}
//			}
			//往h_dzz_jfinfo插入数据
			Map<String, Object> map2 = db().getMap("select * from h_dzz_jfinfo where compid=? and organid=? and year=? and month=?", new Object [] {compid,organid,year,month});
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("organid", organid);
			paramMap.put("year", year);
			paramMap.put("month", month);
			paramMap.put("porgid", map.get("porgid"));
			paramMap.put("receivable", map.get("receivable"));
			paramMap.put("actualin", map.get("actualin"));
			paramMap.put("otherfee", otherfee);
			paramMap.put("payable", map.get("payable"));
			paramMap.put("actualout", actualout);
			paramMap.put("retain", retain);
			paramMap.put("retainbase", map.get("retainbase"));
			paramMap.put("allocate", 0);
			paramMap.put("wkfunds", DateEnum.YYYYMMDDHHMMDD.format());
			paramMap.put("compid", compid);
			if(map2==null) {
				insertHDzzJfinfo(paramMap);

				String itemname = session.getCurrentOrg().getOrgname() + "上报党费（" + month + "月）";
				String bizid = organid + "," + year + "," + month;
				addTodoItem(Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES, itemname, bizid, Todo.FunctionURL.PARTY_DUES.getPath(), session);
			}else {
				updateHDzzJfinfo(paramMap);
				String itemname = session.getCurrentOrg().getOrgname() + "上报党费（" + month + "月）";
				String bizid = organid + "," + year + "," + month;
				addTodoItem(Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES, itemname, bizid, Todo.FunctionURL.PARTY_DUES.getPath(), session);
			}
		}
	}



	private void insertHDzzJfinfo(Map<String, Object> paramMap) {
		db().insert("INSERT INTO h_dzz_jfinfo (organid, year, month, porgid, receivable, actualin, otherfee, payable, actualout, retain, retainbase, allocate, wkfunds, compid) VALUES (:organid,:year, :month,:porgid,:receivable,:actualin,:otherfee,:payable,:actualout,:retain,:retainbase,:allocate,:wkfunds,:compid)", paramMap);
	}

	private void updateHDzzJfinfo(Map<String, Object> paramMap) {
		db().insert("update h_dzz_jfinfo set porgid=:porgid, receivable=:receivable, actualin=:actualin, otherfee=:otherfee, payable=:payable, actualout=:actualout, retain=:retain, retainbase=:retainbase, allocate=:allocate, wkfunds=:wkfunds where compid=:compid and organid=:organid and year=:year and month=:month", paramMap);
	}

	/**
	 * Description: 党总支以及上级上报党费页面内初始化数据
	 * @author PT
	 * @date 2019年8月26日
	 * @param
	 */
	@Transactional(rollbackFor = Exception.class)
	public void initialization(String organid,String year,String month) {
//		Long compid = session.getCompid();
//		String organid = session.getCustom();
//		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		//查询当前组织的上级组织机构id
		List<Map<String,Object>> lists = db().listMap("select * from t_dzz_info where parentid=? and (delflag !=1 or delflag is null) ", new Object[] {organid});
		if(lists!=null && lists.size()>0) {
			for (Map<String, Object> map : lists) {
				String childorganid = map.get("organid").toString();
				Map<String, Object> map2 = db().getMap("select * from h_dzz_jfinfo where compid=? and organid=? and year=? and month=?", new Object [] {1,childorganid,year,month});
				if(map2==null) {
					Map<String, Object> paramMap = new HashMap<String, Object>();
					paramMap.put("organid", childorganid);
					paramMap.put("year", year);
					paramMap.put("month", month);
					paramMap.put("porgid", organid);
					paramMap.put("receivable", 0);
					paramMap.put("actualin", 0);
					paramMap.put("otherfee", 0);
					paramMap.put("payable", 0);
					paramMap.put("actualout", 0);
					paramMap.put("retain", 0);
					paramMap.put("retainbase", 0);
					paramMap.put("allocate", 0);
					paramMap.put("wkfunds", "");
					paramMap.put("compid", 1);
					insertHDzzJfinfo(paramMap);
				}
				Map<String, Object> map3 = db().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?", new Object [] {1,childorganid,year,month});
				if(map3==null) {
					insertHDzzJfmx(month, year, childorganid, 1l, 0.0, 0.0, organid, "0", 0.0, 0.0, 0.0, 0.0, 0, 0,Constant.ENPTY,0,Constant.ENPTY,0D);
				}
			}
		}
	}


	/**
	 * Description: 上报党费审核
	 * @author PT
	 * @date 2019年8月26日
	 * @param month
	 * @param year
	 * @param organid 机构id
	 * @param remark 备注
	 * @param status 状态 2通过 3驳回
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public void audit(Session session, String month, String year, String organid, String remark, int status) {
		String currentOrganid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		Map<String,Object> map = db().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?",
				new Object[] {session.getCompid(),organid,year,month});
		int apstatus = Integer.valueOf(map.get("apstatus").toString());
		db().update("update h_dzz_jfmx set apmesg=?,apstatus=? where compid=? and organid=? and year=? and month=?", new Object[] {remark,status,session.getCompid(),organid,year,month});

		if(status==2) {//审核通过
			//更新上报组织的余额,以及更新上报组织的上级组织的余额
			updateRetain(session,month,year,map.get("actualout").toString(),organid);
		}else if(apstatus==2 && status==3) {//第一次通过后面由于上级驳回导致这里也要驳回,驳回就意味着重新上报,那么就得将该机构的上报的钱退回该机构
			Double actualout = Double.valueOf(map.get("actualout").toString());//实际上报的钱
			DecimalFormat df=new DecimalFormat("0.00");
			Map<String,Object> map2 = db().getMap("select * from h_dzz_fundfee where organid=?", new Object[] {organid});
			//被退回的组织余额增加
			String jiajine = df.format(Double.valueOf(map2.get("retain").toString())+actualout);
			db().update("update h_dzz_fundfee set retain=? where organid=?", new Object[] {jiajine,organid});
			Map<String,Object> map3 = db().getMap("select * from h_dzz_fundfee where organid=?", new Object[] {currentOrganid});
			//当前组织余额减少
			String jiianjine = df.format(Double.valueOf(map3.get("retain").toString())-actualout);
			db().update("update h_dzz_fundfee set retain=? where organid=?", new Object[] {jiianjine,currentOrganid});
			//当前组织收缴的钱减少(即将被驳回组织的上报的相关钱都清空恢复初始化未上报状态)
			db().update("update h_dzz_jfmx set receivable=0,actualin=0,otherfee=0,payable=0,actualout=0,retainbase=0 where organid=?", new Object[] {organid});
		}
		String bizid = organid + "," + year + "," + month;
		removeTodoItem(Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES, bizid, session);
	}

	/**
	 * Description: 更新上报组织的余额,以及更新上报组织的上级组织的余额 (审核通过后进行扣款操作)
	 * @author PT
	 * @date 2019年9月5日
	 * @param session
	 * @param month
	 * @param year
	 * @param actualout TODO
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateRetain(Session session, String month, String year, String actualout, String organid) {

		String currentOrganid = sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());

		Map<String,Object> map = db().getMap("select * from h_dzz_fundfee where  organid=? ",new Object[] {organid});
		//上报组织的余额减去上报金额
		Double retain = Double.valueOf(map.get("retain").toString());
		Double actual1 = Double.valueOf(actualout);
		DecimalFormat df=new DecimalFormat("0.00");
		String newretain = df.format(retain-actual1);
		db().update("update h_dzz_fundfee set retain=? where organid=?",new Object[] {newretain,organid});
		Map<String,Object> pmap = db().getMap("select * from h_dzz_fundfee where  organid=?",new Object[] {currentOrganid});
		if(pmap==null) {//没有查询到
			db().insert("insert into h_dzz_fundfee (organid,retain,allocate) values (?,?,?)", new Object[] {currentOrganid,actualout,0});
		}else {
			//上报组织的上级组织的余额加上上报的金额
			Double pretain = Double.valueOf(pmap.get("retain").toString());
			Double pactual1 = Double.valueOf(actualout);
			String pnewretain = df.format(pretain+pactual1);
			db().update("update h_dzz_fundfee set retain=? where organid=?",new Object[] {pnewretain,currentOrganid});
		}
	}




	public List<Map<String, Object>> list(Session session) {
		return db().listMap("select * from t_dfgl_jssz where compid=? order by min asc", new Object[] {session.getCompid()});
	}

	/**
	 * Description: 党支部上报党费时根据上报实收金额更新h_dzz_jfmx的实收党费和实付党费
	 * @author PT
	 * @date 2019年9月2日
	 * @param session
	 * @param month
	 * @param year
	 * @param actualout
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateActualout(Session session, String month, String year, String actualout) {
		db().update("update h_dzz_jfmx set actualin=? , actualout=? , retainbase=? where compid=? and organid=? and year=? and month=?",
				new Object[] {actualout,actualout,actualout,session.getCompid(),sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode()),year,month});
	}

	/**
	 * 根据组织机构编码获取改组织党费管理党费基数授权状态
	 * @param orgcode
	 * @param session
	 * @return
	 */
	public String getGrandStatusByOrgCode(String orgcode, Session session) {
		if(StringUtils.isEmpty(orgcode)) {
			LehandException.throwException("参数不能为空");
		}
//		String sqsave = db().getSingleColumn("SELECT IFNULL(remarks4,0)remarks4 FROM `pw_organ` where orgcode = ?  and compid = ?", String.class, new Object[] {orgcode,session.getCompid()});
		String sqsave = db().getSingleColumn("SELECT IFNULL(remarks4,0)remarks4 FROM `urc_organization` where orgcode = ?  and compid = ?", String.class, new Object[] {orgcode,session.getCompid()});
		return sqsave;
	}


	public String exportJsModel(HttpServletResponse response, String orgid,Session session){
		List<TDyInfo> list  = generalSqlComponent.query(SqlCode.getTDyInfoList,new Object[]{zhengshidangyuan,yubeidangyuan,orgid});
		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("姓名");
		tables.add("身份证号");
		tables.add("缴费基数");
		List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(TDyInfo li:list){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("姓名",li.getXm());
			tempMap.put("身份证号",li.getZjhm());
			tempMap.put("缴费基数","");
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "缴费基数导入";
		ExcelUtils.write(response,tablesStr,explist,tableName);
		return null;
	}

	public String exportJs(HttpServletResponse response, String orgid,Session session){
		//年份
		String years = DateEnum.YYYY.format();
		//月份
		int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
		List<Map<String,Object>> list  = generalSqlComponent.query(SqlCode.getdfjsdc,new Object[]{orgid,zhengshidangyuan,yubeidangyuan});

		exportFiles(response, list);
		return null;
	}

	private void exportFiles(HttpServletResponse response, List<Map<String, Object>> list) {
		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("姓名");
		tables.add("身份证号");
		tables.add("缴费基数");
		List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(Map<String,Object> li: list){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("姓名",li.get("xm").toString());
			tempMap.put("身份证号",li.get("zjhm").toString());
			tempMap.put("缴费基数",StringUtils.isEmpty(li.get("basefee"))?"":li.get("basefee").toString());
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "缴费基数";
		ExcelUtils.write(response,tablesStr,explist,tableName);
	}


	private int getValidate(int i, String idcard, String xm, StringBuffer buffer) {
		Integer num = 0;
		//判断导入的姓名是否为空
		if(StringUtils.isEmpty(xm)){
			buffer.append("第"+i+"条数据姓名为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		//判断导入的idcard是否为空
		if(StringUtils.isEmpty(idcard)){
			buffer.append("第"+i+"条数据身份证号码为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		return num;
	}

	private TDyInfo gettDyInfo(String idcard, Session session,String orgid) {
		TDyInfo dy = generalSqlComponent.query(SqlCode.getTDyInfoZjhm,new Object[]{orgid, idcard});
		return dy;
	}

	private TDyInfo gettDyInfo2(String idcard, Session session,String orgid) {
		TDyInfo dy = generalSqlComponent.query(SqlCode.getTDyInfoZjhm2,new Object[]{idcard,orgid});
		return dy;
	}

	@Transactional(rollbackFor = Exception.class)
	public Message importJs(HttpServletResponse response,MultipartFile file, Session session, Message message,String orgid,Integer months) throws Exception {
		{
			if(StringUtils.isEmpty(orgid)){
				LehandException.throwException("组织编码id不能为空！！");
			}

			//年份
			String years = DateEnum.YYYY.format();
			//月份
//			int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
			//验证当前机构有没有一提交党费
			List<Map<String,Object>> importD = generalSqlComponent.query(SqlCode.getDfjjDr,new Object[]{orgid, years,months});
			if(importD.size()>0){
				LehandException.throwException("本月党费已提交");
				return null;
			}
			InputStream inputStream = file.getInputStream();
			List<List<String>> list = ExcelUtils.readExcelNew(file.getOriginalFilename(), inputStream,NumberUtils.num2);
			List<Map<String,String>> listNo = new ArrayList<Map<String,String>>();
			if (list.size()<1){
				LehandException.throwException("无数据");
				return null;
			}
			int total = list.size();
			int  insertnum = 0; //插入数
			int  updatenum = 0; //修改数
			StringBuffer buffer = new StringBuffer("导入结果: ");
			buffer.append("\r\n");

			int num = 0;
			int num1 = 0;
			Map<String,String> dataNo = null;

			for (int i = 0; i < total ; i++) {
				int h = 0;int j = 0;
				if(list.get(0).size()>list.get(i).size()){
					num++;
					buffer.append("第"+(i+1)+"条数据不完整;");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",list.get(i).get(1));
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("错误原因","数据不完整");
					listNo.add(dataNo);
					continue;
				}
				//用户名称
				String username = list.get(i).get(0);
				//用户身份证号
				String usercard  = list.get(i).get(1);

				//校验导入数据
//				num1 = getValidate(i+1,usercard,username,buffer);

				if(getValidate(i+1,usercard,username,buffer)>0){
					num++;
					continue;
				}
				TDyInfo dy = gettDyInfo(usercard, session,orgid);
				if(dy==null){
					buffer.append("第"+(i+1)+"条数据不存在当前党组织");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",usercard);
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("错误原因","不存在当前党组织");
					listNo.add(dataNo);
					num++;
					continue;
				}

				TDyInfo dy2 = gettDyInfo2(usercard, session,orgid);
				if(dy2==null){
					buffer.append("第"+(i+1)+"条数据不是正式党员或预备党员");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",usercard);
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("错误原因","不是正式党员或预备党员");
					listNo.add(dataNo);
					num++;
					continue;
				}
				Map<String, Object> hDyDues = dfglGzjfHandle.getHDyDues(years, String.valueOf(months), orgid, list.get(i).get(1));
				if(hDyDues!=null&&hDyDues.size()>0){
					int push = (int) hDyDues.get("push");
					if(1==push){
						buffer.append("第"+(i+1)+"条数据已经推送到人力系统");
						buffer.append("\r\n");
						dataNo = new HashMap<String,String>();
						dataNo.put("姓名",list.get(i).get(0));
						dataNo.put("身份证号",usercard);
						dataNo.put("缴费基数",list.get(i).get(2));
						dataNo.put("错误原因","已经推送到人力系统");
						listNo.add(dataNo);
						num++;
						continue;
					}
				}

				//查询数据是否已经插入
				Map jfjs = getJfjs(years,months,usercard,session,orgid);
				if(jfjs==null){//插入
					try {
						//删除个人党费明细
						dfglGzjfHandle.deleteHDyDues2(years,months,orgid,list.get(i).get(1));

						insertHDyJfjs(years,months,list.get(i),session,orgid,false);
					} catch (Exception e) {
						LehandException.throwException("导入失败，请检查表格");
					}

					insertnum += 1;
				}else{
					//是否是人力资源推送
					long synchronous = (long) jfjs.get("synchronous");
					if(1==synchronous){
						buffer.append("第"+(i+1)+"条数据是人力资源推送");
						buffer.append("\r\n");
						dataNo = new HashMap<String,String>();
						dataNo = new HashMap<String,String>();
						dataNo.put("姓名",list.get(i).get(0));
						dataNo.put("身份证号",usercard);
						dataNo.put("缴费基数",list.get(i).get(2));
						dataNo.put("错误原因","数据是人力资源推送的,不可修改");
						listNo.add(dataNo);
						num++;
						continue;
					}
					try {
						//删除个人党费明细
						dfglGzjfHandle.deleteHDyDues2(years,months,orgid,list.get(i).get(1));

						updateHDyJfjs(years,months,list.get(i),session,orgid,false);
					} catch (Exception e) {
						LehandException.throwException("导入失败，请检查表格");
					}
					updatenum += 1;
				}

			}
			buffer.append("共"+(total)+"条数据,共导入成功"+(insertnum+updatenum)+"条数据;");
			if(num>0){
				message.fail("999999",buffer.toString()+"失败");
			}else{
				message.success();
			}
			if(listNo!=null&&listNo.size()>0){
				List<String> tables = new ArrayList<String>();
				tables.add("姓名");
				tables.add("身份证号");
				tables.add("缴费基数");
				tables.add("错误原因");
				List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
//				for(Map<String,String> li: listNo){
//					Map<String, String> tempMap = new HashMap<>();
//					tempMap.put("姓名",li.get("xm"));
//					tempMap.put("身份证号",li.get("zjhm"));
//					tempMap.put("缴费基数",li.get("basefee"));
//					tempMap.put("错误原因",li.get("errorcontent"));
//				}
				String[] tablesStr = tables.toArray(new String[tables.size()]);
				//表名
				String tableName = "缴费基数";
				File file2 = ExcelUtils.write2(tablesStr,listNo,tableName,location);
				//Map<String,Object> data  = documentManagerBusiness.uploadFile(file2,session);
				file2.delete();
				//message.setData(data.get("dir"));
			}
			return message;
		}
	}

	public Map<String,Object> getJfjs(String years,Integer months,String usercard,Session session,String orgid){
		return generalSqlComponent.query(SqlCode.getHDyJfjs,new Object[]{orgid,usercard});
	}

	public void insertHDyJfjs(String years,Integer months,List<String> data,Session session,String orgid,Boolean b){
		//插入党费基数表
		TDyInfo dy = gettDyInfo(data.get(1),session,orgid);
		Map<String,Object> parms1 = new HashMap<String,Object>();
		parms1.put("userid", dy.getUserid());
		parms1.put("username", data.get(0));
		parms1.put("organid", orgid);
		parms1.put("basefee",  StringUtils.isEmpty(data.get(2))?0:data.get(2));
		parms1.put("onpost", NumberUtils.num1);
		parms1.put("years", years);
		parms1.put("months", months);
		parms1.put("compid", session.getCompid());
		generalSqlComponent.insert(SqlCode.insertDfjs,parms1);
		if(!b){
			dfglGzjfHandle.insertHDyDues2(years,months,data,orgid,Double.valueOf(StringUtils.isEmpty(data.get(2))?"0":data.get(2)));
			//插入每月缴费信息表
			hDYJfInfo(Double.valueOf(StringUtils.isEmpty(data.get(2))?"0":data.get(2)),dy.getUserid(),data.get(0),orgid,String.valueOf(NumberUtils.num1)
					,null,null,null,session.getCompid(),Integer.valueOf(years),months);
		}
	}

	public void updateHDyJfjs(String years,Integer months,List<String> data,Session session,String orgid,Boolean b) throws Exception{
		//插入党费基数表
		TDyInfo dy = gettDyInfo(data.get(1),session,orgid);
		Map<String,Object> parms1 = new HashMap<String,Object>();
		parms1.put("userid", dy.getUserid());
		parms1.put("basefee",  StringUtils.isEmpty(data.get(2))?0:data.get(2));
		parms1.put("onpost", NumberUtils.num1);
		parms1.put("compid", session.getCompid());
		generalSqlComponent.insert(SqlCode.updateDfjs,parms1);
		if(!b){
			dfglGzjfHandle.insertHDyDues2(years,months,data,orgid,Double.valueOf(StringUtils.isEmpty(data.get(2))?"0":data.get(2)));
			//插入每月缴费信息表
			hDYJfInfo(Double.valueOf(StringUtils.isEmpty(data.get(2))?"0":data.get(2)),dy.getUserid(),data.get(0),orgid,String.valueOf(NumberUtils.num1)
					,null,null,null,session.getCompid(),Integer.valueOf(years),months);
		}
//		try {
//
//		} catch (Exception e) {
//			System.out.println("******************************************************************************************");
//
//			LehandException.throwException("导入失败，请检查表格");
//		}
	}


	public String dfExport(HttpServletResponse response,String orgid,String year,String month,String xm,Session session){
		Pager pager = new Pager();
		pager.setPageNo(1);
		pager.setPageSize(1000);
		reportPage(session, orgid, month, year, xm, pager);
		List<Map<String,Object>> listMap = (List<Map<String, Object>>) pager.getRows();
		Map<String,Object> map3 = db().getMap("select * from t_dzz_info where organid=?", new Object[] {orgid});
		if(dangzhibu.equals(map3.get("zzlb").toString()) || lianhedangzhibu.equals(map3.get("zzlb").toString())) {
			dfExportZb(response, listMap);
		}else{
			dfExportZz(response, listMap);
		}

		return null;
	}

	private void dfExportZb(HttpServletResponse response, List<Map<String, Object>> listMap) {
		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("党员姓名");
		tables.add("缴费基数");
		tables.add("缴费比例");
		tables.add("所属月份");
		tables.add("应交党费(元)");
		tables.add("实交党费(元)");
		tables.add("状态");
		tables.add("备注");
		List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(Map<String,Object> li: listMap){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("党员姓名",li.get("username").toString());
			tempMap.put("缴费基数",String.valueOf(li.get("basefee")));
			tempMap.put("缴费比例",li.get("rate")==null?"":li.get("rate").toString()+"%");
			tempMap.put("所属月份",li.get("month").toString());
			tempMap.put("应交党费(元)",li.get("payable")==null?"":String.valueOf(li.get("payable")));
			boolean b = StringUtils.isEmpty(li.get("actual"));
			tempMap.put("实交党费(元)",b?"0":li.get("actual").toString());
			tempMap.put("状态",b?"未交":"已交");
			tempMap.put("备注",li.get("remark")==null?"":li.get("remark").toString());
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "党费收缴导出";
		ExcelUtils.write(response,tablesStr,explist,tableName);
	}

	private void dfExportZz(HttpServletResponse response, List<Map<String, Object>> listMap) {
		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("上报党组织");
		tables.add("应上报党费");
		tables.add("实上报党费");
		tables.add("上报月份");
		tables.add("状态");
		tables.add("备注");
		List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(Map<String,Object> li: listMap){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("上报党组织",li.get("orgname").toString());
			tempMap.put("应上报党费",li.get("payable")==null?"":li.get("payable").toString());
			tempMap.put("实上报党费",li.get("actualout")==null?"":li.get("actualout").toString());
			tempMap.put("上报月份",li.get("month").toString());
			if(li.get("status").equals(0)){
				tempMap.put("状态","待上报");
			}else if(li.get("status").equals(1)){
				tempMap.put("状态","待审核");
			}else if(li.get("status").equals(2)){
				tempMap.put("状态","已审批");
			}else if(li.get("status").equals(3)){
				tempMap.put("状态","驳回");
			}else{
				tempMap.put("状态","未知状态");
			}
			tempMap.put("备注",li.get("remark")==null?"":li.get("remark").toString());
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "党费审核导出";
		ExcelUtils.write(response,tablesStr,explist,tableName);
	}

	/**待办任务begin****************************************************************************************************************/
	@Resource
	private TodoItemService todoItemService;

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param name 事项名称
	 * @param bizid 该事项对应的业务ID
	 * @param path 资源路径
	 * @param session 当前会话
	 */
	private void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setBizid(bizid);
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
//		itemDTO.setUserid(session.getUserid());
//		itemDTO.setUsername(session.getUsername());
		itemDTO.setOrgid(session.getCurrentOrg().getPorgid());
		itemDTO.setName(name);
		itemDTO.setBizid(bizid);
		itemDTO.setTime(new Date());
		itemDTO.setPath(path);
		itemDTO.setState(Todo.STATUS_NO_READ);
		todoItemService.createTodoItem(itemDTO);
	}

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param bizid 该事项对应的业务ID
	 * @param session 当前会话
	 */
	private void removeTodoItem(Todo.ItemType itemType,  String bizid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setBizid(bizid);
		todoItemService.deleteTodoItem(itemDTO);
	}
	/**待办任务end****************************************************************************************************************/

	public List<Map<String, Object>> exportJsModelAll(HttpServletResponse response,Session session){
		List<Map<String, Object>> list = new ArrayList<>();
		getCurrAndSubListOrg(session,session.getCurrentOrg().getOrgcode(),list);
		List<TDyInfo> listTDyInfo  = new ArrayList<>();
		if(list!=null&&list.size()>0){
			for (Map<String, Object> map:list) {
				List<TDyInfo>  listInfo  = generalSqlComponent.query(SqlCode.getTDyInfoList,new Object[]{zhengshidangyuan,yubeidangyuan,map.get("organid")});
				listTDyInfo.addAll(listInfo);
			}
		}
		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("姓名");
		tables.add("身份证号");
		tables.add("缴费基数");
		tables.add("组织名称");
		tables.add("组织编码");
		List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(TDyInfo li:listTDyInfo){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("姓名",li.getXm());
			tempMap.put("身份证号",li.getZjhm());
			tempMap.put("缴费基数","");
			Map<String,Object> map = db().getMap("select * from t_dzz_info where organid=?", new Object[] {li.getOrganid()});
			tempMap.put("组织名称",map!=null?(String) map.get("dzzmc"):"");
			tempMap.put("组织编码",li.getOrganid());
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "缴费基数导入";
		ExcelUtils.write(response,tablesStr,explist,tableName);
		return null;
	}
	/**
	 * 查询当前组织和当前组织下的组织数据
	 * @param session
	 * @param organid
	 * @return
	 */
	public Object getCurrAndSubListOrg(Session session, String organid,List<Map<String, Object>> list) {

		Map<String, Object> paramMap = new HashMap<>();

		paramMap.put("organid", organid);
		//查询 id_path
		TDzzInfoSimple tDzzInfoSimple = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.queryTdzzInfoSimple, paramMap);
		//查询下级组织
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(tDzzInfoSimple.getIdPath()).append("%");
		paramMap.put("idPath", stringBuffer.toString());
		List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
		records = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.queryTdzzInfoSimpleByIdpath, paramMap);
		list.addAll(records);
		return treeUtils(tDzzInfoSimple, records);
	}

	//返回组织树节点的类别
	public Map<String, Object> getSlots(String zzlb) {
		Map<String, Object> map = new HashMap<>();
		map.put("icon", zzlb);
		return map;
	}
	/**
	 * 党组织树形化输出
	 * @param tDzzInfoSimple
	 * @param records
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> treeUtils(TDzzInfoSimple tDzzInfoSimple, List<Map<String, Object>> records) {
		Map<String, Map<String, Object>> tree = new HashMap<>();
		Map<String, Object> head = new HashMap<>();
		head.put("title", tDzzInfoSimple.getDzzmc());
		head.put("key", tDzzInfoSimple.getOrganid());
		head.put("slots", getSlots(tDzzInfoSimple.getZzlb()));
		head.put("organcode", tDzzInfoSimple.getOrgancode());
		head.put("organid", tDzzInfoSimple.getOrganid());
		head.put("parentcode", tDzzInfoSimple.getParentcode());
		head.put("children", new ArrayList<Map<String, Object>>());
		head.put("zzlb", tDzzInfoSimple.getZzlb());
		tree.put(tDzzInfoSimple.getOrgancode(), head);

		for (int i = 0, l = records.size(); i < l; i++) {
			Map<String, Object> record = records.get(i);
			String parentcode = record.get("parentcode").toString();
			record.put("children", new ArrayList<Map<String, Object>>());
			if (tree.containsKey(parentcode)) {
				List<Map<String, Object>> children = (List<Map<String, Object>>) tree.get(parentcode).get("children");
				record.put("key", record.get("organid"));
				record.put("title", record.get("dzzmc"));
				record.put("slots", getSlots(record.get("zzlb").toString()));
				children.add(record);
				tree.put(record.get("organcode").toString(), record);
			}
		}
		return tree.get(tDzzInfoSimple.getOrgancode());
	}

	@Transactional(rollbackFor = Exception.class)
	public Message importJsAll(HttpServletResponse response,MultipartFile file, Session session, Message message,Integer months) throws Exception {
		{
			//年份
			String years = DateEnum.YYYY.format();
			//月份
//			int months = Integer.valueOf(DateEnum.YYYYMMDD.format().substring(5,7));
			InputStream inputStream = file.getInputStream();
			List<List<String>> list = ExcelUtils.readExcelNew(file.getOriginalFilename(), inputStream,NumberUtils.num2);
			List<Map<String,String>> listNo = new ArrayList<Map<String,String>>();
			if (list.size()<1){
				LehandException.throwException("无数据");
				return null;
			}
			int total = list.size();
			int  insertnum = 0; //插入数
			int  updatenum = 0; //修改数
			StringBuffer buffer = new StringBuffer("导入结果: ");
			buffer.append("\r\n");

			int num = 0;
			int num1 = 0;
			Map<String,String> dataNo = null;

			for (int i = 0; i < total ; i++) {
				int h = 0;int j = 0;
				String orgName = list.get(i).get(3);
				String orgid = list.get(i).get(4);

				if(list.get(0).size()>list.get(i).size()){
					num++;
					buffer.append("第"+(i+1)+"条数据不完整;");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",list.get(i).get(1));
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("组织名称",orgName);
					dataNo.put("组织编码",orgid);
					dataNo.put("错误原因","数据不完整");
					listNo.add(dataNo);
					continue;
				}
				//用户名称
				String username = list.get(i).get(0);
				//用户身份证号
				String usercard  = list.get(i).get(1);

				//校验导入数据
//				num1 = getValidate(i+1,usercard,username,buffer);

				if(getValidateAll(i+1,usercard,username,orgid,buffer)>0){
					num++;
					continue;
				}
				//党组织是否上报党费
				Map<String,Object> hdzz = db().getMap("select * from h_dzz_jfmx where compid=? and organid=? and year=? and month=?", new Object[] {session.getCompid(),orgid,years,months});
				Boolean b = false;
				if(hdzz!=null){
					Integer apstatus = (Integer) hdzz.get("apstatus");
					if(apstatus!=0&&apstatus!=3){
						//已经上报
						b = true;
					}
				}
				if(b){
					buffer.append("第"+(i+1)+"条数据党组织已经上报党费");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",usercard);
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("组织名称",orgName);
					dataNo.put("组织编码",orgid);
					dataNo.put("错误原因","已经上报党费");
					listNo.add(dataNo);
					num++;
					continue;
				}
				TDyInfo dy = gettDyInfo(usercard, session,orgid);
				if(dy==null){
					buffer.append("第"+(i+1)+"条数据不存在当前党组织");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",usercard);
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("组织名称",orgName);
					dataNo.put("组织编码",orgid);
					dataNo.put("错误原因","不存在当前党组织");
					listNo.add(dataNo);
					num++;
					continue;
				}

				TDyInfo dy2 = gettDyInfo2(usercard, session,orgid);
				if(dy2==null){
					buffer.append("第"+(i+1)+"条数据不是正式党员或预备党员");
					buffer.append("\r\n");
					dataNo = new HashMap<String,String>();
					dataNo.put("姓名",list.get(i).get(0));
					dataNo.put("身份证号",usercard);
					dataNo.put("缴费基数",list.get(i).get(2));
					dataNo.put("组织名称",orgName);
					dataNo.put("组织编码",orgid);
					dataNo.put("错误原因","不是正式党员或预备党员");
					listNo.add(dataNo);
					num++;
					continue;
				}
				Map<String, Object> hDyDues = dfglGzjfHandle.getHDyDues(years, String.valueOf(months), orgid, list.get(i).get(1));
				if(hDyDues!=null&&hDyDues.size()>0){
					int push = (int) hDyDues.get("push");
					if(1==push){
						buffer.append("第"+(i+1)+"条数据已经推送到人力系统");
						buffer.append("\r\n");
						dataNo = new HashMap<String,String>();
						dataNo.put("姓名",list.get(i).get(0));
						dataNo.put("身份证号",usercard);
						dataNo.put("缴费基数",list.get(i).get(2));
						dataNo.put("错误原因","已经推送到人力系统");
						listNo.add(dataNo);
						num++;
						continue;
					}
				}

				//查询数据是否已经插入
				Map jfjs = getJfjs(years,months,usercard,session,orgid);
				if(jfjs==null){//插入
					try {
						//删除个人党费明细
						if(!b){
							dfglGzjfHandle.deleteHDyDues2(years,months,orgid,list.get(i).get(1));
						}
						insertHDyJfjs(years,months,list.get(i),session,orgid,b);
					} catch (Exception e) {
						LehandException.throwException("导入失败，请检查表格");
					}

					insertnum += 1;
				}else{
					//是否是人力资源推送
					long synchronous = (long) jfjs.get("synchronous");
					if(1==synchronous){
						buffer.append("第"+(i+1)+"条数据是人力资源推送数据");
						buffer.append("\r\n");
						dataNo = new HashMap<String,String>();
						dataNo.put("姓名",list.get(i).get(0));
						dataNo.put("身份证号",usercard);
						dataNo.put("缴费基数",list.get(i).get(2));
						dataNo.put("组织名称",orgName);
						dataNo.put("组织编码",orgid);
						dataNo.put("错误原因","人力资源推送数据不能修改");
						listNo.add(dataNo);
						num++;
						continue;
					}
					try {
						//删除个人党费明细
						if(!b){
							dfglGzjfHandle.deleteHDyDues2(years,months,orgid,list.get(i).get(1));
						}
						updateHDyJfjs(years,months,list.get(i),session,orgid,b);
					} catch (Exception e) {
						LehandException.throwException("导入失败，请检查表格");
					}
					updatenum += 1;
				}

			}
			buffer.append("共"+(total)+"条数据,共导入成功"+(insertnum+updatenum)+"条数据;");
			if(num>0){
				message.fail("999999",buffer.toString()+"失败");
			}else{
				message.success();
			}
			if(listNo!=null&&listNo.size()>0){
				List<String> tables = new ArrayList<String>();
				tables.add("姓名");
				tables.add("身份证号");
				tables.add("缴费基数");
				tables.add("组织名称");
				tables.add("组织编码");
				tables.add("错误原因");
				List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
//				for(Map<String,String> li: listNo){
//					Map<String, String> tempMap = new HashMap<>();
//					tempMap.put("姓名",li.get("xm"));
//					tempMap.put("身份证号",li.get("zjhm"));
//					tempMap.put("缴费基数",li.get("basefee"));
//					tempMap.put("错误原因",li.get("errorcontent"));
//				}
				String[] tablesStr = tables.toArray(new String[tables.size()]);
				//表名
				String tableName = "缴费基数";
				File file2 = ExcelUtils.write2(tablesStr,listNo,tableName,location);
				//Map<String,Object> data  = documentManagerBusiness.uploadFile(file2,session);
				file2.delete();
				//message.setData(data.get("dir"));
			}
			return message;
		}
	}
	private int getValidateAll(int i, String idcard, String xm, String orgid, StringBuffer buffer) {
		Integer num = 0;
		//判断导入的姓名是否为空
		if(StringUtils.isEmpty(xm)){
			buffer.append("第"+i+"条数据姓名为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		//判断导入的idcard是否为空
		if(StringUtils.isEmpty(idcard)){
			buffer.append("第"+i+"条数据身份证号码为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		//判断导入的orgid是否为空
		if(StringUtils.isEmpty(orgid)){
			buffer.append("第"+i+"条数据组织编码为空;");
			buffer.append("\r\n");
			num++;
			return num;
		}
		return num;
	}
	public String exportJsAll(HttpServletResponse response,Session session){
		List<Map<String, Object>> list = new ArrayList<>();
		getCurrAndSubListOrg(session,session.getCurrentOrg().getOrgcode(),list);
		List<Map<String,Object>> listResult  = new ArrayList<>();
		if(list!=null&&list.size()>0){
			for (Map<String, Object> map:list) {
				List<Map<String,Object>> listInfo  = generalSqlComponent.query(SqlCode.getdfjsdc,new Object[]{map.get("organid"),zhengshidangyuan,yubeidangyuan});
				listResult.addAll(listInfo);
			}
		}

		exportFilesAll(response, listResult);
		return null;
	}
	 public String exportFilesAll(HttpServletResponse response,List<Map<String, Object>> list) {

		//表头
		List<String> tables = new ArrayList<String>();
		tables.add("姓名");
		tables.add("身份证号");
		tables.add("缴费基数");
		tables.add("组织名称");
		tables.add("组织编码");
		 List<Map<String,String>> explist = new ArrayList<Map<String,String>>();
		for(Map<String,Object> li: list){
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("姓名",li.get("xm").toString());
			tempMap.put("身份证号",li.get("zjhm").toString());
			tempMap.put("缴费基数",StringUtils.isEmpty(li.get("basefee"))?"":li.get("basefee").toString());
			Map<String,Object> map = db().getMap("select * from t_dzz_info where organid=?", new Object[] {(String)li.get("organid")});
			tempMap.put("组织名称",map!=null?(String) map.get("dzzmc"):"");
			tempMap.put("组织编码", StringUtils.isEmpty(li.get("organid"))?"":li.get("organid").toString());
			explist.add(tempMap);
		}
		String[] tablesStr = tables.toArray(new String[tables.size()]);
		//表名
		String tableName = "缴费基数";
		ExcelUtils.write(response,tablesStr,explist,tableName);
		return null;
	}
	/**
	 * 获取当前组织下所有组织编码
	 * @param organcode
	 * @return
	 */
	private void getOrgCode(String organcode,List<String> listOrganId) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("organcode", organcode);
		List<Map<String,Object>> list  = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getTDzzInfoSimpleTree2, paramMap);
		for(Map<String,Object> li:list){
			listOrganId.add("'"+li.get("organid").toString()+"'");
			getOrgCode(li.get("organcode").toString(),listOrganId);
		}
	}
    public List<Map<String, Object>> duesStatistical(Session session, String orgcode, String organid, String year) {
		//保留两位小数

		List<Map<String, Object>> result = new ArrayList<>();
		List<String> listOrganId = new ArrayList<>();
		//获取当前组织编码及其下所有组织编码
		listOrganId.add("'"+organid+"'");
		getOrgCode(orgcode,listOrganId);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("organid", org.apache.commons.lang.StringUtils.strip(listOrganId.toString(),"[]"));
		paramMap.put("year",year);
		for (int i=1;i<=12;i++){
			paramMap.put("month",i);
			Map<String, Object> data = generalSqlComponent.query(SqlCode.duesstatistical, paramMap);
			data.put("payable",data.get("payable")!=null?data.get("payable"):0);
			data.put("month",i);
			data.put("actual",data.get("actual")!=null?data.get("actual"):0);
			result.add(data);
		}
		return result;
    }

    /**
     * Description: 党支部批量更新党员的实交党费
     * @author lx
     * @date 2021-04-18
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void batchModiactual(Session session,List<PeisonActualDto> peisonActualDtos){
        List<Map<String,Object>> listPeison = new ArrayList<>();
        for (PeisonActualDto peisonActualDto:
                peisonActualDtos) {
            modiActual(session, peisonActualDto.getMonth(), peisonActualDto.getYear(), peisonActualDto.getActual(),peisonActualDto.getPeisonid(),"",peisonActualDto.getPayable());
        }
    }

    public Pager pageMaintainDues(Session session, String organid, Pager pager) {
        //获取所有人力没有推送的基数的数据
        db().pageMap(pager,"select a.xm, a.userid,a.zjhm, 0 as payable, b.basefee as jnjs from t_dy_info a left join h_dy_jfjs b on a.userid = b.userid where a.organid = ? and (a.operatetype != 3 or a.operatetype is null) and (dylb = ? or dylb = ?) and (a.delflag != 1 or a.delflag is null) and (b.synchronous != 1 or b.synchronous is null)", new Object[]{organid, zhengshidangyuan, yubeidangyuan});
        LocalDate localDate = LocalDate.now();
        int months = localDate.getMonthValue();
        int years = localDate.getYear();
        List<Map<String,Object>> rows = (List<Map<String, Object>>) pager.getRows();
        for (Map<String,Object> map:rows) {//是否有生成的缴费记录
            Map<String, Object> hDyDues = getHDyDues(years, months, String.valueOf(map.get("zjhm")));
            map.put("payable",hDyDues==null?0:hDyDues.get("payable"));
        }
        return pager;
    }

    public void modifyMaintainDues(Session session, String organid, String userid, String zjhm, String payable) {
        LocalDate localDate = LocalDate.now();
        int months = localDate.getMonthValue();
        int years = localDate.getYear();
        Map<String, Object> hDyDues = getHDyDues(years, months, zjhm);
        Map<String, Object> paramMap = new HashMap<>();
        Map<String, Object> dyMap = db().getMap("select * from t_dy_info where userid=? limit 1", new Object[]{userid});
        if(dyMap==null){
            LehandException.throwException("党员不存在！");
        }
        paramMap.put("years",years);
        paramMap.put("months",months);
        paramMap.put("usercard",zjhm);
        paramMap.put("username",dyMap.get("xm"));
        paramMap.put("organcode",organid);
        paramMap.put("deductionseven",0);
        paramMap.put("basefee",0);
        paramMap.put("rate",0);
        paramMap.put("payable",payable);
        paramMap.put("status",1);
        if(hDyDues==null){//不存在则新增
            //插入个人党费明细
            db().insert("INSERT INTO h_dy_dues (years, months, usercard, username, organcode, deductionseven, basefee, rate, payable, status) VALUES (:years, :months, :usercard, :username, :organcode, :deductionseven, :basefee, :rate, :payable, :status);\n",paramMap);
        }else{
            //修改个人党费明细
            db().update("UPDATE h_dy_dues SET  payable=:payable WHERE years=:years AND months=:months AND usercard=:usercard;",paramMap);
        }
    }
}
