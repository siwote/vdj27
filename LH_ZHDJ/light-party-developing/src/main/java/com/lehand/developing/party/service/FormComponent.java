//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lehand.developing.party.service;

import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.util.BeanUtil;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.constant.FormSqlCode;
import com.lehand.developing.party.dto.Condition;
import com.lehand.developing.party.pojo.FormInfo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class FormComponent {
    @Resource
    protected GeneralSqlComponent generalSqlComponent;

    public FormComponent() {
    }

    @Transactional(
        rollbackFor = {Exception.class}
    )
    public Long addForm(FormInfo info, Long compid) {
        info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        info.setStatus(1);
        info.setCompid(compid);
        Long formId = (Long)this.generalSqlComponent.insert(FormSqlCode.insertForm, info);
        info.setId(formId);
        return formId;
    }

    public Pager pageForm(Pager pager, Condition condition, Long compid) {
        condition.setCompid(compid);
        Map<String, Object> params = BeanUtil.bean2map(condition);
        this.generalSqlComponent.pageQuery(FormSqlCode.pageQueryForm, params, pager);
        return pager;
    }

    @Transactional(
        rollbackFor = {Exception.class}
    )
    public void deleteFormById(Long formId, Long compid) {
        this.generalSqlComponent.update(FormSqlCode.deleteFormById, new Object[]{formId, compid});
    }

    @Transactional(
        rollbackFor = {Exception.class}
    )
    public void updateForm(FormInfo info, Long compid) {
        Long id = info.getId();
        FormInfo oldForm = this.getFormById(id, compid);
        this.toUpdateForm(oldForm, info);
        oldForm.setCompid(compid);
        this.generalSqlComponent.update(FormSqlCode.updateForm, oldForm);
    }

    private void toUpdateForm(FormInfo oldForm, FormInfo info) {
        String formname = info.getFormname();
        String jsonstr = info.getJsonstr();
        Long bussid = info.getBussid();
        Integer status = info.getStatus();
        oldForm.setUpdatetime(DateEnum.YYYYMMDDHHMMDD.format());
        if (StringUtils.hasText(formname)) {
            oldForm.setFormname(formname);
        }

        if (StringUtils.hasText(jsonstr)) {
            oldForm.setJsonstr(jsonstr);
        }

        if (bussid != null) {
            oldForm.setBussid(bussid);
        }

        if (status != null) {
            oldForm.setStatus(status);
        }

    }

    public FormInfo getFormById(Long id, Long compid) {
        return (FormInfo)this.generalSqlComponent.query(FormSqlCode.getFormById, new Object[]{id, compid});
    }

    public FormInfo getFormByBusId(Long bussid, Long compid) {
        return (FormInfo)this.generalSqlComponent.query(FormSqlCode.getFormByBusId, new Object[]{bussid, compid});
    }

    public GeneralSqlComponent getGeneralSqlComponent() {
        return this.generalSqlComponent;
    }

    public void setGeneralSqlComponent(GeneralSqlComponent generalSqlComponent) {
        this.generalSqlComponent = generalSqlComponent;
    }
}
