package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyRdzysStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_rdzys where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		if(map!=null) {
			String fileids = String.valueOf(map.get("file"));
			List<Map<String, Object>> files = new ArrayList<Map<String,Object>>(0);
			if(!StringUtils.isEmpty(fileids)) {
				files = db().listMap("select * from dl_file where compid=? and id in ("+fileids+")", new Object[] {session.getCompid()});
			}
			map.put("files", files);
		}else {
			map = new HashMap<String, Object>();
		}
		Map<String, Object> map1 = db().getMap("select * from t_fzdy_dwys where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		if(map1!=null) {
			map.put("rdzysbh", map1.get("rdzysbh"));
			map.put("lqrdzysrq", map1.get("lqrdzysrq"));
		}
		return map;
	}
	

	
	//txrdzysrq
	//	person_id
	//	file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("rdzys_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("scfj")==null) {
			params.put("scfj", Constant.ENPTY);
		}
		
		int num = db().delete("UPDATE t_fzdy_rdzys SET syncstatus='D' WHERE  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_rdzys (compid, rdzys_id, scfj, created_by, create_date, delete_flag, person_id, syncstatus, synctime, file, txrdzysrq)" + 
				" VALUES (:compid,:rdzys_id,:scfj,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file,:txrdzysrq)", params);
		
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		
		saveFile(params);
		return num;
	}
}
