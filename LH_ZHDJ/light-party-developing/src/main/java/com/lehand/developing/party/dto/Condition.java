//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lehand.developing.party.dto;

public class Condition {
    private Long bussid;
    private String formname;
    private String starttime;
    private String endtime;
    private int status = 1;
    private Long compid;

    public Condition() {
    }

    public String getFormname() {
        return this.formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public String getStarttime() {
        return this.starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return this.endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Long getBussid() {
        return this.bussid;
    }

    public void setBussid(Long bussid) {
        this.bussid = bussid;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCompid() {
        return this.compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }
}
