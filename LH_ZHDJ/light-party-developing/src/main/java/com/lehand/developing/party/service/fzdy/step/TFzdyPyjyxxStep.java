package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.base.common.Pager;
import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyPyjyxxStep extends BaseStep {
	
	//person_id 
	public Pager page(Pager pager, Map<String, Object> params, Session session) {
	    db().pageMap(pager, "select * from t_fzdy_pyjyxx where type=:type and person_id=:person_id and syncstatus='A' and compid=:compid order by pyjysj asc", params);
		return pager;
	}

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_pyjyxx where edu_id=:edu_id and person_id=:person_id and syncstatus='A' and compid=:compid", params);
		String fileids = String.valueOf(map.get("file"));
		List<Map<String, Object>> files = new ArrayList<Map<String,Object>>(0);
		if(!StringUtils.isEmpty(fileids)) {
			files = db().listMap("select * from dl_file where compid=? and id in ("+fileids+")", new Object[] {session.getCompid()});
		}
		map.put("files", files);
		return map;
	}
	

	//pyjyqk
	//pyjysj 
	//stage
	//status
	//pyjylb
	//file
	//person_id
    //  flags 新增传1 修改传0
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		int flags = Integer.valueOf(params.get("flags").toString());
		if(flags!=0) {
			params.put("edu_id", UUID.randomUUID().toString().replaceAll("-", ""));
		}
		params.put("created_by", session.getUserid());
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("pyjyfj")==null) {
			params.put("pyjyfj", Constant.ENPTY);
		}
		int num = 0;
		if(flags==0) {
			db().delete("UPDATE t_fzdy_pyjyxx SET syncstatus='D' WHERE edu_id=:edu_id and person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		}
		params.put("edu_id", UUID.randomUUID().toString().replaceAll("-", ""));
		num += db().insert("INSERT INTO t_fzdy_pyjyxx (compid, edu_id,type, pyjysj, pyjylb, pyjyqk, pyjyfj, status, stage, created_by,  create_date, delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:edu_id,:type,:pyjysj,:pyjylb,:pyjyqk,:pyjyfj,:status,:stage,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		List<Map<String,Object>> data = db().listMap("select file from t_fzdy_pyjyxx where type=? and person_id=? and syncstatus='A'",  new Object[] {params.get("step"),params.get("person_id")});	
		String fileids="";
		if(data!=null && data.size()>0) {
			for (Map<String, Object> map2 : data) {
				fileids += map2.get("file")+",";
			}
		}
		if(StringUtils.isEmpty(params.get("file"))) {
			fileids = fileids.substring(0,fileids.length()-1);
		}else {
			fileids = fileids+params.get("file");
		}
		params.put("file", fileids);
		saveFile(params);
		return num;
	}
}
