package com.lehand.developing.party.utils;

import com.alibaba.druid.util.StringUtils;
import com.lehand.base.common.Organs;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.dto.UrcOrgan;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class SessionUtils {

    @Resource
    protected GeneralSqlComponent generalSqlComponent;

    public DBComponent db() {
        return generalSqlComponent.getDbComponent();
    }

    /** 联表查询系统组织用户信息*/
    private final  String getOrgansInfoByOrgid = "SELECT a.compid,a.orgid,a.orgname,a.orgcode, a.orgtypeid otid, b.userid " +
            "FROM urc_organization a " +
            "left join  " +
            "( select s.userid,t.orgid,s.compid from urc_user s LEFT JOIN urc_user_org_map t on s.userid = t.userid and s.compid = t.compid and s.accflag = 1 ) b " +
            "on a.orgid = b.orgid  and a.compid = b.compid where  a.orgid = ?  and a.compid = ?";

    /**
     * 根据组织机构编码获取组织机构id
     * @param organCode
     * @return
     */
    public  String  getOrganidByOrganCode(String organCode){
        String organid = db().getSingleColumn("select organid  from  t_dzz_info where organcode = ?",String.class,new Object[]{organCode});
        return organid;
    }

    /**
     * 根据组织机构编码获取组织机构类别
     * @param organCode
     * @return
     */
    public  String  getOrgTypeByOrganCode(String organCode){
        String zzlb = db().getSingleColumn("select zzlb  from  t_dzz_info where organcode = ?",String.class,new Object[]{organCode});
        return zzlb;
    }

    /**
     * 获取最高组织机构编码
     * @return
     */
    public  String  getRootOrgCode(){
        String organCode = db().getSingleColumn("select organcode from t_dzz_info_simple where organid = path1",String.class,new Object[]{});
        return organCode;
    }

    /**
     * 根据组织机构编码获取 urc组织机构
     * @param organcode
     * @param compid
     * @return
     */
    public UrcOrgan getUrcOrgByOrgancode(String organcode, Long compid){
        UrcOrgan org  = db().getBean("select * from urc_organization where orgcode = ? and compid=?",
                UrcOrgan.class,new Object[]{organcode,compid});
        return org;
    }

    /**
     * 根据urc的orgid获取 urc组织机构
     * @param orgid
     * @param compid
     * @return
     */
    public UrcOrgan getUrcOrgByOrgid(Long orgid,Long compid){
        UrcOrgan org  = db().getBean("select * from urc_organization where orgid = ? and compid = ?",
                UrcOrgan.class,new Object[]{orgid,compid});
        return org;
    }

    /**
     * 获取secondOrgan属性(上级中的二级党委)
     * @param currorgid
     * @param compid
     * @return
     */
    public Organs getSecondOrgan(Long currorgid ,Long compid){
        UrcOrgan organ = db().getBean("select * from urc_organization where orgid = ? and compid=?",UrcOrgan.class, new Object[] { currorgid, compid });

        Organs org = new Organs();
        //业务表查二级党委id
        String  secondOrganid = db().getSingleColumn("select path2 from t_dzz_info_simple where organcode = ? ",String.class,new Object[]{organ.getOrgcode()});
        if(StringUtils.isEmpty(secondOrganid)){
            return new Organs();
        }
        //查出二级党委code到系统表查询二级党委信息
        String secondOrganCode = db().getSingleColumn("select organcode from t_dzz_info_simple where organid = ? ",String.class,new Object[]{secondOrganid});
        UrcOrgan secondOran = db().getBean("select * from urc_organization where orgcode = ? and compid=?",UrcOrgan.class, new Object[] { secondOrganCode, compid });

        if(secondOran == null){
            return new Organs();
        }
        if(!"6100000035".equals(String.valueOf(secondOran.getOrgtypeid()))){
            return new Organs();
        }

        org.setOrgid(secondOran.getOrgid());
        org.setOrgname(secondOran.getOrgname());
        org.setOrgcode(secondOran.getOrgcode());
        org.setOrgfzdyid(secondOrganid);
        return org;
    }

    /**
     * 获取上级党组织
     * @param porgid
     * @param compid
     * @return
     */
    public Organs getUpOrganByOrgid (Long porgid,Long compid){
        UrcOrgan porgans = getUrcOrgByOrgid(porgid,compid);
        if(porgans == null||porgans.getOrgid() == null){
            return null;
        }
        //获取组织机构信息
        Organs organs = getOrgansInfoByOrgid(porgans.getOrgid(),compid);
        return organs;
    }

    /**
     * 根据orgid获取Organs信息
     * @param orgid
     * @param compid
     * @return
     */
    private Organs getOrgansInfoByOrgid(Long orgid, Long compid) {
        Organs organs = db().getBean(getOrgansInfoByOrgid,
                Organs.class,new Object[]{orgid,compid});
        return organs;
    }


    /**
     * secondOrgan 组装
     * @param org
     * @return
     */
    public Organs makeSecondOrgan(Organs org){
        if(org == null){
            return org;
        }

        if(StringUtils.isEmpty(org.getOrgcode())){
            return org;
        }

        String organid = getOrganidByOrganCode(org.getOrgcode());
        org.setOrgfzdyid(organid);
        org.setOtid(getOrgTypeByOrganCode(org.getOrgcode()));
        return org;
    }
}
