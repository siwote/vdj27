package com.lehand.developing.party.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.apache.tools.ant.launch.Locator.encodeURI;

/**
 * @author PT
 * @Title:
 * @Package
 * @Description:
 * @date 2020/8/2415:34
 */
public class NewsPush {
    public static String load(String url, String query) throws Exception {
        URL restURL = new URL(url);
        /*
         * 此处的urlConnection对象实际上是根据URL的请求协议(此处是http)生成的URLConnection类 的子类HttpURLConnection
         */
        HttpURLConnection conn = (HttpURLConnection) restURL.openConnection();
        String res = "";
        conn.setDoOutput(true); // 必须设置这两个请求属性为true，就表示默认使用POST发送
        conn.setDoInput(true);
        OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
        out.write(query);
        out.flush(); // 立即充刷至请求体）PrintWriter默认先写在内存缓存中
        try// 发送正常的请求（获取资源）
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line + "\n";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void main(String[] args) {
        try {
            NewsPush restUtil = new NewsPush();
            String param = encodeURI(encodeURI("receiveAcc=340602198710042417&content=测试企业微信文本消息推送&sendAcc=B034020660156"));
            String resultString = restUtil.load("http://10.1.50.243:9050/message/push/newsPush",param);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
        }
    }
}


