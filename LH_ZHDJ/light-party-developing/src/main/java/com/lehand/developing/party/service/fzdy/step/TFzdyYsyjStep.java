package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyYsyjStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_ysyj where person_id=:person_id and syncstatus='A' and compid=:compid", params);
//		if (map != null) {
//			String fileids = String.valueOf(map.get("file"));
//			List<Map<String, Object>> files = new ArrayList<Map<String, Object>>(0);
//			if (!StringUtils.isEmpty(fileids)) {
//				files = db().listMap("select * from dl_file where compid=? and id in (" + fileids + ")",
//						session.getCompid());
//			}
//			map.put("files", files);
//		}
		makeFiles(map,session);
		return map;
	}
	

	
	//bsjdwysrq
	//yj
	//	person_id
	//	file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("reporte_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("bsjdwysjg")==null) {
			params.put("bsjdwysjg", Constant.ENPTY);
		}
		
		int num = db().delete("UPDATE t_fzdy_ysyj SET syncstatus='D' WHERE  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_ysyj (compid, reporte_id, bsjdwysrq, yj, bsjdwysjg, created_by, create_date,  delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:reporte_id,:bsjdwysrq,:yj,:bsjdwysjg,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		saveFile(params);
		
		return num;
	}
}
