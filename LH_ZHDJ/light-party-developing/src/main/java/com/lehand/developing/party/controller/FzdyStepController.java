package com.lehand.developing.party.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.util.BeanUtil;
//import com.lehand.base.util.ServletUtil;
import com.lehand.developing.party.service.FzdyStepService;
import com.lehand.developing.party.service.fzdy.handle.FzdyBzxxHandle;
import com.lehand.developing.party.service.fzdy.handle.FzdyDzzspHandle;

import io.swagger.annotations.Api;

@Api(value = "每一步的服务", tags = { "每一步的服务" })
@RestController
@RequestMapping("/developing/step")
public class FzdyStepController extends BaseController {

	@Resource
	private FzdyStepService fzdyStepService;
	@Resource 
	private FzdyBzxxHandle fzdyBzxxHandle;
	@Resource 
	private FzdyDzzspHandle fzdyDzzspHandle;
	
	/**
	 * 48-step total interface 48 步总接口
	 * @param request
	 * @param response
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "48-step total interface 48 步总接口", notes = "48-step total interface 48 步总接口", httpMethod = "POST")
	@RequestMapping("/process")
	public Message process(HttpServletRequest request,HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			//Map<String, Object> params = ServletUtil.params2map(request);
			//Object result = fzdyStepService.process(params , getSession());
			//msg.success().setData(result);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Click to enter the next stage 点击进入下一阶段       
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "点击进入下一阶段", notes = "点击进入下一阶段", httpMethod = "POST")
	@RequestMapping("/nextStage")
	public Message nextStage(HttpServletRequest request,HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			/*Map<String, Object> params = ServletUtil.params2map(request);
			Object result = fzdyBzxxHandle.nextStage(params , getSession());
			msg.success().setData(result);*/
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	/**
	 * Click to modify the information for the specified phase  点击修改指定阶段的信息
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "点击修改指定阶段的信息", notes = "点击修改指定阶段的信息", httpMethod = "POST")
	@RequestMapping("/enterSpecificStage")
	public Message enterSpecificStage(HttpServletRequest request,HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			//Map<String, Object> params = ServletUtil.params2map(request);
			//fzdyBzxxHandle.enterSpecificStage(params , getSession());
			//msg.success();
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Audit list 审核列表
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "审核列表", notes = "审核列表", httpMethod = "POST")
	@RequestMapping("/auditPage")
	public Message auditPage(HttpServletRequest request,HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			//Map<String, Object> params = ServletUtil.params2map(request);
			Object result = null;
			/*if ("page".equals(params.get("optflag").toString())) {
				Pager pager = new Pager();
				BeanUtil.map2bean(params, pager);
				result = fzdyDzzspHandle.page(pager, params, getSession());
			}*/
			msg.success().setData(result);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Party branch and party committee approval interface 修改信息审核
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改信息审核", notes = "修改信息审核", httpMethod = "POST")
	@RequestMapping("/audit")
	public Message audit(HttpServletRequest request, HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			/*Map<String, Object> params = ServletUtil.params2map(request);
			fzdyDzzspHandle.auditBaseMesg(params, getSession());*/
			msg.success();
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	
	/**
	 * Description: 判断每步是否可以编辑
	 * @author PT  
	 * @date 2019年8月23日 
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "判断每步是否可以编辑", notes = "判断每步是否可以编辑", httpMethod = "POST")
	@RequestMapping("/getmap")
	public Message getMap(HttpServletRequest request, HttpServletResponse response) {
		Message msg = Message.FAIL;
		try {
			/*Map<String, Object> params = ServletUtil.params2map(request);
			Map<String,Object> map = fzdyDzzspHandle.getMap(params, getSession());
			msg.success().setData(map);*/
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description: 发展党员巡查列表分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param pager
	 * @param stageid
	 * @param name
	 * @param idcard
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "发展党员巡查列表分页查询", notes = "发展党员巡查列表分页查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(Pager pager,String orgid,Integer stageid,String name,String idcard ) {
		Message msg = Message.FAIL;
		try {
		    pager = fzdyDzzspHandle.page2(getSession(),pager,stageid,name,idcard,orgid);
			msg.success().setData(pager);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	
	/**
	 * Description: 查询当前登录机构下面正式党员的集合
	 * @author PT  
	 * @date 2019年9月9日 
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询当前登录机构下面正式党员的集合", notes = "查询当前登录机构下面正式党员的集合", httpMethod = "POST")
	@RequestMapping("/list")
	public Message list(String name) {
		Message msg = Message.FAIL;
		try {
		    List<Map<String,Object>> data = fzdyStepService.list(getSession(),name);
			msg.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description: 获取党员的详细信息
	 * @author PT  
	 * @date 2019年9月10日 
	 * @param userid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取党员的详细信息", notes = "获取党员的详细信息", httpMethod = "POST")
	@RequestMapping("/get")
	public Message get(String userid) {
		Message msg = Message.FAIL;
		try {
		    Map<String,Object> data = fzdyStepService.get(getSession(),userid);
			msg.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description:工作岗位下拉框
	 * @author PT  
	 * @date 2019年9月10日 
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "工作岗位下拉框", notes = "工作岗位下拉框", httpMethod = "POST")
	@RequestMapping("/listgzgw")
	public Message listGzgw() {
		Message msg = Message.FAIL;
		try {
		    List<Map<String,Object>> data = fzdyStepService.listGzgw();
			msg.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description: 资料打包
	 * @author PT  
	 * @date 2019年9月16日 
	 * @param person_id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "资料打包", notes = "资料打包", httpMethod = "POST")
	@RequestMapping("/datapackaging")
	public Message dataPackaging(String person_id) {
		Message msg = Message.FAIL;
		try {
		    String data = fzdyStepService.dataPackaging(person_id,getSession());
			msg.success().setData(data);
		} catch (Exception e) {
			msg.success().setData(null);
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	
	/**
	 * Description: 发展党员资料导出
	 * @author PT  
	 * @date 2019年9月17日 
	 * @param person_id
	 * @param stepids
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "发展党员资料导出", notes = "发展党员资料导出", httpMethod = "POST")
	@RequestMapping("/fileexport")
	public Message fileExport(String person_id,String stepids) {
		Message msg = Message.FAIL;
		try {
		    String data = fzdyStepService.fileExport(person_id,stepids);
			msg.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	/**
	 * Description: 合成pdf暂时未调通，还需继续调
	 * @author PT  
	 * @date 2019年10月9日 
	 * @param files
	 * @param newfile
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = " 合成pdf暂时未调通，还需继续调", notes = " 合成pdf暂时未调通，还需继续调", httpMethod = "POST")
	@RequestMapping("/mergePdfFiles")
	public Message mergePdfFiles(String[] files, String newfile) {
		Message msg = Message.FAIL;
		try {
		    String data = fzdyStepService.mergePdfFiles(files,newfile);
			msg.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setMessage(e.getMessage());
		}
		return msg;
	}
	
	
	/**
	 * Description: 党员附件导出列表查询
	 * @author PT  
	 * @date 2019年9月29日 
	 * @param person_id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "党员附件导出列表查询", notes = "党员附件导出列表查询", httpMethod = "POST")
	@RequestMapping("/listfiles")
	public Message listFiles(String person_id) {
		Message msg = Message.FAIL;
		try {
			List<Map<String,Object>> data = fzdyStepService.listFiles(person_id,getSession());
			msg.success().setData(data);
		} catch (Exception e) {
			msg.success().setData(null);
			msg.setMessage(e.getMessage());
		}
		return msg;
	}

	/**
	* @Description 获取当前组织的支部书记
	* @Author zwd
	* @Date 2020/10/30 15:37
	* @param
	* @return
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取当前组织的支部书记", notes = "获取当前组织的支部书记", httpMethod = "POST")
	@RequestMapping("/getSj")
	public Message   getSj(String orgid){
		Message msg = Message.FAIL;
		msg.success().setData(fzdyStepService.getSj(getSession(),orgid));
		return msg;
	}
	
}
