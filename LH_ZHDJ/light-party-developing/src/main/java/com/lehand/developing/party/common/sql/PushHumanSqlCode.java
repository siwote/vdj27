package com.lehand.developing.party.common.sql;

/**
 * 
 * @ClassName:：PushHumanSqlCode
 * @Description：数据库sql语句静态常量
 * @author ：lx
 * @date ：2021/03/12
 *
 */
public class PushHumanSqlCode {

	/**
	 * 获取所有党员证件号码和组织id
	 * SELECT a.zjhm zjhm, a.organid organid ,a.userid userid,a.xm xm FROM t_dy_info a
	 * LEFT JOIN t_dzz_info_simple b ON a.organid = b.organid
	 * AND ( a.delflag != 1 OR a.delflag IS NULL )
	 * AND b.operatetype != 3
	 * AND( b.delflag != 1 OR b.delflag IS NULL)
	 */
	public static String listZJHMByTDyInfo="listZJHMByTDyInfo";
	/**
	 * 获取所有党员缴费基数和openid
	 * select a.username,b.zjhm,a.basefee,b.organid,a.userid,c.remarks3 openid  from h_dy_jfjs a
	 * LEFT JOIN t_dy_info b ON b.userid = a.userid
	 * LEFT JOIN urc_user c ON b.zjhm=c.idno
	 * where a.compid = 1 AND (b.delflag=0 OR b.delflag IS NULL) AND (c.remarks3 IS NOT NULL)
	 */
    public static String getDfjsOpenidList="getDfjsOpenidList";
}
