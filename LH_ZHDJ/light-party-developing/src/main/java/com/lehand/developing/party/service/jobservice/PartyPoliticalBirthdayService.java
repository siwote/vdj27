package com.lehand.developing.party.service.jobservice;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.developing.party.utils.NewsPush;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.tools.ant.launch.Locator.encodeURI;

public class PartyPoliticalBirthdayService {

	private static final Logger logger = LogManager.getLogger(PartyPoliticalBirthdayService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务

	@Value("${wx_service_url}")
	private String url;

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	
	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	

	public void partyPoliticalBirthday() {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			final List<Map<String,Object>> list = db().listMap("select * from t_dy_info", new Object[] {});
			if (list.size()<=0) {
				return;
			}
			logger.info("定时发送党员政治生日消息任务开始...");
			
			//遍历各个党支部
			for (final Map<String,Object> maps : list) {
				threadPoolTaskExecutor.execute(new Runnable() {
					public void run() {
						try {
							String idcard = String.valueOf(maps.get("zjhm"));
							String rdsj = String.valueOf(maps.get("rdsj"));
							if(!StringUtils.isEmpty(idcard) && !StringUtils.isEmpty(rdsj)){
								if(rdsj.substring(5,10).equals(DateEnum.YYYYMMDD.format().substring(5))){
									String content = "尊敬的"+maps.get("xm")+"党员同志，您好!今天是您入党纪念日。在此，特向您表示热烈的祝贺，感谢您一直以来做出的努力和贡献";
									NewsPush.load(url,encodeURI(encodeURI("receiveAcc="+"'"+idcard+"'"+"&content="+content+"&sendAcc=ydjadmin" )));
								}
							}
						} catch (Exception e) {
							logger.error("定时发送党员政治生日消息任务异常",e);
						}
					}
				});
			}
			logger.info("定时发送党员政治生日消息任务结束...");
		} catch (Exception e) {
			logger.error("定时发送党员政治生日消息任务异常",e);
		} finally {
			CREATE.set(false);
		}
	}
	

	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
}


