package com.lehand.developing.party.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.controller.BaseController;
import com.lehand.developing.party.service.dfgl.dfsjgl.DfglJsszHandle;
import com.lehand.developing.party.service.dfgl.dfsjgl.DfglSjblHandle;
import com.lehand.developing.party.service.dfgl.dfsygl.DfglDfsyHandle;
import com.lehand.developing.party.service.dfgl.dfsygl.PartyFeeApplicationAuditDto;

@Api(value = "党费", tags = { "党费申请、报销等" })
@RestController
@RequestMapping("/developing/dfsy")
public class DfgldfsyController extends BaseController{
	
	@Resource DfglJsszHandle dfglJsszHandle;
	
	@Resource DfglSjblHandle dfglSjblHandle;
	
	//党支部code
	@Value("${jk.organ.dangzhibu}")
	private String dangzhibu;
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	@Resource private DfglDfsyHandle dfglDfsyHandle;
	
	
	
	/**=============================================党费和工作经费费用类别===================================================================*/

	/**
	 * Description: 费用类别查询
	 * @author PT  
	 * @date 2019年9月3日
	 * @param type
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "费用类别查询", notes = "费用类别查询", httpMethod = "POST")
	@RequestMapping("/list")
	public Message list(Integer type) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = dfglDfsyHandle.list(getSession(),type);
		    message.setData(list);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("分类查询出错");
		}
		return message;
	}
	
	
	/**
	 * Description:分类新增/修改
	 * 
	 * @author PT
	 * @date 2019年9月3日
	 * @param type 分类类型(0:党费,1:工作经费)
	 * @param pid
	 * @param name
	 * @param id 新增传空串 修改传id
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "分类新增/修改", notes = "分类新增/修改", httpMethod = "POST")
	@RequestMapping("/save")
	public Message save(Integer type,Integer pid, String name, String id) {
		Message message = new Message();
		try {
			dfglDfsyHandle.save(getSession(),type,pid,name,id);
		    message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			message.setMessage("分类查询出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 分类删除
	 * @author PT  
	 * @date 2019年9月3日
	 * @param id
	 */
	@OpenApi(optflag=3)
	@ApiOperation(value = "分类删除", notes = "分类删除", httpMethod = "POST")
	@RequestMapping("/delete")
	public Message delete(String id) {
		Message message = new Message();
		try {
			dfglDfsyHandle.delete(getSession(),id);
		    message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("分类查询出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 任务分类拖拽
	 * @author PT  
	 * @date 2019年9月3日 
	 * @param srcid
	 * @param tagid
	 * @param flag
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "任务分类拖拽", notes = "任务分类拖拽", httpMethod = "POST")
	@RequestMapping("/move")
	public Message move(Long srcid, Long tagid, int flag) {
		Message message = new Message();
		try {
			dfglDfsyHandle.move(srcid, tagid, flag, getSession().getCompid());
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("分类查询出错");
		}
		return message;
	}
	
	
	/**======================================================党费使用申请===============================================================*/
	
	/**
	 * Description: 党费是用申请保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param title
	 * @param feetype
	 * @param appfee
	 * @param appdesc
	 * @param appid
	 * @param json
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "党费是用申请保存", notes = "党费是用申请保存", httpMethod = "POST")
	@RequestMapping("/savepartyapplication")
	public Message savePartyApplication(String title,Integer feetype, String appfee, String appdesc,String appid,String json) {
		Message message = new Message();
		try {
			dfglDfsyHandle.savePartyApplication(getSession(),title,feetype,appfee,appdesc,appid,json);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费使用申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 保存申请单时关联的申请单列表数据源
	 * @author PT  
	 * @date 2019年9月4日 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "保存申请单时关联的申请单列表数据源", notes = "保存申请单时关联的申请单列表数据源", httpMethod = "POST")
	@RequestMapping("/listpartyfeeapplicationform")
	public Message listPartyFeeApplicationForm() {
		Message message = new Message();
		try {
			List<Map<String,Object>> data = dfglDfsyHandle.listPartyFeeApplicationForm(getSession());
		    message.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费使用申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 党费申请/报销申请审核
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param plaad
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费申请/报销申请审核", notes = "党费申请/报销申请审核", httpMethod = "POST")
	@RequestMapping("/auditpartyfeeapplication")
	public Message auditPartyFeeApplication(PartyFeeApplicationAuditDto plaad) {
		Message message = new Message();
		try {
			dfglDfsyHandle.auditPartyFeeApplication(getSession(),plaad);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 下拨党费列表分页查询
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param pager
	 * @param name
	 * @param time
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "下拨党费列表分页查询", notes = "下拨党费列表分页查询", httpMethod = "POST")
	@RequestMapping("/lowerpartyfeepage")
	public Message lowerPartyFeePage(Pager pager,String name,String time) {
		Message message = new Message();
		try {
			pager = dfglDfsyHandle.lowerPartyFeePage(getSession(),pager,name,time);
		    message.success().setData(pager);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 下拨党费保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param orgid
	 * @param orgname
	 * @param retain
	 * @param allocate
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "下拨党费保存", notes = "下拨党费保存", httpMethod = "POST")
	@RequestMapping("/savelowerpartyfee")
	public Message saveLowerPartyFee(String orgid,String orgname, Double retain, Double allocate) {
		Message message = new Message();
		try {
			dfglDfsyHandle.saveLowerPartyFee(getSession(),orgid,orgname,retain,allocate,1);
		    message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费使用申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 党费报销申请保存
	 * @author PT  
	 * @date 2019年9月4日 
	 * @param flag
	 * @param title
	 * @param appid
	 * @param sumfee
	 * @param feelist
	 * @param remarks3
	 * @param remarks4
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "党费报销申请保存", notes = "党费报销申请保存", httpMethod = "POST")
	@RequestMapping("/savereimbursementfundingapplication")
	public Message saveReimbursementFundingApplication(Integer flag,String title, String appid,
			String sumfee, String feelist, String remarks3, String remarks4) {
		Message message = new Message();
		try {
			dfglDfsyHandle.saveReimbursementFundingApplication(getSession(),title, flag, appid, sumfee,
					feelist, remarks3, remarks4);
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 下拨党费列表汇总 
	 * @author PT  
	 * @date 2019年9月10日 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "下拨党费列表汇总", notes = "下拨党费列表汇总", httpMethod = "POST")
	@RequestMapping("/getsummary")
	public Message getSummary() {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglDfsyHandle.getSummary(getSession());
		    message.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费使用申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 党组织获取下拨党费余额以及自留党费余额
	 * @author PT  
	 * @date 2019年9月6日 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "党组织获取下拨党费余额以及自留党费余额", notes = "党组织获取下拨党费余额以及自留党费余额", httpMethod = "POST")
	@RequestMapping("/getbalance")
	public Message getBalance() {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglDfsyHandle.getBalance(getSession());
		    message.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("党费使用申请保存出错");
		}
		return message;
	}
	
	
	/**==================================================党费信息汇总======================================================================*/
	
	
	/**
	 * Description: 党支部新增利息
	 * @author PT  
	 * @date 2019年9月6日
	 * @param addinterest
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "党支部新增利息", notes = "党支部新增利息", httpMethod = "POST")
	@RequestMapping("/saveinterest")
	public Message saveInterest(String addinterest) {
		Message message = new Message();
		try {
			dfglDfsyHandle.saveInterest(getSession(), addinterest);
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 新增修改账户信息
	 * @author PT  
	 * @date 2019年9月12日
	 * @param accountname
	 * @param balancecard
	 * @param handaddress
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "新增修改账户信息", notes = "新增修改账户信息", httpMethod = "POST")
	@RequestMapping("/savesum")
	public Message saveSum(String accountname, String balancecard, String handaddress) {
		Message message = new Message();
		try {
			dfglDfsyHandle.saveSum(getSession(),accountname, balancecard, handaddress);
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 查询账户信息
	 * @author PT  
	 * @date 2019年9月16日 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "查询账户信息", notes = "查询账户信息", httpMethod = "POST")
	@RequestMapping("/getsum")
	public Message getSum() {
		Message message = new Message();
		try {
			Map<String,Object> data = dfglDfsyHandle.getSum(getSession());
			message.success().setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/**
	 * Description: 查询现有党费
	 * @author PT  
	 * @date 2019年9月12日 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "查询现有党费", notes = "查询现有党费", httpMethod = "POST")
	@RequestMapping("/getfee")
	public Message getFee() {
		Message message = new Message();
		try {
			String data = dfglDfsyHandle.getFee(getSession());
			message.success().setData(data);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	/** 
	 * Description: 利息列表查询
	 * @author PT  
	 * @date 2019年9月6日 
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "利息列表查询", notes = "利息列表查询", httpMethod = "POST")
	@RequestMapping("/listinterest")
	public Message listInterest( String starttime, String endtime) {
		Message message = new Message();
		try {
			List<Map<String,Object>> data = dfglDfsyHandle.listInterest(getSession(), starttime,endtime);
			message.success().setData(data);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
	
	
	/**
	 * Description: 党费汇总列表查询
	 * @author PT  
	 * @date 2019年9月6日 
	 * @param orgid
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "党费汇总列表查询", notes = "党费汇总列表查询", httpMethod = "POST")
	@RequestMapping("/listpartyfeesummary")
	public Message listPartyFeeSummary(String orgid, String starttime, String endtime) {
		Message message = new Message();
		try {
			List<Map<String,Object>> data = dfglDfsyHandle.listPartyFeeSummary(getSession(),orgid,starttime,endtime);
			message.success().setData(data);;
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("工作经费申请保存出错");
		}
		return message;
	}
	
}
