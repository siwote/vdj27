package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.service.fzdy.handle.FzdyBzxxHandle;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyZsqkStep extends BaseStep {
	
	@Resource private FzdyBzxxHandle fzdyBzxxHandle;

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_zsqk where person_id=:person_id and syncstatus='A' and compid=:compid", params);
//		if(map!=null) {
//			String fileids = String.valueOf(map.get("file"));
//			List<Map<String, Object>> files = new ArrayList<Map<String,Object>>(0);
//			if(!StringUtils.isEmpty(fileids)) {
//				files = db().listMap("select * from dl_file where compid=? and id in ("+fileids+")", new Object[] {session.getCompid()});
//			}
//			map.put("files", files);
//		}
		makeFiles(map,session);
		return map;
	}
	

	//zsfs
	//zsjg
	//zswcrq
	//zsbz
	//person_id
	//file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("candidate_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("zsqtzl")==null) {
			params.put("zsqtzl", Constant.ENPTY);
		}
		int num = db().delete("UPDATE t_fzdy_zsqk SET syncstatus='D' WHERE  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_zsqk (compid, candidate_id, zsfs, zsjg, zswcrq, zsqtzl, zsbz, created_by, create_date,delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:candidate_id,:zsfs,:zsjg,:zswcrq,:zsqtzl,:zsbz,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		
		saveFile(params);
		
		//插入数据完成后就得更新下审核表对应数据的状态
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("compid", session.getCompid());
		paramMap.put("status", params.get("zsjg"));
		paramMap.put("person_id", params.get("person_id"));
		paramMap.put("audit_id", session.getUserid());
		paramMap.put("moditime", DateEnum.YYYYMMDDHHMMDD.format());
		paramMap.put("id", params.get("id"));
		db().update("update fzdy_dzzsp set status=:status,moditime=:moditime where compid=:compid and person_id=:person_id and audit_id=:audit_id and id=:id", paramMap);
		//如果是不通过那么就得更新步骤表直接返回到301
		if("-1".equals(params.get("zsjg").toString())) {
			Map<String,Object> params2 = new HashMap<String,Object>();
			params2.put("person_id", params.get("person_id"));
			params2.put("type", "301");
			fzdyBzxxHandle.update2(params2, session);
			fzdyBzxxHandle.update1(params2, session);
			db().delete("delete from fzdy_bzxx where person_id=? and type=302", new Object[] {params.get("person_id")});
		}else {
			//审核通过后就自动进入下一步
			fzdyBzxxHandle.nextStage(params, session);
		}
		//更新审核人的待办为已办
		db().update("update my_to_do_list set ishandle=1 where module=4 and userid=? and busid=?", new Object[] {session.getUserid(),Constant.THREE});
		return num;
	}
}
