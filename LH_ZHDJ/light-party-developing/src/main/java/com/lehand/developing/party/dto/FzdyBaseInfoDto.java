package com.lehand.developing.party.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description= "返回响应数据")
public class FzdyBaseInfoDto {
	@ApiModelProperty(value = "发展党员id")
	private String userid;//党员(发展党员id)
	@ApiModelProperty(value = "所在支部id")
	private String organid;//所在支部id
	@ApiModelProperty(value = "姓名")
	private String xm;//姓名
	@ApiModelProperty(value = "性别")
	private String xb;//性别
	@ApiModelProperty(value = "民族")
	private String mz;//民族
	@ApiModelProperty(value = "证件号码(身份证号)")
	private String zjhm;//证件号码(身份证号)
	@ApiModelProperty(value = "出生日期")
	private String csrq;//出生日期
	@ApiModelProperty(value = "籍贯")
	private String jg;//籍贯
	@ApiModelProperty(value = "党员类别")
	private String dylb = "5";//党员类别(默认申请入党积极分子)
	@ApiModelProperty(value = "联系电话")
	private String lxdh;//联系电话
	@ApiModelProperty(value = "学历")  
	private String xl; //学历
	@ApiModelProperty(value = "现居地址")
	private String xjzd;//现居地址
	@ApiModelProperty(value = "头像")
	private String photourl;//头像
	@ApiModelProperty(value = "所在单位")
	private String dydaszdw;//所在单位
	@ApiModelProperty(value = "创建时间")
	private String createtime;
	@ApiModelProperty(value = "工作岗位")
	private String gzgw;

	@ApiModelProperty(value = "年龄")
	private Integer age;//年龄
	@ApiModelProperty(value = "毕业时间")
	private String bysj;//毕业时间
	@ApiModelProperty(value = "毕业院校及专业")
	private String byyx;//毕业院校及专业
	@ApiModelProperty(value = "学位")
	private String xw;//学位
	@ApiModelProperty(value = "工作时间")
	private String cjgzrq;//工作时间
	@ApiModelProperty(value = "技术职称")
	private String jszc;//技术职称
	@ApiModelProperty(value = "最高学历")
	private String zgxl;//最高学历
	@ApiModelProperty(value = "更新时间")
	private String updatetime;//更新时间
	
	public String getGzgw() {
		return gzgw;
	}
	public void setGzgw(String gzgw) {
		this.gzgw = gzgw;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getOrganid() {
		return organid;
	}
	public void setOrganid(String organid) {
		this.organid = organid;
	}
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}
	public String getXb() {
		return xb;
	}
	public void setXb(String xb) {
		this.xb = xb;
	}
	public String getMz() {
		return mz;
	}
	public void setMz(String mz) {
		this.mz = mz;
	}
	public String getZjhm() {
		return zjhm;
	}
	public void setZjhm(String zjhm) {
		this.zjhm = zjhm;
	}
	public String getCsrq() {
		return csrq;
	}
	public void setCsrq(String csrq) {
		this.csrq = csrq;
	}
	public String getJg() {
		return jg;
	}
	public void setJg(String jg) {
		this.jg = jg;
	}
	public String getDylb() {
		return dylb;
	}
	public void setDylb(String dylb) {
		this.dylb = dylb;
	}
	public String getLxdh() {
		return lxdh;
	}
	public void setLxdh(String lxdh) {
		this.lxdh = lxdh;
	}
	public String getXl() {
		return xl;
	}
	public void setXl(String xl) {
		this.xl = xl;
	}
	
	public String getXjzd() {
		return xjzd;
	}
	public void setXjzd(String xjzd) {
		this.xjzd = xjzd;
	}
	public String getPhotourl() {
		return photourl;
	}
	public void setPhotourl(String photourl) {
		this.photourl = photourl;
	}
	public String getDydaszdw() {
		return dydaszdw;
	}
	public void setDydaszdw(String dydaszdw) {
		this.dydaszdw = dydaszdw;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getBysj() {
		return bysj;
	}

	public void setBysj(String bysj) {
		this.bysj = bysj;
	}

	public String getByyx() {
		return byyx;
	}

	public void setByyx(String byyx) {
		this.byyx = byyx;
	}

	public String getXw() {
		return xw;
	}

	public void setXw(String xw) {
		this.xw = xw;
	}

	public String getCjgzrq() {
		return cjgzrq;
	}

	public void setCjgzrq(String cjgzrq) {
		this.cjgzrq = cjgzrq;
	}

	public String getJszc() {
		return jszc;
	}

	public void setJszc(String jszc) {
		this.jszc = jszc;
	}

	public String getZgxl() {
		return zgxl;
	}

	public void setZgxl(String zgxl) {
		this.zgxl = zgxl;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
}
