package com.lehand.developing.party.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.developing.party.dto.FzdyPartyLinkDto2;
import com.lehand.developing.party.dto.FzdyPartyLinkDto3;
import com.lehand.developing.party.pojo.FzdyPartyLink;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.developing.party.dto.FzdyBaseInfoDto;
import com.lehand.developing.party.service.FzdyBaseInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(value = "发展党员管理", tags = { "发展党员管理" })
@RestController
@RequestMapping("/developing/baseinfo")
public class FzdyBaseInfoController extends FzdyBaseController{
	
	private static final Logger logger = LogManager.getLogger(FzdyBaseController.class);
	@Resource FzdyBaseInfoService infoService ;
	
	/**
	 * 发展党员分页列表
	 * @param pager
	 * @param type
	 * @param nameStr
	 * @param codeStr
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员管理分页", notes = "发展党员管理分页", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "pageNo", value = "当前页码/必填", required = true, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "pageSize", value = "每页条数/必填", required = true, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "type", value = "类型 5:申请入党积极分子,4:入党积极分子,3:发展党员,2:预备党员,1:正式党员,-1:作废", required = false, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "nameStr", value = "姓名关键字", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "codeStr", value = "身份证号关键字", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/queryPageFzdy")
	public Message queryPageFzdy(Pager pager,String type,String nameStr,String codeStr) {
		Message msg = new Message();
		try {
			msg.setData(infoService.queryPageFzdy(pager,type,nameStr,codeStr,getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员保存", notes = "发展党员保存", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "xm", value = "姓名", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "xb", value = "性别", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "zjhm", value = "证件号码(身份证号)", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "mz", value = "民族", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "xl", value = "学历", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "age", value = "年龄", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "jg", value = "籍贯", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "bysj", value = "毕业时间", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "byyx", value = "毕业院校及专业", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "xw", value = "学位", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "zgxl", value = "最高学历", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "cjgzrq", value = "工作时间", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "gzgw", value = "工作岗位", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "jszc", value = "技术职称", required = false, paramType = "query", dataType = "String", defaultValue = ""), @ApiImplicitParam(name = "gzgw", value = "获得时间", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "lxdh", value = "联系电话", required = true, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "xjzd", value = "现居住地址", required = true, paramType = "query", dataType = "String", defaultValue = ""),

			@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "userid", value = "党员(发展党员id)", required = false, paramType = "query", dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "organid", value = "所在支部id", required = true, paramType = "query", dataType = "String", defaultValue = ""),




//		@ApiImplicitParam(name = "csrq", value = "出生日期", required = true, paramType = "query", dataType = "String", defaultValue = ""),

//		@ApiImplicitParam(name = "dylb", value = "党员类别(1:正式党员,2 预备党员,3 发展对象,4入党积极分子 , 默认5)", required = true, paramType = "query", dataType = "String", defaultValue = ""),



		@ApiImplicitParam(name = "photourl", value = "头像", required = true, paramType = "query", dataType = "String", defaultValue = ""),
//		@ApiImplicitParam(name = "dydaszdw", value = "党员档案管理单位", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/saveFzdy")
	public Message saveFzdy(@ApiIgnore FzdyBaseInfoDto info) {
		Message msg = new Message();
		try {
			infoService.save(info,getSession());
			msg.success();
		} catch (Exception e) {
			msg.setMessage("保存失败！"+e.getMessage());
		}
		return msg;
	}

	@OpenApi(optflag=3)
	@ApiOperation(value = "发展党员信息作废", notes = "发展党员信息作废", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "userid", value = "发展党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/delFzdy")
	public Message delFzdy(String userid) {
		Message msg = new Message();
		try {
			infoService.delFzdy(3,userid);
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员信息详情", notes = "发展党员信息详情", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "userid", value = "发展党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/queryFzdyById")
	public Message queryFzdyById(String userid) {
		Message msg = new Message();
		try {
			msg.setData(infoService.queryFzdyById(userid));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员信息恢复", notes = "发展党员信息恢复", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "userid", value = "发展党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/recoverFzdy")
	public Message recoverFzdy(String userid) {
		Message msg = new Message();
		try {
			infoService.delFzdy( 2,userid);
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员组织关系转接 转出", notes = "发展党员组织关系转接 转出", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "userid", value = "发展党员id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "type", value = "转出类别 1:集团内,2:集团外", required = true, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "organid", value = "目标组织机构id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/fzdyZzgxzj")
	public Message fzdyZzgxzj(String userid,int type,String organid) {
		Message msg = new Message();
		try {
			infoService.fzdyZzgxzj(userid,type,organid,getSession());
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 * 发展党员组织关系转接接收
	 * @param userid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员组织关系转接审核", notes = "发展党员组织关系转接审核", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "userid", value = "发展党员id", required = true, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "status", value = "审核结果 1:通过,2:不通过", required = true, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "remark", value = "备注", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/receiveFzdy")
	public Message receiveFzdy(String userid,int status,String remark) {
		Message msg = new Message();
		try {
			infoService.receiveFzdy(userid,status,remark,getSession());
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员组织关系转接列表", notes = "发展党员组织关系转接列表", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "pageNo", value = "当前页码/必填", required = true, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "pageSize", value = "每页条数/必填", required = true, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "type", value = "类型 5:申请入党积极分子,4:入党积极分子,3:发展党员,2:预备党员,1:正式党员,-1:作废", required = false, paramType = "query", dataType = "int", defaultValue = ""),
		@ApiImplicitParam(name = "nameStr", value = "姓名关键字", required = false, paramType = "query", dataType = "String", defaultValue = ""),
		@ApiImplicitParam(name = "codeStr", value = "身份证号关键字", required = false, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/queryZzgxAccept")
	public Message queryZzgxAccept(Pager pager,String type,String nameStr,String codeStr) {
		Message msg = new Message();
		try {
			msg.setData(infoService.queryZzgxAccept(pager,type,nameStr,codeStr,getSession()));
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	
	/**
	 * 党员类别下拉
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "党员类别下拉列表", notes = "党员类别下拉列表", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/listDylbSel")
	public Message listDylbSel() {
		Message msg = new Message();
		try {
			List<Map<String,Object>> list = infoService.listDylbSel(getSession(),"d_dy_rylb");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("name", "全部阶段");
			map.put("id","");
			list.add(map);
			msg.setData(list);
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 * 新增下拉
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "新增选项集合", notes = "新增的下拉和ridio", httpMethod = "POST")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "sessionid", value = "系统sessinid", required = true, paramType = "header", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/listAddSel")
	public Message listAddSel() {
		Message msg = new Message();
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			map.put("xb", infoService.listDylbSel(getSession(),"d_sexid"));
			map.put("xl", infoService.listDylbSel(getSession(),"d_dy_xl"));
			map.put("mz", infoService.listDylbSel(getSession(),"d_national"));
			map.put("xwDm", infoService.listDylbSel(getSession(),"d_dy_xw"));
			map.put("xshjclx_dm",infoService.listDylbSel(getSession(), "d_dy_shjc"));
			Map<String,Object> maps = new HashMap<String,Object>();
//			maps.put("id", getSession().getOrganids().get(0).getOrgid());
//			maps.put("name",getSession().getOrganids().get(0).getOrgname());
			maps.put("id", getSession().getCurrentOrg().getOrgid());
			maps.put("name",getSession().getCurrentOrg().getOrgname());
			map.put("dzz", maps);
			msg.setData(map);
			msg.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return msg;
	}

	/**
	* @Description //获取发展党员5环节数据
	* @Author zwd
	* @Date 2020/10/31 14:48
	* @param
	* @return
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取发展党员5环节数据", notes = "获取发展党员5环节数据", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userid", value = "发展党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/listlink")
	public Message getFzdyPartyLink(String userid){
		Message message = new Message();
		message.setData(infoService.getFzdyPartyLink(userid,getSession())).success();
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "修改发展党员5环节（各环节数据都要带着）", notes = "修改发展党员5环节", httpMethod = "POST")
	@RequestMapping("/updatelink")
	public Message updateFzdyPartyLink(@RequestBody  FzdyPartyLinkDto3 bean, String savetype){
		Message message = new Message();
		try {
			message.setData(infoService.updateFzdyPartyLink(bean,savetype,getSession())).success();
		}catch (Exception e){
			message.setMessage(e.getMessage());
		}
		return message;
	}
	/**
	 * @Description 发展党员：正式党员步骤延迟党员预备期
	 * @Author lx
	 * @Date 2021/3/27
	 * @param
	 * @return
	 **/
	@OpenApi(optflag=0)
	@ApiOperation(value = "发展党员：正式党员步骤延迟党员预备期", notes = "发展党员：正式党员步骤延迟党员预备期", httpMethod = "POST")
	@RequestMapping("/addHisLink")
	public Message addHisLink(@RequestBody  FzdyPartyLinkDto3 bean){
		Message message = new Message();
		try {
			infoService.addHisLink(bean,getSession());
			message.success();
		}catch (Exception e){
			message.setMessage(e.getMessage());
		}
		return message;
	}
	/**
	 * @Description 发展党员：正式党员步骤延迟党员预备期
	 * @Author lx
	 * @Date 2021/3/27
	 * @param
	 * @return
	 **/
	@OpenApi(optflag=0)
	@ApiOperation(value = "分页查询发展党员：正式党员步骤延迟党员预备期", notes = "分页查询发展党员：正式党员步骤延迟党员预备期", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userid", value = "当前党员id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/pageHisLink")
	public Message pageHisLink(Pager pager,String userid){
		Message message = new Message();
		try {
			message.setData(infoService.pageHisLink(pager,getSession(),userid));
			message.success();
		}catch (Exception e){
			message.setMessage(e.getMessage());
		}
		return message;
	}
	/**
	* @Description 导出发展党员
	* @Author zwd
	* @Date 2020/11/3 14:31
	* @param 
	* @return 
	**/
	@OpenApi(optflag=0)
	@ApiOperation(value = "导出发展党员", notes = "导出发展党员", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orgid", value = "支部id", required = true, paramType = "query", dataType = "String", defaultValue = "")
	})
	@RequestMapping("/exportFzdy")
	public Message exportFzdy(HttpServletResponse response,@RequestBody Map<String,String> data){
		Message message = new Message();

		message.setData(infoService.exportFzdy(response,data.get("orgid"),data.get("stageid"),data.get("name"),data.get("idcard"),getSession())).success();
		return message;
	}

}
