package com.lehand.developing.party.service.fzdy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.developing.party.utils.SessionUtils;
import org.springframework.stereotype.Service;

import com.greenpineyu.fel.common.StringUtils;
import com.lehand.base.common.Pager;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
@Service
public class BaseStep {

	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	@Resource
	protected SessionUtils sessionUtils;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}

	public Pager page(Pager pager, Map<String, Object> params, Session session) {
		return null;
	}

	public Map<String, Object> get(Map<String, Object> params, Session session) {
		return null;
	}

	public int delete(Map<String, Object> params, Session session) {
		return 0;
	}
	
	public int save(Map<String, Object> params, Session session) {
		return 0;
	}
	
	//102 207 208 209 303 304 306 504   505 表格步骤id
	public void saveFile(Map<String, Object> params) {
		if(params.get("file")!=null) {
			params.put("stepid", params.get("step"));
			params.put("fileids", params.get("file"));
			db().delete("delete from fzdy_special where  person_id=:person_id and stepid=:step", params);
			if(!StringUtils.isEmpty(params.get("file").toString())) {
				db().insert("INSERT INTO fzdy_special (person_id, stepid, fileids) VALUES (:person_id, :stepid, :fileids)", params);
			}
		}
	}

	/**
	 * 处理附件信息
	 * @param map
	 * @param session
	 */
	protected  void makeFiles(Map<String,Object> map,Session session){
		if(map!=null) {
			String fileids = String.valueOf(map.get("file"));
			List<Map<String, Object>> files = new ArrayList<Map<String,Object>>(0);
			if(!org.springframework.util.StringUtils.isEmpty(fileids)) {
				files = db().listMap("select * from dl_file where compid=? and id in ("+fileids+")", new Object[] {session.getCompid()});
			}
			map.put("files", files);
		}else{
			
		}
	}
}
