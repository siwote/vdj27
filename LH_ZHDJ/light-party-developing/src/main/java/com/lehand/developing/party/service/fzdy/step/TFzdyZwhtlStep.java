package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyZwhtlStep extends BaseStep {
	

	//person_id 
	//type
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_zwhtl where type=:type and person_id=:person_id and syncstatus='A' and compid=:compid", params);
		return map;
	}
	

	//type
	//zwhkssj
	//zwhdd
	//zwhzcr
	//zwhjlr
	//zwhtljg
	//zwhyj
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("branch_disc_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("created_by", session.getUserid());
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("delete_flag", "0");
		params.put("backflag", "1");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("zwhsbbarq")==null) {
			params.put("zwhsbbarq", Constant.ENPTY);
		}
		if(params.get("zwhyj")==null) {
			params.put("zwhyj", Constant.ENPTY);
		}
		if(params.get("zwhtlfj")==null) {
			params.put("zwhtlfj", Constant.ENPTY);
		}
		if(params.get("zwhjssj")==null) {
			params.put("zwhjssj", Constant.ENPTY);
		}
		int num = 0;
		num = db().delete("UPDATE t_fzdy_zwhtl SET syncstatus='D' WHERE type=:type and person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		num += db().insert("INSERT INTO t_fzdy_zwhtl (compid, branch_disc_id, type, zwhkssj, zwhdd, zwhzcr, zwhjlr, zwhtljg, zwhyj, zwhsbbarq, zwhjssj, zwhtlfj, created_by, create_date, delete_flag, person_id, backflag, syncstatus, synctime, file)" + 
				" VALUES (:compid,:branch_disc_id,:type,:zwhkssj,:zwhdd,:zwhzcr,:zwhjlr,:zwhtljg,:zwhyj,:zwhsbbarq,:zwhjssj,:zwhtlfj,:created_by,:create_date,:delete_flag,:person_id,:backflag,:syncstatus,:synctime,:file)", params);
		
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		saveFile(params);
		
		return num;
	}
	
	
}
