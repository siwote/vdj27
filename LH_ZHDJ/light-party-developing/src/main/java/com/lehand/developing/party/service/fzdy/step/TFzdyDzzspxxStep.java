package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;


@Service
public class TFzdyDzzspxxStep extends BaseStep {
	
	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_dzzspxx where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		makeFiles(map,session);
		return map;
	}
	
	
    //sbsjdzzsprq
	//spjg
	//sbsjdwspyj
	//person_id
	//file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("generala_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		
		int num = db().delete("UPDATE t_fzdy_dzzspxx SET syncstatus='D' WHERE person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_dzzspxx (compid, generala_id, sbsjdzzsprq, spjg, sbsjdwspyj, created_by, updated_by, create_date, last_update_date, version, delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:generala_id,:sbsjdzzsprq,:spjg,:sbsjdwspyj,:created_by,:updated_by,:create_date,:last_update_date,:version,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		//插入数据完成后就得更新下审核表对应数据的状态
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("status", params.get("syjg"));
		paramMap.put("person_id", params.get("person_id"));
		paramMap.put("audit_id", session.getUserid());
		db().update("update fzdy_dzzsp set status=:status where compid=:compid and person_id=:person_id and audit_id=:audit_id", paramMap);
		
		saveFile(params);
		
		return num;
	}
}
