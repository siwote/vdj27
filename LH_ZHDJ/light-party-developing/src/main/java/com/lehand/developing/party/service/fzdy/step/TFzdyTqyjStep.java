package com.lehand.developing.party.service.fzdy.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.common.constant.Constant;


@Service
public class TFzdyTqyjStep extends BaseStep {

	//person_id 
	public Map<String, Object> get(Map<String, Object> params, Session session) {
		Map<String, Object> map = db().getMap("select * from t_fzdy_tqyj where person_id=:person_id and syncstatus='A' and compid=:compid", params);
		makeFiles(map,session);
		return map;
	}
	

	//	tqyjly
	//	tqsj
	//	tqyjnr
	//  pylxryj
	//	person_id
	//	file
	@Transactional(rollbackFor = Exception.class)
	public int save(Map<String, Object> params, Session session) {
		params.put("listen_id", UUID.randomUUID().toString().replaceAll("-", ""));
		params.put("create_date", DateEnum.YYYYMMDDHHMMDD.format());
		params.put("created_by", session.getUserid());
		params.put("delete_flag", "0");
		params.put("syncstatus", "A");
		params.put("synctime", DateEnum.YYYYMMDDHHMMDD.format());
		if(params.get("dxzyj")==null) {
			params.put("dxzyj", Constant.ENPTY);
		}
		if(params.get("dyhqzdbyj")==null) {
			params.put("dyhqzdbyj", Constant.ENPTY);
		}
		int num = db().delete("UPDATE t_fzdy_tqyj SET syncstatus='D' WHERE  person_id=:person_id and syncstatus='A' and compid=:compid ", params);
		
		num += db().insert("INSERT INTO t_fzdy_tqyj (compid, listen_id, tqsj, tqyjly, tqyjnr, pylxryj, dxzyj, dyhqzdbyj, created_by, create_date, delete_flag, person_id, syncstatus, synctime, file)" + 
				" VALUES (:compid,:listen_id,:tqsj,:tqyjly,:tqyjnr,:pylxryj,:dxzyj,:dyhqzdbyj,:created_by,:create_date,:delete_flag,:person_id,:syncstatus,:synctime,:file)", params);
		
		Map<String, Object> map = db().getMap("select * from fzdy_bzxx where compid=:compid and type=:step and person_id=:person_id", params);
		if(map!=null && map.get("sfipen").toString().equals("0")) {
			db().update("update fzdy_bzxx set uneditable=1,sfipen=1 where compid=:compid and person_id=:person_id and type=:step", params);
		}
		saveFile(params);
		
		return num;
	}
}
