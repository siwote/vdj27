package com.lehand.developing.party.service.dfgl.dfsjgl;

import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;

import com.lehand.base.exception.LehandException;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;


/**
 * Description: 党费基数上交比率配置
 * @author pantao
 *
 */
@Service
public class DfglJsszHandle {
	
	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	/**
	 * Description: 党费基数上交比率配置 （前端控制min max 必须输入整数）
	 * @author PT  
	 * @date 2019年8月14日 
	 * @param params （flag 1新增 0修改；min;max;ratio;flag=0时带上id）
	 * @param session
	 */
	public void save(Map<String, Object> params, Session session) {
		int flag = Integer.parseInt(params.get("flag").toString());
		String max = params.get("max").toString();
		String min = params.get("min").toString();
		Long compid = session.getCompid();
		params.put("compid", compid);
		
		//最小值为空就赋值为0
		if("".equals(min)) {
			min = "0";
			params.put("min", min);
		}
		//如果最大值为空（默认给最大值赋值1000000）
		if("".equals(max)) {
			max = "1000000";
			params.put("max", max);
		}
		
		//最大值必须大于最小值
		if(Integer.parseInt(max)<=Integer.parseInt(min)) {
			LehandException.throwException("最大值必须大于最小值");
		}
		
		if(flag==1) {//新增
			//如果最小值为空（查询数据库最小值为0的记录是否存在如果存在保存失败，不存在就给最小值赋值为0）
			if("".equals(min)) {
				Map<String,Object> map2 = db().getMap("SELECT * FROM t_dfgl_jssz where compid=? and min=0", new Object[] {compid});
				if(map2!=null) {
					LehandException.throwException("最小值不能为空");
				}
			}
			//查询数据库最大的最大值的记录如果存在就需要判断新增的数据的最小值必须大于当前最大值
			Map<String,Object> map = db().getMap("SELECT * FROM t_dfgl_jssz where compid=? order by max desc limit 1", new Object[] {compid});
			if(!"".equals(min) && map!=null ) {
				int max2 = Integer.parseInt(map.get("max").toString());
				if(Integer.parseInt(min)<max2) {
					LehandException.throwException("最小值必须大于已有数据的最大值");
				}
			}
			db().insert("insert into t_dfgl_jssz (compid,min,max,ratio) values (:compid,:min,:max,:ratio)", params);
		}else {//修改
			Long id = Long.valueOf(params.get("id").toString());
			if("".equals(min)) {
				Map<String,Object> map = db().getMap("SELECT * FROM t_dfgl_jssz where compid=? and min=0 and id!=?", new Object[] {compid,id});
				if(map!=null) {
					LehandException.throwException("最小值不能为空");
				}
			}
			//查询修改的记录对象
			Map<String,Object> map = db().getMap("SELECT * FROM t_dfgl_jssz where compid=? and id=?", new Object[] {compid,id});
			Long selfmin = Long.valueOf(map.get("min").toString());
			Long selfmax = Long.valueOf(map.get("max").toString());
			//该修改的记录的上一条记录
			Map<String,Object> mapup = db().getMap("SELECT * FROM t_dfgl_jssz where compid=? and max<=? order by max desc limit 1", new Object[] {compid,selfmin});
			if(mapup!=null) {
				if(Integer.parseInt(min)<Integer.parseInt(mapup.get("max").toString())) {
					LehandException.throwException("最小值不得小于上一记录的最大值");
				}
			}
			//该修改记录的下一条记录
			Map<String,Object> mapdown = db().getMap("SELECT * FROM t_dfgl_jssz where compid=? and min>=? order by max asc limit 1", new Object[] {compid,selfmax});
			if(mapdown!=null) {
				if(Integer.parseInt(max)>Integer.parseInt(mapdown.get("min").toString())) {
					LehandException.throwException("最大值不得大于下一记录的最小值");
				}
			}
			db().update("update t_dfgl_jssz set min=:min,max=:max,ratio=:ratio where compid=:compid and id=:id", params);
		}
		
	}
	
	
	public void delete(Map<String, Object> params, Session session) {
		Long compid = session.getCompid();
		String id = params.get("id").toString();
		db().delete("delete from t_dfgl_jssz where compid=? and id=?", new Object[] {compid,id});
	}

}
