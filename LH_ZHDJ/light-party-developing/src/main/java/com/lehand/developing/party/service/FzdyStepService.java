package com.lehand.developing.party.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.lehand.base.common.Session;
import com.lehand.developing.party.common.sql.SqlCode;
import com.lehand.developing.party.dto.FzdyPartyLinkDto2;
import com.lehand.developing.party.dto.TDzzBzcyxxDto;
import com.lehand.developing.party.utils.SessionUtils;
import com.lehand.document.lib.service.FileDelService;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.BeanUtil;
import com.lehand.base.util.CommonUtil;
import com.lehand.base.util.SpringUtil;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.developing.party.service.fzdy.BaseStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyClgdStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyClsqStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDwbsStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDwspStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDwysStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDxztlqkStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDzzsyxxStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyDzzthStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyFzdxStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyJzpxStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyKcStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyPyjyqkStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyPyjyxxStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyPylxrStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyRdjsrStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyRdsqsStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyRdxsStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyRdzysStep;
import com.lehand.developing.party.service.fzdy.step.TFzdySbdwspStep;
import com.lehand.developing.party.service.fzdy.step.TFzdySbsjdwbaxxStep;
import com.lehand.developing.party.service.fzdy.step.TFzdySjdwbaxxStep;
import com.lehand.developing.party.service.fzdy.step.TFzdySjdzzspStep;
import com.lehand.developing.party.service.fzdy.step.TFzdySpecialStep;
import com.lehand.developing.party.service.fzdy.step.TFzdySxhbStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyTjtyStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyTqyjStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyWsybdyStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyYbdyStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyYbdyspStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyYsyjStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZbdhStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZbdhtlStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZbzqyjStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZsqkStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZwhscStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZwhtlStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZzgsStep;
import com.lehand.developing.party.service.fzdy.step.TFzdyZzsqStep;

@Service
public class FzdyStepService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;
	@Resource
	private SessionUtils sessionUtils;

	@Resource FzdyBaseInfoService fzdyBaseInfoService;

	@Resource
    FileDelService fileDelService;
	
	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	
	 @Resource private DFSComponent dFSComponent;
	
	//正式党员code
	@Value("${jk.manager.down_ip}")
	private String down_ip;
	
	
	//附件地址
	@Value("${jk.organ.zhengshidangyuan}")
	private String zhengshidangyuan;
	
	public Object process(Map<String, Object> params, Session session) throws Exception {
		String stepCode = CommonUtil.assertString(params.get("step"), "step不能为空");
		Step step = Step.valueOf("$"+stepCode);
		if (null==step) {
			LehandException.throwException(stepCode+"步骤没有找到处理方式");
		}
//		params.put("compid", session.getCompid());
		params.put("compid", 1);
		Object result = null;
		BaseStep handler = step.getStepHandler();
		String optflag = CommonUtil.assertString(params.get("optflag"), "optflag不能为空");
		if ("page".equals(optflag)) {
			Pager pager = new Pager();
			BeanUtil.map2bean(params, pager);
			result = handler.page(pager, params, session);
		} else if ("get".equals(optflag)) {
			result = handler.get(params, session);
		} else if ("save".equals(optflag)) {
			result = handler.save(params, session);
		} else if ("delete".equals(optflag)) {
			result = handler.delete(params, session);
		} 
		return result;
	}
	
	public enum Step {
		//第1步
		$101("递交入党申请书",TFzdyRdsqsStep.class),
		
		//第2步
		$102("党组织派人谈话",TFzdyDzzthStep.class),
		
		//第3步
		$201("推荐推优记录",TFzdyTjtyStep.class),
		
		//第4步
		$202("支委会/党员大会决议",TFzdyZwhtlStep.class),
		
		//第5步
		$203("上级党委备案信息",TFzdySbsjdwbaxxStep.class),
		
		//第6步
		$204("党总支审议",TFzdyDzzsyxxStep.class),
		
		//第7步
		$205("上级党委备案信息",TFzdySjdwbaxxStep.class),
		
		//第8步
		$206("指定培养联系人",TFzdyPylxrStep.class),
		
		//第9步
		$207("思想汇报",TFzdySxhbStep.class),
		
		//第10步
		$208("培养教育情况",TFzdyPyjyxxStep.class),
		
		//第11步
		$209("半年度考核",TFzdyKcStep.class),
		
		//第11步
		$210("培养教育考察表",TFzdySpecialStep.class),
		
		//第13步
		$211("听取意见",TFzdyTqyjStep.class),
		
		//第14步
		$212("支委会/党员大会讨论",TFzdyZwhtlStep.class),
		
		//第15步
		$213("上报上级党委备案信息",TFzdyDwbsStep.class),
		
		//第16步
		$214("党总支审议",TFzdyDzzsyxxStep.class),
		
		//第17步
		$215("上级党委备案信息",TFzdySjdwbaxxStep.class),
		
		//第18步
		$301("确定入党介绍人",TFzdyRdjsrStep.class),
		
		//第19步
		$302("政治审查",TFzdyZsqkStep.class),
		
		//第20步
		$303("思想汇报",TFzdySxhbStep.class),
		
		//第21步
		$304("培养教育情况",TFzdyPyjyxxStep.class),
		
		//第21步
		$305("发展对象培养考察表 ",TFzdySpecialStep.class),
		
		//第23步
		$306("开展集中培训",TFzdyJzpxStep.class),
		
		//第24步
		$307("发展对象公示",TFzdyFzdxStep.class),
		
		//第25步
		$401("支委会/党员大会预审",TFzdyYbdyStep.class),
		
		//第26步
		$402("支委会/党员大会上报预审",TFzdyYsyjStep.class),
		
		//第27步
		$403("党总支审查",TFzdyDzzsyxxStep.class),
		
		//第28步
		$404("上报党委预审",TFzdyDwysStep.class),
		
		//第29步
		$405("填写入党志愿书",TFzdyRdzysStep.class),
		
		//第30步
		$406("支部大会讨论",TFzdyZbdhStep.class),
		
		//第31步
		$407("上报上级党组织审批",TFzdySjdzzspStep.class),
		
		//第32步
		$408("党总支审议信息",TFzdyDzzsyxxStep.class),
		
		//第33步
		$409("党委审批信息",TFzdyDwspStep.class),
		
		//第34步
		$501("完善信息",TFzdyWsybdyStep.class),
		
		//第35步
		$502("完善信息",TFzdyRdxsStep.class),
		
		//第21步
		$503("预党员基本信息 ",TFzdySpecialStep.class),
		
		//第37步
		$504("思想汇报",TFzdySxhbStep.class),
		
		//第38步
		$505("继续教育考察",TFzdyPyjyqkStep.class),
		
		//第39步
		$506("转正申请",TFzdyZzsqStep.class),
		
		//第40步
		$507("党小组讨论情况",TFzdyDxztlqkStep.class),
		
		//第41步
		$508("支部征求意见",TFzdyZbzqyjStep.class),
		
		//第42步
		$509("党委会/党员大会审查",TFzdyZwhscStep.class),
		
		//第21步
		$510("预备党员教育考察表 ",TFzdySpecialStep.class),
				
		//第44步
		$511("转正公示",TFzdyZzgsStep.class),
		
		//第45步
		$512("支部大会讨论",TFzdyZbdhtlStep.class),
		
		//第46步
		$513("上报上级党组织审批",TFzdySbdwspStep.class),
		
		//第47步
		$514("上报党总支审议",TFzdyDzzsyxxStep.class),
		
		//第48步
		$515("党委转正审批",TFzdyYbdyspStep.class),
		
		//第49步
		$516("材料收取",TFzdyClsqStep.class),
		
		//第50步
		$517("后续工作",TFzdySbdwspStep.class),//TODO 2019-08-14
		
		//第51步
		$518("材料归档 ",TFzdyClgdStep.class);
		
		
		private String step;
		private Class<? extends BaseStep> clazz;
		
		private Step(String step,Class<? extends BaseStep> clazz) {
			this.step = step;
			this.clazz = clazz;
		}
		public BaseStep getStepHandler() {
			return SpringUtil.getBean(this.clazz);
		}
		public String getStep() {
			return step;
		}
		public Class<? extends BaseStep> getClazz() {
			return clazz;
		}
	}

	public List<Map<String, Object>> list(Session session,String name) {
//		String organid = session.getCustom();
		String organid = 	sessionUtils.getOrganidByOrganCode(session.getCurrentOrg().getOrgcode());
		List<Map<String, Object>> list = null;
		if(StringUtils.isEmpty(name)) {
			list = db().listMap("select xm,userid from t_dy_info  where organid = ? and operatetype != 3 and dylb=?", new Object[] {organid,zhengshidangyuan});
		}else {
			list = db().listMap("select xm,userid from t_dy_info  where organid = ? and operatetype != 3 and dylb=? and xm like '%"+name+"%'", new Object[] {organid,zhengshidangyuan});
		}
		return list;
	}

	public Map<String, Object> get(Session session, String userid) {
		Map<String, Object> map = db().getMap("select a.userid,a.xm,a.xb,a.zjhm,a.mz,a.xl,a.age,a.jg,b.bysj,b.byyx,b.xw,b.zgxl,b.cjgzrq,a.gzgw,b.jszc,a.lxdh,a.xjzd,b.photourl FROM t_dy_info a left join t_dy_info_bc b on a.userid=b.userid where a.userid=?", new Object[] {userid});
		Map<String, Object> file = db().getMap("select * from dl_file where id=(select photourl from t_dy_info_bc where userid=? )", new Object[] {userid});
		if(file!=null) {
			file.put("dir", down_ip+file.get("dir"));
		}
		map.put("file", file);
		return map;
	}

	public List<Map<String, Object>> listGzgw() {
		return db().listMap("SELECT uuid,item_name FROM g_dict_item where code = 'd_dy_gzgw' and uuid in (1000000001,1020300007,1020400008,1020500009,1020600010,1020700011,10208000125000000040,5010000041,5020000042,5030000043,6000000045,6110000052,1020000055,6030000056,1030000013,1030100014,1030200015)", new Object[] {});
	}
	
	
	
	/**
	 * Description: 发展党员资料导出
	 * @author PT  
	 * @date 2019年9月17日 
	 * @param person_id
	 * @param stepids 附件的id集合 
	 * @return
	 */
	public String fileExport(String person_id,String stepids) {
		Map<String, Object> maps = db().getMap("select xm from t_dy_info where userid=?", new Object[] {person_id});
		List<Map<String, Object>> data = db().listMap("select * from dl_file where id in ("+stepids+")", new Object[] {});
		List<File> srcFiles = new ArrayList<File>();
		for (Map<String, Object> map : data) {
			String path = down_ip+map.get("dir").toString();
			File file;
			try {
				file = downLoadFromUrl(path,map.get("name").toString().substring(0,map.get("name").toString().length()-4)+System.currentTimeMillis()+".pdf", "temp");//只能上传pdf,导出的都是pdf
//                file = downLoadFromUrl(path,map.get("name").toString().substring(0,map.get("name").toString().length()-4)+System.currentTimeMillis()+"."+map.get("type"), "temp");//导出文件的是上传的文件格式
                srcFiles.add(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String pathname = "\\"+person_id+".zip";
		File zipFile = new File(pathname);
		// 调用压缩方法
		zipFiles(srcFiles, zipFile);
		String path = "";
		try {
			path = dFSComponent.upload(pathname);
			for (File file : srcFiles) {
				file.delete();
			}
			zipFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return down_ip+path+"?download=true&attname="+maps.get("xm")+"的资料包.zip";
		
	}
	
	
	
	
	/**
	 * Description:
	 * @author PT  
	 * @date 2019年9月30日 
	 * @param files 要合成文件的路径["E:/pdf/交控2期第二次提测内容 .pdf","E:/pdf/交控2期第一次提测内容.pdf"]
	 * @param newfile 需要保存文件的路径 D:\\temp.pdf
	 * @return
	 */
	public String mergePdfFiles(String[] files, String newfile) {  
        boolean retValue = false;  
        Document document = null;
        try {  
            document = new Document(new PdfReader(files[0]).getPageSize(1));
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(newfile));
            document.open();  
            for (int i = 0; i < files.length; i++) {  
                PdfReader reader = new PdfReader(files[i]);  
                int n = reader.getNumberOfPages();  
                for (int j = 1; j <= n; j++) {  
                    document.newPage();  
                    PdfImportedPage page = copy.getImportedPage(reader, j);
                    copy.addPage(page);  
                }  
            }  
            retValue = true;  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            document.close();  
        }  
        System.out.println(retValue);
        return newfile;  
    } 
	
	
	/**
	 * Description: 党员附件导出列表查询
	 * @author PT  
	 * @date 2019年9月29日 
	 * @param person_id
	 * @return
	 */
	public List<Map<String,Object>> listFiles(String person_id,Session session) {

		//获取5环节信息
		FzdyPartyLinkDto2 fzdy = generalSqlComponent.query(SqlCode.getFzdyPartyLink,new Object[] {person_id} );

		String files1 = "";
		String files2 = "";
		String files3 = "";
		String files4 = "";
		String files5 = "";
		String files6 = "";
		//拼接附件id
		if(fzdy!=null){
			if(!StringUtils.isEmpty(fzdy.getApplyFiles())){
				files1+=fzdy.getApplyFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getActivistFiles())){
				files2+=fzdy.getActivistFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getDevelopFiles())){
				files3+=fzdy.getDevelopFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getReadyPoliticalReviewFiles())){
				files4+=fzdy.getReadyPoliticalReviewFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getReadyFiles())){
				files5+=fzdy.getReadyFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getFormalFiles())){
				files5+=fzdy.getFormalFiles()+",";
			}
			files1 = files1.length()>0?files1.substring(0,files1.length()-1):null;
			files2 = files2.length()>0?files2.substring(0,files2.length()-1):null;
			files3 = files3.length()>0?files3.substring(0,files3.length()-1):null;
			files4 = files4.length()>0?files4.substring(0,files4.length()-1):null;
			files5 = files5.length()>0?files5.substring(0,files5.length()-1):null;
			if(files1==null&&files2==null&&files3==null&&files4==null&&files5==null){
				LehandException.throwException("无资料可打包");
				return null;
			}

		}
		List<Map<String, Object>> maps = generalSqlComponent.getDbComponent().listMap("SELECT * FROM fzdy_party_link_his where userid=?", new Object[]{person_id});
		for (Map<String, Object> m: maps) {
			if(!StringUtils.isEmpty(m.get("formalFiles"))){
				files6+=m.get("formalFiles")+",";
			}
		}
		if(!StringUtils.isEmpty(files6)){
			files6 = files6.length()>0?files6.substring(0,files6.length()-1):null;
		}
		//获取文件集合
		List<Map<String, Object>> data1 = fileDelService.getFiles(files1, session);
		Map<String,Object> map1 = new HashMap<String, Object>();
		map1.put("one", data1);
		List<Map<String, Object>> data2 = fileDelService.getFiles(files2, session);
		Map<String,Object> map2 = new HashMap<String, Object>();
		map2.put("two", data2);
		List<Map<String, Object>> data3 = fileDelService.getFiles(files3, session);
		Map<String,Object> map3 = new HashMap<String, Object>();
		map3.put("three", data3);
		List<Map<String, Object>> data4 = fileDelService.getFiles(files4, session);
		Map<String,Object> map4 = new HashMap<String, Object>();
		map4.put("four", data4);
		List<Map<String, Object>> data5 = fileDelService.getFiles(files5, session);
		Map<String,Object> map5 = new HashMap<String, Object>();
		map5.put("five", data5);
		List<Map<String, Object>> data6 = fileDelService.getFiles(files6, session);
		Map<String,Object> map6 = new HashMap<String, Object>();
		map6.put("six", data6);


//		Map<String,Object> fileids1 = db().getMap("select group_concat(fileids) fileids from fzdy_special where person_id=? and stepid<200", new Object[] {person_id});
//		List<Map<String, Object>> data1 = new ArrayList<>();
//		if(fileids1!=null) {
//			data1 = db().listMap("select * from dl_file where id in ("+fileids1.get("fileids")+")", new Object[] {});
//		}
//		for (Map<String, Object> map : data1) {
//			map.put("path", down_ip+map.get("dir"));
//		}
//		Map<String,Object> map1 = new HashMap<String, Object>();
//		map1.put("one", data1);
//
//		Map<String,Object> fileids2 = db().getMap("select group_concat(fileids) fileids from fzdy_special where person_id=? and stepid>200 and stepid<300", new Object[] {person_id});
//		List<Map<String, Object>> data2 = new ArrayList<>();
//		if(fileids2!=null) {
//			data2 = db().listMap("select * from dl_file where id in ("+fileids2.get("fileids")+")", new Object[] {});
//		}
//		for (Map<String, Object> map : data2) {
//			map.put("path", down_ip+map.get("dir"));
//		}
//		Map<String,Object> map2 = new HashMap<String, Object>();
//		map2.put("two", data2);
//
//		Map<String,Object> fileids3 = db().getMap("select group_concat(fileids) fileids from fzdy_special where person_id=? and stepid>300 and stepid<400", new Object[] {person_id});
//		List<Map<String, Object>> data3 = new ArrayList<>();
//		if(fileids2!=null) {
//			data3 = db().listMap("select * from dl_file where id in ("+fileids3.get("fileids")+")", new Object[] {});
//		}
//		for (Map<String, Object> map : data3) {
//			map.put("path", down_ip+map.get("dir"));
//		}
//		Map<String,Object> map3 = new HashMap<String, Object>();
//		map3.put("three", data3);
//
//		Map<String,Object> fileids4 = db().getMap("select group_concat(fileids) fileids from fzdy_special where person_id=? and stepid>400 and stepid<500", new Object[] {person_id});
//		List<Map<String, Object>> data4 = new ArrayList<>();
//		if(fileids2!=null) {
//			data4 = db().listMap("select * from dl_file where id in ("+fileids4.get("fileids")+")", new Object[] {});
//		}
//		for (Map<String, Object> map : data4) {
//			map.put("path", down_ip+map.get("dir"));
//		}
//		Map<String,Object> map4 = new HashMap<String, Object>();
//		map4.put("four", data4);
//
//		Map<String,Object> fileids5 = db().getMap("select group_concat(fileids) fileids from fzdy_special where person_id=? and stepid>500 ", new Object[] {person_id});
//		List<Map<String, Object>> data5 = new ArrayList<>();
//		if(fileids2!=null) {
//			data5 = db().listMap("select * from dl_file where id in ("+fileids5.get("fileids")+")", new Object[] {});
//		}
//		for (Map<String, Object> map : data5) {
//			map.put("path", down_ip+map.get("dir"));
//		}
//		Map<String,Object> map5 = new HashMap<String, Object>();
//		map5.put("five", data5);
//
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		data.add(map1);
		data.add(map2);
		data.add(map3);
		data.add(map4);
		data.add(map5);
		data.add(map6);
		return data;
	}
	
	

	/**
	 * Description: 资料打包
	 * @author PT  
	 * @date 2019年9月16日 
	 * @param person_id
	 * @return
	 */
	public String dataPackaging(String person_id,Session session) {
		List<Map<String, Object>> data = filesData(person_id, session);
		if (data == null) return null;
		Map<String, Object> maps = db().getMap("select xm from t_dy_info where userid=?", new Object[] {person_id});
//		List<Map<String, Object>> data = db().listMap("select * from dl_file where id in (select fileids fileids from fzdy_special where person_id=?)", new Object[] {person_id});
		if(data==null || data.size()<=0) {
			return "";
		}
		List<File> srcFiles = new ArrayList<File>();
		for (Map<String, Object> map : data) {
			String path = down_ip+map.get("dir").toString();
			File file;
			try {
				file = downLoadFromUrl(path,map.get("name").toString().substring(0,map.get("name").toString().length()-4)+System.currentTimeMillis()+".pdf", "temp");
				srcFiles.add(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String pathname = "\\"+person_id+".zip";
		File zipFile = new File(pathname);
		// 调用压缩方法
		zipFiles(srcFiles, zipFile);
		String path = "";
		try {
			path = dFSComponent.upload(pathname);
			for (File file : srcFiles) {
				file.delete();
			}
			zipFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return down_ip+path+"?download=true&attname="+maps.get("xm")+"的资料包.zip";
	}


	private List<Map<String, Object>> filesData(String person_id, Session session) {
		//获取5环节信息
		FzdyPartyLinkDto2 fzdy = generalSqlComponent.query(SqlCode.getFzdyPartyLink,new Object[] {person_id} );
		String files = "";
		//拼接附件id
		if(fzdy!=null){
			if(!StringUtils.isEmpty(fzdy.getApplyFiles())){
				files+=fzdy.getApplyFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getActivistFiles())){
				files+=fzdy.getActivistFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getDevelopFiles())){
				files+=fzdy.getDevelopFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getReadyPoliticalReviewFiles())){
				files+=fzdy.getReadyPoliticalReviewFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getReadyFiles())){
				files+=fzdy.getReadyFiles()+",";
			}
			if(!StringUtils.isEmpty(fzdy.getFormalFiles())){
				files+=fzdy.getFormalFiles()+",";
			}
			if(files.length()>0){
				files = files.substring(0,files.length()-1);
			}else{
				LehandException.throwException("无资料可打包");
				return null;
			}

		}
		//获取文件集合
		List<Map<String, Object>> data = fileDelService.getFiles(files, session);
		return data;
	}

	/**
	 * 从网络Url中下载文件
	 * @param urlStr
	 * @param fileName
	 * @param savePath
	 * @throws IOException
	 */
	public File downLoadFromUrl(String urlStr,String fileName,String savePath) throws IOException{
	    URL url = new URL(urlStr);
	    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	    //设置超时间为3秒
	    conn.setConnectTimeout(3*1000);
	    //防止屏蔽程序抓取而返回403错误
	    conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

	    //得到输入流
	    InputStream inputStream = conn.getInputStream();
	    //获取自己数组
	    byte[] getData = readInputStream(inputStream);

	    //文件保存位置
	    File saveDir = new File(savePath);
	    //System.out.println(saveDir.getAbsolutePath());
	    if(!saveDir.exists()){
	        saveDir.mkdir();
	    }
	    File file = new File(saveDir+File.separator+fileName);
	    FileOutputStream fos = new FileOutputStream(file);
	    fos.write(getData);
	    if(fos!=null){
	        fos.close();
	    }
	    if(inputStream!=null){
	        inputStream.close();
	    }
	    return file;
	}
	
	/**
	 * 从输入流中获取字节数组
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public static  byte[] readInputStream(InputStream inputStream) throws IOException {
	    byte[] buffer = new byte[1024];
	    int len = 0;
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    while((len = inputStream.read(buffer)) != -1) {
	        bos.write(buffer, 0, len);
	    }
	    bos.close();
	    return bos.toByteArray();
	} 

	/**
	 * Description: 压缩文件
	 * @author PT  
	 * @date 2019年9月17日 
	 * @param srcFiles
	 * @param zipFile
	 */
	public  void zipFiles(List<File> srcFiles, File zipFile) {
		// 判断压缩后的文件存在不，不存在则创建
		if (!zipFile.exists()) {
			try {
				zipFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// 创建 FileOutputStream 对象
		FileOutputStream fileOutputStream = null;
		// 创建 ZipOutputStream
		ZipOutputStream zipOutputStream = null;
		// 创建 FileInputStream 对象
		FileInputStream fileInputStream = null;

		try {
			// 实例化 FileOutputStream 对象
			fileOutputStream = new FileOutputStream(zipFile);
			// 实例化 ZipOutputStream 对象
			zipOutputStream = new ZipOutputStream(fileOutputStream);
			// 创建 ZipEntry 对象
			ZipEntry zipEntry = null;
			// 遍历源文件数组
			for (int i = 0; i < srcFiles.size(); i++) {
				// 将源文件数组中的当前文件读入 FileInputStream 流中
				fileInputStream = new FileInputStream(srcFiles.get(i));
				// 实例化 ZipEntry 对象，源文件数组中的当前文件
				zipEntry = new ZipEntry(srcFiles.get(i).getName());
				zipOutputStream.putNextEntry(zipEntry);
				// 该变量记录每次真正读的字节个数
				int len;
				// 定义每次读取的字节数组
				byte[] buffer = new byte[1024];
				while ((len = fileInputStream.read(buffer)) > 0) {
					zipOutputStream.write(buffer, 0, len);
				}
				fileInputStream.close();
			}
			zipOutputStream.closeEntry();
			zipOutputStream.close();
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public TDzzBzcyxxDto getSj(Session session,String orgid){
		Map<String,String> parms = new HashMap<String,String>(1);
		parms.put("organid",orgid);
		TDzzBzcyxxDto bzcy = generalSqlComponent.query(SqlCode.getZbsj,parms);
		return bzcy;
	}
	
}
