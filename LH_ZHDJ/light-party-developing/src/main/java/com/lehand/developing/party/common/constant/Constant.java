package com.lehand.developing.party.common.constant;

public class Constant {
	
	public static final String ENPTY = "";
	
	/**
	 * 入党积极分子审批
	 */                              
	public static final String ONE = "onecode";
	
	/**
	 * 发展对象审批 
	 */
	public static final String TWO = "twocode";
	
	/**
	 * 政治审查
	 */
	public static final String THREE = "threecode";
	/**
	 * 预备党员预审
	 */
	public static final String FOUR = "fourcode";
	/**
	 * 预备党员接收审批
	 */
	public static final String FIVE = "fivecode";
	/**
	 * 预备党员转正审批
	 */
	public static final String SIX = "sixcode";
	/**
	 * 信息修改审批
	 */
	public static final String SEVEN = "sevencode";
}
