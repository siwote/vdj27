package com.lehand.document.lib.pojo;

public class DlType {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.id
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.compid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Long compid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.year
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Short year;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.name
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String name;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.pid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Long pid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.orderid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Integer orderid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.createtime
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String createtime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.moditime
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String moditime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.creator
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Long creator;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.moditor
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Long moditor;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.orgid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Long orgid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.orgname
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String orgname;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String remark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark1
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Integer remark1;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark2
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private Integer remark2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark3
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String remark3;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark4
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String remark4;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark5
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String remark5;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dl_type.remark6
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    private String remark6;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.id
     *
     * @return the value of dl_type.id
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.id
     *
     * @param id the value for dl_type.id
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.compid
     *
     * @return the value of dl_type.compid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Long getCompid() {
        return compid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.compid
     *
     * @param compid the value for dl_type.compid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setCompid(Long compid) {
        this.compid = compid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.year
     *
     * @return the value of dl_type.year
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Short getYear() {
        return year;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.year
     *
     * @param year the value for dl_type.year
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setYear(Short year) {
        this.year = year;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.name
     *
     * @return the value of dl_type.name
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.name
     *
     * @param name the value for dl_type.name
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.pid
     *
     * @return the value of dl_type.pid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Long getPid() {
        return pid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.pid
     *
     * @param pid the value for dl_type.pid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.orderid
     *
     * @return the value of dl_type.orderid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Integer getOrderid() {
        return orderid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.orderid
     *
     * @param orderid the value for dl_type.orderid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.createtime
     *
     * @return the value of dl_type.createtime
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.createtime
     *
     * @param createtime the value for dl_type.createtime
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.moditime
     *
     * @return the value of dl_type.moditime
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getModitime() {
        return moditime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.moditime
     *
     * @param moditime the value for dl_type.moditime
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setModitime(String moditime) {
        this.moditime = moditime == null ? null : moditime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.creator
     *
     * @return the value of dl_type.creator
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Long getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.creator
     *
     * @param creator the value for dl_type.creator
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setCreator(Long creator) {
        this.creator = creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.moditor
     *
     * @return the value of dl_type.moditor
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Long getModitor() {
        return moditor;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.moditor
     *
     * @param moditor the value for dl_type.moditor
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setModitor(Long moditor) {
        this.moditor = moditor;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.orgid
     *
     * @return the value of dl_type.orgid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Long getOrgid() {
        return orgid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.orgid
     *
     * @param orgid the value for dl_type.orgid
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.orgname
     *
     * @return the value of dl_type.orgname
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getOrgname() {
        return orgname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.orgname
     *
     * @param orgname the value for dl_type.orgname
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setOrgname(String orgname) {
        this.orgname = orgname == null ? null : orgname.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark
     *
     * @return the value of dl_type.remark
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark
     *
     * @param remark the value for dl_type.remark
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark1
     *
     * @return the value of dl_type.remark1
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Integer getRemark1() {
        return remark1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark1
     *
     * @param remark1 the value for dl_type.remark1
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark1(Integer remark1) {
        this.remark1 = remark1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark2
     *
     * @return the value of dl_type.remark2
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public Integer getRemark2() {
        return remark2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark2
     *
     * @param remark2 the value for dl_type.remark2
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark2(Integer remark2) {
        this.remark2 = remark2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark3
     *
     * @return the value of dl_type.remark3
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getRemark3() {
        return remark3;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark3
     *
     * @param remark3 the value for dl_type.remark3
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark4
     *
     * @return the value of dl_type.remark4
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getRemark4() {
        return remark4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark4
     *
     * @param remark4 the value for dl_type.remark4
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark4(String remark4) {
        this.remark4 = remark4 == null ? null : remark4.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark5
     *
     * @return the value of dl_type.remark5
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getRemark5() {
        return remark5;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark5
     *
     * @param remark5 the value for dl_type.remark5
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark5(String remark5) {
        this.remark5 = remark5 == null ? null : remark5.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dl_type.remark6
     *
     * @return the value of dl_type.remark6
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public String getRemark6() {
        return remark6;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dl_type.remark6
     *
     * @param remark6 the value for dl_type.remark6
     *
     * @mbg.generated Wed Apr 10 16:34:41 CST 2019
     */
    public void setRemark6(String remark6) {
        this.remark6 = remark6 == null ? null : remark6.trim();
    }
}