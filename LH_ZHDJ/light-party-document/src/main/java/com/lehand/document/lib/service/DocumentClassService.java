package com.lehand.document.lib.service;

import com.lehand.base.common.Org;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.document.lib.constant.CommSqlCfgCode;
import com.lehand.document.lib.constant.Constant;
import com.lehand.document.lib.dto.DlTypeDto;
import com.lehand.document.lib.pojo.DlType;
import com.lehand.document.lib.util.CheckingUtil;
import com.lehand.module.common.pojo.SystemParam;
import com.lehand.module.urc.pojo.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.lehand.module.common.constant.CommonSqlCode.getSysParam;

@Service
public class DocumentClassService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

    private static final Long FUNCTION_PID = 0L;
	

	//层级的限制
	@Value("${jk.class.level}")
	private int level;
	
	/**
	 * 资源分类 新增
	 * 
	 * @author huruohan
	 * @date 2019年4月10日17:16:25
	 * @param dlType
	 * @param userVO 当前用户 remarks 备注 orderid 序号  pid 上级分类  name 分类名称
	 * @return 分类自增id
	 * @throws Exception 
	 */
	@Transactional(rollbackFor=Exception.class)
	public Long addDlType(DlType dlType, Session userVO) throws Exception {
		 //分类名称的校验
        String name = CheckingUtil.nameChecking(dlType.getName(),userVO, Constant.MAX_DLTYPE_NAME);
        //租户id
        long compid = userVO.getCompid();
        //pid，新增时的父类id，没有父类就为0
        Long pid = dlType.getPid();
        // 如果没有pid就为0
        if (null == pid) {
            dlType.setPid(Constant.TOP_PID_DLTYPE);
        }
        // 如果新增的不是父分类
        if (Constant.TOP_PID_DLTYPE != pid) {
            //执行层级限制的方法
            levelChecking(pid, compid);
        }
        // 判断有没有资料 参数id，compid
        Map<String, Object> param = new HashMap<String, Object>(3);
        param.put("typeId", pid);
        param.put("compid", compid);
        // 判断源分类有没有文件夹
        Integer srcDatasNum = generalSqlComponent.query(CommSqlCfgCode.getDatasNum, param);
        if (0 < srcDatasNum) {
            LehandException.throwException("该分类下有资料!");
        }
        // 判断是不是有相同名字
        param.put("name", name);
        int count = generalSqlComponent.query(CommSqlCfgCode.getDlTypeByName, param);
        if (0 < count) {
            LehandException.throwException("名称重复,请更换名称!");
        }
        // 资源分类对象的初始化
        initDocumentClass(dlType, userVO);
        // 新增分类
        Long id = generalSqlComponent.insert(CommSqlCfgCode.addDlType, dlType);
        return id;
	}
	
	/**
     * 层级限制的方法
     * 不断的获取父分类的pid，边获取边计数，直到获取到的pid为0时就停止。
     * @author: huruohan   
     * @date: 2019年5月13日 上午10:41:33 
     * @param pid 父类id 
     * @param compid 租户id
     * @return
     */
    private int levelChecking(long pid, long compid) {
        // 如果新增的不是父分类
        // 判断是不是超过资料分类层级的限制 返回0
        int sum = 0;
        for (int i = 0; i < level; i++) {
            // 查询父分类的pid
            pid = generalSqlComponent.query(CommSqlCfgCode.getPid, new Long[] {pid, compid});
            // 如果计数器大于等于level就是超过了层级限制
            // 如果遍历到最高层级分类直接跳出
            if (Constant.TOP_PID_DLTYPE >= pid) {
                break;
            }
            // 计数器
            if (sum ++ >= level) {
                LehandException.throwException("最多创建" + level + "级分类!");
            }
        }
        return sum;
    }
	
	
	/**
	 * 资料分类对象的初始化
	 * @author huruohan
	 * @date 2019年4月11日10:48:49
	 * 
	 * @param dlType 分类对象
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public void initDocumentClass(DlType dlType,Session userVO) {
		dlType.setPid(dlType.getPid());//父类id，如果null的话设为0，
		dlType.setCompid(userVO.getCompid());//租户id
		//获取最小排序号
		Integer min=generalSqlComponent.query(CommSqlCfgCode.getMinOrderid, dlType);
		//如果为null则为0
		if(null==min) {
			min=0;
		//否则为最小-1
		}else {
			min-=1;
		}
		dlType.setOrderid(min);//排序id
		dlType.setYear(Short.valueOf(DateEnum.YYYYMMDD.format().substring(0, 4)));//年度
		dlType.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());//创建时间
		dlType.setCreator(userVO.getUserid());//创建人
		long organId;
		String organName;
//		List<Organ> organList=userVO.getOrganids();
        Org org = userVO.getCurrentOrg();
//		if(CollectionUtils.isEmpty(org)) {
        if(org == null) {
			organId=1L;
			organName=StringConstant.EMPTY;
		}else {
//			Organ organ=organList.get(0);
			organId = org.getOrgid();
			organName= org.getOrgname();
		}
		dlType.setOrgid(organId);//组织机构id
		dlType.setOrgname(organName);//组织机构名称
		dlType.setRemark1(0);//备注1
		dlType.setRemark2(0);//备注2
		dlType.setRemark3(StringConstant.EMPTY);//备注3
		dlType.setRemark4(StringConstant.EMPTY);//备注4
		dlType.setRemark5(StringConstant.EMPTY);//备注5
		dlType.setRemark6(StringConstant.EMPTY);//备注6
	}


	/**
	 * 修改资源分类
	 * @author huruohan
	 * @date 2019年4月11日10:49:12
	 * @param id 修改的资料分类的id
	 * @param name 修改的资料分类的名称
	 * @param userVO 当前登录的用户
	 * @throws Exception 
	 */
	@Transactional(rollbackFor=Exception.class)
	public Integer updateDlType(Long id,String name, Session userVO) throws Exception {
		name = CheckingUtil.nameChecking(name,userVO,Constant.MAX_DLTYPE_NAME);
        // 判断是不是有相同名字 返回-1
        Map<String, Object> param = new HashMap<String, Object>(3);
        param.put("name", name);
        param.put("id", id);
        param.put("compid", userVO.getCompid());
        //获取名称一样id不一样的资料文件夹的数量
        int count = generalSqlComponent.query(CommSqlCfgCode.getDlTypeByName, param);
        if (0 < count) {
            LehandException.throwException("名称重复,请重新命名!");
        }
        DlType dlType = new DlType();
        // 分类id
        dlType.setId(id);
        // 分类名称
        dlType.setName(name);
        // 修改时间
        dlType.setModitime(DateEnum.YYYYMMDDHHMMDD.format());
        // 修改人
        dlType.setModitor(userVO.getUserid());
        // 租户id
        dlType.setCompid(userVO.getCompid());
        // 修改资料分类
        Integer flag = generalSqlComponent.update(CommSqlCfgCode.updateDlType, dlType);
        return flag;
	}

	/**
	  *删除分类
	 *@author huruohan
	 *@date 2019年4月11日10:49:44
	 *
	 *@param id 要删除分类的id
	 *@param userVO 当前用户
	 */
	@Transactional(rollbackFor=Exception.class)
	public Integer deleteDlType(Long id,Session userVO) {
		// 如果是未分类，则无法删除
        // if (Long.valueOf(systemId).equals(id)) {
        // LehandException.throwException("未分类无法删除!");
        // }
        Map<String, Long> param = new HashMap<String, Long>(2);
        // 储存要删除分类id和子分类id的所有集合的集合
        List<Long> ids = new ArrayList<Long>();
        // 调用获取子分类方法的临时储存子分类id的集合参数
        List<Long> deleteIds = new ArrayList<Long>();
        ids.add(id);
        deleteIds.add(id);
        //查询到最低层分类

        while (true) {
            // 获取所有的子分类
            deleteIds = getSunId(deleteIds, userVO);
            if (0 >= deleteIds.size()) {
                break;
            }
            // 将子分类添加到最后要删除的id的集合中
            ids.addAll(deleteIds);
        }

        // 循环去查询所有的分类有没有资料
        param.put("compid", userVO.getCompid());
        for (Long typeId : ids) {
            param.put("id", typeId);
            //查询分类下的资料文件夹的数量
            int sum = generalSqlComponent.query(CommSqlCfgCode.getSubordinatesSum, param);
            //如果数量大于0则报错
            if (0 < sum) {
                LehandException.throwException("分类下有资料文件夹无法删除!");
            }
        }
        for (Long classId : ids) {
            param.put("id", classId);
            // 删除分类
            generalSqlComponent.delete(CommSqlCfgCode.deleteDlType, param);
        }
        // 删除成功则返回1
        return 1;
	}


	/**
	 * 拖动排序资源分类
	 * @author huruohan
	 * @date 2019年4月11日10:50:43
	 * 
	 * @param srcId 拖动源的分类id
	 * @param targetId 拖动目标的分类id
	 * @param type 3:在目标里面，1：在目标上面，2：在目标下面4
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor=Exception.class)
	public Integer moveDlType(Long srcId, Long targetId, String type,Session userVO) {
		long compid = userVO.getCompid();
        Integer flag = null;
        Map<String, Object> param= new HashMap<String, Object>(5);
        param.put("typeId", targetId);
        param.put("compid", compid);
        // 判断目的分类是不是4级分类
        int num = 0;
        List<Long> ids = new ArrayList<Long>();
        ids.add(srcId);
        // 获取源分类的子分类个数
        while (true) {
            ids = getSunIdSum(ids, userVO);
            if (0 >= ids.size()) {
                break;
            }
            num++;
        }
        // 获取目标分类的父分类个数
        num += levelChecking(targetId, compid);
        // 无法移动到4级目录下
        if (Constant.TYPE_3.equals(type) && num >= Integer.valueOf(level) - 1) {
            LehandException.throwException("最多" + level + "级分类!");
        } else if (num >= Integer.valueOf(level)) {
            LehandException.throwException("最多" + level + "级分类!");
        }
        // 移动到分类里面
        long sum = 0L;
        if (Constant.TYPE_3.equals(type)) {
            // 判断目标分类有没有文件夹
            Integer targetDatasNum = generalSqlComponent.query(CommSqlCfgCode.getDatasNum, param);
            if (0 < targetDatasNum) {
                LehandException.throwException("无法移动到有资料的分类下!");
            }
            // 直接设置源分类的pid为目标的id，排序号为0
            param.put("pid", targetId);
        } else {
            // 获取目标分类的pid和排序号
            param.put("id", targetId);
            // 拿到目标分类的对象
            DlType dlType = generalSqlComponent.query(CommSqlCfgCode.getDlType, param);
            // pid
            param.put("pid", dlType.getPid());
            // 排序号
            param.put("orderid", dlType.getOrderid());
            // 移动到分类上面
            if (Constant.TYPE_1.equals(type)) {
                generalSqlComponent.update(CommSqlCfgCode.updateDlTypeOrderDown, param);
                sum = dlType.getOrderid();
            // 移动到分类下面
            } else if (Constant.TYPE_2.equals(type)) {
                generalSqlComponent.update(CommSqlCfgCode.updateDlTypeOrderUp, param);
                sum = dlType.getOrderid();
            } else {
                LehandException.throwException("没有对应的处理方式!");
            }
        }
        // 直接设置源分类的pid为目标的id，排序号为0
        param.put("id", srcId);
        param.put("orderid", sum);
        flag = generalSqlComponent.update(CommSqlCfgCode.updateDlTypeOrder, param);
        return flag;
	}


	/**
	 * 资源分类列表树
	 * @param type 1:资料分类  0:资料管理
	 * @param likeName 模糊查询的名称
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor=Exception.class)
	public List<DlTypeDto> listDlType(String type, String likeName, Session userVO) {
		long compid = userVO.getCompid();
        Map<String, Object> param = new HashMap<String, Object>(3);
        // 搜索
        boolean flag = StringUtils.hasLength(likeName);
        if (flag) {
            param.put("likeName", likeName);
        }
        param.put("compid", compid);
        // 获取资料分类的集合
        List<DlTypeDto> dlTypeList = generalSqlComponent.query(CommSqlCfgCode.ListDlType, param);
        if (CollectionUtils.isEmpty(dlTypeList)) {
            return null;
        }
        List<Long> pidList = new ArrayList<Long>();
        List<DlTypeDto> tree = null;
        // true是模糊查询 false不是模糊查询
        // 如果是模糊查询,则将没有父分类的分类的pid设为0，并将原来的pid保存起来
        if (flag) {
            List<Long> idList = new ArrayList<Long>();
            for (DlTypeDto temp : dlTypeList) {
                idList.add(temp.getId());
            }
            for (DlTypeDto temp : dlTypeList) {
                if (!idList.contains(temp.getPid())) {
                    pidList.add(temp.getPid());
                    temp.setPid(Constant.TOP_PID_DLTYPE);
                }
            }
        }
        // 树底层的icon换成file的方法
        for (DlTypeDto temp : dlTypeList) {
            Integer sunNum = haveSun(Long.valueOf(temp.getId()), compid);
            if (sunNum.equals(0)) {
                temp.setIcon("file");
                temp.setDisabled(false);
            }
        }

        // 调用递归方法,将集合转为多级树结构并返回
        tree = makeTree(dlTypeList, Constant.TOP_PID_DLTYPE, compid);
        // 模糊查询,将父分类的pid还原
        if (flag) {
            for (int i = 0; i < tree.size(); i++) {
                tree.get(i).setPid(pidList.get(i));
            }
        }
        return tree;
	}
	
	/**
	 * 递归查询树
	 * @param items 集合
	 * @param pid 最高层的pid
	 * @return
	 * @author taogang
	 */
	public List<DlTypeDto> makeTree(List<DlTypeDto> items,Long pid,Long compid){
		 // 子类
        List<DlTypeDto> children = items.stream().filter(x -> x.getPid().equals(pid)).collect(Collectors.toList());
        // 后辈中的非子类
        List<DlTypeDto> successor = items.stream().filter(x -> !x.getPid().equals(pid)).collect(Collectors.toList());
        children.forEach(x -> {
            makeTree(successor, x.getId(), compid).forEach(y -> x.getChildren().add(y));
        });

        return children;
	}
	
//	public List<DlTypeDto> makeTree2(List<DlTypeDto> items,Integer pid,Long compid){
//		//子类
//		List<DlTypeDto> children = items.stream().filter(x -> Integer.valueOf(x.getPid()).equals(pid)).collect(Collectors.toList());
//		//后辈中的非子类
//		List<DlTypeDto> successor = items.stream().filter(x -> !Integer.valueOf(x.getPid()).equals(pid)).collect(Collectors.toList());
//		children.forEach(x ->
//        {
//        		 makeTree(successor, Integer.valueOf(x.getId()),compid).forEach(
//                		 y -> x.getChildren().add(y)
//                 ); 
//        }
//		);
//		
//		return children;
//	}
	
	/**
	 * 获取子分类的数量
	 * @param id
	 * @param compid
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public Integer haveSun(Long id,Long compid) {
		//获取该分类的儿子数量
		Integer i=generalSqlComponent.query(CommSqlCfgCode.getChildrenSum, new Long[] {id,compid});
		return i;
	}
	
	/**
	 * 返回所有子分类id集合
	 * @param ids id集合
	 * @param userVO 当前用户
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
    public List<Long> getSunId( List<Long> ids,Session userVO) {
		List<Long> result=new ArrayList<Long>();
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("compid",userVO.getCompid());
		for(Long id:ids) {
			param.put("id",id);
			//否则  继续执行，获取子分类，
			List<Long> idList=generalSqlComponent.query(CommSqlCfgCode.getChildrenId, param);
			//如果有   ids.add();
			//将分类添加idAll中
			result.addAll(idList);
		}
		return result;
	}
	
	
	//返回所有子分类  null 表示分类下有资料    return 子分类id集合
	@Transactional(rollbackFor = Exception.class)
    public List<Long> getSunIdSum( List<Long> ids,Session userVO) {
		List<Long> result = new ArrayList<Long>();
        Map<String, Object> param = new HashMap<String, Object>(2);
        //租户id
        param.put("compid", userVO.getCompid());
        for (Long id : ids) {
            param.put("id", id);
            // 否则 继续执行，获取子分类，
            List<Long> idList = generalSqlComponent.query(CommSqlCfgCode.getChildrenId, param);
            // 如果有 ids.add();
            // 将分类添加idAll中
            result.addAll(idList);
        }
        return result;
	}


	/**
	 * 新增分类的判断
	 * @author huruohan
	 * @date 2019年4月22日14:16:33
	 * @param id 分类id
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public Boolean limitAddDlType(Long id, Session userVO) throws Exception {
		long compid=userVO.getCompid();
        // admin用户无法新建
//        if (CollectionUtils.isEmpty(userVO.getOrganids())) {
        if(userVO.getCurrentOrg() == null) {
            LehandException.throwException("admin用户无法新建!");
        }
        // 判断有没有资料 参数id，compid
        Map<String, Object> param = new HashMap<String, Object>(2);
        param.put("typeId", id);
        param.put("compid", compid);
        // 判断源分类有没有文件夹
        Integer srcDatasNum = generalSqlComponent.query(CommSqlCfgCode.getDatasNum, param);
        if (0 < srcDatasNum) {
            LehandException.throwException("该分类下有资料!");
        }
        // 判断是不是超过资源分类层级的限制
        if (!Constant.TOP_PID_DLTYPE.equals(id)) {
           levelChecking(id, compid);
        }
        return true;
	}

    /**
     * 查询菜单层级是否可以新增下级
     * @param fctid
     * @param session
     * @return
     */
    public boolean checkMenuLev(Long fctid, Session session) {
        int lev = 1;
        int limt = 3;
        SystemParam param = generalSqlComponent.query(getSysParam,new Object[]{session.getCompid(), "menu_level_cfg"});
        if(param!=null&&!StringUtils.isEmpty(param.getParamvalue())){
            int value = Integer.parseInt(param.getParamvalue());
            limt = (value>0&&value<4)?value: 3;
        }
        int rel = recursion(lev,fctid,session.getCompid());
        return rel<limt;
    }

    /**
     * 递归查询当前菜单层级
     * @param lev
     * @param fctid
     * @param compid
     * @return
     */
    private int recursion(int lev, Long fctid, Long compid) {
        Function function = generalSqlComponent.query("getFuntion",new Object[]{fctid,compid});
        if(function==null){
            return 100000;
        }
        //功能按钮暂不支持新增下级
        if(function.getFctflag()==2){
            return 100000;
        }
        if(!FUNCTION_PID.equals(function.getPfctid())){
            lev++;
            return	recursion(lev,function.getPfctid(),compid);
        }
        return lev;
    }
	
//	private DlTypeDto addSun(DlTypeDto dlTypeDto,Session userVO,String likeName) {
//	List<DlTypeDto> sun=generalSqlComponent.query(CommSqlCfgCode.listSun, new Object[] {dlTypeDto.getId(),userVO.getCompid(),likeName});
//	if(!CollectionUtils.isEmpty(sun)) {
//		for (DlTypeDto dlTypeDto2 : sun) {
//			dlTypeDto.getChildren().add(addSun(dlTypeDto2,userVO,likeName));
//		}
//	}else {
//		dlTypeDto.setDisabled(false);
//		dlTypeDto.setIcon("file");
//	}
//	return dlTypeDto;
//}


//	@Override
//	public List<DlTypeDto> demo(String likeName, Session userVO) throws Exception {
//		String type="1";
//		Map<String,Object> param=new HashMap<String,Object>();
//		//搜索
//		if(StringUtils.hasLength(likeName.trim())) {
//			param.put("likeName",likeName);
//		}
//		param.put("compid", userVO.getCompid());
//		//如果不是0则是资料分类
//		if(!"0".equals(type)) {
//			param.put("remark1",0);
//		}
//		//获取资料分类的集合
//		List<DlTypeDto> dlTypeList=generalSqlComponent.query(CommSqlCfgCode.ListDlType, param);
//		if(CollectionUtils.isEmpty(dlTypeList))return dlTypeList=new ArrayList<DlTypeDto>();
//		List<String> pidList=new ArrayList<String>();
//		List<DlTypeDto> tree=null;
//		if(StringUtils.hasLength(likeName.trim())){
//			//遍历将数据对象转为Dto对象，其Dto的属性名称根据前端的需求而定
//			//调用递归方法,将集合转为多级树结构并返回
//			//树底层的icon换成file的方法
//			
//			List<String> idList=new ArrayList<String>();
//			for(DlTypeDto temp:dlTypeList) {
//				idList.add(temp.getId());
//			}
//			for(DlTypeDto temp:dlTypeList) {
//				Integer sunNum=haveSun(Long.valueOf(temp.getId()),userVO.getCompid());
//				if(sunNum.equals(0)) {
//					temp.setIcon("file");
//					temp.setDisabled(false);
//				}
//				if(!idList.contains(temp.getPid())) {
//					pidList.add(temp.getPid());
//					temp.setPid("0");
//				}
//			}
//			tree=makeTree2(dlTypeList,0,userVO.getCompid()) ;
//			int i=0;
//			for(DlTypeDto temp:tree) {
//				temp.setPid(pidList.get(i++));
//			}
//		}else {
//			for(DlTypeDto temp:dlTypeList) {
//				Integer sunNum=haveSun(Long.valueOf(temp.getId()),userVO.getCompid());
//				if(sunNum.equals(0)) {
//					temp.setIcon("file");
//					temp.setDisabled(false);
//				}
//			}
//			tree=makeTree2(dlTypeList,0,userVO.getCompid()) ;
//		}
//		return tree;
//		
//		
//	}
	
	
//	private void treeAll(List<DlTypeDto> result,List<DlTypeDto> temp,DlTypeDto sun,Session userVO) {
//		
//		//
//	}
//	
}
