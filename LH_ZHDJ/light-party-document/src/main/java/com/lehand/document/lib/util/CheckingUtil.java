 package com.lehand.document.lib.util;

 import com.lehand.base.common.Session;
 import com.lehand.base.constant.StringConstant;
 import com.lehand.base.exception.LehandException;
 import org.springframework.util.StringUtils;

 /**
  * 验证工具
  * @author huruohan
  * @date 2019年5月17日10:44:39
  */
 public class CheckingUtil {

     /**
      * 名称的验证           1、是否为空，2、先去空格 ，3、是否超过限定的字符串长度(长度是调用方法传的) ,4、是不是admin用户
      * 功能描述：
      * @author: huruohan
      * @date: 2019年5月17日 上午11:09:00
      * @param name 要验证的名称
      * @param userVO 当前用户
      * @param maxName 限制的名称长度
      * @return 放回验证后的名称
      * @throws Exception
      */
      public static String nameChecking(String name, Session userVO, int maxName) throws Exception {
          //字符为空时无法新建
          if(StringUtils.isEmpty(name)) {
              LehandException.throwException("名称不能为空格!");
          }
          name=name.replaceAll("\\s*", StringConstant.EMPTY);
          //字数的验证不能超过maxName字
          if(maxName<name.length()) {
              LehandException.throwException("名称的字数不能超过"+maxName+"字!");
          }
          //判断是不是admin用户，admin用户无法创建
          if(userVO.getUserid().equals(1L)) {
              LehandException.throwException("admin用户无法操作!");
          }
          return name;
      }


 //     public static void main(String[] args) throws Exception {
 //        String str="sdfsd";
 //        System.out.println(str.indexOf(" "));
 //    }
 }
