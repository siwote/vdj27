package com.lehand.document.lib.constant;

public class CommSqlCfgCode {
	//资源分类
	/**
	 * 新增资料分类<P/>
	 * insert into dl_type (compid,year,name,pid,orderid,createtime,creator,orgid,orgname,remark,remark1,remark2,remark3,remark4,remark5,remark6) 
	 *values (:compid,:year,:name,:pid,:orderid,:createtime,:creator,:orgid,:orgname,:remark,:remark1,:remark2,:remark3,:remark4,:remark5,:remark6)
	 */
	public final static String addDlType="addDlType";
	/**
	 * 修改资料分类<P/>
	 * 'update dl_type set name=:name,moditime=:moditime,moditor=:moditor 
	 * where id=:id and compid=:compid'+sqlAppend(' and remark1=:remark1','remark1',PARAMS.remark1)
	 */
	public final static String updateDlType="updateDlType";
	/**
	 * 删除分类删除分类<P/>
	 * delete from dl_type where id=:id and compid=:compid
	 */
	public final static String deleteDlType="deleteDlType";
	/**
	 * 设置分类id的pid和排序号或分类id后面兄弟的pid和排序id+1<P/>
	 * 'update dl_type SET pid=:pid'+sqlAppend(' ,orderid=:orderId ','orderId',PARAMS.orderId)+sqlAppend(' ,orderid=orderid+:change ','change',PARAMS.change)+' 
	 * where 1=1 '
	 * +sqlAppend(' and pid=:changePid ','changePid',PARAMS.changePid)
	 * +sqlAppend(' and id=:id ','id',PARAMS.id)
	 * +sqlAppend(' and orderid>=:downOrder ','downOrder',PARAMS.downOrder)
	 * +sqlAppend(' and orderid<=:upOrder ','upOrder',PARAMS.upOrder)
	 */
	public final static String updateDlTypeOrder="updateDlTypeOrder";
	/**
	 * 获取资料分类的列表<P/>
	 * select id,pid,name as label,false as isedit,false as isshow,false as checked,orderid,"folder" as icon,true as disabled from dl_type 
	 * where compid=:compid
	 */
	public final static String ListDlType = "ListDlType";
	/**
	 * 修改文件数据<P/>
	 * update dl_file set viewfilepath=:viewfilepath,viewtype=:viewtype,viewsize=:viewsize 
	 * where id=:id and compid=:compid
	 */
	public final static String updateDlFile="updateDlFile";
	/**
	 * 查询下级id<P/>
	 * select id from dl_type 
	 * where pid=:id and compid=:compid
	 */
	public final static String getChildrenId="getChildrenId";
	/**
	 * 获取该分类的儿子数量<P/>
	 * select count(1) as sum from dl_type 
	 * where pid=? and compid=?
	 */
	public final static String getChildrenSum="getChildrenSum";
	/**
	 * 获取目标分类的pid和排序号<P/>
	 * select id,compid,year,name,pid,orderid,createtime,moditime,creator,moditor,orgid,orgname,remark from dl_type 
	 * where 1=1 and id=:id and compid=:compid
	 */
	public final static String getDlType="getDlType";
	/**
	 * 获取重复的分类名称的分类<P/>
	 * select * from dl_type 
	 * where name=? and compid=?
	 */
	public final static String getDlTypeByName="getDlTypeByName";
	/**
	 * 获取最小排序号<P/>
	 * select min(orderid) as min from dl_type 
	 * where pid=:pid and compid=:compid order by orderid
	 */
	public final static String getMinOrderid="getMinOrderid";
	/**
	 * 获取父分类的pid<P/>
	 * select pid from dl_type 
	 * where id=? and compid=?
	 */
	public final static String getPid="getPid";
	//管理器
	
	/**
	 * 修改资料管理器<P/>
	 * update dl_datas 
	 * set name=:name,year=:year,month=:month,typeid=:typeid,typename=:typename,remark=:remark,moditime=:moditime,moditor=:moditor 
	 * where id=:id and compid=:compid
	 */
	public final static String updateDlDatas="updateDlDatas";
	/**
	 * 查询文件关联表的数据<P/>
	 * select count(*) as sum from dl_file_business 
	 * where fileid=:fileid and compid=:compid and businessname!=:businessname
	 */
	public final static String list_File_Businessname="list_File_Businessname";
	/**
	 * 查询下级资源管理器的总数<P/>
	 * select count(*) as sum from dl_datas 
	 * where typeid=:id and compid=:compid
	 */
	public final static String getSubordinatesSum="getSubordinatesSum";
	/**
	 * 查询文件<P/>
	 * select * from dl_file 
	 * where id=:id and compid=:compid
	 */
	public final static String getFiles="getFiles";
	/**
	 * 删除文件<P/>
	 * delete from dl_file 
	 * where id=:id and compid=:compid
	 */
	public final static String deleteFile="deleteFile";
	/**
	 * 新增附件关联<P/>
	 * insert into dl_file_business (fileid,businessid,businessname,compid,intostatus) 
	 * values (:fileid,:businessid,:businessname,:compid,:intostatus)
	 */
	public final static String addDlFileBusiness="addDlFileBusiness";
	/**
	 * 新增文件<P/>
	 * insert into dl_file (compid,name,dir,size,type,usertor,remark,createtime,remark1,remark2,remark3,remark4,remark5,remark6) 
	 * values (:compid,:name,:dir,:size,:type,:usertor,:remark,:createtime,:remark1,:remark2,:remark3,:remark4,:remark5,:remark6)
	 */
	public final static String addDlFile="addDlFile";
	/**
	 * 新增资料管理器<P/>
	 * insert into dl_datas (compid,name,year,month,typeid,typename,remark,createtime,creator,filetype,subjectid,subjectname,remark1,remark2,remark3,remark4,remark5,remark6) 
	 * values (:compid,:name,:year,:month,:typeid,:typename,:remark,:createtime,:creator,:filetype,:subjectid,:subjectname,:remark1,:remark2,:remark3,:remark4,:remark5,:remark6)
	 */
	public final static String addDlDatas="addDlDatas";
	/**
	 * 查询文件通过资料管理器id<P/>
	 * select a.id,a.compid,a.name,a.dir,a.size,a.type,a.viewfilepath,a.viewtype,a.viewsize,a.usertor,a.remark,a.createtime,a.remark1,a.remark2,a.remark3,a.remark4,a.remark5,a.remark6 from dl_file a left join dl_file_business b on a.id=b.fileid 
	 * where b.businessid=:id and b.businessname=:businessname and a.compid=:compid and b.compid=:compid
	 */
	public final static String listFilesByBusiness="listFilesByBusiness";
	/**
	 * 获取资料管理器下的所有文件的数量<P/>
	 * 'select count(*) as sum from dl_file_business 
	 * where businessid=:businessid and businessname=:businessname and compid=:compid' +sqlAppend(' and fileid=:fileid','fileid',PARAMS.fileid)+sqlAppend(' and intostatus=:intostatus','intostatus',PARAMS.intostatus)
	 */
	public final static String getFilesSum="getFilesSum";
	/**
	 * 删除资源管理器<P/>
	 * delete from dl_datas 
	 * where id=? and compid=?
	 */
	public final static String deleteDlDatas="deleteDlDatas";
	/**
	 * 修改附件关联表的管理器id<P/>
	 * update dl_file_business set businessid=? 
	 * where fileid=? and businessname=? and compid=?
	 */
	public final static String update_File_Businessname="update_File_Businessname";
	/**
	 * 文件重命名<P/>
	 * update dl_file set name=:name 
	 * where id=:id and compid=:compid
	 */
	public final static String update_file_name="update_file_name";
	/**
	 * 获取资料管理器详情<P/>
	 * select * from dl_datas 
	 * where id=:id and compid=:compid
	 */
	public final static String getDlDatas="getDlDatas";
	/**
	 * 分页资料管理器<P/>
	 * select id,compid,name,year,month,typeid,typename,remark,createtime,moditime,creator,moditor,filetype,subjectid,subjectname,remark1,remark2,remark3,remark4,remark5,remark6 from dl_datas 
	 * where typeid=:id and compid=:compid
	 */
	public final static String pageDatas="pageDatas";
	/**
	 * 获取分类下的所有文件夹集合<P/>
	 * select id,compid,name,year,month from dl_datas 
	 * where typeid=:typeid and compid=:compid
	 */
	public final static String listDatasAll="listDatasAll";
	/**
	 * 获取分类下，组织机构下的管理器名称重复的数量<P/>
	 * 'select count(*) as nameSum from dl_datas 
	 * where typeid=:typeid and name=:name and compid=:compid' 
	 * +sqlAppend(' and subjectid=:subjectid','subjectid',PARAMS.subjectid)
	 * +sqlAppend(' and creator=:creator','creator',PARAMS.creator)
	 * +sqlAppend(' and id!=:id','id',PARAMS.id)
	 */
	public final static String getDatasNameSum="getDatasNameSum";
	/**
	 * 获取分类下有多少个文件夹<P/>
	 * select count(*) as num from dl_datas 
	 * where typeid=:typeId and compid=:compid
	 */
	public final static String getDatasNum="getDatasNum";
	/**
	 * 根据业务id和公司id删除附件关联<P/>
	 * delete from dl_file_business  
	 * where businessid = ? and businessname=? and compid = ?
	 */
	public final static String deleteFileByBusinessId="deleteFileByBusinessId";
	/**
	 * 删除没有关联业务的文件关联表<P/>
	 * delete from dl_file_business 
	 * where fileid=? and compid=? and businessname=?
	 */
	public final static String deleteFileBusinessAll="deleteFileBusinessAll";
	/**
	 * 通过文件id获取管理器的所有其他文件对象<P/>
	 * select c.* from dl_file_business a right join dl_file_business b on a.businessid=b.businessid join dl_file c on b.fileid=c.id 
	 * where a.fileid=:fileid and a.businessname=:businessname and a.compid=:compid and b.compid=:compid and c.compid=:compid and b.fileid!=:fileid
	 */
	public final static String getFileidBrother="getFileidBrother";
	/**
	 * 获取所有儿子分类的集合<P/>
	 * select id,pid,name as label,false as isedit,false as isshow,false as checked,orderid,"folder" as icon,true as disabled from dl_type 
	 * where pid=? and compid=? and name like ?
	 */
	public final static String listSun="listSun";
	public static String updateDlTypeOrderDown="updateDlTypeOrderDown";
	public static String updateDlTypeOrderUp="updateDlTypeOrderUp";
	/**
	 * select dir from dl_file where id=:id and compid=:compid
	 */
	public static String getFilesDir="getFilesDir";
	public static String listFilesNameByBusiness="listFilesNameByBusiness";
	public static String getFileName="getFileName";
	
}
