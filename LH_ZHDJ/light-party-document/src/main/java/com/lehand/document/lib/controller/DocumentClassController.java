package com.lehand.document.lib.controller;

import java.util.List;
import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.document.lib.dto.DlTypeDto;
import com.lehand.document.lib.pojo.DlType;
import com.lehand.document.lib.service.DocumentClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.lehand.base.common.Message;

/**
 * 资源分类
 * 
 * @author huruohan
 * @data 2019年4月10日16:47:31
 *
 */
@Api(value = "资源分类", tags = { "资源分类" })
@RestController
@RequestMapping("/document/class")
public class DocumentClassController extends DocumentBaseController {
	
	private static final Logger logger = LogManager.getLogger(DocumentClassController.class);
	
	@Resource
	private DocumentClassService documentClassService;
	
	//层级的限制
	@Value("${jk.class.level}")
	private String level;
	/**
	 * 新增分类
	 * @param  dlType : name 分类名称  pid 上级分类  serial 序号  remarks 备注
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "新增分类", notes = "新增分类", httpMethod = "POST")
	@RequestMapping("/addClass")
	public Message addDlType(DlType dlType){
		Message message = null;
		Long result = null;
		try {
			result = documentClassService.addDlType(dlType,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("新增资料分类成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 修改分类
	 * @param name 修改的分类名称
	 * @return
	 */
	@OpenApi(optflag=2)
    @ApiOperation(value = "修改分类", notes = "修改分类", httpMethod = "POST")
	@RequestMapping("/updateClass")
	public Message updateDlType(Long id,String name){
		Message message = null;
		Integer result = null;
		try {
			result = documentClassService.updateDlType(id,name,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("修改分类名称成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 删除资料分类
	 * @param id
	 * @return
	 */
	@OpenApi(optflag=3)
    @ApiOperation(value = "删除资料分类", notes = "删除资料分类", httpMethod = "POST")
	@RequestMapping("/deleteClass")
	public Message deleteDlType(Long id){
		Message message = null;
		Integer result = null;
		try {
			result = documentClassService.deleteDlType(id,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("删除资料分类成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 拖动排序
	 * @param srcId 拖动源的分类id
	 * @param targetId 拖动目标的分类id
	 * @param type 0：在里面，1：在上面，2：在下面
	 * @return
	 */
	@OpenApi(optflag=2)
    @ApiOperation(value = "拖动排序", notes = "拖动排序", httpMethod = "POST")
	@RequestMapping("/moveClass")
	public Message moveDlType(Long srcId,Long targetId,String type){
		Message message =null;
		Integer result = null;
		try {
			result = documentClassService.moveDlType(srcId,targetId,type,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("拖动排序资源分类成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 资料分类列表查询
	 * @param like 名称模糊查询
	 * @param type 0:资料分类  1:资料管理
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "资料分类列表查询", notes = "资料分类列表查询", httpMethod = "POST")
	@RequestMapping("/listClass")
	public Message listDlType(String type,String like){
		Message message = null;
		List<DlTypeDto> result= null;
		try {
			result = documentClassService.listDlType(type,like,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("资料分类列表查询成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	/**
	 * 新增需求
	 * 准备新增时做出的判断
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "新增需求", notes = "新增需求（准备新增时做出的判断）", httpMethod = "POST")
	@RequestMapping("/limitAddDlType")
	public Message limitAddDlType(Long id){
		Message message = null;
		Boolean result= null;
		try {
			result = documentClassService.limitAddDlType(id,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("可以新增资料分类");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@OpenApi(optflag=0)
	@ApiOperation(value = "判断当前菜单是否可以新增子菜单", notes = "判断当前菜单是否可以新增子菜单", httpMethod = "POST")
	@RequestMapping(value = "/checkMenuLev", method= RequestMethod.POST )
	@ApiImplicitParams({
			@ApiImplicitParam(name = "fctid", value = "菜单id", dataType = "Long", example = "1")
	})
	public Message<Object> checkMenuLev(Long fctid){
		Message<Object> msg = new Message<Object>();
		msg.setData(documentClassService.checkMenuLev(fctid,getSession()));
		return msg.success();
	}
	
	
//	/**
//	 * 新增需求
//	 * 准备新增时做出的判断
//	 */
//	@RequestMapping("/demo")
//	public Message demo(String like){
//		Message message = null;
//		List<DlTypeDto> result= null;
//		try {
//			result = documentClassService.demo(like,getSession());
//			message=Message.SUCCESS;
//			message.setData(result);
//			message.setMessage("可以新增资料分类");
//		} catch (Exception e) {
//			message=Message.FAIL;
//			message.setMessage("不可新增资料分类:"+e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		return message;
//	}
//	
	
	
}
