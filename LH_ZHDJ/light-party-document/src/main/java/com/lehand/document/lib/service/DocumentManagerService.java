package com.lehand.document.lib.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.FileUtil;
import com.lehand.components.converter.doc.DocConverter;
import com.lehand.components.converter.doc.ExcelConverter;
import com.lehand.components.converter.doc.PPTConverter;
import com.lehand.components.converter.doc.WordConverter;
import com.lehand.components.converter.video.VideoFreamConverter;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.document.lib.constant.CommSqlCfgCode;
import com.lehand.document.lib.constant.Constant;
import com.lehand.document.lib.pojo.DlDatas;
import com.lehand.document.lib.pojo.DlFile;
import com.lehand.document.lib.pojo.DlFileBusiness;
import com.lehand.document.lib.util.CheckingUtil;
import com.lehand.document.lib.util.RegUtil;
import com.lehand.document.lib.util.SizeConvertUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * 资料管理
 * @author huruohan
 * @date 2019年5月13日 上午10:45:38
 */
@Service
public class DocumentManagerService {

	//url请求工具类
    @Resource private RestTemplate restTemplate;

    //高拍仪上传服务器路径
    @Value("${file-server.pdfUrl}")private String uploadPath;

    @Resource private GeneralSqlComponent generalSqlComponent;

    @Resource private DFSComponent dFSComponent;


	//上传路径
	@Value("${spring.http.multipart.location}")private String location;

	//上传大小限制
	@Value("${spring.http.multipart.max-file-size}")private String maxSize;

	//预览和上传的路径
	@Value("${jk.manager.down_ip}")private String downIp;

	/**
	 * 上传文件
	 * @author huruohan
	 * @date 2019年4月12日10:38:52
	 * @param file 文件   type 类型  0，不做操作，1，文件上传到未分类中  remark 备注
	 * @param userVO 当前用户
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public Map<String,Object> uploadFile(MultipartFile file, Session userVO) throws Exception {
		 //获取文件名称，包含后缀名
        String name=file.getOriginalFilename();
        String fileName=name.substring(0, name.lastIndexOf(Constant.SPORT));
        //文件名称的验证
        CheckingUtil.nameChecking(fileName, userVO, Constant.MAX_DLFILE_NAME);

        //获取文件大小
        Long size=file.getSize();
        synchronized (maxSize) {
            //将限制大小分割成数字和字母，即20，m
            List<String> reg= RegUtil.subStringByReg(maxSize,"\\d","[a-zA-Z]");
            //把限制大小转换成kb
            long maxKb= SizeConvertUtil.toKb(reg.get(0),reg.get(1));
            //判断文件的大小，如果超过限制的max大小则报错
            if(size>maxKb) {
                LehandException.throwException("上传文件超过限制的"+maxSize+"!");
            }
        }

        //获取文件的类型
        String filetype=name.substring(name.lastIndexOf(Constant.SPORT)+1).toLowerCase();
        String fileNameEnd=fileName+Constant.SPORT+filetype;

        name=fileName+Constant.SPORT+filetype;

        //计算出路径
        String path=location+"/"+fileNameEnd;
        //目标文件
        File data = new File(path);
        //如果文件不存在则新建
        if (!data.getParentFile().exists()) {
            data.getParentFile().mkdirs();
        }
        //文件的提交
        file.transferTo(data);
        String filePath=data.getPath();
        //获取文件上传后返回的路径
        String dir;
        synchronized (dFSComponent) {
            dir=dFSComponent.upload(filePath);
        }
        DlFile dlFile=new DlFile();
        //路径
        dlFile.setDir(dir);
        //文件名称
        dlFile.setName(fileNameEnd);
        //文件大小
        dlFile.setSize(size);
        //文件类型
        dlFile.setType(filetype);
        String remark=StringConstant.EMPTY;
        if(Constant.MP4.equalsIgnoreCase(filetype)) {
            String jpgPath=location+"/"+UUID.randomUUID().toString().replaceAll("-", "")+".jpg";
            new VideoFreamConverter().fetchFrame(data.getPath(),jpgPath,5);
            synchronized (dFSComponent) {
                remark=dFSComponent.upload(jpgPath);
            }
            FileUtil.delete(new File(jpgPath));
        }
        dlFile.setRemark(remark);
        //上传之后将本地文件删除
        FileUtil.delete(data);
        return initFile(StringConstant.EMPTY,dlFile,userVO);
	}


	@Transactional(rollbackFor = Exception.class)
	public Map<String,Object> uploadFile(File file, Session userVO) throws Exception {
		//获取文件名称，包含后缀名
		String name=file.getName();
		String fileName=name.substring(0, name.lastIndexOf(Constant.SPORT));
		//文件名称的验证
		CheckingUtil.nameChecking(fileName, userVO, Constant.MAX_DLFILE_NAME);

		//获取文件大小
		Long size=file.length();
		synchronized (maxSize) {
			//将限制大小分割成数字和字母，即20，m
			List<String> reg= RegUtil.subStringByReg(maxSize,"\\d","[a-zA-Z]");
			//把限制大小转换成kb
			long maxKb= SizeConvertUtil.toKb(reg.get(0),reg.get(1));
			//判断文件的大小，如果超过限制的max大小则报错
			if(size>maxKb) {
				LehandException.throwException("上传文件超过限制的"+maxSize+"!");
			}
		}

		//获取文件的类型
		String filetype=name.substring(name.lastIndexOf(Constant.SPORT)+1).toLowerCase();
		String fileNameEnd=fileName+Constant.SPORT+filetype;

		name=fileName+Constant.SPORT+filetype;

		//计算出路径
		String path=location+"/"+fileNameEnd;
		//目标文件
		File data = new File(path);
		//如果文件不存在则新建
		if (!data.getParentFile().exists()) {
			data.getParentFile().mkdirs();
		}
		//文件的提交
//		file.transferTo(data);
		String filePath=data.getPath();
		//获取文件上传后返回的路径
		String dir;
		synchronized (dFSComponent) {
			dir=dFSComponent.upload(filePath);
		}
		DlFile dlFile=new DlFile();
		//路径
		dlFile.setDir(dir);
		//文件名称
		dlFile.setName(fileNameEnd);
		//文件大小
		dlFile.setSize(size);
		//文件类型
		dlFile.setType(filetype);
		String remark=StringConstant.EMPTY;
		if(Constant.MP4.equalsIgnoreCase(filetype)) {
			String jpgPath=location+"/"+UUID.randomUUID().toString().replaceAll("-", "")+".jpg";
			new VideoFreamConverter().fetchFrame(data.getPath(),jpgPath,5);
			synchronized (dFSComponent) {
				remark=dFSComponent.upload(jpgPath);
			}
			FileUtil.delete(new File(jpgPath));
		}
		dlFile.setRemark(remark);
		//上传之后将本地文件删除
		FileUtil.delete(data);
		return initFile(StringConstant.EMPTY,dlFile,userVO);
	}

	/**
	 * 文件删除
	 * @author huruohan
	 * @date 2019年4月12日10:40:30
	 * @param fileid 文件id
	 * @param userVO 当前用户
	 */
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public Integer deleteFile(Long fileid, Session userVO) throws Exception {
//		//如果文件id为空或为0,则报错误信息
//		if(null==fileid||fileid.equals(0L)) {
//			LehandException.throwException("请勾选要删除的文件!");
//		}
//		Integer result=null;
//		Map<String,Object> param=new HashMap<String,Object>();
//		param.put("fileid",fileid);//文件id
//		param.put("compid",userVO.getCompid());//租户id
//		//获取文件关联表的数据集合
//		List<DlFileBusiness> DlFileBusinessList=generalSqlComponent.query(CommSqlCfgCode.list_File_Businessname,param);
//		//获取文件对象
//		param.put("id", fileid);
//		DlFile dlFileList=generalSqlComponent.query(CommSqlCfgCode.getFiles,param);
//		//判断该文件有没有关联业务
//		if(DlFileBusinessList.size()>0) {
//			for (DlFileBusiness dlFileBusiness : DlFileBusinessList) {
//				short businessname=dlFileBusiness.getBusinessname();
//				//文件关联了其它业务
//				if((short)0!=businessname) {
//					LehandException.throwException("文件已关联其它业务,请先删除业务中的文件!!");
//					//否则就是关联了资料管理器
//				}
//			}
//			//如果没有一个关联其它业务，则直接删除
//			//删除文件关联表根据文件id,删除文件id的所有关联表数据
//			generalSqlComponent.delete(CommSqlCfgCode.deleteFileBusinessAll,new Object[] {fileid,userVO.getCompid(),0});
//		}
//			//
//			//直接删除文件
//			dFSComponent.delete(dlFileList.getDir());
//			//删除文件的表数据
//			result=generalSqlComponent.delete(CommSqlCfgCode.deleteFile, param);
//		return result;
//	}



	/**
	 * 文件删除
	 * @author huruohan
	 * @date 2019年4月12日10:40:30
	 * @param fileid 文件id
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public Integer deleteFile(Long fileid, Session userVO) throws Exception {
		long compid=userVO.getCompid();
        //如果文件id为空或为0,则报错误信息
        if(null==fileid||fileid.equals(0L)) {
            LehandException.throwException("请勾选要删除的文件!");
        }
        Integer result=null;
        Map<String,Object> param=new HashMap<String,Object>(4);
        //文件id
        param.put("fileid",fileid);
        //租户id
        param.put("compid",compid);
        param.put("businessname",Constant.BUSINESSNAME_DATA);
        //获取文件关联表的数据集合
        int flag=generalSqlComponent.query(CommSqlCfgCode.list_File_Businessname,param);
        //获取文件对象
        param.put("id", fileid);
        String dir=generalSqlComponent.query(CommSqlCfgCode.getFilesDir,param);
        //判断该文件有没有关联业务
        if(flag>0) {
            //文件关联了其它业务
            LehandException.throwException("文件已关联其它业务,请先删除业务中的文件!!");
            //否则就是关联了资料管理器
            //如果没有一个关联其它业务，则直接删除
        }
        //删除文件关联表根据文件id,删除文件id的所有关联表数据
        generalSqlComponent.delete(CommSqlCfgCode.deleteFileBusinessAll,new Object[] {fileid,compid,0});

        synchronized (dFSComponent) {
            //直接删除文件
            dFSComponent.delete(dir);
            //删除文件的表数据
            result=generalSqlComponent.delete(CommSqlCfgCode.deleteFile, param);
        }
        return result;
	}


	/**
	 * 文件的预览
	 * 1.根据文件id获取文件数据
	 * 2.根据文件信息下载文件流
	 * 3.文件流转为File
	 * 4.office文件转为html，如果已转过就直接返回给前端
	 * 5.返回给前端
	 * @author huruohan
	 * @date 2019年4月12日10:47:03
	 * @param fileid 文件id
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public synchronized String viewFile(Long fileid, Session userVO) throws Exception {
		long compid=userVO.getCompid();
		//1
		Map<String,Object> param=new HashMap<String,Object>(2);
		param.put("id", fileid);
		param.put("compid", compid);
		//获取文件对象
		DlFile dlFileList=generalSqlComponent.query(CommSqlCfgCode.getFiles,param);
		//转换后的路径
		String viewPath=dlFileList.getViewfilepath();
		//类型
		String type=dlFileList.getType().toLowerCase();
		if(Constant.ZIP.equalsIgnoreCase(type)||Constant.RAR.equalsIgnoreCase(type)) {
			LehandException.throwException("压缩包无法预览!");
		}
		//文件路径
		String path=dlFileList.getDir();
		//如果转码地址本身就有的就直接返回
		if(StringUtils.hasLength(viewPath)) {
			return downIp+viewPath;
		}
		boolean flag="doc".equals(type)||"docx".equals(type)||
				"xls".equals(type)||"xlsx".equals(type)||
				"ppt".equals(type)||"pptx".equals(type);
		//文件格式
		if(!flag) {
		    //如果不是office文档就直接放回路径
		    return downIp+path;
		}
		//2、下载文件
		File target;
		byte[] fileByte=dFSComponent.download(path);
		//3、流转换成File
		File src=getFile(fileByte, location, path);
		//转码后的文件
		target=new File(location+"/"+path+".pdf");
		//4、office转为html
		if(!converter(src,target,type)) {
			LehandException.throwException("文件转码失败！");
		}
		//转码后的文件上传并修改数据库
		//上传
		viewPath=dFSComponent.upload(target.getPath());
        Long viewSize=FileUtil.sizeOf(target);
        String fileName=target.getName();
        String viewType=fileName.substring(fileName.lastIndexOf(Constant.SPORT)+1);
        DlFile dlFile=new DlFile();
        dlFile.setId(fileid);
        dlFile.setCompid(compid);
        dlFile.setViewfilepath(viewPath);
        dlFile.setViewsize(viewSize);
        dlFile.setViewtype(viewType);
        generalSqlComponent.update(CommSqlCfgCode.updateDlFile,dlFile);
        target.delete();
		//返回转码路径
		return downIp+viewPath;
	}




	/**
	 * 转码
	 *
	 * @author: huruohan
	 * @date: 2019年5月13日 下午2:10:14
	 * @param src
	 * @param target
	 * @param type
	 * @return
	 * @throws Exception
	 */
	private boolean converter(File src,File target,String type) throws Exception {
		DocConverter doc=null;
        synchronized (DocConverter.class) {
            //判断文件是不是需要转码，office都需要转码
            if(Constant.DOC.equals(type)||Constant.DOCX.equals(type)) {
                doc=new WordConverter();
            }else if(Constant.XLS.equals(type)||Constant.XLSX.equals(type)) {
                doc=new ExcelConverter();
            }else if(Constant.PPT.equals(type)||Constant.PPTX.equals(type)) {
                doc=new PPTConverter();
            }else {
                return false;
            }
            try {
                doc.convert(src, target);
            } catch (Exception e) {
                LehandException.throwException("文件格式不正确！");
            }
        }
        return true;
	}

    /**
     * 文件下载
     * @author lx
     * @date 2021-04-20 11:26
     * @param fileid 文件id
     * @param userVO 当前用户
     */
    @Transactional(rollbackFor = Exception.class)
    public void downFile(HttpServletRequest request, HttpServletResponse response, Long fileid, Session userVO) throws Exception {
        String userAgent = request.getHeader("User-Agent");
        response.setCharacterEncoding("UTF-8");

        Map<String,Object> map=new HashMap<String,Object>(2);
        map.put("id", fileid);
        map.put("compid", 1);
        DlFile dlFileList=generalSqlComponent.query(CommSqlCfgCode.getFiles,map);
        if(dlFileList==null){
            LehandException.throwException("文件不存在！");
        }
        //远程文件地址
        URL url = new URL(downIp+dlFileList.getDir());
        String formFileName = dlFileList.getName();

        String path=downIp+dlFileList.getDir();

        if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
            formFileName = java.net.URLEncoder.encode(formFileName, "UTF-8");
        } else {
            // 非IE浏览器的处理：
            formFileName = new String(formFileName.getBytes("UTF-8"), "ISO-8859-1");
        }
        response.setHeader("Content-disposition",
                String.format("attachment; filename=\"%s\"", formFileName));
        response.addHeader("Content-Type", "application/octet-stream");
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        try(InputStream in = conn.getInputStream()){
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            int readNum = -1;
            byte[] buff = new byte[1024];
            while ((readNum = in.read(buff)) != -1) {
                os.write(buff, 0, readNum);
                os.flush();
            }

            byte[] totalBuff = os.toByteArray();
            response.getOutputStream().write(totalBuff);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
	/**
	 * 文件下载
	 * @author huruohan
	 * @date 2019年4月12日14:37:14
	 * @param fileid 文件id
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public void downFile1(HttpServletRequest request, HttpServletResponse response, Long fileid, Session userVO) throws Exception {

		Map<String,Object> map=new HashMap<String,Object>(2);
		map.put("id", fileid);
		map.put("compid", userVO.getCompid());
		DlFile dlFileList=generalSqlComponent.query(CommSqlCfgCode.getFiles,map);
//		String path=dlFileList.getDir()+"?download=true"+"&attname="+dlFileList.getName();
		if(dlFileList==null){
			LehandException.throwException("文件不存在！");
		}
		//远程文件地址
		URL url = new URL(downIp+dlFileList.getDir());
		try {
			String fileName = dlFileList.getName();
			String agent = request.getHeader("User-agent");
//			if (agent.indexOf("Firefox") != -1) {
//				response.addHeader("content-Disposition", "attachment;fileName==?UTF-8?B?" + Base64.getEncoder().encodeToString(fileName.getBytes("utf-8")) + "?=");
//			} else if (agent.indexOf("Edge") != -1) {
				response.addHeader("content-Disposition", "attachment;fileName=" + URLEncoder.encode(fileName, "utf-8"));
//			} else {
//				response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
//			}

			response.addHeader("content-Type", "application/octet-stream");
			ServletOutputStream output = response.getOutputStream();
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			try(InputStream input = conn.getInputStream()){
				byte[] bytes;
				while (true){
					bytes = new byte[512];
					int byteNum = input.read(bytes);
					if(byteNum == -1){
						break;
					}else {
						output.write(bytes);
						output.flush();
					}
				}
			}catch (IOException e){
				throw e;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 从网络Url中下载文件
	 * @param urlStr
	 * @param fileName
	 * @param savePath
	 * @throws IOException
	 */
	public File downLoadFromUrl(String urlStr,String fileName,String savePath) throws IOException{
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		//设置超时间为3秒
		conn.setConnectTimeout(3*1000);
		//防止屏蔽程序抓取而返回403错误
		conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

		//得到输入流
		InputStream inputStream = conn.getInputStream();
		//获取自己数组
		byte[] getData = readInputStream(inputStream);

		//文件保存位置
		File saveDir = new File(savePath);
		//System.out.println(saveDir.getAbsolutePath());
		if(!saveDir.exists()){
			saveDir.mkdir();
		}
		File file = new File(saveDir+File.separator+fileName);
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(getData);
		if(fos!=null){
			fos.close();
		}
		if(inputStream!=null){
			inputStream.close();
		}
		return file;
	}
	/**
	 * 从输入流中获取字节数组
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public static  byte[] readInputStream(InputStream inputStream) throws IOException {
		byte[] buffer = new byte[1024];
		int len = 0;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		while((len = inputStream.read(buffer)) != -1) {
			bos.write(buffer, 0, len);
		}
		bos.close();
		return bos.toByteArray();
	}
	/**
	 * 资料管理器的保存
	 * @author huruohan
	 * @date 2019年4月12日14:49:30
	 * @param dlDatas 包含了分类id，分类名称，资料名称，年度，简介，filetype,所属主体类型，1组织，0，个人
	 * @param ids 附件id集合
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public Long saveDatas(DlDatas dlDatas, String ids, Session userVO) throws Exception {
		//名称的验证
	    dlDatas.setName(CheckingUtil.nameChecking(dlDatas.getName(), userVO, Constant.MAX_DLDATA_NAME));
		//id集合不为空时执行，附件id集合json转为集合对象
		List<Long> idList=null;
		if(StringUtils.hasLength(ids)) {
			idList=JSON.parseArray(ids, Long.class);
			if(Constant.MAX_DLFILE_SAVE<idList.size()) {
			    String message="共选择了"+idList.size()+",最多上传"+Constant.MAX_DLFILE_SAVE+"个文件!";
                LehandException.throwException(message);
            }
			if(!repeatFileName(idList,userVO)) {
				LehandException.throwException("有重复名称的文件!");
			}
		}
		Long dataId=0L;
		long compId=userVO.getCompid();
		long userId=userVO.getUserid();
		dlDatas.setCompid(compId);
		dlDatas.setMonth((short) 0);
		dlDatas.setRemark1(0);
		//没有id则是新增
		if(null==dlDatas.getId()) {
			dlDatas.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
			dlDatas.setCreator(userId);
			if(null==dlDatas.getFiletype()||dlDatas.getFiletype().equals((short)0)) {
                //设置为用户id和用户名称
                dlDatas.setSubjectid(userId);
                dlDatas.setSubjectname(userVO.getUsername());
              //如果主体类型为1   组织
            }else if(dlDatas.getFiletype().equals((short)1)) {
				//设置为组织机构id和组织机构名称
				dlDatas.setSubjectid(userVO.getCurrentOrg().getOrgid());
				dlDatas.setSubjectname(userVO.getCurrentOrg().getOrgname());
				//如果主体类型为0   个人
			}
			dataId=generalSqlComponent.insert(CommSqlCfgCode.addDlDatas, dlDatas);
		}else {//否则是修改
			dataId=dlDatas.getId();
			//修改时间
			dlDatas.setModitime(DateEnum.YYYYMMDDHHMMDD.format());
			//修改人
			dlDatas.setModitor(userId);
			generalSqlComponent.update(CommSqlCfgCode.updateDlDatas, dlDatas);
		}
		if(!CollectionUtils.isEmpty(idList)) {
			DlFileBusiness dlFileBusiness=new DlFileBusiness();
			//资料管理器id
			dlFileBusiness.setBusinessid(dataId);
			//关联业务模块
			dlFileBusiness.setBusinessname(Constant.BUSINESSNAME_DATA);
			//租户id
			dlFileBusiness.setCompid(compId);
			dlFileBusiness.setIntostatus((short)0);
			//遍历插入附件关联表
			for (Long fileid : idList) {
				dlFileBusiness.setFileid(fileid);
				int flag=generalSqlComponent.query(CommSqlCfgCode.getFilesSum, dlFileBusiness);
				if(0!=flag) {
					continue;
				}
				generalSqlComponent.insert(CommSqlCfgCode.addDlFileBusiness, dlFileBusiness);
			}
		}
		return dataId;
	}
	/**
	 * 删除资源管理器
	 * @author huruohan
	 * @date 2019年4月12日16:42:43
	 * @param dataId 资源管理器的id
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public Integer deleteDatas(Long dataId, Session userVO) throws Exception {
//		if(systemId.equals(dataId)) {
//			LehandException.throwException("未分类资料无法删除!");
//		}
		Map<String,Object> param=new HashMap<String,Object>(3);
		param.put("businessid", dataId);
		param.put("businessname", Constant.BUSINESSNAME_DATA);
		param.put("compid", userVO.getCompid());
		//先获取该资料管理器下的文件个数
		Integer sum=generalSqlComponent.query(CommSqlCfgCode.getFilesSum,param);
		//如果有文件则无法删除
		if(sum>0) {
			LehandException.throwException("该文件夹下有文件无法删除!");
		}
		//没有则直接删除
		return generalSqlComponent.delete(CommSqlCfgCode.deleteDlDatas, param);
	}

	/**
	 * 展示资料管理器
	 * @author huruohan
	 * @date 2019年4月12日16:50:10
	 * @param classId 分类id
	 * @param orgid 组织机构id
	 * @param like 模糊查询 名称
	 * @param page 分页
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public Pager listDatas(Long classId,Long orgid,String like,Pager page,Session userVO) throws Exception {

		//分类为空的验证
		if(null==classId) {
			List<Integer> rows=new ArrayList<Integer>();
			page.setRows(rows);
			return page;
		}
		//直接通过分类id连表查询出该分类的
		Map<String,Object> param=new HashMap<String,Object>(5);
		if(null==orgid) {
			param.put("userId",userVO.getUserid());
		}else {
			param.put("orgid",orgid);
		}
		param.put("id", classId);
		param.put("compid",userVO.getCompid());
		if(StringUtils.hasLength(like.trim())) {
			param.put("likeName", like);
		}
		//获取
		page=generalSqlComponent.pageQuery(CommSqlCfgCode.pageDatas, param,page);
		List<Map<String,Object>> result=(List<Map<String, Object>>) page.getRows();
		Map<String,Object> fileMap;
		List<Object> resultList;
		for (Map<String,Object> map : result) {
			param.put("id",map.get("id"));
			param.put("businessname", Constant.BUSINESSNAME_DATA);
			List<DlFile> dlFiles=generalSqlComponent.query(CommSqlCfgCode.listFilesByBusiness, param);
			resultList=new ArrayList<Object>();
			for (DlFile file : dlFiles) {
			    fileMap=new HashMap<String,Object>(5);
			    fileMap.put("id", file.getId());
			    fileMap.put("name", file.getName());
			    fileMap.put("size",file.getSize());
			    fileMap.put("icon", "source");
			    fileMap.put("checked",false);
				resultList.add(fileMap);
			}
			map.put("checked", false);
			map.put("checkedArr",new ArrayList<Object>());
			map.put("children", resultList);
			if(!CollectionUtils.isEmpty(resultList)) {
				map.put("disabled",false);
			}else {
				map.put("disabled", true);
			}
		}
		return page;
	}

	//1，1、资料管理器——>分类     srcIds,tagetIds,   type  2:资料管理器移到
	//2、文件——>资料管理器     srcIds,tagetIds    type  1:文件移到
	/**
	 * 文件、资源管理器移动
	 * @author huruohan
	 * @date 2019年4月12日19:08:06
	 * @param srcIds 源文件的id集合json
	 * @param targetId 目标资源管理器id
	 * @param userVO 当前用户
	 */
	@Transactional(rollbackFor = Exception.class)
	public Integer moveDatas(String srcIds,String targetId, Session userVO) throws Exception {
		if(null==targetId) {
			LehandException.throwException("请选择文件夹!");
		}
		long compid=userVO.getCompid();
		Map<String,Object> param=new HashMap<String,Object>(3);
		param.put("id", Long.valueOf(targetId));
		param.put("businessname", (short)0);
		param.put("compid",compid);

		//查询数据库存不存在目标资料文件夹
		Map<String,Object> dlDatas=generalSqlComponent.query(CommSqlCfgCode.getDlDatas, param);
		if(null==dlDatas) {
			LehandException.throwException("该资料文件夹不存在!");
		}
		//源id集合
		List<Long> srcIdList=JSON.parseArray(srcIds,Long.class);
		if(null==srcIdList) {
			LehandException.throwException("请选择文件!");
		}
//		//判断文件夹重复
//		StringBuilder existFileDatas=new StringBuilder();
		//判断文件名重复
//		StringBuilder existFileName=new StringBuilder();
		//先获取目标的所有文件集合
		List<String> dlFileNameList=generalSqlComponent.query(CommSqlCfgCode.listFilesNameByBusiness, param);
		Set<String> fileName=new HashSet<String>();
		for(Long id :srcIdList) {
			//判断源和目标是不是同一个文件夹   多个文件对应多个文件夹移到一个目标文件夹
			//判断文件名重复
			param.put("id", id);
			//将转移文件的id和目标的文件集合进行判断，相等的则把名字放进String容器
			String dlFileName2=generalSqlComponent.query(CommSqlCfgCode.getFileName, param);
			//分别把文件名称存进set中,有重复的边返回false,则报错。
			if(!fileName.add(dlFileName2)) {
				LehandException.throwException("转移的文件存在文件名重复!");
			}
			if(0<dlFileNameList.size()) {
				for(String dlFileName :dlFileNameList) {
					if(dlFileName.equals(dlFileName2)) {
						//existFileName.append(dlFile2.getName()+",");
						LehandException.throwException("转移的文件存在文件名重复!");
					}
				}
			}
		}
		//先获取源文件夹的
		Integer i=null;
		//文件移动
		DlFileBusiness dlFileBusiness=new DlFileBusiness();
		dlFileBusiness.setIntostatus((short)0);
		dlFileBusiness.setCompid(compid);
		dlFileBusiness.setBusinessname(Constant.BUSINESSNAME_DATA);
		dlFileBusiness.setBusinessid(Long.valueOf(targetId));
		for (Long srcId : srcIdList) {
			//修改附件关联表的管理器id
		    dlFileBusiness.setFileid(srcId);
			i=generalSqlComponent.update(CommSqlCfgCode.update_File_Businessname, dlFileBusiness);
			if(i<=0) {
				dlFileBusiness.setFileid(Long.valueOf(srcId));
				generalSqlComponent.insert(CommSqlCfgCode.addDlFileBusiness, dlFileBusiness);
			}
		}
		return i;
	}

	/**
	 * 获取资源管理器和文件的树列表
	 * @author huruohan
	 * @date 2019年4月13日11:54:36
	 * @param classId 分类id
	 * @param orgid 组织机构id
	 * @param userId 用户id
	 * @param type 类型0组织，1个人,2所有
	 * @param like 文件的模糊查询
	 * @param userVO 当前用户
	 * @return 资料和文件的树结构
	 */

	@Transactional(rollbackFor = Exception.class)
	public List<Map<String,Object>> listDatasAndFiles(Long classId, Long orgid, Long userId, Integer type, String like, Session userVO) throws Exception {
		if(null==classId) {
			LehandException.throwException("请选择分类!");
		}
		Map<String,Object> param=new HashMap<String,Object>(7);
		List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
		//将id集合字符串转为id集合
		param.put("typeid", classId);
		param.put("compid", userVO.getCompid());
		//现在暂时只有用户id
		//如果是组织，直接用组织结构id，如果是个人，并且给的组织机构和当前的组织机构id一样则用用户id，否则就用orgid，并把type转为1
		if(type.equals(0)){
            if(orgid.equals(userVO.getCurrentOrg().getOrgid())) {
                param.put("userId",userVO.getUserid());
            }else {
                param.put("orgid",orgid);
                type=1;
            }
        }else if(type.equals(1)) {
            param.put("orgid",orgid);
        }
		param.put("type", type);
		List<Map<String,Object>> dlDatasList =generalSqlComponent.query(CommSqlCfgCode.listDatasAll, param);
		if(CollectionUtils.isEmpty(dlDatasList)) {
			return new ArrayList<Map<String,Object>>();
		}
		//遍历获取
		for(Map<String,Object> dlDatas:dlDatasList) {
			dlDatas.put("checked", false);
			dlDatas.put("checkedArr", new ArrayList<Map<String,Object>>());
			param.put("id", dlDatas.get("id"));
			param.put("businessname",Constant.BUSINESSNAME_DATA);
			if(StringUtils.hasLength(like)) {
				param.put("likeName", like);
			}
			List<DlFile> dlFiles=generalSqlComponent.query(CommSqlCfgCode.listFilesByBusiness, param);
			List<Map<String,Object>> children=new ArrayList<Map<String,Object>>();
			Map<String,Object> fileMap;
			if(!CollectionUtils.isEmpty(dlFiles)) {
				for (DlFile file : dlFiles) {
					fileMap=new HashMap<String,Object>(5);
					fileMap.put("id", file.getId());
					fileMap.put("name", file.getName());
					fileMap.put("icon", "source");
					fileMap.put("checked",false);
					fileMap.put("size",file.getSize());
					children.add(fileMap);
				}
			}
			dlDatas.put("children",children);
			if(StringUtils.hasLength(like)) {
				if(!CollectionUtils.isEmpty(dlFiles)) {
					dlDatas.put("disabled",false);
					result.add(dlDatas);
				}
			}else {
				if(!CollectionUtils.isEmpty(dlFiles)) {
					dlDatas.put("disabled",false);
				}else {
					dlDatas.put("disabled", true);
				}
				result.add(dlDatas);
			}
		}
		return result;
	}



//	/**
//	 * 文件选择器
//	 * @author huruohan
//	 * @date 2019年4月13日13:04:55
//	 * @param ids文件的集合
//	 * @param targetId 目标资源管理器的id
//	 * @param type 关联业务模块  0 资料库  1 考核 2 任务部署 3 反馈
//	 * @param userVO 当前用户
//	 */
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public Integer fileChoose(String ids, Long targetId,String type, Session userVO) throws Exception {
//		if(StringUtils.isEmpty(ids)) {
//			LehandException.throwException("请勾选文件!");
//		}
//		Integer flag=0;
//		//文件id集合json转list集合
//		List<Long> idList=JSON.parseArray(ids, Long.class);
//		Long compid=userVO.getCompid();
//		Map<String,Object> param=new HashMap<String,Object>();
//		//如果有文件，则添加的目标资源管理器下
//		if(0>idList.size()) {
////			if(null==targetId||targetId.equals(0L)) {
////				//如果没有给出目标id,则将文件移到未分类的资源管理器下,
////				targetId=systemId;//未分类的id
////			}
//			for(Long fileid:idList) {
//				param.put("fileid", fileid);//文件id
//				param.put("businessid", targetId);//关联的管理器id
//				param.put("businessname", type);//关联的管理器名称
//				param.put("compid", compid);//租户id
//				param.put("intostatus", (short)0);
//				//修改文件的关联关系
//				flag=generalSqlComponent.update(CommSqlCfgCode.addDlFileBusiness, param);
//			}
//		}
//		return flag;
//	}



	/**
	 * 将字节流转为文件并保存到本地的方法
	 * @param data 字节流
	 * @param filePath 本地的路径，d:/upload
	 * @param fileName fastFds获取到的路径   group1/M01/00/02/wKg84Fyz5tuAI9soAAB8AOLLcUU944.jpg
	 * @return
	 * @throws IOException
	 */
	private File getFile(byte[] data, String filePath,String fileName) throws IOException {
		String targetPath=filePath+"/"+fileName;
	    File file  = new File(targetPath);
	    //文件存在则删除
	      if(file.exists()){
	         file.delete();
	      }
	      //创建路径以及文件
	      if(!file.getParentFile().exists()) {
	    	  try {
	    	        file.getParentFile().mkdirs();
	    	        file.createNewFile();
	    	    } catch (IOException e) {
	    	        e.printStackTrace();
	    	    }
	      }
	      //新建输出流，将字节输出到文件中
	      FileOutputStream fos = new FileOutputStream(file);
	      fos.write(data,0,data.length);
	      fos.flush();
	      fos.close();
	      return file;
		 }


	/**
	 * 重命名
	 * @author huruohan
	 * @date 2019年4月13日14:11:15
	 * @param id 要重命名的文件的id
	 * @param name 修改的后的名称
	 * @param userVO 当前用户
	 */

	@Transactional(rollbackFor = Exception.class)
	public String rename(Long id, String name, Session userVO) throws Exception {
		if(null==name) {
	        LehandException.throwException("文件名不能为空!");
	    }
	    String topName=name.substring(0,name.lastIndexOf("."));
	    //名称的验证
	    CheckingUtil.nameChecking(topName, userVO, Constant.MAX_DLFILE_NAME);
	    //验证特殊字符
        RegUtil.CheckingSpecialName(topName);
		Map<String,Object> param=new HashMap<String,Object>(3);
		//判断在管理器下有没有相同的文件名
		//fileNameRepeatOnDatas(id,name,userVO);
		//id
		param.put("id", id);
		//租户id
		param.put("compid", userVO.getCompid());
		//修改后的名称
		param.put("name", name);
		//修改文件名称
		generalSqlComponent.update(CommSqlCfgCode.update_file_name, param);
		//返回名称
		return name;
	}

	/**
	 * 资料管理器详情获取
	* @author huruohan
	* @date 2019年4月13日15:24:20
	* @param id 要获取详情的管理器id
	* @param userVO 当前用户
	*/

	@Transactional(rollbackFor = Exception.class)
	public Map<String,Object> getDlDatas(Long id,Session userVO) throws Exception {
		if(null==id) {
			return null;
		}
		//先获取资料管理器详情
		Map<String,Object> param=new HashMap<String,Object>(4);
		param.put("id", id);
		param.put("compid", userVO.getCompid());
		param.put("businessname", Constant.BUSINESSNAME_DATA);
		//获取资料管理器详情
		Map<String,Object> result=generalSqlComponent.query(CommSqlCfgCode.getDlDatas, param);
		//获取文件
		List<DlFile> files=generalSqlComponent.query(CommSqlCfgCode.listFilesByBusiness, param);
		//将文件集合添加到返回值里
		if(CollectionUtils.isEmpty(files)) {
			result.put("files", new ArrayList<DlFile>());
		}else {
			result.put("files", files);
		}
		return result;
	}


//	/**
//	 * @param type 0,不需要文件。。。1：需要文件   用不到
//	 */
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public List<Map<String,Object>> listDatasAll(Long classId,Long orgid,String type, String like, Session session) throws Exception {
//		
//		Map<String,Object> param=new HashMap<String,Object>();
//		param.put("id", classId);
//		if(null!=like&&!StringConstant.EMPTY.equals(like)) {
//			param.put("like", "%"+like+"%");
//		}
//		param.put("compid", session.getCompid());
//		if(null!=orgid&&0L!=orgid) {
//			param.put("orgid", orgid);
//		}else {
//			param.put("userId", session.getSessionid());
//		}
//		List<Map<String,Object>> result= generalSqlComponent.query(CommSqlCfgCode.listDatasAll, param);
//		
//		if("1".equals(type)) {
//			for (Map<String, Object> map : result) {
//				param=new HashMap<String,Object>();
//				param.put("id",map.get("id"));
//				param.put("compid", session.getCompid()); 
//				param.put("businessname", 0); 
//				List<DlFile> file=generalSqlComponent.query(CommSqlCfgCode.listFilesByBusiness, param);
//				map.put("file", file);
//			}
//		}
//		return result;
//	}

	/**
	 * 资料管理器重命名的判断
	 * @author huruohan
	 * @param classId 分类id
	 * @param id 编辑资料传的id
	 * @param type 类型 1组织，0个人
	 * @param name 名称
	 * @param userVO 当前用户
	 */

	@Transactional(rollbackFor = Exception.class)
	public Boolean duplicateName(Long classId,Long id,Integer type, String name, Session userVO) {
		Map<String,Object> param=new HashMap<String,Object>(6);
		param.put("name", name);
		param.put("typeid", classId);
		param.put("compid", userVO.getCompid());
		//如果主体类型为1   组织
        if(null==type||0==type) {
        //设置为用户id和用户名称
            param.put("creator",userVO.getUserid());
        }else if(1==type) {
            //设置为组织机构id和组织机构名称
            param.put("subjectid", userVO.getCurrentOrg().getOrgid());
            //如果主体类型为0   个人
        }
		if(null!=id&&!id.equals(0L)) {
			param.put("id", id);
		}
		Integer nameSum=generalSqlComponent.query(CommSqlCfgCode.getDatasNameSum, param);
		if(0!=nameSum) {
			//没有重复名称的资料
			return false;
		}
		//有重复名称的资料
		return true;
	}

	/**
	 * 保存资料和修改资料时判断文件名是否有重复的
	 * @param ids
	 * @param userVO
	 * @return
	 */
	private boolean repeatFileName(List<Long> ids,Session userVO) {
		 //存放文件名的set
        Set<String> fileSet=new HashSet<String>();
        Map<String,Object> param=new HashMap<String,Object>(2);
        param.put("compid",userVO.getCompid());
        //遍历获取文件对象
        for(Long id:ids) {
            param.put("id", id);
            DlFile dlFile=generalSqlComponent.query(CommSqlCfgCode.getFiles, param);
            //在HashSet中添加文件名,有重复的则添加失败并放回false
            if(!fileSet.add(dlFile.getName())) {
                return false;
            }
        }
        return true;
	}

	/**
	 * 高拍仪上传
	 */

	public Map<String, Object> highMeterUpload(String uuid, String pdfName , String newName, Session userVO) throws Exception{
		 String [] nameArr = newName.split("\\.");
	        String topName=nameArr[0];
	        if(Constant.MAX_DLFILE_NAME < topName.length()) {
	            LehandException.throwException("名称不能超过"+Constant.MAX_DLFILE_NAME+"个字符!");
	        }

	        //验证特殊字符
	        RegUtil.CheckingSpecialName(topName);
	        //uuid 跟 pdfname 不可为空，找不到原始文件
	        if(StringUtils.isEmpty(uuid) || StringUtils.isEmpty(pdfName)) {
	            LehandException.throwException("请选择文件!");
	        }

	        //获取文件上传后返回的路径
	        Map<String, Object> dirMap = uploadPdf(uuid, pdfName);
	        //获取文件的类型
	        String filetype="pdf";
	        DlFile dlFile=new DlFile();
	        //路径
	        dlFile.setDir(dirMap.get("dir").toString());
	        //文件名称
	        dlFile.setName(newName);
	        //文件大小
	        dlFile.setSize(Long.valueOf(dirMap.get("fileSize").toString()));
	        //文件类型
	        dlFile.setType(filetype);
	        //创建时间
	        dlFile.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
	        //租户id
	        dlFile.setCompid(userVO.getCompid());
	        //用户id
	        dlFile.setUsertor(userVO.getUserid());
	        //备注
	        dlFile.setRemark(StringConstant.EMPTY);
	        //继续初始化文件对象并插入数据库新数据
	        return initFile(Constant.GPY,dlFile,userVO);
	}

	/**
	 *
	 * @Title：请求文件服务获取fastdfs路径
	 * @param ：@param uuid
	 * @param ：@param pdfName
	 * @param ：@return
	 * @return ：String
	 * @throws
	 */
	private Map<String, Object> uploadPdf(String uuid,String pdfName) {
		//定义一个map对象
        Map<String, Object> bean = new HashMap<>(2);
        String url = uploadPath + "?uuid="+uuid+"&pdfName="+pdfName;
        String result = restTemplate.getForObject(url, String.class);
        Map<String,Object> data = JSON.parseObject(result, Map.class);
        if(Constant.UPLOAD_PDF_CODE_VALUE.equals(data.get(Constant.UPLOAD_PDF_CODE_KEY))) {
            bean = JSON.parseObject(data.get("data").toString(), Map.class);
        }
        return bean;
	}

	/**
     * 文件对象的初始化
     *
     * @author: huruohan
     * @date: 2019年5月13日 下午1:39:56
     * @param type
     * @param dlFile
     * @param userVO
     * @return
     */
    private Map<String,Object> initFile(String type,DlFile dlFile,Session userVO){
        //创建时间   
        dlFile.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        //租户id   
        dlFile.setCompid(userVO.getCompid());
        //用户id
        dlFile.setUsertor(userVO.getUserid());
        //备注1   用来判断是否是上传的文件，
        dlFile.setRemark1(0);
        //备注2
        dlFile.setRemark2(0);
        //备注3
        dlFile.setRemark3(StringConstant.EMPTY);
        //备注4
        dlFile.setRemark4(StringConstant.EMPTY);
        //备注5
        dlFile.setRemark5(StringConstant.EMPTY);
        //备注6
        dlFile.setRemark6(StringConstant.EMPTY);
        //新增文件数据
        Long fileId=generalSqlComponent.insert(CommSqlCfgCode.addDlFile, dlFile);
        dlFile.setId(fileId);
        //List<DlFile> dlFileList=generalSqlComponent.query(CommSqlCfgCode.listUpload, StringConstant.);
        Map<String,Object> result=new HashMap<String,Object>(8);
        //id
        result.put("id", dlFile.getId());
        String dir=dlFile.getDir();
        //上传到服务器后的地址
        result.put("dir", dir);
        //名称，包含后缀名
        result.put("name", dlFile.getName());
        //大小
        result.put("size", dlFile.getSize());
        //类型
        result.put("type", dlFile.getType());
//        if(Constant.GPY.equals(type)) {
            result.put("url",downIp+dir);
//        }
        //前端
        result.put("checked", false);
        //前端
        result.put("disabled", false);
        return result;
    }
}
