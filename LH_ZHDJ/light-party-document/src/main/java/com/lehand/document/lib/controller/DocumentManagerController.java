package com.lehand.document.lib.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.document.lib.pojo.DlDatas;
import com.lehand.document.lib.service.DocumentManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 资源管理器
 * 
 * @author huruohan
 * @data 2019年4月10日16:47:31
 *
 */
@Api(value = "文档管理", tags = { "文档管理" })
@RestController
@RequestMapping("/document/manager")
public class DocumentManagerController extends DocumentBaseController{
	private static final Logger logger = LogManager.getLogger(DocumentClassController.class);
	
	@Resource
	private DocumentManagerService documentManagerBusiness;
	
	/**
	 * 文件上传
	 * 
	 * @param file 文件  type 上传的类型 0、不操作，1、未分类  remark 备注
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "文件上传", notes = "文件上传", httpMethod = "POST")
	@RequestMapping("/uploadFile")
	public Message uploadFile(@RequestParam("file")MultipartFile file){
		Message message = null;
		Map<String,Object> result = null;
		try {
			result = documentManagerBusiness.uploadFile(file,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("上传文件成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 文件删除
	 * @param fileid 文件ID
	 * @return
	 */
	@OpenApi(optflag=3)
	@ApiOperation(value = "文件删除", notes = "文件删除", httpMethod = "POST")
	@RequestMapping("/deleteFile")
	public Message deleteFile(Long fileid){
		Message message = null;
		Integer result = null;
		try {
			result = documentManagerBusiness.deleteFile(fileid,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("文件删除成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 文件的预览
	 * @param fileid 文件id
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "文件的预览", notes = "文件的预览", httpMethod = "POST")
	@RequestMapping("/viewFile")
	public Message viewFile(Long fileid){
		Message message = null;
		String result = null;
		try {
			result = documentManagerBusiness.viewFile(fileid,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("预览成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 文件的下载
	 * @param fileid 文件id
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "文件的下载", notes = "文件的下载", httpMethod = "POST")
	@RequestMapping("/downFile")
	public Message downFile(HttpServletRequest request, Long fileid, HttpServletResponse response){
		Message message =null;
		try {
			documentManagerBusiness.downFile(request,response,fileid,getSession());
			message=Message.SUCCESS;
			message.setMessage("文件下载成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
			LehandException.throwException("文件下载成功");
		}
		return message;
	}

	/**
	 * 资料管理器新增
	 * @param dlDatas
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "资料管理器新增", notes = "资料管理器新增", httpMethod = "POST")
	@RequestMapping("/saveDatas")
	public Message saveDatas(DlDatas dlDatas, String ids){
		Message message =null;
		Long result = null;
		try {
			result = documentManagerBusiness.saveDatas(dlDatas,ids,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("新增成功!");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	
	/**
	 * 资料管理器修改
	 */
//	@RequestMapping("/updateDatas")
//	public Message updateDatas(DlDatas dlDatas,String ids){
//		Message message = null;
//		Integer result = null;
//		try {
//			result = documentManagerBusiness.updateDatas(dlDatas,ids,getSession());
//			message=Message.SUCCESS;
//			message.setData(result);
//			message.setMessage("资料管理器修改成功");
//		} catch (Exception e) {
//			message=Message.FAIL;
//			message.setMessage("资料管理器修改失败:"+e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		return message;
//	}
	
	/**
	 * 删除资料管理器
	 */
	@OpenApi(optflag=3)
	@ApiOperation(value = "删除资料管理器", notes = "删除资料管理器", httpMethod = "POST")
	@RequestMapping("/deleteDatas")
	public Message deleteDatas(Long dataId){
		Message message=null;
		Integer result = null;
		try {
			result = documentManagerBusiness.deleteDatas(dataId,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("删除资料管理器成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 展示资料管理器
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "展示资料管理器", notes = "展示资料管理器", httpMethod = "POST")
	@RequestMapping("/listDatas")
	public Message listDatas(Long classId,Long orgid,String like,Pager page){
		Message message = null;
		Pager result = null;
		try {
			result = documentManagerBusiness.listDatas(classId,orgid,like,page,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("查询资料管理器成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 资料转移
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "资料转移", notes = "资料转移", httpMethod = "POST")
	@RequestMapping("/moveDatas")
	public Message moveDatas(String srcIds ,String targetId){
		Message message = null;
		Integer result = null;
		try {
			result = documentManagerBusiness.moveDatas(srcIds,targetId, getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("资料转移成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 获取资源管理器和文件的树列表
	 * @author huruohan
	 * @date 2019年4月13日12:55:34
	 * @param classId 分类id
	 * @param orgid 组织机构id
	 * @param userId 用户id
	 * @param type 类型0组织，1个人
	 * @param like 文件的模糊查询
	 * 
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取资源管理器和文件的树列表", notes = "获取资源管理器和文件的树列表", httpMethod = "POST")
	@RequestMapping("/listDatasAndFiles")
	public Message listDatasAndFiles(Long classId,Long orgid,Long userId,Integer type,String like){
		Message message = null;
		List<Map<String,Object>> result = null;
		try {
			result = documentManagerBusiness.listDatasAndFiles(classId,orgid,userId,type,like, getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("获取资源管理器和文件的树列表成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 文件选择器
	 * @author huruohan
	 * @date 2019年4月13日12:58:06
	 * @param ids 文件id集合
	 * @param targetId 目标id
	 * @param type 关联业务模块  0 资料库  1 考核 2 任务部署 3 反馈
	 * @return
	 */
//	@RequestMapping("/fileChoose")
//	public Message fileChoose(String ids,Long targetId,String type){
//		Message message = null;
//		Integer result = null;
//		try {
//			result = documentManagerBusiness.fileChoose(ids, targetId,type,getSession());
//			message=Message.SUCCESS;
//			message.setData(result);
//			message.setMessage("文件选择器保存成功");
//		} catch (Exception e) {
//			message=Message.FAIL;
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		return message;
//	}
	/**
	 * 重命名
	 * @author huruohan
	 * @date 2019年4月13日12:58:06
	 * @param id 文件id
	 * @param name
	 * @descption type 关联业务模块  0 资料库  1 考核 2 任务部署 3 反馈  targetId 目标id
	 * @return
	 */
	@OpenApi(optflag=2)
    @ApiOperation(value = "重命名", notes = "重命名", httpMethod = "POST")
	@RequestMapping("/rename")
	public Message rename(Long id,String name){
		Message message = null;
		String result = null;
		try {
			result = documentManagerBusiness.rename(id, name,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("文件重命名成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	/**
	 * 获取资源管理器详情
	 * @param id
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "获取资源管理器详情", notes = "获取资源管理器详情", httpMethod = "POST")
	@RequestMapping("/getDlDatas")
	public Message getDlDatas(Long id){
		Message message = null;
		Map<String,Object> result= null;
		try {
			result = documentManagerBusiness.getDlDatas(id,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("资源分类列表查询成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
//	/**
//	 * 展示资料管理器 所有的数据
//	 */
//	@RequestMapping("/listDatasAll")
//	public Message listDatasAll(Long classId,Long orgid,String type,String like){
//		Message message =null;
//		List<Map<String,Object>>  result= null;
//		try {
//			result = documentManagerBusiness.listDatasAll(classId,orgid,type,like,getSession());
//			message=Message.SUCCESS;
//			message.setData(result);
//			message.setMessage("查询资料管理器成功");
//		} catch (Exception e) {
//			message=Message.FAIL;
//			message.setMessage("查询资料管理器失败:"+e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		return message;
//	}
	
	/**
	 * 新增资料管理器的判断条件
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "新增资料管理器的判断条件", notes = "新增资料管理器的判断条件", httpMethod = "POST")
	@RequestMapping("/duplicateName")
	public Message duplicateName(Long classId,Long id,Integer type, String name){
		Message message = null;
		Boolean result= null;
		try {
			result = documentManagerBusiness.duplicateName(classId,id,type,name,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("新增资料管理器的判断条件查询成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
	 * </br>
	 * 功能描述：
	 * @Package: com.lehand.document.lib.controller 
	 * @author: huruohan   
	 * @date: 2019年5月7日 下午12:44:12 
	 * @param pdfName
	 * @param newName
	 * @return
	 */
	@OpenApi(optflag=0)
    @ApiOperation(value = "highMeterUpload", notes = "highMeterUpload", httpMethod = "POST")
	@RequestMapping("/highMeterUpload")
	public Message highMeterUpload(String uuid,String pdfName,String newName){
		Message message = null;
		Map<String,Object> result = null;
		try {
			result= documentManagerBusiness.highMeterUpload(uuid,pdfName,newName,getSession());
			message=Message.SUCCESS;
			message.setData(result);
			message.setMessage("文件上传成功");
		} catch (Exception e) {
			message=Message.FAIL;
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
}
