package com.lehand.document.lib.controller;

import org.springframework.beans.factory.annotation.Value;

import com.lehand.components.web.controller.BaseController;

public class DocumentBaseController extends BaseController{

	private static final String STR = "/";

	@Value("${file-server.url}") 
	private String fileServerUrl;
	
	public String getFileHttpUrl(String uuid, Integer model) {
		StringBuilder sb = new StringBuilder(fileServerUrl).append(STR);
		sb.append(model).append(STR);
		sb.append(uuid).append(STR);
		return sb.toString();
	}
}
