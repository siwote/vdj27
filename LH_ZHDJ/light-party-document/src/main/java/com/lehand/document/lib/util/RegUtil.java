 package com.lehand.document.lib.util;

 import com.lehand.base.exception.LehandException;

 import java.util.ArrayList;
 import java.util.List;
 import java.util.regex.Matcher;
 import java.util.regex.Pattern;


 /**
  * 正则表达式工具
  * @author huruohan
  */
 public class RegUtil {
      private static Pattern p=null;
      private static Matcher m=null;
      private static String specialName=null;
      /**
       * 初始化
       * 功能描述：
       * @Package: com.lehand.document.lib.common.util
       * @author: huruohan
       * @date: 2019年5月17日 上午11:02:04
       */
      private static void initSpecialName() {
          if(null==specialName) {
             specialName="[\\/:*?\\\"<>|]{1,40}";
          }
      }

      /**
       * 功能描述：根据给的多个正则表达式，进行匹配切割
       * @author: huruohan
       * @date: 2019年5月17日 上午11:03:09
       * @param str 需要切割的字符串    23432dfsadf
       * @param regEx 正则表达式的 列   接受之后就是一个数组     "\\d","[a-zA-Z]"
       * @return  切割之后放回的字符串数组   ["23432","dfsadf"]
       */
      public static List<String> subStringByReg(String str,String... regEx){
          List<String> result=new ArrayList<String>();
          if(null==regEx||0>=regEx.length){
              return null;
          }
          for(String reg:regEx){
              p=Pattern.compile(reg);
              m=p.matcher(str);
              StringBuffer temp=new StringBuffer();
              while(m.find()){
                  temp.append(m.group());
              }
              result.add(temp.toString());
          }
          return result;

      }

      /**
       *
       * 功能描述：验证字符串是否包含特殊字符
       * @author: huruohan
       * @date: 2019年5月17日 上午11:06:52
       * @param str 要验证的字符串
       */
      public static void CheckingSpecialName(String str) {
          initSpecialName();
          p=Pattern.compile(specialName);
          m=p.matcher(str);
          if(m.find()) {
              LehandException.throwException("名称不可包含特殊字符!");
          }
      }
      public static void main(String[] args) {
          CheckingSpecialName("++\\");
     }
 }
