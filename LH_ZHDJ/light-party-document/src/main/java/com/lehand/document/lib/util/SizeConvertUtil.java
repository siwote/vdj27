 package com.lehand.document.lib.util;

import org.springframework.util.StringUtils;


 //文件大小转换工具
 public class SizeConvertUtil {

     /**
      * 功能描述：将mb格式转为kb格式
      * @author: huruohan
      * @date: 2019年5月17日 上午11:17:45
      * @param size 大小
      * @param format 格式  mb
      * @return
      */
      public static long toKb(String size,String format) {

          if(StringUtils.isEmpty(size)||StringUtils.isEmpty(format)) {
              return 0L;
          }
          long kb=0L;
          double sizeD=Double.valueOf(size);
          if("mb".equalsIgnoreCase(format)) {
              kb=(long)(sizeD*1048576);
          }
         return kb;
      }
 //     public static void main(String[] args) {
 //        System.out.println(toKb("20","m"));
 //    }
 }
