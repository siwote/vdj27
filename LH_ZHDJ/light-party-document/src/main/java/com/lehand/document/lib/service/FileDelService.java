package com.lehand.document.lib.service;

import com.lehand.base.common.Session;
import com.lehand.components.db.DBComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.document.lib.pojo.DlFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @ClassName:：定时任务删除文件
 * @author ：taogang
 * @date ：2019年5月14日 下午2:56:41
 *
 */
@Service
public class FileDelService {
	@Resource
	protected GeneralSqlComponent generalSqlComponent;
	private static final Logger logger = LogManager.getLogger(FileDelService.class);

	public DBComponent db() {
		return generalSqlComponent.getDbComponent();
	}
	/**
	 * 文件路径
	 */
	@Value("${spring.http.multipart.location}")
	private String ctxPath;

	/**
	 * 是否正在删除任务
	 */
	private static final AtomicBoolean DELFILE = new AtomicBoolean(false);

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	public void delFiles() {
		try {

			synchronized (DELFILE) {
				if(DELFILE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				DELFILE.set(true);
			}
			logger.info("定时操作删除临时文件开始...");
			threadPoolTaskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					deleteFile(ctxPath);
				}
			});
			logger.info("定时操作删除临时文件结束...");
		} catch (Exception e) {
			logger.error("定时操作删除临时异常",e);
		} finally {
			DELFILE.set(false);
		}
	}

	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}


	/**
	 * @Title：删除当前文件夹下所有文件，不包括自身
	 * @param ：@param path
	 * @param ：@return
	 * @return ：boolean
	 * @throws
	 */
	private static boolean deleteFile(String path) {
	    boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(path + tempList[i]);
            } else {
                temp = new File(path + File.separator + tempList[i]);
            }
            if (temp.isDirectory()) {
            	// 先删除文件夹里面的文件
            	deleteFile(path + "//" + tempList[i]);
                flag = true;
            }
            temp.delete();
        }
        return flag;
	}

	/**
	* @Description  获取附件信息
	* @Author zwd
	* @Date 2020/11/2 9:13
	* @param
	* @return
	**/
	public List<Map<String,Object>> getFiles(String fileids,Session session){
		List<Map<String,Object>> files = new ArrayList<Map<String,Object>>(0);
		if(!org.springframework.util.StringUtils.isEmpty(fileids)) {
			files = db().listMap("select * from dl_file where compid=? and id in ("+fileids+")", new Object[] {session.getCompid()});
		}
		return files;
	}

}
