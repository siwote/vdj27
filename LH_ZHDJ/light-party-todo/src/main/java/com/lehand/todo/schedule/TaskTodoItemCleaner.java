package com.lehand.todo.schedule;

import com.google.common.collect.Maps;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p> 党建考核自评.</p>
 *
 * @Author zl
 **/
//@Component("taskTodoItemCleaner")
public class TaskTodoItemCleaner implements TodoItemGenerator {
    private static final Logger log = LogManager.getLogger(TaskTodoItemCleaner.class);
    private static final String SQL_OF_TK_TASK = "SELECT COUNT(id) FROM tk_task_deploy WHERE compid = :compid AND id = :id AND endtime < :now";
    private static final String SQL_OF_TODO_ITEM = "SELECT id, compid, bizid, category FROM todo_item WHERE compid = :compid AND category = :category";

    @Resource
    private TodoItemService todoItemService;
    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @PostConstruct
    public void init() {
        log.info("{} 启动", this.getClass().getName());
    }

    /**
     * 超过既定时间，移除待办事项.
     */
    @Scheduled(cron = "${todo.taskTodo.cron}")
    @Override
    public void execute() {
        try {
            Map<String, Object> param = Maps.newHashMap();
            param.put("category", Todo.ItemType.TYPE_DEPLOY.getCode());
            param.put("compid", 1);
            List<TodoItemDTO> items = this.generalSqlComponent.getDbComponent().listBean(SQL_OF_TODO_ITEM, TodoItemDTO.class, param);
            if (items != null && items.size() > 0) {
                String now = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
                for (TodoItemDTO item : items) {
                    log.info("【任务部署待办事项移除】正在检查待办事项compid: {}， id: {}", item.getCompid(), item.getId());
                    Map<String, Object> paramMap = Maps.newHashMap();
                    paramMap.put("now", now);
                    paramMap.put("id", item.getBizid());
                    paramMap.put("compid", item.getCompid());
                    Integer result = this.generalSqlComponent.getDbComponent().getSingleColumn(SQL_OF_TK_TASK, paramMap, Integer.class);
                    if (result != null && result.intValue() > 0) {
                        todoItemService.deleteTodoItem(item);
                        log.info("【任务部署待办事项移除】完成移除待办事项compid: {}， id: {}", item.getCompid(), item.getId());
                    }
                }
            }
        } catch (Exception e) {
            log.warn("【任务部署待办事项移除】，发生异常：\n", e);
        }
    }
}
