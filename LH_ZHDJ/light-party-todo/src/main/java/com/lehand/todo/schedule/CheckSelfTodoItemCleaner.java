package com.lehand.todo.schedule;

import com.google.common.collect.Maps;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p> 党建考核自评.</p>
 *
 * @Author zl
 **/
@Component("checkSelfTodoItemCleaner")
public class CheckSelfTodoItemCleaner implements TodoItemGenerator {
    private static final Logger log = LogManager.getLogger(CheckSelfTodoItemCleaner.class);
    private static final String SQL_OF_EL_ASSESS = "SELECT COUNT(id) FROM el_assess WHERE compid = :compid AND id = :id AND enddate < :now";
    private static final String SQL_OF_TODO_ITEM = "SELECT id, compid, bizid, subjectid, category, orgid FROM todo_item WHERE compid = :compid AND category = :category AND deleted = 0 AND subjectid IS NOT NULL";
    private static final String SQL_OF_DELETE_TODO_ITEM = "UPDATE todo_item SET deleted = 1 WHERE id = :id";

    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @PostConstruct
    public void init() {
        log.info("{} 启动", this.getClass().getName());
    }

    /**
     * 超过既定时间，移除待办事项.
     */
    @Scheduled(cron = "${todo.checkSelf.cron}")
    @Override
    public void execute() {
        try {
            Map<String, Object> param = Maps.newHashMap();
            param.put("category", Todo.ItemType.TYPE_ASSESSMENT_SELF.getCode());
            param.put("compid", 1);
            List<TodoItemDTO> items = this.generalSqlComponent.getDbComponent().listBean(SQL_OF_TODO_ITEM, TodoItemDTO.class, param);
            if (items != null && items.size() > 0) {
                String now = DateFormatUtils.ISO_DATE_FORMAT.format(new Date());
                for (TodoItemDTO item : items) {
                    log.info("【党建考核自评移除】正在检查待办事项compid: {}， id: {}", item.getCompid(), item.getId());
                    Map<String, Object> paramMap = Maps.newHashMap();
                    paramMap.put("now", now);
                    paramMap.put("id", item.getSubjectid());
                    paramMap.put("compid", item.getCompid());
                    Integer result = this.generalSqlComponent.getDbComponent().getSingleColumn(SQL_OF_EL_ASSESS, paramMap, Integer.class);
                    if (result != null && result.intValue() > 0) {
                        this.generalSqlComponent.getDbComponent().update(SQL_OF_DELETE_TODO_ITEM, item);
                        log.info("【党建考核自评移除】完成移除待办事项compid: {}， id: {}, subjectid: {}", item.getCompid(), item.getId(), item.getSubjectid());
                    }
                }
            }
        } catch (Exception e) {
            log.warn("【党建考核自评移除】，发生异常：\n", e);
        }
    }
}
