package com.lehand.todo.constant;

/**
 * <p> 待办事项SQL编码表.</p>
 *
 * @Author zl
 * @Since 1.0.0
 **/
public interface TodoSqlCode {
    /**
     * 新增待办事项.
     */
    String SAVE_TODO_ITEM = "INSERT INTO todo_item(category, name, path, state, userid, username, time, readtime, bizid, orgid, compid, subjectid) VALUES (:category, :name, :path, :state, :userid, :username, :time, :readtime, :bizid, :orgid, :compid, :subjectid)";
    /**
     * 删除待办事项.
     */
    String DELETE_TODO_ITEM = "UPDATE todo_item SET deleted = 1 WHERE compid = :compid AND category = :category AND bizid = :bizid";
    /**
     * 查询待办事项.
     */
    String LIST_TODO_ITEM = "SELECT id, category, name, path, time FROM todo_item WHERE compid = :compid AND deleted = 0 AND state IN (0,1) AND category IN(:category)";
    /**
     * 查询待办事项.
     */
    String LIST_TODO_ITEM_OUTER = "SELECT id, category, name, path, time FROM (:unionSQL) a";
    /**
     * 更新待办事项状态.
     */
    String UPDATE_TODO_ITEM = "UPDATE todo_item SET state=:state, readtime=:readtime WHERE compid = :compid AND id = :id AND deleted = 0";

    /**
     * 查询是否是AB账号
     * select * from  urc_user_account where userid = :userid
     *  and (account = (select CONCAT("A",orgcode) from urc_organization where orgid = (select orgid from urc_user_org_map where userid = :userid))
     *   or account = (select CONCAT("B",orgcode) from urc_organization where orgid = (select orgid from urc_user_org_map where userid =:userid)))
     */
    public static String getABaccountList = "getABaccountList";

    /**
     * 查询是不是党支部数据
     * select * from t_dzz_bzcyxx where userid = (select userid from t_dy_info where zjhm = (select idno from urc_user where userid =:userid))
     * and organid = (select organid from t_dzz_info where organcode = :organcode) and zfbz =0 and dnzwname = '5100000017'
     */
    public static String getdzbsj = "getdzbsj";

    /**
     * 查询待办事项.
     */
    String LIST_TODO_ITEM_PARTY_TRANSFER = "SELECT id, category, name, path, time FROM todo_item WHERE compid = :compid AND deleted = 0 AND state IN (0,1) AND category IN(:category) and node in (:node)";
    /**
     * 退回待办事项状态.
     */
    String UPDATE_ROLLBACK_TODO_ITEM = "UPDATE todo_item SET deleted=0 WHERE compid = :compid AND deleted = 1 AND orgid=:orgid AND category =:category AND bizid=:bizid";
    /**
     * 根据bizid查询待办事项.
     */
    String LIST_TODO_ITEM_BY_BIZID = "SELECT * FROM todo_item  WHERE compid = ? AND category = ? AND bizid = ? AND orgid = ?";
}
