package com.lehand.todo.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.lehand.base.common.Org;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.web.interceptors.DefaultInterceptor;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.constant.TodoSqlCode;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.pojo.TodoItemPO;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p> 待办事项管理服务.</p>
 *
 * @Author zl
 **/
@Service
public class TodoItemService {
    private static final Logger log = LogManager.getLogger(TodoItemService.class);
    @Resource
    private GeneralSqlComponent generalSqlComponent;

    /**
     * 保存待办事项.
     *
     * @param itemDTO
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void createTodoItem(TodoItemDTO itemDTO) {
        TodoItemPO item = new TodoItemPO();
        item.setCategory(itemDTO.getCategory());
        item.setCompid(itemDTO.getCompid());
        item.setName(itemDTO.getName());
        item.setBizid(itemDTO.getBizid());
        item.setOrgid(itemDTO.getOrgid());
        item.setTime(itemDTO.getTime());
        item.setPath(itemDTO.getPath());
        item.setState(itemDTO.getState());
        item.setUserid(itemDTO.getUserid());
        item.setUsername(itemDTO.getUsername());
        item.setSubjectid(itemDTO.getSubjectid());
        this.generalSqlComponent.getDbComponent().insert(TodoSqlCode.SAVE_TODO_ITEM, item);
    }

    /**
     * 根据业务id删除对应的待办事项.
     *
     * @param itemDTO
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void deleteTodoItem(TodoItemDTO itemDTO) {
        String sql = TodoSqlCode.DELETE_TODO_ITEM;
        Session session = DefaultInterceptor.getSession();
        if (Todo.ItemType.TYPE_ASSESSMENT_SELF.getCode().equals(itemDTO.getCategory()) || Todo.ItemType.TYPE_ASSESSMENT_SCORE.getCode().equals(itemDTO.getCategory())) {
            if (session.getUserid() != null && session.getUserid().longValue() > 0) {
                sql = sql + " AND (orgid=" + session.getCurrentOrg().getOrgid() + " OR orgid=" + session.getUserid() + " OR orgid=" + Long.valueOf(session.getCurrentOrg().getOrgcode()) + ")";
            }
        }
        if (Todo.ItemType.TYPE_DEPLOY.getCode().equals(itemDTO.getCategory()) && itemDTO.getUserid() != null && itemDTO.getUserid().longValue() > 0) {
            sql = sql + " AND subjectid = " + session.getUserid();
        }
        if (Todo.ItemType.TYPE_MAINTAIN_MEETING.getCode().equals(itemDTO.getCategory())) {
            sql = sql + " AND (orgid=" + session.getCurrentOrg().getOrgid() + " OR orgid=" + session.getUserid() + ")";
        }
        this.generalSqlComponent.getDbComponent().update(sql, itemDTO);
    }

    /**
     * 更新待办事项状态.
     *
     * @param itemDTO
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void updateReadState(TodoItemDTO itemDTO) {
        Map<String, Object> param = Maps.newHashMap();
        param.put("id", itemDTO.getId());
        Date now = new Date();
        param.put("readtime", now);
        param.put("compid", itemDTO.getCompid());
        param.put("state", Todo.STATUS_ALREADY_READ);
        this.generalSqlComponent.getDbComponent().update(TodoSqlCode.UPDATE_TODO_ITEM, param);
    }

    /**
     * 分页查询待办事项.
     *
     * @param item
     */
    public Pager page(TodoItemDTO item) {
        Pager pager = item.getPage();
        if (pager == null) {
            pager = new Pager();
        }
        Session session = DefaultInterceptor.getSession();
        Map<String, Object> param = Maps.newHashMap();
        String subSql = buildQuerySql(item, param, session);
        if (subSql == null || subSql.trim().length() == 0) {
            return pager;
        }
        String sql = TodoSqlCode.LIST_TODO_ITEM_OUTER;
        sql = sql.replace(":unionSQL", subSql);
        param.put("compid", session.getCompid());
        pager.setUseSort(false);
//        pager.setDirection("time DESC");
        this.generalSqlComponent.getDbComponent().pageMap(pager, sql + " ORDER BY time DESC", param);
        setCategory(pager);
        return pager;
    }

    /**
     * 构建查询sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        StringBuilder sqlBuilder = new StringBuilder();
        // 完成任务反馈
        String taskFeedbackSql = buildFinishTaskFeedbackQuerySql(item, param, session);
        if (taskFeedbackSql != null) {
            sqlBuilder.append(taskFeedbackSql);
        }

        //任务反馈审核
        String taskFeedAuditSql = buildTaskFeedAuditQuerySql(item, param, session);
        if (taskFeedAuditSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(taskFeedAuditSql);
        }

        //下发任务反馈审核
        String taskDeployAuditSql = buildTaskDeployAuditQuerySql(item, param, session);
        if (taskDeployAuditSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(taskDeployAuditSql);
        }

        //会议
        String meetingSql = buildMeetingQuerySql(item, param, session);
        if (meetingSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(meetingSql);
        }

        //党费
        String partySql = buildPartyDuesQuerySql(item, param, session);
        if (partySql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(partySql);
        }
        //自评
        String partyCheckSelfSql = buildPartyCheckSelfQuerySql(item, param, session);
        if (partyCheckSelfSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(partyCheckSelfSql);
        }
        //评分
        String partyScoreSql = buildPartyScoreQuerySql(item, param, session);
        if (partyScoreSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(partyScoreSql);
        }
        //党员迁移
        String partyTransferSql = buildPartyTransferQuerySql(item, param, session);
        if (partyTransferSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(partyTransferSql);
        }
        //工作汇报审核
        String reportrAuditQuerySql = buildReportrAuditQuerySql(item, param, session);
        if (partyTransferSql != null) {
            if (sqlBuilder.length() > 0) {
                sqlBuilder.append(" UNION ");
            }
            sqlBuilder.append(reportrAuditQuerySql);
        }
        return sqlBuilder.toString();
    }

    /**
     * 工作汇报审核sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildReportrAuditQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<Integer> categorySet = Sets.newHashSet();
            categorySet.add(Todo.ItemType.TYPE_MEETING_REPORT_AUDIT.getCode());
            Integer[] arr = new Integer[categorySet.size()];
            categorySet.toArray(arr);
            category = fromArray(arr);
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND orgid = " + session.getCurrentOrg().getOrgcode();
        return sql;
    }
    private String buildTaskDeployAuditQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    if (Todo.ROLE_TASK_AUDIT.equals(roleCode)) {
                        categorySet.add(Todo.ItemType.TYPE_ISSUE_TASK.getCode());
                    }
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND (subjectid = " + session.getUserid() + " OR subjectid = " + session.getCurrentOrg().getOrgid() + ")";
        return sql;
    }

    /**
     * 完成任务反馈.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildFinishTaskFeedbackQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    if (Todo.ROLE_DW.equals(roleCode) || Todo.ROLE_DZZ.equals(roleCode) || Todo.ROLE_DZB.equals(roleCode)) {
                        categorySet.add(Todo.ItemType.TYPE_DEPLOY.getCode());
                    }
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND (subjectid = " + session.getUserid() + " OR subjectid = " + session.getCurrentOrg().getOrgid() + ")";
        return sql;
    }

    /**
     * 任务反馈审核sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildTaskFeedAuditQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    if (Todo.ROLE_DW.equals(roleCode) || Todo.ROLE_DZZ.equals(roleCode) || Todo.ROLE_DZB.equals(roleCode)) {
                        categorySet.add(Todo.ItemType.TYPE_REPORT_TASK.getCode());
                    }
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND (subjectid = " + session.getUserid() + " OR subjectid = " + session.getCurrentOrg().getOrgid() + ")";
        return sql;
    }

    /**
     * 会议部署sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildMeetingQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    log.info("rolecode:{}, org:{} {}, username: {}", roleCode, session.getCurrentOrg().getOrgid(), session.getCurrentOrg().getOrgcode(), session.getUsername());
                    if (Todo.ROLE_DW.equals(roleCode) || Todo.ROLE_DZZ.equals(roleCode) || Todo.ROLE_DZB.equals(roleCode)) {
                        categorySet.add(Todo.ItemType.TYPE_MAINTAIN_MEETING.getCode());
                        categorySet.add(Todo.ItemType.TYPE_RETURNED_MEETING.getCode());
                    }

                    if (Todo.ROLE_DY.equals(roleCode)) {
                        categorySet.add(Todo.ItemType.TYPE_ATTEND_MEETING.getCode());
                    }
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND orgid = " + session.getCurrentOrg().getOrgid();
        return sql;
    }

    /**
     * 党费审核sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildPartyDuesQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    if (Todo.ROLE_DW.equals(roleCode) || Todo.ROLE_DZZ.equals(roleCode)) {
                        categorySet.add(Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES.getCode());
                    }
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND orgid = " + session.getCurrentOrg().getOrgid();
        return sql;
    }

    /**
     * 党费考核自评sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildPartyCheckSelfQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    Set<Integer> catSet = Sets.newHashSet();
                    if (Todo.ROLE_DW.equals(roleCode) || Todo.ROLE_DZZ.equals(roleCode) || Todo.ROLE_DZB.equals(roleCode)) {
                        catSet.add(Todo.ItemType.TYPE_ASSESSMENT_SELF.getCode());
                    }
                    categorySet.addAll(catSet);
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND orgid = " + session.getCurrentOrg().getOrgcode();
        return sql;
    }

    /**
     * 党费考核评分sql.
     *
     * @param item
     * @param param
     * @param session
     * @return
     */
    private String buildPartyScoreQuerySql(TodoItemDTO item, Map<String, Object> param, Session session) {
        String sql = TodoSqlCode.LIST_TODO_ITEM;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            Set<String> roleCodes = session.getRoleCodes();
            if (roleCodes != null && roleCodes.size() > 0) {
                Set<Integer> categorySet = Sets.newHashSet();
                for (String roleCode : roleCodes) {
                    Set<Integer> catSet = Sets.newHashSet();
                    if (Todo.ROLE_SCORE_MAN.equals(roleCode)) {
                        catSet.add(Todo.ItemType.TYPE_ASSESSMENT_SCORE.getCode());
                    }
                    categorySet.addAll(catSet);
                }
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }
        sql = sql.replace(":category", category);
        sql = sql + " AND orgid = " + session.getUserid();
        return sql;
    }
    /**
    * @Description 党员迁移sql
    * @Author zwd
    * @Date 2020/11/30 10:32
    * @param
    * @return
    **/
    private String buildPartyTransferQuerySql(TodoItemDTO item, Map<String, Object> param, Session session){
        String sql = TodoSqlCode.LIST_TODO_ITEM_PARTY_TRANSFER;
        if (StringUtils.isNotEmpty(item.getTimebegin())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') >= :timebegin ";
            param.put("timebegin", item.getTimebegin().trim());
        }
        if (StringUtils.isNotEmpty(item.getTimeend())) {
            sql = sql + " AND DATE_FORMAT(time, '%Y-%m-%d') <= :timeend ";
            param.put("timeend", item.getTimeend().trim());
        }
        String category = null;
        String node = null;
        //是否AB账号
        Map<String,Object> parmsab = new HashMap<String,Object>(1);
        parmsab.put("userid",session.getUserid());
        List<Map<String,Object>> ABlist = generalSqlComponent.query(TodoSqlCode.getABaccountList,parmsab);

        //是否支部书记
        Map<String,Object> parmszbsj = new HashMap<String,Object>(2);
        parmszbsj.put("userid",session.getUserid());
        parmszbsj.put("organcode",session.getCurrentOrg().getOrgcode());
        Map<String,Object> mapzbsj = generalSqlComponent.query(TodoSqlCode.getdzbsj,parmszbsj);
        //当前登录是否是党委
        Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from t_dzz_info where organcode=? and `zzlb` = '6100000035'", new Object[]{session.getCurrentOrg().getOrgcode()});

        if (item.getCategory() == null || item.getCategory().intValue() <= 0) {
            if(ABlist.size()>0||mapzbsj!=null||map!=null){
                Set<Integer> categorySet = Sets.newHashSet();
                Set<Integer> catSet = Sets.newHashSet();
                catSet.add(Todo.ItemType.TYPE_PARTY_TRANSFER.getCode());
                categorySet.addAll(catSet);
                if (categorySet.size() == 0) {
                    return null;
                }
                Integer[] arr = new Integer[categorySet.size()];
                categorySet.toArray(arr);
                category = fromArray(arr);
            }

//            if (roleCodes != null && roleCodes.size() > 0) {
//                Set<Integer> categorySet = Sets.newHashSet();
//                for (String roleCode : roleCodes) {
//                    Set<Integer> catSet = Sets.newHashSet();
//                    if (Todo.ROLE_SCORE_MAN.equals(roleCode)) {
//                        catSet.add(Todo.ItemType.TYPE_ASSESSMENT_SCORE.getCode());
//                    }
//                    categorySet.addAll(catSet);
//                }
//                if (categorySet.size() == 0) {
//                    return null;
//                }
//                Integer[] arr = new Integer[categorySet.size()];
//                categorySet.toArray(arr);
//                category = fromArray(arr);
//            }
        } else {
            category = String.valueOf(item.getCategory());
        }
        if (category == null) {
            return null;
        }

        //账号不会同时是AB账号和支部书记
        if(ABlist.size()>0){
            node = "0,3";
        }
        if(mapzbsj!=null){
            node = "1,2";
        }
        if(map!=null){
            node = "4";
        }
        sql = sql.replace(":category", category);
        sql = sql.replace(":node", node);
        sql = sql + " AND orgid = " + session.getCurrentOrg().getOrgcode();
        return sql;
    }

    /**
     * 根据category code设置category名称.
     *
     * @param pager
     */
    private void setCategory(Pager pager) {
        if (pager.getRows() != null) {
            List<Map<String, Object>> rows = (List<Map<String, Object>>) pager.getRows();
            for (Map<String, Object> row : rows) {
                Object category = row.get("category");
                if (category != null) {
                    row.put("categoryStr", Todo.ItemType.getName((Integer) category));
                }
            }
        }
    }

    /**
     * 可查看的待办事项类型.
     */
    public List<Todo.ItemType> getCurrentUserCategory() {
        List<Todo.ItemType> itemTypes = Lists.newArrayList();
        for (Todo.ItemType itemType : Todo.ItemType.values()) {
            itemTypes.add(itemType);
        }
        return itemTypes;
    }

    /**
     * 可查看的待办事项类型.
     */
    @Deprecated
    public List<Todo.ItemType> getCurrentUserCategoryDes() {
        Session session = DefaultInterceptor.getSession();
        Set<String> roleCodeSet = session.getRoleCodes();
        Set<Todo.ItemType> categorySet = Sets.newHashSet();
        if (roleCodeSet != null && roleCodeSet.size() > 0) {
            for (String roleCode : roleCodeSet) {
                if (Todo.ROLE_DW.equals(roleCode) || Todo.ROLE_DZZ.equals(roleCode) || Todo.ROLE_DZB.equals(roleCode)) {
                    categorySet.add(Todo.ItemType.TYPE_MAINTAIN_MEETING);
                    categorySet.add(Todo.ItemType.TYPE_RETURNED_MEETING);
                }
                if (Todo.ROLE_DY.equals(roleCode)) {
                    categorySet.add(Todo.ItemType.TYPE_ATTEND_MEETING);
                }
            }
        }

        List<Org> orgs = session.getOrgs();
        if (orgs != null && orgs.size() > 0) {
            Org org = orgs.get(0);
            if (org.getOrgtypeid() != null) {
                String orgtypeid = String.valueOf(org.getOrgtypeid());
                //党委
                if (Todo.DW.equals(orgtypeid)) {
                    categorySet.addAll(getPartyCommitteeCategory());
                }
                //党总支
                if (Todo.DZZ.equals(orgtypeid)) {
                    categorySet.addAll(getGeneralPartyBranchCategory());
                }
                //党支部
                if (Todo.DZB.equals(orgtypeid)) {
                    categorySet.addAll(getPartyBranchCategory());
                }
                //党员
                boolean isPartyRole = false;
                Set<String> roleCodes = session.getRoleCodes();
                if (roleCodes != null && roleCodes.size() > 0) {
                    for (String roleCode : roleCodes) {
                        if (Todo.PARTY_MEMBER_ROLE.equals(roleCode)) {
                            isPartyRole = true;
                            break;
                        }
                    }
                }
                if (isPartyRole) {
                    categorySet.addAll(getPartyCategory());
                }
            }
        }

        List<Todo.ItemType> itemTypes = Lists.newArrayList();
        if (categorySet.size() > 0) {
            for (Todo.ItemType itemType : categorySet) {
                itemTypes.add(itemType);
            }
        }
        return itemTypes;
    }

    /**
     * 党委.
     *
     * @return
     */
    private List<Todo.ItemType> getPartyCommitteeCategory() {
        return Arrays.asList(Todo.ItemType.TYPE_MAINTAIN_MEETING
                , Todo.ItemType.TYPE_ASSESSMENT_SELF
                , Todo.ItemType.TYPE_ASSESSMENT_SCORE
                , Todo.ItemType.TYPE_DEPLOY
                , Todo.ItemType.TYPE_ISSUE_TASK
                , Todo.ItemType.TYPE_REPORT_TASK
                , Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES
                , Todo.ItemType.TYPE_RETURNED_MEETING);
    }

    /**
     * 总支.
     *
     * @return
     */
    private List<Todo.ItemType> getGeneralPartyBranchCategory() {
        return Arrays.asList(Todo.ItemType.TYPE_MAINTAIN_MEETING
                , Todo.ItemType.TYPE_ASSESSMENT_SELF
                , Todo.ItemType.TYPE_ASSESSMENT_SCORE
                , Todo.ItemType.TYPE_DEPLOY
                , Todo.ItemType.TYPE_ISSUE_TASK
                , Todo.ItemType.TYPE_REPORT_TASK
                , Todo.ItemType.TYPE_AUDITING_PARTY_EXPENSES
                , Todo.ItemType.TYPE_RETURNED_MEETING);
    }

    /**
     * 支部.
     *
     * @return
     */
    private List<Todo.ItemType> getPartyBranchCategory() {
        return Arrays.asList(Todo.ItemType.TYPE_MAINTAIN_MEETING
                , Todo.ItemType.TYPE_ASSESSMENT_SELF
                , Todo.ItemType.TYPE_ASSESSMENT_SCORE
                , Todo.ItemType.TYPE_DEPLOY
                , Todo.ItemType.TYPE_ISSUE_TASK
                , Todo.ItemType.TYPE_REPORT_TASK
                , Todo.ItemType.TYPE_RETURNED_MEETING);
    }

    /**
     * 党员.
     *
     * @return
     */
    private List<Todo.ItemType> getPartyCategory() {
        return Arrays.asList(Todo.ItemType.TYPE_ATTEND_MEETING
                , Todo.ItemType.TYPE_ISSUE_TASK);
    }

    private String fromArray(Object[] array) {
        String result = Arrays.toString(array);
        return result.substring(1, result.length() - 1);
    }

    /**
     *退回会议代办事项
     * @param itemDTO
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void rollBackTodoItem(TodoItemDTO itemDTO) {
        this.generalSqlComponent.getDbComponent().update(TodoSqlCode.UPDATE_ROLLBACK_TODO_ITEM, itemDTO);
    }
    /**
     *根据bizid获取会议代办事项
     * @param itemDTO
     */
    public TodoItemDTO getTodoItem(TodoItemDTO itemDTO) {
        return  this.generalSqlComponent.getDbComponent().getBean(TodoSqlCode.LIST_TODO_ITEM_BY_BIZID,TodoItemDTO.class,new Object[]{itemDTO.getCompid(),itemDTO.getCategory(),itemDTO.getBizid(),itemDTO.getOrgid()});
    }
}
