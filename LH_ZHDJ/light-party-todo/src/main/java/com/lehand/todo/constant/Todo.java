package com.lehand.todo.constant;

import com.lehand.base.exception.LehandException;

/**
 * <p> 待办事项类型.</p>
 *
 * @Author zl
 **/
public interface Todo {
    /**
     * 未读.
     */
    Integer STATUS_NO_READ = 0;
    /**
     * 已读.
     */
    Integer STATUS_ALREADY_READ = 1;
    /**
     * 已办.
     */
    Integer STATUS_ALREADY_DO = 2;

    /**
     * 党员
     */
    String PARTY_MEMBER_ROLE = "PARTYMEMBER";
    /**
     * 党委 .
     */
    String DW = "6100000035";
    /**
     * 党总支 .
     */
    String DZZ = "6200000036";
    /**
     * 党支部 .
     */
    String DZB = "6300000037";

    /**
     * 党委角色.
     */
    String ROLE_DW = "PARTYCOMMIT";
    /**
     * 党总支角色.
     */
    String ROLE_DZZ = "PARTYSUPPORT";
    /**
     * 党支部角色.
     */
    String ROLE_DZB = "PARTYBRANCH";
    /**
     * 党员角色.
     */
    String ROLE_DY = "PARTYMEMBER";
    /**
     * 任务审核人角色.
     */
    String ROLE_TASK_AUDIT = "100012";
    /**
     * 考核评分人角色.
     */
    String ROLE_SCORE_MAN = "100013";

    enum ItemType {
        TYPE_MAINTAIN_MEETING(1, "维护会议记录"),
        TYPE_ATTEND_MEETING(2, "参加会议"),
        TYPE_ASSESSMENT_SELF(3, "党建考核自评"),
        TYPE_ASSESSMENT_SCORE(4, "党建考核评分"),
        TYPE_DEPLOY(5, "完成反馈任务"),
        TYPE_ISSUE_TASK(6, "下发任务审核"),
        TYPE_REPORT_TASK(7, "上报任务审核"),
        TYPE_AUDITING_PARTY_EXPENSES(8, "党费审核"),
        TYPE_RETURNED_MEETING(9, "会议记录退回"),
        TYPE_PARTY_TRANSFER(10, "党员迁移审核"),
        TYPE_MEETING_REPORT_AUDIT(11, "工作汇报审核"),
        TYPE_ASSESSMENT_SCORE_AUDIT(20, "党建考核评分审核"),;
        private Integer code;
        private String name;

        ItemType(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public static final String getName(Integer code) {
            if (code == null) {
                throw LehandException.throwException("can not find todo item type by your code in null.");
            }
            for (ItemType itemType : ItemType.values()) {
                if (itemType.code.equals(code)) {
                    return itemType.getName();
                }
            }
            throw LehandException.throwException(String.format("can not find todo item type by your code: %d", code));
        }
    }

    enum FunctionURL {
        BRANCH_COMMITTEE("/affairsManagement/orgLife/sessions/basicLef?mTypeName={mTypeName}&mType={mType}&handleType={handleType}&years={years}", "支部委员会"),
        BRANCH_PARTY_CONGRE("/affairsManagement/orgLife/sessions/basicLef?mTypeName={mTypeName}&mType={mType}&handleType={handleType}&years={years}", "支部党员大会"),
        PARTY_GROUP_MEETING("/affairsManagement/orgLife/sessions/basicLef?mTypeName={mTypeName}&mType={mType}&handleType={handleType}&years={years}", "党小组会"),
        PARTY_LECTURE("/affairsManagement/orgLife/sessions/basicLef?mTypeName={mTypeName}&mType={mType}&handleType={handleType}&years={years}", "上党课"),
        BRANCH_COMMITTEE1("/affairsManagement/orgLife/sessions/basicLef?mTypeName=支部委员会&mType=branch_committee&handleType=add&years=", "支部委员会"),
        BRANCH_PARTY_CONGRE2("/affairsManagement/orgLife/sessions/basicLef?mTypeName=支部党员大会&mType=branch_party_congress&handleType=add&years=", "支部党员大会"),
        PARTY_GROUP_MEETING3("/affairsManagement/orgLife/sessions/basicLef?mTypeName=党小组会&mType=party_group_meeting&handleType=add&years=", "党小组会"),
        PARTY_LECTURE4("/affairsManagement/orgLife/sessions/basicLef?mTypeName=上党课&mType=party_lecture&handleType=add&years=", "上党课"),
        DE_LIFE_MEETING("/affairsManagement/orgLife/delifemeeting", "民主生活会"),
        COMMITTEE_MEETING("/affairsManagement/orgLife/committeemeeting", "党委会"),
        THEME_PARTY_DAY("/affairsManagement/orgLife/themepartyday", "党员活动日"),
        ORG_LIFE_METING("/affairsManagement/orgLife/orglifemeting", "组织生活会"),
        APPR_MEMBERS("/affairsManagement/orgLife/apprmembers", "民主评议党员"),
        TASK_FEEDBACK_AUDIT("/task/lookTask", "任务上报审核"),
        MY_TASK("/task/myTask", "待办任务"),
        TASK_DEPLOY_AUDIT("/task/giveTask", "任务下发审核"),
        CHECK_SELF("/check/checkSelf", "考核自评"),
        CHECK_SELF_AUDIT("/check/checkSelfAudit", "考核自评审核"),
        CHECK_EVALUATE("/check/checkEvaluate", "考核评分"),
        PARTY_DUES("/dues/dfsjshDues", "党费审核"),
        MEETING_REPORT_AUDIT_PATH("/orgLife/reportReview", "工作汇报(待审核)"),
        MEETING_REPORT_AUDIT_BACK_PATH("/orgLife/workreport", "工作汇报(已驳回)"),
        ;
        private String path;
        private String name;

        FunctionURL(String path, String name) {
            this.path = path;
            this.name = name;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    enum MeetType {
        APPRAISAL_OF_MEMBERS("appraisal_of_members_time", "appraisal_of_members", "民主评议党员"),
        BRANCH_COMMITTEE("branch_committee_time", "branch_committee", "支部委员会"),
        PARTY_GROUP_MEETING("party_group_meeting_time", "party_group_meeting", "党小组会"),
        PARTY_LECTURE("party_lecture_time", "party_lecture", "上党课"),
        ORG_LIFE_MEETING("org_life_meeting_time", "org_life_meeting", "组织生活会"),
        THEME_PARTY_DAY("theme_party_day_time", "theme_party_day", "党员活动日"),
        BRANCH_PARTY_CONGRESS("branch_party_congress_time", "branch_party_congress", "支部党员大会"),
        DEMOCRATIC_LIFE_MEETING("democratic_life_meeting_time", "democratic_life_meeting", "民主生活会"),
        COMMITTEE_MEETING("committee_meeting_time", "committee_meeting", "党委会"),
        ;
        private String timeCode;
        private String nameCode;
        private String nameText;

        MeetType(String timeCode, String nameCode, String nameText) {
            this.timeCode = timeCode;
            this.nameCode = nameCode;
            this.nameText = nameText;
        }

        public String getTimeCode() {
            return timeCode;
        }

        public String getNameCode() {
            return nameCode;
        }

        public String getNameText() {
            return nameText;
        }
    }
    /*会议周期 1：每年；2：每半年；3：每季度；4：每月*/
    /**
     * 每年
     */
    Integer YEAR = 1;
    /**
     * 每半年
     */
    Integer HALF_A_YEAR = 2;
    /**
     * 每季度
     */
    Integer QUARTER = 3;
    /**
     * 每月
     */
    Integer A_MONTH = 4;

    /*常量字段*/
    Integer CONSTANT_ZERO = 0;
    Integer CONSTANT_ONE = 1;
}
