package com.lehand.todo.pojo;

import com.lehand.base.common.Pager;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * 待办事项实体类.
 *
 * @author zl
 */
@ApiModel(value = "待办事项", description = "待办事项")
public class TodoItemDTO {
    @ApiModelProperty(value = "ID", name = "id")
    private Long id;

    @ApiModelProperty(value = "类别", name = "category")
    private Integer category;

    @ApiModelProperty(value = "事项", name = "name")
    private String name;

    @ApiModelProperty(value = "路径", name = "path")
    private String path;

    @ApiModelProperty(value = "时间", name = "timestr")
    private String timestr;

    @ApiModelProperty(value = "时间", name = "time")
    private Date time;

    @ApiModelProperty(value = "起始时间", name = "timebegin")
    private String timebegin;

    @ApiModelProperty(value = "结束时间", name = "timeend")
    private String timeend;

    @ApiModelProperty(value = "业务ID", name = "bizid")
    private String bizid;

    @ApiModelProperty(value = "执行主体ID", name = "subjectid")
    private Long subjectid;

    @ApiModelProperty(value = "租户ID", name = "compid")
    private Long compid;

    @ApiModelProperty(value = "用户ID", name = "userid")
    private Long userid;

    @ApiModelProperty(value = "用户名", name = "username")
    private String username;

    @ApiModelProperty(value = "组织ID", name = "orgid")
    private Long orgid;

    @ApiModelProperty(value = "状态(0:未读,1:已读,2:已办)", name = "state")
    private Integer state;

    @ApiModelProperty(value = "分页", name = "page")
    private Pager page;

    @ApiModelProperty(value = "是否删除", name = "是否删除： 0.否,1.是")
    private Integer deleted;

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTimestr() {
        return timestr;
    }

    public void setTimestr(String timestr) {
        this.timestr = timestr;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimebegin() {
        return timebegin;
    }

    public void setTimebegin(String timebegin) {
        this.timebegin = timebegin;
    }

    public String getTimeend() {
        return timeend;
    }

    public void setTimeend(String timeend) {
        this.timeend = timeend;
    }

    public String getBizid() {
        return bizid;
    }

    public void setBizid(String bizid) {
        this.bizid = bizid;
    }

    public Long getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Long subjectid) {
        this.subjectid = subjectid;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Pager getPage() {
        return page;
    }

    public void setPage(Pager page) {
        this.page = page;
    }

}
