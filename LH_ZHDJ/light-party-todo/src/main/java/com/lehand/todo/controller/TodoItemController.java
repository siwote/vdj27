package com.lehand.todo.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lehand.base.common.Message;
import com.lehand.base.common.PagerUtils;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p> 待办事项接口控制器.</p>
 *
 * @Author zl
 * @Since 1.0.0
 **/
@Api(value = "工作台", tags = {"工作台"})
@RestController
@RequestMapping("/horn/workbench/todo")
public class TodoItemController extends BaseController {
    private static final Logger log = LogManager.getLogger(TodoItemController.class);
    @Resource
    private TodoItemService todoItemService;

    @OpenApi(optflag = 2)
    @ApiOperation(value = "已读代办事项已读", notes = "已读代办事项已读", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "当前会话ID", required = true, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "id", value = "事项ID", required = true, paramType = "query", dataType = "Long")
    })
    @PostMapping(value = "/read")
    public Message readTodoItem(@RequestParam Long id) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        itemDTO.setId(id);
        itemDTO.setCompid(getSession().getCompid());
        todoItemService.updateReadState(itemDTO);
        return Message.SUCCESS;
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "查询代办事项", notes = "查询代办事项", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "当前会话ID", required = true, paramType = "header", dataType = "String")
    })
    @PostMapping(value = "/query")
    public Message queryTodoItem(@RequestBody(required = false) TodoItemDTO item) {
        PagerUtils<Map<String, Object>> mapPagerUtils = new PagerUtils<>(todoItemService.page(item));
        return Message.SUCCESS.setData(mapPagerUtils);
    }

    @OpenApi(optflag = 0)
    @ApiOperation(value = "查询代办事项类型", notes = "查询代办事项类型", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionid", value = "当前会话ID", required = true, paramType = "header", dataType = "String")
    })
    @GetMapping(value = "/category")
    public Message queryCategory() {
        List<Todo.ItemType> currentUserCategory = todoItemService.getCurrentUserCategory();
        List<Map<String, Object>> result = Lists.newArrayList();
        if (currentUserCategory.size() > 0) {
            for (Todo.ItemType itemType : currentUserCategory) {
                Map<String, Object> category = Maps.newHashMap();
                category.put("code", itemType.getCode());
                category.put("name", itemType.getName());
                result.add(category);
            }
        }
        return Message.SUCCESS.setData(result);
    }
}
