package com.lehand.todo.pojo;

import java.io.Serializable;

/**
 * 待办事项实体类.
 *
 * @author zl
 */
public class TodoItemPO extends TodoItemDO implements Serializable {
    private static final long serialVersionUID = 65439529009949419L;
    /**
     * 所属组织类别id.
     */
    private String bizid;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBizid() {
        return bizid;
    }

    public void setBizid(String bizid) {
        this.bizid = bizid;
    }

    @Override
    public String toString() {
        return "TodoItemPO{" +
                "bizid='" + bizid + '\'' +
                "} " + super.toString();
    }
}
