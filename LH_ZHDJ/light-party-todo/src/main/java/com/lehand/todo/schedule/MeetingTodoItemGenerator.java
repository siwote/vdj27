package com.lehand.todo.schedule;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;

/**
 * <p> 会议待办任务生成器.</p>
 *
 * @Author zl
 **/
@Component("myMeetingTodoItemGenerator")
public class MeetingTodoItemGenerator implements com.lehand.todo.schedule.TodoItemGenerator {

    @Resource
    private TodoItemService todoItemService;

    private static final Logger log = LogManager.getLogger(MeetingTodoItemGenerator.class);
    /*查询党组织*/
    private static final String LIST_URC_ORGANIZATION = "SELECT * FROM `urc_organization` WHERE `orgtypeid` IN (6100000035,6200000036,6300000037)";
    /*是否有党小组*/
    private static final String BOOLEAN_HAS_PARTY_GROUP = "select count(*) from t_party_group where  compid= 1 and orgcode = ?";
    /*会议配置*/
    private static final String LIST_SYS_PARAM = "SELECT * FROM `sys_param` WHERE `upparamkey` = 'meeting_remind_time_set'";
    /*查询待办事项*/
    private static final String LIST_TODO_ITEM = "SELECT * FROM `todo_item` WHERE category =1 and compid =1 ";
    /*批量插入*/
    private static final String BATCH_INSERT_TODO_ITEM = "INSERT INTO todo_item(category, name, path, state, userid, username, time, bizid, orgid, compid) VALUES (:category, :name, :path, :state, :userid, :username, :time, :bizid, :orgid, :compid) ";
    /* 查询会议是否记录*/
    public static final String LIST_MEETING_RECORD_BY_STATUS = "select * from meeting_record where  status = ? and compid = ? and orgid =?";
    /*	根据会议id查询标签编码集合*/
    public static final String GET_MEETING_TAG_CODE_BY_MRID = "select tagcode  from meeting_tags_map where subjectid = ? and subjecttype = ? and compid = ?";
    /*根据bizid查询待办事项*/
    private static final String LIST_TODO_ITEM_BY_BIZID = "SELECT * FROM `todo_item` WHERE category =1 and compid =1 and bizid =? and orgid=?";
    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @PostConstruct
    public void init() {
        log.info("{} 启动", this.getClass().getName());
    }

//     @Scheduled(cron = "${todo.meeting.cron}")
//    @Override
    public void execute1() {
        try {
//            log.info("{}    {}", Thread.currentThread().getId(), Thread.currentThread().getName());
            /*查询党组织*/
            List<Map<String, Object>> listUrcOrganization = generalSqlComponent.getDbComponent().listMap(LIST_URC_ORGANIZATION, new Object[]{});
            /*会议周期 1：每年；2：每半年；3：每季度；4：每月*/
            if (listUrcOrganization.size() > 0 && listUrcOrganization != null) {
                for (Map<String, Object> map : listUrcOrganization) {
                    String orgtypeid = String.valueOf(map.get("orgtypeid"));
                    Long orgid = (Long) map.get("orgid");
                    /*是否有党小组*/
                    Integer count = generalSqlComponent.getDbComponent().getSingleColumn(BOOLEAN_HAS_PARTY_GROUP, Integer.class, new Object[]{map.get("orgcode")});
                    /* 查询会议是否开了*/
                    List<Map<String, Object>> meetingRecord = generalSqlComponent.getDbComponent().listMap(LIST_MEETING_RECORD_BY_STATUS, new Object[]{2, 1, orgid});
                    List<String> list = new ArrayList<>();
                    if (meetingRecord.size() > 0 && meetingRecord != null) {
                        for (Map<String, Object> m : meetingRecord) {
                            String mrtype = (String) m.get("mrtype");
                            Integer mryear = (Integer) m.get("mryear");
                            Integer mrmonth = (Integer) m.get("mrmonth");
                            //是否有标签编码
                            Long mrid = (Long) m.get("mrid");
                            List<Map<String, Object>> meetingTagsCode = generalSqlComponent.getDbComponent().listMap(GET_MEETING_TAG_CODE_BY_MRID,new Object[]{mrid, 0, 1});
                            if (meetingTagsCode.size() > 0) {
                                for (Map<String, Object> mstr: meetingTagsCode) {
                                    list.add(switchCode((String) mstr.get("tagcode"), mryear, mrmonth));
                                }
                            }
                            list.add(switchCode(mrtype, mryear, mrmonth));
                        }
                    }
                    LocalDate localDate = LocalDate.now();
                    int localMonthValue = localDate.getMonthValue();
                    int localYear = localDate.getYear();
                    /*查询待办事项*/
                    List<TodoItemDTO> listTodoItemDTO = generalSqlComponent.getDbComponent().listBean(LIST_TODO_ITEM, TodoItemDTO.class, new Object[]{});

                    List<TodoItemDTO> list1 = new ArrayList<>();
                    List<TodoItemDTO> list2 = new ArrayList<>();
                    List<TodoItemDTO> list3 = new ArrayList<>();
                    for (TodoItemDTO todoItemDTO : listTodoItemDTO) {
                        Long orgid1 = todoItemDTO.getOrgid();
                        if (orgid.equals(orgid1)) {
                            /*6100000035	党委   民主生活会*/
                            if (Todo.DW.equals(orgtypeid)) {
                                list1.add(todoItemDTO);
                            }
                            /*6200000036	党总支部   支部党委会，民主生活会*/
                            if (Todo.DZZ.equals(orgtypeid)) {
                                list2.add(todoItemDTO);
                            }
                            /*6300000037	党支部  三会一课，党员活动日，民主评议党员，组织生活，党委中心组学习*/
                            if (Todo.DZB.equals(orgtypeid)) {
                                list3.add(todoItemDTO);
                            }
                        }
                    }
                    //是否已经开过会议
                    boolean bc = true, bc1 = true, bc2 = true, bc3 = true, bc4 = true, bc5 = true, bc6 = true, bc7 = true, bc8 = true;
                    if (list != null && list.size() > 0) {
                        for (String str : list) {
                            /*民主生活会1 每年*/
                            if ((Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear).equals(str)) {
                                //已存在待办事项
                                log.info("已开过会议: \n" + str);
                                bc = false;
                            }
                            /*支部委员会4   每月*/
                            if ((Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue).equals(str)) {
                                //已存在待办事项
                                log.info("已开过会议: \n" + str);
                                bc1 = false;
                            }
                            /*党课3 每季度*/
                            if ((Todo.MeetType.PARTY_LECTURE.getNameCode() + localYear + getThisQuarter(localMonthValue)).equals(str)) {
                                //已存在待办事项
                                log.info("已开过会议: \n" + str);
                                bc2 = false;
                            }
                            if (count > 0) {
                                /*党小组会4   每月*/
                                if ((Todo.MeetType.PARTY_GROUP_MEETING.getNameCode() + localYear + localMonthValue).equals(str)) {
                                    //已存在待办事项
                                    log.info("已开过会议: \n" + str);
                                    bc3 = false;
                                }
                            }
                            /*支部党员大会3   每季度*/
                            if ((Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode() + localYear + getThisQuarter(localMonthValue)).equals(str)) {
                                //已存在待办事项
                                log.info("已存在待办事项:\n" + str);
                                bc4 = false;
                            }
                            /*党员活动日4   每月*/
                            if ((Todo.MeetType.THEME_PARTY_DAY.getNameCode() + localYear + localMonthValue).equals(str)) {
                                //已存在待办事项
                                log.info("已开过会议: \n" + str);
                                bc5 = false;
                            }
                            /*民主评议党员1   每年*/
                            if ((Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode() + localYear).equals(str)) {
                                //已存在待办事项
                                log.info("已开过会议: \n" + str);
                                bc6 = false;
                            }
                            /*组织生活2   每半年*/
                            if ((Todo.MeetType.ORG_LIFE_MEETING.getNameCode() + localYear + halfAYear(localMonthValue)).equals(str)) {
                                //已存在待办事项
                                log.info("已开过会议: \n" + str);
                                bc7 = false;
                            }
                        }
                    }
                    /*6100000035	党委   民主生活会1*/
                    if (Todo.DW.equals(orgtypeid)) {
                        if (list1 != null && list1.size() > 0) {
                            boolean b1 = true;
                            for (TodoItemDTO t1 : list1) {
                                /*民主生活会1 每年*/
                                if ((Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear).equals(t1.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项: \n" + Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear);
                                    b1 = false;
                                }
                            }
                            if (b1 && bc) {
                                /*民主生活会1   每年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameText() + "(" + localYear + "年)",
                                        Todo.FunctionURL.DE_LIFE_MEETING.getPath());
                            }
                        } else {
                            if (bc) {
                                /*民主生活会1   每年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameText() + "(" + localYear + "年)",
                                        Todo.FunctionURL.DE_LIFE_MEETING.getPath());
                            }
                        }
                    }
                    /*6200000036	党总支部   支部党委会4，民主生活会1*/
                    if (Todo.DZZ.equals(orgtypeid)) {
                        if (list2 != null && list2.size() > 0) {
                            boolean b21 = true, b22 = true;
                            for (TodoItemDTO t2 : list2) {
                                /*民主生活会1   每年*/
                                if ((Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear).equals(t2.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear);
                                    b21 = false;
                                }
                                /*支部委员会4   每月*/
                                if ((Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue).equals(t2.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue);
                                    b22 = false;
                                }
                            }
                            if (b21 && bc) {
                                /*民主生活会1   每年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameText() + "(" + localYear + "年)",
                                        Todo.FunctionURL.DE_LIFE_MEETING.getPath());
                            }
                            if (b22 && bc1) {
                                /*支部委员会4   每月*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameText() + "(" + localMonthValue + "月份)",
                                        Todo.FunctionURL.BRANCH_COMMITTEE1.getPath() + localYear);
                            }
                        } else {
                            if (bc) {
                                /*民主生活会1   每年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode() + localYear,
                                        Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameText() + "(" + localYear + "年)",
                                        Todo.FunctionURL.DE_LIFE_MEETING.getPath());
                            }
                            if (bc1) {
                                /*支部委员会4   每月*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameText() + "(" + localMonthValue + "月份)",
                                        Todo.FunctionURL.BRANCH_COMMITTEE1.getPath() + localYear);
                            }

                        }
                    }
                    /*6300000037	党支部  三会一课3，党员活动日4，民主评议党员1，组织生活2，党委中心组学习2*/
                    if (Todo.DZB.equals(orgtypeid)) {
                        if (list3 != null && list3.size() > 0) {
                            boolean b31 = true, b32 = true, b33 = true, b34 = true, b35 = true, b36 = true, b37 = true;
                            for (TodoItemDTO t3 : list3) {
                                /*三会一课 党课，支部委员会，党小组会，支部党员大会*/
                                /*党课3 每季度*/
                                if ((Todo.MeetType.PARTY_LECTURE.getNameCode() + localYear + getThisQuarter(localMonthValue)).equals(t3.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.PARTY_LECTURE.getNameCode() + localYear + getThisQuarter(localMonthValue));
                                    b31 = false;
                                }
                                /*支部委员会4   每月*/
                                if ((Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue).equals(t3.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue);
                                    b32 = false;
                                }
                                if (count > 0) {
                                    /*党小组会4   每月*/
                                    if ((Todo.MeetType.PARTY_GROUP_MEETING.getNameCode() + localYear + localMonthValue).equals(t3.getBizid())) {
                                        //已存在待办事项
                                        log.info("已存在待办事项:\n" + Todo.MeetType.PARTY_GROUP_MEETING.getNameCode() + localYear + localMonthValue);
                                        b33 = false;
                                    }
                                }
                                /*支部党员大会3   每季度*/
                                if ((Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode() + localYear + getThisQuarter(localMonthValue)).equals(t3.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode() + localYear + getThisQuarter(localMonthValue));
                                    b34 = false;
                                }
                                /*党员活动日4   每月*/
                                if ((Todo.MeetType.THEME_PARTY_DAY.getNameCode() + localYear + localMonthValue).equals(t3.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.THEME_PARTY_DAY.getNameCode() + localYear + localMonthValue);
                                    b35 = false;
                                }
                                /*民主评议党员1   每年*/
                                if ((Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode() + localYear).equals(t3.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode() + localYear);
                                    b36 = false;
                                }
                                /*组织生活2   每半年*/
                                if ((Todo.MeetType.ORG_LIFE_MEETING.getNameCode() + localYear + halfAYear(localMonthValue)).equals(t3.getBizid())) {
                                    //已存在待办事项
                                    log.info("已存在待办事项:\n" + Todo.MeetType.ORG_LIFE_MEETING.getNameCode() + localYear + halfAYear(localMonthValue));
                                    b37 = false;
                                }
                            }
                            if (b31 && bc2) {
                                /*党课3 每季度*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.PARTY_LECTURE.getNameCode() + localYear + getThisQuarter(localMonthValue),
                                        Todo.MeetType.PARTY_LECTURE.getNameText() + "(" + getQuarter(localMonthValue) + ")",
                                        Todo.FunctionURL.PARTY_LECTURE4.getPath() + localYear);

                            }
                            if (b32 && bc1) {
                                /*支部委员会4   每月*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameText() + "(" + localMonthValue + "月份)",
                                        Todo.FunctionURL.BRANCH_COMMITTEE1.getPath() + localYear);
                            }
                            if (count > 0) {
                                if (b33 && bc3) {
                                    /*党小组会4   每月*/
                                    setTodoItemDTO(map,
                                            Todo.MeetType.PARTY_GROUP_MEETING.getNameCode() + localYear + localMonthValue,
                                            Todo.MeetType.PARTY_GROUP_MEETING.getNameText() + "(" + localMonthValue + "月份)",
                                            Todo.FunctionURL.PARTY_GROUP_MEETING3.getPath() + localYear);
//                                            batchTodoItemDTO.add(todoItemDTO);

                                }
                            }
                            if (b34 && bc4) {
                                /*支部党员大会3   每季度*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode() + localYear + getThisQuarter(localMonthValue),
                                        Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameText() + "(" + getQuarter(localMonthValue) + ")",
                                        Todo.FunctionURL.BRANCH_PARTY_CONGRE2.getPath() + localYear);
                            }
                            if (b35 && bc5) {
                                /*党员活动日4   每月*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.THEME_PARTY_DAY.getNameCode() + localYear + localMonthValue,
                                        Todo.MeetType.THEME_PARTY_DAY.getNameText() + "(" + localMonthValue + "月份)",
                                        Todo.FunctionURL.THEME_PARTY_DAY.getPath());
                            }
                            if (b36 && bc6) {
                                /*民主评议党员1   每年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode() + localYear,
                                        Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameText() + "(" + localYear + "年)",
                                        Todo.FunctionURL.APPR_MEMBERS.getPath() + localYear);
                            }
                            if (b37 && bc7) {
                                /*组织生活2   每半年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.ORG_LIFE_MEETING.getNameCode() + localYear + halfAYear(localMonthValue),
                                        Todo.MeetType.ORG_LIFE_MEETING.getNameText() + "(" + getHalfAYear(localMonthValue) + ")",
                                        Todo.FunctionURL.ORG_LIFE_METING.getPath() + localYear);
                            }
                        } else {
                            if (bc2) {
                                /*党课3 每季度*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.PARTY_LECTURE.getNameCode() + localYear + getThisQuarter(localMonthValue),
                                        Todo.MeetType.PARTY_LECTURE.getNameText() + "(" + getQuarter(localMonthValue) + ")",
                                        Todo.FunctionURL.PARTY_LECTURE4.getPath() + localYear);
                            }
                            if (bc1) {
                                /*支部委员会4   每月*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameCode() + localYear + localMonthValue,
                                        Todo.MeetType.BRANCH_COMMITTEE.getNameText() + "(" + localMonthValue + "月份)",
                                        Todo.FunctionURL.BRANCH_COMMITTEE1.getPath() + localYear);
                            }
                            if (count > 0) {
                                if (bc3) {
                                    /*党小组会4   每月*/
                                    setTodoItemDTO(map,
                                            Todo.MeetType.PARTY_GROUP_MEETING.getNameCode() + localYear + localMonthValue,
                                            Todo.MeetType.PARTY_GROUP_MEETING.getNameText() + "(" + localMonthValue + "月份)",
                                            Todo.FunctionURL.PARTY_GROUP_MEETING3.getPath() + localYear);
                                }
                            }
                            if (bc4) {
                                /*支部党员大会3   每季度*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode() + localYear + getThisQuarter(localMonthValue),
                                        Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameText() + "(" + getQuarter(localMonthValue) + ")",
                                        Todo.FunctionURL.BRANCH_PARTY_CONGRE2.getPath() + localYear);
                            }
                            if (bc5) {
                                /*党员活动日4   每月*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.THEME_PARTY_DAY.getNameCode() + localYear + localMonthValue,
                                        Todo.MeetType.THEME_PARTY_DAY.getNameText() + "(" + localMonthValue + "月份)",
                                        Todo.FunctionURL.THEME_PARTY_DAY.getPath());
                            }
                            if (bc6) {
                                /*民主评议党员1   每年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode() + localYear,
                                        Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameText() + "(" + localYear + "年)",
                                        Todo.FunctionURL.APPR_MEMBERS.getPath());
                            }
                            if (bc7) {
                                /*组织生活2   每半年*/
                                setTodoItemDTO(map,
                                        Todo.MeetType.ORG_LIFE_MEETING.getNameCode() + localYear + halfAYear(localMonthValue),
                                        Todo.MeetType.ORG_LIFE_MEETING.getNameText() + "(" + getHalfAYear(localMonthValue) + ")",
                                        Todo.FunctionURL.ORG_LIFE_METING.getPath());
                            }
                            log.info("会议生成待办事项");
                        }
                    }
                }
            }
            log.info("————————————————————会议生成待办事项———————————结束————————————");
        } catch (Exception e) {
            log.warn("【生成会议代办事项】，异常：\n", e);
        }
    }


    @Scheduled(cron = "${todo.meeting.cron}")
    @Override
    public void execute() {
        try {
            /*查询党组织*/
            List<Map<String, Object>> listUrcOrganization = generalSqlComponent.getDbComponent().listMap(LIST_URC_ORGANIZATION, new Object[]{});
            /*会议周期 1：每年；2：每半年；3：每季度；4：每月*/
            if (listUrcOrganization.size() > 0 && listUrcOrganization != null) {
                for (Map<String, Object> map : listUrcOrganization) {
                    //组织类别
                    String orgtypeid = String.valueOf(map.get("orgtypeid"));
                    //组织orgid
                    Long orgid = (Long) map.get("orgid");
                    //组织编码
                    String orgcode = (String) map.get("orgcode");
                    //当前时间
                    LocalDate localDate = LocalDate.now();
                    int localYear = localDate.getYear();
                    int localMonthValue = localDate.getMonthValue();

                    /*是否有党小组*/
                    Integer count = generalSqlComponent.getDbComponent().getSingleColumn(BOOLEAN_HAS_PARTY_GROUP, Integer.class, new Object[]{orgcode});
                    /* 查询当前组织已提交会议记录*/
                    List<Map<String, Object>> meetingRecord = generalSqlComponent.getDbComponent().listMap(LIST_MEETING_RECORD_BY_STATUS, new Object[]{2, 1, orgid});
                    List<String> meetingListBizids = new ArrayList<>();
                    if (meetingRecord.size() > 0 && meetingRecord != null) {
                        for (Map<String, Object> m : meetingRecord) {
                            //会议类型
                            String mrtype = (String) m.get("mrtype");
                            //年
                            Integer mryear = (Integer) m.get("mryear");
                            //月
                            Integer mrmonth = (Integer) m.get("mrmonth");
                            //是否有标签编码
                            Long mrid = (Long) m.get("mrid");
                            List<Map<String, Object>> meetingTagsCode = generalSqlComponent.getDbComponent().listMap(GET_MEETING_TAG_CODE_BY_MRID,new Object[]{mrid, 0, 1});
                            if (meetingTagsCode.size() > 0) {
                                for (Map<String, Object> mstr: meetingTagsCode) {
                                    //添加标签会议类型
                                    String tagcode = (String) mstr.get("tagcode");
                                    Map<String, String> switchPath = switchPath(tagcode, mryear, mrmonth);
                                    String bizid = switchPath.get("bizid");
                                    meetingListBizids.add(bizid);
                                }
                            }
                            //会议类型
                            Map<String, String> switchPath = switchPath(mrtype, mryear, mrmonth);
                            String bizid = switchPath.get("bizid");
                            meetingListBizids.add(bizid);
                        }
                    }
                    //当前组织需要生成的待办事项类型
                    List<String> types = switchOrganizationType(orgtypeid, localYear, localMonthValue);
                    for (String type:types) {
                        Map<String, String> switchPath = switchPath(type, localYear, localMonthValue);
                        if(switchPath==null){
                            continue;
                        }
                        String bizid = switchPath.get("bizid");
                        String name = switchPath.get("name");
                        String path = switchPath.get("path");
                        Map<String, Object> oldMap = generalSqlComponent.getDbComponent().getMap(LIST_TODO_ITEM_BY_BIZID, new Object[]{switchPath.get("bizid"),orgid});
                        //没有待办数据
                        if(oldMap==null){
                            //待办对应的会议记录没有数据
                            if(!meetingListBizids.contains(bizid)){
                                //是否是党小组会议
                                if((Todo.MeetType.PARTY_GROUP_MEETING.getNameCode()+ localYear + localMonthValue).equals(bizid)){
                                    //没有党小组
                                    if(count < 0){
                                        log.info(String.format("{%s}该组织还没有党小组,不生成党小组待办事项！！！",orgid));
                                        continue;
                                    }
                                }else{
                                    addTodoItemDTO(orgid, bizid, name, path);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.warn("【生成会议代办事项】，异常：\n", e);
        }
    }

    private void setTodoItemDTO(Map<String, Object> map, String type, String name, String path) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        itemDTO.setCategory(Todo.CONSTANT_ONE);
        itemDTO.setName(name);
        itemDTO.setPath(path);
        itemDTO.setUserid(Long.valueOf(Todo.CONSTANT_ONE));
        itemDTO.setUsername("生成");
        itemDTO.setTime(new Date());
        itemDTO.setCompid(Long.valueOf(Todo.CONSTANT_ONE));
        itemDTO.setBizid(type);
        itemDTO.setOrgid((Long) map.get("orgid"));
        itemDTO.setState(Todo.CONSTANT_ZERO);

        TodoItemDTO todoItem = todoItemService.getTodoItem(itemDTO);
        if(todoItem!=null){
            log.info("已经存在会议待办事项");
        }else{
            this.generalSqlComponent.getDbComponent().insert(BATCH_INSERT_TODO_ITEM, itemDTO);
        }
    }

//    public static void main(String[] args) {
//        LocalDate date = LocalDate.now();
//
//        System.out.println("获取年：" + date.getYear());
//        System.out.println("获取月份：" + date.getMonth().getValue());
//        System.out.println("获取月份：" + date.getMonthValue());
//
//        Date date1 = new Date();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date1);
//
//        int year = calendar.get(Calendar.YEAR);
//        System.out.println(String.format("获取年：%s ",year));
//
//    }
    /**
     * 当前季度的时间 列如：123,345
     */
    public String getThisQuarter(int month) {
        String b = null;
        switch (month) {
            case 1:
            case 2:
            case 3:
                b = "123";
                break;
            case 4:
            case 5:
            case 6:
                b = "456";
                break;
            case 7:
            case 8:
            case 9:
                b = "789";
                break;
            case 10:
            case 11:
            case 12:
                b = "101112";
                break;
        }
        return b;
    }

    /**
     * 当前半年
     */
    public static String halfAYear(int month) {
        LocalDate date = LocalDate.now();
        String b;
        switch (date.getMonthValue()) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return b = "123456";
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                return b = "789101112";
        }
        return null;
    }

    /**
     * 获取当前月份季节
     *
     * @param month
     * @return
     */
    public static String getQuarter(int month) {
        switch (month) {
            case 1:
            case 2:
            case 3:
                return "第1季度";
            case 4:
            case 5:
            case 6:
                return "第2季度";
            case 7:
            case 8:
            case 9:
                return "第3季度";
            case 10:
            case 11:
            case 12:
                return "第4季度";
        }
        return null;
    }

    /**
     * 获取当前月份半年
     *
     * @param month
     * @return
     */
    public static String getHalfAYear(int month) {
        switch (month) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return "上半年";
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                return "下半年";
        }

        return null;
    }

    private String switchCode(String type, Integer mryear, Integer mrmonth) {
        switch (type) {
            case "appraisal_of_members"://民主评议党员
                return type + mryear;
            case "branch_committee"://支部委员会
                return type + mryear + mrmonth;
            case "party_group_meeting"://党小组会
                return type+ mryear + mrmonth;
            case "party_lecture"://上党课
                return type + mryear + getThisQuarter(mrmonth);
            case "org_life_meeting"://组织生活会
                return type + mryear + halfAYear(mrmonth);
            case "theme_party_day"://党员活动日
                return type + mryear + mrmonth;
            case "branch_party_congress"://支部党员大会
                return type+ mryear + getThisQuarter(mrmonth);
            case "democratic_life_meeting"://民主生活会
                return type+ mryear;
        }
        return null;
    }

    /**
     * 获取当前组织类型应该生成的待办类型
     * @param orgtypeid
     * @return
     */
    private List<String> switchOrganizationType(String orgtypeid, Integer mryear, Integer mrmonth) {
        List<String> result = new ArrayList<>();
        switch (orgtypeid) {
            case Todo.DW:/*6100000035	党委   民主生活会*/
                //民主生活会
                result.add(Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode());
                break;
            case Todo.DZZ:/*6200000036	党总支部   支部党委会，民主生活会*/
                //支部党委会
                result.add(Todo.MeetType.BRANCH_COMMITTEE.getNameCode());
                //民主生活会
                result.add(Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameCode());
                break;
            case Todo.DZB:/*6300000037	党支部  三会一课，党员活动日，民主评议党员，组织生活，党委中心组学习*/
                //上党课
                result.add(Todo.MeetType.PARTY_LECTURE.getNameCode());
                //支部委员会
                result.add(Todo.MeetType.BRANCH_COMMITTEE.getNameCode());
                //党小组会
                result.add(Todo.MeetType.PARTY_GROUP_MEETING.getNameCode());
                //支部党员大会
                result.add(Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameCode());
                //党员活动日
                result.add(Todo.MeetType.THEME_PARTY_DAY.getNameCode());
                //民主评议党员
                result.add(Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameCode());
                //组织生活会
                result.add(Todo.MeetType.ORG_LIFE_MEETING.getNameCode());
                break;
        }
        return result;
    }

    /**
     * 根据待办类型生成相关待办信息
     * @param type 待办类型
     * @param mryear 年
     * @param mrmonth 月
     * @return
     */
    private Map<String, String> switchPath(String type, Integer mryear, Integer mrmonth) {
        Map<String, String> map = new HashMap<>();
        switch (type) {
            case "appraisal_of_members"://民主评议党员
                map.put("bizid",type + mryear);
                map.put("name",Todo.MeetType.APPRAISAL_OF_MEMBERS.getNameText() + "(" + mryear + "年)");
                map.put("path",Todo.FunctionURL.APPR_MEMBERS.getPath());
                break;
            case "branch_committee"://支部委员会
                map.put("bizid",type+mryear+mrmonth);
                map.put("name", Todo.MeetType.BRANCH_COMMITTEE.getNameText() + "(" + mrmonth + "月份)");
                map.put("path",Todo.FunctionURL.BRANCH_COMMITTEE1.getPath() + mryear);
                break;
            case "party_group_meeting"://党小组会
                map.put("bizid",type+mryear+mrmonth);
                map.put("name",Todo.MeetType.PARTY_GROUP_MEETING.getNameText() + "(" + mrmonth + "月份)");
                map.put("path",Todo.FunctionURL.PARTY_GROUP_MEETING3.getPath() + mryear);
                break;
            case "party_lecture"://上党课
                map.put("bizid",type+mryear+getThisQuarter(mrmonth));
                map.put("name",Todo.MeetType.PARTY_LECTURE.getNameText() + "(" + getQuarter(mrmonth) + ")");
                map.put("path",Todo.FunctionURL.PARTY_LECTURE4.getPath() + mryear);
                break;
            case "org_life_meeting"://组织生活会
                map.put("bizid",type+mryear+halfAYear(mrmonth));
                map.put("name",Todo.MeetType.ORG_LIFE_MEETING.getNameText() + "(" + getHalfAYear(mrmonth) + ")");
                map.put("path",Todo.FunctionURL.ORG_LIFE_METING.getPath());
                break;
            case "theme_party_day"://党员活动日
                map.put("bizid",type+mryear+mrmonth);
                map.put("name",Todo.MeetType.THEME_PARTY_DAY.getNameText() + "(" + mrmonth + "月份)");
                map.put("path",Todo.FunctionURL.THEME_PARTY_DAY.getPath());
                break;
            case "branch_party_congress"://支部党员大会
                map.put("bizid",type+mryear+getThisQuarter(mrmonth));
                map.put("name",Todo.MeetType.BRANCH_PARTY_CONGRESS.getNameText() + "(" + getQuarter(mrmonth) + ")");
                map.put("path",Todo.FunctionURL.BRANCH_PARTY_CONGRE2.getPath() + mryear);
                break;
            case "democratic_life_meeting"://民主生活会
                map.put("bizid",type+mryear);
                map.put("name",Todo.MeetType.DEMOCRATIC_LIFE_MEETING.getNameText() + "(" + mryear + "年)");
                map.put("path",Todo.FunctionURL.DE_LIFE_MEETING.getPath());
                break;
        }
        return map;
    }

    /**
     * 添加待办数据
     * @param orgid
     * @param bizid
     * @param name
     * @param path
     */
    private void addTodoItemDTO(Long orgid, String bizid, String name, String path) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        itemDTO.setCategory(Todo.CONSTANT_ONE);
        itemDTO.setName(name);
        itemDTO.setPath(path);
        itemDTO.setUserid(Long.valueOf(Todo.CONSTANT_ONE));
        itemDTO.setUsername("超管");
        itemDTO.setTime(new Date());
        itemDTO.setCompid(Long.valueOf(Todo.CONSTANT_ONE));
        itemDTO.setBizid(bizid);
        itemDTO.setOrgid(orgid);
        itemDTO.setState(Todo.CONSTANT_ZERO);
        TodoItemDTO todoItem = todoItemService.getTodoItem(itemDTO);
        if(todoItem!=null){
            log.info(String.format("【 %s 】:已经存在会议待办事项",orgid));
        }else{
            log.info(String.format("【 %s 】:生成会议待办事项",orgid));
            this.generalSqlComponent.getDbComponent().insert(BATCH_INSERT_TODO_ITEM, itemDTO);
        }
    }
}