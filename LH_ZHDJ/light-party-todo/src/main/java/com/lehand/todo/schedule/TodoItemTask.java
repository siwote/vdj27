package com.lehand.todo.schedule;

/**
 * <p> 待办任务.</p>
 *
 * @Author zl
 **/
public interface TodoItemTask {
    /**
     * 执行任务.
     */
    void execute();
}
