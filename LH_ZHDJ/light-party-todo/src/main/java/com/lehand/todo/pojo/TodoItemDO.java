package com.lehand.todo.pojo;

import java.util.Date;

/**
 * 待办事项实体类.
 *
 * @author zl
 */
public class TodoItemDO {
    /**
     * 待办ID.
     */
    private Long id;

    /**
     * 事项类别.
     */
    private Integer category;

    /**
     * 事项名称.
     */
    private String name;

    /**
     * 链接地址.
     */
    private String path;

    /**
     * 状态(0:未读,1:已读,2:已办).
     */
    private Integer state;

    /**
     * 用户ID.
     */
    private Long userid;

    /**
     * 用户名称.
     */
    private String username;

    /**
     * 创建时间.
     */
    private Date time;

    /**
     * 阅读时间.
     */
    private Date readtime;

    /**
     * 租户ID.
     */
    private Long compid;

    /**
     * 主体id.
     */
    private Long subjectid;

    /**
     * 所属组织id.
     */
    private Long orgid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getReadtime() {
        return readtime;
    }

    public void setReadtime(Date readtime) {
        this.readtime = readtime;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public Long getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Long subjectid) {
        this.subjectid = subjectid;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }
}
