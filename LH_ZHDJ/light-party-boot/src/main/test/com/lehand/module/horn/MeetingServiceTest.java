package com.lehand.module.horn;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Session;
import com.lehand.module.boot.BootApplication;
import com.lehand.module.horn.meeting.pojo.MeetingRecordPlace;
import com.lehand.module.horn.meeting.service.MeetingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = MeetingServiceTest.class)
public class MeetingServiceTest {

    @Resource
    private MeetingService meetingService;

    /**
     * @Author linqs
     * @Date 2020年03月11日
     * @Descp 测试会议查询
     */
    @Test
    public void getMeetingPlace() {
        MeetingRecordPlace meetingRecordPlace = new MeetingRecordPlace();
        meetingRecordPlace.setOrgcode("034010501311");
        meetingRecordPlace.setPlace("安徽");
        Session session = new Session();
        List<String> places =  meetingService.getMeetingPlace(meetingRecordPlace, session);
        System.out.println(JSON.toJSONString(places));
    }
}
