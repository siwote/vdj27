package com.lehand.lightparty.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lehand.base.constant.StringConstant;
import com.lehand.components.web.config.SwaggerBase;
import com.lehand.module.boot.config.BaseSwaggerConfig;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spring.web.plugins.Docket;
@SuppressWarnings("rawtypes")
@Configuration
public class SwaggerConfig extends BaseSwaggerConfig {

    @Bean
    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        return converter;
    }


    static {
		SwaggerBase.apiInfo("LightParty", "淮矿党建二期", "0.0.1", "127.0.0.1:9680", ApiInfo.DEFAULT_CONTACT, System.getProperty("license"), StringConstant.EMPTY, new ArrayList<VendorExtension>(0));
//		SwaggerBase.apiInfo("LightParty", "智慧党建产品", "0.0.1", "127.0.0.1:9999", ApiInfo.DEFAULT_CONTACT, System.getProperty("license"), StringConstant.EMPTY, new ArrayList<VendorExtension>(0));
	}

	@Bean
	public Docket allDocket() {
		return SwaggerBase.createDocket("all", "com.lehand");
	}

	@Bean
	public Docket hornOrgDocket() {
		return SwaggerBase.createDocket("党建-党组织-党员", "com.lehand.horn.partyorgan.controller");
	}
	
	@Bean
	public Docket hornDevmemberDocket() {
		return SwaggerBase.createDocket("党建-发展党员", "com.lehand.developing.party.controller");
	}

	@Bean
	public Docket DocumentDocket() {
		return SwaggerBase.createDocket("党建-文档管理", "com.lehand.document.lib.controller");
	}

	@Bean
	public Docket hornMeetingDocket() {
		return SwaggerBase.createDocket("党建-会议", "com.lehand.meeting.controller");
	}

	@Bean
	public Docket hornProjectManageDocket() {
		return SwaggerBase.createDocket("党建-项目管理", "com.lehand.pm.controller");
	}
	@Bean
	public Docket appbuiderDocket() {
		return SwaggerBase.createDocket("党建-自定义表单", "com.lehand.lightparty.appbuilder.controller");
	}

	@Bean
	public Docket partyDocket() {
		return SwaggerBase.createDocket("党建-党费", "com.lehand.developing.party.controller");
	}
}
