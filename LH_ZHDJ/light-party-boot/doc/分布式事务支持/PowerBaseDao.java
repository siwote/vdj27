package com.lehand.power.api.dao;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.lehand.dao.BaseDao;
import com.lehand.util.db.MySQLDialect;
import com.lehand.util.db.SpringJDBCUtil;

public class PowerBaseDao<T> extends BaseDao<T>{

	@Resource(name="powerJdbcTemplate") 
	private JdbcTemplate powerJdbcTemplate;
	
	@Resource(name="powerNamedParameterJdbcTemplate") 
	private NamedParameterJdbcTemplate powerNamedParameterJdbcTemplate;
	
	@PostConstruct
	public void init() {
		jdbc = new SpringJDBCUtil(new MySQLDialect(),powerJdbcTemplate,powerNamedParameterJdbcTemplate);
	}
	
}
