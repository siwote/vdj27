package com.lehand.duty;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lehand.controller.BaseController;
import com.lehand.duty.api.business.TkAttachmentBusiness;
import com.lehand.util.db.MySQLDialect;
import com.lehand.util.db.SpringJDBCUtil;



/**
 * 
 * 附件上传  
 * 
 * @author pantao  
 * @date 2018年10月10日 下午3:16:53
 *
 */
@Controller
@RequestMapping("/test")
public class TestController extends BaseController {
	
	@Resource private TkAttachmentBusiness tkAttachmentBusiness;
	
	@Resource(name="dutyJdbcTemplate") 
	private JdbcTemplate dutyJDBC;
	
	@Resource(name="dutyNamedParameterJdbcTemplate") 
	private NamedParameterJdbcTemplate dutyNameJDBC;
	
	@Resource(name="powerJdbcTemplate") 
	private JdbcTemplate powerJDBC;
	
	@Resource(name="powerNamedParameterJdbcTemplate") 
	private NamedParameterJdbcTemplate powerNameJDBC;
	
	private SpringJDBCUtil powerJdbc;
	private SpringJDBCUtil dutyJdbc;
	
	@PostConstruct
	public void init() {
		dutyJdbc = new SpringJDBCUtil(new MySQLDialect(),dutyJDBC,dutyNameJDBC);
		powerJdbc = new SpringJDBCUtil(new MySQLDialect(),powerJDBC,powerNameJDBC);
	}
	
	/**
	 *  测试分布式事务提交
	 * @param id
	 */
	@RequestMapping("/atomikos/commit")
	@Transactional(rollbackOn= {Exception.class})
	public void commit(Long id) {
		String sql = "INSERT INTO zim_test (col1) VALUES (?)";
		dutyJdbc.execute(sql,id);
		powerJdbc.execute(sql,id);
	}
	
	/**
	 * 测试分布式事务回滚
	 * @param id
	 */
	@RequestMapping("/atomikos/rollback")
	@Transactional(rollbackOn= {Exception.class})
	public void rollback(Long id) {
		String sql = "INSERT INTO zim_test (col1) VALUES (?)";
		dutyJdbc.execute(sql,id);
		powerJdbc.execute(sql+"...",id);
	}
}
