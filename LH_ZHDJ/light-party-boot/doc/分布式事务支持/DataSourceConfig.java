package com.lehand.duty.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class DataSourceConfig {

	private static final String DATA_SOURCE_CLASS = "com.alibaba.druid.pool.xa.DruidXADataSource";
	private static final String PROP_PREFIX = "spring.datasource.druid.";
	
	@Autowired private Environment env;
	
	@Bean(name = "dutyDataSource")
    @Primary
    public DataSource dutyDataSource() {
        return initDataSource("dutyDB");
    }

	@Autowired
    @Bean(name = "powerDataSource")
    public AtomikosDataSourceBean powerDataSource() {
        return initDataSource("powerDB");
    }

    @Bean("dutyJdbcTemplate")
    public JdbcTemplate dutyJdbcTemplate(@Qualifier("dutyDataSource") DataSource ds) {
        return new JdbcTemplate(ds);
    }
    
    @Bean("dutyNamedParameterJdbcTemplate")
    public NamedParameterJdbcTemplate dutyNamedParameterJdbcTemplate(@Qualifier("dutyDataSource") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean("powerJdbcTemplate")
    public JdbcTemplate powerJdbcTemplate(@Qualifier("powerDataSource") DataSource ds) {
        return new JdbcTemplate(ds);
    }
    
    @Bean("powerNamedParameterJdbcTemplate")
    public NamedParameterJdbcTemplate powerNamedParameterJdbcTemplate(@Qualifier("powerDataSource") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    private AtomikosDataSourceBean initDataSource(String name) {
		AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
        Properties prop = build(name);
        ds.setXaDataSourceClassName(DATA_SOURCE_CLASS);
        ds.setUniqueResourceName(name);
        ds.setXaProperties(prop);
		return ds;
	}
    
    private Properties build(String prefix) {
    	prefix = PROP_PREFIX+prefix+".";
    	
        Properties prop = new Properties();
        prop.put("url",                    get(prefix,"url",String.class));
        prop.put("username",               get(prefix,"username",String.class));
        prop.put("password",               get(prefix,"password",String.class));
        prop.put("driverClassName",        get(prefix,"driver-class-name",String.class));
        prop.put("initialSize",            get(prefix,"initial-size",Integer.class));
        prop.put("maxActive",              get(prefix,"max-active",Integer.class));
        prop.put("minIdle",                get(prefix,"min-idle",Integer.class));
        prop.put("maxWait",                get(prefix,"max-wait",Integer.class));
        prop.put("poolPreparedStatements", get(prefix,"pool-prepared-statements",Boolean.class));
        prop.put("validationQuery",        get(prefix,"validation-query",String.class));
        prop.put("validationQueryTimeout", get(prefix,"validation-query-timeout",Integer.class));
        prop.put("testOnBorrow",           get(prefix,"test-on-borrow",Boolean.class));
        prop.put("testOnReturn",           get(prefix,"test-on-return",Boolean.class));
        prop.put("testWhileIdle",          get(prefix,"test-while-idle",Boolean.class));
        prop.put("filters",                get(prefix,"filters",String.class));

        prop.put("maxPoolPreparedStatementPerConnectionSize",
       		     get(prefix, "max-pool-prepared-statement-per-connection-size", Integer.class));
        
        prop.put("timeBetweenEvictionRunsMillis",
        		 get(prefix,"time-between-eviction-runs-millis",Integer.class));
        
        prop.put("minEvictableIdleTimeMillis", 
        		 get(prefix,"min-evictable-idle-time-millis", Integer.class));

        return prop;
    }
    
    private <T> T get(String prefix,String field,Class<T> clazz) {
    	return env.getProperty(prefix+field, clazz);
    }
}
