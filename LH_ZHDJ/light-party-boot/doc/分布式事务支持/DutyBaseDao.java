package com.lehand.duty.api.dao;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.lehand.dao.BaseDao;
import com.lehand.util.db.MySQLDialect;
import com.lehand.util.db.SpringJDBCUtil;

public class DutyBaseDao<T> extends BaseDao<T> {

	@Resource(name="dutyJdbcTemplate") 
	private JdbcTemplate dutyJdbcTemplate;
	
	@Resource(name="dutyNamedParameterJdbcTemplate") 
	private NamedParameterJdbcTemplate dutyNamedParameterJdbcTemplate;
	
	@PostConstruct
	public void init() {
		jdbc = new SpringJDBCUtil(new MySQLDialect(),dutyJdbcTemplate,dutyNamedParameterJdbcTemplate);
	}
}
