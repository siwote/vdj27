package com.lehand.evaluation.lib.service.assess;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssess;

/**
 * 
 * @ClassName: ElAssessBusiness
 * @Description: 考核
 * @Author dong
 * @DateTime 2019年4月12日 下午5:04:40
 */
public class ElAssessBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	/**
	 * 
	 * @Title: initAssessClass
	 * @Description: 考核对象初始化
	 * @Author dong
	 * @DateTime 2019年4月11日 下午4:06:04
	 * @param elAssess
	 * @param packageId
	 * @param session
	 */
	public long initAssessClass(ElAssess elAssess, Long packageId, Session session) {
		elAssess.setCompid(session.getCompid());
		elAssess.setPackageid(packageId);
		elAssess.setPackagename(elAssess.getName());
		elAssess.setStatus((byte) 0);
		elAssess.setOrgid(Long.valueOf(session.getCurrentOrg().getOrgcode()));
		elAssess.setOrgname(session.getCurrentOrg().getOrgname());
		elAssess.setCreater(session.getUserid());
		elAssess.setCreatename(session.getUsername());
		elAssess.setModiler(session.getUserid());
		elAssess.setModilname(session.getUsername());
		elAssess.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		elAssess.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		elAssess.setRemark1(0);
		elAssess.setRemark2(0);
		elAssess.setRemark3(StringConstant.EMPTY);
		elAssess.setRemark4(StringConstant.EMPTY);
		elAssess.setRemark5(StringConstant.EMPTY);
		elAssess.setRemark6(StringConstant.EMPTY);
		return generalSqlComponent.insert(ComesqlElCode.addAssessType, elAssess);
	}
	
	/**
	 * 
	 * @Title: updateAssessClass
	 * @Description: 修改考核
	 * @Author dong
	 * @DateTime 2019年4月15日 下午12:41:06
	 * @param elAssess
	 * @param packageId
	 * @param session
	 * @return
	 */
	public void updateAssessClass(ElAssess elAssess,Long packageId, Session session) {
		elAssess.setCompid(session.getCompid());
		elAssess.setPackageid(packageId);
		elAssess.setPackagename(elAssess.getName());
		elAssess.setModiler(session.getUserid());
		elAssess.setModilname(session.getUsername());
		elAssess.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		generalSqlComponent.insert(ComesqlElCode.updateAssessDataType, elAssess);
	}

	/**
	 * 
	 * @Title: copyAssessClass
	 * @Description: 复制考核表
	 * @Author dong
	 * @DateTime 2019年4月15日 下午3:46:59
	 * @param elAssess
	 * @param packageId
	 * @param session
	 * @return
	 */
	public long copyAssessClass(ElAssess elAssess,Long packageId, Session session) {
		elAssess.setCompid(session.getCompid());
		elAssess.setPackageid(packageId);
		elAssess.setPackagename(elAssess.getName());
		elAssess.setStatus((byte) 0);
		elAssess.setOrgid(Long.valueOf(session.getCurrentOrg().getOrgcode()));
		elAssess.setOrgname(session.getCurrentOrg().getOrgname());
		elAssess.setCreater(session.getUserid());
		elAssess.setCreatename(session.getUsername());
		elAssess.setModiler(session.getUserid());
		elAssess.setModilname(session.getUsername());
		elAssess.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		elAssess.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		elAssess.setRemark1(0);
		return generalSqlComponent.insert(ComesqlElCode.addAssessType, elAssess);
	}
}
