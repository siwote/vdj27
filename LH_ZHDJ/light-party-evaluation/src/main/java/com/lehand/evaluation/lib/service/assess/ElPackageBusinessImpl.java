package com.lehand.evaluation.lib.service.assess;


import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.business.ElPackageBusiness;
import com.lehand.evaluation.lib.pojo.*;
import com.lehand.evaluation.lib.service.message.MessageBusinessImp;
import com.lehand.evaluation.lib.util.NumUtil;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.meeting.dto.SmsSendDto;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import org.springframework.util.StringUtils;

/**
 * 
 * @ClassName: ElPackageBusinessImpl
 * @Description: 
 * @Author dong
 * @DateTime 2019年4月11日 上午11:00:12
 */
public  class ElPackageBusinessImpl implements ElPackageBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	@Resource
	ElAssessItemBusiness elAssessItemBusiness;
	@Resource
	ElAssessEvaluateCandidateBusiness elAssessEvaluateCandidateBusiness;
	@Resource
	ElPackagesBusiness elPackagesBusiness;
	@Resource
	ElAssessBusiness elAssessBusiness;
	@Resource
	ElAssessFeedbackScoreRuleBusiness elAssessFeedbackScoreRuleBusiness;
	@Resource
	ElAssessItemReceiveBusiness elAssessItemReceiveBusiness;
	@Resource
	ElAssessObjectBusiness elAssessObjectBusiness;
	@Resource
	ElAssessEvaluateBusiness elAssessEvaluateBusiness;
	@Resource
	ElAssessFeedbackBusiness elAssessFeedbackBusiness;
	@Resource
	MessageBusinessImp messageBusiness;
	
	/**
	 * 创建体系
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<Map<String,Object>> savePackage(String assess, String object, String evaluate, String type, String oldassessid, Session session) {
		//考核id
		long assessid = 0;
		//体系id
		long packageid = 0;
		//体系
		ElAssess elAssess  = JSON.parseObject(assess, ElAssess.class);
		//被考核对象
		List<ElAssessObject> objects = JSON.parseArray(object, ElAssessObject.class);
		//评分者
		List<ElAssessEvaluateCandidate> evaluates = JSON.parseArray(evaluate, ElAssessEvaluateCandidate.class);
		
		Map<String,Object> querymap = new HashMap<String, Object>();
		querymap.put("name", elAssess.getName());
		querymap.put("compid", session.getCompid());
		querymap.put("year", elAssess.getYear());
		List<ElAssess> elAssess2  = generalSqlComponent.query(ComesqlElCode.getAssessNameType, querymap);
		if(elAssess2.size()>0) {
			
			LehandException.throwException("体系名称重复");
		}
		
		if(!NumUtil.checkNum(elAssess.getScore().toString(),1)) {
			LehandException.throwException("请输入正确的数字格式!");
		}
		
		try {
			/**
			 * 初始化体系对象
			 */
			ElPackage elPackage = new ElPackage();
			packageid = elPackagesBusiness.initPackageClass(elAssess,elPackage,session);
			/**
			 * 初始化考核
			 */
			assessid = elAssessBusiness.initAssessClass(elAssess,packageid,session);
			/**
			 * 初始化被考核对象
			 */
			elAssess.setId(assessid);
			for(ElAssessObject ob:objects) {
				elAssessObjectBusiness.initObjectCalss(elAssess,ob,session);
			}
			/**
			 * 初始化评分者候选
			 */
			for(ElAssessEvaluateCandidate ev:evaluates) {
				elAssessEvaluateCandidateBusiness.initEvaluateCandidateClass(elAssess,ev,type,session);
			}
			
		} catch (Exception e) {
			LehandException.throwException("体系新增失败", e);
		}
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("packageid", packageid);
		map.put("assessid", assessid);
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		list.add(map);
		if(!StringUtils.isEmpty(oldassessid)) {
			copyAssess(Long.valueOf(oldassessid),packageid,session);
			/**
			 * 根据体系id获取评分者表
			 */
			List<ElAssessEvaluate> oldevaluates = generalSqlComponent.query(ComesqlElCode.getEvaliateType2, new Object[] {packageid,session.getCompid()});
			for(ElAssessEvaluate li:oldevaluates) {
				ElAssessEvaluateCandidate candidate = generalSqlComponent.query(ComesqlElCode.getCandidateCodeType, new Object[] {packageid,li.getCode(),session.getCompid()});
				if(candidate==null) {
					LehandException.throwException(li.getEvaluatename()+"已参与评分不可删除");
				}
			}
		}
		
		return list;
		
	}
	
	/**
	 * 新增类别
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveItem(ElAssessItem item,Session session) {
//		//转为指标对象
//		ElAssessItem assessitem  = JSON.parseObject(item, ElAssessItem.class);
//		//转为评分对象数组
//		List<ElAssessEvaluate> assessevaluate = JSON.parseArray(evaluate, ElAssessEvaluate.class);
//		//**是否需要评分人
//		if(assessitem.getType()==1&&assessevaluate.size()==0) {
//			LehandException.throwException("评分人不能为空");
//		}
//		if(assessitem.getType()==0&&assessevaluate.size()>0) {
//			LehandException.throwException("不需要维护评分人");
//		}
		//根据名称查询指标或类别
		Map<String,Object>  map = new HashMap<String,Object>(4);
		map.put("name", item.getName());
		map.put("packageid", item.getPackageid());
		map.put("compid", session.getCompid());
		ElAssessItem item2 = generalSqlComponent.query(ComesqlElCode.getItemNameType, map);
		if(item2!=null&&item.getPid()==0) {
			LehandException.throwException("类别名重复");
		}
		if(item2!=null&&item.getPid()!=0) {
			LehandException.throwException("指标名重复");
		}
		
		if(!NumUtil.checkNum(item.getScore().toString(),1)) {
			LehandException.throwException("请输入正确的数字格式!");
		}
		//获取体系
		ElPackage pack = generalSqlComponent.query(ComesqlElCode.getPackageType, new Object[] {item.getPackageid(),session.getCompid()});
		//获取父类别或指标
		ElAssessItem Piditem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {item.getPid(),session.getCompid()});
		//获取同一pid子类总分
		Double sumScore = generalSqlComponent.query(ComesqlElCode.getSumScoreItemType, new Object[] {item.getPid(),session.getCompid()});
		
		//分数控制
		if(NumUtil.tonumber2(item.getScore())>NumUtil.tonumber2(pack.getScore())&&item.getPid() == 0) {
			LehandException.throwException("类别分值大于体系分值");
		}
		if(Piditem!=null) {
			if(Piditem.getIndextype()==0&&NumUtil.tonumber2(item.getScore())>NumUtil.tonumber2(Piditem.getScore())) {
				LehandException.throwException("分值大于上级分值");
			}
			if(Piditem.getSorttype()==1&&(NumUtil.tonumber2(item.getScore()+sumScore))>NumUtil.tonumber2(Piditem.getScore())) {
				LehandException.throwException("本级别总分值大于上级分值");
			}
		}
		
		
		try {
			/**
			 * 初始化 类别
			 */
			elAssessItemBusiness.initItemCalss(item,session);
		} catch (Exception e) {
			LehandException.throwException("类别保存失败", e);
		}
		
	}
	
	/**
	 * 修改类别
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void updateItem(ElAssessItem item,Session session) {
		//根据名称查询指标或类别
		Map<String,Object>  map = new HashMap<String,Object>(4);
		map.put("name", item.getName());
		map.put("packageid", item.getPackageid());
		map.put("compid", session.getCompid());
		map.put("id", item.getId());
		ElAssessItem item2 = generalSqlComponent.query(ComesqlElCode.getItemNameType, map);
		if(item2!=null&&item.getPid()==0) {
			LehandException.throwException("类别名重复");
		}
		if(item2!=null&&item.getPid()!=0) {
			LehandException.throwException("指标名重复");
		}
		
		if(!NumUtil.checkNum(item.getScore().toString(),1)) {
			LehandException.throwException("请输入正确的数字格式!");
		}
		
		//获取体系
		ElPackage pack = generalSqlComponent.query(ComesqlElCode.getPackageType, new Object[] {item.getPackageid(),session.getCompid()});
		//获取父类别或指标
		ElAssessItem Piditem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {item.getPid(),session.getCompid()});
		//获取同一pid子类总分
		Double sumScore = generalSqlComponent.query(ComesqlElCode.getSumScoreItemNotSelfType, new Object[] {item.getPid(),item.getId(),session.getCompid()});
		//获取子类总分
		Double sunScore = generalSqlComponent.query(ComesqlElCode.getSumScoreItemNotSelfType, new Object[] {item.getId(),item.getId(),session.getCompid()});
		
		//分数控制
		if(NumUtil.tonumber2(item.getScore())>NumUtil.tonumber2(pack.getScore())&&item.getPid() == 0) {
			LehandException.throwException("类别分值大于体系分值");
		}
		if(Piditem!=null) {
			if(Piditem.getIndextype()==0&&NumUtil.tonumber2(item.getScore())>NumUtil.tonumber2(Piditem.getScore())) {
				LehandException.throwException("分值大于上级分值");
			}
			if(Piditem.getSorttype()==1&&(NumUtil.tonumber2(item.getScore()+sumScore))>NumUtil.tonumber2(Piditem.getScore())) {
				LehandException.throwException("本级别总分值大于上级分值");
			}
		}
		if(NumUtil.tonumber2(item.getScore())<NumUtil.tonumber2(sunScore)) {
			
			if(item.getPid() ==0) {
				LehandException.throwException("分值小于子指标总分");
			}else {
				LehandException.throwException("分值小于评分标准总分");
			}
			
		}
		
		try {
			/**
			 * 修改 类别
			 */
			elAssessItemBusiness.updateItemCalss(item,session);
		} catch (Exception e) {
			LehandException.throwException("类别保存失败", e);
		}
		
	}

	/**
	 * 新增指标
	 * @return 
	 */
	@Override
//	@Transactional(rollbackFor=Exception.class)
//	public void saveItemTarget(String item,String names,Session session,String evaluate,String evaluatetype,Short rule) {
//		//转为指标对象
//		ElAssessItem assessitem  = JSON.parseObject(item, ElAssessItem.class);
////		//转为评分对象数组
//		List<String> listName = JSON.parseArray(names,String.class);
//		//新增指标
//		for(String name:listName) {
//			elAssessItemBusiness.initItemCalss2(assessitem,name,session);
//		}
//		//转为评分对象数组
//		List<ElAssessEvaluate> assessevaluate = JSON.parseArray(evaluate, ElAssessEvaluate.class);
//		
//		//插入评分单位或人
//		for(ElAssessEvaluate ev:assessevaluate) {
//			elAssessEvaluateBusiness.initEvaluateClass(ev,assessitem,evaluatetype,session);
//		}
//		
//	}
	@Transactional(rollbackFor=Exception.class)
	public Message saveItemTarget(String item,Session session,String evaluate,String evaluatetype,Short rule) {
		Message message = new Message();
		Map<String,Object> listmap = new HashMap<String,Object>();
		//转为指标对象
		List<Map<String,Object>> assessitem  = JSON.parseObject(item,new TypeReference<List<Map<String,Object>>>(){});
//		//转为评分对象数组
//		List<ElAssessEvaluate> assessevaluate = JSON.parseArray(evaluate, ElAssessEvaluate.class);
		
		Map<String,Object>  namemap= new HashMap<String,Object>();
		boolean repeatIs = false;
		for(Map<String,Object> i :assessitem) {
			//根据名称查询指标或类别
			Map<String,Object>  map = new HashMap<String,Object>(4);
			map.put("name", i.get("name"));
			map.put("packageid", i.get("packageid"));
			map.put("compid", session.getCompid());
			ElAssessItem item2 = generalSqlComponent.query(ComesqlElCode.getItemNameType, map);
			i.put("namerepeat", false);
			i.put("lengthbeyond", false);
			if(i.get("evaluatename")==null) {
				repeatIs = true;
				message.setMessage("评分单位为空");
				i.put("message", "评分单位为空");
			}
			if(item2!=null) {
				i.put("namerepeat", true);
				repeatIs = true;
				message.setMessage("评分标准重复");
				i.put("message", "评分标准重复");
//				LehandException.throwException("评分标准重复");
			}
			if(namemap.get(i.get("name"))!=null) {
				i.put("namerepeat", true);
				repeatIs = true;
				message.setMessage("评分标准重复");
				i.put("message", "评分标准重复");
//				LehandException.throwException("评分标准重复");
			}
			namemap.put(i.get("name").toString(), i.get("name"));
			if(i.get("name").toString().trim().length()>300) {
				i.put("lengthbeyond", true);
				repeatIs = true;
				message.setMessage("评分标准超过300字符");
				i.put("message", "评分标准超过300字符");
//				LehandException.throwException("评分标准超过300字");
			}
			if(i.get("score")==null) {
				repeatIs = true;
				message.setMessage("分数不能为空");
				i.put("message", "分数不能为空");
			}else {
				if(!NumUtil.checkNum(i.get("score").toString(),1)) {
					LehandException.throwException("请输入正确的数字格式!");
				}
			}
		}
		if(repeatIs) {
			listmap.put("success", false);
			listmap.put("data", assessitem);
			message.success().setData(listmap);
			return message;
		}
		//新增指标
		Double addsumScore = 0d;
		for(Map<String,Object> i :assessitem) {
			//根据名称查询指标或类别
			Map<String,Object>  map = new HashMap<String,Object>(4);
			map.put("name", i.get("name"));
			map.put("packageid", i.get("packageid"));
			map.put("compid", session.getCompid());
//			ElAssessItem item2 = generalSqlComponent.query(ComesqlElCode.getItemNameType, map);
//			if(item2!=null) {
//				LehandException.throwException("评分标准重复");
//			}
//			if(namemap.get(i.get("name"))!=null) {
//				LehandException.throwException("评分标准重复");
//			}
//			namemap.put(i.get("name").toString(), i.get("name"));
//			if(i.get("name").toString().length()>300) {
//				LehandException.throwException("评分标准超过300字");
//			}
			if(!NumUtil.checkNum(i.get("score").toString(),1)) {
				LehandException.throwException("请输入正确的数字格式!");
			}
			
			//获取父类别
			ElAssessItem Piditem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {i.get("pid"),session.getCompid()});
			//获取同一pid子类总分
//			System.out.println("pid&&&&&&&&&&&&&&&&&&&"+i.get("pid"));
			Double sumScore = generalSqlComponent.query(ComesqlElCode.getSumScoreItemType, new Object[] {i.get("pid"),session.getCompid()});
			
			//分数控制
			if(Piditem!=null) {
				if(Piditem.getIndextype()==0&&NumUtil.tonumber2(Double.valueOf(i.get("score").toString()))>NumUtil.tonumber2(Piditem.getScore())) {
					LehandException.throwException("分值大于上级分值");
				}
				addsumScore +=Double.valueOf(i.get("score").toString());
				if(Piditem.getSorttype()==1&&(NumUtil.tonumber2(Double.valueOf(i.get("score").toString())+NumUtil.tonumber2(sumScore)))>NumUtil.tonumber2(Piditem.getScore())) {
					LehandException.throwException("本级别总分值大于上级分值");
				}
			}
			
			long itemid = elAssessItemBusiness.initItemCalss2 (i,session);
			i.put("id", itemid);
			//插入评分单位或人
			if(i.get("evaluatename")==null) {
				LehandException.throwException("评分单位为空");
			}
			ElAssessEvaluate ev = new ElAssessEvaluate();
			elAssessEvaluateBusiness.initEvaluateClass2(ev,i,evaluatetype,session);
			
			// 插入指标时插入评分规则
			if(Long.valueOf(i.get("type").toString())==1) {
				ElAssessFeedbackScoreRule scorerule = new ElAssessFeedbackScoreRule();
				elAssessFeedbackScoreRuleBusiness.initScoreRuleCalss2(scorerule,i,rule,session);
			}
//			for(ElAssessEvaluate ev:assessevaluate) {
//				elAssessEvaluateBusiness.initEvaluateClass2(ev,i,evaluatetype,session);
//			}
		}
		listmap.put("success", false);
		listmap.put("data", assessitem);
		message.success().setData(listmap);
		return message;
		
		
	}
	
	/**
	 * 修改指标
	 * @return 
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Message updateItemTarget(String data /**ElAssessItem item,String evaluate,String evaluatetype,Short rule*/,Session session) {
		Message message = new Message();
		Map<String,Object> listmap = new HashMap<String,Object>();
		List<Map<String,Object>> dataList  = JSON.parseObject(data,new TypeReference<List<Map<String,Object>>>(){});
		
		
		Map<String,Object>  namemap= new HashMap<String,Object>();
		boolean repeatIs = false;
		Double updateScore = 0d;
		ArrayList<String> notid = new ArrayList<>();
		ElAssessItem item3 = JSON.parseObject(JSON.toJSONString(dataList.get(0)), ElAssessItem.class);
		//获取父类别
		ElAssessItem Piditem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {item3.getPid(),session.getCompid()});
		
		for(Map<String,Object> li :dataList) {
			Map<String,Object> i = li;
			i.put("namerepeat", false);
			i.put("lengthbeyond", false);
			
			List<ElAssessEvaluate> assessevaluate = JSON.parseArray(li.get("evaluate").toString(), ElAssessEvaluate.class);
			if(assessevaluate.size()<=0) {
				repeatIs = true;
				message.setMessage("评分单位为空");
				i.put("message", "评分单位为空");
			}
			
			//根据名称查询指标或类别
			Map<String,Object>  map = new HashMap<String,Object>(4);
			map.put("name", i.get("name"));
			map.put("packageid", i.get("packageid"));
			map.put("compid", session.getCompid());
			map.put("id", i.get("id"));
			//根据名称查询指标或类别
			ElAssessItem item2 = generalSqlComponent.query(ComesqlElCode.getItemNameType, map);
			i.put("message", "");
			if(item2!=null) {
				i.put("namerepeat", true);
				repeatIs = true;
				message.setMessage("评分标准重复");
				i.put("message", "评分标准重复");
//				LehandException.throwException("评分标准重复");
			}
			if(namemap.get(i.get("name"))!=null) {
				i.put("namerepeat", true);
				repeatIs = true;
				message.setMessage("评分标准重复");
				i.put("message", "评分标准重复");
//				LehandException.throwException("评分标准重复");
			}
			namemap.put(i.get("name").toString(), i.get("name"));
			if(i.get("name").toString().trim().length()>300) {
				i.put("lengthbeyond", true);
				repeatIs = true;
				message.setMessage("评分标准超过300字符");
				i.put("message", "评分标准超过300字符");
//				LehandException.throwException("评分标准超过300字");
			}
			
			//分数控制
			if(li.get("score")==null) {
				repeatIs = true;
				message.setMessage("分数不能为空");
				i.put("message", "分数不能为空");
			}else {
				updateScore +=Double.valueOf(li.get("score").toString());
				if(!NumUtil.checkNum(li.get("score").toString(),1)) {
					LehandException.throwException("请输入正确的数字格式!");
				}
				if(Piditem!=null) {
					if(Piditem.getIndextype()==0&&NumUtil.tonumber2(item3.getScore())>NumUtil.tonumber2(Piditem.getScore())) {
						LehandException.throwException("分值大于上级分值");
					}
//					if(Piditem.getSorttype()==1&&(NumUtil.tonumber2(item.getScore())+NumUtil.tonumber2(sumScore))>Piditem.getScore()) {
//						LehandException.throwException("本级别总分值大于上级分值");
//					}
				}
			}
			
			notid.add(li.get("id").toString());
			i.put("message", message.getMessage());
		}
		Map<String,Object> mapSumScore = new HashMap<String, Object>();
		mapSumScore.put("pid", item3.getPid());
		mapSumScore.put("notid", notid);
		mapSumScore.put("compid", session.getCompid());
		Double sumScore = generalSqlComponent.query(ComesqlElCode.getitemScoreNInIdType, mapSumScore);
		if(NumUtil.tonumber2(updateScore+sumScore)>NumUtil.tonumber2(Piditem.getScore())) {
			LehandException.throwException("本级别总分值大于上级分值");
		}
		if(repeatIs) {
			listmap.put("success", false);
			listmap.put("data", dataList);
			message.success().setData(listmap);
			return message;
		}
		
		for(Map<String,Object> li:dataList) {
			
			ElAssessItem item = JSON.parseObject(JSON.toJSONString(li), ElAssessItem.class);
			
			//转为评分对象数组
			List<ElAssessEvaluate> assessevaluate = JSON.parseArray(li.get("evaluate").toString(), ElAssessEvaluate.class);
			
//			//获取同一pid子类总分不包括自己
//			Double sumScore = generalSqlComponent.query(ComesqlElCode.getSumScoreItemNotSelfType, new Object[] {item.getPid(),item.getId(),session.getCompid()});
			
			
//			//分数控制
//			if(Piditem!=null) {
//				if(Piditem.getIndextype()==0&&NumUtil.tonumber2(item.getScore())>NumUtil.tonumber2(Piditem.getScore())) {
//					LehandException.throwException("分值大于上级分值");
//				}
//				if(Piditem.getSorttype()==1&&(NumUtil.tonumber2(item.getScore())+NumUtil.tonumber2(sumScore))>Piditem.getScore()) {
//					LehandException.throwException("本级别总分值大于上级分值");
//				}
//			}
			//根据名称查询指标或类别
			Map<String,Object>  map = new HashMap<String,Object>(4);
			map.put("name", item.getName());
			map.put("packageid", item.getPackageid());
			map.put("compid", session.getCompid());
			map.put("id", item.getId());
//			ElAssessItem item2 = generalSqlComponent.query(ComesqlElCode.getItemNameType, map);
//			if(item2!=null) {
//				LehandException.throwException("评分标准重复");
//			}
//			if(item.getName().toString().length()>300) {
//				LehandException.throwException("评分标准超过300字");
//			}
			try {
				item.setCompid(session.getCompid());
				item.setModiler(session.getUserid());
				item.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
				//修改评分指标
				generalSqlComponent.update(ComesqlElCode.updateItemTargetType, item);
				
				//删除评分单位或人
				generalSqlComponent.delete(ComesqlElCode.deleteEvaluateType, new Object[] {item.getId(),session.getCompid()});
				//插入评分单位或人
				for(ElAssessEvaluate ev:assessevaluate) {
					if(ev.getCode()==null) {
						LehandException.throwException("评分单位为空");
					}
					elAssessEvaluateBusiness.initEvaluateClass(ev,item,li.get("evaluatetype").toString(),session);
				}

				//删除评分规则
				generalSqlComponent.delete(ComesqlElCode.deleteScoreRuleType, new Object[] {item.getId(),session.getCompid()});
				// 插入指标时插入评分规则
				if(item.getType()==1) {
					ElAssessFeedbackScoreRule scorerule = new ElAssessFeedbackScoreRule();
					elAssessFeedbackScoreRuleBusiness.initScoreRuleCalss(scorerule,item,Short.valueOf(li.get("rule").toString()),session);
				}
			} catch (Exception e) {
				LehandException.throwException(e.getMessage(), e);
			}
		}
		listmap.put("success", true);
		listmap.put("data", dataList);
		message.success().setData(listmap);
		return message;
	}
	/**
	 * 发布考核
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void releaseAssess(long assessid,Session session) {
		
		//获取考核
		ElAssess assess = generalSqlComponent.query(ComesqlElCode.getAssessType, new Object[] {assessid,session.getCompid()});
		//获取体系
		ElPackage packag = generalSqlComponent.query(ComesqlElCode.getPackageType, new Object[] {assess.getPackageid(),session.getCompid()});
		
		//获取指标数据
		List<ElAssessItem> itemList = generalSqlComponent.query(ComesqlElCode.getItemTypeListType, new Object[] {assess.getPackageid(),1,session.getCompid()});
		if(itemList.size()<1) {
			LehandException.throwException("请维护评分标准");
		}
		
		ElAssess EndDateAssess = generalSqlComponent.query(ComesqlElCode.getAssessEndDateType111, new Object[] {assessid,session.getCompid()});
		if(EndDateAssess==null) {
			LehandException.throwException("考核时间已结束");
		}
		
		
		
		
		//获取所有类别
		List<ElAssessItem> itemList1 = generalSqlComponent.query(ComesqlElCode.getAssessItemPidType, new Object[] {assess.getPackageid(),0,session.getCompid()});
		for(ElAssessItem li1:itemList1) {
			//根据pid获取指标
			List<ElAssessItem> itemList2 = generalSqlComponent.query(ComesqlElCode.getAssessItemPidType, new Object[] {assess.getPackageid(),li1.getId(),session.getCompid()});
//			if(itemList2.size()<=0) {
//				LehandException.throwException(li1.getName()+"下的指标累计分值与该指标类别的分值不一致，请确认1");
//			}
//			
			//根据pid获取指标总分
			double sumScore2 = generalSqlComponent.query(ComesqlElCode.getSumScoreItemType, new Object[] {li1.getId(),session.getCompid()});
			if(NumUtil.tonumber2(sumScore2)!=NumUtil.tonumber2(li1.getScore())) {
				LehandException.throwException(li1.getName()+"下的指标累计分值与该类别的分值不一致，请确认");
			}
			for(ElAssessItem li2:itemList2) {
//				//根据pid获取评分标准
//				List<ElAssessItem> itemList3 = generalSqlComponent.query(ComesqlElCode.getAssessItemPidType, new Object[] {assess.getPackageid(),li2.getId(),session.getCompid()});
//				System.out.println(itemList3.size());
//				if(itemList3.size()<=0) {
//					LehandException.throwException(li1.getName()+"下的评分标准累计分值与该指标类别的分值不一致，请确认1");
//				}
				//根据pid获取总分
				double sumScore3 = generalSqlComponent.query(ComesqlElCode.getSumScoreItemType, new Object[] {li2.getId(),session.getCompid()});
				if(NumUtil.tonumber2(sumScore3)!=NumUtil.tonumber2(li2.getScore())) {
					LehandException.throwException(li2.getName()+"下的评分标准累计分值与该指标的分值不一致，请确认");
				}
			}
			
		}
		
		
		
		
		//获取类别总分
		double sumScore = generalSqlComponent.query(ComesqlElCode.getSumScoreItemType2, new Object[] {assess.getPackageid(),session.getCompid()});
		if(NumUtil.tonumber2(assess.getScore())>NumUtil.tonumber2(sumScore)) {
			LehandException.throwException("体系总分大于类别总分");
		}
		
		//获取被考核对象
		String msg = "【智慧党建】: 您好,您的党建考核[" + assess.getName() + "]待自评,请及时办理!";
		List<ElAssessObject> objectList = generalSqlComponent.query(ComesqlElCode.getAssessObjectType, new Object[] {assessid,session.getCompid()});
		if(objectList.size() > 0) {
			for (ElAssessObject elAssessObject : objectList) {
				String code = "0" + elAssessObject.getCode().toString();
				if (StringUtils.isEmpty(code)){
					continue;
				}
				Map<String,String> partyOrgMap = generalSqlComponent.query(ComesqlElCode.geTTDzzInfoBycode, new Object[]{code});
				if(partyOrgMap==null){
					continue;
				}
				String phone = partyOrgMap.get("lxrphone");
				if (StringUtils.isEmpty(phone)) {
					continue;
				}
				String organid = partyOrgMap.get("organid");
				SmsSendDto smsSendDto = new SmsSendDto();
				smsSendDto.setCompid(session.getCompid());
				smsSendDto.setUsermark(organid);
				smsSendDto.setPhone(phone);
				smsSendDto.setContent(msg);
				smsSendDto.setBusinessid(7L);
				smsSendDto.setStatus(0);
				smsSendDto.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format(new Date()));
				smsSendDto.setDelflag(0);
				//插入短信发送内容
				generalSqlComponent.insert(SqlCode.addSmsSend, smsSendDto);
			}
		}
		
		//修改考核状态
		generalSqlComponent.update(ComesqlElCode.updateAsseaType, new Object[] {1,assessid,session.getCompid()});
		//修改体系状态
		generalSqlComponent.update(ComesqlElCode.updatePackageType, new Object[] {1,assessid,session.getCompid()});
		
		ElAssessItemReceive receive;
		ElAssessFeedback feedback;
		for(ElAssessObject obj:objectList) {
			//创建反馈接收对象
			receive  = new ElAssessItemReceive();
			//初始化反馈接收对象
			elAssessItemReceiveBusiness.initReceiveClass(receive,assess, packag,obj.getCode(), session);
			for(ElAssessItem item:itemList) {
				//创建反馈表
				feedback = new ElAssessFeedback();
				elAssessFeedbackBusiness.initFeedBackClass(feedback,assess, packag,item,obj, session);
			}
			//给被考核对象发送消息
//			AccountInfoDto info = messageBusiness.getAccountByOrgid(obj.getCode(), obj.getCompid());
//			try {
//				String remindTime = assess.getStartdate() + Constant.TIMESTR;
//				if(DateEnum.parseStr(DateEnum.YYYYMMDDHHMMDD.format(), DateEnum.YYYYMMDDHHMMDD.toString()).after(DateEnum.parseStr(remindTime,DateEnum.YYYYMMDDHHMMDD.toString()))){
//					remindTime = Constant.EMPTY;
//				}
//				messageBusiness.insert(session, Constant.MODULE, Constant.MID, MessageEnum.PACKAGE_RELEASE.getCode(),
//						MessageEnum.PACKAGE_RELEASE.getMsg(), "您有一个考核体系待签收", info.getUserid(), info.getUsername(),remindTime);
//			} catch (Exception e) {
//				LehandException.throwException("向考核单位发送消息失败",e);
//			}


			addTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SELF, packag.getName() + "（" + assess.getYear()  + "年）", String.valueOf(obj.getCode()), Todo.FunctionURL.CHECK_SELF.getPath(), obj.getCode(), assessid, session);
		}

	}
	
	 
	/**
	 * 查询考核列表
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Pager queryAssess(Pager pager,String likeStr,Session session){
		Map<String,Object> map = new HashMap<String,Object>(3);
		map.put("orgid", session.getCurrentOrg().getOrgcode());
		map.put("likestr", likeStr);
		map.put("compid", session.getCompid());
		generalSqlComponent.pageQuery(ComesqlElCode.getAssessListType, map,pager);
		return pager;
	}
	

	
	/**
	 * 根据考核id查询
	 * @Title: getAssess
	 * @Description: 
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:03:04
	 * @return
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public ElAssess getAssess(long assessid,Session session) {
		ElAssess assess = generalSqlComponent.getDbComponent().getBean(
		        "select a.*,b.selfassessaudit from el_assess a left join " +
                        "el_package b on a.compid=b.compid and a.packageid=b.id" +
                        " where a.id = ? and a.compid = ?"
                ,ElAssess.class,new Object[]{assessid,session.getCompid()});
                //generalSqlComponent.query(ComesqlElCode.getAssessType, new Object[] {assessid,session.getCompid()});
		return assess;
		
	};
	
	/**
	 * 
	 * @Title: getAssessObject
	 * @Description: 根据体系id获取考核对象
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:12:18
	 * @param
	 * @return
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<ElAssessObject> getAssessObjectBy(long assessid,Session session) {
		List<ElAssessObject> obj = generalSqlComponent.query(ComesqlElCode.getAssessObjectType3, new Object[] {assessid,session.getCompid()});
		// 这里返回之前需要判断这个党组织是否已经全部自评完(查询el_assess_feedback表看看是否存在状态为0的，如果存在就没有自评完成反之就是自评完成)
		if(obj!=null && obj.size()>0) {
			for (ElAssessObject elAssessObject : obj) {
				//直接查询已提交中是否有数据如果有就是已评
				Map<String,Object> map =generalSqlComponent.query(ComesqlElCode.getAssessFeedback, new Object[] {2,assessid,session.getCompid(),elAssessObject.getCode()});
				if(map==null) {//1 已评 2 未评
					elAssessObject.setRemark1(2);
				}else {
					elAssessObject.setRemark1(1);
				}
			}
		}
		return obj;
	};
	
	/**
	 * 
	 * @Title: getAssessEvaluate
	 * @Description: 根据体系id获取评分者候选数据
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:26:08
	 * @param
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<ElAssessEvaluateCandidate> getAssessEvaluateCandidate(long packgaeid, Session session) {
		List<ElAssessEvaluateCandidate> candidate = generalSqlComponent.query(ComesqlElCode.getCandidateType,
				new Object[] { packgaeid, session.getCompid() });
		if (candidate != null && candidate.size() > 0) {
			for (ElAssessEvaluateCandidate elAssessObject : candidate) {
				//直接查询已提交中是否有数据如果有就是已评
				Map<String, Object> map = generalSqlComponent.query(ComesqlElCode.getAssessEvaluateCandidate2,
						new Object[] {1,session.getCompid(), elAssessObject.getCode(), elAssessObject.getPackageid() });
				if (map == null ) {//1 已评 2 未评
					elAssessObject.setRemark1(2);
				} else {
					elAssessObject.setRemark1(1);
				}
			}
		}
		return candidate;
	}
	
	/**
	 * 修改体系
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<Map<String,Object>> updatePackage(String assess,String object,String evaluate,String type,long packageid,Session session) {
		//体系
		ElAssess elAssess  = JSON.parseObject(assess, ElAssess.class);
		//被考核对象
		List<ElAssessObject> objects = JSON.parseArray(object, ElAssessObject.class);
		//评分者
		List<ElAssessEvaluateCandidate> evaluates = JSON.parseArray(evaluate, ElAssessEvaluateCandidate.class);
//		try {
		Map<String,Object> querymap = new HashMap<String, Object>();
		querymap.put("name", elAssess.getName());
		querymap.put("compid", session.getCompid());
		querymap.put("year", elAssess.getYear());
		querymap.put("id", elAssess.getId());
		List<ElAssess> elAssess2  = generalSqlComponent.query(ComesqlElCode.getAssessNameType, querymap);
		if(elAssess2.size()>0) {
			LehandException.throwException("体系名称重复");
		}
		
			List<ElAssessItem> item = generalSqlComponent.query(ComesqlElCode.getAssessItem, new Object[] {packageid,session.getCompid()});
		
			if(item.size()>0) {
				/**
				 * 获取一级最大分值
				 */
				double maxScore = generalSqlComponent.query(ComesqlElCode.getItemScoreMaxType, new Object[] {packageid,0,session.getCompid()});
				if(NumUtil.tonumber2(elAssess.getScore())<NumUtil.tonumber2(maxScore)) {
					LehandException.throwException("分数小于类别分");
				}
			}
			
			
			/**
			 * 修改体系对象
			 */
			elPackagesBusiness.updatePackageClass(elAssess,packageid,session);
			
			/**
			 * 修改考核
			 */
			elAssessBusiness.updateAssessClass(elAssess,packageid,session);
			 
			/**
			 * 删除被考核对象
			 */
			generalSqlComponent.delete(ComesqlElCode.deleteAssessObjType, new Object[] {elAssess.getId(),session.getCompid()});
			/**
			 * 重新创建被考核对象
			 */
			for(ElAssessObject ob:objects) {
				elAssessObjectBusiness.initObjectCalss(elAssess,ob,session);
			}
			
			/**
			 * 删除评分者候选
			 */
			generalSqlComponent.delete(ComesqlElCode.deleteCandidate, new Object[] {packageid,session.getCompid()});
			
			/**
			 * 初始化评分者候选
			 */
			for(ElAssessEvaluateCandidate ev:evaluates) {
				elAssessEvaluateCandidateBusiness.initEvaluateCandidateClass(elAssess,ev,type,session);
			}
			
			/**
			 * 根据体系id获取评分者表
			 */
			List<ElAssessEvaluate> oldevaluates = generalSqlComponent.query(ComesqlElCode.getEvaliateType2, new Object[] {packageid,session.getCompid()});
			for(ElAssessEvaluate li:oldevaluates) {
				ElAssessEvaluateCandidate candidate = generalSqlComponent.query(ComesqlElCode.getCandidateCodeType, new Object[] {packageid,li.getCode(),session.getCompid()});
				if(candidate==null) {
					LehandException.throwException(li.getEvaluatename()+"已参与评分不可删除");
				}
			}
			
			
			
//		} catch (Exception e) {
//			LehandException.throwException("体系修改失败", e);
//		}
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("packageid", packageid);
		map.put("assessid", elAssess.getId());
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		list.add(map);
		return list;
		
	}
	
	/**
	 * 删除考核体系
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public long deletePackage(long packageid,Session session) {
		
		
		/**
		 * 删除体系
		 */
		generalSqlComponent.delete(ComesqlElCode.deletepackageType, new Object[] {packageid,session.getCompid()});
		/**
		 * 查询考核
		 */
		ElAssess assess = generalSqlComponent.query(ComesqlElCode.getAssesspackageidType, new Object[] {packageid,session.getCompid()});
		/**
		 * 删除考核
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteAssessType, new Object[] {assess.getId(),session.getCompid()});
		/**
		 * 删除评分者候选表
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteCandidate, new Object[] {packageid,session.getCompid()});
		/**
		 * 删除被考核对象表
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteAssessObjType, new Object[] {assess.getId(),session.getCompid()});
		/**
		 * 删除评分者
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteEvaluatePackageidType, new Object[] {packageid,session.getCompid()});
		
		/**
		 * 删除评分规则
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteRuleType, new Object[] {packageid,session.getCompid()});

        /**
         * 删除整改信息
         */
		removePackageRectification(packageid,session);
		
		return packageid;
	}

	/**
	 * 
	 * @Title: copyAssess
	 * @Description: 复制考核体系
	 * @Author dong
	 * @DateTime 2019年4月15日 下午3:05:36
	 * @param
	 * @param session
	 * @return
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public long copyAssess(long oldassessid,long newpackageid,Session session) {
		/**
		 * 查询考核    复制前
		 */
		ElAssess assess = generalSqlComponent.query(ComesqlElCode.getAssessType, new Object[] {oldassessid,session.getCompid()});
//		/**
//		 * 查询体系
//		 */
//		ElPackage elpackage = generalSqlComponent.query(ComesqlElCode.getPackageType, new Object[] {assess.getPackageid(),session.getCompid()});
//		/**
//		 * 查询考核对象  
//		 */
//		List<ElAssessObject> object =generalSqlComponent.query(ComesqlElCode.getAssessObjectType, new Object[] {assessid,session.getCompid()});
//		/**
//		 * 查询评分者候选表
//		 */
//		List<ElAssessEvaluateCandidate> evaluates =generalSqlComponent.query(ComesqlElCode.getCandidateType, new Object[] {assess.getPackageid(),session.getCompid()});
		
		/**
		 * 查询类别和指标  复制前
		 */
		List<ElAssessItem> item =generalSqlComponent.query(ComesqlElCode.getAssessItem, new Object[] {assess.getPackageid(),session.getCompid()});
		
//		/**
//		 * 复制体系
//		 */
//		ElPackage elPackage2 = elpackage;
//		long packageid = elPackagesBusiness.copyPackageClass(elPackage2,session);
//		/**
//		 * 复制考核表
//		 */
//		ElAssess assess2 = assess;
//		long assessid2 = elAssessBusiness.copyAssessClass(assess2,packageid,session);
//		
//		/**
//		 * 复制被考核对象
//		 */
//		for(ElAssessObject obj:object) {
//			ElAssessObject obj2 = obj;
//			elAssessObjectBusiness.copyObjectCalss(assessid2,obj2,session);
//		}
//		/**
//		 * 复制评分候选表
//		 */
//		for(ElAssessEvaluateCandidate ev:evaluates) {
//			ElAssessEvaluateCandidate ev2 = ev;
//			elAssessEvaluateCandidateBusiness.copyEvaluateCandidateClass(packageid,ev2,session);
//		}
		
		
		
		/**
		 * 复制类别和指标
		 */
//		for(ElAssessItem ite:item) {
//			ite.setPackageid(packageid);
//			if(ite.getType()==0) {
//				elAssessItemBusiness.copyItemCalss(ite,session);
//			}else {
//				elAssessItemBusiness.copyItemCalss2(ite,session);
//			}
//			
//		}
		makeTree(item, newpackageid,0,0,session); 
		return oldassessid;
		
	}
	
	
	public List<ElAssessItem> makeTree(List<ElAssessItem> items,Long packageid,long pid,long newpid,Session session){
//		List<ElAssessItem> item2 =generalSqlComponent.query(ComesqlElCode.getAssessItem, new Object[] {assess.getPackageid(),session.getCompid()});
		//子类
		List<ElAssessItem> children = items.stream().filter(x -> Long.valueOf(x.getPid())  == pid).collect(Collectors.toList());
		//后辈中的非子类
		List<ElAssessItem> successor = items.stream().filter(x -> Long.valueOf(x.getPid())  != pid).collect(Collectors.toList());
				children.forEach(x ->
		         {   
		        	 //树底层的icon换成file的方法，现在暂时不需要了
//		        	 Integer i=haveSun(Long.valueOf(x.getId()),compid);
//		        	 if(0==i) {
//		        		 x.setIcon("file");
//		        		 x.setDisabled(false);
//		        	 }
		        	 long newpid2;
		        	 x.setPackageid(packageid);
		        	 if(x.getType()==0) {
		        		 newpid2 = elAssessItemBusiness.copyItemCalss(x,newpid,session);
		 			}else {
		 				newpid2 = elAssessItemBusiness.copyItemCalss2(x,newpid,session);
		 				/**
		 				 * 复制评分规则表
		 				 */
//		 				ElAssessFeedbackScoreRule scorerule = new ElAssessFeedbackScoreRule();
		 				ElAssessFeedbackScoreRule rule =generalSqlComponent.query(ComesqlElCode.getScoreRuleType, new Object[] {x.getId(),session.getCompid()});
	 					
	 					/**
		 				 * 复制评分单位
		 				 */
		 				ElAssessEvaluate evaluate =generalSqlComponent.query(ComesqlElCode.getEvaliateType, new Object[] {x.getId(),session.getCompid()});
		 				x.setId(newpid2);
		 				elAssessFeedbackScoreRuleBusiness.initScoreRuleCalss(rule,x,rule.getRule(),session);
		 				elAssessEvaluateBusiness.initEvaluateClass(evaluate,x,evaluate.getType(),session);
		 			}
        		    makeTree(successor, packageid,Long.valueOf(x.getId()),newpid2,session); 
		         }
				);
				return children;
	}

	/**
	 * 
	 * @Title: itemTree
	 * @Description: 类别树
	 * @Author dong
	 * @DateTime 2019年4月16日 上午9:32:24
	 * @param
	 * @param session
	 * @return
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<Map<String, Object>> itemTree(long packageid,Session session){
		List<ElAssessItem> item = elAssessItemBusiness.getItemTree(packageid,0L,session);
		Map<String,Object> map;
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		
		if(item.size()>=1) {
			for(ElAssessItem it:item) {
				map = new HashMap<String, Object>();
				map.put("id", it.getId());
				map.put("name", it.getName());
				map.put("pid", it.getPid());
				map.put("score", it.getScore());
				map.put("isShow", false);
				map.put("isOpen", false);
				map.put("icon", "folder");
				map.put("level", it.getLevel());
				map.put("packageid", it.getPackageid());
				map.put("type", it.getType());
				List<ElAssessItem> item2 = elAssessItemBusiness.getItemTree(packageid,it.getId(),session);
				List<Map<String,Object>> list2 = new ArrayList<Map<String,Object>>();
				for(ElAssessItem i:item2) {
					Map<String,Object> map2 = new HashMap<String, Object>();
					map2.put("id", i.getId());
					map2.put("name", i.getName());
					map2.put("pid", i.getPid());
					map2.put("score", i.getScore());
					map2.put("isShow", false);
					map2.put("isOpen", false);
					map2.put("icon", "file");
					map2.put("level", i.getLevel());
					map2.put("packageid", it.getPackageid());
					map2.put("type", i.getType());
					list2.add(map2);
				}
				map.put("childList", list2);
				list.add(map);
			}
		}
		return list;
		
	}
	
	/**
	 * 删除类别和指标
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteItem(long itemid,Short type,Session session) {
		List<ElAssessItem> itemlist = generalSqlComponent.query(ComesqlElCode.getItemPidList, new Object[] {itemid,session.getCompid()});
		ElAssessItem item  = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {itemid,session.getCompid()});
		if(itemlist.size()>0) {
			if(item.getPid()==0) {
				LehandException.throwException("请先删除指标");
			}else {
				LehandException.throwException("请先删除评分标准");
			}
			
		}
		generalSqlComponent.delete(ComesqlElCode.deleteItemType, new Object[] {itemid,session.getCompid()});
		generalSqlComponent.delete(ComesqlElCode.deleteEvaluateItemType, new Object[] {itemid,session.getCompid()});
		generalSqlComponent.delete(ComesqlElCode.deleteScoreRuleType, new Object[] {itemid,session.getCompid()});
		
	}
	
	/**
	 * 根据pid获取指标列表
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Pager getItem( Pager pager,long pid,Session session){
		generalSqlComponent.pageQuery(ComesqlElCode.getItemListType, new Object[] {pid,session.getCompid()}, pager);
		List<Map<String,Object>> list = (List<Map<String, Object>>) pager.getRows();
		
		for(Map<String,Object> item:list) {
			item.put("isShow", false);
			item.put("code", Double.valueOf(item.get("code").toString()));
		}
		pager.setRows(list);
		return pager;
	}
	

	/**
	 * 
	 * @Title: revokeAssess
	 * @Description: 撤销考核
	 * @Author dong
	 * @DateTime 2019年4月16日 下午9:03:54
	 * @return
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void revokeAssess(long packageid,Session session) {
		/**
		 * 判断考核能否撤回
		 */
		ElAssess assess1 = generalSqlComponent.query(ComesqlElCode.getAssessPackageStatusType, new Object[] {packageid,3,session.getCompid()});
		if(assess1!=null) {
			LehandException.throwException("该考核已公布不可撤销");
		}
		
		/**
		 * 修改体系
		 */
//		generalSqlComponent.update(ComesqlElCode.updatePackageType, new Object[] {-1,packageid,session.getCompid()});
		/**
		 * 查询考核
		 */
		ElAssess assess = generalSqlComponent.query(ComesqlElCode.getAssesspackageidType, new Object[] {packageid,session.getCompid()});
		/**
		 * 修改考核
		 */
		generalSqlComponent.delete(ComesqlElCode.updateAsseaType, new Object[] {-1,assess.getId(),session.getCompid()});
//		/**
//		 * 删除评分者候选表
//		 */
//		generalSqlComponent.delete(ComesqlElCode.deleteCandidate, new Object[] {packageid,session.getCompid()});
//		/**
//		 * 删除被考核对象表
//		 */
//		generalSqlComponent.delete(ComesqlElCode.deleteCandidate, new Object[] {assess.getId(),session.getCompid()});
//		/**
//		 * 删除评分者
//		 */
//		generalSqlComponent.delete(ComesqlElCode.deleteEvaluatePackageidType, new Object[] {packageid,session.getCompid()});
		
//		/**
//		 * 删除指标
//		 */
//		generalSqlComponent.delete(ComesqlElCode.deleteItemPackageidType, new Object[] {packageid,session.getCompid()});
//		
		/**
		 * 删除指标签收表
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteReceiveType, new Object[] {assess.getId(),session.getCompid()});
		
		/**
		 * 删除评分签收表
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteEvaluateReceiveType, new Object[] {packageid,session.getCompid()});
		
		List<ElAssessFeedback> back = generalSqlComponent.query(ComesqlElCode.getFeedBackType, new Object[] {assess.getId(),session.getCompid()});
		for(ElAssessFeedback b:back) {
			/**
			 * 删除评分记录表
			 */
			generalSqlComponent.delete(ComesqlElCode.deleteRecordType, new Object[] {b.getId(),session.getCompid()});
			
			/**
			 * 删除关系表 dl_file_business
			 */
			generalSqlComponent.delete(ComesqlElCode.deleteFileBusinessType, new Object[] {b.getId(),session.getCompid()});
			
			/**
			 * 删除佐证表 el_assess_feedback_file
			 */
			generalSqlComponent.delete(ComesqlElCode.deleteFeedbackFileType, new Object[] {b.getId(),session.getCompid()});
		}
		/**
		 * 删除反馈表
		 */
		generalSqlComponent.delete(ComesqlElCode.deleteFeedbackType, new Object[] {assess.getId(),session.getCompid()});

		//查询被考核对象
		List<ElAssessObject> assessObject = generalSqlComponent.query(ComesqlElCode.getAssessObjectType, new Object[] {assess.getId(),session.getCompid()});
		for(ElAssessObject li:assessObject){
			/**
			 * 删除待办
			 */
			generalSqlComponent.update(SqlCode.updateTodoItemDelete2,new Object[]{3,li.getCode(),li.getAssessid()});
		}

		//查询评分对象
		List<ElAssessEvaluate> assessEvaliate = generalSqlComponent.query(ComesqlElCode.getEvaliateType2, new Object[] {packageid,session.getCompid()});
		for(ElAssessEvaluate li:assessEvaliate){
			/**
			 * 删除待办
			 */
			generalSqlComponent.update(SqlCode.updateTodoItemDelete3,new Object[]{4,packageid,li.getCode()});
		}

		//删除整改以及反馈的信息
        removePackageRectification(packageid, session);

		//删除自评审核信息
        removeSelfAuditInfo(packageid, session);
    }

    private void removeSelfAuditInfo(long packageid, Session session) {
        generalSqlComponent.getDbComponent().delete(
                "DELETE FROM el_assess_feedback_audit WHERE compid=? and packageid = ?",
                new Object[]{session.getCompid(),packageid});
    }

    private void removePackageRectification(long packageid, Session session) {
        generalSqlComponent.getDbComponent().delete(
                "DELETE FROM el_package_rectification WHERE compid=? and packageid = ?",
                new Object[]{session.getCompid(),packageid});

        generalSqlComponent.getDbComponent().delete(
                "DELETE FROM el_package_rectification_audit WHERE compid=? and packageid = ?",
                new Object[]{session.getCompid(),packageid});

        generalSqlComponent.getDbComponent().delete(
                "DELETE FROM el_package_rectification_feedback WHERE compid=? and packageid = ?",
                new Object[]{session.getCompid(),packageid});
    }


    /**
	 * 
	 * @Title: move
	 * @Description: 类别指标拖拽
	 * @Author dong
	 * @DateTime 2019年4月23日 上午10:45:47
	 * @param srcid 源类别指标id
	 * @param tagid 目标类别指标id
	 * @param flag  1:上面,2:下面,3:里面
	 * @param session
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void move(long srcid,long tagid,long flag,Session session) {
		//源类别
		ElAssessItem sourceItem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {srcid,session.getCompid()});
		
		//目标类别 
		ElAssessItem targetItem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {tagid,session.getCompid()});
		
		//查询子集
		List<ElAssessItem> sourceSunItem = generalSqlComponent.query(ComesqlElCode.getItemPidList, new Object[] {srcid,session.getCompid()});
		if (flag == 3) {
			if(targetItem.getPid()!=0) {
				LehandException.throwException("不支持的目标层级");
			}
			if(sourceSunItem.size()>0&&sourceItem.getPid()==0) {
				LehandException.throwException("类别下存在指标，不允许拖拽");
			}
			//到目标类别里面只将源类别pid修改为目标id
			sourceItem.setPid(targetItem.getId());
			sourceItem.setLevel(elAssessItemBusiness.getItemLevel(sourceItem.getPid(),session.getCompid()));
			generalSqlComponent.update(ComesqlElCode.updateItemOrderidType, sourceItem);
			
		} else {
			
			//源类 为有评分标准的指标并且目标为类别 则不能拖拽(有评分保准的指标不能转为类别)
			if(sourceItem.getPid()!=0&&sourceSunItem.size()>0&&targetItem.getPid()==0) {
				LehandException.throwException("指标下存在评分标准，不允许拖拽");
			}
			
			//源类为有指标的类别 并且目标不是类别   不允许拖拽
			if(sourceItem.getPid()==0&&sourceSunItem.size()>0&&targetItem.getPid()!=0) {
				LehandException.throwException("类别下存在指标，不允许拖拽");
			}
			
			//目标类别 的父类别
			ElAssessItem targetPidItem = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {targetItem.getPid(),session.getCompid()});
			
			sourceItem.setPid(targetPidItem==null? 0:targetPidItem.getId());
			sourceItem.setPid(targetPidItem==null? 0:targetPidItem.getId());
			sourceItem.setLevel(elAssessItemBusiness.getItemLevel(sourceItem.getPid(),session.getCompid()));
			if (flag == 1) {
				//目标orderid减1 如果目标类别没有父类别，则pid为0 否则是目标类别的父类别的id
				sourceItem.setOrderid(targetItem.getOrderid()-1);
				generalSqlComponent.update(ComesqlElCode.updateItemOrderidType, sourceItem);
			} else {
				//orderid加1 如果目标类别没有父类别，则pid为0 否则是目标类别的父类别的id
				sourceItem.setOrderid(targetItem.getOrderid()+1);
				sourceItem.setPid(targetPidItem==null? 0:targetPidItem.getId());
				generalSqlComponent.update(ComesqlElCode.updateItemOrderidType, sourceItem);
			}
		}
		if(sourceItem.getType()==0) {
			updateItem(sourceItem,session);
		}
	}




	/**待办任务begin****************************************************************************************************************/
	@Resource
	private TodoItemService todoItemService;

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param name 事项名称
	 * @param bizid 该事项对应的业务ID
	 * @param path 资源路径
	 * @param session 当前会话
	 */
	private void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path, Long orgCode, Long assessid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setBizid(bizid);
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
//		itemDTO.setUserid(session.getUserid());
//		itemDTO.setUsername(session.getUsername());
		itemDTO.setOrgid(orgCode);
		itemDTO.setName(name);
		itemDTO.setBizid(bizid);
		itemDTO.setSubjectid(assessid);
		itemDTO.setTime(new Date());
		itemDTO.setPath(path);
		itemDTO.setState(Todo.STATUS_NO_READ);
		todoItemService.createTodoItem(itemDTO);
	}

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param bizid 该事项对应的业务ID
	 * @param session 当前会话
	 */
	private void removeTodoItem(Todo.ItemType itemType,  String bizid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setBizid(bizid);
		todoItemService.deleteTodoItem(itemDTO);
	}
	/**待办任务end****************************************************************************************************************/
}
