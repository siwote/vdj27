package com.lehand.evaluation.lib.business;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessEvaluateCandidate;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackAudit;
import com.lehand.evaluation.lib.pojo.ElAssessItem;
import com.lehand.evaluation.lib.pojo.ElAssessObject;

import java.util.List;
import java.util.Map;

public interface ElAssessFeedbackAuditBusiness {

    public ElAssessFeedbackAudit getElAssessFeedbackAudit(Long packageid,Long subjectid, Session session);

    public void audit(Long packageid, Long subjectid,Byte status,String auditInfo, Session session);

}
