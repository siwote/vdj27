package com.lehand.evaluation.lib.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.ElPackagePublishBusiness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;

/**
 * 
 * @ClassName: ElPackagePublishController
 * @Description: 考核体系公布
 * @Author dong
 * @DateTime 2019年4月15日 下午5:14:16
 */
@RestController
@RequestMapping("/evaluation/publish")
public class ElPackagePublishController  extends ElBaseController{
	private static final Logger logger = LogManager.getLogger(ElPackageController.class);
	
	@Resource
	ElPackagePublishBusiness elPackagePublishBusiness;
	/**
	 * 
	 * @Title: queryAsssessNot
	 * @Description: 考核结果公布列表
	 * @Author dong
	 * @DateTime 2019年4月15日 下午5:18:21
	 * @return
	 */
	@RequestMapping("/publish")
	private Message queryAsssessNot(Pager pager,long status,String likeStr) {
		Message message = new Message();
		try {
			elPackagePublishBusiness.queryAsssess(pager,likeStr,status,getSession());
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
		
	}
	
	/**
	 * 
	 * @Title: queryAsssessCount
	 * @Description: 考核结果公布列表数量
	 * @Author dong
	 * @DateTime 2019年4月23日 上午8:49:26
	 * @param
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping("/publishcount")
	private Message queryAsssessCount() {
		Message message = new Message();
		try {
			List<Map<String,Object>> listMap = elPackagePublishBusiness.queryAsssessCount(getSession());
			message.success().setData(listMap);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
		
	}
	
	/**
	 * 
	 * @Title: queryObject
	 * @Description: 获取考核对象及得分
	 * @Author dong
	 * @DateTime 2019年4月15日 下午9:41:48
	 * @param
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/obj")
	private Message queryObject(Pager pager,long assessid,String oneself) {
		Message message = new Message();
		try {
			elPackagePublishBusiness.queryObject(pager,assessid,oneself,getSession());
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
		
	}
	
	/**
	 * 
	 * @Title: queryItemScore
	 * @Description: 获取指标及自评分、他评分
	 * @Author dong
	 * @DateTime 2019年4月16日 下午3:15:40
	 * @param pager
	 * @param pid
	 * @return
	 */
	@RequestMapping("/item")
	private Message queryItemScore(Pager pager,long pid,long objid,long code) {
		Message message = new Message();
		try {
			elPackagePublishBusiness.queryItemScore(pager,pid,objid,code,getSession());
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * Description: 发布人有最终的一票否决权，可以修改他评数据
	 * @author PT  
	 * @date 2020年5月20日 
	 * @param feedbackid
	 * @param content
	 * @param score
	 * @return
	 */
	@RequestMapping("/modiotherscore")
	private Message modiOtherScore(Long feedbackid,String content,Double score,String fileids) {
		Message message = new Message();
		try {
			elPackagePublishBusiness.modiOtherScore(getSession(),feedbackid,content,score,fileids);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: publish
	 * @Description: 公布
	 * @Author dong
	 * @DateTime 2019年4月17日 上午11:36:43
	 * @param assessid
	 * @param
	 * @return
	 */
	@RequestMapping("/publish2")
	private Message publish(long assessid,int remark1) {
		Message message = new Message();
		try {
			elPackagePublishBusiness.publish(assessid,remark1,getSession());
			message.success().setData("已公布");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: getEvaluate
	 * @Description: 获取各评分单位对被考核对象的评分和自评分
	 * @Author dong
	 * @DateTime 2019年4月18日 下午6:00:22
	 * @param assessid
	 * @param code
	 * @return
	 */
	@RequestMapping("/evaluate")
	private Message getEvaluate(long assessid,long code) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = elPackagePublishBusiness.getEvaluate(assessid,code,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	@RequestMapping("/tree")
	private Message getTree(long orgid,long packageid) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = elPackagePublishBusiness.getTree(orgid,packageid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	
}
