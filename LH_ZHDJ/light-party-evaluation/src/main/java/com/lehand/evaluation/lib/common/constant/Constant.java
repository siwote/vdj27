package com.lehand.evaluation.lib.common.constant;

public class Constant {
	public static final String EMPTY = "";
	
	//考核业务类型
	public static final Integer MODULE = 4;
		
	//固定参数
	public static final Long MID = (long) 2;
	
	public static final String TIMESTR = " 10:00:00";
}
