package com.lehand.evaluation.lib.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.ElpackageRankBusiness;
import com.lehand.evaluation.lib.pojo.ElAssess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;

/**
 * 
 * @ClassName: ElpackageRankController
 * @Description: 考核结果排名
 * @Author dong
 * @DateTime 2019年4月17日 下午3:16:17
 */
@RestController
@RequestMapping("/evaluation/ranking")
public class ElpackageRankController  extends ElBaseController{
	private static final Logger logger = LogManager.getLogger(ElPackageController.class);

	@Resource
	ElpackageRankBusiness elpackageRankBusiness;
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 获取考核列表
	 * @Author dong
	 * @DateTime 2019年4月17日 下午3:23:00
	 * @return
	 */
	@RequestMapping("/assess")
	private Message getAssess(Short year) {
		Message message = new Message();
		try {
			List<ElAssess> list  = elpackageRankBusiness.getAssess(year,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}
	
	/**
	 * 
	 * @Title: orgRank
	 * @Description: 获取党组织考核结果排名
	 * @Author dong
	 * @DateTime 2019年4月17日 下午4:35:01
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/orgrank")
	private Message orgRank(String assessid) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list  = elpackageRankBusiness.orgRank(assessid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}
	
	/**
	 * 
	 * @Title: itemRank
	 * @Description: 评分标准扣分次数结果排名
	 * @Author dong
	 * @DateTime 2019年4月19日 下午4:11:24
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/item1")
	private Message itemRank(String assessid) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list  = elpackageRankBusiness.itemRank(assessid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}
	
	/**
	 * 
	 * @Title: itemRank2
	 * @Description: 指标扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月19日 下午5:37:07
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/item2")
	private Message itemRank2(String assessid) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list  = elpackageRankBusiness.itemRank2(assessid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}
	
	/**
	 * 
	 * @Title: itemRank3
	 * @Description: 类别扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月22日 下午9:58:55
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/item3")
	private Message itemRank3(String assessid) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list  = elpackageRankBusiness.itemRank3(assessid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}
	
	/**
	 * 
	 * @Title: itemRank3
	 * @Description: 类别扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月22日 下午9:58:55
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/rank")
	private Message rank(String assessid,int type) {
		Message message = new Message();
		try {
			Map<String,Object> result  = elpackageRankBusiness.rank(assessid,type,getSession());
			message.success().setData(result);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
	}
}
