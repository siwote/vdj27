package com.lehand.evaluation.lib.dto;

import java.util.List;
import java.util.Map;

/**
 * 	自评
 *	@author Tangtao
 *
 */
public class SelfAssessmentScoreDto {
	//反馈表主键ID
	private Long id;
	
	//考核对象id
	private Long code;
	
	//被考核对象名称
	private String name;
	
	//考核编码
	private Long assessid;
	
	//指标id
	private Long itemid;
	
	//自评分
	private Double selfscore;
	
	//当前指标分
	private Double score;
	
	//自查情况
	private String feedbackcontent;
	
	//资料档案
	private List<Map<String,Object>> filenames;

    //附件(直接上传附件不需要从资料库里去)
    private String files;

    private List<Map<String,Object>> listFiles;

    public List<Map<String, Object>> getListFiles() {
        return listFiles;
    }

    public void setListFiles(List<Map<String, Object>> listFiles) {
        this.listFiles = listFiles;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAssessid() {
		return assessid;
	}

	public void setAssessid(Long assessid) {
		this.assessid = assessid;
	}

	public Long getItemid() {
		return itemid;
	}

	public void setItemid(Long itemid) {
		this.itemid = itemid;
	}

	public Double getSelfscore() {
		return selfscore;
	}

	public void setSelfscore(Double selfscore) {
		this.selfscore = selfscore;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getFeedbackcontent() {
		return feedbackcontent;
	}

	public void setFeedbackcontent(String feedbackcontent) {
		this.feedbackcontent = feedbackcontent;
	}

	public List<Map<String, Object>> getFilenames() {
		return filenames;
	}

	public void setFilenames(List<Map<String, Object>> filenames) {
		this.filenames = filenames;
	}
		
}
