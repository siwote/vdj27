package com.lehand.evaluation.lib.service.jobservices;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.pojo.ElAssessScoreRecord;
import com.lehand.evaluation.lib.pojo.ElEvaluateReceive;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.common.constant.EvalResultScoreSqlCfg;
import com.lehand.evaluation.lib.common.constant.ResultSelfEvalSqlCfg;

public class ScheJobsService {
	
private static final Logger logger = LogManager.getLogger(ScheJobsService.class);
	
	private static final AtomicBoolean CREATE = new AtomicBoolean(false);//是否正在创建任务
	
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	/**
	 * 评分单位 信息
	 * 
	 * SELECT DISTINCT
			x.`code`,	
			x.compid,
			y.`name`,
			y.score,
			y.packageid,
			y.cycle,
			y.enddate,
		y.assessid
		FROM
			`el_assess_evaluate` x
		INNER JOIN (
			SELECT
				a.id assessid,
				a.packageid,
				b.compid,
				a.`name`,
				a.cycle,
				a.score,
				b.`code`,
				b.id,
				b.`status`,
				a.enddate
			FROM
				el_assess a
			INNER JOIN el_assess_item_receive b ON a.id = b.assessid
			AND a.compid = b.compid
			WHERE
				b.`status` <> 2
			AND date_format(now(), '%Y-%m-%d') >  str_to_date(a.enddate, '%Y-%m-%d %H')
		) y ON x.compid = y.compid AND x.packageid = y.packageid
	 */
	private final String EvaluationGetEvaluateInfo = "EvaluationGetEvaluateInfo";
	
	/**
	 * 反馈表id和compid
	 * 
	 * SELECT DISTINCT (x.id),x.compid,y.code FROM el_assess_feedback x INNER JOIN (
			SELECT
				a.id assessid,
				a.packageid,
				b.compid,
				a.`name`,
				a.score,
				b.`code`,
				b.id,
				b.`status`,
				a.enddate
			FROM
				el_assess a
			INNER JOIN el_assess_item_receive b ON a.id = b.assessid
			AND a.compid = b.compid
			WHERE
				b.`status` <> 2
			AND date_format(now(), '%Y-%m-%d') >  str_to_date(a.enddate, '%Y-%m-%d %H')
		) y ON x.compid = y.compid AND x.`code` = y.`code` WHERE x.`status` <> 2 AND x.`status` <>3
	 * 
	 */
	private final String EvaluationGetFeedBackList = "EvaluationGetFeedBackList";
	
	/**
	 * 考核对象接收表数据
	 * SELECT
			a.id assessid,
		a.compid,
		a.packageid,
			b.`code`,
			b.id,
			b.`status`,
		a.enddate
		FROM
			el_assess a
		INNER JOIN el_assess_item_receive b ON a.id = b.assessid
		AND a.compid = b.compid
		
		WHERE b.`status` <> 2
		
		AND date_format(now(), '%Y-%m-%d') > str_to_date(a.enddate, '%Y-%m-%d %H')
	 */
	private final String EvaluationGetObjecRecive ="EvaluationGetObjecRecive";
	
	/**
	 * 通过考核对象接收表id修改考核对象接收表 状态
	 * 
	 * UPDATE el_assess_item_receive SET `status` = ?,remark2 = ? WHERE id = ? and compid = ?
	 */
	private final String UpdateObjRecivesById = "UpdateObjRecivesById";
	
	@Resource
	private GeneralSqlComponent sqlComponent;
	
	
	@Transactional(rollbackFor=Exception.class)
	public void updateStatus()  throws LehandException {
		try {
			synchronized (CREATE) {
				if (CREATE.get()) {
					logger.info("之前任务正在执行,本次不处理");
					return;
				}
				CREATE.set(true);
			}
			logger.info("定时操作修改开始...");
			
			List<Map<String,Object>> evaluateList = sqlComponent.query(EvaluationGetEvaluateInfo, new Object[] {});
			if(evaluateList.size()<0) {
				logger.info("返回返回");
				return;
			}
			List<Map<String,Object>> feedbackidList = sqlComponent.query(EvaluationGetFeedBackList,new Object[] {});
			
			List<Map<String,Object>>  objectReciveList = sqlComponent.query(EvaluationGetObjecRecive,new Object[] {});
			threadPoolTaskExecutor.execute(new Runnable() {
				@Override
				public void run() {					
					//插入评分单位
					for(Map<String,Object> evaluate :evaluateList) {
						ElEvaluateReceive rev = new  ElEvaluateReceive();
						rev.setPackageid((Long) evaluate.get("packageid"));
						rev.setPackagename((String) evaluate.get("name"));
						rev.setCode( (Long) evaluate.get("code"));
						rev.setCompid((Long) evaluate.get("compid"));
						rev.setStatus((short) 0);
						rev.setAssessscore((Double) evaluate.get("score"));
						rev.setCycle((String) evaluate.get("cycle"));
						rev.setOverdate("enddate");		
						List<ElEvaluateReceive> reciver  = new ArrayList<ElEvaluateReceive>();
						try {
							reciver =  sqlComponent.query(ResultSelfEvalSqlCfg.CheckBeforeCreateReceive,rev); 				
						} catch (Exception e) {
							LehandException.throwException("查询是否已经存在他评接收记录失败");
							logger.error("定时操作查询是否已经存在他评接收记录失败",e);
						}
						if(reciver.size()<1) {					
							try {
									sqlComponent.insert(ResultSelfEvalSqlCfg.ResultSelfEvalCreateReceive, rev);
							} catch (Exception e) {
								LehandException.throwException("插入评分单位数据失败");
								logger.error("定时操作插入评分单位数据失败",e);
							}
						}
					}
					
					//修改反馈表状态&&插入评分记录
					if(feedbackidList.size()>0) {
						for(Map<String,Object> feedbackid:feedbackidList) {
							try {		
								//修改反馈表状态
								sqlComponent.update(EvalResultScoreSqlCfg.UpdateFeedBackStatusById,
										new Object[] {2,1,DateEnum.YYYYMMDDHHMMDD.format(),feedbackid.get("id"),feedbackid.get("compid")});
							} catch (Exception e) {
								LehandException.throwException("修改反馈表状态异常");
								logger.error("定时操作修改反馈表状态异常",e);
							}
							
							//插入评分记录
							ElAssessScoreRecord record = new ElAssessScoreRecord();
							record.setCompid((Long) feedbackid.get("compid"));
							record.setContent("未按时上报");
							record.setFeedbackid((Long) feedbackid.get("id"));
							record.setScore((double) 0);
							record.setUsertor((long) feedbackid.get("code"));
							try {								
								sqlComponent.insert(EvalResultScoreSqlCfg.EvalResultScoreSaveScore, record);
							} catch (Exception e) {
								LehandException.throwException("定时操作插入评分数据异常");
								logger.error("定时操作插入评分数据异常",e);
							}
						}
					}
					
					//修改考对象接收表&修改考核表
					if(objectReciveList.size()>0) {
						Object obj = new Object();
						for(Map<String,Object> objRec:objectReciveList) {
							//考核对象表
							sqlComponent.update(UpdateObjRecivesById, new Object[] {2,1,objRec.get("id"),objRec.get("compid")});
							if(objRec.get("assessid").equals(obj)) {
								continue;
							}
							//修改考核表
							sqlComponent.update(ComesqlElCode.updateAsseaType,new Object[] {4,objRec.get("assessid"),objRec.get("compid")});
							obj = objRec.get("assessid");
						}
					}
					
					
					
				}
			});
			logger.info("定时操作结束...");
		} catch (Exception e) {
			logger.error("定时操作异常",e);
		} finally {
			CREATE.set(false);
		}
			
	}
	
	public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
		return threadPoolTaskExecutor;
	}

	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}
}
