package com.lehand.evaluation.lib.dto;

public class AccountInfoDto {
	
	//账号类型 0个人,1组织
	private Short accflag;
	
	//账号userid
	private Long userid;
	
	//账号名
	private String username;
	
	//组织机构id
	private Long orgid;
		
	//组织机构名
	private String orgname;

	public Short getAccflag() {
		return accflag;
	}

	public void setAccflag(Short accflag) {
		this.accflag = accflag;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getOrgid() {
		return orgid;
	}

	public void setOrgid(Long orgid) {
		this.orgid = orgid;
	}

	public String getOrgname() {
		return orgname;
	}

	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}
	
	
}
