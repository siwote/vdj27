package com.lehand.evaluation.lib.business.selfevaluation;

import java.util.Map;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.dto.AssessmentScoreDto;

/**
 *	@author Tangtao
 *
 */
public interface EvalResultScoreBusiness {

	/**
	 *	获取考核对象列表
	 * @param orgid 
	 *	@param
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月11日
	 *	Tangtao
	 */
	public Message getEvalObjectList(Long packageid,String str,Long compid, Long orgid ) throws Exception;
	
	/**
	 *	他评列表
	 *	@param pager
	 *	@param str
	 *	@param session
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	public Message pageQuery(Pager pager,String str,Short status, Session session)throws Exception;
	
	/**
	 *	保存考核评分
	 *	@param feedBack
	 * @param
	 *	@return
	 *	@throws Exception
	 *	boolean
	 *	2019年4月11日
	 *	Tangtao
	 */
	public Message saveScore(AssessmentScoreDto feedBack, Long compId, Long currUserId) throws Exception;
	
	
	/**
	 *	评分指标详情
	 *	@param id
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月11日
	 *	Tangtao
	 */
	public Message getEvalPerforInfo(Long id,Long compId) throws Exception;
	
	
	/**
	 *	上报评分
	 *	@param packageid
	 *	@param session
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	public Message reportEval(Long packageid,Session session) throws Exception;
	
	
	
	/**
	 *	获取他评状态树
	 *	@param compid
	 *	@param code
	 *	@return
	 *	@throws Exception
	 *	Map<String,Object>
	 *	2019年4月17日
	 *	Tangtao
	 */
	public Map<String,Object> getStatus(Long compid,Long code) throws Exception;
}
