package com.lehand.evaluation.lib.service.assess;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackAudit;
import com.lehand.evaluation.lib.pojo.ElPackage;
import com.lehand.horn.partyorgan.constant.Constant;
import org.springframework.util.StringUtils;

/**
 * 
 * @ClassName: ElPackageBusiness
 * @Description: 体系
 * @Author dong
 * @DateTime 2019年4月12日 下午4:32:34
 */
public class ElPackagesBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;

    public ElPackage getElPackage(Long id, Session session) {
        return generalSqlComponent.query(ComesqlElCode.getElPackage, new Object[]{session.getCompid(),id});
    }
	
	/**
	 * 
	 * @Title: initPackageClass
	 * @Description: 初始化体系
	 * @Author dong
	 * @DateTime 2019年4月11日 下午3:49:04
	 * @param elAssess
	 * @param elPackage
	 * @param session
	 */
	public  long initPackageClass(ElAssess elAssess, ElPackage elPackage, Session session) {
		elPackage.setCompid(session.getCompid());
		elPackage.setName(elAssess.getName());
		elPackage.setContent(elAssess.getContent());
		elPackage.setStatus((byte) 1);
		elPackage.setScore(elAssess.getScore());
		elPackage.setIndustry(StringConstant.EMPTY);
		elPackage.setScoremode(elAssess.getScoremode());
		elPackage.setOrderid(elAssess.getOrderid());
		elPackage.setOrgid(Long.valueOf(session.getCurrentOrg().getOrgcode()));
		elPackage.setOrgname(session.getCurrentOrg().getOrgname());
		elPackage.setCreater(session.getUserid());
		elPackage.setModier(session.getUserid());
		elPackage.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		elPackage.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		elPackage.setRemark1(0);
		elPackage.setRemark2(0);
		elPackage.setRemark3(StringConstant.EMPTY);
		elPackage.setRemark4(StringConstant.EMPTY);
		elPackage.setRemark5(StringConstant.EMPTY);
		elPackage.setRemark6(StringConstant.EMPTY);
		elPackage.setSelfassessaudit((byte) (elAssess.getSelfassessaudit()==0 ? 0 : 1));
		return generalSqlComponent.insert(ComesqlElCode.addPackType, elPackage);
	}
	
	/**
	 * 
	 * @Title: updatePackageClass
	 * @Description: 修改体系
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:57:15
	 * @param elAssess
	 * @param
	 * @param session
	 * @return
//	 */
	public  void updatePackageClass(ElAssess elAssess,long packageid, Session session ){
		ElPackage elPackage = generalSqlComponent.query(ComesqlElCode.getPackageType, new Object[] {packageid,session.getCompid()});
		elPackage.setCompid(session.getCompid());
		elPackage.setName(elAssess.getName());
		elPackage.setContent(elAssess.getContent());
		elPackage.setScore(elAssess.getScore());
		elPackage.setIndustry(StringConstant.EMPTY);
		elPackage.setScoremode(elAssess.getScoremode());
		elPackage.setOrderid(elAssess.getOrderid());
		elPackage.setModier(session.getUserid());
		elPackage.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		elPackage.setRemark1(0);
		elPackage.setRemark2(0);
		elPackage.setRemark3(StringConstant.EMPTY);
		elPackage.setRemark4(StringConstant.EMPTY);
		elPackage.setRemark5(StringConstant.EMPTY);
		elPackage.setRemark6(StringConstant.EMPTY);
        elPackage.setSelfassessaudit(elAssess.getSelfassessaudit());
		generalSqlComponent.update(ComesqlElCode.updatePackageDataType, elPackage);
		return;
		
	}
	
	/**
	 * 
	 * @Title: copyPackageClass
	 * @Description: 复制体系
	 * @Author dong
	 * @DateTime 2019年4月15日 下午3:41:45
	 * @param
	 * @param
	 * @param session
	 * @return
	 */
	public long copyPackageClass(ElPackage elPackage,Session session) {
		elPackage.setCompid(session.getCompid());
		elPackage.setStatus((byte) 1);
		elPackage.setOrgid(Long.valueOf(session.getCurrentOrg().getOrgcode()));
		elPackage.setOrgname(session.getCurrentOrg().getOrgname());
		elPackage.setCreater(session.getUserid());
		elPackage.setModier(session.getUserid());
		elPackage.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		elPackage.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		return generalSqlComponent.insert(ComesqlElCode.addPackType, elPackage);
		
	}
}
