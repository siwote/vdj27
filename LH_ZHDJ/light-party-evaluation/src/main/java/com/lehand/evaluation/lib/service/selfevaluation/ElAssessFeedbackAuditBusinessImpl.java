package com.lehand.evaluation.lib.service.selfevaluation;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.business.ElAssessFeedbackAuditBusiness;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.common.constant.ResultSelfEvalSqlCfg;
import com.lehand.evaluation.lib.dto.OrgInfo;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackAudit;
import com.lehand.evaluation.lib.pojo.ElEvaluateReceive;
import com.lehand.evaluation.lib.pojo.ElPackage;
import com.lehand.evaluation.lib.pojo.UrcUser;
import com.lehand.todo.constant.Todo;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ElAssessFeedbackAuditBusinessImpl implements ElAssessFeedbackAuditBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;

	@Resource
    ResultSelfEvalBusinessImpl resultSelfEvalBusinessImpl ;

    /**
     * 添加自评审核信息
     * @param elAssessFeedbackAudit
     * @param session
     * @return
     */
	public long initElAssessFeedbackAudit(ElAssessFeedbackAudit elAssessFeedbackAudit, Session session) {
		elAssessFeedbackAudit.setAuditcontent("");
		elAssessFeedbackAudit.setCompid(session.getCompid());
		elAssessFeedbackAudit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		if(getElAssessFeedbackAudit(elAssessFeedbackAudit.getPackageid(),elAssessFeedbackAudit.getSubjectid(),
                session)!=null){
            generalSqlComponent.update(ComesqlElCode.updateElAssessFeedbackAudit,
                    new Object[]{session.getCompid(),elAssessFeedbackAudit.getPackageid(),elAssessFeedbackAudit.getSubjectid()});
            return 0L;
        }
		return generalSqlComponent.insert(ComesqlElCode.addElAssessFeedbackAudit, elAssessFeedbackAudit);
	}

    /**
     * 获取审核信息
     * @param packageid
     * @param subjectid
     * @param session
     * @return
     */
    @Override
    public  ElAssessFeedbackAudit getElAssessFeedbackAudit(Long packageid,Long subjectid, Session session) {
        return generalSqlComponent.query(ComesqlElCode.listElAssessFeedbackAudit, new Object[]{session.getCompid(),
                packageid,subjectid});
    }

    /**
     * 自评审核
     * @param packageid
     * @param subjectid
     * @param status
     * @param auditInfo
     * @param session
     */
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void audit(Long packageid, Long subjectid,Byte status,String auditInfo, Session session) {
        List<OrgInfo> orgs = getCodeLong(session.getCompid(),session.getUserid());
        if(orgs==null || orgs.size()<=0){
            LehandException.throwException("审核组织不存在！");
        }
        if(orgs.get(0).getAccflag()==0){//书记账号审核
            for (OrgInfo org : orgs) {
                Long orgid = org.getOrgid();
                Long orgid2 = Long.valueOf(org.getOrgcode());
                //校验审核记录是否存在
                Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from el_assess_feedback_audit " +
                                "where packageid=? and subjectid=? and compid=? and status=0 and modiler=? limit 1",
                        new Object[]{packageid,orgid,session.getCompid(),session.getUserid()});
                if(map!=null){
                    auditSelfInfo(packageid, status, auditInfo, session, orgid, orgid2);
                    break;
                }
            }
        }else{//组织账号审核
            Long orgid = session.getCurrentOrg().getOrgid();
            Long orgid2 = Long.valueOf(session.getCurrentOrg().getOrgcode());
            Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from el_assess_feedback_audit " +
                            "where packageid=? and subjectid=? and compid=? and status=0",
                    new Object[]{packageid,orgid,session.getCompid()});
            if(map==null){
                LehandException.throwException("该记录已被他人审核完成请返回！");
            }
            auditSelfInfo(packageid, status, auditInfo, session, orgid, orgid2);
        }
    }


    private void auditSelfInfo(Long packageid, Byte status, String auditInfo, Session session, Long orgid, Long orgid2) {
        generalSqlComponent.getDbComponent().update("update el_assess_feedback_audit set status=?,auditcontent=? " +
                "where compid=? and packageid=? and subjectid=?",new Object[]{status,
                auditInfo,session.getCompid(), packageid,orgid});
        if(status==2){//nopass
            generalSqlComponent.getDbComponent().update("update el_assess_item_receive set status=1 where compid=? " +
                    "AND assessid=? and code =?",new Object[]{session.getCompid(), packageid,orgid2});
        }else{//pass
            addTtherCommentsScore(packageid, session, orgid2);
        }
    }

    public void addTtherCommentsScore(Long packageid, Session session, Long orgid) {
        generalSqlComponent.getDbComponent().update("update el_assess_item_receive set status=2 where compid=? " +
                "AND assessid=? and code =?",new Object[]{session.getCompid(), packageid,orgid});
        ElPackage pack = generalSqlComponent.query(ComesqlElCode.getPackageType, new Object[]{packageid, session.getCompid()});
        //判断是否全部都审核通过了
        Map<String,Object> data = generalSqlComponent.getDbComponent().getMap(
                "select (select count(*) from el_assess_feedback_audit where compid=a.compid and packageid=a.id " +
                        "AND status=1) passnum,(select count(*) from el_assess_object where compid=a.compid and " +
                        "assessid=a.id) allnum  from el_package a where a.id=? and a.compid=?",
                new Object[]{packageid,session.getCompid()});
        if (data.get("passnum").equals(data.get("allnum"))) {
            //查询评分单位
            List<Map<String, Object>> codes = generalSqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalRevceiveInfo, new Object[]{session.getCompid(), packageid});
            if (codes.size() < 1) {
                LehandException.throwException("没有评分单位");
            }
            for (Map<String, Object> code : codes) {
                ElEvaluateReceive rev = new ElEvaluateReceive();
                rev.setPackageid(packageid);
                rev.setPackagename((String) code.get("name"));
                rev.setCode((Long) code.get("code"));
                rev.setCompid(session.getCompid());
                rev.setStatus((short) 0);
                rev.setAssessscore((Double) code.get("score"));
                rev.setCycle("");
                rev.setOverdate("");
                List<ElEvaluateReceive> reciver = new ArrayList<>();
                try {
                    reciver = generalSqlComponent.query(ResultSelfEvalSqlCfg.CheckBeforeCreateReceive, rev);
                } catch (Exception e) {
                    LehandException.throwException("查询是否已经存在他评接收记录失败", e);
                    e.printStackTrace();
                }
                if (reciver.size() < 1) {
                    try {
                        generalSqlComponent.insert(ResultSelfEvalSqlCfg.ResultSelfEvalCreateReceive, rev);
                    } catch (Exception e) {
                        LehandException.throwException("插入评分单位数据失败", e);
                    }
                }
                //生成评分对象的待办事项
                resultSelfEvalBusinessImpl.addTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SCORE,
                        pack.getName() + "（" + pack.getCreatetime().trim().substring(0, 4) + "年-" + session.getCurrentOrg().getOrgname() + "）", String.valueOf(packageid), Todo.FunctionURL.CHECK_EVALUATE.getPath(), (Long) code.get("code"), session);
            }
            generalSqlComponent.update(ResultSelfEvalSqlCfg.ResultSelfChangeAssaStatus,
                    new Object[]{session.getCompid(), packageid,
                            session.getCurrentOrg().getOrgid()});
        }
    }

    public List<OrgInfo> getCodeLong(Long compid,  Long userid) {
        List<OrgInfo> list = new ArrayList();
        OrgInfo org = new OrgInfo();
        UrcUser user = generalSqlComponent.getDbComponent().getBean("select * from urc_user where compid=? and userid=?",
                UrcUser.class,new Object[]{compid,userid});
        String idno = user.getIdno();
        Integer accflag = user.getAccflag();
        //个人账号登录需要判断下是否是组织书记
        if(accflag.equals(0)){
            //3.通过身份证号查询该人的班子成员信息
            List<Map<String,Object>> listMap = generalSqlComponent.getDbComponent().listMap(
                    "SELECT * FROM t_dzz_bzcyxx where zjhm=? and zfbz=0 and dnzwname in (3100000009,4100000013," +
                            "5100000017)",new Object[]{idno} );
            //4.班子成员中查询到了书记职务信息
            if(listMap!=null && listMap.size()>0){
                for (Map<String, Object> map : listMap) {
                    // 5.通过组织32位ID查询任职组织的ID
                    org = generalSqlComponent.getDbComponent().getBean(
                            "SELECT b.orgid,b.orgcode FROM t_dzz_info_simple a " +
                                    "left join urc_organization b on a.organcode = b.orgcode where organid=? limit 1",
                            OrgInfo.class,
                            new Object[]{map.get("organid")}
                    );
                    org.setAccflag(accflag);
                    list.add(org);
                }
            }
        }else{//登录的是创建的组织账号
            //直接通过userid找到对应的组织
            org = generalSqlComponent.getDbComponent().getBean(
                    "SELECT b.orgid,b.orgcode FROM urc_organization b " +
                            "LEFT JOIN URC_USER_ORG_MAP a on b.orgid=a.orgid and b.compid=a.compid where b" +
                            ".compid=? and a.userid=? limit 1",
                    OrgInfo.class,new Object[]{compid,userid}
            );
            org.setAccflag(accflag);
            list.add(org);
        }
        return list;
    }



}
