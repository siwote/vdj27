package com.lehand.evaluation.lib.common.constant;

/**
 *	@author Tangtao
 *
 */
public class ResultSelfEvalSqlCfg {
	/**
	 * 自评列表
	 * SELECT
	 	*
		FROM
			(
				SELECT
					a.packageid,
					a.`year`,
		
				IF (
					b.`status` = 1
					AND b.remark1 = 1,
					3,
					b.`status`
				) `status`,
				a.`name`,
				a.enddate,
				a.startdate,
				a.score
			FROM
				el_assess a
			INNER JOIN el_assess_item_receive b ON a.id = b.assessid
			AND a.compid = b.compid
			WHERE
				date_format(now(), '%Y-%m-%d') >= str_to_date(a.startdate, '%Y-%m-%d')
			AND b.status = :status	AND a.compid = :compid AND b.code = :code
			) t
	 */
	public final static String ResultSelfEvalPageQuery="ResultSelfEvalPageQuery";
	/**
	 * 自评列表总条数
	 * SELECT COUNT(1) FROM ( SELECT  a.packageid FROM el_assess a INNER JOIN el_assess_item_receive b ON a.id = b.assessid AND a.compid = b.compid WHERE b.status = :status AND a.compid = :compid AND b.`code` = :code 
  		AND a.`name` LIKE :likeStr  GROUP BY a.packageid ) t
	 */
	public static final String ResultSelfEvalPageQueryCount = "ResultSelfEvalPageQueryCount";//
	//自评详情
	public final static String ResultSelfEvalDetail="ResultSelfEvalDetail";//

	/**
	 * 添加考核单位接收
	 * INSERT INTO el_evaluate_receive SET compid = :compid,packageid = :packageid,packagename = :packagename,`status` = :status,overdate = :overdate,
		assessscore = :assessscore,cycle = :cycle,code = :code,remark1 = :remark1,remark2 = :remark2,remark3 = :remark3,remark4 = :remark4
	 */
	public final static String ResultSelfEvalCreateReceive="ResultSelfEvalCreateReceive";//

	/**
	 * 	查询接收单位ID和NAME  去重
	 * SELECT DISTINCT a.`code`,b.`name`,b.score FROM `el_assess_evaluate` a LEFT JOIN `el_package` b ON a.packageid = b.id AND a.compid = b.compid 
	 * WHERE a.compid = ? AND b.id = ? AND a.type = 'org'
	 */
	public final static String ResultSelfEvalRevceiveInfo="ResultSelfEvalRevceiveInfo";//
	
	/**
	 * 	当前体系下的考核接收对象
	 * SELECT b.assessid FROM el_assess a 
	 * INNER JOIN el_assess_item_receive b ON a.id = b.assessid AND a.compid = b.compid 
	 * WHERE  a.packageid = ? AND  a.compid = ? AND b.`code` = ? 
	 */
	public static final String ResultSelfEvalSignGetAssIds = "ResultSelfEvalSignGetAssIds";

	/**
	 *	 考核体系签收
	 * UPDATE `el_assess_item_receive` SET `status` = : STATUS,receivedate = : receivedate 
	 * WHERE assessid = : assessid AND `code` = : CODE AND compid = : compid
	 */
	public final static String ResultSelfEvalSign="ResultSelfEvalSign";//
	
	//
	/**
	 * 	考核指标树第一层
	 *
	SELECT * FROM
		(
		SELECT
			a.id,
			a.name,
		a.score,
		a.orderid,
			b.id assessid,
		c.`code`
		FROM
			el_assess_item a
		INNER JOIN el_assess b ON a.compid = b.compid AND a.packageid = b.packageid
		INNER JOIN el_assess_item_receive c ON b.compid = c.compid AND b.id = c.assessid
		
		WHERE
			a.pid = 0
		AND a.type = 0
		AND b.packageid = ?
		AND c.`code` = ?
		AND a.compid = ?
		) t
		ORDER BY t.orderid ,t.id ASC
	 */
	public final static String SelfEvalSystemList="SelfEvalSystemList";//
	
	/**考核指标树第二层
	 * SELECT
	 		*
			FROM
				(
					SELECT
						a.id,
						a.`name`,
						a.score,
						(
							SELECT
								COUNT(DISTINCT(t.id))
							FROM
								el_assess_feedback t
							INNER JOIN el_assess_item s ON t.compid = s.compid
							AND t.itemid = s.id
							INNER JOIN el_assess_evaluate v ON s.compid = v.compid
							AND s.id = v.itemid
							WHERE
								s.pid = a.id
							AND t.`code` = :cobjcode
							AND v.`code` = :cscorecode
						) child,
						(
							SELECT
								COUNT(IF(b.`status` <> 2, 1, NULL))
							FROM
								el_assess_feedback b
							INNER JOIN el_assess_item c ON b.itemid = c.id
							AND b.compid = c.compid
							INNER JOIN el_assess_evaluate d ON c.id = d.itemid
							WHERE
								c.compid = a.compid
							AND c.pid = a.id
							AND b.`code` = :robjcode
							AND d.`code` = :rscorecode
						) unreport,
						(
							SELECT
								COUNT(IF(b.`status` = 0, 1, NULL))
							FROM
								el_assess_feedback b
							INNER JOIN el_assess_item c ON b.itemid = c.id
							AND b.compid = c.compid
							AND b.`code` = :sobjcode
							WHERE
								c.compid = a.compid
							AND c.pid = a.id
						) unscore
					FROM
						el_assess_item a
					WHERE
						a.pid = :pid
					AND a.compid = :compid
				) x
	
		评分是追加条件 where  x.child > 0 ,过滤无子指标的指标类别
	 */
	public final static String SelfEvalSystemListSecond="SelfEvalSystemListSecond";
	
	/**
	 * 考核指标底层列表
	 * SELECT
	 		*
		FROM
			(
				SELECT
					b.id,
					a.`name`,
					a.content,
					b.`status`,
					a.orderid,
					a.score,
					IFNULL(b.selfscore, "未自评") selfscore,
					(
						SELECT
							IFNULL(score, "未评分")
						FROM
							el_assess_score_record
						WHERE
							feedbackid = b.id
						AND compid = b.compid
					) othscore
				FROM
					el_assess_item a
				INNER JOIN el_assess_feedback b ON a.id = b.itemid
				AND a.compid = b.compid
				WHERE
					a.pid = ?
				AND b.compid = ?
				AND b. CODE = ?
			) t
		ORDER BY
			t.orderid,
			t.id ASC
	 */
	public final static String SelfEvalSystemListLast="SelfEvalSystemListLast";//

	/**
	 * 	保存考核自评分
	 * UPDATE el_assess_feedback SET `status` = 1, selfscore = ?,feedbackcontent = ?,modiler = ?,modiltime = ? ,files=?
	 * WHERE id = ? AND compid = ?
	 */
	public static final String ResultSelfSaveScore = "ResultSelfSaveScore";//
	
	/**
	 * 	 创建佐证材料文件夹
	 * 	INSERT INTO el_assess_feedback_file 
	 * SET compid = :compid,createtime = :createtime,
	 * userid = :userid,name = :name,
	 * feedbackid = :feedbackid,username =:username,remark3 = :remark3; 
	 */
	public static final String ResultSelfCreateForder = "ResultSelfCreateForder";
	//反馈详情 文件夹查询
	public static final String ResultSelfEvalForders = "ResultSelfEvalForders";//
	//反馈详情 文件列表
	public static final String ResultSelfEvalFiles = "ResultSelfEvalFiles";//
	//上级指标分数
	public static final String EvalScore = "EvalScore";//
	//当前指标分数
	public static final String CurrEvalScore = "CurrEvalScore";//
	//三级指标自评分数列表(算分)
	public static final String SelfEvalScoreList = "SelfEvalScoreList";//
	//三级评分列表(算分)
	public static final String EvalScoreList = "EvalScoreList";//
	
	/**
	 * 	评分详情
	 * SELECT a.id feedbackid, (SELECT id FROM el_assess_score_record WHERE feedbackid = a.id AND compid = a.compid) id,
	 * a.`code`,a.assessid,a.itemid,a.selfscore,a.feedbackcontent,(SELECT score FROM el_assess_score_record WHERE feedbackid = a.id AND compid = a.compid) score,
	 * (SELECT content FROM el_assess_score_record WHERE feedbackid = a.id AND compid = a.compid) content FROM el_assess_feedback a WHERE a.id = 5 AND a.compid = 1 
	 */
	public static final String ResultEvalDetail = "ResultEvalDetail";//
	/**
	 * 	文件关系表插入数据
	 * 	INSERT	 INTO  dl_file_business SET fileid = :id,businessid = :businessid,compid =:compid,intostatus = :intostatus,businessname = :businessname
	 */
	public static final String ResultInsertFile = "ResultInsertFile";
	
	/**
	 *	 自评列表状态数
	 *	
SELECT COUNT(IF(t.sta= 2,1,NULL)) reported,
COUNT(IF(t.sta= 1,1,NULL)) recived,
COUNT(IF(t.sta= 0,1,NULL)) unrecived 
FROM (SELECT  b.status sta FROM el_assess a 
INNER JOIN el_assess_item_receive b ON a.id = b.assessid AND a.compid = b.compid 
WHERE date_format(now(), '%Y-%m-%d') >= str_to_date(a.startdate, '%Y-%m-%d %H') AND a.compid = ? AND b.code = ? ) t


	 */
	public static final String ResultSelfGetSelStatus = "ResultSelfGetSelStatus";
	
	/**
	 * 自评上报修改接收表状态
	 * UPDATE el_assess_item_receive SET 
	 * `status` = ?,updatedate = ? WHERE compid = ? 
	 * AND id = ( SELECT t.id FROM( SELECT b.id FROM el_assess a 
	 * 		INNER JOIN el_assess_item_receive b ON a.id = b.assessid AND a.compid = b.compid WHERE a.compid = ? AND a.packageid = ? AND b.code = ?)t)
	 */
	public static final String ResultSelfEvalReportStatus = "ResultSelfEvalReportStatus";
	/**
	 * 根据体系id查询未自评指标数
	 * SELECT COUNT(IF(a.`status` <> 2 AND a.`status` <>1,1,NULL)) cou FROM `el_assess_feedback` a INNER JOIN el_assess b ON a.assessid = b.id AND a.compid = b.compid 
		INNER JOIN  el_assess_object c ON a.`code` = c.`code` AND c.assessid = b.id AND b.compid = c.compid
		WHERE b.packageid = ? AND a.compid = ? AND c.`code` = ? 
	 */
	public static final String ResultSelfEvalCheckUnScored = "ResultSelfEvalCheckUnScored";
	
	/**
	 * 查询当前体系状态:已签收,已上报
	 * SELECT IF( date_format(now(), '%Y-%m-%d') > str_to_date(b.enddate, '%Y-%m-%d %H'),4,a.`status`) `status` 
	 * FROM `el_assess_item_receive` a INNER JOIN el_assess b ON a.assessid = b.id AND a.compid = b.compid 
	 * WHERE b.packageid = ? AND a.compid = ? AND a.`code` = ?
	 */
	public static final String ResultSelfEvalGetReportStatus = "ResultSelfEvalGetReportStatus";
	
	/**
	 * 当前体系签收状态
	 * SELECT a.`status` FROM el_assess_item_receive a INNER JOIN el_assess b ON a.compid = b.compid AND a.assessid = b.id 
	 * WHERE a.compid = ? AND b.packageid = ? AND a.`code` =?
	 */
	public static final String ResultSelfEvalGetSignStatus = "ResultSelfEvalGetSignStatus";
	
	/**
	 * 	根据体系id查所有自评状态
	 * SELECT count(DISTINCT a.id) unselfscore FROM el_assess_item_receive a INNER JOIN el_assess b ON a.compid = b.compid AND a.assessid = b.id WHERE a.`status` <> 2 AND  a.compid = ? AND b.packageid =? 
	 */
	public static final String ResultSelfAllStatus = "ResultSelfAllStatus";
	
	/**
	 * 	修改 考核表状态为 4 :已全部自评
	 * 
	 *  UPDATE el_assess SET `status`  = 4 WHERE id =
			( SELECT t.id FROM (SELECT a.id  FROM el_assess a INNER JOIN el_assess_object b ON a.compid = b.compid AND a.id = b.assessid WHERE
			
			a.compid = ? AND a.packageid = ? AND b.`code` = ?)t)
	 */
	public static final String ResultSelfChangeAssaStatus = "ResultSelfChangeAssaStatus";
	
	/**
	 * 创建他评单位前根据体系id和他评单位组织机构id查询是否已存在体系id和组织机构id相同的接收表数据
	 * SELECT * FROM el_evaluate_receive WHERE packageid = :packageid AND `code` = :code AND compid = :compid
	 */
	public static final String CheckBeforeCreateReceive = "CheckBeforeCreateReceive";
	
	/**
	 * 清除文件关联表
	 * DELETE FROM dl_file_business WHERE businessname = 1 AND businessid IN ( SELECT id FROM el_assess_feedback_file WHERE feedbackid  = ? AND compid = ?)
	 */
	public static final String clearRelevantFiles = "clearRelevantFiles";//
	//清除关联文件夹
	public static final String clearRelevantFordes = "clearRelevantFordes";//
	/**
	 * 他评指标列表
	 * SELECT
			b.id,
			a.`name`,
			a.content,
			b.`status`,
			IFNULL(b.selfscore, "未自评") selfscore,
			(
				SELECT
					IFNULL(score, "未评分")
				FROM
					el_assess_score_record
				WHERE
					feedbackid = b.id
				AND compid = b.compid
			) othscore
		FROM
			el_assess_item a
		INNER JOIN el_assess_feedback b ON a.id = b.itemid
		AND a.compid = b.compid
		INNER JOIN  el_evaluate_receive c  ON a.packageid = c.packageid 
		AND a.compid = c.compid
		INNER JOIN el_assess_evaluate d ON c.`code` = d.`code` AND d.itemid = a.id
		WHERE
			a.pid = ?
		AND b.compid = ?
		AND b.`code` = ?
		AND c.`code` = ?
	 */
	public static final String EvalSystemListLast = "EvalSystemListLast";

	/**
	 * 他评类别
	 * 
		select * from (
			select d.id,d.name,d.pid ,d.orderid,? as code from el_assess_item d
			left join  el_assess_item c on c.pid = d.id 
			left join  el_assess_item a on a.pid = c.id
			left join el_assess_evaluate  b on b.itemid = a.id and b.compid = a.compid  
			where b.packageid = ? and b.code = ? and b.compid = ? 
			
			GROUP BY d.id
		) t ORDER BY t.orderid,t.id ASC
	 */
	public static final String EvalSystemList = "EvalSystemList";
	
	/**
	 * 增加自评状态 在remark1 字段
	 */
	public static final String ResultSelfEvalReportRemark = "ResultSelfEvalReportRemark";
	
	
	/**
	 * 通过被考核对象查询考核表信息
	 * SELECT b.* FROM `el_assess_item_receive` a  INNER JOIN el_assess b ON a.compid = b.compid AND a.assessid = b.id 

		WHERE a.compid = ? AND  a.`code` =  ? and b.packageid = ?
	 * 
	 */
	public static final String getAssessInfoByItemReciver = "getAssessInfoByItemReciver";
	
	public static String listDlfileByInIds = "listDlfileByInIds";
	
}
