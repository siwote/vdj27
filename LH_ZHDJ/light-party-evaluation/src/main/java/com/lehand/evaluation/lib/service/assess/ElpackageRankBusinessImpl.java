package com.lehand.evaluation.lib.service.assess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.business.ElpackageRankBusiness;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.util.NumUtil;

public class ElpackageRankBusinessImpl implements ElpackageRankBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 获取考核列表
	 * @Author dong
	 * @DateTime 2019年4月17日 下午3:23:00
	 * @return
	 */
	@Override
	public List<ElAssess> getAssess(Short yaer, Session session){
		List<ElAssess> list = generalSqlComponent.query(ComesqlElCode.getAssessRankType, new Object[] {session.getCurrentOrg().getOrgcode(),session.getCompid(),yaer});
		return list;
		
	}
	
	/**
	 * 
	 * @Title: orgRank
	 * @Description: 获取党组织考核结果排名
	 * @Author dong
	 * @DateTime 2019年4月17日 下午5:25:06
	 * @param assessid
	 * @param session
	 * @return
	 */
	@Override
	public List<Map<String,Object>> orgRank(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessid", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankOrgType, param);
		return list;
		
	}
	
	/**
	 * 
	 * @Title: itemRank
	 * @Description: 评分标准扣分次数结果排名
	 * @Author dong
	 * @DateTime 2019年4月19日 下午4:11:24
	 * @param assessid
	 * @return
	 */
	@Override
	public List<Map<String,Object>> itemRank(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType1, param);
		return list;
	}
	
	/**
	 * 
	 * @Title: itemRank2
	 * @Description: 指标扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月19日 下午5:37:55
	 * @param assessid
	 * @param session
	 * @return
	 */
	@Override
	public List<Map<String,Object>> itemRank2(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType2, param);
		return list;
	}
	
	/**
	 * 
	 * @Title: itemRank3
	 * @Description: 类别扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月22日 下午9:58:55
	 * @param assessid
	 * @return
	 */
	@Override
	public List<Map<String,Object>> itemRank3(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType3, param);
		return list;
	}
	

	public List<Map<String,Object>> itemRank4(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType4, param);
		return list;
	}
	
	public List<Map<String,Object>> itemRank5(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType5, param);
		return list;
	}
	
	public List<Map<String,Object>> itemRank6(String assessid,Session session){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType6, param);
		return list;
	}
	
	public List<Map<String,Object>> itemRanks(String assessid,Session session,Integer type){
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("assessids", assessid);
		param.put("compid", session.getCompid());
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.rankItemType + type, param);
		//过滤分数小于0的（只显示扣分的）
		if(type>=4) {
			for (Map<String, Object> map : list) {
				map.put("score", NumUtil.tonumber2(Double.valueOf(map.get("score").toString())));
			}
			list = list.stream().filter(s -> Double.valueOf(s.get("score").toString()) > 0).collect(Collectors.toList());
		}else {
			list = list.stream().filter(s -> Double.valueOf(s.get("num").toString()) > 0).collect(Collectors.toList());
		}
		return list;
	}

	/**
	 * 
	 * @Title: itemRank3
	 * @Description: 类别扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月22日 下午9:58:55
	 * @param assessid
	 * @return
	 */
	@Override
	public Map<String, Object> rank(String assessid, int type, Session session) {

//		if(StringUtils.isNullOrEmpty(assessid)) {
//			LehandException.throwException("请选择体系");
//		}
		//获取到的数据
		List<Map<String,Object>> data=null;
		//解析后返回的map
		Map<String,Object> result=new LinkedHashMap<String,Object>();
		//y轴的集合
		List<Object> name=new ArrayList<Object>();
		//y轴的悬浮显示集合
		List<Object> dzzqcename=new ArrayList<Object>();
		//x轴的集合
		List<Object> value=new ArrayList<Object>();
		//除了结果排名都是num
		String valueKey="num";
		//按考核类别扣分次数由多到少排列 type = 1  按考核指标扣分次数由多到少排列 type = 2  按评分标准扣分次数由多到少排列 type = 3
		//按考核类别扣分分值由高到低排列 type = 4  按考核指标扣分分值由高到低排列 type = 5  按评分标准扣分分值由高到低排列 type = 6
		switch (type) {
		case 0:
			//结果排名是score
			valueKey="score";
			//获取数据
			data=orgRank(assessid,session);
			break;
		default:
			data=itemRanks(assessid,session,type);
			break;
		}
		//解决单个体系查询就不显示体系名称
//		boolean flag = assessid.indexOf(",") > 0 ? true : false;
		//集合不为空时进行解析，name放进name集合，num\score放进value集合
		System.out.println("**********************************"+data);
		if(null!=data) {
			for (Map<String, Object> map : data) {
//				if(flag) {
					name.add(map.get("name")+"*"+map.get("packagename"));
//				}else {
//					name.add(map.get("name"));
//				}
				dzzqcename.add(map.get("dzzqc"));
				if(type<4) {
					//System.out.println(NumUtil.tonumber2(Double.valueOf(map.get(valueKey).toString())));
					map.put("score", NumUtil.tonumber2(Double.valueOf(map.get(valueKey).toString())));
					value.add(NumUtil.tonumber2(Double.valueOf(map.get(valueKey).toString())));
				}else {
					value.add(NumUtil.tonumber2(Double.valueOf(map.get("score").toString())));
				}
			}
			Collections.reverse(name);
			Collections.reverse(dzzqcename);
			Collections.reverse(value);
			//把name集合、value集合、data原始数据放进map并返回
			result.put("name", name);
			result.put("dzzqcename",dzzqcename);
			result.put("value", value);
			result.put("data", data);
		}
		return result;
	}
}
