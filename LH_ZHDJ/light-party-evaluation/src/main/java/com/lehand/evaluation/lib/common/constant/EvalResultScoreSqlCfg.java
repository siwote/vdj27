package com.lehand.evaluation.lib.common.constant;

public class EvalResultScoreSqlCfg {
	/**
	 * 获取考核对象列表
	 *SELECT
			a.`code`,
			a.`name`,
			b.packageid,
			(
				(
					SELECT
						COUNT(id)
					FROM
						el_assess_feedback
					WHERE
						assessid = a.assessid
						AND `code` = c.code
				)
					-
				(
					SELECT
						COUNT(DISTINCT(t.feedbackid))
					FROM
						el_assess_score_record t
						INNER JOIN el_assess_feedback s
						ON t.feedbackid = s.id  AND t.compid = s.compid 				
					WHERE				
						s.assessid = a.assessid   
						AND s.`code` = c.code
				) 
			) unscored
		FROM
			el_assess_object a
		INNER JOIN el_assess b ON a.assessid = b.id
		INNER JOIN el_evaluate_receive c ON c.packageid = b.packageid
		WHERE
		a.compid = b.compid = c.compid 
		AND	b.packageid = :packageid
		AND c.code = :orgid
		AND a.compid = :compid
	 */
	public final static String EvalResultScoreObjects="EvalResultScoreObjects";//

	/**
	 * 他评考核体系列表
	 *SELECT * FROM (SELECT DISTINCT a.packageid,IF(b.`status`=0 AND b.remark1 = 1,3,b.`status`) status ,a.`year`,a.`name`,a.enddate, a.startdate,a.score FROM el_assess a 
	 *INNER JOIN el_evaluate_receive b ON a.packageid = b.packageid AND a.compid = b.compid 
	 *WHERE  b.status = :status AND a.compid = :compid AND b.`code` = :code) t
	 */
	public final static String EvalResultScorePageQuery="EvalResultScorePageQuery";//
	//保存考核分数
	public static final String EvalResultScoreSaveScore = "EvalResultScoreSaveScore";//

	/**
	 *	 修改考核分数
	 * UPDATE el_assess_score_record SET score = :score,content = :content,usertor = :usertor 
	 *  WHERE id = :id AND compid = :compid AND feedbackid = :feedbackid
	 */
	public static final String EvalResultScoreUpdateScore = "EvalResultScoreUpdateScore";//	
	
	/**
	 * 保存核分数接收表
	 * UPDATE el_evaluate_receive SET status = 1,overdate = ? WHERE  compid = ? AND packageid = (SELECT packageid FROM el_assess WHERE id = ?)
	 */
	public static final String EvalResultScoreReportScoreRec = "EvalResultScoreReportScoreRec";//
	
//	/**
//	 * 上报考核分数反馈表
//	 * UPDATE el_assess_feedback SET status = 2,modiltime = ? WHERE compid = ? AND assessid IN (
//	SELECT id FROM el_assess WHERE packageid = ?)
//	 */
//	public static final String EvalResultScoreReportScoreRep = "EvalResultScoreReportScoreRep";//
//	
	/**
	 * 	考核状态
	 *SELECT
		COUNT(IF(t.sta = 1, 1, NULL)) reported,
		COUNT(IF(t.sta = 0, 1, NULL)) unreported
		FROM
			(
				SELECT
					b.`status` sta
				FROM
					el_assess a
				INNER JOIN el_evaluate_receive b ON a.packageid = b.packageid
				AND a.compid = b.compid
				WHERE
					a.compid = ?
				AND b. CODE = ?
			) t
	 */
	public static final String EvalResultScoreGetStatus = "EvalResultScoreGetStatus";
	
	
	/**
	 * 根据体系id查询是否存在未评分指标
	 *  
		SELECT COUNT(IF (b.status <>2, 1, NULL)) unscored FROM 	el_assess_item a
		INNER JOIN el_assess_feedback b ON a.id = b.itemid
		AND a.compid = b.compid
		INNER JOIN  el_evaluate_receive c  ON a.packageid = c.packageid 
		AND a.compid = c.compid
		INNER JOIN el_assess_evaluate d ON c.`code` = d.`code` AND d.itemid = a.id
		
		WHERE a.packageid = ? AND b.compid = ? and c.`code` = ? 
	 */
	public static final String EvalResultGetUnScoreNum = "EvalResultGetUnScoreNum";
	
	/**
	 * 根据体系id查询体系评分上报状态
	 * 	SELECT IF( date_format(now(), '%Y-%m-%d') > str_to_date(b.enddate, '%Y-%m-%d %H'),4,a.`status`) `status`
	 *  FROM `el_evaluate_receive` a INNER JOIN el_assess b ON a.packageid = b.packageid AND a.compid = b.compid  
	 *  WHERE a. packageid = ? AND b.compid = ? AND a.`code` = ?
	 */
	public static final String EvalResultGetReportStatus = "EvalResultGetReportStatus";
	
	/**
	 * 根据考核表id修改状态
	 * UPDATE el_assess_feedback SET `status` = ? ,selfscore = ?,feedbackcontent = ?,modiler = ?,modiltime = ?
	 *  WHERE id = ? AND compid = ?
	 */
	public static final String UpdateFeedBackStatusById = "UpdateFeedBackStatusById";
	/**
	 * 修改考核单位接收表状态
	 * UPDATE `el_evaluate_receive` SET `status` = 1 WHERE packageid = ? AND `code` = ? AND compid = ?
	 */
	public static final String UpdateEvalResultRecve = "UpdateEvalResultRecve";

	/**
	 * 通过体系id查询未提交评分的评分单位数
	 * SELECT COUNT(DISTINCT(a.id)) num FROM el_evaluate_receive a INNER JOIN el_assess b ON a.compid = b.compid AND a.packageid = b.packageid WHERE a.`status` <> 1 AND a.compid = ? AND b.packageid = ?
	 */
	public static final String getUnreportedNum ="getUnreportedNum";
	
}
