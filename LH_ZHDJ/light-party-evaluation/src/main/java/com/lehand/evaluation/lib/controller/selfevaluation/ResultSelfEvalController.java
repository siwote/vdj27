package com.lehand.evaluation.lib.controller.selfevaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.selfevaluation.ResultSelfEvalBusiness;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.evaluation.lib.controller.ElBaseController;
import com.mysql.jdbc.StringUtils;


/**
 *	@author Tangtao
 *	考核结果自评
 */
@Api(value = "考核结果自评", tags = { "考核结果自评" })
@RestController
@RequestMapping("/evaluation/resultselfeval")
public class ResultSelfEvalController extends ElBaseController{
	@Resource
	ResultSelfEvalBusiness selfEvalBiness;
	
	public final Logger logger = LogManager.getLogger(ElBaseController.class);
	/**
	 *	考核结果自评列表
	 *	@param pager
	 *	@param
	 *	@return
	 *	Message
	 *	2019年4月10日
	 *	Tangtao
	 */
	@RequestMapping("/page")
	public Message pageQuery(Pager pager,Short status,String Str) {
		
		Message msg = new Message();
		try {
			msg = selfEvalBiness.pageQuery(pager, Str,status,getSession());
		} catch (Exception e) {
			msg.fail("自评分页失败");
			logger.error("查询自评列表错误",e);
			e.printStackTrace();
		}
		return msg;		
	}
	
	/**
	 *	签收页面
	 *	@param
	 *	@return
	 *	Message
	 *	2019年4月11日
	 *	Tangtao
	 */
	@RequestMapping("/sign")
	public Message signForEvalSystem(Long packageid) {
		Message msg = new Message();
		try {
			msg = selfEvalBiness.signForEvalSystem(packageid, getSession());			
		} catch (Exception e) {
			logger.error("签收错误",e);
			e.printStackTrace();
		}
		return msg;		
	}
	
	/**
	 *	自评详情
	 *	@param id//反馈表id
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/formInfo")
	public Message formInfo(Long id) {
		Message msg = new Message();
		try {
			msg = selfEvalBiness.formInfo(id, getSession().getCompid(),getSession().getCurrentOrg().getOrgcode());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			msg.fail("获取详情失败");
			logger.error("获取详情失败",e);
			e.printStackTrace();
		}
		return msg;	
	}
	
	/**
	 *	保存考核评分
	 *	@param id
	 *	@param selfscore
	 *	@param feedbackcontent
	 *	@param itemid
	 *	@param code
	 *	@param
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/saveSelfEvalResult")
	public Message saveSelfEvalResult(Long id,String selfscore,String feedbackcontent,Long itemid,Long code,@RequestParam("list")String jsonStr,Long packageid,String files) {
		Message msg = new Message();
		List<Map<String,Object>> mlist = new ArrayList<Map<String,Object>>();
//		if(!NumUtil.checkNum(selfscore,1)) {
//			msg.fail("请输入正确的数字格式!");
//			return msg;
//		}
		if(StringUtils.isNullOrEmpty(feedbackcontent.trim())) {
			msg.fail("未填写自评说明!");
			return msg;
		}
		if(feedbackcontent.length()>500) {
			msg.fail("自评说明输入过长!");
			return msg;
		}
		if(!StringUtils.isNullOrEmpty(jsonStr)) {
			mlist =JSON.parseObject(jsonStr,new TypeReference<List<Map<String,Object>>>(){});
		}
		try {
			msg = selfEvalBiness.saveSelfEvalResult(id, Double.parseDouble(selfscore), feedbackcontent, itemid, code, mlist, getSession(),packageid,files);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			msg.fail(e.getMessage());
			logger.error("保存自评分异常",e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 *	上报自评
	 *	@param packageid
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/reportEval")
	public Message reportEval(Long packageid) {
		Message msg = new Message();
		try {
			msg = selfEvalBiness.reportSelfEval(packageid,getSession());
		} catch (Exception e) {
			msg.fail("上报自评异常");
			logger.error("上报自评异常",e);
			e.printStackTrace();
		}
		
		return msg;
	}
	
	/**
	 *	查询指标类别树
	 *	@param packageid
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/getEvalTree")
	public Message getSelfEvalSystemList(Long packageid,String type,Long orgid,Integer status) {
		Message msg = new Message();
		List<Map<String,Object>> list = null;
		try {
			if("hascode".equals(type)) {
				list  = selfEvalBiness.getSelfEvalSystemList(packageid, getSession().getCompid(),orgid,getSession().getUserid(),"hasCode");
			}else {
			    if(status!=null){
                    list  = selfEvalBiness.getSelfEvalSystemList(packageid, getSession().getCompid(),  orgid,
                            getSession().getUserid(),"noCode");
                }else{
                    list  = selfEvalBiness.getSelfEvalSystemList(packageid, getSession().getCompid(),  Long.valueOf(getSession().getCurrentOrg().getOrgcode()),getSession().getUserid(),"noCode");
                }
			}
			msg.success();
			msg.setData(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("查询指标类别树列表错误",e);
			msg.fail("查询指标类别列表树错误");
			e.printStackTrace();
		}
		return msg;	
	}
	
	/**
	 *
	 *	@param id
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/getEvalList")
	public Message getSelfEvalSystemListLast(Long id,String type,Long orgid,Integer status) {
		Message msg = new Message() ;
		try {
			List<Map<String,Object>> list =  new ArrayList<>();
			if("hascode".equals(type)) {//他评
				list = selfEvalBiness.getEvalSystemListLast(id,getSession().getCompid(),orgid,/*getSession().getOrganids().get(0).getOrgid()*/getSession().getUserid());
			}else {//自评
			    if(status==null){
                    list = selfEvalBiness.getSelfEvalSystemListLast(id,getSession().getCompid(),
                            Long.valueOf(getSession().getCurrentOrg().getOrgcode()),getSession().getUserid());
                }else{
                    list = selfEvalBiness.getSelfEvalSystemListLast(id,getSession().getCompid(),orgid,getSession().getUserid());
                }

			}
			msg.success();
			msg.setData(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("查询指标列表错误",e);
			msg.fail("查询指标列表错误");
			e.printStackTrace();
		}
		return msg;	
	}
	
	
	/**
	 *	自评状态
	 *	@return
	 *	Message
	 *	2019年4月17日
	 *	Tangtao
	 */
	@RequestMapping("/getSelfStatus")
	public Message getSelfStatus () {
		Message msg = new Message();
		try {
			Map<String,Object> map = selfEvalBiness.getSelStatus(getSession().getCompid(),Long.valueOf(getSession().getCurrentOrg().getOrgcode()));
			msg.success();
			msg.setData(map);
		} catch (Exception e) {
			logger.error("查询自评状态异常",e);
			msg.fail("查询自评状态异常");
			e.printStackTrace();
		}
		return msg;	
	}
}
