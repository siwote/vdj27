package com.lehand.evaluation.lib.service.assess;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.*;

/**
 * 
 * @ClassName: ElAssessFeedbackBusiness
 * @Description: 反馈表
 * @Author dong
 * @DateTime 2019年4月13日 下午5:03:11
 */
public class ElAssessFeedbackBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	/**
	 * 
	 * @Title: initFeedBackClass
	 * @Description: 新增反馈表
	 * @Author dong
	 * @DateTime 2019年4月15日 上午9:25:44
	 * @param feedback
	 * @param assess
	 * @param packag
	 * @param item
	 * @param object
	 * @param session
	 */
	public void initFeedBackClass(ElAssessFeedback feedback, ElAssess assess, ElPackage packag, ElAssessItem item, ElAssessObject object,
								  Session session) {
		feedback.setCompid(session.getCompid());
		feedback.setCode(object.getCode());
		feedback.setName(object.getName());
		feedback.setAssessid(assess.getId());
		feedback.setItemid(item.getId());
		feedback.setType(assess.getType());
		feedback.setStatus((short) 0);
		feedback.setFeedbackcontent(StringConstant.EMPTY);
		feedback.setSelfscore(0d);
		feedback.setStartdate(assess.getStartdate());
		feedback.setEnddate(assess.getEnddate());
		feedback.setCreater(session.getUserid());
		feedback.setModiler(session.getUserid());
		feedback.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		feedback.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		feedback.setRemark1(0);
		feedback.setRemark2(0);
		feedback.setRemark3(StringConstant.EMPTY);
		feedback.setRemark4(StringConstant.EMPTY);
		feedback.setRemark5(StringConstant.EMPTY);
		feedback.setRemark6(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addFeedbackType, feedback);
	}


}
