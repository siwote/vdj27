package com.lehand.evaluation.lib.service.summary;

import java.util.List;
import java.util.Map;


import cn.afterturn.easypoi.excel.annotation.Excel;

@SuppressWarnings("serial")
public class SummInfoDto implements java.io.Serializable {
	
	@Excel(name = "类别",mergeVertical = true,  width = 30, isImportField = "true_st")
	private String typeName;
		
	@Excel(name = "序号",mergeVertical = true,  width = 30, isImportField = "true_st")
	private String number;
		
	@Excel(name =  "指标",mergeVertical = true,  width = 30, isImportField = "true_st")
	private String itemTotalName;
	
	@Excel(name =  "分值",mergeVertical = true, mergeRely = {2},width = 30, isImportField = "true_st")
	private String itemTotalScore;
	
	@Excel(name =  "标准", width = 30, isImportField = "true_st")
	private String name;
			
	@Excel(name =  "自查情况")
	private String feedbackcontent;
	
	@Excel(name =  "资料档案",width = 80, isImportField = "true_st")
	private String filenamesforexp;
	
	//资料
	private List<Map<String,Object>> filenames;
	
	@Excel(name =  "自评分", width = 30, isImportField = "true_st")
	private String selfscore;
	
	@Excel(name =  "评分", width = 30, isImportField = "true_st")
	private String score;
	
//	@ExcelProperty(value = "评分说明",index = 0)
	private String content;
	
	//类别id
	private String lvOne;
	
	//指标id
	private String lvTwo;

	//附件（2021-04-26新需求）
	private List<Map<String,Object>> listFiles;

    public List<Map<String, Object>> getListFiles() {
        return listFiles;
    }

    public void setListFiles(List<Map<String, Object>> listFiles) {
        this.listFiles = listFiles;
    }

    public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getItemTotalName() {
		return itemTotalName;
	}

	public void setItemTotalName(String itemTotalName) {
		this.itemTotalName = itemTotalName;
	}

	public String getItemTotalScore() {
		return itemTotalScore;
	}

	public void setItemTotalScore(String itemTotalScore) {
		this.itemTotalScore = itemTotalScore;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFeedbackcontent() {
		return feedbackcontent;
	}

	public void setFeedbackcontent(String feedbackcontent) {
		this.feedbackcontent = feedbackcontent;
	}

	public List<Map<String, Object>> getFilenames() {
		return filenames;
	}

	public void setFilenames(List<Map<String, Object>> filenames) {
		this.filenames = filenames;
	}

	
	public String getFilenamesforexp() {
		return filenamesforexp;
	}

	public void setFilenamesforexp(String filenamesforexp) {
		this.filenamesforexp = filenamesforexp;
	}

	public String getSelfscore() {
		return selfscore;
	}

	public void setSelfscore(String selfscore) {
		this.selfscore = selfscore;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLvOne() {
		return lvOne;
	}

	public void setLvOne(String lvOne) {
		this.lvOne = lvOne;
	}

	public String getLvTwo() {
		return lvTwo;
	}

	public void setLvTwo(String lvTwo) {
		this.lvTwo = lvTwo;
	}
	
		
}
