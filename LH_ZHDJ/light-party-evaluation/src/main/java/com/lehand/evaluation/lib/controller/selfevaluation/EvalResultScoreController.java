package com.lehand.evaluation.lib.controller.selfevaluation;

import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.selfevaluation.EvalResultScoreBusiness;
import com.lehand.evaluation.lib.dto.AssessmentScoreDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.evaluation.lib.controller.ElBaseController;
import com.mysql.jdbc.StringUtils;

/**
 *	@author Tangtao
 *	考核结果他评
 */
@RestController
@RequestMapping("/evaluation/resulteval")
public class EvalResultScoreController extends ElBaseController {
	
	public final Logger logger = LogManager.getLogger(ElBaseController.class);
	
	@Resource
	EvalResultScoreBusiness evalResultScoreBiness;
	
	/**
	 *	获取考核对象列表
	 *	@param packageid
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/getEvalObjectList")
	public Message getEvalObjectList(Long packageid,String str) {
		Message msg = new Message();
		try {
			msg = evalResultScoreBiness.getEvalObjectList(packageid,str, getSession().getCompid(),/*getSession().getOrganids().get(0).getOrgid()*/getSession().getUserid());
		} catch (Exception e) {
			msg.fail("获取考核对象失败");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 *	他评列表
	 *	@param pager
	 *	@param str
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/pageQuery")
	public Message pageQuery(Pager pager,Short status,String str) {
		Message msg = new Message();
		try {
			msg = evalResultScoreBiness.pageQuery(pager, str,status, getSession());
		} catch (Exception e) {
			msg.fail("获取他评列表失败");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 *	保存他评
	 *	@param feedBack
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/saveScore")
	public Message saveScore(AssessmentScoreDto feedBack) {
		Message msg = new Message();
		if(feedBack.getCode()==null) {
			feedBack.setCode((long) 0);
		}
		if(StringUtils.isNullOrEmpty(feedBack.getContent().trim())) {
			msg.fail("未填写评分结果说明!");
			return msg;
		}
		if(feedBack.getContent().length()>500) {
			msg.fail("评分结果说明输入过长!");
			return msg;
		}
//		if(!NumUtil.checkNum(feedBack.getScore(),1)) {
//			msg.fail("请输入正确的数字格式!");
//			return msg;
//		}
		try {
			msg = evalResultScoreBiness.saveScore(feedBack, getSession().getCompid(), getSession().getUserid()/*getSession().getOrganids().get(0).getOrgid()*/);
		} catch (Exception e) {
			msg.fail("保存他评失败");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 *	评分指标详情
	 *	@param id
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/getEvalPerforInfo")
	public Message getEvalPerforInfo(Long id) {
		Message msg = new Message();
		if(id==null) {
			msg.fail("参数异常");
			return msg;
		}
		try {
			msg = evalResultScoreBiness.getEvalPerforInfo(id, getSession().getCompid());
		} catch (Exception e) {
			msg.fail("获取评分指标详情失败");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	
	/**
	 *	上报他评
	 *	@param packageid
	 *	@param
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/reportEval")
	public Message reportEval(Long packageid) {
		Message msg = new Message();
		if(packageid==null) {
			msg.fail("参数异常");
			return msg;
		}
		try {
			msg = evalResultScoreBiness.reportEval(packageid, getSession());
		} catch (Exception e) {
			msg.fail("上报他评失败");
			logger.error("上报他评失败",e);
			e.printStackTrace();
		}
		return msg;
	}

	
	/**
	 *	他评状态数目
	 *	@return
	 *	Message
	 *	2019年4月17日
	 *	Tangtao
	 */
	@RequestMapping("/getEvalStatus")
	public Message getEvalStatus() {
		Message msg = new Message();
		try {
			Map<String,Object> map = evalResultScoreBiness.getStatus(getSession().getCompid(),/*getSession().getOrganids().get(0).getOrgid()*/getSession().getUserid());
			msg.success();
			msg.setData(map);
		} catch (Exception e) {
			msg.fail("获取他评状态失败");
			logger.error("获取他评状态失败",e);
			e.printStackTrace();
		}
		return msg;
	}
}
