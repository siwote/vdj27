package com.lehand.evaluation.lib.business;

import java.util.List;
import java.util.Map;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessEvaluateCandidate;
import com.lehand.evaluation.lib.pojo.ElAssessItem;
import com.lehand.evaluation.lib.pojo.ElAssessObject;

public interface ElPackageBusiness {
	
	/**
	 * 保存考核体系
	 * @param
	 * @return 
	 * @throws LehandException
	 */
	public List<Map<String,Object>> savePackage(String assess, String object, String evaluate, String type, String oldassessid, Session session) ;
	
	/**
	 * 
	 * @Title: saveItem
	 * @Description: 保存类
	 * @Author dong
	 * @DateTime 2019年4月12日 上午11:10:21
	 * @param
	 * @param session
	 */
	public void saveItem(ElAssessItem item, Session session);
	
	/**
	 * 
	 * @Title: saveItem
	 * @Description: 保存类
	 * @Author dong
	 * @DateTime 2019年4月12日 上午11:10:21
	 * @param
	 * @param session
	 * @return 
	 */
	public void updateItem(ElAssessItem item,Session session);
	
	/**
	 * 
	 * @Title: saveItem2
	 * @Description: 保存指标
	 * @Author dong
	 * @DateTime 2019年4月12日 下午8:26:30
	 * @param item
	 * @param
	 * @param session
	 */
//	public void saveItemTarget(String item,String names,Session session,String evaluate,String evaluatetype,Short rule);
	public Message saveItemTarget(String item,Session session,String evaluate,String evaluatetype,Short rule);
	
	/**
	 * 
	 * @Title: updateItemTarget
	 * @Description: 修改指标
	 * @Author dong
	 * @DateTime 2019年4月13日 上午11:34:42
	 * @param
	 * @param
	 * @param
	 * @param
	 * @param session
	 */
	public Message updateItemTarget(String data /**ElAssessItem item,String evaluate,String evaluatetype,Short rule*/,Session session);
	
	
	/**
	 * 
	 * @Title: releaseAssess
	 * @Description: 发布考核
	 * @Author dong
	 * @DateTime 2019年4月12日 上午11:33:40
	 * @param assessid
	 * @param session
	 */
	public void releaseAssess(long assessid,Session session);


	/**
	 * 
	 * @Title: queryAssess
	 * @Description: 查询考核列表
	 * @Author dong
	 * @DateTime 2019年4月15日 上午9:34:40
	 * @param session
	 * @return
	 */
	public Pager queryAssess(Pager pager,String likeStr,Session session);
	
	
	/**
	 * 根据考核id查询
	 * @Title: getAssess
	 * @Description: 
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:03:04
	 * @return
	 */
	public ElAssess getAssess(long assessid, Session session);
	
	/**
	 * 
	 * @Title: getAssessObject
	 * @Description: 根据体系id获取考核对象
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:12:18
	 * @param
	 * @return
	 */
	public List<ElAssessObject> getAssessObjectBy(long assessid, Session session);
	
	/**
	 * 
	 * @Title: getAssessEvaluate
	 * @Description: 根据体系id获取评分者候选数据
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:26:08
	 * @param packageid
	 * @return
	 */
	public List<ElAssessEvaluateCandidate> getAssessEvaluateCandidate(long packageid, Session session);
	
	
	/**
	 * 修改考核体系
	 * @param
	 * @return 
	 * @throws LehandException
	 */
	public List<Map<String,Object>> updatePackage(String assess,String object,String evaluate,String type,long packageid,Session session) ;
	
	/**
	 * 删除考核体系
	 * @Title: deletePackage
	 * @Description: 
	 * @Author dong
	 * @DateTime 2019年4月15日 下午1:48:59
	 * @param packageid
	 * @param session
	 * @return
	 */
	public long deletePackage(long packageid,Session session);
	
	/**
	 * 
	 * @Title: copyAssess
	 * @Description: 复制考核体系
	 * @Author dong
	 * @DateTime 2019年4月15日 下午3:05:36
	 * @param
	 * @param session
	 * @return
	 */
	public long copyAssess(long oldassessid,long newpackageid,Session session);
	
	/**
	 * 
	 * @Title: itemTree
	 * @Description: 类别树
	 * @Author dong
	 * @DateTime 2019年4月16日 上午9:32:24
	 * @param
	 * @param session
	 * @return
	 */
	public List<Map<String, Object>> itemTree(long packageid,Session session);
	
	/**
	 * 
	 * @Title: deleteItem
	 * @Description: 删除类别和指标
	 * @Author dong
	 * @DateTime 2019年4月16日 上午11:55:59
	 * @param
	 * @param session
	 */
	public void deleteItem(long itemid,Short type,Session session);
	
	/**
	 * 
	 * @Title: getItem
	 * @Description: 根据pid获取指标列表
	 * @Author dong
	 * @DateTime 2019年4月16日 下午2:19:56
	 * @param pid
	 * @param session
	 * @return
	 */
	public Pager getItem(Pager pager,long pid,Session session);
	
	/**
	 * 
	 * @Title: revokeAssess
	 * @Description: 撤销考核
	 * @Author dong
	 * @DateTime 2019年4月16日 下午9:03:54
	 * @return
	 */
	public void revokeAssess(long packageid,Session session);
	
	/**
	 * 
	 * @Title: move
	 * @Description: 类别指标拖拽
	 * @Author dong
	 * @DateTime 2019年4月23日 上午10:45:47
	 * @param srcid
	 * @param tagid
	 * @param flag
	 * @param session
	 */
	public void move(long srcid,long tagid,long flag,Session session);
	
}
