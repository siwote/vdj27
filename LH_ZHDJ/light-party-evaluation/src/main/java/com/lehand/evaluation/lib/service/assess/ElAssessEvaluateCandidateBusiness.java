package com.lehand.evaluation.lib.service.assess;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessEvaluateCandidate;

/**
 * 
 * @ClassName: ElAssessEvaluateCandidateBusiness
 * @Description: 评分者候选
 * @Author dong
 * @DateTime 2019年4月12日 下午5:58:01
 */
public class ElAssessEvaluateCandidateBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	public void initEvaluateCandidateClass(ElAssess elAssess, ElAssessEvaluateCandidate evaluate, String type, Session session) {
		evaluate.setCompid(session.getCompid());
		evaluate.setPackageid(elAssess.getPackageid());
		evaluate.setType(type);
		evaluate.setRemark1(0);
		evaluate.setRemark2(0);
		evaluate.setRemark3(StringConstant.EMPTY);
		evaluate.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addEvaluateCandidateType, evaluate);
	}
	
	/**
	 * 
	 * @Title: copyEvaluateCandidateClass
	 * @Description: 复制评分者候选表
	 * @Author dong
	 * @DateTime 2019年4月15日 下午3:56:17
	 * @param packageid
	 * @param evaluate
	 * @param session
	 */
	public void copyEvaluateCandidateClass(long packageid, ElAssessEvaluateCandidate evaluate,Session session) {
		evaluate.setCompid(session.getCompid());
		evaluate.setPackageid(packageid);
		generalSqlComponent.insert(ComesqlElCode.addEvaluateCandidateType, evaluate);
	}
	
	//判断评分候选者是否可删除
	
	public void EvaluateCandidate() {
		
	}

}
