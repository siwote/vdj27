package com.lehand.evaluation.lib.business;

import java.util.List;
import java.util.Map;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;

/**
 * 
 * @ClassName: ElPackagePublishBusiness
 * @Description: 考核体系公布
 * @Author dong
 * @DateTime 2019年4月15日 下午5:22:10
 */
public interface ElPackagePublishBusiness {

	/**
	 * 
	 * @Title: queryAsssessNot
	 * @Description: 考核结果公布列表
	 * @Author dong
	 * @DateTime 2019年4月15日 下午5:23:07
	 * @return
	 */
	public Pager queryAsssess(Pager pager, String likeStr, long status, Session session);
	
	/**
	 * 
	 * @Title: queryAsssessCount
	 * @Description: 考核结果公布列表数量
	 * @Author dong
	 * @DateTime 2019年4月23日 上午8:50:19
	 * @param
	 * @param
	 * @param
	 * @param session
	 * @return
	 */
	public List<Map<String,Object>> queryAsssessCount(Session session);
	
	
	
	/**
	 * 
	 * @Title: queryObject
	 * @Description: 获取考核对象及得分
	 * @Author dong
	 * @DateTime 2019年4月15日 下午9:58:36
	 * @param pager
	 * @param assessid
	 * @param session
	 * @return
	 */
	public  Pager queryObject(Pager pager,long assessid,String oneself,Session session);
	
	/**
	 * 
	 * @Title: queryItemScore
	 * @Description: 获取指标及自评分、他评分
	 * @Author dong
	 * @DateTime 2019年4月16日 下午3:20:00
	 * @param pager
	 * @param pid
	 * @param session
	 * @return
	 */
	public Pager queryItemScore(Pager pager,long pid,long objid,long code,Session session);
	
	/**
	 * 公布
	 * @Title: publish
	 * @Description: 
	 * @Author dong
	 * @DateTime 2019年4月16日 下午9:53:24
	 * @param assessid
	 * @param
	 * @param sessionid
	 */
	public void publish(long assessid,int remark1,Session sessionid);



	/**
	 * 
	 * @Title: getEvaluate
	 * @Description: 获取各评分单位对被考核对象的评分和自评分
	 * @Author dong
	 * @DateTime 2019年4月18日 下午6:01:58
	 * @return
	 */
	public List<Map<String,Object>> getEvaluate(long assessid,long code,Session session);
	
	
	/**
	 * 考核公布--树
	 * @param orgid
	 * @param packageid
	 * @return
	 */
	public List<Map<String,Object>> getTree(long orgid,long packageid,Session session);

	public void modiOtherScore(Session session, Long feedbackid,String content,Double score,String fileids);
}
