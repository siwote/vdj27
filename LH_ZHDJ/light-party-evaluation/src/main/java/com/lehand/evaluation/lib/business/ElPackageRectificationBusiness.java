package com.lehand.evaluation.lib.business;

import com.lehand.base.common.Message;
import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackAudit;
import com.lehand.evaluation.lib.pojo.ElPackageRectification;
import com.lehand.evaluation.lib.pojo.ElPackageRectificationAudit;
import com.lehand.evaluation.lib.pojo.ElPackageRectificationFeedback;

import java.util.List;
import java.util.Map;

public interface ElPackageRectificationBusiness {

    /**
     * 获取整改信息
     * @param packageid
     * @param session
     * @return
     */
    public ElPackageRectification getElPackageRectification(Long packageid,Long subjectid, Session session);

    /**
     * 发送整改
     * @param packageid
     * @param endtime
     * @param content
     * @param files
     * @param session
     */
    public void save(Long packageid,Long subjectid, String endtime,String content,String files, Session session);

    /**
     * 查询考核体系
     * @param year
     * @param session
     * @return
     */
    public List<ElAssess> listElPackageRectification(String year, Session session);

    /**
     * 整改反馈
     * @param packageid
     * @param files
     * @param content
     * @param subjectid
     * @param session
     */
    public void saveFeedBack(Long packageid, String files,String content,Long subjectid, Session session);

    /**
     * 获取反馈信息
     * @param packageid
     * @param subjectid
     * @param session
     * @return
     */
    public ElPackageRectificationFeedback getFeedback(Long packageid, Long subjectid, Session session);

    /**
     * 整改反馈审核
     * @param packageid
     * @param subjectid
     * @param status
     * @param result
     * @param remark
     * @param session
     */
    public void audit(Long packageid, Long subjectid, Byte status, Byte result, String remark, Session session);

    /**
     * 获取审核记录
     * @param packageid
     * @param subjectid
     * @param session
     */
    public List<ElPackageRectificationAudit> getAudit(Long packageid, Long subjectid, Session session);

    public List<Map<String,Object>> getEvalObjectList(Long packageid, String str,Integer flag, Session session);
}
