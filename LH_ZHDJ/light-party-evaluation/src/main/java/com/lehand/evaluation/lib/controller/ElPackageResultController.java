package com.lehand.evaluation.lib.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.ElPackageResultBusiness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;

@RestController
@RequestMapping("/evaluation/result")
public class ElPackageResultController   extends ElBaseController{
	private static final Logger logger = LogManager.getLogger(ElPackageController.class);
	@Resource
	ElPackageResultBusiness elPackageResultBusiness;
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 获取考核列表 考核者 和 被考核者
	 * @Author dong
	 * @DateTime 2019年4月16日 下午8:00:59
	 * @return
	 */
	@RequestMapping("/assess")
	private Message getAssess(Short year,String likeStr) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = elPackageResultBusiness.getAssess(year,likeStr,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		
		return message;
		
	}
	
	/**
	 * 
	 * @Title: getItemScore
	 * @Description: 指标 自评分  他评分
	 * @Author dong
	 * @DateTime 2019年4月16日 下午8:32:30
	 * @param
	 * @param pid
	 * @param
	 * @return
	 */
	@RequestMapping("/item")
	private Message queryItemScore(Pager pager,long objid,long pid) {
		Message message = new Message();
		try {
			elPackageResultBusiness.queryItemScore(pager,pid,objid,getSession());
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
//	@RequestMapping("/item")
//	private Message getItemScore(Pager pager,long assessid,long objId,long pid,String likeStr) {
//		Message message = new Message();
//		try {
//			elPackageResultBusiness.getItemScore(pager,assessid,objId,pid, likeStr,getSession());
//			message.success().setData(pager);
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		
//		return message;
//	}

}
