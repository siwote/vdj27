package com.lehand.evaluation.lib.service.message;

import javax.annotation.Resource;

import com.alibaba.druid.util.StringUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.Constant;
import com.lehand.evaluation.lib.dto.AccountInfoDto;
import com.lehand.evaluation.lib.pojo.message.TkTaskRemindList;

public class MessageBusinessImp {
	
	/**
	 * 插入消息(已有sql,直接调用)
	 */
	private final String addTTRLByTuihui = "addTTRLByTuihui";
	
	
	/**
	 * 根据组织机构id查询个人账号信息
	 * SELECT a.userid,b.username,a.organid,c.orgname,b.accflag FROM pw_user_organ_map a INNER JOIN pw_user b ON a.compid = b.compid AND a.userid = b.id 

		INNER JOIN  pw_organ c ON a.compid = c.compid AND a.organid = c.id  WHERE b.accflag = 1
		
		
		AND a.organid = ? AND a.compid = ?
	 */
	private final String getAccountByOrgid ="getAccountByOrgid"; 
	
	
	/**
	 * select a.id,a.username from pw_user a where a.id=? and a.compid=?
	 */
	private final String getAccountByUserid ="getAccountByUserid";
	
	@Resource GeneralSqlComponent sqlComponent;	
	
	public void insert(Session session, int module, Long mid, String rulecode, String rulename, String mesg, Long subjectid, String subjectname, String remindTime) {
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(session.getCompid());
		ls.setNoteid(-1L);
		ls.setModule(module);//模块(0:任务发布,1:任务反馈,4考核体系发布,5考核反馈)
		ls.setMid(mid);//对应模块的ID
		ls.setRemind(StringUtils.isEmpty(remindTime)?DateEnum.YYYYMMDDHHMMDD.format():remindTime);//提醒日期
		ls.setRulecode(rulecode);//编码
		ls.setRulename(rulename);
		ls.setOptuserid(session.getUserid());
		ls.setOptusername(session.getUsername());
		ls.setUserid(subjectid);//被提醒人
		ls.setUsername(subjectname);//
		ls.setIsread(0);//
		ls.setIshandle(0);//
		ls.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
		ls.setMesg(mesg);//消息
		ls.setRemarks1(1);//备注1
		ls.setRemarks2(0);//备注2
		ls.setRemarks3("考核管理");//备注3
		ls.setRemarks4(Constant.EMPTY);//备注3
		ls.setRemarks5(Constant.EMPTY);//备注3
		ls.setRemarks6(Constant.EMPTY);//备注3
		sqlComponent.insert(addTTRLByTuihui, ls);
	}
	
	
	
	/**
	 *	通过组织机构id查询账号信息
	 *	@return
	 *	AccountInfoDto
	 *	2019年4月26日
	 *	Tangtao
	 */
	public AccountInfoDto getAccountByOrgid(Long orgid, Long compid) {
		AccountInfoDto info = new AccountInfoDto();
		if(orgid==null) {
			return info;
		}
		info = sqlComponent.query(getAccountByOrgid, new Object[] {"0"+orgid,compid});
		return info;	
	}
	
	/**
	 * Description: 通过
	 * @author PT  
	 * @date 2020年4月29日 
	 * @param
	 * @param compid
	 * @return
	 */
	public AccountInfoDto getAccountByUserid(Long userid, Long compid) {
		AccountInfoDto info = new AccountInfoDto();
		if(userid==null) {
			return info;
		}
		info = sqlComponent.query(getAccountByUserid, new Object[] {userid,compid});
		return info;	
	}
}
