package com.lehand.evaluation.lib.pojo;

import com.lehand.module.urc.pojo.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="用户基本信息",description="用户基本信息")
public class UrcUser extends User {
   

	private Integer accflag;

    public Integer getAccflag() {
        return accflag;
    }

    public void setAccflag(Integer accflag) {
        this.accflag = accflag;
    }
}