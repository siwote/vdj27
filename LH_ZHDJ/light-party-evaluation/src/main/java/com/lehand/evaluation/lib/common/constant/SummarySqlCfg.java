package com.lehand.evaluation.lib.common.constant;

public class SummarySqlCfg {
	/**
	 * 查询所有考核体系
	 *	select * from el_assess where `year` = ? and ( status= 2 or status =3 ) and compid  = ?
	 */
	public static final String EvaGetAllPackage = "EvaGetAllPackage";
	
	/**
	 * 根据体系id和考核对象code查询类别
	 * SELECT id,name,packageid,score FROM el_assess_item 
	 * WHERE type = 0 AND pid = 0  AND compid = ? AND packageid = ? 
	 * ORDER BY orderid ASC
	 */
	public static final String EvaGetCategory = "EvaGetCategory";

	/**
	 * 	SELECT id,name,score FROM  el_assess_item WHERE   compid = ? AND pid = ? ORDER BY orderid ASC 
	 */
	public static final String EvaGetItemOne = "EvaGetItemOne";

	/**
	 * SELECT * FROM (SELECT a.id,a.feedbackcontent,a.selfscore,b.score,c.`name`,c.orderid FROM el_assess_feedback a INNER JOIN el_assess_score_record b ON a.id = b.feedbackid INNER JOIN el_assess_item c ON a.itemid = c.id  WHERE	a. STATUS = 2 AND c.pid = ? AND a.compid = b.compid = c.compid = ? AND `code`= ?) t ORDER BY t.orderid
		) t
	ORDER BY
		t.orderid
	 */
	public static final String EvaGetItemTwo = "EvaGetItemTwo";

	/**
	 * 	考核对象列表
	 * SELECT
		a.`code`,
		a.`name`,
		b.packageid
		FROM
			el_assess_object a
		INNER JOIN el_assess b ON a.assessid = b.id
		INNER JOIN el_evaluate_receive c ON c.packageid = b.packageid
		WHERE
		a.compid = b.compid = c.compid
		AND	b.packageid = ?
		AND a.compid = ? GROUP BY a.`code`
	 */
	public static final String EvaGetObjects = "EvaGetObjects";
	
	
	/**
	 * 新增文件<P/>
	 * insert into dl_file (compid,name,dir,size,type,usertor,remark,createtime,remark1,remark2,remark3,remark4,remark5,remark6) 
	 * values (:compid,:name,:dir,:size,:type,:usertor,:remark,:createtime,:remark1,:remark2,:remark3,:remark4,:remark5,:remark6)
	 */
	public final static String addDlFile="addDlFile";
	

}
