package com.lehand.evaluation.lib.pojo;

import java.util.List;
import java.util.Map;

/**
 * 整改反馈
 * @author Administrator
 *
 */
public class ElPackageRectificationFeedback {

	private Long packageid;

    private Long compid;

    private String content;

    private String code;

    private String files;

    private List<Map<String, Object>> fileLists;

	public Long getPackageid() {
		return packageid;
	}

	public void setPackageid(Long packageid) {
		this.packageid = packageid;
	}

	public Long getCompid() {
		return compid;
	}

	public void setCompid(Long compid) {
		this.compid = compid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

    public List<Map<String, Object>> getFileLists() {
        return fileLists;
    }

    public void setFileLists(List<Map<String, Object>> fileLists) {
        this.fileLists = fileLists;
    }
}