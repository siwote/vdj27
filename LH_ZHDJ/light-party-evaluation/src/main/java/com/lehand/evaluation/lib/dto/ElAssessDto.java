package com.lehand.evaluation.lib.dto;

import com.lehand.evaluation.lib.pojo.ElAssess;

public class ElAssessDto extends ElAssess {

    private String orgcode;

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }
}
