package com.lehand.evaluation.lib.service.selfevaluation;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.business.selfevaluation.ResultSelfEvalBusiness;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.common.constant.ResultSelfEvalSqlCfg;
import com.lehand.evaluation.lib.common.modulemethod.FilePreviewAndDownload;
import com.lehand.evaluation.lib.dto.OrgInfo;
import com.lehand.evaluation.lib.dto.SelfAssessmentScoreDto;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackAudit;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackFile;
import com.lehand.evaluation.lib.pojo.ElEvaluateReceive;
import com.lehand.evaluation.lib.pojo.ElPackage;
import com.lehand.evaluation.lib.pojo.UrcUser;
import com.lehand.evaluation.lib.service.assess.ElPackagesBusiness;
import com.lehand.evaluation.lib.service.message.MessageBusinessImp;
import com.lehand.module.urc.service.RoleService;
import com.lehand.module.urc.service.UserService;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultSelfEvalBusinessImpl implements ResultSelfEvalBusiness {

    @Resource
    GeneralSqlComponent sqlComponent;
    @Resource
    ElAssessFeedbackAuditBusinessImpl elAssessFeedbackAuditBusinessImpl;
    @Resource
    FilePreviewAndDownload filePreviewAndDownload;

    @Override
    public Message pageQuery(Pager pager, String str, Short status, Session session) throws Exception {
        Message msg = new Message();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("compid", session.getCompid());
        map.put("code", session.getCurrentOrg().getOrgcode());
        map.put("status", status);
        map.put("likeStr", str);
        if(status==3){
            selfAssessmentPage(pager, map,session);
        }else{
            pager = sqlComponent.pageQuery(ResultSelfEvalSqlCfg.ResultSelfEvalPageQuery, map, pager);
        }
        msg.success();
        msg.setData(pager);
        return msg;
    }

    private void selfAssessmentPage(Pager pager, Map<String, Object> params,Session session) {
        /**
         *  这边审核列表查询逻辑如下：
         *  首先被考核单位是否有账号在考核自评角色中如果有就是这些账号去审核（并且书记不会收到审核数据）
         *  如果没有就去找被考核单位的书记审核
         *  如果书记也没找到就不用管了（这种情况和项目经理沟通过了基本上不会出现不用考虑）
         */

        //1.获取当前登录人的userid
        Long userid = session.getUserid();
        //2.通过userid获取到登录人的身份证号码并判断当前登录账号是组织账号还是个人账号
        UrcUser user = sqlComponent.getDbComponent().getBean("select * from urc_user where compid=? and userid=?",
                UrcUser.class,new Object[]{session.getCompid(),userid});
        String idno = user.getIdno();
        Integer accflag = user.getAccflag();
        //个人账号登录需要判断下是否是组织书记
        if(accflag.equals(0)){
            //3.通过身份证号查询该人的班子成员信息(同一个人有可能是党支部书记同时又是上级党组织书记)
            List<Map<String,Object>> list = sqlComponent.getDbComponent().listMap(
                    "SELECT * FROM t_dzz_bzcyxx where zjhm=? and zfbz=0 and dnzwname in (3100000009,4100000013," +
                            "5100000017) ",new Object[]{idno}
            );
            //4.班子成员中查询到了书记职务信息
            if(list!=null && list.size()>0){
                String organcode = "";
                for (int i = 0; i < list.size(); i++) {
                    // 5.通过组织32位ID查询任职组织的ID
                    Map<String,Object> org = sqlComponent.getDbComponent().getMap(
                            "SELECT b.orgid,b.orgcode FROM t_dzz_info_simple a " +
                                    "left join urc_organization b on a.organcode = b.orgcode where organid=? limit 1",
                            new Object[]{list.get(i).get("organid")}
                    );
                    if(org!=null){
                        //判断当前组织下有没有人在考核自评审核角色里面
                        List<Map<String, Object>> datas = getRoleMaps(Long.valueOf(org.get("orgid").toString()),
                                session.getCompid());
                        //判断下当前登陆人是否在考核自评角色里面，
                        Map<String,Object> data = sqlComponent.getDbComponent().getMap(
                                "select a.sjid from urc_role_map a left join " +
                                        "urc_role b on a.roleid=b.roleid and a.compid=b.compid " +
                                        "where a.sjid=? and a.compid=? and b.rolecode=?" ,
                                new Object[]{session.getUserid(), session.getCompid(),"SELFAUDIT"});
                        //当前登录人不在该角色里面，并且当前登录人所在组织下有人在该角色里面
                        if(data==null && datas!=null && datas.size()>0){
                            pager.setRows(null);
                            pager.setTotalRows(0);
                        }else{
                            organcode += "'"+org.get("orgcode")+"',";
                            if(i==list.size()-1){
                                sqlComponent.getDbComponent().pageMap(pager,"select * from (\n" +
                                        "select a.packageid, a.`year`, IF(b.`status` = 1 and b.remark1 = 1, 3, b.`status`) as `status` ," +
                                        " a.`name`, a.enddate, a.startdate, a.score,b.code orgid from el_assess a\n" +
                                        "inner join el_assess_item_receive b on a.id = b.assessid and a.compid = b.compid\n" +
                                        "where date_format(now(), '%Y-%m-%d') >= str_to_date(a.startdate, '%Y-%m-%d')\n" +
                                        "and b.status = :status and a.compid = :compid and b.code in("+organcode.substring(0,organcode.length()-1)+")\n" +
                                        ") t order by STR_TO_DATE(t.startdate, '%Y-%m-%d') desc",params);
                            }
                        }
                    }
                }
            }else{
                //没有查询到书记就直接返回空数据
                pager.setRows(null);
                pager.setTotalRows(0);
            }
        }else{
            //组织账号直接查询
            pager = sqlComponent.pageQuery(ResultSelfEvalSqlCfg.ResultSelfEvalPageQuery, params, pager);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Message signForEvalSystem(Long packageid, Session session) throws Exception {
        Message msg = new Message();
        Integer signStatus = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalGetSignStatus, new Object[]{session.getCompid(), packageid, session.getCurrentOrg().getOrgcode()});
        if (signStatus == 1) {
            msg.fail("已签收,请勿重复签收");
            return msg;
        }
        if (signStatus == 2) {
            msg.fail("已上报,无需签收");
            return msg;
        }
        List<Long> assessids = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalSignGetAssIds, new Object[]{packageid, session.getCompid(), session.getCurrentOrg().getOrgcode()});
        if (assessids.size() < 1) {
            msg.fail("没有评分单位");
            return msg;
        }
        for (Long id : assessids) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("status", 1);
            map.put("receivedate", DateEnum.YYYYMMDDHHMMDD.format());
            map.put("code", session.getCurrentOrg().getOrgcode());
            map.put("assessid", id);
            map.put("compid", session.getCompid());
            int result = sqlComponent.update(ResultSelfEvalSqlCfg.ResultSelfEvalSign, map);
            if (result < 1) {
                LehandException.throwException("修改状态失败");
                msg.fail("修改状态失败");
                return msg;
            }
        }
        //获取考核表信息
        Map<String, Object> assessInfo = sqlComponent.query(ResultSelfEvalSqlCfg.getAssessInfoByItemReciver, new Object[]{session.getCompid(), session.getCurrentOrg().getOrgcode(), packageid});

//		try {
//			//给体系发布者(考核表修改人)发送消息
//			messageBusiness.insert(session, Constant.MODULE, Constant.MID, MessageEnum.ASESS_SIGN.getCode(), MessageEnum.ASESS_SIGN.getMsg(), "考核体系:"+assessInfo.get("name").toString()+"已被 "+session.getUsername()+"签收", (long) assessInfo.get("modiler"), assessInfo.get("modilname").toString(),Constant.EMPTY);
//		} catch (Exception e) {
//			LehandException.throwException("向体系发布者发送消息失败",e);
//			msg.fail("向体系发布者发送消息失败");
//			return msg;
//		}
        msg.success();
        return msg;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Message saveSelfEvalResult(Long id, Double selfscore, String feedbackcontent, Long itemid, Long code, List<Map<String, Object>> list, Session session, Long packageid, String files) throws Exception {
        Message msg = new Message();
        if (selfscore == null) {
            selfscore = (double) 0;
        }
//		Double leftScore = getSelfResidualScore(itemid,session.getCompid(),code);
//		if(selfscore>leftScore) {
//			msg.fail("超过当前可加分数!");
//			return msg;
//		}
        try {
            Integer result = sqlComponent.update(ResultSelfEvalSqlCfg.ResultSelfSaveScore,
                    new Object[]{selfscore, feedbackcontent, session.getCurrentOrg().getOrgcode(), DateEnum.YYYYMMDDHHMMDD.format(), files, id, session.getCompid()});
            if (result < 1) {
                LehandException.throwException("保存评分失败");
                msg.fail("保存失败!");
                return msg;
            }
        } catch (Exception e) {
            LehandException.throwException("保存评分异常");
            msg.fail("保存异常!");
            return msg;
            // TODO: handle exception
        }
        //清除佐证材料,重新生成
        clearCurrFiles(id, session.getCompid());
        if (list != null && list.size() > 0) {
            for (Map<String, Object> m : list) {
                if (m.get("name") == null) {
                    LehandException.throwException("文件夹名为空!");
                    msg.fail("文件夹名为空!");
                    return msg;
                }
                if (m.get("name").toString().length() > 100) {
                    LehandException.throwException("文件夹名称过长");
                    msg.fail("文件夹名称过长!");
                    return msg;
                }
                ElAssessFeedbackFile forder = new ElAssessFeedbackFile();
                forder.setCompid(session.getCompid());
                forder.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
                forder.setName(m.get("name").toString());
                forder.setFeedbackid(id);
                forder.setUserid(session.getUserid());
                forder.setUsername(session.getUsername());
                forder.setRemark3("");
                Long forderid = sqlComponent.insert(ResultSelfEvalSqlCfg.ResultSelfCreateForder, forder);
                if (forderid < 1) {
                    LehandException.throwException("新建文件夹失败");
                    msg.fail("新建文件夹失败");
                    return msg;
                }
                if (m.get("children") != null) {
                    @SuppressWarnings("unchecked")
                    List<Map<String, Object>> fileList = (List<Map<String, Object>>) m.get("children");
                    if (fileList.size() > 0) {
                        for (Map<String, Object> map : fileList) {
                            Map<String, Object> fileMap = new HashMap<String, Object>();
                            fileMap.put("id", map.get("id"));
                            fileMap.put("compid", session.getCompid());
                            fileMap.put("businessid", forderid);
                            fileMap.put("businessname", 1);
                            fileMap.put("intostatus", 0);
                            fileMap.put("year", map.get("year"));
                            Integer fileid = sqlComponent.insert(ResultSelfEvalSqlCfg.ResultInsertFile, fileMap);
                            if (fileid < 1) {
                                LehandException.throwException("创建文件关系失败");
                                msg.fail("创建文件关系失败");
                            }
                        }
                    }
                }
            }
        }

        try {
            sqlComponent.update(ResultSelfEvalSqlCfg.ResultSelfEvalReportRemark,
                    new Object[]{DateEnum.YYYYMMDDHHMMDD.format(), session.getCompid(), session.getCompid(), packageid, session.getCurrentOrg().getOrgcode()});
        } catch (Exception e) {
            LehandException.throwException(e.getMessage());
            msg.fail("修改接收表状态失败");
            return msg;
        }
        msg.success();
        return msg;
    }

    /**
     * 清除所有相关佐证
     *
     * @param id
     * @param compid
     * @return Boolean
     * 2019年4月13日
     * Tangtao
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean clearCurrFiles(Long id, Long compid) {
        sqlComponent.update(ResultSelfEvalSqlCfg.clearRelevantFiles, new Object[]{id, compid});
        sqlComponent.update(ResultSelfEvalSqlCfg.clearRelevantFordes, new Object[]{id, compid});
        return true;
    }


    @Override
    public List<Map<String, Object>> getSelfEvalSystemList(Long packageid, Long compid, Long orgid, Long userid, String type) throws Exception {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> FirstList = new ArrayList<Map<String, Object>>();
        if ("hasCode".equals(type)) {
            FirstList = sqlComponent.query(ResultSelfEvalSqlCfg.EvalSystemList, new Object[]{orgid, packageid, userid, compid});
        } else {
            FirstList = sqlComponent.query(ResultSelfEvalSqlCfg.SelfEvalSystemList, new Object[]{packageid, orgid, compid});
        }
        if (FirstList.size() < 1) {
            return list;
        }
        for (Map<String, Object> m : FirstList) {
            Map<String, Object> map = new HashMap<String, Object>();
            //子节点个数查询条件
            map.put("cobjcode", orgid);
            map.put("cscorecode", userid);
            //未评分个数查询条件
            map.put("robjcode", orgid);
            map.put("rscorecode", userid);
            //未自评个数查询条件
            map.put("sobjcode", orgid);
            //指标id和租户id
            map.put("pid", m.get("id"));
            map.put("compid", compid);
            //是否显示子节点 传值不显示,不传显示
            map.put("hide", "hasCode".equals(type) ? "Y" : null);
            List<Map<String, Object>> SecondList = sqlComponent.query(ResultSelfEvalSqlCfg.SelfEvalSystemListSecond, map);
            m.put("childList", SecondList);
            m.put("hasChild", SecondList.size() > 0 ? true : false);
            m.put("icon", "folder");
            list.add(m);
        }
        return list;
    }


    @Override
    public List<Map<String, Object>> getSelfEvalSystemListLast(Long id, Long compid, Long orgid, Long userid) throws Exception {
        List<Map<String, Object>> list = sqlComponent.query(ResultSelfEvalSqlCfg.SelfEvalSystemListLast, new Object[]{id, compid, orgid});
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Message reportSelfEval(Long packageid, Session session) throws Exception {
        Message msg = new Message();
        Integer flag = CheckReprotStatus(packageid, session.getCompid(), Long.valueOf(session.getCurrentOrg().getOrgcode()));
        if (flag == 1) {
            msg.fail("存在未自评的指标");
            return msg;
        }
        if (flag == 2) {
            msg.fail("当前体系未签收");
            return msg;
        }
        if (flag == 3) {
            msg.fail("当前体系已上报");
            return msg;
        }
        if (flag == 4) {
            msg.fail("已超过上报时间");
            return msg;
        }
        try {
            sqlComponent.update(ResultSelfEvalSqlCfg.ResultSelfEvalReportStatus,
                    new Object[]{DateEnum.YYYYMMDDHHMMDD.format(), session.getCompid(), session.getCompid(),
                            packageid, session.getCurrentOrg().getOrgcode()});
        } catch (Exception e) {
            e.printStackTrace();
            LehandException.throwException("修改考核接收表状态失败", e);
            msg.fail("修改考核接收表状态失败");
            return msg;
        }
        //判断体系是否需要自评审核
        //ElPackage ep = elPackagesBusiness.getElPackage(packageid, session);
        ElPackage pack = sqlComponent.query(ComesqlElCode.getPackageType, new Object[]{packageid, session.getCompid()});
        if (pack == null) {
            LehandException.throwException("the package is null!");
        }
        if (pack.getSelfassessaudit() == 0) {
            noNeedSelfAudit(packageid, session, msg, pack);
        } else {
            addAuditInfoAndSendUpcoming(packageid, session, pack);
        }
        removeTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SELF, String.valueOf(Long.valueOf(session.getCurrentOrg().getOrgcode())), session);
        msg.success();
        return msg;
    }

    /**
     * 不需要自评审核
     * @param packageid
     * @param session
     * @param msg
     * @param pack
     */
    private void noNeedSelfAudit(Long packageid, Session session, Message msg, ElPackage pack) {
        //查看当前体系下是否所有 被考核对象都已自评
        Integer allStatus = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfAllStatus, new Object[]{session.getCompid(), packageid});
        if (allStatus < 1) {
            //查询评分单位
            List<Map<String, Object>> codes = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalRevceiveInfo, new Object[]{session.getCompid(), packageid});
            if (codes.size() < 1) {
                msg.fail("没有评分单位");
            }
            for (Map<String, Object> code : codes) {
                ElEvaluateReceive rev = new ElEvaluateReceive();
                rev.setPackageid(packageid);
                rev.setPackagename((String) code.get("name"));
                rev.setCode((Long) code.get("code"));
                rev.setCompid(session.getCompid());
                rev.setStatus((short) 0);
                rev.setAssessscore((Double) code.get("score"));
                rev.setCycle("");
                rev.setOverdate("");
                List<ElEvaluateReceive> reciver = new ArrayList<>();
                try {
                    reciver = sqlComponent.query(ResultSelfEvalSqlCfg.CheckBeforeCreateReceive, rev);
                } catch (Exception e) {
                    LehandException.throwException("查询是否已经存在他评接收记录失败", e);
                    e.printStackTrace();
                    msg.fail("查询是否已经存在他评接收记录失败");
                }
                if (reciver.size() < 1) {
                    try {
                        sqlComponent.insert(ResultSelfEvalSqlCfg.ResultSelfEvalCreateReceive, rev);
                    } catch (Exception e) {
                        LehandException.throwException("插入评分单位数据失败", e);
                        msg.fail("插入评分单位数据失败");
                    }
                }
                //生成评分对象的待办事项
                addTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SCORE, pack.getName() + "（" + pack.getCreatetime().trim().substring(0, 4) + "年-" + session.getCurrentOrg().getOrgname() + "）", String.valueOf(packageid), Todo.FunctionURL.CHECK_EVALUATE.getPath(), (Long) code.get("code"), session);
            }
            sqlComponent.update(ResultSelfEvalSqlCfg.ResultSelfChangeAssaStatus, new Object[]{session.getCompid(), packageid, session.getCurrentOrg().getOrgcode()});
        }
    }

    /**
     * 添加审核记录并发送待办消息
     * @param packageid
     * @param session
     * @param pack
     */
    private void addAuditInfoAndSendUpcoming(Long packageid, Session session, ElPackage pack) {
        ElAssessFeedbackAudit ea = new ElAssessFeedbackAudit();
        ea.setPackageid(packageid);
        ea.setStatus((byte) 0);
        ea.setSubjectid(session.getCurrentOrg().getOrgid());
        //查询考核自评审核角色下上报党组织的审核人如果没有找到就直接是该组织的书记审核
        List<Map<String, Object>> datas = getRoleMaps(session.getCurrentOrg().getOrgid(),session.getCompid());
        if( datas!=null&&datas.size()>0){
            //将自评记录更新为待审核状态
            sqlComponent.getDbComponent().update("update el_assess_item_receive set status=3 where compid=? " +
                    "AND assessid=? and code =?",new Object[]{session.getCompid(), packageid,
                    session.getCurrentOrg().getOrgcode().substring(1)});
            ea.setAuditid(Long.valueOf(datas.get(0).get("orgid").toString()));
            ea.setModiler(Long.valueOf(datas.get(0).get("sjid").toString()));
            elAssessFeedbackAuditBusinessImpl.initElAssessFeedbackAudit(ea, session);
            //添加自评审核待办事项
            addTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SCORE_AUDIT,
                    pack.getName() + "（" + pack.getCreatetime().trim().substring(0, 4) + "年-"
                            + session.getCurrentOrg().getOrgname() + "）",
                    String.valueOf(packageid), Todo.FunctionURL.CHECK_SELF_AUDIT.getPath(), ea.getAuditid(), session);
        }else{
            //查询当前组织的书记
            Map<String, Object> sjmap = getOrganSecretary(session);
            if(sjmap==null){
                //没有查询到党组织书记信息(这种情况一般是不会发生的)
                //这里也是有可能存在一个极端情况就是所有被考核对象所在组织
                //都没有在角色里配置审核人并且也都没有组织书记
                //这样一来就会出现这个体系自评完就无法进入他评
                //这里这种极端情况暂时不予考虑
                ea.setStatus((byte) 1);
                elAssessFeedbackAuditBusinessImpl.addTtherCommentsScore(packageid,session,
                        Long.valueOf(session.getCurrentOrg().getOrgcode().substring(1)));
            }else{
                //将自评记录更新为待审核状态
                sqlComponent.getDbComponent().update("update el_assess_item_receive set status=3 where compid=? " +
                        "AND assessid=? and code =?",new Object[]{session.getCompid(), packageid,
                        session.getCurrentOrg().getOrgcode().substring(1)});
                ea.setAuditid(Long.valueOf(sjmap.get("organcode").toString().substring(1)));
                ea.setModiler(Long.valueOf(sjmap.get("userid").toString()));
                //添加自评审核待办事项
                addTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SCORE_AUDIT,
                        pack.getName() + "（" + pack.getCreatetime().trim().substring(0, 4) + "年-" + session.getCurrentOrg().getOrgname() + "）",
                        String.valueOf(packageid), Todo.FunctionURL.CHECK_SELF_AUDIT.getPath(), ea.getAuditid(), session);
            }
            ea.setAuditid(sjmap==null ? null : Long.valueOf(sjmap.get("userid").toString()));
            elAssessFeedbackAuditBusinessImpl.initElAssessFeedbackAudit(ea, session);
        }
    }
// TODO
    private Map<String, Object> getOrganSecretary(Session session) {
        return sqlComponent.getDbComponent().getMap(
                        "select c.userid,b.organcode from t_dzz_bzcyxx a left join t_dzz_info b on a.organid=b.organid left join " +
                                "urc_user c on a.zjhm=c.idno where b.organcode=? and a.dnzwname in (5100000017," +
                                "4100000013,3100000009) and a.zfbz=0 limit 1",
                        new Object[]{session.getCurrentOrg().getOrgcode()});
    }

    private List<Map<String, Object>> getRoleMaps(Long orgid,Long compid) {
        return sqlComponent.getDbComponent().listMap(
            "select a.* from urc_role_map a left join " +
                "urc_role b on a.roleid=b.roleid and a.compid=b.compid where a.orgid=? and a.compid=? and b" +
                ".rolecode=?" ,new Object[]{orgid,
                        compid, "SELFAUDIT"});
    }


    /**
     * 上报前判断
     *
     * @param packageid
     * @param compid
     * @param orgid
     * @return Integer
     * 2019年4月18日
     * Tangtao
     */
    private Integer CheckReprotStatus(Long packageid, Long compid, Long orgid) {
        Integer status = 0;//可上报状态
        //查询是否存在没有自评分数的
        Integer count = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalCheckUnScored, new Object[]{packageid, compid, orgid});
        if (count > 0) {
            status = 1;//存在未评分的指标
            return status;
        }
        //查询是否是可上报状态
        Integer reportStatus = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalGetReportStatus, new Object[]{packageid, compid, orgid});
        if (reportStatus == 0) {
            status = 2;//当前体系未签收
            return status;
        }
        if (reportStatus == 2) {
            status = 3;//当前体系已上报
            return status;
        }
        if (reportStatus == 4) {
            status = 4;//当前体系已过时
            return status;
        }
        return status;
    }

    @Override
    public Message formInfo(Long id, Long compid, String code) throws Exception {
        Message msg = new Message();
        SelfAssessmentScoreDto data = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalDetail, new Object[]{id, compid});
        Double score = getSelfResidualScore(id, compid, code);
        //直接取上传附件数据集合
        data.setListFiles(filePreviewAndDownload.listFile(data.getFiles(),compid));

        List<ElAssessFeedbackFile> folderList = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalForders, new Object[]{id, compid});
        if (folderList.size() < 1) {
            msg.success();
            msg.setData(data);
            return msg;
        }
        List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> proofList = new ArrayList<Map<String, Object>>();
        if (folderList.size() > 0) {
            for (ElAssessFeedbackFile folder : folderList) {
                fileList = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalFiles, new Object[]{compid, folder.getId()});
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("id", folder.getId());
                map.put("compid", compid);
                map.put("name", folder.getName());
                map.put("createtime", folder.getCreatetime());
                map.put("creator", folder.getUserid());
//				map.put("checkedArr", folder.getRemark3());
//				map.put("year", folder.getRemark4());
                if (fileList.size() > 0) {
                    for (Map<String, Object> ms : fileList) {
                        ms.put("checked", true);
                    }
                }
                map.put("children", fileList);
                proofList.add(map);
            }
        }
        data.setFilenames(proofList);
        if (score != null) {
            data.setScore(score);
        }
        msg.success();
        msg.setData(data);
        return msg;
    }


    /**
     * 当前指标剩余可加分数
     *
     * @param id
     * @param compid
     * @param code
     * @return Double
     * 2019年4月13日
     * Tangtao
     */
    private Double getSelfResidualScore(Long id, Long compid, String code) {
        Double totalScore = sqlComponent.query(ResultSelfEvalSqlCfg.CurrEvalScore, new Object[]{id, compid});
//		List<Double> scoreList = sqlComponent.query(ResultSelfEvalSqlCfg.SelfEvalScoreList, new Object[] {id,id,compid,code});
//		Double score = sqlComponent.query(ResultSelfEvalSqlCfg.EvalScore, new Object[] {id,compid});
//		if(totalScore == null) {
//			return score;
//		}
//		if(scoreList.size()>0){			
//			for(Double childScore : scoreList ) {
//				totalScore = totalScore-childScore;
//			}
//		}
//		if(score>totalScore) {
//			score = totalScore;
//		}
//		if(totalScore<0) {
//			return (double) 0;
//		}
//		return score;
        return totalScore;
    }

    @Override
    public Map<String, Object> getSelStatus(Long compid, Long code) throws Exception {
        Map<String, Object> map = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfGetSelStatus, new Object[]{compid, code});
        return map;
    }

    @Override
    public List<Map<String, Object>> getEvalSystemListLast(Long id, Long compid, Long orgid, Long currOrgid) {
        List<Map<String, Object>> list = sqlComponent.query(ResultSelfEvalSqlCfg.EvalSystemListLast, new Object[]{id, compid, orgid, currOrgid});
        return list;
    }


    /**
     * 待办任务begin
     ****************************************************************************************************************/
    @Resource
    private TodoItemService todoItemService;

    /**
     * 添加待办事项.
     *
     * @param itemType 事项类型
     * @param name     事项名称
     * @param bizid    该事项对应的业务ID
     * @param path     资源路径
     * @param session  当前会话
     */
    public void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path, Long orgCode,
                         Session session) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        itemDTO.setBizid(bizid);
        itemDTO.setCategory(itemType.getCode());
        itemDTO.setCompid(session.getCompid());
//		itemDTO.setUserid(session.getUserid());
//		itemDTO.setUsername(session.getUsername());
        itemDTO.setOrgid(orgCode);
        itemDTO.setName(name);
        itemDTO.setBizid(bizid);
        itemDTO.setTime(new Date());
        itemDTO.setPath(path);
        itemDTO.setState(Todo.STATUS_NO_READ);
        todoItemService.createTodoItem(itemDTO);
    }

    /**
     * 添加待办事项.
     *
     * @param itemType 事项类型
     * @param bizid    该事项对应的业务ID
     * @param session  当前会话
     */
    private void removeTodoItem(Todo.ItemType itemType, String bizid, Session session) {
        TodoItemDTO itemDTO = new TodoItemDTO();
        itemDTO.setCategory(itemType.getCode());
        itemDTO.setCompid(session.getCompid());
        itemDTO.setBizid(bizid);
        todoItemService.deleteTodoItem(itemDTO);
    }
    /**待办任务end****************************************************************************************************************/
}
