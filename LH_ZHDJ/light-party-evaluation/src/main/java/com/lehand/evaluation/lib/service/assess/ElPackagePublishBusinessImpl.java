package com.lehand.evaluation.lib.service.assess;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.business.ElPackagePublishBusiness;
import com.lehand.evaluation.lib.dto.AccountInfoDto;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessObject;
import com.lehand.evaluation.lib.service.message.MessageBusinessImp;
import com.lehand.evaluation.lib.util.NumUtil;
import org.springframework.transaction.annotation.Transactional;

import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.common.constant.Constant;
import com.lehand.evaluation.lib.common.constant.MessageEnum;

/**
 * 
 * @ClassName: ElPackagePublishBusinessImpl
 * @Description: 考核公布
 * @Author dong
 * @DateTime 2019年4月15日 下午5:29:49
 */
public class ElPackagePublishBusinessImpl implements ElPackagePublishBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	@Resource
	MessageBusinessImp messageBusiness;
	/**
	 * 考核结果公布列表
	 */
	@Override
	public Pager queryAsssess(Pager pager, String likeStr, long status, Session session) {
		Map<String,Object> map = new HashMap<String,Object>(4);
		map.put("orgId", session.getCurrentOrg().getOrgcode());
		map.put("status", status);
		map.put("likeStr", likeStr);
		map.put("compid", session.getCompid());
		generalSqlComponent.pageQuery(ComesqlElCode.getAssessListStatusType, map,pager);
		return pager;
	}
	
	/**
	 * 
	 * @Title: queryAsssessCount
	 * @Description: 考核结果公布列表数量
	 * @Author dong
	 * @DateTime 2019年4月23日 上午8:50:19
	 * @param
	 * @param
	 * @param
	 * @param session
	 * @return
	 */
	@Override
	public List<Map<String,Object>> queryAsssessCount(Session session) {
		Map<String,Object> map = new HashMap<String,Object>(4);
		map.put("orgId", session.getCurrentOrg().getOrgcode());
		map.put("compid", session.getCompid());
		List<Map<String,Object>> listMap = generalSqlComponent.query(ComesqlElCode.getAssessStatusCouType, map);
		return listMap;
		
	}
	
	/**
	 * 
	 * @Title: queryObject
	 * @Description: 获取考核对象及得分
	 * @Author dong
	 * @DateTime 2019年4月15日 下午9:58:36
	 * @param pager
	 * @param assessid
	 * @param session
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Pager queryObject(Pager pager,long assessid,String oneself,Session session){
		Map<String,Object> querymap = new HashMap<String, Object>();
		querymap.put("assessid", assessid);
		querymap.put("compid", session.getCompid());
		if("1".equals(oneself)) {
			querymap.put("code", session.getCurrentOrg().getOrgcode());
		}
		
		generalSqlComponent.pageQuery(ComesqlElCode.getAssessObjectPageType,querymap,pager);
		
		Map<String,Object> map;
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		List<ElAssessObject> objs = (List<ElAssessObject>) pager.getRows();
		for(ElAssessObject obj:objs) {
			double selfScore = generalSqlComponent.query(ComesqlElCode.getObjectScoreSelfType, new Object[] {assessid,obj.getCode(),session.getCompid()});
			double onthScore = generalSqlComponent.query(ComesqlElCode.getObjectScoreOthType, new Object[] {assessid,obj.getCode(),session.getCompid()});
			Integer status = generalSqlComponent.query(ComesqlElCode.getFeedBackStatusCodeType, new Object[] {assessid,obj.getCode(),session.getCompid()});
			map = new HashMap<String,Object>();
			map.put("code", obj.getCode());
			map.put("name", obj.getName());
			map.put("assessid", obj.getAssessid());
			map.put("type", obj.getType());
			map.put("selfScore", NumUtil.tonumber2(selfScore));
			map.put("onthScore", NumUtil.tonumber2(onthScore));
			map.put("status", status);
			list.add(map);
			pager.setRows(list);
		}
		return pager;
		
	}
	
	/**
	 * 
	 * @Title: queryItemScore
	 * @Description: 获取指标及自评分、他评分
	 * @Author dong
	 * @DateTime 2019年4月16日 下午3:20:00
	 * @param pager
	 * @param pid
	 * @param session
	 * @return
	 */
	@Override
	public Pager queryItemScore(Pager pager,long pid,long objid,long code,Session session) {
		Map<String,Object>  map = new HashMap<String, Object>();
		map.put("pid", pid);
		map.put("type", 1);
		map.put("compid", session.getCompid());
		map.put("code", objid);
		map.put("usertor", code);
		generalSqlComponent.pageQuery(ComesqlElCode.getItemScoreType, map, pager);
		return pager;
		
	}
	
	/**
	 * 公布
	 * @Title: publish
	 * @Description: 
	 * @Author dong
	 * @DateTime 2019年4月16日 下午9:53:24
	 * @param assessid
	 * @param
	 * @param
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void publish(long assessid,int remark1,Session session) {
//		generalSqlComponent.update(ComesqlElCode.updatePackageType, new Object[] {packageid,3,session.getCompid()});
		generalSqlComponent.update(ComesqlElCode.updateAsseaRemark1Type, new Object[] {3,remark1,assessid,session.getCompid()});
		
		/** 发送消息*/
		//根据考核id获取被考核对象
		List<ElAssessObject> codes  = generalSqlComponent.query(ComesqlElCode.getAssessObjectType,new Object[] {assessid,session.getCompid()});
		//
		ElAssess ass  = generalSqlComponent.query(ComesqlElCode.getAssessType,new Object[] {assessid,session.getCompid()});
		
//		for(ElAssessObject obj:codes) {
//			AccountInfoDto info = messageBusiness.getAccountByOrgid(obj.getCode(), obj.getCompid());
//			try {
//				messageBusiness.insert(session, Constant.MODULE, Constant.MID, MessageEnum.ASESS_PUBLICITY.getCode(), MessageEnum.ASESS_PUBLICITY.getMsg(), "考核体系:"+ass.getName()+" 已经公示", info.getUserid(), info.getUsername(),Constant.EMPTY);
//			} catch (Exception e) {
//				LehandException.throwException("向被考核单位发送公示消息失败",e);
//			}
//		}
	}
	

	/**
	 * 
	 * @Title: getEvaluate
	 * @Description: 获取各评分单位对被考核对象的评分和自评分
	 * @Author dong
	 * @DateTime 2019年4月18日 下午6:01:58
	 * @return
	 */
	@Override
	public List<Map<String,Object>> getEvaluate(long assessid, long code, Session session) {
		List<Map<String,Object>> list= generalSqlComponent.query(ComesqlElCode.getEvaluateObjectType, new Object[] {code,assessid,session.getCompid()});
//		double selfSumScore = 0d;
		double othSumScore = 0d;
		for(Map<String,Object> li:list) {
//			selfSumScore +=Double.valueOf(li.get("selfScore").toString());
			othSumScore += Double.valueOf(li.get("othScore").toString());
			li.put("selfscore", NumUtil.tonumber2(Double.valueOf(li.get("selfscore").toString())));
			li.put("othscore", NumUtil.tonumber2(Double.valueOf(li.get("othscore").toString())));
			
		}
		for(Map<String,Object> li:list) {
			li.put("selfSumScore", NumUtil.tonumber2(othSumScore));
			li.put("othSumScore", NumUtil.tonumber2(othSumScore));
		}
//		if(list.size()>0) {
//			Map<String,Object> map = new HashMap<String, Object>();
//			map.put("selfSumScore", othSumScore);
//			map.put("othSumScore", othSumScore);
//			list.add(map);
//		}
		
		return list;
		
	}
	
	@Override
	public List<Map<String,Object>> getTree(long orgid,long packageid,Session session){
		Map<String,Object> querymap1 = new HashMap<String,Object>();
		querymap1.put("packageid", packageid);
		querymap1.put("code", orgid);
		querymap1.put("compid", session.getCompid());
		List<Map<String,Object>> FirstList = generalSqlComponent.query(ComesqlElCode.getTreePublishType1, querymap1);
		Map<String,Object> map;
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		
		if(FirstList.size()>=1) {
			for(Map<String,Object> it:FirstList) {
				map = new HashMap<String, Object>();
				map.put("id", it.get("id"));
				map.put("name", it.get("name"));
				map.put("pid", it.get("pid"));
				map.put("isShow", false);
				map.put("isOpen", false);
				map.put("icon", "folder");
				map.put("packageid", it.get("packageid"));
				map.put("score", it.get("score"));
				
				querymap1.put("pid", it.get("id"));
				List<Map<String,Object>> item2 = generalSqlComponent.query(ComesqlElCode.getTreePublishType2, querymap1);
				List<Map<String,Object>> list2 = new ArrayList<Map<String,Object>>();
				for(Map<String,Object> i:item2) {
					Map<String,Object> map2 = new HashMap<String, Object>();
					map2.put("id", i.get("id"));
					map2.put("name", i.get("name"));
					map2.put("pid", i.get("pid"));
					map2.put("isShow", false);
					map2.put("isOpen", false);
					map2.put("icon", "folder");
					map2.put("packageid", i.get("packageid"));
					map2.put("score", i.get("score"));
					list2.add(map2);
				}
				map.put("childList", list2);
				list.add(map);
			}
		}
		return list;
	}

	@Override
	public void modiOtherScore(Session session, Long feedbackid,String content,Double score,String fileids) {
		generalSqlComponent.update(ComesqlElCode.modiOtherScore, new Object[] {score,content,fileids,session.getCompid(),feedbackid});
	}

}
