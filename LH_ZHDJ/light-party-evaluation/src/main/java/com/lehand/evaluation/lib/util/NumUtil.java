package com.lehand.evaluation.lib.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mysql.jdbc.StringUtils;

public class NumUtil {
	/**
	 *	判断是否是数字类型
	 *	@param score
	 *	@return
	 *	boolean
	 *	2019年4月24日
	 *	Tangtao
	 */
	public static boolean checkNum(String score,Integer len) {
		if(StringUtils.isNullOrEmpty(score)) {
			return false;
		}
		//正数判断 浮点型最多两位小数
		Pattern pattern=Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,"+len+"})?$");
        Matcher isNum = pattern.matcher(score);
        if( !isNum.matches() ){
            return false;
        }
		return true;
	}
	
	
	public static double tonumber2(Double num) {
		return Double.valueOf(String.format("%.2f", num));
		
	}
	/**
	 * 四舍五入保留一位小数
	 * @param num
	 * @return
	 */
	public static double tonumber1(Double num) {
		return Double.valueOf(String.format("%.1f", num));
		
	}
}
