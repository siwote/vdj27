package com.lehand.evaluation.lib.business.summary;

import java.util.List;
import java.util.Map;

import com.lehand.base.common.Message;
import com.lehand.base.common.Session;

/**
 *	@author Tangtao
 *	考核汇总
 */
public interface AssessmentSummaryBusiness {

	/**
	 *	所有考核体系列表
	 *	@param year 年份
	 *	@param
	 *	@param
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月16日
	 *	Tangtao
	 */
	public List<Map<String,Object>> getSummSystemList(String year, Session session) throws Exception;
	
	
	/**
	 *	考核指标汇总表
	 *	@param ObjectCode 考核对象
	 *	@param packageid  体系id
	 *	@param compid
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月16日
	 *	Tangtao
	 */
	public Map<String,Object> getSummData(Long ObjectCode,Long packageid,Long compid) throws Exception;


	/**
	 *	考核对象列表
	 *	@param packageid
	 *	@param str
	 *	@param compid
	 *	@return
	 *	Message
	 *	2019年4月19日
	 *	Tangtao
	 */
	public Message getEvalObjectList(Long packageid, String str, Long compid);


	/**
	 *	导出excel
	 *	@param packageid
	 *	@param str
	 *	@param compid
	 *	@return
	 *	Message
	 *	2019年4月29日
	 *	Tangtao
	 */
	public Message exportData(Long packageid, String str, Session compid);
	
	
	
	
}
