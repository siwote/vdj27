package com.lehand.evaluation.lib.dto;

public class OrgInfo {

    private String orgcode;
    private Long orgid;
    private Integer accflag;

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public Integer getAccflag() {
        return accflag;
    }

    public void setAccflag(Integer accflag) {
        this.accflag = accflag;
    }
}
