package com.lehand.evaluation.lib.service.selfevaluation;

import java.util.*;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.components.web.interceptors.DefaultInterceptor;
import com.lehand.evaluation.lib.business.selfevaluation.EvalResultScoreBusiness;
import com.lehand.evaluation.lib.dto.AssessmentScoreDto;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackFile;
import com.lehand.evaluation.lib.pojo.ElAssessObject;
import com.lehand.evaluation.lib.pojo.ElAssessScoreRecord;
import com.lehand.horn.partyorgan.constant.SqlCode;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.common.constant.EvalResultScoreSqlCfg;
import com.lehand.evaluation.lib.common.constant.ResultSelfEvalSqlCfg;
import org.springframework.util.StringUtils;

public class EvalResultScoreBusinessImpl implements EvalResultScoreBusiness {

	@Resource GeneralSqlComponent sqlComponent;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;
	@Override
	public Message getEvalObjectList(Long packageid,String str,Long compId,Long orgid) throws Exception {
		
		ElAssess assess = sqlComponent.query(ComesqlElCode.getAssesspackageidType,new Object[] {packageid,compId});
		Message msg = new Message();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("assessid", assess.getId());
		map.put("orgid", orgid);
		map.put("compid", compId);
		map.put("likeStr", str);
		List<Map<String,Object>> data = sqlComponent.query(EvalResultScoreSqlCfg.EvalResultScoreObjects,map);
		msg.success();
		msg.setData(data);
		return msg;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Message saveScore(AssessmentScoreDto feedBack, Long compId, Long currUserId) throws Exception {
		Message msg = new Message();		
//		Double leftScore = getSelfResidualScore(feedBack.getItemid(),compId,feedBack.getCode());
//		if(Double.parseDouble(feedBack.getScore())>leftScore) {
//			msg.fail("超过当前可加分数!");
//			return msg;
//		}
		Integer result = 0;
		ElAssessScoreRecord record = new ElAssessScoreRecord();
		record.setCompid(compId);
		record.setContent(feedBack.getContent());
		record.setFeedbackid(feedBack.getFeedbackid());
		record.setScore(feedBack.getScore()== null?0 : Double.parseDouble(feedBack.getScore()) );
		record.setUsertor(currUserId);
		record.setRemark4(feedBack.getFileids());
		if(feedBack.getId()==null) {			
			//新增
			result = sqlComponent.insert(EvalResultScoreSqlCfg.EvalResultScoreSaveScore, record);
		}else {
			//修改
			record.setId(feedBack.getId());
			record.setModiler(currUserId);
			result = sqlComponent.update(EvalResultScoreSqlCfg.EvalResultScoreUpdateScore, record);
		}
		if(result < 1) {
			msg.fail("保存失败!");
			return msg;
		}
		try {
			
			sqlComponent.update(EvalResultScoreSqlCfg.EvalResultScoreReportScoreRec, new Object[] {DateEnum.YYYYMMDDHHMMDD.format(),compId,currUserId,feedBack.getAssessid()});

		} catch (Exception e) {
			LehandException.throwException("考核单位接收表保存失败");
			msg.fail("考核单位接收表保存失败");
			return msg;
		}
		try {			
			sqlComponent.update(EvalResultScoreSqlCfg.UpdateFeedBackStatusById,
					new Object[] {2,currUserId,DateEnum.YYYYMMDDHHMMDD.format(),feedBack.getFeedbackid(),compId});
		} catch (Exception e) {
			LehandException.throwException("修改考核反馈表状态异常", e);
		}
		msg.success();
		return msg;
	}
    /**
     * 获取附件list集合
     * @param fileids
     * @return
     */
    private List<Map<String, Object>> listFile(String fileids) {
        List<Map<String,Object>> listFile = new ArrayList<>();
        if(!StringUtils.isEmpty(fileids)){
            String[] fileid = fileids.split(",");
            for (String id:fileid) {
                Map<String,Object> m =  sqlComponent.query(SqlCode.getPrintFile, new HashMap<String,Object>(){{put("compid",1);put("id",id);}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    m.put("fileid",id);
                    m.put("flpath", String.format("%s%s", downIp, dir));
                    m.put("name",m.get("name").toString().substring(0,m.get("name").toString().lastIndexOf(".")));
                    listFile.add(m);
                }
            }
        }
        return  listFile;
    }
	@Override
	public Message getEvalPerforInfo(Long id,Long compid) throws Exception {
		Message msg = new Message(); 
		AssessmentScoreDto data  = new AssessmentScoreDto();
		try {
			data = sqlComponent.query(ResultSelfEvalSqlCfg.ResultEvalDetail, new Object[] {id,compid});
		} catch (Exception e) {
			LehandException.throwException("获取指标详情失败");
		}
		if(data==null) {
			msg.fail("当前指标不存在");
			return msg;
		}
		Map<String,Object> params = new HashMap<>();
		params.put("fileids", data.getFileids());
		if(data.getFileids()==null || "".equals(data.getFileids())) {
			data.setOtherfilenames(new ArrayList<>());
		}else {
			data.setOtherfilenames(sqlComponent.query(ResultSelfEvalSqlCfg.listDlfileByInIds,params));
		}
        //直接取上传附件数据集合
        data.setListFiles(listFile(data.getFiles()));
		Double score = getSelfResidualScore(id,compid,data.getCode());		
		List<ElAssessFeedbackFile> folderList =sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalForders,new Object[]{id,compid});
		if(folderList.size()<1) {
			msg.success();
			msg.setData(data);
			return msg;
		}
		
		List<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> proofList = new ArrayList<Map<String,Object>>();
		if(folderList.size()>0) {		
			for(ElAssessFeedbackFile folder:folderList) {
				fileList = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalFiles,new Object[]{compid,folder.getId()});	
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("id", folder.getId());
				map.put("compid", compid);
				map.put("name", folder.getName());
				map.put("createtime", folder.getCreatetime());
				map.put("creator", folder.getUserid());
				map.put("checkedArr", folder.getRemark3());
				map.put("year", folder.getRemark4());
				if(fileList.size()>0) {					
					for(Map<String,Object> ms :fileList) {
						ms.put("checked", true);
					}
				}
				map.put("children", fileList);
				proofList.add(map);
			}		
		}
		data.setFilenames(proofList);
		data.setBzscore(score.toString());
		msg.success();
		msg.setData(data);
		return msg;
	}

	/**
	 *	剩余可加分数
	 *	@param id
	 *	@param compid
	 * @param code 
	 *	@return
	 *	Double
	 *	2019年4月13日
	 *	Tangtao
	 */
	private Double getSelfResidualScore(Long id,Long compid, Long code) {
//		Double totalScore = (double) 0;
//		List<Double> scoreList = new ArrayList<Double>();
		Double score =  (double) 0;
//		try {			
//			 totalScore = sqlComponent.query(ResultSelfEvalSqlCfg.EvalScore, new Object[] {id,compid});
//		} catch (Exception e) {
//			LehandException.throwException("获取总分异常");
//		}
//		try {	
//			//同级其他分数
//			scoreList = sqlComponent.query(ResultSelfEvalSqlCfg.EvalScoreList, new Object[] {id,id,compid,code});
//		} catch (Exception e) {
//			LehandException.throwException("获取同级其他分数异常");
//		}
		try {
			//当前指标
			score = sqlComponent.query(ResultSelfEvalSqlCfg.CurrEvalScore, new Object[] {id,compid});			
		} catch (Exception e) {
			LehandException.throwException("获取当前指标分异常");
		}
//		if(scoreList.size()<1) {
//			return score!=null?score:0;
//		}
//		for(Double childScore : scoreList ) {
//			totalScore = totalScore-childScore;
//		}
//		if(score>totalScore) {
//			score = totalScore;
//		}
//		if(totalScore<0) {
//			return (double) 0;
//		}
//		return score;
		return score!=null?score:0;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Message reportEval(Long packageid, Session session) throws Exception {
		Message msg = new Message();
		Integer checkReport = checkEvalReport(packageid,session.getCompid(),/*session.getOrganids().get(0).getOrgid()*/session.getUserid());
		if(checkReport == 1) {
			msg.fail("存在未评分的指标");
			return msg;
		}
		if(checkReport == 2) {
			msg.fail("已上报,勿重复上报");
			return msg;
		}		
//		if(checkReport == 4) {
//			msg.fail("已超过上报时间");
//			return msg;
//		}		
		try {			
			sqlComponent.update(EvalResultScoreSqlCfg.UpdateEvalResultRecve, new Object[] {packageid,/*session.getOrganids().get(0).getOrgid()*/session.getUserid(),session.getCompid()});
		} catch (Exception e) {
			LehandException.throwException("考核单位反馈表修改失败");
			msg.fail("考核单位反馈表修改失败");
			return msg;
		}
		//查询考核信息
		Integer unreport  = sqlComponent.query(EvalResultScoreSqlCfg.getUnreportedNum, new Object[] {session.getCompid(),packageid});
		if(unreport<1) {			
			ElAssess assess  = sqlComponent.query(ComesqlElCode.getAssesspackageidType, new Object[] {packageid,session.getCompid()});
			Integer assessStatus = sqlComponent.update(ComesqlElCode.updateAsseaType, new Object[] {2,assess.getId(),session.getCompid()});
			if(assessStatus < 1) {
				msg.fail("考核表状态保存失败");
				return msg;
			}

			//全部自评完，生成评分单位的待办任务
//			if(){
//				List<ElAssessObject> objectList = sqlComponent.query(ComesqlElCode.getAssessObjectType, new Object[] {assess.getId(),session.getCompid()});
//				for (ElAssessObject elAssessObject : objectList) {
//					addTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SCORE, assess.getPackagename() + "（" + assess.getYear() + "-" + assess.getOrgname() + "）", String.valueOf(assess.getId()), Todo.FunctionURL.CHECK_EVALUATE.getPath(), elAssessObject.getCode(), session);
//				}
//			}
		}
		removeTodoItem(Todo.ItemType.TYPE_ASSESSMENT_SCORE, String.valueOf(packageid), session);
		msg.success();
		return msg;
	}

	private Integer checkEvalReport(Long packageid, Long compid, Long orgid) {
		Integer status = 0;//正常状态
		//是否存在未评分指标
		Integer unscored = sqlComponent.query(EvalResultScoreSqlCfg.EvalResultGetUnScoreNum, new Object[] {packageid,compid,orgid});
		if(unscored>0) {//存在未评分数据
			status = 1;
			return status;
		}
		Integer reportStatus = sqlComponent.query(EvalResultScoreSqlCfg.EvalResultGetReportStatus, new Object[] {packageid,compid,orgid});
		
		if(reportStatus == 1) {//已上报
			status = 2;
			return status;
		}
		if(reportStatus == 4) {//已上报
			status = 4;
			return status;
		}
		return status;
	}

	@Override
	public Message pageQuery(Pager pager, String str,Short status, Session session) throws Exception {
		Message msg = new Message();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("compid", session.getCompid());
		map.put("code", /*session.getOrganids().get(0).getOrgid()*/session.getUserid());
		map.put("status", status);
		map.put("likeStr", str);
		pager = sqlComponent.pageQuery(EvalResultScoreSqlCfg.EvalResultScorePageQuery, map, pager);
		msg.success();
		msg.setData(pager);
		return msg;
	}

	@Override
	public Map<String, Object> getStatus(Long compid, Long code) throws Exception {
		Map<String,Object> map = sqlComponent.query(EvalResultScoreSqlCfg.EvalResultScoreGetStatus, new Object[] {compid,code});
		return map;
	}


	/**待办任务begin****************************************************************************************************************/
	@Resource
	private TodoItemService todoItemService;

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param name 事项名称
	 * @param bizid 该事项对应的业务ID
	 * @param path 资源路径
	 * @param session 当前会话
	 */
	private void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path, Long orgCode, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setBizid(bizid);
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setUserid(session.getUserid());
		itemDTO.setUsername(session.getUsername());
		itemDTO.setOrgid(orgCode);
		itemDTO.setName(name);
		itemDTO.setBizid(bizid);
		itemDTO.setTime(new Date());
		itemDTO.setPath(path);
		itemDTO.setState(Todo.STATUS_NO_READ);
		todoItemService.createTodoItem(itemDTO);
	}

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param bizid 该事项对应的业务ID
	 * @param session 当前会话
	 */
	private void removeTodoItem(Todo.ItemType itemType,  String bizid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setBizid(bizid);
		todoItemService.deleteTodoItem(itemDTO);
	}
	/**待办任务end****************************************************************************************************************/
}
