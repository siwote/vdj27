package com.lehand.evaluation.lib.service.assess;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessObject;

/**
 * 
 * @ClassName: ElAssessObjectBusiness
 * @Description: 被考核对象
 * @Author dong
 * @DateTime 2019年4月12日 下午5:07:18
 */
public class ElAssessObjectBusiness {
	@Resource
	GeneralSqlComponent generalSqlComponent;

	/**
	 * 
	 * @Title: initObjectCalss
	 * @Description:被考核对象初始化
	 * @Author dong
	 * @DateTime 2019年4月11日 下午7:44:21
	 * @param elAssess
	 * @param object
	 * @param session
	 */
	public  void initObjectCalss(ElAssess elAssess, ElAssessObject object, Session session) {
		object.setCompid(session.getCompid());
		object.setAssessid(elAssess.getId());
		object.setType(elAssess.getType());
		object.setRemark1(0);
		object.setRemark2(0);
		object.setRemark3(StringConstant.EMPTY);
		object.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addObjectType, object);
		
	}
	
	/**
	 * 
	 * @Title: copyObjectCalss
	 * @Description:复制被考核对象 
	 * @Author dong
	 * @DateTime 2019年4月18日 上午8:53:05
	 * @param assessid
	 * @param object
	 * @param session
	 */
	public  void copyObjectCalss(long assessid,ElAssessObject object, Session session) {
		object.setCompid(session.getCompid());
		object.setAssessid(assessid);
		generalSqlComponent.insert(ComesqlElCode.addObjectType, object);
		
	}

}
