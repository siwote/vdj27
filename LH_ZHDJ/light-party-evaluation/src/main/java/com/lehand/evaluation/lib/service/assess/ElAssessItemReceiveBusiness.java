package com.lehand.evaluation.lib.service.assess;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessItemReceive;
import com.lehand.evaluation.lib.pojo.ElPackage;

/**
 * 
 * @ClassName: ElAssessItemReceiveBusiness
 * @Description: 评分对象
 * @Author dong
 * @DateTime 2019年4月12日 下午2:10:07
 */
public class ElAssessItemReceiveBusiness {
	@Resource
	GeneralSqlComponent generalSqlComponent;
	/**
	 * 
	 * @Title: initReceiveClass
	 * @Description: 反馈接收
	 * @Author dong
	 * @DateTime 2019年4月12日 下午2:08:33
	 * @param receive
	 * @param
	 * @param type
	 */
	public void initReceiveClass(ElAssessItemReceive receive, ElAssess assess, ElPackage packag, long code, Session session) {
		receive.setCompid(session.getCompid());
		receive.setAssessid(assess.getId());
		receive.setAssessname(assess.getName());
		receive.setStatus((short) 0);
		receive.setReceivedate(StringConstant.EMPTY);
		receive.setUpdatedate(StringConstant.EMPTY);
		receive.setOverduestatus((short) 0);
		receive.setAssessscore(packag.getScore());
		receive.setScore(0d);
		receive.setCycle(assess.getCycle());
		receive.setCode(code);
		receive.setRemark1(0);
		receive.setRemark2(0);
		receive.setRemark3(StringConstant.EMPTY);
		receive.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addItemReceive, receive);
	}
}
