package com.lehand.evaluation.lib.dto;

import java.util.ArrayList;
import java.util.List;

public class AssessmentIndexDto {
	//指标ID
	private Long id;
	//指标名称
	private String name;
	//指标父节点(根节点)
	private Long pid;	
	//考核体系编码
	private Long packageid;
	//考核对象id
	private Long code;
	//反馈表ID
	private Long feedid;
	//type org:对组织机构考核、user:对人员考核
	private String type;
	//状态 0:未反馈、1:已反馈、2:已评分、3:公示
	private Short status;
	//反馈内容
	private String feedbackcontent;
	//自评分
	private Double selfscore;
	
	private List<AssessmentIndexDto> children = new ArrayList<>();
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getPackageid() {
		return packageid;
	}

	public void setPackageid(Long packageid) {
		this.packageid = packageid;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public Long getFeedid() {
		return feedid;
	}

	public void setFeedid(Long feedid) {
		this.feedid = feedid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public String getFeedbackcontent() {
		return feedbackcontent;
	}

	public void setFeedbackcontent(String feedbackcontent) {
		this.feedbackcontent = feedbackcontent;
	}

	public Double getSelfscore() {
		return selfscore;
	}

	public void setSelfscore(Double selfscore) {
		this.selfscore = selfscore;
	}

	public List<AssessmentIndexDto> getChildren() {
		return children;
	}

	public void setChildren(List<AssessmentIndexDto> children) {
		this.children = children;
	}
	  
}
