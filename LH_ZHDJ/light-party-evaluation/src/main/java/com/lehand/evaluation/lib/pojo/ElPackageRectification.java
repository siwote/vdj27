package com.lehand.evaluation.lib.pojo;

import java.util.List;
import java.util.Map;

/**
 * 发布整改
 * @author Administrator
 *
 */
public class ElPackageRectification {

	private Long packageid;

    private Long compid;

    private String content;

    private String endtime;

    private String files;

    private Long code;

	public Long getPackageid() {
		return packageid;
	}

	public void setPackageid(Long packageid) {
		this.packageid = packageid;
	}

	public Long getCompid() {
		return compid;
	}

	public void setCompid(Long compid) {
		this.compid = compid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

    private List<Map<String, Object>> fileLists;

    public List<Map<String, Object>> getFileLists() {
        return fileLists;
    }

    public void setFileLists(List<Map<String, Object>> fileLists) {
        this.fileLists = fileLists;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }
}