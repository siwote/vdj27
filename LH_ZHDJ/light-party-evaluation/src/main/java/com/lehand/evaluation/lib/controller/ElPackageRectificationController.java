package com.lehand.evaluation.lib.controller;

import com.lehand.base.common.Message;
import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.business.ElPackageRectificationBusiness;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElPackageRectification;
import com.lehand.evaluation.lib.pojo.ElPackageRectificationAudit;
import com.lehand.evaluation.lib.pojo.ElPackageRectificationFeedback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * 考核整改
 */
@RestController
@RequestMapping("/evaluation/rectification")
public class ElPackageRectificationController extends ElBaseController{

	private static final Logger logger = LogManager.getLogger(ElPackageRectificationController.class);
	@Resource
    ElPackageRectificationBusiness elPackageRectificationBusiness;

    @RequestMapping("/getpackageinfo")
    private Message getElAssessFeedbackAudit(Long packageid,Long subjectid) {
        Message message = new Message();
        try {
            ElPackageRectification elPackageRectification=
                    elPackageRectificationBusiness.getElPackageRectification(packageid,subjectid,getSession());
            message.success().setData(elPackageRectification);
        } catch (Exception e) {
            message.setMessage("查询失败");
            logger.error("查询失败",e);
        }
        return message;

    }

	@RequestMapping("/save")
	private Message save(Long packageid, Long subjectid, String endtime,String content,String files) {
		Message message = new Message();
		try {
			elPackageRectificationBusiness.save(packageid,subjectid,endtime,content,files,getSession());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	@RequestMapping("/listpackage")
	private Message listElPackageRectification(String year) {
		Message message = new Message();
		try {
            List<ElAssess> data=
                    elPackageRectificationBusiness.listElPackageRectification (year,getSession());
			message.success().setData(data);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error("查询失败",e);
		}
		return message;

	}


    @RequestMapping("/savefeedback")
    private Message saveFeedBack(Long packageid, String files,String content,Long subjectid) {
        Message message = new Message();
        try {
            elPackageRectificationBusiness.saveFeedBack(packageid,files,content,subjectid,getSession());
            message.success();
        } catch (Exception e) {
            message.setMessage(e.getMessage());
            logger.error(e.getMessage(),e);
        }
        return message;
    }

    @RequestMapping("/getfeedback")
    private Message getFeedback(Long packageid, Long subjectid) {
        Message message = new Message();
        try {
            ElPackageRectificationFeedback data=
                    elPackageRectificationBusiness.getFeedback(packageid,subjectid,getSession());
            message.success().setData(data);
        } catch (Exception e) {
            message.setMessage("查询失败");
            logger.error("查询失败",e);
        }
        return message;

    }

    @RequestMapping("/audit")
    private Message audit(Long packageid, Long subjectid, Byte status, Byte result, String remark) {
        Message message = new Message();
        try {
            elPackageRectificationBusiness.audit(packageid,subjectid,status,result,remark,getSession());
            message.success();
        } catch (Exception e) {
            message.setMessage(e.getMessage());
            logger.error(e.getMessage(),e);
        }
        return message;
    }


    @RequestMapping("/listaudit")
    private Message listElPackageRectification(Long packageid, Long subjectid) {
        Message message = new Message();
        try {
            List<ElPackageRectificationAudit> data=
                    elPackageRectificationBusiness.getAudit (packageid,subjectid,getSession());
            message.success().setData(data);
        } catch (Exception e) {
            message.setMessage("查询失败");
            logger.error("查询失败",e);
        }
        return message;

    }

    @RequestMapping("/getEvalObjectList")
    public Message getEvalObjectList(Long packageid,String str,Integer flag) {
        Message msg = new Message();
        try {
            List<Map<String,Object>> data = elPackageRectificationBusiness.getEvalObjectList(packageid,str,flag,
                    getSession());
            msg.setData(data).success();
        } catch (Exception e) {
            msg.fail("获取汇总考核对象失败");
            logger.error(e.getMessage(),e);
            e.printStackTrace();
        }
        return msg;
    }

}
