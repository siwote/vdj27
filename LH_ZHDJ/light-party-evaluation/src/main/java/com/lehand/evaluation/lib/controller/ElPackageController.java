package com.lehand.evaluation.lib.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.ElPackageBusiness;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElAssessEvaluateCandidate;
import com.lehand.evaluation.lib.pojo.ElAssessItem;
import com.lehand.evaluation.lib.pojo.ElAssessObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;



/**
 * 体系控制层
 */
@RestController
@RequestMapping("/evaluation/pack")
public class ElPackageController extends ElBaseController{
	
	private static final Logger logger = LogManager.getLogger(ElPackageController.class);
	@Resource
	ElPackageBusiness elPackageBusiness;
	
	/**
	 * 
	 * @Title: savePackage
	 * @Description: 创建体系
	 * @Author dong
	 * @DateTime 2019年4月11日 上午10:56:13
	 * @param
	 */
	@RequestMapping("/savepack")
	private Message savePackage(String assess,String object,String evaluate,String type,String oldassessid) {
		Message message = new Message(); 
		try {
			List<Map<String,Object>> list = elPackageBusiness.savePackage(assess,object,evaluate,type,oldassessid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: saveItem
	 * @Description: 保存类别
	 * @Author dong
	 * @DateTime 2019年4月12日 上午9:42:13
	 * @param
	 */
	@RequestMapping("/saveitem")
	private Message saveItem(ElAssessItem item) {
		Message message = new Message();
		try {
			elPackageBusiness.saveItem(item,getSession());
			message.success().setData("保存成功");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: saveItem
	 * @Description: 修改类别
	 * @Author dong
	 * @DateTime 2019年4月12日 上午9:42:13
	 * @param
	 */
	@RequestMapping("/updateitem")
	private Message updateitem(ElAssessItem item) {
		Message message = new Message();
		try {
			elPackageBusiness.updateItem(item,getSession());
			message.success().setData("修改成功");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: saveItem
	 * @Description: 保存指标
	 * @Author dong
	 * @DateTime 2019年4月12日 下午7:45:57
	 * @param item
	 * @param evaluate
	 * @param
	 * @param rule
	 * @return
	 */
	@RequestMapping("/savetarget")
	private Message saveItemTarget(String item,String names,String evaluate,String evaluatetype,Short rule) {
		Message message = new Message();
		try {
			message = elPackageBusiness.saveItemTarget(item,getSession(),evaluate,evaluatetype,rule);
//			elPackageBusiness.updateItemTarget(item,evaluate,evaluatetype,rule,getSession());
//			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: updateItemTarget
	 * @Description: 修改指标
	 * @Author dong
	 * @DateTime 2019年4月13日 上午11:03:38
	 * @return
	 */
	@RequestMapping("/updatetarget")
	private Message updateItemTarget(String data /**ElAssessItem item,String evaluate,String evaluatetype,Short rule*/) {
		Message message = new Message();
		try {
			message = elPackageBusiness.updateItemTarget(data,getSession());
//			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
		
	}
	
	/**
	 * 
	 * @Title: releaseAssess
	 * @Description: 发布考核
	 * @Author dong
	 * @DateTime 2019年4月13日 下午2:42:34
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/release")
	private  Message releaseAssess(long assessid) {
		Message message = new Message();
		try {
			elPackageBusiness.releaseAssess(assessid,getSession());
			message.success().setData("发布成功");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: queryAssess
	 * @Description: 查询考核列表
	 * @Author dong
	 * @DateTime 2019年4月15日 上午10:19:26
	 * @param pager
	 * @return
	 */
	@RequestMapping("/queryassess")
	private Message queryAssess(Pager pager,String likeStr) {
		Message message = new Message();
		try {
			elPackageBusiness.queryAssess(pager,likeStr,getSession());
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error("查询失败",e);
		}
		return message;
		
	}
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 根据id获取考核
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:08:47
	 * @param assessid
	 * @return
	 */
	@RequestMapping("/getassess")
	private Message getAssess(long assessid) {
		Message message = new Message();
		try {
			ElAssess assess= elPackageBusiness.getAssess(assessid,getSession());
			message.success().setData(assess);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error("查询失败",e);
		}
		return message;
		
	}
	
	/**
	 * 
	 * @Title: getAssessObject
	 * @Description: 根据体系id获取考核对象
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:12:18
	 * @param
	 * @return
	 */
	@RequestMapping("/getobject")
	private Message getAssessObjectBy(long assessid) {
		Message message = new Message();
		try {
			List<ElAssessObject> obj= elPackageBusiness.getAssessObjectBy(assessid,getSession());
			message.success().setData(obj);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error("查询失败",e);
		}
		return message;
		
	}
	
	/**
	 * 
	 * @Title: getAssessEvaluateEvaluate
	 * @Description: 根据体系id获取评分者候选数据
	 * @Author dong
	 * @DateTime 2019年4月15日 上午11:26:08
	 * @param packageid
	 * @return
	 */
	@RequestMapping("/evaluate")
	private Message getAssessEvaluateCandidate(long packageid) {
		Message message = new Message();
		try {
			List<ElAssessEvaluateCandidate> candidate= elPackageBusiness.getAssessEvaluateCandidate(packageid,getSession());
			message.success().setData(candidate);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error("查询失败",e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: updatePackage
	 * @Description:修改考核 
	 * @Author dong
	 * @DateTime 2019年4月15日 下午1:43:14
	 * @param assess
	 * @param object
	 * @param evaluate
	 * @param type
	 * @param packageid
	 * @return
	 */
	@RequestMapping("/updatepack")
	private Message updatePackage(String assess,String object,String evaluate,String type,long packageid) {
		Message message = new Message(); 
		try {
			List<Map<String,Object>> list = elPackageBusiness.updatePackage(assess,object,evaluate,type,packageid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: deletePackage
	 * @Description: 删除考核体系
	 * @Author dong
	 * @DateTime 2019年4月15日 下午2:31:56
	 * @param packageid
	 * @return
	 */
	@RequestMapping("/deletepackage")
	private Message deletePackage(long packageid) {
		Message message = new Message(); 
		try {
			elPackageBusiness.deletePackage(packageid,getSession());
			message.success().setData("删除成功");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: copyAssess
	 * @Description: 复制考核体系
	 * @Author dong
	 * @DateTime 2019年4月15日 下午4:32:27
	 * @param
	 * @return
	 */
	@RequestMapping("/copyassess")
	private Message copyAssess(long oldassessid,long newpackageid) {
		Message message = new Message(); 
		try {
			elPackageBusiness.copyAssess(oldassessid,newpackageid,getSession());
			message.success().setData("复制成功");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: itemTree
	 * @Description: 类别树(创建体系)
	 * @Author dong
	 * @DateTime 2019年4月16日 上午11:50:34
	 * @param packageid
	 * @return
	 */
	@RequestMapping("/tree")
	private Message itemTree(long packageid) {
		Message message = new Message(); 
		try {
			List<Map<String, Object>> list = elPackageBusiness.itemTree(packageid,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	/**
	 * 
	 * @Title: deleteItem
	 * @Description: 删除类别或指标
	 * @Author dong
	 * @DateTime 2019年4月16日 下午12:27:26
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	private Message deleteItem (long id,Short type) {
		Message message = new Message(); 
		try {
			elPackageBusiness.deleteItem(id,type,getSession());
			message.success().setData("删除成功");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: getItem
	 * @Description: 根据pid查询指标列表
	 * @Author dong
	 * @DateTime 2019年4月16日 下午2:23:52
	 * @param pager
	 * @param pid
	 * @return
	 */
	@RequestMapping("/item")
	private Message getItem (Pager pager,long pid) {
		Message message = new Message(); 
		try {
			elPackageBusiness.getItem(pager,pid,getSession());
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: revokeAssess
	 * @Description: 撤销考核
	 * @Author dong
	 * @DateTime 2019年4月16日 下午9:03:54
	 * @return
	 */
	@RequestMapping("/revoke")
	private Message revokeAssess(long packageid) {
		Message message = new Message(); 
		try {
			elPackageBusiness.revokeAssess(packageid,getSession());
			message.success().setData("已撤销");
		} catch (Exception e) {
			message.setMessage("撤销失败");
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 
	 * @Title: move
	 * @Description: 类别指标拖拽
	 * @Author dong
	 * @DateTime 2019年4月23日 上午10:47:46
	 * @param srcid
	 * @param tagid
	 * @param flag
	 * @return
	 */
	@RequestMapping("/move")
	private Message move(long srcid, long tagid, int flag) {
		Message message = new Message(); 
		try {
			elPackageBusiness.move(srcid,tagid,flag,getSession());
			message.success().setData("已修改");
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
		
	}
	
}
