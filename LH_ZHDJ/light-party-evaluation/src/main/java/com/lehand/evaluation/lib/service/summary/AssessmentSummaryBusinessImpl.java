package com.lehand.evaluation.lib.service.summary;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.business.summary.AssessmentSummaryBusiness;
import com.lehand.evaluation.lib.common.modulemethod.FilePreviewAndDownload;
import com.lehand.evaluation.lib.dto.AccountInfoDto;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackFile;
import com.lehand.evaluation.lib.pojo.dl.DlFile;
import com.lehand.evaluation.lib.service.message.MessageBusinessImp;
import com.lehand.evaluation.lib.util.NumUtil;
import com.lehand.horn.partyorgan.constant.SqlCode;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Value;

import com.lehand.base.common.Message;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.FileUtil;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.components.dfs.DFSComponent;
import com.lehand.evaluation.lib.common.constant.Constant;
import com.lehand.evaluation.lib.common.constant.ResultSelfEvalSqlCfg;
import com.lehand.evaluation.lib.common.constant.SummarySqlCfg;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import org.springframework.util.StringUtils;

public class AssessmentSummaryBusinessImpl implements AssessmentSummaryBusiness {
	
	//上传路径
	@Value("${spring.http.multipart.location}")
	private String location;

    @Resource
    FilePreviewAndDownload filePreviewAndDownload;
	
	@Resource GeneralSqlComponent sqlComponent;

	@Resource private DFSComponent dFSComponent;

    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;
	/**
	 * 	体系列表(所有)
	 *	getSummSystemList
	 *	AssessmentSummaryImpl
	 *	@param year
	 *	@param
	 *	@return
	 *	@throws Exception
	 *	Tangtao
	 *	2019年4月16日
	 */
	@Override
	public List<Map<String,Object>> getSummSystemList(String year, Session session) throws Exception {
		List<Map<String,Object>> list = sqlComponent.query(SummarySqlCfg.EvaGetAllPackage, new Object[] {year,session.getCompid()});		
		return list;
	}

	/**
	 * 	汇总数据
	 *	getSummData
	 *	AssessmentSummaryImpl
	 *	@param ObjectCode
	 *	@param packageid 考核对象
	 *	@param compid
	 *	@return
	 *	@throws Exception
	 *	Tangtao
	 *	2019年4月16日
	 */
	private List<SummInfoDto> getSummFormData(Long ObjectCode, Long packageid, Long compid) throws Exception {
		List<SummInfoDto> list = new ArrayList<SummInfoDto>();
		Double /* typeScoreSum = 0, */ itemTotalScoreSum = (double) 0, /* itemScoreSum = 0, */selfScoreSum = (double) 0,scoreSum = (double) 0;
		//考核对象列表
		List<Map<String,Object>> categoryArr = sqlComponent.query(SummarySqlCfg.EvaGetCategory, new Object[] {compid,packageid});
		if(categoryArr.size() < 1) {
			return list;
		}
		int i = 1;
		for(Map<String,Object>  cat : categoryArr) {
			//一级指标
			List<Map<String,Object>> itemOneArr = sqlComponent.query(SummarySqlCfg.EvaGetItemOne, new Object[] {compid,cat.get("id")});
			if(itemOneArr.size()<1) {
				SummInfoDto map = new SummInfoDto();
				map.setTypeName(cat.get("name")!=null?(String) cat.get("name"):Constant.EMPTY+"("+cat.get("score")!=null?(String) cat.get("score"):Constant.EMPTY+")");				
				map.setNumber(String.valueOf(i));
				map.setItemTotalName(Constant.EMPTY);
				map.setItemTotalScore(Constant.EMPTY);
				map.setName(Constant.EMPTY);
				map.setFeedbackcontent(Constant.EMPTY);
				map.setSelfscore(null);
				map.setScore(null);
				map.setContent(Constant.EMPTY);
				map.setFilenames(null);
				map.setFilenamesforexp(Constant.EMPTY);
				map.setLvOne("");
				map.setLvTwo(Constant.EMPTY);
//				map.setCellStyleMap(cellStyleMap());
				list.add(map);				
			}else {
				for(Map<String,Object> item :itemOneArr) {
					//二级指标
					List<Map<String,Object>> itemTwoArr = sqlComponent.query(SummarySqlCfg.EvaGetItemTwo, new Object[] {item.get("id"),compid,ObjectCode});
					if(itemTwoArr.size()<1) {
						SummInfoDto map = new SummInfoDto();
						map.setTypeName(cat.get("name")!=null?(String) cat.get("name"):Constant.EMPTY+"("+cat.get("score")!=null?(String) cat.get("score"):Constant.EMPTY+")");
						map.setNumber(String.valueOf(i));
						map.setItemTotalName(item.get("name")!=null?item.get("name").toString():Constant.EMPTY);
						map.setItemTotalScore(item.get("score")!=null?item.get("score").toString():Constant.EMPTY);
						map.setLvOne(item.get("id")!=null?item.get("id").toString():Constant.EMPTY);
						map.setName(Constant.EMPTY);
						map.setFeedbackcontent(Constant.EMPTY);
						map.setSelfscore(null);
						map.setScore(null);
						map.setContent(Constant.EMPTY);
						map.setFilenames(null);
                        map.setListFiles(new ArrayList<>());
						map.setFilenamesforexp(Constant.EMPTY);
						map.setLvTwo(Constant.EMPTY);
//						map.setCellStyleMap(cellStyleMap());
						list.add(map);
					}else {
						for(Map<String,Object> ls : itemTwoArr) {
							SummInfoDto map = new SummInfoDto();
							List<Map<String,Object>> proofs = getProofs((long) ls.get("id"),ObjectCode,compid);
						    String expProofs = GetroofsforExport(proofs);
							map.setTypeName(cat.get("name")!=null?(String) cat.get("name"):Constant.EMPTY+"("+cat.get("score")!=null?(String) cat.get("score"):Constant.EMPTY+")");
							map.setNumber(String.valueOf(i));
							map.setItemTotalName(item.get("name")!=null?item.get("name").toString():Constant.EMPTY);
							map.setItemTotalScore(item.get("score")!=null?item.get("score").toString():Constant.EMPTY);							
							map.setName(ls.get("name")!=null?ls.get("name").toString():Constant.EMPTY);
							map.setFeedbackcontent(ls.get("feedbackcontent")!=null?ls.get("feedbackcontent").toString():Constant.EMPTY);
							map.setSelfscore(ls.get("selfScore")!=null?ls.get("selfScore").toString():null);
							map.setScore(ls.get("score")!=null?ls.get("score").toString():null);
							map.setContent(ls.get("content")!=null?ls.get("content").toString():Constant.EMPTY);
							map.setFilenames(getProofs((long) ls.get("id"),ObjectCode,compid));
							map.setLvTwo(ls.get("id")!=null?ls.get("id").toString():Constant.EMPTY);
							map.setFilenames( proofs);
                            map.setListFiles(filePreviewAndDownload.listFile((String) ls.get("files"),compid));
							map.setFilenamesforexp(expProofs);
							map.setLvTwo(ls.get("id")!=null?ls.get("id").toString():Constant.EMPTY);
//							map.setCellStyleMap(cellStyleMap());
							list.add(map);
//							itemScoreSum += Double.parseDouble(ls.get("itemscore").toString());
							selfScoreSum += Double.parseDouble(ls.get("selfScore")!=null?ls.get("selfScore").toString():"0");
							scoreSum += Double.parseDouble(ls.get("score")!=null?ls.get("score").toString():"0");
//							i++;
						}
					}
					i++;
					itemTotalScoreSum += Double.parseDouble(item.get("score")!=null?item.get("score").toString():"0");
				}
			}
			
//			typeScoreSum += Double.parseDouble(cat.get("score").toString());
		}
		SummInfoDto sum = new SummInfoDto();
		sum.setTypeName("汇总");
		sum.setNumber("-");
		sum.setItemTotalName("-");
		sum.setItemTotalScore(String.valueOf(NumUtil.tonumber1(itemTotalScoreSum)));
		sum.setName("-");
		sum.setFeedbackcontent("-");
		sum.setSelfscore(String.valueOf(NumUtil.tonumber1(selfScoreSum)));
		sum.setScore(String.valueOf(NumUtil.tonumber1(scoreSum)));
		sum.setContent("-");
		sum.setFilenames(null);
        sum.setListFiles(new ArrayList<>());
		sum.setFilenamesforexp("-");
//		sum.setCellStyleMap(cellStyleMap());
		list.add(sum);
		return list;
	}


	/**
	 *
	 *	@param
	 *	@param objectCode
	 * @param compid 
	 *	@return
	 *	Object
	 *	2019年4月17日
	 *	Tangtao
	 */
	private List<Map<String,Object>> getProofs(Long id, Long objectCode, Long compid) {
		List<Map<String,Object>> proofList = new ArrayList<Map<String,Object>>();
		List<ElAssessFeedbackFile> folderList =sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalForders,new Object[]{id,compid});
		if(folderList.size()<1) {
			return proofList;
		}
		
		List<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		if(fileList.size()<1) {			
			for(ElAssessFeedbackFile folder:folderList) {
				fileList = sqlComponent.query(ResultSelfEvalSqlCfg.ResultSelfEvalFiles,new Object[]{compid,folder.getId()});
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("id", folder.getId());
				map.put("compid", compid);
				map.put("name", folder.getName());
				map.put("createtime", folder.getCreatetime());
				map.put("creator", folder.getUserid());
				map.put("checkedArr", folder.getRemark3());
				map.put("year", folder.getRemark4());
				if(fileList.size()>0) {					
					for(Map<String,Object> ms :fileList) {
						ms.put("checked", true);
					}
				}
				map.put("children", fileList);
				proofList.add(map);			}		
		}

		return proofList;
	}

	@Override
	public Message getEvalObjectList(Long packageid, String str, Long compid) {
		Message msg = new Message();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("packageid", packageid);
		map.put("compid", compid);
		map.put("likeStr", str);
		List<Map<String,Object>> data = sqlComponent.query(SummarySqlCfg.EvaGetObjects,map);
		msg.success();
		msg.setData(data);
		return msg;
	}

	
	
	@Override
	public Map<String, Object> getSummData(Long ObjectCode, Long packageid, Long compid) throws Exception {
		Map<String,Object> map  = new HashMap<String,Object>();
		List<SummInfoDto> list =  new ArrayList<SummInfoDto>();
		String title = Constant.EMPTY;
		map.put("title", title);
		map.put("data", list);
		try {
			title = getTitle(ObjectCode,packageid,compid);	
			map.put("title", title);
		} catch (Exception e) {
			LehandException.throwException("获取表头数据失败",e);
		}
		try {			
			list = getSummFormData(ObjectCode,packageid,compid);
			map.put("title", title);
			map.put("data", list);
		} catch (Exception e) {
			LehandException.throwException("获取汇总表数据失败",e);
			return map;
		}
		return map;
	}

	/**
	 *	表头数据
	 *	@param ObjectCode
	 *	@param packageid
	 *	@param compid
	 *	@return
	 *	String
	 *	2019年4月28日
	 *	Tangtao
	 */
	private String getTitle(Long ObjectCode, Long packageid, Long compid) {
		String title = Constant.EMPTY;
		//考核体系数据
		Map<String,Object>  assessInfo  = new HashMap<String,Object>();
		try {
			assessInfo= sqlComponent.query(ResultSelfEvalSqlCfg.getAssessInfoByItemReciver, new Object[] {compid,ObjectCode,packageid});			
		} catch (Exception e) {
			LehandException.throwException("获取考核信息失败",e);	
			return title;
		}
		
		//获取考核对象信息
		AccountInfoDto acc =  new  AccountInfoDto();
		try {			
//			acc = messageBusiness.getAccountByOrgid(ObjectCode, compid);
			Map<String,Object> data = sqlComponent.query(SqlCode.getTDzzInfo2,new Object[]{ObjectCode});
			title = data.get("dzzmc").toString();
		} catch (Exception e) {
			LehandException.throwException("获取考核对象信息失败",e);	
			return title;
		}
		//考核对象数据
		StringBuffer buffer =  new StringBuffer(title);
		buffer.append("-");
		buffer.append(assessInfo.get("name"));
		buffer.append("汇总表");
		title = buffer.toString();
		return title;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Message exportData(Long packageid,String str, Session session) {
		Message msg = new Message();
		List<Map<String,Object>> data = new ArrayList<Map<String,Object>>();
		try {
			 data = getExcelData(packageid,str,session.getCompid());			
		} catch (Exception e) {
			LehandException.throwException("获取汇总数据异常",e);
			msg.fail("获取汇总数据异常");
			e.printStackTrace();
			return msg;
		}
		if(data.size()<1) {
			msg.success();
			msg.setMessage("无数据");
			return msg;
		}
		Workbook workbook = null;
		List<Map<String, Object>> sheetsList = new ArrayList<>();
		int i =1;
		for(Map<String,Object> m :data) {
			List<SummInfoDto> exportList = (List<SummInfoDto>) m.get("data");
			// 创建参数对象（用来设定excel得sheet得内容等信息）
            ExportParams deptExportParams = new ExportParams();
//            deptExportParams.setSheetName(m.get("name").toString());
            deptExportParams.setSheetName("sheet"+i);
            deptExportParams.setTitle(m.get("name").toString()+"-"+m.get("packagename").toString());
         // 创建sheet1使用得map
            Map<String, Object> deptExportMap = new HashMap<>();
            // title的参数为ExportParams类型，目前仅仅在ExportParams中设置了sheetName
            deptExportMap.put("title", deptExportParams);
            // 模版导出对应得实体类型
            deptExportMap.put("entity", SummInfoDto.class);
            // sheet中要填充得数据
            deptExportMap.put("data", exportList);
            
            sheetsList.add(deptExportMap);
            i++;
		}
		// 执行方法
        workbook = ExcelExportUtil.exportExcel(sheetsList, ExcelType.HSSF);
 
		  File dirFile = new File(location);

		  if (!dirFile.exists()) {
		   dirFile.mkdir();
		  }
		  File efile = new File(location+"/考核汇总-" +DateEnum.YYYYMMDD.format()+"-"+new Random().nextInt(9999) +".xls");
		  FileOutputStream fos = null;
		  String dir = "";
		  try {
		   fos = new FileOutputStream(efile);
		   workbook.write(fos);
		   //获取文件上传后返回的路径
		   dir=dFSComponent.upload(efile.getPath());
		  } catch (Exception e) {
//			  throw new UncheckedException("导出excel失败", e);
		  } finally {
		   if (null != fos) {
		    try {
		     fos.close();
		    } catch (IOException e) {
//		     throw new UncheckedException("导出excel失败", e);
		    }
		   }
		  }
//		  String path=dir+"?attname="+efile.getName()+"&download=true";
		  msg.success();
		  msg.setData(downIp+dir+"?attname="+efile.getName()+"&download=true");
		  try {
			 //文件保存到数据库
			saveDlFile(dir,efile,session);
		} catch (IOException e) {
			LehandException.throwException("文件保存失败",e);
			msg.fail("文件保存失败");
		}
		return msg;		
	}
	
	/**
	 *	根据体系获取所有考核对象汇总数据
	 *	@param packageid
	 *	@param str
	 *	@param compid
	 *	@return
	 *	@throws Exception
	 *	List<Map<String,Object>>
	 *	2019年4月29日
	 *	Tangtao
	 */
	private List<Map<String,Object>> getExcelData(Long packageid, String str, Long compid) throws Exception {
		Message message = getEvalObjectList(packageid, str, compid);
		List<Map<String,Object>> objectList = (List<Map<String, Object>>) message.getData();
		List<Map<String,Object>> data = new ArrayList<Map<String,Object>>();
		if(objectList.size()<1) {
			return data;
		}
		
		for(Map<String,Object> obj:objectList) {
			Map<String,Object> map =   getSummData((long) obj.get("code"), packageid, compid);
			map.put("name", obj.get("name"));
			map.put("packagename", obj.get("packagename"));
			data.add(map);
		}
		return data;
	}
	
//	@SuppressWarnings("null")
//	private Map<Integer, CellStyle> cellStyleMap(){
//		Map<Integer, CellStyle> cellMap = new HashMap<Integer,CellStyle>();
//		CellStyle style = null;
//		style.setLeftBorderColor((short) 0);
//		style.setRotation((short) 0);
//		cellMap.put(1, style);
//		return cellMap;		
//	};
	
	/**
	 *	佐证材料导出
	 *	@param proofs
	 *	@return
	 *	String
	 *	2019年4月30日
	 *	Tangtao
	 */
	private String GetroofsforExport(List<Map<String, Object>> proofs) {
		StringBuffer proofStr =  new StringBuffer();
		if(proofs.size()<1) {
			return proofStr.toString();
		}
		int i = 0;
		for(Map<String,Object> m : proofs) {
			i++;
			proofStr.append(i+"."+m.get("name")+"\n");
			@SuppressWarnings("unchecked")
			List<Map<String,Object>> files = (List<Map<String, Object>>) m.get("children");
			if(files.size()>0) {
				int j = 0;
				for(Map<String,Object> f :files) {
					j++;
					proofStr.append("   ("+j+")"+f.get("name")+"\n");
				}
			}
		}
		return proofStr.toString();
	}
	
	private void  saveDlFile(String dir, File efile,Session session) throws IOException {
		DlFile dlFile=new DlFile();
		dlFile.setDir(dir);	//路径
		dlFile.setName(efile.getName());//文件名称
		dlFile.setSize(FileUtil.sizeOf(efile));//文件大小
		dlFile.setType(efile.getName().substring(efile.getName().lastIndexOf(".")+1));//文件类型
		dlFile.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());//创建时间
		dlFile.setCompid(session.getCompid());//租户id
		dlFile.setUsertor(session.getUserid());
		dlFile.setRemark(StringConstant.EMPTY);//备注
		dlFile.setRemark1(0);//备注1   用来判断是否是上传的文件，
		dlFile.setRemark2(1);//备注2  1为保存的导出文件
		dlFile.setRemark3(StringConstant.EMPTY);//备注3
		dlFile.setRemark4(StringConstant.EMPTY);//备注4
		dlFile.setRemark5(StringConstant.EMPTY);//备注5
		dlFile.setRemark6(StringConstant.EMPTY);//备注6
		//新增文件数据
		sqlComponent.insert(SummarySqlCfg.addDlFile, dlFile);
	}
}
