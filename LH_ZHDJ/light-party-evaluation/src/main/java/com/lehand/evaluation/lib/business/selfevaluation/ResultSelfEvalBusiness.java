package com.lehand.evaluation.lib.business.selfevaluation;

import java.util.List;
import java.util.Map;

import com.lehand.base.common.Session;
import org.springframework.stereotype.Service;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;

/**
 *	@author Tangtao
 *	考核结果自评service层
 */
@Service
public interface ResultSelfEvalBusiness {
	/**
	 *	获取考核结果自评列表
	 *	@param str
	 * @param
	 * @param session 
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月10日
	 *	Tangtao
	 */
	public Message pageQuery(Pager pager,String str,Short status, Session session) throws Exception;
	
	/**
	 *	签收考对象签收
	 *	@param
	 *	@return
	 *	@throws Exception
	 *	Integer
	 *	2019年4月10日
	 *	Tangtao
	 */
	public Message signForEvalSystem(Long packageid,Session session) throws Exception;
	
	/**
	 *	自评页面信息
	 *	@param id
	 *	@param compid
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月12日
	 *	Tangtao
	 */
	public Message formInfo(Long id,Long compid,String code) throws Exception;
	
	/**
	 *	保存考核反馈
	 * @param packageid 
	 * @param
	 *	@return
	 *	@throws Exception
	 *	boolean
	 *	2019年4月10日
	 *	Tangtao
	 */
	public Message saveSelfEvalResult(Long id,Double selfscore,String feedbackcontent,Long itemid,Long code,List<Map<String,Object>> list,Session session, Long packageid,String files)throws Exception;
	
	/**
	 *	上报自评
	 *	@param
	 *	@return
	 *	@throws Exception
	 *	Boolean
	 *	2019年4月11日
	 *	Tangtao
	 */
	public Message reportSelfEval(Long packageid,Session session) throws Exception;
	
	/**
	 *	自评指标树
	 * @param type 
	 *	@param
	 *	@param
	 *	@param
	 *	@return
	 *	@throws Exception
	 *	Message
	 *	2019年4月11日
	 *	Tangtao
	 */
	public List<Map<String,Object>> getSelfEvalSystemList(Long packageid,Long compid,Long orgid,Long currOrgid ,
                                                          String type) throws Exception;
	
	/**
	 *	底层指标列表
	 *	@param id
	 *	@param compid
	 *	@return
	 *	@throws Exception
	 *	List<Map<String,Object>>
	 *	2019年4月15日
	 *	Tangtao
	 */
	public List<Map<String,Object>> getSelfEvalSystemListLast (Long id,Long compid,Long orgid,Long userid) throws Exception;
	
	
	
	/**
	 *	他评指标列表
	 *	@param orgid
	 *	@param compid
	 *	@return
	 *	@throws Exception
	 *	Map<String,Object>
	 *	2019年4月17日
	 *	Tangtao
	 */
	public List<Map<String, Object>> getEvalSystemListLast(Long id, Long compid, Long orgid,Long currOrgid);
	
	public Map<String,Object> getSelStatus(Long compid,Long code) throws Exception;

}
