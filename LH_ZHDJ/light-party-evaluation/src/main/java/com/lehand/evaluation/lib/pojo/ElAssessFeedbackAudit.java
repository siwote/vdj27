package com.lehand.evaluation.lib.pojo;

/**
 * 自评审核
 * @author Administrator
 *
 */
public class ElAssessFeedbackAudit {

	private Long id;

    private Long compid;

    private Long packageid;

    private Long subjectid;

    private Byte status;

    private String auditcontent;

    private Long auditid;

    private Long modiler;

    private String createtime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompid() {
		return compid;
	}

	public void setCompid(Long compid) {
		this.compid = compid;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getAuditcontent() {
		return auditcontent;
	}

	public void setAuditcontent(String auditcontent) {
		this.auditcontent = auditcontent;
	}

	public Long getAuditid() {
		return auditid;
	}

	public void setAuditid(Long auditid) {
		this.auditid = auditid;
	}

	public Long getModiler() {
		return modiler;
	}

	public void setModiler(Long modiler) {
		this.modiler = modiler;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public Long getPackageid() {
		return packageid;
	}

	public void setPackageid(Long packageid) {
		this.packageid = packageid;
	}

    public Long getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Long subjectid) {
        this.subjectid = subjectid;
    }
}