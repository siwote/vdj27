package com.lehand.evaluation.lib.business;

import java.util.List;
import java.util.Map;

import com.lehand.base.common.Session;
import com.lehand.evaluation.lib.pojo.ElAssess;

public interface ElpackageRankBusiness {
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 获取考核列表
	 * @Author dong
	 * @DateTime 2019年4月17日 下午3:23:00
	 * @return
	 */
	public List<ElAssess> getAssess(Short yaer, Session session);
	
	/**
	 * 
	 * @Title: orgRank
	 * @Description: 获取党组织考核结果排名
	 * @Author dong
	 * @DateTime 2019年4月17日 下午5:25:06
	 * @param assessid
	 * @param session
	 * @return
	 */
	public List<Map<String,Object>> orgRank(String assessid,Session session);
	
	/**
	 * 
	 * @Title: itemRank
	 * @Description: 评分标准扣分次数结果排名
	 * @Author dong
	 * @DateTime 2019年4月19日 下午4:11:24
	 * @param assessid
	 * @return
	 */
	public List<Map<String,Object>> itemRank(String assessid,Session session);
	
	/**
	 * 
	 * @Title: itemRank2
	 * @Description: 指标扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月19日 下午5:37:55
	 * @param assessid
	 * @param session
	 * @return
	 */
	public List<Map<String,Object>> itemRank2(String assessid,Session session);
	
	/**
	 * 
	 * @Title: itemRank3
	 * @Description: 类别扣分次数排名
	 * @Author dong
	 * @DateTime 2019年4月22日 下午9:58:55
	 * @param assessid
	 * @return
	 */
	public List<Map<String,Object>> itemRank3(String assessid, Session session);

	/**
	 * @Title: ranking
	 * @Description: 考核排名总接口
	 * @author huruohan
	 * @DateTime 2019年4月23日11:29:44
	 * @param assessid
	 * @param type
	 * @param session
	 * @return
	 */
	public Map<String, Object> rank(String assessid, int type, Session session);

}
