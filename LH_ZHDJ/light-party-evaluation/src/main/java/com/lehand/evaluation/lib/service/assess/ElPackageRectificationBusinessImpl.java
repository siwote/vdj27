package com.lehand.evaluation.lib.service.assess;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.business.ElPackageRectificationBusiness;
import com.lehand.evaluation.lib.common.modulemethod.FilePreviewAndDownload;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.evaluation.lib.pojo.ElPackageRectification;
import com.lehand.evaluation.lib.pojo.ElPackageRectificationAudit;
import com.lehand.evaluation.lib.pojo.ElPackageRectificationFeedback;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElPackageRectificationBusinessImpl implements ElPackageRectificationBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	@Resource
    FilePreviewAndDownload filePreviewAndDownload;


    @Override
    public ElPackageRectification getElPackageRectification(Long packageid,Long subjectid, Session session) {
        ElPackageRectification data = getElPackageRectificationInfo(packageid, subjectid, session.getCompid());
        if(data!=null){
            data.setFileLists(filePreviewAndDownload.listFile(data.getFiles(),session.getCompid()));
        }
        return data;
    }

    private ElPackageRectification getElPackageRectificationInfo(Long packageid, Long subjectid,  Long compid) {
        return generalSqlComponent.getDbComponent().getBean(
                "select * from el_package_rectification where packageid=? and code=? and compid=?",
                ElPackageRectification.class, new Object[]{packageid,subjectid,
                        compid});
    }

    @Override
    public void save(Long packageid,Long subjectid, String endtime, String content, String files, Session session) {
        // 校验整改信息是否已经存在如果存在就修改(防止出现重复提交这里就不返回错误信息给前端直接修改)
        ElPackageRectification data = getElPackageRectificationInfo(packageid,subjectid,session.getCompid());
        if(data!=null){
            generalSqlComponent.getDbComponent().update(
                    "UPDATE el_package_rectification set content=?, endtime=?, files=? where compid=? and packageid=?" +
                            " and code=?",
                    new Object[]{content,endtime,files,session.getCompid(),packageid,subjectid});
            return;
        }
        generalSqlComponent.getDbComponent().insert(
                "INSERT INTO el_package_rectification (compid, packageid,code, content, endtime, files) VALUES (?,?," +
                        "?,?,?,?)",
                new Object[]{session.getCompid(),packageid,subjectid,content,endtime,files});
    }

    @Override
    public List<ElAssess> listElPackageRectification(String year, Session session) {
        String sql = "select * from (" +
                "select * ,0 flag from el_assess where compid=:compid and year=:year and " +
                "orgid=:orgid and ( status= 2 or status= 3 ) " +
                "union all " +
                "select b.*,1 flag from el_assess_object a left join el_assess b on a.compid=b.compid and a.assessid=b.id  " +
                "where a.compid=:compid and b.year=:year and a.code=:orgid and ( b.status= 2 or b.status = 3 ) " +
                ") c order by c.createtime desc";
        Map<String,Object> param = new HashMap<>();
        param.put("compid",session.getCompid());
        param.put("year",year);
        param.put("orgid",session.getCurrentOrg().getOrgcode().substring(1));
        List<ElAssess> datas = generalSqlComponent.getDbComponent().listBean(sql, ElAssess.class, param);
        return datas;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void saveFeedBack(Long packageid, String files, String content, Long subjectid, Session session) {
        ElPackageRectificationFeedback data = generalSqlComponent.getDbComponent().getBean(
                "select * from el_package_rectification_feedback where compid=? and packageid=? and code=? limit 1",
                ElPackageRectificationFeedback.class,new Object[]{session.getCompid(),packageid,subjectid});
        if(data==null){
            generalSqlComponent.getDbComponent().insert("INSERT INTO el_package_rectification_feedback (compid, packageid, code, content, files) VALUES (?,?,?,?,?)",
                    new Object[]{session.getCompid(),packageid,subjectid,content ,files});
        }else{
            generalSqlComponent.getDbComponent().update("UPDATE el_package_rectification_feedback " +
                            "set content=?,files=? where compid=? and packageid=? and code=?",
                    new Object[]{content ,files,session.getCompid(),packageid,subjectid});
        }
        generalSqlComponent.getDbComponent().insert("INSERT INTO el_package_rectification_audit (compid, packageid, " +
                        "code, content, files, remark, status, results,createtime) VALUES (?,?,?,?,?,?,?,?,?)",
                new Object[]{session.getCompid(),packageid,subjectid,content ,files,"",0,0,DateEnum.YYYYMMDDHHMMDD.format()});
    }

    @Override
    public void audit(Long packageid, Long subjectid, Byte status, Byte result, String remark, Session session) {
        generalSqlComponent.getDbComponent().update("update el_package_rectification_audit set status=?,results=?," +
                        "remark=? where packageid=? and code=? and compid=? and status=0",
                new Object[]{status,result,remark,packageid,subjectid,session.getCompid()});
    }

    @Override
    public ElPackageRectificationFeedback getFeedback(Long packageid, Long subjectid, Session session) {
        ElPackageRectificationFeedback data = generalSqlComponent.getDbComponent().getBean("select * from " +
                        "el_package_rectification_feedback where " +
                        "compid=? and packageid=? and code=? ",
                ElPackageRectificationFeedback.class,
                new Object[]{session.getCompid(),packageid,subjectid});
        if(data!=null){
            data.setFileLists(filePreviewAndDownload.listFile(data.getFiles(),session.getCompid()));
        }
        return data;
    }

    @Override
    public List<ElPackageRectificationAudit> getAudit(Long packageid, Long subjectid, Session session) {
        List<ElPackageRectificationAudit> datas = generalSqlComponent.getDbComponent().listBean("select * from " +
                        "el_package_rectification_audit where " +
                        "compid=? and packageid=? and code=? order by createtime desc ",
                ElPackageRectificationAudit.class,
                new Object[]{session.getCompid(),packageid,subjectid});
        if(datas!=null&&datas.size()>0){
            datas.forEach(a->{
                a.setFileLists(filePreviewAndDownload.listFile(a.getFiles(),session.getCompid()));
            });
        }
        return datas;
    }

    @Override
    public List<Map<String,Object>> getEvalObjectList(Long packageid, String str,Integer flag,Session session) {
        Long compid =  session.getCompid();
        Map<String,Object> map = new HashMap<>();
        map.put("packageid", packageid);
        map.put("compid", compid);
        map.put("likeStr", str);
        map.put("code", session.getCurrentOrg().getOrgcode().substring(1));
        List<Map<String,Object>> data = null;
        String sql = "select a.code, a.name as dzzqc, d.sorgname as name, b.packagename, b.packageid from el_assess_object a\n" +
                "inner join el_assess b on a.assessid = b.id\n" +
                "inner join el_evaluate_receive c on c.packageid = b.packageid\n" +
                "left join urc_organization d on d.orgcode = a.code\n" +
                "where a.compid = :compid and b.packageid = :packageid ";
        if(flag==0){
            data = generalSqlComponent.getDbComponent().listMap(sql+" group by a.code",map);
        }else{
            data = generalSqlComponent.getDbComponent().listMap(sql+" and a.code=:code",map);
        }
        if(data!=null&&data.size()>0){
            data.forEach(a->{
                ElPackageRectificationAudit epra = generalSqlComponent.getDbComponent().getBean(
                        "select * from el_package_rectification_audit where compid=? and packageid=? and code=? order" +
                                " by createtime desc limit 1",
                        ElPackageRectificationAudit.class,new Object[]{compid,packageid,a.get("code")});
                //判断体系是否发送整改了
               if(getElPackageRectificationInfo(packageid,Long.valueOf(a.get("code").toString()),session.getCompid())==null){
                   a.put("status",4);
                }else if(epra==null){//没有查询到审核相关信息说明还没有反馈
                    a.put("status",3);
                }else if(epra!=null){
                    a.put("status",epra.getStatus());
                }
            });
        }
        return data;
    }
}
