package com.lehand.evaluation.lib.service.assess;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.business.ElPackageResultBusiness;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;

public class ElPackageResultBusinessImpl implements ElPackageResultBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 获取考核列表 考核者 和 被考核者
	 * @Author dong
	 * @DateTime 2019年4月16日 下午8:03:00
	 * @return
	 */
	@Override
	public List<Map<String,Object>> getAssess(Short year, String likeStr, Session session){
//		generalSqlComponent.pageQuery(ComesqlElCode.getAssessResultListType, new Object[] {session.getOrganids().get(0).getOrgid(),session.getCompid(),year,"%"+likeStr+"%",session.getOrganids().get(0).getOrgid(),session.getCompid(),year,"%"+likeStr+"%"},pager);
		Map<String,Object> map = new HashMap<String,Object>(4);
		map.put("orgid", session.getCurrentOrg().getOrgcode());
		map.put("compid", session.getCompid());
		map.put("year", year);
		map.put("likestr", likeStr);
		map.put("code", session.getCurrentOrg().getOrgcode());
		System.out.println("**********************"+map);
		List<Map<String,Object>> list = generalSqlComponent.query(ComesqlElCode.getAssessResultListType, map);
		return list;
	}
	
	/**
	 * 
	 * @Title: getItemScore
	 * @Description: 指标 自评分  他评分
	 * @Author dong
	 * @DateTime 2019年4月16日 下午8:32:30
	 * @param
	 * @param pid
	 * @param
	 * @return
	 */
	@Override
	public Pager queryItemScore(Pager pager,long pid,long objid,Session session) {
		Map<String,Object>  map = new HashMap<String, Object>();
		map.put("pid", pid);
		map.put("type", 1);
		map.put("compid", session.getCompid());
		map.put("code", objid);
		generalSqlComponent.pageQuery(ComesqlElCode.getItemScoreType, map, pager);
		return pager;
		
	}
//	public  Pager  getItemScore(Pager pager,long assessid,long objId,long pid,String likeStr,Session session){
//		Map<String,Object> mapquery = new HashMap<String,Object>(3);
//		mapquery.put("objId", objId);
//		mapquery.put("compid", session.getCompid());
//		mapquery.put("likestr", likeStr);
//		mapquery.put("pid", pid);
//		
//		generalSqlComponent.pageQuery(ComesqlElCode.getItemListResultType, mapquery, pager);
//		List<ElAssessItem> list = (List<ElAssessItem>) pager.getRows();
//		Map<String,Object> map;
//		List<Map<String,Object>> list2 = new ArrayList<Map<String,Object>>();
//		for(ElAssessItem li:list) {
//			double selfScore = generalSqlComponent.query(ComesqlElCode.getObjectScoreSelfType, new Object[] {assessid,objId,session.getCompid()});
//			double onthScore = generalSqlComponent.query(ComesqlElCode.getObjectScoreOthType, new Object[] {assessid,objId,session.getCompid()});
//			map = new HashMap<String,Object>();
//			map.put("id", li.getId());
//			map.put("name", li.getName());
//			map.put("assessid", assessid);
//			map.put("packageid", li.getPackageid());
//			map.put("selfScore", selfScore);
//			map.put("onthScore", onthScore);
//			list2.add(map);
//			pager.setRows(list2);
//		}
//		return pager;
//		
//	}
}
