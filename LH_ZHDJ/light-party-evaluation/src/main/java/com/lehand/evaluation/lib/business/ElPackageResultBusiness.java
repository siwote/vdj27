package com.lehand.evaluation.lib.business;

import java.util.List;
import java.util.Map;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;

public interface ElPackageResultBusiness {
	
	/**
	 * 
	 * @Title: getAssess
	 * @Description: 获取考核列表 考核者 和 被考核者
	 * @Author dong
	 * @DateTime 2019年4月16日 下午8:03:00
	 * @return
	 */
	public List<Map<String,Object>> getAssess(Short year,String likeStr,Session session);
	
	/**
	 * 
	 * @Title: getItemScore
	 * @Description: 指标 自评分  他评分
	 * @Author dong
	 * @DateTime 2019年4月16日 下午8:32:30
	 * @param
	 * @param pid
	 * @param
	 * @return
	 */
	public Pager queryItemScore(Pager pager, long pid, long objid, Session session);

}
