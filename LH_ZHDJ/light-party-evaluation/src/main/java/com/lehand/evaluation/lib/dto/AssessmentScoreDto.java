package com.lehand.evaluation.lib.dto;

import java.util.List;
import java.util.Map;

/**
 * 	评分
 *	@author Tangtao
 *
 */
public class AssessmentScoreDto {
	//评分记录表ID
	private Long id;
	
	//反馈表Id
	private Long feedbackid;
	
	//考核对象组织机构编码
	private Long code;
	
	//考核编码
	private Long assessid;
	
	//体系id
	private Long packageid;
	
	//指标id
	private Long itemid;
	
	//指标名
	private String name;
	
	//自评分
	private String selfscore;
	
	//自查情况
	private String feedbackcontent;
	
	//评分分值
	private	String score;
	
	//评分标准
	private String bzscore;
	
	//评分结果说明
	private String content;
	
	//自评资料档案
	private List<Map<String,Object>> filenames;
	
	//他评资料档案
	private List<Map<String,Object>> otherfilenames;
	
	//他评附件
	private String fileids;

    //自评附件集合(2021-04-26新需求)
    private String files;
    private List<Map<String,Object>> listFiles;

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public List<Map<String, Object>> getListFiles() {
        return listFiles;
    }

    public void setListFiles(List<Map<String, Object>> listFiles) {
        this.listFiles = listFiles;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFeedbackid() {
		return feedbackid;
	}

	public void setFeedbackid(Long feedbackid) {
		this.feedbackid = feedbackid;
	}

	

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Long getAssessid() {
		return assessid;
	}

	public void setAssessid(Long assessid) {
		this.assessid = assessid;
	}

	public Long getPackageid() {
		return packageid;
	}

	public void setPackageid(Long packageid) {
		this.packageid = packageid;
	}

	public Long getItemid() {
		return itemid;
	}

	public void setItemid(Long itemid) {
		this.itemid = itemid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSelfscore() {
		return selfscore;
	}

	public void setSelfscore(String selfscore) {
		this.selfscore = selfscore;
	}

	public String getFeedbackcontent() {
		return feedbackcontent;
	}

	public void setFeedbackcontent(String feedbackcontent) {
		this.feedbackcontent = feedbackcontent;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getBzscore() {
		return bzscore;
	}

	public void setBzscore(String bzscore) {
		this.bzscore = bzscore;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<Map<String, Object>> getFilenames() {
		return filenames;
	}

	public void setFilenames(List<Map<String, Object>> filenames) {
		this.filenames = filenames;
	}

	public String getFileids() {
		return fileids;
	}

	public void setFileids(String fileids) {
		this.fileids = fileids;
	}

	public List<Map<String, Object>> getOtherfilenames() {
		return otherfilenames;
	}

	public void setOtherfilenames(List<Map<String, Object>> otherfilenames) {
		this.otherfilenames = otherfilenames;
	}
	
	
}
