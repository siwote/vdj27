package com.lehand.evaluation.lib.service.assess;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssessItem;

/**
 * 
 * @ClassName: ElAssessItemBusiness
 * @Description: 类别与指标
 * @Author dong
 * @DateTime 2019年4月12日 下午5:09:09
 */
public class ElAssessItemBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	/**
	 * 
	 * @Title: initItemCalss
	 * @Description: 初始化类别
	 * @Author dong
	 * @DateTime 2019年4月11日 下午9:38:45
	 * @param bean
	 * @param session
	 */
	public  long initItemCalss(ElAssessItem bean, Session session) {
		bean.setCompid(session.getCompid());
		bean.setContent(StringConstant.EMPTY);
		bean.setLevel(getItemLevel(bean.getPid(),session.getCompid()));
		bean.setCreater(session.getUserid());
		bean.setModiler(session.getUserid());
		bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		bean.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		bean.setRemark1(0);
		bean.setRemark2(0);
		bean.setRemark3(StringConstant.EMPTY);
		bean.setRemark4(StringConstant.EMPTY);
		bean.setRemark5(StringConstant.EMPTY);
		bean.setRemark6(StringConstant.EMPTY);
		return generalSqlComponent.insert(ComesqlElCode.addItemType, bean);
	}
	
	/**
	 * 
	 * @Title: initItemCalss
	 * @Description: 修改类别
	 * @Author dong
	 * @DateTime 2019年4月11日 下午9:38:45
	 * @param bean
	 * @param session
	 */
	public  void updateItemCalss(ElAssessItem bean,Session session) {
		bean.setCompid(session.getCompid());
		bean.setModiler(session.getUserid());
		bean.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		generalSqlComponent.insert(ComesqlElCode.updateItemType, bean);
	}
	
	/**
	 * 
	 * @Title: copyItemCalss
	 * @Description: 复制类别
	 * @Author dong
	 * @DateTime 2019年4月15日 下午4:06:23
	 * @param bean
	 * @param session
	 * @return
	 */
	public  long copyItemCalss(ElAssessItem bean,long pid,Session session) {
		bean.setCompid(session.getCompid());
		bean.setPid(pid);
		bean.setCreater(session.getUserid());
		bean.setModiler(session.getUserid());
		bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		bean.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		return generalSqlComponent.insert(ComesqlElCode.addItemType, bean);
	}
	
	/**
	 * 
	 * @Title: initItemCalss2
	 * @Description: 新增指标
	 * @Author dong
	 * @DateTime 2019年4月13日 上午10:50:21
	 * @param bean
	 * @param name
	 * @param session
	 * @return
	 */
//	public  long initItemCalss2(ElAssessItem bean,String name,Session session) {
//		bean.setCompid(session.getCompid());
//		bean.setName(name);
//		bean.setContent(StringConstant.EMPTY);
//		bean.setScore(0d);
//		bean.setOrderid(0);
//		bean.setLevel(getItemLevel(bean.getPid(),session.getCompid()));
//		bean.setCreater(session.getUserid());
//		bean.setModiler(session.getUserid());
//		bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
//		bean.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
//		bean.setRemark1(0);
//		bean.setRemark2(0);
//		bean.setRemark3(StringConstant.EMPTY);
//		bean.setRemark4(StringConstant.EMPTY);
//		bean.setRemark5(StringConstant.EMPTY);
//		bean.setRemark6(StringConstant.EMPTY);
//		return generalSqlComponent.insert(ComesqlElCode.addItemType, bean);
//	}
	
	/**
	 * 
	 * @Title: initItemCalss2
	 * @Description: 新增指标
	 * @Author dong
	 * @DateTime 2019年4月13日 上午10:50:21
	 * @param bean
	 * @param
	 * @param session
	 * @return
	 */
	public  long initItemCalss2(Map<String,Object> bean,Session session) {
		bean.put("compid", session.getCompid());
		bean.put("content", StringConstant.EMPTY);
		bean.put("orderid", 0);
		bean.put("level", getItemLevel(Long.valueOf(bean.get("pid").toString()),session.getCompid()));
		bean.put("creater", session.getUserid());
		bean.put("modiler", session.getUserid());
		bean.put("createtime",DateEnum.YYYYMMDDHHMMDD.format());
		bean.put("modiltime", DateEnum.YYYYMMDDHHMMDD.format());
		bean.put("remark1", 0);
		bean.put("remark2", 0);
		bean.put("remark3", StringConstant.EMPTY);
		bean.put("remark4", StringConstant.EMPTY);
		bean.put("remark5", StringConstant.EMPTY);
		bean.put("remark6", StringConstant.EMPTY);
		return generalSqlComponent.insert(ComesqlElCode.addItemType, bean);
	}
	
	/**
	 * 
	 * @Title: copyItemCalss2
	 * @Description: 复制指标
	 * @Author dong
	 * @DateTime 2019年4月15日 下午4:07:36
	 * @param bean
	 * @param
	 * @param session
	 * @return
	 */
	public  long copyItemCalss2(ElAssessItem bean,long pid,Session session) {
		bean.setCompid(session.getCompid());
		bean.setPid(pid);
		bean.setCreater(session.getUserid());
		bean.setModiler(session.getUserid());
		bean.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		bean.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		return generalSqlComponent.insert(ComesqlElCode.addItemType, bean);
	}
	
	/**
	 * 
	 * @Title: getItemLevel
	 * @Description: 根据pid 获取当前level
	 * @Author dong
	 * @DateTime 2019年4月11日 下午9:25:32
	 * @param pid
	 * @return
	 */
	public  Short getItemLevel(long pid,long compid) {
		Short level = 1;
		ElAssessItem item = generalSqlComponent.query(ComesqlElCode.getItemType, new Object[] {pid,compid});
		if(item==null) {
			return level;
		}else {
			level = (short) (item.getLevel()+1);
			return level;
		}
	}
	
	/**
	 * 
	 * @Title: getItemTree
	 * @Description: 单层树数据
	 * @Author dong
	 * @DateTime 2019年4月16日 上午9:51:19
	 * @param packageid
	 * @param type
	 * @param session
	 * @return
	 */
	public List<ElAssessItem> getItemTree(long packageid,long type,Session session) {
		List<ElAssessItem> item = generalSqlComponent.query(ComesqlElCode.getItemTreeType, new Object[] {packageid,type,session.getCompid()});
//		Map<String,Object> map;
//		for(ElAssessItem it:item) {
//			map = new HashMap<String, Object>();
//			map.put("id", it.getId());
//			map.put("name", it.getName());
//			map.put("pid", it.getPid());
//		}
		return item;
	}
	
}
