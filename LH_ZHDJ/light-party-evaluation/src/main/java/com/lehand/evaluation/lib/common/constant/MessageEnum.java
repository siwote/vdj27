package com.lehand.evaluation.lib.common.constant;

public enum MessageEnum {
	
	/** 考核体系发布 .*/
    PACKAGE_RELEASE("PACKAGE_RELEASE", "考核体系发布"),
    
	/** 考核体系签收 .*/
	ASESS_SIGN("ASESS_SIGN", "考核体系签收"),
	
	/** 考核体评分 .*/
	ASESS_SCORE("ASESS_SCORE", "考核体系评分"),
	
	/** 考核体系公示 .*/
	ASESS_PUBLICITY("ASESS_PUBLICITY", "考核体系公示"),
	
	/** 未定义 .*/
	UNKNOWN_CODE("UNKNOWN_CODE", "未定义");
	
	private String code;
    private String msg;

    MessageEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    
    /**
     * 静态方法：根据编码获取信息
     * @param code
     * @return
     */
    public static String getMsg(String code) {
        for (MessageEnum mc : MessageEnum.values()) {
            if(mc.code.equals(code)) {
                return mc.msg;
            }
        }
        return null;
    }

    /**
     * 根据错误编码获取MsgCode枚举对象
     * @param code
     * @return
     */
    public static MessageEnum getMsgCode(String code) {
        for (MessageEnum mc : MessageEnum.values()) {
            if(mc.code.equals(code)) {
                return mc;
            }
        }
        return MessageEnum.UNKNOWN_CODE;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getMsg() {
        return msg;
    }
}
