package com.lehand.evaluation.lib.pojo;

public class ElAssessLevel extends ElAssessLevelKey {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column el_assess_level.level
     *
     * @mbg.generated Tue Apr 09 20:36:05 CST 2019
     */
    private Short level;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column el_assess_level.level
     *
     * @return the value of el_assess_level.level
     *
     * @mbg.generated Tue Apr 09 20:36:05 CST 2019
     */
    public Short getLevel() {
        return level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column el_assess_level.level
     *
     * @param level the value for el_assess_level.level
     *
     * @mbg.generated Tue Apr 09 20:36:05 CST 2019
     */
    public void setLevel(Short level) {
        this.level = level;
    }
}