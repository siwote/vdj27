package com.lehand.evaluation.lib.service.assess;

import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssessEvaluate;
import com.lehand.evaluation.lib.pojo.ElAssessItem;

/**
 * 
 * @ClassName: ElAssessEvaluateBusiness
 * @Description: 评分者表
 * @Author dong
 * @DateTime 2019年4月13日 下午5:05:17
 */
public class ElAssessEvaluateBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	
	public void initEvaluateClass2(ElAssessEvaluate evaluate, Map<String,Object> assessitem, String type, Session session) {
		evaluate.setCompid(session.getCompid());
		evaluate.setCode(Long.valueOf(assessitem.get("code").toString()));
		evaluate.setEvaluatename(assessitem.get("evaluatename").toString());
		evaluate.setType(type);
		evaluate.setPackageid(Long.valueOf(assessitem.get("packageid").toString()));
		evaluate.setItemid(Long.valueOf(assessitem.get("id").toString()));
		evaluate.setCreater(session.getUserid());
		evaluate.setModiler(session.getUserid());
		evaluate.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		evaluate.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		evaluate.setRemark1(0);
		evaluate.setRemark2(0);
		evaluate.setRemark3(StringConstant.EMPTY);
		evaluate.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addEvaluateType, evaluate);
		
	}
	
	/**
	 * 
	 * @Title: initReceiveClass
	 * @Description: 评分对象初始化
	 * @Author dong
	 * @DateTime 2019年4月12日 下午2:08:33
	 * @param
	 * @param assessitem
	 * @param type
	 */
	public void initEvaluateClass(ElAssessEvaluate evaluate, ElAssessItem assessitem, String type, Session session) {
		evaluate.setCompid(session.getCompid());
		evaluate.setType(type);
		evaluate.setPackageid(assessitem.getPackageid());
		evaluate.setItemid(assessitem.getId());
		evaluate.setCreater(session.getUserid());
		evaluate.setModiler(session.getUserid());
		evaluate.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		evaluate.setModiltime(DateEnum.YYYYMMDDHHMMDD.format());
		evaluate.setRemark1(0);
		evaluate.setRemark2(0);
		evaluate.setRemark3(StringConstant.EMPTY);
		evaluate.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addEvaluateType, evaluate);
		
	}

}
