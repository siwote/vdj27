package com.lehand.evaluation.lib.common.constant;

public class ComesqlElCode {

	/**
	 * 新增体系
	 */
	public final static String addPackType="addPackType";
	
	/**
	 * 新增考核
	 */
	public final static String addAssessType = "addAssessType";
	
	/**
	 * 新增
	 */
	public final static String addObjectType = "addObjectType";
	
	/**
	 * 新增指标或类别
	 */
	public final static String addItemType = "addItemType";
	
	/**
	 * 根据id 获取类别或指标
	 */
	public final static String getItemType = "getItemType";
	
	/**
	 * 根据pid 获取类别或指标总分
	 */
	public final static String getSumScoreItemType = "getSumScoreItemType";
	
	/**
	 * 根据pid获取类别或指标总分不包括自己
	 * select ifnull(sum(score),0) from el_assess_item where pid = ? and id != ? and compid = ?
	 */
	public final static String getSumScoreItemNotSelfType = "getSumScoreItemNotSelfType";
	
	/**
	 * 新增评分单位或人
	 */
	public final static String addEvaluateType = "addEvaluateType";
	
	/**
	 * 删除评分单位或人
	 */
	public final static String deleteEvaluateType = "deleteEvaluateType";
	
	/**
	 * 删除评分接收表
	 * delete  from el_assess_evaluate where packageid = ? and compid = ?
	 */
	public final static String deleteEvaluateReceiveType = "deleteEvaluateReceiveType";
	
	/**
	 * 根据体系id删除评分者表
	 */
	public final static String deleteEvaluatePackageidType = "deleteEvaluatePackageidType";
	
	/**
	 * 新增评分规则
	 */
	public final static String addScoreRuleType = "addScoreRuleType";
	
	/**
	 * 删除评分规则
	 */
	public final static String deleteScoreRuleType = "deleteScoreRuleType";
	
	/**
	 * 新增评分者候选表
	 */
	public final static String addEvaluateCandidateType = "addEvaluateCandidateType";
	
	/**
	 * 修改指标
	 */
	public final static String updateItemTargetType = "updateItemTargetType";
	
	
	/**
	 * 根据id获取考核表
	 */
	public final static String getAssessType = "getAssessType";
	
	/**
	 * 根据名称查询考核
	 * select * from el_assess where name = ? and compid = ?
	 */
	public final static String getAssessNameType = "getAssessNameType";
	
	/**
	 * 修改考核表状态
	 */
	public final static String updateAsseaType = "updateAsseaType";
	
	/**
	 * 修改考核表
	 */
	public final static String updateAssessDataType = "updateAssessDataType";
	
	
	/**
	 * 根据id获取体系表
	 */
	public final static String getPackageType = "getPackageType";
	
	/**
	 * 修改体系表状态
	 */
	public final static String updatePackageType = "updatePackageType";
	
	/**
	 * 根据考核id获取被考核对象
	 */
	public final static String getAssessObjectType = "getAssessObjectType";

	public final static String getAssessObjectType3 = "getAssessObjectType3";
	
	/**SELECT * FROM el_assess_feedback where assessid=? and compid=? and code=? and status=0 limit 1
	 * SELECT a.* FROM el_assess a
	 * INNER JOIN el_assess_item_receive b ON a.id = b.assessid AND a.compid = b.compid
	 * WHERE  b.status = ? and a.packageid=? AND a.compid = ? AND b.code = ? 
	 */
	public final static String getAssessFeedback = "getAssessFeedback";
	
	/**
	 * select * from el_assess_feedback where assessid=2 and itemid in (SELECT itemid FROM el_assess_evaluate a where a.compid=1 and a.packageid=2 and code=2) and status=1 limit 1
	 */
	public final static String getAssessEvaluateCandidate = "getAssessEvaluateCandidate";
	
	public final static String getAssessEvaluateCandidate2 = "getAssessEvaluateCandidate2";
	/**
	 * 根据考核id获取被考核对象分页
	 */
	public final static String getAssessObjectPageType = "getAssessObjectPageType";
	
	/**
	 * 根据体系id获取指标
	 */
	public final static String getAssessItem = "getAssessItem";
	
	/**
	 * 新增反馈数据
	 */
	public final static String addFeedbackType = "addFeedbackType";
	
	/**
	 * 新增反馈接收表数据
	 */
	public final static String addItemReceive = "addItemReceive";
	
	
	/**
	 * 查询考核列表
	 */
	public final static String getAssessListType = "getAssessListType";
	
	/**
	 * 根据体系id查询评分者候选List
	 */
	public final static String getCandidateType ="getCandidateType";
	
	/**
	 * 修改体系
	 */
	public final static String updatePackageDataType = "updatePackageDataType";
	
	/**
	 * 根据体系id获取一级类别总分
	 */
	public final static String getSumScoreItemType2 = "getSumScoreItemType2";
	
	/**
	 * 删除被考核对象
	 */
	public final static String deleteAssessObjType = "deleteAssessObjType";
	
	/**
	 * 删除评分者候选
	 */
	public final static String deleteCandidate = "deleteCandidate";
	
	/**
	 * 修改类别
	 */
	public final static String updateItemType = "updateItemType";
	
	/**
	 * 删除体系
	 */
	public final static String deletepackageType = "deletepackageType";
	
	/**
	 * 根据体系id查询考核
	 */
	public final static String getAssesspackageidType = "getAssesspackageidType";
	
	/**
	 * 删除考核表
	 */
	public final static String deleteAssessType = "deleteAssessType";
	
	/**
	 * 根据状态查询考核
	 */
	public final static String getAssessListStatusType = "getAssessListStatusType";
	
	/**
	 * 根据状态查询考核数
	 */
	public final static String getAssessListStatusCountType = "getAssessListStatusCountType";
	
	/**
	 * 获取自评分数
	 * 
	 */
	public final static String getObjectScoreSelfType = "getObjectScoreSelfType";
	
	/**
	 * 获取他评分数
	 */
	public final static String getObjectScoreOthType = "getObjectScoreOthType";
	
	
	/**
	 * 创建考核是的类别树  
	 * select * from el_assess_item where packageid = ? and type = '0' and pid = ? and compid = ?
	 */
	public final static String getItemTreeType = "getItemTreeType";
	
	/**
	 * 删除类别或指标
	 * delete from el_assess_item where id = ? and compid = ?
	 */
	public final static String deleteItemType ="deleteItemType";
	
	/**
	 * 根据packageid删除类别或指标
	 *  delete from el_assess_item where packageid = ? and compid = ?
	 */
	public final static String deleteItemPackageidType ="deleteItemPackageidType";
	
	
	/**
	 * 根据指标id删除评分者 
	 * delete from el_assess_evaluate where itemid = ? and compid = ?
	 */
	public final static String deleteEvaluateItemType = "deleteEvaluateItemType";
	
	/**
	 * 根据类别id 获取指标列表
	 * select a.* ,b.code from el_assess_item a left join  (select group_concat(code) as code,group_concat(evaluatename) as evaluatename,itemid from el_assess_evaluate group by code) b on b.itemid = a.id  where pid = ? and compid = ? 
	 */
	public final static String getItemListType = "getItemListType";
	
	/**
	 * 根据类别id获取指标名及自评分、他评分
	 * 作废  select a.name,b.selfscore,c.score  from el_assess_item a inner join el_assess_feedback b on b.itemid = a.id  inner join el_assess_score_record c on c.feedbackid = b.id  where where a.pid = ? and a.type = ? and a.compid = ?
	 * 
	 * select a.name,ifnull(b.selfscore,0) as selfscore ,ifnull(c.score,0) score,b.id as feedbackid,b.itemid  from el_assess_item a  left join el_assess_feedback b on b.itemid = a.id  left join el_assess_score_record c on c.feedbackid = b.id  where a.pid = ? and a.type = ? and a.compid =? and b.code = ?
	 */
	public final static String getItemScoreType = "getItemScoreType";
	
	
	/**
	 * 考核结果 考核列表
	 */
	public final static String getAssessResultListType = "getAssessResultListType";
	
	/**
	 * 根据类别id和被考核人查询指标
	 *  select * from el_assess_item a inner join el_assess_feedback b on b.itemid = a.id where b.code = ? and b.compid = ? and name like ?
	 */
	public final static String getItemListResultType = "getItemListResultType";
	
	/**
	 * 根据考核id查询反馈表
	 *  select * from el_assess_feedback where assessid = ? and compid = ?
	 */
	public final static String getFeedBackType = "getFeedBackType";
	
	/**
	 * 根据反馈id删除评分记录表
	 *  delete from el_assess_score_record where feedbackid  = ? and compid = ?
	 */
	public final static String deleteRecordType = "deleteRecordType";
	
	/**
	 * 根据反馈id删除附件关系表
	 */
	public final static String deleteFileBusinessType = "deleteFileBusinessType";
	
	/**
	 * 根据反馈id删除佐证表
	 *  delete from el_assess_feedback_file where feedbackid =? and compid = ?
	 */
	public final static String deleteFeedbackFileType = "deleteFeedbackFileType";
	
	
	/**
	 * 删除反馈表
	 *  delete from el_assess_feedback where id = ? and compid = ?
	 */
	public final static String deleteFeedbackType = "deleteFeedbackType";
	
	
	/**
	 * 根据体系id和状态查询考核
	 *   select * from  el_assess where packageid = ? and status = ?  and compid = ?
	 */
	public final static String getAssessPackageStatusType = "getAssessPackageStatusType";
	
	/**
	 * 根据体系id和状态查询考核数量
	 * select ifnull(sum(if(status=3,1,0)),0) endpublish,ifnull(sum(if(status=2,1,0)),0) notpublish from el_assess where orgid=:orgId and compid=:compid
	 */
	public final static String getAssessStatusCouType = "getAssessStatusCouType";
	
	/**
	 * 查询已提交的和已公布的考核
	 *   select * from el_assess where   orgid = ?  and (status = 2 or status 3) and compid = ?
	 */
	public final static String getAssessRankType = "getAssessRankType";
	
	
	
	/**
	 * 根据类别id获取指标
	 * select * from el_assess_item where pid = ? and compid = ?
	 */
	public final static String getItemPidList = "getItemPidList";
	
	/**
	 * 根据体系id、pid获取类别或指标
	 * select  * from  el_assess_item where packageid  = ? and pid = ?  and compid = ?
	 */
	public final static String getAssessItemPidType = "getAssessItemPidType";
	
	/**
	 * 根据体系id、type获取指标
	 * select * from el_assess_item where packageid = ? and  type = ? and compid = ?
	 */
	public final static String getItemTypeListType = "getItemTypeListType";
	
	/**
	 * 修改考核表状态及remark1
	 * update el_assess set status = ?,remark1 =  ? where id = ? and compid = ?
	 */
	public final static String updateAsseaRemark1Type = "updateAsseaRemark1Type";
	
	/**
	 * 获取各评分单位对被考核对象的评分和自评分
	 * select d.code,d.name,d.assessid,sum(d.selfscore),sum(d.othscore),d.evaluatename from 
	 * ( select a.code,a.name,a.assessid,ifnull(a.selfscore,0) as selfscore,ifnull(b.score,0) as othscore,b.usertor,c.evaluatename 
	 * from el_assess_feedback a  left join el_assess_evaluate c on c.itemid = a.itemid 
	 * left join el_assess_score_record b  on b.feedbackid= a.id where a.code = ? and a.assessid = ? and compid = ? ) d group by d.usertor
	 */
	public final static String getEvaluateObjectType = "getEvaluateObjectType";
	
	/**
	 * 根据指标获取评分规则
	 * select * from el_assess_feedback_score_rule where itemid = ? and compid = ?
	 */
	public final static String getScoreRuleType = "getScoreRuleType";
	
	/**
	 * 删除指标接收表
	 * delete from  el_assess_item_receive where assessid = ? and compid = ?
	 * 
	 */
	public final static String deleteReceiveType = "deleteReceiveType";
	
	/**
	 * 根据指标id获取评分人表
	 * select * from el_assess_evaluate where itemid = ? and compid = ?
	 * 
	 */
	public final static String getEvaliateType = "getEvaliateType";
	
	/**
	 * 删除评分规则表
	 * delete * from el_assess_feedback_score_rule where packageid = ? and compid = ?
	 * 
	 */
	public final static String deleteRuleType = "deleteRuleType";
	
	/**
	 * 党组织考核结果排名
	 * select e.* from ( select a.code,a.name,ifnull(d.score,0) as score from  el_assess_object a 
	 * left join  ( select b.code,sum(c.score) score from el_assess_feedback b left  join  el_assess_score_record c on c.feedbackid = b.id  group by b.code  )
	 *  as d on d.code = a.code where a.assessid = ?   and compid = ?  ) e order by e.score desc
	 */
	public final static String rankOrgType = "rankOrgType";
	
	/**
	 * 评分标准扣分次数结果排名
	 * select a.id,a.name,count(if(a.score>ifnull(c.score,0),1,0)) as num from el_assess_item as a 
	 * left join el_assess_feedback b on b.itemid = a.id left join el_assess_score_record c on c.feedbackid = b.id where b.assessid = ? 
	 * group by a.id   order by num desc,id asc
	 * 
	 */
	public final static String rankItemType1 = "rankItemType1";
	
	
	/**
	 * 指标扣分次数结果排名
	 * select d.id,d.name,sum(if(a.score>ifnull(c.score,0),1,0)) as num from el_assess_item d 
	 * left join el_assess_item a on a.pid = d.id left join el_assess_feedback b on b.itemid = a.id 
	 * left join el_assess_score_record c on c.feedbackid = b.id where b.assessid = ?  group by d.id   order by num desc,d.id asc
	 */
	public final static String rankItemType2 = "rankItemType2";
	
	
	/**
	 * 类别扣分次数结果排名
	 * select e.id,e.name,sum(if(a.score>ifnull(c.score,0),1,0)) as num from el_assess_item e 
	 * left join el_assess_item d on d.pid = e.id
	 * left join el_assess_item a on a.pid = d.id 
	 * left join el_assess_feedback b on b.itemid = a.id 
	 * left join el_assess_score_record c on c.feedbackid = b.id 
	 * where b.assessid = ? and b.compid = ?  group by d.id   order by num desc,d.id asc
	 */
	public final static String rankItemType3 = "rankItemType3";
	
	public final static String rankItemType4 = "rankItemType4";
	public final static String rankItemType5 = "rankItemType5";
	public final static String rankItemType6 = "rankItemType6";
	public final static String rankItemType = "rankItemType";
	
	/**
	 * 根基名称查询类别或指标
	 * select * from el_assess_item where name  = ? and packageid = ? and  compid = ?
	 */
	public final static String getItemNameType = "getItemNameType";
	
	
	/**
	 * 根据体系id 和pid获取类别或指标分数最大值
	 * select max(score) from el_assess_item where packageid = ? and pid = ?  and  compid= ?
	 */
	public final static String getItemScoreMaxType = "getItemScoreMaxType";
	
	/**
	 * 根据体系id、被考核人获取反馈状态
	 * select min(status) from el_assess_feedback where assessid = ? and code  = ? and compid = ?
	 */
	public final static String getFeedBackStatusCodeType = "getFeedBackStatusCodeType";
	
	/**
	 *修改类别的排序号
	 * update el_assess_item set orderid = :orderid,pid = :pid where id = :id and compid = :compid
	 */
	public final static String updateItemOrderidType = "updateItemOrderidType";
	
	/**
	 *查询类别，评分单位参与评分的
	 * select * from (select d.id,d.name,d.pid ,d.orderid from el_assess_item d left join  el_assess_item c on c.pid = d.id
	 *   left join  el_assess_item a on a.pid = c.id left join el_assess_evaluate  b on b.itemid = a.id and b.compid = a.compid
	 *     where b.packageid = ? and b.code = ? and b.compid = ?  GROUP BY d.id ) t ORDER BY t.orderid,t.id ASC
	 */
	public final static String getTreePublishType1 = "getTreePublishType1";
	
	/**
	 *查询指标，评分单位参与评分的
	 *select * from ( select d.id,d.name,d.pid ,d.orderid,d.packageid from el_assess_item d 
	 *left join  el_assess_item a on a.pid = d.id left join el_assess_evaluate  b on b.itemid = a.id and b.compid = a.compid  
	 *where b.packageid = :packageid and b.code = :code and b.compid = :compid and d.pid = :pid GROUP BY d.id ) t ORDER BY t.orderid,t.id ASC
	 */
	public final static String getTreePublishType2 = "getTreePublishType2";
	
	/**
	 * 根据id获取当前时间大于等于结束时间
	 *  select * from el_assess where id = :id and date_format(now(), '%Y-%m-%d') <= str_to_date(a.enddate, '%Y-%m-%d') and compid = :compid
	 */
	public final static String getAssessEndDateType111 = "getAssessEndDateType111";
	
	
	/**
	 * 根据体系id和评分者编码查询评分者候选表
	 *   select * from el_assess_evaluate_candidate where packageid = ? and code = ? and compid = ?
	 */
	public final static String getCandidateCodeType = "getCandidateCodeType";
	
	/**
	 * 根据体系id获取评分者表
	 *   select * from el_assess_evaluate where packageid = ? and compid = ?
	 */
	public final static String getEvaliateType2 = "getEvaliateType2";
	
	
	/**
	 * 根据pid获取not in(id) 总分数
	 *   select ifnull(sum(score),0) from el_assess_item where pid = :pid and id not in(:notid) and compid = :compid
	 */
	public final static String getitemScoreNInIdType = "getitemScoreNInIdType";

	public static String modiOtherScore = "modiOtherScore";

	/**
	 * 根据code获取党组织信息
	 * select * from t_dzz_info where organcode=?
	 */
	public static String geTTDzzInfoBycode = "geTTDzzInfoBycode";
    public static String addElAssessFeedbackAudit = "addElAssessFeedbackAudit";
    public static String listElAssessFeedbackAudit = "listElAssessFeedbackAudit";
    public static String getElPackage = "getElPackage";
    public static String updateElAssessFeedbackAudit = "updateElAssessFeedbackAudit";
    public static String getPrintFile = "getPrintFile";
    /**
	 * 
	 * 留存
	 * select  a.id,a.name,a.score,a.packageid,ifnull(b.code,'') as code,ifnull(b.evaluatename,'') as evaluatename from el_assess_item a left join el_assess_evaluate b on b.itemid = a.id  where a.pid = ? and a.compid = ?
	 */
	
	/**
	 * 
	 * 获取各评分单位对被考核对象的评分和自评分
	 * 
	 * select d.code,d.name,d.assessid,sum(d.selfscore),sum(d.score),d.evaluatename from (
select a.code,a.name,a.assessid,ifnull(a.selfscore,0) as selfscore,ifnull(b.score,0) as score,b.usertor,c.evaluatename from el_assess_feedback a 
left join el_assess_evaluate c on c.itemid = a.itemid
left join el_assess_score_record b  on b.feedbackid= a.id

where a.code = '34171250006' and a.assessid = '123'
) d group by d.usertor
	 * 
	 */
}
