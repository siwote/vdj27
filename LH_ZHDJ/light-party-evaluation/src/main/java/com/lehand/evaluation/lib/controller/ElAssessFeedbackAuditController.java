package com.lehand.evaluation.lib.controller;

import com.lehand.base.common.Message;
import com.lehand.evaluation.lib.business.ElAssessFeedbackAuditBusiness;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackAudit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 自评审核
 */
@RestController
@RequestMapping("/evaluation/selfaudit")
public class ElAssessFeedbackAuditController extends ElBaseController{
	
	private static final Logger logger = LogManager.getLogger(ElAssessFeedbackAuditController.class);
	@Resource
    ElAssessFeedbackAuditBusiness elAssessFeedbackAuditBusiness;
	

	@RequestMapping("/audit")
	private Message audit(Long packageid, Long subjectid,Byte status,String auditInfo) {
		Message message = new Message(); 
		try {
			elAssessFeedbackAuditBusiness.audit(packageid,subjectid,status,auditInfo,getSession());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	


	
	@RequestMapping("/getaudit")
	private Message getElAssessFeedbackAudit(Long packageid,Long subjectid) {
		Message message = new Message();
		try {
            ElAssessFeedbackAudit elAssessFeedbackAudit=
                    elAssessFeedbackAuditBusiness.getElAssessFeedbackAudit(packageid,subjectid,getSession());
			message.success().setData(elAssessFeedbackAudit);
		} catch (Exception e) {
			message.setMessage("查询失败");
			logger.error("查询失败",e);
		}
		return message;
		
	}
	

	
}
