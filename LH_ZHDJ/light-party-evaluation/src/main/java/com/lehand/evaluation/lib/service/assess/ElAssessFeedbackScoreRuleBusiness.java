package com.lehand.evaluation.lib.service.assess;

import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.common.Session;
import com.lehand.base.constant.StringConstant;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.evaluation.lib.common.constant.ComesqlElCode;
import com.lehand.evaluation.lib.pojo.ElAssessFeedbackScoreRule;
import com.lehand.evaluation.lib.pojo.ElAssessItem;

/**
 * 
 * @ClassName: ElAssessFeedbackScoreRuleBusiness
 * @Description: 评分规则
 * @Author dong
 * @DateTime 2019年4月12日 下午3:59:43
 */
public class ElAssessFeedbackScoreRuleBusiness {

	@Resource
	GeneralSqlComponent generalSqlComponent;

	/**
	 * 
	 * @Title: initScoreRuleCalss
	 * @Description: 初始化评分规则
	 * @Author dong
	 * @DateTime 2019年4月12日 下午4:04:22
	 * @param scorerule
	 * @param assessitem
	 * @param rule
	 * @param session
	 */
	public void initScoreRuleCalss(ElAssessFeedbackScoreRule scorerule, ElAssessItem assessitem, Short rule, Session session) {
		scorerule.setCompid(session.getCompid());
		scorerule.setPackageid(assessitem.getPackageid());
		scorerule.setItemid(assessitem.getId());
		scorerule.setRule(rule==null? 0:rule);
		scorerule.setRemark1(0);
		scorerule.setRemark2(0);
		scorerule.setRemark3(StringConstant.EMPTY);
		scorerule.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addScoreRuleType, scorerule);
	}
	
	public void initScoreRuleCalss2(ElAssessFeedbackScoreRule scorerule, Map<String,Object> assessitem, Short rule,Session session) {
		scorerule.setCompid(session.getCompid());
		scorerule.setPackageid(Long.valueOf(assessitem.get("packageid").toString()));
		scorerule.setItemid(Long.valueOf(assessitem.get("id").toString()));
		scorerule.setRule(rule==null? 0:rule);
		scorerule.setRemark1(0);
		scorerule.setRemark2(0);
		scorerule.setRemark3(StringConstant.EMPTY);
		scorerule.setRemark4(StringConstant.EMPTY);
		generalSqlComponent.insert(ComesqlElCode.addScoreRuleType, scorerule);
	}
}
