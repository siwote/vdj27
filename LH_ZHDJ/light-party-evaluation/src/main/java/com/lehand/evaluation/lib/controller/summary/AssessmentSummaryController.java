package com.lehand.evaluation.lib.controller.summary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.evaluation.lib.business.summary.AssessmentSummaryBusiness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.components.web.controller.BaseController;
import com.lehand.evaluation.lib.controller.ElBaseController;

/**
 *	@author Tangtao
 *	考核汇总
 */
@RestController
@RequestMapping("/evaluation/asessmentsummary")
public class AssessmentSummaryController extends BaseController {
	
	public final Logger logger = LogManager.getLogger(ElBaseController.class);
	@Resource
	AssessmentSummaryBusiness summaryBusiness;
	
	/**
	 *	所有考核体系列表
	 *	@param year
	 *	@return
	 *	Message
	 *	2019年4月16日
	 *	Tangtao
	 */
	@RequestMapping("/getSummSystemList")
	public Message getSummSystemList(String year) {
		Message msg = new Message();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try {
			list = summaryBusiness.getSummSystemList(year,getSession());
			msg.success();
			msg.setData(list);
		} catch (Exception e) {
			msg.fail("获取考核对象失败");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 *	考核指标汇总表
	 *	@param ObjectCode 考核对象code
	 *	@param packageid	体系id
	 *	@return
	 *	Message
	 *	2019年4月16日
	 *	Tangtao
	 */
	@RequestMapping("/getSummData")
	public Message getSummData(Long ObjectCode,Long packageid) {
		Message msg = new Message();
		Map<String,Object> list = new HashMap<String,Object>();
		try {
			list = summaryBusiness.getSummData(ObjectCode, packageid, getSession().getCompid());
			msg.success();
			msg.setData(list);
		} catch (Exception e) {
			msg.fail(e.getMessage());
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	/**
	 *	获取考核对象列表
	 *	@param packageid
	 *	@return
	 *	Message
	 *	2019年4月15日
	 *	Tangtao
	 */
	@RequestMapping("/getEvalObjectList")
	public Message getEvalObjectList(Long packageid,String str) {
		Message msg = new Message();
		try {
			msg = summaryBusiness.getEvalObjectList(packageid,str, getSession().getCompid());
		} catch (Exception e) {
			msg.fail("获取汇总考核对象失败");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		}
		return msg;
	}
	
	
	@RequestMapping("/exportExcel")
	public Message exportExcel(Long packageid, String str) {
		Message msg = new Message();
		try {
			msg  = summaryBusiness.exportData(packageid,str, getSession());
			msg.success();
		} catch (Exception e) {
			msg.fail(e.getMessage());
			e.printStackTrace();
			return msg;
		}
		return msg;
	}
}
