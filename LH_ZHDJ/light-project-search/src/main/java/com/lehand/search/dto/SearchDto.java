package com.lehand.search.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: SearchDto
 * @Description: 搜索扩展类
 * @Author lx
 * @DateTime 2021-05-20 14:00
 * 党组织信息、党员信息、组织各会议、领航计划、攻坚项目、任务中心、党建考核
 */
@ApiModel(value="搜索扩展类", description="搜索扩展类")
public class SearchDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**----------------------------start--------------------------------*/

    /**--------------党组织信息 t_dzz_info------------------*/

    @ApiModelProperty(value = "组织id", name = "organid")
    private String organid;

    @ApiModelProperty(value = "组织编码", name = "organcode")
    private String organcode;

    @ApiModelProperty(value = "党组织名称", name = "dzzmc")
    private String dzzmc;

    @ApiModelProperty(value = "党组织隶属关系", name = "dzzlsgx")
    private String dzzlsgx;

    @ApiModelProperty(value = "地址", name = "address")
    private String address;

    @ApiModelProperty(value = "党组织书记", name = "dzzsj")
    private String dzzsj;

    @ApiModelProperty(value = "行政区划", name = "xzqh")
    private String xzqh;

    /**--------------党员信息 t_dy_info------------------*/

    @ApiModelProperty(value = "党员userid", name = "userid")
    private String userid;

    @ApiModelProperty(value = "姓名", name = "xm")
    private String xm;

    @ApiModelProperty(value = "性别", name = "xbname")
    private String xbname;

    @ApiModelProperty(value = "民族", name = "mzname")
    private String mzname;

    @ApiModelProperty(value = "籍贯", name = "jgname")
    private String jgname;

    @ApiModelProperty(value = "学历", name = "xlname")
    private String xlname;

    @ApiModelProperty(value = "身份证号码", name = "zjhm")
    private String zjhm;

    @ApiModelProperty(value = "工作岗位", name = "gzgwname")
    private String gzgwname;

    @ApiModelProperty(value = "党员类别", name = "dylbname")
    private String dylbname;

    @ApiModelProperty(value = "现居住地", name = "xjzd")
    private String xjzd;

    /**--------------组织各会议 meeting_record,meeting_tags_map,meeting_record_subject------------------*/

    @ApiModelProperty(value = "会议id", name = "mrid")
    private String mrid;

    @ApiModelProperty(value = "会议类型", name = "mrtypename")
    private String mrtypename;

    @ApiModelProperty(value = "主持人名称", name = "moderatorname")
    private String moderatorname;

    @ApiModelProperty(value = "记录人名称", name = "recordername")
    private String recordername;

    @ApiModelProperty(value = "主题/授课内容", name = "topic")
    private String topic;

    @ApiModelProperty(value = "内容(议题)", name = "context")
    private String context;

    @ApiModelProperty(value = "会议决定", name = "decision")
    private String decision;

    /**--------------领航计划 t_dzz_party_brand,t_dzz_party_brand_dynamic------------------*/

    @ApiModelProperty(value = "领航计划id", name = "brandid")
    private String brandid;

    @ApiModelProperty(value = "品牌名称", name = "brandname")
    private String brandname;

    @ApiModelProperty(value = "品牌宣传语", name = "slogan")
    private String slogan;

    @ApiModelProperty(value = "品牌介绍", name = "introduce")
    private String introduce;

    @ApiModelProperty(value = "品牌理念", name = "concept")
    private String concept;

    @ApiModelProperty(value = "品牌思路", name = "trainofthought")
    private String trainofthought;

    @ApiModelProperty(value = "品牌措施", name = "measures")
    private String measures;

    @ApiModelProperty(value = "动态标题", name = "title")
    private String title;

    @ApiModelProperty(value = "品牌动态内容", name = "content")
    private String content;

    /**--------------攻坚项目 pm_project------------------*/

    @ApiModelProperty(value = "攻坚项目id", name = "pmid")
    private String pmid;

    @ApiModelProperty(value = "项目名称", name = "pmname")
    private String pmname;

    @ApiModelProperty(value = "项目介绍", name = "introduction")
    private String introduction;

    @ApiModelProperty(value = "结项理由", name = "closereason")
    private String closereason;

    /**--------------任务中心 tk_task_deploy------------------*/

    @ApiModelProperty(value = "任务id", name = "taskid")
    private String taskid;

    @ApiModelProperty(value = "任务名称", name = "tkname")
    private String tkname;

    @ApiModelProperty(value = "任务备注", name = "tkdesc")
    private String tkdesc;

    @ApiModelProperty(value = "任务联系人", name = "publisher")
    private String publisher;

    /**--------------党建考核 el_assess,el_assess_feedback------------------*/

    @ApiModelProperty(value = "党建考核id", name = "assessid")
    private String assessid;

    @ApiModelProperty(value = "党建考核名称", name = "assessname")
    private String assessname;

    @ApiModelProperty(value = "党建考核分类名称", name = "packagename")
    private String packagename;

    @ApiModelProperty(value = "对考核的说明", name = "assesscontent")
    private String assesscontent;

    /**----------------------------end--------------------------------*/


    @ApiModelProperty(value = "创建时间", name = "createtime")
    private String createtime;

    @ApiModelProperty(value = "生成搜索记录时间", name = "updatetime")
    private String updatetime;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getOrgancode() {
        return organcode;
    }

    public void setOrgancode(String organcode) {
        this.organcode = organcode;
    }

    public String getDzzmc() {
        return dzzmc;
    }

    public void setDzzmc(String dzzmc) {
        this.dzzmc = dzzmc;
    }

    public String getDzzlsgx() {
        return dzzlsgx;
    }

    public void setDzzlsgx(String dzzlsgx) {
        this.dzzlsgx = dzzlsgx;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDzzsj() {
        return dzzsj;
    }

    public void setDzzsj(String dzzsj) {
        this.dzzsj = dzzsj;
    }

    public String getXzqh() {
        return xzqh;
    }

    public void setXzqh(String xzqh) {
        this.xzqh = xzqh;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getXbname() {
        return xbname;
    }

    public void setXbname(String xbname) {
        this.xbname = xbname;
    }

    public String getMzname() {
        return mzname;
    }

    public void setMzname(String mzname) {
        this.mzname = mzname;
    }

    public String getJgname() {
        return jgname;
    }

    public void setJgname(String jgname) {
        this.jgname = jgname;
    }

    public String getXlname() {
        return xlname;
    }

    public void setXlname(String xlname) {
        this.xlname = xlname;
    }

    public String getZjhm() {
        return zjhm;
    }

    public void setZjhm(String zjhm) {
        this.zjhm = zjhm;
    }

    public String getGzgwname() {
        return gzgwname;
    }

    public void setGzgwname(String gzgwname) {
        this.gzgwname = gzgwname;
    }

    public String getDylbname() {
        return dylbname;
    }

    public void setDylbname(String dylbname) {
        this.dylbname = dylbname;
    }

    public String getXjzd() {
        return xjzd;
    }

    public void setXjzd(String xjzd) {
        this.xjzd = xjzd;
    }

    public String getMrid() {
        return mrid;
    }

    public void setMrid(String mrid) {
        this.mrid = mrid;
    }

    public String getMrtypename() {
        return mrtypename;
    }

    public void setMrtypename(String mrtypename) {
        this.mrtypename = mrtypename;
    }

    public String getModeratorname() {
        return moderatorname;
    }

    public void setModeratorname(String moderatorname) {
        this.moderatorname = moderatorname;
    }

    public String getRecordername() {
        return recordername;
    }

    public void setRecordername(String recordername) {
        this.recordername = recordername;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getBrandid() {
        return brandid;
    }

    public void setBrandid(String brandid) {
        this.brandid = brandid;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getTrainofthought() {
        return trainofthought;
    }

    public void setTrainofthought(String trainofthought) {
        this.trainofthought = trainofthought;
    }

    public String getMeasures() {
        return measures;
    }

    public void setMeasures(String measures) {
        this.measures = measures;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPmid() {
        return pmid;
    }

    public void setPmid(String pmid) {
        this.pmid = pmid;
    }

    public String getPmname() {
        return pmname;
    }

    public void setPmname(String pmname) {
        this.pmname = pmname;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getClosereason() {
        return closereason;
    }

    public void setClosereason(String closereason) {
        this.closereason = closereason;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getTkname() {
        return tkname;
    }

    public void setTkname(String tkname) {
        this.tkname = tkname;
    }

    public String getTkdesc() {
        return tkdesc;
    }

    public void setTkdesc(String tkdesc) {
        this.tkdesc = tkdesc;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAssessid() {
        return assessid;
    }

    public void setAssessid(String assessid) {
        this.assessid = assessid;
    }

    public String getAssessname() {
        return assessname;
    }

    public void setAssessname(String assessname) {
        this.assessname = assessname;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    public String getAssesscontent() {
        return assesscontent;
    }

    public void setAssesscontent(String assesscontent) {
        this.assesscontent = assesscontent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrganid() {
        return organid;
    }

    public void setOrganid(String organid) {
        this.organid = organid;
    }
}
