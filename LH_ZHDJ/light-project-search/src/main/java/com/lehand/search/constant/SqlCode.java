package com.lehand.search.constant;

public class SqlCode {
    /**
     * 根据搜索条件筛选
     *（党组织） 党组织名称，党组织隶属关系，党组织类别，行政区划，党组织书记，所在单位类别，地址，党组织所在单位情况，
     *（党员）姓名，籍贯,学历,身份证号码，工作岗位名称，党员类别名称，党员档案所在单位，毕业院校，学位名称，专业户籍所在地，
     *（会议） 会议id,组织名称，会议类型，主持人名称，记录人名称，地点，参会人身份证号码，参会人名称
     */
    public static final String pageSearch="pageSearch";
    /**
     * 添加搜索记录
     */
    public static final String insertSearchLog="insertSearchLog";
    /**
     * 获取所有未删除党员信息
     */
    public static final String listALLTDyInfo="listALLTDyInfo";
    /**
     * 获取所有未删除党组织信息
     */
    public static final String listAllTDzzInfo="listAllTDzzInfo";
    /**
     * 删除搜索日志里所有党组织信息和党员信息
     */
    public static final String delSearchLogByOrganidUserid="delSearchLogByOrganidUserid";
    /**
     * 获取所有大于搜索日志最新时间领航计划
     */
    public static String listAllTDzzPartyBrand="listAllTDzzPartyBrand";
    /**
     * 获取所有大于搜索日志最新时间会议
     */
    public static String listALLMeeting="listALLMeeting";
    /**
     * 获取所有大于搜索日志最新时间攻坚项目
     */
    public static String listALLPmProject="listALLPmProject";
    /**
     * 获取所有大于搜索日志最新时间任务
     */
    public static String listAllTaskDeploy="listAllTaskDeploy";
    /**
     * 获取所有大于搜索日志最新时间考核
     */
    public static String listALLAssess="listALLAssess";
}
