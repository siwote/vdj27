package com.lehand.search.service;


import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.search.constant.SqlCode;
import com.lehand.search.dto.SearchDto;
import net.sf.jxls.tag.ForEachTag;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class SearchService {

    @Resource
    GeneralSqlComponent generalSqlComponent;


    public Pager page(Pager pager,String orgCode,String likeStr, Session session) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("orgcode",orgCode);
        paramMap.put("likeStr",likeStr);
        paramMap.put("pageNo",pager.getPageNo());
        paramMap.put("pageSize",pager.getPageSize());
        if(StringUtils.isEmpty(likeStr)){
            return pager;
        }
        generalSqlComponent.pageQuery(SqlCode.pageSearch,paramMap,pager);
        List<Map<String, Object>> rows = (List<Map<String, Object>>) pager.getRows();
        List<Map<String, Object>> results = new ArrayList<>();
        for (Map<String, Object> map:rows) {
            StringBuffer sb=new StringBuffer();
            Set<String> keys = map.keySet();
            for (String key:keys) {
                if(!StringUtils.isEmpty(map.get(key))){
                    sb.append(map.get(key)+" ");
                }
            }
            Map<String, Object> m = new HashMap<>();
            m.put("content",sb);
            switchId(m,map);
            results.add(m);
        }
        pager.setRows(results);
        return pager;
    }

    private void switchId(Map<String, Object> m,Map<String, Object> map) {
//         organid,userid,mrid,brandid,pmid,taskid,assessid
        if(!StringUtils.isEmpty(map.get("organid"))){
            m.put("id",map.get("organid"));
            m.put("title","党组织信息");
            m.put("type",1);
            m.put("mrtype","");
        }
        if(!StringUtils.isEmpty(map.get("userid"))){
            m.put("id",map.get("userid"));
            m.put("title","党员信息");
            m.put("type",2);
            m.put("mrtype","");
        }
        if(!StringUtils.isEmpty(map.get("mrid"))){
            m.put("id",map.get("mrid"));
            m.put("title","会议信息");
            m.put("type",3);
            m.put("mrtype",map.get("mrtypename"));
        }
        if(!StringUtils.isEmpty(map.get("brandid"))){
            m.put("id",map.get("brandid"));
            m.put("title","领航计划信息");
            m.put("type",4);
            m.put("mrtype","");
        }
        if(!StringUtils.isEmpty(map.get("pmid"))){
            m.put("id",map.get("pmid"));
            m.put("title","党组织攻坚项目信息");
            m.put("type",5);
            m.put("mrtype","");
        }
        if(!StringUtils.isEmpty(map.get("taskid"))){
            m.put("id",map.get("taskid"));
            m.put("title","任务信息");
            m.put("type",6);
            m.put("mrtype","");
        }
        if(!StringUtils.isEmpty(map.get("assessid"))){
            m.put("id",map.get("assessid"));
            m.put("title","考核信息");
            m.put("type",7);
            m.put("mrtype","");
        }
    }

    public void addSearch(SearchDto searchDto) {
        generalSqlComponent.insert(SqlCode.insertSearchLog,searchDto);
    }
}
