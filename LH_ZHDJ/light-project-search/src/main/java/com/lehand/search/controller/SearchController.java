package com.lehand.search.controller;


import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.components.web.controller.BaseController;
import com.lehand.search.service.SearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(
        value = "党建搜索",
        tags = {"党建搜索"}
)
@RestController
@RequestMapping("/lhdj/search")
public class SearchController extends BaseController {

    private static final Logger logger = LogManager.getLogger(SearchController.class);
    @Resource
    private SearchService searchService;

    @OpenApi(optflag = 0)
    @ApiOperation(value = "党建搜索查询", notes = "党建搜索查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "组织机构编码", required = true, paramType = "query", dataType = "int", defaultValue = ""),
            @ApiImplicitParam(name = "likeStr", value = "组织机构编码", required = true, paramType = "query", dataType = "int", defaultValue = "")
    })
    @RequestMapping("/page")
    public Message page(String code, String likeStr, Pager pager) {
        Message msg = new Message();
        try {
            msg.setData(searchService.page(pager, code, likeStr, getSession()));
            msg.success();
        } catch (Exception e) {
            logger.error(e);
            msg.fail("搜索查询错误！" + e.getMessage());
        }
        return msg;
    }
}
