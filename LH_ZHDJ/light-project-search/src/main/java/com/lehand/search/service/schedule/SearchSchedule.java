package com.lehand.search.service.schedule;


import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.evaluation.lib.dto.ElAssessDto;
import com.lehand.evaluation.lib.pojo.ElAssess;
import com.lehand.horn.partyorgan.constant.SysConstants;
import com.lehand.horn.partyorgan.dto.MemberInfoDto;
import com.lehand.horn.partyorgan.pojo.dzz.TDzzInfo;
import com.lehand.meeting.dto.MeetingRecordDto;
import com.lehand.pm.dto.PmProjectDto;
import com.lehand.search.constant.SqlCode;
import com.lehand.search.dto.SearchDto;
import com.lehand.search.service.SearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class SearchSchedule {
    //记录日志
    private static final Logger logger = LogManager.getLogger(SearchSchedule.class);
    //是否正在创建任务
    private static final AtomicBoolean CREATE = new AtomicBoolean(false);
    //线程池
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    private static final String YYYYMMDDHHMMDD = DateEnum.YYYYMMDDHHMMDD.format();

    private static final String YYYYMMDD = DateEnum.YYYYMMDD.format();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @Resource
    private GeneralSqlComponent generalSqlComponent;

    @Resource
    private SearchService searchService;

    @Transactional(rollbackFor = Exception.class)
    public void addSearch() {
        try {
            synchronized (CREATE) {
                if (CREATE.get()) {
                    logger.info("之前任务正在执行,本次不处理");
                    return;
                }
                CREATE.set(true);
            }
            logger.info("定时添加搜索信息开始......");
            //搜索日志信息集合
            List<SearchDto> searchDtos = new ArrayList<>();
            //参数集合
            Map<String, Object> paramMap = new HashMap<>();
            //最新时间
            String updatetime = generalSqlComponent.getDbComponent().getSingleColumn("SELECT updatetime FROM `t_search_log` ORDER BY updatetime DESC LIMIT 1", String.class, new Object[]{});
            String formatUpdatetime ="";
            if(StringUtils.isEmpty(updatetime)){
                updatetime="";
            }else {
                formatUpdatetime = sdf.format(sdf.parse(updatetime));
            }
            paramMap.put("updatetime",updatetime);
            paramMap.put("formatUpdatetime",formatUpdatetime);
            //删除之前的党组织信息和党员信息
            generalSqlComponent.delete(SqlCode.delSearchLogByOrganidUserid, paramMap);
            //获取所有党组织信息
            List<TDzzInfo> dzzInfos = generalSqlComponent.query(SqlCode.listAllTDzzInfo, paramMap);
            if (dzzInfos != null && dzzInfos.size() > 0) {
                addSearchByTDzzInfo(dzzInfos, searchDtos);
            }
            //获取所有党员信息
            List<MemberInfoDto> memberInfoDtos = generalSqlComponent.query(SqlCode.listALLTDyInfo, paramMap);
            if (memberInfoDtos != null && memberInfoDtos.size() > 0) {
                addSearchByTDyInfo(memberInfoDtos, searchDtos);
            }
            //获取所有大于搜索日志最新时间会议
            List<MeetingRecordDto> meetingRecordDtos = generalSqlComponent.query(SqlCode.listALLMeeting, paramMap);
            if (meetingRecordDtos != null && meetingRecordDtos.size() > 0) {
                addMeeting(meetingRecordDtos, searchDtos);
            }
            //获取所有大于搜索日志最新时间领航计划
            List<Map<String, Object>> partyBrands = generalSqlComponent.query(SqlCode.listAllTDzzPartyBrand, paramMap);
            if (partyBrands != null && partyBrands.size() > 0) {
                addPartyBrand(partyBrands, searchDtos);
            }
            //获取所有大于搜索日志最新时间攻坚项目
            List<PmProjectDto> PmProjectDtos = generalSqlComponent.query(SqlCode.listALLPmProject, paramMap);
            if (PmProjectDtos != null && PmProjectDtos.size() > 0) {
                setSearchDtoByPmProject(PmProjectDtos, searchDtos);
            }
            //获取所有大于搜索日志最新时间任务
            List<TkTaskDeploy> tkTaskDeploys = generalSqlComponent.query(SqlCode.listAllTaskDeploy, paramMap);
            if (tkTaskDeploys != null && tkTaskDeploys.size() > 0) {
                setSearchDtoByTkTaskDeploy(tkTaskDeploys, searchDtos);
            }
            //获取所有大于搜索日志最新时间考核
            List<ElAssessDto> elAssesss = generalSqlComponent.query(SqlCode.listALLAssess, paramMap);
            if (elAssesss != null && elAssesss.size() > 0) {
                setSearchDtoByElAssess(elAssesss, searchDtos);
            }
            //批量添加
            for (SearchDto searchDto : searchDtos) {
                searchDto.setUpdatetime(YYYYMMDDHHMMDD);
                searchService.addSearch(searchDto);
            }
            logger.info("定时添加搜索信息结束...");
        } catch (Exception e) {
            logger.error("定时添加搜索信息异常", e);
        } finally {
            CREATE.set(false);
        }
    }

    /**
     *
     */
    private void setSearchDtoByElAssess(List<ElAssessDto> elAssesss, List<SearchDto> searchDtos) {
        for (ElAssessDto dto : elAssesss) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrgancode(dto.getOrgcode());
            searchDto.setAssessid(String.valueOf(dto.getId()));
            searchDto.setAssessname(dto.getName());
            searchDto.setPackagename(dto.getPackagename());
            searchDto.setAssesscontent(dto.getContent());
            searchDtos.add(searchDto);
        }
    }

    /**
     * @param tkTaskDeploys
     * @param searchDtos
     */
    private void setSearchDtoByTkTaskDeploy(List<TkTaskDeploy> tkTaskDeploys, List<SearchDto> searchDtos) {
        for (TkTaskDeploy dto : tkTaskDeploys) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrgancode(dto.getOrgcode());
            searchDto.setTaskid(String.valueOf(dto.getId()));
            searchDto.setTkname(dto.getTkname());
            searchDto.setTkdesc(dto.getTkdesc());
            searchDto.setPublisher(dto.getPublisher());
            searchDtos.add(searchDto);
        }
    }

    /**
     * @param pmProjectDtos 攻坚项目集合
     * @param searchDtos
     */
    private void setSearchDtoByPmProject(List<PmProjectDto> pmProjectDtos, List<SearchDto> searchDtos) {
        for (PmProjectDto dto : pmProjectDtos) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrgancode(dto.getOrgcode());
            searchDto.setPmid(String.valueOf(dto.getId()));
            searchDto.setPmname(dto.getName());
            searchDto.setIntroduction(dto.getIntroduction());
            searchDto.setClosereason(dto.getClosereason());
            searchDtos.add(searchDto);
        }
    }

    /**
     * @param meetingRecordDtos 会议集合
     * @param searchDtos
     */
    private void addMeeting(List<MeetingRecordDto> meetingRecordDtos, List<SearchDto> searchDtos) {
        for (MeetingRecordDto dto : meetingRecordDtos) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrgancode(dto.getOrgcode());
            searchDto.setMrid(String.valueOf(dto.getMrid()));
            searchDto.setZjhm(dto.getIdcard());
            searchDto.setXm(dto.getUsername());
            searchDto.setMrtypename(dto.getMrtype());
            searchDto.setModeratorname(dto.getModeratorname());
            searchDto.setRecordername(dto.getRecordername());
            searchDto.setTopic(dto.getTopic());
            searchDto.setContext(dto.getContext());
            searchDto.setDecision(dto.getDecision());
            searchDtos.add(searchDto);
        }
    }

    /**
     * @param partyBrands 领航计划信息集合
     * @param searchDtos  搜索日志
     */
    private void addPartyBrand(List<Map<String, Object>> partyBrands, List<SearchDto> searchDtos) {
        for (Map<String, Object> map : partyBrands) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrgancode(String.valueOf(map.get("orgcode")));
            searchDto.setBrandid(String.valueOf(map.get("id")));
            searchDto.setBrandname(String.valueOf(map.get("name")));
            searchDto.setSlogan(String.valueOf(map.get("slogan")));
            searchDto.setIntroduce(String.valueOf(map.get("introduce")));
            searchDto.setConcept(String.valueOf(map.get("concept")));
            searchDto.setTrainofthought(String.valueOf(map.get("trainofthought")));
            searchDto.setMeasures(String.valueOf(map.get("measures")));
            searchDto.setTitle(String.valueOf(map.get("title")));
            searchDto.setContent(String.valueOf(map.get("content")));
            searchDto.setCreatetime(String.valueOf(map.get("createtime")));
            searchDtos.add(searchDto);
        }
    }

    /**
     * @param memberInfoDtos 所有党员信息集合
     * @param searchDtos     搜索日志信息集合
     */
    private void addSearchByTDyInfo(List<MemberInfoDto> memberInfoDtos, List<SearchDto> searchDtos) {
        for (MemberInfoDto memberInfoDto : memberInfoDtos) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrgancode(memberInfoDto.getOrgancode());
            searchDto.setUserid(memberInfoDto.getUserid());
            searchDto.setXm(memberInfoDto.getXm());
            /**
             * 0000000002	未知的性别
             * 1000000003	男
             * 2000000004	女
             */
            String xb = memberInfoDto.getXb();
            if (SysConstants.DYXB.MALE.equals(xb)) {
                searchDto.setXbname("男");
            } else if (SysConstants.DYXB.FEMALE.equals(xb)) {
                searchDto.setXbname("女");
            } else {
                searchDto.setXbname("没有说明的");
            }
            String mz = memberInfoDto.getMz();
            searchDto.setMzname(getGDictItem("d_national", mz) == null ? mz : getGDictItem("d_national", mz));
            searchDto.setJgname(memberInfoDto.getJg());
            String xl = memberInfoDto.getXl();
            searchDto.setXlname(getGDictItem("d_dy_xl", xl) == null ? xl : getGDictItem("d_dy_xl", xl));
            searchDto.setZjhm(memberInfoDto.getZjhm());
            String gzgw = memberInfoDto.getGzgw();
            searchDto.setGzgwname(getGDictItem("d_dy_gzgw", gzgw) == null ? gzgw : getGDictItem("d_dy_gzgw", gzgw));
            String dylb = memberInfoDto.getDylb();
            searchDto.setDylbname(switchDYLB(dylb));
            searchDto.setXjzd(memberInfoDto.getXjzd());
            searchDto.setCreatetime(memberInfoDto.getCreatetime());
            searchDtos.add(searchDto);
        }
    }

    /**
     * 判断党员类别
     *
     * @param dylb
     * @return
     */
    private String switchDYLB(String dylb) {
        switch (dylb) {
            case "1000000001":
                return "正式党员";
            case "2000000002":
                return "预备党员";
            case "3000000003":
                return "发展党员";
            case "4000000004":
                return "入党积极分子";
            case "5000000005":
                return "入党申请人";
            default:
                return "无";
        }
    }

    /**
     * 根据
     *
     * @param code
     * @param itemCode
     * @return
     */
    private String getGDictItem(String code, String itemCode) {
        return  generalSqlComponent.getDbComponent().getSingleColumn("SELECT item_name FROM g_dict_item where code = ? and item_code=? limit 1", String.class, new Object[]{code, itemCode});
    }

    /**
     * @param dzzInfos   所有党组织信息集合
     * @param searchDtos 搜索日志信息集合
     */
    private void addSearchByTDzzInfo(List<TDzzInfo> dzzInfos, List<SearchDto> searchDtos) {
        for (TDzzInfo tDzzInfo : dzzInfos) {
            SearchDto searchDto = new SearchDto();
            searchDto.setOrganid(tDzzInfo.getOrganid());
            searchDto.setOrgancode(tDzzInfo.getOrgancode());
            searchDto.setDzzmc(tDzzInfo.getDzzmc());
            searchDto.setDzzlsgx(tDzzInfo.getDzzlsgx());
            searchDto.setAddress(tDzzInfo.getAddress());
            searchDto.setDzzsj(tDzzInfo.getDzzsj());
            searchDto.setXzqh(tDzzInfo.getXzqh());
            searchDto.setCreatetime(tDzzInfo.getCreatetime());
            searchDtos.add(searchDto);
        }
    }

    public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
        return threadPoolTaskExecutor;
    }

    public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.threadPoolTaskExecutor = threadPoolTaskExecutor;
    }

}
