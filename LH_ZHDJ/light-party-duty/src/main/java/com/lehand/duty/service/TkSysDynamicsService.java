package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.duty.dao.TkSysDynamicsDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TkSysDynamicsService  {

	@Resource private TkSysDynamicsDao tkSysDynamicsDao;
	
	/**
	 * 系统动态分页查询
	 */
	public void page(Session session,Pager pager) {
		tkSysDynamicsDao.page(session, pager);
	}
}
