package com.lehand.duty.service;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkImportDeploy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TkImportDeployService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public long insert(TkImportDeploy info) {
		return generalSqlComponent.insert(SqlCode.TkImportDeployInsert, info);
	}
	
	public int updateRemarks1(Long compid,String updatetime,int remark1,long id) {
		return generalSqlComponent.update(SqlCode.tkImportDeployUpdate, new Object[] {updatetime, remark1, compid,id});
	}
}
