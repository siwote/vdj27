package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.pojo.TkTaskSubject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Service
public class TkTaskSubjectService {

	@Resource
	GeneralSqlComponent generalSqlComponent;
	/**
	 * 新增任务主体返回id
	 * @param info
	 * @return
	 */
	public int insert(TkTaskSubject info) {
		return generalSqlComponent.insert(SqlCode.insertTkTaskSubject,info);
	}

	/**
	 * 新增任务主体
	 * @param compid
	 * @param taskid
	 * @param flag
	 * @param sjtype
	 * @param subjectid
	 * @param subjectname
	 * @param remarks3
	 */
	public void insert(Long compid,Long taskid,int flag, int sjtype,Long subjectid,String subjectname,String remarks3) {
		TkTaskSubject sub = new TkTaskSubject();
		sub.setCompid(compid);
		sub.setTaskid(taskid);
		sub.setSubjectid(subjectid);
		sub.setSjtype(sjtype);
		sub.setFlag(flag);
		sub.setSubjectname(subjectname);
		sub.setRemarks1(0);
		sub.setRemarks2(0);
		sub.setRemarks3(remarks3);
		sub.setRemarks4(Constant.EMPTY);
		sub.setRemarks5(Constant.EMPTY);
		sub.setRemarks6(Constant.EMPTY);
		insert(sub);
	}

	/**
	 * 删除牵头领导
	 * @param compid
	 * @param taskid
	 * @param flag
	 * @return
	 */
	public int deleteByFlagTaskid(Long compid,Long taskid,int flag) {
//		return super.delete("DELETE FROM tk_task_subject WHERE compid=? and sjtype = 0 and taskid = ? and flag = ?", compid,taskid,flag);
		return generalSqlComponent.delete(SqlCode.deleteByFlagTaskid,new Object[]{compid,taskid,flag});
	}

	/**
	 * 根据任务id删除任务主体
	 * @param compid
	 * @param taskid
	 * @return
	 */
	public int deleteByTaskid(Long compid,Long taskid) {
//		return super.delete("DELETE FROM tk_task_subject WHERE compid=?  and taskid = ? and remarks1 !=-1 ", compid,taskid);
		return generalSqlComponent.delete(SqlCode.deleteByTaskid,new Object[]{compid,taskid});
	}

	/**
	 * 删除非执行人的所有相关人员
	 * @param compid
	 * @param taskid
	 * @return
	 */
	public int delNotExcuterByTaskid(Long compid,Long taskid) {
//		return super.delete("DELETE FROM tk_task_subject WHERE compid=?  and taskid = ? and flag !=0 ", compid,taskid);
		return generalSqlComponent.delete(SqlCode.delNotExcuterByTaskid,new Object[]{compid,taskid});
	}
	
	/**
	 * 查询某任务的主体(不分页)
	 * @param taskid
	 * @param flag   
	 * @return
	 */
	public List<TkTaskSubject> findSubjects(Long compid,long taskid,int flag){
//		return super.list2bean(findByTaskIdAndFlag, TkTaskSubject.class, compid,taskid,flag);
		return generalSqlComponent.delete(SqlCode.listSubjectsByTaskid,new Object[]{compid,taskid,flag});
	}

	/**
	 * 根据任务查询主体数
	 * @param compid
	 * @param taskid
	 * @param flag
	 * @return
	 */
	public int findSubjectsSize(Long compid,long taskid,int flag){
//		return super.getInt("SELECT count(*) FROM tk_task_subject WHERE compid=? and taskid=? and flag=? and remarks1!=-1", compid,taskid,flag);
		return generalSqlComponent.query(SqlCode.getSubjectsCountByTaskid,new Object[]{compid,taskid,flag});
	}

	/**
	 * 查询任务的执行人和关注人
	 * @param compid
	 * @param taskid
	 * @return
	 */
	public List<TkTaskSubject> findSubjects(Long compid,long taskid){//查询任务的执行人和关注人
//		return super.list2bean(findByTaskIdAndremarks1, TkTaskSubject.class, compid,taskid);
		return generalSqlComponent.query(SqlCode.listByTaskIdAndremarks1,new Object[]{compid,taskid});
	}

	/**
	 * 查询任务的执行人和关注人
	 * @param compid
	 * @param taskid
	 * @return
	 */
	public List<Subject> listSubjects(Long compid, long taskid){//查询任务的执行人和关注人
//		return super.list2bean(findByTaskIdAndremarks2, Subject.class, compid,taskid);
		return generalSqlComponent.query(SqlCode.listByTaskIdAndremarks2,new HashMap(){{put("compid",compid);put("taskid",taskid);}});
	}

	/**
	 * 查询任务的执行人和关注人
	 * @param compid
	 * @param taskid
	 * @param flag
	 * @return
	 */
	public List<Subject> listSubjects(Long compid,long taskid,int flag){//查询任务的执行人和关注人
//		return super.list2bean(findByTaskIdAndremarks2+" and flag=?", Subject.class, compid,taskid,flag);
		return generalSqlComponent.query(SqlCode.listByTaskIdAndremarks2,new HashMap(){{put("compid",compid);put("taskid",taskid);put("flag",flag);}});
	}
	
	/**
	 * 查询某任务的主体(分页查询)
	 * @author pantao
	 * @date 2018年12月3日上午11:58:27
	 * 
	 * @param taskid
	 * @param flag
	 * @param pager
	 * @return
	 */
	public Pager findSubjectsPage(Long compid, long taskid, int flag, Pager pager, String name){
//		if(StringUtils.isEmpty(name)) {
//			return super.pageBeanNotCount(findByTaskId, pager, StatusSubject.class, compid,taskid,flag);
//		}else {
//			return super.pageBeanNotCount(findByTaskId+" and subjectname like '%"+name+"%'", pager,StatusSubject.class, compid,taskid,flag);
//		}
		return generalSqlComponent.pageQuery(SqlCode.pageSubjectsByTaskid,
				new HashMap(){{put("compid",compid);put("taskid",taskid);put("flag",flag);put("name",name);}},pager);
	}
	
	/**
	 * 获取执行人总数
	 * @param taskid
	 * @param flag
	 * @param name
	 * @return
	 */
	public int count(Long compid,long taskid,int flag,String name){
//		if(StringUtils.isEmpty(name)) {
//			return super.getInt(num, compid,taskid,flag);
//		}else {
//			return super.getInt(num+" and subjectname like '%"+name+"%'",compid,taskid,flag);
//		}

		return generalSqlComponent.query(SqlCode.getSubjectsCountByTaskidp,
				new HashMap(){{put("compid",compid);put("taskid",taskid);put("flag",flag);put("name",name);}});
	}

	/**
	 * 获取执行人总数(包含删除的执行人)
	 * @param taskid
	 * @param flag
	 * @return
	 */
	public int countContainDel(Long compid,long taskid,int flag){

//		return super.getInt(num2, compid,taskid,flag);
		return generalSqlComponent.query(SqlCode.getSubjectsCountByTaskidt,
				new Object[]{compid,taskid,flag});
	}

	/**
	 * 根据主任务id和执行人id删除主题表中该任务的该执行人
	 * @param id
	 * @param userid
	 */
	public void deleteSubject(Long id, Long userid) {
//		super.update("update tk_task_subject set remarks1 = -1 WHERE taskid =? and flag = 0 and sjtype = 0 and subjectid = ?", id,userid);
		generalSqlComponent.update(SqlCode.updateSubjectRemarks1,new Object[]{id,userid});
	}
	
	/**
	 * 查询任务的关注人
	 */
	public List<TkTaskSubject> listBytaskid(Long compid,Long taskid,int flag){
//		return super.list2bean("SELECT * FROM tk_task_subject where compid=? and taskid = ? and flag = ? and sjtype = 0", TkTaskSubject.class, compid,taskid,flag);
		return generalSqlComponent.query(SqlCode.listTaskFollowers,new Object[]{compid,taskid,flag});
	}
	/**
	 * 查询任务退回的执行人
	 */
	public List<TkTaskSubject> listReturnBytaskid(Long compid,Long taskid){
//		return super.list2bean("SELECT * FROM tk_task_subject where compid=? and taskid = ? and remarks1 = -1", TkTaskSubject.class, compid,taskid);
//		return findSubjects(compid,taskid);
		return generalSqlComponent.getDbComponent()
				.listBean("SELECT * FROM tk_task_subject where compid=? and taskid = ? and remarks1 = -1",TkTaskSubject.class,new Object[]{compid,taskid});
	}
	
	/**
	 * 查询该人是否关注该任务：主体类型flag为1表示为关注主体
	 * @param subjectid
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
	public List<TkTaskSubject> isFoucs(Long compid,Long subjectid,Long taskid){
//		return super.list2bean(isFocus, TkTaskSubject.class, compid,taskid,subjectid);
		return generalSqlComponent.query(SqlCode.listTkIsFoucs,new Object[]{compid,taskid,subjectid});
	}
	
	/**
	 * 取消关注任务，将关注时新增的flag为1的数据删除（真删除）
	 * @param taskid
	 * @param userid
	 * @author yuxianzhu
	 */
	public void cancelFocus(Long compid,Long userid,Long taskid){
//		super.delete(cancelFocus, new Object[]{ compid,taskid,userid });
		generalSqlComponent.delete(SqlCode.deleteTkFoucsByUserid,new Object[]{ compid,taskid,userid });
	}

	public void delete(Long compid,Long taskid) {
//		super.delete("DELETE FROM tk_task_subject WHERE compid=? and taskid = ?", compid,taskid);
		generalSqlComponent.delete(SqlCode.deleteSubjectByTaskid,new Object[]{compid,taskid});
	}
	
	/**
	 * 通过flag和人员id删除任务id（取消数据授权的时候用）
	 * @param subjectid
	 * @param flag
	 */
	public int deleteByFlag(Long compid,Long subjectid,int flag) {
//		return super.delete("DELETE FROM tk_task_subject WHERE compid=? and sjtype = 0 and subjectid = ? and flag = ?", compid,subjectid,flag);
		return generalSqlComponent.delete(SqlCode.deleteSubjectByFlag,
				new HashMap<String,Object>(){{put("compid",compid);put("subjectid",subjectid);put("flag",flag);}});
	}

	/**
	 * 通过flag和人员id删除任务id（取消数据授权的时候用）
	 * @param subjectid
	 * @param flag
	 */
	public int deleteByFlagAndSubjectId(Long taskid,Long compid,Long subjectid,int flag) {
//		return super.delete("DELETE FROM tk_task_subject WHERE taskid = ? and compid=? and sjtype = 0 and subjectid = ? and flag = ?", taskid,compid,subjectid,flag);
		return generalSqlComponent.delete(SqlCode.deleteSubjectByFlag,
				new HashMap<String,Object>(){{put("compid",compid);put("subjectid",subjectid);put("flag",flag);put("taskid",taskid);}});
	}

	/**
	 * 根据执行人id查询name
	 * @param
	 * @param subjectid
	 */
	public TkTaskSubject getBySubject(Long compid,Long subjectid) {
//		return super.get("select subjectname from tk_task_subject where compid=? and subjectid = ? limit 0,1", TkTaskSubject.class, compid,subjectid);
		return generalSqlComponent.query(SqlCode.getTkSubjectName,new Object[]{compid,subjectid});
	}
	
	//用新改的subjectid来查，如果not in有数据则为已经移除的
	public List<TkTaskSubject> getTkTaskSubjectRemove(Long compid, Long taskid, String newSubject){
//		return super.list2bean(getByTaskidAndSubidRemove, TkTaskSubject.class, compid,taskid,newSubject );
		return generalSqlComponent.query(SqlCode.listByTaskidAndSubidRemove,new Object[]{compid,taskid,newSubject});
	}
	
	//修改执行人时查询执行人
	public int getTkTaskSubject(Long taskid,Long subjectid){
//		return super.getInt(getByTaskidAndSubid, taskid,subjectid);
		return generalSqlComponent.query(SqlCode.getCountByTaskidAndSubid,new Object[]{taskid,subjectid});
	}
	
	public TkTaskSubject getTkTaskSubjectBy(Long taskid,Long subjectid){
//		return super.get(getByTaskidAndSubid,TkTaskSubject.class, taskid,subjectid);
		return generalSqlComponent.query(SqlCode.getByTaskidAndSubid,new Object[]{taskid,subjectid});
	}

	public void updateByRemarks1(Long compid,Long taskid, Long subjectid) {
//		super.update("UPDATE tk_task_subject SET  remarks1 = 0 WHERE compid=? and taskid = ? and flag = 0 and sjtype = 0 and subjectid = ?", compid,taskid,subjectid);
		generalSqlComponent.update(SqlCode.updateByRemarks1,new Object[]{compid,taskid,subjectid});
	}

	public void updateByRemarks2(Long compid,Long taskid, Long subjectid) {
//		super.update("UPDATE tk_task_subject SET  remarks2 = 0 WHERE compid=? and taskid = ? and flag = 0 and sjtype = 0 and subjectid = ?", compid,taskid,subjectid);
		generalSqlComponent.update(SqlCode.updateByRemarks1,new Object[]{compid,taskid,subjectid});
	}
	/**
	 * 暂停执行人的任务过后需要更新主题表中该执行人对应的remarks3 为 suspend(为了反馈详情列表中显示执行人列表中每个执行人的状态)
	 * @param taskid
	 * @param str
	 * @param subjectid
	 */
	public void updateRemarks3(Long compid,Long taskid, String str,Long subjectid) {
//		super.update("UPDATE tk_task_subject SET  remarks3 = ? WHERE compid=? and taskid = ? and flag = 0 and sjtype= 0 and subjectid = ?", str,compid,taskid,subjectid);

		generalSqlComponent.update(SqlCode.updateByRemarks3,new Object[]{str,compid,taskid,subjectid});
	}

	/**
	 * 修该执行人时，将去除的执行人从主体表中删除掉（有可能存在多个）
	 * @param taskid
	 * @param userid
	 */
	public void deleteSubjectByIn(Long compid,Long taskid, String userid) {
//		super.delete("DELETE FROM tk_task_subject WHERE compid=? and taskid = ? and flag = 0 and sjtype = 0 and subjectid in ("+userid+")", compid,taskid);
		generalSqlComponent.delete(SqlCode.deleteSubjectByIn,new Object[]{compid,taskid,userid});
	}

	/**
	 * 查询督办人关注的任务
	 * @param subjectid
	 * @param flag
	 */
	public List<TkTaskSubject> listByUseridAndFlag(Long compid,Long subjectid,int flag) {
//		return super.list2bean("select * FROM tk_task_subject WHERE compid=? and sjtype = 0 and subjectid = ? and flag = ?", TkTaskSubject.class, compid,subjectid,flag);
		return generalSqlComponent.query(SqlCode.listByUseridAndFlag,new Object[]{compid,subjectid,flag});
	}

	/**
	 * 查询角色关系表 remarks2>0 的
	 * 和role_map表关联
	 * @param compid
	 * @param subjectid
	 * @return
	 */
	public int getRemarks2BySubid(Long compid,Long subjectid){
		// 原业务关联 select remarks2 from pw_role_map where compid=? and subjecttp=0 and subjectid=? and remarks2>0

		return generalSqlComponent.query(SqlCode.getRemarks2BySubid,new Object[]{compid,subjectid});
	}
}
