package com.lehand.duty.dto;


import com.lehand.duty.pojo.TkTaskSubject;

import java.util.List;

public class SubjectReponse extends TkTaskSubject {

	private Long feedCount;//应当上报数

	private Long actualCount;//实际上报数

	private Long overCount; //逾期上报数

	private Long overNotCount;//逾期未报数

	private List<String> remindColor; //反馈状态

	private String status;//返给前端的执行人删除状态 0 （）

	private Integer remarks2;//执行人状态  -1表示删除

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public Integer getRemarks2() {
		return remarks2;
	}

	@Override
	public void setRemarks2(Integer remarks2) {
		this.remarks2 = remarks2;
	}


	public Long getFeedCount() {
		return feedCount;
	}

	public void setFeedCount(Long feedCount) {
		this.feedCount = feedCount;
	}

	public Long getActualCount() {
		return actualCount;
	}

	public void setActualCount(Long actualCount) {
		this.actualCount = actualCount;
	}

	public Long getOverCount() {
		return overCount;
	}

	public void setOverCount(Long overCount) {
		this.overCount = overCount;
	}

	public Long getOverNotCount() {
		return overNotCount;
	}

	public void setOverNotCount(Long overNotCount) {
		this.overNotCount = overNotCount;
	}

	public List<String> getRemindColor() {
		return remindColor;
	}

	public void setRemindColor(List<String> remindColor) {
		this.remindColor = remindColor;
	}

	public SubjectReponse() {
	}


}
