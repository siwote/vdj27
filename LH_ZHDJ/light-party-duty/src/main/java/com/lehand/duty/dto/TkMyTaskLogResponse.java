package com.lehand.duty.dto;

import com.lehand.duty.pojo.TkAttachment;

import java.util.List;

public class TkMyTaskLogResponse {

	private Long id;
	private Double progress;
	private String opttime;//c操作时间
	private String mesg;//进展
	private String username;//名称
	private List<TkAttachment> attachment;//附件
	private List<TkCommentResponse> comment;//评论
	private Boolean isshow;
	private String opttype;//操作类型
	private Boolean isopen;
	private int commentnum=0;//评论条数
	private String backReason;
	private int parisenum;//点赞数量
	private int flag;//是否已点赞0表示未点赞，1表示已点赞
	private int auditstatus;//反馈记录的审核状态
	
	
	
	public int getParisenum() {
		return parisenum;
	}
	public void setParisenum(int parisenum) {
		this.parisenum = parisenum;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public String getBackReason() {
		return backReason;
	}
	public void setBackReason(String backReason) {
		this.backReason = backReason;
	}
	public int getCommentnum() {
		return commentnum;
	}
	public void setCommentnum(int commentnum) {
		this.commentnum = commentnum;
	}
	public Boolean getIsopen() {
		return isopen;
	}
	public void setIsopen(Boolean isopen) {
		this.isopen = isopen;
	}
	public Boolean getIsshow() {
		return isshow;
	}
	public void setIsshow(Boolean isshow) {
		this.isshow = isshow;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getProgress() {
		return progress;
	}
	public void setProgress(Double progress) {
		this.progress = progress;
	}
	public String getOpttime() {
		return opttime;
	}
	public void setOpttime(String opttime) {
		this.opttime = opttime;
	}
	public String getMesg() {
		return mesg;
	}
	public void setMesg(String mesg) {
		this.mesg = mesg;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<TkAttachment> getAttachment() {
		return attachment;
	}
	public void setAttachment(List<TkAttachment> attachment) {
		this.attachment = attachment;
	}
	public List<TkCommentResponse> getComment() {
		return comment;
	}
	public void setComment(List<TkCommentResponse> comment) {
		this.comment = comment;
	}
	public String getOpttype() {
		return opttype;
	}
	public void setOpttype(String opttype) {
		this.opttype = opttype;
	}
	public int getAuditstatus() {
		return auditstatus;
	}
	public void setAuditstatus(int auditstatus) {
		this.auditstatus = auditstatus;
	}
	
}
