package com.lehand.duty.controller.setting;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkTaskAttributeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greenpineyu.fel.common.StringUtils;
import com.lehand.base.common.Message;
import com.lehand.duty.controller.DutyBaseController;

/**
 * 任务属性
 * @author pantao
 */
@Api(value = "任务属性", tags = { "任务属性" })
@RestController
@RequestMapping("/duty/taskAttribute")
public class TkTaskAttributeController extends DutyBaseController{

    private static final Logger logger = LogManager.getLogger(TkTaskAttributeController.class);
	
	@Resource private TkTaskAttributeService tkTaskAttributeService;
	
	/**
	 * 保存任务属性
	 * @param data
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "保存任务属性", notes = "保存任务属性", httpMethod = "POST")
	@RequestMapping("/save")
	public Message save(String data) {
		Message message = new Message();
		try {	
			if(StringUtils.isEmpty(data)) {
				message.setMessage("参数不能为空！");
				return message;
			}
			tkTaskAttributeService.save(getSession().getCompid(),data);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 查询任务属性
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询任务属性", notes = "查询任务属性", httpMethod = "POST")
	@RequestMapping("/query")
	public Message query() {
		Message message = new Message();
		try {			
			message.success().setData(tkTaskAttributeService.query(getSession().getCompid()));;
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
}
