package com.lehand.duty.controller.task;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkTaskRemindListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.duty.controller.DutyBaseController;

@Api(value = "提醒列表", tags = { "提醒列表" })
@RestController
@RequestMapping("/duty/remindList")
public class TkTaskRemindListController extends  DutyBaseController{
	
	@Resource
	TkTaskRemindListService tkTaskRemindListService;
    private static final Logger logger = LogManager.getLogger(TkTaskRemindListController.class);

	@OpenApi(optflag=0)
	@ApiOperation(value = " 分页", notes = " 分页", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(int flag,Pager pager) {
		Message message = new Message();
		try {	
			Pager page = tkTaskRemindListService.page(flag,getSession(),pager);
			message.setData(page);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	/**
	 *查询未读消息总条数
	 * @author pantao
	 * @date 2018年11月14日下午5:11:57
	 * 
	 * @param flag
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 查询未读消息总条数", notes = " 查询未读消息总条数", httpMethod = "POST")
	@RequestMapping("/getNum")
	public Message getNum(int flag) {
		Message message = new Message();
		try {	
			int num = tkTaskRemindListService.getCount(flag,getSession());
			message.setData(num);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 更新消息的已读状态
	 * @param noteid
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "更新消息的已读状态", notes = "更新消息的已读状态", httpMethod = "POST")
	@RequestMapping("/update")
	public Message update(Long noteid) {
		Message message = new Message();
		try {
			tkTaskRemindListService.update(getSession().getCompid(),noteid);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	   * 批量更新消息的已读状态
	 * @param noteids
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "批量更新消息的已读状态", notes = "批量更新消息的已读状态", httpMethod = "POST")
	@RequestMapping("/batchupdate")
	public Message batchUpdate(String noteids) {
		Message message = new Message();
		try {
			tkTaskRemindListService.batchUpdate(getSession().getCompid(),noteids);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
}
