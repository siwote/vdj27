package com.lehand.duty.component;

import com.alibaba.fastjson.JSON;
import com.lehand.base.exception.LehandException;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.service.TkTaskRemindNoteService;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.pojo.TkRemindRule;
import com.lehand.duty.pojo.TkTaskRemindNote;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class RemindNoteComponent {
	
	private static final Logger logger = LogManager.getLogger(RemindNoteComponent.class);
	
	@Resource 
	private DutyCacheComponent dutyCacheComponent;
	@Resource 
	private TkTaskRemindNoteService tkTaskRemindNoteService;
	@Resource 
	private TkTaskDeployDao tkTaskDeployDao;
	
	/**
	 * 提醒登记
	 * @param module
	 * @param id
	 * @param ruleEnum
	 * @param times 提醒的时间列表
	 * @param subjects 提醒的用户列表,(预留,有就传,没有就不传)  [{userid:"",username:""},{.....}]  
	 * @throws LehandException
	 */
	public void register(Long compid, Module module, Long id, RemindRuleEnum ruleEnum, Set<String> times, Subject handler, Set<Subject> subjects) throws LehandException {
		TkRemindRule rule = dutyCacheComponent.getTkRemindRule(compid,ruleEnum);
		if (!rule.isOpen()) {
			return;
		}
		for (String time : times) {
			saveRemindNote(compid,module, id, ruleEnum, handler, subjects, time, null);
		}
	}
	
	/**
	 * 提醒登记
	 * @param module
	 * @param id
	 * @param ruleEnum
	 * @param time
	 * @param subject
	 * @throws LehandException
	 */
	public void register(Long compid,Module module,Long id,RemindRuleEnum ruleEnum,String time,Subject handler,Subject subject) throws LehandException {
		Set<Subject> subs = new HashSet<Subject>(1);
		subs.add(subject);
		register(compid,module, id, ruleEnum, time, handler, subs);
	}
	
	public void register(Long compid,Module module,Long id,RemindRuleEnum ruleEnum,String time,Subject handler,Subject subject,Map<String, Object> remindParams) throws LehandException {
		TkRemindRule rule = dutyCacheComponent.getTkRemindRule(compid,ruleEnum);
		if (!rule.isOpen()) {
			return;
		}
		Set<Subject> subs = new HashSet<Subject>(1);
		subs.add(subject);
		saveRemindNote(compid,module, id, ruleEnum, handler, subs, time, remindParams);
	}
	
	public void register(Long compid,Module module,Long id,RemindRuleEnum ruleEnum,String time,Subject handler,Set<Subject> subjects) throws LehandException {
		TkRemindRule rule = dutyCacheComponent.getTkRemindRule(compid,ruleEnum);
		if (!rule.isOpen()) {
			return;
		}
		saveRemindNote(compid,module, id, ruleEnum, handler, subjects, time, null);
	}
	
	public void register(Long compid,Module module,Long id,RemindRuleEnum ruleEnum,String time,Subject handler,Set<Subject> subjects,Map<String, Object> remindParams) throws LehandException {
		TkRemindRule rule = dutyCacheComponent.getTkRemindRule(compid,ruleEnum);
		if (!rule.isOpen()) {
			return;
		}
		saveRemindNote(compid,module, id, ruleEnum, handler, subjects, time, remindParams);
	}
	
	private void saveRemindNote(Long compid,Module module, Long id, RemindRuleEnum ruleEnum, Subject handler,Set<Subject> subjects, String time, Map<String, Object> remindParams) {
		TkTaskRemindNote info = new TkTaskRemindNote();
		info.setCompid(compid);
		info.setModule(module.getValue());//模块(0:任务发布,1:任务反馈)
		info.setMid(id);//对应模块的ID
		info.setRemindtime(time);//提醒日期
		info.setRulecode(ruleEnum.name());//编码
		info.setOptuser(JSON.toJSONString(handler));
		info.setNoticeusers(JSON.toJSONString(subjects));//提醒人列表
		info.setIssend(Constant.TkTaskRemindNote.SEND_NO);//是否已提醒(0:未提醒,1:已提醒)
		info.setMesg(Constant.EMPTY);//消息
		info.setRemarks1(0);//备注1
		info.setRemarks2(0);//备注2
		if (remindParams==null || remindParams.size()<=0) {
			info.setRemarks4(Constant.EMPTY);//备注4
		} else {
			info.setRemarks4(JSON.toJSONString(remindParams));//备注4
		}
		info.setRemarks3(Constant.EMPTY);//备注3
		info.setRemarks5(Constant.EMPTY);//备注3
		info.setRemarks6(Constant.EMPTY);//备注3
		tkTaskRemindNoteService.insert(info);
		if (logger.isDebugEnabled()) {
			logger.debug("提醒登记:{}",JSON.toJSONString(info));
		}
	}
	
}
