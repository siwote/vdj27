package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.component.RemindNoteComponent;
import com.lehand.duty.dao.*;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.dto.*;
import com.lehand.duty.pojo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

/**
 * 
 * 分解任务接口实现类 
 * 
 * @author pantao  
 * @date 2018年10月22日 上午9:56:46
 *
 */
@Service
public class TkTaskResolveService {

	private static final Logger logger = LogManager.getLogger(TkTaskResolveService.class);

	@Resource private TkDictService tkDictDao;
	@Resource private TkTaskDeployService tkTaskDeployBusinessImpl;
	@Resource private TkAttachmentService tkAttachmentService;
	@Resource private TkTaskClassDao tkTaskClassDao;
	@Resource private TkResolveInfoService tkResolveInfoDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkTaskSubjectService tkTaskSubjectService;
	@Resource private RemindNoteComponent remindNoteComponent;
	@Resource private TkPraiseDetailsService tkPraiseDetailsDao;
	@Resource private TkCommentService tkCommentDao;
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkMyTaskLogService tkMyTaskLogDao;
	@Resource private TkResolveListDao tkResolveListDao;
	@Resource private TkIntegralRuleService tkIntegralRuleService;
	@Resource private TkIntegralDetailsService tkIntegralDetailsService;
	@Resource private TkTaskDeploySubDao tkTaskDeploySubDao;
	@Resource private TkCommentReplyService tkCommentReplyDao;
	@Resource private TkTaskTimeNodeService tkTaskTimeNodeService;
	/**
	 * 查询前台下拉框数据
	 */
	public List<TkDict> list(Long compid, String pcode) {
		return tkDictDao.list(compid,pcode);
	}


	/**
	 * 部署普通分解任务
	 */
	@Transactional(rollbackFor = Exception.class)
	public Long deployCommonTask(TkTaskDeployRequest request, Session session) throws Exception {
		logger.info("发布普通分解任务...");
		// 首先判断开始时间是否小于当前时间
		if (request.getStarttime().compareTo(DateEnum.YYYYMMDD.format()) < 0) {
			LehandException.throwException("任务开始时间不能小于当前时间");
		}
		// 部署任务前先验证分类的class id在分类表里是否存在
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		// 新增部署信息
		TkTaskDeploy info = tkTaskDeployBusinessImpl.insertTaskDeployInfo(request, session, Constant.TkTaskDeploy.PERIOD_RESOLVE, Constant.TkTaskDeploy.RESOLVE_NO);
		logger.debug("普通分解任务ID:{}", info.getId());
		// 附件绑定
		if (!StringUtils.isEmpty(request.getUuid())) {
			logger.debug("绑定附件,UUID:{},任务ID:{}", request.getUuid(), info.getId());
			tkAttachmentService.updateId(session.getCompid(),info.getId(), Constant.TkAttachments.MODEL_DEPLOY, request.getUuid());
		}
		// 时间节点表
		tkTaskDeployBusinessImpl.insertTkTaskTimeNode(info.getCompid(),info.getId(), info.getStarttime(), info.getEndtime(),0);
		// 提醒
		Set<String> times = tkTaskDeployBusinessImpl.insertTkTaskRemindNote(info, request, session);
		// 执行人
		tkTaskDeployBusinessImpl.insertTkTaskSubject(session,info, request);
		insertResolveInfo(request, session, info);
		TaskProcessEnum.DEPLOY.handle(session, request, info, times);
		return info.getId();
	}


	/**
	 * 将数据插入分解表
	 * @throws LehandException
	 */
	@SuppressWarnings("rawtypes")
	private void insertResolveInfo(TkTaskDeployRequest request, Session session, TkTaskDeploy info)
			throws ParseException, LehandException {
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		String timedata = request.getTimedataresolve();
		Map maps = JSON.parseObject(timedata);
		String timecode = maps.get("timecode").toString();// 时间编码
		String timename = tkDictDao.get(session.getCompid(),timecode).getName();// 时间名称
		int day = Integer.valueOf(maps.get("days").toString());
		// 保存分解信息
		List<Map> listMap = request.getJsons();
		if (listMap != null && listMap.size() > 0) {
			for (Map map : listMap) {
				String id = map.get("id").toString();
				Long taskid = info.getId();// 任务id
				String nodename = map.get("topic").toString();// 节点名称
				Double idxvalue = Double.valueOf(map.get("idxvalue").toString());// 目标值
				String unit = map.get("unit").toString();// 单位
				String rsldesc = "";
				if (!StringUtils.isEmpty(map.get("rsldesc"))) {
					rsldesc = map.get("rsldesc").toString();// 描述
				}
				Long objid = null;
				String objname = "";
				if (!StringUtils.isEmpty(map.get("objid"))) {
					objid = Long.valueOf(map.get("objid").toString());// 执行对象id
					objname = map.get("objname").toString();// 执行对象名称
				}
				String starttime = DateEnum.YYYYMMDDHHMMDD.format();// 分解任务创建时间
				String fbtime = "";
				String endtime = "";
				if(map.get("fbtime")!=null && !"".equals(map.get("fbtime"))) {
					fbtime = map.get("fbtime").toString() + " 23:59:59";// 反馈截止时间 2019-3-5 23:59:59 这种格式
					fbtime = DateEnum.YYYYMMDDHHMMDD.format(DateEnum.YYYYMMDDHHMMDD.parse(fbtime)); //转换成 2019-03-05 23:59:59 这种格式
					endtime = DateEnum.YYYYMMDDHHMMDD.format(DateEnum.addDay(DateEnum.YYYYMMDDHHMMDD.parse(fbtime), -day));// 反馈结束时间
				}
				int stageseqno = 0;// 阶段序号(这里暂时赋值为0，应为排序可以用开始时间来排序)
				String stagename = map.get("stagename").toString();// 阶段名称
				String pid = map.get("parentid").toString();// 父序号
				tkResolveInfoDao.insert(session.getCompid(), id, taskid, nodename, idxvalue, unit, rsldesc, timecode, timename, objid,
						objname, fbtime, stageseqno, stagename, pid, starttime, endtime);
				List<Subject> oldsubs = tkTaskSubjectService.listSubjects(session.getCompid(),taskid/*, Constant.TkTaskSubject.FLAG_EXCUTE*/);
				Set<Subject> setSubject = new HashSet<Subject>(oldsubs);
				remindNoteComponent.register(session.getCompid(), Module.MODIFY_TASK_TIME, taskid, RemindRuleEnum.MODIFY_TASK_TIME, DateEnum.YYYYMMDDHHMMDD.format(),
						sessionSub, setSubject);
			}
		} else {
			LehandException.throwException("任务分解信息不能为空！");
		}
	}

	/**
	 * 修改任务基本信息：任务名称、备注、分类、起始时间等
	 *
	 * @param request
	 * @param session
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateTaskBase(TkTaskDeployRequest request, Session session) throws Exception {
		// 验证修改的class id是否存在
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		Long taskid = request.getTaskid();
		//对该任务相关人员发消息（包括执行人，跟踪人，分管领导，牵头领导）
		List<Subject> oldsubs = tkTaskSubjectService.listSubjects(session.getCompid(),taskid/*, Constant.TkTaskSubject.FLAG_EXCUTE*/);
		Set<Subject> setSubject = new HashSet<Subject>(oldsubs);
		//修改自定义表单信息(该版本不实现)
		Set<Subject> subject1 = request.getSubjects1();//关注人
		//第一步：删除该任务的关注人
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_FOCUS);
		if(subject1!=null && subject1.size()>0) {
			for (Subject sub1 : subject1) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_FOCUS, 0, sub1.getSubjectid(), sub1.getSubjectname(),Constant.EMPTY);
			}
		}
		Set<Subject> subject2 = request.getSubjects2();//协办人
		//删除任务协办人
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_COORDINATOR);
		if(subject2!=null && subject2.size()>0) {
			for (Subject sub2 : subject2) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_COORDINATOR, 0, sub2.getSubjectid(), sub2.getSubjectname(),Constant.EMPTY);
			}
		}
		List<Subject> subject3 = request.getSubjects3();//牵头领导
		//删除牵头领导
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_LEADLEADER);
		if(subject3!=null && subject3.size()>0) {
			for (Subject sub3 : subject3) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_LEADLEADER, 0, sub3.getSubjectid(), sub3.getSubjectname(),Constant.EMPTY);
			}
		}
		Set<Subject> subject4 = request.getSubjects4();//分管领导
		//删除分管领导
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_CHARGELEADER);
		if(subject4!=null && subject4.size()>0) {
			for (Subject sub4 : subject4) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_CHARGELEADER, 0, sub4.getSubjectid(), sub4.getSubjectname(),Constant.EMPTY);
			}
		}
		//修改主表基本信息以及差异化数据
		tkTaskDeployDao.updateTaskBase(session.getCompid(),request);
		//修改分解信息
		insertResolveInfo(request, session, tkTaskDeployDao.get(session.getCompid(),request.getTaskid()));
		remindNoteComponent.register(session.getCompid(),Module.MODIFY_TASK_BASIC, taskid, RemindRuleEnum.MODIFY_TASK_BASIC, DateEnum.YYYYMMDDHHMMDD.format(),
					sessionSub, setSubject);
	}


	public TimeNodeResponse list(Long compid, Long taskid, Pager pager, Long userid) {
		List<TimeResponse> times = tkResolveInfoDao.list(compid,taskid, pager);
		TimeNodeResponse timeNode = new TimeNodeResponse();
		timeNode.setTimes(times);
		return timeNode;
	}

	public int countByTaskIdAndSubjectId(Long compid,Long taskid, Long userid) {
		return tkResolveInfoDao.countBytaskIdAndSubjectId(compid,taskid);
	}


	/**
	 * 反馈详情 flag 0为时间优先，1为分解对象优先
	 */
	public List<Map<String, Object>> listBean(Long compid,Long taskid, String endtime,int flag) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		TkResolveInfo tk = null;
		if(flag==0) {
			tk = tkResolveInfoDao.getBean(compid,taskid, endtime);
		}else {
			tk = tkResolveInfoDao.getBeanFlag(compid,taskid, endtime);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		TkDict dicts = tkDictDao.get(compid,tk.getUnit());
		map.put("code", "阶段名");
		map.put("value", endtime);
		map.put("ind", "name");
		map.put("backvalue", tk.getIdxvalue());
		map.put("unit", dicts.getName());
		list.add(map);
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("value", tk.getFbtime().substring(0, 10));
		map1.put("code", "反馈截止日");
		map1.put("ind", "fbtime");
		map1.put("backvalue", "");
		list.add(map1);
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("value", "未反馈");
		map2.put("code", "实际反馈时间");
		map2.put("ind", "acutal");
		map2.put("backvalue", "");
		list.add(map2);
		if(flag==0) {
			Map<String, Object> map4 = new HashMap<String, Object>();
			map4.put("value", tk.getRsldesc());
			map4.put("code", "阶段目标描述");
			map4.put("ind", "desc");
			map4.put("backvalue", "");
			list.add(map4);
		}
		Map<String, Object> map5 = new HashMap<String, Object>();
		map5.put("value", tk.getRsldesc());
		map5.put("code", "实际阶段描述");
		map5.put("ind", "asc");
		map5.put("backvalue", "");
		list.add(map5);
		List<TkResolveInfo> info = tkResolveInfoDao.listBean(compid,taskid, endtime);
		for (TkResolveInfo tkResolveInfo : info) {
			TkDict dict = tkDictDao.get(compid,tkResolveInfo.getUnit());
			Map<String, Object> map3 = new HashMap<String, Object>();
			map3.put("value", tkResolveInfo.getIdxvalue());
			map3.put("code", tkResolveInfo.getObjname());
			map3.put("objid", tkResolveInfo.getObjid());
			map3.put("rslid", tkResolveInfo.getId());
			map3.put("ind", "obj");
			map3.put("unit", dict.getName());
			map3.put("backvalue", "");
			list.add(map3);
		}
		return list;
	}

	/**
	 * 分解反馈详情提交保存
	 * @param taskid
	 * @param endtime
	 * @param mesg
	 * @param uuid
	 * @param session
	 * @throws LehandException
	 */

    @SuppressWarnings("rawtypes")
    @Transactional(rollbackFor = Exception.class)
	public void save(Long taskid,String endtime,String mesg,String uuid,int flag,Session session) throws LehandException {
    	//第一步：保存我的任务日志表，其中mesg字段为空
    	TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),taskid);
    	TkMyTask myTask = tkMyTaskDao.getByTaskidAndSubjectid(session.getCompid(),taskid, session.getUserid());
    	List<Map> list = JSON.parseArray(mesg, Map.class);
    	MyTaskLogRequest request = new MyTaskLogRequest();
    	request.setMytaskid(myTask.getId());
    	request.setNow(DateEnum.YYYYMMDDHHMMDD.format());
    	request.setMessage("阶段分解");
    	TaskProcessData data = new TaskProcessData();
    	TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTaskLogRequest(request);
		data.setMyTask(myTask);
		data.setDeploy(deploy);
		data.setSession(session);
    	if(flag==0) {//新增
	    	//每次新增前都要查询下该任务是否有反馈记录便于计算进度
	    	int infocount = tkResolveInfoDao.getCount(session.getCompid(),taskid);//该任务阶段总数
	    	int listcount = tkResolveListDao.getCount(session.getCompid(),taskid);//该任务已反馈阶段数
	    	Double progress = 0.0;
	    	DecimalFormat df=new DecimalFormat("0.00");
	    	if(infocount>listcount) {//TODO
	    		progress =  ((listcount+1)/((double)infocount))*100;
	    	}else {
	    		progress = 100.0;
	    	}
	    	request.setProgress(Double.valueOf(df.format(progress)));
	    	request.setUuid(uuid);
	    	TkMyTaskLog info = tkMyTaskLogDao.insert(session.getCompid(),deploy, myTask, request, TaskProcessEnum.FEED_BACK, session,endtime,Constant.EMPTY,0);
	    	data.setMyTaskLog(info);
    		//我的任务日志保存完紧接着更新主任务中的任务进度
        	tkTaskDeployDao.updateProgressAndnewbacktime(session.getCompid(),Double.valueOf(df.format(progress)), DateEnum.YYYYMMDDHHMMDD.format(), taskid);
        	//更新我的任务表中该任务的进度值由于这里的执行人只有一个所以进度值和主任务的进度值一样
        	tkMyTaskDao.updateDocProgress(session.getCompid(),Double.valueOf(df.format(progress)),taskid);
        	//紧接着更新附件表
        	if(!StringUtils.isEmpty(uuid)) {
				tkAttachmentService.updateId(session.getCompid(),info.getId(), Module.MY_TASK_LOG.getValue(), uuid);
        	}
        	//第二步：将第一步中的mesg的消息拆开存入分解详情表中
        	if(list!=null && list.size()>0) {
        		String rsldesc = "";
        		for (Map map : list) {
        			if("asc".equals(map.get("ind")) && !StringUtils.isEmpty(map.get("backvalue"))) {
        				rsldesc = map.get("backvalue").toString();
        			}
    				if("obj".equals(map.get("ind"))) {
    					tkResolveListDao.insert(session.getCompid(),info.getId(), taskid, myTask.getId(), map.get("rslid").toString(), Double.valueOf(map.get("backvalue").toString()), rsldesc);
    				}
    			}
        	}
        	//积分与消息相关业务逻辑
        	TkIntegralRule tkIntegralRule = tkIntegralRuleService.get(session.getCompid(),String.valueOf(TaskProcessEnum.FEED_BACK));
			Double inteval = tkIntegralRule.getInteval();
			data.putRemindParam("inteval", inteval);
			if(inteval!=0) {
				//逾期反馈就扣分（只扣一次）如果结束日期之前有正常反馈的记录就不扣分（逾期未反馈扣分就通过批处理来执行）
				int count = tkIntegralDetailsService.count(myTask.getCompid(),myTask.getId(),DateEnum.YYYYMMDDHHMMDD.format(),String.valueOf(TaskProcessEnum.OVER_BACK));
				if(myTask.getDatenode().compareTo(DateEnum.YYYYMMDDHHMMDD.format())<0 && count<=0) {
					//暂定OVER_BACK为逾期
					tkIntegralDetailsService.insert(session.getCompid(),session.getUserid(),session.getUsername(),String.valueOf(TaskProcessEnum.OVER_BACK),deploy.getId(),myTask.getId(),-inteval,0L);
				}else {
					if(myTask.getDatenode().compareTo(DateEnum.YYYYMMDDHHMMDD.format())>0) {
						tkIntegralDetailsService.insert(session.getCompid(),session.getUserid(),session.getUsername(),String.valueOf(TaskProcessEnum.FEED_BACK),deploy.getId(),myTask.getId(),inteval,0L);
					}
					TaskProcessEnum.GET_INTEGER.handle(data.getSession(),inteval,tkIntegralRule.getMaxval(),inteval,myTask);
			   }
			}
			//查询任务的督办人
			List<TkTaskSubject> tkTaskSubjects = tkTaskSubjectService.listBytaskid(data.getDeploy().getCompid(),data.getDeploy().getId(),Constant.TkTaskSubject.FLAG_FOCUS);
    		if(tkTaskSubjects!=null && tkTaskSubjects.size()>0) {
    			for(TkTaskSubject subject : tkTaskSubjects) {
    				Subject sub = new Subject(subject.getSubjectid(),subject.getSubjectname());
    				remindNoteComponent.register(session.getCompid(),Module.MY_TASK_LOG, request.getMytaskid(), RemindRuleEnum.BACK_TASK,request.getNow(),data.getSessionSubject(),sub);
    			}
    		}
			remindNoteComponent.register(session.getCompid(),Module.MY_TASK_LOG, data.getMyTaskLog().getId(), RemindRuleEnum.BACK_TASK,
					request.getNow(),data.getSessionSubject(),data.getDeploySubject());
    	}else {//修改
    		request.setProgress(deploy.getProgress());
    		request.setUuid(uuid);
    		//更新我的日志表
    		tkMyTaskLogDao.updateUuid(session.getCompid(),uuid,taskid,endtime);
    		TkMyTaskLog log = tkMyTaskLogDao.getBean(session.getCompid(),taskid,endtime);
    		data.setMyTaskLog(log);
    		//更新分解详情表
    		if(list!=null && list.size()>0) {
        		String rsldesc = "";
        		for (Map map : list) {
        			if("asc".equals(map.get("ind")) && !StringUtils.isEmpty(map.get("backvalue"))) {
        				rsldesc = map.get("backvalue").toString();
        			}
    				if("obj".equals(map.get("ind"))) {
    					tkResolveListDao.update(session.getCompid(),taskid, map.get("rslid").toString(), Double.valueOf(map.get("backvalue").toString()), rsldesc);
    				}
    			}
        	}
    		//查询任务的督办人
    		List<TkTaskSubject> tkTaskSubjects = tkTaskSubjectService.listBytaskid(data.getDeploy().getCompid(),data.getDeploy().getId(),Constant.TkTaskSubject.FLAG_FOCUS);
    		if(tkTaskSubjects!=null && tkTaskSubjects.size()>0) {
    			for(TkTaskSubject subject : tkTaskSubjects) {
    				Subject sub = new Subject(subject.getSubjectid(),subject.getSubjectname());
    				remindNoteComponent.register(session.getCompid(),Module.MY_TASK_LOG, request.getMytaskid(), RemindRuleEnum.UPDATE_BACK_TASK,request.getNow(),data.getSessionSubject(),sub);
    			}
    		}
			remindNoteComponent.register(session.getCompid(),Module.MY_TASK_LOG, data.getMyTaskLog().getId(), RemindRuleEnum.UPDATE_BACK_TASK,
					request.getNow(),data.getSessionSubject(),data.getDeploySubject());
    	}
    }


	/**
	 *修改反馈详情前查询显示
	 */
	public List<Map<String, Object>> listQuery(Long compid, Long taskid, String endtime,int flag) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		TkResolveInfo tk = null;
		if(flag==0) {
			tk = tkResolveInfoDao.getBean(compid,taskid, endtime);
		}else {
			tk = tkResolveInfoDao.getBeanFlag(compid,taskid, endtime);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		TkDict dicts = tkDictDao.get(compid,tk.getUnit());
		map.put("code", "阶段名");
		map.put("value", endtime);
		map.put("ind", "name");
		map.put("backvalue", tk.getIdxvalue());
		map.put("unit", dicts.getName());
		list.add(map);
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("value", tk.getFbtime().substring(0, 10));
		map1.put("code", "反馈截止日");
		map1.put("ind", "fbtime");
		map1.put("backvalue", "");
		list.add(map1);
		Map<String, Object> map2 = new HashMap<String, Object>();
		TkMyTaskLog log = tkMyTaskLogDao.getBean(compid,taskid,endtime);
		String time = log.getOpttime().substring(0, 10);
		map2.put("value", time);
		map2.put("code", "实际反馈时间");
		map2.put("ind", "acutal");
		map2.put("backvalue", "");
		list.add(map2);
		Map<String, Object> map4 = new HashMap<String, Object>();
		map4.put("value", tk.getRsldesc());
		map4.put("code", "阶段目标描述");
		map4.put("ind", "desc");
		map4.put("backvalue", "");
		list.add(map4);
		Map<String, Object> map5 = new HashMap<String, Object>();
		TkResolveList relistone = tkResolveListDao.getBeanOne(compid,log.getId());
		map5.put("value", "");
		map5.put("code", "实际阶段描述");
		map5.put("ind", "asc");
		map5.put("backvalue", relistone.getRsldesc());
		list.add(map5);
		List<TkResolveInfo> info = tkResolveInfoDao.listBean(compid,taskid, endtime);
		for (TkResolveInfo tkResolveInfo : info) {
			TkDict dict = tkDictDao.get(compid,tkResolveInfo.getUnit());
			TkResolveList relist = tkResolveListDao.getBean(compid,log.getId(),tkResolveInfo.getId());
			Map<String, Object> map3 = new HashMap<String, Object>();
			map3.put("value", tkResolveInfo.getIdxvalue());
			map3.put("code", tkResolveInfo.getObjname());
			map3.put("objid", tkResolveInfo.getObjid());
			map3.put("rslid", tkResolveInfo.getId());
			map3.put("ind", "obj");
			map3.put("unit", dict.getName());
			map3.put("backvalue", relist.getRslvalue());
			list.add(map3);
		}
		return list;
	}


	/**
	 * 分解任务反馈详情页面
	 * @param taskid
	 * @param endtime
	 * @param session
	 * @return
	 */
	public List<TkTaskResolveInfoResponse> page(Long taskid,String endtime,int flag,Session session) {
		List<TkTaskResolveInfoResponse> reInfoRes = new ArrayList<TkTaskResolveInfoResponse>();
		TkMyTaskLog log = tkMyTaskLogDao.getBean(session.getCompid(),taskid,endtime);
		if(log!=null) {//已经存在反馈的内容，否则只查询签收的内容
			List<Map<String, Object>> list = listQuery(session.getCompid(),taskid, endtime,flag);
			String mesg = JSON.toJSONString(list);
			TkTaskResolveInfoResponse relove = new TkTaskResolveInfoResponse();
			TkCommentResponse tkCommentResponse;
			List<TkCommentResponse> comments = new ArrayList<TkCommentResponse>();
			//退回原因
			TkMyTask tkMyTask=tkMyTaskDao.get(session.getCompid(),log.getMytaskid());
			relove.setBackReason(tkMyTask.getRemarks3());
			relove.setId(log.getId());
			relove.setOpttime(log.getOpttime());
			relove.setUsername(log.getUsername());
			relove.setIsshow(false);
			relove.setIsopen(false);
			relove.setOpttype(log.getOpttype());
			relove.setMesg(mesg);
			//点赞相关
			int num = tkPraiseDetailsDao.getNum(session.getCompid(),log.getId());
			relove.setParisenum(num);
			int count = tkPraiseDetailsDao.getCount(log.getId(), session);
			if(count<=0) {//0表示登录人没有点赞该反馈
				relove.setFlag(0);
			}else {
				relove.setFlag(1);
			}
			if(StringUtils.isEmpty(log.getFileuuid())) {
				relove.setAttachment(new ArrayList<TkAttachmentResponse>(0));
			}else {
				List<TkAttachmentResponse> listAttachments = tkAttachmentService.list2(session.getCompid(),log.getFileuuid());
				relove.setAttachment(listAttachments);//这里面的remarks1 被用为语音评论的时长
			}
			relove.setCommentnum(log.getCommcount());
			if(log.getCommcount()>0) {
				//控制创建人和督办人只能看到自己的评论，而执行人能看到所有评论
				TkMyTask mytask = tkMyTaskDao.get(session.getCompid(),log.getMytaskid());
				List<TkComment> listCommens = null;
				if(mytask.getSubjectid().equals(session.getUserid())) {//执行人看到的评论
					 listCommens = tkCommentDao.list(session.getCompid(),log.getId());
				}else {
					 listCommens = tkCommentDao.listBySession(session.getCompid(),log.getId(),session.getUserid());
				}
				if(listCommens==null || listCommens.size()<=0) {
					relove.setComment(new ArrayList<TkCommentResponse>(0));
				}else {
					for (TkComment tkComment : listCommens) {
						tkCommentResponse = new TkCommentResponse();
						tkCommentResponse.setId(tkComment.getId());
						tkCommentResponse.setCommflag(tkComment.getCommflag());
						tkCommentResponse.setCommtext(tkComment.getCommtext());
						tkCommentResponse.setDuration(tkComment.getRemarks1());
						tkCommentResponse.setCommtime(tkComment.getCommtime());
						tkCommentResponse.setFsubjectname(tkComment.getFsubjectname());
						tkCommentResponse.setModel(tkComment.getModel());
						tkCommentResponse.setModelid(tkComment.getModelid());
						List<TkCommentReply>  listReplys = tkCommentReplyDao.list(tkComment.getId());
						if(listReplys==null || listReplys.size()<=0) {
							tkCommentResponse.setReply(new ArrayList<TkCommentReply>(0));
						}else {
							tkCommentResponse.setReplynum(listReplys.size());
							tkCommentResponse.setReply(listReplys);
						}
						comments.add(tkCommentResponse);
					}
					relove.setComment(comments);
					relove.setCommentnum(comments.size());
				}
			}
			reInfoRes.add(relove);
		}
		TkTaskResolveInfoResponse relove2 = new TkTaskResolveInfoResponse();
		TkMyTaskLog log2 = tkMyTaskLogDao.getBean(session.getCompid(),taskid,Constant.EMPTY);
		//这里如果log2为空则说明该分解任务还没有被签收所以其详情页面也是没有数据的
		if(log2!=null) {
			TkMyTask tkMyTask=tkMyTaskDao.get(session.getCompid(),log2.getMytaskid());
			relove2.setBackReason(tkMyTask.getRemarks3());
			relove2.setId(log2.getId());
			relove2.setMesg(log2.getMesg());
			relove2.setOpttime(log2.getOpttime());
			relove2.setUsername(log2.getUsername());
			relove2.setOpttype(log2.getOpttype());
			relove2.setIsshow(false);
			relove2.setIsopen(false);
			reInfoRes.add(relove2);
		}
		return reInfoRes;
	}

	/**
	 * 修改分解任务的时间以及分解信息（修改基本信息以及执行人均调用之前的接口）
	 * @param request
	 * @param session
	 * @throws ParseException
	 * @throws LehandException
	 */
	public void modityDecom(TkTaskDeployRequest request, Session session) throws ParseException, LehandException {
		//修改分解任务的时间
		//第一步：更新主表中差异化数据的json数据，并且更新任务的起止时间
		tkTaskDeployDao.updateTimeAndDiffdata(session.getCompid(),request.getTaskid(),request.getStarttime(),request.getEndtime(),request.getTimedataresolve());
		TkTaskDeploy info = tkTaskDeployDao.get(session.getCompid(),request.getTaskid());
		//更新子表的起始时间
		tkMyTaskDao.updateStartAndEnd(session.getCompid(),request.getTaskid(),request.getStarttime(),request.getEndtime());
		//第二步：更新时间节点中的任务时间信息
		// 删除时间节点表数据
		tkTaskTimeNodeService.deleteByTaskid(session.getCompid(),request.getTaskid());
		// 重新插入时间节点表数据
		tkTaskDeployBusinessImpl.insertTkTaskTimeNode(session.getCompid(),request.getTaskid(), request.getStarttime(), request.getEndtime(),2);
		//第三步：删除分解表中该任务的所有数据，将新的分解数据插入分解表中
		tkResolveInfoDao.deleteByTaskid(session.getCompid(),request.getTaskid());
		insertResolveInfo(request, session, info);
	}

	/**
	 * 获取分解对象
	 * @param taskid
	 * @return
	 */
	public List<Subject> listObj(Long compid,Long taskid){
		return tkResolveInfoDao.listObj(compid,taskid);
	}

	/**
	  * 督办跟踪分解任务反馈详情页面对比tab页柱状图接口
	 * @param taskid
	 * @param time
	 * @return
	 */
	public List<Map<String , Object>> listDuibi(Long compid,Long taskid,String time){
//		List<Double> list1 = tkResolveInfoDao.listMubiaoDuibi(taskid, time);
//		List<Double> list2 = tkResolveInfoDao.listShijiDuibi(taskid, time);
//		List<Map<String , Object>> list = new ArrayList<Map<String,Object>>();
//		Map<String , Object> map1 = new HashMap<String, Object>();
//		map1.put("name", "目标值");
//		map1.put("type", "bar");
//		map1.put("data", list1);
//		list.add(map1);
//		Map<String , Object> map2 = new HashMap<String, Object>();
//		map2.put("name", "实际值");
//		map2.put("type", "bar");
//		map2.put("data", list2);
//		list.add(map2);
		return tkResolveInfoDao.listDuibi(compid,taskid, time);
	}

	/**
	 * 督办跟踪分解任务反馈详情页面分布tab饼状图接口
	 * @param taskid
	 * @param time
	 * @return
	 */
	public List<Map<String,Object>> listFenbu(Long compid,Long taskid,String time){
		return tkResolveInfoDao.listFenbu(compid,taskid, time);
	}


	/**
	 * 督办跟踪分解任务反馈详情页面走势tab折线图接口
	 * @param taskid
	 * @param subjectid
	 * @return
	 */
	public List<Map<String , Object>> listZoushi(Long compid,Long taskid,Long subjectid){
//		List<Double> list1 = tkResolveInfoDao.listMubiaoZoushi(taskid, subjectid);
//		List<Double> list2 = tkResolveInfoDao.listShijiZoushi(taskid, subjectid);
//		List<Map<String , Object>> list = new ArrayList<Map<String,Object>>();
//		Map<String , Object> map1 = new HashMap<String, Object>();
//		map1.put("name", "目标值");
//		map1.put("type", "line");
//		map1.put("data", list1);
//		list.add(map1);
//		Map<String , Object> map2 = new HashMap<String, Object>();
//		map2.put("name", "实际值");
//		map2.put("type", "line");
//		map2.put("data", list2);
//		list.add(map2);
		return tkResolveInfoDao.listZoushi(compid,taskid, subjectid);
	}

	/**
	 * 获取任务的单位
	 * @param taskid
	 * @return
	 */
	public String getUnit(Long compid,Long taskid) {
		return tkResolveInfoDao.getUnit(compid,taskid);
	}

	/**
	 * 获取根节点的目标值和目标描述
	 * @param taskid
	 * @param id
	 * @return
	 */
	public Map<String,Object> getRootInfo(Long compid,Long taskid,String id){
		Map<String,Object> map = tkResolveInfoDao.getRoot(compid,taskid,id);
		return map;
	}




	public static void main(String[] args) {
		int infocount = 3;
    	int listcount = 1;
    	Double progress = 0.0;
    	DecimalFormat df=new DecimalFormat("0.00");
    	if(infocount>listcount) {
    		progress =  ((listcount+1)/((double)infocount))*100;
    	}else {
    		progress = 100.0;
    	}
    	System.out.println(Double.valueOf(df.format(progress)));
	}


}
