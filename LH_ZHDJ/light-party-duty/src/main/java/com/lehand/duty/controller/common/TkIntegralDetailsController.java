package com.lehand.duty.controller.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkIntegralDetailsService;
import com.lehand.duty.dto.TkIntegralRespond;
import com.lehand.duty.pojo.TkIntegralDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.duty.controller.DutyBaseController;

/**
 * 积分列表类
 * @author pantao
 * @date 2018年11月28日上午11:27:29
 *
 */
@Api(value = "积分列表类", tags = { "积分列表类" })
@RestController
@RequestMapping("/duty/integral")
public class TkIntegralDetailsController extends DutyBaseController{

	private static final Logger logger = LogManager.getLogger(TkIntegralDetailsController.class);
	@Resource private TkIntegralDetailsService tkIntegralDetailsService;
	
	/**
	 * 积分列表查询
	 * @author pantao
	 * @date 2018年11月28日上午11:11:45
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "积分列表查询", notes = "积分列表查询", httpMethod = "POST")
	@RequestMapping("/get")
	public Message integral(){
		Message message = new Message();
		try {
			TkIntegralRespond info = tkIntegralDetailsService.getMyScore(getSession().getCompid(),getSession().getUserid());
			message.success().setData(info);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 总积分排名
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "总积分排名", notes = "总积分排名", httpMethod = "POST")
	@RequestMapping("/scorerank")
	public Message scorerank(Pager pager){
		Message message = new Message();
		try {
			int count = tkIntegralDetailsService.getListScore(getSession().getCompid());
			List<TkIntegralDetails> info = tkIntegralDetailsService.getListScore(getSession().getCompid(),pager);
			pager.setRows(info);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("endtime", DateEnum.YYYYMMDDHHMMDD.format());
			map.put("rank", pager);
			message.success().setData(map);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
}
