package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import com.lehand.duty.pojo.TkTaskSubject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ApplySuspendTaskHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkTaskDeploy deploy = data.getParam(0);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
		data.setDeploySub(deploySub); 
		data.setDeploy(deploy);
		
		// process
		int flag = data.getParam(1);
		switch (flag) {
			case 1://暂停所有任务
				//先暂停主任务(排除退回和完成的)
	//			tkTaskDeployDao.updateStatus(deploy.getId(), Constant.TkTaskDeploy.SUSPENDED_STATUS);
				//更新主表中remarks5字段为暂停的时间
				tkTaskDeployDao.updateStatusRemarks5(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SUSPENDED_STATUS,DateEnum.YYYYMMDDHHMMDD.format());
				//再暂停所有子任务
				tkMyTaskDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkMyTask.STATUS_SUSPEND);
				//将提醒登记簿中的remarks3设置为suspend(切记批处理时候扫描都要带上remarks3字段)
				tkTaskRemindNoteService.updateRemarks3(deploy.getCompid(),deploy.getId(),"suspend");
				break;
			default:
				break;
		}
		//这里要给一个标识好让数据库去判断到底是暂停任务还是暂停后续任务（将这里的flag存入到数据库读取的数据中）
		data.putRemindParam("flagstatus", flag);
		
		// remind
		String userid = data.getParam(2);
		List<Subject> subjects = new ArrayList<Subject>();
		Set<Subject> sub = null;
		switch (flag) {
			case 1:
				subjects = tkTaskSubjectService.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//给该任务相关人发消息
			    sub = new HashSet<Subject>(subjects);//去重
				break;
			case 2:
				TkTaskSubject user = tkTaskSubjectService.getBySubject(data.getDeploy().getCompid(),Long.valueOf(userid));
				Subject s = new Subject(Long.valueOf(userid),user.getSubjectname());
				subjects.add(s);
				sub = new HashSet<Subject>(subjects);
				break;
			case 3:
				subjects = tkTaskSubjectService.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//发消息内容为后续任务被暂停
			    sub = new HashSet<Subject>(subjects);//去重
				break;
			default:
				break;
		}
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), deploy.getId(), processEnum.getRemindRule(), DateEnum.YYYYMMDDHHMMDD.format(),
				data.getSessionSubject(), sub);
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getParam(0);
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
//		data.setDeploySub(deploySub); 
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		int flag = data.getParam(1);
//		TkTaskDeploy deploy = data.getDeploy();
//		switch (flag) {
//		case 1://暂停所有任务
//			//先暂停主任务(排除退回和完成的)
////			tkTaskDeployDao.updateStatus(deploy.getId(), Constant.TkTaskDeploy.SUSPENDED_STATUS);
//			//更新主表中remarks5字段为暂停的时间
//			tkTaskDeployDao.updateStatusRemarks5(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SUSPENDED_STATUS,DateEnum.YYYYMMDDHHMMDD.format());
//			//再暂停所有子任务
//			tkMyTaskDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkMyTask.STATUS_SUSPEND);
//			//将提醒登记簿中的remarks3设置为suspend(切记批处理时候扫描都要带上remarks3字段)
//			tkTaskRemindNoteDao.updateRemarks3(deploy.getCompid(),deploy.getId(),"suspend");
//			break;
//			/*case 2://暂停该执行人的的任务
//			String userid = data.getParam(2);
//			Long subjectid = Long.valueOf(userid);
//			//先暂停该执行人的所有任务(排除退回和完成的)
//			tkMyTaskDao.updateStatusBySubject(deploy.getId(),subjectid,Constant.TkMyTask.STATUS_SUSPEND);
//			//判断下该任务下是不是所有人的任务都暂停了如果是就更新主任务的状态为暂停
//			int count = tkMyTaskDao.getCountByStstus(deploy.getId(),Constant.TkMyTask.STATUS_SUSPEND);
//			if(count<=0) {
//				//将主任务更新为暂停状态
//				tkTaskDeployDao.updateStatus(deploy.getId(), Constant.TkTaskDeploy.SUSPENDED_STATUS);
//				if(deploy.getPeriod() == 0) {//普通任务
//					//将提醒登记簿中的remarks3设置为suspend(切记批处理时候扫描都要带上remarks3字段)
//					tkTaskRemindNoteDao.updateRemarks3(deploy.getId(),"suspend");
//				}
//			}
//			//暂停执行人的任务过后需要更新主题表中该执行人对应的remarks3 为 suspend(为了反馈详情列表中显示执行人列表中每个执行人的状态)
//			tkTaskSubjectDao.updateRemarks3(deploy.getId(),"suspend",subjectid);
//			//消息问题后面讨论解决
//			break;
//		case 3://暂停后续任务（针对周期任务）
//			//获取当前时间
//			String now = DateEnum.YYYYMMDDHHMMDD.format();
//			//暂停开始时间大于当前时间的任务
//			List<TkMyTask> mylist = tkMyTaskDao.listByTaskidAll(deploy.getId());
//			int num = 0;
//			for(TkMyTask tks : mylist) {
//				if(tks.getCreatetime().compareTo(now)>0) {//开始时间大于当前时间就暂停(排除退回和完成的)
//					tkMyTaskDao.updateStatusById(tks.getId(),Constant.TkMyTask.STATUS_SUSPEND);
//				}else {//如果在当前时间之前已有任务开始了那么就不用更新主表状态否则更新主表状态为暂停状态
//					num++;
//				}
//			}
//			if(num<=0) {
//				//将主任务更新为暂停状态
//				tkTaskDeployDao.updateStatus(deploy.getId(), Constant.TkTaskDeploy.SUSPENDED_STATUS);
//				//将提醒登记簿中的remarks3设置为suspend(切记批处理时候扫描都要带上remarks3字段)
//				tkTaskRemindNoteDao.updateRemarks3(deploy.getId(),"suspend");
//			}
//			//消息问题后面讨论解决
//			break;*/
//		default:
//			break;
//		}
//		//这里要给一个标识好让数据库去判断到底是暂停任务还是暂停后续任务（将这里的flag存入到数据库读取的数据中）
//		data.putRemindParam("flagstatus", flag);
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		int flag = data.getParam(1);
//		String userid = data.getParam(2);
//		TkTaskDeploy deploy = data.getDeploy();
//		List<Subject> subjects = new ArrayList<Subject>();
//		Set<Subject> sub = null;
//		switch (flag) {
//		case 1:
//			subjects = tkTaskSubjectDao.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//给该任务相关人发消息
//		    sub = new HashSet<Subject>(subjects);//去重
//			break;
//		case 2:
//			TkTaskSubject user = tkTaskSubjectDao.getBySubject(data.getDeploy().getCompid(),Long.valueOf(userid));
//			Subject s = new Subject(Long.valueOf(userid),user.getSubjectname());
//			subjects.add(s);
//			sub = new HashSet<Subject>(subjects);
//			break;
//		case 3:
//			subjects = tkTaskSubjectDao.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//发消息内容为后续任务被暂停
//		    sub = new HashSet<Subject>(subjects);//去重
//			break;
//		default:
//			break;
//		}
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, deploy.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
//				data.getSessionSubject(), sub);
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
