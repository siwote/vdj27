package com.lehand.duty.dto;

/**
 * 普通任务和周期任务详情
 * 
 * @author pantao
 * @date 2018年11月15日上午9:17:27
 *
 */
public class TkTaskResolveInfoRespondDto{
	
	    String fbtime;     //总任务截止时间
		String id;		//根节点
		Double idxvalue;		//目标值
		boolean isroot;		//是否是根节点
		String objid;	//分解对象id
		String objname;		//分解对象名称
		String parentid;		//父节点id
		String rsldesc;	//阶段目标描述
		String stagename;		//阶段名  根节点为任务名 不可为空
		String topic;  //显示的div  阶段名称 目标值 目标单位拼接 
		String unit;    //单位
		public String getFbtime() {
			return fbtime;
		}
		public void setFbtime(String fbtime) {
			this.fbtime = fbtime;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public Double getIdxvalue() {
			return idxvalue;
		}
		public void setIdxvalue(Double idxvalue) {
			this.idxvalue = idxvalue;
		}
		public boolean isIsroot() {
			return isroot;
		}
		public void setIsroot(boolean isroot) {
			this.isroot = isroot;
		}
		public String getObjid() {
			return objid;
		}
		public void setObjid(String objid) {
			this.objid = objid;
		}
		public String getObjname() {
			return objname;
		}
		public void setObjname(String objname) {
			this.objname = objname;
		}
		public String getParentid() {
			return parentid;
		}
		public void setParentid(String parentid) {
			this.parentid = parentid;
		}
		public String getRsldesc() {
			return rsldesc;
		}
		public void setRsldesc(String rsldesc) {
			this.rsldesc = rsldesc;
		}
		public String getStagename() {
			return stagename;
		}
		public void setStagename(String stagename) {
			this.stagename = stagename;
		}
		public String getTopic() {
			return topic;
		}
		public void setTopic(String topic) {
			this.topic = topic;
		}
		public String getUnit() {
			return unit;
		}
		public void setUnit(String unit) {
			this.unit = unit;
		}
		
		
}
