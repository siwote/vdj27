package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Component;

@Component
public class CuiBanHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkMyTask tkMyTask = data.getParam(0);
		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTask(tkMyTask);
		data.setDeploy(deploy);
		
		TkMyTask mytask = data.getMyTask();
		//提醒执行人
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), mytask.getId(), 
				                     processEnum.getRemindRule(), DateEnum.YYYYMMDDHHMMDD.format(),
				                     data.getSessionSubject(), data.getMyTaskSubject());
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkMyTask tkMyTask = data.getParam(0);
//		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTask(tkMyTask);
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		TkMyTask mytask = data.getMyTask();
//		//提醒执行人
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, mytask.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
//				data.getSessionSubject(), data.getMyTaskSubject());
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
