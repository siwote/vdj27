package com.lehand.duty.dao;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.pojo.TkResolveList;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class TkResolveListDao  {
	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public void insert(TkResolveList info) {
		generalSqlComponent.getDbComponent().insert("INSERT INTO tk_resolve_list (compid, mytasklogid, taskid, mytaskid, rslid, rslvalue, rsldesc, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid, :mytasklogid, :taskid, :mytaskid, :rslid, :rslvalue, :rsldesc, :remarks1, :remarks2, :remarks3, :remarks4, :remarks5, :remarks6)", info);
	}
	
	public void insert(Long compid,Long mytasklogid,Long taskid,Long mytaskid,String rslid,Double rslvalue,String rsldesc) {
		TkResolveList info = new TkResolveList();
		info.setCompid(compid);
		info.setMytasklogid(mytasklogid);
		info.setTaskid(taskid);
		info.setMytaskid(mytaskid);
		info.setRslid(rslid);
		info.setRslvalue(rslvalue);
		info.setRsldesc(rsldesc);
		info.setRemarks1(0);
		info.setRemarks2(0);
		info.setRemarks3(Constant.EMPTY);
		info.setRemarks4(Constant.EMPTY);
		info.setRemarks5(Constant.EMPTY);
		info.setRemarks6(Constant.EMPTY);
		insert(info);
	}
	
	public int getCount(Long compid,Long taskid) {
		return generalSqlComponent.getDbComponent().getBean("SELECT count(*) from ( select taskid,mytasklogid FROM tk_resolve_list where compid=? and taskid=?  group by taskid,mytasklogid)a ", Integer.class, new Object[] {compid,taskid});
	}
	
	public void update(Long compid,Long taskid,String rslid,Double rslvalue,String rsldesc) {
		generalSqlComponent.getDbComponent().update("UPDATE tk_resolve_list SET rslvalue = ?, rsldesc = ? WHERE compid=? and taskid = ? and rslid = ?", new Object[] {rslvalue,rsldesc,compid,taskid,rslid});
	}

	public TkResolveList getBean(Long compid,Long taskid, String relid) {
		return generalSqlComponent.getDbComponent().getBean("SELECT * FROM tk_resolve_list where compid=? and mytasklogid = ? and rslid = ? ", TkResolveList.class, new Object[] {compid,taskid,relid});
	}

	public TkResolveList getBeanOne(Long compid,Long logid) {
		return generalSqlComponent.getDbComponent().getBean("SELECT * FROM tk_resolve_list where compid=? and mytasklogid = ? LIMIT 0,1", TkResolveList.class, new Object[] {compid,logid});
	}
}
