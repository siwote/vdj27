package com.lehand.duty.dao;

import com.lehand.base.common.Pager;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.pojo.TkSysDynamics;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TkSysDynamicsDao  {
	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	private static final String listbysubjectid = "select * from tk_sys_dynamics where compid=? and subjectid = ? and flag = 1 and sjtype = 0 order by opttime desc";
	private static final String insert = " INSERT INTO tk_sys_dynamics (compid, taskid, flag, sjtype, subjectid, subjectname, optenum, optuserid, optusernm, opttime, context, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid, :taskid, :flag, :sjtype, :subjectid, :subjectname,:optenum, :optuserid, :optusernm, :opttime, :context, :remarks1, :remarks2, :remarks3, :remarks4, :remarks5, :remarks6)";

	public void insert(TkSysDynamics info) {
        generalSqlComponent.getDbComponent().insert(insert, info);
	}
	
	/**
	 * 插入动态
	 * @param taskid 主任务id
	 * @param userid 关注人id
	 * @param username 关注人名称
	 * @param msg 消息内容
	 * @param opttime 操作时间
	 * @param flag 类别(0:执行主体,1:关注主体,2:协办人,3:牵头人,4:分管领导)
	 * @param optenum 操作枚举
	 * @param optuserid 操作人id 
	 * @param optusernm 操作人名称 （session中取值）
	 */
	public void insertInfo(Long compid,Long taskid,Long userid,String username,String msg,String opttime,int flag,String optenum,Long optuserid,String optusernm) {
		TkSysDynamics info = new TkSysDynamics();
		info.setCompid(compid);
		info.setTaskid(taskid);
		info.setSubjectid(userid);
		info.setSubjectname(username);
		info.setContext(msg);
		info.setOpttime(opttime);
		info.setFlag(flag);
		info.setSjtype(0);
		info.setOptenum(optenum);
		info.setOptuserid(optuserid);
		info.setOptusernm(optusernm);
		info.setRemarks1(0);
		info.setRemarks2(0);
		info.setRemarks3(StringConstant.EMPTY);
		info.setRemarks4(StringConstant.EMPTY);
		info.setRemarks5(StringConstant.EMPTY);
		info.setRemarks6(StringConstant.EMPTY);
		insert(info);
	}
	
	/**
	 * 查询关注人的动态消息分页查询
	 * @param session
	 * @return
	 */
	public void page(Session session,Pager pager) {
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = new StringBuilder(listbysubjectid);	
		ps.add(session.getCompid());
		ps.add(session.getUserid());
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}

	public void delete(Long compid,Long taskid) {
        generalSqlComponent.getDbComponent().delete("delete from tk_sys_dynamics where compid=? and taskid = ?", new Object[] {compid,taskid});
	}

}
