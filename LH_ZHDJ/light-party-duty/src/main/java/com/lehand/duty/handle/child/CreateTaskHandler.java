package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import org.springframework.stereotype.Component;

@Component
public class CreateTaskHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		data.setDeploy((TkTaskDeploy) data.getParam(0));
		data.setMyTask((TkMyTask) data.getParam(2));
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(),data.getMyTask().getId(), 
				                     processEnum.getRemindRule(), 
					                 DateEnum.YYYYMMDDHHMMDD.format(), 
					                 data.getDeploySubject(),
						             data.getMyTaskSubject());
	}

//	protected void init(TaskProcessData data) throws LehandException {
//		data.setDeploy((TkTaskDeploy) data.getParam(0));
//		data.setMyTask((TkMyTask) data.getParam(2));
//	}
//	
//	protected void process(TaskProcessData data) throws LehandException {
//	}
//
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	protected void remind(TaskProcessData data) throws LehandException {
//		remindNoteComponent.register(data.getDeploy().getCompid(),module,data.getMyTask().getId(), remindRule, 
//				                    DateEnum.YYYYMMDDHHMMDD.format(), 
//				                    data.getDeploySubject(),
//						            data.getMyTaskSubject());
//		
//	}
//
//	protected void setTaskDeploySub(TaskProcessData data) {
//		
//	}
}
