package com.lehand.duty.handle;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.ExpressionUtil;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.component.RemindNoteComponent;
import com.lehand.duty.dao.*;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.pojo.TkRemindRule;
import com.lehand.duty.pojo.TkTaskDeploySub;
import com.lehand.duty.pojo.TkTaskSubject;
import com.lehand.duty.service.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseHandler {

	@Resource protected RemindNoteComponent remindNoteComponent;
	@Resource protected TkTaskDeployDao tkTaskDeployDao;
	@Resource protected TkMyTaskDao tkMyTaskDao;
	@Resource protected TkMyTaskLogService tkMyTaskLogService;
	@Resource protected TkTaskDeploySubDao tkTaskDeploySubDao;
	@Resource protected TkIntegralDetailsService tkIntegralDetailsService;
	@Resource protected TkIntegralRuleService tkIntegralRuleService;
	@Resource protected TkCommentService tkCommentService;
	@Resource protected TkCommentReplyService tkCommentReplyService;
	@Resource protected TkTaskSubjectService tkTaskSubjectService;
	@Resource protected TkSysDynamicsDao tkSysDynamicsDao;
	@Resource protected TkTaskRemindNoteService tkTaskRemindNoteService;
	@Resource protected TkTaskTimeNodeService tkTaskTimeNodeService;
	@Resource protected TkTaskRemindListDao tkTaskRemindListDao;
	@Resource protected TkPraiseDetailsService tkPraiseDetailsService;
	@Resource protected DutyCacheComponent dutyCacheComponent;
	@Resource protected GeneralSqlComponent generalSqlComponent;
	@Resource protected TkAttachmentService tkAttachmentService;
    @Resource protected BrowerHisService browerHisService;
	
	public TaskProcessData process(TaskProcessEnum processEnum, Session session, Object... params) {
		TaskProcessData data = new TaskProcessData(session,params);
		try {
			process(processEnum, data);
			updateDeploySub(processEnum, session, data);
		}catch (LehandException e) {
			LehandException.throwException(e.getMessage(), e);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * 查询是否存在督办人如果存在就给其发信息
	 * @param data
	 * @param module
	 * @param remindRule
	 * @param mystackid
	 * @param now
	 * @throws LehandException
	 */
	public void supervise(TaskProcessData data, Module module, RemindRuleEnum remindRule, Long mystackid, String now) throws LehandException {
		//查询任务的督办人
		List<TkTaskSubject> tkTaskSubjects = tkTaskSubjectService.listBytaskid(data.getDeploy().getCompid(),data.getDeploy().getId(),Constant.TkTaskSubject.FLAG_FOCUS);
		if(tkTaskSubjects!=null && tkTaskSubjects.size()>0) {
			for(TkTaskSubject subject : tkTaskSubjects) {
				Subject sub = new Subject(subject.getSubjectid(),subject.getSubjectname());
				if(data.getSession()==null) {
					remindNoteComponent.register(data.getDeploy().getCompid(),module, mystackid, remindRule,now, TaskProcessData.getSystem(), sub);
				}else {
					remindNoteComponent.register(data.getDeploy().getCompid(),module, mystackid, remindRule,now,data.getSessionSubject(), sub);
				}
			}
		}
	}

	public void updateDeploySub(TaskProcessEnum processEnum, Session session, TaskProcessData data) throws LehandException {
		TkTaskDeploySub deploySub = data.getDeploySub();
		if(null==deploySub) {
			return;
		}
		Map<String,Object> env = new HashMap<String,Object>(1);
		env.put("data", data);
		TkRemindRule rule = processEnum.getRemindRule().getTkRemindRule(data.getDeploy().getCompid());
		 Object[] mesg = ExpressionUtil.objectValue("array("+rule.getNewsformat()+")", env);
		//同一操作需要发不同消息
		if(mesg.length>0) {//多条不同消息
			for(Object obj : mesg) {
				if(session==null) {//批处理走这里
					deploySub.setNewuserid(TaskProcessData.getSystem().getSubjectid());
					deploySub.setNewusernm(TaskProcessData.getSystem().getSubjectname());
					deploySub.setNewtime(DateEnum.YYYYMMDDHHMMDD.format());
				}else {//操作按钮走这里
					deploySub.setNewuserid(session.getUserid());
					deploySub.setNewusernm(session.getUsername());
					deploySub.setNewtime(DateEnum.YYYYMMDDHHMMDD.format());
				}
				deploySub.setNewmesg(obj.toString());
				tkTaskDeploySubDao.update(deploySub);
				//附属表中的最新消息就是最新动态，故此更新一次附属表就往动态表中插入一条数据
				Long taskid = data.getDeploy().getId();
				//查询任务的督办人
				List<TkTaskSubject> tkTaskSubjects = tkTaskSubjectService.listBytaskid(data.getDeploy().getCompid(),taskid, Constant.TkTaskSubject.FLAG_FOCUS);
				if(tkTaskSubjects!=null && tkTaskSubjects.size()>0) {
					for(TkTaskSubject subject : tkTaskSubjects) {
						if(session==null) {
							tkSysDynamicsDao.insertInfo(data.getDeploy().getCompid(),taskid, subject.getSubjectid(), subject.getSubjectname(), obj.toString(), DateEnum.YYYYMMDDHHMMDD.format(), 1, processEnum.name(), TaskProcessData.getSystem().getSubjectid(), TaskProcessData.getSystem().getSubjectname());
						}else {
							tkSysDynamicsDao.insertInfo(data.getDeploy().getCompid(),taskid, subject.getSubjectid(), subject.getSubjectname(), obj.toString(), DateEnum.YYYYMMDDHHMMDD.format(), 1, processEnum.name(), session.getUserid(), session.getUsername());
						}
					}
				}
			}
		}
	}

	public abstract void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception;
}
