package com.lehand.duty.common;

/**
 * 模块
 * @author Zim
 *
 */
public enum Module {

	/**
	 * 任务发布   tk_task_deploy
	 */
	DEPLOY_TASK(0),
	
	TASK_AUDIT_NOPASS(14),
	
	/**
	 * 我的任务   对应tk_my_task
	 */
	MY_TASK(1),
	/**
	 * 我的任务明细   对应tk_my_task_log
	 */
	MY_TASK_LOG(2),
	/**
	 * 评论   对应tk_comment
	 */
	COMMENT(3),
	/**
	 * 回复  对应tk_comment_reply
	 */
	REPLY(4),
	/**
	 * 删除（所有表）
	 */
	DELETE_TASK(5),
	
	/**
	 * 主任务表 tk_task_deploy
	 */
	SUSPEND_TASK(6),
	
	/**
	 * 主任务表 tk_task_deploy
	 */
	COMPLETE_TASK(7),
	
	/**
	 * 主任务表 tk_task_deploy
	 */
	MODIFY_TASK_BASIC(8),
	
	/**
	 * 主任务表 tk_task_deploy
	 */
	MODIFY_TASK_SUBJECT(9),
	/**
	 * 主任务表 tk_task_deploy
	 */
	MODIFY_TASK_TIME(10),
	/**
	 * 点赞  对应tk_my_task_log
	 */
	FEED_PARISE(11),
	/**
	 * 获得积分
	 */
	GET_INTEGER(12),
	/**
	 * 恢复任务
	 */
	RECOVERY_TASK(13);
	
	private int value;

	private Module(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
}
