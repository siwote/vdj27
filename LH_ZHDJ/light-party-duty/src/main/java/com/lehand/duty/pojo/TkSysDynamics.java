package com.lehand.duty.pojo;

public class TkSysDynamics extends FatherPojo {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.taskid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Long taskid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.flag
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Integer flag;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.sjtype
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Integer sjtype;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.subjectid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Long subjectid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.subjectname
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String subjectname;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.optenum
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String optenum;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.optuserid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Long optuserid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.optusernm
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String optusernm;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.opttime
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String opttime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.context
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String context;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.remarks1
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Integer remarks1;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.remarks2
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private Integer remarks2;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.remarks3
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String remarks3;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.remarks4
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String remarks4;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.remarks5
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String remarks5;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_sys_dynamics.remarks6
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	private String remarks6;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.taskid
	 * @return  the value of tk_sys_dynamics.taskid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Long getTaskid() {
		return taskid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.taskid
	 * @param taskid  the value for tk_sys_dynamics.taskid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.flag
	 * @return  the value of tk_sys_dynamics.flag
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Integer getFlag() {
		return flag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.flag
	 * @param flag  the value for tk_sys_dynamics.flag
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.sjtype
	 * @return  the value of tk_sys_dynamics.sjtype
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Integer getSjtype() {
		return sjtype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.sjtype
	 * @param sjtype  the value for tk_sys_dynamics.sjtype
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setSjtype(Integer sjtype) {
		this.sjtype = sjtype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.subjectid
	 * @return  the value of tk_sys_dynamics.subjectid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Long getSubjectid() {
		return subjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.subjectid
	 * @param subjectid  the value for tk_sys_dynamics.subjectid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setSubjectid(Long subjectid) {
		this.subjectid = subjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.subjectname
	 * @return  the value of tk_sys_dynamics.subjectname
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getSubjectname() {
		return subjectname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.subjectname
	 * @param subjectname  the value for tk_sys_dynamics.subjectname
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname == null ? null : subjectname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.optenum
	 * @return  the value of tk_sys_dynamics.optenum
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getOptenum() {
		return optenum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.optenum
	 * @param optenum  the value for tk_sys_dynamics.optenum
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setOptenum(String optenum) {
		this.optenum = optenum == null ? null : optenum.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.optuserid
	 * @return  the value of tk_sys_dynamics.optuserid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Long getOptuserid() {
		return optuserid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.optuserid
	 * @param optuserid  the value for tk_sys_dynamics.optuserid
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setOptuserid(Long optuserid) {
		this.optuserid = optuserid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.optusernm
	 * @return  the value of tk_sys_dynamics.optusernm
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getOptusernm() {
		return optusernm;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.optusernm
	 * @param optusernm  the value for tk_sys_dynamics.optusernm
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setOptusernm(String optusernm) {
		this.optusernm = optusernm == null ? null : optusernm.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.opttime
	 * @return  the value of tk_sys_dynamics.opttime
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getOpttime() {
		return opttime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.opttime
	 * @param opttime  the value for tk_sys_dynamics.opttime
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setOpttime(String opttime) {
		this.opttime = opttime == null ? null : opttime.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.context
	 * @return  the value of tk_sys_dynamics.context
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getContext() {
		return context;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.context
	 * @param context  the value for tk_sys_dynamics.context
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setContext(String context) {
		this.context = context == null ? null : context.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.remarks1
	 * @return  the value of tk_sys_dynamics.remarks1
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Integer getRemarks1() {
		return remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.remarks1
	 * @param remarks1  the value for tk_sys_dynamics.remarks1
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setRemarks1(Integer remarks1) {
		this.remarks1 = remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.remarks2
	 * @return  the value of tk_sys_dynamics.remarks2
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public Integer getRemarks2() {
		return remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.remarks2
	 * @param remarks2  the value for tk_sys_dynamics.remarks2
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setRemarks2(Integer remarks2) {
		this.remarks2 = remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.remarks3
	 * @return  the value of tk_sys_dynamics.remarks3
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getRemarks3() {
		return remarks3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.remarks3
	 * @param remarks3  the value for tk_sys_dynamics.remarks3
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3 == null ? null : remarks3.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.remarks4
	 * @return  the value of tk_sys_dynamics.remarks4
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getRemarks4() {
		return remarks4;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.remarks4
	 * @param remarks4  the value for tk_sys_dynamics.remarks4
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setRemarks4(String remarks4) {
		this.remarks4 = remarks4 == null ? null : remarks4.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.remarks5
	 * @return  the value of tk_sys_dynamics.remarks5
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getRemarks5() {
		return remarks5;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.remarks5
	 * @param remarks5  the value for tk_sys_dynamics.remarks5
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setRemarks5(String remarks5) {
		this.remarks5 = remarks5 == null ? null : remarks5.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_sys_dynamics.remarks6
	 * @return  the value of tk_sys_dynamics.remarks6
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public String getRemarks6() {
		return remarks6;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_sys_dynamics.remarks6
	 * @param remarks6  the value for tk_sys_dynamics.remarks6
	 * @mbg.generated  Fri Dec 14 13:49:53 CST 2018
	 */
	public void setRemarks6(String remarks6) {
		this.remarks6 = remarks6 == null ? null : remarks6.trim();
	}
}