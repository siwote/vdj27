package com.lehand.duty.service;


import com.lehand.base.common.Pager;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkTaskTimeNode;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Repository
@Service
public class TkTaskTimeNodeService {

	@Resource
	protected GeneralSqlComponent generalSqlComponent;

	public int insert(TkTaskTimeNode node) {
		return generalSqlComponent.insert(SqlCode.addTktasktimenode,node);
	}

	/**
	 *
	 * 通过主表id查询时间节点表相关数据
	 * @author pantao
	 * @date 2018年11月1日上午11:52:07
	 *
	 * @param taskid
	 * @return
	 */
	public List<TkTaskTimeNode> listTimeByTaskId(Long compid,Long taskid){
		return generalSqlComponent.query(SqlCode.listTktasktimenodeByTaskid,new Object[]{compid,taskid});
	}


	/**
	 * 根据任务id查询时间结点数
	 * @param compid
	 * @param taskid
	 * @return
	 */
	public int getTimeCountByTaskId(Long compid,Long taskid){
		return generalSqlComponent.query(SqlCode.getTktasktimenodeCountByTaskid,new Object[]{compid,taskid});
	}

	/**
	 * 分页查询
	 * @author pantao
	 * @date 2018年11月21日下午1:31:22
	 *
	 * @param taskid
	 * @param pager
	 * @return
	 */
	public Pager pageQuery(Long compid,Long taskid,Pager pager){
		return generalSqlComponent.pageQuery(SqlCode.pagerTktasktimenodeByTaskid,new Object[]{compid,taskid},pager);
	}

	/**
	 * 根据任务id删除时间结点
	 * @param compid
	 * @param taskid
	 */
	public void deleteByTaskid(Long compid,Long taskid) {
		generalSqlComponent.delete(SqlCode.deleteTktasktimenodeByTaskid,new Object[]{compid,taskid});

	}

	/**
	 * 确认完成某个周期下的所有人的任务
	 * @param enddate
	 */
	public void updateRemarks3(Long compid,Long taskid, String enddate, String str) {
		generalSqlComponent.update(SqlCode.updateTktasktimenode,new Object[]{str,compid,taskid,enddate});
	}

	/**
	 * 周期任务删除该周期任务
	 * @param taskid
	 * @param now
	 */
	public void deleteByStarttime(Long compid,Long taskid, String now) {
		generalSqlComponent.delete(SqlCode.deleteTimeByTaskidAndEnddate,new Object[]{compid,taskid,now});
	}

	/**
	 * 周期任务根据主任务id和结束时间查询对应的节点对象
	 * @param taskid
	 * @param endtime
	 * @return
	 */
	public TkTaskTimeNode getBean(Long compid,Long taskid,String endtime) {
		return generalSqlComponent.query(SqlCode.getTimeBytaskidAndEnddate,new Object[]{compid,taskid,endtime});
	}

	public int getTimeCountByTaskIdAndEndDate(Long compid,Long taskid,String endtime){
		return  generalSqlComponent.query(SqlCode.getTimeCountByTaskIdAndEndDate,new Object[]{compid,taskid,endtime})  ;
	}
}
