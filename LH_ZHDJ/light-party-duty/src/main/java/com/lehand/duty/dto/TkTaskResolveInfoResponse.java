package com.lehand.duty.dto;

import java.util.List;

public class TkTaskResolveInfoResponse {

	private Long id;
	private String mesg;
	private List<TkAttachmentResponse> attachment;//附件
	private List<TkCommentResponse> comment;//评论
	private Boolean isshow;
	private String opttype;//操作类型
	private Boolean isopen;
	private int commentnum=0;//评论条数
	private String backReason;
	private int parisenum;//点赞数量
	private int flag;//是否已点赞0表示未点赞，1表示已点赞
	private String opttime;
	private String username;


	public String getOpttime() {
		return opttime;
	}
	public void setOpttime(String opttime) {
		this.opttime = opttime;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMesg() {
		return mesg;
	}
	public void setMesg(String mesg) {
		this.mesg = mesg;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<TkAttachmentResponse> getAttachment() {
		return attachment;
	}
	public void setAttachment(List<TkAttachmentResponse> attachment) {
		this.attachment = attachment;
	}
	public List<TkCommentResponse> getComment() {
		return comment;
	}
	public void setComment(List<TkCommentResponse> comment) {
		this.comment = comment;
	}
	public Boolean getIsshow() {
		return isshow;
	}
	public void setIsshow(Boolean isshow) {
		this.isshow = isshow;
	}
	public String getOpttype() {
		return opttype;
	}
	public void setOpttype(String opttype) {
		this.opttype = opttype;
	}
	public Boolean getIsopen() {
		return isopen;
	}
	public void setIsopen(Boolean isopen) {
		this.isopen = isopen;
	}
	public int getCommentnum() {
		return commentnum;
	}
	public void setCommentnum(int commentnum) {
		this.commentnum = commentnum;
	}
	public String getBackReason() {
		return backReason;
	}
	public void setBackReason(String backReason) {
		this.backReason = backReason;
	}
	public int getParisenum() {
		return parisenum;
	}
	public void setParisenum(int parisenum) {
		this.parisenum = parisenum;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
}
