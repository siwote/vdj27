package com.lehand.duty.common.modulemethod;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pt
 * @Title: 文件预览下载信息
 * @Package
 * @Description:
 * @date 2021/6/114:02
 */
@Service
public class FilesPreviewAndDownload {

    @Resource GeneralSqlComponent generalSqlComponent;
    //预览和上传的路径
    @Value("${jk.manager.down_ip}")private String downIp;

    /**
     * 获取附件list集合
     * @param fileids
     * @param compid
     * @return
     */
    public List<Map<String, Object>> listFile(String fileids,Long compid) {
        List<Map<String,Object>> listFile = new ArrayList<>();
        if(!StringUtils.isEmpty(fileids)){
            String[] fileid = fileids.split(",");
            for (String id:fileid) {
                Map<String,Object> m =  generalSqlComponent.query(SqlCode.getPrintFile,
                        new HashMap<String,Object>(){{put("compid",compid);put("id",id);}});
                if(m!=null){
                    String dir = (String) m.get("dir");
                    m.put("fileid",id);
                    m.put("flpath", String.format("%s%s", downIp, dir));
                    m.put("name",m.get("name").toString().substring(0,m.get("name").toString().lastIndexOf(".")));
                    listFile.add(m);
                }
            }
        }
        return  listFile;
    }
}
