package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.common.modulemethod.FilesPreviewAndDownload;
import com.lehand.duty.dao.*;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.utils.DateUtil;
import com.lehand.duty.dto.*;
import com.lehand.duty.pojo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

/**
 * 
 * 我的任务/任务反馈 接口实现类
 * 
 * @author pantao
 * @date 2018年10月15日 上午9:01:22
 *
 */
@Service
public class TkMyTaskService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	private static final String SELECT_LIST_SQL = "select a.compid,a.id,a.taskid,a.remarks6,d.period,d.tkname,d.userid,d.username,d.classid,d.clname,d.endtime,a.createtime,a.progress,a.status,s.newtime from tk_my_task a"
			+" left join (select b.id id, b.userid userid,b.username username,b.classid classid,b.tkname tkname,c.clname clname,b.period period,b.endtime endtime,b.compid compid from "
			+" tk_task_deploy b left join tk_task_class c on b.classid = c.id and b.compid=c.compid)d on d.id=a.taskid and a.compid=d.compid"
			+" left join (select compid,taskid,max(opttime) newtime from tk_my_task_log where userid=? group by compid,taskid) s on s.compid=a.compid and a.taskid=s.taskid "
			+" where a.subjectid =? and a.compid=? ";
	private static final Logger logger = LogManager.getLogger(TkMyTaskService.class);
	private static final String COUNT_SQL_BY_STATUS = "select count(if(status=3,true,null)) as '3',count(if(status=2,true,null)) as '2',count(if(status in (4,5),true,null)) as '100',count(if(status=6,true,null)) as '6' from tk_my_task where compid=? and subjectid=?";

    @Resource
    FilesPreviewAndDownload filesPreviewAndDownload;
	@Resource
	private TkTaskDeployDao tkTaskDeployDao;
	@Resource
	private TkMyTaskDao tkMyTaskDao;
	@Resource
	private TkAttachmentService tkAttachmentService;
	@Resource
	private TkCommentReplyService tkCommentReplyDao;
	@Resource
	private TkIntegralRuleService tkIntegralRuleService;
	@Resource
	private TkTaskClassDao tkTaskClassDao;
	@Resource
	private TkTaskSubjectService tkTaskSubjectService;
	@Resource
	private TkTaskDeployService tkTaskDeployBusinessImpl;
	@Resource
	TkTaskFeedBackAuditService tkTaskFeedBackAuditBusiness;
	/**
	 * 
	 * 我的任务实现类方法
	 * 
	 * @author pantao
	 * @date 2018年10月15日 上午9:06:30
	 * 
	 * @param map
	 * @return
	 * @throws LehandException
	 */
	@SuppressWarnings("unchecked")
	public void page(Map<String, Object> map, Long userid,Long compid, Pager pager) throws LehandException {

		Calendar calSmall = Calendar.getInstance();
		calSmall.setTime(calSmall.getTime());
		if(StringUtils.isEmpty(map.get("status"))) {
			LehandException.throwException("请求参数不正确！");
		}
		Integer status = Integer.valueOf(String.valueOf(map.get("status")));// 状态(2:未签收,3:办理中,4:已暂停,5:已完成,6:已退回,100 暂停或已完成)
		Object classId = map.get("classid");// 任务分类ID
		Object createtime = map.get("createtime");// 查询时间段
		Object newtime = map.get("newtime");
		Object tkname = map.get("tkname");// 模糊查询
		Object progress = map.get("progress");// 进度值
		String sort = pager.getSort();// 排序字段 tkname classname
		String direction = pager.getDirection();// 排序方向 desc asc
		// 返回的结果集
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = new StringBuilder(SELECT_LIST_SQL);
		ps.add(userid);
		ps.add(userid);
		ps.add(compid);
		query_sql = byStatus(status, ps, query_sql);
		query_sql =byClassid(classId, query_sql);
		query_sql =byTkname(tkname, query_sql);
		query_sql =byCreateTime(calSmall, createtime, ps, query_sql);
		query_sql =byNewTime(calSmall, newtime, ps, query_sql);
		query_sql =byCreateid(map, ps, query_sql);
		query_sql =byProgress(progress, ps, query_sql);
		query_sql =byOrder(sort, direction, query_sql);
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
		List<Map<String,Object>> myTaskList = (List<Map<String, Object>>) pager.getRows();
		for (Map<String, Object> taskMap : myTaskList) {
			Long taskid = (Long) taskMap.get("taskid");
			taskMap.put("feedmsg","");
			taskMap.put("remindColor","");
			taskMap.put("feedid","");
			TkMyTaskLog tktaskLog = generalSqlComponent.query(SqlCode.getlatestFeedNotAudit, new Object[]{taskid, compid, userid});
			List<TaskRemindTimes> taskRemindTimes = tkTaskFeedBackAuditBusiness.listRemindTimes(compid, taskid, userid);
			ArrayList<String> times = new ArrayList<>(taskRemindTimes.size());
			for (TaskRemindTimes taskRemindTime : taskRemindTimes) {
				times.add(String.valueOf(taskRemindTime.getFeedstatus()));
			}
			if (tktaskLog != null) {
				taskMap.put("feedmsg", tktaskLog.getMesg());
				taskMap.put("feedid", tktaskLog.getId());
			}
			taskMap.put("remindColor", times);//反馈状态每个人的状态列表
		}


	}


    public List<Map<String, Object>> listMyTask(Session session, String taskid, String tkname) throws ParseException {
		Assert.hasText(taskid,"taskid cant be null");
		HashMap<String, Object> params = new HashMap<>();
		params.put("compid",session.getCompid());
		params.put("status", Constant.TkMyTask.STATUS_SGIN);
		params.put("subjectid",session.getUserid());
		params.put("tkname",tkname);
		params.put("taskid",taskid);
		List<Map<String, Object>> tasks = generalSqlComponent.query(SqlCode.listMyTask, params);
		//去除不允许反馈的时间点
		reSetFeedTime(tasks);
		return tasks;
	}

	@SuppressWarnings("rawtypes")
	public void reSetFeedTime(List<Map<String, Object>> tasks) throws ParseException {
		Date now = DateEnum.now();
		for (Map<String, Object> task : tasks) {
			String remarks6 = (String) task.get("remarks6");
			if(StringUtils.hasText(remarks6)){
				//[{"feedTime":"2019-04-27 23:59:59","remindTimes":"2019-04-24 09:00:00,2019-04-26 09:00:00","require":"gggggggggg"}]
				int i = 0;
				List<Map> maps = JSON.parseArray(remarks6, Map.class);
				Iterator<Map> iterator = maps.iterator();
				while(iterator.hasNext()){
					Map next = iterator.next();
					String feedTime = (String) next.get("feedTime");
					if(now.before(DateEnum.YYYYMMDDHHMMDD.parse(feedTime))){
						if(i > 0){
							iterator.remove();
						}
						i++;
					}
				}
				task.put("feedTime",JSON.toJSONString(maps));
			}
		}
	}

	private StringBuilder byCreateid(Map<String, Object> map, List<Object> ps, StringBuilder query_sql) {
		if(!StringUtils.isEmpty(map.get("createid"))) {
			Integer createid = Integer.valueOf(map.get("createid").toString());
			query_sql.append(" and userid=? ");
			ps.add(createid);
		}
		return query_sql;
	}

	
	private StringBuilder byOrder(String sort, String direction, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(direction)) {// 带排序
			String sort2 = sort.toString();
			String direction2 = direction.toString();
			query_sql.append(" order by " + sort2 + " " + direction2);
		} else {
			query_sql.append(" order by s.newtime desc,a.createtime desc ");
		}
		return query_sql;
	}

	private StringBuilder byProgress(Object progress, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(progress)) {// 带进度值
			String prog = progress.toString();
			String[] progs = prog.split(",");
			String progress1 = progs[0];// 进度最小值
			String progress2 = progs[1];// 进度最大值
			query_sql.append(" and progress >= ? and progress <= ? ");
			ps.add(progress1);
			ps.add(progress2);
		}
		return query_sql;
	}
	
	private StringBuilder byNewTime(Calendar calSmall, Object time, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(time)) {
			String timeStr = time.toString();
			String[] times = timeStr.split(",");
			String time1 = times[0]+" 00:00:00";
			String time2 = times[1]+" 23:59:59";
			query_sql.append(" and s.newtime >= ? and s.newtime <= ? ");
			ps.add(time1);
			ps.add(time2);
		}
		return query_sql;
	}

	private StringBuilder byCreateTime(Calendar calSmall, Object time, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(time)) {
			String timeStr = time.toString();
			String[] times = timeStr.split(",");
			String time1 = times[0]+" 00:00:00";
			String time2 = times[1]+" 23:59:59";
			query_sql.append(" and createtime >= ? and endtime <= ? ");
			ps.add(time1);
			ps.add(time2);
		}
		return query_sql;
	}

	private StringBuilder byTkname(Object likeStr, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(likeStr)) {
			String likeStr1 = likeStr.toString();
			likeStr1 = likeStr1.replaceAll("\'", "");
			query_sql.append(" and tkname like '%" + likeStr1 + "%'");
		}
		return query_sql;
	}

	private StringBuilder byClassid(Object classId, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(classId)) {// 带任务分类名
			String classs = classId.toString();
			String[] str = classs.split(",");
			String clid = "";
			for (String string : str) {
				clid += string + ",";
			}
			clid = clid.substring(0, clid.length() - 1);
			query_sql.append(" and classid in (" + clid + ")");
		}
		return query_sql;
	}

	private StringBuilder byStatus(Integer status, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(status)) {
			if (status == 100) {
				Integer status1 = 4;
				Integer status2 = 5;
				query_sql.append(" and status in (" + status1 + "," + status2 + ") ");
			} else {
				query_sql.append(" and  status = ? ");
				ps.add(status);
			}
		}
		return query_sql;
	}

	/**
	 * 
	 * 我的任务列表 头部状态角标
	 * 
	 * @author pantao
	 * @date 2018年10月15日 下午3:51:18
	 * 
	 * @param userid
	 * @return
	 */
	public List<Map<String, Object>> listCountByStatus(Long compid,Long userid, int sjtype) {
		Map<String, Object> map = generalSqlComponent.getDbComponent().getMap(COUNT_SQL_BY_STATUS, new Object[] {compid, userid });
		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
		lists.add(getMap("3", "办理中", map));
		lists.add(getMap("2", "未签收", map));
		lists.add(getMap("100", "暂停或完成", map));
		return lists;
	}

	/**
	 * 反馈进度
	 * @param request
	 * @param session
	 * @throws LehandException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public void feedBack(TaskProcessEnum taskProcessEnum, MyTaskLogRequest request, Session session) throws LehandException {
		taskProcessEnum.handle(session, request,session);
	}
	
	/**
	 * 批量签收、退回
	 * @param ids  -->  id1,id2,...
	 * @param status 3:签收,6:退回
	 */
	@Transactional(rollbackFor=Exception.class)
	public void batchOperate(String msg,String ids,Session session,int status) throws LehandException {
		if (!StringUtils.hasText(ids)) {
			LehandException.throwException("请选中任务!");
		}
		MyTaskLogRequest request = new MyTaskLogRequest();
		request.setMessage(msg);
		String[] strs = ids.split(",");
		for (String string : strs) {
			TkMyTask tkMyTask = tkMyTaskDao.get(session.getCompid(),Long.parseLong(string));
			if(tkMyTask==null) {
				LehandException.throwException("任务信息已变更！");
			}
		}
		for (String str : strs) {
			request.setMytaskid(Long.parseLong(str));
			//退回需要审核所以这里就不能直接更新状态需要审核完成并通过后才能改变状态，而签收就不一样，可以直接改变子任务状态
			if(status!=Constant.TkMyTask.STATUS_BACK) {
				int num = tkMyTaskDao.updateTkMyTaskStatus(session.getCompid(),status, request.getMytaskid());
				if (num<=0) {
					continue;
				}
			}
			switch (status) {
				case Constant.TkMyTask.STATUS_SGIN:
					TaskProcessEnum.SIGN_IN.handle(session, request,session);
					break;
				case Constant.TkMyTask.STATUS_BACK:
					TaskProcessEnum.RETURN.handle(session, request,session);
					break;
				default:
					break;
			}
		}
	}
	
	/**
	 * 新增回复
	 * @author pantao
	 * @date 2018年11月28日上午9:49:24
	 *
	 * @param request
	 * @param session
	 * @throws LehandException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void insertCommentReply(TaskProcessEnum taskProcessEnum, CommentReplyRequest request, Session session) throws LehandException{
		taskProcessEnum.handle(session, request,session);
	}
	
	/**
	 * 删除回复
	 * @author pantao
	 * @date 2018年11月28日上午9:49:42
	 *
	 */
	public void deleteReply(Long compid,Long id) {
		tkCommentReplyDao.delete(compid,id);
	}

	/**
	 * 查看任务详情
	 * @author pantao
	 * @date 2018年11月15日下午3:09:05
	 *
	 * @param myid 任务子表id
	 * @return
	 * @throws ParseException 
	 * @throws LehandException 
	 */
	public TkTaskRespond details(Long myid, int flag, Session session) throws ParseException, LehandException {
		int module = 2;
//		Date now = DateEnum.now();
		TkTaskRespond tkTaskRespond = new TkTaskRespond();
		TkMyTask tkMyTask = tkMyTaskDao.get(session.getCompid(),myid);
		if(tkMyTask==null) {
			LehandException.throwException("该任务信息已被变更！");
		}
		tkTaskRespond.setMystatus(tkMyTask.getStatus());
		TkTaskDeploy tkTaskDeploy = tkTaskDeployDao.get(session.getCompid(),tkMyTask.getTaskid());
		String diffdata = tkTaskDeploy.getDiffdata();
		tkTaskRespond.setTaskid(tkTaskDeploy.getId());
		tkTaskRespond.setTkname(tkTaskDeploy.getTkname());
		tkTaskRespond.setUsername(tkTaskDeploy.getUsername());
		tkTaskRespond.setExcuteusers(session.getUsername());
		tkTaskRespond.setTkdesc(tkTaskDeploy.getTkdesc());
		tkTaskRespond.setTabulardata(tkTaskDeploy.getRemarks6());
		tkTaskRespond.setDiffdata(diffdata);
		tkTaskRespond.setProgress(tkTaskDeploy.getProgress());
		tkTaskRespond.setStatus(tkTaskDeploy.getStatus());
		tkTaskRespond.setCrossFlag(tkTaskDeploy.getRemarks2());
		tkTaskRespond.setSrctaskid(tkTaskDeploy.getSrctaskid());
		tkTaskRespond.setAnnex(tkTaskDeploy.getAnnex());
		tkTaskRespond.setPublisher(tkTaskDeploy.getPublisher());
		tkTaskRespond.setTphone(tkTaskDeploy.getTphone());
		//任务总分
//		TkIntegralDetails tkIntegralDetails = tkIntegralDetailsDao.byTaskId(session.getCompid(),tkMyTask.getTaskid());
		//任务点赞总积分
//		TkPraiseDetails tkPariseDetails = tkPraiseDetailsDao.byTaskId(tkMyTask.getTaskid());
//		Double score = tkIntegralDetails.getScore();//+tkPariseDetails.getScore();
		tkTaskRespond.setScore(0.0);
		Long taskid = tkTaskDeploy.getId();
		// 执行人集合以及执行人机构集合
		List<TkTaskSubject> list = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		tkTaskDeployBusinessImpl.getSubject(session.getCompid(),tkTaskRespond, list,0);
		// 任务属性中设置的自定义字段
		//关注人
		List<TkTaskSubject> list1 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_FOCUS);
		tkTaskDeployBusinessImpl.getSubject(session.getCompid(),tkTaskRespond, list1,1);
		//协办人
		List<TkTaskSubject> list2 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_COORDINATOR);
		tkTaskDeployBusinessImpl.getSubject(session.getCompid(),tkTaskRespond, list2, 2);
		//牵头人
		List<TkTaskSubject> list3 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_LEADLEADER);
		tkTaskDeployBusinessImpl.getSubject(session.getCompid(),tkTaskRespond, list3, 3);
		//分管领导
		List<TkTaskSubject> list4 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_CHARGELEADER);
		tkTaskDeployBusinessImpl.getSubject(session.getCompid(),tkTaskRespond, list4,4);
		String excuteusers = tkMyTask.getSubjectname();
		String excuteusersids = tkMyTask.getSubjectid().toString();
		String orgnames = generalSqlComponent.query(SqlCode.getOrgname, new Object[] {session.getCompid(),session.getCompid(),tkMyTask.getSubjectid()});
		tkTaskRespond.setExcuteusers(excuteusers);
		tkTaskRespond.setExcuteusersids(excuteusersids);
		tkTaskRespond.setOrganname(orgnames);
	    tkTaskRespond.setMytaskid(myid);
		if(flag == 1) {//周期（和数据库保持一致）
			tkTaskRespond.setStarttime(tkMyTask.getCreatetime());
			tkTaskRespond.setEndtime(tkMyTask.getDatenode());
			tkTaskRespond.setDays(DateUtil.calculationDay(DateEnum.YYYYMMDDHHMMDD.parse(tkMyTask.getDatenode())));
		}else {//普通
			tkTaskRespond.setStarttime(tkMyTask.getCreatetime());//由于现在任务都是立即创建的，所以任务详情中的开始时间都是取子任务中的创建时间
			tkTaskRespond.setEndtime(tkTaskDeploy.getEndtime());
			tkTaskRespond.setDays(DateUtil.calculationDay(DateEnum.YYYYMMDDHHMMDD.parse(tkTaskDeploy.getEndtime())));
		}
		List<Map<String,Object>> listMap = tkAttachmentService.list2map(session.getCompid(),module, tkTaskDeploy.getId());
		tkTaskRespond.setAttachment(listMap);
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),tkTaskDeploy.getClassid(),1);
		tkTaskRespond.setClassname(taskClass.getClname());
		tkTaskRespond.setClassid(taskClass.getId());
		//转发标志，默认不允许转发
		tkTaskRespond.setShareFlag("0");
		if("admin".equals(session.getLoginAccount())){
			tkTaskRespond.setShareFlag("1");
		}else{
			Long count = generalSqlComponent.query(SqlCode.getChildCount, new Object[]{session.getCompid(), session.getCurrentOrg().getOrgid()});
			Integer status = tkMyTask.getStatus();
			count = count == null ? 0 :count;
			if(count>0 && status!=-1&&status!=0){      //有下级且没有没有被移除,没有退回任务，允许转发
				tkTaskRespond.setShareFlag("1");
			}
		}
		//从上传附件
        tkTaskRespond.setFiles(filesPreviewAndDownload.listFile(tkTaskDeploy.getFiles(),session.getCompid()));
		logger.info("任务详情查询成功！");

		return tkTaskRespond;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String setDelFlag(Session session, Date now, TkTaskDeploy tkTaskDeploy, String diffdata) throws ParseException {
		if(StringUtils.hasText(diffdata)){
			List<Map> timeMaps = JSON.parseArray(diffdata,Map.class);
			if(!CollectionUtils.isEmpty(timeMaps)){
				for (Map timeMap : timeMaps) {
					String delFlag = "1";
					String flag1 = String.valueOf((Integer) timeMap.get("flag"));
					String data = (String) timeMap.get("data");
					if("1".equals(flag1)){
						Date date = DateEnum.YYYYMMDDHHMMDD.parse(data);
						Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime, new Object[]{session.getCompid(), tkTaskDeploy.getId(), data});
						if(now.before(date)||count>0){
							delFlag = "0";
						}
					}
					timeMap.put("delFlag",delFlag);
				}
			}
			diffdata = JSON.toJSONString(timeMaps);
		}
		return diffdata;
	}

	private static Map<String, Object> getMap(String status, String name, Map<String, Object> map) {
		Map<String, Object> maps = new HashMap<>(3);
		maps.put("status", status);
		maps.put("name", name);
		if(map!=null) {
			maps.put("number", map.get(status));
		}else {
			maps.put("number", Constant.EMPTY);
		}
		return maps;
	}


    public FeedDetails getMyTask(Session session, Long taskid, Long userid) {
        Long compid = session.getCompid();
        return generalSqlComponent.query(SqlCode.getMydetail,new Object[]{compid,taskid,userid, compid,compid});
    }

    public Double getSignIntegral(Long compid) {
		return  tkIntegralRuleService.getIntegral(compid);
	}
	

	/**
	 * 修改反馈前查询该条反馈的详情
	 * @param feedbackid 反馈表id
	 * @return
	 */
	public FeedDetails getFeedBack(Session session, Long feedbackid) {
		FeedDetails info = generalSqlComponent.query(SqlCode.getFeedDetails2, new Object[] {session.getCompid(),feedbackid});
		if(info!=null) {
			if(!StringUtils.isEmpty(info.getUuid())) {
				List<TkAttachment> attachment = generalSqlComponent.query(SqlCode.listTkAttachment, new Object[] {info.getUuid(),session.getCompid()});
			    info.setAttachment(attachment);
			}else{
				info.setAttachment(new ArrayList<TkAttachment>());
			}
            if(!StringUtils.isEmpty(info.getFile())) {
                info.setFiles(filesPreviewAndDownload.listFile(info.getFile(),session.getCompid()));
            }else{
                info.setAttachment(new ArrayList<>());
            }
		}else {
			LehandException.throwException("数据不存在！");
		}
		return info;
	}
}
