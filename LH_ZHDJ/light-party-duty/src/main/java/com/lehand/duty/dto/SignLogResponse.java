package com.lehand.duty.dto;

public class SignLogResponse {

	Long subjectid;//执行人id
	String subjectname;//执行人名称
	String orgname;//执行人的组织机构集合多个机构逗号隔开
	int status;//任务状态
    String signtime;//签收时间
	Long mytaskId;

	public Long getMytaskId() {
		return mytaskId;
	}

	public void setMytaskId(Long mytaskId) {
		this.mytaskId = mytaskId;
	}

	private Integer hasfeeded;//已反馈 1，未反馈0

	public Integer getHasfeeded() {
		return hasfeeded;
	}

	public void setHasfeeded(Integer hasfeeded) {
		this.hasfeeded = hasfeeded;
	}

	public Long getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(Long subjectid) {
		this.subjectid = subjectid;
	}
	
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public String getOrgname() {
		return orgname;
	}
	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getSigntime() {
		return signtime;
	}
	public void setSigntime(String signtime) {
		this.signtime = signtime;
	}
    
}
