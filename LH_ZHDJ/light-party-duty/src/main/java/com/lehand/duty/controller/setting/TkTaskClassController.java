package com.lehand.duty.controller.setting;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkTaskAuthClassService;
import com.lehand.duty.service.TkTaskClassService;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dto.DlTypeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.exception.LehandException;
import com.lehand.duty.controller.DutyBaseController;

/**
 *    任务分类
 * @author pantao  
 * @date 2018年10月9日 下午2:13:00
 */
@Api(value = "任务分类", tags = { "任务分类" })
@RestController
@RequestMapping("/duty/taskClass")
public class TkTaskClassController extends DutyBaseController {

	private static final Logger logger = LogManager.getLogger(TkTaskClassController.class);
	
	@Resource private TkTaskClassService tkTaskClassService;
	
	@Resource private TkTaskAuthClassService tkTaskAuthClassService;
	
	/**
	 * 任务分类查询
	 * @author pantao  
	 * @date 2018年10月9日 下午6:51:43
	 * @param auth 0代表一般查询1代表根据权限查询
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务分类查询", notes = "任务分类查询", httpMethod = "POST")
	@RequestMapping("/listByFlag")
	public Message listByFlag(String search,int auth){
		Message message = new Message();
		if(!validateUTF8Length(search,0,300)) {
		    message.setMessage("任务名称不能超过100个汉字！");
		    return message;
		}
		int flag = 0;//现在已经没有分类标识了，这里就直接写死
		int status = Constant.TkTaskClass.STATUS_ACTIVE;
		List<Map<String, Object>> result = null;
		try {
			if(auth == 0) {
				result = tkTaskClassService.listByFlag(flag,status,search);
				message.setData(result);
				message.success();
			}else {
				//result = tkTaskClassBusiness.listByAuth(getSession().getUserid(),search);
				List<DlTypeDto>  result1 = tkTaskAuthClassService.listByAuth(getSession(),search);
				message.setData(result1);
				message.success();
			}
		    
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	
	/**  
	 * 任务分类 修改
	 * @author pantao  
	 * @date 2018年10月9日 下午6:51:22
	 * @param id
	 * @param name
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "任务分类 修改", notes = "任务分类 修改", httpMethod = "POST")
	@RequestMapping("/update")
	public Message update(Long id,String name) {
		Message message = new Message();
		try {			
			if(!validateUTF8Length(name,300)) {
				message.setMessage("任务分类名称不能为空且不能超过100个汉字！");
				return message;
			}
			tkTaskClassService.update(getSession().getCompid(),id, name);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	
	/**
	 * 任务分类 新增
	 * @author pantao  
	 * @date 2018年10月9日 下午7:45:06
	 * @param pid
	 * @param name
	 * @return
	 */
    @OpenApi(optflag=1)
    @ApiOperation(value = "任务分类 新增", notes = "任务分类 新增", httpMethod = "POST")
	@RequestMapping("/add")
	public Message add(long pid,String name) {
		int flag=0;
		Message message = new Message();
		try {
			if(!validateUTF8Length(name,300)) {
				message.setMessage("任务分类名称不能为空且不能超过100个汉字！");
				return message;
			}
			tkTaskClassService.addNode(getSession().getCompid(),flag, pid, name);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	
	/**
	   *    任务分类 删除
	 * @author pantao  
	 * @date 2018年10月10日 上午9:18:25
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "任务分类 删除", notes = "任务分类 删除", httpMethod = "POST")
	@RequestMapping("/delete")
	public Message delete(Long oldClassid,Long newClassid) {
		Message message = new Message();
		try {
			tkTaskClassService.delete(oldClassid,newClassid,getSession().getCompid());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 任务分类拖拽
	 * @author pantao
	 * @date 2018年11月7日上午10:37:25
	 * @param srcid 拖动组织
	 * @param tagid 拖动到哪个组织
	 * @param flag 1:上面,2:下面,3:里面d
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "任务分类拖拽", notes = "任务分类拖拽", httpMethod = "POST")
	@RequestMapping("/move")
	public Message move(Long srcid,Long tagid,int flag) {
		Message message = new Message();
		try {
			tkTaskClassService.move(srcid, tagid, flag,getSession().getCompid());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 * 获取任务分类设计的任务数量
	 * @param classid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取任务分类设计的任务数量", notes = "获取任务分类设计的任务数量", httpMethod = "POST")
	@RequestMapping("/gettaskcount")
	public Message getTaskCount(Long classid) {
		Message message = new Message();
		try {
			int num = tkTaskClassService.getNum(getSession().getCompid(),classid);
			message.success().setData(num);;
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error("任务条数查询出错！");
		}
		return message;
	}
	
}
