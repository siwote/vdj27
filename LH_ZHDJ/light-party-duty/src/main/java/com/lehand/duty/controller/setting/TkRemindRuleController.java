package com.lehand.duty.controller.setting;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSONObject;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkRemindRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Message;
import com.lehand.base.exception.LehandException;
import com.lehand.duty.controller.DutyBaseController;

/**
 * 
 *    提醒规则  
 * 
 * @author pantao  
 * @date 2018年10月9日 下午9:19:45
 *
 */
@Api(value = "提醒规则", tags = { "提醒规则" })
@RestController
@RequestMapping("/duty/remindRule")
public class TkRemindRuleController extends DutyBaseController {
	
	private static final Logger logger = LogManager.getLogger(TkTaskClassController.class);
	
	@Resource private TkRemindRuleService tkRemindRuleService;

	/**
	 * 
	   *   提醒规则  更新
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 上午10:32:30
	 *  
	 * @param list
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "提醒规则  更新", notes = "提醒规则  更新", httpMethod = "POST")
	@RequestMapping("/update")
	public Message update(String list) {
		Message message = new Message();
		try {
            List<Map> listMap = JSON.parseArray(list,Map.class);
            if(listMap == null) {
                LehandException.throwException("请传递提醒规则的编码！");
            }
			tkRemindRuleService.update(getSession().getCompid(),listMap);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
		
	}
	
	
	/**
	 * 
	 *   提醒规则 查询
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 上午11:12:12
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "提醒规则 查询", notes = "提醒规则 查询", httpMethod = "POST")
	@RequestMapping("/list")
	public Message list() {
		int status = 1;
		Message message = new Message();
		try {
			List<Map<String, Object>> result = tkRemindRuleService.listByStatus(getSession().getCompid(),status);
			message.setData(result);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
}
