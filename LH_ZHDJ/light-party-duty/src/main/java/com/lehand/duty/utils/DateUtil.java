package com.lehand.duty.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * 计算剩余天数  
 * 
 * @author pantao  
 * @date 2018年10月22日 上午10:38:10
 *
 */
public class DateUtil {

	/**
	 * 
	 * 计算当前日期距离任务截止日期剩余的天数  
	 * 
	 * @author pantao  
	 * @date 2018年10月22日 上午10:46:05
	 *  
	 * @param date (2018-10-25 00:00:00)
	 * @return
	 */
	public static int calculationDay(Date date) {
		Date current = new Date();
		int days =(int) Math.ceil((date.getTime() - current.getTime()) / (1000*3600*24)) ;
		if(days<=0) {
			days = 0;
		}
		return days;
	}
	
	/**
	 * 计算当前日期前n天的日期
	 * @param day
	 * @return
	 */
	public static Date beforeDay(int day) {
		Date current = new Date();
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(current); 
		calendar.add(Calendar.SECOND, -day);
		return calendar.getTime();
	}
}
