package com.lehand.duty.dto;

import java.util.List;
import java.util.Map;

/**
 * 详情页面右侧执行人树数据对象
 * @author pantao
 *
 */
public class TreeSubjectResponse {
	private int num;
	private List<Map<String,Object>> list;
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	
	
}
