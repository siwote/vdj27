package com.lehand.duty.pojo;

public class TkComment extends FatherPojo {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.id
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Long id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.model
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer model;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.modelid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Long modelid;
	
	private Long taskid;
	
	private Long mytaskid;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.fsjtype
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer fsjtype;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.fsubjectid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Long fsubjectid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.fsubjectname
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String fsubjectname;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.commtime
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String commtime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.userflag
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer userflag;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.tsjtype
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer tsjtype;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.tsubjectid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Long tsubjectid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.tsubjectname
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String tsubjectname;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.commflag
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer commflag;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.commtext
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String commtext;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.remarks1
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer remarks1;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.remarks2
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private Integer remarks2;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.remarks3
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String remarks3;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.remarks4
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String remarks4;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.remarks5
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String remarks5;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_comment.remarks6
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	private String remarks6;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.id
	 * @return  the value of tk_comment.id
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Long getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.id
	 * @param id  the value for tk_comment.id
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.model
	 * @return  the value of tk_comment.model
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getModel() {
		return model;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.model
	 * @param model  the value for tk_comment.model
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setModel(Integer model) {
		this.model = model;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.modelid
	 * @return  the value of tk_comment.modelid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Long getModelid() {
		return modelid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.modelid
	 * @param modelid  the value for tk_comment.modelid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setModelid(Long modelid) {
		this.modelid = modelid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.fsjtype
	 * @return  the value of tk_comment.fsjtype
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getFsjtype() {
		return fsjtype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.fsjtype
	 * @param fsjtype  the value for tk_comment.fsjtype
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setFsjtype(Integer fsjtype) {
		this.fsjtype = fsjtype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.fsubjectid
	 * @return  the value of tk_comment.fsubjectid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Long getFsubjectid() {
		return fsubjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.fsubjectid
	 * @param fsubjectid  the value for tk_comment.fsubjectid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setFsubjectid(Long fsubjectid) {
		this.fsubjectid = fsubjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.fsubjectname
	 * @return  the value of tk_comment.fsubjectname
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getFsubjectname() {
		return fsubjectname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.fsubjectname
	 * @param fsubjectname  the value for tk_comment.fsubjectname
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setFsubjectname(String fsubjectname) {
		this.fsubjectname = fsubjectname == null ? null : fsubjectname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.commtime
	 * @return  the value of tk_comment.commtime
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getCommtime() {
		return commtime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.commtime
	 * @param commtime  the value for tk_comment.commtime
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setCommtime(String commtime) {
		this.commtime = commtime == null ? null : commtime.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.userflag
	 * @return  the value of tk_comment.userflag
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getUserflag() {
		return userflag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.userflag
	 * @param userflag  the value for tk_comment.userflag
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setUserflag(Integer userflag) {
		this.userflag = userflag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.tsjtype
	 * @return  the value of tk_comment.tsjtype
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getTsjtype() {
		return tsjtype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.tsjtype
	 * @param tsjtype  the value for tk_comment.tsjtype
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setTsjtype(Integer tsjtype) {
		this.tsjtype = tsjtype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.tsubjectid
	 * @return  the value of tk_comment.tsubjectid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Long getTsubjectid() {
		return tsubjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.tsubjectid
	 * @param tsubjectid  the value for tk_comment.tsubjectid
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setTsubjectid(Long tsubjectid) {
		this.tsubjectid = tsubjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.tsubjectname
	 * @return  the value of tk_comment.tsubjectname
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getTsubjectname() {
		return tsubjectname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.tsubjectname
	 * @param tsubjectname  the value for tk_comment.tsubjectname
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setTsubjectname(String tsubjectname) {
		this.tsubjectname = tsubjectname == null ? null : tsubjectname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.commflag
	 * @return  the value of tk_comment.commflag
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getCommflag() {
		return commflag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.commflag
	 * @param commflag  the value for tk_comment.commflag
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setCommflag(Integer commflag) {
		this.commflag = commflag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.commtext
	 * @return  the value of tk_comment.commtext
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getCommtext() {
		return commtext;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.commtext
	 * @param commtext  the value for tk_comment.commtext
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setCommtext(String commtext) {
		this.commtext = commtext == null ? null : commtext.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.remarks1
	 * @return  the value of tk_comment.remarks1
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getRemarks1() {
		return remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.remarks1
	 * @param remarks1  the value for tk_comment.remarks1
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setRemarks1(Integer remarks1) {
		this.remarks1 = remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.remarks2
	 * @return  the value of tk_comment.remarks2
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public Integer getRemarks2() {
		return remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.remarks2
	 * @param remarks2  the value for tk_comment.remarks2
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setRemarks2(Integer remarks2) {
		this.remarks2 = remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.remarks3
	 * @return  the value of tk_comment.remarks3
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getRemarks3() {
		return remarks3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.remarks3
	 * @param remarks3  the value for tk_comment.remarks3
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3 == null ? null : remarks3.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.remarks4
	 * @return  the value of tk_comment.remarks4
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getRemarks4() {
		return remarks4;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.remarks4
	 * @param remarks4  the value for tk_comment.remarks4
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setRemarks4(String remarks4) {
		this.remarks4 = remarks4 == null ? null : remarks4.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.remarks5
	 * @return  the value of tk_comment.remarks5
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getRemarks5() {
		return remarks5;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.remarks5
	 * @param remarks5  the value for tk_comment.remarks5
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setRemarks5(String remarks5) {
		this.remarks5 = remarks5 == null ? null : remarks5.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_comment.remarks6
	 * @return  the value of tk_comment.remarks6
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public String getRemarks6() {
		return remarks6;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_comment.remarks6
	 * @param remarks6  the value for tk_comment.remarks6
	 * @mbg.generated  Tue Nov 20 15:23:33 CST 2018
	 */
	public void setRemarks6(String remarks6) {
		this.remarks6 = remarks6 == null ? null : remarks6.trim();
	}

	public Long getTaskid() {
		return taskid;
	}

	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}

	public Long getMytaskid() {
		return mytaskid;
	}

	public void setMytaskid(Long mytaskid) {
		this.mytaskid = mytaskid;
	}
	
}