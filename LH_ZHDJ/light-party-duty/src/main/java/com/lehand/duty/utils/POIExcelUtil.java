package com.lehand.duty.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddressList;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean要继承BaseRowModel,并且属性要使用@ExcelProperty
 * @author Zim
 *
 */
public class POIExcelUtil {
	
	public static final String[] COLUMNS = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	
	public static final String[] HANDERS = {"*任务分类","*任务名称","*起止时间","*执行人","*任务类型"};
	public static final int[] CHECK_CELL_INDEXES = {0,1,2,3,4};
	public static final int [] DOWN_COLUMN_INDEXS = {0,4}; //下拉的列序号数组(序号从0开始)
	public static final String[] TASK_CLASSES = {"普通任务","周期按年","周期按季","周期按月","周期按周","周期按天"};
	public static final int EXCEL_MAX_SIZE = 1000;
	
	/**
     * 
     * @Title: SetDataValidation 
     * @Description: 下拉列表元素很多的情况 (255以上的下拉)
     * @param @param strFormula
     * @param @param firstRow   起始行
     * @param @param endRow     终止行
     * @param @param firstCol   起始列
     * @param @param endCol     终止列
     * @param @return
     * @return HSSFDataValidation
     * @throws
     */
    private static HSSFDataValidation SetDataValidation(String strFormula, 
            int firstRow, int endRow, int firstCol, int endCol) {
        
        // 设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        DVConstraint constraint = DVConstraint.createFormulaListConstraint(strFormula);
        HSSFDataValidation dataValidation = new HSSFDataValidation(regions,constraint);
        
        dataValidation.createErrorBox("请选择", "只能选择,不能自定义!");
        dataValidation.createPromptBox("", null);
    
        return dataValidation;
    }
    
    
//    /**
//     * 
//     * @Title: setDataValidation 
//     * @Description: 下拉列表元素不多的情况(255以内的下拉)
//     * @param @param sheet
//     * @param @param textList
//     * @param @param firstRow
//     * @param @param endRow
//     * @param @param firstCol
//     * @param @param endCol
//     * @param @return
//     * @return DataValidation
//     * @throws
//     */
//    private static DataValidation setDataValidation(Sheet sheet, String[] textList, int firstRow, int endRow, int firstCol, int endCol) {
//        DataValidationHelper helper = sheet.getDataValidationHelper();
//        //加载下拉列表内容
//        DataValidationConstraint constraint = helper.createExplicitListConstraint(textList);
//        constraint.setExplicitListValues(textList);
//        
//        //设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
//        CellRangeAddressList regions = new CellRangeAddressList((short) firstRow, (short) endRow, (short) firstCol, (short) endCol);
//    
//        //数据有效性对象
//        DataValidation data_validation = helper.createValidation(constraint, regions);
//        //处理Excel兼容性问题  
//        if(data_validation instanceof XSSFDataValidation) {  
//        	data_validation.setSuppressDropDownArrow(true);  
//        	data_validation.setShowErrorBox(true);  
//        }else {  
//        	data_validation.setSuppressDropDownArrow(false);  
//        }  
//        sheet.addValidationData(data_validation);  
//        return data_validation;
//    }
    
    /**
     * 创建批量创建任务模板
     * @param filePath
     * @param clsnameList
     * @param orgnameList
     * @param users
     * @throws Exception
     */
    public static void createTaskDeployExcelTemplate(String filePath,
    		                                         List<String> clsnameList,
    		                                         List<String> orgnameList,
                                                     List<String> users) throws Exception{
//		String[] str1 = {"2-商机跟踪","5-智慧党建业务线","7-目标督查业务线","12-2019年业绩目标","14-重要会议","15-重点工作","16-重点项目","17-智慧党建业务线","18-目标督查业务线"};
		String[] clsnames = new String[clsnameList.size()];
		clsnameList.toArray(clsnames);
		
//		String[] str2 = {"1-安徽力瀚科技有限公司","2-产品研发中心","3-软件营销部","4-产品管理部","5-项目管理部","6-软件研发部","7-质量管理部","8-技术运维部","9-创新研究院","10-营销中心","11-工程中心","12-高管团队","13-人力资源中心","14-商务部","15-财务中心","21-项目管理部","22-客户服务部","24-储运部","25-财务部","28-洛尼","30-铜陵","31-磐荣","32-技术支持部","33-党政事业部","34-政教事业部","35-公共安全事业部","42-解决方案中心","43-标案部","44-售前咨询部","46-计划经营部","48-西嘉","49-云端事业部","51-人力资源部","53-总经办","54-党群文化部","55-培训发展部","56-一组","57-二组","58-三组","59-四组","60-分销组"};
//		String[] orgnames = new String[orgnameList.size()];
//		orgnameList.toArray(orgnames);
		
		List<String[]> downIndexData = new ArrayList<String[]>(3);
		downIndexData.add(clsnames);
//        downIndexData.add(orgnames);
        downIndexData.add(TASK_CLASSES);
        
    	createTaskDeployExcelTemplate(filePath, HANDERS, downIndexData, DOWN_COLUMN_INDEXS, EXCEL_MAX_SIZE, users);
    }
    
	/**
	 * @throws Exception 
     * @Title: createExcelTemplate 
     * @Description: 生成Excel导入模板  
     * @param @param filePath  Excel文件路径
     * @param @param handers   Excel列标题(数组)
     * @param @param downIndexData  下拉框数据(数组)
     * @param @param downColumnIndexs  下拉列的序号(数组,序号从0开始)
     * @return void
     * @throws
     */
    private static void createTaskDeployExcelTemplate(String filePath, 
    		                               String[] handers,List<String[]> downIndexData, 
    		                               int[] downColumnIndexs,int downNum,List<String> users) throws Exception{
    	HSSFWorkbook wb = null;
    	FileOutputStream out = null;
    	try {
    		InputStream ins=new FileInputStream(filePath);
    		//创建工作薄
            wb = new HSSFWorkbook(ins);
          /*  //表头样式
            HSSFCellStyle style = wb.createCellStyle();  
            style.setAlignment(HorizontalAlignment.CENTER);
            //字体样式
            HSSFFont fontStyle = wb.createFont();    
            fontStyle.setFontName("微软雅黑");    
            fontStyle.setFontHeightInPoints((short)12);    
            fontStyle.setBold(true);
            style.setFont(fontStyle);
            
            //新建sheet
            //新建sheet
            HSSFSheet sheet1 = wb.createSheet("任务发布信息");
            HSSFSheet sheet2 = wb.createSheet("数据字典");
            sheet1.createFreezePane(0, 1, 0, 1);
            
            //生成sheet1内容
            HSSFRow rowFirst = sheet1.createRow(1);//第一个sheet的第一行为标题
            //写标题
            for(int i=0;i<handers.length;i++){
                HSSFCell cell = rowFirst.createCell(i); //获取第一行的每个单元格
                sheet1.setColumnWidth(i, 4000); //设置每列的列宽
                cell.setCellStyle(style); //加样式
                cell.setCellValue(handers[i]); //往单元格里写数据
            }
            HSSFPatriarch patr1 = sheet1.createDrawingPatriarch();
            HSSFComment comment1 = patr1.createCellComment(patr1.createAnchor(0, 0, 0, 0, 8, 1, 8,1));
            comment1.setString(new HSSFRichTextString("示例：2019-01-01,2019-12-31  注意:月要2位，分隔符号为英文逗号"));
            sheet1.getRow(1).getCell(2).setCellComment(comment1);
            
            HSSFPatriarch patr2 = sheet1.createDrawingPatriarch();
            HSSFComment comment2 = patr2.createCellComment(patr2.createAnchor(0, 0, 0, 0, 8, 1, 8,1));
            comment2.setString(new HSSFRichTextString("执行人姓名(见\"数据字典\"工作簿第3列)，以英文半角逗号分隔！示例：1-张三,6-李四,...."));
            sheet1.getRow(1).getCell(3).setCellComment(comment2);*/
            
            
            HSSFSheet sheet2 = wb.createSheet("数据字典");
            int index = 0;
            for (int i = 0; i < downColumnIndexs.length; i++) {
    			int downIndex = downColumnIndexs[i];
    			String[] downColumnData = downIndexData.get(i);
//    			if (downColumnData.length<5) {
//    				DataValidation setDataValidation = setDataValidation(sheet1, downColumnData, 1, 10000, downIndex ,downIndex);
//    				sheet1.addValidationData(setDataValidation); //超过255个报错
//    				continue;
//    			}
    			String COLUMN_IDX = COLUMNS[index];
				String strFormula = "数据字典!$" + COLUMN_IDX + "$1:$" + COLUMN_IDX + "$"+downColumnData.length; // Sheet2第A1到A5000作为下拉列表来源数据
    			sheet2.setColumnWidth(i, 4000); // 设置每列的列宽
    			// 设置数据有效性加载在哪个单元格上,参数分别是：从sheet2获取A1到A5000作为一个下拉的数据、起始行、终止行、起始列、终止列
    			wb.getSheetAt(0).addValidationData(SetDataValidation(strFormula, 1, downNum, downIndex ,downIndex)); // 下拉列表元素很多的情况
    			// 2、生成sheet2内容
    			for (int k = 0; k < downColumnData.length; k++) {
    				String value = downColumnData[k];
    				if (index == 0) { // 第1个下拉选项，直接创建行、列
    					HSSFRow row = sheet2.createRow(k); // 创建数据行
    					sheet2.setColumnWidth(k, 4000); // 设置每列的列宽
    					row.createCell(0).setCellValue(value); // 设置对应单元格的值
    				} else { // 非第1个下拉选项
    					int rowCount = sheet2.getLastRowNum();
    					if (k <= rowCount) { // 前面创建过的行，直接获取行，创建列
    						// 获取行，创建列
    						sheet2.getRow(k).createCell(index).setCellValue(value); // 设置对应单元格的值
    					} else { // 未创建过的行，直接创建行、创建列
    						sheet2.setColumnWidth(k, 4000); // 设置每列的列宽
    						// 创建行、创建列
    						sheet2.createRow(k).createCell(index).setCellValue(value); // 设置对应单元格的值
    					}
    				}
    			}
    			index++;
    		}
            if(users!=null && users.size()>0) {
            	for (int i = 0; i < users.size(); i++) {
            		String user = users.get(i);
            		HSSFRow row = sheet2.getRow(i);
            		if (row==null) {
						row = sheet2.createRow(i);
					}
            		HSSFCell cell = row.createCell(downColumnIndexs.length);
            		cell.setCellValue(user);
				}
            }
            out = new FileOutputStream(filePath);
            wb.write(out);    
		} finally {
			if (null!=wb) {
				wb.close();
			}
			if (null!=out) {
				out.flush();  
				out.close();
			}
		}
    }
    
    public static void updateExcelTemplate(String filePath,int sheetIndex,int [] downRows,List<String[]> downIndexData,int downNum) throws Exception {
    	FileInputStream in = null;
    	HSSFWorkbook workbook = null;
    	try {
    		in = new FileInputStream(filePath);
    		workbook = new HSSFWorkbook(in);
    		
    		HSSFSheet sheet1 = workbook.getSheetAt(0);
    		HSSFSheet sheet2 = workbook.getSheetAt(sheetIndex);
    		for (int i = 0; i < downIndexData.size(); i++) {
    			String[] downColumns = downIndexData.get(i);
    			String COL = COLUMNS[i];
    			for (int j = 0; j < downColumns.length; j++) {
    				CellAddress address = new CellAddress(COL+(j+1));
    				HSSFRow row = sheet2.getRow(address.getRow());//得到行
    				HSSFCell cell = row.getCell(address.getColumn());//得到列
//    				System.out.println(cell.getStringCellValue());//打印该数据到控制台
    				cell.setCellValue(downColumns[j]);//改变数据
    			}
    			for (int j = downColumns.length; j < downNum; j++) {
    				CellAddress address = new CellAddress(COL+(j+1));
    				HSSFRow row = sheet2.getRow(address.getRow());//得到行
    				if (null==row) {
						break;
					}
    				HSSFCell cell = row.getCell(address.getColumn());//得到列
    				if (null==cell) {
						break;
					}
    				cell.setCellValue("");//改变数据
				}
    			String COLUMN_IDX = COLUMNS[i];
    			String strFormula = "Sheet2!$" + COLUMN_IDX + "$1:$" + COLUMN_IDX + "$"+downNum; // Sheet2第A1到A5000作为下拉列表来源数据
    			// 设置数据有效性加载在哪个单元格上,参数分别是：从sheet2获取A1到A5000作为一个下拉的数据、起始行、终止行、起始列、终止列
    			int downIndex = downRows[i];
    			sheet1.addValidationData(SetDataValidation(strFormula, 1, downNum, downIndex ,downIndex)); // 下拉列表元素很多的情况
    		}
		} finally {
			if (null!=in) {
				in.close();
			}
		}
    	FileOutputStream out = null;
    	try {
    		out = new FileOutputStream(filePath);//写数据到这个路径上
    		workbook.write(out);
    		out.flush();
		} finally {
			if (null!=out) {
				out.close();
			}
			if (null!=workbook) {
				workbook.close();
			}
		}
    }
    
//    public static class TkTaskDeployListener extends AnalysisEventListener<List<String>>{
//
//    	private List<TkTaskDeployRequest> data = new ArrayList<TkTaskDeployRequest>();
//    	
//    	/**
//    	 * excel读取每一行时调用
//    	 */
//		@Override
//		public void invoke(List<String> row, AnalysisContext context) {
//			int rowNum = context.getCurrentRowNum();
//			if (rowNum<=0) {
//				return;
//			}
//			if (row.size()!=HANDERS.length) {
//				throw new RuntimeException((rowNum+1)+"行数据不完整,请检查后在重新上传!");
//			}
//			for (int index : POIExcelUtil.CHECK_CELL_INDEXES) {
//				String cell = row.get(index);
//				if (!StringUtils.hasText(cell)) {
//					throw new RuntimeException((rowNum+1)+"行"+(index+1)+"列数据不完整,请检查后在重新上传!");
//				}
//			}
//			String classes = row.get(1).trim();
//			String tkname = row.get(2).trim();
//			String times = row.get(3).trim();
//			String excuteusers = row.get(4).trim();
//			String type = row.get(5).trim();
//			//任务分类
//			int index = classes.indexOf("-");
//			Long classid = Long.parseLong(classes.substring(0, index));
//			//执行人
//			String[] users = excuteusers.split(",");
//			Set<Subject> subjects = new HashSet<Subject>();
//			for (String user : users) {
//				String[] us = user.split("-");
//				Subject subject = new Subject();
//				subject.setSubjectid(Long.parseLong(us[0].trim()));
//				subject.setSubjectname(us[1].trim());
//				subjects.add(subject);
//			}
//			
//			//时间信息
//			List<Map<String, String>> diffdata = new ArrayList<Map<String, String>>(1);
//			Map<String, String> map = new HashMap<String, String>();
//			if (TASK_CLASSES[0].equals(type)) {
//				map.put("flag", "1");
//				map.put("data", times.substring(11));
//				map.put("end", "");
//			} else if (TASK_CLASSES[1].equals(type)) {
//				map.put("flag", "2");
//				map.put("data", "1,Y,1");
//				map.put("end", "365");
//			} else if (TASK_CLASSES[2].equals(type)) {
//				map.put("flag", "2");
//				map.put("data", "1,Q,1");
//				map.put("end", "90");
//			} else if (TASK_CLASSES[3].equals(type)) {
//				map.put("flag", "2");
//				map.put("data", "1,M,1");
//				map.put("end", "28");
//			} else if (TASK_CLASSES[4].equals(type)) {
//				map.put("flag", "2");
//				map.put("data", "1,W,1");
//				map.put("end", "7");
//			} else {
//				throw new RuntimeException((rowNum+1)+"行任务类型错误,请检查后在重新上传!");
//			}
//			diffdata.add(map);
////			[{"flag":1,"data":"2019-02-28","end":""}]
////			[{"flag":"2","data":"1,W,7","end":"7"}]		
//			TkTaskDeployRequest req = new TkTaskDeployRequest();
//			req.setClassid(classid);
//			req.setTkname(tkname);
//			req.setStarttime(times.substring(0, 10)+" 00:00:00");
//			req.setEndtime(times.substring(11)+" 23:59:59");
//			//			req.setExcuteusers(excuteusers);
//			req.setSubjects(subjects);
//			req.setTimedata(JSON.toJSONString(diffdata));//普通周期任务用
////			req.setTimedataresolve(timedataresolve); //分解任务用
//			req.setTkdesc(Constant.EMPTY);
//			req.setSrctaskid(-1L);
//			req.setUuid(Constant.EMPTY);
//			
////			req.setChargeleader(chargeleader);//分管领导
////			req.setCoordinator(coordinator);//协办人
////			req.setJson(json);//分解任务用
////			req.setLeadleader(leadleader);//牵头领导
////			req.setNeedbacktime(needbacktime);// 反馈时间点要求 预留
////			req.setNow(now);
////			req.setTaskid(taskid);
////			req.setTracker(tracker);//跟踪人
//		}
//
//		@Override
//		public void doAfterAllAnalysed(AnalysisContext context) {
//		}
//    	
//		public List<TkTaskDeployRequest> getData() {
//			return data;
//		}
//    }
    
    public static void main(String[] args) throws Exception {
//    	//列标题
//        String[] handers = {"序号","*任务分类","*任务名称","*起止时间","*执行人","*任务类型"}; 
//        //下拉的列序号数组(序号从0开始)
//        int [] downRows = {1,5}; 
//        
//        //下拉框数据
//        List<String[]> downData = new ArrayList<String[]>();
//        String[] str1 = {"2-商机跟踪","5-智慧党建业务线","7-目标督查业务线","12-2019年业绩目标","14-重要会议","15-重点工作","16-重点项目","17-智慧党建业务线","18-目标督查业务线"};
////        String[] str2 = {"1-安徽力瀚科技有限公司","2-产品研发中心","3-软件营销部","4-产品管理部","5-项目管理部","6-软件研发部","7-质量管理部","8-技术运维部","9-创新研究院","10-营销中心","11-工程中心","12-高管团队","13-人力资源中心","14-商务部","15-财务中心","21-项目管理部","22-客户服务部","24-储运部","25-财务部","28-洛尼","30-铜陵","31-磐荣","32-技术支持部","33-党政事业部","34-政教事业部","35-公共安全事业部","42-解决方案中心","43-标案部","44-售前咨询部","46-计划经营部","48-西嘉","49-云端事业部","51-人力资源部","53-总经办","54-党群文化部","55-培训发展部","56-一组","57-二组","58-三组","59-四组","60-分销组"};
//        String[] str3 = {"普通任务","周期按年","周期按季","周期按月","周期按周"};
//        downData.add(str1);
////        downData.add(str2);
//        downData.add(str3);
//        //执行人
//        List<String> users = new ArrayList<String>();
//        users.add("1-系统管理员");
//        users.add("2-白建东");
//		createTaskDeployExcelTemplate("D://2.xls", handers, downData, downRows, 1000,users);
////      updateExcelTemplate("D://快钉批量创建任务模板.xls", 1,downRows,downData,50);
    	
//    	FileInputStream in = null;
//    	File file = new File("D:/快钉批量创建任务模板.xls");
//    	try {
//    		String name = file.getName();
//    		String tempName = name.toLowerCase();
//    		in = new FileInputStream(file);
//    		TkTaskDeployListener listener = new TkTaskDeployListener();
//    		if (tempName.endsWith("xls")) {
//    			EasyExcelUtil.readXLS(in, listener);
//    		} else if (tempName.endsWith("xlsx")) {
//    			EasyExcelUtil.readXLSX(in, listener);
//    		}
//    		List<TkTaskDeployRequest> list = listener.getData();
//		} finally {
//			if (null!=in) {
//				in.close();
//			}
//		}
    	
	}
}
