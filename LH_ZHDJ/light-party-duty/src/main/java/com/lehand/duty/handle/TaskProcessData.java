package com.lehand.duty.handle;

import com.lehand.base.common.Session;
import com.lehand.duty.dto.CommentReplyRequest;
import com.lehand.duty.dto.CommentRequest;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.pojo.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TaskProcessData {

	private Session session;
	
	private Object[] params;
	
	private TkTaskDeploy deploy;
	
	private TkTaskDeploySub deploySub;
	
	private TkMyTask myTask;
	
	private TkMyTaskLog myTaskLog;
	
	private MyTaskLogRequest myTaskLogRequest;
	
	private CommentRequest commentRequest;
	
	private TkComment tkComment;
	
	private CommentReplyRequest commentReplyRequest;
	
	private TkCommentReply tkCommentReply;
	
	private Map<String, Object> remindParams = new HashMap<String, Object>();
	
	private static Subject system = new Subject(0L, "系统管理员");
	
//	public static void setSystem(Subject system) {
//		TaskProcessData.system = system;
//	}

	public static Subject getSystem() {
		return system;
	}

	public TkCommentReply getTkCommentReply() {
		return tkCommentReply;
	}

	public void setTkCommentReply(TkCommentReply tkCommentReply) {
		this.tkCommentReply = tkCommentReply;
	}

	public CommentReplyRequest getCommentReplyRequest() {
		return commentReplyRequest;
	}

	public void setCommentReplyRequest(CommentReplyRequest commentReplyRequest) {
		this.commentReplyRequest = commentReplyRequest;
	}

	public TkComment getTkComment() {
		return tkComment;
	}

	public void setTkComment(TkComment tkComment) {
		this.tkComment = tkComment;
	}

	public CommentRequest getCommentRequest() {
		return commentRequest;
	}

	public void setCommentRequest(CommentRequest commentRequest) {
		this.commentRequest = commentRequest;
	}

	public TaskProcessData() {
	}

	public TaskProcessData(Session session, Object[] params) {
		this.session = session;
		this.params = params;
	}

	public TkTaskDeploy getDeploy() {
		return deploy;
	}

	public void setDeploy(TkTaskDeploy deploy) {
		this.deploy = deploy;
	}

	public TkMyTask getMyTask() {
		return myTask;
	}

	public void setMyTask(TkMyTask myTask) {
		this.myTask = myTask;
	}

	public TkMyTaskLog getMyTaskLog() {
		return myTaskLog;
	}

	public void setMyTaskLog(TkMyTaskLog myTaskLog) {
		this.myTaskLog = myTaskLog;
	}

	public MyTaskLogRequest getMyTaskLogRequest() {
		return myTaskLogRequest;
	}

	public void setMyTaskLogRequest(MyTaskLogRequest myTaskLogRequest) {
		this.myTaskLogRequest = myTaskLogRequest;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getParam(int index) {
		return (T) params[index];
	}
	
	public Subject getTkMyTaskLogSubject() {
		if (null==myTaskLog) {
			return null;
		}
		return new Subject(myTaskLog.getUserid(),myTaskLog.getUsername());
	}
	
	public Subject getSessionSubject() {
		if (null==session) {
			return null;
		}
		return new Subject(session.getUserid(),session.getUsername());
	}
	
	public Subject getDeploySubject() {
		if (null==deploy) {
			return null;
		}
		return new Subject(deploy.getUserid(),deploy.getUsername());
	}
	
//	public Subject getDeploySubject() {
//		List<Subject> list = new ArrayList<Subject>(1);
//		return new Subject(deploy.getUserid(),deploy.getUsername());
//	}
	
	public Subject getMyTaskSubject() {
		if (null==myTask) {
			return null;
		}
		return new Subject(myTask.getSubjectid(),myTask.getSubjectname());
	}
	
	public Set<String> getTimes(String time) {
		Set<String> set = new HashSet<String>(1);
		set.add(time);
		return set;
	}

	public TkTaskDeploySub getDeploySub() {
		return deploySub;
	}

	public void setDeploySub(TkTaskDeploySub deploySub) {
		this.deploySub = deploySub;
	}

	public Map<String, Object> getRemindParams() {
		return remindParams;
	}
	
	public void putRemindParam(String key,Object value) {
		this.remindParams.put(key, value);
	}

	public void setRemindParams(Map<String, Object> remindParams) {
		this.remindParams = remindParams;
	}
	
}
