package com.lehand.duty.dto;

import com.lehand.duty.pojo.TkAttachment;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

public class FeedDetails {

	private Long taskid;//主任务id
	private Long mytaskid;//主任务id
	private Long feedbackid;//主任务id
	private Integer feedstatus;//反馈状态
	private String starttime;//开始时间
	private String endtime;//结束时间
	private String feedtime;//反馈时间
	private String feedcontent;//反馈内容
	private String tkname;//任务名称
	private String subjectname;//执行人名称
	private Long subjectid;//执行人id
	private String uuid;//附件的uuid
	private String remindremark;//时间点对应要求
	private String auditremark;//审核备注
	private Integer auditstatus;//审核状态
	private String opttype;//签收/反馈
	private String backreason;//退回原因
	private String remindtime;//提醒时间点
	private List<TkAttachment> attachment;//附件
	
	//任务责任联系人
	private String publisher;
	//责任人联系电话
	private String tphone;
	//附件必填 1:是 0:否
	private int annex;
	//反馈负责人
	private String feedbackPerson;
	//反馈负责人电话
	private String feedbackPhone;
    //附件id
    private String file;
    //附件集合
    List<Map<String, Object>> files;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public List<Map<String, Object>> getFiles() {
        return files;
    }

    public void setFiles(List<Map<String, Object>> files) {
        this.files = files;
    }

    public Long getTaskid() {
		return taskid;
	}
	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}
	public Long getMytaskid() {
		return mytaskid;
	}
	public void setMytaskid(Long mytaskid) {
		this.mytaskid = mytaskid;
	}
	public Long getFeedbackid() {
		return feedbackid;
	}
	public void setFeedbackid(Long feedbackid) {
		this.feedbackid = feedbackid;
	}
	public Integer getFeedstatus() {
		return feedstatus;
	}
	public void setFeedstatus(Integer feedstatus) {
		this.feedstatus = feedstatus;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getFeedtime() {
		return feedtime;
	}
	public void setFeedtime(String feedtime) {
		this.feedtime = feedtime;
	}
	public String getFeedcontent() {
		return feedcontent;
	}
	public void setFeedcontent(String feedcontent) {
		this.feedcontent = feedcontent;
	}
	public String getTkname() {
		return tkname;
	}
	public void setTkname(String tkname) {
		this.tkname = tkname;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public Long getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(Long subjectid) {
		this.subjectid = subjectid;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getRemindremark() {
		return remindremark;
	}
	public void setRemindremark(String remindremark) {
		this.remindremark = remindremark;
	}
	public String getAuditremark() {
		return auditremark;
	}
	public void setAuditremark(String auditremark) {
		this.auditremark = auditremark;
	}
	public Integer getAuditstatus() {
		return auditstatus;
	}
	public void setAuditstatus(Integer auditstatus) {
		this.auditstatus = auditstatus;
	}
	public String getOpttype() {
		return opttype;
	}
	public void setOpttype(String opttype) {
		this.opttype = opttype;
	}
	public String getBackreason() {
		return backreason;
	}
	public void setBackreason(String backreason) {
		this.backreason = backreason;
	}
	public String getRemindtime() {
		return remindtime;
	}
	public void setRemindtime(String remindtime) {
		this.remindtime = remindtime;
	}
	public List<TkAttachment> getAttachment() {
		return attachment;
	}
	public void setAttachment(List<TkAttachment> attachment) {
		this.attachment = attachment;
	}
	
	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getTphone() {
		return tphone;
	}

	public void setTphone(String tphone) {
		this.tphone = tphone;
	}

	public int getAnnex() {
		return StringUtils.isEmpty(annex)?0:annex;
	}

	public void setAnnex(int annex) {
		this.annex = StringUtils.isEmpty(annex)?0:annex;;
	}
	public String getFeedbackPerson() {
		return feedbackPerson;
	}
	public void setFeedbackPerson(String feedbackPerson) {
		this.feedbackPerson = feedbackPerson;
	}
	public String getFeedbackPhone() {
		return feedbackPhone;
	}
	public void setFeedbackPhone(String feedbackPhone) {
		this.feedbackPhone = feedbackPhone;
	}
	
	
}
