package com.lehand.duty.dto;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Session;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.pipeline.Request;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *  普通任务和周期任务发布的请求接口
 * @author Zim
 *
 */
public class TkTaskDeployRequest extends Request{

	private String uuid;// 有附件时要上传
	private String tkname;// 任务名称
	private Long classid;// 任务分类ID
	private String starttime;// 起始时间 普通任务填
	private String endtime;// 终止时间
	private String tkdesc;// 任务备注/要求
	private Long srctaskid;// 源任务ID  预留
	private String needbacktime;// 反馈时间点要求 预留
	private String draftStartTime;//草稿开始时间
	private String draftEndTime;//草稿结束时间
	private String fileid;//文件id
	private Long taskid;//修改时使用 任务id
	private String draftFlag;//草稿：1，非草稿0
	private int crossFlag;//跨级1，非跨级0
	private String addOrUpdate;//新增0，修改1
	private String tabTimedata;//时间json数据
	private String feedTime;//补发提醒的反馈时间点
	private String require;//补发提醒的要求
	private String excuteusers;//执行人
	private String coordinator;//协办人
	private String leadleader;//牵头领导
	private String chargeleader;//分管领导
	private String tracker;//跟踪人
	private Session session;//任务创建人的session信息
	private String timedata;// 时间
	private String json;// 分解任务存表格数据
	private int flag;//"评判标准（1：完成值大于或等于目标值为达标，2：完成值小于或等于目标值为达标，3：完成值在目标值左右浮动范围内为达标）",
	private double downfloat;//"向下浮动值",
	private double upfloat;//"向上浮动值",
	private String unit;//"单位（没有传空）"
	private int deco;//0不分解，1定性分解，2定量分解，3执行人分解
	private String timedataresolve;// 分解对象
	private int resolveFlag;//0基本信息，1分解信息
	private int resolveaddorupdate;//执行人分解信息提交 0 新增，1修改
	private Long riskid;//风险id
	
	/** 2019 -12-12  新增*/
	//任务责任联系人
	private String publisher;
	//责任人联系电话
	private String tphone;
	//附件必填 1:是 0:否
	private int annex;
    /**2021-04-25  新需求补充资料库去附件*/
    //附件
    private String files;

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }
	public Long getRiskid() {
		return riskid;
	}

	public void setRiskid(Long riskid) {
		this.riskid = riskid;
	}

	public int getResolveaddorupdate() {
		return resolveaddorupdate;
	}

	public void setResolveaddorupdate(int resolveaddorupdate) {
		this.resolveaddorupdate = resolveaddorupdate;
	}

	public int getResolveFlag() {
		return resolveFlag;
	}

	public void setResolveFlag(int resolveFlag) {
		this.resolveFlag = resolveFlag;
	}

	public int getDeco() {
		return deco;
	}

	public void setDeco(int deco) {
		this.deco = deco;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public double getDownfloat() {
		return downfloat;
	}

	public void setDownfloat(double downfloat) {
		this.downfloat = downfloat;
	}

	public double getUpfloat() {
		return upfloat;
	}

	public void setUpfloat(double upfloat) {
		this.upfloat = upfloat;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	private Set<Subject> subjects;
	private Set<Subject> subjects2;
	private List<Subject> subjects3;
	private Set<Subject> subjects4;
	private Set<Subject> subjects1;
	
	/**
	 * [
	 *    {flag:1,data:"2018-10-09",end:""}, //1-->固定日期
	 *    {flag:2,data:"3,M,1end:"30"},    //2-->周期日期
	 * ]
	 */
	
	@SuppressWarnings("rawtypes")
	private List<Map> times;

    @SuppressWarnings("rawtypes")
	private List<Map> jsons;

	public String getFileid() {
		return fileid;
	}

	public void setFileid(String fileid) {
		this.fileid = fileid;
	}

	public String getFeedTime() {
		return feedTime;
	}

	public void setFeedTime(String feedTime) {
		this.feedTime = feedTime;
	}

	public String getRequire() {
		return require;
	}

	public void setRequire(String require) {
		this.require = require;
	}

	public String getDraftStartTime() {
		return draftStartTime;
	}

	public void setDraftStartTime(String draftStartTime) {
		this.draftStartTime = draftStartTime;
	}

	public String getDraftEndTime() {
		return draftEndTime;
	}

	public void setDraftEndTime(String draftEndTime) {
		this.draftEndTime = draftEndTime;
	}

	public String getTabTimedata() {
		return tabTimedata;
	}

	public void setTabTimedata(String tabTimedata) {
		this.tabTimedata = tabTimedata;
	}

	public String getAddOrUpdate() {
		return addOrUpdate;
	}

	public void setAddOrUpdate(String addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}

	public String getDraftFlag() {
		return draftFlag;
	}

	public void setDraftFlag(String draftFlag) {
		this.draftFlag = draftFlag;
	}

	public int getCrossFlag() {
		return crossFlag;
	}

	public void setCrossFlag(int crossFlag) {
		this.crossFlag = crossFlag;
	}

	public String getTimedataresolve() {
		return timedataresolve;
	}

	public void setTimedataresolve(String timedataresolve) {
		this.timedataresolve = timedataresolve;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
		if(!StringUtils.isEmpty(coordinator)) {
			this.subjects2 = new HashSet<Subject>(JSON.parseArray(this.coordinator, Subject.class));
		}else {
			this.subjects2 = new HashSet<Subject>();
		}
	}

	public String getLeadleader() {
		return leadleader;
	}

	public void setLeadleader(String leadleader) {
		this.leadleader = leadleader;
		if(!StringUtils.isEmpty(leadleader)) {
			this.subjects3 = new ArrayList<>(JSON.parseArray(this.leadleader, Subject.class));
		}else {
			this.subjects3 = new ArrayList<>();
		}
	}

	public String getChargeleader() {
		return chargeleader;
	}

	public void setChargeleader(String chargeleader) {
		this.chargeleader = chargeleader;
		if(!StringUtils.isEmpty(chargeleader)) {
			this.subjects4 = new HashSet<Subject>(JSON.parseArray(this.chargeleader, Subject.class));
		}else {
			this.subjects4 = new HashSet<Subject>();
		}
	}

	public String getTracker() {
		return tracker;
	}

	public void setTracker(String tracker) {
		this.tracker = tracker;
		if(!StringUtils.isEmpty(tracker)) {
			this.subjects1 = new HashSet<Subject>(JSON.parseArray(this.tracker, Subject.class));
		}else {
			this.subjects1 = new HashSet<Subject>();
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTkname() {
		return tkname;
	}

	public void setTkname(String tkname) {
		this.tkname = tkname;
	}

	public Long getClassid() {
		return classid;
	}

	public void setClassid(Long classid) {
		this.classid = classid;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		if (starttime.length()>10) {
			this.starttime = starttime;
		} else {
			this.starttime = starttime+" 00:00:00";
		}
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		if (endtime.length()>10) {
			this.endtime = endtime;
		} else {
			this.endtime = endtime+" 23:59:59";
		}
	}

	public String getTkdesc() {
		return tkdesc;
	}

	public void setTkdesc(String tkdesc) {
		this.tkdesc = tkdesc;
	}

	public Long getSrctaskid() {
		return srctaskid;
	}

	public void setSrctaskid(Long srctaskid) {
		this.srctaskid = srctaskid;
	}

	public String getNeedbacktime() {
		return needbacktime;
	}

	public void setNeedbacktime(String needbacktime) {
		this.needbacktime = needbacktime;
	}

	public String getExcuteusers() {
		return excuteusers;
	}

	public void setExcuteusers(String excuteusers) {
		this.excuteusers = excuteusers;
		this.subjects = new HashSet<Subject>(JSON.parseArray(this.excuteusers, Subject.class));
	}

	public String getTimedata() {
		return timedata;
	}

	public void setTimedata(String timedata) {
		this.timedata = timedata;
		this.times = JSON.parseArray(this.timedata, Map.class);
	}
	
	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
		this.jsons = JSON.parseArray(this.json, Map.class);
	}
	
	public Set<Subject> getSubjects() {
		return subjects;
	}
	
	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
		this.excuteusers = JSON.toJSONString(subjects);
	}

	public Set<Subject> getSubjects1() {
		return subjects1;
	}
	
	public Set<Subject> getSubjects2() {
		return subjects2;
	}
	
	public List<Subject> getSubjects3() {
		return subjects3;
	}
	
	public Set<Subject> getSubjects4() {
		return subjects4;
	}

	@SuppressWarnings("rawtypes")
	public List<Map> getTimes() {
		return times;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Map> getJsons() {
		return jsons;
	}

	@Override
	public void validate() throws LehandException {
		/*if (null == times || times.size() <= 0) {
			LehandException.throwException("任务时间节点不能为空!");
		}*/
		/*if (null == this.subjects || this.subjects.size() <= 0) {
			LehandException.throwException("任务执行人不能为空!");
		}
		if (StringUtils.isEmpty(endtime)  ) {
			LehandException.throwException("任务终止时间不能为空!");
		}
		if (StringUtils.isEmpty(tkname)  ) {
			LehandException.throwException("任务名称不能为空!");
		}
		if (StringUtils.isEmpty(classid)  ) {
			LehandException.throwException("任务类别不能为空!");
		}*/
	}

	public Long getTaskid() {
		return taskid;
	}

	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	@SuppressWarnings("rawtypes")
	public void setJsons(List<Map> jsons) {
		this.jsons = jsons;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getTphone() {
		return tphone;
	}

	public void setTphone(String tphone) {
		this.tphone = tphone;
	}

	public int getAnnex() {
		return StringUtils.isEmpty(annex)?0:annex;
	}

	public void setAnnex(int annex) {
		this.annex = StringUtils.isEmpty(annex)?0:annex;;
	}
	
	
	
}
