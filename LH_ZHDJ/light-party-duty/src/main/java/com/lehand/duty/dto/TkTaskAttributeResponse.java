package com.lehand.duty.dto;

public class TkTaskAttributeResponse {

	String style;//前台文本框类型
	String lable;//名称
	String code;//编码
	String labelStatus;//是否选中
	String placeholder;
	String stor;//是否带星
	String selectLable;
	Boolean showitem;//是否显示
	int checked;
	int seqno;//排序号
	
	
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public int getChecked() {
		return checked;
	}
	public void setChecked(int checked) {
		this.checked = checked;
	}
	public String getLabelStatus() {
		return labelStatus;
	}
	public void setLabelStatus(String labelStatus) {
		this.labelStatus = labelStatus;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getLable() {
		return lable;
	}
	public void setLable(String lable) {
		this.lable = lable;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPlaceholder() {
		return placeholder;
	}
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
	public String getStor() {
		return stor;
	}
	public void setStor(String stor) {
		this.stor = stor;
	}
	public String getSelectLable() {
		return selectLable;
	}
	public void setSelectLable(String selectLable) {
		this.selectLable = selectLable;
	}
	public Boolean getShowitem() {
		return showitem;
	}
	public void setShowitem(Boolean showitem) {
		this.showitem = showitem;
	}
	
	
}
