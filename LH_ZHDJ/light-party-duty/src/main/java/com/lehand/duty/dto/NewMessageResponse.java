package com.lehand.duty.dto;

public class NewMessageResponse {

	int module;//模块类型 取值见枚举类
	Long mid;//对应模块id
	String remind;//提醒时间
	String optusername;//操作人名称
	Long optuserid;//操作人id
	int isread;//是否已读，0未读，1已读
	String mesg;//消息内容
	int remarks1;//状态(0:删除,1:正常)
	Long mytaskid;//我的任务id
	int period;//是否是周期任务 0不是，1是
	int status;//状态
	Long noteid;//消息id
	Long userid;//通知人id
	Long taskid;//主任务id
	int exsjnum;//执行人数量
	int resolve;//是否是分解任务 0不是，1是
	int flag;//跳转页面类型 0跳到我的任务页面，1跳到已部署任务页面，2跳到督办跟踪页面
	String rulecode;//消息类型
	int mystatus;

	public int getMystatus() {
		return mystatus;
	}
	public void setMystatus(int mystatus) {
		this.mystatus = mystatus;
	}
	public String getRulecode() {
		return rulecode;
	}
	public void setRulecode(String rulecode) {
		this.rulecode = rulecode;
	}
	public int getExsjnum() {
		return exsjnum;
	}
	public void setExsjnum(int exsjnum) {
		this.exsjnum = exsjnum;
	}
	public int getResolve() {
		return resolve;
	}
	public void setResolve(int resolve) {
		this.resolve = resolve;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public Long getTaskid() {
		return taskid;
	}
	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}
	public Long getNoteid() {
		return noteid;
	}
	public void setNoteid(Long noteid) {
		this.noteid = noteid;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public Long getOptuserid() {
		return optuserid;
	}
	public void setOptuserid(Long optuserid) {
		this.optuserid = optuserid;
	}
	public int getModule() {
		return module;
	}
	public void setModule(int module) {
		this.module = module;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getRemind() {
		return remind;
	}
	public void setRemind(String remind) {
		this.remind = remind;
	}
	public String getOptusername() {
		return optusername;
	}
	public void setOptusername(String optusername) {
		this.optusername = optusername;
	}
	public int getIsread() {
		return isread;
	}
	public void setIsread(int isread) {
		this.isread = isread;
	}
	public String getMesg() {
		return mesg;
	}
	public void setMesg(String mesg) {
		this.mesg = mesg;
	}
	public int getRemarks1() {
		return remarks1;
	}
	public void setRemarks1(int remarks1) {
		this.remarks1 = remarks1;
	}
	public Long getMytaskid() {
		return mytaskid;
	}
	public void setMytaskid(Long mytaskid) {
		this.mytaskid = mytaskid;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
