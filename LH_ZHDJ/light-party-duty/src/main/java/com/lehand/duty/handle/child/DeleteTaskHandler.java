package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DeleteTaskHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkTaskDeploy deploy = data.getParam(0);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
		data.setDeploySub(deploySub);
		data.setDeploy(deploy);

		//process
		int flag = data.getParam(1);
		Long taskid = deploy.getId();
		if(flag==1) {//周期任务删除该周期的任务
			//第一步：时间节点表中该周期的时间节点
			String now = data.getParam(2);
			tkTaskTimeNodeService.deleteByStarttime(deploy.getCompid(),taskid,now);
			//第二步：删除子表中该周期的的任务
			tkMyTaskDao.deleteByStarttime(deploy.getCompid(),taskid,now);
			//删除过后查询一下是否存在未签收的任务，如果存在就不改变主任务的状态
			//第三步：查询下该主任务系的子任务除了退回状态的任务还存不存在其他状态的任务，如果只是存在退回的就将主任务状态改为0
			int allnum = tkMyTaskDao.getTaskCount(deploy.getCompid(),taskid);
			int nosignnum = tkMyTaskDao.getTaskByStatus(deploy.getCompid(),taskid,2);
			int tuihuinum = tkMyTaskDao.getTaskByStatus(deploy.getCompid(),taskid,6);
			int wanchengnum = tkMyTaskDao.getTaskByStatus(deploy.getCompid(),taskid,5);
			if(allnum==tuihuinum || allnum==0) {
				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.DELETE_STATUS);
			}else {
				if(wanchengnum!=0 && (tuihuinum+wanchengnum==allnum)) {
					tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
				}
				if(nosignnum!=0 && nosignnum+tuihuinum==allnum) {
					tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.NO_SIGNED_STATUS);
				}
			}
		}else if(flag==2) {//已部署列表中退回模块删除退回的子任务
			//TODO这里的业务逻辑和后续退回里面的按钮一起讨论解决
		}else{//删除所有任务
			//注解：删除逻辑目前就全部删除后续如果有必要就放入备份表中
			//删除主表任务(不能真删除，后面批处理时还要用到任务的名称)
			tkTaskDeployDao.delete(deploy.getCompid(),taskid);
			//删除子表任务
			tkMyTaskDao.delete(deploy.getCompid(),taskid);
			if(deploy.getPeriod()==1) {//周期任务
				//删除时间节点表数据
				tkTaskTimeNodeService.deleteByTaskid(deploy.getCompid(),taskid);
			}
			//删除提醒表中数据,删除消息表中数据
			tkTaskRemindNoteService.deleteByTaskid(deploy.getCompid(),taskid, Module.DEPLOY_TASK.getValue());
			List<TkMyTask> mys = tkMyTaskDao.listByTaskidAll(deploy.getCompid(),taskid);
			if(mys!=null && mys.size()>0) {
				for(TkMyTask my : mys) {
					tkTaskRemindNoteService.deleteByTaskid(deploy.getCompid(),my.getId(),Module.MY_TASK.getValue());
					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),my.getId(),Module.MY_TASK.getValue());
				}
			}

			List<TkMyTaskLog> logs = tkMyTaskLogService.listByTaskid(deploy.getCompid(),taskid);
			if(logs!=null && logs.size()>0) {
				for(TkMyTaskLog log : logs) {
					tkTaskRemindNoteService.deleteByTaskid(deploy.getCompid(),log.getId(),Module.MY_TASK_LOG.getValue());
					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),log.getId(),Module.MY_TASK_LOG.getValue());
				}
			}
			List<TkComment> comments = tkCommentService.listByTaskid(deploy.getCompid(),taskid);
			if(comments!=null && comments.size()>0) {
				for(TkComment comment : comments) {
					tkTaskRemindNoteService.deleteByTaskid(deploy.getCompid(),comment.getId(),Module.COMMENT.getValue());
					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),comment.getId(),Module.COMMENT.getValue());
				}
			}
			List<TkCommentReply> replys = tkCommentReplyService.listByTaskid(deploy.getCompid(),taskid);
			if(replys!=null && replys.size()>0) {
				for(TkCommentReply reply : replys) {
					tkTaskRemindNoteService.deleteByTaskid(deploy.getCompid(),reply.getId(),Module.REPLY.getValue());
					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),reply.getId(),Module.REPLY.getValue());
				}
			}
			//删除反馈时间节点提醒
			generalSqlComponent.delete(SqlCode.deleteFeedTime,new Object[]{deploy.getCompid(),taskid});
			//删除审核数据
			generalSqlComponent.delete(SqlCode.deleteAuditByTaskId,new Object[]{deploy.getCompid(),taskid});
			//删除反馈表数据
			tkMyTaskLogService.deleteByTaskid(deploy.getCompid(),taskid);
			//删除评论表数据
			tkCommentService.deleteByTaskid(deploy.getCompid(),taskid);
			//删除回复表数据
			tkCommentReplyService.deleteByTaskid(deploy.getCompid(),taskid);
			//删除系统动态表数据
			tkSysDynamicsDao.delete(deploy.getCompid(),taskid);
			//删除浏览历史表
			browerHisService.delete(deploy.getCompid(),taskid);
			//删除积分详情表
			tkIntegralDetailsService.deleteByTaskId(deploy.getCompid(),taskid);
		}

		//remind
		List<TkTaskSubject> subjects = tkTaskSubjectService.findSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());
		for(TkTaskSubject subject : subjects) {
			Subject sub = new Subject(subject.getSubjectid(),subject.getSubjectname());
			//提醒执行人和关注人
			remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), data.getDeploy().getId(), processEnum.getRemindRule(),DateEnum.YYYYMMDDHHMMDD.format(),
					data.getDeploySubject(), sub);
		}
		if(flag==0) {//删除所有任务
			//删除主体表数据
			tkTaskSubjectService.delete(data.getDeploy().getCompid(),data.getDeploy().getId());
		}else {//周期删除后续任务
			//判断子表中的任务是否全部被删除
			List<TkMyTask> lists = tkMyTaskDao.listByTaskidAll(data.getDeploy().getCompid(),data.getDeploy().getId());
			if(lists==null || lists.size()<=0) {
				//删除主表任务(假删除)
				tkTaskDeployDao.delete(data.getDeploy().getCompid(),data.getDeploy().getId());
				//删除主体表数据
				tkTaskSubjectService.delete(data.getDeploy().getCompid(),data.getDeploy().getId());
			}
		}
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getParam(0);
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
//		data.setDeploySub(deploySub);
//		data.setDeploy(deploy);
//	}
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		int flag = data.getParam(1);
//		TkTaskDeploy deploy = data.getDeploy();
//		Long taskid = deploy.getId();
//		if(flag==1) {//周期任务删除该周期的任务
//			//第一步：时间节点表中该周期的时间节点
//			String now = data.getParam(2);
//			tkTaskTimeNodeDao.deleteByStarttime(deploy.getCompid(),taskid,now);
//			//第二步：删除子表中该周期的的任务
//			tkMyTaskDao.deleteByStarttime(deploy.getCompid(),taskid,now);
//			//删除过后查询一下是否存在未签收的任务，如果存在就不改变主任务的状态
//			//第三步：查询下该主任务系的子任务除了退回状态的任务还存不存在其他状态的任务，如果只是存在退回的就将主任务状态改为0
//			int allnum = tkMyTaskDao.getTaskCount(deploy.getCompid(),taskid);
//			int nosignnum = tkMyTaskDao.getTaskByStatus(deploy.getCompid(),taskid,2);
//			int tuihuinum = tkMyTaskDao.getTaskByStatus(deploy.getCompid(),taskid,6);
//			int wanchengnum = tkMyTaskDao.getTaskByStatus(deploy.getCompid(),taskid,5);
//			if(allnum==tuihuinum || allnum==0) {
//				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.DELETE_STATUS);
//			}else {
//				if(wanchengnum!=0 && (tuihuinum+wanchengnum==allnum)) {
//					tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
//				}
//				if(nosignnum!=0 && nosignnum+tuihuinum==allnum) {
//					tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.NO_SIGNED_STATUS);
//				}
//			}
//		}else if(flag==2) {//已部署列表中退回模块删除退回的子任务
//			//TODO这里的业务逻辑和后续退回里面的按钮一起讨论解决
//		}else{//删除所有任务
//			//注解：删除逻辑目前就全部删除后续如果有必要就放入备份表中
//			//删除主表任务(不能真删除，后面批处理时还要用到任务的名称)
//			tkTaskDeployDao.delete(deploy.getCompid(),taskid);
//			//删除子表任务
//			tkMyTaskDao.delete(deploy.getCompid(),taskid);
//			if(deploy.getPeriod()==1) {//周期任务
//				//删除时间节点表数据
//				tkTaskTimeNodeDao.deleteByTaskid(deploy.getCompid(),taskid);
//			}
//			//删除提醒表中数据,删除消息表中数据
//			tkTaskRemindNoteDao.deleteByTaskid(deploy.getCompid(),taskid,Module.DEPLOY_TASK.getValue());
//			List<TkMyTask> mys = tkMyTaskDao.listByTaskidAll(deploy.getCompid(),taskid);
//			if(mys!=null && mys.size()>0) {
//				for(TkMyTask my : mys) {
//					tkTaskRemindNoteDao.deleteByTaskid(deploy.getCompid(),my.getId(),Module.MY_TASK.getValue());
//					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),my.getId(),Module.MY_TASK.getValue());
//				}
//			}
//			
//			List<TkMyTaskLog> logs = tkMyTaskLogDao.listByTaskid(deploy.getCompid(),taskid);
//			if(logs!=null && logs.size()>0) {
//				for(TkMyTaskLog log : logs) {
//					tkTaskRemindNoteDao.deleteByTaskid(deploy.getCompid(),log.getId(),Module.MY_TASK_LOG.getValue());
//					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),log.getId(),Module.MY_TASK_LOG.getValue());
//				}
//			}
//			List<TkComment> comments = tkCommentDao.listByTaskid(deploy.getCompid(),taskid);
//			if(comments!=null && comments.size()>0) {
//				for(TkComment comment : comments) {
//					tkTaskRemindNoteDao.deleteByTaskid(deploy.getCompid(),comment.getId(),Module.COMMENT.getValue());
//					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),comment.getId(),Module.COMMENT.getValue());
//				}
//			}
//			List<TkCommentReply> replys = tkCommentReplyDao.listByTaskid(deploy.getCompid(),taskid);
//			if(replys!=null && replys.size()>0) {
//				for(TkCommentReply reply : replys) {
//					tkTaskRemindNoteDao.deleteByTaskid(deploy.getCompid(),reply.getId(),Module.REPLY.getValue());
//					tkTaskRemindListDao.deleteByTaskid(deploy.getCompid(),reply.getId(),Module.REPLY.getValue());
//				}
//			}
//			//删除反馈表数据
//			tkMyTaskLogDao.deleteByTaskid(deploy.getCompid(),taskid);
//			//删除评论表数据 
//			tkCommentDao.deleteByTaskid(deploy.getCompid(),taskid);
//			//删除回复表数据
//			tkCommentReplyDao.deleteByTaskid(deploy.getCompid(),taskid);
//			//删除系统动态表数据
//			tkSysDynamicsDao.delete(deploy.getCompid(),taskid);
//			//删除浏览历史表
//			browerHisDao.delete(deploy.getCompid(),taskid);
//			//删除积分详情表
//			tkIntegralDetailsDao.deleteByTaskId(deploy.getCompid(),taskid);
//		}
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		int flag = data.getParam(1);
//		List<TkTaskSubject> subjects = tkTaskSubjectDao.findSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());
//		for(TkTaskSubject subject : subjects) {
//			Subject sub = new Subject(subject.getSubjectid(),subject.getSubjectname());
//			//提醒执行人和关注人
//			remindNoteComponent.register(data.getDeploy().getCompid(),module, data.getDeploy().getId(), remindRule,DateEnum.YYYYMMDDHHMMDD.format(),
//					data.getDeploySubject(), sub);
//		}
//		if(flag==0) {//删除所有任务
//			//删除主体表数据
//			tkTaskSubjectDao.delete(data.getDeploy().getCompid(),data.getDeploy().getId());
//		}else {//周期删除后续任务
//			//判断子表中的任务是否全部被删除
//			List<TkMyTask> lists = tkMyTaskDao.listByTaskidAll(data.getDeploy().getCompid(),data.getDeploy().getId());
//			if(lists==null || lists.size()<=0) {
//				//删除主表任务(假删除)
//				tkTaskDeployDao.delete(data.getDeploy().getCompid(),data.getDeploy().getId());
//				//删除主体表数据
//				tkTaskSubjectDao.delete(data.getDeploy().getCompid(),data.getDeploy().getId());
//			}
//		}
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
