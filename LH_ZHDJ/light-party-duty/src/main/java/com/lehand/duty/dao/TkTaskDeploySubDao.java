package com.lehand.duty.dao;

import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.dto.TkTaskDeployRequest;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Set;

@Repository
public class TkTaskDeploySubDao {

	@Resource
	GeneralSqlComponent generalSqlComponent;

	private static final String INSERT = "INSERT INTO tk_task_deploy_sub (compid, id, period, resolve, receivenum, tmndnum, fbacknum, commnum, replynum, overnum, newuserid, newusernm, newtime, newmesg, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid,:id,:period,:resolve,:receivenum,:tmndnum,:fbacknum,:commnum,:replynum,:overnum,:newuserid,:newusernm,:newtime,:newmesg,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)";
	private static final String GET = "select * from tk_task_deploy_sub where compid=? and id=?";
	private static final String UPDATE = "UPDATE tk_task_deploy_sub SET  period=:period,resolve=:resolve,receivenum=:receivenum,tmndnum=:tmndnum,fbacknum=:fbacknum,commnum=:commnum,replynum=:replynum,overnum=:overnum,newuserid=:newuserid,newusernm=:newusernm,newtime=:newtime,newmesg=:newmesg,remarks1=:remarks1,remarks2=:remarks2,remarks3=:remarks3,remarks4=:remarks4,remarks5=:remarks5,remarks6=:remarks6 WHERE compid=:compid and id=:id";
	
	public int update(TkTaskDeploySub info) {
//		return super.updateByBean(UPDATE, info);
		return generalSqlComponent.getDbComponent().update(UPDATE, info);
	}
	
	public TkTaskDeploySub get(Long compid,Long id) {
//		return super.get(GET, TkTaskDeploySub.class, compid,id);
		return generalSqlComponent.getDbComponent().getBean(GET, TkTaskDeploySub.class, new Object[]{compid,id});
	}
	
	public int insert(TkTaskDeploySub info) throws LehandException {
//		return super.insert(INSERT, info);
		return generalSqlComponent.getDbComponent().insert(INSERT, info);
	}
	
	public void insert(String newmsg, Session session, TkTaskDeployRequest request, Set<String> times, TkTaskDeploy deploy)
			throws LehandException {
		TkTaskDeploySub info = new TkTaskDeploySub();
		info.setCompid(session.getCompid());
		info.setId(deploy.getId());//任务ID(自增长主键)
		info.setPeriod(deploy.getPeriod());//是周期任务?(0:不是,1:是)
		info.setResolve(deploy.getResolve());//是分解任务?(0:不是,1:是)
		info.setReceivenum(0);//签收的数量
		info.setTmndnum(times.size());//时间节点的数量(普通任务时为提醒时间数量，周期时为计算后的时间数量)
		info.setFbacknum(0);//反馈数量
		info.setCommnum(0);//评论数量
		info.setReplynum(0);//回复数量
		info.setOvernum(0);//逾期数量
		info.setNewuserid(session.getUserid());//最新操作人ID
		info.setNewusernm(session.getUsername());//最新操作人姓名
		info.setNewtime(request.getNow());//最新操作时间
		info.setNewmesg(newmsg);//最新操作内容
		info.setRemarks1(0);//备注1
		info.setRemarks2(0);//备注2
		info.setRemarks3(StringConstant.EMPTY);//备注3
		info.setRemarks4(StringConstant.EMPTY);//备注3
		info.setRemarks5(StringConstant.EMPTY);//备注3
		info.setRemarks6(StringConstant.EMPTY);//备注3
		insert(info);
	}

	/**
	 * 当创建人将该任务的已签收的执行人删除时就需要调用该方法
	 * @param taskid
	 */
	public void updateNum(Long compid,Long taskid) {
//		super.update("update tk_task_deploy_sub set receivenum = receivenum-1 where compid=? and id = ?", compid,taskid);
		generalSqlComponent.getDbComponent().update("update tk_task_deploy_sub set receivenum = receivenum-1 where compid=? and id = ?", new Object[]{compid,taskid});
	}

	/**
	 * 当创建人将该任务的已签收的执行人删除时就需要调用该方法
	 * @param taskid
	 */
	public void deleteByTaskId(Long compid,Long taskid) {
//		super.delete("delete from tk_task_deploy_sub  where compid=? and id = ?", compid,taskid);
		generalSqlComponent.getDbComponent().delete("delete from tk_task_deploy_sub  where compid=? and id = ?", new Object[]{compid,taskid});
	}

}
