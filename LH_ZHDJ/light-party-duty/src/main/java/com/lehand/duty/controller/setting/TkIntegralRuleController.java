package com.lehand.duty.controller.setting;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkIntegralRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Message;
import com.lehand.base.exception.LehandException;
import com.lehand.duty.controller.DutyBaseController;

@Api(value = "规则设置", tags = { "规则设置" })
@RestController
@RequestMapping("/duty/listRule")
public class TkIntegralRuleController extends DutyBaseController {
	
	private static Logger logger = LogManager.getLogger(TkIntegralRuleController.class);
	@Resource
	TkIntegralRuleService tkIntegralRuleService;

	/**
	 * 查询积分规则
	 * @author pantao
	 * @date 2018年11月5日下午5:39:38
	 * 
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "查询积分规则", notes = "查询积分规则", httpMethod = "POST")
	@RequestMapping("/listByStatus")
	public Message listByStatus(){
		int status = 1;
		Message message = new Message();
		try {
			List<Map<String, Object>> result = tkIntegralRuleService.listByStatus(getSession().getCompid(),status);
			message.setData(result);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	
	/**
	 * 更新积分规则
	 * @author pantao
	 * @date 2018年11月5日下午5:39:13
	 * 
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@OpenApi(optflag=0)
	@ApiOperation(value = "更新积分规则", notes = "更新积分规则", httpMethod = "POST")
	@RequestMapping("/update")
	public Message update(String list){
		List<Map> listMap = JSON.parseArray(list,Map.class);
		Message message = new Message();
		if(list.isEmpty()) {
			message.setMessage("参数不能为空！");
			return message;
		}
		try {
			tkIntegralRuleService.update(getSession().getCompid(),listMap);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
}
