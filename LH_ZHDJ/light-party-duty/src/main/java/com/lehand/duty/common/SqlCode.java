package com.lehand.duty.common;


public class SqlCode {

    public static String getPrintFile = "getPrintFile";

    public static class VIEW_USER_ACCOUNT {
		public static final String getUserByAccount = "getUserByAccount";
	}

	/**UPDATE tk_task_feed_back_audit SET createtime = ? WHERE compid=? and id = ?*/
	public static final String modiTTFBAByCreatetime = "modiTTFBAByCreatetime";

	//===========================dl_file_business========================
	public static String delFileByBusinessId = "delFileByBusinessId";

	//===========================TK_TASK_SUBJECT=========================
	public static String delSubjectByTaskid = "delSubjectByTaskid";
	/*  SELECT * FROM tk_task_subject WHERE compid=? and taskid=? and flag= ? and remarks1 !=-1  执行人时包含已删除的
	* */
	public static String getSubjectByTaskId = "getSubjectByTaskId";
	public static String listSubject = "listSubject";


	//===========================tk_task_time_node=======================
	public static String updateNodeBytaskId = "updateNodeBytaskId";

	//===========================tk_my_task ===============================
	public static String updateTaskNodeBytaskId = "updateTaskNodeBytaskId";

	public static String getUserIdByMytaskIds = "getUserIdByMytaskIds";


	/**
	 * select d.tkname,d.remarks6,t.id from tk_my_task t inner join tk_task_deploy d on t.taskid = d.id where t.compid = ? and t.`status` =? and t.subjectid = ? and  d.remarks2 =0 and d.tkname like '%${PARAMS.tkname}%'
	 */
	public static String listMyTask = "listMyTask";


	//===========================tk_my_task_log=======================================================================
	//select * from tk_my_task_log  where compid = ? and taskid = ? and userid= ? and opttype = 'SIGN_IN';
	public static String getSignLog = "getSignLog";
	//update tk_my_task_log set opttime = ? where compid = ? and  id = ?
	public static String updateLogTimeByid = "updateLogTimeByid";

	//select GROUP_CONCAT(a.id SEPARATOR ',')  from (select * from tk_my_task_log l where l.compid = ? and l.updatetime = ? and l.mytaskid != ?)a
	public static String getLogIds = "getLogIds";
	//update tk_my_task_log set remarks4 = ? where compid = ? and id = ?
	public static String updateLogRelate = "updateLogRelate";

	//update tk_my_task_log set remark4 = :ids,feedbackPerson = :feedbackPerson,feedbackPhone = :feedbackPhone where compid = :compid and id = :id
	public static String updateLogRelate2 = "updateLogRelate2";

	//select * from tk_my_task_log l where l.compid = ? and l.updatetime = ? and l.mytaskid = ?
	public static String getLogBytime = "getLogBytime";

	/**SELECT * FROM tk_my_task_log where compid=? and mytaskid!=? and remarks4=? and opttime=?*/
	public static String getTMTLByMytaskidRemarks4Opt = "getTMTLByMytaskidRemarks4Opt";

	/**SELECT * FROM tk_my_task_log where compid=? and id=?*/
	public static String getTMTLByid = "getTMTLByid";

	/*	select l.* from tk_task_feed_back_audit a inner join  tk_my_task_log l on a.feedbackid = l.id
	where a.taskid = ? and a.compid = ? and a.`status` = 1 and  l.opttype= 'FEED_BACK' ORDER BY l.opttime desc limit 1
	 */
	public static String getlatestFeed = "getlatestFeed";
	/*
	select l.* from tk_task_feed_back_audit a inner join  tk_my_task_log l on a.feedbackid = l.id
	where l.taskid = ? and l.compid = ? and  l.userid = ? and a.`status` = 1 and  l.opttype= 'FEED_BACK' ORDER BY l.opttime desc limit 1
	*/
	public static String getlatestFeedNotAudit = "getlatestFeedNotAudit";

	//'select d.taskid,d.remarks4,d.remarks2,count(*) c from (select  l.taskid,l.remarks4,l.remarks2,l.userid from tk_my_task_log l where l.opttype = 2 and l.compid = :comid and l.taskid in ('+PARAMS.taskids+') group by l.taskid,l.remarks4,l.remarks2,l.userid) d GROUP BY d.taskid,d.remarks4,d.remarks2'
	public static String listRemindCount = "listRemindCount";

	/*select  l.userid ,count(*) c from tk_my_task_log l where l.opttype = 2 and l.compid = ${PARAMS.compid}
	and l.taskid = ${PARAMS.taskid} and l.remarks2 in ($PARAMS.sta) group by l.userid   根据userid 和状态查询反馈次数*/
	public static String listFeedCountByUserid = "listFeedCountByUserid";
	/**根据反馈节点和任务id，查询反馈次数
	 * select  count(DISTINCT l.userid) c from tk_my_task_log l where l.opttype = 'FEED_BACK' and l.compid = ? and l.taskid = ? and l.remarks4 = ?
	 */
	public static String getFeedcountByTime = "getFeedcountByTime";

	/**根据反馈节点和任务id，查询有审核记录的反馈数
	 *  select  count(*) c from tk_my_task_log l inner join  tk_task_feed_back_audit a  on l.id = a.feedbackid
     *  where l.opttype = 'FEED_BACK' and l.compid = ? and l.taskid = ? and l.remarks4 = ? and a.`status`!=0
	 */
	public static String getAuditCountByTime = "getAuditCountByTime";

	/*select r.subjectid userid , count(*) from
	(select s.subjectid,s.subjectname ,f.feedtime,l.id from tk_task_subject s
	inner join (select taskid,feedtime,compid  from tk_task_feed_time  group by taskid,feedtime,compid)f on s.taskid = f.taskid
	left join tk_my_task_log l on s.subjectid = l.userid and s.taskid = l.taskid and l.remarks4 = f.feedtime
	where  s.compid = ? and s.flag = 0 and f.compid= ? and s.taskid = ? and f.taskid = ? and l.id is null and f.feedtime <?)r group by r.subjectid
	查询某个任务，每个人在已经过期的时间点范围内的，上报次数为0的次数，即每个人逾期未报的总次数
	*/
	public static String listOverCountByUserid = "listOverCountByUserid";

	/*
	*  select count(*) from tk_my_task_log l where l.compid = ? and l.taskid = 401 and l.userid = ? and l.opttype = 'FEED_BACK'
	*  查询某个人某个任务的反馈次数，判断是否反馈
	* */
	public static String getMyFeedCount = "getMyFeedCount";

	//update tk_my_task_log set remarks4 = ?, remarks2=1 where compid = ? and taskid = ? and remarks4 = ? and opttype = 'FEED_BACK'
	public static String updateEndTime = "updateEndTime";

	/*
	*  select count(*) from tk_my_task_log l where l.compid = ? and l.taskid = ?  and l.opttype = 2
	*  查询某个任务的反馈次数
	* */
	public static String getallFeedCount = "getallFeedCount";
	/**
	 * select d.tkname ,l.taskid ,l.mytaskid id ,l.remarks4 feedTime ,l.id feedid ,f.requirement 'require'
	 * from tk_my_task_log l inner join tk_task_deploy d on l.taskid = d.id left join tk_task_feed_time f on
	 * l.taskid = f.taskid and l.remarks4  = f.feedtime where l.compid = ${PARAMS.compid} and l.id in (${PARAMS.ids})
	 * GROUP BY d.tkname ,l.taskid ,l.mytaskid ,l.remarks4 ,f.requirement,l.id
	 */
	public static String listRelatedTask = "listRelatedTask";

	//============================tk_task_feed_time=========================
	public static String insertFeedTime = "insertFeedTime";
	/*select GROUP_CONCAT(a.id SEPARATOR ',') from
	(select t.id from tk_task_feed_time t where t.compid = ? and t.remindtimes > ? and t.remarks2 = 0 GROUP BY t.id )a*/
	public static String listTimeIds = "listTimeIds";
	/*update tk_task_feed_time set remarks2 = 2 where id in (${PARAMS.ids})*/

	public static String updateTimeStatus = "updateTimeStatus";

/*
	update tk_feed_task_time set requirement = ? where compid = ? and taskid = ? and feedtime = ?
*/
	public static String updateRequire = "updateRequire";



	// ==========================pw_auth_other_map==========================
	public static String delPAOTMByRoleid = "delPAOTMByRoleid";

	public static String delPAOTMBySubject = "delPAOTMBySubject";

	public static String addPAOTM = "addPAOTM";

	public static String listPAOTMUser = "listPAOTMUser";

	public static String listPAOTMUserAndRole = "listPAOTMUserAndRole";

	public static String listPAOTM = "listPAOTM";

	public static String listPAOTMByPid = "listPAOTMByPid";

	public static String listPAOTMByPidList = "listPAOTMByPidList";

	public static String listPAOTMByIdString = "listPAOTMByIdString";

	public static String modPAOTMByRemarks3 = "modPAOTMByRemarks3";

	// ============================pw_code===================================
	public static String delPwCode = "delPwCode";

	// ============================pw_user==================================



	public static String getUsernameByid = "getUsernameByid";

	public static String getPUUserid = "getPUUserid";

	public static String listAllUsernames = "listAllUsernames";

	/**SELECT GROUP_CONCAT(sorgname) orgname FROM  urc_organization where compid=? and orgid in (SELECT orgid  FROM urc_user_org_map where compid=? and userid = ?)  and orgname like ? */
	public static String getOrgnameLike = "getOrgnameLike";

	/**SELECT GROUP_CONCAT(orgname) orgname FROM  pw_organ where compid=? and id in (SELECT organid  FROM pw_user_organ_map where compid=? and userid = ?)*/
	public static String getOrgname = "getOrgname";

	public static String delPAOMByClassid = "delPAOMByClassid";

	// ============================pw_auth_other_map==================================
	public static String listPAOMBySubjectidAndRemarks3 = "listPAOMBySubjectidAndRemarks3";

	public static String getPwUserByID = "getPwUserByID";

	// ================================tk_task_feed_back_audit==============================
	public static String getTTFBATaskidBySubjectid = "getTTFBATaskidBySubjectid";

	public static String addTTFBA = "addTTFBA";

	/*select * from tk_task_feed_back_audit a where a.remarks2 = 1 and  a.compid = ? and a.taskid = ? and a.remarks3 =?*/
	public static String getAuditByTaskid = "getAuditByTaskid";


	// ==================================pw_organ====================================

	/**select * from urc_organization where id=? and compid=?*/
	public static String getPOByid = "getPOByid";


    /**select * from urc_organization where remarks3=? and compid=?*/
	public static String getPOByRemarks3 = "getPOByRemarks3";

	/**SELECT group_concat(orgid) ids FROM urc_organization WHERE porgid = (select orgid from  urc_organization where remarks3=?)*/
	public static String getPOIDByRemarks3 = "getPOIDByRemarks3";

	/**INSERT INTO dl_file_business(`fileid`, `businessid`, `businessname`, `compid`) VALUES (?, ?, ?, ?)*/
	public static String addDlFileBusiness = "addDlFileBusiness";


	/**select fileid from dl_file_business where compid = ? and businessid = ? and businessname = ?*/
	public static String listFileBussinessBytaskid = "listFileBussinessBytaskid";


	/**update dl_file_business set businessid = ? where compid = ? and fileid = ? */
	public static String updateFileBusiness = "updateFileBusiness";

	public static String getSuperiors = "getSuperiors";

	/**
	 * UPDATE tk_task_feed_back_audit SET  status = ?, remark = ?,opttime = ? WHERE id = ? and compid=?
	 */
	public static String modiTTFBAByid = "modiTTFBAByid";

	public static String getTTFBAByid = "getTTFBAByid";

	/**select count(*) from pw_organ where compid = ? and pid = ?*/
	public static String getChildCount = "getChildCount";

	// ==================================tk_task_deploy====================================
	/**select id taskid,srctaskid pid,tkname name,userid,username from tk_task_deploy where compid=? and srctaskid=?*/
	public static String listTTDBySons = "listTTDBySons";

	/**select count(*) from tk_task_deploy where a.status!=0 and a.status!=1 and  compid=? and srctaskid=?*/
	public static String getTTDBySrctaskid = "getTTDBySrctaskid";

	/**
	 * update tk_task_deploy set srctaskid = ' ' where compid = ? and userid = ? and srctaskid = ?
	 */
	public static String updateSrcTaskId = "updateSrcTaskId";

	/**select a.id taskid,a.srctaskid pid,a.tkname name,a.userid,a.username,b.remarks6 from tk_task_deploy a left join pw_organ b on a.userid= b.remarks3 and a.compid=b.compid where a.status!=0 and a.status!=1 and  a.compid=? and a.id=?*/
	public static String listTTDByid = "listTTDByid";

	public static String listDraftTask = "listDraftTask";
	/**select * from (SELECT a.id taskid,a.tkname,b.clname,d.mytaskid,d.id feedbackid,d.userid,d.username,
		d.opttime,c.status,c.id auditid,0 flag,d.remarks2 feedstatus,d.remarks4 remindtime,a.newbacktime
		FROM tk_task_deploy a left join tk_task_class b on a.classid=b.id left join tk_my_task_log d on a.id=d.taskid
		left join tk_task_feed_back_audit c on d.id=c.feedbackid union all SELECT a.id taskid,a.tkname,b.clname,
		d.id mytaskid,null feedbackid,d.subjectid userid,d.subjectname username,d.newtime opttime,c.status,c.id auditid,1 flag,
		0 feedstatus,null remindtime,a.newbacktime  FROM tk_task_deploy a left join tk_task_class b on a.classid=b.id
		left join tk_my_task d on a.id=d.taskid left join tk_task_feed_back_audit c on d.id=c.mytaskid) z where 1=1 */
	public static String listPageTTD = "listPageTTD";

	public static String listPageTTDCount = "listPageTTDCount";


	/**select * from tk_task_deploy where compid=? and id=?*/
	public static String getTTDByid = "getTTDByid";


	/**select exsjnum from tk_task_deploy where compid=? and id=?*/
	public static String getTTDByiDexsjnum = "getTTDByiDexsjnum";

	public static String getRemindByTaskId = "getRemindByTaskId";

	/**update tk_task_deploy d left join tk_my_task m on d.id = m.taskid set d.remarks1 = -1 ,m.status = 0
	where d.compid = ${PARAMS.compid} and m.compid = ${PARAMS.compid}  and d.id =${PARAMS.taskid}
	and m.subjectid in (${PARAMS.subid})  修改某个任务的状态为删除，特定人员的我的任务的状态为删除
	*/
	public static String updateTaskStatus = "updateTaskStatus";


	/***/
	public static String modiTTDNewbacktime = "modiTTDNewbacktime";

	/**
	 * UPDATE tk_task_deploy SET exsjnum=exsjnum-1 WHERE compid=? and id=?
	 */
	public static String updateExsjnum = "updateExsjnum";

	/**
	 * select exsjnum,period from tk_task_deploy where compid=? and id = ?
	 */
	public static String getTask = "getTask";

	/**
	 * UPDATE tk_task_deploy SET status=? , updatetime =? WHERE compid=? and id=? and status != 50
	 */
	public static String updateExsjnums = "updateExsjnums";

	// ==================================tk_my_task_log====================================


	/**UPDATE tk_my_task_log SET updatetime=? WHERE compid=? and id=?*/
	public static String modiTMTLByOpttime = "modiTMTLByOpttime";

	/**UPDATE tk_my_task_log SET updatetime=? WHERE compid=? and id!=? and updatetime = ?*/
	public static String modiOtherByOpttime = "modiOtherByOpttime";

	/**SELECT * FROM tk_my_task_log where compid=? and taskid=? and userid=? and remarks4=? and remarks2=?*/
	public static String listTMTLCount = "listTMTLCount";


	/**UPDATE tk_my_task_log SET  remarks2 = ?, remarks4 = ? WHERE compid=? and id = ?*/
	public static String modiTMTLByid = "modiTMTLByid";


	public static String addTMTL = "addTMTL";


	/**
	 * SELECT a.taskid,a.mytaskid,a.id feedbackid,a.remarks2 feedstatus,b.starttime,b.endtime,a.opttime feedtime,
	 *  a.feedbackPerson  ,a.feedbackPhone ,b.annex,
	 * a.mesg feedcontent,b.tkname,a.userid subjectid,a.username subjectname,
	 *	a.fileuuid uuid,c.remarks6 remindremark,d.status auditstatus,d.remark auditremark,a.opttype  FROM tk_my_task_log a left join tk_task_deploy b on a.taskid=b.id left join tk_task_remind_note c on a.remarks4=c.remindtime
	 *	left join tk_task_feed_back_audit d on a.id=d.feedbackid where a.compid=? and a.id=? and b.compid=? and c.compid=? and d.compid=?
	 */
	public static String getFeedDetails = "getFeedDetails";


	//SELECT a.taskid,a.mytaskid,a.id feedbackid,a.remarks2 feedstatus,b.starttime,b.endtime,
	// a.opttime feedtime,a.mesg feedcontent,b.tkname,a.userid subjectid,a.username subjectname,
	// a.fileuuid uuid,d.status auditstatus,d.remark auditremark,a.opttype,c.remarks3 backreason
	// FROM tk_my_task_log a left join tk_my_task c on a.mytaskid=c.id left join tk_task_deploy b on a.taskid=b.id
	// left join tk_task_feed_back_audit d on a.id=d.feedbackid where a.compid=? and d.id=? and b.compid=? and d.compid=?

	public static String getFeedDetailsTask = "getFeedDetailsTask";


	/**
	 * SELECT a.taskid,a.mytaskid,a.id feedbackid,a.remarks2 feedstatus,b.starttime,b.endtime,
	 * a.opttime feedtime,a.mesg feedcontent,b.tkname,a.userid subjectid,a.username subjectname,
	 * a.fileuuid uuid,c.remarks6 remindremark,d.status auditstatus,d.remark auditremark,a.opttype
	 * FROM tk_my_task_log a  left join tk_task_deploy b on a.taskid=b.id  left join tk_task_remind_note c
	 * on a.remarks4=c.remindtime left join tk_task_feed_back_audit d on a.id=d.feedbackid where
	 * a.id in(SELECT id FROM tk_my_task_log where compid=? and mytaskid=?) and a.compid=? and b.compid=?
	 * and c.compid=? and d.compid=?
	 */

	public static String listTMTLByMytaskid = "listTMTLByMytaskid";

	public static String listTMTLByMytaskidNoPage = "listTMTLByMytaskidNoPage";

	public static String listRemindtimeNoPage = "listRemindtimeNoPage";


	/*SELECT a.taskid,a.mytaskid,a.id feedbackid,a.remarks2 feedstatus,b.starttime,b.endtime,a.opttime
	feedtime,a.mesg feedcontent,b.tkname,a.userid subjectid,a.username subjectname,a.fileuuid uuid,d.status
	auditstatus,d.remark auditremark,a.opttype,c.remarks3 backreason FROM tk_my_task_log a left join tk_my_task c on a.mytaskid=c.id
	left join tk_task_deploy b on a.taskid=b.id left join tk_task_feed_back_audit d on a.id=d.feedbackid where a.compid=? and c.taskid=? and
	c.subjectid = ? and b.compid=? and d.compid=?*/


	public static String getMydetail = "getMydetail";


	/**SELECT remarks2 feedstatus FROM tk_my_task_log where taskid=:taskid and remarks4=:remindtime  and userid=:userid*/
	public static String listTMTLByremarks4 = "listTMTLByremarks4";

	/**SELECT l.remarks2 feedstatus FROM tk_my_task_log l left join tk_task_subject s on l.taskid = s.taskid and l.userid = s.subjectid where l.taskid=:taskid and l.remarks4=:remindtime and s.remarks2 !=-1*/
	public static String listLogStatusNotDel = "listLogStatusNotDel";


	/**select a.mesg,a.opttime from tk_my_task_log a left join tk_task_feed_back_audit b on a.id=b.feedbackid
       where a.compid=? and b.compid=? and a.taskid=? and a.opttype='FEED_BACK' and b.status=1 order by a.opttime desc limit 0,1*/
	public static String getTMTLMesgAndOpttime = "getTMTLMesgAndOpttime";


	/**select a.tkname,a.id,a.classid,b.clname from tk_task_deploy a left join tk_task_class b on a.classid = b.id where a.compid=? and b.compid=? and a.id in (select taskid from tk_task_subject where compid=? and subjectid=? and flag!=0)*/
	public static String listRelatedToMe = "listRelatedToMe";


	public static String listRelatedToMeCount = "listRelatedToMeCount";

	//update tk_task_deploy set diffdata = ? ,remarks6 = ? where compid = ? and id = ?
	public static String updateTimeDataByTaskId = "updateTimeDataByTaskId";


	/**DELETE FROM tk_my_task_log WHERE compid=? and id = ?*/
	public static String delTMTLByid = "delTMTLByid";

	// ==================================tk_task_remind_note====================================
	/**
	 *  SELECT feedtime remindtime FROM tk_task_feed_time where compid=? and taskid=?  group by feedtime
	 */
	public static String listRemindtime = "listRemindtime";


	/** SELECT feedtime remindtime FROM tk_task_feed_time where compid=? and taskid=? GROUP BY feedtime desc*/
	/**SELECT feedtime remindtime,IF((SELECT remarks4 FROM tk_my_task_log where compid=? and taskid = ? and userid =? and opttype='FEED_BACK' ORDER BY opttime desc limit 1)=feedtime,1,0) flag  FROM tk_task_feed_time where compid=? and taskid=? GROUP BY flag desc, feedtime desc*/
	public static String listRemindtimePage = "listRemindtimePage";


	/**
	 *  SELECT remarks4 remindtime FROM tk_my_task_log where compid=? and taskid=? and userid=? and opttype='FEED_BACK' ORDER BY opttime desc limit 0,1
	 */
	public static String listnewRemindtime = "listnewRemindtime";

	/**
	 * 	 select t.feedtime from tk_task_feed_time t where t.compid = ? and t.taskid = ? group by t.feedtime order by t.feedtime asc
	 */
	public static String listFeedtime = "listFeedtime";
	/*select t.* from tk_task_feed_time t inner join tk_task_deploy d on t.taskid = d.id
	where  t.compid = ? and t.remindtimes < ? and t.remarks2 = 0 and d.`status` in (30,40)
	列出需要发送的任务提醒
	**/
	public static String listRemindFeedTime = "listRemindFeedTime";

	/*select taskid ,subjectid ,subjectname  from tk_task_subject  where compid = ? and taskid  in
	(select GROUP_CONCAT(t.taskid SEPARATOR ',') from (select taskid from tk_task_feed_time where compid = ?
	and remindtimes > ? and remarks2 = 0 group by taskid)t)
	and flag=0 and remarks1!=-1 group by taskid ,subjectid ,subjectname
	列出需要提醒反馈的执行人
	*/
	public static String listRemindSubject = "listRemindSubject";
	//select t.compid from tk_task_feed_time t group by t.compid
	public static String listCompid = "listCompid";
	//delete from tk_task_feed_time where compid = ? and taskid = ?
	public static String deleteFeedTime = "deleteFeedTime";



	// ==================================tk_attachment====================================
	/**
	 * SELECT * FROM tk_attachment where remarks3=? and compid=?
	 */
	public static String listTkAttachment = "listTkAttachment";

	/**DELETE FROM tk_attachment WHERE compid=? and remarks4=?*/
	public static String delAttachment = "delAttachment";

	public static String addTkAttachment = "addTkAttachment";

	// ==================================tk_my_task====================================

	/**UPDATE tk_my_task SET  status = 3, remarks6 = '' WHERE id = ?*/
	public static String modiTMTByStatusAndRemarks6 = "modiTMTByStatusAndRemarks6";

	/**UPDATE tk_my_task SET  newtime = ? WHERE id = ? and compid=?*/
	public static String modiTMTNewtime = "modiTMTNewtime";

	/**
	 * UPDATE tk_my_task SET status=?,newtime = ? WHERE compid=? and id=? and status!=?
	 */
	public static String updateTkMyTaskStatus = "updateTkMyTaskStatus";

	/**
	 * select * from tk_my_task where compid=? and id=?
	 */
	public static String getSubjectidByMytaskid = "getSubjectidByMytaskid";

	// ==================================tk_task_subject====================================

	/**
	 * delete from tk_task_subject WHERE compid=? and taskid =? and flag = 0 and sjtype = 0 and subjectid = ?
	 */
	public static String delSubject = "delSubject";
	/**
	 * update tk_task_subject set remarks1 = -1 WHERE compid=? and taskid =? and flag = 0 and sjtype = 0 and subjectid = ?
	 */
	public static String updateSubject = "updateSubject";
	/**
	 * SELECT a.subjectid,b.id organid,a.subjectname,b.pid FROM tk_task_subject a left join pw_organ b on a.subjectid=b.remarks3 where a.taskid=? and a.compid=? order by pid asc
	 */
	public static String listTreeSubject = "listTreeSubject";
	/**
	 * SELECT a.subjectid,b.orgid organid,a.subjectname,b.porgid FROM tk_task_subject a left join urc_organization b
	 * on a.subjectid=b.remarks3 where a.taskid=? and a.compid=? order by b.porgid asc
	 */
	public static String listTreeSubjectNew = "listTreeSubjectNew";

	/**SELECT group_concat(subjectid) userids FROM tk_task_subject where taskid=? and flag=0*/
	public static String getTTSUseridByTaskid = "getTTSUseridByTaskid";

	// ==================================tk_task_deploy_sub====================================
	/**
	 * select * from tk_task_deploy_sub where compid=? and id=?
	 */
	public static String getTaskSub = "getTaskSub";

	//==================================tk_task_feed_back_audit====================================

	/**SELECT count(*) FROM tk_task_feed_back_audit where compid=? and feedbackid=? and status!=0*/
	public static String getTTFBABycount = "getTTFBABycount";


	/**SELECT count(*) FROM tk_task_feed_back_audit where compid=? and feedbackid=? and status=0*/
	public static String getTTFBABycount2 = "getTTFBABycount2";



	/**
	 * SELECT a.subjectId,a.status,a.remark,a.feedbackid,b.username,a.createtime FROM tk_task_feed_back_audit a left join pw_user b on a.subjectId=b.id where a.compid=? and a.feedbackid=?
	 */
	public static String listTTFBAByFeedbackid = "listTTFBAByFeedbackid";


	/**UPDATE tk_task_feed_back_audit SET createtime=?,status = ?, remark = ? WHERE id =?*/
	public static String modiTTFBABystatus = "modiTTFBABystatus";


	//delete from tk_task_feed_back_audit  where compid = ? and taskid = ?
	public static String deleteAuditByTaskId = "deleteAuditByTaskId";


	/**DELETE FROM tk_task_feed_back_audit WHERE compid=? and feedbackid = ?*/
	public static String deleteAuditByFeedbackid = "deleteAuditByFeedbackid" ;


	/**
	 * SELECT * FROM tk_task_feed_back_audit where  feedbackid=? order by createtime desc limit 0,1
	 */
	public static String getTTFBAByNewone = "getTTFBAByNewone";

	/**
	 * SELECT * FROM tk_task_feed_back_audit where subjectId=? and feedbackid=? order by createtime desc limit 0,1
	 */
	public static String getTTFBAByByUser = "getTTFBAByByUser";


	//==================================tk_task_feed_time====================================
	public static String addTTFT = "addTTFT";

	/**DELETE FROM tk_task_feed_time WHERE taskid=? and remindtimes>?*/
	public static String delTTFTByTaskidAndRemindtimes ="delTTFTByTaskidAndRemindtimes";

	/**SELECT * FROM tk_task_feed_time where compid=? and taskid=? and feedtime=? limit 0,1*/
	public static String getFeedRequirement = "getFeedRequirement";

	//==================================tk_task_remind_list====================================
	public static String addTTRLByTuihui = "addTTRLByTuihui";

   //==================================dl_file_business====================================
	public static String getDFBByid = "getDFBByid";

	public static String deleteTkFileBusiness = "deleteTkFileBusiness";

	/**select noteid, mid,module,remind,optuserid,optusername,isread,mesg,
	 * remarks1,rulecode from tk_task_remind_list where compid=:compid and userid =:userid
	 * and remarks1 = 1 and module !=0 and now() > remind  order by remind desc
	 * */
	public static String listMesg = "listMesg";

	/**UPDATE tk_task_deploy_sub SET  receivenum =receivenum+1 where id=?*/
	public static String modiTTDSReceivenum = "modiTTDSReceivenum";

	public static String getTKSUserid = "getTKSUserid";

	/**SELECT * FROM tk_attachment where remarks3 = ?*/
	public static String listAttachments = "listAttachments";

	/**
	 * select count(*) from pw_user_organ_map where compid=? and organid  = ?
	 */
	public static String getPUOMBynum = "getPUOMBynum";

	/**
	 * SELECT a.taskid,a.mytaskid,a.id feedbackid,a.remarks2 feedstatus,a.opttime feedtime,a.mesg feedcontent,
     *  a.userid subjectid,a.username subjectname,a.fileuuid uuid,a.opttype,a.remarks4 remindtime
     *  FROM tk_my_task_log a where a.compid=? and a.id = ?
	 *
	 * */
	public static String getFeedDetails2 = "getFeedDetails2";


	/**SELECT count(*) FROM dl_file_business where compid=? and fileid=? and businessid=? and businessname=?*/
	public static String getDFB = "getDFB";


	/**SELECT organid FROM pw_user_organ_map where compid=? and userid=?*/
	public static String getPUOMByUserid = "getPUOMByUserid";


	/**SELECT count(*) FROM tk_task_feed_back_audit where mytaskid=? and feedbackid=? and status=? and subjectId=?*/
	public static String getTTFBAByCount1 = "getTTFBAByCount1";



	/**select a.id taskid,a.srctaskid pid,a.tkname name,a.userid,a.username,b.remarks6 from tk_task_deploy a left join pw_organ b on a.userid= b.remarks3 and a.compid=b.compid where a.status!=0 and a.status!=1 and a.compid=? and a.srctaskid=? */
	public static String getidstring = "getidstring";


	/**select group_concat(id) id from tk_task_deploy where compid=? and srctaskid=? */
	public static String listorganstring = "listorganstring";

	/**select a.id taskid,a.srctaskid pid,a.tkname name,a.userid,a.username,b.remarks6 from tk_task_deploy a left join pw_organ b on a.userid= b.remarks3 and a.compid=b.compid where a.status!=0 and a.status!=1 and  a.compid=:compid "+" and a.srctaskid in ("+ids+")*/
	public static String getidstringlong = "getidstringlong";

	/**select group_concat(id) id from tk_task_deploy where compid=? "+" and pid in ("+ids+")*/
	public static String listorganlong = "listorganlong";



	/**select * from tk_my_task where compid=? and id=?*/
	public static String getTaskidBymyid = "getTaskidBymyid";


	/**SELECT opttime feedtime, mesg, opttype FROM tk_my_task_log where compid=? and mytaskid=? and (opttype='SIGN_IN' or opttype='RETURN')*/
	public static String listTMTLByMytaskidSingin = "listTMTLByMytaskidSingin";

	/** SELECT l.remarks2 feedstatus FROM tk_my_task_log l left join tk_task_subject s on l.taskid = s.taskid and l.userid = s.subjectid where l.taskid=:taskid and l.remarks4=:remindtime and s.remarks2 !=-1*/
	public static String listLogStatusNotDel2 = "listLogStatusNotDel2";


	public static String listTMTLByMytaskidAudit = "listTMTLByMytaskidAudit";

	 public static String listTMTLByMytaskidAudit2 = "listTMTLByMytaskidAudit2";

	public static String updateTTDByStatus = "updateTTDByStatus";


	/**=====================================================2019-08-05 新增===================================================================*/

	/**
	 * select remarks2 from pw_role_map where compid=? and subjecttp=0 and subjectid=? and remarks2>0 limit 1
	 */
	public static String getAuditType = "getAuditType";

	/**
	 * delete from tk_task_feed_back_audit where compid=? and taskid=? and subjectId!=? and remarks2=2
	 */
	public static String delteOtherAudit = "delteOtherAudit";

	/**
	 * select * from pw_role_map where compid=? and subjectid=? and  subjecttp=0 and remarks2>0 limit 1
	 */
	public static String getRoleMap = "getRoleMap";

	/**
	 * select * from tk_task_subject where compid=? and taskid=? and remarks3=1 and flag=3 and sjtype=0  limit 1
	 */
	public static String getSubjectMap = "getSubjectMap";

	/**
	 * update tk_task_deploy set status=? where compid=? and id=?
	 */
	public static String modiTaskStatus = "modiTaskStatus";

	public static String listPageTTDTask = "listPageTTDTask";



	public static String addSmsVerif = "addSmsVerif";

	public static String getUserByAccount = "getUserByAccount";

	/**
	 * select * from urc_user_account where compid=? and userid=?
	 */
	public static String listPhones = "listPhones";

	public final static String ListDlType2 = "listDlType2";

	public final static String getChildrenSum="getChildrenSum";


	/**
	 * 查询浏览历史数量
	 * select count(*) from brower_his where compid=? and userid = ?
     * getSingleColumn
	 */
    public final static String browserhisCount = "browserhisCount";

    /**
     * 浏览历史删除
     * DELETE FROM brower_his WHERE compid=? and userid = ? and model = ? and modelid = ? and createtime = ?
     */
	public final static String browserhisDelete = "browserhisDelete";

    /**
     * browserhisInsert
     * INSERT INTO brower_his (compid, userid, model, modelid, createtime, num, flag, username) VALUES (:compid, :userid, :model, :modelid, :createtime, :num, :flag, :username)
     */
    public final static String browserhisInsert = "browserhisInsert";

    /**
     * 更新浏览记录
     * UPDATE brower_his SET  createtime = ?, num = ?  WHERE compid=? and modelid =? and userid =?
     */
    public final static String browserhisupdate = "browserhisupdate";

    /**
     * 查询记录
     * SELECT e.tkname,e.progress,e.id,e.userid,e.subjectname ,e.model,e.createtime,e.userid,f.newtime,f.newmesg,e.status,e.period from (SELECT b.tkname tkname,b.progress progress,b.id id,a.userid userid,b.subjectname subjectname,a.model model,a.createtime createtime,b.status status, b.period period FROM brower_his a left join  (select c.id id,c.tkname tkname,c.progress progress,c.period period,d.subjectname subjectname,c.status status from tk_task_deploy c left join (select taskid, GROUP_CONCAT(subjectname) subjectname from tk_task_subject where flag=0 and remarks1!=-1 group by taskid)d on c.id=d.taskid)b on a.modelid=b.id ) e left join tk_task_deploy_sub f on e.id=f.id where userid = ? order by createtime desc
     */
    public final static String browserhislikethis = "browserhislikethis";

    /**
     * 查询某一条记录
     * select * from brower_his where compid=? and modelid = ? and model = ? and userid = ?
     */
    public final static String browserhisByInfo = "browserhisByInfo";

    /**
     * 根据USerID删除记录
     * DELETE FROM brower_his WHERE compid=? and userid = ?
     */
    public final static String delBrowserhisByUserid = "delBrowserhisByUserid";

    /**
     * 根据modileid删除记录
     * DELETE FROM brower_his WHERE compid=? and modelid = ? and model = 0
     */
    public final static String delBrowserhisBymodelid = "delBrowserhisBymodelid";

    /**
     * 删除记录包含某些模块
     * DELETE FROM brower_his WHERE compid=? and userid = ? and modelid in ?
     */
    public final static String delBrowserhisBymodelin = "delBrowserhisBymodelin";

    /**
     * tk附件列表
     * select * from tk_attachment where compid=? and remarks3 = ?
     */
    public final static String tkAttachmentList = "tkAttachmentList";

    /**
     * tk附件列表
     * select model, id, attaname name, path, size, suffix, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6 from tk_attachment where compid=? and remarks3 = ?
     */
    public final static String tkAttachmentList2 = "tkAttachmentList2";

    /**
     * 保存附件信息
     * INSERT INTO tk_attachment (compid, model, id, attaname, path, size, suffix, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid,:model,:id,:attaname,:path,:size,:suffix,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
     */
    public final static String tkAttachmentInsert = "tkAttachmentInsert";

    /**
     * 更新附件信息
     * update tk_attachment set id=? where compid=? and  model=? and remarks3=?
     */
    public final static String tkAttachUpdatemr3 = "tkAttachUpdatemr3";

    /**
     * 根据模块和ID查询附件信息
     * SELECT model,remarks4 id,attaname name,path,remarks3 uuid FROM tk_attachment WHERE compid=? and model=? and id=?
     */
    public final static String querytkAttachBymid = "querytkAttachBymid";

    /**
     * 根据名称和备注3删除附件
     * DELETE FROM tk_attachment WHERE compid=? and attaname=? and remarks3=?
     */
    public final static String deltkAttachBynameAndR3 = "deltkAttachBynameAndR3";

    /**
     * 查询附件根据信息
     * SELECT * FROM tk_attachment where compid=? and remarks3 = ? and attaname = ?
     */
    public final static String querytkAttachBynameAndR3 = "querytkAttachBynameAndR3";

    /**
     * 删除评论
     * DELETE FROM tk_comment WHERE compid=? and id = ?
     */
    public final static String tkCommentdelete = "tkCommentdelete";

    /**
     * 根据任务ID删除评论
     * DELETE FROM tk_comment WHERE compid=? and taskid = ?
     */
    public final static String tkCommentdelByTaskid = "tkCommentdelByTaskid";

    /**
     * 根据ID查询评论
     * SELECT * FROM tk_comment WHERE compid=? and id=?
     */
    public final static String tkCommentget = "tkCommentget";

    /**
     * 根据模块id查询评论
     * select * from tk_comment where compid=? and modelid =? order by commtime desc
     */
    public final static String tkCommentlist = "tkCommentlist";


    /**
     * 保存评论
     * INSERT INTO tk_comment (compid, model, modelid, taskid, mytaskid, fsjtype, fsubjectid, fsubjectname, commtime, userflag, tsjtype, tsubjectid, tsubjectname, commflag, commtext, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid, :model,:modelid,:taskid,:mytaskid,:fsjtype,:fsubjectid,:fsubjectname,:commtime,:userflag,:tsjtype,:tsubjectid,:tsubjectname,:commflag,:commtext,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
     */
    public final static String tkCommentInsert = "tkCommentInsert";

    /**
     * 根据用户ID查询评论
     * SELECT * FROM tk_comment where compid=? and modelid =? and fsubjectid=? order by commtime desc
     */
    public final static String querytkCommentinfo = "querytkCommentinfo";

    /**
     * 根据任务ID查询评论
     * SELECT taskid FROM tk_comment where compid=? and taskid = ?
     */
    public final static String querytkCommentBytid = "querytkCommentBytid";

    /**
     * 根据主体删除评论
     * DELETE FROM tk_comment WHERE compid=? and taskid = ? and tsubjectid=?
     */
    public final static String deltkCommentBysubid = "deltkCommentBysubid";

    /**
     * 删除评论回复
     *  DELETE FROM tk_comment_reply WHERE compid=? and id=?
     */
    public final static String delTkCommentReply = "delTkCommentReply";

    /**
     * 查询评论回复列表
     * select * from tk_comment_reply where commid = ? order by replytime desc
     */
    public final static String queryTkCommentReplyList = "queryTkCommentReplyList";

    /**
     * 保存评论回复
     * INSERT INTO tk_comment_reply (compid, taskid, mytaskid, commid, repsjtype, repsjid, rsjname, replytime, sjtype, subjectid, subjectname, commtime, replyflag, replytext, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid,:taskid,:mytaskid,:commid,:repsjtype,:repsjid,:rsjname,:replytime,:sjtype,:subjectid,:subjectname,:commtime,:replyflag,:replytext,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
     */
    public final static String saveTkCommentReply = "saveTkCommentReply";

    /**
     * 根据ID查询评论回复
     * SELECT * FROM tk_comment_reply WHERE compid=? and id=?
     */
    public final static String queryTkCommentReplyById = "queryTkCommentReplyById";

    /**
     * 根据任务ID删除评论回复
     * delete from tk_comment_reply where compid=? and taskid = ?
     */
    public final static String delTkCommentReplyBytId = "delTkCommentReplyBytId";

    /**
     * 根据任务id查询记录
     * SELECT taskid FROM tk_comment_reply where compid=? and taskid = ?
     */
    public final static String queryTkCommentReplyBytId = "queryTkCommentReplyBytId";

    /**
     * 删除评论回复根据条件
     * delete from tk_comment_reply where compid=? and taskid = ? and repsjid = ?
     */
    public final static String delTkCommentReplyByteid = "delTkCommentReplyByteid";

    /**
     * 根据上级编码查询列表
     * SELECT * FROM tk_dict where compid=? and pcode = ? order by seqno asc
     */
    public final static String queryTkDictByPcode = "queryTkDictByPcode";

    /**
     * 根据编码查询
     * SELECT * FROM tk_dict where compid=? and code = ?
     */
    public final static String queryTkDictByCode = "queryTkDictByCode";

    //********************************************************************************dao层改service开始 by tangtao*******************************************************//
		/**
		 * 新增任务时间节点
		 * INSERT INTO tk_task_time_node (compid,taskid, enddate, crtdate, progress, status, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6)
		 * VALUES (:compid,:taskid,:enddate,:crtdate,:progress,:status,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
		 */
		public static final String addTktasktimenode = "addTktasktimenode" ;

		/**
		 * 根据任务id查询任务结点
		 * SELECT * FROM tk_task_time_node WHERE compid=? and taskid=? order by crtdate asc
		 */
		public static final String listTktasktimenodeByTaskid = "listTktasktimenodeByTaskid" ;

		/**
		 *根据任务id查询任务结点数
         * SELECT count(*) FROM tk_task_time_node WHERE compid=? and taskid=?
		 */
		public static final String getTktasktimenodeCountByTaskid = "getTktasktimenodeCountByTaskid" ;

		/**根据任务id查询任务结点(分页查询)
		 *
		 * SELECT * FROM tk_task_time_node WHERE compid=? and taskid=? order by crtdate asc
		 */
		public static final String pagerTktasktimenodeByTaskid = "pagerTktasktimenodeByTaskid" ;

		/**
		 * 根据任务id删除任务结点
		 *
		 * DELETE FROM tk_task_time_node WHERE compid=? and taskid =?
		 */
		public static final String deleteTktasktimenodeByTaskid = "deleteTktasktimenodeByTaskid" ;

		/**
		 * 确认完成某个周期下的所有人的任务
		 *
		 * update tk_task_time_node set remarks3=? where compid=? and taskid=? and enddate=?
		 */
		public static final String updateTktasktimenode = "updateTktasktimenode" ;

		/**
		 * 根据任务id和结束日期删除任务结点
		 *
		 * DELETE FROM tk_task_time_node WHERE compid=? and taskid = ? and enddate = ?
		 */
		public static final String deleteTimeByTaskidAndEnddate = "deleteTimeByTaskidAndEnddate" ;

		/**
		 * 根据主任务id和结束时间查询对应的节点对象
		 *
		 * SELECT * FROM tk_task_time_node WHERE compid=? and taskid=? and enddate=?
		 */
		public static final String getTimeBytaskidAndEnddate = "getTimeBytaskidAndEnddate" ;

		/**
		 * 根据主任务id和结束时间查询对应的节点对象
		 *
		 * select count(*) from tk_task_time_node where compid=? and taskid=? and enddate=?
		 */
		public static final String getTimeCountByTaskIdAndEndDate = "getTimeCountByTaskIdAndEndDate" ;

		// TkTaskSubjectSertice 開始 #######################//

		/**
		 * 新增任务主体
		 *INSERT INTO tk_task_subject (compid, taskid, flag, sjtype, subjectid, subjectname, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6)
		 * VALUES (:compid, :taskid,:flag,:sjtype,:subjectid,:subjectname,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
		 */
		public static final String insertTkTaskSubject = "insertTkTaskSubject" ;

		/**
		 * 删除牵头领导
		 *DELETE FROM tk_task_subject WHERE compid=? and sjtype = 0 and taskid = ? and flag = ?
		 */
		public static final String deleteByFlagTaskid = "deleteByFlagTaskid" ;

		/**
		 * 根据任务id删除任务主体
		 *DELETE FROM tk_task_subject WHERE compid=?  and taskid = ? and remarks1 !=-1
		 */
		public static final String deleteByTaskid = "deleteByTaskid" ;

		/**
		 * 删除非执行人的所有相关人员
		 *DELETE FROM tk_task_subject WHERE compid=?  and taskid = ? and flag !=0
		 */
		public static final String delNotExcuterByTaskid = "delNotExcuterByTaskid" ;

		/**
		 * 查询某任务的主体(不分页)
		 *SELECT * FROM tk_task_subject WHERE compid=? and taskid=? and flag=? and remarks1!=-1 and remarks2!=-1
		 */
		public static final String listSubjectsByTaskid = "listSubjectsByTaskid" ;

		/**
		 * 根据任务查询主体数
		 *SELECT count(*) FROM tk_task_subject WHERE compid=? and taskid=? and flag=? and remarks1!=-1
		 */
		public static final String getSubjectsCountByTaskid = "getSubjectsCountByTaskid" ;

		/**
		 * 查询任务的执行人和关注人
		 *SELECT * FROM tk_task_subject WHERE compid=? and taskid=? and remarks1!=-1
		 */
		public static final String listByTaskIdAndremarks1 = "listByTaskIdAndremarks1" ;

		/**
		 * 查询任务的执行人和关注人
		 *SELECT subjectid,subjectname FROM tk_task_subject WHERE compid=? and taskid=? and remarks1!=-1
		 */
		public static final String listByTaskIdAndremarks2 = "listByTaskIdAndremarks2" ;

		/**
		 * 查询某任务的主体(分页查询)
		 *SELECT subjectid,subjectname,remarks3 surstatus FROM tk_task_subject WHERE compid= :compid and taskid= :taskid and flag= :flag and remarks1!=-1
		 * and subjectname like ${PARAMS.name}
		 */
		public static final String pageSubjectsByTaskid = "pageSubjectsByTaskid" ;

		/**
		 * 获取执行人总数
		 *SELECT count(*) FROM tk_task_subject WHERE compid= :compid and taskid= :taskid and flag= :flag and sjtype=0 and remarks1!=-1 and remarks2!=-1
		 */
		public static final String getSubjectsCountByTaskidp = "getSubjectsCountByTaskidp" ;

		/**
		 * 获取执行人总数(包含删除的执行人)
		 *SELECT count(*) FROM tk_task_subject WHERE compid=? and taskid=? and flag=? and sjtype=0 and remarks1!=-1
		 */
		public static final String getSubjectsCountByTaskidt = "getSubjectsCountByTaskidt" ;

		/**
		 * 根据主任务id和执行人id删除主题表中该任务的该执行人
		 *update tk_task_subject set remarks1 = -1 WHERE taskid =? and flag = 0 and sjtype = 0 and subjectid = ?
		 */
		public static final String updateSubjectRemarks1 = "updateSubjectRemarks1" ;

		/**
		 *  查询任务的关注人
		 *SELECT * FROM tk_task_subject where compid=? and taskid = ? and flag = ? and sjtype = 0
		 */
		public static final String listTaskFollowers = "listTaskFollowers" ;

		/**
		 *  查询任务的关注人
		 *select * from tk_task_subject where compid=? and taskid=? and subjectid=? and sjtype=0 and flag=1 and remarks1!=-1
		 */
		public static final String listTkIsFoucs = "listTkIsFoucs" ;

		/**
		 * 取消关注任务，将关注时新增的flag为1的数据删除（真删除）
		 *DELETE from tk_task_subject where  compid = ? and flag=1  and remarks1!=-1 and taskid=? and subjectid=?
		 */
		public static final String deleteTkFoucsByUserid = "deleteTkFoucsByUserid" ;

		/**
		 * 根据任务删除任务主体
		 *DELETE FROM tk_task_subject WHERE compid=? and taskid = ?
		 */
		public static final String deleteSubjectByTaskid = "deleteSubjectByTaskid" ;

		/**
		 * 根据flag删除任务主体
		 *DELETE FROM tk_task_subject WHERE compid=:compid and sjtype = 0 and subjectid = :subjectid and flag = :flag
         *  and taskid = :taskid
		 */
		public static final String deleteSubjectByFlag = "deleteSubjectByFlag" ;

		/**
		 * 根据执行人id查询name
		 *select subjectname from tk_task_subject where compid=? and subjectid = ? limit 0,1
		 */
		public static final String getTkSubjectName = "getTkSubjectName" ;

		/**
		 * 根据执行人id查询name
		 *select * from tk_task_subject where compid=? and taskid=? and remarks1!=-1  and flag=0 and subjectid not in (?)
		 */
		public static final String listByTaskidAndSubidRemove = "listByTaskidAndSubidRemove" ;

		/**
		 * 修改执行人时查询执行人(数)
		 *select count(*) from tk_task_subject where  sjtype=0 and flag=0  and taskid=? and subjectid=?
		 */
		public static final String getCountByTaskidAndSubid = "getCountByTaskidAndSubid" ;


		/**
         * 修改执行人时查询执行人
		 * 这里不加remarks1!=-1是因为修该执行人时存在添加的执行人就是退回的人，这样一来插入数据时就汇报主键重复错误
		 *select * from tk_task_subject where  sjtype=0 and flag=0  and taskid=? and subjectid=?
		 */
		public static final String getByTaskidAndSubid = "getByTaskidAndSubid" ;

		/**
         * 更新remarks1字段
         *UPDATE tk_task_subject SET  remarks1 = 0 WHERE compid=? and taskid = ? and flag = 0 and sjtype = 0 and subjectid = ?
		 */
		public static final String updateByRemarks1 = "updateByRemarks1" ;

		/**
         * 更新remarks2字段
         *UPDATE tk_task_subject SET  remarks2 = 0 WHERE compid=? and taskid = ? and flag = 0 and sjtype = 0 and subjectid = ?
		 */
		public static final String updateByRemarks2 = "updateByRemarks2" ;

		/**
         * 更新remarks3字段
         *UPDATE tk_task_subject SET  remarks3 = ? WHERE compid=? and taskid = ? and flag = 0 and sjtype= 0 and subjectid = ?
		 */
		public static final String updateByRemarks3 = "updateByRemarks3" ;

        /**
         * 修该执行人时，将去除的执行人从主体表中删除掉（有可能存在多个）
         *DELETE FROM tk_task_subject WHERE compid=? and taskid = ? and flag = 0 and sjtype = 0 and subjectid in (?)
		 */
		public static final String deleteSubjectByIn = "deleteSubjectByIn" ;

		/**
         * 查询督办人关注的任务
         *select * FROM tk_task_subject WHERE compid=? and sjtype = 0 and subjectid = ? and flag = ?
		 */
		public static final String listByUseridAndFlag = "listByUseridAndFlag" ;

		/**
         * 查询角色关系表 remarks2>0 的
         *select remarks2 from urc_role_map where compid=? and sjtype =0 and sjid =? and remarks2>0
		 */
		public static final String getRemarks2BySubid = "getRemarks2BySubid" ;

		//TkTaskSubjectSertice 结束 #######################//


		// TkTaskRemindNoteService 开始####################################//

		/**
		 * 更新提醒登记簿提醒状态
		 *UPDATE tk_task_remind_note SET issend=? WHERE compid=? and id=?
		 */
		public static final String updateIssendById = "updateIssendById" ;


		/**
		 * 更新扫描标识（remarks1字段）任务即将截止
		 *UPDATE tk_task_remind_note SET issend=1 , remarks1 = 1  WHERE compid=? and id = ?
		 */
		public static final String updateRemarks1ById = "updateRemarks1ById" ;

		/**
		 * 新增提醒登记簿提醒状态
		 *INSERT INTO tk_task_remind_note (compid, id, module, mid, remindtime, rulecode, optuser, noticeusers, issend, mesg, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6)
         * VALUES (:compid,:id,:module,:mid,:remindtime,:rulecode,:optuser,:noticeusers,:issend,:mesg,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
		 */
		public static final String insertRemindNote = "insertRemindNote" ;

        /**
		 * 新增提醒登记簿提醒状态
         *SELECT * FROM tk_task_remind_note where compid=? and id=?
		 */
		public static final String getRemindNoteById = "getRemindNoteById" ;

		/**
		 * 查询待发送提醒列表
         *SELECT * FROM tk_task_remind_note WHERE issend=? and remindtime<=? and rulecode != 'DEPLOY_TASK' and remarks3 != 'suspend' and remarks3 != 'complete'
		 */
		public static final String listNeedSendRemindNote = "listNeedSendRemindNote" ;

		/**
		 * 通过主表id查询任务提醒表相关数据
         *SELECT * FROM tk_task_remind_note WHERE compid=? and mid=? and module = 0 order by remindtime asc
		 */
		public static final String listRemindNoteByTaskId = "listRemindNoteByTaskId" ;

		/**
		 * 查询普通任务的时间节点集合（专门用于过了提醒时间而没有反馈的批处理）
         *SELECT * FROM tk_task_remind_note where module=0 and remindtime<? and remarks1!=1 and remarks3!='suspend' and remarks3 != 'complete'
		 */
		public static final String listOverRemindNote = "listOverRemindNote" ;

		/**
		 * 查询普通任务的时间节点集合（专门用于过了提醒时间而没有反馈的批处理）
         *SELECT remindtime FROM tk_task_remind_note where compid=? and mid=? and module=0 and remindtime<? order by remindtime desc limit 0,1
		 */
		public static final String getMinRemindByTaskid = "getMinRemindByTaskid" ;

		/**
		 * 批量操作获取提醒时间节点集合
         *SELECT remindtime,mid,id FROM tk_task_remind_note where module=0 and remarks2!=1 and remarks3!= 'suspend' and remarks3 != 'complete' order by remindtime
		 */
		public static final String listTkTaskRemindNote = "listTkTaskRemindNote" ;

		/**
		 * 发送消息查询下次反馈提醒时间
         *SELECT remindtime FROM tk_task_remind_note where compid=? and  mid=? and module=0  and remindtime >? and remarks2!=1  order by remindtime limit 0,1
		 */
		public static final String getNextRemindNote = "getNextRemindNote" ;

		/**
		 * 按任务删除提醒节点
         *delete from tk_task_remind_note where compid=? and mid = ? and module = ?
		 */
		public static final String deleteByTaskidAndModule = "deleteByTaskidAndModule" ;

		/**
		 * 更新remark3字段(真心不知道干什么用的 ,问潘涛)
         *UPDATE tk_task_remind_note SET remarks3 = ? WHERE compid=? and module=0 and mid = ?
		 */
		public static final String updateRemindNoteRemarks3 = "updateRemindNoteRemarks3" ;

		// TkTaskRemindNoteService 结束####################################//


    //********************************************************************************dao层改service结束 by tangtao*******************************************************//

    /**
     * 保存导入的任务
     * INSERT INTO tk_import_deploy (compid, userid, username, sumcount, errcount, ulcount, createtime, updatetime, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid, :userid,:username,:sumcount,:errcount,:ulcount,:createtime,:updatetime,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
     */
    public final static String TkImportDeployInsert = "TkImportDeployInsert";

    /**
     *更新任务信息
     * UPDATE tk_import_deploy SET updatetime=?, remarks1=? WHERE compid=? and id=?
     */
    public final static String tkImportDeployUpdate = "tkImportDeployUpdate";

    /**
     * 保存导入任务列表
     * INSERT INTO tk_import_deploy_list (compid, impid, seqno, classid, classes, tkname, starttime, endtime, users, tktpnm, ckflag, ckdesc, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid,:impid,:seqno,:classid,:classes,:tkname,:starttime,:endtime,:users,:tktpnm,:ckflag,:ckdesc,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)
     */
    public final static String saveImportDeployList = "saveImportDeployList";

    /**
     * 根据租户查询任务列表
     * select * from tk_import_deploy_list where impid=?
     * page java.util.Map
     */
    public final static String queryImportDeployList = "queryImportDeployList";

    /**
     * 根据租户查询任务列表
     * select * from tk_import_deploy_list where impid=?
     * list com.lehand.duty.pojo.TkImportDeployList
     */
    public final static String queryImportDeployList2 = "queryImportDeployList2";

	/**
	 * 查询分数
	 * SELECT IFNULL(SUM(score),0) score FROM tk_integral_details where compid=? and taskid = ?
	 */
	public final static String getIntegralDetailsScore = "getIntegralDetailsScore";

    /**
     * 获取数量
     * SELECT count(*) FROM tk_integral_details where compid=? and mytaskid = ? and createtime <= ? and rulecode = ?
     */
    public final static String  getIntegralDetailsCount = "getIntegralDetailsCount";


    /**
     * 获取详情
     * SELECT IFNULL(SUM(score),0) score FROM tk_integral_details where compid=? and mytaskid = ? and rulecode = ? and createtime>=? and createtime<?
     */
    public final static String integralDetailsGet = "integralDetailsGet";

    /**
     *保存信息
     * INSERT INTO tk_integral_details (compid, userid, username, rulecode, score, createtime, remarks1, remarks2, remarks3, taskid, mytaskid) VALUES (:compid, :userid, :username, :rulecode, :score, :createtime, :remarks1, :remarks2, :remarks3, :taskid, :mytaskid)
     */
    public final static String integralDetailInsert = "integralDetailInsert";

    /**
     * 查询一段时间内的详情
     * SELECT  1 id,userid, username, sum(score) score FROM tk_integral_details where compid=? and createtime>=? and createtime<=? group by userid,username  order by score desc
     * list com.lehand.duty.pojo.TkIntegralDetails
     */
    public final static String integralDetailslists = "integralDetailslists";

    /**
     * 查询一段时间内的详情
     * SELECT  1 id,userid, username, sum(score) score FROM tk_integral_details where compid=? and createtime>=? and createtime<=? group by userid,username  order by score desc
     * page com.lehand.duty.dto.TkIntegralResponse
     */
    public final static String integralDetailslists2 = "integralDetailslists2";

    /**
     * 删除某任务下某个执行人获得的积分
     * DELETE FROM tk_integral_details WHERE compid=? and taskid=?
     */
    public final static String delintegralDetailsByTid = "delintegralDetailsByTid";

    /**
     * 删除某任务下某个执行人获得的积分
     * DELETE FROM tk_integral_details WHERE compid=? and taskid=? and userid=?
     */
    public final static String delintegralDetailsByTU = "delintegralDetailsByTU";

    /**
     *根据备注一删除
     * DELETE FROM tk_integral_details WHERE compid=? and remarks1 = ?
     */
    public final static String delintegralDetailsByRek1 = "delintegralDetailsByRek1";

    /**
     * 我的总积分
     * SELECT IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and userid = ?
     */
    public final static String getintegralDetailsTotal = "getintegralDetailsTotal";

    /**
     * 我获得的积分
     * SELECT IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and userid = ? and score>0
     */
    public final static String getidByMScore = "getidByMScore";

    /**
     * 我被扣除的积分
     * SELECT IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and userid = ? and score<0
     */
    public final static String getidByMinScore = "getidByMinScore";

    /**
     * 该年度获得的积分
     * SELECT IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and userid = ? and score>0 and createtime>=? and createtime<=?
     */
    public final static String getidByYearScore = "getidByYearScore";

    /**
     * 该年度扣除的积分
     * SELECT IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and userid = ? and score<0 and createtime>=? and createtime<=?
     */
    public final static String getidByYearminus = "getidByYearminus";

    /**
     * 该年度参与排名的总人数
     * SELECT COUNT(*) ranknums FROM (SELECT userid FROM tk_integral_details where compid=? and createtime>=? and createtime <=?  group by userid) a
     */
    public final static String getRankPNumYear = "getRankPNumYear";

    /**
     * 该年度我的总积分
     *SELECT IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and userid = ? and createtime>=? and createtime<=?
     */
    public final static String getIDYearsumscore = "getIDYearsumscore";

    /**
     * 该年度参与排名的人的总积分集合(排除自己)
     * SELECT userid,IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and  createtime>=? and createtime <=? and userid!=? group by userid
     */
    public final static String getIDYearsscore = "getIDYearsscore";

    /**
     * 当前年份总积分排名
     * SELECT userid,username,IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and  createtime>=? and createtime <=? group by userid,username order by score desc
     */
    public final static String getIDYearListscore = "getIDYearListscore";

    /**
     *  分页当前年份总积分排名
     * SELECT userid,username,IFNULL(sum(score),0.0) score FROM tk_integral_details where compid=? and createtime>=? and createtime <=? group by userid,username order by score desc
     */
    public final static String pageIDYearListscore = "pageIDYearListscore";

    /**
     * 获取规则
     * select * from tk_remind_rule where compid=? and code=?
     */
    public final static String getTkRemindRule = "getTkRemindRule";

    /**
     * 查询所有提醒规则
     * select * from tk_remind_rule where compid=?
     */
    public final static String queryTkRemindRuleAll = "queryTkRemindRuleAll";

    /**
     * 查询
     * SELECT * FROM tk_remind_rule WHERE compid=? and isview=? order by seqno
     */
    public final static String findByIsviewOrderSeqno = "findByIsviewOrderSeqno";

    /**
     * 更新状态
     * UPDATE TK_REMIND_RULE SET STATUS=0 WHERE compid=? and ISVIEW = 1
     */
    public final static String updateTkRemindRuleStatus = "updateTkRemindRuleStatus";

    /**
     * 更新提醒规则
     * UPDATE TK_REMIND_RULE SET STATUS=1 WHERE COMPID= ? AND ISVIEW = 1 AND CODE IN ?
     */
    public final static String updateTkRemindRule = "updateTkRemindRule";

    /**
     * 更新规则可选
     * UPDATE TK_REMIND_RULE SET PARAMS = CASE CODE  WHEN 'BACK_NEXT_BEFORE' THEN ? WHEN 'BACK_END_BEFORE' THEN ? END  WHERE CODE IN ? and compid=?
     */
    public final static String updateTkRemindRuleOpt = "updateTkRemindRuleOpt";

    /**
     * 规则数据
     * select inteval,maxval from tk_integral_rule where compid=? and code = ?
     */
    public final static String queryRuleByCode = "queryRuleByCode";

    /**
     * 根据编码更新
     * UPDATE tk_integral_rule SET flag=:flag,inteval=:inteval,maxval=:maxval,status=:status WHERE compid=:compid and code=:code
     */
    public final static String updateRuleByCode = "updateRuleByCode";

    /**
     * 根据状态查询规则
     * SELECT code id,inteval,maxval,flag,status FROM tk_integral_rule WHERE compid=? and status=?
     */
    public final static String findRuleByStatus = "findRuleByStatus";

    /**
     * 获取时序
     * select inteval from tk_integral_rule where compid=? and code='SIGN_IN'
     *
     */
    public final static String queryRuleInterval = "queryRuleInterval";

    /**
     * 保存赞赏详情
     * INSERT INTO tk_praise_details (compid, id, taskid, mytaskid, feedid, userid, username, optid, optname, rulecode, score, createtime, remarks1, remarks2, remarks3) VALUES (:compid, :id, :taskid, :mytaskid, :feedid, :userid, :username, :optid, :optname, :rulecode, :score, :createtime, :remarks1, :remarks2, :remarks3)
     */
    public final static String savePraiseDetails = "savePraiseDetails";

    /**
     * 每条反馈记录的点赞总数
     * select count(*) from tk_praise_details where compid=? and feedid = ?
     */
    public final static String getPraiseDetailsNum = "getPraiseDetailsNum";

    /**
     * 获取任务点赞总积分
     * SELECT IFNULL(SUM(score),0) score from tk_praise_details where taskid = ?
     */
    public final static String getPraiseDetailsNumByTid = "getPraiseDetailsNumByTid";

    /**
     * 取消点赞
     * delete from tk_praise_details where feedid = ? and optid = ?
     */
    public final static String cancelPraiseDetails = "cancelPraiseDetails";

    /**
     * 判断登录人是否对该任务点赞了
     * select count(*) from tk_praise_details where compid=? and feedid = ? and optid = ?
     */
    public final static String getPraiseDetailsByUserid = "getPraiseDetailsByUserid";

    /**
     * 登陆人获得的总点赞数
     * select count(*) from tk_praise_details where compid=? and userid = ?
     */
    public final static String getAllPraiseByUserid = "getAllPraiseByUserid";

	/**
	 * 判断有没有反馈
	 * SELECT count(*) FROM tk_my_task_log where compid=? and mytaskid = ? and opttype = ?
	 */
	public final static String queryMyTaskLogFbCount = "queryMyTaskLogFbCount";

	/**
	 * 判断有没有反馈,针对积分设置中超过提醒时间没反馈的就扣分的sql语句
	 * SELECT count(*) FROM tk_my_task_log where compid=? and mytaskid = ? and opttype = ? and opttime>? and opttime<=?
	 */
	public final static String getMTfeedbackcount = "getMTfeedbackcount";

	/**
	 * 更新我的任务日志
	 * UPDATE tk_my_task_log SET  commcount = commcount+1 WHERE compid=? and id = ?
	 */
	public final static String updateMyTaskLog = "updateMyTaskLog";

	/**
	 * 任务查询数量
	 * select count(*) from tk_my_task_log where compid=? and mytaskid = ?
	 */
	public final static String getMyTaskpagetotle = "getMyTaskpagetotle";

	/**
	 * 任务列表
	 * select * from tk_my_task_log where compid=? and mytaskid = ? order by opttime desc
	 */
	public final static String queryTkMyTaskLogList = "queryTkMyTaskLogList";

	/**
	 * 获取反馈的消息
	 * SELECT * FROM tk_my_task_log WHERE  compid=? and id=?
	 */
	public final static String getMyTaskLog = "getMyTaskLog";

    /**
     * 保存反馈
     * INSERT INTO tk_my_task_log (compid, taskid, mytaskid, opttime, opttype, mesg, progress, fileuuid, commcount, userid, username, updatetime, integral, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6,publisher,tphone,annex) VALUES (:compid,:taskid,:mytaskid,:opttime,:opttype,:mesg,:progress,:fileuuid,:commcount,:userid,:username,:updatetime,:integral,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6,:publisher,:tphone,:annex)
     */
    public final static String saveMyTaskLog = "saveMyTaskLog";

    /**
     * 更新反馈记录
     * UPDATE tk_my_task_log SET mesg = ?, progress = ?, fileuuid = ?, updatetime = ? WHERE compid=? and id = ?
     */
    public final static String  updateMyTaskLog2 = "updateMyTaskLog2";

    /**
     * 获取状态为签收的操作时间
     * select opttime from tk_my_task_log  where opttype='SIGN_IN' and compid=? and taskid= ? and mytaskid = ?
     */
    public final static String queryMyTaskSignTime = "queryMyTaskSignTime";

    /**
     * 新增反馈时要将之前反馈过的最新进度查出来并显示
     * SELECT progress FROM tk_my_task_log where compid=? and mytaskid = ? order by opttime desc limit 0,1
     */
    public final static String queryMyTaskProcess = "queryMyTaskProcess";

    /**
     * 根据任务ID删除反馈
     * delete from tk_my_task_log where compid=? and taskid = ?
     */
    public final static String delMyTaskLogByTid = "delMyTaskLogByTid";

    /**
     * 根据任务查询反馈
     * SELECT taskid FROM tk_my_task_log where compid=? and taskid = ?
     */
    public final static String queryMytaskByTid = "queryMytaskByTid";

    /**
     * 根据任务ID删除
     * delete from tk_my_task_log where compid=? and taskid = ? and userid=?
     */
    public final static String delMyTaskByTid = "delMyTaskByTid";

    /**
     * 根据任务状态删除
     * delete from tk_my_task_log where compid=? and taskid = ? and userid=? and opttype = 'SIGN_IN'
     */
    public final static String delMyTaskByStatus = "delMyTaskByStatus";

    /**
     * 查询任务反馈
     * SELECT * FROM tk_my_task_log where  compid=? and taskid = ? and remarks3 = ?
     */
    public final static String getMyTaskLogBean = "getMyTaskLogBean";

    /**
     * 跟新反馈UUID
     * UPDATE tk_my_task_log SET  fileuuid = ?  WHERE compid=? and taskid = ? and remarks3 = ?
     */
    public final static String updatMyTaskUUID = "updatMyTaskUUID";

    /**
     * 更新任务属性
     * UPDATE tk_task_attribute SET checked = ?, seqno = ? WHERE compid=? and code =?
     */
    public final static String updateTkTaskAttribute = "updateTkTaskAttribute";

    /**
     * 查询任务属性
     * SELECT * FROM tk_task_attribute where compid=?  order by checked desc,seqno asc
     */
    public final static String listTkTaskAttr = "listTkTaskAttr";

    /**
     * 更新任务属性
     * UPDATE tk_task_attribute SET checked = 0 where compid=?
     */
    public final static String updateTkTaskAttr = "updateTkTaskAttr";

    /**
     * 保存任务分解信息
     * INSERT INTO tk_resolve_info (compid, id, taskid, nodename, idxvalue, unit, rsldesc, timecode, timename, objid, objname, starttime, endtime, fbtime, stageseqno, stagename, pid, status, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid, :id, :taskid, :nodename, :idxvalue, :unit, :rsldesc, :timecode, :timename, :objid, :objname,:starttime, :endtime, :fbtime, :stageseqno, :stagename, :pid, :status, :remarks1,:remarks2, :remarks3, :remarks4, :remarks5, :remarks6)
     */
    public final static String saveResolveInfo = "saveResolveInfo";

    /**
     * 查询分解阶段信息
     * SELECT stagename endtime FROM tk_resolve_info where compid=? and taskid = ? and pid!='root' and stagename != '' group by endtime, stagename
     */
    public final static String queryResolveInfoList = "queryResolveInfoList";

    /**
     * 计算数量
     * select count(*) from(SELECT stagename FROM tk_resolve_info where compid=? and taskid = ? and pid!='root' and stagename != '' group by stagename) a
     */
    public final static String countBytaskIdAndSubjectId = "countBytaskIdAndSubjectId";

    /**
     * 分解信息
     * SELECT * FROM tk_resolve_info where compid=? and taskid = ? and pid!='root' and stagename != '' and stagename=?
     */
    public final static String listResolveinfo = "listResolveinfo";

    /**
     * 获取阶段信息
     * SELECT * FROM tk_resolve_info where compid=? and taskid = ? and pid='root' and stagename=?
     */
    public final static String getResoleInfo = "getResoleInfo";

    /**
     * 获取阶段进展
     * SELECT * FROM tk_resolve_info where compid=? and taskid = ?  and stagename=? limit 0,1
     */
    public final static String getResolveinfoflag = "getResolveinfoflag";

    /**
     * 获取阶段数量
     * select count(*) from ( SELECT stagename FROM tk_resolve_info where compid=? and taskid = ? and pid!='root' and stagename != '' GROUP BY stagename)a
     */
    public final static String getResolveInfoCount = "getResolveInfoCount";

    /**
     * 根据任务删除分解信息
     * DELETE FROM tk_resolve_info WHERE compid=? and taskid = ?
     */
    public final static String delResolveInfoListByTid = "delResolveInfoListByTid";

    /**
     * 分解相关人
     * SELECT objid subjectid, objname subjectname FROM tk_resolve_info where compid=? and taskid=? and objname != '' group by objid,objname
     */
    public final static String queryResolveSubjects = "queryResolveSubjects";

    /**
     * 目标人对比
     * SELECT idxvalue FROM tk_resolve_info  where compid=? and taskid = ? and objname != '' and stagename=?
     */
    public final static String listMubiaoDuibi = "listMubiaoDuibi";

    /**
     * 数据对比列表
     * SELECT IFNULL(b.rslvalue,0.0) rslvalue FROM tk_resolve_info a left join tk_resolve_list b on a.taskid=b.taskid and a.id=b.rslid  where a.compid=? and b.compid=? and a.taskid = ? and a.objname != '' and a.stagename=?
     */
    public final static String listShijiDuibi = "listShijiDuibi";

    /**
     * 列举
     * SELECT a.objname name,a.idxvalue, IFNULL(b.rslvalue,0.0) backValue FROM tk_resolve_info a left join tk_resolve_list b on a.taskid=b.taskid and a.id=b.rslid and b.compid=? where a.compid=? and a.taskid=? and a.objname != '' and a.stagename=?
     */
    public final static String listDuibi = "listDuibi";


    /**
     * 列表分布
     * SELECT a.objname name, b.rslvalue value FROM tk_resolve_info a left join tk_resolve_list b on a.taskid=b.taskid and a.id=b.rslid and b.compid=? where a.compid=?  and a.taskid=? and a.objname != '' and a.stagename=?
     */
    public final static String listFenbu = "listFenbu";

    /**
     * 列表2
     * SELECT a.stagename name,a.idxvalue, IFNULL(b.rslvalue,0.0) backValue FROM tk_resolve_info a left join tk_resolve_list b on a.taskid=b.taskid and a.id=b.rslid and b.compid=? where a.compid=?  and a.taskid=? and a.objname != '' and a.objid=? order by a.endtime
     */
    public final static String listZoushi = "listZoushi";

    /**
     * 列表目标
     *SELECT idxvalue FROM tk_resolve_info  where compid=? and taskid = ? and objname != '' and objid = ?
     */
    public final static String listMubiaoZoushi = "listMubiaoZoushi";

    /**
     * 列表3
     * SELECT IFNULL(b.rslvalue,0.0) rslvalue FROM tk_resolve_info a left join tk_resolve_list b on a.taskid=b.taskid and a.id=b.rslid  where a.compid=? and b.compid=? and a.taskid = ? and a.objname != '' and a.objid = ?
     */
    public final static String listShijiZoushi = "listShijiZoushi";

    /**
     * 获取单位
     * SELECT name FROM tk_resolve_info a left join tk_dict b on a.unit = b.code where a.compid=? and b.compid=? and a.taskid=? limit 0,1
     */
    public final static String getResolveUnit = "getResolveUnit";

    /**
     * 获取根节点
     * SELECT a.idxvalue,a.rsldesc,b.name FROM tk_resolve_info a left join tk_dict b on a.unit=b.code where a.compid=? and b.compid=? and taskid=? and id=?
     */
    public final static String getResolveRoot = "getResolveRoot";

    /**
     * 根据任务ID查询所有分解
     * select * from tk_resolve_info where compid=? and taskid = ?
     */
    public final static String listAllByTaskid = "listAllByTaskid";

    /**
     * 根据任务id和执行人的id查找我的任务id(普通任务)
     * SELECT id, taskid, subjectid,createtime,status FROM tk_my_task where compid=? and taskid = ? and subjectid= ? limit 1
     */
    public final static String getmytaskidperiod = "getmytaskidperiod";

    /**
     * 根据任务id和执行人的id查找我的任务id(周期任务)
     *SELECT id, taskid, subjectid,createtime FROM tk_my_task where compid=? and taskid = ? and subjectid= ? and datenode = ?
     */
    public final static String getmytaskid = "getmytaskid";

    /**
     * 我的任务列表
     * select createtime,datenode endtime,status from tk_my_task where compid=? and taskid = ? and subjectid = ? order by datenode asc
     */
    public final static String listMyTask2 = "listMyTask2";


    /**
     * 我的周期任务时间节点总条数
     * select count(*) from tk_my_task where compid=? and taskid = ? and subjectid = ?
     */
    public final static String listMyTaskCount = "listMyTaskCount";

    /**
     * 已部署周期任务时间节点集合
     * select createtime,datenode endtime,1 status from tk_my_task where compid=? and taskid = ? group by datenode, createtime order by datenode asc
     * page
     */
    public final static String listMyTaskByTaskid = "listMyTaskByTaskid";

    /**
     *查询时间周期集合不分页（修改周期任务执行人时需要用）
     * select createtime,datenode endtime,1 status from tk_my_task where compid=? and taskid = ? group by datenode, createtime order by datenode asc
     */
    public final static String listMyTaskByTaskid2 = "listMyTaskByTaskid2";

    /**
     *  已部署周期任务时间节点总条数
     *  select count(DISTINCT datenode) AS count from tk_my_task where compid=? and taskid = ?
     */
    public final static String listbytaskidcountbytaskid = "listbytaskidcountbytaskid";

    /**
     *  保存主任务信息
     *  INSERT INTO tk_my_task (compid, taskid, datenode, sjtype, subjectid, subjectname, createtime, newtime, progress, integral, status, score, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6,publisher,tphone,annex) VALUES (:compid, :taskid,:datenode,:sjtype,:subjectid,:subjectname,:createtime,:newtime,:progress,:integral,:status,:score,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6,:publisher,:tphone,:annex)
     */
    public final static String saveMyTask = "saveMyTask";

	/**
	 *根据orgCode查询组织用户
	 *select * from t_dzz_info where organcode=?
	 */
	public final static String geTTDzzInfoBycode = "geTTDzzInfoBycode";


	/**
	 * 根据任务id获取任务主体
	 *
	 *select * from tk_task_subject where taskid = ? and flag = ? and compid = ?
	 */
	public static String queryTkTaskSubjectByTaskId = "queryTkTaskSubjectByTaskId";

	/**
	 * 查询组织用户
	 * select * from urc_user where userid = ? and accflag = 1
	 */
	public static String queryUrcUserBySubjectId = "queryUrcUserBySubjectId";

	/**
	 * 根据组织编码集合查询所有任务
	 *SELECT count(*),b.classid FROM tk_my_task a,tk_task_deploy b WHERE a.taskid=b.id AND b.status!=0
	 *and FIND_IN_SET(b.orgcode,:orgcode)
	 * and b.status=:status
	 *  AND b.classid=:classid
	 *   GROUP BY b.classid
	 */
	public static String listTaskIdByCodes = "listTaskIdByCodes";

}
