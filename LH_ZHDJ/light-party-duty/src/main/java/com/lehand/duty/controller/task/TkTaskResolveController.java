package com.lehand.duty.controller.task;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkTaskResolveService;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.dto.TimeNodeResponse;
import com.lehand.duty.dto.TkTaskDeployRequest;
import com.lehand.duty.dto.TkTaskResolveInfoResponse;
import com.lehand.duty.pojo.TkDict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.duty.controller.DutyBaseController;

/**
 * 
 * 任务分解
 * 
 * @author pantao
 * @date 2018年10月22日 上午11:29:56
 *
 */
@Api(value = "任务分解", tags = { "任务分解" })
@RestController
@RequestMapping("/duty/taskResolve")
public class TkTaskResolveController extends DutyBaseController {

	private static final Logger logger = LogManager.getLogger(TkTaskResolveController.class);

	@Resource
	TkTaskResolveService tkTaskResolveService;

	/**
	   * 时间阶段查询（前台下拉框）
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 时间阶段查询（前台下拉框）", notes = " 时间阶段查询（前台下拉框）", httpMethod = "POST")
	@RequestMapping("/stage")
	public Message stage() {
		Message message = new Message();
		try {
			List<TkDict> list = tkTaskResolveService.list(getSession().getCompid(),"stage");
			message.success().setData(list);
		} catch (Exception e) {
			logger.error("查询失败");
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	   * 策略查询（前台下拉框）
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 策略查询（前台下拉框）", notes = " 策略查询（前台下拉框）", httpMethod = "POST")
	@RequestMapping("/strategy")
	public Message strategy() {
		Message message = new Message();
		try {
			List<TkDict> list = tkTaskResolveService.list(getSession().getCompid(),"strategy");
			message.success().setData(list);
		} catch (Exception e) {
			logger.error("查询失败");
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	   * 单位查询（前台下拉框）
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 单位查询（前台下拉框）", notes = " 单位查询（前台下拉框）", httpMethod = "POST")
	@RequestMapping("/unit")
	public Message unit() {
		Message message = new Message();
		try {
			List<TkDict> list = tkTaskResolveService.list(getSession().getCompid(),"unit");
			message.success().setData(list);
		} catch (Exception e) {
			logger.error("查询失败");
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 部署普通分解任务
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 部署普通分解任务", notes = " 部署普通分解任务", httpMethod = "POST")
	@RequestMapping("/deploy")
	public Message deploy(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			request.validate();
			request.setUuid(getUUIDByHash(request.getUuid())+"");
			tkTaskResolveService.deployCommonTask(request, getSession());
			message.success();
		} catch (Exception e) {
			logger.error("保存失败");
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 获取分解任务的阶段
	 * @param taskid
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 获取分解任务的阶段", notes = " 获取分解任务的阶段", httpMethod = "POST")
	@RequestMapping("/listtime")
	public Message listTimeByMyTaskId(Long taskid,Pager pager ) {
		Message message = new Message();
		try {
			int count = tkTaskResolveService.countByTaskIdAndSubjectId(getSession().getCompid(),taskid, getSession().getUserid());
			TimeNodeResponse times = tkTaskResolveService.list(getSession().getCompid(),taskid, pager,getSession().getUserid());
			pager.setRows(times);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 获取分解任务的阶段
	 * @param taskid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 获取分解任务的阶段", notes = " 获取分解任务的阶段", httpMethod = "POST")
	@RequestMapping("/listobj")
	public Message listObj(Long taskid) {
		Message message = new Message();
		try {
			List<Subject> list = tkTaskResolveService.listObj(getSession().getCompid(),taskid);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 获取阶段详情列表
	 * @param taskid
	 * @param endtime
	 * @param flag 0为时间优先，1为分解对象优先
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 获取阶段详情列表", notes = " 获取阶段详情列表", httpMethod = "POST")
	@RequestMapping("/listbean")
	public Message listBean(Long taskid,String endtime,int flag) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = tkTaskResolveService.listBean(getSession().getCompid(),taskid,endtime,flag);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	   * 分解任务提交反馈保存
	 * @param taskid 主任务id
	 * @param endtime 阶段名称
	 * @param mesg 表格json数据字符串
	 * @param uuid 附件uuid没有传空
	 * @param flag 0新增 1修改
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "分解任务提交反馈保存", notes = "分解任务提交反馈保存", httpMethod = "POST")
	@RequestMapping("/save")
	public Message save(Long taskid,String endtime,String mesg,String uuid,int flag) {
		Message message = new Message();
		try {
			tkTaskResolveService.save(taskid,endtime,mesg,uuid,flag,getSession());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 反馈详情修改前查询
	 * @param taskid
	 * @param endtime
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "反馈详情修改前查询", notes = "反馈详情修改前查询", httpMethod = "POST")
	@RequestMapping("/query")
	public Message query(Long taskid,String endtime,int flag) {
		Message message = new Message();
		try {
			List<TkTaskResolveInfoResponse> list = tkTaskResolveService.page(taskid,endtime,flag,getSession());
			message.success().setData(list);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 修改分解任务的时间信息
	 * @param request
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "修改分解任务的时间信息", notes = "修改分解任务的时间信息", httpMethod = "POST")
	@RequestMapping("/modity")
	public Message modity(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			tkTaskResolveService.modityDecom(request,getSession());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 督办跟踪分解任务反馈详情页面对比tab页柱状图接口
	 * @param taskid
	 * @param time
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "督办跟踪分解任务反馈详情页面对比tab页柱状图接口", notes = "督办跟踪分解任务反馈详情页面对比tab页柱状图接口", httpMethod = "POST")
	@RequestMapping("/duibi")
	public Message listDuibi(Long taskid,String time) {
		Message message = new Message();
		try {
			List<Map<String , Object>> list = tkTaskResolveService.listDuibi(getSession().getCompid(),taskid,time);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	
	/**
	 * 督办跟踪分解任务反馈详情页面分布tab饼柱状图接口
	 * @param taskid
	 * @param time
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "督办跟踪分解任务反馈详情页面分布tab饼柱状图接口", notes = "督办跟踪分解任务反馈详情页面分布tab饼柱状图接口", httpMethod = "POST")
	@RequestMapping("/fenbu")
	public Message listFenbu(Long taskid,String time) {
		Message message = new Message();
		try {
			List<Map<String,Object>> list = tkTaskResolveService.listFenbu(getSession().getCompid(),taskid,time);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 督办跟踪分解任务反馈详情页面走势tab饼柱状图接口
	 * @param taskid
	 * @param subjectid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "督办跟踪分解任务反馈详情页面走势tab饼柱状图接口", notes = "督办跟踪分解任务反馈详情页面走势tab饼柱状图接口", httpMethod = "POST")
	@RequestMapping("/zoushi")
	public Message listZoushi(Long taskid,Long subjectid) {
		Message message = new Message();
		try {
			List<Map<String , Object>> list = tkTaskResolveService.listZoushi(getSession().getCompid(),taskid,subjectid);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	
	/**
	 * 获取任务的单位
	 * @param taskid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取任务的单位", notes = "获取任务的单位", httpMethod = "POST")
	@RequestMapping("/getunit")
	public Message getUnit(Long taskid) {
		Message message = new Message();
		try {
			String unit = tkTaskResolveService.getUnit(getSession().getCompid(),taskid);
			message.setData(unit);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 获取根节点的目标值，阶段描述，单位
	 * @param taskid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 获取根节点的目标值，阶段描述，单位", notes = " 获取根节点的目标值，阶段描述，单位", httpMethod = "POST")
	@RequestMapping("/getroot")
	public Message getRoot(Long taskid) {
		Message message = new Message();
		try {
			Map<String,Object>  map = tkTaskResolveService.getRootInfo(getSession().getCompid(),taskid,"root");
			message.setData(map);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
}
