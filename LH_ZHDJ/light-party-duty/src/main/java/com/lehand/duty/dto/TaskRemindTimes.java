package com.lehand.duty.dto;

public class TaskRemindTimes {

	private int feedstatus;//反馈的状态： 0正常反馈，1逾期反馈，2逾期未报
	
	private String remindtime;//提醒时间点
	
	private int flag;//判断是否可以选中 0表示不可选 1表示可选
	
	private Boolean isShow = false;

	public int getFeedstatus() {
		return feedstatus;
	}

	public void setFeedstatus(int feedstatus) {
		this.feedstatus = feedstatus;
	}

	public String getRemindtime() {
		return remindtime;
	}

	public void setRemindtime(String remindtime) {
		this.remindtime = remindtime;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public Boolean getIsShow() {
		return isShow;
	}

	public void setIsShow(Boolean isShow) {
		this.isShow = isShow;
	}
	
	
}
