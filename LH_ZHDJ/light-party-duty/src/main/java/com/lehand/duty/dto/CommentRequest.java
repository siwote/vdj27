package com.lehand.duty.dto;

import com.lehand.base.exception.LehandException;
import com.lehand.components.web.pipeline.Request;

public class CommentRequest extends Request {

	/**
	 * 反馈ID
	 */
	private Long myTaskLogId;
	
	/**
	 * 评论人类型
	 */
	private Integer commentUserType;
	
	/**
	 * 评论内容的类型 (0:文本,1:语音)
	 */
	private Integer commFlag;
	
	/**
	 * (评论类型为0时，存储文本,评论类型为1时，存储附件UUID)
	 */
	private String commText;
	
	public Long getMyTaskLogId() {
		return myTaskLogId;
	}

	public void setMyTaskLogId(Long myTaskLogId) {
		this.myTaskLogId = myTaskLogId;
	}

	public Integer getCommentUserType() {
		return commentUserType;
	}

	public void setCommentUserType(Integer commentUserType) {
		this.commentUserType = commentUserType;
	}

	public Integer getCommFlag() {
		return commFlag;
	}

	public void setCommFlag(Integer commFlag) {
		this.commFlag = commFlag;
	}

	public String getCommText() {
		return commText;
	}

	public void setCommText(String commText) {
		this.commText = commText;
	}

	@Override
	public void validate() throws LehandException {
		
	}
}
