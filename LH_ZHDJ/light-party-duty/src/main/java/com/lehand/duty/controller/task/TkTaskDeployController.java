package com.lehand.duty.controller.task;

import java.io.File;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.dto.urc.UrcUser;
import com.lehand.duty.service.TkTaskClassService;
import com.lehand.duty.service.TkTaskDeployService;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.dto.TkTaskDeployRequest;
import com.lehand.duty.dto.TkTaskFormRespond;
import com.lehand.duty.dto.TkTaskRequest;
import com.lehand.duty.dto.TkTaskRespond;
import com.lehand.duty.utils.POIExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.FileUtil;
import com.lehand.components.cache.CacheComponent;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.controller.DutyBaseController;

/**
 * 任务部署
 * 
 * @author Zim
 *
 */
@Api(value = "任务部署", tags = { "任务部署" })
@RestController
@RequestMapping("/duty/taskDeploy")
public class TkTaskDeployController extends DutyBaseController {

	private static final Logger logger = LogManager.getLogger(TkTaskDeployController.class);

	@Resource private TkTaskDeployService tkTaskDeployService;
	@Resource private TkTaskClassService tkTaskClassService;
	@Resource private CacheComponent cacheComponent;
	@Resource private GeneralSqlComponent generalSqlComponent;

	/**
	 * 批量任务提交
	 * @param importId
	 * @return
	 */
    @OpenApi(optflag=1)
    @ApiOperation(value = "批量任务提交", notes = "批量任务提交", httpMethod = "POST")
	@RequestMapping("/submitUploadTask")
	public Message submitUploadTask(Long importId) {
		Message message = new Message();
		try {
			tkTaskDeployService.submitUploadTask(importId,getSession());
            message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error("批量任务提交异常",e);
		}
		return message;
	}
	
	/**
	 * 批量任务分页查询
	 * @param importId
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "批量任务分页查询", notes = "批量任务分页查询", httpMethod = "POST")
	@RequestMapping("/pageUploadTask")
	public Message pageUploadTask(Long importId,Pager pager) {
		Message message = new Message();
		try {
			tkTaskDeployService.pageUploadTask(importId,pager);
            message.success().setData(pager);
		} catch (Exception e) {
			logger.error("批量任务预览异常",e);
			message.setMessage(e.getMessage());
		}
		return message;
	}
	
	/**
	 * 上传批量任务
	 * @param file
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "上传批量任务", notes = "上传批量任务", httpMethod = "POST")
	@RequestMapping("/uploadExcelTemplate")
	public Message uploadExcelTemplate(MultipartFile file) {
		Message message = new Message();
		try {
			Session session = getSession();
			if (file.isEmpty()) {
				message.setMessage("文件为空");
				return message;
			}
			String filename = file.getOriginalFilename();
			File dest = new File(getFileUploadDir("up_"+session.getUserid()+"_"+new Date().getTime(), 99),filename);
			file.transferTo(dest);
			Long import_id = tkTaskDeployService.uploadExcelTemplate(dest, session);
            message.success().setData(import_id);
		} catch (Exception e) {
			logger.error("上传批量任务异常",e);
			message.setMessage(e.getMessage());
		}
		return message;
	}
	
	/**
	 * 下载批量任务模板
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "下载批量任务模板", notes = "下载批量任务模板", httpMethod = "POST")
	@RequestMapping("/downExcelTemplate")
	public Message downExcelTemplate(HttpServletRequest request, HttpServletResponse response) {
		Message message = new Message();
		try {
			Session session = getSession();
			String uploadDir = getUploadBaseDir();
			String dirPath = "/file/templates/excel/"+session.getUserid()+"_"+new Date().getTime();
			File dir = new File(uploadDir, dirPath);
			if ((!dir.exists()) && (!dir.mkdirs())) {
				message.setMessage("批量任务模板所在目录创建失败!");
				return message;
			}
			File file = new File(dir, "力瀚OTS批量任务导入模板.xls");
			List<String> clsnameList = tkTaskClassService.listAllClsnames(session.getCompid());
			List<String> users = generalSqlComponent.query(SqlCode.listAllUsernames, new Object[] {getSession().getCompid(),1});
			String srcPath = uploadDir+"\\file\\mode\\力瀚OTS批量任务导入模板.xls";
			File srcFile = new File(srcPath);
			FileUtil.copyFile(srcFile, file);
			POIExcelUtil.createTaskDeployExcelTemplate(file.getAbsolutePath(),
					                                   clsnameList,
					                                   null,users);
			message.success().setData(dirPath+"/"+file.getName());;
		} catch (Exception e) {
			logger.error("生成批量创建任务模板文件异常!",e);
			message.setMessage("生成模板失败!");
		}
        return message;
	}


	
	/**
	 * 任务普通部署
	 * 
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务普通部署", notes = "任务普通部署", httpMethod = "POST")
	@RequestMapping("/deployCommonTask")
	public Message deployCommonTask(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			request.validate();
			Long id = tkTaskDeployService.deployCommonTask(request,getSession());
			message.setData(id);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
	 * 获取上级人员列表
	 *
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取上级人员列表", notes = "获取上级人员列表", httpMethod = "POST")
	@RequestMapping("/listSuperiors")
	public Message listSuperiors(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
//			List<PwUser> users = tkTaskDeployService.listSuperiors(request,getSession());
			List<UrcUser> users = tkTaskDeployService.listSuperiors(request,getSession());
			message.setData(users);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
	 * 修改普通任务
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改普通任务", notes = "修改普通任务", httpMethod = "POST")
	@RequestMapping("/updateCommonTask")
	public Message updateCommonTask(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			request.validate();
			Long id = tkTaskDeployService.updateCommonTask(request,getSession());
			message.setData(id);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
	 * @Description  保存任务草稿
	 * @Author zouchuang
	 * @Date 2019/4/11 8:54
	 * @Param [request]
	 * @return com.lehand.base.common.Message
	 **/
    @OpenApi(optflag=1)
    @ApiOperation(value = "保存任务草稿", notes = "保存任务草稿", httpMethod = "POST")
	@RequestMapping("/saveTaskDraft")
	public Message saveTaskDraft(TkTaskDeployRequest request){
		Message message = new Message();
		try {
			request.validate();
			Long id = tkTaskDeployService.saveTaskDraft(request,getSession());
			message.setData(id);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
	 * 计算反馈时间点并返回给前台
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "计算反馈时间点并返回给前台", notes = "计算反馈时间点并返回给前台", httpMethod = "POST")
	@RequestMapping("/getCycleTimesNew")
	public Message getCycleTimesNew(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			List<Map<String, Object>> cycleTimes = tkTaskDeployService.getCycleTimesNew(request, getSession());
			message.setData(cycleTimes);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
	 * 计算反馈时间点并返回给前台
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "计算反馈时间点并返回给前台", notes = "计算反馈时间点并返回给前台", httpMethod = "POST")
	@RequestMapping("/remindForOne")
	public Message remindForOne(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			tkTaskDeployService.remindForOne(request, getSession());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

    @OpenApi(optflag=0)
    @ApiOperation(value = "草稿", notes = "草稿", httpMethod = "POST")
	@RequestMapping("/listDraftTask")
	public Message listDraftTask(TkTaskDeployRequest request,Pager pager) {
		Message message = new Message();
		try {
			pager = tkTaskDeployService.listDraftTask(request, getSession(), pager);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
         * @Description  修改时部署草稿任务 ,完整的部署逻辑
         * @Author zouchuang
         * @Date 2019/4/11 8:54
         * @Param [request]
         * @return com.lehand.base.common.Message
         **/
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改时部署草稿任务 ,完整的部署逻辑", notes = "修改时部署草稿任务 ,完整的部署逻辑", httpMethod = "POST")
	@RequestMapping("/deployDraftCompelete")
	public Message deployDraftCompelete(TkTaskDeployRequest request){

		Message message = new Message();
		try {
			request.setDraftFlag("1");
			tkTaskDeployService.saveTaskDraft(request,getSession());
			System.out.println("***************************888");
			Long id = tkTaskDeployService.deployDraftTask(request,getSession());
			message.setData(id);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 部署周期任务
	 * @author pantao
	 * @date 2018年11月13日上午9:52:03
	 * 
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "部署周期任务", notes = "部署周期任务", httpMethod = "POST")
	@RequestMapping("/deployPeriodTask")
	public Message deployPeriodTask(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			request.validate();
			request.setUuid(getUUIDByHash(request.getUuid())+"");
			Long id = tkTaskDeployService.deployPeriodTask(request,getSession());
			message.setData(id);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 查看任务详情
	 * @author pantao
	 * @date 2018年11月15日下午4:19:49
	 * 
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看任务详情", notes = "查看任务详情", httpMethod = "POST")
	@RequestMapping("/details")
	public Message details(Long id) {
		int isPC=0;
		Message message = new Message();
		try {
			TkTaskRespond tkTaskRespond = tkTaskDeployService.details(id,isPC,getSession());
			message.success().setData(tkTaskRespond);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("任务详情查询出错!");
		}
		return message;
	}
	
	
	/**
	 * 任务列表查询入口
	 * 
	 * @author pantao  
	 * @date 2018年10月12日 下午3:56:37
	 *  
	 * @param request
	 * @return
	 * @throws LehandException
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务列表查询入口", notes = "任务列表查询入口", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(TkTaskRequest request, Pager pager){
		Message message = new Message();
		if (null == request) {
			LehandException.throwException("信息不能为空!");
		}		
		try {
			tkTaskDeployService.page(request,pager,true,getSession().getUserid(), getSession());//true代表PC端已部署任务列表
			message.setData(pager);
			message.success();
		} catch (LehandException | ParseException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	/**
	 * 已发任务全部即统计 列表查询
	 *
	 * @author pantao
	 * @date 2018年10月12日 下午3:56:37
	 * @return
	 * @throws LehandException
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "已发任务全部即统计 列表查询", notes = "已发任务全部即统计 列表查询", httpMethod = "POST")
	@RequestMapping("/allPage")
	public Message allPage(Long taskid,Integer status,Pager pager){
		Message message = new Message();

		try {
			tkTaskDeployService.allPage(getSession(),taskid,status,pager);//true代表PC端已部署任务列表
			message.setData(pager);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	/**
	 * 已发任务 应报 ，实报 87 条，差额 13 条  逾期未报 5 人，逾期反馈 4 人，正常上报 30 人 统计
	 *
	 * @author pantao
	 * @date 2018年10月12日 下午3:56:37
	 * @return
	 * @throws LehandException
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务", notes = "已发任务 应报 ，实报 87 条，差额 13 条  逾期未报 5 人，逾期反馈 4 人，正常上报 30 人 统计", httpMethod = "POST")
	@RequestMapping("/feedCount")
	public Message feedCount(Long taskid){
		Message message = new Message();

		try {
			message.setData(tkTaskDeployService.feedCount(getSession(),taskid));
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	
	/**
	 * 查询部署任务列表头部状态角标
	 * 
	 * @author pantao  
	 * @date 2018年10月15日 下午9:40:26
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询部署任务列表头部状态角标", notes = "查询部署任务列表头部状态角标", httpMethod = "POST")
	@RequestMapping("/count")
	public Message listCountByStatus() {
		int sjtype = 0;
		Message message = new Message();		
		try {
			List<Map<String, Object>> list = tkTaskDeployService.listCountByStatus(getSession(),getSession().getUserid(),sjtype);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("各状态数量查询出错!");
		}		
		return message;		
	}
	/**
	 * 任务督办列表查询
	 * @param request
	 * @param pager
	 * @return
	 * @throws LehandException
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务督办列表查询", notes = "任务督办列表查询", httpMethod = "POST")
	@RequestMapping("/oversee")
	public Message overSeePage(TkTaskRequest request,Pager pager) throws LehandException{
		Message message = new Message();
		try {	
			if (null == request) {
				LehandException.throwException("信息不能为空!");
			}
			Pager pager2= tkTaskDeployService.overSeePage(request,getSession().getCompid(),getSession().getUserid(), pager);
			message.setData(pager2);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	/**
	 * 任务督办列表查询
	 * @param request
	 * @param pager
	 * @param flag 3分管领导，4牵头领导
	 * @return
	 * @throws LehandException
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务督办列表查询", notes = "任务督办列表查询", httpMethod = "POST")
	@RequestMapping("/lingdao")
	public Message lingDaoPage(TkTaskRequest request,int flag,Pager pager) throws LehandException{
		Message message = new Message();
		try {	
			if (null == request) {
				LehandException.throwException("信息不能为空!");
			}
			Pager pager2= tkTaskDeployService.lingDaoPage(request,flag,getSession().getCompid(),getSession().getUserid(), pager);
			message.setData(pager2);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 任务督办列表获取各状态数量
	 * @return
	 * @author yuxianzhu
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务督办列表获取各状态数量", notes = "任务督办列表获取各状态数量", httpMethod = "POST")
	@RequestMapping("/statuscount")
	public Message getStatusCount(Long classid){
		Message message = new Message();
		try {
			List<Map<String, Object>> list = tkTaskDeployService.getStatusCount(getSession().getUserid(),classid,getSession().getCompid());
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("各状态数量查询出错!");
		}	
		return message;
	}
	
	
	/**
	 * 点击关注指定任务
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 * @throws LehandException 
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "点击关注指定任务", notes = "点击关注指定任务", httpMethod = "POST")
	@RequestMapping("/foucstask")
	public Message foucsTask(Long taskid) throws LehandException  {
		Message message = new Message();
		try {
			if (StringUtils.isEmpty(taskid)){
				LehandException.throwException("信息不能为空!");
			}
			tkTaskDeployService.focusTask(getSession().getCompid(),taskid, getSession().getUserid(), getSession().getUsername());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 点击按钮进行催办操作
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "点击按钮进行催办操作", notes = "点击按钮进行催办操作", httpMethod = "POST")
	@RequestMapping("/clickurgent")
	public Message toUrgent(Long taskid,String enddate)  {
		Message message = new Message();
		try {
			if (StringUtils.isEmpty(taskid)){
				LehandException.throwException("信息不能为空!");
			}
			//判断催办是否处于1分钟以内，是就不让其继续
			if(StringUtils.isEmpty(enddate)) {
				enddate = DateEnum.YYYYMMDD.format()+" 23:59:59";
		    }
			String key = "toUrgent_clickurgent:"+taskid+enddate;
			String cache = cacheComponent.get(key);
			if(StringUtils.isEmpty(cache)) {
				cacheComponent.set(key, "催办保存时间", 60000L);
			}else {
				message.setCode("666666");//和前端约定好一旦code为666666则弹出警告提示语
				message.setMessage("催办冷却中请稍后继续！");
				return message;
			}
			tkTaskDeployService.toUrgent(taskid,enddate, getSession());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e){
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 取消关注操作（后台删除关注操作时新增的flag为1的数据）
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "取消关注操作（后台删除关注操作时新增的flag为1的数据）", notes = "取消关注操作（后台删除关注操作时新增的flag为1的数据）", httpMethod = "POST")
	@RequestMapping("/cancelfocus")
	public Message canaelFocus(Long taskid){
		Message message = new Message();
		try {
			if (StringUtils.isEmpty(taskid)){
				LehandException.throwException("信息不能为空!");
			}
			tkTaskDeployService.cancelFocus(getSession(), taskid);
			message.success();
		} catch (LehandException e) {
			logger.error(e.getMessage(), e);
			message.setMessage(e.getMessage());
		}
		return message;
	}
	


	/**
	 * 暂停任务
	 * 
	 * @author pantao  
	 * @date 2018年10月18日 上午10:37:09
	 *  
	 * @param id
	 * @param flag 1暂停所有任务，2暂停某执行人的任务，3暂停周期任务的后续任务
	 * @param userid 对应的执行人，没有就穿空
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "暂停任务", notes = "暂停任务", httpMethod = "POST")
	@RequestMapping("/suspend")
	public Message suspend(Long id,int flag,String userid) {
		Message message = new Message();
		try {
			tkTaskDeployService.suspend(id,flag,userid,getSession());
			message.success();	
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }catch (Exception e) {
	    	e.printStackTrace();
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
	
	
	/**
	 * 恢复任务
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "恢复任务", notes = "恢复任务", httpMethod = "POST")
	@RequestMapping("/recovery")
	public Message recovery(Long id) {
		Message message = new Message();
		try {
			tkTaskDeployService.recovery(id,getSession());
			message.success();	
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }catch (Exception e) {
	    	e.printStackTrace();
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
	
	/**
	 * 确认完成任务
	 * @param id 主任务id
	 * @param flag 1表示完成所有，2表示完成某个执行人，3表示完成某个周期
	 * @param date 周期对应的截止时间，没有就穿空
	 * @param userid 对应的执行人，没有就穿空
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "确认完成任务", notes = "确认完成任务", httpMethod = "POST")
	@RequestMapping("/complete")
	public Message complete(Long id,int flag,String date,String userid) {
		Message message = new Message();
		try {
			tkTaskDeployService.complete(id,flag,date,userid,getSession());
			message.success();	
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }catch (Exception e) {
	    	e.printStackTrace();
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}

	
	/**
	 * 修改任务基本信息
	 * @param request
	 * @return
	 * @author yuxianzhu
	 * @throws Exception 
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改任务基本信息", notes = "修改任务基本信息", httpMethod = "POST")
	@RequestMapping("/updateTaskBase")
	public Message updateTaskBase(TkTaskDeployRequest request) throws Exception{
		Message message=new Message();
		try {
			tkTaskDeployService.updateTaskBase(request, getSession());
			message.success();
		}catch (LehandException e) {
			logger.error(e);
			message.setMessage(e.getMessage());
			return message;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 修改任务执行人
	 * @param request
	 * @return
	 * @author yuxianzhu
	 * @throws LehandException 
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改任务执行人", notes = "修改任务执行人", httpMethod = "POST")
	@RequestMapping("/updateTaskSubjects")
	public Message updateTaskSubjects(TkTaskDeployRequest request) throws Exception{
		Message message=new Message();
		try {
			int status = tkTaskDeployService.updateTaskSubjects(request, getSession());
			message.success().setData(status);;
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			e.printStackTrace();
		}catch (Exception e) {
			message.setMessage("修改任务执行人出现异常");
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 修改时间起止时间和普通任务反馈时间节点
	 * @param request
	 * @return
	 * @author yuxianzhu
	 * @throws Exception 
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改时间起止时间和普通任务反馈时间节点", notes = "修改时间起止时间和普通任务反馈时间节点", httpMethod = "POST")
	@RequestMapping("/updateTaskTimenode")
	public Message updateTaskTimenode(TkTaskDeployRequest request) throws Exception{
		Message message=new Message();
		try {
			tkTaskDeployService.updateTimes(request, getSession());
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("修改任务异常");
			return message;
		}
		return message;
	}
	
	/**
	 * 
	 * 删除任务 (删除所有任务)
	 * 
	 * @author pantao  
	 * @date 2018年10月18日 上午10:37:46
	 *  
	 * @param id
	 * @param flag 0删除所有，1周期任务删除当前周期任务
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = " 删除任务 (删除所有任务)", notes = " 删除任务 (删除所有任务)", httpMethod = "POST")
	@RequestMapping("/deleteAll")
	public Message deleteAll(Long id,int flag,String enddate) {
		Message message = new Message();
		try {
			tkTaskDeployService.deleteAll(id,flag,enddate,getSession());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    } catch (Exception e) {
	    	e.printStackTrace();
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
	
	/**
	 * 自定义表单
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = " 自定义表单", notes = " 自定义表单", httpMethod = "POST")
	@RequestMapping("/queryform")
	public Message queryForm() {
		Message message = new Message();		
		try {
			List<TkTaskFormRespond> list = tkTaskDeployService.form(getSession().getCompid());
			message.setData(list);
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			message.setMessage("自定义表单查询出错!");
		}		
		return message;		
	}
	/**
	 * 驾驶舱党建任务督查统计
	 *@author lx
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "驾驶舱党建任务督查统计", notes = "驾驶舱党建任务督查统计", httpMethod = "POST")
	@RequestMapping("/taskStatistical")
	public Message taskStatistical() {
		Message message = new Message();
		try {
			message.setData(tkTaskDeployService.taskStatistical(getSession()));
			message.success();
		} catch (Exception e) {
			LehandException.throwException("驾驶舱党建任务督查统计"+e);
		}
		return message;
	}
}