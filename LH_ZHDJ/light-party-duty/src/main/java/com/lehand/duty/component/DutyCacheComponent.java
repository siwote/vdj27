package com.lehand.duty.component;

import com.alibaba.fastjson.JSON;
import com.lehand.base.exception.LehandException;
import com.lehand.components.cache.CacheComponent;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.pojo.TkRemindRule;
import com.lehand.duty.service.TkRemindRuleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class DutyCacheComponent {

	private static final Logger logger = LogManager.getLogger(DutyCacheComponent.class);
	
	@Resource private TkRemindRuleService tkRemindRuleService;
	@Resource private CacheComponent cacheComponent;
	
	/**
	 * 获取提醒规则配置信息
	 * @return
	 * @throws LehandException 
	 */
	public TkRemindRule getTkRemindRule(Long compid, String code) throws LehandException {
		String key = generatorCacheKey(CahcheKey.TkRemindRuleKey, code);
		String value = cacheComponent.get(key);
		if (null!=value) {
			return JSON.parseObject(value,TkRemindRule.class);
		}
		TkRemindRule rule = tkRemindRuleService.get(compid,code);
		logger.info("查询{}提醒规则",code);
		if (null!=rule) {
			cacheComponent.set(key, JSON.toJSONString(rule));
			return rule;
		}
		return null;
	}
	
	/**
	 * 获取提醒规则
	 * @param ruleEnum
	 * @return
	 * @throws LehandException
	 */
	public TkRemindRule getTkRemindRule(Long compid, RemindRuleEnum ruleEnum) throws LehandException {
		TkRemindRule rule = getTkRemindRule(compid,ruleEnum.name());
		if (null==rule) {
			LehandException.throwException("没有找到"+ruleEnum.name()+"的配置!");
		}
		return rule;
	}
	
	/**
	 * 清空提醒规则缓存
	 * @throws LehandException 
	 */
	public void cleanTkRemindRulesCache(Long compid) throws LehandException {
		logger.info("清空所有提醒规则缓存");
		List<TkRemindRule> rules = tkRemindRuleService.findAll(compid);
		for (TkRemindRule rule : rules) {
			String key = generatorCacheKey(CahcheKey.TkRemindRuleKey, rule.getCode());
			cacheComponent.delete(key);
		}
	}

	private static class CahcheKey{
		private static final String TkRemindRuleKey = "TkRemindRule_";
	}
	
	private String generatorCacheKey(String pre,String key) {
		return new StringBuilder(pre).append(key).toString();
	}
	
}
