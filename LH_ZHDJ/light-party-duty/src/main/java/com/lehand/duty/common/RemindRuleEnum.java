package com.lehand.duty.common;

import com.alibaba.fastjson.JSON;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.ExpressionUtil;
import com.lehand.base.util.SpringUtil;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.dao.*;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.pojo.*;
import com.lehand.duty.service.TkCommentReplyService;
import com.lehand.duty.service.TkCommentService;
import com.lehand.duty.service.TkIntegralRuleService;
import com.lehand.duty.service.TkMyTaskLogService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RemindRuleEnum {

	// 发布普通任务时，提醒执行人反馈任务情况
	DEPLOY_TASK("自定义提醒",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler, Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	}, 
	// 有新任务时，提醒任务执行人
	NEW_TASK("有新任务",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask back = getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),back.getTaskid());
			params.put("back", back);
			params.put("deploy", deploy);
			return params;
		}
	}, 
	// 任务被签收，提醒任务创建人
	SIGN_TASK("有任务被签收",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask back = getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),back.getTaskid());
			params.put("back", back);
			params.put("deploy", deploy);
			TkIntegralRule integral = getTkIntegralRuleDao().get(rule.getCompid(),"SIGN_IN");
			params.put("inteval", integral.getInteval());
			return params;
		}
	}, 
	// 退回任务,提醒任务创建人
	RETURN_TASK("有任务被退回",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask back =  getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),back.getTaskid());
			params.put("back", back);
			params.put("deploy", deploy);
			return params;
		}
	}, 
	// 有新进展反馈，提醒任务创建人
	BACK_TASK("有新反馈",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTaskLog backmesg = getTkMyTaskLog().get(rule.getCompid(),note.getMid());
			TkMyTask back =  getTkMyTaskDao().get(rule.getCompid(),backmesg.getMytaskid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),back.getTaskid());
			params.put("backmesg", backmesg);
			params.put("back", back);
			params.put("deploy", deploy);
//			TkMyTaskLog tkMyTaskLog = getTkMyTaskLog().get(note.getMid());
//			String date = DateEnum.YYYY_MM_DD.format();
//			String [] str = date.split("-");
//			String year = str[0];
//			String month = str[1];
//			String mintime = year+"-"+month+"-01 00:00:00";
//			String maxtime = "";
//			if(month.equals("12")) {
//				year = String.valueOf(Integer.valueOf(year)+1);
//				maxtime = year+"-"+"01-01 00:00:00";		
//			}else {
//				month = String.valueOf(Integer.valueOf(month)+1);
//				maxtime = year+"-"+month+"-01 00:00:00";
//			}
//			TkIntegralDetails max = getTkIntegralDetailsDao().get(mintime,maxtime,tkMyTaskLog.getMytaskid(),note.getRulecode());
//			params.put("sumscore", max.getScore());
//			TkIntegralRule integral = getTkIntegralRuleDao().get("FEED_BACK");
//			params.put("maxval", integral.getMaxval());
//			params.put("inteval", integral.getInteval());
			return params;
		}
	},
	// 修改反馈进展
	UPDATE_BACK_TASK("反馈被修改",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			return BACK_TASK.getParams(rule, note, handler, subject);
		}
	},
	// 有新评论时，提醒任务执行人
	COMMENT_BACK("有新评论",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkComment comment = getTkCommentDao().get(rule.getCompid(),note.getMid());
			TkMyTaskLog backmesg = getTkMyTaskLog().get(rule.getCompid(),comment.getModelid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),comment.getTaskid());
			params.put("backmesg", backmesg);
			params.put("deploy", deploy);
			return params;
		}
	}, 
	// 回复评论时，提醒评论人
	REPLY_COMMENT("有新回复",false) {
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkCommentReply tkCommentReply = getTkCommentReplyDao().get(rule.getCompid(),note.getMid());
			TkComment comment =  getTkCommentDao().get(rule.getCompid(),tkCommentReply.getCommid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),comment.getTaskid());
			params.put("comment", comment);
			params.put("deploy", deploy);
			return params;
		}
	}, 
	
	// 任务逾期未反馈，提醒任务执行人
	OVERDUE_TASK("逾期未反馈",true){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask mytask = getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),mytask.getTaskid());
			params.put("mytask", mytask);
			params.put("deploy", deploy);//数据库发消息凡是涉及到任务名称的都需要主表
			TkIntegralRule integral = getTkIntegralRuleDao().get(rule.getCompid(),"REMINDBACK");
			params.put("inteval", integral.getInteval());
			return params;
		}
	}, 
	
	// 在任务即将达到截止日期前，提醒反馈人，提前{day}天提醒（可以输入小数），提醒时间为：{time}
	BACK_END_BEFORE("任务即将截止",true){
//		@SuppressWarnings("unchecked")
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask mytask = getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),mytask.getTaskid());
			params.put("mytask", mytask);
			params.put("deploy", deploy);
			return params;
		}
	}, 
	
	// 在任务即将达到下一次反馈日期前，提醒反馈人，提前{day}天提醒（可以输入小数），提醒时间为：{time}
	BACK_NEXT_BEFORE("任务反馈提醒",true){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask mytask = getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),mytask.getTaskid());
			params.put("mytask", mytask);
			params.put("deploy", deploy);
			params.put("nexttime", note.getRemindtime());
			return params;
		}
	},
	//催办
	CUI_BAN("催办提醒",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkMyTask mytask = getTkMyTaskDao().get(rule.getCompid(),note.getMid());
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),mytask.getTaskid());
			params.put("mytask", mytask);
			params.put("deploy", deploy);
			return params;
		}
	},
	SCORE_TASK("",false), // 任务评分时，提醒任务执行人
	// 创建人暂停任务，提醒任务执行人
	APPLY_SUSPEND_TASK("任务被暂停",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	}, 
	// 任务被恢复时提醒执行人
	RECOVERY_TASK("任务被恢复",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	}, 
	REJECT_APPLY("",false), // 任务创建人驳回申请，提醒任务执行人
	// 任务确认完成时，提醒任务执行人
	COMPLETE_TASK("任务确认完成",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	}, 
	MODIFY_TASK_BASIC("修改任务基本信息",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	}, // 任务被修改时，提醒任务执行人
    MODIFY_TASK_SUBJECT("变更任务执行人",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	}, // 任务被修改时，提醒任务执行人
    MODIFY_TASK_TIME("变更任务反馈时间要求",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			
			params.put("deploy", deploy);
			return params;
		}
	}, // 任务被修改时，提醒任务执行人
	//反馈被点赞
	FEED_PARISE("反馈被点赞",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	},
	// 任务被删除时，提醒任务执行人
	DELETE_TASK("任务被删除",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
			TkTaskDeploy deploy = getTkTaskDeployDao().get(rule.getCompid(),note.getMid());
			params.put("deploy", deploy);
			return params;
		}
	},
	GET_INTEGER("获得积分",false){
		public Map<String, Object> getParams(TkRemindRule rule, TkTaskRemindNote note, Subject handler,Subject subject) {
			Map<String, Object> params = super.getParams(rule, note, handler,subject);
//			TkTaskDeploy deploy = getTkTaskDeployDao().get(note.getMid());
//			params.put("deploy", deploy);
			return params;
		}
	};
	
	
	
	@SuppressWarnings({ "unchecked" })
	public void notice(TkRemindRule rr,TkTaskRemindNote note,Map<String, Object> args) throws LehandException {
		Subject handler = this.getHandlerSubjects(note);
		List<Subject> subjects = this.getRemindSubjects(note);
		for (Subject subject : subjects) {
			Map<String, Object> ps = this.getParams(rr, note, handler, subject);
			if (null!=args && args.size()>0) {
				ps.putAll(args);
			}
			if (StringUtils.hasLength(note.getRemarks4())) {
				Map<String, Object> params = JSON.parseObject(note.getRemarks4(), Map.class);
 				ps.putAll(params);
			}
			//remindParams.periodtime<3 ? '任务《'+deploy.tkname+'》已完成。':'《'+deploy.tkname+'》的'+remindParams.endtime+'阶段任务已完成。'
//			try {
//				System.out.println(JSON.toJSON(ps));
//				System.out.println(rr.getMsgformat());
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
			//需要发送多条不同的消息
			Object[] mesg = ExpressionUtil.objectValue("array("+rr.getMsgformat()+")", ps);
			if(mesg.length>0) {
				for (int i = 0; i < mesg.length; i++) {
					if(i>0) {//第二条信息是发给操作人的
						getTkTaskRemindListDao().insertTkTaskRemindList(note,subject, handler, mesg[i].toString());
					}else {//第一条信息是发给创建人或督办人的
						getTkTaskRemindListDao().insertTkTaskRemindList(note,handler, subject, mesg[i].toString());
					}
				}
			}
		}
	}
	

	private Subject getHandlerSubjects(TkTaskRemindNote note){
		String subjects = note.getOptuser();
		logger.debug("操作人列表:{}",subjects);
		if (!StringUtils.isEmpty(subjects)) {
			return JSON.parseObject(subjects, Subject.class);
		}
		//TODO 暂不实现
		return null;
	}
	
	private List<Subject> getRemindSubjects(TkTaskRemindNote note){
		String subjects = note.getNoticeusers();
		logger.debug("提醒人列表:{}",subjects);
		if (!StringUtils.isEmpty(subjects)) {
			return JSON.parseArray(subjects,Subject.class);
		}
		//TODO 暂不实现
		return new ArrayList<Subject>(0);
	}

	protected Map<String, Object> getParams(TkRemindRule rule,TkTaskRemindNote note,Subject handler,Subject subject) {
		HashMap<String, Object> ps = new HashMap<String, Object>();
		ps.put("rule", rule);
		ps.put("note", note);
		ps.put("notice", subject);
		ps.put("operator", handler);
		return ps;
	}
	
	public TkRemindRule getTkRemindRule(Long compid) throws LehandException {
		return getDutyCacheComponent().getTkRemindRule(compid,this);
	}
	
	private static final Logger logger = LogManager.getLogger(RemindRuleEnum.class);
	
	private String rulename;
	private boolean timetask;
	private RemindRuleEnum(String rulename,boolean timetask) {
		this.rulename = rulename;
		this.timetask = timetask;
	}
	
	public String getRulename() {
		return rulename;
	}

	public boolean isTimetask() {
		return timetask;
	}

	private static TkTaskDeployDao getTkTaskDeployDao() {
		return SpringUtil.getBean(TkTaskDeployDao.class);
	}
	
	private static TkMyTaskDao getTkMyTaskDao() {
		return SpringUtil.getBean(TkMyTaskDao.class);
	}
	
	private static TkMyTaskLogService getTkMyTaskLog() {
		return SpringUtil.getBean(TkMyTaskLogService.class);
	}
	
//	private static TkIntegralDetailsDao getTkIntegralDetailsDao() {
//		return SpringUtil.getBean(TkIntegralDetailsDao.class);
//	}
	
	private static TkIntegralRuleService getTkIntegralRuleDao() {
		return SpringUtil.getBean(TkIntegralRuleService.class);
	}
	
	private static TkTaskRemindListDao getTkTaskRemindListDao() {
		return SpringUtil.getBean(TkTaskRemindListDao.class);
	}
	
	private static TkCommentService getTkCommentDao() {
		return SpringUtil.getBean(TkCommentService.class);
	}
	
	private static TkCommentReplyService getTkCommentReplyDao() {
		return SpringUtil.getBean(TkCommentReplyService.class);
	}
	
	private static DutyCacheComponent getDutyCacheComponent() {
		return SpringUtil.getBean(DutyCacheComponent.class);
	}
	
//	private static TkTaskRemindNoteDao getTkTaskRemindNoteDao() {
//		return SpringUtil.getBean(TkTaskRemindNoteDao.class);
//	}
	
	
	
	public static void main(String[] args) {
//		Map<String, Object> env = new HashMap<String, Object>();
//		env = JSON.parseObject("{\"note\":{\"rulecode\":\"BACK_NEXT_BEFORE\",\"remarks6\":\"\",\"issend\":0,\"remarks5\":\"\",\"remarks4\":\"\",\"module\":1,\"remarks3\":\"\",\"mid\":11,\"optuser\":\"{\\\"subjectid\\\":0,\\\"subjectname\\\":\\\"系统管理员\\\"}\",\"noticeusers\":\"[{\\\"subjectid\\\":28,\\\"subjectname\\\":\\\"黄炳丽\\\"}]\",\"remarks2\":0,\"remarks1\":0,\"id\":178,\"remindtime\":\"2018-12-20 11:33:00\",\"mesg\":\"\"},\"nexttime\":\"2018-12-22\",\"rule\":{\"code\":\"BACK_NEXT_BEFORE\",\"seqno\":300,\"remarks3\":\"\",\"rulename\":\"在任务即将达到下一次反馈日期前，提醒反馈人，提前$day$天提醒（可以输入小数），提醒时间为：$time$\",\"newsformat\":\"\",\"params\":\"{\\\"time\\\":\\\"11:33\\\",\\\"day\\\":\\\"3.8\\\"}\",\"optuser\":\"robot\",\"msgformat\":\"'请在'+nexttime+'前及时反馈任务《’+deploy.tkname+'》的最新进展。'\",\"isview\":1,\"remarks2\":\"false\",\"remarks1\":1,\"open\":true,\"reminduser\":\"executor\",\"status\":1},\"operator\":{\"subjectname\":\"系统管理员\",\"subjectid\":0},\"mytask\":{\"subjectname\":\"黄炳丽\",\"createtime\":\"2018-12-18 00:00:00\",\"remarks6\":\"\",\"remarks5\":\"\",\"remarks4\":\"\",\"newtime\":\"\",\"remarks3\":\"\",\"subjectid\":28,\"score\":0.0,\"integral\":0.0,\"datenode\":\"2018-12-22 23:59:59\",\"remarks2\":0,\"sjtype\":0,\"progress\":0.0,\"remarks1\":0,\"id\":11,\"taskid\":3,\"status\":2},\"notice\":{\"subjectname\":\"黄炳丽\",\"subjectid\":28},\"deploy\":{\"deletetime\":\"\",\"remarks6\":\"\",\"resolve\":0,\"remarks5\":\"\",\"remarks4\":\"\",\"remarks3\":\"\",\"starttime\":\"2018-12-18 00:00:00\",\"tkname\":\"测试普通任务开始时间大于当前时间是否会立即创建\",\"tkdesc\":\"\",\"userid\":30,\"srctaskid\":-1,\"diffdata\":\"[{\\\"flag\\\":1,\\\"data\\\":\\\"2018-12-22\\\",\\\"end\\\":\\\"\\\"}]\",\"newbacktime\":\"2018-12-17 12:47:28\",\"id\":3,\"exsjnum\":2,\"createtime\":\"2018-12-17 12:47:28\",\"period\":0,\"endtime\":\"2018-12-22 23:59:59\",\"classid\":4,\"needbacktime\":\"\",\"remarks2\":0,\"progress\":0.0,\"remarks1\":0,\"updatetime\":\"\",\"status\":30,\"username\":\"潘涛\"}}", Map.class);
//		Object[] obj = (Object[])ExpressionUtil.eval("array('请在'+nexttime+'前及时反馈任务《'+deploy.tkname+'》的最新进展。')", env );
//		for(Object s : obj) {
//			System.out.println(s);
//		}
//		System.out.println(obj);
//		System.out.println(obj.getClass());
	}
}
