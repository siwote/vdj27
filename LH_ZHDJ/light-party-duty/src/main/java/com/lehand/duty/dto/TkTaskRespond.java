package com.lehand.duty.dto;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 普通任务和周期任务详情
 * 
 * @author pantao
 * @date 2018年11月15日上午9:17:27
 *
 */
public class TkTaskRespond {

	private Long taskid;// 主表任务id
	private Long mytaskid;//子表任务id
	private String tkname;// 任务名称
	private String username;// 创建人名称
	private String starttime;// 起始时间 （普通任务取主表中的开始时间，周期任务取反馈表中的开始时间）
	private String endtime;// 终止时间 （普通任务取主表中的截止时间，周期任务取反馈表中的截止时间）
	private String tkdesc;// 任务备注/要求
	private String diffdata;// 显示时间节点字段
	private List<Map<String,Object>> attachment;//附件
	private Integer days;//任务剩余天数
	private Double progress;//进度
	private Double score;//总积分
	private int status;
	private int period; 
	private int exsjnum; 
	private int resolve;
	private Long classid;//任务分类id，目前用于修改时前端值传递到修改页面
	private String classname;//任务分类名称，目前用于修改时前端值传递到修改页面
	private String shareFlag; //转发标志 0 不允许，1 允许
	private Long srctaskid;//转发原任务id
	
	private String excuteusers;// 执行人"name1,name2,name3..."
	private String excuteusersids;// 执行人"name1,name2,name3..."
	private String organname;//执行人部门
	private List<Map<String, Object>> subjects;//任务修改时用于传值
	
	private String tracker; //跟踪人
	private String trackerorganname;//跟踪人部门
	private List<Map<String, Object>> trackersubjects;//任务修改时用于传值
	
	private String leadleader; //牵头领导
	private String leadleaderorganname;//牵头领导部门
	private List<Map<String, Object>> leadleadersubjects;//任务修改时用于传值
	
	private String coordinator; //协办人
	private String coordinatororganname;//协办人部门
	private List<Map<String, Object>> coordinatorsubjects;//任务修改时用于传值
	
	private String chargeleader; //分管领导
	private String chargeleaderorganname;//分管领导部门
	private List<Map<String, Object>> chargeleadersubjects;//任务修改时用于传值
	
	private String resolvejson;//分解任务专属字段
	
	private int mystatus;
	
	private String tabulardata;
	
	private int crossFlag;//跨级1，非跨级0
	
	private String  excuteuserjson;//执行人回显json数据
	
	private String  trackerjson;//抄送人回显json数据

	private String linkNum;//包含删除人员的执行人数量

	/** 2019 -12-15  新增*/
	//任务责任联系人
	private String publisher;
	//责任人联系电话
	private String tphone;
	//附件必填 1:是 0:否
	private int annex;
    /**2021-04-25  新需求补充资料库去附件*/
    //附件集合
    private List<Map<String,Object>> files;

    public List<Map<String, Object>> getFiles() {
        return files;
    }

    public void setFiles(List<Map<String, Object>> files) {
        this.files = files;
    }

    public Long getSrctaskid() {
		return srctaskid;
	}

	public void setSrctaskid(Long srctaskid) {
		this.srctaskid = srctaskid;
	}

	public String getLinkNum() {
		return linkNum;
	}

	public void setLinkNum(String linkNum) {
		this.linkNum = linkNum;
	}

	public String getShareFlag() {
		return shareFlag;
	}

	public void setShareFlag(String shareFlag) {
		this.shareFlag = shareFlag;
	}

	public int getCrossFlag() {
		return crossFlag;
	}
	public void setCrossFlag(int crossFlag) {
		this.crossFlag = crossFlag;
	}
	public String getTabulardata() {
		return tabulardata;
	}
	public void setTabulardata(String tabulardata) {
		this.tabulardata = tabulardata;
	}
	public int getMystatus() {
		return mystatus;
	}
	public void setMystatus(int mystatus) {
		this.mystatus = mystatus;
	}
	public String getResolvejson() {
		return resolvejson;
	}
	public void setResolvejson(String resolvejson) {
		this.resolvejson = resolvejson;
	}
	public List<Map<String, Object>> getTrackersubjects() {
		return trackersubjects;
	}
	public void setTrackersubjects(List<Map<String, Object>> trackersubjects) {
		this.trackersubjects = trackersubjects;
	}
	public List<Map<String, Object>> getLeadleadersubjects() {
		return leadleadersubjects;
	}
	public void setLeadleadersubjects(List<Map<String, Object>> leadleadersubjects) {
		this.leadleadersubjects = leadleadersubjects;
	}
	public List<Map<String, Object>> getCoordinatorsubjects() {
		return coordinatorsubjects;
	}
	public void setCoordinatorsubjects(List<Map<String, Object>> coordinatorsubjects) {
		this.coordinatorsubjects = coordinatorsubjects;
	}
	public List<Map<String, Object>> getChargeleadersubjects() {
		return chargeleadersubjects;
	}
	public void setChargeleadersubjects(List<Map<String, Object>> chargeleadersubjects) {
		this.chargeleadersubjects = chargeleadersubjects;
	}
	public String getTrackerorganname() {
		return trackerorganname;
	}
	public void setTrackerorganname(String trackerorganname) {
		this.trackerorganname = trackerorganname;
	}
	public String getLeadleaderorganname() {
		return leadleaderorganname;
	}
	public void setLeadleaderorganname(String leadleaderorganname) {
		this.leadleaderorganname = leadleaderorganname;
	}
	public String getCoordinatororganname() {
		return coordinatororganname;
	}
	public void setCoordinatororganname(String coordinatororganname) {
		this.coordinatororganname = coordinatororganname;
	}
	public String getChargeleaderorganname() {
		return chargeleaderorganname;
	}
	public void setChargeleaderorganname(String chargeleaderorganname) {
		this.chargeleaderorganname = chargeleaderorganname;
	}
	public String getTracker() {
		return tracker;
	}
	public void setTracker(String tracker) {
		this.tracker = tracker;
	}
	public String getLeadleader() {
		return leadleader;
	}
	public void setLeadleader(String leadleader) {
		this.leadleader = leadleader;
	}
	public String getCoordinator() {
		return coordinator;
	}
	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}
	public String getChargeleader() {
		return chargeleader;
	}
	public void setChargeleader(String chargeleader) {
		this.chargeleader = chargeleader;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public int getExsjnum() {
		return exsjnum;
	}
	public void setExsjnum(int exsjnum) {
		this.exsjnum = exsjnum;
	}
	public int getResolve() {
		return resolve;
	}
	public void setResolve(int resolve) {
		this.resolve = resolve;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public String getOrganname() {
		return organname;
	}
	public void setOrganname(String organname) {
		this.organname = organname;
	}
	public Double getProgress() {
		return progress;
	}
	public void setProgress(Double progress) {
		this.progress = progress;
	}
	public Long getTaskid() {
		return taskid;
	}
	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}
	public Long getMytaskid() {
		return mytaskid;
	}
	public void setMytaskid(Long mytaskid) {
		this.mytaskid = mytaskid;
	}
	public String getTkname() {
		return tkname;
	}
	public void setTkname(String tkname) {
		this.tkname = tkname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getTkdesc() {
		return tkdesc;
	}
	public void setTkdesc(String tkdesc) {
		this.tkdesc = tkdesc;
	}
	public String getExcuteusers() {
		return excuteusers;
	}
	public void setExcuteusers(String excuteusers) {
		this.excuteusers = excuteusers;
	}
	public String getDiffdata() {
		return diffdata;
	}
	public void setDiffdata(String diffdata) {
		this.diffdata = diffdata;
	}
	
	public List<Map<String,Object>> getAttachment() {
		return attachment;
	}
	public void setAttachment(List<Map<String,Object>> attachment) {
		this.attachment = attachment;
	}
	public Integer getDays() {
		return days;
	}
	public void setDays(Integer days) {
		this.days = days;
	}
	public Long getClassid() {
		return classid;
	}
	public void setClassid(Long classid) {
		this.classid = classid;
	}
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	public List<Map<String, Object>> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<Map<String, Object>> subjects) {
		this.subjects = subjects;
	}
	public String getExcuteusersids() {
		return excuteusersids;
	}
	public void setExcuteusersids(String excuteusersids) {
		this.excuteusersids = excuteusersids;
	}

	public String getExcuteuserjson() {
		return excuteuserjson;
	}

	public void setExcuteuserjson(String excuteuserjson) {
		this.excuteuserjson = excuteuserjson;
	}

	public String getTrackerjson() {
		return trackerjson;
	}

	public void setTrackerjson(String trackerjson) {
		this.trackerjson = trackerjson;
	}
	
	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getTphone() {
		return tphone;
	}

	public void setTphone(String tphone) {
		this.tphone = tphone;
	}

	public int getAnnex() {
		return StringUtils.isEmpty(annex)?0:annex;
	}

	public void setAnnex(int annex) {
		this.annex = StringUtils.isEmpty(annex)?0:annex;;
	}
	
}
