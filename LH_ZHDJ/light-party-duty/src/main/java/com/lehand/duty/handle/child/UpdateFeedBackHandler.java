package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import com.lehand.duty.pojo.TkTaskFeedBackAudit;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class UpdateFeedBackHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
		Session session = data.getParam(1);
		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
		if(myTask==null) {
			LehandException.throwException("该任务已经被撤销，请返回！");
		}
		if(myTask.getStatus()==4) {
			LehandException.throwException("该任务已经被暂停，请返回！");
		}
		if(myTask.getStatus()==5) {
			LehandException.throwException("该任务已经被确认完成，请返回！");
		}
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
		if(deploy==null) {
			LehandException.throwException("该任务已经被撤销，请返回！");
		}
		if(deploy.getStatus()==45) {
			LehandException.throwException("该任务已经被暂停，请返回！");
		}
		if(deploy.getStatus()==50) {
			LehandException.throwException("该任务已经被确认完成，请返回！");
		}
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTaskLogRequest(request);
		data.setMyTask(myTask);
		data.setDeploy(deploy);

		//process
		tkMyTaskLogService.update(data.getSession().getCompid(),request);
		// 更新我的任务的进度和最新反馈时间
		tkMyTaskDao.updateProgressByID(myTask.getCompid(),request.getNow(), request.getProgress(), myTask.getId());
		// 更新任务发布表进度和最新反馈时间(排除退回的)
		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(myTask.getCompid(),myTask.getTaskid());
		avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
		tkTaskDeployDao.updateProgressAndnewbacktime(myTask.getCompid(),avgprogress, request.getNow(), myTask.getTaskid());
		
		//修改反馈能够更换审核人所以每一次修改反馈先到审核表中将该反馈的审核记录（只有一条）删除然后在新增（前提是反馈审核人被变更了）
		//通过mytaskid feedbackid status=0 来查询修改前待审核的记录，取出审核人id与修改传过来审核人id比较，相等就不删除，不相等就先删除后新增
		int num = generalSqlComponent.query(SqlCode.getTTFBAByCount1, new Object[] {request.getMytaskid(),request.getFeedbackid(),0,request.getAudituserid()});
		if(num<=0) {
			generalSqlComponent.delete(SqlCode.deleteAuditByFeedbackid, new Object[] {session.getCompid(),request.getFeedbackid()});
			TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
			audit.setCompid(session.getCompid().intValue());
			audit.setTaskid(myTask.getTaskid());
			audit.setMytaskid(myTask.getId());
			audit.setFeedbackid(request.getFeedbackid());
			audit.setSubjectId(request.getAudituserid());
			audit.setSubtype(Constant.status);//TODO 这里暂时定为0，后续根据需要再修改
			audit.setStatus(Constant.status);
			audit.setRemark(StringConstant.EMPTY);
			audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
			audit.setOpttime(StringConstant.EMPTY);
			audit.setRemarks2(Constant.status);//0代表反馈审核，1代表退回任务审核
			audit.setRemarks3(StringConstant.EMPTY);
			audit.setRemarks4(StringConstant.EMPTY);
			audit.setRemarks5(StringConstant.EMPTY);
			audit.setRemarks6(StringConstant.EMPTY);
			Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
			audit.setId(id);
		}
		
		
		
		
		
		//remind
//		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),request.getMytaskid(),request.getNow());
//		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), 
//				                     request.getFeedbackid(), processEnum.getRemindRule(), 
//									 request.getNow(), 
//									 data.getSessionSubject(), 
//									 data.getDeploySubject());
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
//		Session session = data.getParam(1);
//		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
//		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTask.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTaskLogRequest(request);
//		data.setMyTask(myTask);
//		data.setDeploy(deploy);
//	}
	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		tkMyTaskLogDao.update(data.getSession().getCompid(),request);
//		TkMyTask myTask = data.getMyTask();
//		// 更新我的任务的进度和最新反馈时间
//		tkMyTaskDao.updateProgressByID(myTask.getCompid(),request.getNow(), request.getProgress(), myTask.getId());
//		// 更新任务发布表进度和最新反馈时间(排除退回的)
//		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(myTask.getCompid(),myTask.getTaskid());
//		avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
//		tkTaskDeployDao.updateProgressAndnewbacktime(myTask.getCompid(),avgprogress, request.getNow(), myTask.getTaskid());
//		
//	}

//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		supervise(data,module,remindRule,request.getMytaskid(),request.getNow());
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, request.getFeedbackid(), remindRule, 
//				                     request.getNow(), 
//				                     data.getSessionSubject(), 
//				                     data.getDeploySubject());
//		
//	}
//
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//		
//	}
}
