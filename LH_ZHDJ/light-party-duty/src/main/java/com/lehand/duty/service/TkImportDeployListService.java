package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkImportDeployList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TkImportDeployListService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public int insert(TkImportDeployList info) {
		return generalSqlComponent.insert(SqlCode.saveImportDeployList, info);
	}
	
	public void pageByImpid(Long impid,Pager pager) {
		generalSqlComponent.pageQuery(SqlCode.queryImportDeployList, new Object[] {impid}, pager);
	}
	
	public List<TkImportDeployList> listByImpid(Long impid) {
		return generalSqlComponent.query(SqlCode.queryImportDeployList2, new Object[] {impid});
	}
	
}
