package com.lehand.duty.controller.common;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkAttachmentService;
import com.lehand.duty.pojo.TkAttachment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lehand.base.common.Message;
import com.lehand.base.exception.LehandException;
import com.lehand.components.converter.ConverterComponent;
import com.lehand.duty.controller.DutyBaseController;
import com.lehand.duty.controller.setting.TkTaskClassController;

/**
 * 
 * 附件上传  
 * 
 * @author pantao  
 * @date 2018年10月10日 下午3:16:53
 *
 */
@Api(value = "附件上传", tags = { "附件上传" })
@RestController
@RequestMapping("/duty/file")
public class TkAttachmentController extends DutyBaseController {
	
	private static final Logger logger = LogManager.getLogger(TkTaskClassController.class);
	
	private static final int MAX_PREVIEW_COUNTER = 2;
	private static final AtomicInteger PREVIEW_COUNTER = new AtomicInteger(0);
	
	@Resource private TkAttachmentService tkAttachmentService;
	
	
	/**
	 * 关联资料库的附件上传
	 * @param id
	 * @param fileid
	 * @param uuid 同一个任务或反馈共用一个uuid
	 * @param model
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "关联资料库的附件上传", notes = "关联资料库的附件上传", httpMethod = "POST")
	@RequestMapping("/newupload")
	@ResponseBody
	public Message newUplode(Long id, String fileid,String uuid, int model) {
		Message message = new Message();	
		try {
			tkAttachmentService.listAttachments(getSession(), id, fileid, uuid, model);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("附件上传出错!");
		}
		
		return message;
	}
	
	/**
	 * 新附件表附件删除
	 * @param model
	 * @param fileid
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "新附件表附件删除", notes = "新附件表附件删除", httpMethod = "POST")
	@RequestMapping("/newdelete")
	@ResponseBody
	public Message newDelete(String fileid,int model,Long businessid) {
		Message message = new Message();
		try {
			tkAttachmentService.newDelete(getSession().getCompid(),fileid,model,businessid);
			message.success();
		} catch (Exception e) {
			 e.printStackTrace();
		}
		return message;
	}
	
	
	/**
	 * 
	 * 附件查询
	 * 
	 * @author pantao  
	 * @date 2018年10月15日 下午8:37:35
	 *  
	 * @param module
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "附件查询", notes = "附件查询", httpMethod = "POST")
	@RequestMapping("/list")
	@ResponseBody
	public Message list(Integer module,Long id) {
		Message message = new Message();	
		try {
			List<Map<String , Object>> list =  tkAttachmentService.list(getSession().getCompid(),id, module);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("附件查询出错!");
		}
		
		return message;
	}
	/**
	 * 
	 * 附件删除  
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午5:27:02
	 *  
	 * @param uuid
	 * @param filename
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "附件删除", notes = "附件删除", httpMethod = "POST")
	@RequestMapping("/delete")
	@ResponseBody
	public Message delete(String uuid,String filename) {
		Message message = new Message();
		try {
			tkAttachmentService.delete(getSession().getCompid(),uuid, filename);
			message.success();
		} catch (Exception e) {
			 e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 
	 * 附件上传
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午3:38:15
	 *  
	 * @param file
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "附件上传", notes = "附件上传", httpMethod = "POST")
	@RequestMapping("/upload")
	@ResponseBody
	public Message upload(Long id,String uuid,Integer model,MultipartFile file) {
		Message message = new Message();
		try {
			if (file.isEmpty()) {
				message.setMessage("文件为空");
				return message;
			}
			//前台传过来的uuid需要判断下是否是转换过后的如果是就不再转换
			if(!StringUtils.isEmpty(uuid)) {
				List<TkAttachment> list = tkAttachmentService.list(getSession().getCompid(),uuid);
				if(list.size()<=0) {
					uuid = getUUIDByHash(uuid)+"";
				}
			}
			String filename = file.getOriginalFilename();
			File dest = new File(getFileUploadDir(uuid, model),filename);
			file.transferTo(dest);

			tkAttachmentService.insert(getSession().getCompid(),id,uuid, model,0,getFileHttpUrl(uuid, model), dest, null);
			
            Map<String, Object> map = new HashMap<String, Object>(3);
            map.put("name", filename);
            map.put("uuid", uuid);
            map.put("model", model);
            message.success().setData(map);
		} catch (Exception e) {
			String msg = "上传附件失败!";
			message.setMessage(msg);
			logger.error(msg,e);
		}
        return message;
	}
	
	/**
	 * 
	 * 附件下载 
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午3:43:53
	 *  
	 * @param request
	 * @param response
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "附件下载", notes = "附件下载", httpMethod = "POST")
	@RequestMapping("/download")
    public Message download(HttpServletRequest request, HttpServletResponse response){
		Message message = new Message();
		BufferedInputStream bis = null;
		OutputStream os = null;
		try {
			String model = request.getParameter("model");
			String uuid = request.getParameter("uuid");
			uuid = getUUIDByHash(uuid)+"";
			String filename = request.getParameter("filename");
			if (StringUtils.isEmpty(model) || StringUtils.isEmpty(uuid) || StringUtils.isEmpty(filename)) {
				message.setMessage("传参错误!");
				return message;
			}
			File file = new File(getFileUploadDir(uuid, Integer.parseInt(model)), filename);
			if (!file.exists()) {
				message.setMessage("文件不存在!");
				return message;
			}
			response.setContentType("application/force-download");//应用程序强制下载
			response.addHeader("Content-Disposition","attachment;fileName=" +  URLEncoder.encode(filename, "utf-8"));// 设置文件名               
			byte[] buffer = new byte[1024];
			bis = new BufferedInputStream(new FileInputStream(file));
			os = response.getOutputStream();
			int i = bis.read(buffer);
			while (i != -1) {
				os.write(buffer, 0, i);
				i = bis.read(buffer);
			}
			message.success();
		} catch (Exception e) {
			
		} finally {
			 if (bis != null) {
                 try {
                     bis.close();
                 } catch (IOException e1) {
                     e1.printStackTrace();
                 }
             }
			 if (os != null) {
                 try {
                	 os.flush();
                	 os.close();
                 } catch (IOException e1) {
                     e1.printStackTrace();
                 }
             }
		}
        return message;
    }
	
	/**
	 * 附件预览
	 * @param path 关于前端传路径问题需要注意凡是带有"/"的都需要进行编码与解码，倘若不进行编码解码，在eclipse中是没有问题的，一旦放到生产上使用jar启动就会识别不了path
	 * @param uuid
	 * @param filename
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "附件预览", notes = "附件预览", httpMethod = "POST")
	@RequestMapping("/preview")
	@ResponseBody
	public Message preview(String path,String uuid,String filename) {
		Message message = new Message();
		try {
			String path1 = URLDecoder.decode(path,"utf-8");
			String path2 = URLDecoder.decode(path1,"utf-8");
			String uploadDir = getUploadBaseDir();
			String targetpath = path2.replaceFirst("/file/", "/fileview/");
			File srcDir = new File(uploadDir, path2);
			File tagDir = new File(uploadDir, targetpath);
			File srcfile = new File(srcDir,filename);
			File tagfile = new File(tagDir,filename+".html");
			File cvtfile = new File(tagDir,tagfile.getName()+".cvt");
			
			if(!tagDir.exists() || !tagfile.exists()) {
				if(cvtfile.exists()) {
					message.setMessage("正在转码请稍后！");
					return message;
				}
				synchronized (PREVIEW_COUNTER) {
					int count = PREVIEW_COUNTER.get();
					if (count>=MAX_PREVIEW_COUNTER) {
						LehandException.throwException("同时预览次数超过上限,请稍后再试!");
					}
					PREVIEW_COUNTER.incrementAndGet();
				}
				ConverterComponent.convert(srcfile, tagfile);
			}
			message.setData(targetpath+tagfile.getName());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		} finally {
			PREVIEW_COUNTER.decrementAndGet();
		}
		return message;
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		String path = "/file/0/-1549842";
		String pathencode = java.net.URLEncoder.encode(path,"utf-8");
		String pathdecode = java.net.URLDecoder.decode(pathencode,"utf-8");
		System.out.println(pathdecode);
	}
}
