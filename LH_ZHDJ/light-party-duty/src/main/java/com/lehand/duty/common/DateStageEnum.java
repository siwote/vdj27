package com.lehand.duty.common;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;

import java.text.ParseException;
import java.util.*;

/**
 * 
 * 将时间周期计算成对应的具体时间点
 * 
 * @author pantao  
 * @date 2018年10月19日 上午10:51:38
 *
 */
public enum DateStageEnum {
	Y {
		@Override 
		protected List<String> create(Date start, Date end, int rate, int day) throws ParseException {
			List<String> list = new ArrayList<String>();
			Calendar cld = Calendar.getInstance();
			cld.setTime(start);
			cld.set(Calendar.MONTH, 0);
			cld.set(Calendar.DAY_OF_MONTH, 1);
			cld.add(Calendar.DAY_OF_YEAR, day-1);
			while (true) {
				cld.add(Calendar.YEAR, rate);
				cld.set(Calendar.HOUR_OF_DAY, 23);
				cld.set(Calendar.MINUTE, 59);
				cld.set(Calendar.SECOND, 59);
				if (cld.getTime().compareTo(end)>=0) {
					break;
				}
				//if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(DateEnum.addDay(end,-day))<=0) {
				if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(end)<=0) {
					list.add(DateEnum.YYYYMMDDHHMMDD.format(cld.getTime()));
				}
			} 
			return list;
		}
	},
	M {
		@Override 
		protected List<String> create(Date start, Date end, int rate, int day) throws ParseException {
			List<String> list = new ArrayList<String>();
			Calendar cld = Calendar.getInstance();
			cld.setTime(start);
			cld.set(Calendar.DAY_OF_YEAR, 1);
			cld.add(Calendar.DAY_OF_YEAR, day-1);
			cld.add(Calendar.MONTH, -1);
			while (true) {
				cld.add(Calendar.MONTH, rate);
				cld.set(Calendar.HOUR_OF_DAY, 23);
				cld.set(Calendar.MINUTE, 59);
				cld.set(Calendar.SECOND, 59);
				if (cld.getTime().compareTo(end)>=0) {
					break;
				}
				//if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(DateEnum.addDay(end,-day))<=0) {
				if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(end)<=0) {
					list.add(DateEnum.YYYYMMDDHHMMDD.format(cld.getTime()));
				}
			} 
			return list;
		}
	},
	Q {
		@Override 
		protected List<String> create(Date start, Date end, int rate, int day) throws ParseException {
			List<String> list = new ArrayList<String>();
			Calendar cld = Calendar.getInstance();
			cld.setTime(start);
			int[] q = new int[]{1,4,7,10};
			int curr = cld.get(Calendar.MONTH)+1;
			int ret = 0;
			for (int i = 0; i < q.length; i++) {
				if (curr<q[i]) {
					break;
				}
				ret = q[i];
			}
			cld.set(Calendar.MONTH, ret-1);
			cld.set(Calendar.DAY_OF_YEAR, 1);
			cld.add(Calendar.DAY_OF_YEAR, day-1);
			while (true) {
				cld.add(Calendar.MONTH, rate * 3);
				cld.set(Calendar.HOUR_OF_DAY, 23);
				cld.set(Calendar.MINUTE, 59);
				cld.set(Calendar.SECOND, 59);
				if (cld.getTime().compareTo(end)>=0) {
					break;
				}
				//if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(DateEnum.addDay(end,-day))<=0) {
				if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(end)<=0) {
					list.add(DateEnum.YYYYMMDDHHMMDD.format(cld.getTime()));
				}
			} 
			return list;
		}
	},
	W {
		@Override 
		protected List<String> create(Date start, Date end, int rate, int day) throws ParseException {
			List<String> list = new ArrayList<String>();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(start);
			calendar.setFirstDayOfWeek(1);
			calendar.set(Calendar.DAY_OF_WEEK, 1);
			calendar.add(Calendar.DAY_OF_YEAR, day);
			calendar.add(Calendar.WEEK_OF_YEAR, -1);
			while (true) {
				calendar.add(Calendar.WEEK_OF_YEAR, rate);
				calendar.set(Calendar.HOUR_OF_DAY, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				if (calendar.getTime().compareTo(end)>=0) {
					break;
				}

				//if (calendar.getTime().compareTo(start)>=0 && calendar.getTime().compareTo(DateEnum.addDay(end,-day))<=0) {
				if (calendar.getTime().compareTo(start)>=0 && calendar.getTime().compareTo(end)<=0) {
					list.add(DateEnum.YYYYMMDDHHMMDD.format(calendar.getTime()));
				}
			}
			return list;
		}
	},
	D {
		@Override 
		protected List<String> create(Date start, Date end, int rate, int day) throws ParseException {
			List<String> list = new ArrayList<String>();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(start);
			while (true) {
				calendar.add(Calendar.DAY_OF_YEAR, rate);
				calendar.set(Calendar.HOUR_OF_DAY, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				if (calendar.getTime().compareTo(end)>=0) {
					break;
				}
					if(calendar.getTime().compareTo(start)>=0 && calendar.getTime().compareTo(DateEnum.now())>=0) {
					list.add(DateEnum.YYYYMMDDHHMMDD.format(calendar.getTime()));
				}
			}			
			return list;
		}
	};


	/**
	 * 
	 * 判断逻辑确定调用具体的方法
	 * 
	 * @author pantao
	 * @date 2018年10月11日 上午10:38:27
	 * 
	 * @param start
	 * @param end
	 * @param format (1,M,1)
	 * @return
	 * @throws ParseException 
	 * @throws NumberFormatException 
	 */
	public static List<String> parse(Date start, Date end, String format) throws Exception {
		String[] data = format.split(",");
		DateStageEnum dateStageEnum = DateStageEnum.valueOf(data[1]);
		int rate = Integer.parseInt(data[0]);
		int day = 0;
		if(!data[1].toString().equals("D")) {
		   day = Integer.parseInt(data[2]);
		}
		if (rate<=0) {
			LehandException.throwException("频率不能小于等于0");
		}
		if (dateStageEnum!= DateStageEnum.D && day<=0) {
			LehandException.throwException("天数不能小于等于0");
		}
		start = DateEnum.YYYYMMDD.parse(DateEnum.YYYYMMDD.format(start));		
		return dateStageEnum.create(start, end, rate, day);
	}
	
	/**
	 * 
	 * 将时间周期计算成具体的时间点
	 * 
	 * @author pantao
	 * @date 2018年10月11日 上午10:28:18
	 * 
	 * @param start 起始日期
	 * @param end   截止日期
	 * @param rate  频率
	 * @param day
	 * @return
	 * @throws ParseException 
	 */
	protected abstract List<String> create(Date start, Date end, int rate, int day) throws ParseException;
	
	
	/**
	 * 
	 *测试代码
	 * 
	 * @author pantao  
	 * @date 2018年10月19日 上午10:40:41
	 *  
	 * @param args
	 * @throws ParseException
	 * 						 [2018-12-05 00:00:00, 2018-12-06 00:00:00]
	 */
    public static void main(String[] args) throws ParseException {
    	Date start =DateEnum.YYYYMMDD.parse("2019-01-09 00:00:00");
		Date end = DateEnum.YYYYMMDDHHMMDD.parse("2019-04-09 23:59:59");
		int day = 9;
		int rate = 1;
		List<String> list = new ArrayList<String>();
		Calendar cld = Calendar.getInstance();
		cld.setTime(start);
		cld.set(Calendar.DAY_OF_YEAR, 1);
		cld.add(Calendar.DAY_OF_YEAR, day-1);
		cld.add(Calendar.MONTH, -1);
		while (true) {
			cld.add(Calendar.MONTH, rate);	
			if (cld.getTime().compareTo(end)>=0) {
				break;
			}
			if (cld.getTime().compareTo(start)>=0 && cld.getTime().compareTo(DateEnum.addDay(end,-day))<=0) {
				list.add(DateEnum.YYYYMMDDHHMMDD.format(cld.getTime()));
			}
		}
		System.out.println(list);
	}
}
