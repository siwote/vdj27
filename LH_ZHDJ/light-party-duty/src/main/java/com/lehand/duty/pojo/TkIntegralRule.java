package com.lehand.duty.pojo;

public class TkIntegralRule extends FatherPojo {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.code
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private String code;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.rulename
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private String rulename;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.flag
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private Integer flag;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.minval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private Double minval;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.inteval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private Double inteval;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.maxval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private Double maxval;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.rateconfig
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private String rateconfig;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.status
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private Integer status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.remarks1
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private Integer remarks1;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.remarks2
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private String remarks2;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_integral_rule.remarks3
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	private String remarks3;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.code
	 * @return  the value of tk_integral_rule.code
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public String getCode() {
		return code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.code
	 * @param code  the value for tk_integral_rule.code
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.rulename
	 * @return  the value of tk_integral_rule.rulename
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public String getRulename() {
		return rulename;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.rulename
	 * @param rulename  the value for tk_integral_rule.rulename
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setRulename(String rulename) {
		this.rulename = rulename == null ? null : rulename.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.flag
	 * @return  the value of tk_integral_rule.flag
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public Integer getFlag() {
		return flag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.flag
	 * @param flag  the value for tk_integral_rule.flag
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.minval
	 * @return  the value of tk_integral_rule.minval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public Double getMinval() {
		return minval;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.minval
	 * @param minval  the value for tk_integral_rule.minval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setMinval(Double minval) {
		this.minval = minval;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.inteval
	 * @return  the value of tk_integral_rule.inteval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public Double getInteval() {
		return inteval;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.inteval
	 * @param inteval  the value for tk_integral_rule.inteval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setInteval(Double inteval) {
		this.inteval = inteval;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.maxval
	 * @return  the value of tk_integral_rule.maxval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public Double getMaxval() {
		return maxval;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.maxval
	 * @param maxval  the value for tk_integral_rule.maxval
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setMaxval(Double maxval) {
		this.maxval = maxval;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.rateconfig
	 * @return  the value of tk_integral_rule.rateconfig
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public String getRateconfig() {
		return rateconfig;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.rateconfig
	 * @param rateconfig  the value for tk_integral_rule.rateconfig
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setRateconfig(String rateconfig) {
		this.rateconfig = rateconfig == null ? null : rateconfig.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.status
	 * @return  the value of tk_integral_rule.status
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.status
	 * @param status  the value for tk_integral_rule.status
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.remarks1
	 * @return  the value of tk_integral_rule.remarks1
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public Integer getRemarks1() {
		return remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.remarks1
	 * @param remarks1  the value for tk_integral_rule.remarks1
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setRemarks1(Integer remarks1) {
		this.remarks1 = remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.remarks2
	 * @return  the value of tk_integral_rule.remarks2
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public String getRemarks2() {
		return remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.remarks2
	 * @param remarks2  the value for tk_integral_rule.remarks2
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setRemarks2(String remarks2) {
		this.remarks2 = remarks2 == null ? null : remarks2.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_integral_rule.remarks3
	 * @return  the value of tk_integral_rule.remarks3
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public String getRemarks3() {
		return remarks3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_integral_rule.remarks3
	 * @param remarks3  the value for tk_integral_rule.remarks3
	 * @mbg.generated  Mon Oct 22 20:55:10 CST 2018
	 */
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3 == null ? null : remarks3.trim();
	}
}