package com.lehand.duty.service;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkPraiseDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TkPraiseDetailsService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	public void insert(TkPraiseDetails info) {
		Long id = generalSqlComponent.insert(SqlCode.savePraiseDetails, info);
		info.setId(id);
	}
	
	public void insert(Long compid,Long taskid,Long mytaskid,Long feedid,Long userid,String username,Long optid,String optname,String rulecode,Double score) {
		TkPraiseDetails info = new TkPraiseDetails();
		info.setCompid(compid);
		info.setTaskid(taskid);
		info.setMytaskid(mytaskid);
		info.setFeedid(feedid);
		info.setUserid(userid);
		info.setUsername(username);
		info.setOptid(optid);
		info.setOptname(optname);
		info.setRulecode(rulecode);
		info.setScore(score);
		info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setRemarks1(0);
		info.setRemarks2(StringConstant.EMPTY);
		info.setRemarks3(StringConstant.EMPTY);
		insert(info);
	}
	
	/**
	 * 每条反馈记录的点赞总数
	 * @param feedid
	 * @return
	 */
	public int getNum(Long compid,Long feedid) {
		return generalSqlComponent.query(SqlCode.getPraiseDetailsNum, new Object[] {compid,feedid});
	}

	/**
	 * 获取任务点赞总积分
	 * @param taskid
	 * @return
	 */
	public TkPraiseDetails byTaskId(Long taskid) {
		return generalSqlComponent.query(SqlCode.getPraiseDetailsNumByTid, new Object[] {taskid});
	}
	
	/**
	 * 取消点赞
	 * @param feedid
	 * @param session
	 */
	public void delete(Long feedid,Session session) {
		generalSqlComponent.delete(SqlCode.cancelPraiseDetails, new Object[]{feedid,session.getUserid()});
	}
	
	/**
	 * 判断登录人是否对该任务点赞了
	 * @param feedid
	 * @param session
	 * @return
	 */
	public int getCount(Long feedid,Session session) {
		return generalSqlComponent.query(SqlCode.getPraiseDetailsByUserid, new Object[] {session.getCompid(),feedid,session.getUserid()});
	}
	
	/**
	 * 登陆人获得的总点赞数
	 * @param userid
	 * @return
	 */
	public int getSum(Long compid,Long userid) {
		return generalSqlComponent.query(SqlCode.getAllPraiseByUserid, new Object[] {compid,userid});
	}
}
