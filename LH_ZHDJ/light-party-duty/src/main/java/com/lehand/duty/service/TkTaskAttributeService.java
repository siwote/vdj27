package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.TkTaskAttributeResponse;
import com.lehand.duty.pojo.TkTaskAttribute;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TkTaskAttributeService  {


	@Resource
	private GeneralSqlComponent generalSqlComponent;

	public void save(Long compid,String code,int checked,int seqno) {
		generalSqlComponent.update(SqlCode.updateTkTaskAttribute, new Object[] {checked,seqno,compid,code});
	}

	public List<TkTaskAttribute> query2(Long compid){
		return generalSqlComponent.query(SqlCode.listTkTaskAttr, new Object[] {compid});
	}

	public void update(Long compid) {
		generalSqlComponent.update(SqlCode.updateTkTaskAttr,new Object[] {compid});
	}

	/**
	 * 保存任务属性
	 */
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
	public void save(Long compid,String data) {
		List<Map> list = JSON.parseArray(data, Map.class);
		String code="";
		int checked = 1;
		int seqno = 0;
		update(compid);
		if(list!=null && list.size()>0) {
			for (Map map : list) {
				code = map.get("code").toString();
				checked = Integer.valueOf(map.get("checked").toString());
				seqno += 1;
				save(compid,code, checked, seqno);
			}
		}
	}
	
	/**
	 * 查询任务属性
	 * @return
	 */
	public List<TkTaskAttributeResponse> query(Long compid){
		List<TkTaskAttribute> tasks = query2(compid);
		List<TkTaskAttributeResponse> lists = new ArrayList<TkTaskAttributeResponse>();
		for (TkTaskAttribute tkTaskAttribute : tasks) {
			TkTaskAttributeResponse info = new TkTaskAttributeResponse();
			info.setCode(tkTaskAttribute.getCode());
			info.setLable(tkTaskAttribute.getName());
			info.setStyle(tkTaskAttribute.getRemarks3());
			if(tkTaskAttribute.getChecked()==1) {
				info.setLabelStatus("1");
			}else {
				info.setLabelStatus("0");
			}
			if("1".equals(tkTaskAttribute.getRemarks3()) || "3".equals(tkTaskAttribute.getRemarks3())) {
				info.setPlaceholder(tkTaskAttribute.getName());
			}
			if(tkTaskAttribute.getType() == 1) {
				info.setStor("0");
				info.setShowitem(false);
			}else {
				info.setStor("1");
				info.setShowitem(true);
			}
			info.setSeqno(tkTaskAttribute.getSeqno());
			info.setChecked(tkTaskAttribute.getChecked());
			info.setSelectLable(tkTaskAttribute.getRemarks4());
			lists.add(info);
		}
		return lists;
	}
}
