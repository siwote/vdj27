package com.lehand.duty.common;

public class Constant {
	
	
	public static final int STATUS_10=10;
	
	public static final int STATUS_30=30;
	
	public static final int STATUS_40=40;
	
	public static final int STATUS_45=45;
	
	public static final int STATUS_50=50;
	
	public static final int STATUS_100=100;
	
	public static final String str_0 = "0";
	
	 public static final Long TOP_PID_DLTYPE = 0L;
	

	public static final String EMPTY = "";
	
	public static final int status = 0;
	
	public static final int statuss = -1;
	
	public static final int statusss = 1;
	
	public static final int statussss = 2;
	
	public static final int colourone = 1;//正常反馈
	public static final int colourtwo = 2;//逾期反馈
	public static final int colourthere = 3;//逾期未反馈
	public static final int colourfour = 4;//正常未反馈

	public static final int STATUS_3 = 3;

	public static final  int STATUS_2 = 2;

	public static final int STATUS_5 = 5;


	public static class TkTaskClass {

		/**
		 * 激活
		 */
		public static final int STATUS_ACTIVE = 1;

		/**
		 * 未激活
		 */
		public static final int STATUS_NO_ACTIVE = 0;

	}

	public static class TkTaskSubject {
		/**
		 * 执行人
		 */
		public static final int FLAG_EXCUTE = 0;

		/**
		 * 关注人
		 */
		public static final int FLAG_FOCUS = 1;
		/**
		 * 协同人
		 */
		public static final int FLAG_COORDINATOR = 2;
		/**
		 * 牵头领导
		 */
		public static final int FLAG_LEADLEADER = 3;
		
		/**
		 * 分管领导
		 */
		public static final int FLAG_CHARGELEADER = 4;

		//审核上级，我相关的
		public static final int FLAG_SUPERIORS = 5;

		
		/**
		 * 用户
		 */
		public static final int SJTYPE_USER = 0;

		//机构用户
		public static final int SJTYPE_ORG = 1;

		

	}
	
	public static class TkTaskDeploy {
		/**
		 * 普通任务
		 */
		public static final int PERIOD_NO = 0;

		/**
		 * 周期任务
		 */
		public static final int PERIOD_YES = 1;

		/**
		 * 不是分解任务
		 */
		public static final int RESOLVE_NO = 0;

		/**
		 * 分解任务
		 */
		public static final int PERIOD_RESOLVE = 2;
		
		
		/**
		 * 删除
		 */
		public static final int DELETE_STATUS = 0;
		/**
		 * 草稿
		 */
		public static final int DRAFT_STATUS = 1;

		/**
		 * 未签收
		 */
		public static final int NO_SIGNED_STATUS = 30;
		
		/**
		 * 已签收
		 */
		public static final int SIGNED_STATUS = 40;
		
		/**
		 * 被驳回
		 */
		public static final int BACK_STATUS = 5;
	
		/**
		 * 审核中
		 */
		public static final int NOW_STATUS = 3;
		
		/**
		 * 暂停
		 */
		public static final int SUSPENDED_STATUS = 45;
		
		/**
		 * 完成
		 */
		public static final int COMPLETE_STATUS = 50;
		
		/**
		 * 被修改
		 */
		public static final int REMARKS1_YES = 1;

		/**
		 * 未被修改
		 */
		public static final int REMARKS1_NO = 0;
		
		/**
		 * 前台传的提醒时间中固定时间标识为1，周期时间标识为2
		 */
		public static final String TIME_FLAG_1 = "1";
		
		/**
		 * 前台传的提醒时间中固定时间标识为1，周期时间标识为2
		 */
		public static final String TIME_FLAG_2 = "2";

		public static final String DRAFT_FLAG1 = "1";
		public static final String DRAFT_FLAG_0 = "2";

		public static final String CROSS_FLAG_1 = "1";
		public static final String CROSS_FLAG_0 = "0";
		//任务部署模块id
		public static final String DEPLOY_MODLE_ID = "2";




		
	}
	
	public static class TkTaskTimeNode {
//		状态(0:未创建,1:正在创建,2:创建完成)
		/**
		 * 临时
		 */
		public static final int STATUS_TEMP = -1;
		
		/**
		 * 未创建任务
		 */
		public static final int STATUS_NO_CREATE = 0;
		
		/**
		 * 正在创建中
		 */
		public static final int STATUS_CREATE_ING = 1;
		
		/**
		 * 创建完成
		 */
		public static final int STATUS_CREATE_FINISH = 2;
		
		/**
		 * 创建失败
		 */
		public static final int STATUS_CREATE_FIAL = 3;
	}
	
	public static class TkMyTask {
		
		/**
		 * 已删除
		 */
		public static final int DELETE_STATUS= 0;
		/**
		 * 未签收
		 */
		public static final int STATUS_NO_SGIN = 2;
		
		/**
		 * 已签收
		 */
		public static final int STATUS_SGIN = 3;
		
		/**
		 * 已暂停
		 */
		public static final int STATUS_SUSPEND = 4;
		
		/**
		 * 已完成
		 */
		public static final int STATUS_FINISH= 5;
		
		/**
		 * 退回
		 */
		public static final int STATUS_BACK= 6;
	}
	
	public static class TkRemindRule {
		/**
		 * 未激活
		 */
		public static final int STATUS_NO = 0;
		
		/**
		 * 激活
		 */
		public static final int STATUS_YES = 1;
	}
	
	public static class TkTaskRemindNote {
		
		/**
		 * 未发送
		 */
		public static final int SEND_NO = 0;
		
		/**
		 * 已发送
		 */
		public static final int SEND_YES = 1;
		
		/**
		 * 不能发送
		 */
		public static final int SEND_NOT = 2;
	}
	
	public static class TkTaskResolve {
		/**
		 * 新增
		 */
		public static final int ADD_YES = 0;
		
		/**
		 * 修改
		 */
		public static final int ADD_NO = 1;
	}
	public static class TkAttachments {
		public static final int MODEL_DEPLOY = 2;
		public static final int MODEL_BACK = 3;
	}


	//发布任务
	public static final String PUBLISH_TASK= "3";

	//取消任务
	public static final String CANCEL_TASK= "4";

	//换届提醒
	public static final String CHANGE_TERM_REMIND= "5";
}
