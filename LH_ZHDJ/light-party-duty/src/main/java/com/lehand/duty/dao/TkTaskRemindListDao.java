package com.lehand.duty.dao;

import com.alibaba.fastjson.JSON;
import com.aspose.slides.p2cbca448.mid;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.dto.NewMessageResponse;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.pojo.*;
import com.lehand.duty.service.TkCommentReplyService;
import com.lehand.duty.service.TkCommentService;
import com.lehand.duty.service.TkMyTaskLogService;
import com.lehand.duty.service.TkTaskRemindNoteService;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class TkTaskRemindListDao  {

	@Resource
	GeneralSqlComponent generalSqlComponent;

	private static final String update = "update tk_task_remind_list set remarks1 = 0 WHERE compid=? and noteid=? and userid=? ";
	private static final String GET_COUNT = "select count(*) from tk_task_remind_list where compid=? and isread = ? and userid = ? and now() > remind ";//2019-09-12 去掉 and noteid!=-1
	private static final String INSERT = "INSERT INTO tk_task_remind_list (compid, noteid, module, mid, remind, rulecode,rulename, optuserid, optusername, userid, username, isread, ishandle, issend, mesg, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid,:noteid,:module,:mid,:remind,:rulecode,:rulename,:optuserid,:optusername,:userid,:username,:isread,:ishandle,:issend,:mesg,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)";
	private static final String SELECT_LIST = "select noteid, mid,module,remind,optuserid,optusername,isread,mesg,remarks1,rulecode from tk_task_remind_list where compid=? and isread = ? and userid = ? and remarks1 = 1 and module !=0 order by remind desc ";
	private static final String SELECT_LIST_ALL = "select noteid, mid,module,remind,optuserid,optusername,isread,mesg,remarks1,rulecode from tk_task_remind_list where compid=? and userid = ? and remarks1 = 1 and module !=0  order by remind desc ";
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkMyTaskLogService tkMyTaskLogDao;
	@Resource private TkCommentService tkCommentDao;
	@Resource private TkCommentReplyService tkCommentReplyDao;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	
	public void insert(TkTaskRemindList info) {
//		long id = super.insert(INSERT, info);
		long id = generalSqlComponent.getDbComponent().insertReturnId(INSERT,info,null);
		info.setId(id);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void insertTkTaskRemindList(TkTaskRemindNote note, Subject handler, Subject subject, String mesg) throws LehandException {
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(note.getCompid());
		ls.setNoteid(note.getId());
		ls.setModule(note.getModule());//模块(0:任务发布,1:任务反馈)
		ls.setMid(note.getMid());//对应模块的ID
		ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format());//提醒日期
		ls.setRulecode(note.getRulecode());//编码
		ls.setRulename(RemindRuleEnum.valueOf(note.getRulecode()).getRulename());
		ls.setOptuserid(handler.getSubjectid());
		ls.setOptusername(handler.getSubjectname());
		ls.setUserid(subject.getSubjectid());//提醒人
		ls.setUsername(subject.getSubjectname());//
		ls.setIsread(0);//
		ls.setIshandle(0);//
		ls.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
		ls.setMesg(mesg);//消息
		ls.setRemarks1(1);//备注1
		ls.setRemarks2(0);//备注2
		ls.setRemarks3("工作督查");//备注3
		ls.setRemarks4(Constant.EMPTY);//备注3
		ls.setRemarks5(Constant.EMPTY);//备注3
		ls.setRemarks6(Constant.EMPTY);//备注3
		insert(ls);
		
//		WxWorkComponent wxWorkComponent = SpringUtil.getBean("app_wxWorkComponent");
//		if (null==wxWorkComponent) {
//			return;
//		}
//		Map<String, Object> user = super.getMap("select remarks3 from pw_user where compid=? and id=?", new Object[] {note.getCompid(),subject.getSubjectid()});
//		if (user==null || user.size()<=0) {
//			return;
//		}
//		String wxwork_account = String.valueOf(user.get("remarks3"));
//		if (StringUtils.isEmpty(wxwork_account)) {
//			return;
//		}
//		AccessToken token = wxWorkComponent.getAccessToken();
//		TextSendMesgRequest request = new TextSendMesgRequest();
//		request.setAgentid(wxWorkComponent.getAgentid());
//		request.setTouser(wxwork_account.trim());
//		TextSendMesgRequestBody text = new TextSendMesgRequestBody();
//		text.setContent(mesg);
//		request.setText(text);
//		wxWorkComponent.sendMessage(token.getAccess_token(), request);
	}
	
	public TkTaskRemindList getNewMessage(Long compid,Long mid,Long userid) {
//		return super.get("SELECT * FROM tk_task_remind_list where compid=? and mid=? and rulecode='GET_INTEGER' and userid=? order by remind desc limit 0,1", TkTaskRemindList.class, compid,mid,userid);
		return generalSqlComponent.getDbComponent().getBean("SELECT * FROM tk_task_remind_list where compid=? and mid=? and rulecode='GET_INTEGER' and userid=? order by remind desc limit 0,1",
				TkTaskRemindList.class,new Object[]{compid,mid,userid});
	}
	/**
	    *  查询已提醒的消息列表
	 * @author pantao
	 * @date 2018年11月13日下午1:40:16
	 *
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Pager page(int flag,Session session,Pager pager) {
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql =null;
		if(flag == 2) {
			query_sql = new StringBuilder(SELECT_LIST_ALL);	
			ps.add(session.getCompid());
			ps.add(session.getUserid());
		}else {
			query_sql = new StringBuilder(SELECT_LIST);
			ps.add(session.getCompid());
			ps.add(flag);
			ps.add(session.getUserid());
		}
//		Pager page = jdbc.pageMap(query_sql.toString(), ps.toArray(), pager.getPageNo(),pager.getPageSize());
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
		@SuppressWarnings("unchecked")
		List<Map<String,Object>> list = (List<Map<String,Object>>) pager.getRows();
		List<NewMessageResponse> lists = new ArrayList<NewMessageResponse>();
		for(Map<String,Object> mesg :list) {
			NewMessageResponse news = new NewMessageResponse();
			news.setIsread(Integer.valueOf(mesg.get("isread").toString()));
			news.setMesg(mesg.get("mesg").toString());
			news.setModule(Integer.valueOf(mesg.get("module").toString()));
			news.setOptuserid(Long.valueOf(String.valueOf(mesg.get("optuserid"))));
			news.setOptusername(mesg.get("optusername").toString());
			news.setRemarks1(Integer.valueOf(mesg.get("remarks1").toString()));
			news.setRemind(mesg.get("remind").toString());
			news.setNoteid(Long.valueOf(String.valueOf(mesg.get("noteid"))));
			news.setUserid(session.getUserid());
			Long mid = Long.valueOf(mesg.get("mid").toString());
			news.setMid(mid);
			String rulecode = mesg.get("rulecode").toString();
			news.setRulecode(rulecode);
			if( "APPLY_SUSPEND_TASK".equals(rulecode) || "BACK_END_BEFORE".equals(rulecode) || "BACK_NEXT_BEFORE".equals(rulecode) 
					|| "COMMENT_BACK".equals(rulecode) || "COMPLETE_TASK".equals(rulecode)
					|| "CUI_BAN".equals(rulecode) || "DELETE_TASK".equals(rulecode) || "FEED_PARISE".equals(rulecode)
					|| "OVERDUE_TASK".equals(rulecode) || "NEW_TASK".equals(rulecode) || "RECOVERY_TASK".equals(rulecode)
					) {
				news.setFlag(0);
			}else {
				news.setFlag(1);
			}
			Long mytaskid= 0L;
			int mystatus=0;
			Long taskid = null;
			switch (Integer.valueOf(mesg.get("module").toString())) {
			case 1://根据枚举中的值进行区分的(我的任务对应tk_my_task)
				TkMyTask mytask1 = tkMyTaskDao.get(session.getCompid(),mid);
				if(mytask1==null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					mytaskid = mid;
					taskid = mytask1.getTaskid();
					mystatus = mytask1.getStatus();
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				break;
			case 2://(我的任务明细   对应tk_my_task_log)
				TkMyTaskLog tasklog = tkMyTaskLogDao.get(session.getCompid(),mid);
				if(tasklog==null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					mytaskid = tasklog.getMytaskid();
					taskid = tasklog.getTaskid();
					TkMyTask mytask2 = tkMyTaskDao.get(session.getCompid(),mytaskid);
					if(mytask2!=null) {
						mystatus = mytask2.getStatus();
					}else {
						news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
					}
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				
				break;
			case 3://(评论   对应tk_comment)
				TkComment comment = tkCommentDao.get(session.getCompid(),mid);
				if(comment==null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					mytaskid = comment.getMytaskid();
					taskid = comment.getTaskid();
					TkMyTask mytask3 = tkMyTaskDao.get(session.getCompid(),mytaskid);
					if(mytask3!=null) {
						mystatus = mytask3.getStatus();
					}else {
						news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
					}
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				break;
			case 4://(回复  对应tk_comment_reply)
				TkCommentReply reply = tkCommentReplyDao.get(session.getCompid(),mid);
				if(reply == null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					mytaskid = reply.getMytaskid();
					taskid = reply.getTaskid();
					TkMyTask mytask4 = tkMyTaskDao.get(session.getCompid(),mytaskid);
					if(mytask4!=null) {
						mystatus = mytask4.getStatus();
					}else {
						news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
					}
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				break;
			case 6:case 7:case 13:
				//暂停完成
				TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),mid);
				if(deploy == null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					if(deploy.getPeriod()==0) {//普通任务
						TkMyTask mystasks = tkMyTaskDao.getByTaskidAndSubjectid(session.getCompid(),mid, session.getUserid());
						if(mystasks!=null) {
							mytaskid = mystasks.getId();
						}else {
							news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
						}
					}else {
						if("COMPLETE_TASK".equals(rulecode)) {
							//{"periodtime":3,"endtime":"2019-01-10 23:59:59"}
							Long noticeid = Long.valueOf(mesg.get("noteid").toString());
							TkTaskRemindNote note = tkTaskRemindNoteService.get(session.getCompid(),noticeid);
							String json = note.getRemarks4();
							if(!StringUtils.isEmpty(json)) {
								Map map = (Map)JSON.parse(json);  
								String enddate = map.get("endtime").toString();
								TkMyTask mystasks = tkMyTaskDao.getByTaskidAndSubjectidAndDatenode(session.getCompid(),mid, session.getUserid(),enddate);
								if(mystasks!=null) {
									mytaskid = mystasks.getId();
								}else {
									news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
								}
							}
						}
					}
					taskid = mid;
					news.setMytaskid(mytaskid);
					news.setTaskid(taskid);
					news.setStatus(deploy.getStatus());
					news.setMystatus(100);
					news.setPeriod(deploy.getPeriod());
					news.setExsjnum(deploy.getExsjnum());
					news.setResolve(deploy.getResolve());
				}
				break;
			case 8:case 9:case 10:
				//修改
				TkTaskDeploy deploy1 = tkTaskDeployDao.get(session.getCompid(),mid);
				if(deploy1 == null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					mytaskid = null;
					taskid = mid;
					news.setMytaskid(mytaskid);
					news.setTaskid(taskid);
					news.setStatus(deploy1.getStatus());
					news.setMystatus(0);
					news.setPeriod(deploy1.getPeriod());
					news.setExsjnum(deploy1.getExsjnum());
					news.setResolve(deploy1.getResolve());
				}
				break;
			case 11://(我的任务明细   对应tk_my_task_log)
				TkMyTaskLog tasklog2 = tkMyTaskLogDao.get(session.getCompid(),mid);
				if(tasklog2==null) {
					news.setMesg(mesg.get("mesg").toString()+"(该任务被删除)");
				}else {
					mytaskid = tasklog2.getMytaskid();
					taskid = tasklog2.getTaskid();
					news.setMytaskid(mytaskid);
					news.setTaskid(taskid);
					news.setMystatus(0);
				}
				break;
			default:
				break;
			}
			lists.add(news);
		}
		pager.setRows(lists);
		return pager;
	}

	private void shareMethod(Long compid,NewMessageResponse news, Long mytaskid, int mystatus, Long taskid) {
		TkTaskDeploy tkTaskDeploy=tkTaskDeployDao.get(compid,taskid);
		news.setMytaskid(mytaskid);
		news.setTaskid(taskid);
		news.setStatus(tkTaskDeploy.getStatus());
		if(mystatus==4 || mystatus==5) {
			news.setMystatus(100);
		}else {
			news.setMystatus(mystatus);
		}
		news.setPeriod(tkTaskDeploy.getPeriod());
		news.setExsjnum(tkTaskDeploy.getExsjnum());
		news.setResolve(tkTaskDeploy.getResolve());
	}
	
	public int getCount(int flag,Session session) {
//		return  super.getInt(GET_COUNT, session.getCompid(),flag,session.getUserid());
		return  generalSqlComponent.getDbComponent().getSingleColumn(GET_COUNT,Integer.class,new Object[]{session.getCompid(),flag,session.getUserid()});
	}

	@Transactional(rollbackFor = Exception.class)
	public void delete(Long compid,Long id,Long userid) {

//		super.delete(update,compid,id,userid);
		generalSqlComponent.getDbComponent().delete(update,new Object[]{compid,id,userid});
	}

	/**
	 * 小程序根据督办人id查询相关所有消息
	 * @param session
	 * @param pager
	 * @return
	 */
	public List<TkTaskRemindList> list(Session session, Pager pager) {
//		return super.pageBeanNotCount("SELECT mid,module,remind,optuserid,optusername,isread,mesg,remarks1,rulecode,noteid,userid FROM tk_task_remind_list where compid=? and userid=? and remarks1=1 and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME') order by remind desc", pager, TkTaskRemindList.class, session.getCompid(),session.getUserid());
		return generalSqlComponent.getDbComponent().listBean(
				"SELECT mid,module,remind,optuserid,optusername,isread,mesg,remarks1,rulecode,noteid,userid FROM tk_task_remind_list where compid=? and userid=? and remarks1=1 and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME') order by remind desc",
				TkTaskRemindList.class,new Object[]{session.getCompid(),session.getUserid()}
				);
	}
	
	/**
	 * 查询消息的总条数
	 * @param flag
	 * @param session
	 * @return
	 */
	public int count(int flag,Session session) {
		if(flag==2) {//所有
//			return super.getInt("SELECT count(*) FROM tk_task_remind_list where compid=? and userid=? and remarks1=1 and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK')", session.getCompid(),session.getUserid());
			return generalSqlComponent.getDbComponent().getSingleColumn("SELECT count(*) FROM tk_task_remind_list where compid=? and userid=? and remarks1=1 and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK')",
					int.class,
					new Object[]{session.getCompid(),session.getUserid()});
		}else if(flag == 1) {//已读
//			return super.getInt("SELECT count(*) FROM tk_task_remind_list where compid=? and isread = 1 and remarks1=1 and userid=? and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK')", session.getCompid(),session.getUserid());
			return generalSqlComponent.getDbComponent().getSingleColumn("SELECT count(*) FROM tk_task_remind_list where compid=? and isread = 1 and remarks1=1 and userid=? and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK')",
					int.class,
					new Object[]{session.getCompid(),session.getUserid()});
		}else {//未读
//			return super.getInt("SELECT count(*) FROM tk_task_remind_list where compid=? and isread = 0 and remarks1=1 and userid=? and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK')", session.getCompid(),session.getUserid());
			return generalSqlComponent.getDbComponent().getSingleColumn("SELECT count(*) FROM tk_task_remind_list where compid=? and isread = 0 and remarks1=1 and userid=? and rulecode in ('BACK_TASK','COMMENT_BACK','REPLY_COMMENT','UPDATE_BACK_TASK','OVERDUE_TASK')",
					int.class,
					new Object[]{session.getCompid(),session.getUserid()});
		}
	}

	public void update(Long compid,Long noteid) {
//		super.update("UPDATE tk_task_remind_list SET  isread = 1  WHERE compid=? and id=? and remarks1=1",compid, noteid);
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_remind_list SET  isread = 1  WHERE compid=? and id=? and remarks1=1",new Object[]{compid, noteid});
	}

	public void deleteByTaskid(Long compid, Long mid, int module) {
		generalSqlComponent.getDbComponent().delete ("delete from tk_task_remind_list where compid=? and mid = ? and module = ?", new Object[]{compid,mid,module});
	}
	
	//===================================上面为领导版小程序=====================下面为基础班小程序===============================================	
	/**
	 * 查询消息的总条数 基础版
	 * @param flag
	 * @param session
	 * @return
	 */
	public int mycount(int flag,Session session) {
		if(flag==2) {//所有
//			return super.getInt("SELECT count(*) FROM tk_task_remind_list where userid=? and remarks1=1 and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','OVERDUE_TASK','GET_INTEGER')", session.getUserid());
			return generalSqlComponent.getDbComponent().getSingleColumn("SELECT count(*) FROM tk_task_remind_list where userid=? and remarks1=1 and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','OVERDUE_TASK','GET_INTEGER')",
					int.class,
					new Object[]{session.getUserid()});
		}else if(flag == 1) {//已读
//			return super.getInt("SELECT count(*) FROM tk_task_remind_list where isread = 1 and remarks1=1 and userid=? and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','OVERDUE_TASK','GET_INTEGER')", session.getUserid());
			return  generalSqlComponent.getDbComponent().getSingleColumn("SELECT count(*) FROM tk_task_remind_list where isread = 1 and remarks1=1 and userid=? and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','OVERDUE_TASK','GET_INTEGER')",
					int.class,
					new Object[]{session.getUserid()});
		}else {//未读
//			return super.getInt("SELECT count(*) FROM tk_task_remind_list where isread = 0 and remarks1=1 and userid=? and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','OVERDUE_TASK','GET_INTEGER')", session.getUserid());
			return generalSqlComponent.getDbComponent().getSingleColumn("SELECT count(*) FROM tk_task_remind_list where isread = 0 and remarks1=1 and userid=? and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','OVERDUE_TASK','GET_INTEGER')",
					int.class,
					new Object[]{session.getUserid()});
		}
	}
	
	
	/**
	 * 小程序根据督办人id查询相关所有消息 基础版
	 * @param session
	 * @param pager
	 * @return
	 */
	public List<TkTaskRemindList> mylist(Session session, Pager pager) {
//		return super.pageBeanNotCount("SELECT mid,module,remind,optuserid,optusername,isread,mesg,remarks1,rulecode,noteid,userid FROM tk_task_remind_list where compid=? and userid=? and remarks1=1 and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','OVERDUE_TASK','BACK_TASK','GET_INTEGER','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','RECOVERY_TASK','SIGN_TASK','RETURN_TASK','UPDATE_BACK_TASK') order by remind desc", pager, TkTaskRemindList.class, session.getCompid(),session.getUserid());
		return generalSqlComponent.getDbComponent().listBean(
			"SELECT mid,module,remind,optuserid,optusername,isread,mesg,remarks1,rulecode,noteid,userid FROM tk_task_remind_list where compid=? and userid=? and remarks1=1 and rulecode in ('APPLY_SUSPEND_TASK','BACK_END_BEFORE','BACK_NEXT_BEFORE','COMMENT_BACK','COMPLETE_TASK','CUI_BAN','DELETE_TASK','FEED_PARISE','OVERDUE_TASK','BACK_TASK','GET_INTEGER','NEW_TASK','MODIFY_TASK_BASIC','MODIFY_TASK_SUBJECT','MODIFY_TASK_TIME','RECOVERY_TASK','SIGN_TASK','RETURN_TASK','UPDATE_BACK_TASK') order by remind desc"	,
				TkTaskRemindList.class,
				new Object[]{session.getCompid(),session.getUserid()}
		);
	}

	/**
	 * 一键已读全部消息
	 * @param userid
	 */
	public void updateByUserid(Long compid,Long userid) {
//		super.update("UPDATE tk_task_remind_list SET isread = 1 WHERE compid=? and isread=0 and userid=?", compid,userid);
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_remind_list SET isread = 1 WHERE compid=? and isread=0 and userid=?",new Object[]{compid,userid});

	}
	
	
}
