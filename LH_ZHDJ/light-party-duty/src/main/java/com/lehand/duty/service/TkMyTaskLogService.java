package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkMyTaskLog;
import com.lehand.duty.pojo.TkTaskDeploy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TkMyTaskLogService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public int update(Long compid, MyTaskLogRequest request) {
		return generalSqlComponent.update(SqlCode.updateMyTaskLog2, new Object[] {request.getMessage(),request.getProgress(),request.getUuid(),request.getSynchronizationTime(),compid,request.getFeedbackid()});
	}
	
	private TkMyTaskLog insert(TkMyTaskLog info) {
		Long id = generalSqlComponent.insert(SqlCode.addTMTL, info);
		//Long id = super.insertRetrunID(INSERT, info);
		info.setId(id);
		return info;
	}
	
	public TkMyTaskLog insert(Long compid, TkTaskDeploy deploy, TkMyTask myTask, MyTaskLogRequest request, TaskProcessEnum process, Session session, String remarks3, String remarks4, int remarks2) {
		TkMyTaskLog info = new TkMyTaskLog();
		info.setCompid(compid);
		info.setTaskid(myTask.getTaskid());
		info.setMytaskid(request.getMytaskid());
		info.setMesg(request.getMessage());//	反馈消息
		info.setFileuuid(request.getUuid());//附件UUID
		info.setProgress(request.getProgress());//	反馈进度值
		info.setOpttime(request.getNow());//	创建时间
		info.setOpttype(process.name());//	操作类型(0:签收,1:退回,2:反馈)
		info.setCommcount(0);//	评论数量
		info.setUserid(session.getUserid());//	
		info.setUsername(session.getUsername());
		info.setUpdatetime(request.getSynchronizationTime());//	更新时间
		info.setIntegral(0);//	积分
		info.setRemarks1(deploy.getPeriod());//	任务类别 0 普通任务，1 周期任务，2 分解任务
		info.setRemarks2(remarks2);//	备注2
		info.setRemarks3(remarks3);//	备注3
		info.setRemarks4(remarks4);//	备注3
		info.setRemarks5(Constant.EMPTY);//	备注3
		info.setRemarks6(Constant.EMPTY);//	备注3
		info.setFeedbackPerson(request.getFeedbackPerson());
		info.setFeedbackPhone(request.getFeedbackPhone());
		insert(info);
		return info;
	}
	
	/**
	 * 获取反馈的消息
	 * @param id
	 * @return
	 */
	public TkMyTaskLog get(Long compid,Long id) {
		return generalSqlComponent.query(SqlCode.getMyTaskLog,  new Object[] {compid,id});
	}
	
	
	public List<TkMyTaskLog> list(Long compid,Long mytaskid,Pager pager){
		Pager page =  generalSqlComponent.pageQuery(SqlCode.queryTkMyTaskLogList, new Object[] {compid,mytaskid}, pager);
		PagerUtils pagerUtils = new PagerUtils(page);
		return pagerUtils.getRows();
	}

	public int count(Long compid,Long myid) {
		return generalSqlComponent.query(SqlCode.getMyTaskpagetotle, new Object[] { compid,myid});
	}
	
	public void updateId(Long compid,Long id) {
		generalSqlComponent.update(SqlCode.updateMyTaskLog, new Object[] {compid,id});
	}

	/**
	 * 判断有没有反馈
	 * @param myid
	 * @return
	 */
	public int listFeedBack(Long compid, Long myid) {
		return generalSqlComponent.query(SqlCode.queryMyTaskLogFbCount,new Object[] {compid, myid,"FEED_BACK"});
	}
	
	/**
	 * 判断有没有反馈
	 * 针对积分设置中超过提醒时间没反馈的就扣分的sql语句
	 * @param myid
	 * @param mintime
	 * @param maxtime
	 * @return
	 */
	public int listFeedBack(Long compid,Long myid,String mintime,String maxtime) {
		return generalSqlComponent.query(SqlCode.getMTfeedbackcount, new Object[] {compid, myid,"FEED_BACK",mintime,maxtime});
	}
	
	/**
	 * 根据taskid和mytaskid
	 * 获取状态为签收的操作时间
	 * @param taskid
	 * @param mytaskid
	 * @return
	 */
	public TkMyTaskLog getSignTime(Long compid,Long taskid,Long mytaskid){
		TkMyTaskLog tkMyTaskLog = generalSqlComponent.query(SqlCode.queryMyTaskSignTime, new Object[] { compid,taskid,mytaskid});
		return tkMyTaskLog;
	}
	
	/**
	 * 新增反馈时要将之前反馈过的最新进度查出来并显示
	 * @param mytaskid
	 * @return
	 */
	public TkMyTaskLog getProgress(Long compid,Long mytaskid) {
		return generalSqlComponent.query(SqlCode.queryMyTaskProcess,  new Object[] {compid,mytaskid});
	}

	public void deleteByTaskid(Long compid,Long taskid) {
		generalSqlComponent.delete(SqlCode.delMyTaskLogByTid, new Object[] {compid,taskid});
	}

	public List<TkMyTaskLog> listByTaskid(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.queryMytaskByTid, new Object[] {compid,taskid});
	}
	
	
	public void deleteByMyTaskid(Long compid,Long taskid,Long userid) {
		generalSqlComponent.delete(SqlCode.delMyTaskByTid, new Object[] {compid,taskid,userid});
	}

	public void deleteByStatus(Long compid,Long taskid,Long userid) {
		generalSqlComponent.delete(SqlCode.delMyTaskByStatus,new Object[] { compid,taskid,userid});
	}

	public TkMyTaskLog getBean(Long compid,Long taskid, String endtime) {
		return generalSqlComponent.query(SqlCode.getMyTaskLogBean, new Object[] {compid,taskid,endtime});
	}

	public void updateUuid(Long compid,String uuid ,Long taskid, String endtime) {
		generalSqlComponent.update(SqlCode.updatMyTaskUUID, new Object[] {uuid,compid,taskid,endtime});
	}
}
