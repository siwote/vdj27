package com.lehand.duty.pojo;

//TODO 待改造  资料库分类授权
public class PwAuthOtherMap extends FatherPojo{

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.subjecttp
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Integer subjecttp;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.subjectid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Long subjectid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.module
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Integer module;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.bizid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Long bizid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.cuserid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Long cuserid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.createtime
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private String createtime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.remarks1
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Integer remarks1;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.remarks2
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private Integer remarks2;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.remarks3
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private String remarks3;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column pw_auth_other_map.remarks4
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	private String remarks4;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.subjecttp
	 * @return  the value of pw_auth_other_map.subjecttp
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Integer getSubjecttp() {
		return subjecttp;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.subjecttp
	 * @param subjecttp  the value for pw_auth_other_map.subjecttp
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setSubjecttp(Integer subjecttp) {
		this.subjecttp = subjecttp;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.subjectid
	 * @return  the value of pw_auth_other_map.subjectid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Long getSubjectid() {
		return subjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.subjectid
	 * @param subjectid  the value for pw_auth_other_map.subjectid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setSubjectid(Long subjectid) {
		this.subjectid = subjectid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.module
	 * @return  the value of pw_auth_other_map.module
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Integer getModule() {
		return module;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.module
	 * @param module  the value for pw_auth_other_map.module
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setModule(Integer module) {
		this.module = module;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.bizid
	 * @return  the value of pw_auth_other_map.bizid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Long getBizid() {
		return bizid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.bizid
	 * @param bizid  the value for pw_auth_other_map.bizid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setBizid(Long bizid) {
		this.bizid = bizid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.cuserid
	 * @return  the value of pw_auth_other_map.cuserid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Long getCuserid() {
		return cuserid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.cuserid
	 * @param cuserid  the value for pw_auth_other_map.cuserid
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setCuserid(Long cuserid) {
		this.cuserid = cuserid ;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.createtime
	 * @return  the value of pw_auth_other_map.createtime
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public String getCreatetime() {
		return createtime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.createtime
	 * @param createtime  the value for pw_auth_other_map.createtime
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setCreatetime(String createtime) {
		this.createtime = createtime == null ? null : createtime.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.remarks1
	 * @return  the value of pw_auth_other_map.remarks1
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Integer getRemarks1() {
		return remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.remarks1
	 * @param remarks1  the value for pw_auth_other_map.remarks1
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setRemarks1(Integer remarks1) {
		this.remarks1 = remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.remarks2
	 * @return  the value of pw_auth_other_map.remarks2
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public Integer getRemarks2() {
		return remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.remarks2
	 * @param remarks2  the value for pw_auth_other_map.remarks2
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setRemarks2(Integer remarks2) {
		this.remarks2 = remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.remarks3
	 * @return  the value of pw_auth_other_map.remarks3
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public String getRemarks3() {
		return remarks3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.remarks3
	 * @param remarks3  the value for pw_auth_other_map.remarks3
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3 == null ? null : remarks3.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column pw_auth_other_map.remarks4
	 * @return  the value of pw_auth_other_map.remarks4
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public String getRemarks4() {
		return remarks4;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column pw_auth_other_map.remarks4
	 * @param remarks4  the value for pw_auth_other_map.remarks4
	 * @mbg.generated  Tue Dec 04 11:14:24 CST 2018
	 */
	public void setRemarks4(String remarks4) {
		this.remarks4 = remarks4 == null ? null : remarks4.trim();
	}
}