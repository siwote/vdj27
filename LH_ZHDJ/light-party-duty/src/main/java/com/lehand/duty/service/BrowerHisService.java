package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dao.*;
import com.lehand.duty.dto.*;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

@Service
public class BrowerHisService  {

	private static final String list = "select c.id id,c.tkname tkname,c.status status,c.progress progress,c.period period,c.subjectname subjectname,d.newtime newtime,d.newmesg newmesg from "
			+" (select a.id id,a.tkname tkname,a.status status,a.progress progress,a.period period,b.subjectname subjectname,a.newbacktime newbacktime,a.createtime createtime,a.updatetime updatetime,a.endtime endtime,a.classid classid,a.starttime starttime from tk_task_deploy a left join "
			 +" (select taskid, GROUP_CONCAT(subjectname) subjectname from tk_task_subject  where compid=? and flag=0 and remarks1!=-1 group by taskid)b on a.id=b.taskid and a.compid=?) c left join tk_task_deploy_sub d on c.id = d.id  where d.compid=? and c.classid in (SELECT bizid classid FROM urc_auth_other_map where compid=? and subjecttp=0 and module=0 and subjectid=?) and c.status!=0 ";
	
	private static final String SELECT_LIST_SQL="select a.id,a.taskid,d.period,d.tkname,d.userid,d.username,d.classid,d.clname,a.datenode,a.status from "
			+" tk_my_task a left join (select b.id id, b.userid userid,b.username username,b.classid classid,b.tkname tkname,c.clname clname,b.period period from "
			+" tk_task_deploy b left join tk_task_class c on b.classid = c.id and b.compid=? and c.compid=? )d on d.id=a.taskid where a.compid=? and a.subjectid = ?  ";
	@Resource private TkTaskClassDao tkTaskClassDao;
	@Resource private TkIntegralDetailsService tkIntegralDetailsService;
	@Resource private TkCommentService tkCommentDao;
	@Resource private TkCommentReplyService tkCommentReplyDao;
	@Resource private TkTaskRemindListDao tkTaskRemindListDao;;
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkMyTaskLogService tkMyTaskLogDao;
	@Resource private TkPraiseDetailsService tkPraiseDetailsDao;
	@Resource private TkAttachmentService tkAttachmentService;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	/**
	 * 快速查询(默认带上后台授权的任务分类id)
	 * @author pantao
	 * @date 2018年11月21日下午3:49:25
	 *
	 * @param time （"1,M,1"）最近一个月有进展；（"1,M,2"）最近一个月新任务；（"1,M,3"）最近一个月刚完成；（"1,M,4"）最近一个月逾期无进展
	 */
	public void page(Session session , String time, Pager pager) {
		Calendar calSmall = Calendar.getInstance();
		calSmall.setTime(calSmall.getTime());
		String[] times = time.split(",");
		int num =Integer.valueOf(times[0]) ;
		String date = times[1];
		int equno = Integer.valueOf(times[2]) ;
		if ("Y".equals(date)) {
			calSmall.set(Calendar.YEAR, calSmall.get(Calendar.YEAR) - num);
		} else if ("M".equals(date)) {
			calSmall.set(Calendar.MONTH, calSmall.get(Calendar.MONTH) - num);
		}
	    String timeStr = DateEnum.YYYYMMDDHHMMDD.format(calSmall.getTime());
	    List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = new StringBuilder(list); 
		ps.add(session.getCompid());
		ps.add(session.getCompid());
		ps.add(session.getCompid());
		ps.add(session.getCompid());
		ps.add(session.getUserid());
		switch (equno) {
		case 1://最近有新进展
			query_sql.append(" and c.newbacktime >= ? and c.status <> ? and c.status <> 0 and c.period!=2 order by c.newbacktime desc ");
			ps.add(timeStr);
			ps.add(Constant.TkTaskDeploy.NO_SIGNED_STATUS);
			break;
		case 2://近期新任务
			query_sql.append(" and c.createtime >= ? and c.status <> ? and c.status <> 0 and c.period!=2  order by c.createtime desc ");
			ps.add(timeStr);
			ps.add(Constant.TkTaskDeploy.NO_SIGNED_STATUS);
			break;
		case 3://近期新完成
			query_sql.append(" and c.updatetime >= ? and c.status = ? and c.period!=2  order by c.updatetime desc ");
			ps.add(timeStr);
			ps.add(Constant.TkTaskDeploy.COMPLETE_STATUS);
			break;
		case 4://逾期无进展
			query_sql.append(" and c.endtime < ? and c.status = ? and c.period!=2  and NOT EXISTS (select 1 from tk_my_task_log h where c.id=h.taskid and h.opttype='FEED_BACK') order by c.endtime desc ");
			ps.add(DateEnum.YYYYMMDDHHMMDD.format());
			ps.add(Constant.TkTaskDeploy.SIGNED_STATUS);
			break;
		default:
			break;
		}
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}

	/**
	 * 详细查询
	 * @author pantao
	 * @date 2018年11月21日下午6:08:35
	 *
	 * @param request
	 * @param pager
	 * @throws LehandException 
	 */
	public void page(Session session , TkTaskRequest request, Pager pager) throws LehandException {
		String nowTime = DateEnum.YYYYMMDDHHMMDD.format();
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = new StringBuilder(list+" and status <> ? and c.period!=2 ");
		ps.add(session.getCompid());
		ps.add(session.getCompid());
		ps.add(session.getCompid());
		ps.add(session.getCompid());
		ps.add(session.getUserid());//248
		ps.add(Constant.TkTaskDeploy.NO_SIGNED_STATUS);//30
		if(!StringUtils.isEmpty(request.getClassid())) {
			query_sql.append(" and c.classid = ? ");
			ps.add(request.getClassid());
		}else {
			//如果传过来的分类id为空，那就查询所有的已授权的分类
			List<TkTaskClass> listClass =tkTaskClassDao.listAuthClass(session.getCompid(),session.getUserid());
			String classid = "";
			if(listClass!=null && listClass.size()>0) {
				for (TkTaskClass map : listClass) {
					classid += map.getId().toString()+",";
				}
			}
			if(!"".equals(classid)) {
				classid = classid.substring(0, classid.length()-1);//1,3,2
			}else {
				LehandException.throwException("您没有分类授权,请联系管理员进行分类授权！");
			}		
			query_sql.append(" and c.classid in ("+classid+") ");

		}
		if (!StringUtils.isEmpty(request.getSubjectid())) {
			query_sql.append(" and c.id in (select taskid from tk_task_subject where subjectid in ("+request.getSubjectid()+") )");
		}
		if(!StringUtils.isEmpty(request.getStatus())) {
			if(request.getStatus()==100) {
				Integer status1 = 45;
				Integer status2 = 50;
				query_sql.append(" and (c.status = ? or c.status = ?) ");
				ps.add(status1);
				ps.add(status2);
			}else if(request.getStatus()==1000) {//查所有状态40，45，50
				Integer status1 = 45;
				Integer status2 = 50;
				Integer status3 = 40;
				query_sql.append(" and (c.status = ? or c.status = ? or c.status = ?) ");
				ps.add(status1);
				ps.add(status2);
				ps.add(status3);
			}else {
				query_sql.append(" and c.status = ? ");
				ps.add(request.getStatus());//40
			}
		}
		if(!StringUtils.isEmpty(request.getStarttime())) {
			query_sql.append(" and c.starttime >= ? and c.starttime <= ?");
			String date = request.getStarttime()+" 00:00:00";
			Date date1;
			String date2= "";
			try {
				date1 = DateEnum.YYYYMMDDHHMMDD.parse(date);
				date2 = DateEnum.YYYYMMDDHHMMDD.format(date1);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			ps.add(date2);
			ps.add(nowTime);
		}
		if(!StringUtils.isEmpty(request.getEndtime())) {
			query_sql.append(" and c.endtime >= ? and c.endtime <= ?");
			ps.add(nowTime);
			String date = request.getEndtime()+" 23:59:59";
			Date date1;
			String date2= "";
			try {
				date1 = DateEnum.YYYYMMDDHHMMDD.parse(date);
				date2 = DateEnum.YYYYMMDDHHMMDD.format(date1);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			ps.add(date2);
		}
		if(!StringUtils.isEmpty(request.getTkname())) {
			String likeStr1 = request.getTkname().replaceAll("\'", "");
			query_sql.append(" and c.tkname like '%"+likeStr1+"%'");//55
		}
		
		query_sql.append(" order by c.createtime desc");
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}

	/**
	 *    任务分类集合查询（根据授权的分类来）
	 * @author pantao
	 * @date 2018年11月23日下午4:28:40
	 *
	 * @return
	 */
	public List<TkTaskClass> list(Session session) {
		List<TkTaskClass> list = tkTaskClassDao.list(session);
		return list;
	}

	/**
	 * 浏览历史列表
	 * @author pantao
	 * @date 2018年11月23日下午6:00:11
	 *
	 * @param userid
	 * @return
	 */
	public List<BrowerHisResponse> listHis(Long userid, Pager pager) {
		List<BrowerHisResponse> list = list(userid,pager);
		return list;
	}

	public List<BrowerHisResponse> list(Long userid, Pager pager) {
		Pager page = generalSqlComponent.pageQuery(SqlCode.browserhislikethis, new Object[] {userid}, pager);
		PagerUtils<BrowerHisResponse> pagerUtils = new PagerUtils<BrowerHisResponse>(page);
		return pagerUtils.getRows();
	}
	
	/**
	 * 列表删除
	 * @author pantao
	 * @date 2018年11月27日上午9:18:36
	 *
	 * @param userid
	 * @param model
	 * @param modelid
	 * @param time
	 */
	@Transactional(rollbackFor = Exception.class)
	public void delete(Long compid,Long userid, int model, Long modelid, String time) {
		generalSqlComponent.delete(SqlCode.browserhisDelete, new Object[] {compid,userid,model,modelid,time});
	}

	/**
	 * 统计条数
	 * @author pantao
	 * @date 2018年11月27日上午9:18:53
	 * @param userid
	 * @return
	 */
	public int count(Long compid,Long userid) {
	    return generalSqlComponent.query(SqlCode.browserhisCount, new Object[] {compid,userid});
	}

	/**
	 * 积分列表(这里暂时这样后续再优化)
	 * @author pantao
	 * @date 2018年11月28日上午10:49:51
	 *
	 * @param time
	 * @throws LehandException 
	 */
	public List<TkIntegralResponse> listIntegral(Long compid, String time, Pager pager) throws LehandException {
		if(!StringUtils.isEmpty(time)) {
			String date = getstartandend(time);
			String[] times = date.split(",");
			String starttime = times[0];
			String endtime = times[1];
			return tkIntegralDetailsService.pageList(compid,starttime,endtime,pager);
		}
		return null;
	}
	

	/**
	 * 计算总数
	 * @author pantao
	 * @date 2018年11月30日下午4:58:16
	 *
	 * @param time
	 * @param compid
	 * @return
	 * @throws LehandException 
	 */
	public int countIntegral(Long compid,String time) throws LehandException {
		if(!StringUtils.isEmpty(time)) {
			String date = getstartandend(time);
			String[] times = date.split(",");
			String starttime = times[0];
			String endtime = times[1];
			List<TkIntegralDetails> list = tkIntegralDetailsService.list(compid,starttime,endtime);
			return list.size();
		}
		return 0;
	}	
	
	private String getstartandend(String time) throws LehandException {
		String[] times = time.split("-");
		String starttime = "";
		String endtime = "";
		String b = " 23:59:59";
		String a = " 00:00:00";
		int year =Integer.parseInt(times[0]) ;//年份 2018
		int quarter = Integer.parseInt(times[1]);//季度 0（所有季度），1，2，3，4
		int month = Integer.parseInt(times[2]);//月份0（所有月）
		if(quarter == 0 && month == 0) {//第一种：2018-0-0  全年
			starttime=year+"-01-01"+a;
			endtime=year+"-12-31"+b;
		}else if(quarter != 0 && month == 0) {//第二种：2018-1-0 全季度
			switch (quarter) {
			case 1:
				starttime = year+"-01-01"+a;
				endtime = year+"-03-31"+b;
				break;
			case 2:
				starttime = year+"-04-01"+a;
				endtime = year+"-06-30"+b;
				break;
			case 3:
				starttime = year+"-07-01"+a;
				endtime = year+"-09-30"+b;
				break;
			case 4:
				starttime = year+"-10-01"+a;
				endtime = year+"-12-31"+b;
				break;
			default:
				break;
			}
		}else if(quarter != 0 && month != 0) {//第三种：2018-1-1 全月
			if(month < 10) {
				starttime = year+"-0"+month+"-01"+a;
				endtime = year+"-0"+month+"-28"+b;
			}else {
				starttime = year+"-"+month+"-01"+a;
				endtime = year+"-"+month+"-28"+b;
			}
		}else {
			LehandException.throwException("参数错误！");
		}
		return starttime+","+endtime;
	}

	/**
	 * 小程序消息列表查询 type 0表示领导版  1表示基础版
	 */
	@SuppressWarnings("rawtypes")
	public List<NewMessageResponse> page(int type, int flag, Session session, Pager pager) {
		List<TkTaskRemindList> lists = null;
		List<NewMessageResponse> newmsg = new ArrayList<NewMessageResponse>();
		if(type==0) {
		    lists = tkTaskRemindListDao.list(session,pager);
		}else {
			lists = tkTaskRemindListDao.mylist(session,pager);
		}
		
		if(lists==null || lists.size()<=0) {
			return newmsg;
		}
		for(TkTaskRemindList msg : lists) {
			NewMessageResponse news = new NewMessageResponse();
			news.setIsread(msg.getIsread());
			news.setMesg(msg.getMesg());
			news.setModule(msg.getModule());
			news.setOptuserid(msg.getOptuserid());
			news.setOptusername(msg.getOptusername());
			news.setRemarks1(msg.getRemarks1());
			news.setRemind(msg.getRemind());
			news.setMid(msg.getMid());
			news.setUserid(msg.getUserid());
			news.setNoteid(msg.getNoteid());
			news.setRulecode(msg.getRulecode());
			Long mid = msg.getMid();
			Long mytaskid= null;
			int mystatus=0;
			Long taskid = null;
			String rulecode = msg.getRulecode();
			switch (msg.getModule()) {
			case 1://根据枚举中的值进行区分的(我的任务对应tk_my_task)
				TkMyTask mytask1 = tkMyTaskDao.get(session.getCompid(),mid);
				if(mytask1==null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					mytaskid = mid;
					taskid = mytask1.getTaskid();
					mystatus = mytask1.getStatus();
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				break;
			case 2://(我的任务明细   对应tk_my_task_log)
				TkMyTaskLog tasklog = tkMyTaskLogDao.get(session.getCompid(),mid);
				if(tasklog==null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					mytaskid = tasklog.getMytaskid();
					taskid = tasklog.getTaskid();
					TkMyTask mytask2 = tkMyTaskDao.get(session.getCompid(),mytaskid);
					if(mytask2!=null) {
						mystatus = mytask2.getStatus();
					}else {
						news.setMesg(msg.getMesg().toString()+"(该任务被删除)");
					}
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				
				break;
			case 3://(评论   对应tk_comment)
				TkComment comment = tkCommentDao.get(session.getCompid(),mid);
				if(comment==null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					mytaskid = comment.getMytaskid();
					taskid = comment.getTaskid();
					TkMyTask mytask3 = tkMyTaskDao.get(session.getCompid(),mytaskid);
					if(mytask3!=null) {
						mystatus = mytask3.getStatus();
					}else {
						news.setMesg(msg.getMesg().toString()+"(该任务被删除)");
					}
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				break;
			case 4://(回复  对应tk_comment_reply)
				TkCommentReply reply = tkCommentReplyDao.get(session.getCompid(),mid);
				if(reply == null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					mytaskid = reply.getMytaskid();
					taskid = reply.getTaskid();
					TkMyTask mytask4 = tkMyTaskDao.get(session.getCompid(),mytaskid);
					if(mytask4!=null) {
						mystatus = mytask4.getStatus();
					}else {
						news.setMesg(msg.getMesg()+"(该任务被删除)");
					}
					shareMethod(session.getCompid(),news, mytaskid, mystatus, taskid);
				}
				break;
			case 6:case 7:
				//暂停完成
				TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),mid);
				if(deploy == null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					if(deploy.getPeriod()==0) {//普通任务
						TkMyTask mystasks = tkMyTaskDao.getByTaskidAndSubjectid(session.getCompid(),mid, session.getUserid());
						if(mystasks!=null) {
							mytaskid = mystasks.getId();
						}else {
							news.setMesg(msg.getMesg()+"(该任务被删除)");;
						}
					}else {
						if("COMPLETE_TASK".equals(rulecode)) {
							//{"periodtime":3,"endtime":"2019-01-10 23:59:59"}
							Long noticeid = Long.valueOf(msg.getNoteid());
							TkTaskRemindNote note = tkTaskRemindNoteService.get(session.getCompid(),noticeid);
							String json = note.getRemarks4();
							if(!StringUtils.isEmpty(json)) {
								Map map = (Map)JSON.parse(json);  
								String enddate = map.get("endtime").toString();
								TkMyTask mystasks = tkMyTaskDao.getByTaskidAndSubjectidAndDatenode(session.getCompid(),mid, session.getUserid(),enddate);
								if(mystasks!=null) {
									mytaskid = mystasks.getId();
								}else {
									news.setMesg(msg.getMesg()+"(该任务被删除)");
								}
								mytaskid = mystasks.getId();
							}
						}
					}
					taskid = mid;
					news.setMytaskid(mytaskid);
					news.setTaskid(taskid);
					news.setStatus(deploy.getStatus());
					news.setMystatus(100);
					news.setPeriod(deploy.getPeriod());
					news.setExsjnum(deploy.getExsjnum());
					news.setResolve(deploy.getResolve());
				}
				break;
			case 8:case 9:case 10:
				//修改
				TkTaskDeploy deploy1 = tkTaskDeployDao.get(session.getCompid(),mid);
				if(deploy1 == null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					mytaskid = null;
					taskid = mid;
					news.setMytaskid(mytaskid);
					news.setTaskid(taskid);
					news.setStatus(deploy1.getStatus());
					news.setMystatus(0);
					news.setPeriod(deploy1.getPeriod());
					news.setExsjnum(deploy1.getExsjnum());
					news.setResolve(deploy1.getResolve());
				}
				break;
			case 11://(我的任务明细   对应tk_my_task_log)
				TkMyTaskLog tasklog2 = tkMyTaskLogDao.get(session.getCompid(),mid);
				if(tasklog2==null) {
					news.setMesg(msg.getMesg()+"(该任务被删除)");
				}else {
					mytaskid = tasklog2.getMytaskid();
					taskid = tasklog2.getTaskid();
					news.setMytaskid(mytaskid);
					news.setTaskid(taskid);
					news.setMystatus(0);
				}
				break;
			default:
				break;
			}
			newmsg.add(news);
		}
		return newmsg;
	}
	
	private void shareMethod(Long compid,NewMessageResponse news, Long mytaskid, int mystatus, Long taskid) {
		TkTaskDeploy tkTaskDeploy=tkTaskDeployDao.get(compid,taskid);
		news.setMytaskid(mytaskid);
		news.setTaskid(taskid);
		news.setStatus(tkTaskDeploy.getStatus());
		if(mystatus==4 || mystatus==5) {
			news.setMystatus(100);
		}else {
			news.setMystatus(mystatus);
		}
		news.setPeriod(tkTaskDeploy.getPeriod());
		news.setExsjnum(tkTaskDeploy.getExsjnum());
		news.setResolve(tkTaskDeploy.getResolve());
	}
	
	/**
	 * 小程序获取消息总条数
	 * @param flag
	 * @param session
	 * @return
	 */
	public int getCount(int flag,Session session) {
		return tkTaskRemindListDao.count(flag, session);
	}
	
	/**
	 * 更新小程序消息的已读状态
	 */
	public void update(Long compid,Long noteid) {
		tkTaskRemindListDao.update(compid,noteid);
	}

//===================================上面为领导版小程序=====================下面为基础版小程序===============================================

	/**
	 * 我的任务列表
	 */
	public void page(Long compid,int status,String serach,Long userId, Pager pager) {
		// 返回的结果集
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = new StringBuilder(SELECT_LIST_SQL);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		ps.add(userId);
		if(status==10) {
			query_sql.append(" and a.status in (4,5)");
		}else {
			query_sql.append(" and a.status = ?");
			ps.add(status);
		}
		if(!StringUtils.isEmpty(serach)) {
			query_sql.append(" and (tkname like '%"+serach+"%' or clname like '%"+serach+"%' or username like '%"+serach+"%')");
		}
		query_sql.append(" order by datenode desc");
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}
	
	/**
	 * 小程序获取消息总条数
	 * @param flag
	 * @param session
	 * @return
	 */
	public int getMyCount(int flag,Session session) {
		return tkTaskRemindListDao.mycount(flag, session);
	}
	
	/**
	 * 基础版和领导版点击反馈进入下一个页面的接口
	 * @param id
	 * @param session
	 * @return
	 */
	public TkMyTaskLogResponse listChilds(int flag, Long id, Session session) {
		TkMyTaskLogResponse tkMyTaskLogResponse = null;
		TkCommentResponse tkCommentResponse = null;
		TkMyTaskLog tkMyTaskLog = tkMyTaskLogDao.get(session.getCompid(),id);
		List<TkCommentResponse> comments = new ArrayList<TkCommentResponse>();
		//退回原因
		TkMyTask tkMyTask=tkMyTaskDao.get(session.getCompid(),tkMyTaskLog.getMytaskid());
		tkMyTaskLogResponse = new TkMyTaskLogResponse();
		tkMyTaskLogResponse.setBackReason(tkMyTask.getRemarks3());
		tkMyTaskLogResponse.setId(tkMyTaskLog.getId());
		tkMyTaskLogResponse.setProgress(tkMyTaskLog.getProgress());
		tkMyTaskLogResponse.setMesg(tkMyTaskLog.getMesg());
		tkMyTaskLogResponse.setOpttime(tkMyTaskLog.getOpttime());
		tkMyTaskLogResponse.setUsername(tkMyTaskLog.getUsername());
		tkMyTaskLogResponse.setIsshow(false);
		tkMyTaskLogResponse.setIsopen(false);
		tkMyTaskLogResponse.setOpttype(tkMyTaskLog.getOpttype());
		//点赞相关
		int num = tkPraiseDetailsDao.getNum(session.getCompid(),tkMyTaskLog.getId());
		tkMyTaskLogResponse.setParisenum(num);
		int count = tkPraiseDetailsDao.getCount(tkMyTaskLog.getId(), session);
		if(count<=0) {//0表示登录人没有点赞该反馈
			tkMyTaskLogResponse.setFlag(0);
		}else {
			tkMyTaskLogResponse.setFlag(1);
		}
		if(StringUtils.isEmpty(tkMyTaskLog.getFileuuid())) {
			tkMyTaskLogResponse.setAttachment(new ArrayList<TkAttachment>(0));
		}else {
			List<TkAttachment> listAttachments = tkAttachmentService.list(session.getCompid(),tkMyTaskLog.getFileuuid());
			tkMyTaskLogResponse.setAttachment(listAttachments);//这里面的remarks1 被用为语音评论的时长
		}
		tkMyTaskLogResponse.setCommentnum(tkMyTaskLog.getCommcount());
		if(tkMyTaskLog.getCommcount()>0) {
			List<TkComment> listCommens = null;
			if(flag == 2) {
				listCommens = tkCommentDao.list(session.getCompid(),tkMyTaskLog.getId());
			}else {
				//控制创建人和督办人只能看到自己的评论，而执行人能看到所有评论
				TkMyTask mytask = tkMyTaskDao.get(session.getCompid(),tkMyTaskLog.getMytaskid());
				if(mytask.getSubjectid() == session.getUserid()) {//执行人看到的评论
					 listCommens = tkCommentDao.list(session.getCompid(),tkMyTaskLog.getId());
				}else {
					 listCommens = tkCommentDao.listBySession(session.getCompid(),tkMyTaskLog.getId(),session.getUserid());
				}
			}
			if(listCommens==null || listCommens.size()<=0) {
				tkMyTaskLogResponse.setComment(new ArrayList<TkCommentResponse>(0));
			}else {
				for (TkComment tkComment : listCommens) {
					tkCommentResponse = new TkCommentResponse();
					tkCommentResponse.setId(tkComment.getId());
					tkCommentResponse.setCommflag(tkComment.getCommflag());
					tkCommentResponse.setCommtext(tkComment.getCommtext());
					tkCommentResponse.setDuration(tkComment.getRemarks1());
					tkCommentResponse.setCommtime(tkComment.getCommtime());
					tkCommentResponse.setFsubjectid(tkComment.getFsubjectid());
					tkCommentResponse.setFsubjectname(tkComment.getFsubjectname());
					tkCommentResponse.setModel(tkComment.getModel());
					tkCommentResponse.setModelid(tkComment.getModelid());
					List<TkCommentReply>  listReplys = tkCommentReplyDao.list(tkComment.getId());
					if(listReplys==null || listReplys.size()<=0) {
						tkCommentResponse.setReply(new ArrayList<TkCommentReply>(0));
					}else {
						tkCommentResponse.setReplynum(listReplys.size());
						tkCommentResponse.setReply(listReplys);
					}
					comments.add(tkCommentResponse);
				}
				tkMyTaskLogResponse.setComment(comments);
				tkMyTaskLogResponse.setCommentnum(comments.size());
			}
		}
		return tkMyTaskLogResponse;
	}

	public List<TkTaskClass> listByFlag(Long compid) {
		return tkTaskClassDao.listByFlag(compid);
	}


    public void insert(BrowerHis browerHis) {
        generalSqlComponent.insert(SqlCode.browserhisInsert, browerHis);
    }

    public BrowerHis get(Long compid,Long taskid,int model,Long userid) {
        return generalSqlComponent.query(SqlCode.browserhisByInfo, new Object[] {compid,taskid,model,userid});
    }

    public void update(Long compid,int num,String time,Long modelid,Long userid) {
        generalSqlComponent.update(SqlCode.browserhisupdate,new Object[] {compid,time,num,modelid,userid});

    }


    /**
     * 取消数据授权时要清空该关注人关注过的历史列表
     * @param userid
     */
    public void deleteByUserid(Long compid,Long userid) {
        generalSqlComponent.getDbComponent().delete(SqlCode.delBrowserhisByUserid, new Object[] {compid,userid});
    }

    public void deleteByUserid(Long compid,Long userid,String taskid) {
        generalSqlComponent.getDbComponent().delete(SqlCode.delBrowserhisBymodelin, new Object[] {compid,userid,taskid});
    }


    public void delete(Long compid,Long taskid) {
        generalSqlComponent.getDbComponent().delete(SqlCode.delBrowserhisBymodelid, new Object[] {compid,taskid});
    }
}
