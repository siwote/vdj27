package com.lehand.duty.controller.task;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkAttachmentService;
import com.lehand.duty.service.TkMyTaskService;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.CommentReplyRequest;
import com.lehand.duty.dto.FeedDetails;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.dto.TkTaskRespond;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkAttachment;
import com.lehand.duty.pojo.TkMyTaskLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.controller.DutyBaseController;
@Api(value = "我的任务", tags = { "我的任务" })
@RestController
@RequestMapping("/duty/myTask")
public class TkMyTaskController extends DutyBaseController {

	private static final Logger logger = LogManager.getLogger(TkMyTaskController.class);

	@Resource
	private TkMyTaskService tkMyTaskService;
    @Resource
    private TkAttachmentService tkAttachmentService;
    @Resource
    private GeneralSqlComponent generalSqlComponent;
	/**
	 * 我的任务列表查询
	 * 
	 * @author pantao
	 * @date 2018年10月23日下午2:44:55
	 * 
	 * @param request
	 * @return
	 * @throws LehandException
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "我的任务列表查询", notes = "我的任务列表查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(@RequestParam Map<String, Object> request, Pager pager) throws LehandException {
		if (null == request) {
			LehandException.throwException("信息不能为空!");
		}
		Message message = new Message();
		try {
			tkMyTaskService.page(request, getSession().getUserid(),getSession().getCompid(), pager);
			message.success().setData(pager);
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	/**
	 * 我签收的任务列表查询，不分页
	 * @throws LehandException
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "我签收的任务列表查询，不分页", notes = "我签收的任务列表查询，不分页", httpMethod = "POST")
	@RequestMapping("/myTaskList")
	public Message myTaskList(String tkname,String taskid) throws LehandException {
		Message message = new Message();
		try {
			message.success().setData(tkMyTaskService.listMyTask(getSession(),taskid,tkname));
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		} catch (ParseException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}

	/**
	 * 
	 * 查询部署任务列表头部状态角标
	 * 
	 * @author pantao
	 * @date 2018年10月19日 下午2:43:12
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "查询部署任务列表头部状态角标", notes = "查询部署任务列表头部状态角标", httpMethod = "POST")
	@RequestMapping("/count")
	public Message listCountByStatus() {
		int sjtype = 0;
		Message message = new Message();
		try {
			List<Map<String, Object>> list = tkMyTaskService.listCountByStatus(getSession().getCompid(),getSession().getUserid(), sjtype);
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("各状态数量查询出错!");
		}

		return message;
	}
	
	/**
	 * 查看任务详情
	 * @author pantao
	 * @date 2018年11月15日下午4:19:49
	 * 
	 * @param id
	 * @param flag (1表示周期任务，0表示普通任务)
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "查看任务详情", notes = "查看任务详情", httpMethod = "POST")
	@RequestMapping("/details")
	public Message details(Long id,int flag) {
		Message message = new Message();
		try {
			TkTaskRespond tkTaskRespond = tkMyTaskService.details(id,flag,getSession());
			message.success().setData(tkTaskRespond);
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
		}catch (Exception e) {
			logger.error(e);
			message.setMessage("任务详情查询出错!");
		}
		return message;
	}

	/**获取服务器时间
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "获取服务器时间", notes = "获取服务器时间", httpMethod = "POST")
	@RequestMapping("/currentTime")
	public Message currentTime() {
		Message message = new Message();
		try {
			long time = DateEnum.now().getTime();
			message.success().setData(time);
		}catch (Exception e) {
			logger.error(e);
			message.setMessage("获取服务器时间出错!");
		}
		return message;
	}

	/**
	 * 反馈进展
	 * @return
	 */
	@OpenApi(optflag=1)
	@ApiOperation(value = "反馈进展", notes = "反馈进展", httpMethod = "POST")
	@RequestMapping("/feedback")
	public Message feedback(MyTaskLogRequest request) {
		Message message = new Message();
		try {
			Long compid = getSession().getCompid();
			request.validateFeedBack();
			//前台传过来的uuid需要判断下是否是转换过后的如果是就不再转换
			if(!StringUtils.isEmpty(request.getUuid())) {
				List<TkAttachment> list = tkAttachmentService.list(compid,request.getUuid());
				if(list.size()<=0) {
					request.setUuid(getUUIDByHash(request.getUuid())+"");
				}
			}
			request.setSynchronizationTime(DateEnum.YYYYMMDDHHMMDD.format());
			tkMyTaskService.feedBack(TaskProcessEnum.FEED_BACK, request, getSession());
            Long mytaskid = request.getMytaskid();
            //关联任务反馈
			List<Map<String, Object>> tasks = request.getRelationtask();
			if(!CollectionUtils.isEmpty(tasks)){
				for (Map<String, Object> taskMap : tasks) {
					Long mytaskId = Long.valueOf((String) taskMap.get("id"));
					request.setMytaskid(mytaskId);
					Long userid = generalSqlComponent.query(SqlCode.getUserIdByMytaskIds, new Object[]{compid,compid,mytaskId});
					request.setRemindtime((String) taskMap.get("feedTime"));
					request.setAudituserid(userid);
					tkMyTaskService.feedBack(TaskProcessEnum.FEED_BACK, request, getSession());
				}
			}
            updatelogRelate(request, mytaskid);
            message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage());			
		}catch (Exception e) {
			logger.error(e);
			message.setMessage("反馈进展失败!");
		}
		return message;
	}

	@Transactional(rollbackFor = Exception.class)
    public void updatelogRelate(MyTaskLogRequest request, Long mytaskid) {

        String ids = generalSqlComponent.query(SqlCode.getLogIds,
                new Object[]{getSession().getCompid(), request.getSynchronizationTime(), mytaskid});
        TkMyTaskLog log = generalSqlComponent.query(SqlCode.getLogBytime,
                new Object[]{getSession().getCompid(), request.getSynchronizationTime(), mytaskid});
        Map<String,Object> params = new HashMap<String,Object>(5);
        params.put("feedbackPerson", request.getFeedbackPerson());
        params.put("feedbackPhone", request.getFeedbackPhone());
        params.put("compid",getSession().getCompid() );
        params.put("id",log.getId());
        if(StringUtils.hasText(ids)&&log!=null){
        	params.put("ids", ids);
            generalSqlComponent.update(SqlCode.updateLogRelate,new Object[]{ids,getSession().getCompid(),log.getId()});
        }
        generalSqlComponent.update(SqlCode.updateLogRelate2,params);
        //反馈添加文件
        generalSqlComponent.getDbComponent().update("update tk_my_task_log  set files=? where compid =? and id =? ",new Object[]{request.getFiles(),getSession().getCompid(),log.getId()});
    }


    /**
	 * 修改反馈前查询该条反馈的详情
	 * @param feedbackid 反馈表id
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "修改反馈前查询该条反馈的详情", notes = "修改反馈前查询该条反馈的详情", httpMethod = "POST")
	@RequestMapping("/modibeforequery")
	public Message modiBeforeQuery(Long feedbackid) {
		Message message = new Message();
		try {
			FeedDetails data = tkMyTaskService.getFeedBack(getSession(), feedbackid);
			message.success().setData(data);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("查询反馈详情失败！");
		}
		return message;
	}

	/**
	 * 修改反馈信息
	 * @param request
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "修改反馈信息", notes = "修改反馈信息", httpMethod = "POST")
	@RequestMapping("/updateFeedBack")
	public Message updateFeedBack(MyTaskLogRequest request) {
		Message message = new Message();
		try {
			request.validateUpdateFeedBack();
			String uuid = request.getUuid();
			if(!StringUtils.isEmpty(uuid)) {
				List<TkAttachment> list = tkAttachmentService.list(getSession().getCompid(),uuid);
				if(list.size()<=0) {
					request.setUuid(getUUIDByHash(uuid)+"");
				}
			}
			request.setSynchronizationTime(DateEnum.YYYYMMDDHHMMDD.format());
			Long mytaskid = request.getMytaskid();
			tkMyTaskService.feedBack(TaskProcessEnum.UPDATE_FEED_BACK, request, getSession());
			//关联任务修改反馈
			feedRelateDuty(request);
			//重新设置反馈关联id
			updatelogRelate(request, mytaskid);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("反馈进展失败!");
		}
		return message;
	}

	public void feedRelateDuty(MyTaskLogRequest request) {
		Long feedbackid = request.getFeedbackid();//页面传来的主反馈的id
		Long compid = getSession().getCompid();
		TkMyTaskLog log = generalSqlComponent.query(SqlCode.getTMTLByid, new Object[] {compid,feedbackid});
		List<Map<String, Object>> tasks = request.getRelationtask();
		ArrayList<String> newFeedids = new ArrayList<>();
		if(!CollectionUtils.isEmpty(tasks)){
			for (Map<String, Object> taskMap : tasks) {
				//插入关联反馈之前先需要查询一次根据mytaskid remarks5 opttime 如果查询到了就修改，如果没查到就新增
				//首先根据request中的feedbackid找到该条反馈取出其中的opttime
				String myTaskIdFromMap = (String) taskMap.get("id");
				Long userid = generalSqlComponent.query(SqlCode.getUserIdByMytaskIds, new Object[]{compid,compid,Long.valueOf(myTaskIdFromMap)});
				request.setAudituserid(userid);
				String feedId = (String) taskMap.get("feedId");
				String feedTime = (String) taskMap.get("feedTime");
				if(StringUtils.hasText(feedId)){
					request.setFeedbackid(Long.valueOf(feedId));
					newFeedids.add(feedId);
				}
				request.setMytaskid(Long.valueOf( myTaskIdFromMap));
				request.setRemindtime(feedTime);
				//然后再根据新的mytaskid remarks5 opttime查询记录 如果查询到了就修改，如果没查到就新增
				if(StringUtils.isEmpty(feedId)){
					tkMyTaskService.feedBack(TaskProcessEnum.FEED_BACK, request, getSession());
				}else{
					int num = generalSqlComponent.query(SqlCode.getTTFBABycount, new Object[] {compid,feedId});
					if(num>0) {
						//断开关联
						Map<String, Object> params = new HashMap<>();
						params.put("updatetime",DateEnum.YYYYMMDDHHMMDD.format());
						params.put("compid", compid);
						params.put("feedid",feedId);
						params.put("feedbackPerson", request.getFeedbackPerson());
						params.put("feedbackPhone", request.getFeedbackPhone());
						
						generalSqlComponent.update(SqlCode.modiTMTLByOpttime, params);
						tkMyTaskService.feedBack(TaskProcessEnum.FEED_BACK, request, getSession());

					}else {
						TkMyTaskLog oldlog = generalSqlComponent.query(SqlCode.getTMTLByid, new Object[] {compid,feedId});
						if(oldlog==null){
							continue;
						}
						if(com.alibaba.druid.util.StringUtils.equals(oldlog.getMytaskid()+oldlog.getRemarks4(),myTaskIdFromMap+feedTime )){
							tkMyTaskService.feedBack(TaskProcessEnum.UPDATE_FEED_BACK, request, getSession());
						}else{
							generalSqlComponent.delete(SqlCode.delTMTLByid, new Object[] {compid,feedId});
							generalSqlComponent.delete(SqlCode.deleteAuditByFeedbackid, new Object[] {compid,feedId});
							tkMyTaskService.feedBack(TaskProcessEnum.FEED_BACK, request, getSession());
						}
					}
				}
			}
		}

		//删除的关联的任务列表
		String remarks5 = log.getRemarks5();
		if(StringUtils.hasText(remarks5)){
			String[] oldFeedIds = remarks5.split(",");
			for (String oldFeedId : oldFeedIds) {
				if(!newFeedids.contains(oldFeedId)){
					//断开关联
					Map<String, Object> params = new HashMap<>();
					params.put("updatetime",DateEnum.YYYYMMDDHHMMDD.format());
					params.put("compid", compid);
					params.put("feedid",oldFeedId);
					
					params.put("feedbackPerson", request.getFeedbackPerson());
					params.put("feedbackPhone", request.getFeedbackPhone());

					generalSqlComponent.update(SqlCode.modiTMTLByOpttime, params);
					//如果
					int num = generalSqlComponent.query(SqlCode.getTTFBABycount, new Object[] {compid,oldFeedId});
					if(num == 0 ) {
						//没有进入审核流程，删除反馈日志，审核申请
						generalSqlComponent.delete(SqlCode.delTMTLByid, new Object[] {compid,oldFeedId});
						generalSqlComponent.delete(SqlCode.deleteAuditByFeedbackid, new Object[] {compid,oldFeedId});

					}
				}
			}
		}


	}

	/**
	 * 回复评论
	 * @author pantao
	 * @date 2018年11月28日上午9:27:32
	 * 
	 * @param request
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "回复评论", notes = "回复评论", httpMethod = "POST")
	@RequestMapping("/reply")
	public Message reply(CommentReplyRequest request) {
		Message message = new Message();
		try {
			request.validate();
			tkMyTaskService.insertCommentReply(TaskProcessEnum.REPLY,request, getSession());
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("回复评论失败!");
		}
		return message;
	}
	
	/**
	 * 删除回复
	 * @author pantao
	 * @date 2018年11月28日上午9:45:48
	 * 
	 * @param id
	 * @return
	 */
	@OpenApi(optflag=3)
	@ApiOperation(value = "删除回复", notes = "删除回复", httpMethod = "POST")
	@RequestMapping("/deleteReply")
	public Message deleteReply(Long id) {
		Message message = new Message();
		try {
			tkMyTaskService.deleteReply(getSession().getCompid(),id);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("删除回复失败!");
		}
		return message;
	}
	
	
	/**
	 * 
	 * 批量签收/签收
	 * 
	 * @author pantao  
	 * @date 2018年10月21日 下午9:47:01
	 *  
	 * @param ids
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "批量签收/签收", notes = "批量签收/签收", httpMethod = "POST")
	@RequestMapping("/updateStatus")
	public Message updateStatus(String ids,int status,String msg){
		Message message = new Message();
		try {	
//			Double score =  tkMyTaskBusiness.getSignIntegral(getSession().getCompid());
			tkMyTaskService.batchOperate(msg,ids,getSession(),status);
			message.success();//.setData(score);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;	
		
	}

	/**
	 * 通过任务id和userid查询我的任务
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "通过任务id和userid查询我的任务", notes = "通过任务id和userid查询我的任务", httpMethod = "POST")
	@RequestMapping("/getDetialsByTaskId")
	public Message getDetialsByTaskId(Long taskid,Long userid) {
		Message message = new Message();
		try {
			FeedDetails data = tkMyTaskService.getMyTask(getSession(),taskid,userid);
			message.success().setData(data);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	/**
	 * 批量退回/退回
	 * @param ids
	 * @param status
	 * @param msg
	 * @return
	 */
	@OpenApi(optflag=2)
	@ApiOperation(value = "批量退回/退回", notes = "批量退回/退回", httpMethod = "POST")
	@RequestMapping("/updateback")
	public Message updateStatus2(String ids,int status,String msg){
		Message message = new Message();
		try {	
			Double score =  tkMyTaskService.getSignIntegral(getSession().getCompid());
			tkMyTaskService.batchOperate(msg,ids,getSession(),status);
			message.success().setData(score);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;	
		
	}
}
