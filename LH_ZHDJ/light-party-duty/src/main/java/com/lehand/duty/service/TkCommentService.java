package com.lehand.duty.service;

import com.lehand.base.constant.StringConstant;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.CommentRequest;
import com.lehand.duty.pojo.TkComment;
import com.lehand.duty.pojo.TkMyTaskLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TkCommentService {

    @Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public TkComment get(Long compid,Long id) {
		return generalSqlComponent.query(SqlCode.tkCommentget, new Object[] { compid,id});
	}
	
	public TkComment insert(TkComment info) {
		Long id = generalSqlComponent.insert(SqlCode.tkCommentInsert, info);
		info.setId(id);
		return info;
	}
	
	/**
	 * 执行人查看评论
	 * @param modelid
	 * @return
	 */
	public List<TkComment> list(Long compid,Long modelid){
		return generalSqlComponent.query(SqlCode.tkCommentlist, new Object[] {compid,modelid});
	}
	
	/**
	 * 创建人和督办人查看评论
	 * @param modelid
	 * @param userid
	 * @return
	 */
	public List<TkComment> listBySession(Long compid, Long modelid, Long userid) {
		return generalSqlComponent.query(SqlCode.querytkCommentinfo, new Object[] {compid,modelid,userid});
	}
	
	public TkComment insert(TkMyTaskLog myTaskLog, CommentRequest request, Session session, int duration){//remarks1作为语音评论的时长
		TkComment info = new TkComment();
		info.setCompid(session.getCompid());
		info.setModel(Module.MY_TASK_LOG.getValue());// 模块(0:任务发布,1:任务反馈,2:任务反馈明细)
		info.setModelid(request.getMyTaskLogId());// 某个模块相应的ID,如任务发布
		info.setTaskid(myTaskLog.getTaskid());//主任务ID
		info.setMytaskid(myTaskLog.getMytaskid());//我的任务ID
		info.setFsjtype(0);//评论主体类型(0:用户,1:机构/组/部门,2:角色)
		info.setFsubjectid(session.getUserid());//评论主体ID(用户ID、角色ID、机构ID等等)
		info.setFsubjectname(session.getUsername());
		info.setCommtime(request.getNow());//评论时间
		info.setUserflag(request.getCommentUserType());//评论人类别(0:无用,1:创建人,2:跟踪督办人)
		info.setTsjtype(0);//被评论主体类型
		info.setTsubjectid(myTaskLog.getUserid());//被评论主题ID
		info.setTsubjectname(myTaskLog.getUsername());
		info.setCommflag(request.getCommFlag());//评论类型(0:文本,1:语言)
		info.setCommtext(request.getCommText());//评论内容(评论类型为0时，存储文本,评论类型为1时，存储附件UUID)
		info.setRemarks1(duration);//备注1
		info.setRemarks2(0);//备注2
		info.setRemarks3(StringConstant.EMPTY);//备注3
		info.setRemarks4(StringConstant.EMPTY);//备注3
		info.setRemarks5(StringConstant.EMPTY);//备注3
		info.setRemarks6(StringConstant.EMPTY);//备注3
		insert(info);	
		return info;
	}

	/**
	 * 删除评论
	 * @param id
	 */
	public void delete(Long compid,Long id) {
		generalSqlComponent.delete(SqlCode.tkCommentdelete, new Object[] {compid,id});
	}
	
	public void deleteByTaskid(Long compid,Long taskid) {
		generalSqlComponent.delete(SqlCode.tkCommentdelByTaskid, new Object[] {compid,taskid});
	}

	public List<TkComment> listByTaskid(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.querytkCommentBytid, new Object[] {compid,taskid});
	}

	public void deleteByMyTaskid(Long compid,Long taskid,Long userid) {
		generalSqlComponent.delete(SqlCode.deltkCommentBysubid, new Object[] {compid,taskid,userid});
	}
}
