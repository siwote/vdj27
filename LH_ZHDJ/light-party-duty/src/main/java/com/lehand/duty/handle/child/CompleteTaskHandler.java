package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import com.lehand.duty.pojo.TkTaskSubject;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CompleteTaskHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkTaskDeploy deploy = data.getParam(0);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
		data.setDeploySub(deploySub);
		data.setDeploy(deploy);
		
		//process
		int flag = data.getParam(1);
		String enddate = data.getParam(2);
		switch (flag) {
			case 1://确认完成总任务（完成所有任务）
				//先确认完成主任务
				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
				//再确认完成所有子任务
				tkMyTaskDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkMyTask.STATUS_FINISH);
				//将提醒登记簿中的remarks3设置为complete(切记批处理时候扫描都要带上remarks3字段)
				tkTaskRemindNoteService.updateRemarks3(deploy.getCompid(),deploy.getId(),"complete");
				break;
			case 2://确认完成某个人的任务（普通任务）
				String userid = data.getParam(3);
				Long subjectid = Long.valueOf(userid);
				//先确认完成该执行人的任务
				tkMyTaskDao.updateStatusBySubject(deploy.getId(),subjectid,Constant.TkMyTask.STATUS_FINISH);
				//判断下该任务下是不是所有人的任务都暂停了如果是就更新主任务的状态为暂停
				int num = tkMyTaskDao.getCountByStstus(deploy.getCompid(),deploy.getId(),Constant.TkMyTask.STATUS_FINISH);
				if(num<=0) {
					//将主任务更新为暂停状态
					tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
					//将提醒登记簿中的remarks3设置为complete(切记批处理时候扫描都要带上remarks3字段)
					tkTaskRemindNoteService.updateRemarks3(deploy.getCompid(),deploy.getId(),"complete");
				}
				break;
			case 3://确认完成某个周期任务（即该周期下所有人的任务）
				//第一步：先确认完成该周期下所有人的任务
				tkMyTaskDao.updateByEndtimeStatus(deploy.getCompid(),deploy.getId(), enddate ,Constant.TkMyTask.STATUS_FINISH);
				//第二步：判断该主任务下面的子任务是不是全部都被确认完成了，如果是就更新主表状态为已完成，否则不更新。
				int count = tkMyTaskDao.getCountByStstus(deploy.getCompid(),deploy.getId(),Constant.TkMyTask.STATUS_FINISH);
				if(count<=0) {
					//将主任务更新为完成状态
					tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
				}
				data.putRemindParam("periodtime", flag);
				data.putRemindParam("endtime", enddate);
				break;
			default:
				break;
		}
		
		//更新执行人的待办为已办
		generalSqlComponent.getDbComponent().update("update my_to_do_list set ishandle=1 where module=1 and busid=?", new Object[] {deploy.getId()});
		
		// remind
		String userid = data.getParam(3);
		List<Subject> subjects = new ArrayList<Subject>();
		Set<Subject> sub = null;
		switch (flag) {
		case 1:
			subjects = tkTaskSubjectService.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//给该任务相关人发消息
			sub = new HashSet<Subject>(subjects);//去重
			break;
		case 2:
			TkTaskSubject user = tkTaskSubjectService.getBySubject(deploy.getCompid(),Long.valueOf(userid));
			Subject s = new Subject(Long.valueOf(userid),user.getSubjectname());
			subjects.add(s);
			sub = new HashSet<Subject>(subjects);
			break;
		case 3:
			subjects = tkTaskSubjectService.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//发消息内容xxx阶段任务已被确认完成
			sub = new HashSet<Subject>(subjects);//去重
			break;
		default:
			break;
		}
		Map<String, Object> remindParams = data.getRemindParams();
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), deploy.getId(), processEnum.getRemindRule(), DateEnum.YYYYMMDDHHMMDD.format(),
				data.getSessionSubject(), sub,remindParams);
	}
	
//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getParam(0);
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
//		data.setDeploySub(deploySub);
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		int flag = data.getParam(1);
//		String enddate = data.getParam(2);
//		TkTaskDeploy deploy = data.getDeploy();
//		switch (flag) {
//		case 1://确认完成总任务（完成所有任务）
//			//先确认完成主任务
//			tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
//			//再确认完成所有子任务
//			tkMyTaskDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkMyTask.STATUS_FINISH);
//			//将提醒登记簿中的remarks3设置为complete(切记批处理时候扫描都要带上remarks3字段)
//			tkTaskRemindNoteDao.updateRemarks3(deploy.getCompid(),deploy.getId(),"complete");
//			break;
//		case 2://确认完成某个人的任务（普通任务）
//			String userid = data.getParam(3);
//			Long subjectid = Long.valueOf(userid);
//			//先确认完成该执行人的任务
//			tkMyTaskDao.updateStatusBySubject(deploy.getId(),subjectid,Constant.TkMyTask.STATUS_FINISH);
//			//判断下该任务下是不是所有人的任务都暂停了如果是就更新主任务的状态为暂停
//			int num = tkMyTaskDao.getCountByStstus(deploy.getCompid(),deploy.getId(),Constant.TkMyTask.STATUS_FINISH);
//			if(num<=0) {
//				//将主任务更新为暂停状态
//				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
//				//将提醒登记簿中的remarks3设置为complete(切记批处理时候扫描都要带上remarks3字段)
//				tkTaskRemindNoteDao.updateRemarks3(deploy.getCompid(),deploy.getId(),"complete");
//			}
//			break;
//		case 3://确认完成某个周期任务（即该周期下所有人的任务）
//			//第一步：先确认完成该周期下所有人的任务
//			tkMyTaskDao.updateByEndtimeStatus(deploy.getCompid(),deploy.getId(), enddate ,Constant.TkMyTask.STATUS_FINISH);
//			//第二步：判断该主任务下面的子任务是不是全部都被确认完成了，如果是就更新主表状态为已完成，否则不更新。
//			int count = tkMyTaskDao.getCountByStstus(deploy.getCompid(),deploy.getId(),Constant.TkMyTask.STATUS_FINISH);
//			if(count<=0) {
//				//将主任务更新为完成状态
//				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.COMPLETE_STATUS);
//			}
//			data.putRemindParam("periodtime", flag);
//			data.putRemindParam("endtime", enddate);
//			break;
//		default:
//			break;
//		}
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		int flag = data.getParam(1);
//		String userid = data.getParam(3);
//		TkTaskDeploy deploy = data.getDeploy();
//		List<Subject> subjects = new ArrayList<Subject>();
//		Set<Subject> sub = null;
//		switch (flag) {
//		case 1:
//			subjects = tkTaskSubjectDao.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//给该任务相关人发消息
//		    sub = new HashSet<Subject>(subjects);//去重
//			break;
//		case 2:
//			TkTaskSubject user = tkTaskSubjectDao.getBySubject(deploy.getCompid(),Long.valueOf(userid));
//			Subject s = new Subject(Long.valueOf(userid),user.getSubjectname());
//			subjects.add(s);
//			sub = new HashSet<Subject>(subjects);
//			break;
//		case 3:
//			subjects = tkTaskSubjectDao.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//发消息内容xxx阶段任务已被确认完成
//		    sub = new HashSet<Subject>(subjects);//去重
//			break;
//		default:
//			break;
//		}
//		Map<String, Object> remindParams = data.getRemindParams();
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, deploy.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
//				data.getSessionSubject(), sub,remindParams);
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}

}
