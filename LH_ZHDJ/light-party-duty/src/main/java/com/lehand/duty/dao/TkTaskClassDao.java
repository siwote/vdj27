package com.lehand.duty.dao;

import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.pojo.TkTaskClass;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class TkTaskClassDao {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	private static final String list = "SELECT a.bizid id,b.clname,b.pid,b.seqno FROM urc_auth_other_map a left join tk_task_class b on a.bizid = b.id where a.compid=? and b.compid=? and subjecttp=0 and subjectid=? and module=0 and b.status=1 ";
	private static final String INSERT = "INSERT INTO tk_task_class (compid, flag, clname, pid, cldesc, seqno, status, remarks1, remarks2, remarks3) VALUES (:compid, :flag,:clname,:pid,:cldesc,:seqno,:status,:remarks1,:remarks2,:remarks3)";
	private static final String SET_PID_SEQNO = "UPDATE tk_task_class SET  pid = ? , seqno = ? WHERE compid=? and id = ? ";
	private static final String SET_SEQNO_ADD = "update tk_task_class set seqno = seqno-2 where compid=? and id != ? and pid = ? and seqno <= ?";
	private static final String SET_SEQNO_SUB = "update tk_task_class set seqno = seqno+2 where compid=? and id != ? and pid = ? and seqno >= ?";
	private static final String UPDATE_SET_STATUS_BY_ID = "UPDATE tk_task_class SET status=? WHERE compid=? and id=?";
	private static final String UPDATE_BY_ID = "UPDATE tk_task_class SET clname=? WHERE compid=? and id=?";
	private static final String SELECT_PID_SEQNO = "select seqno,pid from tk_task_class where compid=? and id=?";
	private static final String listauthtaskclass = "SELECT a.bizid id,b.clname,b.pid,b.seqno FROM urc_auth_other_map a left join tk_task_class b on a.bizid = b.id where a.compid=? and b.compid=? and subjecttp=0 and subjectid=? and module=0 union SELECT a.bizid id,b.clname,b.pid,b.seqno FROM urc_auth_other_map a left join tk_task_class b on a.bizid = b.id where subjecttp=1 and subjectid in (SELECT roleid FROM pw_role_map where subjectid = ?) and module=0";
	private static final String ALL_CLSNAMES = "SELECT concat( id, '-' , clname) clsnames FROM tk_task_class WHERE compid=? and status=?";
	
	/**
	 * 所有可用状态的分类名称
	 * @return
	 */
	public List<String> listAllClsnames(Long compid){
		return generalSqlComponent.getDbComponent().listSingleColumn(ALL_CLSNAMES, String.class, new Object[] {compid,1});
	}
	
	/**
	 * 
	 * 根据id查找任务分类对象
	 * 
	 * @author pantao  
	 * @date 2018年10月23日 上午8:38:49
	 *  
	 * @param id
	 * @return
	 */
	public TkTaskClass get(Long compid,Long id,int status) {
		return generalSqlComponent.getDbComponent().getBean("SELECT * FROM TK_TASK_CLASS WHERE compid=? and ID=? AND STATUS=?", TkTaskClass.class, new Object[] {compid,id,status});
	}

	public void updateTkTaskClass1(Long compid,Long srcid, Long tagid, Long num) {
		generalSqlComponent.getDbComponent().update(SET_PID_SEQNO, new Object[] {tagid,num,compid,srcid});
	}
	
	public void updateTkTaskClass2(Long compid,Long srcid, TkTaskClass tag, Long tagPid) {
		generalSqlComponent.getDbComponent().update(SET_SEQNO_ADD, new Object[] {compid,srcid,tagPid,tag.getSeqno()});
	}
	
	public void updateTkTaskClass3(Long compid,Long srcid, TkTaskClass tag, Long tagPid) {
		generalSqlComponent.getDbComponent().update(SET_SEQNO_SUB, new Object[] {compid,srcid,tagPid,tag.getSeqno()});
	}
	
	public TkTaskClass get2Bean(Long compid,Long tagid) {
		return generalSqlComponent.getDbComponent().getBean(SELECT_PID_SEQNO,TkTaskClass.class,new Object[] {compid,tagid});
	}
	
	public int updateStatus(Long compid,Long id) {
		return generalSqlComponent.getDbComponent().update(UPDATE_SET_STATUS_BY_ID, new Object[] {Constant.TkTaskClass.STATUS_NO_ACTIVE,compid,id});
	}
	
	public int insert(TkTaskClass record) {
		return generalSqlComponent.getDbComponent().insert(INSERT, record);
	}
	
	public int updateTkname(Long compid,Long id, String name) {
		return generalSqlComponent.getDbComponent().update(UPDATE_BY_ID, new Object[] {name,compid,id});
	}
	
	public List<TkTaskClass> list(Session session){
		return generalSqlComponent.getDbComponent().listBean(list, TkTaskClass.class, new Object[] {session.getCompid(),session.getCompid(),session.getUserid()});
	}

	public int findByPid(Long compid,Long id) {
		return generalSqlComponent.getDbComponent().getBean("SELECT count(*) FROM TK_TASK_CLASS WHERE compid=? and pid=? AND STATUS=1", Integer.class, new Object[]{compid,id});
	}
	
	public List<TkTaskClass> findByPidList(Long compid,Long id) {
		return generalSqlComponent.getDbComponent().listBean("SELECT * FROM TK_TASK_CLASS WHERE compid=? and pid=? AND STATUS=1",TkTaskClass.class ,new Object[] {compid,id});
	}
	
	public String getIdString(Long compid,Long pid) {
		return generalSqlComponent.getDbComponent().getBean("select group_concat(id) id from TK_TASK_CLASS where compid=? and status=1 and pid = ?",String.class, new Object[] {compid,pid});
	}

	public List<TkTaskClass> listClass(Long compid,String ids){
		return generalSqlComponent.getDbComponent().listBean("select id,pid from TK_TASK_CLASS where compid=? and status=1"+" and pid in ("+ids+")",TkTaskClass.class,new Object[] {compid});
	}
	
	public String getIdString(Long compid,String ids) {
		return generalSqlComponent.getDbComponent().getBean("select group_concat(id) id from TK_TASK_CLASS where compid=? and status=1"+" and pid in ("+ids+")",String.class,new Object[]{compid});
	}
	
	public List<TkTaskClass> listAuthClass(Long compid,Long subjectid){
		return generalSqlComponent.getDbComponent().listBean(listauthtaskclass, TkTaskClass.class,new Object[] { compid,compid,subjectid,subjectid});
	}

	public List<TkTaskClass> listAuth(Long compid) {
		return generalSqlComponent.getDbComponent().listBean("SELECT * FROM tk_task_class where compid=? and status!=0 and id in (SELECT bizid FROM urc_auth_other_map group by bizid)", TkTaskClass.class,new Object[] {compid});
	}

	public List<TkTaskClass> listByFlag(Long compid) {
		return generalSqlComponent.getDbComponent().listBean("SELECT * FROM tk_task_class where compid=? and status = 1 order by seqno", TkTaskClass.class,new Object[] {compid});
	}

	public void updatePid(Long compid,String classid) {
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_class SET  pid = 0 WHERE compid=? and id in ("+classid+")",new Object[] {compid});
	}
}
