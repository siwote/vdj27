package com.lehand.duty.service;

import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkIntegralRule;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 
 * 积分设置接口实现类  
 * 
 * @author pantao  
 * @date 2018年10月22日 下午8:56:04
 *
 */
@Service
public class TkIntegralRuleService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	public int update(TkIntegralRule info) {
		return generalSqlComponent.update(SqlCode.updateRuleByCode, info);
	}

	public TkIntegralRule get(Long compid,String code) {
		return generalSqlComponent.query(SqlCode.queryRuleByCode, new Object[] {compid,code});
	}

	public Double getIntegral(Long compid) {
		return generalSqlComponent.query(SqlCode.queryRuleInterval, new Object[] {compid});
	}
	
	/**
	 * 
	 * 积分设置查询  
	 * 
	 * @author pantao  
	 * @date 2018年10月22日 下午8:56:27
	 *  
	 * @param status
	 * @return
	 * @throws LehandException
	 */
	public List<Map<String, Object>> listByStatus(Long compid,int status) throws LehandException {
        return generalSqlComponent.query(SqlCode.findRuleByStatus,  new Object[] {compid,status});
	}

	/**
	 * 
	 * 积分 规则修改接口实现方法  
	 * 
	 * @author pantao  
	 * @date 2018年10月22日 下午9:12:18
	 *  
	 * @param listMap
	 * @throws LehandException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(rollbackFor = Exception.class)
	public void update(Long compid,List<Map> listMap) throws LehandException {
		TkIntegralRule tkIntegralRule = null;
		for (Map<String, Object> map : listMap) {
			tkIntegralRule = new TkIntegralRule();
			tkIntegralRule.setCode(map.get("code").toString());
			if(!StringUtils.isEmpty(map.get("inteval"))) {
				tkIntegralRule.setInteval(Double.parseDouble(map.get("inteval").toString()));
			}
			if(!StringUtils.isEmpty(map.get("maxval"))) {
				tkIntegralRule.setMaxval(Double.parseDouble(map.get("maxval").toString()));
			}
			tkIntegralRule.setFlag(Integer.parseInt(map.get("flag").toString()));
			tkIntegralRule.setStatus(Integer.parseInt(map.get("status").toString()));
			tkIntegralRule.setCompid(compid);
			update(tkIntegralRule);
		}
	}

}
