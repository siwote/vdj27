package com.lehand.duty.handle.child;

import com.lehand.base.common.Session;
import com.lehand.duty.dto.CommentReplyRequest;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkComment;
import com.lehand.duty.pojo.TkCommentReply;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Component;

@Component
public class ReplyHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		CommentReplyRequest request = (CommentReplyRequest) data.getParam(0);
		Session session = data.getParam(1);
		TkComment comment = tkCommentService.get(session.getCompid(),request.getCommentid());
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),comment.getTaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),comment.getTaskid());
		data.setDeploySub(deploySub);
		data.setCommentReplyRequest(request);
		data.setDeploy(deploy);
		data.setTkComment(comment);
		
		//process
		TkCommentReply tkCommentReply = tkCommentReplyService.insert(comment, request, data.getSession());
		data.setTkCommentReply(tkCommentReply);
		
		//remind
		Subject subject = new Subject();
		subject.setSubjectid(comment.getFsubjectid());
		subject.setSubjectname(comment.getFsubjectname());
		//谁评论发给谁
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), data.getTkCommentReply().getId(), processEnum.getRemindRule(), 
				request.getNow(), 
				data.getSessionSubject(), 
				subject);

		if(deploySub!=null) {
			deploySub.setReplynum(deploySub.getReplynum()+1);
		}
	}
	
//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		CommentReplyRequest request = (CommentReplyRequest) data.getParam(0);
//		Session session = data.getParam(1);
//		TkComment comment = tkCommentDao.get(session.getCompid(),request.getCommentid());
//		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),comment.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),comment.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setCommentReplyRequest(request);
//		data.setDeploy(deploy);
//		data.setTkComment(comment);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		CommentReplyRequest request = data.getCommentReplyRequest();
//		TkComment comment = data.getTkComment();
//		TkCommentReply tkCommentReply = tkCommentReplyDao.insert(comment, request, data.getSession());
//		data.setTkCommentReply(tkCommentReply);
//	}
//
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		CommentReplyRequest request = data.getCommentReplyRequest();
//		TkComment comment = data.getTkComment();
//		Subject subject = new Subject();
//		subject.setSubjectid(comment.getFsubjectid());
//		subject.setSubjectname(comment.getFsubjectname());
//		//谁评论发给谁
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, data.getTkCommentReply().getId(), remindRule, 
//					request.getNow(), 
//					data.getSessionSubject(), 
//					subject);
//	}
//
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//		TkTaskDeploySub deploySub = data.getDeploySub();
//		if(deploySub!=null) {
//			deploySub.setReplynum(deploySub.getReplynum()+1);
//		}
//	}

}
