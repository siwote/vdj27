package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component
public class BackEndBeforeHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkMyTask tkMyTask = data.getParam(0);
		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTask(tkMyTask);
		data.setDeploy(deploy);
		
		//remind
		String time =data.getParam(1);
		String[] times = time.split(":");
		int hours =Integer.valueOf(times[0])*60+Integer.valueOf(times[1]);//计算出具体的分钟数便于下面统计毫秒
		String remindtime = data.getParam(2);
		Date date;
		String remind = "";
		try {
			date = DateEnum.YYYYMMDDHHMMDD.parse(remindtime);
			date.setTime(date.getTime() + hours*60*1000+1000);
			remind = DateEnum.YYYYMMDDHHMMDD.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String sendtime = "";
		if(remind.compareTo(DateEnum.YYYYMMDDHHMMDD.format())>0) {//如果提醒时间大于当前时间就定时发送
			sendtime = remind;
		}else {//如果小于就立即发送
			sendtime = DateEnum.YYYYMMDDHHMMDD.format();
		}
		TkMyTask mytask = data.getMyTask();
		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),mytask.getId(),sendtime);
		//提醒执行人
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), mytask.getId(), processEnum.getRemindRule(),sendtime,
				TaskProcessData.getSystem(), data.getMyTaskSubject());
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkMyTask tkMyTask = data.getParam(0);
//		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTask(tkMyTask);
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		String time =data.getParam(1);
//		String[] times = time.split(":");
//		int hours =Integer.valueOf(times[0])*60+Integer.valueOf(times[1]);//计算出具体的分钟数便于下面统计毫秒
//		String remindtime = data.getParam(2);
//		Date date;
//		String remind = "";
//		try {
//			date = DateEnum.YYYYMMDDHHMMDD.parse(remindtime);
//			date.setTime(date.getTime() + hours*60*1000+1000);
//		    remind = DateEnum.YYYYMMDDHHMMDD.format(date);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		String sendtime = "";
//		if(remind.compareTo(DateEnum.YYYYMMDDHHMMDD.format())>0) {//如果提醒时间大于当前时间就定时发送
//			sendtime = remind;
//		}else {//如果小于就立即发送
//			sendtime = DateEnum.YYYYMMDDHHMMDD.format();
//		}
//		TkMyTask mytask = data.getMyTask();
//		supervise(data,module,remindRule,mytask.getId(),sendtime);
//		//提醒执行人
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, mytask.getId(), remindRule,sendtime,
//				TaskProcessData.getSystem(), data.getMyTaskSubject());
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
