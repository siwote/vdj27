 package com.lehand.duty.controller.task;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dto.*;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkAttachment;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskClass;
import com.lehand.duty.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.components.converter.AudioConverter;
import com.lehand.duty.controller.DutyBaseController;

 @Api(value = "任务分类", tags = { "任务分类" })
@RestController
@RequestMapping("/duty/weixin")
public class BrowerHisController extends  DutyBaseController {
	
	private static final Logger logger = LogManager.getLogger(BrowerHisController.class);
	@Resource private BrowerHisService browerHisService;
	@Resource private TkTaskRemindListService tkTaskRemindListService;
	@Resource private TkTaskDeployService tkTaskDeployService;
	@Resource private TaskDetailsService taskDetailsService;
	@Resource private TkMyTaskService tkMyTaskService;
	@Resource private TkAttachmentService tkAttachmentService;
	@Resource(name="silk2MP3Converter") 
	private AudioConverter silk2MP3Converter;
	
	/**
	 * 快速查询
	 * @author pantao
	 * @date 2018年11月21日下午5:56:26
	 * 
	 * @param time
	 * @param pager
	 * @return
	 * @throws LehandException
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "快速查询", notes = "快速查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(String time, Pager pager){
		Message message = new Message();
		try {
			browerHisService.page(getSession(),time, pager);
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	
	/**
	 * 详细查询
	 * @author pantao
	 * @date 2018年11月21日下午6:33:30
	 * 
	 * @param request
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "详细查询", notes = "详细查询", httpMethod = "POST")
	@RequestMapping("/detailsPage")
	public Message page(TkTaskRequest request, Pager pager){
		Message message = new Message();
		try {
			browerHisService.page(getSession(),request, pager);
			message.success().setData(pager);
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	

//   /**
//    * 消息列表
//    * @author pantao
//    * @date 2018年11月23日上午9:52:28
//    * 
//    * @param flag
//    * @param pager
//    * @return
//    */
//	@RequestMapping("/pageList")
//	public Message pageList(int flag,Pager pager) {
//		Message message = new Message();
//		try {	
//			List<NewMessageResponse> list = browerHisService.page(0,flag,getSession(),pager);
//			int count = browerHisService.getCount(flag, getSession());
//			pager.setRows(list);
//			System.out.println(list.toString());
//			pager.setTotalRows(count);
//			int i = count % pager.getPageSize();
//			int v = count / pager.getPageSize();
//			pager.setTotalPages(i>0 ? v+1 : v);
//			message.setData(pager);
//			message.success();
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		return message;		
//	}
	
	/**
	   * 小程序获取未读消息总条数
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "小程序获取未读消息总条数", notes = "小程序获取未读消息总条数", httpMethod = "POST")
	@RequestMapping("/getCount")
	public Message getCount() {
		Message message = new Message();
		try {	
			int count = browerHisService.getCount(0, getSession());
			message.setData(count);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 更新消息的已读状态
	 * @param noteid
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "更新消息的已读状态", notes = "更新消息的已读状态", httpMethod = "POST")
	@RequestMapping("/update")
	public Message update(Long noteid) {
		Message message = new Message();
		try {
			browerHisService.update(getSession().getCompid(),noteid);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	   * 删除消息
	 * @author pantao
	 * @date 2018年11月26日下午1:16:03
	 * 
	 * @param noteid
	 * @param userid
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除消息", notes = "删除消息", httpMethod = "POST")
	@RequestMapping("/delete")
	public Message delete(Long noteid,Long userid) {
		Message message = new Message();
		try {
			tkTaskRemindListService.delete(getSession().getCompid(),noteid, userid);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	/**
	 * 查看任务详情
	 * @author pantao
	 * @date 2018年11月15日下午4:19:49
	 * 
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看任务详情", notes = "查看任务详情", httpMethod = "POST")
	@RequestMapping("/details")
	public Message details(Long id) {
		int isPC=1;
		Message message = new Message();
		try {
			TkTaskRespond tkTaskRespond = tkTaskDeployService.details(id,isPC,getSession());
			message.success().setData(tkTaskRespond);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("任务详情查询出错!");
		}
		return message;
	}
	
	
	/**
	 * 任务分类列表查询授权列表
	 * @author pantao
	 * @date 2018年11月23日下午4:34:58
	 * 
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务分类列表查询授权列表", notes = "任务分类列表查询授权列表", httpMethod = "POST")
	@RequestMapping("/listClass")
	public Message listClass() {
		Message message = new Message();
		try {
			List<TkTaskClass> list = browerHisService.list(getSession());
			message.success().setData(list);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("任务分类列表查询出错!");
		}
		return message;
	}
	
	/**
	 * 
	   *   任务分类查询  （部署任务列表）
	 * 
	 * @author pantao  
	 * @date 2018年10月9日 下午6:51:43
	 *  
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务分类查询  （部署任务列表）", notes = "任务分类查询  （部署任务列表）", httpMethod = "POST")
	@RequestMapping("/listByFlag")
	public Message listByFlag(){
		Message message = new Message();
		try {
			List<TkTaskClass> list = browerHisService.listByFlag(getSession().getCompid());
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}
	
	/**
	 *浏览历史查询
	 * @author pantao
	 * @date 2018年11月26日上午11:12:35
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "浏览历史查询", notes = "浏览历史查询", httpMethod = "POST")
	@RequestMapping("/listHis")
	public Message listHis(Pager pager) {
		Message message = new Message();
		try {
			Long userid = getSession().getUserid();
			int count = browerHisService.count(getSession().getCompid(),userid);
			List<BrowerHisResponse> list = browerHisService.listHis(userid,pager);
			pager.setRows(list);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.success().setData(pager);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("浏览历史查询出错!");
		}
		return message;
	}
	
	/**
	 * 历史列表删除
	 * @author pantao
	 * @date 2018年11月26日下午1:22:50
	 * 
	 * @param userid
	 * @param model
	 * @param modelid
	 * @param time
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "历史列表删除", notes = "历史列表删除", httpMethod = "POST")
	@RequestMapping("/deleteHis")
	public Message deleteHis(Long userid, int model, Long modelid, String time) {
		Message message = new Message();
		try {
			browerHisService.delete(getSession().getCompid(),userid, model, modelid, time);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("浏览历史查询出错!");
		}
		return message;
	}
	
	
	/**
	 * 积分列表查询
	 * @author pantao
	 * @date 2018年11月28日上午11:11:45
	 * 
	 * @param time
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "积分列表查询", notes = "积分列表查询", httpMethod = "POST")
	@RequestMapping("/integral")
	public Message integral(String time,Pager pager){
		Message message = new Message();
		try {
			int count = browerHisService.countIntegral(getSession().getCompid(),time);
			List<TkIntegralResponse> list = browerHisService.listIntegral(getSession().getCompid(),time,pager);
			pager.setRows(list);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.success().setData(pager);
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
//	/**
//	 * 已部署任务反馈详情页面普通任务下一个反馈时间
//	 * @author pantao
//	 * @date 2018年12月3日上午10:28:13
//	 * 
//	 * @param taskid
//	 * @return
//	 */
//	@RequestMapping("/getTimeByTaskid")
//	public Message getRemindTimeByTaskId(Long taskid,Long subjectid) {
//		Message message = new Message();
//		try {
//			String time = taskDetailsBusiness.getRemindTime(getSession().getCompid(),taskid,subjectid);
//			message.setData(time);
//			message.success();
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(), e);
//		}
//		return message;
//	}
	
	/**
	 * 已部署任务反馈详情页面周期任务时间节点集合
	 * @author pantao
	 * @date 2018年12月3日上午10:30:33
	 * 
	 * @param taskid
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "已部署任务反馈详情页面周期任务时间节点集合", notes = "已部署任务反馈详情页面周期任务时间节点集合", httpMethod = "POST")
	@RequestMapping("/listTimeByTaskid")
	public Message listTimeByTaskId(Long taskid,Pager pager ) {
		Message message = new Message();
		try {
			int count = taskDetailsService.countByTaskId(getSession().getCompid(),taskid);
			TimeNodeResponse times = taskDetailsService.deployedList(getSession().getCompid(),taskid, pager);
			pager.setRows(times);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 已部署任务反馈详情列表中获取任务执行人集合
	 * @author pantao
	 * @date 2018年12月3日上午10:52:11
	 * 
	 * @param taskid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "已部署任务反馈详情列表中获取任务执行人集合", notes = "已部署任务反馈详情列表中获取任务执行人集合", httpMethod = "POST")
	@RequestMapping("/listDepolySubjects")
	public Message listDepolySubjects(Long taskid,Pager pager,String name ) {
		int flag =0;
		Message message = new Message();
		try {
			int count = taskDetailsService.count(getSession().getCompid(),taskid,flag,name);
			List<StatusSubject> subjects= taskDetailsService.listDepolySubjects(getSession().getCompid(),taskid,flag, pager,name);
			pager.setRows(subjects);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	
	/**
	 * 获取我的任务id
	 * @author pantao
	 * @date 2018年12月3日上午11:39:57
	 * 
	 * @param taskid
	 * @param subjectid
	 * @param date
	 * @param period
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取我的任务id", notes = "获取我的任务id", httpMethod = "POST")
	@RequestMapping("/getMyTaskId")
	public Message getMyTaskId(Long taskid,Long subjectid,String date,int period ) {
		Message message = new Message();
		try {
			Long mytaskid= taskDetailsService.getMyTaskId(getSession().getCompid(),taskid,subjectid,date,period);
			message.setData(mytaskid);
			message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 分页查询反馈详情
	 * @author pantao
	 * @date 2018年11月20日下午9:56:51
	 * 
	 * @param myid
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询反馈详情", notes = "分页查询反馈详情", httpMethod = "POST")
	@RequestMapping("/pageFeedBack")
	public Message pageFeedBack(Long myid ,Pager pager) {
		Message message = new Message();
		try {
			int count = taskDetailsService.count(getSession().getCompid(),myid);
			List<TkMyTaskLogResponse> list= taskDetailsService.page(myid, pager,getSession());
			pager.setRows(list);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 新增评论
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=1)
    @ApiOperation(value = "新增评论", notes = "新增评论", httpMethod = "POST")
	@RequestMapping("/comment")
	public Message comment(CommentRequest request) {
		Message message = new Message();
		try {
			request.validate();
			taskDetailsService.insertComment(TaskProcessEnum.COMMENT,request, getSession());
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("新增评论失败!");
		}
		return message;
	}
	
	/**
	 * 删除评论
	 * @param commentId
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除评论", notes = "删除评论", httpMethod = "POST")
	@RequestMapping("/deleteComment")
	public Message deleteComment(Long commentId) {
		Message message = new Message();
		try {
			taskDetailsService.deleteComment(getSession().getCompid(),commentId);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("删除评论失败!");
		}
		return message;
	}
	
	/**
	 * 
	 * 小程序上评论语音上传
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午3:38:15
	 *  
	 * @param file
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "小程序上评论语音上传", notes = "小程序上评论语音上传", httpMethod = "POST")
	@RequestMapping("/upload")
	@ResponseBody
	public Message upload(Long id,String uuid,Integer model,MultipartFile file) {
		Message message = new Message();
		try {
			if (file.isEmpty()) {
				message.setMessage("文件为空");
				return message;
			}
			uuid = getUUIDByHash(uuid)+"";
			String filename = file.getOriginalFilename();
			File dest = new File(getFileUploadDir(uuid, model),filename);
			file.transferTo(dest);
			
			File converFile = new File(dest.getParentFile(),dest.getName());
			silk2MP3Converter.convert(dest, converFile);
	        MP3File f = (MP3File)AudioFileIO.read(converFile);
	        MP3AudioHeader audioHeader = (MP3AudioHeader)f.getAudioHeader();
	         //获取mp3的时长（单位秒）
	        int duration = audioHeader.getTrackLength();
//			tkAttachmentBusiness.insert(id,uuid, model,duration,getFileHttpUrl(uuid, model),dest, converFile);
			tkAttachmentService.insert(getSession().getCompid(),id,uuid, model,duration,getFileHttpUrl(uuid, model),dest, null);
            Map<String, Object> map = new HashMap<String, Object>(3);
            map.put("name", filename);
            map.put("uuid", uuid);
            map.put("model", model);
            message.success().setData(map);
		} catch (Exception e) {
			String msg = "上传附件失败!";
			message.setMessage(msg);
			logger.error(msg,e);
		}
        return message;
	}
	
	/**
	 * 获得点赞
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获得点赞", notes = "获得点赞", httpMethod = "POST")
	@RequestMapping("/getParise")
	public Message getParise(Long feedid) {
		Message message = new Message();
		try {
			taskDetailsService.getParise(feedid,getSession(),TaskProcessEnum.FEED_PARISE);
			message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 反馈取消点赞
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "反馈取消点赞", notes = "反馈取消点赞", httpMethod = "POST")
	@RequestMapping("/awayParise")
	public Message awayParise(Long feedid) {
		Message message = new Message();
		try {
			taskDetailsService.awayParise(feedid,getSession());
			message.success();
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	} 
	
	/**
	 * 获取点赞数量
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取点赞数量", notes = "获取点赞数量", httpMethod = "POST")
	@RequestMapping("/getNum")
	public Message getNum(Long feedid) {
		Message message = new Message();
		try {
			Map<String,Object> map =taskDetailsService.getNum(feedid,getSession());
			message.success().setData(map);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
//===================================上面为领导版小程序=====================下面为基础班小程序===============================================
	
//	/**
//	 * 我的任务列表
//	 * @param status
//	 * @param search
//	 * @param pager
//	 * @return
//	 */
//	@RequestMapping("/mypage")
//	public Message mypage(int status,String search, Pager pager) {
//		Message message = new Message();
//		try {
//			Session session = getSession();
//			browerHisService.page(session.getCompid(),status, search,session.getUserid(), pager);
//			message.success().setData(pager);
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(), e);
//		}
//		return message;
//	}
	
	/**
	 * 查看任务详情
	 * @author pantao
	 * @date 2018年11月15日下午4:19:49
	 * 
	 * @param id
	 * @param flag (1表示周期任务，0表示普通任务)
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看任务详情", notes = "查看任务详情", httpMethod = "POST")
	@RequestMapping("/mydetails")
	public Message mydetails(Long id,int flag) {
		Message message = new Message();
		try {
			TkTaskRespond tkTaskRespond = tkMyTaskService.details(id,flag,getSession());
			message.success().setData(tkTaskRespond);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("任务详情查询出错!");
		}
		return message;
	}
	
	/**
	 * 分页查询反馈详情
	 * @author pantao
	 * @date 2018年11月20日下午9:56:51
	 * 
	 * @param myid
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询反馈详情", notes = "分页查询反馈详情", httpMethod = "POST")
	@RequestMapping("/feedpage")
	public Message feedpage(Long myid ,Pager pager) {
		Message message = new Message();
		try {
			int count = taskDetailsService.count(getSession().getCompid(),myid);
			List<TkMyTaskLogResponse> list= taskDetailsService.page(myid,pager,getSession());
			pager.setRows(list);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
//	/**
//	 * 我的任务查询下一阶段的提醒时间（普通任务反馈详情页面）
//	 * @author pantao
//	 * @date 2018年11月21日下午12:00:19
//	 * 
//	 * @param myid
//	 * @return
//	 */
//	@RequestMapping("/getRemindTime")
//	public Message getRemindTimeByMyTaskId(Long myid ) {
//		Message message = new Message();
//		try {
//			String time = taskDetailsBusiness.get(getSession().getCompid(),myid);
//			message.setData(time);
//			message.success();
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(), e);
//		}
//		return message;
//	}
	
	/**
	 * 反馈进展
	 * @return
	 */
    @OpenApi(optflag=1)
    @ApiOperation(value = "反馈进展", notes = "反馈进展", httpMethod = "POST")
	@RequestMapping("/myfeedback")
	public Message myfeedback(MyTaskLogRequest request) {
		Message message = new Message();
		try {
			request.validateFeedBack();
			tkMyTaskService.feedBack(TaskProcessEnum.FEED_BACK, request, getSession());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error("",e);			
		}catch (Exception e) {
			logger.error("",e);
			message.setMessage("反馈进展失败!");
		}
		return message;
	}
	
	/**
	 * 修改反馈信息
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "修改反馈信息", notes = "修改反馈信息", httpMethod = "POST")
	@RequestMapping("/updatemyfeedback")
	public Message updateMyFeedBack(MyTaskLogRequest request) {
		Message message = new Message();
		try {
			request.validateUpdateFeedBack();
			tkMyTaskService.feedBack(TaskProcessEnum.UPDATE_FEED_BACK, request, getSession());
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("反馈进展失败!");
		}
		return message;
	}
	
	/**
	 * 回复评论
	 * @author pantao
	 * @date 2018年11月28日上午9:27:32
	 * 
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "回复评论", notes = "回复评论", httpMethod = "POST")
	@RequestMapping("/reply")
	public Message reply(CommentReplyRequest request) {
		Message message = new Message();
		try {
			request.validate();
			tkMyTaskService.insertCommentReply(TaskProcessEnum.REPLY,request, getSession());
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("回复评论失败!");
		}
		return message;
	}
	
	/**
	 * 删除回复
	 * @author pantao
	 * @date 2018年11月28日上午9:45:48
	 * 
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "删除回复", notes = "删除回复", httpMethod = "POST")
	@RequestMapping("/deleteReply")
	public Message deleteReply(Long id) {
		Message message = new Message();
		try {
			tkMyTaskService.deleteReply(getSession().getCompid(),id);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("删除回复失败!");
		}
		return message;
	}
	
	/**
	 * 批量签收
	 * 
	 * @author pantao  
	 * @date 2018年10月21日 下午9:47:01
	 *  
	 * @param ids
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "批量签收", notes = "批量签收", httpMethod = "POST")
	@RequestMapping("/updateStatus")
	public Message updateStatus(String ids,int status,String msg){
		Message message = new Message();
		try {	
			Double score =  tkMyTaskService.getSignIntegral(getSession().getCompid());
		    tkMyTaskService.batchOperate(msg,ids,getSession(),status);
			message.success().setData(score);;
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;	
	}
	
//	 /**
//	 * 消息列表
//     * @author pantao
//     * @date 2018年11月23日上午9:52:28
//     * 
//     * @param flag
//     * @param pager
//     * @return
//     */
//	@RequestMapping("/mypagelist")
//	public Message myPageList(int flag,Pager pager) {
//		Message message = new Message();
//		try {	
//			int count = browerHisService.getMyCount(flag, getSession());
//			List<NewMessageResponse> list = browerHisService.page(1,flag,getSession(),pager);
//			pager.setRows(list);
//			pager.setTotalRows(count);
//			int i = count % pager.getPageSize();
//			int v = count / pager.getPageSize();
//			pager.setTotalPages(i>0 ? v+1 : v);
//			message.setData(pager);
//			message.success();
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(),e);
//		}
//		return message;		
//	}
	
	/**
	   * 小程序获取未读消息总条数
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "小程序获取未读消息总条数", notes = "小程序获取未读消息总条数", httpMethod = "POST")
	@RequestMapping("/getmycount")
	public Message getMyCount() {
		Message message = new Message();
		try {	
			int count = browerHisService.getMyCount(0, getSession());
			message.setData(count);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 一键已读全部消息
	 * @return
	 */
    @OpenApi(optflag=2)
    @ApiOperation(value = "一键已读全部消息", notes = "一键已读全部消息", httpMethod = "POST")
	@RequestMapping("/readall")
	public Message readAll() {
		Message message = new Message();
		try {	
		    tkTaskRemindListService.readAll(getSession());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 
	 * 附件删除  
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午5:27:02
	 *  
	 * @param uuid
	 * @param filename
	 * @return
	 */
    @OpenApi(optflag=3)
    @ApiOperation(value = "附件删除", notes = "附件删除", httpMethod = "POST")
	@RequestMapping("/deleteatt")
	@ResponseBody
	public Message deleteAtt(String uuid,String filename) {
		Message message = new Message();
		try {
			tkAttachmentService.delete(getSession().getCompid(),uuid, filename);
			message.success();
		} catch (Exception e) {
			 e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 
	 * 附件查询
	 * 
	 * @author pantao  
	 * @date 2018年10月15日 下午8:37:35
	 *  
	 * @param module
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "附件查询", notes = "附件查询", httpMethod = "POST")
	@RequestMapping("/listatt")
	@ResponseBody
	public Message listAtt(Integer module,Long id) {
		Message message = new Message();	
		try {
			List<Map<String , Object>> list =  tkAttachmentService.list(getSession().getCompid(),id, module);
			message.setData(list);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("附件查询出错!");
		}
		return message;
	}
	
	/**
	 * 基础版附件上传
	 * @param id
	 * @param uuid
	 * @param model
	 * @param file
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "基础版附件上传", notes = "基础版附件上传", httpMethod = "POST")
	@RequestMapping("/uploadatt")
	@ResponseBody
	public Message uploadAtt(Long id,String uuid,Integer model,MultipartFile file) {
		Message message = new Message();
		try {
			if (file.isEmpty()) {
				message.setMessage("文件为空");
				return message;
			}
			//前台传过来的uuid需要判断下是否是转换过后的如果是就不再转换
			if(!StringUtils.isEmpty(uuid)) {//1370357657
				List<TkAttachment> list = tkAttachmentService.list(getSession().getCompid(),uuid);
				if(list.size()<=0) {
					uuid = getUUIDByHash(uuid)+"";
				}
			}
			String filename = file.getOriginalFilename();
			File dest = new File(getFileUploadDir(uuid, model),filename);
			file.transferTo(dest);   

			tkAttachmentService.insert(getSession().getCompid(),id,uuid, model,0,getFileHttpUrl(uuid, model),dest, null);
			
            Map<String, Object> map = new HashMap<String, Object>(3);
            map.put("name", filename);
            map.put("uuid", uuid);
            map.put("model", model);
            message.success().setData(map);
		} catch (Exception e) {
			String msg = "上传附件失败!";
			message.setMessage(msg);
			logger.error(msg,e);
		}
        return message;
	}
	
	/**
	 * 基础版和领导版点击反馈进入下一个页面的接口
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "基础版和领导版点击反馈进入下一个页面的接口", notes = "基础版和领导版点击反馈进入下一个页面的接口", httpMethod = "POST")
	@RequestMapping("/listchilds")
	public Message listChilds(int flag,Long id) {
		Message message = new Message();
		try {	
			TkMyTaskLogResponse tkMyTaskLogResponse = browerHisService.listChilds(flag,id,getSession());
			message.success().setData(tkMyTaskLogResponse);;
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 获取我的任务最新的反馈进度
	 * @param mytaskid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取我的任务最新的反馈进度", notes = "获取我的任务最新的反馈进度", httpMethod = "POST")
	@RequestMapping("/getProgress")
	public Message getProgress(Long mytaskid) {
		Message message = new Message();
		try {
			Double num = taskDetailsService.getProgress(getSession().getCompid(),mytaskid);
			message.setData(num);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 任务普通部署
	 * 
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "任务普通部署", notes = "任务普通部署", httpMethod = "POST")
	@RequestMapping("/deployCommonTask")
	public Message deployCommonTask(TkTaskDeployRequest request) {
		Message message = new Message();
		try {
			request.validate();
			//request.setUuid(getUUIDByHash(request.getUuid())+"");
			Long id = tkTaskDeployService.deployCommonTask(request,getSession());
			message.setData(id);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	

	/**
	 *已发任务列表查询入口
	 * 
	 * @author pantao  
	 * @date 2018年10月12日 下午3:56:37
	 *  
	 * @param request
	 * @return
	 * @throws LehandException
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "已发任务列表查询入口", notes = "已发任务列表查询入口", httpMethod = "POST")
	@RequestMapping("/deployPage")
	public Message deployPage(TkTaskRequest request,Pager pager) {
		Message message = new Message();
		if (null == request) {
			LehandException.throwException("信息不能为空!");
		}		
		try {
			tkTaskDeployService.page(request,pager,false,getSession().getUserid(), getSession());//false代表小程序端已部署任务列表
			message.setData(pager);
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}catch (Exception e) {
			message.setMessage("任务列表查询失败！");
		}
		return message;		
	}
	
	/**
	 * 暂停任务
	 * 
	 * @author pantao  
	 * @date 2018年10月18日 上午10:37:09
	 *  
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "暂停任务", notes = "暂停任务", httpMethod = "POST")
	@RequestMapping("/suspend")
	public Message suspend(Long id) {
		Message message = new Message();
		try {
			tkTaskDeployService.suspend(id,1,"",getSession());
			message.success();	
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }catch (Exception e) {
	    	e.printStackTrace();
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
	
	/**
	 * 确认完成任务
	 * @param id 主任务id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "确认完成任务", notes = "确认完成任务", httpMethod = "POST")
	@RequestMapping("/complete")
	public Message complete(Long id) {
		Message message = new Message();
		try {
			tkTaskDeployService.complete(id,1,"","",getSession());
			message.success();	
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }catch (Exception e) {
	    	e.printStackTrace();
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
	
	/**
	 * 修改任务基本信息
	 * @param request
	 * @return
	 * @author yuxianzhu
	 * @throws Exception 
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改任务基本信息", notes = "修改任务基本信息", httpMethod = "POST")
	@RequestMapping("/updateTaskBase")
	public Message updateTaskBase(TkTaskDeployRequest request) throws Exception{
		Message message=new Message();
		try {
			tkTaskDeployService.updateTaskBase(request, getSession());
			message.success();
		}catch (LehandException e) {
			logger.error(e);
			message.setMessage(e.getMessage());
			return message;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 修改任务执行人
	 * @param request
	 * @return
	 * @author yuxianzhu
	 * @throws LehandException 
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改任务执行人", notes = "修改任务执行人", httpMethod = "POST")
	@RequestMapping("/updateTaskSubjects")
	public Message updateTaskSubjects(TkTaskDeployRequest request) throws Exception{
		Message message=new Message();
		try {
			tkTaskDeployService.updateTaskSubjects(request, getSession());
			message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			e.printStackTrace();
		}catch (Exception e) {
			message.setMessage("修改任务执行人出现异常");
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 修改时间起止时间和普通任务反馈时间节点
	 * @param request
	 * @return
	 * @author yuxianzhu
	 * @throws Exception 
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "修改时间起止时间和普通任务反馈时间节点", notes = "修改时间起止时间和普通任务反馈时间节点", httpMethod = "POST")
	@RequestMapping("/updateTaskTimenode")
	public Message updateTaskTimenode(TkTaskDeployRequest request) throws Exception{
		Message message=new Message();
		try {
			tkTaskDeployService.updateTimes(request, getSession());
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			message.setMessage("修改任务异常");
			return message;
		}
		return message;
	}
	
	/**
	 * 
	 * 删除任务 (删除所有任务)
	 * 
	 * @author pantao  
	 * @date 2018年10月18日 上午10:37:46
	 *  
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = " 删除任务 (删除所有任务)", notes = " 删除任务 (删除所有任务)", httpMethod = "POST")
	@RequestMapping("/deleteAll")
	public Message deleteAll(Long id) {
		Message message = new Message();
		try {
			tkTaskDeployService.deleteAll(id,0, Constant.EMPTY,getSession());
			message.success();
		} catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    } catch (Exception e) {
	    	e.printStackTrace();
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
	
	/**
	 * 自定义表单
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "自定义表单", notes = "自定义表单", httpMethod = "POST")
	@RequestMapping("/queryform")
	public Message queryForm() {
		Message message = new Message();		
		try {
			List<TkTaskFormRespond> list = tkTaskDeployService.form(getSession().getCompid());
			message.setData(list);
			message.success();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			message.setMessage("自定义表单查询出错!");
		}		
		return message;		
	}
	
	/**
	 * 查看任务详情(已部署任务详情接口)
	 * @author pantao
	 * @date 2018年11月15日下午4:19:49
	 * 
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看任务详情(已部署任务详情接口)", notes = "查看任务详情(已部署任务详情接口)", httpMethod = "POST")
	@RequestMapping("/detailsdeploy")
	public Message detailsDeploy(Long id) {
		int isPC=0;
		Message message = new Message();
		try {
			TkTaskRespond tkTaskRespond = tkTaskDeployService.details(id,isPC,getSession());
			message.success().setData(tkTaskRespond);
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("任务详情查询出错!");
		}
		return message;
	}
	
	/**
	 * 获取签收日志数据
	 * @param taskid
	 * @param orgName
	 * @param subName
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取签收日志数据", notes = "获取签收日志数据", httpMethod = "POST")
	@RequestMapping("/getSignLog")
	public Message getSignLog(String status, Long taskid,String orgName,String subName,Pager pager,Long userid){
		Message message = new Message();
		try {
			int count = taskDetailsService.getSignLogNum(getSession().getCompid(),status, taskid, subName,orgName,userid);
			List<SignLogResponse> data = taskDetailsService.getSignLog( getSession().getCompid(),status,taskid, orgName, subName,pager,0,Constant.EMPTY,1,userid);
			pager.setRows(data);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 获取签收日志各状态的数量
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取签收日志各状态的数量", notes = "获取签收日志各状态的数量", httpMethod = "POST")
	@RequestMapping("/getSignStatus")
	public Message getSignStatus(Long taskid){
		Message message = new Message();
		try {
			if (StringUtils.isEmpty(taskid)){
				message.setMessage("任务id不能为空 ");
				message.setData(new ArrayList<>(0));
				return message;
			}
			List<Map<String, Object>> data=taskDetailsService.getStatusCount(getSession().getCompid(),taskid,0,Constant.EMPTY);
			message.setData(data);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("各状态数量查询出错!");
		}
		return message;
	}
	
	/**
	 * 查看退回原因
	 * @param mytaskid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看退回原因", notes = "查看退回原因", httpMethod = "POST")
	@RequestMapping("/getRemarks")
	public Message getRemarks3(Long mytaskid) {
		Message message = new Message();
		try {
			TkMyTask tkMyTask = taskDetailsService.getRemarks3(getSession().getCompid(),mytaskid);
			message.setData(tkMyTask.getRemarks3());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 恢复任务
	 * @param id
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "恢复任务", notes = "恢复任务", httpMethod = "POST")
	@RequestMapping("/recovery")
	public Message recovery(Long id) {
		Message message = new Message();
		try {
			tkTaskDeployService.recovery(id,getSession());
			message.success();	
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }catch (Exception e) {
	    	e.printStackTrace();
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);		
	    }
		return message;
	}
}

