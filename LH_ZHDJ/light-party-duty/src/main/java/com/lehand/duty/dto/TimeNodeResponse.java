package com.lehand.duty.dto;

import java.util.List;

public class TimeNodeResponse {

	private List<TimeResponse> times;

	public List<TimeResponse> getTimes() {
		return times;
	}

	public void setTimes(List<TimeResponse> times) {
		this.times = times;
	}

	

}
