package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dao.TkTaskRemindListDao;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class TkTaskRemindListService {

	@Resource
	TkTaskRemindListDao tkTaskRemindListDao;
	@Resource GeneralSqlComponent generalSqlComponent;
	/**
	    *  查询已提醒的消息列表
	 * @author pantao
	 * @date 2018年11月13日下午1:40:16
	 *
	 * @return
	 */
	public Pager page(int flag,Session session,Pager pager) {
		Map<String,Object> param = new HashedMap<String, Object>();
		param.put("compid", session.getCompid());
		param.put("userid", session.getUserid());
		param.put("isread", flag);
		Pager data =  generalSqlComponent.pageQuery(SqlCode.listMesg, param,pager);
		return data;
		//return tkTaskRemindListDao.page(flag, session, pager);
	}
	/**
	 * 查询未读条数
	 * @author pantao
	 * @date 2018年11月14日下午5:09:00
	 *
	 * @param flag
	 * @param session
	 * @return
	 */
	public int getCount(int flag, Session session) {
		return tkTaskRemindListDao.getCount(flag, session);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void delete(Long compid,Long id,Long userid) {
		tkTaskRemindListDao.delete(compid,id,userid);
	}
	/**
	 * 更新已读状态
	 */
	@Transactional(rollbackFor = Exception.class)
	public void update(Long compid,Long noteid) {
		tkTaskRemindListDao.update(compid,noteid);
	}
	
	/**
	 * 一键已读
	 */
	@Transactional(rollbackFor = Exception.class)
	public void readAll(Session session) {
		tkTaskRemindListDao.updateByUserid(session.getCompid(),session.getUserid());
	}

    
	
	/**
	 * 批量已读消息
	 */
	public void batchUpdate(Long compid, String noteids) {
		String [] strs = noteids.split(",");
		for (String str : strs) {
			update(compid, Long.valueOf(str));
		}
	}

}
