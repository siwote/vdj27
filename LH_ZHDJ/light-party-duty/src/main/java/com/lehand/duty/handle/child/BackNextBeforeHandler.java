package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BackNextBeforeHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkMyTask tkMyTask = data.getParam(0);
		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTask(tkMyTask);
		data.setDeploy(deploy);
		
		//remind
		String nextRemindTime = data.getParam(2);
		data.putRemindParam("nexttime", nextRemindTime);
		String time = "";
		if(data.getParam(1).toString().length()<=5) {
			time = data.getParam(1)+":00";//数据库中时间的json字符串
		}else {
			time = data.getParam(1);//数据库中时间的json字符串
		}
		
		String remindtime = DateEnum.YYYYMMDD.format()+time;
		String remind = remindtime.substring(0,10)+" "+remindtime.substring(10);
		String sendtime = "";
		if(remind.compareTo(DateEnum.YYYYMMDDHHMMDD.format())>0) {//如果提醒时间大于当前时间就定时发送
			sendtime = remind;
		}else {//如果小于就立即发送
			sendtime = DateEnum.YYYYMMDDHHMMDD.format();
		}
		TkMyTask mytask = data.getMyTask();
		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),mytask.getId(),sendtime);
		//提醒执行人
		Map<String, Object> remindParams = data.getRemindParams();
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), mytask.getId(), processEnum.getRemindRule(),sendtime,
				TaskProcessData.getSystem(), data.getMyTaskSubject(),remindParams);
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkMyTask tkMyTask = data.getParam(0);
//		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTask(tkMyTask);
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		String nextRemindTime = data.getParam(2);
//		data.putRemindParam("nexttime", nextRemindTime);
//		Map<String, Object> remindParams = data.getRemindParams();
//		String time = "";
//		if(data.getParam(1).toString().length()<=5) {
//			time = data.getParam(1)+":00";//数据库中时间的json字符串
//		}else {
//			time = data.getParam(1);//数据库中时间的json字符串
//		}
//		
//		String remindtime = DateEnum.YYYYMMDD.format()+time;
//		String remind = remindtime.substring(0,10)+" "+remindtime.substring(10);
//		String sendtime = "";
//		if(remind.compareTo(DateEnum.YYYYMMDDHHMMDD.format())>0) {//如果提醒时间大于当前时间就定时发送
//			sendtime = remind;
//		}else {//如果小于就立即发送
//			sendtime = DateEnum.YYYYMMDDHHMMDD.format();
//		}
//		TkMyTask mytask = data.getMyTask();
//		supervise(data,module,remindRule,mytask.getId(),sendtime);
//		//提醒执行人
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, mytask.getId(), remindRule,sendtime,
//				TaskProcessData.getSystem(), data.getMyTaskSubject(),remindParams);
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
