package com.lehand.duty.handle.child;

import com.lehand.base.common.Message;
import com.lehand.base.constant.DateEnum;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
//import com.lehand.sms.SMSComponent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class OverBackHandler extends BaseHandler {
	
//	@Resource private SMSComponent smsComponent;

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkMyTask tkMyTask = data.getParam(0);
		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTask(tkMyTask);
		data.setDeploy(deploy);
		
		//process
		TkMyTask mytask = data.getMyTask();
		
//		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(mytask.getCompid(),"OVER_BACK");
//		Double inteval = tkIntegralRule.getInteval();
//		if(inteval>0) {
//			//插入数据之前判断下积分详情表中是否存在扣分或者加分记录（签收不算，反馈加分算，逾期反馈减分算）
//			int count = tkIntegralDetailsDao.count(mytask.getCompid(),mytask.getId(),mytask.getDatenode(),"OVER_BACK");
//			if(count<=0) {
//				tkIntegralDetailsDao.insert(mytask.getCompid(),mytask.getSubjectid(), mytask.getSubjectname(), "OVER_BACK", mytask.getTaskid(), mytask.getId(), -inteval,0L);
//			}
//		}
//		data.putRemindParam("inteval", inteval);
		
		//remind
		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),mytask.getId(),DateEnum.YYYYMMDDHHMMDD.format());
		Subject subject = new Subject(deploy.getUserid(), deploy.getUsername());
//		给执行人发
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), mytask.getId(), processEnum.getRemindRule(), DateEnum.YYYYMMDDHHMMDD.format(),
				subject, data.getMyTaskSubject(),data.getRemindParams());
		
		//创建短信消息
		insertSms("您有一条待办任务已逾期，任务名称："+deploy.getTkname(), getPhones(deploy.getCompid(), data.getMyTaskSubject().getSubjectid()));
				
		
		if(deploySub!=null) {
			deploySub.setOvernum(deploySub.getOvernum()+1);
		}
	}
	
	
	/**
	 * a 新增短信验证码
	 * @Title：insertSms 
	 * @Description：TODO
	 * @param ：@param object 
	 * @return ：void 
	 * @throws
	 */
	@Transactional(rollbackFor=Exception.class)
	@Scope(value="prototype")
	public Message insertSms(String content,String phones) {
		Message msg = new Message();
		String id = UUID.randomUUID().toString().replaceAll("-", "");
		//发送短信
		String[] ph = phones.split(",");
		try {
			for (String phone : ph) {
//				smsComponent.sendSms(id, phone, content, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}
	
	
	/**
	 * Description: 根据短信接收人的id查询对应的手机号集合
	 * @author PT  
	 * @date 2019年11月12日 
	 * @param subjectid
	 * @return
	 */
	public String getPhones(Long compid,Long subjectid) {
		List<Map<String,Object>> lists = generalSqlComponent.query(SqlCode.listPhones, new Object[] {compid,subjectid});
		String phones = "";
		if(lists!=null && lists.size()>0) {
			for (Map<String, Object> map : lists) {
				phones += map.get("remarks3")+",";
			}
			if(!StringUtils.isEmpty(phones)) {
				phones = phones.substring(0,phones.length()-1);
			}
		}
		return phones;
	}
	
//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkMyTask tkMyTask = data.getParam(0);
//		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTask.getCompid(),tkMyTask.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTask(tkMyTask);
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		TkMyTask mytask = data.getMyTask();
//		
//		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(mytask.getCompid(),"OVER_BACK");
//		Double inteval = tkIntegralRule.getInteval();
//		if(inteval>0) {
//			//插入数据之前判断下积分详情表中是否存在扣分或者加分记录（签收不算，反馈加分算，逾期反馈减分算）
//			int count = tkIntegralDetailsDao.count(mytask.getCompid(),mytask.getId(),mytask.getDatenode(),"OVER_BACK");
//			if(count<=0) {
//				tkIntegralDetailsDao.insert(mytask.getCompid(),mytask.getSubjectid(), mytask.getSubjectname(), "OVER_BACK", mytask.getTaskid(), mytask.getId(), -inteval,0L);
//			}
//		}
//		data.putRemindParam("inteval", inteval);
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		TkMyTask mytask = data.getMyTask();
//		TkTaskDeploy deploy = data.getDeploy();
//		Subject subject = new Subject(deploy.getUserid(), deploy.getUsername());
//		supervise(data,module,remindRule,mytask.getId(),DateEnum.YYYYMMDDHHMMDD.format());
//		//给执行人发
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, mytask.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
//				subject, data.getMyTaskSubject(),data.getRemindParams());
//		//给创建人发
////		data.putRemindParam("flag", 1);
////		remindNoteComponent.register(module, mytask.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
////				TaskProcessData.getSystem(), data.getDeploySubject(),data.getRemindParams());
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//		TkTaskDeploySub deploySub = data.getDeploySub();
//		if(deploySub!=null) {
//			deploySub.setOvernum(deploySub.getOvernum()+1);
//		}
//	}

}
