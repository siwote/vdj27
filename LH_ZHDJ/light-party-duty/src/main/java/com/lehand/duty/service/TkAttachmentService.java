package com.lehand.duty.service;

import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.TkAttachmentResponse;
import com.lehand.duty.pojo.DlFile;
import com.lehand.duty.pojo.TkAttachment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 
 * 附件上传接口实现类  
 * 
 * @author pantao  
 * @date 2018年10月10日 下午4:30:02
 *
 */
@Service
public class TkAttachmentService {

	@Resource private GeneralSqlComponent generalSqlComponent;

	public int insert(TkAttachment info) throws LehandException {
		return generalSqlComponent.insert(SqlCode.tkAttachmentInsert, info);
	}


	/**
	 * 
	 * 保存附件 (remarks1存储mp3的时长)
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午4:29:35
	 * @return
	 * @throws LehandException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public TkAttachment insert(Long compid, Long id, String uuid, Integer model, int duration, String path, File dest, File convertDest) throws LehandException {
		String fileName = dest.getName();
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
		
		TkAttachment info = new TkAttachment();
		info.setPath(path);
		info.setCompid(compid);
		info.setId(id);
		info.setRemarks3(uuid);
		info.setModel(model);
		info.setSuffix(suffixName);
		info.setSize(dest.length());
		info.setAttaname(fileName);
		info.setRemarks1(duration);
		info.setRemarks2(0);
		if(StringUtils.isEmpty(convertDest)) {
			info.setRemarks4(Constant.EMPTY);
		}else {//小程序上传的语音转化成mp3格式后的新路径
			info.setRemarks4(convertDest.getName());
		}
		//用remark5接收附件id
		info.setRemarks5(id.toString());
		info.setRemarks6(Constant.EMPTY);
		insert(info);
		return info;
	}
	
	/**
	 * 
	 * 附件查询  
	 * 
	 * @author pantao  
	 * @date 2018年10月15日 下午8:36:36
	 *  
	 * @param taskId
	 * @param module
	 * @return
	 * @throws LehandException 
	 */
	public List<Map<String , Object>> list(Long compid,Long taskId,Integer module) {
		return list2map(compid,module, taskId);
	}

    public List<Map<String, Object>> list2map(Long compid,Integer module,Long id) {
        return generalSqlComponent.query(SqlCode.querytkAttachBymid, new Object[] {compid,module,id});
    }
	
	/**
	 * 
	 * 附件上传删除
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 下午4:31:58
	 * @return
	 */
	@Transactional(rollbackFor=Exception.class)
	public int delete(Long compid,String uuid,String fileName) {
        return generalSqlComponent.delete(SqlCode.deltkAttachBynameAndR3, new Object[] {compid,fileName,uuid});
	}

	
	/**
	 * 新附件上传关联资料库 
	 * @param id 上传时先传-1，等保存任务后在更新过来
	 * @param fileid 资料库文件表的id
	 * @param model 2 任务附件，3 反馈附件
	 */
	@Transactional(rollbackFor=Exception.class)
	public void listAttachments(Session session,Long id, String fileid,String uuid, int model) {
		//到资料库表dl_file_business中查找存入附件表中需要的数据，并关联好业务id
		String [] strs = fileid.split(",");
		for (String str : strs) {
			DlFile df = generalSqlComponent.query(SqlCode.getDFBByid, new Object[] {str});
			TkAttachment info = new TkAttachment();
			info.setPath(df.getDir());
			info.setCompid(session.getCompid());
			info.setId(id);
			info.setRemarks3(uuid);//直接存附件id集合，一个任务对应多个附件，后面查询好查
			info.setModel(model);
			info.setSuffix(df.getType());
			info.setSize(df.getSize());
			info.setAttaname(df.getName());
			info.setRemarks1(0);
			info.setRemarks2(0);
			info.setRemarks4(str.toString());//存放单个附件的id
			info.setRemarks5(df.getRemark());
			info.setRemarks6(Constant.EMPTY);
			generalSqlComponent.insert(SqlCode.addTkAttachment, info);
			//保存好附件表后，还需要将业务id和附件id存入资料库的关系表dl_file中
//			DlFileBusiness dfb = new DlFileBusiness();
//			dfb.setBusinessid(id);
//			dfb.setBusinessname((short)model);
//			dfb.setCompid(session.getCompid());
//			dfb.setFileid(Long.valueOf(str));
//			dfb.setIntostatus((short)0);
//			generalSqlComponent.insert(SqlCode.addDlFileBusiness, dfb);
		}
	}

	/**
	 * 新附件删除接口
	 */
	@Transactional(rollbackFor=Exception.class)
	public void newDelete(Long compid,String fileid,int model,Long businessid) {
		//删除附件首先根据uuid 和 fileid 删除tk_attachment,并且要相应删除资料库中的 dl_file 和 dl_file_business
		generalSqlComponent.delete(SqlCode.delAttachment, new Object[] {compid,fileid});
		generalSqlComponent.delete(SqlCode.deleteTkFileBusiness, new Object[] {fileid,compid,businessid,model});
	}

    public int updateId(Long compid,Long id,int model,String remarks3) {
        return generalSqlComponent.update(SqlCode.tkAttachUpdatemr3, new Object[] {id,compid,model,remarks3});
    }

    public List<TkAttachment> list(Long compid,String uuid){
        return generalSqlComponent.query(SqlCode.tkAttachmentList, new Object[] {compid,uuid});
    }
    public List<TkAttachmentResponse> list2(Long compid, String uuid){
        return generalSqlComponent.query(SqlCode.tkAttachmentList2, new Object[] {compid,uuid});
    }

    /**
     * 针对小程序上面语音评论（每条评论作为一个附件对应一个uuid）
     * @param uuid
     * @return
     */
    public TkAttachment get(Long compid,String uuid) {
        return generalSqlComponent.query(SqlCode.tkAttachmentList, new Object[] {compid,uuid});
    }

    public TkAttachment get(Long compid, String uuid, String filename) {
        return generalSqlComponent.query(SqlCode.querytkAttachBynameAndR3, new Object[] {compid,uuid,filename});
    }
}
