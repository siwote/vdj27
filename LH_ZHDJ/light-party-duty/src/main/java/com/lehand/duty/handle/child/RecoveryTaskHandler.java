package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class RecoveryTaskHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkTaskDeploy deploy = data.getParam(0);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
		data.setDeploySub(deploySub); 
		data.setDeploy(deploy);

		//process
		//恢复主任务和子任务的状态
		tkTaskDeployDao.updateStatusRemarks6(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SIGNED_STATUS,DateEnum.YYYYMMDDHHMMDD.format());
		//再暂停所有子任务
		tkMyTaskDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkMyTask.STATUS_SGIN);
		//将提醒登记簿中的remarks3设置为""(切记批处理时候扫描都要带上remarks3字段)
		tkTaskRemindNoteService.updateRemarks3(deploy.getCompid(),deploy.getId(),Constant.EMPTY);
		
		//remind
		List<Subject> subjects = tkTaskSubjectService.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//给该任务相关人发消息
		Set<Subject> sub  = new HashSet<Subject>(subjects);//去重
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), deploy.getId(), processEnum.getRemindRule(), DateEnum.YYYYMMDDHHMMDD.format(),
				data.getSessionSubject(), sub);
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getParam(0);
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
//		data.setDeploySub(deploySub); 
//		data.setDeploy(deploy);
//	}
//
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getDeploy();
//		//恢复主任务和子任务的状态
//		tkTaskDeployDao.updateStatusRemarks6(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SIGNED_STATUS,DateEnum.YYYYMMDDHHMMDD.format());
//		//再暂停所有子任务
//		tkMyTaskDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkMyTask.STATUS_SGIN);
//		//将提醒登记簿中的remarks3设置为""(切记批处理时候扫描都要带上remarks3字段)
//		tkTaskRemindNoteDao.updateRemarks3(deploy.getCompid(),deploy.getId(),Constant.EMPTY);
//	}
//
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {			
//		TkTaskDeploy deploy = data.getDeploy();
//		List<Subject> subjects = tkTaskSubjectDao.listSubjects(data.getDeploy().getCompid(),data.getDeploy().getId());//给该任务相关人发消息
//		Set<Subject> sub  = new HashSet<Subject>(subjects);//去重
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, deploy.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
//				data.getSessionSubject(), sub);
//	}
//
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
