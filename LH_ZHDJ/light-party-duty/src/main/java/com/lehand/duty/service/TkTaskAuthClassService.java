package com.lehand.duty.service;

import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.DlTypeDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TkTaskAuthClassService {
	
	
	@Resource
	private GeneralSqlComponent generalSqlComponent;

	public List<DlTypeDto> listByAuth(Session session, String search) {
//		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
//		Long compid = session.getCompid();
//		Long userid = session.getUserid();
//		// 授权id集合
//		String classids = jdbc.getString(
//				"select group_concat(bizid) from pw_auth_other_map where compid=? and subjecttp=0 and module=0 and subjectid=?",
//				compid, userid);
//		// 查找授权id的上级id
//		String classid = jdbc
//				.getString("select group_concat(pid) from dl_type where compid=? and id in (" + classids + ")", compid);
//		// 授权分类集合
//		List<Map<String, Object>> rows = jdbc
//				.list2Map("select * from dl_type where compid=? and id in (" + classids + ")", compid);
//		lists.addAll(rows);
//		listParents(compid, classid, lists);
//		lists = listByAuth(lists, session, search);
//		return lists;
		
		

		long compid = session.getCompid();
        Map<String, Object> param = new HashMap<String, Object>(3);
        boolean flag = StringUtils.hasLength(search);
        if (flag) {
            param.put("likeName", search);
        }
        param.put("compid", compid);
        // 获取资料分类的集合
        List<DlTypeDto> dlTypeList = generalSqlComponent.query(SqlCode.ListDlType2, param);
        delUnwanted2(dlTypeList, session);
        if (CollectionUtils.isEmpty(dlTypeList)) {
            return null;
        }
        List<Long> pidList = new ArrayList<Long>();
        List<DlTypeDto> tree = null;
        // true是模糊查询 false不是模糊查询
        // 如果是模糊查询,则将没有父分类的分类的pid设为0，并将原来的pid保存起来
        if (flag) {
            List<Long> idList = new ArrayList<Long>();
            for (DlTypeDto temp : dlTypeList) {
                idList.add(temp.getId());
            }
            for (DlTypeDto temp : dlTypeList) {
                if (!idList.contains(temp.getPid())) {
                    pidList.add(temp.getPid());
                    temp.setPid(Constant.TOP_PID_DLTYPE);
                }
            }
        }
        // 树底层的icon换成file的方法
        for (DlTypeDto temp : dlTypeList) {
            Integer sunNum = haveSun(Long.valueOf(temp.getId()), compid);
            if (sunNum.equals(0)) {
                temp.setIcon("file");
                temp.setDisabled(false);
            }
        }

        // 调用递归方法,将集合转为多级树结构并返回
        tree = makeTree(dlTypeList, Constant.TOP_PID_DLTYPE, compid);
        // 模糊查询,将父分类的pid还原
        if (flag) {
            for (int i = 0; i < tree.size(); i++) {
                tree.get(i).setPid(pidList.get(i));
            }
        }
        return tree;
		
	}
	
	/**
	 * 删除集合中不符合要求的 普通遍历，每删除一个集合长度就会减少一个 迭代器遍历就不存在这个问题
	 * 
	 * @author pantao
	 */
	private void delUnwanted2(List<DlTypeDto> maps,Session session) {
		Iterator<DlTypeDto> iterator = maps.iterator();
		List<Map<String,Object>> map2 = generalSqlComponent.getDbComponent().listMap("select bizid id from urc_auth_other_map where subjectid=?", new Object[] {session.getUserid()});
		while (iterator.hasNext()) {
			DlTypeDto map = iterator.next();
			int i = 0;
			for (Map<String,Object> dlTypeDtos : map2) {
				if (dlTypeDtos.get("id").toString().equals(map.getId().toString())) {
					i++;
				}
			}
			if(i<=0){
				iterator.remove();
			}
		}
	}
	
	
	/**
	 * 获取子分类的数量
	 * @param id
	 * @param compid
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public Integer haveSun(Long id,Long compid) {
		//获取该分类的儿子数量
		Integer i=generalSqlComponent.query(SqlCode.getChildrenSum, new Long[] {id,compid});
		return i;
	}
	

	/**
	 * 递归查询树
	 * @param items 集合
	 * @param pid 最高层的pid
	 * @return
	 * @author taogang
	 */
	public List<DlTypeDto> makeTree(List<DlTypeDto> items,Long pid,Long compid){
		 // 子类
        List<DlTypeDto> children = items.stream().filter(x -> x.getPid().equals(pid)).collect(Collectors.toList());
        // 后辈中的非子类
        List<DlTypeDto> successor = items.stream().filter(x -> !x.getPid().equals(pid)).collect(Collectors.toList());
        children.forEach(x -> {
            makeTree(successor, x.getId(), compid).forEach(y -> x.getChildren().add(y));
        });

        return children;
	}

//	private List<Map<String, Object>> listParents(Long compid, String classid, List<Map<String, Object>> lists) {
//		// 授权分类父节点集合
//		List<Map<String, Object>> rows = jdbc
//				.list2Map("select * from dl_type where compid=? and id in (" + classid + ")", compid);
//		lists.addAll(rows);
//		String classid2 = jdbc.getString(
//				"select group_concat(pid) from dl_type where pid!=0 and compid=? and id in (" + classid + ")", compid);
//		if (!StringUtils.isEmpty(classid2)) {
//			listParents(compid, classid2, lists);
//		}
//		return rows;
//	}

	/**
	 * 授权分类列表
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listByAuth(List<Map<String, Object>> rows, Session session, String search)
			throws LehandException {
		Long compid = session.getCompid();
		// 返回一个List<Map<String, Object>>
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		// 对上面的rows集合进行数据分析处理
		Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(rows.size());
		if (rows == null || rows.size() <= 0) {
			return result;
		}
		String ids = "";
		for (Map<String, Object> map1 : rows) {
			ids += map1.get("id") + ",";
		}
		ids = ids.substring(0, ids.length() - 1);
		if (StringUtils.isEmpty(search)) {// 查询要包括角色的授权
			rows = generalSqlComponent.getDbComponent().listMap("select * from dl_type where compid=? and id in (" + ids + ") order by pid ", compid);
		} else {
			search = search.replaceAll("\'", "");
			rows = generalSqlComponent.getDbComponent().listMap("select * from dl_type where compid=? and id in (" + ids + ") and name like '" + '%'
					+ search + '%' + "' order by pid", compid);
		}
		// 对集合数据进行遍历处理
		for (Map<String, Object> row : rows) {
			Long id = Long.valueOf(row.get("id").toString());
			Long pid = Long.valueOf(row.get("pid").toString());
			// 返回的数据里面共有的信息（id和name）
			Map<String, Object> line = new HashMap<String, Object>(3);
			line.put("id", id);
			line.put("label", row.get("name"));
			line.put("pid", pid);
			line.put("isshow", false);
			line.put("isedit", false);
			if (pid <= 0) {// 根节点
				// 父节点的话再将下面的子集put进去
				line.put("children", new ArrayList<Map<String, Object>>());
				// 再将集合添加到结果集中
				result.add(line);
			} else {// 是子节点
				if (map.get(pid) != null) {
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					childrens.add(line);
				} else {
					result.add(line);
				}
			}
			map.put(id, line);
		}
		return result;
	}

}
