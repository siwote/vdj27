package com.lehand.duty.handle.child;

import com.lehand.base.common.Session;
import com.lehand.duty.common.Module;
import com.lehand.duty.dto.CommentRequest;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Component;

@Component
public class CommentHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		CommentRequest request = (CommentRequest) data.getParam(0);
		Session session = data.getParam(1);
		TkMyTaskLog myTaskLog = tkMyTaskLogService.get(session.getCompid(),request.getMyTaskLogId());
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTaskLog.getTaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTaskLog.getTaskid());
		TkMyTask mytask = tkMyTaskDao.get(myTaskLog.getCompid(),myTaskLog.getMytaskid());
		data.setDeploySub(deploySub);
		data.setCommentRequest(request);
		data.setMyTaskLog(myTaskLog);
		data.setDeploy(deploy);
		data.setMyTask(mytask);
		
		//process
		String uuid = request.getCommText();
		int duration = 0;
		if(request.getCommFlag()==1) {//评论类型为语音，则评论内容字段存入附件的mp3的新路径
			TkAttachment tkAttachment = tkAttachmentService.get(myTaskLog.getCompid(),uuid);
			duration = tkAttachment.getRemarks1();
			request.setCommText(tkAttachment.getPath()+tkAttachment.getAttaname());
		}
		TkComment tkComment = tkCommentService.insert(myTaskLog, request, data.getSession(),duration);
		data.setTkComment(tkComment);
		if(request.getCommFlag()==1) {
			tkAttachmentService.updateId(myTaskLog.getCompid(),tkComment.getId(), Module.COMMENT.getValue(), uuid);
		}
		
		//detail
		tkMyTaskLogService.updateId(data.getDeploy().getCompid(),request.getMyTaskLogId());
		
		//remind
		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),mytask.getId(),request.getNow());
		//只给执行人发消息
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), data.getTkComment().getId(), processEnum.getRemindRule(), 
				request.getNow(), 
				data.getDeploySubject(), 
				data.getTkMyTaskLogSubject());
		
		if(deploySub!=null) {
			deploySub.setCommnum(deploySub.getCommnum()+1);
		}
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		CommentRequest request = (CommentRequest) data.getParam(0);
//		Session session = data.getParam(1);
//		TkMyTaskLog myTaskLog = tkMyTaskLogDao.get(session.getCompid(),request.getMyTaskLogId());
//		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTaskLog.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTaskLog.getTaskid());
//		TkMyTask mytask = tkMyTaskDao.get(myTaskLog.getCompid(),myTaskLog.getMytaskid());
//		data.setDeploySub(deploySub);
//		data.setCommentRequest(request);
//		data.setMyTaskLog(myTaskLog);
//		data.setDeploy(deploy);
//		data.setMyTask(mytask);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		CommentRequest request = data.getCommentRequest();
//		TkMyTaskLog myTaskLog = data.getMyTaskLog();
//		String uuid = request.getCommText();
//		int duration = 0;
//		if(request.getCommFlag()==1) {//评论类型为语音，则评论内容字段存入附件的mp3的新路径
//			TkAttachment tkAttachment = tkAttachmentDao.get(myTaskLog.getCompid(),uuid);
//			duration = tkAttachment.getRemarks1();
//			request.setCommText(tkAttachment.getPath()+tkAttachment.getAttaname());
//		}
//		TkComment tkComment = tkCommentDao.insert(myTaskLog, request, data.getSession(),duration);
//		data.setTkComment(tkComment);
//		if(request.getCommFlag()==1) {
//			tkAttachmentDao.updateId(myTaskLog.getCompid(),tkComment.getId(), Module.COMMENT.getValue(), uuid);
//		}
//		//评论和回复一样不加分
////		Session session = data.getSession();
////		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(String.valueOf(TaskProcessEnum.FEED_BACK));
////		Double inteval = tkIntegralRule.getInteval();
////		tkIntegralDetailsDao.insert(session.getUserid(),session.getUsername(),String.valueOf(TaskProcessEnum.FEED_BACK),myTaskLog.getTaskid(),myTaskLog.getMytaskid(),inteval);
//	}
//
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//		// 修改反馈详情表中评论数量
//		CommentRequest request = data.getCommentRequest();	
//		tkMyTaskLogDao.updateId(data.getDeploy().getCompid(),request.getMyTaskLogId());	
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		CommentRequest request = data.getCommentRequest();
//		TkMyTask mytask = data.getMyTask();
////		Set<Subject> subs = new HashSet<Subject>(2);
////		subs.add(data.getDeploySubject());
////		subs.add(data.getTkMyTaskLogSubject());
//		supervise(data,module,remindRule,mytask.getId(),request.getNow());
//		//只给执行人发消息
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, data.getTkComment().getId(), remindRule, 
//                request.getNow(), 
//                data.getDeploySubject(), 
//                data.getTkMyTaskLogSubject());
//	}
//
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//		TkTaskDeploySub deploySub = data.getDeploySub();
//		if(deploySub!=null) {
//			deploySub.setCommnum(deploySub.getCommnum()+1);
//		}
//	}
}
