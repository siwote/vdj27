package com.lehand.duty.controller.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkMyTaskLogService;
import com.lehand.duty.service.TaskDetailsService;
import com.lehand.duty.dto.*;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkMyTask;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.exception.LehandException;
import com.lehand.duty.controller.DutyBaseController;

@Api(value = "任务详情", tags = { "任务详情" })
@RestController
@RequestMapping("/duty/taskDetails")
public class TaskDetailsController extends DutyBaseController{
	
	@Resource private TaskDetailsService taskDetailsService;
	@Resource private TkMyTaskLogService tkMyTaskLogDao;
	private static final Logger logger = LogManager.getLogger(TaskDetailsController.class);
	
	/**
	 * 分页查询反馈详情
	 * @author pantao
	 * @date 2018年11月20日下午9:56:51
	 * 
	 * @param myid
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "分页查询反馈详情", notes = "分页查询反馈详情", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(Long myid ,Pager pager) {
		Message message = new Message();
		try {
			int count = taskDetailsService.count(getSession().getCompid(),myid);
			List<TkMyTaskLogResponse> list= taskDetailsService.page(myid,pager,getSession());
			pager.setRows(list);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 查询某条反馈以及其下附件评论回复信息
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查询某条反馈以及其下附件评论回复信息", notes = "查询某条反馈以及其下附件评论回复信息", httpMethod = "POST")
	@RequestMapping("/feedpage")
	public Message feedPage(Long feedid) {
		Message message = new Message();
		try {
			TkMyTaskLogResponse list= taskDetailsService.refreshPage(feedid,getSession());
			message.setData(list);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
//	/**
//	 * 我的任务查询下一阶段的提醒时间（普通任务反馈详情页面）
//	 * @author pantao
//	 * @date 2018年11月21日下午12:00:19
//	 * 
//	 * @param myid
//	 * @return
//	 */
//	@RequestMapping("/getRemindTime")
//	public Message getRemindTimeByMyTaskId(Long myid ) {
//		Message message = new Message();
//		try {
//			String time = taskDetailsBusiness.get(getSession().getCompid(),myid);
//			message.setData(time);
//			message.success();
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(), e);
//		}
//		return message;
//	}
	
	/**
	 * 我的任务周期任务查询时间节点（周期任务反馈详情页面）
	 * @author pantao
	 * @date 2018年11月21日下午12:55:29
	 * 
	 * @param myid
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "我的任务周期任务查询时间节点（周期任务反馈详情页面）", notes = "我的任务周期任务查询时间节点（周期任务反馈详情页面）", httpMethod = "POST")
	@RequestMapping("/listTime")
	public Message listTimeByMyTaskId(Long myid,Pager pager ) {
		Message message = new Message();
		try {
			int count = taskDetailsService.countByTaskIdAndSubjectId(getSession().getCompid(),myid, getSession().getUserid());
			TimeNodeResponse times = taskDetailsService.list(getSession().getCompid(),myid, pager,getSession().getUserid());
			pager.setRows(times);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
//	/**
//	 * 已部署任务反馈详情页面普通任务下一个反馈时间
//	 * @author pantao
//	 * @date 2018年12月3日上午10:28:13
//	 * 
//	 * @param taskid
//	 * @return
//	 */
//	@RequestMapping("/getTimeByTaskid")
//	public Message getRemindTimeByTaskId(Long taskid,Long subjectid) {
//		Message message = new Message();
//		try {
//			String time = taskDetailsBusiness.getRemindTime(getSession().getCompid(),taskid,subjectid);
//			message.setData(time);
//			message.success();
//		} catch (Exception e) {
//			message.setMessage(e.getMessage());
//			logger.error(e.getMessage(), e);
//		}
//		return message;
//	}
	
	/**
	 * 已部署任务反馈详情页面周期任务时间节点集合
	 * @author pantao
	 * @date 2018年12月3日上午10:30:33
	 * 
	 * @param taskid
	 * @param pager
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "已部署任务反馈详情页面周期任务时间节点集合", notes = "已部署任务反馈详情页面周期任务时间节点集合", httpMethod = "POST")
	@RequestMapping("/listTimeByTaskid")
	public Message listTimeByTaskId(Long taskid,Pager pager ) {
		Message message = new Message();
		try {
			int count = taskDetailsService.countByTaskId(getSession().getCompid(),taskid);
			TimeNodeResponse times = taskDetailsService.deployedList(getSession().getCompid(),taskid, pager);
			pager.setRows(times);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 已部署任务反馈详情列表中获取任务执行人集合
	 * @author pantao
	 * @date 2018年12月3日上午10:52:11
	 * 
	 * @param taskid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "已部署任务反馈详情列表中获取任务执行人集合", notes = "已部署任务反馈详情列表中获取任务执行人集合", httpMethod = "POST")
	@RequestMapping("/listDepolySubjects")
	public Message listDepolySubjects(Long taskid,Pager pager,String name ) {
		int flag =0;
		Message message = new Message();
		try {
			int count = taskDetailsService.count(getSession().getCompid(),taskid,flag,name);
			List<StatusSubject> subjects= taskDetailsService.listDepolySubjects(getSession().getCompid(),taskid,flag, pager,name);
			pager.setRows(subjects);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	
	/**
	 * 获取我的任务id
	 * @author pantao
	 * @date 2018年12月3日上午11:39:57
	 * 
	 * @param taskid
	 * @param subjectid
	 * @param date
	 * @param period
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取我的任务id", notes = "获取我的任务id", httpMethod = "POST")
	@RequestMapping("/getMyTaskId")
	public Message getMyTaskId(Long taskid,Long subjectid,String date,int period ) {
		Message message = new Message();
		try {
			Long mytaskid= taskDetailsService.getMyTaskId(getSession().getCompid(),taskid,subjectid,date,period);
			message.setData(mytaskid);
			message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e) {
			message.setMessage("获取任务失败");
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 新增评论
	 * @param request
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "新增评论", notes = "新增评论", httpMethod = "POST")
	@RequestMapping("/comment")
	public Message comment(CommentRequest request) {
		Message message = new Message();
		try {
			request.validate();
			taskDetailsService.insertComment(TaskProcessEnum.COMMENT,request, getSession());
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("新增评论失败!");
		}
		return message;
	}
	
	/**
	 * 删除评论
	 * @param commentId
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "删除评论", notes = "删除评论", httpMethod = "POST")
	@RequestMapping("/deleteComment")
	public Message deleteComment(Long commentId) {
		Message message = new Message();
		try {
			taskDetailsService.deleteComment(getSession().getCompid(),commentId);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			message.setMessage("删除评论失败!");
		}
		return message;
	}
	
	/**
	 * 获取签收日志数据
	 * @param taskid
	 * @param orgName
	 * @param subName
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取签收日志数据", notes = "获取签收日志数据", httpMethod = "POST")
	@RequestMapping("/getSignLog")
	public Message getSignLog(String status, Long taskid,String orgName,String subName,Pager pager,int period,String datenode,Long userid){
		Message message = new Message();
		try {
			int count = taskDetailsService.getSignLogNum(getSession().getCompid(),status, taskid, subName,orgName,userid);
			List<SignLogResponse> data = taskDetailsService.getSignLog(getSession().getCompid(),status,taskid, orgName, subName,pager,period,datenode,0,userid);
			pager.setRows(data);
			pager.setTotalRows(count);
			int i = count % pager.getPageSize();
			int v = count / pager.getPageSize();
			pager.setTotalPages(i>0 ? v+1 : v);
			message.setData(pager);
			message.success();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return message;
	}
	
	/**
	 * 获取签收日志各状态的数量
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取签收日志各状态的数量", notes = "获取签收日志各状态的数量", httpMethod = "POST")
	@RequestMapping("/getSignStatus")
	public Message getSignStatus(Long taskid,int period,String datenode){
		Message message = new Message();
		try {
			if (StringUtils.isEmpty(taskid)){
				message.setMessage("任务id不能为空 ");
				message.setData(new ArrayList<>(0));
				return message;
			}
			List<Map<String, Object>> data=taskDetailsService.getStatusCount(getSession().getCompid(),taskid,period,datenode);
			message.setData(data);
			message.success();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
			message.setMessage("各状态数量查询出错!");
		}
		return message;
	}
	
	/**
	 * 查看退回原因
	 * @param mytaskid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "查看退回原因", notes = "查看退回原因", httpMethod = "POST")
	@RequestMapping("/getRemarks")
	public Message getRemarks3(Long mytaskid) {
		Message message = new Message();
		try {
			TkMyTask tkMyTask = taskDetailsService.getRemarks3(getSession().getCompid(),mytaskid);
			message.setData(tkMyTask.getRemarks3());
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 获取我的任务最新的反馈进度
	 * @param mytaskid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取我的任务最新的反馈进度", notes = "获取我的任务最新的反馈进度", httpMethod = "POST")
	@RequestMapping("/getProgress")
	public Message getProgress(Long mytaskid) {
		Message message = new Message();
		try {
			Double num = taskDetailsService.getProgress(getSession().getCompid(),mytaskid);
			message.setData(num);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 获得点赞
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获得点赞", notes = "获得点赞", httpMethod = "POST")
	@RequestMapping("/getParise")
	public Message getParise(Long feedid) {
		Message message = new Message();
		try {
			taskDetailsService.getParise(feedid,getSession(),TaskProcessEnum.FEED_PARISE);
			message.success();
		}catch (LehandException e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 反馈取消点赞
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "反馈取消点赞", notes = "反馈取消点赞", httpMethod = "POST")
	@RequestMapping("/awayParise")
	public Message awayParise(Long feedid) {
		Message message = new Message();
		try {
			taskDetailsService.awayParise(feedid,getSession());
			message.success();
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
	
	/**
	 * 获取点赞数量
	 * @param feedid
	 * @return
	 */
    @OpenApi(optflag=0)
    @ApiOperation(value = "获取点赞数量", notes = "获取点赞数量", httpMethod = "POST")
	@RequestMapping("/getNum")
	public Message getNum(Long feedid) {
		Message message = new Message();
		try {
			Map<String,Object> map =taskDetailsService.getNum(feedid,getSession());
			message.success().setData(map);
		}catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
}
