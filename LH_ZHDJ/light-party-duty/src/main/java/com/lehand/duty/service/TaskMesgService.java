package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Message;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
//import com.lehand.sms.SMSComponent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class TaskMesgService {
	
	@Resource GeneralSqlComponent generalSqlComponent;
	
	//短信
//	@Resource private SMSComponent smsComponent;

	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor=Exception.class)
	@Scope(value="prototype")
	public Message insertSmsNew(String account, String content) {
		Message msg = new Message();
		Map<String,Object> user = generalSqlComponent.query(SqlCode.getUserByAccount, new Object[] {account});
		if (null == user) {
			msg.setMessage("账户不存在!");
			return msg;
		}
		if(StringUtils.isEmpty(user.get("phone").toString())) {
			msg.setMessage("该账户未维护联系方式，请联系管理员!");
			return msg;	
		}
		String phone = user.get("phone").toString();
		String result = null;
		String id = UUID.randomUUID().toString().replaceAll("-", "");
		//发送短信
		try {
//			result = smsComponent.sendSms(id, phone, content, "");
//			if(!StringUtils.isEmpty(result)) {
//				Map<String, String> objMap = JSON.parseObject(result, Map.class);
//				if("AAAAAAA".equals(objMap.get("code"))) {
//					//插入数据
//					Map<String, Object> params = new HashMap<String, Object>(9);
//					params.put("id", id);
//					params.put("code", "task");
//					params.put("phone", phone);
//					params.put("account", account);
//					params.put("vertime", DateEnum.YYYYMMDDHHMMDD.format());
//					params.put("remark1", 0);
//					params.put("remark2", 0);
//					params.put("remark3", content);
//					params.put("remark4", "");
//					generalSqlComponent.insert(SqlCode.addSmsVerif, params);
//
//					msg = Message.SUCCESS;
//					msg.setMessage(URLDecoder.decode(objMap.get("message").toString(), "GBK"));
//				}else {
//					msg.fail(URLDecoder.decode(objMap.get("message").toString(), "GBK"));
//					return msg;
//				}
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}
}
