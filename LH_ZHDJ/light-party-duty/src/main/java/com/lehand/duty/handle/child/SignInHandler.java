package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.dto.urc.UrcOrgan;
import com.lehand.duty.dto.urc.UrcUser;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Component
public class SignInHandler extends BaseHandler {

	@Resource
	private GetIntegerHandler getIntegerHandler;
	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		//init
		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
		request.setSynchronizationTime(DateEnum.YYYYMMDDHHMMDD.format());
		Session session = data.getParam(1);
		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
		if(myTask==null) {
			LehandException.throwException("该任务已经被撤销，请返回！");
		}
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
		if(deploy==null) {
			LehandException.throwException("该任务已经被撤销，请返回！");
		}
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),deploy.getId());
		data.setMyTaskLogRequest(request);
		data.setDeploy(deploy);
		data.setMyTask(myTask);
		data.setDeploySub(deploySub);
		
		//process
		if(deploy.getPeriod()==0) {
			if(deploy.getExsjnum().equals(deploySub.getReceivenum()+1)) {
				tkTaskDeployDao.updateTkTaskDeployStauts(myTask.getCompid(), Constant.TkTaskDeploy.SIGNED_STATUS, deploy.getId());
			}
		}else {
			int num = tkMyTaskDao.countBySign(deploy.getCompid(),deploy.getId(),deploy.getExsjnum());
			if(num>0) {
				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SIGNED_STATUS);
			}
		}
		
		//签收加积分
//		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(deploy.getCompid(),TaskProcessEnum.SIGN_IN.toString());
//		if(tkIntegralRule.getInteval()!=0) {
//			tkIntegralDetailsDao.insert(session.getCompid(),session.getUserid(),session.getUsername(),TaskProcessEnum.SIGN_IN.toString(),deploy.getId(),myTask.getId(),tkIntegralRule.getInteval(),0L);
//		}
//		Double inteval = tkIntegralRule.getInteval();
//		data.putRemindParam("inteval", inteval);
//		getIntegerHandler.process(TaskProcessEnum.GET_INTEGER, session, inteval,2.0,1.0,myTask);
		
		//detail
		if(StringUtils.isEmpty(request.getMessage())) {
			request.setMessage("签收了任务！");
		}
		TkMyTaskLog log = generalSqlComponent.query(SqlCode.getSignLog, new Object[]{session.getCompid(), deploy.getId(), session.getUserid()});
		if(log ==null){
			log = tkMyTaskLogService.insert(deploy.getCompid(),deploy,myTask, request, processEnum, data.getSession(),Constant.EMPTY,Constant.EMPTY,0);
		}else{
			//删除这个执行人重新添加，签收时更新签收时间
			generalSqlComponent.update(SqlCode.getSignLog, new Object[]{DateEnum.YYYYMMDDHHMMDD.format(), session.getCompid(),log.getId()});
		}
		data.setMyTaskLog(log);
		//remind
//		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),request.getMytaskid(),request.getNow());
//		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), request.getMytaskid(), processEnum.getRemindRule(), 
//				                     request.getNow(), 
//				                     data.getSessionSubject(), 
//				                     data.getDeploySubject());
		if(deploySub!=null) {
			deploySub.setReceivenum(deploySub.getReceivenum()+1);
		}
		TkTaskRemindList ls = null;
		if(deploy.getRemarks2()==0) {//直发
			ls = insert(session, Module.MY_TASK.getValue(),myTask.getId(), "SIGN_TASK", "任务被签收", "您有一条任务被签收。",deploy.getUserid(),deploy.getUsername());
		}else {//跨级
//			PwOrgan org = generalSqlComponent.query(SqlCode.getPOByRemarks3, new Object[] {session.getUserid(),deploy.getCompid()});
//			PwOrgan orgpid = generalSqlComponent.query(SqlCode.getPOByid, new Object[] {org.getPid(),deploy.getCompid()});
			UrcOrgan org = generalSqlComponent.query(SqlCode.getPOByRemarks3, new Object[] {session.getUserid(),deploy.getCompid()});
			UrcOrgan orgpid = generalSqlComponent.query(SqlCode.getPOByid, new Object[] {org.getPorgid(),deploy.getCompid()});
			if(orgpid!=null) {
//				PwUser user = generalSqlComponent.query(SqlCode.getPUUserid,new Object[] {Long.valueOf(orgpid.getRemarks3())});
				UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid,new Object[] {Long.valueOf(orgpid.getRemarks3())});
				ls = insert(session, Module.MY_TASK.getValue(),myTask.getId(), "SIGN_TASK", "任务被签收", "您有一条任务被签收。",user.getUserid(),user.getUsername());
			}else {
//				PwUser user = generalSqlComponent.query(SqlCode.getPUUserid,new Object[] {Long.valueOf(org.getRemarks3())});
				UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid,new Object[] {Long.valueOf(org.getRemarks3())});
				ls = insert(session, Module.MY_TASK.getValue(),myTask.getId(), "SIGN_TASK", "任务被签收", "您有一条任务被签收。",user.getUserid(),user.getUsername());
			}
		}
		Long id = generalSqlComponent.insert(SqlCode.addTTRLByTuihui, ls);
		ls.setId(id);
	}
	
	/**
	   * 消息对象的创建
	 * @param session 发送人信息
	 * @param module 模块
	 * @param mid 模块对应的id
	 * @param rulecode 提醒规则
	 * @param rulename 规则名称
	 * @param mesg 提醒消息文本
	 * @param subjectid 被提醒人id
	 * @param subjectname 被提醒人名称
	 * @return
	 */
	private TkTaskRemindList insert(Session session,int module,Long mid,String rulecode,String rulename,String mesg,Long subjectid,String subjectname) {
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(session.getCompid());
		ls.setNoteid(-1L);
		ls.setModule(module);//模块(0:任务发布,1:任务反馈)
		ls.setMid(mid);//对应模块的ID
		ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format());//提醒日期
		ls.setRulecode(rulecode);//编码
		ls.setRulename(rulename);
		ls.setOptuserid(session.getUserid());
		ls.setOptusername(session.getUsername());
		ls.setUserid(subjectid);//被提醒人
		ls.setUsername(subjectname);//
		ls.setIsread(0);//
		ls.setIshandle(0);//
		ls.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
		ls.setMesg(mesg);//消息
		ls.setRemarks1(1);//备注1
		ls.setRemarks2(0);//备注2
		ls.setRemarks3("工作督查");//备注3
		ls.setRemarks4(Constant.EMPTY);//备注3
		ls.setRemarks5(Constant.EMPTY);//备注3
		ls.setRemarks6(Constant.EMPTY);//备注3
		return ls;
	}

//	protected void init(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
//		Session session = data.getParam(1);
//		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
//		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),deploy.getId());
//		data.setMyTaskLogRequest(request);
//		data.setDeploy(deploy);
//		data.setMyTask(myTask);
//		data.setDeploySub(deploySub);
//	}
	
//	protected void process(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getDeploy();
//		TkTaskDeploySub deploySub = data.getDeploySub();
//		TkMyTask myTask =data.getMyTask();
//		if(deploy.getPeriod()==0) {
//			if(deploy.getExsjnum().equals(deploySub.getReceivenum()+1)) {
//				tkTaskDeployDao.updateTkTaskDeployStauts(myTask.getCompid(),Constant.TkTaskDeploy.SIGNED_STATUS, deploy.getId());
//			}
//		}else {
//			int num = tkMyTaskDao.countBySign(deploy.getCompid(),deploy.getId(),deploy.getExsjnum());
//			if(num>0) {
//				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SIGNED_STATUS);
//			}
//		}
//		
//		//签收加积分
//		Session session = data.getSession();
//		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(deploy.getCompid(),TaskProcessEnum.SIGN_IN.toString());
//		if(tkIntegralRule.getInteval()!=0) {
//			tkIntegralDetailsDao.insert(session.getCompid(),session.getUserid(),session.getUsername(),TaskProcessEnum.SIGN_IN.toString(),deploy.getId(),myTask.getId(),tkIntegralRule.getInteval(),0L);
//		}
//		Double inteval = tkIntegralRule.getInteval();
//		data.putRemindParam("inteval", inteval);
//	    GET_INTEGER.handle(data.getSession(),inteval,2.0,1.0,myTask);
//	}

//	protected void detail(TaskProcessData data) throws LehandException {
//		TkMyTask myTask = data.getMyTask();
//		TkTaskDeploy deploy = data.getDeploy();
//		MyTaskLogRequest request = data.getMyTaskLogRequest(); 
//		if(StringUtils.isEmpty(request.getMessage())) {
//			request.setMessage("签收了任务！");
//		}
//		TkMyTaskLog log = tkMyTaskLogDao.insert(deploy.getCompid(),deploy,myTask, request, this, data.getSession(),Constant.EMPTY,Constant.EMPTY,0);
//		
//		data.setMyTaskLog(log);
//	}
	
//	protected void remind(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		supervise(data,module,remindRule,request.getMytaskid(),request.getNow());
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, request.getMytaskid(), remindRule, 
//				                     request.getNow(), 
//				                     data.getSessionSubject(), 
//				                     data.getDeploySubject());
//	}

	protected void setTaskDeploySub(TaskProcessData data) {
		TkTaskDeploySub deploySub = data.getDeploySub();
		if(deploySub!=null) {
			deploySub.setReceivenum(deploySub.getReceivenum()+1);
		}
	}
}
