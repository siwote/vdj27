package com.lehand.duty.handle.child;

import com.lehand.base.common.Message;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dao.TkTaskClassDao;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.dto.urc.UrcOrgan;
import com.lehand.duty.dto.urc.UrcUser;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.*;
//import com.lehand.sms.SMSComponent;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.*;

@Component
public class FeedBackHandler extends BaseHandler {
	
//	@Resource private SMSComponent smsComponent;
@Resource private TkTaskClassDao tkTaskClassDao;
	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
		Session session = data.getParam(1);
		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
		if(myTask==null) {
			LehandException.throwException("该任务已经被撤销，请返回！");
		}
		if(myTask.getStatus()==4) {
			LehandException.throwException("该任务已经被暂停，请返回！");
		}
		if(myTask.getStatus()==5) {
			LehandException.throwException("该任务已经被确认完成，请返回！");
		}

		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
		if(deploy==null) {
			LehandException.throwException("该任务已经被撤销，请返回！");
		}
		if(deploy.getStatus()==45) {
			LehandException.throwException("该任务已经被暂停，请返回！");
		}
		if(deploy.getStatus()==50) {
			LehandException.throwException("该任务已经被确认完成，请返回！");
		}
		removeTodoItem(Todo.ItemType.TYPE_DEPLOY, String.valueOf(myTask.getTaskid()), session);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTask.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTaskLogRequest(request);
		data.setMyTask(myTask);
		data.setDeploy(deploy);
		
		//process
		// 更新我的任务的进度和最新反馈时间
		tkMyTaskDao.updateProgressByID(myTask.getCompid(),request.getNow(), request.getProgress(), myTask.getId());
		// 更新任务发布表进度和最新反馈时间(排除退回的)
		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(myTask.getCompid(),myTask.getTaskid());
		avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
		tkTaskDeployDao.updateProgressAndnewbacktime(deploy.getCompid(),avgprogress, request.getNow(), myTask.getTaskid());
		
		//detail
		long nowtime = DateEnum.now().getTime();
		long feedtime = 0L;
		int remarks2=0;
		try {
		    feedtime = DateEnum.YYYYMMDDHHMMDD.parse(request.getRemindtime()).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(nowtime>feedtime) {
			//逾期反馈时得先查询下之前是否存在正常反馈，如果存在这次逾期反馈就不算，仍旧算正常反馈
			int num = generalSqlComponent.query(SqlCode.listTMTLCount, new Object[] {data.getSession().getCompid(),deploy.getId(),data.getSession().getUserid(),request.getRemindtime(),Constant.colourone});
			if(num<=0) {
				remarks2=2;//逾期反馈为2
			}else {
				remarks2=1;//正常反馈为1
			}
		}else {
			remarks2=1;//正常反馈为1
		}
		TkMyTaskLog log = tkMyTaskLogService.insert(deploy.getCompid(),deploy,myTask, request, processEnum, data.getSession(),Constant.EMPTY,request.getRemindtime(),remarks2);
		//反馈表新增完成后需要更新主任务表和子任务表的最新反馈时间（newbacktime 和  newtime）
		generalSqlComponent.update(SqlCode.modiTTDNewbacktime, new Object[] {DateEnum.YYYYMMDDHHMMDD.format(),log.getTaskid(),deploy.getCompid()});
		generalSqlComponent.update(SqlCode.modiTMTNewtime, new Object[] {DateEnum.YYYYMMDDHHMMDD.format(),log.getMytaskid(),deploy.getCompid()});
		/**反馈记录插入过后需要对资料库的关系表进行附件关系的插入*/
		String uuid = request.getUuid();
		if(!StringUtils.isEmpty(uuid)) {
			List<TkAttachment> ta = generalSqlComponent.query(SqlCode.listAttachments, new Object[] {uuid});
			for (TkAttachment tkAttachment : ta) {
				DlFileBusiness dfb = new DlFileBusiness();
				dfb.setBusinessid(log.getId());
				dfb.setBusinessname((short)3);
				dfb.setCompid(session.getCompid());
				dfb.setFileid(Long.valueOf(tkAttachment.getRemarks4()));
				dfb.setIntostatus((short)0);
				int num = generalSqlComponent.query(SqlCode.getDFB ,new Object[] {session.getCompid(),tkAttachment.getRemarks4(),log.getId(),3});
				if(num<=0) {
					generalSqlComponent.insert(SqlCode.addDlFileBusiness, dfb);
				}
			}
		}
		//TODO 新增反馈完成后需要查询该执行机构的父节点 ，并在反馈审核表中插入一条数据，状态默认为待审核
		//找到对应的机构
//		PwOrgan org = generalSqlComponent.query(SqlCode.getPOByRemarks3, new Object[] {session.getUserid(),deploy.getCompid()});
//		PwOrgan orgpid = generalSqlComponent.query(SqlCode.getPOByid, new Object[] {org.getPid(),deploy.getCompid()});

		UrcOrgan org = generalSqlComponent.query(SqlCode.getPOByRemarks3, new Object[] {session.getUserid(),deploy.getCompid()});
		UrcOrgan orgpid = generalSqlComponent.query(SqlCode.getPOByid, new Object[] {org.getPorgid(),deploy.getCompid()});
		//再通过该机构的pid去查其父节点的机构信息并取得其
//		int num = generalSqlComponent.query(SqlCode.getPUOMBynum, new Object[] {session.getCompid(),orgpid.getId()});
		int num = generalSqlComponent.query(SqlCode.getPUOMBynum, new Object[] {session.getCompid(),orgpid.getOrgid()});
		TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
		if(num>1) {
			audit.setSubjectId(deploy.getUserid());
		}else {
			audit.setSubjectId(Long.valueOf(orgpid.getRemarks3()));
		}
		audit.setCompid(session.getCompid().intValue());
		audit.setTaskid(log.getTaskid());
		audit.setMytaskid(log.getMytaskid());
		audit.setFeedbackid(log.getId());
		if(request.getAudituserid()!=null){
			audit.setSubjectId(request.getAudituserid());
		}
		audit.setSubtype(Constant.status);//TODO 这里暂时定为0，后续根据需要再修改
		audit.setStatus(Constant.status);
		audit.setRemark(StringConstant.EMPTY);
		audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		audit.setOpttime(StringConstant.EMPTY);
		audit.setRemarks2(Constant.status);//0代表反馈审核，1代表退回任务审核
		audit.setRemarks3(StringConstant.EMPTY);
		audit.setRemarks4(StringConstant.EMPTY);
		audit.setRemarks5(StringConstant.EMPTY);
		audit.setRemarks6(StringConstant.EMPTY);
		Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
		audit.setId(id);
		
		//创建审核人的待办信息 TODO
		insertMyToDoList(2, deploy.getId().toString(),"《"+deploy.getTkname()+"》"+"待您审核。", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), audit.getSubjectId().toString());
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),deploy.getClassid(),1);
		addTodoItem(Todo.ItemType.TYPE_REPORT_TASK, deploy.getTkname() +  "(" +taskClass.getClname() + ")", String.valueOf(id),  Todo.FunctionURL.TASK_FEEDBACK_AUDIT.getPath(), audit.getSubjectId(), session);
//
//		//创建短信消息
//		insertSms("您有一条上报任务待审核，任务名称："+deploy.getTkname(), getPhones(session.getCompid(), audit.getSubjectId()));
		
		
		if (!StringUtils.isEmpty(request.getUuid())) {//这里这个3是根据资料库中规定来的
			tkAttachmentService.updateId(deploy.getCompid(),log.getId(), 3, request.getUuid());
		}
		data.setMyTaskLog(log);
		
		if(deploySub!=null) {
			deploySub.setFbacknum(deploySub.getFbacknum()+1);
		}
		
//		TkTaskRemindList ls = insert(session, audit, "FEED_AUDIT", "反馈审核", "有一条新的任务反馈，请审核。");
//		long zid = generalSqlComponent.insert(SqlCode.addTTRLByTuihui, ls);
//		ls.setId(zid);
	}
	
	
	/**
	 * a 新增短信验证码
	 * @Title：insertSms 
	 * @Description：TODO
	 * @param ：@param object 
	 * @return ：void 
	 * @throws
	 */
	@Transactional(rollbackFor=Exception.class)
	@Scope(value="prototype")
	public Message insertSms(String content,String phones) {
		Message msg = new Message();
		String id = UUID.randomUUID().toString().replaceAll("-", "");
		//发送短信
		String[] ph = phones.split(",");
		try {
			for (String phone : ph) {
//				smsComponent.sendSms(id, phone, content, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}
	
	
	/**
	 * Description: 根据短信接收人的id查询对应的手机号集合
	 * @author PT  
	 * @date 2019年11月12日 
	 * @param subjectid
	 * @return
	 */
	public String getPhones(Long compid,Long subjectid) {
		List<Map<String,Object>> lists = generalSqlComponent.query(SqlCode.listPhones, new Object[] {compid,subjectid});
		String phones = "";
		if(lists!=null && lists.size()>0) {
			for (Map<String, Object> map : lists) {
				phones += map.get("remarks3")+",";
			}
			if(!StringUtils.isEmpty(phones)) {
				phones = phones.substring(0,phones.length()-1);
			}
		}
		return phones;
	}
	
	public void insertMyToDoList(int module,String busid,String content,String remind,String optuserid,String userid) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("module", module);
		paramMap.put("busid", busid);
		paramMap.put("content", content);
		paramMap.put("remind", remind);
		paramMap.put("optuserid", optuserid);
		paramMap.put("userid", userid);
		paramMap.put("isread", 0);
		paramMap.put("ishandle", 0);
		generalSqlComponent.getDbComponent().insert("INSERT INTO my_to_do_list (module, busid, content, remind, optuserid, userid, isread, ishandle) VALUES (:module, :busid, :content, :remind, :optuserid, :userid, :isread, :ishandle)", paramMap);
	}

	private TkTaskRemindList insert(Session session,TkTaskFeedBackAudit info,String rulecode,String rulename,String mesg) {
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(session.getCompid());//租户id
		ls.setNoteid(-1L);//登记簿id
		ls.setModule(Module.MY_TASK_LOG.getValue());//模块(0:任务发布,1:任务反馈)
		ls.setMid(info.getFeedbackid());//对应模块的ID
		ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format());//提醒日期
		ls.setRulecode(rulecode);//编码
		ls.setRulename(rulename);
		ls.setOptuserid(session.getUserid());
		ls.setOptusername(session.getUsername());
		ls.setUserid(info.getSubjectId());//被提醒人
//		PwUser user = generalSqlComponent.query(SqlCode.getPUUserid,new Object[] {info.getSubjectId()});
		UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid,new Object[] {info.getSubjectId()});
		if(user==null) {
			LehandException.throwException("反馈消息提醒人不存在！");
		}
		ls.setUsername(user.getUsername());//
		ls.setIsread(0);//
		ls.setIshandle(0);//
		ls.setIssend(0);//是否已提醒(0:未提醒,1:已提醒)
		ls.setMesg(mesg);//消息
		ls.setRemarks1(1);//备注1
		ls.setRemarks2(0);//备注2
		ls.setRemarks3("工作督查");//备注3
		ls.setRemarks4(Constant.EMPTY);//备注3
		ls.setRemarks5(Constant.EMPTY);//备注3
		ls.setRemarks6(Constant.EMPTY);//备注3
		return ls;
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
//		Session session = data.getParam(1);
//		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
//		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),myTask.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTaskLogRequest(request);
//		data.setMyTask(myTask);
//		data.setDeploy(deploy);
//	}
	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		TkMyTask myTask = data.getMyTask();
//		TkTaskDeploy deploy = data.getDeploy();
//		// 更新我的任务的进度和最新反馈时间
//		tkMyTaskDao.updateProgressByID(myTask.getCompid(),request.getNow(), request.getProgress(), myTask.getId());
//		// 更新任务发布表进度和最新反馈时间(排除退回的)
//		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(myTask.getCompid(),myTask.getTaskid());
//		avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
//		tkTaskDeployDao.updateProgressAndnewbacktime(deploy.getCompid(),avgprogress, request.getNow(), myTask.getTaskid());
//		//新增反馈加积分超过最大值不加（针对任务的截止时间）
////		Session session = data.getSession();
////		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(deploy.getCompid(),String.valueOf(TaskProcessEnum.FEED_BACK));
////		Double inteval = tkIntegralRule.getInteval();
////		data.putRemindParam("inteval", inteval);
////		if(inteval!=0) {
////			//逾期反馈就扣分（只扣一次）如果结束日期之前有正常反馈的记录就不扣分（逾期未反馈扣分就通过批处理来执行）
////			int count = tkIntegralDetailsDao.count(myTask.getCompid(),myTask.getId(),DateEnum.YYYYMMDDHHMMDD.format(),String.valueOf(TaskProcessEnum.OVER_BACK));
////			if(myTask.getDatenode().compareTo(DateEnum.YYYYMMDDHHMMDD.format())<0 && count<=0) {
////				//暂定OVER_BACK为逾期
////				tkIntegralDetailsDao.insert(session.getCompid(),session.getUserid(),session.getUsername(),String.valueOf(TaskProcessEnum.OVER_BACK),deploy.getId(),myTask.getId(),-inteval,0L);
////			}else {
////				//积分详情表中该任务本月已获得的总积分
////				String date = DateEnum.YYYYMMDD.format();
////				String [] str = date.split("-");
////				String year = str[0];
////				String month = str[1];
////				String mintime = year+"-"+month+"-01 00:00:00";
////				String maxtime = "";
////				if(month.equals("12")) {
////					year = String.valueOf(Integer.valueOf(year)+1);
////					maxtime = year+"-"+"01-01 00:00:00";		
////				}else {
////					month = String.valueOf(Integer.valueOf(month)+1);
////					maxtime = year+"-"+month+"-01 00:00:00";
////				}
////				TkIntegralDetails max = tkIntegralDetailsDao.get(deploy.getCompid(),mintime,maxtime,myTask.getId(),String.valueOf(TaskProcessEnum.FEED_BACK));
////				data.putRemindParam("sumscore", max.getScore());
////				data.putRemindParam("maxval", tkIntegralRule.getMaxval());
////				if(tkIntegralRule.getMaxval().compareTo(max.getScore())>0) {
////					if(myTask.getDatenode().compareTo(DateEnum.YYYYMMDDHHMMDD.format())>0) {
////						if((max.getScore()+inteval)>tkIntegralRule.getMaxval()) {
////							Double score = max.getScore()+inteval-tkIntegralRule.getMaxval();
////							tkIntegralDetailsDao.insert(session.getCompid(),session.getUserid(),session.getUsername(),String.valueOf(TaskProcessEnum.FEED_BACK),deploy.getId(),myTask.getId(),score,0L);
////						}else {
////							tkIntegralDetailsDao.insert(session.getCompid(),session.getUserid(),session.getUsername(),String.valueOf(TaskProcessEnum.FEED_BACK),deploy.getId(),myTask.getId(),inteval,0L);
////						}
////					}
////				}
////				//发送"本月该任务您已达到积分上限，虽无积分，但仍然鼓励您及时反馈任务进展。"消息之前先判断是否已经发过了，发过了就不再发送
////				//发送积分消息时mid字段存储的是主任务id
////				//根据主任务id和subjectid查询GET_INTEGER的最新的一条记录，根据remarks4字段判断下获取的最大值和设置的最大值是否相等如果相等就不发 ，小于就发
////				TkTaskRemindList info = tkTaskRemindListDao.getNewMessage(deploy.getCompid(),deploy.getId(),data.getSession().getUserid());
////				if(info!=null && info.getMesg().length()<30) {
////					GET_INTEGER.handle(data.getSession(),inteval,tkIntegralRule.getMaxval(),max.getScore(),myTask);
////				}
////			}
////		}
//	}
	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//		TkMyTask myTask = data.getMyTask();
//		TkTaskDeploy deploy = data.getDeploy();
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		long nowtime = DateEnum.now().getTime();
//		long feedtime = 0L;
//		int remarks2=0;
//		try {
//		    feedtime = DateEnum.YYYYMMDDHHMMDD.parse(request.getRemindtime()).getTime();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		if(nowtime>feedtime) {
//			remarks2=1;//逾期反馈为1
//		}else {
//			remarks2=0;//正常反馈为0 
//		}
//		TkMyTaskLog log = tkMyTaskLogDao.insert(deploy.getCompid(),deploy,myTask, request, this, data.getSession(),Constant.EMPTY,request.getRemindtime(),remarks2);
//		Session session = data.getSession();
//		//TODO 新增反馈完成后需要查询该执行机构的父节点 ，并在反馈审核表中插入一条数据，状态默认为待审核
//		PwOrgan org = generalSqlComponent.query(SqlCode.getPOByid, new Object[] {session.getUserid()});
//		TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
//		audit.setCompid(session.getCompid().intValue());
//		audit.setTaskid(log.getTaskid());
//		audit.setMytaskid(log.getMytaskid());
//		audit.setFeedbackid(log.getId());
//		audit.setSubjectid(Long.valueOf(org.getRemarks3()));
//		audit.setSubtype(Constant.status);//TODO 这里暂时定为0，后续根据需要再修改
//		audit.setStatus(Constant.status);
//		audit.setRemark(StringConstant.EMPTY);
//		audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
//		audit.setOpttime(StringConstant.EMPTY);
//		audit.setRemarks2(Constant.status);//0代表反馈审核，1代表退回任务审核
//		audit.setRemarks3(StringConstant.EMPTY);
//		audit.setRemarks4(StringConstant.EMPTY);
//		audit.setRemarks5(StringConstant.EMPTY);
//		audit.setRemarks6(StringConstant.EMPTY);
//		Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
//		audit.setId(id);
//		
//		if (!StringUtils.isEmpty(request.getUuid())) {
//			tkAttachmentDao.updateId(deploy.getCompid(),log.getId(), Module.MY_TASK_LOG.getValue(), request.getUuid());
//		}
//		data.setMyTaskLog(log);
//	}
	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
////		MyTaskLogRequest request = data.getMyTaskLogRequest();
////		TkMyTask mytask = data.getMyTask();
////		//超过截止日期反馈时只进行扣分不发消息
////		if(mytask.getDatenode().compareTo(DateEnum.YYYYMMDDHHMMDD.format())>=0) {
////			supervise(data,module,remindRule,request.getMytaskid(),request.getNow());
////			remindNoteComponent.register(data.getDeploy().getCompid(),module, data.getMyTaskLog().getId(), remindRule, 
////					request.getNow(), 
////					data.getSessionSubject(), 
////					data.getDeploySubject());
////		}
//	}

//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//		TkTaskDeploySub deploySub = data.getDeploySub();
//		if(deploySub!=null) {
//			deploySub.setFbacknum(deploySub.getFbacknum()+1);
//		}
//	}




	/**待办任务begin****************************************************************************************************************/
	@Resource
	private TodoItemService todoItemService;

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param name 事项名称
	 * @param bizid 该事项对应的业务ID
	 * @param path 资源路径
	 * @param session 当前会话
	 */
	private void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path, Long subjectId, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setBizid(bizid);
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setUserid(session.getUserid());
		itemDTO.setUsername(session.getUsername());
		itemDTO.setOrgid(session.getCurrorganid());
		itemDTO.setName(name);
		itemDTO.setBizid(bizid);
		itemDTO.setTime(new Date());
		itemDTO.setPath(path);
		itemDTO.setState(Todo.STATUS_NO_READ);
		itemDTO.setSubjectid(subjectId);
		todoItemService.createTodoItem(itemDTO);
	}

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param bizid 该事项对应的业务ID
	 * @param session 当前会话
	 */
	private void removeTodoItem(Todo.ItemType itemType,  String bizid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setUserid(session.getUserid());
		itemDTO.setBizid(bizid);
		todoItemService.deleteTodoItem(itemDTO);
	}
	/**待办任务end****************************************************************************************************************/
}
