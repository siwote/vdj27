package com.lehand.duty.dto;

/**
 * 普通任务和周期任务详情
 * 
 * @author pantao
 * @date 2018年11月15日上午9:17:27
 *
 */
public class TkIntegralRespond {
	
	Double sumscore;//总积分
	Double addscore;//得分
	Double deductscore;//扣分
	String year;//当前年份
	Double yearaddscore;//当前年份得分
	Double yeardedcutscore;//当前年份扣分
	int scorerank;//积分排名
	int ranknums;//总排名人数
	int praisecount;//总点赞次数
	public Double getSumscore() {
		return sumscore;
	}
	public void setSumscore(Double sumscore) {
		this.sumscore = sumscore;
	}
	public Double getAddscore() {
		return addscore;
	}
	public void setAddscore(Double addscore) {
		this.addscore = addscore;
	}
	public Double getDeductscore() {
		return deductscore;
	}
	public void setDeductscore(Double deductscore) {
		this.deductscore = deductscore;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Double getYearaddscore() {
		return yearaddscore;
	}
	public void setYearaddscore(Double yearaddscore) {
		this.yearaddscore = yearaddscore;
	}
	public Double getYeardedcutscore() {
		return yeardedcutscore;
	}
	public void setYeardedcutscore(Double yeardedcutscore) {
		this.yeardedcutscore = yeardedcutscore;
	}
	public int getScorerank() {
		return scorerank;
	}
	public void setScorerank(int scorerank) {
		this.scorerank = scorerank;
	}
	public int getRanknums() {
		return ranknums;
	}
	public void setRanknums(int ranknums) {
		this.ranknums = ranknums;
	}
	public int getPraisecount() {
		return praisecount;
	}
	public void setPraisecount(int praisecount) {
		this.praisecount = praisecount;
	}
	
}
