package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.dto.TimeResponse;
import com.lehand.duty.pojo.TkResolveInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class TkResolveInfoService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	public void insert(TkResolveInfo info) {
		generalSqlComponent.insert(SqlCode.saveResolveInfo, info);
	}
	
	public void insert(Long compid,String id,Long taskid,String nodename,Double idxvalue,
			String unit,String rsldesc,String timecode,String timename,
			Long objid,String objname,String fbtime,int stageseqno,String stagename,
			String pid,String starttime,String endtime) {
		TkResolveInfo info = new TkResolveInfo();
		info.setCompid(compid);
		info.setId(id);
		info.setTaskid(taskid);
		info.setNodename(nodename);
		info.setIdxvalue(idxvalue);
		info.setUnit(unit);
		info.setRsldesc(rsldesc);
		info.setTimecode(timecode);
		info.setTimename(timename);
		info.setObjid(objid);
		info.setObjname(objname);
		info.setFbtime(fbtime);
		info.setStageseqno(stageseqno);
		info.setStagename(stagename);
		info.setPid(pid);
		info.setStarttime(starttime);
		info.setEndtime(endtime);
		info.setStatus(1);
		info.setRemarks1(0);
		info.setRemarks2(0);
		info.setRemarks3(Constant.EMPTY);
		info.setRemarks4(Constant.EMPTY);
		info.setRemarks5(Constant.EMPTY);
		info.setRemarks6(Constant.EMPTY);
		insert(info);
	}
	
	public List<TimeResponse> list(Long compid, Long taskid, Pager pager){
		Pager page = generalSqlComponent.pageQuery(SqlCode.queryResolveInfoList, new Object[] {compid,taskid}, pager);
		PagerUtils<TimeResponse> pagerUtils = new PagerUtils<TimeResponse>(page);
		return pagerUtils.getRows();
	}
	
	public int countBytaskIdAndSubjectId(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.countBytaskIdAndSubjectId, new Object[] {compid, taskid});
	}
	
	public List<TkResolveInfo> listBean(Long compid,Long taskid,String endtime){
		return generalSqlComponent.query(SqlCode.listResolveinfo, new Object[] {compid,taskid,endtime});
	}

	public TkResolveInfo getBean(Long compid,Long taskid, String endtime) {
		return generalSqlComponent.query(SqlCode.getResoleInfo, new Object[] {compid,taskid,endtime});
	}
	
	public TkResolveInfo getBeanFlag(Long compid,Long taskid, String endtime) {
		return generalSqlComponent.query(SqlCode.getResolveinfoflag, new Object[] {compid,taskid,endtime});
	}
	
	public int getCount(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.getResolveInfoCount, new Object[] {compid,taskid});
	}

	public void deleteByTaskid(Long compid,Long taskid) {
		generalSqlComponent.delete(SqlCode.delResolveInfoListByTid, new Object[] {compid,taskid});
	}
	
	public List<Subject> listObj(Long compid, Long taskid) {
		return generalSqlComponent.query(SqlCode.queryResolveSubjects, new Object[] {compid,taskid});
	}
	
	public List<Double> listMubiaoDuibi(Long compid,Long taskid, String time) {
		return generalSqlComponent.query(SqlCode.listMubiaoDuibi,new Object[] {compid,taskid,time});
	}
	
	public List<Double> listShijiDuibi(Long compid,Long taskid, String time) {
		return generalSqlComponent.query(SqlCode.listShijiDuibi,  new Object[] {compid,compid,taskid,time});
	}
	
	public List<Map<String,Object>> listDuibi(Long compid,Long taskid,String time){
		return generalSqlComponent.query(SqlCode.listDuibi, new Object[] {compid,compid,taskid,time});
	}
	
	public List<Map<String,Object>> listFenbu(Long compid,Long taskid,String time){
		return generalSqlComponent.query(SqlCode.listFenbu, new Object[] {compid,compid,taskid,time});
	}
	
	public List<Map<String,Object>> listZoushi(Long compid,Long taskid,Long subjectid){
		return generalSqlComponent.query(SqlCode.listZoushi, new Object[] {compid,compid,taskid,subjectid});
	}

	public List<Double> listMubiaoZoushi(Long compid,Long taskid, Long subjectid) {
		return generalSqlComponent.query(SqlCode.listMubiaoZoushi, new Object[] { compid,taskid,subjectid});
	}
	
	public List<Double> listShijiZoushi(Long compid,Long taskid, Long subjectid) {
		return generalSqlComponent.query(SqlCode.listShijiZoushi, new Object[] { compid,compid,taskid,subjectid});
	}

	public String getUnit(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.getResolveUnit, new Object[] {compid,compid,taskid});
	}

	public Map<String, Object> getRoot(Long compid,Long taskid, String id) {
		return generalSqlComponent.query(SqlCode.getResolveRoot, new Object[] {compid,compid,taskid,id});
	}
	
	public List<TkResolveInfo> listAllByTaskid(Long compid,Long taskid){
		return generalSqlComponent.query(SqlCode.listAllByTaskid, new Object[] {compid,taskid});
	}
}
