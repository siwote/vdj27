package com.lehand.duty.controller;

import com.lehand.components.web.ann.OpenApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(value = "管理", tags = { "管理" })
@Controller
public class ManagerController {

	private static final Logger logger = LogManager.getLogger(ManagerController.class);
	
	/**
	 * 系统管理页面
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = "系统管理页面", notes = "系统管理页面", httpMethod = "POST")
	@RequestMapping(value="/showmanager",produces="text/html; charset=utf-8")
	public String shutdown() {
		logger.info(".................");
		return "manager";
	}
	
}
