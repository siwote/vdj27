package com.lehand.duty.dto;

public class RelatedToMeResponse {

	private String tkname;//任务名称
	private Long userid;//创建人id
	private String username;//创建人名称
	private Long classid;//任务分类id
	private String clname;//任务分类名称
	private String mesg;//最新反馈内容
	
	private String subjectname;//执行人名称
	private String newtime;//最新反馈时间
	private String status;//反馈时间点分别对应状态
	public String getTkname() {
		return tkname;
	}
	public void setTkname(String tkname) {
		this.tkname = tkname;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getClassid() {
		return classid;
	}
	public void setClassid(Long classid) {
		this.classid = classid;
	}
	public String getClname() {
		return clname;
	}
	public void setClname(String clname) {
		this.clname = clname;
	}
	public String getMesg() {
		return mesg;
	}
	public void setMesg(String mesg) {
		this.mesg = mesg;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public String getNewtime() {
		return newtime;
	}
	public void setNewtime(String newtime) {
		this.newtime = newtime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
