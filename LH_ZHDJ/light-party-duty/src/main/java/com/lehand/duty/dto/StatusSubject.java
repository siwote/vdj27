package com.lehand.duty.dto;

public class StatusSubject {

	private Long subjectid;
	
	private String subjectname;
	
	private Integer status;//查询任务周期集合需要用到
	
	private String surstatus;//查询任务的执行人集合需要用到
	
	

	public String getSurstatus() {
		return surstatus;
	}

	public void setSurstatus(String surstatus) {
		this.surstatus = surstatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public StatusSubject() {
	}

	public StatusSubject(Long subjectid, String subjectname) {
		this.subjectid = subjectid;
		this.subjectname = subjectname;
	}

	public Long getSubjectid() {
		return subjectid;
	}

	public void setSubjectid(Long subjectid) {
		this.subjectid = subjectid;
	}

	public String getSubjectname() {
		return subjectname;
	}

	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subjectid == null) ? 0 : subjectid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusSubject other = (StatusSubject) obj;
		if (subjectid == null) {
			if (other.subjectid != null)
				return false;
		} else if (!subjectid.equals(other.subjectid))
			return false;
		return true;
	}
	
}
