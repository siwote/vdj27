package com.lehand.duty.service;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dao.TkTaskClassDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.pojo.DlType;
import com.lehand.duty.pojo.TkTaskClass;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * 任务分类接口实现类
 * 
 * @author pantao
 * @date 2018年10月9日 下午2:55:55
 *
 */
@Service
public class TkTaskClassService{

    private static final String listauthtaskclass = "SELECT a.bizid id,b.clname,b.pid,b.seqno FROM urc_auth_other_map a left join tk_task_class b on a.bizid = b.id where subjecttp=0 and subjectid=? and module=0 union SELECT a.bizid id,b.clname,b.pid,b.seqno FROM urc_auth_other_map a left join tk_task_class b on a.bizid = b.id where subjecttp=1 and subjectid in (SELECT roleid FROM pw_role_map where subjectid = ?) and module=0";
	private static final String SRC_CHILD = "select * from tk_task_class where pid = ?";
//	private static final String SELECT_CHILD = "select id from tk_task_class where status=1 and pid = ?";
//	private static final String SELECT_ID_BY_PID = "select id from tk_task_class where id = ? and status=1 ";
	private static final String FIND_BY_FLAG_AND_STATUS_ORDER_BY_PID_SEQNO = "SELECT * FROM tk_task_class WHERE flag=? and status=? ";
	
	@Resource private TkTaskClassDao tkTaskClassDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
    @Resource GeneralSqlComponent  generalSqlComponent;
    /**
	 * 所有可用状态的分类名称列表
	 * @return
	 */
    public List<String> listAllClsnames(Long compid){
		return tkTaskClassDao.listAllClsnames(compid);
	}
    
	/**
	 * 
	 * 查询任务分类所有数据
	 * 
	 * @author pantao
	 * @date 2018年10月9日 下午2:58:18
	 * 
	 * @param search
	 * @return
	 * @throws LehandException
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listByFlag(int flag, int status, String search) throws LehandException {
		// 返回一个List<Map<String, Object>>
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		// 返回一个TkTaskClass对象的集合
		List<TkTaskClass> rows = null;
		if (StringUtils.isEmpty(search)) {//模糊查询分开
			rows = generalSqlComponent.getDbComponent().listBean(FIND_BY_FLAG_AND_STATUS_ORDER_BY_PID_SEQNO + " order by pid asc,seqno desc",
					TkTaskClass.class,new Object[] { flag, status });
		} else {
			search = search.replaceAll("\'", "");
			rows = generalSqlComponent.getDbComponent().listBean(FIND_BY_FLAG_AND_STATUS_ORDER_BY_PID_SEQNO + " and clname like " + "'%" + search
					+ "%'" + " order by pid asc,seqno desc",TkTaskClass.class, new Object[] { flag, status });
		}
		// 对上面的rows集合进行数据分析处理
		Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(rows.size());
		// 对集合数据进行遍历处理
		for (TkTaskClass row : rows) {
			Long id = row.getId();
			Long pid = row.getPid();
			// 返回的数据里面共有的信息（id和name）
			Map<String, Object> line = new HashMap<String, Object>(3);
			line.put("id", id);
			line.put("label", row.getClname());
			line.put("pid", pid);
			line.put("isshow", false);
			line.put("isedit", false);
			//判断是否是根节点
			if (pid <= 0) {//是
				// 父节点的话再将下面的子集put进去
				line.put("children", new ArrayList<Map<String, Object>>());
				// 再将集合添加到结果集中
				result.add(line);
			} else {//不是
//				List<TkTaskClass> lists = null;
//				// 判断有没有子节点
//				if (StringUtils.isEmpty(search)) {
//					 lists = jdbc.list2Bean("select id from tk_task_class where id = ? and status=1" + " order by pid asc,seqno desc",new Object[] { pid }, TkTaskClass.class);
//				}else {
//					 lists = jdbc.list2Bean(SELECT_ID_BY_PID + " and clname like " + "'%" + search + "%'" + " order by pid asc,seqno desc",new Object[] { pid }, TkTaskClass.class);	
//				}
				if (/*lists.size() > 0 &&*/ map.get(pid)!=null) {//有
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					childrens.add(line);
				}else {//没有
					result.add(line);
				}
			}
			map.put(id, line);
		}
		return result;
	}

	/**
	 * 修改 任务分类
	 * 
	 * @author pantao
	 * @date 2018年10月9日 下午5:58:49
	 * 
	 * @param id
	 * @param name
	 */
	@Transactional(rollbackFor = Exception.class) // 事务回滚
	public int update(Long compid,Long id, String name) {
		return tkTaskClassDao.updateTkname(compid,id, name);
	}

	/**
	 * 任务分类 新增
	 * 
	 * @author pantao
	 * @date 2018年11月7日下午2:59:00
	 *
	 * @param flag
	 * @param pid
	 * @param name
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public int addNode(Long compid,int flag, long pid, String name) {
		Long num = DateEnum.now().getTime();
		TkTaskClass record = new TkTaskClass();
		record.setCompid(compid);
		record.setFlag(flag);
		record.setClname(name);
		record.setPid(pid);
		record.setSeqno(num);
		record.setStatus(1);
		record.setRemarks1(0);
		record.setRemarks2(Constant.EMPTY);
		record.setRemarks3(Constant.EMPTY);
		return tkTaskClassDao.insert(record);
	}

	/**
	 * 
	 * 任务分类删除接口实现类
	 * 
	 * @author pantao
	 * @date 2018年10月10日 上午8:46:06
	 * 
	 * @param oldClassid
	 * @return
	 * @throws LehandException
	 */
	@Transactional(rollbackFor = Exception.class)
	public int delete(Long oldClassid,Long newClassid,Long compid) throws LehandException {
		int num = 0;
/*//		// 查询语句下面两种选其一都行
//		Integer count = tkTaskDeployDao.listCount(id);
//		if (count > 0) {
//			LehandException.throwException("任务分类已被使用，不能删除！!");
//		} else {
//			List<TkTaskClass> classs = jdbc.list2Bean(SELECT_CHILD, new Object[] { id }, TkTaskClass.class);
//			if (classs.size() > 0) {
//				LehandException.throwException("该分类下存在子分类，请将其下子分类挂到其他分类下再进行删除操作！");
//			}
//			num = tkTaskClassDao.updateStatus(id);
//		}
*/		updateClassAndTask(compid,oldClassid, newClassid);
		num = tkTaskClassDao.updateStatus(compid,oldClassid);
		//删除授权表中的该分类
		generalSqlComponent.delete(SqlCode.delPAOMByClassid, new Object[] {compid,oldClassid});
		return num;
	}

	/**
	 * 任务分类拖拽
	 * 
	 * @author pantao
	 * @date 2018年11月7日上午10:37:25
	 *
	 * @param srcid 拖动组织
	 * @param tagid 拖动到哪个组织
	 * @param flag  1:上面,2:下面,3:里面d
	 */
	// 1.分别查询目标tag与主体src的父节点pid 注意：如果srcpid=0那么就得判断如果其子节点为空则允许拖动，不为空则不可以拖动；
	// 2.判断父节点是否相等（如果相等那么 flag 就不等于3）
	// 如果flag=1 那么 srcpid = tagpid 将tag下面的节点序号+2（不包括src本身）
	// 并且讲src的序号改为tag序号-1
	// 如果flag=2 那么srcpid = tagpid 将tag上面的节点序号-2（不包括src本身）
	// 并且将src的序号改为tag序号+1

	@Transactional(rollbackFor = Exception.class)
	public void move(Long srcid, Long tagid, int flag,Long compid) throws LehandException {
		TkTaskClass tag = tkTaskClassDao.get2Bean(compid,tagid);// 目标父节点
		TkTaskClass src = tkTaskClassDao.get2Bean(compid,srcid);// 主体父节点
		List<TkTaskClass> srcchild = generalSqlComponent.getDbComponent().listBean(SRC_CHILD,TkTaskClass.class,new Object[] { srcid });// 主体子节点
		 //两个子节点移动 存在子节点的父节点之间的移动 主体为带子节点的父节点目标为子节点
		if ((tag.getPid() != 0 && src.getPid() != 0 && flag == 3)
				|| (tag.getPid() == 0 && src.getPid() == 0 && flag == 3 && srcchild.size() > 0)
				|| (tag.getPid() != 0 && src.getPid() == 0 && srcchild.size() > 0)) {
			LehandException.throwException("任务分类不支持三级菜单");
		}
		Long tagPid = tag.getPid();
		Long num = null;
		num = DateEnum.now().getTime();
		if (flag == 3) {
			tkTaskClassDao.updateTkTaskClass1(compid,srcid, tagid, num);
		} else {
			if (flag == 1) {
				tkTaskClassDao.updateTkTaskClass2(compid,srcid, tag, tagPid);
				num = tag.getSeqno() + 1;
			} else {
				tkTaskClassDao.updateTkTaskClass3(compid,srcid, tag, tagPid);
				num = tag.getSeqno() - 1;
			}
			tkTaskClassDao.updateTkTaskClass1(compid,srcid, tagPid, num);
		}
		//拖拽完成后要查询下分类授权列表中的remarks3字段（为该分类的子节点）
		List<TkTaskClass> list = tkTaskClassDao.listAuth(compid);
		if(list!=null && list.size()>0) {
			for (TkTaskClass tkTaskClass : list) {
				generalSqlComponent.update(SqlCode.modPAOTMByRemarks3, new Object[] {compid,tkTaskClass.getPid(),tkTaskClass.getId()});
			}	
		}
	}
	
	/**
	 * 授权分类列表
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listByAuth2(Long subjectid ,String search) throws LehandException {
		// 返回一个List<Map<String, Object>>
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		// 返回一个TkTaskClass对象的集合
		List<TkTaskClass> rows = null;
		if  (StringUtils.isEmpty(search)){//查询要包括角色的授权
			rows = generalSqlComponent.getDbComponent().listBean(listauthtaskclass+" order by pid,seqno ", TkTaskClass.class,new Object[] { subjectid,subjectid });
		}else {
			search = search.replaceAll("\'", "");
			rows = generalSqlComponent.getDbComponent().listBean( "SELECT * FROM ("+listauthtaskclass+" ) c where clname like '"+'%'+search+'%'+"' order by pid,seqno ",TkTaskClass.class, new Object[] { subjectid,subjectid });
		}
		// 对上面的rows集合进行数据分析处理
		Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(rows.size());
		// 对集合数据进行遍历处理
		for (TkTaskClass row : rows) {
			Long id = row.getId();
			Long pid = row.getPid();
			// 返回的数据里面共有的信息（id和name）
			Map<String, Object> line = new HashMap<String, Object>(3);
			line.put("id", id);
			line.put("label", row.getClname());
			line.put("pid", pid);
			line.put("isshow", false);
			line.put("isedit", false);
			if (pid <= 0) {//根节点
				// 父节点的话再将下面的子集put进去
				line.put("children", new ArrayList<Map<String, Object>>());
				// 再将集合添加到结果集中
				result.add(line);
			} else {//是子节点
				if( map.get(pid)!=null) {
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					childrens.add(line);
				} else {
					result.add(line);
				}
			}
			map.put(id, line);
		}
		return result;
	}

	public int getNum(Long compid,Long classid) {
		return tkTaskDeployDao.listCount(compid,classid);
	}

	private void updateClassAndTask(Long compid,Long oldClassid, Long newClassid) {
		//更新任务分类子节点的PID
		if(!StringUtils.isEmpty(newClassid)) {
			List<TkTaskClass> list = tkTaskClassDao.findByPidList(compid,oldClassid);
			if(list!=null && list.size()>0) {
				String classid = "";
				for (TkTaskClass tkTaskClass : list) {
					classid += tkTaskClass.getId()+",";
				}
				classid = classid.substring(0,classid.length()-1);
				tkTaskClassDao.updatePid(compid,classid);
			}
		}
		int num = getNum(compid,oldClassid);
		if(num>0) {
			//将主任务的相关数据的classid更新为newClassid
			tkTaskDeployDao.updateByClassid(compid,oldClassid,newClassid);
		}
	}

/**========================================================以下是新增的接口2019-08-02==========================================================**/
	
	private static final String listauthdlclass = "SELECT a.bizid id,b.name,b.pid FROM urc_auth_other_map a left join dl_type b on a.bizid = b.id where subjecttp=0 and subjectid=? and module=0 union SELECT a.bizid id,b.name,b.pid FROM urc_auth_other_map a left join dl_type b on a.bizid = b.id where subjecttp=1 and subjectid in (SELECT roleid FROM pw_role_map where subjectid = ?) and module=0";

	/**
	 * 授权分类列表
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listByAuth(Long subjectid ,String search) throws LehandException {
		// 返回一个List<Map<String, Object>>
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		// 返回一个TkTaskClass对象的集合
		List<DlType> rows = null;
		if  (StringUtils.isEmpty(search)){//查询要包括角色的授权
			rows = generalSqlComponent.getDbComponent().listBean(listauthdlclass+" order by pid ", DlType.class,new Object[] { subjectid,subjectid });
		}else {
			search = search.replaceAll("\'", "");
			rows = generalSqlComponent.getDbComponent().listBean( "SELECT * FROM ("+listauthdlclass+" ) c where name like '"+'%'+search+'%'+"' order by pid", DlType.class,new Object[] { subjectid,subjectid });
		}
		// 对上面的rows集合进行数据分析处理
		Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(rows.size());
		// 对集合数据进行遍历处理
		for (DlType row : rows) {
			Long id = row.getId();
			Long pid = row.getPid();
			// 返回的数据里面共有的信息（id和name）
			Map<String, Object> line = new HashMap<String, Object>(3);
			line.put("id", id);
			line.put("label", row.getName());
			line.put("pid", pid);
			line.put("isshow", false);
			line.put("isedit", false);
			if (pid <= 0) {//根节点
				// 父节点的话再将下面的子集put进去
				line.put("children", new ArrayList<Map<String, Object>>());
				// 再将集合添加到结果集中
				result.add(line);
			} else {//是子节点
				if( map.get(pid)!=null) {
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					childrens.add(line);
				} else {
					result.add(line);
				}
			}
			map.put(id, line);
		}
		return result;
	}

}
