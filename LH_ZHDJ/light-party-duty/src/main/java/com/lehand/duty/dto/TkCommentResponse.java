package com.lehand.duty.dto;


import com.lehand.duty.pojo.TkCommentReply;

import java.util.List;

public class TkCommentResponse  {
	
	private Integer commflag;//评论类型(0:文本,1:语言)
	private String commtext;//评论内容(评论类型为0时，存储文本,评论类型为1时，存储附件UUID)
	private String commtime;//评论时间
	private Integer model;//模块(2:任务反馈明细)
	private Long modelid;//反馈详情表id
	private Long fsubjectid;//评论人id
	private String fsubjectname;//评论人
	private List<TkCommentReply> reply;//回复集合
	private boolean isopen = false;
	private Long id;
	private int replynum=0;//回复条数
	private boolean isplay = false;
	int duration;//语音评论时长
	
	
	
	
	public Long getFsubjectid() {
		return fsubjectid;
	}
	public void setFsubjectid(Long fsubjectid) {
		this.fsubjectid = fsubjectid;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public boolean isIsplay() {
		return isplay;
	}
	public void setIsplay(boolean isplay) {
		this.isplay = isplay;
	}
	public int getReplynum() {
		return replynum;
	}
	public void setReplynum(int replynum) {
		this.replynum = replynum;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isIsopen() {
		return isopen;
	}
	public void setIsopen(boolean isopen) {
		this.isopen = isopen;
	}
	public Integer getCommflag() {
		return commflag;
	}
	public void setCommflag(Integer commflag) {
		this.commflag = commflag;
	}
	public String getCommtext() {
		return commtext;
	}
	public void setCommtext(String commtext) {
		this.commtext = commtext;
	}
	public String getCommtime() {
		return commtime;
	}
	public void setCommtime(String commtime) {
		this.commtime = commtime;
	}
	public Integer getModel() {
		return model;
	}
	public void setModel(Integer model) {
		this.model = model;
	}
	public Long getModelid() {
		return modelid;
	}
	public void setModelid(Long modelid) {
		this.modelid = modelid;
	}
	public String getFsubjectname() {
		return fsubjectname;
	}
	public void setFsubjectname(String fsubjectname) {
		this.fsubjectname = fsubjectname;
	}
	public List<TkCommentReply> getReply() {
		return reply;
	}
	public void setReply(List<TkCommentReply> reply) {
		this.reply = reply;
	}
	
	
}
