package com.lehand.duty.service;

import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkDict;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TkDictService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	public List<TkDict> list(Long compid,String pcode){
		return generalSqlComponent.query(SqlCode.queryTkDictByPcode, new Object[] {compid,pcode});
	}

	public TkDict get(Long compid,String timecode) {
		return generalSqlComponent.query(SqlCode.queryTkDictByCode,  new Object[] {compid,timecode});
	}
}
