package com.lehand.duty.handle;

import com.lehand.base.exception.LehandException;
import com.lehand.base.util.SpringUtil;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.handle.child.*;

public enum TaskProcessEnum {
	
	/**
	 * 部署
	 */
	DEPLOY(DeployTaskHandler.class,"部署新任务", Module.DEPLOY_TASK, RemindRuleEnum.DEPLOY_TASK),

	/**
	 * 创建子任务
	 */
	CREATE(CreateTaskHandler.class,"批量处理自动创建子任务", Module.MY_TASK, RemindRuleEnum.NEW_TASK),
	
	/**
	 * 获得积分
	 */
	GET_INTEGER(GetIntegerHandler.class,"获得积分", Module.GET_INTEGER, RemindRuleEnum.GET_INTEGER),

	/**
	 * 签收
	 */
	SIGN_IN(SignInHandler.class,"签收", Module.MY_TASK, RemindRuleEnum.SIGN_TASK),

	/**
	 * 退回
	 */
	RETURN(ReturnHandler.class,"退回", Module.MY_TASK, RemindRuleEnum.RETURN_TASK),

	/**
	 * 新增反馈
	 */
	FEED_BACK(FeedBackHandler.class,"新增反馈", Module.MY_TASK_LOG, RemindRuleEnum.BACK_TASK),

	/**
	 * 修改反馈
	 */
	UPDATE_FEED_BACK(UpdateFeedBackHandler.class,"修改反馈", Module.MY_TASK_LOG, RemindRuleEnum.UPDATE_BACK_TASK),
	
	COMMENT(CommentHandler.class,"评论反馈", Module.COMMENT, RemindRuleEnum.COMMENT_BACK),
	
	REPLY(ReplyHandler.class,"回复评论", Module.REPLY, RemindRuleEnum.REPLY_COMMENT),
	
	OVER_BACK(OverBackHandler.class,"逾期未反馈", Module.MY_TASK, RemindRuleEnum.OVERDUE_TASK),
	
	BACK_END_BEFORE(BackEndBeforeHandler.class,"任务即将结束", Module.MY_TASK, RemindRuleEnum.BACK_END_BEFORE),
	
	BACK_NEXT_BEFORE(BackNextBeforeHandler.class,"即将到达反馈日", Module.MY_TASK, RemindRuleEnum.BACK_NEXT_BEFORE),
	
	DELETE_TASK(DeleteTaskHandler.class,"任务被删除", Module.DELETE_TASK, RemindRuleEnum.DELETE_TASK),
	
	FEED_PARISE(FeedPariseHandler.class,"反馈被点赞", Module.FEED_PARISE, RemindRuleEnum.FEED_PARISE),
	
	CUI_BAN(CuiBanHandler.class,"催办提醒", Module.MY_TASK, RemindRuleEnum.CUI_BAN),
	
	APPLY_SUSPEND_TASK(ApplySuspendTaskHandler.class,"任务被暂停", Module.SUSPEND_TASK, RemindRuleEnum.APPLY_SUSPEND_TASK),
	
	COMPLETE_TASK(CompleteTaskHandler.class,"任务确认完成", Module.COMPLETE_TASK, RemindRuleEnum.COMPLETE_TASK),
	
	RECOVERY_TASK(RecoveryTaskHandler.class,"恢复任务", Module.RECOVERY_TASK, RemindRuleEnum.RECOVERY_TASK) ;

	public TaskProcessData handle(Session session, Object... params) throws LehandException {
		BaseHandler hd = SpringUtil.getBean(this.handler);
		return hd.process(this, session, params);
	}

	protected Class<? extends BaseHandler> handler;
	protected String name;
	protected RemindRuleEnum remindRule;
	protected Module module;
	protected Class<?>[] classes;

	private TaskProcessEnum(Class<? extends BaseHandler> handler, String name, Module module,
                            RemindRuleEnum remindRule) {
		this.handler = handler;
		this.name = name;
		this.module = module;
		this.remindRule = remindRule;
	}

	public RemindRuleEnum getRemindRule() {
		return remindRule;
	}

	public Class<?>[] getParamsType() {
		return classes;
	}

	public String getName() {
		return name;
	}

	public Module getModule() {
		return module;
	}

	public Class<? extends BaseHandler> getHandler() {
		return handler;
	}

}
