package com.lehand.duty.dto;

import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.components.web.pipeline.Request;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

public class MyTaskLogRequest extends Request {

	/**
	 * 我的任务ID   
	 */
	private Long mytaskid;
	
	/**
	 * 反馈ID    修改反馈时使用
	 */
	private Long feedbackid;
	
	private String message;
	
	private double progress = 0.0;
	
	private String remindtime;

	private List<Map<String,Object>> relationtask;//关联任务json数据
	private Long audituserid; //审核人id

	
	/** 2019-12-15 添加*/
	//反馈人姓名
	private String feedbackPerson  ;
	//反馈人电话
	private String feedbackPhone ;
	
	public List<Map<String, Object>> getRelationtask() {
		return relationtask;
	}

	public void setRelationtask(List<Map<String, Object>> relationtask) {
		this.relationtask = relationtask;
	}

	public Long getAudituserid() {
		return audituserid;
	}

	public void setAudituserid(Long audituserid) {
		this.audituserid = audituserid;
	}

	/**
	 * 附件的UUID
	 */
	private String uuid=StringConstant.EMPTY;
	
	private String SynchronizationTime;
	
	public Long getMytaskid() {
		return mytaskid;
	}


	public void setMytaskid(Long mytaskid) {
		this.mytaskid = mytaskid;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	public String getUuid() {
		return uuid==null ? "" : uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public double getProgress() {
		return progress;
	}


	public void setProgress(double progress) {
		this.progress = progress;
	}

	public Long getFeedbackid() {
		return feedbackid;
	}


	public void setFeedbackid(Long feedbackid) {
		this.feedbackid = feedbackid;
	}


	public String getRemindtime() {
		return remindtime;
	}


	public void setRemindtime(String remindtime) {
		this.remindtime = remindtime;
	}


	@Override
	public void validate() throws LehandException {
		
	}

	public String getSynchronizationTime() {
		return SynchronizationTime;
	}

	public void setSynchronizationTime(String synchronizationTime) {
		SynchronizationTime = synchronizationTime;
	}

	public void validateFeedBack() throws LehandException {
		if (null==mytaskid) {
			LehandException.throwException("我的任务ID不能为空!");
		}
		if (!StringUtils.hasLength(this.message)) {
			LehandException.throwException("进展说明不能为空!");
		}
	}
	
	public void validateUpdateFeedBack() throws LehandException {
		if (null==feedbackid) {
			LehandException.throwException("反馈ID不能为空!");
		}
		validateFeedBack();
	}

	public String getFeedbackPerson() {
		return feedbackPerson;
	}

	public void setFeedbackPerson(String feedbackPerson) {
		this.feedbackPerson = feedbackPerson;
	}

	public String getFeedbackPhone() {
		return feedbackPhone;
	}

	public void setFeedbackPhone(String feedbackPhone) {
		this.feedbackPhone = feedbackPhone;
	}
    /**
     * 附件的filds
     */
    private String files;

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }
}
