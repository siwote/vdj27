package com.lehand.duty.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelAnalysisException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.util.EasyExcelUtil;
import com.lehand.base.util.SpringUtil;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.DateStageEnum;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.RemindRuleEnum;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.common.modulemethod.FilesPreviewAndDownload;
import com.lehand.duty.component.RemindNoteComponent;
import com.lehand.duty.dao.TkMyTaskDao;
import com.lehand.duty.dao.TkTaskClassDao;
import com.lehand.duty.dao.TkTaskDeployDao;
import com.lehand.duty.dao.TkTaskDeploySubDao;
import com.lehand.duty.dto.Subject;
import com.lehand.duty.dto.SubjectReponse;
import com.lehand.duty.dto.TaskRemindTimes;
import com.lehand.duty.dto.TimeResponse;
import com.lehand.duty.dto.TkTaskDeployRequest;
import com.lehand.duty.dto.TkTaskFormRespond;
import com.lehand.duty.dto.TkTaskRequest;
import com.lehand.duty.dto.TkTaskResolveInfoRespondDto;
import com.lehand.duty.dto.TkTaskRespond;
import com.lehand.duty.dto.urc.UrcUser;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.BrowerHis;
import com.lehand.duty.pojo.DlFileBusiness;
import com.lehand.duty.pojo.PwAuthOtherMap;
import com.lehand.duty.pojo.TkAttachment;
import com.lehand.duty.pojo.TkDict;
import com.lehand.duty.pojo.TkImportDeploy;
import com.lehand.duty.pojo.TkImportDeployList;
import com.lehand.duty.pojo.TkMyTask;
import com.lehand.duty.pojo.TkMyTaskLog;
import com.lehand.duty.pojo.TkResolveInfo;
import com.lehand.duty.pojo.TkTaskClass;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import com.lehand.duty.pojo.TkTaskFeedBackAudit;
import com.lehand.duty.pojo.TkTaskFeedTime;
import com.lehand.duty.pojo.TkTaskRemindList;
import com.lehand.duty.pojo.TkTaskSubject;
import com.lehand.duty.pojo.TkTaskTimeNode;
import com.lehand.duty.utils.DateUtil;
import com.lehand.duty.utils.POIExcelUtil;
import com.lehand.meeting.dto.SmsSendDto;
import com.lehand.meeting.service.jobservice.SendMessageService;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.lehand.duty.common.Constant.TkTaskDeploy.NO_SIGNED_STATUS;
import static com.lehand.duty.common.Constant.TkTaskDeploy.PERIOD_NO;
import static com.lehand.duty.common.Constant.TkTaskDeploy.RESOLVE_NO;


/**
 * 任务部署实现类
 * 
 * @author Zim
 *
 */
@Service
public class TkTaskDeployService {

	private static final Logger logger = LogManager.getLogger(TkTaskDeployService.class);

    @Resource
    private FilesPreviewAndDownload filesPreviewAndDownload;

    @Resource protected GeneralSqlComponent generalSqlComponent;
	
	@Resource private RemindNoteComponent remindNoteComponent;
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkTaskTimeNodeService tkTaskTimeNodeService;
	@Resource private TkTaskSubjectService tkTaskSubjectService;
	@Resource private TkAttachmentService tkAttachmentService;
	@Resource private BrowerHisService browerHisService;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	@Resource private TkIntegralDetailsService tkIntegralDetailsService;
	@Resource private TkTaskClassDao tkTaskClassDao;
    @Resource private TkTaskDeploySubDao tkTaskDeploySubDao;
    @Resource private TkMyTaskLogService tkMyTaskLogService;
    @Resource private TkCommentService tkCommentService;
    @Resource private TkCommentReplyService tkCommentReplyService;
    @Resource private TkImportDeployService tkImportDeployService;
    @Resource private TkImportDeployListService tkImportDeployListService;
    @Resource private TkResolveInfoService tkResolveInfoService;
    @Resource private TkDictService tkDictService;
    @Resource private TkTaskFeedBackAuditService tkTaskFeedBackAuditService;
    @Resource private SendMessageService sendMessageService;
    @Value("${user.user-id}") private String rootUserId;

    public String getRootUserId() {
        return rootUserId;
    }

    public void setRootUserId(String rootUserId) {
        this.rootUserId = rootUserId;
    }

    public Map<String,String> feedCount(Session session, Long taskid) {
        Assert.notNull(taskid,"taskId cant be null");
		Long compid = session.getCompid();
		Map<String, String> resultMap = new HashedMap<>();
		List<SubjectReponse> subjects = generalSqlComponent.query(SqlCode.listSubject, new Object[]{compid, taskid, Constant.TkTaskSubject.FLAG_EXCUTE});
		List<String> times = generalSqlComponent.query(SqlCode.listFeedtime, new Object[]{compid,taskid});
		int shouldCount = subjects.size() * times.size(); //应报
		Long actulCount = generalSqlComponent.query(SqlCode.getallFeedCount, new Object[]{compid, taskid});//实报
		int overCount = 0;
		int overNotCount = 0;
		int commonCount = 0;
		for (SubjectReponse subject : subjects) {
			//1;//正常反馈2;//逾期反馈3;//逾期未反馈4;//正常未反馈
			List<TaskRemindTimes> remindTimes = tkTaskFeedBackAuditService.listRemindTimes(compid, taskid, subject.getSubjectid());
			ArrayList<String> feedStatus = new ArrayList<>();
			for (TaskRemindTimes remindTime : remindTimes) {
				feedStatus.add(String.valueOf(remindTime.getFeedstatus()));
			}

			if(feedStatus.contains("3")){
				overNotCount ++;
			}else if(feedStatus.contains("2")){
				overCount ++;
			}else if(feedStatus.contains("1")){
				commonCount ++;
			}
		}
		resultMap.put("shouldCount", String.valueOf(shouldCount));  //应报
		resultMap.put("actulCount", String.valueOf(actulCount));   //实报
		resultMap.put("diffent", String.valueOf(shouldCount-actulCount < 0 ? 0 :shouldCount - actulCount ));  //差额
		resultMap.put("overCount", String.valueOf(overCount));    //逾期
		resultMap.put("overNotCount", String.valueOf(overNotCount)); //逾期未报
		resultMap.put("commonCount", String.valueOf(commonCount));  //正常

		return resultMap;
    }

    @SuppressWarnings({ "unchecked" })
    public Pager allPage(Session session, Long taskid,Integer status, Pager pager) {
        Assert.notNull(taskid,"taskId cant be null");
		int pageSize = pager.getPageSize();
		int pageNo = pager.getPageNo();

		pager.setPageSize(5000);//先查询所有的执行人
		pager.setPageNo(1);
		Long compid = session.getCompid();
		List<String> times = generalSqlComponent.query(SqlCode.listFeedtime, new Object[]{compid,taskid});
		//获取所有执行人，包含已删除的
		generalSqlComponent.pageQuery(SqlCode.getSubjectByTaskId
				, new Object[]{compid, taskid, Constant.TkTaskSubject.FLAG_EXCUTE}, pager);
		List<SubjectReponse> executors = (List<SubjectReponse>) pager.getRows();

        HashMap<String, Object> params = new HashMap<>(3);
        params.put("compid",compid);
        params.put("taskid",taskid);
        params.put("sta","1,2");
        List<Map<String,Long>> actualList = generalSqlComponent.query(SqlCode.listFeedCountByUserid,params);
        params.put("sta","2");//逾期上报
        List<Map<String,Long>> overList = generalSqlComponent.query(SqlCode.listFeedCountByUserid,params);
        List<Map<String,Long>> overNotList = generalSqlComponent.query(SqlCode.listOverCountByUserid
				,new Object[]{compid,compid,taskid,taskid,DateEnum.YYYYMMDDHHMMDD.format()});  //逾期未报

        Map<String, Long> actualMap =  listToMap(actualList);
        Map<String, Long> overMap = listToMap(overList);
        Map<String, Long> overNotMap = listToMap(overNotList);

		List<SubjectReponse> resultList = new ArrayList<>(executors.size());

		//应当上报数 实际上报数 逾期上报数 逾期未报数  各个时间点上报状态
		for (SubjectReponse executor : executors) {
            Long subjectid = executor.getSubjectid();
			String userid = String.valueOf(subjectid);
			executor.setFeedCount((long) times.size());  //应当上报数
			executor.setActualCount(actualMap.get(userid)); //实际上报
            executor.setOverCount(overMap.get(userid)); //逾期上报
            executor.setOverNotCount(overNotMap.get(userid));//逾期未报
			executor.setStatus("1");
			if(executor.getRemarks2()==-1){
			    executor.setStatus("0");//前端0为删除
            }
			setRemindStatus(session, taskid, times, executor,status,resultList);//设置上报时间点列表
		}
		int size = resultList.size();
		pager.setTotalRows(size);

		int start = (pageNo - 1) * pageSize+1;
		int end = start + pageSize;
		if(start > size){
			resultList = new ArrayList<>(0);
		}else if (end > size){
			end = size;
			resultList = resultList.subList(start-1,end);
		}else{
			resultList = resultList.subList(start-1,end-1);
		}
		pager.setPageSize(pageSize);
		pager.setPageNo(pageNo);
		pager.setRows(resultList);

		return pager;
    }


    private Map<String, Long> listToMap(List<Map<String, Long>> srcList) {
        HashMap<String, Long> resultMap = new HashMap<>();
        for (Map<String, Long> map : srcList) {
            resultMap.put(String.valueOf(map.get("userid")),map.get("c"));
        }
        return resultMap;
    }

    private void setRemindStatus(Session session, Long taskid, List<String> times, SubjectReponse executor, Integer status, List<SubjectReponse> resultList) {
		List<TaskRemindTimes> remindTimes = tkTaskFeedBackAuditService.listRemindTimes(session.getCompid(), taskid, executor.getSubjectid());
		HashMap<String, Integer> timeAndCountMap = new HashMap<>(remindTimes.size());
		ArrayList<String> feedStatus = new ArrayList<>(remindTimes.size());
		for (TaskRemindTimes remindTime : remindTimes) {
			timeAndCountMap.put(remindTime.getRemindtime(),remindTime.getFeedstatus());
		}
		for (String time : times) {
			Integer st = timeAndCountMap.get(time);
			if(st==null){
				continue;
			}
			feedStatus.add(String.valueOf(st));
		}
		executor.setRemindColor(feedStatus);

		String taskStatus = "";
		if(feedStatus.contains("3")){
			taskStatus = "3";
		}else if (feedStatus.contains("2")){
			taskStatus = "2";
		}else if(feedStatus.contains("1")){
			taskStatus = "1";
		}

		if(status==null || com.alibaba.druid.util.StringUtils.equals(taskStatus,String.valueOf(status))){
			resultList.add(executor);
		}
	}

	@SuppressWarnings("unchecked")
	public Pager listDraftTask(TkTaskDeployRequest request, Session session, Pager pager) throws Exception {
		String tkname = request.getTkname();
		String starttime = request.getDraftStartTime();
		String endtime = request.getDraftEndTime();
		if (endtime.length()<=10) {
			endtime = endtime + "_23_59_59";
		}
		Long userid = session.getUserid();
		Map<String,Object> param = new HashMap<String, Object>(1);
		param.put("tkname", tkname);
		param.put("starttime", starttime);
		param.put("endtime", endtime);
		param.put("userid",userid);
		generalSqlComponent.pageQuery(SqlCode.listDraftTask, param, pager);
		List<Map<String,Object>> tasks = (List<Map<String,Object>>) pager.getRows();
		for (Map<String,Object> task : tasks) {
			//设置执行对象
			task.put("subjectName",tkTaskFeedBackAuditService.judgmentExecutor(session.getCompid(), (Long) task.get("id")));
		}
		return pager;
	}

	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
    public Long saveTaskDraft(TkTaskDeployRequest request, Session session) throws Exception {
        logger.info("保存成草稿...");
        // 首先判断开始时间是否小于当前时间
        if (request.getStarttime().compareTo(DateEnum.YYYYMMDDHHMMDD.format()) < 0) {
            LehandException.throwException("任务开始时间不能小于当前时间");
        }
        // 部署任务前先验证分类的class id在分类表里是否存在
        TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
        if (taskClass == null) {
            LehandException.throwException("任务分类不存在或无效");
        }
        // 新增或修改部署信息
        request.setSession(session);
		TkTaskDeploy info = toTkDeploy(request, session, PERIOD_NO, RESOLVE_NO,3);
		Long taskid = request.getTaskid();
		info.setAnnex(request.getAnnex());
		info.setPublisher(request.getPublisher());
		info.setTphone(request.getTphone());
		info.setOrgcode(session.getCurrentOrg().getOrgcode());
		if(taskid!=null){
			info.setId(taskid);
			if (request.getSubjects3()==null) {
				info.setStatus(NO_SIGNED_STATUS);
			}else {
				info.setStatus(3);
			}
			info.setRemarks1(Constant.TkTaskDeploy.REMARKS1_YES);
			//第一步：更新主任务表
			tkTaskDeployDao.updateByBean(info);
			/**新增加审核数据代码开始（主体表类别为3 审核人）**/
			newAddAudit(request, session, info, taskid);
			/**新增加审核数据代码结束（主体表类别为3 审核人）**/
		}else{// 新增提醒时间点，并返回时间点json
			List<Map> requireMents = getRequireMentMap(request, info.getTkname());
			info.setRemarks6(JSON.toJSON(requireMents).toString());
			long id = tkTaskDeployDao.insertRetrunID(info);
			info.setId(id);
		}
		// 执行人 跨级需要添加相关人
		insertTkTaskSubjectForDraft(session,info, request);

		logger.debug("普通任务ID:{}", info.getId());
        // 附件绑定
		saveOrUpdateFileBusiness(request,session,info);

        return info.getId();
    }

	/**
	 * Description: 增加审核流程信息
	 * @author PT  
	 * @date 2019年8月27日 
	 * @param request
	 * @param session
	 * @param info
	 * @param taskid
	 * @throws ParseException
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	private void newAddAudit(TkTaskDeployRequest request, Session session, TkTaskDeploy info, Long taskid)
			throws ParseException, Exception {
		Long compid= session.getCompid();
        //将审核人按顺序添加到数据库中方便上一个审核人审核完成后找下一个审核人
        List<Subject> audits = request.getSubjects3();
        if(audits!=null&&audits.size()>0){
            generalSqlComponent.getDbComponent().delete("delete from tk_task_audit_order where compid=? and taskid=?",
                    new Object[]{session.getCompid(),taskid});
            for (int i = 0; i < audits.size(); i++) {
                generalSqlComponent.getDbComponent().insert("insert into tk_task_audit_order (compid,taskid,auditid," +
                                "ordernum ) values (?,?,?,?)",
                        new Object[]{session.getCompid(),taskid,audits.get(i).getSubjectid(),i});
            }
        }
		List<Map> requireMents1 = getRequireMentMap(request, request.getTkname());
		if (request.getSubjects3()==null) {// 无需审核 
			continueDeployTask(request, session, requireMents1, info);
		} else {// 需要审核（往审核表中插入数据）
            /** 查询角色关系表 remarks2>0 的   1表示正职  2表示副职*/
            Long subjectid = request.getSubjects3().get(0).getSubjectid();
            int remarks2 = getAuditType(compid, subjectid);
            TkTaskFeedBackAudit tkTaskFeedBackAudit = insertTkTaskFeedBackAudit(compid, taskid,null, null, subjectid, 2,remarks2);
            Long auditid = tkTaskFeedBackAudit.getId();
            UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid, new Object[] { subjectid });
            sendMsg(session, Module.TASK_AUDIT_NOPASS.getValue(), auditid, "TASK_AUDIT",
                    "任务审核", "有一条新的任务，请审核。", subjectid, user.getUsername(),
                    String.valueOf(tkTaskFeedBackAudit.getTaskid()), generalSqlComponent);

            //创建短信消息
            insertSms("有一条下发任务待您审核，任务名称："+request.getTkname(), getPhones(compid, subjectid));

            //创建待办信息 TODO
            insertMyToDoList(0, info.getId().toString(),"《"+info.getTkname()+"》"+"待您审核。", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), subjectid.toString());

            TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
            addTodoItem(Todo.ItemType.TYPE_ISSUE_TASK, info.getTkname() + "(" + taskClass.getClname() + ")", String.valueOf(auditid),  Todo.FunctionURL.TASK_DEPLOY_AUDIT.getPath(), user.getUserid(), session);

		}
	}

	/**
	 * Description: 创建待办消息
	 * @author PT  
	 * @date 2019年9月10日 
	 * @param module
	 * @param content
	 * @param remind
	 * @param optuserid
	 * @param userid
	 */
	public void insertMyToDoList(int module,String busid,String content,String remind,String optuserid,String userid) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("module", module);
		paramMap.put("busid", busid);
		paramMap.put("content", content);
		paramMap.put("remind", remind);
		paramMap.put("optuserid", optuserid);
		paramMap.put("userid", userid);
		paramMap.put("isread", 0);
		paramMap.put("ishandle", 0);
		generalSqlComponent.getDbComponent().insert("INSERT INTO my_to_do_list (module, busid, content, remind, optuserid, userid, isread, ishandle) VALUES (:module, :busid, :content, :remind, :optuserid, :userid, :isread, :ishandle)", paramMap);
	}

	@SuppressWarnings("rawtypes")
	public List<Map> getRequireMentMap(TkTaskDeployRequest request, String tkname) throws ParseException {
		List<Map> requireMents = JSON.parseArray(request.getTabTimedata(), Map.class);//存储发消息的时间
		if (CollectionUtils.isEmpty(requireMents)) {
			requireMents = setDefaultTimeList(requireMents, DateEnum.YYYYMMDDHHMMDD.parse(request.getStarttime())
					, DateEnum.YYYYMMDDHHMMDD.parse(request.getEndtime()), tkname);
		}
		return requireMents;
	}

	/**
     * @Description 部署草稿任务
     * @Author zouchuang
     * @Date 2019/4/11 10:00
     * @Param [request, session]
     * @return java.lang.Long
     **/
    @SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
    public Long deployDraftTask(TkTaskDeployRequest request, Session session) throws Exception {
        Long taskid = request.getTaskid();
        TkTaskDeploy info = getTaskByTaskid(session.getCompid(), taskid);
		//删除子表任务 退回保留的子表脏数据
		tkMyTaskDao.delete(session.getCompid(),taskid);
        // 时间节点表
        insertTkTaskTimeNode(session.getCompid(),info.getId(), info.getStarttime(), info.getEndtime(),0);
        // 提醒
		String remarks6 = info.getRemarks6();
		List<Map> requireMents = JSON.parseArray(remarks6, Map.class);//存储发消息的时间
		insertTaskFeedTime(requireMents,session,info);
		Set<String> times = getReimdTimes(info);
        TaskProcessEnum.DEPLOY.handle(session, request, info, times);
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public void submitUploadTask(Long importId,Session session) throws Exception {
    	if(importId==null) {
    		LehandException.throwException("请先添加文件！");
    	}
    	List<TkImportDeployList> list = tkImportDeployListService.listByImpid(importId);
    	for (TkImportDeployList info : list) {
    		String tktpnm = info.getTktpnm();
    		Set<Subject> subjects = getUsers(info);
			List<Map<String, String>> diffdata = getDiffdata(info, tktpnm);
			TkTaskDeployRequest req = getTaskDeployRequest(info, subjects, diffdata);
			if(POIExcelUtil.TASK_CLASSES[0].equals(tktpnm)) {
				deployCommonTask(req , session);
			} else {
				deployPeriodTask(req, session);
			}
		}
    	tkImportDeployService.updateRemarks1(session.getCompid(),DateEnum.YYYYMMDDHHMMDD.format(), 1, importId);
    }

	private TkTaskDeployRequest getTaskDeployRequest(TkImportDeployList info, Set<Subject> subjects,
			List<Map<String, String>> diffdata) {
		TkTaskDeployRequest req = new TkTaskDeployRequest();
		req.setClassid(info.getClassid());
		req.setTkname(info.getTkname());
		req.setStarttime(info.getStarttime());
		req.setEndtime(info.getEndtime());
		req.setSubjects(subjects);
		req.setTimedata(JSON.toJSONString(diffdata));//普通周期任务用
		req.setTkdesc(Constant.EMPTY);
		req.setSrctaskid(-1L);
		req.setUuid(Constant.EMPTY);
		
		return req;
	}

	private List<Map<String, String>> getDiffdata(TkImportDeployList info, String tktpnm) {
		//时间信息
		List<Map<String, String>> diffdata = new ArrayList<Map<String, String>>(1);
		Map<String, String> map = new HashMap<String, String>();
		if (POIExcelUtil.TASK_CLASSES[0].equals(tktpnm)) {
			map.put("flag", "1");
			map.put("data", info.getEndtime().substring(0, 10));
			map.put("end", "");
		} else if (POIExcelUtil.TASK_CLASSES[1].equals(tktpnm)) {
			map.put("flag", "2");
			map.put("data", "1,Y,1");
			map.put("end", "365");
		} else if (POIExcelUtil.TASK_CLASSES[2].equals(tktpnm)) {
			map.put("flag", "2");
			map.put("data", "1,Q,1");
			map.put("end", "90");
		} else if (POIExcelUtil.TASK_CLASSES[3].equals(tktpnm)) {
			map.put("flag", "2");
			map.put("data", "1,M,1");
			map.put("end", "28");
		} else if (POIExcelUtil.TASK_CLASSES[4].equals(tktpnm)) {
			map.put("flag", "2");
			map.put("data", "1,W,1");
			map.put("end", "7");
		}else if (POIExcelUtil.TASK_CLASSES[5].equals(tktpnm)) {
			map.put("flag", "2");
			map.put("data", "1,D,1");
			map.put("end", "0");
		}
		diffdata.add(map);
		return diffdata;
	}

	private Set<Subject> getUsers(TkImportDeployList info) {
		String[] users = info.getUsers().split(",");
		Set<Subject> subjects = new HashSet<Subject>();
		for (String user : users) {
			String[] us = user.split("-");
			Subject subject = new Subject();
			subject.setSubjectid(Long.parseLong(us[0].trim()));
			subject.setSubjectname(us[1].trim());
			subjects.add(subject);
		}
		return subjects;
	}
    
    public void pageUploadTask(Long importId,Pager pager) {
    	tkImportDeployListService.pageByImpid(importId,pager);
    }
    
    @Transactional(rollbackFor=Exception.class)
	public Long uploadExcelTemplate(File file,Session session) throws Exception {
    	FileInputStream in = null;
    	try {
    		String name = file.getName();
    		String tempName = name.toLowerCase();
    		in = new FileInputStream(file);
    		Long importID = insertTkImportDeploy(session);
    		TkTaskDeployListener listener = new TkTaskDeployListener(session,importID);
    		if (tempName.endsWith("xls")) {
    			EasyExcelUtil.readXLS(in, listener);
    		} else if (tempName.endsWith("xlsx")) {
    			EasyExcelUtil.readXLSX(in, listener);
    		}
    		if (listener.getLine()<=0) {
				LehandException.throwException("上传失败，文件内容不可为空");
			}
    		return importID;
		} catch (ExcelAnalysisException e) {
			LehandException.throwException(e.getCause().getMessage());
		} finally {
			if (null!=in) {
				in.close();
			}
		}
    	return -1L;
	}

	public long insertTkImportDeploy(Session session) {
		TkImportDeploy info = new TkImportDeploy();
		info.setCompid(session.getCompid());
		info.setUserid(session.getUserid());
		info.setUsername(session.getUsername());
		info.setSumcount(0);
		info.setErrcount(0);
		info.setUlcount(0);
		info.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		info.setUpdatetime(Constant.EMPTY);
		info.setRemarks1(0);
		info.setRemarks2(0);
		info.setRemarks3(Constant.EMPTY);
		info.setRemarks4(Constant.EMPTY);
		info.setRemarks5(Constant.EMPTY);
		info.setRemarks6(Constant.EMPTY);
		return tkImportDeployService.insert(info);
	}

	/**
	 * 部署，普通任务，跨级任务
	 */
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
	public Long deployCommonTask2(TkTaskDeployRequest request, Session session) throws Exception {
		logger.info("发布普通任务...");
		// 首先判断开始时间是否小于当前时间
		if (request.getStarttime().compareTo(DateEnum.YYYYMMDDHHMMDD.format()) < 0 ) {
			LehandException.throwException("任务开始时间不能小于当前时间");
		}
		// 部署任务前先验证分类的class id在分类表里是否存在
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		List<Map> requireMents = getRequireMentMap(request, request.getTkname());

		String feedTimeJson = JSON.toJSON(requireMents).toString();
        if(feedTimeJson.length()>4094){
            throw new LehandException("反馈节点数据超过4096，保存失败！");
        }

		TkTaskDeploy info = toTkDeploy(request, session, PERIOD_NO, RESOLVE_NO);
		info.setRemarks6(feedTimeJson);
		long id = tkTaskDeployDao.insertRetrunID(info);
		info.setId(id);
		//保存完json之后，保存提醒时间点
        insertTaskFeedTime(requireMents, session, info);

		logger.debug("普通任务ID:{}", info.getId());
		// 附件绑定
		saveOrUpdateFileBusiness(request, session, info);

		insertTkTaskTimeNode(session.getCompid(),info.getId(), info.getStarttime(), info.getEndtime(),0);
		// 执行人 跨级需要添加相关人
		insertTkTaskSubject(session,info, request);
		// 提醒
		Set<String> times = insertTkTaskRemindNote(info, request, session);
		TaskProcessEnum.DEPLOY.handle(session, request, info, times);
		return info.getId();
	}

	
	
	
	/**
	   * 添加下一级审核人  remarks2 0:表示反馈审核，1：表示任务退回审核，2：表示部署任务审核
	 * @param compid
	 * @param taskid
	 * @param mytaskid
	 * @param feedbackid
	 * @param auditid
	 */
	public TkTaskFeedBackAudit insertTkTaskFeedBackAudit(Long compid, Long taskid, Long mytaskid, Long feedbackid,
			Long auditid, int tasktype,int remarks3) {
		TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
		audit.setCompid(compid.intValue());
		audit.setTaskid(taskid);
		audit.setMytaskid(mytaskid);
		audit.setFeedbackid(feedbackid);
		audit.setSubjectId(auditid);
		// 这里暂时定为0，后续根据需要再修改
		audit.setSubtype(Constant.status);
		audit.setStatus(Constant.status);
		audit.setRemark(StringConstant.EMPTY);
		audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		audit.setOpttime(StringConstant.EMPTY);
		audit.setRemarks2(tasktype);
		audit.setRemarks3(String.valueOf(remarks3));//审核人职位类型1正职 2 副职
		audit.setRemarks4(StringConstant.EMPTY);
		audit.setRemarks5(StringConstant.EMPTY);
		audit.setRemarks6(StringConstant.EMPTY);
		Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
		audit.setId(id);
		return audit;
	}

	public void saveOrUpdateFileBusiness(TkTaskDeployRequest request, Session session, TkTaskDeploy info) {
		String uuid = request.getUuid();
		if (!StringUtils.isEmpty(uuid)) {
			List<String> fileIds = generalSqlComponent.query(SqlCode.listFileBussinessBytaskid, new Object[]{session.getCompid(), info.getId(), 2});
			logger.debug("绑定附件,UUID:{},任务ID:{}", uuid, info.getId());
            tkAttachmentService.updateId(session.getCompid(),info.getId(), Constant.TkAttachments.MODEL_DEPLOY, uuid);
			//资料库附件绑定
			List<TkAttachment> ta = generalSqlComponent.query(SqlCode.listAttachments, new Object[] {uuid});
			for (TkAttachment tkAttachment : ta) {
				if(!CollectionUtils.isEmpty(fileIds)&&fileIds.contains(tkAttachment.getRemarks4())){
					continue;
				}
				DlFileBusiness dfb = new DlFileBusiness();
				dfb.setBusinessid(info.getId());
				dfb.setBusinessname((short)2);
				dfb.setCompid(session.getCompid());
				dfb.setFileid(Long.valueOf(tkAttachment.getRemarks4()));
				dfb.setIntostatus((short)0);
				generalSqlComponent.insert(SqlCode.addDlFileBusiness, dfb);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public void insertTaskFeedTime(List<Map> requireMents, Session session, TkTaskDeploy info) throws ParseException {
		
		//[{"backtime":"2019-01-13 12:12:12","mesg":"反馈时间点的要求","remindtime":"2019-01-12 12:12:12,2019-01-14 12:12:12"},{},{}....]
		if(requireMents!=null && requireMents.size()>0){
			for (Map requireMent : requireMents) {
				String feedtime = (String) requireMent.get("feedTime");//反馈时间点
				String requirement = (String) requireMent.get("require");//反馈要求
				String remindtimes = (String) requireMent.get("remindTimes");//反馈时间点对应的提醒时间
				if(StringUtils.isEmpty(remindtimes)){
					continue;
				}
				String[] strs = remindtimes.split(",");
				if(strs.length > 0) {//不管是多个提醒时间还是一个时间都遍历下这里就不做判断了
					for (String str : strs) {
						insertTkTaskFeedTime(session, info.getId(), feedtime, requirement, str);
					}
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private List<Map> setDefaultTimeList(List<Map> resultTimes, Date start, Date curDate, String tkname){
		HashMap<String, Object> feedTime = new HashMap<>();
		String remind1 = DateEnum.YYYYMMDD.format(DateEnum.addDay(start, 1)) + " 09:00:00";
		if(DateEnum.addDay(start,3).before(curDate)){
			String remind2 = DateEnum.YYYYMMDD.format(DateEnum.addDay(curDate, -1)) + " 09:00:00";
			remind1+=","+remind2;
		}

		feedTime.put("feedTime",DateEnum.YYYYMMDDHHMMDD.format(curDate));
		feedTime.put("require",tkname);
		feedTime.put("remindTimes",remind1);
		resultTimes.add(feedTime);
		return resultTimes;
	}

	/**往提醒时间表中插入数据*/
	private void insertTkTaskFeedTime(Session session, Long taskid, String feedtime, String requirement,String str) throws ParseException {
        long now = DateEnum.now().getTime();
        long remind = DateEnum.YYYYMMDDHHMMDD.parse(str).getTime();
		TkTaskFeedTime ttft = new TkTaskFeedTime();
		ttft.setCompid(session.getCompid());
		ttft.setTaskid(taskid);
		ttft.setFeedtime(feedtime);
		ttft.setRemindtimes(str);
		ttft.setRequirement(requirement);
		ttft.setRemarks2(Constant.status);
		if(remind<now){
			ttft.setRemarks2(2);
		}
		ttft.setRemarks3(StringConstant.EMPTY);
		ttft.setRemarks4(StringConstant.EMPTY);
		ttft.setRemarks5(StringConstant.EMPTY);
		ttft.setRemarks6(StringConstant.EMPTY);
		Long id =generalSqlComponent.insert(SqlCode.addTTFT,ttft);
		ttft.setId(id);
	}

	/**
	 * 新增或修改部署信息，request包含taskid为修改
	 *
	 * @param request
	 * @param session
	 * @param period
	 * @param resolve
	 * @return
	 * @throws ParseException
	 */
	protected TkTaskDeploy insertTaskDeployInfo(TkTaskDeployRequest request, Session session, int period, int resolve) throws ParseException {
		TkTaskDeploy info = toTkDeploy(request, session, period, resolve);
		tkTaskDeployDao.insertRetrunID(info);
		return info;
	}


	/**
	 * 部署，或者修改，普通任务，跨级任务
	 */
	@Transactional(rollbackFor = Exception.class)
	public Long updateCommonTask(TkTaskDeployRequest request, Session session) throws Exception {
		// 验证修改的class id是否存在
		Long compid = session.getCompid();
		TkTaskClass taskClass = tkTaskClassDao.get(compid,request.getClassid(),1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		Long taskid = request.getTaskid();
		List<Subject> oldsubs = tkTaskSubjectService.listSubjects(compid,taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		Set<Subject> setSubject = new HashSet<Subject>(oldsubs);
		TkTaskDeploy info = tkTaskDeployDao.get(compid,taskid);
		if(info.getStatus()!=3 && info.getStatus()!=1 && info.getStatus()!=5) {
			LehandException.throwException("任务状态已变更，请返回");
		}
		String tkname = request.getTkname();
		String endtime = request.getEndtime();
		//封装任务变更消息 , 如果结束时间发生后延，将最后一次反馈的记录，移到新的结束反馈节点下
		String message = getMessage(info, tkname, endtime);
		List<TkTaskSubject> oldTksubs = tkTaskSubjectService.findSubjects(compid,taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		for (TkTaskSubject oldTksub : oldTksubs) {
			tkTaskFeedBackAuditService.insert(session,Module.MODIFY_TASK_BASIC.getValue(),info.getId()
					,"MODIFY_TASK_BASIC","任务信息变更",message,oldTksub.getSubjectid(),oldTksub.getSubjectname());
		}
		//修改主表基本信息
		tkTaskDeployDao.updateTaskBase(compid,request);
		remindNoteComponent.register(compid,Module.MODIFY_TASK_BASIC, taskid, RemindRuleEnum.MODIFY_TASK_BASIC
                , DateEnum.YYYYMMDDHHMMDD.format(), sessionSub, setSubject);

		logger.info("修改执行人与协办人...");
		updateExcutor(request, session, compid,info);

		logger.info("修改附件...");
		saveOrUpdateFileBusiness(request,session,info);
		//判断审核人存不存在不存在就将之前的审核表信息删除 TODO
		if(request.getSubjects3() == null) {//审核人为空
			generalSqlComponent.getDbComponent().delete("delete from tk_task_feed_back_audit where compid=? and taskid=? and status=0 and remarks2=2", new Object[] {compid,request.getTaskid()});
		}else {
			//被驳回的修改需要重新审核
			if(Constant.TkTaskDeploy.BACK_STATUS == info.getStatus()) {
				newAddAudit(request, session, info, taskid);
			}else {
				//待审核时修改（和项目经理确认过了不管审核人有没有变更都进行先删除后新增）
				generalSqlComponent.getDbComponent().delete("delete from tk_task_feed_back_audit where compid=? and taskid=? and status=0 and remarks2=2", new Object[] {compid,request.getTaskid()});
				newAddAudit(request, session, info, taskid);
			}
		}
		//每次重新发布都需要更新remarks3和remarks6字段
		request.setSession(session);
		updateRemarks3AndRemarks6(session, request);
		logger.info("修改结束时间和反馈节点 ...");
		updateTime(request, session, taskid, info);
		List<Subject> subs = tkTaskSubjectService.listSubjects(request.getTaskid(), 0);
		Set<Subject> set = new HashSet<Subject>(subs);
		remindNoteComponent.register(session.getCompid(),Module.MODIFY_TASK_TIME, request.getTaskid()
                , RemindRuleEnum.MODIFY_TASK_TIME, DateEnum.YYYYMMDDHHMMDD.format(), sessionSub, set);

		return info.getId();
	}


	public String getMessage(TkTaskDeploy task, String tkname, String endtime) {
		StringBuilder modifyMsg = new StringBuilder("您的《");
		modifyMsg.append(task.getTkname()).append("》任务发生了变更。");
		if(!com.alibaba.druid.util.StringUtils.equals(task.getTkname(), tkname)){
			modifyMsg.append(" 任务名称更改为:").append(tkname);
		}
		if(!com.alibaba.druid.util.StringUtils.equals(task.getEndtime(),endtime)){
			modifyMsg.append(" 任务结束时间更改为:").append(endtime);
			generalSqlComponent.update(SqlCode.updateEndTime,new Object[]{endtime,task.getCompid(),task.getId(),task.getEndtime()});
		}
		return modifyMsg.toString();
	}

	@SuppressWarnings("rawtypes")
	public void updateTime(TkTaskDeployRequest request, Session session, Long taskid, TkTaskDeploy info) throws ParseException {
		String oldEndTime = info.getEndtime();
		String endtime = request.getEndtime();

		if(request.getEndtime().length() > 19) {
			request.setEndtime(request.getEndtime().substring(0, 10));
		}
		if(!com.alibaba.druid.util.StringUtils.equals(oldEndTime,endtime)){
			// 修改开始结束时间
			tkTaskDeployDao.upadteStartEndTime(session.getCompid(),request);
			//更新子表的起始时间
			tkMyTaskDao.updateStartAndEnd(session.getCompid(),request.getTaskid(),request.getStarttime(),request.getEndtime());
			// 删除时间节点表数据
			tkTaskTimeNodeService.deleteByTaskid(session.getCompid(),request.getTaskid());
			// 重新插入时间节点表数据
			insertTkTaskTimeNode(session.getCompid(),request.getTaskid(), request.getStarttime(), request.getEndtime(),2);
		}
		//更新提醒时间表tk_task_feed_time(这里要注意更新时先将表中数据根据taskid和remindtimes进行筛选凡是提醒时间大于当前时间的全部删除，小于当前时间的已经发送了消息不要动)
		//删除不需要的记录
		if(com.alibaba.druid.util.StringUtils.equals(request.getTabTimedata(),info.getRemarks6())){
			return;
		}
		//清空所有的
		generalSqlComponent.delete(SqlCode.delTTFTByTaskidAndRemindtimes, new Object[] {taskid,"2000-01-01 00:00:00"});
		//插入新的记录
		List<Map> requireMents = getRequireMentMap(request, info.getTkname());
		insertTaskFeedTime(requireMents,session,info);
		//修改反馈时间点数据
		generalSqlComponent.query(SqlCode.updateTimeDataByTaskId,new Object[]{request.getTimedata(),request.getTabTimedata(),session.getCompid(),taskid});
	}

	private void updateExcutor(TkTaskDeployRequest request, Session session, Long compid, TkTaskDeploy task) throws ParseException, Exception {
		Long taskid = task.getId();
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		Set<Subject> subject2 = request.getSubjects2();//协办人
		//删除非执行人的所有相关人员
		tkTaskSubjectService.delNotExcuterByTaskid(compid, taskid);//flag!=0ge't
		if(subject2!=null && subject2.size()>0) {
			for (Subject sub2 : subject2) {
				tkTaskSubjectService.insert(compid,taskid, Constant.TkTaskSubject.FLAG_COORDINATOR, 0, sub2.getSubjectid(), sub2.getSubjectname(),Constant.EMPTY);
			}
		}
		
		List<Subject> subject3 = request.getSubjects3();//审核人
		if(subject3!=null && subject3.size()>0) {
			for (Subject sub3 : subject3) {
//				/**查询角色关系表 remarks2>0 的*/
				// TODO tkTaskSubjectService 改造
//				int remarks2 = tkTaskSubjectService.getInt("select remarks2 from pw_role_map where compid=? and subjecttp=0 and subjectid=? and remarks2>0", compid,sub3.getSubjectid());
				int remarks2 = tkTaskSubjectService.getRemarks2BySubid(compid,sub3.getSubjectid());
				//主体表使用remarks3 作为审核人职位的标识 1正职 2 副职
				tkTaskSubjectService.insert(compid,taskid, Constant.TkTaskSubject.FLAG_LEADLEADER, 0, sub3.getSubjectid(), sub3.getSubjectname(),new Integer(remarks2).toString());
			}
		}

		Set<Subject> subjects = request.getSubjects();
		if(subjects == null) {
			LehandException.throwException("执行人不能为空");
		}
		List<TkTaskSubject> oldTksubs = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(compid,taskid);
		int addnum = 0;
		int delnum = 0;
		int delReceiveNum = 0;
		//重新添加跨级任务的上级相关人
		addSuperiors(request,session,taskid,subjects);
		//第一步：将传过来的执行人全部插入主体表，如果已存在就不插入，否则就插入并发消息
		for (Subject subject : subjects) {// 用新传的subject id遍历查询
			//查询该任务下是否存在该执行人
			TkTaskSubject num = tkTaskSubjectService.getTkTaskSubjectBy(taskid, subject.getSubjectid());
			if (num== null) {//不存在
				//将新增的执行人插入主体表
				addSubject(compid,taskid, subject.getSubjectid(), subject.getSubjectname());
				insertSubject(compid,sessionSub, taskid, subject);
				addnum++;
			}else {
				TkMyTask mytask = tkMyTaskDao.getMyTaskIdPeriod(compid, taskid, subject.getSubjectid());
				if(num.getRemarks1()==-1) {//专门针对先退回了任务，后来创建人又将该执行人添加到该任务中的情况
					tkTaskSubjectService.updateByRemarks1(compid,taskid,subject.getSubjectid());
					tkMyTaskDao.delete(compid,taskid,subject.getSubjectid());
					insertSubject(compid,sessionSub, taskid, subject);
					addnum++;
				}else if(num.getRemarks2()==-1){ //专门针对删除了任务，后来创建人又将该执行人添加到该任务中的情况
					tkTaskSubjectService.updateByRemarks2(compid,taskid,subject.getSubjectid());
					tkMyTaskDao.updateStatusById(compid,mytask.getId(),Constant.TkMyTask.STATUS_NO_SGIN);
					remindNoteComponent.register(compid,Module.MY_TASK, mytask.getId(), RemindRuleEnum.NEW_TASK, DateEnum.YYYYMMDDHHMMDD.format(),
							sessionSub,subject);
					addnum++;
				}
			}
		}

		//第二步：判断出被删除的执行人，并将其从主体表中删除，以及对这些被删除的人发送删除任务的信息
		List<Subject> newsubs = new ArrayList<Subject>(subjects);

		StringBuilder userid = new StringBuilder();
		for (TkTaskSubject oldTksub : oldTksubs) {
			int number = 0;
			for (Subject newsub : newsubs) {
				if (oldTksub.getSubjectid().equals(newsub.getSubjectid())) {
					break;
				} else {
					number++;
				}
			}
			if (number == newsubs.size()) {
				userid.append(oldTksub.getSubjectid()).append(",");
				delnum++;
			}
		}
		if(!StringUtils.isEmpty(userid.toString())) {
			userid = new StringBuilder(userid.substring(0, userid.length() - 1));//3,5
			String[] strs = userid.toString().split(",");
			for (String str : strs) {
				TkTaskDeploy info = tkTaskDeployDao.get(compid,taskid);
				if(info.getPeriod()==0) {
					//在删除该执行人之前先判断该执行人有没有签收任务如果有就需要到任务附属表中将执行人签收数量减一（因为下面需要根据该签收数量来判断是否更新主表的状态）
					TkMyTask my = tkMyTaskDao.getMyTaskIdPeriod(compid, taskid, Long.valueOf(str));
					if(my!=null&&my.getStatus()==3) {
						tkTaskDeploySubDao.updateNum(compid,taskid);
						delReceiveNum ++;
					}
				}

				Long feedCount = generalSqlComponent.query(SqlCode.getMyFeedCount, new Object[]{compid, taskid,str});
				feedCount = feedCount == null ? 0 :feedCount;
				if(!feedCount.equals(0L)){
					//修改任务主体与我的任务为删除状态
					Map<String, Object> parmas = new HashMap<>(3);
					parmas.put("compid",compid);
					parmas.put("taskid",taskid);
					parmas.put("subid",str);
					generalSqlComponent.update(SqlCode.updateTaskStatus,parmas);
				}else{
					tkMyTaskLogService.deleteByStatus(compid,taskid, Long.valueOf(str));
					tkTaskSubjectService.deleteByFlagAndSubjectId(taskid,compid, Long.valueOf(str),0);
					tkMyTaskDao.delete(compid,taskid, Long.valueOf(str));

				}
				String userName = generalSqlComponent.query(SqlCode.getUsernameByid, new Object[]{compid,Long.valueOf(str)});
                Subject sb = new Subject(Long.valueOf(str),userName);
                //删除执行人时，将该执行人转发的该任务断开
                generalSqlComponent.update(SqlCode.updateSrcTaskId,new Object[]{null,compid,str,taskid});
				tkTaskFeedBackAuditService.insert(session,Module.MODIFY_TASK_SUBJECT.getValue(),taskid
						,Module.MODIFY_TASK_SUBJECT.name(),StringConstant.EMPTY,
						"您已被任务发起人从《"+task.getTkname()+"》任务中移除",sb.getSubjectid(),sb.getSubjectname());

			}

		}
		int count = oldTksubs.size();
		logger.info("=============extnum :{},addnum:{},delnum{}",new Object[]{count,addnum,delnum});
		count = count + addnum - delnum;
		tkTaskDeployDao.updateAddExsjnum(compid,taskid,count);
		int receiveNum = 0;
		if(deploySub!=null) {
			receiveNum = deploySub.getReceivenum() - delReceiveNum;
			logger.info("=============recieveSrcnum :{},delreceivenum{}",new Object[]{deploySub.getReceivenum(),delReceiveNum});
		}
		//更新任务状态
		updateTaskStatus(session, task, taskid, count, receiveNum);
		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(session.getCompid(),taskid);
		avgprogress = avgprogress == null ? 0:avgprogress;
		avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
		tkTaskDeployDao.updateProgressAndnewbacktime(compid,avgprogress, request.getNow(), taskid);
	}
	
	private void updateRemarks3AndRemarks6(Session session,TkTaskDeployRequest request) {
		tkTaskDeployDao.updateRemarks3AndRemarks6(session, request);
	}

	private void updateTaskStatus(Session session, TkTaskDeploy task, Long taskid, int exsjnum, int receivenum) {

		logger.info("========================receivenum:{} exsjnum:{}", new Object[]{receivenum, exsjnum});
		if (exsjnum <= receivenum) {
			logger.info("======================change to signed");
			tkTaskDeployDao.updateTkTaskDeployStauts(session.getCompid(), Constant.TkTaskDeploy.SIGNED_STATUS, taskid);
		} else if (Constant.TkTaskDeploy.SIGNED_STATUS == task.getStatus()) {
			logger.info("======================change to nosigned");
			tkTaskDeployDao.updateTkTaskDeployStauts(session.getCompid(), NO_SIGNED_STATUS, taskid);
		} else if(Constant.TkTaskDeploy.BACK_STATUS == task.getStatus()) {
			tkTaskDeployDao.updateTkTaskDeployStauts(session.getCompid(), Constant.TkTaskDeploy.NOW_STATUS, taskid);
		}
		if (exsjnum <= 0) {
			logger.info("======================change to draft");
			tkTaskDeployDao.updateTkTaskDeployStauts(session.getCompid(), Constant.TkTaskDeploy.DRAFT_STATUS, taskid);
		}
	}

	private void addSuperiors(TkTaskDeployRequest request, Session session, Long taskid, Set<Subject> subjects) {
		//如果是跨级任务，添加相关人
		if(Constant.TkTaskDeploy.CROSS_FLAG_1.equals(String.valueOf(request.getCrossFlag()))){
			TkTaskSubject tts = new TkTaskSubject();
			tts.setCompid(session.getCompid());
			tts.setTaskid(taskid);// 任务ID(自增长主键)
			tts.setSjtype(Constant.TkTaskSubject.SJTYPE_USER);// 主体类型(0:用户,1:机构/组/部门,2:角色)
			tts.setRemarks1(0);// 备注1
			tts.setRemarks2(0);// 备注2
			tts.setRemarks3(Constant.EMPTY);// 备注3
			tts.setRemarks4(Constant.EMPTY);// 备注3
			tts.setRemarks5(Constant.EMPTY);// 备注3
			tts.setRemarks6(Constant.EMPTY);// 备注3

			HashMap<Long, UrcUser> userMap = new HashMap<Long, UrcUser>();
			for (Subject subject : subjects) {
				Long subjectid = subject.getSubjectid();
				getSuperiors(session.getUserid().toString(),subjectid.toString(),userMap);
			}

			insertSubject(tts, userMap);
		}
	}


	/**
	 * 创建周期任务
	 * 
	 * @author pantao
	 * @date 2018年11月12日下午2:08:21
	 *
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public Long deployPeriodTask(TkTaskDeployRequest request, Session session) throws Exception {
		logger.info("发布周期任务...");
		// 部署任务前先验证分类的class id在分类表里是否存在
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		request.setStarttime(DateEnum.YYYYMMDD.format());
		TkTaskDeploy info = insertTaskDeployInfo(request, session, Constant.TkTaskDeploy.PERIOD_YES, RESOLVE_NO);
		insertTkTaskSubject(session,info, request);
		Set<String> times = insertPeriodTkTaskTimeNode(info, request,0);
		TaskProcessEnum.DEPLOY.handle(session, request, info, times);
		return info.getId();
	}

	/**
	 * 周期任务时间节点表保存
	 * 
	 * @author pantao
	 * @date 2018年11月12日下午5:12:40
	 * 
	 * @param info
	 * @param request
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Set<String> insertPeriodTkTaskTimeNode(TkTaskDeploy info, TkTaskDeployRequest request,int status) throws Exception {
		List<Map> times = request.getTimes();
		String endtimes = info.getEndtime();
		Map<String, Object> map = times.get(0);
		String data = map.get("data").toString();// 日期
		List<String> dates = new ArrayList<String>(DateStageEnum.parse(DateEnum.YYYYMMDDHHMMDD.parse(info.getStarttime()),
				DateEnum.YYYYMMDDHHMMDD.parse(endtimes), data));
		Integer end = Integer.parseInt(map.get("end").toString());// 日期
		if (dates.size() <= 0) {
			LehandException.throwException(request.getEndtime() + "时间内无法创建周期任务");
		}
		Date endtime = DateEnum.YYYYMMDD.parse(request.getEndtime());
		Date addDay = DateEnum.addDay(DateEnum.YYYYMMDDHHMMDD.parse(dates.get(0)), end);
		if (addDay.compareTo(endtime) >= 0) {
			LehandException.throwException("任务总结束时间必须大于" + DateEnum.YYYYMMDD.format(addDay));
		}
		for (String date : dates) {
			insertTkTaskTimeNode(info.getCompid(),info.getId(), date, DateEnum.YYYYMMDDHHMMDD.addDay(date, end),status);
		}
		return new HashSet<String>(dates);
	}


	private TkTaskDeploy toTkDeploy(TkTaskDeployRequest request, Session session, int period, int resolve){
		TkTaskDeploy info = new TkTaskDeploy();
		info.setCompid(session.getCompid());
		info.setStarttime(request.getStarttime());// 起始时间
		info.setEndtime(request.getEndtime());// 终止时间
		info.setTkname(request.getTkname());// 任务名称
		info.setClassid(request.getClassid());// 任务分类ID(TK_TASK_CLASS表ID)
		info.setTkdesc(request.getTkdesc());// 任务备注
        if(!StringUtils.isEmpty(request.getSrctaskid())){
		    info.setSrctaskid(request.getSrctaskid());// 源任务ID
        }
		if(period==Constant.TkTaskDeploy.PERIOD_RESOLVE) {
			info.setDiffdata(request.getTimedataresolve());// 差异化数据（存储前台传来的时间数组对象）
		}else {
			info.setDiffdata(request.getTimedata());// 差异化数据（存储前台传来的时间数组对象）
		}
		info.setExsjnum(request.getSubjects().size());// 执行主体数量
		info.setUserid(session.getUserid());// 创建人
		info.setUsername(session.getUsername());// 创建人名称
		info.setPeriod(period);// 是周期任务?(0:普通,1:周期，2分解)
		info.setResolve(resolve);// 是分解任务?(0:不是,1:是)
		info.setStatus(NO_SIGNED_STATUS);// 状态(0:删除,1:草稿,2:未创建,3:正在创建,30:未签收,40:已签收,45:暂停,50:完成)
        if(com.alibaba.druid.util.StringUtils.equals(request.getDraftFlag(),"1")){
            info.setStatus(Constant.TkTaskDeploy.DRAFT_STATUS);// 草稿
        }
		info.setRemarks1(Constant.TkTaskDeploy.REMARKS1_NO);// 备注1（标注任务的修改状态0未修改 1已修改）
		info.setNeedbacktime(Constant.EMPTY);// 反馈时间点要求
		info.setProgress((double) 0);// 总进度值
		info.setNewbacktime(request.getNow());// 最新反馈时间
		info.setCreatetime(request.getNow());// 创建时间
		info.setUpdatetime(Constant.EMPTY);// 更新时间
		info.setDeletetime(Constant.EMPTY);// 删除时间
		info.setRemarks2(request.getCrossFlag());// 备注2 //跨级1，非跨级0
		info.setRemarks3(Constant.EMPTY);// 备注3
		info.setRemarks4(Constant.EMPTY);// 备注3
		info.setRemarks5(Constant.EMPTY);// 备注3
		//表格json数据
		String remarks6 = StringUtils.isEmpty(request.getTabTimedata()) ? Constant.EMPTY : request.getTabTimedata();
		info.setRemarks6(remarks6);// 备注3
		return info;
	}

	/**
	 * 保存时间节点
	 * 
	 * @param taskid
	 * @param starttime
	 * @param endtime
	 */
	protected void insertTkTaskTimeNode(Long compid,Long taskid, String starttime, String endtime, int status) {
		endtime = endtime.substring(0, endtime.length() - 9) + " 23:59:59";
		TkTaskTimeNode node = new TkTaskTimeNode();
		node.setCompid(compid);
		node.setTaskid(taskid);// 某个模块相应的ID,如任务发布
		node.setEnddate(endtime);// 截止日期
		node.setCrtdate(starttime);// 创建日期
		node.setProgress(BigDecimal.ZERO);// 总进度值
		node.setStatus(status);// 状态(0:未创建,1:正在创建,2:创建完成)
		node.setRemarks1(0);// 备注1
		node.setRemarks2(0);// 备注2
		node.setRemarks3(Constant.EMPTY);// 备注3
		node.setRemarks4(Constant.EMPTY);// 备注3
		node.setRemarks5(Constant.EMPTY);// 备注3
		node.setRemarks6(Constant.EMPTY);// 备注3
		int num = tkTaskTimeNodeService.getTimeCountByTaskIdAndEndDate(compid,taskid,endtime);
		if(num<=0) {
			tkTaskTimeNodeService.insert(node);
		}
	}

	/**
	 * 普通任务添加提醒时间
	 * 
	 * @param info
	 * @param request
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Set<String> insertTkTaskRemindNote(TkTaskDeploy info, TkTaskDeployRequest request, Session session)
			throws Exception {
		List<Map> times = request.getTimes();
		Set<String> dates = new HashSet<String>();
		if(!CollectionUtils.isEmpty(times)){
			makeDates(info, times, dates);
		}

		return dates;
	}

	/**
	 * 处理日期
	 * @param info
	 * @param times
	 * @param dates
	 * @throws Exception
	 */
	void makeDates(TkTaskDeploy info, List<Map> times, Set<String> dates) throws Exception {
		for (Map<String, Object> map : times) {
			boolean isGuding = "1".equals(map.get("flag").toString());// 1:固定日期,2:周期日期
			String data = map.get("data").toString().trim();// 日期
			if (isGuding) {
				dates.add(DateEnum.appendDefaultTime(data));
			} else {
				dates.addAll(DateStageEnum.parse(DateEnum.YYYYMMDDHHMMDD.parse(info.getStarttime()),
						DateEnum.YYYYMMDDHHMMDD.parse(info.getEndtime()), data));
			}
		}
	}

	/**
	 * @Description 获取反馈时间点列表
	 * @Author zouchuang
	 * @Date 2019/5/6 15:55
	 * @Param [request, session]
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 **/
	@SuppressWarnings({"rawtypes" })
	public List<Map<String, Object>> getCycleTimesNew(TkTaskDeployRequest request, Session session) throws Exception {
		Long taskid = request.getTaskid();
		boolean isUpdate= taskid != null;
		//生成新的提醒列表
		List<Map<String, Object>> remindList = getRemindList(request, session, isUpdate);

        setDelFlagForCycel(session.getCompid(),DateEnum.now(),taskid,remindList);
		//保留提交的提醒要求
		List<Map> requireMents = JSON.parseArray(request.getTabTimedata(), Map.class);
		if(!CollectionUtils.isEmpty(requireMents)){
			Map<String, String> requireMap = new HashMap<>();
			for (Map requireMent : requireMents) {
				requireMap.put((String) requireMent.get("feedTime"),(String) requireMent.get("require"));
			}

			for (Map<String, Object> remind : remindList) {
				String require = requireMap.get(String.valueOf(remind.get("feedTime")));
				if(!StringUtils.isEmpty(require)){
					remind.put("require",require);
				}
			}
		}
        return remindList;
	}

	private void setDelFlagForCycel(Long compid, Date now, Long taskid, List<Map<String, Object>> timeMaps) throws ParseException {
	    //[{"remindTimes":"2019-04-28 09:00:00","feedTime":"2019-04-29 23:59:59","require":null}]
		TkTaskDeploy oldTask = tkTaskDeployDao.get(compid, taskid);
		if (!CollectionUtils.isEmpty(timeMaps)) {
            for (Map<String,Object> timeMap : timeMaps) {
                String updateFlag = "1";
                String showButton = "1";

                String feedTime = (String) timeMap.get("feedTime");
                Date date;
                if (feedTime.length() > 10) {
                    date = DateEnum.YYYYMMDDHHMMDD.parse(feedTime);
                } else {
                    date = DateEnum.YYYYMMDD.parse(feedTime);
                }
                //Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime, new Object[]{compid, taskid, feedTime});
                if (now.after(date)&&oldTask!=null&&Constant.TkTaskDeploy.DRAFT_STATUS!=oldTask.getStatus() ) {
                    updateFlag = "0";
                }
				Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime, new Object[]{compid, taskid, feedTime});
				count = count==null?0:count;
				if (count.equals(0L) &&oldTask!=null&&Constant.TkTaskDeploy.DRAFT_STATUS!=oldTask.getStatus()) {
					showButton = "0";
				}
                timeMap.put("updateFlag", updateFlag);
				timeMap.put("showButton",showButton);
            }
        }
		JSON.toJSONString(timeMaps);
	}

	/**
	 * 普通任务添加提醒时间
	 *
	 * @param info
	 * @param request
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
    private Set<String> getTimesByRule(TkTaskDeploy info, TkTaskDeployRequest request)
			throws Exception {
		Date start = DateEnum.YYYYMMDDHHMMDD.parse(request.getStarttime());
		Date end = DateEnum.YYYYMMDDHHMMDD.parse(request.getEndtime());
		List<Map> times = request.getTimes();
		Set<String> dates = new HashSet<String>();
		if(!CollectionUtils.isEmpty(times)){
			for (Map<String, Object> map : times) {
				boolean isGuding = "1".equals(map.get("flag").toString());// 1:固定日期,2:周期日期
				String data = map.get("data").toString().trim();// 日期
				if (isGuding) {
					Date time = DateEnum.YYYYMMDDHHMMDD.parse(DateEnum.appendDefaultTime(data));
					if(time.after(start)&&time.before(end)){
						dates.add(DateEnum.appendDefaultTime(data));
					}
				} else {
					dates.addAll(DateStageEnum.parse(DateEnum.YYYYMMDDHHMMDD.parse(info.getStarttime()),
							DateEnum.YYYYMMDDHHMMDD.parse(info.getEndtime()), data));
				}
			}
		}
		dates.add(request.getEndtime());

		return dates;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
    private List<Map<String, Object>> getRemindList(TkTaskDeployRequest request, Session session, boolean isUpdate) throws Exception {
		Date now = Calendar.getInstance().getTime();
		TkTaskDeploy info = new TkTaskDeploy();
		String starttime = request.getStarttime();
		String endtime = request.getEndtime();
		String tkname = request.getTkname();
		info.setStarttime(starttime);
		info.setEndtime(endtime);
		//默认将结束时间添加进来
		Set<String> times = getTimesByRule(info, request);
		Set<String> resultTimes = new HashSet<>();
		TkTaskDeploy oldTask = tkTaskDeployDao.get(session.getCompid(), request.getTaskid());
		if(isUpdate&&(oldTask!=null&&Constant.TkTaskDeploy.DRAFT_STATUS!=oldTask.getStatus())){
			//库里存的列表
			String remindJson = generalSqlComponent.query(SqlCode.getRemindByTaskId, new Object[]{session.getCompid(), request.getTaskid()});
			List<Map> maps = JSON.parseArray(remindJson, Map.class);
			for (Map<String,Object> map : maps) {
				String feedTime = (String) map.get("feedTime");
				if(now.after(DateEnum.YYYYMMDDHHMMDD.parse(feedTime))){
					resultTimes.add(feedTime);
				}
			}
			for (String time : times) {
				if(now.before(DateEnum.YYYYMMDDHHMMDD.parse(time))){
					resultTimes.add(time);
				}
			}
		}else{
			resultTimes = times;
		}

		ArrayList<Date> dates = new ArrayList<Date>();
		for (String time : resultTimes) {
			Date date = DateEnum.YYYYMMDDHHMMDD.parse(time);
			dates.add(date);
		}

		//升序
		dates.sort(new Comparator<Date>() {
			@Override
            public int compare(Date o1, Date o2) {
				return o1.compareTo(o2);
			}
		});
		List<Map<String, Object>> resultMap =new ArrayList<>();
		for (int i = 0; i< dates.size(); i++ ){
			Date start = null;
			Date curDate = dates.get(i);
			if(i == 0){
				start = DateEnum.YYYYMMDDHHMMDD.parse(starttime);
			}else{
				start = dates.get(i-1);
			}
			//添加时间列表
			setTimeList(resultMap,start,curDate,tkname);
		}
		return resultMap;
	}

	private void setTimeList(List<Map<String, Object>> resultTimes, Date start, Date curDate, String tkname){
			HashMap<String, Object> feedTime = new HashMap<>();
			String remind1 = DateEnum.YYYYMMDD.format(DateEnum.addDay(start, 1)) + " 09:00:00";
			if (DateEnum.addDay(start,1).after(curDate)){  //反馈点相差小于一天，直接以第一个反馈时间为提醒点
				remind1 = DateEnum.YYYYMMDDHHMMDD.format(start);
			}
			if(DateEnum.addDay(start,3).before(curDate)){ //反馈点相差超过三天，才有第二个提醒点
				String remind2 = DateEnum.YYYYMMDD.format(DateEnum.addDay(curDate, -1)) + " 09:00:00";
				remind1+=","+remind2;
			}

			feedTime.put("feedTime",DateEnum.YYYYMMDDHHMMDD.format(curDate));
			feedTime.put("require",tkname);
			feedTime.put("remindTimes",remind1);
			resultTimes.add(feedTime);
	}

	/**
     * 草稿状态部署时普通任务添加提醒时间
     *
     * @param info
     * @param
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected Set<String> getReimdTimes(TkTaskDeploy info)
            throws Exception {
        String diffdata = info.getDiffdata();
        List<Map> times = (List<Map>) JSONUtils.parse(diffdata);
        Set<String> dates = new HashSet<String>();
		makeDates(info, times, dates);
		return dates;
    }

	/**
	 * 添加执行主体
	 * 
	 * @param info
	 * @param request
	 * @throws LehandException
	 */
	protected void insertTkTaskSubject(Session session,TkTaskDeploy info, TkTaskDeployRequest request) throws LehandException {
		TkTaskSubject tts = new TkTaskSubject();
		Long compid = session.getCompid();
		tts.setCompid(compid);
		tts.setTaskid(info.getId());// 任务ID(自增长主键)
		tts.setSjtype(Constant.TkTaskSubject.SJTYPE_USER);// 主体类型(0:用户,1:机构/组/部门,2:角色)
		tts.setRemarks1(0);// 备注1
		tts.setRemarks2(0);// 备注2
		tts.setRemarks3(Constant.EMPTY);// 备注3
		tts.setRemarks4(Constant.EMPTY);// 备注3
		tts.setRemarks5(Constant.EMPTY);// 备注3
		tts.setRemarks6(Constant.EMPTY);// 备注3
		Set<Subject> subjects = request.getSubjects();
		insertSubjectByFlag(tts, subjects, Constant.TkTaskSubject.FLAG_EXCUTE);

		if(request.getSubjects1()!=null) {
			insertSubjectByFlag(tts, request.getSubjects1(), Constant.TkTaskSubject.FLAG_FOCUS);
		}

		if(request.getSubjects4()!=null) {
			insertSubjectByFlag(tts, request.getSubjects4(), Constant.TkTaskSubject.FLAG_CHARGELEADER);
		}
		if(request.getSubjects2()!=null) {
			insertSubjectByFlag(tts, request.getSubjects2(), Constant.TkTaskSubject.FLAG_COORDINATOR);
		}
		/**审核人，这里最好将审核人的角色类别带进去，方便后面给审核人发送审核信息使用*/
		saveAuditSubject(session, request, tts);

		//如果是跨级任务，添加相关人
		addRelationSubjects(session, request, tts, subjects);
	}

	private void addRelationSubjects(Session session, TkTaskDeployRequest request, TkTaskSubject tts, Set<Subject> subjects) {
		if (Constant.TkTaskDeploy.CROSS_FLAG_1.equals(String.valueOf(request.getCrossFlag()))) {
			HashMap<Long, UrcUser> userMap = new HashMap<Long, UrcUser>();
			for (Subject subject : subjects) {
				Long subjectid = subject.getSubjectid();
				getSuperiors(session.getUserid().toString(), subjectid.toString(), userMap);
			}

			insertSubject(tts, userMap);
		}
	}

    private void insertSubjectByFlag(TkTaskSubject tts, List<Subject> subjects, int flagExcute) {
        for (Subject subject : subjects) {
            tts.setFlag(flagExcute);// 类别(0:执行主体)
            tts.setSubjectid(subject.getSubjectid());// 主体ID(用户ID、角色ID、机构ID等等)
            tts.setSubjectname(subject.getSubjectname());// 主体名称
            tkTaskSubjectService.insert(tts);
        }
    }

	private void insertSubjectByFlag(TkTaskSubject tts, Set<Subject> subjects, int flagExcute) {
		for (Subject subject : subjects) {
			tts.setFlag(flagExcute);// 类别(0:执行主体)
			tts.setSubjectid(subject.getSubjectid());// 主体ID(用户ID、角色ID、机构ID等等)
			tts.setSubjectname(subject.getSubjectname());// 主体名称
			tkTaskSubjectService.insert(tts);
		}
	}

	private void saveAuditSubject(Session session, TkTaskDeployRequest request, TkTaskSubject tts) {
		if (request.getSubjects3() != null) {
			for (Subject subject : request.getSubjects3()) {
				tts.setFlag(Constant.TkTaskSubject.FLAG_LEADLEADER);// 类别(3:牵头人)(2019-08-05新增加了审核人非必填暂时使用原有牵头人的字段来存储)
				Long subjectid = subject.getSubjectid();
				tts.setSubjectid(subjectid);// 主体ID(用户ID、角色ID、机构ID等等)
				tts.setSubjectname(subject.getSubjectname());// 主体名称
				/**查询角色关系表 remarks2>0 的*/
				//TODO 任务 tkTaskSubjectService 改造
//				int remarks2 = tkTaskSubjectService.getInt("select remarks2 from pw_role_map where compid=? and subjecttp=0 and subjectid=? and remarks2>0", compid,subjectid);

				int remarks2 = tkTaskSubjectService.getRemarks2BySubid(session.getCompid(), subjectid);
				tts.setRemarks3(String.valueOf(remarks2));//主体表使用remarks3 作为审核人职位的标识 1正职 2 副职
				tkTaskSubjectService.insert(tts);
			}
		}
	}

	/**
	 * 处理任务主体保存
	 * @param tts
	 * @param userMap
	 */
	private void insertSubject(TkTaskSubject tts, HashMap<Long, UrcUser> userMap) {
		for (Map.Entry<Long, UrcUser> userEntry : userMap.entrySet()) {
			UrcUser user = userEntry.getValue();
			tts.setSjtype(Constant.TkTaskSubject.SJTYPE_ORG);
			tts.setFlag(Constant.TkTaskSubject.FLAG_SUPERIORS);
//				tts.setSubjectid(user.getId());
			tts.setSubjectid(user.getUserid());
			tts.setSubjectname(user.getUsername());
			tkTaskSubjectService.insert(tts);
		}
	}

	/**
	 * 添加执行主体
	 *
	 * @param info
	 * @param request
	 * @param reSubjects
	 * @throws LehandException
	 */
	protected void insertTkTaskSubjectDraft(Session session, TkTaskDeploy info, TkTaskDeployRequest request, List<TkTaskSubject> reSubjects) throws LehandException {
		List<String> subjectIds = new ArrayList<>(reSubjects.size());
		if(!CollectionUtils.isEmpty(reSubjects)){
			for (TkTaskSubject reSubject : reSubjects) {
				subjectIds.add(String.valueOf(reSubject.getSubjectid()));
			}
		}
		TkTaskSubject tts = new TkTaskSubject();
		tts.setCompid(session.getCompid());
		tts.setTaskid(info.getId());// 任务ID(自增长主键)
		tts.setSjtype(Constant.TkTaskSubject.SJTYPE_USER);// 主体类型(0:用户,1:机构/组/部门,2:角色)
		tts.setRemarks1(0);// 备注1
		tts.setRemarks2(0);// 备注2
		tts.setRemarks3(Constant.EMPTY);// 备注3
		tts.setRemarks4(Constant.EMPTY);// 备注3
		tts.setRemarks5(Constant.EMPTY);// 备注3
		tts.setRemarks6(Constant.EMPTY);// 备注3
		Set<Subject> subjects = request.getSubjects();
		for (Subject subject : subjects) {
			//如果是退回的，修改为正常状态
			if(subjectIds.contains(String.valueOf(subject.getSubjectid()))){
				tkTaskSubjectService.updateByRemarks1(session.getCompid(),info.getId(),subject.getSubjectid());
				continue;
			}
			tts.setFlag(Constant.TkTaskSubject.FLAG_EXCUTE);// 类别(0:执行主体)
			tts.setSubjectid(subject.getSubjectid());// 主体ID(用户ID、角色ID、机构ID等等)
			tts.setSubjectname(subject.getSubjectname());// 主体名称
			tkTaskSubjectService.insert(tts);
		}

		if(request.getSubjects1()!=null) {
			insertSubjectByFlag(tts, request.getSubjects1(), Constant.TkTaskSubject.FLAG_FOCUS);
		}

		if(request.getSubjects4()!=null) {
			insertSubjectByFlag(tts, request.getSubjects4(), Constant.TkTaskSubject.FLAG_CHARGELEADER);
		}
		if(request.getSubjects2()!=null) {
			insertSubjectByFlag(tts, request.getSubjects2(), Constant.TkTaskSubject.FLAG_COORDINATOR);
		}
		
		/**审核人，这里最好将审核人的角色类别带进去，方便后面给审核人发送审核信息使用*/
		saveAuditSubject(session, request, tts);

		//如果是跨级任务，添加相关人
		addRelationSubjects(session, request, tts, subjects);
	}

	/**
	 * 修改执行主体,  先对所有非执行人的主体进行删除，然后查出现有的执行主体，比较那些是新增，哪些是删除，
	 *
	 * @param info
	 * @param request
	 * @throws LehandException
	 */
	protected void updateTkTaskSubject(Session session,TkTaskDeploy info, TkTaskDeployRequest request) throws LehandException {
		Long compid = session.getCompid();
		Long taskid = request.getTaskid();
		tkTaskSubjectService.delNotExcuterByTaskid(compid, taskid);//flag!=0
		HashSet<Subject> addSubjects = new HashSet<Subject>();
		HashSet<Subject> delSubjects = new HashSet<Subject>();
		List<TkTaskSubject> oldSubjects = tkTaskSubjectService.findSubjects(compid, taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		Set<Subject> subjects = request.getSubjects();
		HashSet<Long> oldSubjectIds = new HashSet<Long>();//1,2,3,4
		HashSet<Long> oldSubjectIds2 = new HashSet<Long>();//1,2,3,4
		HashSet<Long> newSubjectIds = new HashSet<Long>();//1,3,4,5

		for (TkTaskSubject oldSubject : oldSubjects) {
			oldSubjectIds.add(oldSubject.getSubjectid());
			oldSubjectIds2.add(oldSubject.getSubjectid());
		}

		for (Subject subject : subjects) {
			newSubjectIds.add(subject.getSubjectid());
		}
		//需要删除的
		oldSubjectIds.removeAll(newSubjectIds);
		//需要新增的
		newSubjectIds.removeAll(oldSubjectIds2);
		for (Subject subject : subjects) {
			if(oldSubjectIds.contains(subject.getSubjectid())){
				delSubjects.add(subject);
			}
			if(newSubjectIds.contains(subject.getSubjectid())){
				addSubjects.add(subject);
			}
		}
		for (Long subjectid : oldSubjectIds) {
			generalSqlComponent.update(SqlCode.updateSubject, new Object[] {compid,taskid,subjectid});
		}
		TkTaskSubject tts = new TkTaskSubject();
		tts.setCompid(compid);
		tts.setTaskid(info.getId());// 任务ID(自增长主键)
		tts.setSjtype(Constant.TkTaskSubject.SJTYPE_USER);// 主体类型(0:用户,1:机构/组/部门,2:角色)
		tts.setRemarks1(0);// 备注1
		tts.setRemarks2(0);// 备注2
		tts.setRemarks3(Constant.EMPTY);// 备注3
		tts.setRemarks4(Constant.EMPTY);// 备注3
		tts.setRemarks5(Constant.EMPTY);// 备注3
		tts.setRemarks6(Constant.EMPTY);// 备注3
		insertSubjectByFlag(tts, addSubjects, Constant.TkTaskSubject.FLAG_EXCUTE);

		if(request.getSubjects1()!=null) {
			insertSubjectByFlag(tts, request.getSubjects1(), Constant.TkTaskSubject.FLAG_FOCUS);
		}

		if(request.getSubjects4()!=null) {
			insertSubjectByFlag(tts, request.getSubjects4(), Constant.TkTaskSubject.FLAG_CHARGELEADER);
		}
		if(request.getSubjects2()!=null) {
			insertSubjectByFlag(tts, request.getSubjects2(), Constant.TkTaskSubject.FLAG_COORDINATOR);
		}
		if(request.getSubjects3()!=null) {
			insertSubjectByFlag(tts, request.getSubjects3(), Constant.TkTaskSubject.FLAG_LEADLEADER);
		}

		//如果是跨级任务，添加相关人
		addRelationSubjects(session, request, tts, subjects);
	}

	/**
	 * 添加执行主体
	 *
	 * @param info
	 * @param request
	 * @throws LehandException
	 */
	protected void insertTkTaskSubjectForDraft(Session session,TkTaskDeploy info, TkTaskDeployRequest request) throws LehandException {
		boolean isUpdate = request.getTaskid() != null;
		if(isUpdate){
			//如果是修改，先删除该任务的所有主体，再重新添加
			Long compid = session.getCompid();
			Long taskid = info.getId();
			List<TkTaskSubject> reSubjects = tkTaskSubjectService.listReturnBytaskid(compid, taskid);
			tkTaskSubjectService.deleteByTaskid(compid, taskid);
			insertTkTaskSubjectDraft(session,info,request,reSubjects);
		}else{
			insertTkTaskSubject(session,info,request);
		}
	}

	//获取所有的隶属上级
    private Map<Long, UrcUser> getSuperiors(String userId, String currenUserId, Map<Long, UrcUser> userMap) {
		UrcUser user = generalSqlComponent.query(SqlCode.getSuperiors, new Object[]{currenUserId});
		if(user == null || userId.equals(String.valueOf(user.getUserid())) || String.valueOf(user.getUserid()).equals(rootUserId)){
			return userMap;
		}else{
			userMap.put(user.getUserid(),user);
			return getSuperiors(userId,String.valueOf(user.getUserid()),userMap);
		}
	}

	//获取所有的隶属上级
    private List<UrcUser> getSuperiorsList(String userId, String currenUserId, List<UrcUser> users) {
		UrcUser user = generalSqlComponent.query(SqlCode.getSuperiors, new Object[]{currenUserId});
		if(user == null || userId.equals(String.valueOf(user.getUserid())) || String.valueOf(user.getUserid()).equals(rootUserId)){
			return users;
		}else{
			users.add(user);
			return getSuperiorsList(userId,String.valueOf(user.getUserid()),users);
		}
	}

	/**
	 * 查看任务详情
	 * 
	 * @author pantao
	 * @date 2018年11月15日下午3:09:05
	 *
	 * @param taskid 任务子表ID
	 * @return
	 * @throws ParseException
	 */
	public TkTaskRespond details(Long taskid,int ismobile, Session session) throws ParseException {
		int module = 2;
        Date now = DateEnum.now();
        TkTaskRespond tkTaskRespond = new TkTaskRespond();
		TkTaskDeploy tkTaskDeploy = tkTaskDeployDao.get(session.getCompid(),taskid);
        String diffdata = tkTaskDeploy.getDiffdata();
        String remarks6 = tkTaskDeploy.getRemarks6();
        //[{"flag":1,"data":"2019-05-20","end":"","delFlag","1"}] 设置能否删除时间点
        diffdata = setDelFlag(session, now, tkTaskDeploy, diffdata);
        remarks6 = setDelFlagForTab(session.getCompid(), now, tkTaskDeploy.getId(),remarks6);

		tkTaskRespond.setTaskid(taskid);
		tkTaskRespond.setTkname(tkTaskDeploy.getTkname());
		tkTaskRespond.setUsername(tkTaskDeploy.getUsername());
		tkTaskRespond.setTkdesc(tkTaskDeploy.getTkdesc());
		tkTaskRespond.setDiffdata(diffdata);
		tkTaskRespond.setTabulardata(remarks6);
		tkTaskRespond.setCrossFlag(tkTaskDeploy.getRemarks2());
		tkTaskRespond.setStarttime(tkTaskDeploy.getStarttime());
		tkTaskRespond.setEndtime(tkTaskDeploy.getEndtime());
		tkTaskRespond.setProgress(tkTaskDeploy.getProgress().doubleValue());
		tkTaskRespond.setDays(DateUtil.calculationDay(DateEnum.YYYYMMDDHHMMDD.parse(tkTaskDeploy.getEndtime())));
		tkTaskRespond.setStatus(tkTaskDeploy.getStatus());
		tkTaskRespond.setExsjnum(tkTaskDeploy.getExsjnum());
		tkTaskRespond.setPeriod(tkTaskDeploy.getPeriod());
		tkTaskRespond.setResolve(tkTaskDeploy.getResolve());
		tkTaskRespond.setSrctaskid(tkTaskDeploy.getSrctaskid());
		List<Map<String, Object>> listMap = tkAttachmentService.list2map(session.getCompid(),module, tkTaskDeploy.getId());
		tkTaskRespond.setAttachment(listMap);
		//附件
        tkTaskRespond.setFiles(filesPreviewAndDownload.listFile(tkTaskDeploy.getFiles(),session.getCompid()));
		
		tkTaskRespond.setAnnex(tkTaskDeploy.getAnnex());
		tkTaskRespond.setPublisher(tkTaskDeploy.getPublisher());
		tkTaskRespond.setTphone(tkTaskDeploy.getTphone());
		tkTaskRespond.setScore(0.0);
		int linkNum = tkTaskSubjectService.countContainDel(session.getCompid(), tkTaskDeploy.getId(), 0);
		tkTaskRespond.setLinkNum(String.valueOf(linkNum));
		// 执行人集合以及执行人机构集合
		List<TkTaskSubject> list = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		getSubject(session.getCompid(),tkTaskRespond, list,0);
		//[{account:userid,username:subjectname,id:organid}]抄送人回显json数据
		if(list!=null && list.size()>0) {
			List<Map<String,Object>> lists = new ArrayList<Map<String,Object>>();
			for (TkTaskSubject tkTaskSubject : list) {
				Long userid = tkTaskSubject.getSubjectid();
				String username = tkTaskSubject.getSubjectname();
				UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid, new Object[] {userid});
				Long id = generalSqlComponent.query(SqlCode.getPUOMByUserid, new Object[] {session.getCompid(),userid});
			    Map<String,Object> map = new HashedMap<String,Object>();
			    map.put("subjectid", userid);
			    map.put("subjectname", username);
                if(user!=null&&user.getAccflag()==0) {
                	map.put("id", userid);
                }else {
                	map.put("id", id);
                }
			    lists.add(map);
			}
			String json = JSON.toJSONString(lists);
			tkTaskRespond.setExcuteuserjson(json);
		}
		
		
		// 任务属性中设置的自定义字段
		//关注人
		List<TkTaskSubject> list1 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_FOCUS);
		getSubject(session.getCompid(),tkTaskRespond, list1,1);
		
		//协办人
		List<TkTaskSubject> list2 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_COORDINATOR);
		getSubject(session.getCompid(),tkTaskRespond, list2, 2);
        if(list2!=null && list2.size()>0) {
            List<Map<String,Object>> lists3 = new ArrayList<Map<String,Object>>();
            for (TkTaskSubject tkTaskSubject : list2) {
                Long account = tkTaskSubject.getSubjectid();
                String username = tkTaskSubject.getSubjectname();
                Long id = generalSqlComponent.query(SqlCode.getPUOMByUserid, new Object[] {session.getCompid(),account});
                UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid, new Object[] {account});
                Map<String,Object> map = new HashedMap<String,Object>();
                map.put("subjectid", account);
                map.put("subjectname", username);
                if(user !=null && user.getAccflag()==0) {
                	map.put("id", account);
                }else {
                	map.put("id", id);
                }
                lists3.add(map);
            }
            String json = JSON.toJSONString(lists3);
            tkTaskRespond.setTrackerjson(json);
        }
		//牵头人
		List<TkTaskSubject> list3 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_LEADLEADER);
		getSubject(session.getCompid(),tkTaskRespond, list3, 3);
		//分管领导
		List<TkTaskSubject> list4 = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_CHARGELEADER);
		getSubject(session.getCompid(),tkTaskRespond, list4,4);
		// 插入任务分类id和名称，用于修改时前端传值
		tkTaskRespond.setClassid(tkTaskDeploy.getClassid());
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),tkTaskDeploy.getClassid(),1);
		tkTaskRespond.setClassname(taskClass.getClname());
		if(tkTaskDeploy.getPeriod()==2) {//分解任务
			List<TkResolveInfo> infos = tkResolveInfoService.listAllByTaskid(session.getCompid(),tkTaskDeploy.getId());
			List<TkTaskResolveInfoRespondDto> lists = new ArrayList<TkTaskResolveInfoRespondDto>();
			if(infos!=null && infos.size()>0) {
				for (TkResolveInfo info : infos) {
					String unit = "";
					if(!info.getUnit().equals("nounit")) {
						TkDict dict = tkDictService.get(session.getCompid(),info.getUnit());
						unit = dict.getName();
					}
					TkTaskResolveInfoRespondDto res = new TkTaskResolveInfoRespondDto();
					if(info.getId().equals("root")) {//根节点
						String endtime = "";
						if(!StringUtils.isEmpty(info.getEndtime())) {
							endtime = info.getEndtime().substring(0,10);
						}
						res.setFbtime(endtime);
						res.setIsroot(true);
						res.setStagename("");
						res.setTopic("<div>"+tkTaskDeploy.getTkname()+"</div><div>目标值："+info.getIdxvalue()+unit+"</div><div class='longDesc'>"+info.getRsldesc()+"</div>");
					}else {//非根节点
						//判断是时间优先还是分解对象优先
						String json = tkTaskDeploy.getDiffdata();
						@SuppressWarnings("rawtypes")
						Map map = JSON.parseObject(json,Map.class);
						String flag = map.get("rule").toString();
						String str = info.getId();
				        String sToFind = "-";
				        int num = 0;
				        while (str.contains(sToFind)) {
				            str = str.substring(str.indexOf(sToFind) + sToFind.length());
				            num ++;
				        }
						String fbtime = "";
						if(!StringUtils.isEmpty(info.getFbtime())) {
							fbtime = info.getFbtime().substring(0,10);
						}
						if(flag.equals("time")) {//时间优先
							if(num==1) {//第二层
								res.setTopic("<div>"+info.getStagename()+"</div><div>目标值："+info.getIdxvalue()+unit+"</div><div class='longDesc'>"+info.getRsldesc()+"</div>");
							}
							if(num==2) {//第三层
								res.setTopic("<div>"+info.getObjname()+"</div><div>目标值："+info.getIdxvalue()+unit+"</div><div>截止日："+fbtime+"</div>");
							}
						}else {//分解对象优先
							if(num==1) {//第二层
								res.setTopic("<div>"+info.getObjname()+"</div><div>目标值："+info.getIdxvalue()+unit+"</div><div class='longDesc'>"+info.getRsldesc()+"</div>");
							}
							if(num==2) {//第三层
								res.setTopic("<div>"+info.getStagename()+"</div><div>目标值："+info.getIdxvalue()+unit+"</div><div>截止日："+fbtime+"</div>");
							}
						}
						res.setStagename(info.getStagename());
						res.setFbtime(fbtime);
						res.setIsroot(false);
					}
					res.setId(info.getId());
					res.setIdxvalue(info.getIdxvalue());
					if(StringUtils.isEmpty(info.getObjid())) {
						res.setObjid("");
					}else {
						res.setObjid(info.getObjid().toString());
					}
					res.setObjname(info.getObjname());
					res.setParentid(info.getPid());
					res.setRsldesc(info.getRsldesc());
					res.setUnit(info.getUnit());
					lists.add(res);
				}
			}
			String jsons = JSON.toJSONString(lists);
			tkTaskRespond.setResolvejson(jsons);
		}
		logger.info("任务详情查询成功！");
		if (ismobile == 1) {
			mobile(taskid, session);
		}
		return tkTaskRespond;
	}
	
	
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private String setDelFlag(Session session, Date now, TkTaskDeploy tkTaskDeploy, String diffdata) throws ParseException {
        if(StringUtils.hasText(diffdata)){
            List<Map> timeMaps = JSON.parseArray(diffdata,Map.class);
            if(!CollectionUtils.isEmpty(timeMaps)){
                for (Map timeMap : timeMaps) {
                    String delFlag = "1";
                    String flag1 = String.valueOf(timeMap.get("flag"));
                    String data = (String) timeMap.get("data");
                    if("1".equals(flag1)){
                        Date date = null;
                    	if(data.length()>10){
							date = DateEnum.YYYYMMDDHHMMDD.parse(data);
						}else{
							date = DateEnum.YYYYMMDD.parse(data);
						}
                        Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime, new Object[]{session.getCompid(), tkTaskDeploy.getId(), data});
                        if(now.after(date)||count>0){
                            delFlag = "0";
                        }
                    }
                    timeMap.put("delFlag",delFlag);
                }
            }
            diffdata = JSON.toJSONString(timeMaps);
        }
        return diffdata;
    }

    @SuppressWarnings({ "rawtypes", "unchecked"})
    private String setDelFlagForTab(Long compid, Date now, Long taskid, String diffdata) throws ParseException {
        if(StringUtils.hasText(diffdata)){
            //[{"remindTimes":"2019-04-28 09:00:00","feedTime":"2019-04-29 23:59:59","require":null}]
            List<Map> timeMaps = JSON.parseArray(diffdata,Map.class);
            if(!CollectionUtils.isEmpty(timeMaps)){
                for (Map timeMap : timeMaps) {
                    String updateFlag = "1";
                    String showButton = "1";
                    String feedTime = (String) timeMap.get("feedTime");
					Date date;
					if (feedTime.length() > 10) {
						date = DateEnum.YYYYMMDDHHMMDD.parse(feedTime);
					} else {
						date = DateEnum.YYYYMMDD.parse(feedTime);
					}
					if (now.after(date)) {
						updateFlag = "0";
					}
					Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime, new Object[]{compid, taskid, feedTime});
					count = count==null?0:count;
					if (count.equals(0L)) {
						showButton = "0";
					}
					timeMap.put("updateFlag", updateFlag);
					timeMap.put("showButton", showButton);
                }
            }
            diffdata = JSON.toJSONString(timeMaps);
        }
        return diffdata;
    }

	public void getSubject(Long compid,TkTaskRespond tkTaskRespond, List<TkTaskSubject> list, int flag) {
		String excuteusers = "";
		String excuteusersids = "";
		String orgnames = "";
		// 修改时使用
		List<Map<String, Object>> subjects = new ArrayList<Map<String, Object>>();
		if (list != null && list.size() > 0) {
			for (TkTaskSubject tkTaskSubject : list) {
				excuteusers += tkTaskSubject.getSubjectname() + ",";
				excuteusersids += tkTaskSubject.getSubjectid() + ",";
				orgnames += generalSqlComponent.query(SqlCode.getOrgname, new Object[] {compid,compid,tkTaskSubject.getSubjectid()}) + ";";
				// 修改时需要字段subjects
				Map<String, Object> subMap = new HashMap<String, Object>();
				subMap.put("subjectid", tkTaskSubject.getSubjectid());
				subMap.put("subjectname", tkTaskSubject.getSubjectname());
				subjects.add(subMap);
			}
			excuteusers = excuteusers.substring(0, excuteusers.length() - 1);
			excuteusersids = excuteusersids.substring(0, excuteusersids.length() - 1);
			switch (flag) {
			case 0://0:执行主体
				tkTaskRespond.setExcuteusers(excuteusers);
				tkTaskRespond.setExcuteusersids(excuteusersids);
				orgnames = orgnames.substring(0, orgnames.length() - 1);
				tkTaskRespond.setSubjects(subjects);// 插入subjectID和name数组
				tkTaskRespond.setOrganname(orgnames);
				break;
			case 1://1:关注主体 
				tkTaskRespond.setTracker(excuteusers);
				orgnames = orgnames.substring(0, orgnames.length() - 1);
				tkTaskRespond.setTrackersubjects(subjects);// 插入subjectID和name数组
				tkTaskRespond.setTrackerorganname(orgnames);
				break;
			case 2://2:协办人
				tkTaskRespond.setCoordinator(excuteusers);
				orgnames = orgnames.substring(0, orgnames.length() - 1);
				tkTaskRespond.setCoordinatorsubjects(subjects);// 插入subjectID和name数组
				tkTaskRespond.setCoordinatororganname(orgnames);
				break;
			case 3://3:牵头人
				tkTaskRespond.setLeadleader(excuteusers);
				orgnames = orgnames.substring(0, orgnames.length() - 1);
				tkTaskRespond.setLeadleadersubjects(subjects);// 插入subjectID和name数组
				tkTaskRespond.setLeadleaderorganname(orgnames);
				break;
			case 4://4:分管领导
				tkTaskRespond.setChargeleader(excuteusers);
				orgnames = orgnames.substring(0, orgnames.length() - 1);
				tkTaskRespond.setChargeleadersubjects(subjects);// 插入subjectID和name数组
				tkTaskRespond.setChargeleaderorganname(orgnames);
				break;
			default:
				break;
			}
		}
	}

	private void mobile(Long taskid, Session session) {
		String format = DateEnum.YYYYMMDDHHMMDD.format();
		BrowerHis his = browerHisService.get(session.getCompid(),taskid, 0, session.getUserid());
		if (StringUtils.isEmpty(his)) {
			BrowerHis browerHis = new BrowerHis();
			browerHis.setCreatetime(format);
			browerHis.setFlag(1);
			browerHis.setModel(0);
			browerHis.setModelid(taskid);
			browerHis.setUserid(session.getUserid());
			browerHis.setUsername(session.getUsername());
			browerHis.setNum(1);
			browerHisService.insert(browerHis);
		} else {
			int num = his.getNum() + 1;
			browerHisService.update(session.getCompid(),num, format, taskid,session.getUserid());
		}
	}

	/**
	 * 
	 * 已部署任务列表数据接口实现方法
	 * 
	 * @author pantao
	 * @date 2018年10月12日 下午3:49:32
	 *
	 * @return
	 * @throws LehandException
	 */
	@SuppressWarnings("unchecked")
	public void page(TkTaskRequest request, Pager pager, Boolean flag, Long userid,Session session) throws LehandException, ParseException {

		Long compid = session.getCompid();
		if(null==session.getAccflag() || session.getAccflag()==0) {
			tkTaskDeployDao.page(session,request, pager,flag,userid);
		}else {//机构账号查询其下所有人员账号创建 TODO
			tkTaskDeployDao.page(session,request,pager,flag,userid);
		}
		List<Map<String,Object>> deploys = (List<Map<String,Object>>) pager.getRows();
		if(CollectionUtils.isEmpty(deploys)){
			return;
		}
		StringBuilder taskids = new StringBuilder();
		for (Map<String, Object> deploy : deploys) {
			Long id = (Long) deploy.get("id");
			taskids.append(id).append(",");
		}
		//状态(0:删除,1:草稿,2:未创建,3:正在创建,30:未签收,40:已签收,45:暂停,50:完成 ，100 暂停，已完成  35部分签收（只用作前端展示）
        for (Map<String, Object> deploy : deploys) {  //封装反馈内容，最近反馈时间，颜色集合
			Long taskId = (Long) deploy.get("id");
			Long exsjnum = ((Integer) deploy.get("exsjnum")).longValue();
			Integer status = (Integer) deploy.get("status");
			String subjectName = tkTaskFeedBackAuditService.judgmentExecutor(compid,taskId);
			deploy.put("feedmsg","");
			deploy.put("subjectname",subjectName);
			deploy.put("remindColor","");
			deploy.put("feedid","");

			if(!deploy.get("status").toString().equals("5")) {
				List<TaskRemindTimes> times = tkTaskFeedBackAuditService.listRemindTimesAll(compid, taskId);
				List<String> colors = new ArrayList<>(times.size());
				for (TaskRemindTimes time : times) {
					colors.add(String.valueOf(time.getFeedstatus()));
				}
				deploy.put("remindColor",colors);
				//查询最近的反馈
				TkMyTaskLog log = generalSqlComponent.query(SqlCode.getlatestFeed, new Object[]{ taskId,compid});
				if(log!=null){
					deploy.put("feedmsg",log.getMesg());
					deploy.put("feedid",log.getId());
				}
				if (status == 30){
					TkTaskDeploySub tkTaskDeploySub = tkTaskDeploySubDao.get(compid, taskId);
					if(tkTaskDeploySub!=null && tkTaskDeploySub.getReceivenum()>0 && tkTaskDeploySub.getReceivenum()< exsjnum){
						deploy.put("status",35);
					}
				}
			}else {//任务审核被驳回 带上审核意见
				Map<String,Object> map = generalSqlComponent.getDbComponent().getMap("select * from tk_task_feed_back_audit where compid=? and taskid=? and status=-1 and remarks2=2 order by createtime desc limit 1", new Object[] {compid,taskId});
				if(map!=null) {
					deploy.put("auditRemark", map.get("remark"));
				}else {
					deploy.put("auditRemark", Constant.EMPTY);
				}
			}
		}
	}


	/**
	 * 
	 * 查询列表表头状态的角标
	 * 
	 * @author pantao
	 * @date 2018年10月15日 下午1:22:55
	 *
	 */
	public List<Map<String, Object>> listCountByStatus( Session session ,Long userid, int sjtype) {
		Long compid = session.getCompid();
		Map<String, Object> map = null;
		if(null == session.getAccflag() || session.getAccflag()==0) {
			map = tkTaskDeployDao.get2Map(compid,userid);
		}else {
			map = tkTaskDeployDao.get3Map(compid,session.getCurrorganid());
		}
		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
		lists.add(getMap("40", "办理中", map));
		lists.add(getMap("30", "未签收", map));
		lists.add(getMap("100", "暂停或完成", map));
		lists.add(getMap("60", "审核", map));
		return lists;
	}

	private static Map<String, Object> getMap(String status, String name, Map<String, Object> map) {
		Map<String, Object> maps = new HashMap<String, Object>(3);
		maps.put("status", status);
		maps.put("name", name);
		maps.put("number", map.get(status));
		return maps;
	}

	/**
	 * 督办跟踪列表查询
	 */
	@SuppressWarnings("unchecked")
	public Pager overSeePage(TkTaskRequest request,Long compid, Long userid, Pager pager) throws LehandException {
		tkTaskDeployDao.overSeePage(request, pager,userid,compid);
		Pager pager2 = pager;
		List<Map<String, Object>> data = (List<Map<String, Object>>) pager.getRows();
		makeLinDaoPageInfo(compid, userid, data);
		List<PwAuthOtherMap> list = generalSqlComponent.query(SqlCode.listPAOTM, new Object[] {compid,userid});
	    if(list==null || list.size()<=0) {
	    	pager2.setRows(new ArrayList<Map<String, Object>>());
	    }else {
	    	pager2.setRows(data);
	    }
		return pager2;
	}
	
	/**
	 * 督办跟踪列表查询
	 */
	@SuppressWarnings("unchecked")
	public Pager lingDaoPage(TkTaskRequest request,int flag, Long compid,Long userid, Pager pager) throws LehandException {
		tkTaskDeployDao.lingDaoPage(request,flag, pager,userid,compid);
		Pager pager2 = pager;
		List<Map<String, Object>> data = (List<Map<String, Object>>) pager.getRows();
		makeLinDaoPageInfo(compid, userid, data);
		List<PwAuthOtherMap> list = generalSqlComponent.query(SqlCode.listPAOTM, new Object[] {compid,userid});
	    if(list==null || list.size()<=0) {
	    	pager2.setRows(new ArrayList<Map<String, Object>>());
	    }else {
	    	pager2.setRows(data);
	    }
		return pager2;
	}

	/**
	 * 督办跟踪列表查询数据
	 * @param compid
	 * @param userid
	 * @param data
	 */
	void makeLinDaoPageInfo(Long compid, Long userid, List<Map<String, Object>> data) {
		for (int i = 0; i < data.size(); i++) {
			Long subjectid = userid;// 根据当前用户的id和返回的taskid去主体表查
			Long taskid = (Long) data.get(i).get("id");
			List<TkTaskSubject> list = tkTaskSubjectService.isFoucs(compid, subjectid, taskid);
			if (list.size() > 0) {
				data.get(i).put("isfocus", true);
			} else {
				data.get(i).put("isfocus", false);
			}
			data.get(i).put("isdisabled", false);
		}
	}

	/**
	 * 任务跟踪督办列表获取各状态数据条数
	 * 
	 * @param userid
	 * @return
	 * @author yuxianzhu
	 */
	public List<Map<String, Object>> getStatusCount(Long userid, Long classid,Long compid) {
		Map<String, Object> map = tkTaskDeployDao.getStatsCount(userid, classid,compid);
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		data.add(getMap("40", "办理中", map));
		data.add(getMap("45", "已暂停", map));
		data.add(getMap("50", "已完成", map));
		return data;
	}

	/**
	 * 点击按钮关注指定任务 点击关注按钮，在主体表里添加一条数据，flag为1（关注主体）
	 * 
	 * @param taskid
	 * @param subjectid
	 * @author yuxianzhu
	 */
	@Transactional(rollbackFor = Exception.class)
	public void focusTask(Long compid,Long taskid, Long subjectid, String subjectname) {
		TkTaskSubject tts = new TkTaskSubject();
		tts.setCompid(compid);
		tts.setTaskid(taskid);// 任务ID(自增长主键)
		tts.setFlag(Constant.TkTaskSubject.FLAG_FOCUS);// 类别(1:关注主体)
		tts.setSjtype(Constant.TkTaskSubject.SJTYPE_USER);// 主体类型(0:用户,1:机构/组/部门,2:角色)
		tts.setRemarks1(0);// 备注1
		tts.setRemarks2(0);// 备注2
		tts.setRemarks3(Constant.EMPTY);// 备注3
		tts.setRemarks4(Constant.EMPTY);// 备注3
		tts.setRemarks5(Constant.EMPTY);// 备注3
		tts.setRemarks6(Constant.EMPTY);// 备注3
		tts.setSubjectid(subjectid);
		tts.setSubjectname(subjectname);
		tkTaskSubjectService.insert(tts);
	}

	/**
	 * 点击进行催办操作 yuxianzhu
	 */
	@Transactional(rollbackFor = Exception.class)
	public void toUrgent(Long taskid,String enddate, Session session) throws LehandException {
		List<TkMyTask> list = null;
		if(StringUtils.isEmpty(enddate)) {
			list = tkMyTaskDao.getNeedUegernt(session.getCompid(),taskid);
		}else {
			list = tkMyTaskDao.getNeedUegernt(session.getCompid(),taskid,enddate);
		}
		try {
			for (int i = 0; i < list.size(); i++) {
				TkMyTask tkMyTask = list.get(i);
				TaskProcessEnum.CUI_BAN.handle(session, tkMyTask);
			}
		} catch (LehandException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 取消关注任务 yuxianzhu
	 */
	@Transactional(rollbackFor = Exception.class)
	public void cancelFocus(Session session, Long taskid) {
		try {
			tkTaskSubjectService.cancelFocus(session.getCompid(),session.getUserid(), taskid);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	   * 删除任务  （全部删除）
	 * 
	 * @author pantao  
	 * @date 2018年10月18日 上午10:14:53
	 *  
	 * @param id
	 * @return
	 * @throws LehandException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public void deleteAll(Long id,int flag,String enddate,Session session) throws LehandException {
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),id);
		if(deploy!=null) {
			TaskProcessEnum.DELETE_TASK.handle(session, deploy,flag,enddate);
			//删除待办任务
			removeTodoItem(Todo.ItemType.TYPE_DEPLOY, String.valueOf(id), session);
			String msg = "【智慧党建】: 您好," + deploy.getTkname() + "的反馈暂时取消,给您带来的不便敬请谅解!";
			//根据任务id获取对应的执行者信息
			List<TkTaskSubject> tkTaskSubjects = generalSqlComponent.query(SqlCode.queryTkTaskSubjectByTaskId,new Object[]{id,0,session.getCompid()});
			if(tkTaskSubjects.size() > 0){
				for (TkTaskSubject tkTaskSubject : tkTaskSubjects) {
					//获取到主体id(机构id)
					Long subjectId = tkTaskSubject.getSubjectid();
					if(StringUtils.isEmpty(subjectId.toString())){
						continue;
					}
					Map<String, Object> user = generalSqlComponent.query(SqlCode.queryUrcUserBySubjectId, new Object[]{subjectId});
					String phone = user.get("phone").toString();
					if (user == null || StringUtils.isEmpty(phone)) {
						continue;
					}
					String userid = user.get("userid").toString();
					SmsSendDto smsSendDto = new SmsSendDto();
					smsSendDto.setCompid(session.getCompid());
					smsSendDto.setUsermark(userid);
					smsSendDto.setPhone(phone);
					smsSendDto.setContent(msg);
					smsSendDto.setBusinessid(Long.parseLong(Constant.CANCEL_TASK));
					sendMessageService.attendMeetingSms(smsSendDto);
				}
			}
		}else {
			LehandException.throwException("任务不存在");
		}
	}

	/**
	 * 暂停任务总入口s
	 * 
	 * @author pantao
	 */
	@Transactional(rollbackFor = Exception.class)
	public void suspend(Long id, int flag, String userid, Session session) throws LehandException {
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),id);
		if (deploy == null) {
			LehandException.throwException("任务不存在！");
		}
		TaskProcessEnum.APPLY_SUSPEND_TASK.handle(session, deploy,flag, userid);
	}
	
	/**
	 * 恢复任务
	 */
	@Transactional(rollbackFor = Exception.class)
	public void recovery(Long id,Session session) throws LehandException{
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),id);
		if (deploy == null) {
			LehandException.throwException("任务不存在！");
		}
		TaskProcessEnum.RECOVERY_TASK.handle(session, deploy);
	}
	
	/**
	 * 确认完成
	 */
	@Transactional(rollbackFor = Exception.class)
	public void complete(Long id, int flag, String date,String userid, Session session) throws LehandException{
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),id);
		if (deploy == null) {
			LehandException.throwException("任务不存在！");
		}
		TaskProcessEnum.COMPLETE_TASK.handle(session, deploy, flag, date,userid);
	}

	/**
	 *  根据id获取主任务
	 */
	public TkTaskDeploy getTaskByTaskid(Long compid,Long taskid) {
		return tkTaskDeployDao.get(compid,taskid);
	}

	/**
	 * 修改任务基本信息：任务名称、备注、分类、起始时间等
	 * 
	 * @param request
	 * @param
	 * @author yuxianzhu
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(rollbackFor = Exception.class)
	public void updateTaskBase(TkTaskDeployRequest request, Session session) throws Exception {
		// 验证修改的class id是否存在
		TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		Long taskid = request.getTaskid();
		List<Subject> oldsubs = tkTaskSubjectService.listSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		Set<Subject> setSubject = new HashSet<Subject>(oldsubs);
		TkTaskDeploy taskDeploy = tkTaskDeployDao.get(session.getCompid(),taskid);
		//修改自定义表单信息(该版本不实现)
		Set<Subject> subject1 = request.getSubjects1();//关注人
		//第一步：删除该任务的关注人
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_FOCUS);
		if(subject1!=null && subject1.size()>0) {
			for (Subject sub1 : subject1) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_FOCUS, 0, sub1.getSubjectid(), sub1.getSubjectname(),Constant.EMPTY);
			}
		}
		Set<Subject> subject2 = request.getSubjects2();//协办人
		//删除任务协办人
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_COORDINATOR);
		if(subject2!=null && subject2.size()>0) {
			for (Subject sub2 : subject2) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_COORDINATOR, 0, sub2.getSubjectid(), sub2.getSubjectname(),Constant.EMPTY);
			}
		}
		List<Subject> subject3 = request.getSubjects3();//牵头领导
		//删除牵头领导
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_LEADLEADER);
		if(subject3!=null && subject3.size()>0) {
			for (Subject sub3 : subject3) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_LEADLEADER, 0, sub3.getSubjectid(), sub3.getSubjectname(),Constant.EMPTY);
			}
		}
		Set<Subject> subject4 = request.getSubjects4();//分管领导
		//删除分管领导
		tkTaskSubjectService.deleteByFlagTaskid(session.getCompid(),taskid,Constant.TkTaskSubject.FLAG_CHARGELEADER);
		if(subject4!=null && subject4.size()>0) {
			for (Subject sub4 : subject4) {
				tkTaskSubjectService.insert(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_CHARGELEADER, 0, sub4.getSubjectid(), sub4.getSubjectname(),Constant.EMPTY);
			}
		}
		// 先判断任务是周期还是普通任务（为0是普通任务），普通任务只修改名称分类等，周期任务还需修改任务时间
		if (taskDeploy.getPeriod() ==1) {
			// 周期任务
			// 获取任务相关时间数据
			String timeData = request.getTimes().get(0).toString();
			if (!request.getEndtime().equals(taskDeploy.getEndtime()) || (!taskDeploy.getDiffdata().equals(timeData))) {
				request.setStarttime(DateEnum.YYYYMMDD.format());
				//更新主表数据
				tkTaskDeployDao.upadteTaskBasePeriod(session.getCompid(),request);
				//删除任务子表
				tkMyTaskDao.delete(session.getCompid(),taskid);
				// 周期任务截止时间或者任务生成周期改变
				tkTaskTimeNodeService.deleteByTaskid(session.getCompid(),taskid);// 删除原时间节点表信息
				//删除消息登记簿中关于该任务的消息以及消息详情表中的消息等等不影响任务的暂时不删除，后续有时间再弄
				taskDeploy.setEndtime(request.getEndtime());
				taskDeploy.setStarttime(request.getStarttime());
				taskDeploy.setId(taskid);
				// 调用周期任务保存时间节点方法
				Set<String> times = insertPeriodTkTaskTimeNode(taskDeploy, request,2);
				List<Map> time = request.getTimes();
				Map<String, Object> maps = time.get(0);
				int end =Integer.valueOf(maps.get("end").toString());
				for (String str : times) {
					String endtime = DateEnum.YYYYMMDDHHMMDD.addDay(str, end);
					endtime = endtime.substring(0, endtime.length() - 9) + " 23:59:59";
					//通过时间节点和主任务id查询对应的时间节点表和主体表
					TkTaskTimeNode node = tkTaskTimeNodeService.getBean(taskDeploy.getCompid(),taskDeploy.getId(),endtime);
					List<TkTaskSubject> subjects = tkTaskSubjectService.listBytaskid(taskDeploy.getCompid(),taskDeploy.getId(),Constant.TkTaskSubject.FLAG_EXCUTE);
					for (TkTaskSubject subject : subjects) {
						createSubjectTaskByTkTaskTimeNode(taskDeploy, node, subject);
					}
				}
				remindNoteComponent.register(session.getCompid(),Module.MODIFY_TASK_BASIC, taskid, RemindRuleEnum.MODIFY_TASK_BASIC, times,
						sessionSub, setSubject);
			}
		} else {// 普通任务
			//修改主表基本信息
			tkTaskDeployDao.updateTaskBase(session.getCompid(),request);
			remindNoteComponent.register(session.getCompid(),Module.MODIFY_TASK_BASIC, taskid, RemindRuleEnum.MODIFY_TASK_BASIC, DateEnum.YYYYMMDDHHMMDD.format(),
					sessionSub, setSubject);
		}
	}
	
	/**
	 * 创建执行主体的任务
	 * @param node
	 * @param subject
	 * @throws LehandException 
	 */
	public void createSubjectTaskByTkTaskTimeNode(TkTaskDeploy deploy,TkTaskTimeNode node,TkTaskSubject subject) throws LehandException {
		TkMyTask info = new TkMyTask();
		info.setCompid(deploy.getCompid());
		info.setTaskid(node.getTaskid());//任务ID(自增长主键)
		info.setDatenode(node.getEnddate());//时间节点(等价于截至日)
		info.setSjtype(subject.getSjtype());//主体类型(0:用户,1:机构/组/部门,2:角色)
		info.setSubjectid(subject.getSubjectid());//主体ID(用户ID、角色ID、机构ID等等)
		info.setSubjectname(subject.getSubjectname());
		info.setCreatetime(node.getCrtdate());//创建时间(这里原本是写的当前时间，但是在我的任务，任务详情中有个任务的开始时间，所以将时间节点表中的创建时间赋给我的任务的创建时间)2018/12/17修改
		info.setNewtime(Constant.EMPTY);//最新反馈时间
		info.setProgress(0.0);//反馈进度值
		info.setStatus(Constant.TkMyTask.STATUS_NO_SGIN);//状态(0:删除,1:草稿,2:未签收,3:已签收,4:暂停,5:完成)
		info.setScore(0.0);//评分
		info.setIntegral(0.0);//积分
		info.setRemarks1(0);//备注1
		info.setRemarks2(0);//备注2
		info.setRemarks3(Constant.EMPTY);//备注3
		info.setRemarks4(Constant.EMPTY);//备注3
		info.setRemarks5(Constant.EMPTY);//备注3
		info.setRemarks6(Constant.EMPTY);//备注3
		info.setAnnex(deploy.getAnnex());
		info.setPublisher(deploy.getPublisher());
		info.setTphone(deploy.getTphone());
		tkMyTaskDao.insert(info);
		TaskProcessEnum.CREATE.handle(null, deploy, subject, info);
	}

	/**
	 * 修改执行人
	 * 
	 * @param request
	 * @param session
	 * @author yuxianzhu
	 * @throws LehandException
	 */
	@Transactional(rollbackFor = Exception.class)
	public int updateTaskSubjects(TkTaskDeployRequest request, Session session) throws LehandException {
		if(request.getSubjects() == null) {
			LehandException.throwException("执行人不能为空");
		}
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		//第一步：将传过来的执行人全部插入主体表，如果已存在就不插入，否则就插入并发消息
		Long taskid = request.getTaskid();
		for (Subject subject : request.getSubjects()) {// 用新传的subject id遍历查询
			//查询该任务下是否存在该执行人
			TkTaskSubject num = tkTaskSubjectService.getTkTaskSubjectBy(taskid, subject.getSubjectid());
			if (num== null) {//不存在
				//将新增的执行人插入主体表
				addSubject(session.getCompid(),taskid, subject.getSubjectid(), subject.getSubjectname());
				insertSubject(session.getCompid(),sessionSub, taskid, subject);
			}else {
				if(num.getRemarks1()==-1) {//专门针对先退回了任务，后来创建人又将该执行人添加到该任务中的情况
					tkTaskSubjectService.updateByRemarks1(session.getCompid(),taskid,subject.getSubjectid());
					insertSubject(session.getCompid(),sessionSub, taskid, subject);
				}
			}
		}
		
		//第二步：判断出被删除的执行人，并将其从主体表中删除，以及对这些被删除的人发送删除任务的信息
		List<Subject> newsubs = new ArrayList<Subject>(request.getSubjects());
		//查询包括后来新增的执行人的集合
		List<TkTaskSubject> oldsubs = tkTaskSubjectService.findSubjects(session.getCompid(),taskid, Constant.TkTaskSubject.FLAG_EXCUTE);
		StringBuilder userid = new StringBuilder();
		for (TkTaskSubject oldsub : oldsubs) {
			int number = 0;
			for (Subject newsub : newsubs) {
				if (oldsub.getSubjectid().equals(newsub.getSubjectid())) {
					break;
				} else {
					number++;
				}
			}
			if (number == newsubs.size()) {
				userid.append(oldsub.getSubjectid()).append(",");
			}
		}
		if(!StringUtils.isEmpty(userid.toString())) {
			userid = new StringBuilder(userid.substring(0, userid.length() - 1));//3,5
			String[] strs = userid.toString().split(",");
			for (String str : strs) {
				TkTaskDeploy info = tkTaskDeployDao.get(session.getCompid(),taskid);
				if(info.getPeriod()==0) {
					//在删除该执行人之前先判断该执行人有没有签收任务如果有就需要到任务附属表中将执行人签收数量减一（因为下面需要根据该签收数量来判断是否更新主表的状态）
					TkMyTask my = tkMyTaskDao.getMyTaskIdPeriod(session.getCompid(), taskid, Long.valueOf(str));
					if(my.getStatus()==3) {
						tkTaskDeploySubDao.updateNum(session.getCompid(),taskid);
					}
				}
				TkTaskSubject sub = tkTaskSubjectService.getBySubject(session.getCompid(),Long.valueOf(str));
				Subject sb = new Subject(Long.valueOf(str),sub.getSubjectname());
				remindNoteComponent.register(session.getCompid(),Module.DELETE_TASK, taskid, RemindRuleEnum.DELETE_TASK, DateEnum.YYYYMMDDHHMMDD.format(),
						sessionSub, sb);
				//删除该执行人的任务
				tkMyTaskDao.delete(session.getCompid(),taskid,Long.valueOf(str));
				//删除积分详情表
				tkIntegralDetailsService.deleteByTaskIdAndUserid(session.getCompid(),taskid,Long.valueOf(str));
				//删除反馈表数据
				tkMyTaskLogService.deleteByMyTaskid(session.getCompid(),taskid,Long.valueOf(str));
				//删除评论表数据 
				tkCommentService.deleteByMyTaskid(session.getCompid(),taskid,Long.valueOf(str));
				//删除回复表数据
				tkCommentReplyService.deleteByMyTaskid(session.getCompid(),taskid,Long.valueOf(str));
			}
			//将去除的执行人从主体表中删除掉
			tkTaskSubjectService.deleteSubjectByIn(session.getCompid(),taskid, userid.toString());
		}
		int count = tkTaskSubjectService.count(session.getCompid(),taskid,0,Constant.EMPTY);
		tkTaskDeployDao.updateAddExsjnum(session.getCompid(),taskid,count);
		TkTaskDeploy deploy = tkTaskDeployDao.getTask(session.getCompid(),taskid);
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(session.getCompid(),taskid);
		if(deploy.getPeriod() == 0) {//普通
			if(deploy.getExsjnum().equals(deploySub.getReceivenum())) {//主表的执行人数量等于附属表中的签收人数量
				tkTaskDeployDao.updateStatus(session.getCompid(),taskid, Constant.TkTaskDeploy.SIGNED_STATUS);
			}
		}else {//周期,查询该任务下面有没有某个周期下的任务全部已签收
			int num = tkMyTaskDao.countBySign(session.getCompid(),taskid,deploy.getExsjnum());
			if(num>0) {
				tkTaskDeployDao.updateStatus(session.getCompid(),taskid, Constant.TkTaskDeploy.SIGNED_STATUS);
			}
		}
		// 更新任务发布表进度和最新反馈时间(排除退回的)
		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(session.getCompid(),taskid);
		avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
		tkTaskDeployDao.updateProgressAndnewbacktime(session.getCompid(),avgprogress, request.getNow(), taskid);
		//写入提醒登记簿
		remindNoteComponent.register(session.getCompid(),Module.MODIFY_TASK_SUBJECT, taskid, RemindRuleEnum.MODIFY_TASK_SUBJECT, DateEnum.YYYYMMDDHHMMDD.format(),
				sessionSub, request.getSubjects());
		
		//全部修改完毕后查询下该任务的状态
		TkTaskDeploy info = tkTaskDeployDao.get(session.getCompid(),taskid);
		return info.getStatus();
	}

	//给新增的执行人创建任务并发消息
	private void insertSubject(Long compid,Subject sessionSub, Long taskid, Subject subject) throws LehandException {
		//查询该任务的开始时间和结束时间
		TkTaskDeploy tkTaskDeploy = tkTaskDeployDao.get(compid,taskid);
		Subject sub = new Subject(subject.getSubjectid(), subject.getSubjectname());
		if(tkTaskDeploy.getPeriod()==0) {//普通
			//并且在任务子表中创建一条数据
			TkMyTask mytask = createSubjectTaskByTkTaskTimeNode(compid,taskid,tkTaskDeploy.getStarttime(),tkTaskDeploy.getEndtime(), subject.getSubjectid(), subject.getSubjectname());
			//并给该执行人发送有任务要签收任务的消息
			remindNoteComponent.register(compid,Module.MY_TASK, mytask.getId(), RemindRuleEnum.NEW_TASK, DateEnum.YYYYMMDDHHMMDD.format(),
					sessionSub, sub);
		}else {//周期
			//第一步：查询该任务的所有周期
			List<TimeResponse> list = tkMyTaskDao.listByTaskidNoPage(compid,taskid);
			//遍历所有周期，再逐个周期创建任务
			for (TimeResponse timeResponse : list) {
				TkMyTask mytask = createSubjectTaskByTkTaskTimeNode(compid,taskid,timeResponse.getCreatetime(),timeResponse.getEndtime(), subject.getSubjectid(), subject.getSubjectname());
				remindNoteComponent.register(compid,Module.MY_TASK, mytask.getId(), RemindRuleEnum.NEW_TASK, DateEnum.YYYYMMDDHHMMDD.format(),
						sessionSub, sub);
			}
		}
	}
	
	/**
	 * 手动创建子任务
	 * @param taskid
	 * @param starttime
	 * @param endtime
	 * @param subjectid
	 * @param subjectname
	 * @throws LehandException
	 */
	private TkMyTask createSubjectTaskByTkTaskTimeNode(Long compid,Long taskid,String starttime,String endtime,Long subjectid,String subjectname) throws LehandException {
		TkMyTask info = new TkMyTask();
		info.setCompid(compid);
		info.setTaskid(taskid);//任务ID(自增长主键)
		info.setDatenode(endtime);//时间节点(等价于截至日)
		info.setSjtype(0);//主体类型(0:用户,1:机构/组/部门,2:角色)
		info.setSubjectid(subjectid);//主体ID(用户ID、角色ID、机构ID等等)
		info.setSubjectname(subjectname);
		info.setCreatetime(starttime);//创建时间(这里原本是写的当前时间，但是在我的任务，任务详情中有个任务的开始时间，所以将时间节点表中的创建时间赋给我的任务的创建时间)2018/12/17修改
		info.setNewtime(Constant.EMPTY);//最新反馈时间
		info.setProgress(0.0);//反馈进度值
		info.setStatus(Constant.TkMyTask.STATUS_NO_SGIN);//状态(0:删除,1:草稿,2:未签收,3:已签收,4:暂停,5:完成)
		info.setScore(0.0);//评分
		info.setIntegral(0.0);//积分
		info.setRemarks1(0);//备注1
		info.setRemarks2(0);//备注2
		info.setRemarks3(Constant.EMPTY);//备注3
		info.setRemarks4(Constant.EMPTY);//备注3
		info.setRemarks5(Constant.EMPTY);//备注3
		info.setRemarks6(Constant.EMPTY);//备注3
		return tkMyTaskDao.insert(info);
	}

	/**
	 * 修改执行人时新增执行人
	 * 
	 * @param taskid
	 * @param subjectid
	 * @param subjectname
	 * @author yuxianzhu
	 */
	@Transactional(rollbackFor = Exception.class)
	public void addSubject(Long compid,Long taskid, Long subjectid, String subjectname) {
		TkTaskSubject tts = new TkTaskSubject();
		tts.setCompid(compid);
		tts.setTaskid(taskid);// 任务ID(自增长主键)
		tts.setFlag(Constant.TkTaskSubject.FLAG_EXCUTE);// 类别(0:执行主体)
		tts.setSjtype(Constant.TkTaskSubject.SJTYPE_USER);// 主体类型(0:用户,1:机构/组/部门,2:角色)
		tts.setRemarks1(0);// 备注1
		tts.setRemarks2(0);// 备注2
		tts.setRemarks3(Constant.EMPTY);// 备注3
		tts.setRemarks4(Constant.EMPTY);// 备注3
		tts.setRemarks5(Constant.EMPTY);// 备注3
		tts.setRemarks6(Constant.EMPTY);// 备注3
		tts.setSubjectid(subjectid);
		tts.setSubjectname(subjectname);
		tkTaskSubjectService.insert(tts);
	}

	/**
	 * 修改普通任务起止时间和其反馈时间节点
	 * 
	 * @param request
	 * @author yuxianzhu
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateTimes(TkTaskDeployRequest request, Session session) throws Exception {
		// TkTaskDeploy taskDeploy=tkTaskDeployDao.get(request.getTaskid());
		// 判断起止时间是否发生改变
		String starttime = request.getStarttime();
		String endtime = request.getEndtime();
		starttime = DateEnum.YYYYMMDDHHMMDD.format(DateEnum.YYYYMMDD.parse(starttime));
		endtime = DateEnum.YYYYMMDD.format(DateEnum.YYYYMMDD.parse(endtime))+" 23:59:59";
		request.setStarttime(starttime);
		request.setEndtime(endtime);
		// 首先判断开始时间是否小于当前时间
		if (request.getStarttime().compareTo(DateEnum.YYYYMMDD.format()) < 0) {
			LehandException.throwException("任务开始时间不能小于当前时间");
		}
		if(request.getStarttime().length() > 19) {
			request.setStarttime(request.getStarttime().substring(0, 10));
		}
		if(request.getEndtime().length() > 19) {
			request.setEndtime(request.getEndtime().substring(0, 10));
		}
		// 修改开始结束时间
		tkTaskDeployDao.upadteStartEndTime(session.getCompid(),request);
		//更新子表的起始时间
		tkMyTaskDao.updateStartAndEnd(session.getCompid(),request.getTaskid(),request.getStarttime(),request.getEndtime());
		// 删除时间节点表数据
		tkTaskTimeNodeService.deleteByTaskid(session.getCompid(),request.getTaskid());
		// 重新插入时间节点表数据
		insertTkTaskTimeNode(session.getCompid(),request.getTaskid(), request.getStarttime(), request.getEndtime(),2);
		// 删除消息提醒表
		tkTaskRemindNoteService.deleteByTaskid(session.getCompid(),request.getTaskid(), 0);
		TkTaskDeploy tkTaskDeploy = new TkTaskDeploy();
		tkTaskDeploy.setStarttime(request.getStarttime());
		tkTaskDeploy.setEndtime(request.getEndtime());
		// 调新增时方法处理消息提醒
		Set<String> times = insertTkTaskRemindNote(tkTaskDeploy, request, session);
		// 插入消息提醒数据
		Subject sessionSub = new Subject(session.getUserid(), session.getUsername());
		List<Subject> subs = tkTaskSubjectService.listSubjects(request.getTaskid(), 0);
		Set<Subject> set = new HashSet<Subject>(subs);
		remindNoteComponent.register(session.getCompid(),Module.DEPLOY_TASK, request.getTaskid(), RemindRuleEnum.DEPLOY_TASK, times,
				sessionSub, set);
		remindNoteComponent.register(session.getCompid(),Module.MODIFY_TASK_TIME, request.getTaskid(), RemindRuleEnum.MODIFY_TASK_TIME, DateEnum.YYYYMMDDHHMMDD.format(),
				sessionSub, set);
	}

	/**
	 * 自定义表单查询
	 */
	public List<TkTaskFormRespond> form(Long compid) {
		return tkTaskDeployDao.listForm(compid);
	}
	
	/**
	 * 根据授权的任务分类的id查询所有相关的任务
	 * @param
	 * @return
	 */
	public List<TkTaskDeploy> listByIds(Long compid,String ids){
		return tkTaskDeployDao.listByIds(compid,ids);
	}

	/**
	 * @Description  列出直属上级，直到任务发布人或者一级组织机构为止，不包含发布人及一级组织机构
	 * @Author zouchuang
	 * @Date 2019/5/6 15:49
	 * @Param [request, session]
	 * @return java.util.List<com.lehand.power.api.pojo.PwUser>
	 **/
//    public List<PwUser> listSuperiors(TkTaskDeployRequest request, Session session) {
    public List<UrcUser> listSuperiors(TkTaskDeployRequest request, Session session) {
        Long taskid = request.getTaskid();
        TkTaskDeploy tkTaskDeploy = tkTaskDeployDao.get(session.getCompid(), taskid);
        assert tkTaskDeploy != null;
//        List<PwUser> users = new ArrayList<>();
        List<UrcUser> users = new ArrayList<>();
        getSuperiorsList(String.valueOf(tkTaskDeploy.getUserid()),String.valueOf(session.getUserid()), users);
//        PwUser user = new PwUser();
		UrcUser user = new UrcUser();
        user.setUsername(tkTaskDeploy.getUsername());
//		user.setId(tkTaskDeploy.getUserid());
		user.setUserid(tkTaskDeploy.getUserid());
		users.add(user);
		return users;
    }

	@Transactional(rollbackFor = Exception.class)
	public void remindForOne(TkTaskDeployRequest request, Session session) {
		Long taskid = request.getTaskid();
		String feedTime = request.getFeedTime();
		String require = request.getRequire();
		Assert.hasText(String.valueOf(taskid),"taskid cant be blank" );
		Assert.hasText(feedTime,"feedtime cant be blank");
		Assert.hasText(require,"require cant be blank");
		TkTaskDeploy task = tkTaskDeployDao.get(session.getCompid(), taskid);
		List<TkTaskSubject> subjects = tkTaskSubjectService.findSubjects(session.getCompid(), taskid, 0);
		for (TkTaskSubject subject : subjects) {
			tkTaskFeedBackAuditService.insert(session,Module.MODIFY_TASK_BASIC.getValue(),taskid
					,StringConstant.EMPTY,StringConstant.EMPTY,
					"您的《"+task.getTkname()+"》任务,"+feedTime+"的反馈节点的要求发生了变更,变更为:"+require,subject.getSubjectid(),subject.getSubjectname());
		}
		generalSqlComponent.query(SqlCode.updateRequire,new Object[]{require,session.getCompid(),taskid,feedTime});
	}

	public static class TkTaskDeployListener extends AnalysisEventListener<List<String>>{
		TkTaskClassDao tkTaskClassDao = getTkTaskClassDao();
		private Long importId;
		private Session session;
		private boolean readHead;
		private boolean firstFlag=false;
		private int line = 0;
		
		private TkTaskClassDao getTkTaskClassDao() {
			return  SpringUtil.getBean(TkTaskClassDao.class);
		};
		
		public TkTaskDeployListener(Session session,Long importId) {
			this.importId = importId;
			this.session = session;
		}

		@Override
		public void invoke(List<String> row, AnalysisContext context) {
			if(firstFlag) {
				int crn = context.getCurrentRowNum();
				if (!readHead && crn <= 1) {
					for (int i = 0; i < POIExcelUtil.HANDERS.length; i++) {
						if (!POIExcelUtil.HANDERS[i].equals(row.get(i).trim())) {
							throw new RuntimeException("上传失败，请上传系统指定导入模板!");
						}
					}
					readHead = true;
					return;
				}
				if (row.size() == 4 && "".equals(row.get(0)) && "".equals(row.get(1))
						&& "(TODO)".equals(row.get(2)) && "(TODO)".equals(row.get(3))) {
					return;
				}
				line++;
				if (row.size() != POIExcelUtil.HANDERS.length) {
					throw new RuntimeException("第" + (line + 1) + "行数据不完整,请检查后在重新上传!");
				}
				for (int index : POIExcelUtil.CHECK_CELL_INDEXES) {
					String cell = row.get(index);
					if (!StringUtils.hasText(cell)) {
						throw new RuntimeException((line + 2) + "行" + (index + 1) + "列数据不完整,请检查后在重新上传!");
					}
				}
				String classes = row.get(0).trim();
				String tkname = row.get(1).trim();
				String times = row.get(2).trim();
				String excuteusers = row.get(3).trim();
				String type = row.get(4).trim();

				// 对执行人格式进行检查
				String[] users = excuteusers.split(",");
				for (String strs : users) {
					// 判断"-"出现的次数
					String us = strs;
					int index1 = strs.indexOf("-");
					int num = 0;
					while (strs.contains("-")) {
						strs = strs.substring(index1 + "-".length());
						num++;
					}
					if (num != 1) {
						throw new RuntimeException("《" + excuteusers + "》执行人栏格式错误，请检查！");
					}
					String[] strings = us.split("-");
					Long userid = Long.valueOf(strings[0]);
					String username = strings[1];
					GeneralSqlComponent sql = SpringUtil.getBean(GeneralSqlComponent.class);
//					PwUser user = sql.query(SqlCode.getPwUserByID, new Object[] {userid});
					UrcUser user = sql.query(SqlCode.getPwUserByID, new Object[] {userid});
					if (user == null || !user.getUsername().equals(username)) {
						throw new RuntimeException("《" + excuteusers + "》执行人栏ID与姓名不匹配，请检查！");
					}
				}

				// 任务分类检查
				int index = classes.indexOf("-");
				Long classid = Long.parseLong(classes.substring(0, index));
				String classname = classes.substring(index + 1, classes.length());
				TkTaskClass tkTaskClass = tkTaskClassDao.get(session.getCompid(),classid,1);
				if (tkTaskClass == null) {
					throw new RuntimeException("《" + classname + "》不存在，请下载最新模板！");
				}
				if (!tkTaskClass.getClname().equals(classname)) {
					throw new RuntimeException("《" + classname + "》ID与分类名称不匹配，请检查！");
				}

				// 起始时间检查
				int indexs = times.indexOf(",");
				if (indexs == -1) {
					throw new RuntimeException("《" + times + "》起止时间栏格式错误，请检查！");
				}
				String starttime = times.substring(0, indexs);
				String endtime = times.substring(indexs + 1, times.length());
				if (starttime.length() != 10 || endtime.length() != 10) {
					throw new RuntimeException("《" + times + "》起止时间栏格式错误，请检查！");
				}
				boolean startflag = isDate(starttime);
				boolean endflag = isDate(endtime);
				if (!startflag || !endflag) {
					throw new RuntimeException("《" + times + "》起止时间栏格式错误，请检查！");
				}
				try {
					// 判断起止时间大小，并且不能超过两年
					Long start = DateEnum.YYYYMMDD.parse(starttime).getTime();
					Long end = DateEnum.YYYYMMDD.parse(endtime).getTime();
					if (start >= end) {
						throw new RuntimeException("《" + times + "》起止时间错误，开始时间必须小于结束时间！");
					} else {
						Long twoyear = 2 * 365 * 24 * 60 * 60 * 1000L;
						if (end - twoyear > start) {
							throw new RuntimeException("《" + times + "》起止时间错误，起止时间间隔必须小于两年！");
						}
					}
				} catch (ParseException e) {
					throw new RuntimeException("《" + times + "》起止时间栏格式错误，请检查！");
				}

				TkImportDeployList info = new TkImportDeployList();
				info.setCompid(session.getCompid());
				info.setImpid(importId);
				info.setSeqno(0);
				info.setClassid(classid);
				info.setClasses(classes);
				info.setTkname(tkname);
				info.setStarttime(starttime + " 00:00:00");
				info.setEndtime(endtime + " 23:59:59");
				info.setUsers(excuteusers);
				info.setTktpnm(type);
				info.setCkflag(0);
				info.setCkdesc(Constant.EMPTY);
				info.setRemarks1(0);
				info.setRemarks2(0);
				info.setRemarks3(Constant.EMPTY);
				info.setRemarks4(Constant.EMPTY);
				info.setRemarks5(Constant.EMPTY);
				info.setRemarks6(Constant.EMPTY);
				SpringUtil.getBean(TkImportDeployListService.class).insert(info);
		}
		firstFlag=true;
		}
		@Override
		public void doAfterAllAnalysed(AnalysisContext context) {
		}

		public int getLine() {
			return line;
		}
		
		/**
		 * 判断日期格式和范围
		 */
		public static boolean isDate(String date){
			
			String rexp = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";
			
			Pattern pat = Pattern.compile(rexp);  
			
			Matcher mat = pat.matcher(date);  
			
			boolean dateType = mat.matches();

			return dateType;
		}
		
	}

	
/**===================================================2019-8-5 2期新增接口==========================================================================*/

	
	/**
	 * 部署，普通任务，跨级任务
	 */
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
	public Long deployCommonTask(TkTaskDeployRequest request, Session session) throws Exception {
		logger.info("发布普通任务...");
		// 部署任务前先验证分类的class id在分类表里是否存在
		Long compid = session.getCompid();
		TkTaskClass taskClass = tkTaskClassDao.get(compid, request.getClassid(), 1);
		if (taskClass == null) {
			LehandException.throwException("任务分类不存在或无效");
		}
		List<Map> requireMents = getRequireMentMap(request, request.getTkname());

		String feedTimeJson = JSON.toJSON(requireMents).toString();
		// 将登录人的session信息存入request,方便审核人通过后保存其他信息时需要（发消息时需要创建人的session信息）
		request.setSession(session);
		TkTaskDeploy info = toTkDeploy(request, session, PERIOD_NO, RESOLVE_NO, Constant.STATUS_3);
		info.setRemarks6(feedTimeJson);
		info.setOrgcode(session.getCurrentOrg().getOrgcode());
		long id = tkTaskDeployDao.insertRetrunID(info);
		info.setId(id);
		// 执行人 跨级需要添加相关人
		insertTkTaskSubject(session, info, request);
		// 附件绑定
		saveOrUpdateFileBusiness(request, session, info);
		newAddAudit(request, session, info, id);
		String msg = "【智慧党建】: 您好,请及时完成" + request.getTkname() + "的反馈!";
		//获取对应的执行人的orgCode
		String excuteusers = request.getExcuteusers();
		JSONArray excuteuserArray = JSON.parseArray(excuteusers);
		for(Object obj : excuteuserArray){
			JSONObject dataObj = JSONObject.parseObject(obj.toString());
			String orgcode = dataObj.getString("orgcode");
			if (StringUtils.isEmpty(orgcode)) {
				continue;
			}
			Map<String,Object> user = generalSqlComponent.query(SqlCode.geTTDzzInfoBycode, new Object[]{orgcode});
			if (user == null || StringUtils.isEmpty(user.get("lxrphone"))) {
				continue;
			}
			SmsSendDto smsSendDto = new SmsSendDto();
			smsSendDto.setCompid(session.getCompid());
			smsSendDto.setUsermark(user.get("organid").toString());
			smsSendDto.setPhone(user.get("lxrphone").toString());
			smsSendDto.setContent(msg);
			smsSendDto.setBusinessid(Long.parseLong(Constant.PUBLISH_TASK));
			sendMessageService.attendMeetingSms(smsSendDto);
		}

		return info.getId();
	}
	
	public void sendMsg(Session session, int module, Long mid, String rulecode, String rulename, String mesg,
			Long subjectid, String subjectname, String remarks6, GeneralSqlComponent generalSqlComponent) {
		if (StringUtils.hasText(mesg) && (mesg.contains("请审核") || mesg.contains("及时签收"))) {
			TkTaskDeploy task = generalSqlComponent.query(SqlCode.getTTDByid,
					new Object[] { session.getCompid(), remarks6 });
			String replaceStr = "【" + task.getTkname() + "】，";
			mesg = mesg.replace("，", replaceStr);
		}
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(session.getCompid());
		ls.setNoteid(-1L);
		//模块(0:任务发布,1:任务反馈)
		ls.setModule(module);
		//对应模块的ID
		ls.setMid(mid);
		//提醒日期
		ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format());
		//编码
		ls.setRulecode(rulecode);
		ls.setRulename(rulename);
		ls.setOptuserid(session.getUserid());
		ls.setOptusername(session.getUsername());
		//被提醒人
		ls.setUserid(subjectid);
		ls.setUsername(subjectname);
		ls.setIsread(0);
		ls.setIshandle(0);
		//是否已提醒(0:未提醒,1:已提醒)
		ls.setIssend(0);
		//消息
		ls.setMesg(mesg);
		ls.setRemarks1(1);
		ls.setRemarks2(0);
		ls.setRemarks3("工作督查");
		ls.setRemarks4(Constant.EMPTY);
		ls.setRemarks5(Constant.EMPTY);
		ls.setRemarks6(remarks6);// 消息跳转，业务辅助id
		Long id = generalSqlComponent.insert(SqlCode.addTTRLByTuihui, ls);
		ls.setId(id);
	}
	
	public int getAuditType(Long compid, Long subjectid) {
		Integer remarks2 = generalSqlComponent.query(SqlCode.getAuditType, new Object[] {compid, subjectid});
//				tkTaskSubjectDao.getInt(
//				"select remarks2 from pw_role_map where compid=? and subjecttp=0 and subjectid=? and remarks2>0 limit 1",
//				compid, subjectid);
		return StringUtils.isEmpty(remarks2)?0:remarks2;
	}
	
	@SuppressWarnings("rawtypes")
	public void continueDeployTask(TkTaskDeployRequest request, Session session, List<Map> requireMents,
			TkTaskDeploy info) throws ParseException, Exception {
		tkTaskDeployDao.updateStatus(session.getCompid(), info.getId(), Constant.STATUS_30);
		
		//创建在执行人的待办信息 TODO
		for (Subject subject : request.getSubjects()) {
			insertMyToDoList(1, info.getId().toString(),"《"+info.getTkname()+"》", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), subject.getSubjectid().toString());

			//创建短信消息
			insertSms("您有一条新任务待办，任务名称："+request.getTkname(), getPhones(session.getCompid(), subject.getSubjectid()));

			TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);

			//审核通过发待办给执行人
			addTodoItem(Todo.ItemType.TYPE_DEPLOY, info.getTkname() +  "(" +taskClass.getClname() + ")", String.valueOf(info.getId()),  Todo.FunctionURL.MY_TASK.getPath(), subject.getSubjectid(), session);


		}
		
		// 保存完json之后，保存提醒时间点
		Set<String> times = insertTaskFeedTime2(requireMents, session, info);
		logger.debug("普通任务ID:{}", info.getId());
		insertTkTaskTimeNode(session.getCompid(), info.getId(), info.getStarttime(), info.getEndtime(), 0);
		// 提醒
		TaskProcessEnum.DEPLOY.handle(session, request, info, times);
	}
	
	@SuppressWarnings("rawtypes")
	public Set<String> insertTaskFeedTime2(List<Map> requireMents, Session session, TkTaskDeploy info)
			throws ParseException {
		Set<String> times = new HashSet<>(requireMents.size());
		// [{"backtime":"2019-01-13 12:12:12","mesg":"反馈时间点的要求","remindtime":"2019-01-12
		// 12:12:12,2019-01-14 12:12:12"},{},{}....]
		if (requireMents != null && requireMents.size() > 0) {
			for (Map requireMent : requireMents) {
				String feedtime = (String) requireMent.get("feedTime");// 反馈时间点
				String requirement = (String) requireMent.get("require");// 反馈要求
				String remindtimes = (String) requireMent.get("remindTimes");// 反馈时间点对应的提醒时间
				if (StringUtils.isEmpty(remindtimes)) {
					continue;
				}
				String[] strs = remindtimes.split(",");
				if (strs.length > 0) {// 不管是多个提醒时间还是一个时间都遍历下这里就不做判断了
					for (String str : strs) {
						insertTkTaskFeedTime(session, info.getId(), feedtime, requirement, str);
					}
				}
				times.add(feedtime);
			}
		}
		return times;
	}
	
	private TkTaskDeploy toTkDeploy(TkTaskDeployRequest request, Session session, int period, int resolve,
			int taskstatus) {
		TkTaskDeploy info = new TkTaskDeploy();
		String jsonrequest = JSON.toJSONString(request);
		info.setCompid(session.getCompid());
		info.setStarttime(request.getStarttime());// 起始时间
		info.setEndtime(request.getEndtime());// 终止时间
		info.setTkname(request.getTkname());// 任务名称
		info.setClassid(request.getClassid());// 任务分类ID(TK_TASK_CLASS表ID)
		info.setTkdesc(request.getTkdesc());// 任务备注
		info.setSrctaskid(request.getSrctaskid());// 源任务ID
		info.setAnnex(request.getAnnex());
		info.setPublisher(request.getPublisher());
		info.setTphone(request.getTphone());
		if (period == Constant.TkTaskDeploy.PERIOD_RESOLVE) {
			info.setDiffdata(request.getTimedataresolve());// 差异化数据（存储前台传来的时间数组对象）
			info.setRemarks6(request.getJson());// 表格数据
		} else {
			info.setDiffdata(request.getTimedata());// 差异化数据（存储前台传来的时间数组对象）
			// 表格json数据
			String remarks6 = StringUtils.isEmpty(request.getTabTimedata()) ? Constant.EMPTY : request.getTabTimedata();
			info.setRemarks6(remarks6);// 备注3
		}
		info.setExsjnum(request.getSubjects().size());// 执行主体数量
		info.setUserid(session.getUserid());// 创建人
		info.setUsername(session.getUsername());// 创建人名称
		info.setPeriod(period);// 是周期任务?(0:普通,1:周期，2分解)
		info.setResolve(request.getDeco());// 0不分解，1定性分解，2定量分解，3执行人分解
		info.setStatus(taskstatus);// 状态(0:删除,1:草稿,2:未创建,3:正在创建,30:未签收,40:已签收,45:暂停,50:完成)
		if (com.alibaba.druid.util.StringUtils.equals(request.getDraftFlag(), "1")) {
			info.setStatus(Constant.TkTaskDeploy.DRAFT_STATUS);// 草稿
		}
		info.setRemarks1(Constant.TkTaskDeploy.REMARKS1_NO);// 备注1（标注任务的修改状态0未修改 1已修改）
		info.setNeedbacktime(Constant.EMPTY);// 反馈时间点要求
		info.setProgress((double) 0);// 总进度值
		info.setNewbacktime(request.getNow());// 最新反馈时间
		info.setCreatetime(request.getNow());// 创建时间
		info.setUpdatetime(Constant.EMPTY);// 更新时间
		info.setDeletetime(Constant.EMPTY);// 删除时间
		info.setRemarks2(request.getCrossFlag());// 备注2 //跨级1，非跨级0
		info.setRemarks3(jsonrequest);// 备注3
		info.setRemarks4(Constant.EMPTY);// 备注3
		info.setRemarks5(Constant.EMPTY);// 备注3
        info.setFiles(request.getFiles());//附件
		return info;
	}
	
	//===========================================================任务相关短信发送代码=========================================================================
	
	/**
	 * a 新增短信验证码
	 * @Title：insertSms 
	 * @Description：TODO
	 * @param ：@param object 
	 * @return ：void 
	 * @throws
	 */
	@Transactional(rollbackFor=Exception.class)
	@Scope(value="prototype")
	public Message insertSms(String content,String phones) {
		Message msg = new Message();
		String id = UUID.randomUUID().toString().replaceAll("-", "");
		//发送短信
		String[] ph = phones.split(",");
		try {
			for (String phone : ph) {
//				smsComponent.sendSms(id, phone, content, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}
	
	
	/**
	 * Description: 根据短信接收人的id查询对应的手机号集合
	 * @author PT  
	 * @date 2019年11月12日 
	 * @param subjectid
	 * @return
	 */
	public String getPhones(Long compid,Long subjectid) {
		List<Map<String,Object>> lists = generalSqlComponent.query(SqlCode.listPhones, new Object[] {compid,subjectid});
		String phones = "";
		if(lists!=null && lists.size()>0) {
			for (Map<String, Object> map : lists) {
				phones += map.get("remarks3")+",";
			}
			if(!StringUtils.isEmpty(phones)) {
				phones = phones.substring(0,phones.length()-1);
			}
		}
		return phones;
	}


	/**
	 * 处理正副职审核关系
	 */
	public class MakeRoleAditRelation {
		private TkTaskDeployRequest request;
		private Long compid;
		private int i;
		private int j;

		public MakeRoleAditRelation(TkTaskDeployRequest request, Long compid, int i, int j) {
			this.request = request;
			this.compid = compid;
			this.i = i;
			this.j = j;
		}

		public int getI() {
			return i;
		}

		public int getJ() {
			return j;
		}

		public MakeRoleAditRelation invoke() {
			for (Subject subject : request.getSubjects3()) {
				/** 查询角色关系表 remarks2>0 的   1表示正职  2表示副职*/
				Long subjectid = subject.getSubjectid();
				int remarks2 = getAuditType(compid, subjectid);
				if(remarks2==2) {//副职
					i++;
				}else if(remarks2==1) {//正职
					j++;
				}
			}
			return this;
		}
	}




	/**待办任务begin****************************************************************************************************************/
	@Resource
	private TodoItemService todoItemService;

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param name 事项名称
	 * @param bizid 该事项对应的业务ID
	 * @param path 资源路径
	 * @param subjectId 资源路径
	 * @param session 当前会话
	 */
	private void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path, Long subjectId, Session session) {
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setBizid(bizid);
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setUserid(session.getUserid());
		itemDTO.setUsername(session.getUsername());
		itemDTO.setOrgid(session.getCurrorganid());
		itemDTO.setName(name);
		itemDTO.setBizid(bizid);
		itemDTO.setSubjectid(subjectId);
		itemDTO.setTime(new Date());
		itemDTO.setPath(path);
		itemDTO.setState(Todo.STATUS_NO_READ);
		todoItemService.createTodoItem(itemDTO);
	}

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param bizid 该事项对应的业务ID
	 * @param session 当前会话
	 */
	private void removeTodoItem(Todo.ItemType itemType,  String bizid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setBizid(bizid);
		todoItemService.deleteTodoItem(itemDTO);
	}
	/**待办任务end****************************************************************************************************************/
	/**
	 * 获取当前组织下所有组织编码
	 * @param organcode
	 * @return
	 */
	private void getOrgCode(String organcode,List<String> listOrgCode) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("organcode", organcode);
		List<Map<String,Object>> list  = generalSqlComponent.query(com.lehand.horn.partyorgan.constant.SqlCode.getTDzzInfoSimpleTree2, paramMap);
		for(Map<String,Object> li:list){
			listOrgCode.add(li.get("organcode").toString());
			getOrgCode(li.get("organcode").toString(),listOrgCode);
		}
	}
	public List<Map<String, Object>> taskStatistical(Session session) {
		String orgcode = session.getCurrentOrg().getOrgcode();
		List<String> listOrgCode = new ArrayList<>();
		//获取当前组织编码及其下所有组织编码
		listOrgCode.add(orgcode);
		getOrgCode(orgcode,listOrgCode);
		Map<String, Object> paramMap = new HashMap<>();
//		paramMap.put("orgcode", org.apache.commons.lang.StringUtils.strip(listOrgCode.toString(),"[]"));//统计当前组织及以下
        paramMap.put("orgcode","");//统计所有
		paramMap.put("group", 1);
		List<Map<String, Object>>  listMap= generalSqlComponent.query(SqlCode.listTaskIdByCodes, paramMap);

		for (Map<String, Object> map: listMap) {
			String clname= generalSqlComponent.getDbComponent().getSingleColumn("SELECT clname FROM  `tk_task_class` where id=? ", String.class,new Object[]{map.get("classid")});
			paramMap.put("classid",map.get("classid"));
			paramMap.put("status",50);
			List<Map<String, Object>>  successList= generalSqlComponent.query(SqlCode.listTaskIdByCodes, paramMap);
			if(successList!=null&&successList.size()>0){
				map.put("success",successList.get(0).get("count")!=null?successList.get(0).get("count"):0);
			}else {
				map.put("success",0);
			}
			map.put("name",clname);
		}
		return listMap;
	}
}
