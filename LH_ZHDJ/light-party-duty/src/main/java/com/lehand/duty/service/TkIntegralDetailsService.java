package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.TkIntegralRespond;
import com.lehand.duty.dto.TkIntegralResponse;
import com.lehand.duty.pojo.TkIntegralDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TkIntegralDetailsService {

	@Resource
    TkPraiseDetailsService tkPraiseDetailsDao;

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	/**
	 * 工作台我的积分
	 */
	public TkIntegralRespond getMyScore(Long compid, Long userid) {
		TkIntegralRespond info = new TkIntegralRespond();
		//总积分
		TkIntegralDetails sumscore = getSumscore(compid,userid);
		info.setSumscore(sumscore.getScore());
		//获得的积分
		TkIntegralDetails addscore = getAddscore(compid,userid);
		info.setAddscore(addscore.getScore());
		//扣除的积分
		TkIntegralDetails deductscore = getDeductscore(compid,userid);
		info.setDeductscore(deductscore.getScore());
		//当前年份
		String year = DateEnum.YYYYMMDD.format().substring(0,4);
		info.setYear(year);
		String starttime = year+"-01-01 00:00:00";
		String endtime = year+"-12-31 23:59:59";
		//当前年份得分
		TkIntegralDetails yearaddscore = getYearaddscore(compid,userid,starttime,endtime);
		info.setYearaddscore(yearaddscore.getScore());
		//当前年份扣分
		TkIntegralDetails yeardeductscore = getYeardeductscore(compid,userid,starttime,endtime);
		info.setYeardedcutscore(yeardeductscore.getScore());
		//当前年份总积分
		TkIntegralDetails yearsumscore = getYearsumscore(compid,userid,starttime,endtime);
		//积分排名
		List<TkIntegralDetails> lists = getScore(compid,userid, starttime, endtime);
		int scorerank = 1;
		if(lists!=null && lists.size()>0) {
			for (TkIntegralDetails list : lists) {
				if(list.getScore()>yearsumscore.getScore()) {
					scorerank++;
				}
			}
		}
		info.setScorerank(scorerank);
		//总排名人数
		int ranknums = getRanknums(compid,starttime,endtime);
		info.setRanknums(ranknums);
		//总点赞次数
		int praisecount = tkPraiseDetailsDao.getSum(compid,userid);
		info.setPraisecount(praisecount);
		return info;
	}
	
	/**
	 * 总积分排名
	 * @return
	 */
	public List<TkIntegralDetails> getListScore(Long compid,Pager pager){
		String year = DateEnum.YYYYMMDD.format().substring(0,4);
		String starttime = year+"-01-01 00:00:00";
		String endtime = DateEnum.YYYYMMDDHHMMDD.format();
		return getListScore(compid,starttime, endtime,pager);
	}
	
	public int getListScore(Long compid){
		String year = DateEnum.YYYYMMDD.format().substring(0,4);
		String starttime = year+"-01-01 00:00:00";
		String endtime = DateEnum.YYYYMMDDHHMMDD.format();
		List<TkIntegralDetails> list = getListScore(compid,starttime, endtime);
		if(list!=null && list.size()>0) {
			return list.size();
		}
		return 0;
	}


    public List<TkIntegralResponse> pageList(Long compid, String starttime, String endtime , Pager pager) {
        Pager page = generalSqlComponent.pageQuery(SqlCode.integralDetailslists2, new Object[] {compid,starttime,endtime},pager);
        PagerUtils<TkIntegralResponse> pagerUtils = new PagerUtils<>(page);
        return pagerUtils.getRows();
    }

    public List<TkIntegralDetails> list(Long compid, String starttime, String endtime) {
        return generalSqlComponent.query(SqlCode.integralDetailslists, new Object[] {compid,starttime,endtime});
    }

    public TkIntegralDetails insert(Long compid,Long userid,String username,String rulecode,Long taskid,Long mytaskid,Double score,Long feedid) {
        TkIntegralDetails tkIntegralDetails = new TkIntegralDetails();
        tkIntegralDetails.setCompid(compid);
        tkIntegralDetails.setUserid(userid);
        tkIntegralDetails.setRulecode(rulecode);
        tkIntegralDetails.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
        tkIntegralDetails.setScore(score);
        tkIntegralDetails.setUsername(username);
        tkIntegralDetails.setTaskid(taskid);
        tkIntegralDetails.setMytaskid(mytaskid);
        tkIntegralDetails.setRemarks1(feedid);//作为点赞表中的feedid
        tkIntegralDetails.setRemarks2(Constant.EMPTY);
        tkIntegralDetails.setRemarks3(Constant.EMPTY);
        Long id = generalSqlComponent.insert(SqlCode.integralDetailInsert, tkIntegralDetails);
        tkIntegralDetails.setId(id);
        return tkIntegralDetails;
    }

    public TkIntegralDetails get(Long compid,String mintime,String maxtime,Long mytaskid,String code) {
        return generalSqlComponent.query(SqlCode.integralDetailsGet, new Object[] {compid,mytaskid,code,mintime,maxtime});
    }

    public int count(Long compid,Long mytaskid,String endtime,String code) {
        return generalSqlComponent.query(SqlCode.getIntegralDetailsCount, new Object[] {compid,mytaskid,endtime,code});
    }

    /**
     * 获取总任务积分
     * @author pantao
     * @date 2018年11月30日下午3:28:09
     *
     * @param taskid
     * @return
     */
    public TkIntegralDetails byTaskId(Long compid,Long taskid) {
        return generalSqlComponent.query(SqlCode.getIntegralDetailsScore, new Object[] {compid,taskid});
    }

    /**
     * 删除任务总积分
     * @param taskid
     */
    public void deleteByTaskId(Long compid,Long taskid) {
        generalSqlComponent.delete(SqlCode.delintegralDetailsByTid, new Object[] {compid,taskid});
    }

    /**
     * 删除某任务下某个执行人获得的积分
     * @param taskid
     * @param userid
     */
    public void deleteByTaskIdAndUserid(Long compid,Long taskid, Long userid) {
        generalSqlComponent.delete(SqlCode.delintegralDetailsByTU, new Object[] {compid,taskid,userid});
    }

    public void deletePraise(Long compid,Long feedid) {
        generalSqlComponent.delete(SqlCode.delintegralDetailsByRek1, new Object[] {compid,feedid});
    }

    //我的总积分
    public TkIntegralDetails getSumscore(Long compid,Long userid) {
        return generalSqlComponent.query(SqlCode.getintegralDetailsTotal, new Object[] {compid,userid});
    }
    //我获得的积分
    public TkIntegralDetails getAddscore(Long compid,Long userid) {
        return generalSqlComponent.query(SqlCode.getidByMScore, new Object[] {compid,userid});
    }
    //我被扣除的积分
    public TkIntegralDetails getDeductscore(Long compid,Long userid) {
        return generalSqlComponent.query(SqlCode.getidByMinScore, new Object[] {compid,userid});
    }
    //该年度获得的积分
    public TkIntegralDetails getYearaddscore(Long compid,Long userid,String starttime,String endtime) {
        return generalSqlComponent.query(SqlCode.getidByYearScore, new Object[] {compid,userid,starttime,endtime});
    }
    //该年度扣除的积分
    public TkIntegralDetails getYeardeductscore(Long compid,Long userid,String starttime,String endtime) {
        return generalSqlComponent.query(SqlCode.getidByYearminus, new Object[] {compid,userid,starttime,endtime});
    }
    //该年度参与排名的总人数
    public int getRanknums(Long compid,String starttime,String endtime) {
        return generalSqlComponent.query(SqlCode.getRankPNumYear, new Object[] {compid,starttime,endtime});
    }
    //该年度我的总积分
    public TkIntegralDetails getYearsumscore(Long compid,Long userid,String starttime,String endtime) {
        return generalSqlComponent.query(SqlCode.getIDYearsumscore, new Object[] {compid, userid,starttime,endtime});
    }
    //该年度参与排名的人的总积分集合(排除自己)
    public List<TkIntegralDetails> getScore(Long compid,Long userid,String starttime,String endtime){
        return generalSqlComponent.query(SqlCode.getIDYearsscore, new Object[] {compid,starttime,endtime,userid});
    }
    //当前年份总积分排名
    public List<TkIntegralDetails> getListScore(Long compid,String starttime,String endtime){
        return generalSqlComponent.query(SqlCode.getIDYearListscore, new Object[] {compid,starttime,endtime});
    }

    public List<TkIntegralDetails> getListScore(Long compid,String starttime,String endtime,Pager pager){
        Pager page = generalSqlComponent.pageQuery(SqlCode.pageIDYearListscore, new Object[] {compid,starttime,endtime}, pager);
        PagerUtils<TkIntegralDetails> pagerUtils = new PagerUtils<TkIntegralDetails>(page);
        return pagerUtils.getRows();
    }
}
