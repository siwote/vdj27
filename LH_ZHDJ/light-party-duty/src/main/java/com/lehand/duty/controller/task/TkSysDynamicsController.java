package com.lehand.duty.controller.task;

import javax.annotation.Resource;

import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkSysDynamicsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.duty.controller.DutyBaseController;

@Api(value = "动态任务", tags = { "动态任务" })
@RestController
@RequestMapping("/duty/dynamics")
public class TkSysDynamicsController extends DutyBaseController {

	private static final Logger logger = LogManager.getLogger(TkSysDynamicsController.class);

	@Resource
	private TkSysDynamicsService tkSysDynamicsService;

	@OpenApi(optflag=0)
	@ApiOperation(value = "分页查询", notes = "分页查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(Pager pager) {
		Message message = new Message();
		try {
			tkSysDynamicsService.page(getSession(), pager);
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		}
		return message;
	}
}
