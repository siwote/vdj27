package com.lehand.duty.dto;

public class TkAttachmentResponse {

	
	private Integer model;
	private Long id;
	private String name;
	private String path;
	private Long size;
	private String suffix;
	private Integer remarks1;
	private Integer remarks2;
	private String remarks3;
	private String remarks4;
	private String remarks5;
	private String remarks6;
	public Integer getModel() {
		return model;
	}
	public void setModel(Integer model) {
		this.model = model;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public Integer getRemarks1() {
		return remarks1;
	}
	public void setRemarks1(Integer remarks1) {
		this.remarks1 = remarks1;
	}
	public Integer getRemarks2() {
		return remarks2;
	}
	public void setRemarks2(Integer remarks2) {
		this.remarks2 = remarks2;
	}
	public String getRemarks3() {
		return remarks3;
	}
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3;
	}
	public String getRemarks4() {
		return remarks4;
	}
	public void setRemarks4(String remarks4) {
		this.remarks4 = remarks4;
	}
	public String getRemarks5() {
		return remarks5;
	}
	public void setRemarks5(String remarks5) {
		this.remarks5 = remarks5;
	}
	public String getRemarks6() {
		return remarks6;
	}
	public void setRemarks6(String remarks6) {
		this.remarks6 = remarks6;
	}

	

}