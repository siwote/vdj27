package com.lehand.duty.dto;

import com.lehand.base.exception.LehandException;
import com.lehand.components.web.pipeline.Request;

public class CommentReplyRequest extends Request {

	private Long commentid;
	
	/**
	 * 回复类型(0:文本,1:语音)
	 */
	private Integer replyflag;
	
	/**
	 * 回复内容
	 */
	private String replytext;
	
	public Long getCommentid() {
		return commentid;
	}

	public void setCommentid(Long commentid) {
		this.commentid = commentid;
	}

	public Integer getReplyflag() {
		return replyflag;
	}

	public void setReplyflag(Integer replyflag) {
		this.replyflag = replyflag;
	}

	public String getReplytext() {
		return replytext;
	}

	public void setReplytext(String replytext) {
		this.replytext = replytext;
	}

	@Override
	public void validate() throws LehandException {
		
	}
}
