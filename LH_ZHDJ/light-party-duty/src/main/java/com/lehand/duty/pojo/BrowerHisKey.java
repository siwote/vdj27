package com.lehand.duty.pojo;

public class BrowerHisKey  extends FatherPojo {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column brower_his.model
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    private Integer model;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column brower_his.modelid
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    private Long modelid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column brower_his.createtime
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    private String createtime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column brower_his.userid
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    private Long userid;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column brower_his.model
     *
     * @return the value of brower_his.model
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public Integer getModel() {
        return model;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column brower_his.model
     *
     * @param model the value for brower_his.model
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public void setModel(Integer model) {
        this.model = model;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column brower_his.modelid
     *
     * @return the value of brower_his.modelid
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public Long getModelid() {
        return modelid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column brower_his.modelid
     *
     * @param modelid the value for brower_his.modelid
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public void setModelid(Long modelid) {
        this.modelid = modelid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column brower_his.createtime
     *
     * @return the value of brower_his.createtime
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column brower_his.createtime
     *
     * @param createtime the value for brower_his.createtime
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column brower_his.userid
     *
     * @return the value of brower_his.userid
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public Long getUserid() {
        return userid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column brower_his.userid
     *
     * @param userid the value for brower_his.userid
     *
     * @mbg.generated Tue Nov 20 10:29:35 CST 2018
     */
    public void setUserid(Long userid) {
        this.userid = userid;
    }
}