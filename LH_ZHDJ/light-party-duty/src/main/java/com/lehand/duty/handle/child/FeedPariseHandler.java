package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class FeedPariseHandler extends BaseHandler {

	@Resource
	private GetIntegerHandler getIntegerHandler;
	
	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		TkMyTaskLog tkMyTaskLog = data.getParam(0);
		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTaskLog.getCompid(),tkMyTaskLog.getTaskid());
		TkMyTask tkMyTask = tkMyTaskDao.get(tkMyTaskLog.getCompid(),tkMyTaskLog.getMytaskid());
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTaskLog.getCompid(),tkMyTaskLog.getTaskid());
		data.setDeploySub(deploySub);
		data.setMyTask(tkMyTask);
		data.setMyTaskLog(tkMyTaskLog);
		data.setDeploy(deploy);
		
		//process
		TkIntegralRule tkIntegralRule = tkIntegralRuleService.get(tkMyTask.getCompid(),"FEEDPARISE");
		Double inteval = tkIntegralRule.getInteval();
		//向点赞详情表中插入数据
		tkPraiseDetailsService.insert(data.getDeploy().getCompid(),data.getDeploy().getId(),
				data.getMyTask().getId(), 
				data.getMyTaskLog().getId(), 
				data.getMyTask().getSubjectid(), 
				data.getMyTask().getSubjectname(), 
				data.getSession().getUserid(), 
				data.getSession().getUsername(), 
				TaskProcessEnum.FEED_PARISE.toString(),
				inteval);
		//向点赞表插入数据的同时向积分详情表插入一条数据（反馈id存入remarks3中）
		tkIntegralDetailsService.insert(tkMyTask.getCompid(),data.getMyTaskSubject().getSubjectid(),
				data.getMyTaskSubject().getSubjectname(), 
				TaskProcessEnum.FEED_PARISE.toString(),
				data.getDeploy().getId(), 
				data.getMyTask().getId(), 
				inteval,
				data.getMyTaskLog().getId());
		data.putRemindParam("inteval", inteval);
		getIntegerHandler.process(processEnum,data.getSession(),inteval,2.0,1.0,tkMyTask);
		
		//remind
		TkRemindRule rr = dutyCacheComponent.getTkRemindRule(data.getDeploy().getCompid(), TaskProcessEnum.FEED_PARISE.toString());
		if (rr.isOpen()) {
			//提醒执行人
			Map<String, Object> remindParams = data.getRemindParams();
			remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), tkMyTaskLog.getId(), 
					                     processEnum.getRemindRule(), DateEnum.YYYYMMDDHHMMDD.format(),
					                     data.getSessionSubject(),  data.getMyTaskSubject(),remindParams);
		}
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		TkMyTaskLog tkMyTaskLog = data.getParam(0);
//		TkTaskDeploy deploy = tkTaskDeployDao.get(tkMyTaskLog.getCompid(),tkMyTaskLog.getTaskid());
//		TkMyTask tkMyTask = tkMyTaskDao.get(tkMyTaskLog.getCompid(),tkMyTaskLog.getMytaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(tkMyTaskLog.getCompid(),tkMyTaskLog.getTaskid());
//		data.setDeploySub(deploySub);
//		data.setMyTask(tkMyTask);
//		data.setMyTaskLog(tkMyTaskLog);
//		data.setDeploy(deploy);
//	}
//	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		TkMyTask tkMyTask = data.getMyTask();
//		TkIntegralRule  tkIntegralRule = tkIntegralRuleDao.get(tkMyTask.getCompid(),"FEEDPARISE");
//		Double inteval = tkIntegralRule.getInteval();
//		//向点赞详情表中插入数据
//		tkPraiseDetailsDao.insert(data.getDeploy().getCompid(),data.getDeploy().getId(), 
//				data.getMyTask().getId(), 
//				data.getMyTaskLog().getId(), 
//				data.getMyTask().getSubjectid(), 
//				data.getMyTask().getSubjectname(), 
//				data.getSession().getUserid(), 
//				data.getSession().getUsername(), 
//				TaskProcessEnum.FEED_PARISE.toString(),
//				inteval);
//		//向点赞表插入数据的同时向积分详情表插入一条数据（反馈id存入remarks3中）
//		tkIntegralDetailsDao.insert(tkMyTask.getCompid(),data.getMyTaskSubject().getSubjectid(),
//				data.getMyTaskSubject().getSubjectname(), 
//				TaskProcessEnum.FEED_PARISE.toString(),
//				data.getDeploy().getId(), 
//				data.getMyTask().getId(), 
//				inteval,
//				data.getMyTaskLog().getId());
//		data.putRemindParam("inteval", inteval);
//		GET_INTEGER.handle(data.getSession(),inteval,2.0,1.0,tkMyTask);
//	}
//	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		TkRemindRule rr = dutyCacheComponent.getTkRemindRule(data.getDeploy().getCompid(),TaskProcessEnum.FEED_PARISE.toString());
//		if (rr.isOpen()) {
//			TkMyTaskLog tkMyTaskLog = data.getMyTaskLog();
//			//提醒执行人
//			Map<String, Object> remindParams = data.getRemindParams();
//			remindNoteComponent.register(data.getDeploy().getCompid(),module, tkMyTaskLog.getId(), remindRule, DateEnum.YYYYMMDDHHMMDD.format(),
//					data.getSessionSubject(),  data.getMyTaskSubject(),remindParams);
//		}
//	}
//	
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}
}
