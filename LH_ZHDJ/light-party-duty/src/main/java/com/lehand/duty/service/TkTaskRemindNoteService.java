package com.lehand.duty.service;

import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.pojo.TkTaskRemindNote;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Repository
@Service
public class TkTaskRemindNoteService {

	@Resource
	protected GeneralSqlComponent generalSqlComponent;

//	private static final String INSERT = "INSERT INTO tk_task_remind_note (compid, id, module, mid, remindtime, rulecode, optuser, noticeusers, issend, mesg, remarks1, remarks2, remarks3, remarks4, remarks5, remarks6) VALUES (:compid,:id,:module,:mid,:remindtime,:rulecode,:optuser,:noticeusers,:issend,:mesg,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6)";
	private static final String selectRemindByTaskId = "SELECT * FROM tk_task_remind_note WHERE compid=? and mid=? and module = 0 order by remindtime asc";
	private static final String findNeedSendRemind = "SELECT * FROM tk_task_remind_note WHERE issend=? and remindtime<=? and rulecode != 'DEPLOY_TASK' and remarks3 != 'suspend' and remarks3 != 'complete'";
//	private static final String deleteRemindNote = "delete from tk_task_remind_note where module=? and id=? ";
//	private static final String UPDATE_ISSEND_BY_ID = "UPDATE tk_task_remind_note SET issend=? WHERE compid=? and id=?";

	/**
	 * 更新提醒登记簿提醒状态
	 * @param compid
	 * @param issend
	 * @param id
	 * @return
	 */
	public int update(Long compid,int issend,Long id) {

//		return super.update(UPDATE_ISSEND_BY_ID, issend,compid, id);
		return generalSqlComponent.update(SqlCode.updateIssendById,new Object[]{issend,compid, id});
	}

	/**
	 * 新增提醒登记簿
	 * @param info
	 * @return
	 */
	public int insert(TkTaskRemindNote info) {
//		return super.insert(INSERT, info);
		return generalSqlComponent.update(SqlCode.insertRemindNote,info);
	}
	
	public TkTaskRemindNote get(Long compid,Long id) {
//		return super.get("SELECT * FROM tk_task_remind_note where compid=? and id=?", TkTaskRemindNote.class, compid,id);
		return generalSqlComponent.query(SqlCode.getRemindNoteById,new Object[]{compid,id});
	}
	
	/**
	 * 根据是否发送和提醒时间查询
	 * @param issend
	 * @param remindtime
	 * @return
	 */
	public List<TkTaskRemindNote> findNeedSendRemind( int issend ,String remindtime){
//		return super.list2bean(findNeedSendRemind,TkTaskRemindNote.class,issend,remindtime);
		return generalSqlComponent.query(SqlCode.listNeedSendRemindNote,new Object[]{issend,remindtime});
	}
	
	/**
	 * 
	 * 通过主表id查询任务提醒表相关数据
	 * @author pantao
	 * @date 2018年11月1日上午11:56:39
	 * 
	 * @param taskid
	 * @return
	 */
	public List<TkTaskRemindNote> listRemindByTaskId(Long compid,Long taskid){
//		return super.list2bean(selectRemindByTaskId,TkTaskRemindNote.class,compid,taskid);
		return generalSqlComponent.query(SqlCode.listRemindNoteByTaskId,new Object[]{compid,taskid});
	}
	
	/**
	 * 查询普通任务的时间节点集合（专门用于过了提醒时间而没有反馈的批处理）
	 * @return
	 */
	public List<TkTaskRemindNote> listOverRemind(){
//		return super.list2bean("SELECT * FROM tk_task_remind_note where module=0 and remindtime<? and remarks1!=1 and remarks3!='suspend' and remarks3 != 'complete'", TkTaskRemindNote.class,DateEnum.YYYYMMDDHHMMDD.format());
		return generalSqlComponent.query(SqlCode.listOverRemindNote,new Object[]{DateEnum.YYYYMMDDHHMMDD.format()});
	}
	
	/**
	 * 查询普通任务的时间节点集合（专门用于过了提醒时间而没有反馈的批处理）
	 * @return
	 */
	public TkTaskRemindNote getMinRemind(Long compid,Long taskid){
//		String sql = "SELECT remindtime FROM tk_task_remind_note where compid=? and mid=? and module=0 and remindtime<? order by remindtime desc limit 0,1";
//		return super.get(sql, TkTaskRemindNote.class, compid,taskid,DateEnum.YYYYMMDDHHMMDD.format());
		return generalSqlComponent.query(SqlCode.getMinRemindByTaskid,new Object[]{compid,taskid,DateEnum.YYYYMMDDHHMMDD.format()});
	}
	
	/**
	 * 更新扫描标识（remarks1字段）任务即将截止
	 * @param id
	 */
	public void updateRemarks1(Long compid,Long id) {
//		super.update("UPDATE tk_task_remind_note SET issend=1 , remarks1 = 1  WHERE compid=? and id = ?", compid,id);
		generalSqlComponent.update(SqlCode.updateRemarks1ById,new Object[]{compid, id});
	}
	
	/**
	 * 更新扫描标识（remarks2字段）即将到达反馈提醒点
	 * @param id
	 */
	public void updateRemarks2(Long compid,Long id) {
//		super.update("UPDATE tk_task_remind_note SET issend=1 , remarks2 = 1  WHERE compid=? and id = ?", compid,id);

		generalSqlComponent.update(SqlCode.updateRemarks1ById,new Object[]{compid, id});
	}
//	
//	/**
//	 * 删除提醒登记簿
//	 * @param module
//	 * @param id
//	 * @param ruleEnum
//	 * @throws LehandException
//	 */
//	public int deleteRemindNote(Module module,Long id,RemindRuleEnum remindRuleEnum) throws LehandException {
//		return super.delete(deleteRemindNote, module.getValue(),id);
//	}

	/**
	 * 批量操作获取提醒时间节点集合
	 * @return
	 */
	public List<TkTaskRemindNote> list() {
//		return super.list2bean("SELECT remindtime,mid,id FROM tk_task_remind_note where module=0 and remarks2!=1 and remarks3!= 'suspend' and remarks3 != 'complete' order by remindtime ", TkTaskRemindNote.class);
		return generalSqlComponent.query(SqlCode.listTkTaskRemindNote,new Object[]{});
	}
	
	/**
	 * 发送消息查询下次反馈提醒时间
	 * @param format
	 * @return
	 */
	public TkTaskRemindNote getNextTime(Long compid,Long mid,String format) {
//		return super.get("SELECT remindtime FROM tk_task_remind_note where compid=? and  mid=? and module=0  and remindtime >? and remarks2!=1  order by remindtime limit 0,1", TkTaskRemindNote.class, compid,mid,format);
		return generalSqlComponent.query(SqlCode.getNextRemindNote,new Object[]{compid,mid,format});
	}

	/**
	 * 按任务删除提醒节点
	 * @param compid
	 * @param mid
	 * @param module
	 */
	public void deleteByTaskid(Long compid,Long mid, int module) {
//		super.delete("delete from tk_task_remind_note where compid=? and mid = ? and module = ?", compid,mid,module);
		generalSqlComponent.delete(SqlCode.deleteByTaskidAndModule,new Object[]{compid,mid,module});
	}

	/**
	 * 更新remark3字段(真心不知道干什么用的 ,问潘涛)
	 * @param compid
	 * @param taskid
	 * @param str
	 */
	public void updateRemarks3(Long compid,Long taskid,String str) {
//		super.update(" UPDATE tk_task_remind_note SET remarks3 = ? WHERE compid=? and module=0 and mid = ?",str, compid,taskid);
		generalSqlComponent.update(SqlCode.updateRemindNoteRemarks3,new Object[]{str, compid,taskid});

	}
}
