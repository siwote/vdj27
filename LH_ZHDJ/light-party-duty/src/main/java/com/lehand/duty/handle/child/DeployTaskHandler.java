package com.lehand.duty.handle.child;

import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.dto.TkTaskDeployRequest;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.TkTaskDeploy;
import com.lehand.duty.pojo.TkTaskDeploySub;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class DeployTaskHandler extends BaseHandler {
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception {
		TkTaskDeployRequest request = (TkTaskDeployRequest) data.getParam(0);
		data.setDeploy((TkTaskDeploy) data.getParam(1));
		Set<String> times = (Set<String>)data.getParam(2);
		Session session = data.getSession();
		
		TkTaskDeploy deploy = data.getDeploy();
		TkTaskDeploySub map = tkTaskDeploySubDao.get(deploy.getCompid(), deploy.getId());
		if(map==null) {
			tkTaskDeploySubDao.insert(processEnum.getName(), session, request, times, deploy);
		}
		
		if(deploy.getPeriod().equals(Constant.TkTaskDeploy.PERIOD_YES)) {
			return;
		}
		
		Set<String> time = (Set<String>) data.getParam(2);
		remindNoteComponent.register(deploy.getCompid(),processEnum.getModule(), deploy.getId(), processEnum.getRemindRule(), 
				                     time, data.getSessionSubject(), 
                                     request.getSubjects());
	}
	
//	protected void init(TaskProcessData data) throws LehandException {
//		data.setDeploy((TkTaskDeploy) data.getParam(1));
//	}
//
//	@SuppressWarnings("unchecked")
//	protected void process(TaskProcessData data) throws LehandException {
//		Session session = data.getSession();
//		TkTaskDeployRequest request = (TkTaskDeployRequest)data.getParam(0);
//		Set<String> times = (Set<String>)data.getParam(2);
//		TkTaskDeploy deploy = data.getDeploy();
//		tkTaskDeploySubDao.insert(this.name, session, request, times, deploy);
//	}
//
//	protected void detail(TaskProcessData data) throws LehandException {
//	}
//	
//	@SuppressWarnings("unchecked")
//	protected void remind(TaskProcessData data) throws LehandException {
//		TkTaskDeploy deploy = data.getDeploy();
//		if(deploy.getPeriod().equals(Constant.TkTaskDeploy.PERIOD_YES)) {
//			return;
//		}
//		Set<String> time = (Set<String>) data.getParam(2);
//		TkTaskDeployRequest request = (TkTaskDeployRequest) data.getParam(0);
////		messageComponent.
//		remindNoteComponent.register(deploy.getCompid(),module, deploy.getId(), remindRule, 
//				                     time, data.getSessionSubject(), 
//                                     request.getSubjects());
//	}
//
//	protected void setTaskDeploySub(TaskProcessData data) {
//	}

}
