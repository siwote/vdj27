package com.lehand.duty.pojo;

public class TkTaskFeedBackAudit {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.id
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.compid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Integer compid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.taskid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Long taskid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.mytaskid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Long mytaskid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.feedbackid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Long feedbackid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.subjectId
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Long subjectId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.subtype
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Integer subtype;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.status
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Integer status;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.remark
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String remark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.createtime
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String createtime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.opttime
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String opttime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.remarks2
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private Integer remarks2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.remarks3
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String remarks3;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.remarks4
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String remarks4;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.remarks5
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String remarks5;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tk_task_feed_back_audit.remarks6
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    private String remarks6;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.id
     *
     * @return the value of tk_task_feed_back_audit.id
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.id
     *
     * @param id the value for tk_task_feed_back_audit.id
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.compid
     *
     * @return the value of tk_task_feed_back_audit.compid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Integer getCompid() {
        return compid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.compid
     *
     * @param compid the value for tk_task_feed_back_audit.compid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setCompid(Integer compid) {
        this.compid = compid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.taskid
     *
     * @return the value of tk_task_feed_back_audit.taskid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Long getTaskid() {
        return taskid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.taskid
     *
     * @param taskid the value for tk_task_feed_back_audit.taskid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setTaskid(Long taskid) {
        this.taskid = taskid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.mytaskid
     *
     * @return the value of tk_task_feed_back_audit.mytaskid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Long getMytaskid() {
        return mytaskid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.mytaskid
     *
     * @param mytaskid the value for tk_task_feed_back_audit.mytaskid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setMytaskid(Long mytaskid) {
        this.mytaskid = mytaskid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.feedbackid
     *
     * @return the value of tk_task_feed_back_audit.feedbackid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Long getFeedbackid() {
        return feedbackid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.feedbackid
     *
     * @param feedbackid the value for tk_task_feed_back_audit.feedbackid
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setFeedbackid(Long feedbackid) {
        this.feedbackid = feedbackid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.subjectId
     *
     * @return the value of tk_task_feed_back_audit.subjectId
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Long getSubjectId() {
        return subjectId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.subjectId
     *
     * @param subjectid the value for tk_task_feed_back_audit.subjectId
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.subtype
     *
     * @return the value of tk_task_feed_back_audit.subtype
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Integer getSubtype() {
        return subtype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.subtype
     *
     * @param subtype the value for tk_task_feed_back_audit.subtype
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setSubtype(Integer subtype) {
        this.subtype = subtype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.status
     *
     * @return the value of tk_task_feed_back_audit.status
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.status
     *
     * @param status the value for tk_task_feed_back_audit.status
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.remark
     *
     * @return the value of tk_task_feed_back_audit.remark
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.remark
     *
     * @param remark the value for tk_task_feed_back_audit.remark
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.createtime
     *
     * @return the value of tk_task_feed_back_audit.createtime
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.createtime
     *
     * @param createtime the value for tk_task_feed_back_audit.createtime
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.opttime
     *
     * @return the value of tk_task_feed_back_audit.opttime
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getOpttime() {
        return opttime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.opttime
     *
     * @param opttime the value for tk_task_feed_back_audit.opttime
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setOpttime(String opttime) {
        this.opttime = opttime == null ? null : opttime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.remarks2
     *
     * @return the value of tk_task_feed_back_audit.remarks2
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public Integer getRemarks2() {
        return remarks2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.remarks2
     *
     * @param remarks2 the value for tk_task_feed_back_audit.remarks2
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setRemarks2(Integer remarks2) {
        this.remarks2 = remarks2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.remarks3
     *
     * @return the value of tk_task_feed_back_audit.remarks3
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getRemarks3() {
        return remarks3;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.remarks3
     *
     * @param remarks3 the value for tk_task_feed_back_audit.remarks3
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3 == null ? null : remarks3.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.remarks4
     *
     * @return the value of tk_task_feed_back_audit.remarks4
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getRemarks4() {
        return remarks4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.remarks4
     *
     * @param remarks4 the value for tk_task_feed_back_audit.remarks4
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4 == null ? null : remarks4.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.remarks5
     *
     * @return the value of tk_task_feed_back_audit.remarks5
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getRemarks5() {
        return remarks5;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.remarks5
     *
     * @param remarks5 the value for tk_task_feed_back_audit.remarks5
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setRemarks5(String remarks5) {
        this.remarks5 = remarks5 == null ? null : remarks5.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tk_task_feed_back_audit.remarks6
     *
     * @return the value of tk_task_feed_back_audit.remarks6
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public String getRemarks6() {
        return remarks6;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tk_task_feed_back_audit.remarks6
     *
     * @param remarks6 the value for tk_task_feed_back_audit.remarks6
     *
     * @mbg.generated Tue Apr 09 14:05:29 CST 2019
     */
    public void setRemarks6(String remarks6) {
        this.remarks6 = remarks6 == null ? null : remarks6.trim();
    }
}