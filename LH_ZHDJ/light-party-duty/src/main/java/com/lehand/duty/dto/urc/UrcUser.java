package com.lehand.duty.dto.urc;

import java.io.Serializable;

public class UrcUser implements Serializable {

    /** 所属租户*/
    private long compid;

    /** 用户id*/
    private long userid;

    /** 用户类型(0:普通,1:上级)*/
    private String usertype;

    /** 姓名*/
    private String username;

    /** 性别(0:未知,1:男,2:女)*/
    private Integer sex;

    /** 证件号码*/
    private String idno;

    /** 证件类型*/
    private String idtype;

    /** 手机号*/
    private String phone;

    /** 状态*/
    private String status;

    /** 地址*/
    private String useraddr;

    /** 个人/组织账户标识(0:个人账户,1:组织账户)*/
    private Integer accflag;

    private String remarks1;
    private String remarks2;
    private String remarks3;
    private String remarks4;


    public long getCompid() {
        return compid;
    }

    public void setCompid(long compid) {
        this.compid = compid;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUseraddr() {
        return useraddr;
    }

    public void setUseraddr(String useraddr) {
        this.useraddr = useraddr;
    }

    public String getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(String remarks1) {
        this.remarks1 = remarks1;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    public String getRemarks4() {
        return remarks4;
    }

    public void setRemarks4(String remarks4) {
        this.remarks4 = remarks4;
    }

    public Integer getAccflag() {
        return accflag;
    }

    public void setAccflag(Integer accflag) {
        this.accflag = accflag;
    }
}
