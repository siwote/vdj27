package com.lehand.duty.dao;

import com.lehand.base.common.Pager;
import com.lehand.base.common.PagerUtils;
import com.lehand.base.constant.DateEnum;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.TimeResponse;
import com.lehand.duty.pojo.TkMyTask;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository
public class TkMyTaskDao {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	private static final String get = "SELECT * FROM tk_my_task WHERE compid=? and id=?";
	private static final String STATUS_COUNT_BACK = "SELECT count(*) as '6' FROM tk_my_task a left join tk_task_deploy b on a.taskid = b.id where a.compid=? and a.sjtype = ? and a.status = 6 and b.userid = ? and b.compid=?";
	private static final String updateTkMyTaskStatus = "UPDATE tk_my_task SET status=?,newtime = ? WHERE compid=? and id=? and status!=?";
	private static final String updateTkMyTaskRemarks = "UPDATE tk_my_task SET remarks3=?,remarks6=? WHERE compid=? and id=? ";
	private static final String updateProgressByID = "UPDATE tk_my_task SET newtime=?,progress = ? WHERE compid=? and id = ?";
	private static final String getAvgProgressByTaskid = "SELECT avg(progress) val FROM tk_my_task where compid=? and taskid=? and status!=6";
	//private static final String getBytaskidAndsubnamelike="select id,status,subjectid,subjectname from tk_my_task where taskid=? ";
	private static final String cuiBanSubject="select * from tk_my_task where compid=? and taskid=? and status in (2,3) and datenode=?  and remarks1!=-1  ";
	private static final String cuiBanSubjectputong="select * from 	tk_my_task where compid=? and taskid=? and status in (2,3)  and remarks1!=-1  ";
//	private static final String getSignLonnum="select t.id,t.subjectid subjectid,subjectname,t.status,o.seqno seqno, GROUP_CONCAT(o.orgname) orgname from pw_organ o"
//			+ "  inner join pw_user_organ_map m on o.id=m.organid "
//			+ "  inner join tk_my_task t ON m.userid = t.subjectid "
//			+ "  where o.compid=? and m.compid=? and t.compid=? and t.taskid=? and o.orgname<>'' ";
	private static final String getSignLonnum="select t.id,t.subjectid subjectid,subjectname,t.status,o.orgseqno seqno, GROUP_CONCAT(o.orgname) orgname from urc_organization o"
			+ "  inner join urc_user_org_map m on o.orgid=m.orgid "
			+ "  inner join tk_my_task t ON m.userid = t.subjectid "
			+ "  where o.compid=? and m.compid=? and t.compid=? and t.taskid=? and o.orgname<>'' ";
//	private static final String getSignStatus="select count(if(t.status=2 ,true,null)) as '2', count(if(t.status in (3,4,5) ,true,null)) as '3',count(if(t.status=6 ,true,null)) as '6' " +
//			"from tk_my_task t left join pw_user p on t.subjectid=p.id  where t.compid=? and p.compid=? and t.taskid=? and p.status!=0";
	private static final String getSignStatus="select count(if(t.status=2 ,true,null)) as '2', count(if(t.status in (3,4,5) ,true,null)) as '3',count(if(t.status=6 ,true,null)) as '6' " +
			"from tk_my_task t left join urc_user p on t.subjectid=p.userid  where t.compid=? and p.compid=? and t.taskid=? and p.status!=0";
	
	public Double getAvgProgressByTaskid(Long compid,Long taskId) {
		return generalSqlComponent.getDbComponent().getSingleColumn(getAvgProgressByTaskid,Double.class, new Object[] {compid,taskId});
	}
	
	public int updateProgressByID(Long compid,String newtime,double progress,Long id) {
		return generalSqlComponent.getDbComponent().update(updateProgressByID, new Object[] {newtime,progress,compid,id});
	}
	
	public TkMyTask insert(TkMyTask info) {
		Long id = generalSqlComponent.insert(SqlCode.saveMyTask, info);
		info.setId(id);
		return info;
	}
	
	/**
	 * 查询任务反馈信息
	 * @param id
	 * @return
	 */
	public TkMyTask get(Long compid,Long id) {
		return generalSqlComponent.getDbComponent().getBean(get, TkMyTask.class, new Object[] {compid,id});
	}
	
	public Map<String, Object> get2Map(Long compid,Long userid, int sjtype) {
		return generalSqlComponent.getDbComponent().getMap(STATUS_COUNT_BACK, new Object[] { compid,sjtype,userid,compid});
	}
	
	/**
	 * 更新TkTaskBack表的状态
	 * @param status
	 * @param id
	 */
	public int updateTkMyTaskStatus(Long compid,int status,Long id) {
		return generalSqlComponent.getDbComponent().update(updateTkMyTaskStatus,new Object[] {status,DateEnum.YYYYMMDDHHMMDD.format(),compid,id,status});
	}
	
	/**
	 * 更新退回原因Remarks3字段
	 * @author pantao
	 * @date 2018年11月26日下午7:07:48
	 * 
	 * @param msg
	 * @param id
	 * @return
	 */
	public int updateTkMyTaskRemarks(Long compid,String msg,Long id,String remarks6) {
		return generalSqlComponent.getDbComponent().update(updateTkMyTaskRemarks,new Object[] {msg,remarks6,compid,id});
	}
	/**
	 * 我的周期任务时间节点集合
	 * @param taskid
	 * @param pager
	 * @param userid
	 * @return
	 */
	public List<TimeResponse> list(Long compid, Long taskid, Pager pager, Long userid){
	    Pager page = generalSqlComponent.pageQuery(SqlCode.listMyTask2 , new Object[] {compid,taskid,userid} , pager);
        PagerUtils<TimeResponse> pagerUtils = new PagerUtils<TimeResponse>(page);
        return pagerUtils.getRows();
	}
	/**
	 * 我的周期任务时间节点总条数
	 * @param taskid
	 * @param compid
	 * @param userid
	 * @return
	 */
	public int countBytaskIdAndSubjectId(Long compid,Long taskid,Long userid) {
		return generalSqlComponent.query(SqlCode.listMyTaskCount, new Object[] {compid,taskid,userid});
	}

	/**
	 * 已部署周期任务时间节点集合
	 * @param taskid
	 * @param pager
	 * @param compid
	 * @return
	 */
	public List<TimeResponse> listByTaskid(Long compid,Long taskid, Pager pager) {
		Pager page = generalSqlComponent.pageQuery(SqlCode.listMyTaskByTaskid, new Object[] {compid,taskid}, pager);
		PagerUtils<TimeResponse> pagerUtils = new PagerUtils<TimeResponse>(page);
		return pagerUtils.getRows();
	}
	
	/**
	 * 查询时间周期集合不分页（修改周期任务执行人时需要用）
	 * @param taskid
	 * @return
	 */
	public List<TimeResponse> listByTaskidNoPage(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.listMyTaskByTaskid2, new Object[] {compid,taskid});
	}
	/**
	 * 已部署周期任务时间节点总条数
	 * @param taskid
	 * @param compid
	 * @return
	 */
	public int countBytaskId(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.listbytaskidcountbytaskid, new Object[] {compid,taskid});
	}

	/**
	 * 根据任务id和执行人的id查找我的任务id(周期任务)
	 * @author pantao
	 * @date 2018年12月3日上午11:01:04
	 * 
	 * @param taskid
	 * @param userid
	 */
	public TkMyTask getMyTaskId(Long compid,Long taskid, Long userid, String date) {
		return generalSqlComponent.query(SqlCode.getmytaskid, new Object[] {compid,taskid,userid,date});
	}

	// 
	/**
	 * 根据任务id和执行人的id查找我的任务id(普通任务)
	 * @author pantao
	 * @date 2018年12月3日上午11:01:04
	 * 
	 * @param taskid
	 * @param userid
	 */
	public TkMyTask getMyTaskIdPeriod(Long compid,Long taskid, Long userid) {
		return generalSqlComponent.query(SqlCode.getmytaskidperiod, new Object[] {compid,taskid,userid});
	}
	
	/**
	 * 获取签收日志数据
	 * @param taskid
	 * @param subNameLike
	 * @param pager
	 * @param flag 小程序传1 PC传0
	 * @return
	 */
	public List<TkMyTask>  getBytkidAndlike(Long compid, String status, Long taskid,String subNameLike,String orgNamelike, Pager pager
			,int period,String datenode,int flag,Long userid){
		if (!StringUtils.isEmpty(status)){
			int sts=Integer.parseInt(status);
			if(period==0) {
				if(sts==30) {//小程序专用
				    generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status in (2,3,4,5,6) and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[] {compid,compid,compid,taskid});
				    PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
				    return pagerUtils.getRows();
				}
				if (3==sts){    //已签收  0是删除状态，已反馈的人员，删除时保留签收及反馈日志，反馈的一定已经签收
					if(userid!=null){
                        generalSqlComponent.getDbComponent().pageBean(pager, getSignLonnum+ " and t.status in (3,4,5,0) and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' and t.subjectid = "+userid+"   group by subjectid", TkMyTask.class, new Object[] {compid,compid,compid,taskid});
                        PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                        return pagerUtils.getRows();
					}else{
                        generalSqlComponent.getDbComponent().pageBean(pager, getSignLonnum+ " and t.status in (3,4,5,0) and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[] {compid,compid,compid,taskid});
                        PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                        return pagerUtils.getRows();
					}
				}else if(100 == sts){ //已签收未签收,   0是删除状态，已反馈的人员，删除时保留签收及反馈日志
					if(userid!=null){
                        generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status in (2,3,4,5,0) and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' and t.subjectid = "+userid+"   group by subjectid order by seqno asc", TkMyTask.class, new Object[] {compid,compid,compid,taskid});
                        PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                        return pagerUtils.getRows();
					}else{
                        generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status in (2,3,4,5,0) and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid order by seqno asc", TkMyTask.class, new Object[] {compid,compid,compid,taskid});
                        PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                        return pagerUtils.getRows();
					}
				}else{
					if(flag == 0) {//PC只显示未签收的
						if(userid!=null){
                            generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status=? and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' and t.subjectid = "+userid+"    group by subjectid", TkMyTask.class, new Object[]{ compid,compid,compid,taskid ,sts});
                            PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                            return pagerUtils.getRows();
						}else{
                            generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status=? and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[]{ compid,compid,compid,taskid ,sts});
                            PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                            return pagerUtils.getRows();
						}
					}else {//小程序显示所有人的
                        generalSqlComponent.getDbComponent().pageBean(pager, getSignLonnum+ " and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[]{ compid,compid,compid,taskid});
                        PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                        return pagerUtils.getRows();
					}
				}
				
			}else {
				if (3==sts){
                    generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status in (3,4,5) and t.datenode = ? and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[] {compid,compid,compid,taskid , datenode});
                    PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                    return pagerUtils.getRows();
				}else {
                    generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status=? and t.datenode =? and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[]{ compid,compid,compid,taskid ,sts,datenode});
                    PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                    return pagerUtils.getRows();
				}
			}
		}else {
			if(userid!=null){
                generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status=2 and t.datenode = ?  and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' and t.subjectid = "+userid+"  group by subjectid", TkMyTask.class, new Object[] {taskid,datenode});
                PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                return pagerUtils.getRows();
			}else{
                generalSqlComponent.getDbComponent().pageBean(pager,getSignLonnum+ " and t.status=2 and t.datenode = ?  and subjectname like '"+subNameLike+"' and orgname like '"+orgNamelike+"' group by subjectid", TkMyTask.class, new Object[] {taskid,datenode});
                PagerUtils<TkMyTask> pagerUtils = new PagerUtils<>(pager);
                return pagerUtils.getRows();
			}

		}
	}
	
//	/**
//	 * 获取签收日志的数量
//	 * @param taskid
//	 * @param subNameLike
//	 * @return
//	 */
//	public List<TkMyTask> getSignLogNum(long taskid ,String subNameLike,String orgNamelike){
//		List<TkMyTask> list=super.list2bean(getBytaskidAndsubnamelike+" and subjectname like '"+subNameLike+"' and orgname like'"+orgNamelike+"' GROUP BY subjectid", TkMyTask.class, taskid);
//		return list;
//	}

	/**
	 * 逾期任务
	 * @return
	 */
	public List<TkMyTask> listOverBack(){
		return  generalSqlComponent.getDbComponent().listBean("SELECT * FROM tk_my_task where status in (2,3)  and datenode < ? and remarks1!=1 ",TkMyTask.class, DateEnum.YYYYMMDDHHMMDD.format());
	}
	
	/**
	 * 截止日前提醒
	 * @return
	 */
	public List<TkMyTask> listComingToEndtime(){
		return  generalSqlComponent.getDbComponent().listBean("SELECT * FROM tk_my_task where status in (2,3)  and remarks2!=1  order by datenode asc",TkMyTask.class, new Object[] {});
	}

	/**
	 * 更新逾期标识（remarks1字段）
	 * @param id
	 */
	public void updateRemarks1(Long compid,Long id) {
        generalSqlComponent.getDbComponent().update("UPDATE tk_my_task SET remarks1 = 1  WHERE compid=? and id = ?",new Object[] {compid, id});
	}
	
	public List<TkMyTask> listByTaskid(Long compid,Long taskid){
        return generalSqlComponent.getDbComponent().listBean("SELECT * FROM tk_my_task where compid=? and taskid = ? and status!=6", TkMyTask.class, new Object[] {compid,taskid});
	}
	
	/**
	 * 任务即将结束发提醒标识
	 * @param id
	 */
	public void updateRemarks2(Long compid,Long id) {
        generalSqlComponent.getDbComponent().update("UPDATE tk_my_task SET remarks2 = 1  WHERE compid=? and id = ?", new Object[] {compid,id});
	}
	
	/**
	 * 获取需要催办的周期任务subjectid数据
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
	public List<TkMyTask> getNeedUegernt(Long compid,Long taskid,String datenode){
		List<TkMyTask> subList=generalSqlComponent.getDbComponent().listBean(cuiBanSubject, TkMyTask.class, new Object[] {compid,taskid,datenode});
		return subList;
	}

	/**
	 * 获取需要催办的普通任务subjectid数据
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
	public List<TkMyTask> getNeedUegernt(Long compid,Long taskid){
		List<TkMyTask> subList=generalSqlComponent.getDbComponent().listBean(cuiBanSubjectputong, TkMyTask.class, new Object[] {compid,taskid});
		return subList;
	}
	
	public void delete(Long compid,Long taskid) {
        generalSqlComponent.getDbComponent().delete("delete from tk_my_task where compid=? and taskid = ? ", new Object[] {compid,taskid});
		
	}
	
	/**
	 * 根据主任务id和执行人id删除子表中该执行人的任务
	 * @param taskid
	 * @param subjectid
	 */
	public void delete(Long compid,Long taskid,Long subjectid) {
        generalSqlComponent.getDbComponent().delete("delete from tk_my_task where compid=? and taskid = ? and subjectid =? ",new Object[] { compid,taskid,subjectid});
		
	}
	
	public List<TkMyTask> listByTaskidAll(Long compid,Long taskid){
		return generalSqlComponent.getDbComponent().listBean("SELECT * FROM tk_my_task where compid=? and taskid = ? ", TkMyTask.class, new Object[]{compid,taskid});
	}
	
	/**
	 * 签收日志新增状态查询（提供下拉框查询方法）
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
	public Map<String, Object> getSignStatus(Long compid, Long taskid,int period,String datenode){	
		if(period==0) {
			return generalSqlComponent.getDbComponent().getMap(getSignStatus, new Object[]{ compid,compid,taskid});
		}else {
			return generalSqlComponent.getDbComponent().getMap(getSignStatus+" and datenode=?", new Object[]{ compid,compid,taskid,datenode});
		}
		
	}
	
	public int getNum(Long compid,Long taskid) {
		return generalSqlComponent.getDbComponent().getBean("select count(status) from tk_my_task where compid=? and taskid = ? and status = 3", Integer.class, new Object[] {compid,taskid});
	}
	
	/**
	 * 将主任务下面的子任务状态更新
	 * @param taskid
	 * @param status
	 */
	public void updateStatus(Long compid,Long taskid,int status) {
        generalSqlComponent.getDbComponent().update(" UPDATE tk_my_task SET  status = ? WHERE compid=? and taskid=? and status != 5 and status != 6 ",new Object[] {status,compid,taskid});
	}
	
	/**
	 * 更新某执行人的任务状态
	 * @param taskid
	 * @param subjectid
	 * @param status
	 */
	public void updateStatusBySubject(Long taskid,Long subjectid,int status) {
        generalSqlComponent.getDbComponent().update(" UPDATE tk_my_task SET  status = ? WHERE taskid=? and subjectid = ? and status != 5 and status != 6",new Object[] {status,taskid,subjectid});
	}
	/**
	 * 通过id更新任务状态
	 * @param id
	 * @param status
	 */
	public void updateStatusById(Long compid,Long id , int status) {
        generalSqlComponent.getDbComponent().update(" UPDATE tk_my_task SET  status = ? WHERE compid=? and id=? and status != 5 and status != 6",new Object[] {status,compid,id});
	}

//	public List<TkMyTask> listByTaskidAndSubjectid(Long id, String userid) {
//		return null;
//	}
	
	/**
	 * 判断下该任务下是不是所有人的任务都暂停了
	 * @param id
	 * @param status
	 * @return
	 */
	public int getCountByStstus(Long compid, Long id , int status) {
		return generalSqlComponent.getDbComponent().getBean("select count(status) from tk_my_task where compid=? and taskid=? and status!=? and status!=6", Integer.class,new Object[] {compid,id,status});
	}

	/**
	 * 确认完成该周期下的所有任务
	 * @param taskid
	 * @param enddate
	 * @param status
	 */
	public void updateByEndtimeStatus(Long compid, Long taskid, String enddate, int status) {
        generalSqlComponent.getDbComponent().update("UPDATE tk_my_task SET  status = ? WHERE compid=? and taskid=? and datenode = ? and status!=6", new Object[] {status,compid,taskid,enddate});
	}

	/**
	 * 删除周期任务当前周期的任务
	 * @param taskid
	 * @param now
	 */
	public void deleteByStarttime(Long compid,Long taskid, String now) {
        generalSqlComponent.getDbComponent().delete("DELETE FROM tk_my_task WHERE compid=? and taskid = ? and datenode = ?", new Object[] {compid,taskid,now});
	}
	
	/**
	 * 查询周期任务下有没有哪个周期下面的任务全部签收完毕
	 * @param taskid
	 * @param num
	 * @return
	 */
	public int countBySign(Long compid,Long taskid , int num) {
		return generalSqlComponent.getDbComponent().getBean("select count(1) from ( SELECT count(*) num FROM tk_my_task where compid=? and taskid = ? and status >2  group by datenode ) a where a.num>=?",Integer.class, new Object[] {compid,taskid,num});
	}

	/**
	 * 针对周期任务的暂停查询
	 * @param taskid
	 * @param userid
	 * @return
	 */
	public TkMyTask getByTaskidAndSubjectid(Long compid, Long taskid, Long userid) {
		return generalSqlComponent.getDbComponent().getBean("SELECT * FROM tk_my_task where compid=? and taskid=? and subjectid = ?",TkMyTask.class,new Object[] { compid,taskid,userid});
	}
	
	/**
	 * 针对确认完成该阶段任务的查询我的任务id
	 * @param taskid
	 * @param userid
	 * @param enddate
	 * @return
	 */
	public TkMyTask getByTaskidAndSubjectidAndDatenode(Long compid, Long taskid, Long userid ,String enddate) {
		return generalSqlComponent.getDbComponent().getBean("SELECT * FROM tk_my_task where compid=? and taskid=? and subjectid = ? and datenode =?",TkMyTask.class, new Object[] {compid,taskid,userid,enddate});
	}

	/**
	 * 周期任务查询每个周期的状态办理中（有办理中的并且没有未签收的就为办理中），退回（全部退回就为退回），已完成（全部完成就为完成），未签收（有未签收的就为未签收）
	 * @param taskid
	 * @param endtime
	 */
	public int listTaskidAndEndtimeByStatus(Long compid, Long taskid, String endtime,int status) {
		return generalSqlComponent.getDbComponent().getBean(" SELECT count(*) FROM tk_my_task where compid=? and taskid = ? and datenode = ? and status = ?",Integer.class, new Object[] {compid,taskid,endtime,status});
	}

	/**
	 * 修改任务的起始时间后更新子表的起始时间（针对普通任务）
	 * @param taskid
	 * @param starttime
	 * @param endtime
	 */
	public void updateStartAndEnd(Long compid, Long taskid, String starttime, String endtime) {
        generalSqlComponent.getDbComponent().update("UPDATE tk_my_task SET  createtime = ? , datenode = ? WHERE compid=? and taskid=?", new Object[] {starttime,endtime,compid,taskid});
	}

	/**
	 * 分解任务反馈后更新进度值
	 * @param valueOf
	 * @param taskid
	 */
	public void updateDocProgress(Long compid, Double valueOf, Long taskid) {
        generalSqlComponent.getDbComponent().update("UPDATE tk_my_task SET progress = ? WHERE compid=? and taskid=?", new Object[] {valueOf,compid,taskid});
	}
	
	/**
	 * 查询主任务下子任务的数量
	 * @param taskid
	 * @return
	 */
	public int getTaskCount(Long compid,Long taskid){
		return  generalSqlComponent.getDbComponent().getBean("SELECT count(*) FROM tk_my_task where compid=? and taskid = ?",Integer.class, new Object[] {compid,taskid});
	}
	
	/**
	 * 查询主任务下子任务退回 或 完成的数量
	 * @param taskid
	 * @return
	 */
	public int getTaskByStatus(Long compid,Long taskid,int status){
		return generalSqlComponent.getDbComponent().getBean("SELECT count(*) FROM tk_my_task where compid=? and taskid = ? and status = ?",Integer.class, new Object[] {compid,taskid,status});
	}
}
