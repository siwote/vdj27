package com.lehand.duty.dto;

/**
 * 自定义表单查询
 * 
 * @author pantao
 * @date 2018年11月15日上午9:17:27
 *
 */
public class TkTaskFormRespond {

	private String label;
	private String prop;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getProp() {
		return prop;
	}
	public void setProp(String prop) {
		this.prop = prop;
	}
	
	
}
