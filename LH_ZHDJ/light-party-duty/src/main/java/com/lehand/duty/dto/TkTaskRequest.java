package com.lehand.duty.dto;

import com.lehand.base.exception.LehandException;
import com.lehand.components.web.pipeline.Request;

public class TkTaskRequest extends Request{

	private String tkname;// 任务名称(PC和移动都传)
	private String classid;//任务分类id(PC和移动都传)
	private String time;//查询之间段"time1,time2"(仅PC传)最新反馈时间
	private String createtime;//查询之间段"time1,time2"(仅PC传)
	private String progress;//进度值"progress1,progress2"(仅PC传)
	private String starttime;// 起始时间 (仅移动传)
	private String endtime;// 终止时间 (仅移动传)
	private String subjectid;// 执行人"name1,name2,name3..."(PC和移动都传)
	private Integer status;//状态
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTkname() {
		return tkname;
	}
	public void setTkname(String tkname) {
		this.tkname = tkname;
	}
	public String getClassid() {
		return classid;
	}
	public void setClassid(String classid) {
		this.classid = classid;
	}
	
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(String subjectid) {
		this.subjectid = subjectid;
	}
	@Override
	public void validate() throws LehandException {
		// TODO Auto-generated method stub
		
	}
	
	
}
