package com.lehand.duty.dao;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.SignLogResponse;
import com.lehand.duty.dto.TkTaskDeployRequest;
import com.lehand.duty.dto.TkTaskFormRespond;
import com.lehand.duty.dto.TkTaskRequest;
import com.lehand.duty.pojo.PwAuthOtherMap;
import com.lehand.duty.pojo.TkTaskDeploy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;

@Scope("prototype")
@Repository
public class TkTaskDeployDao {
	
	@Resource
	private GeneralSqlComponent generalSqlComponent;

	@Resource private TkTaskClassDao tkTaskClassDao;
//	@Resource private PwAuthOtherMapDao pwAuthOtherMapDao;
	private List<PwAuthOtherMap> childClass=new ArrayList<PwAuthOtherMap>();
	private boolean flag = false;
	
	private static final String setexsjnum = "UPDATE tk_task_deploy SET exsjnum=exsjnum-1 WHERE compid=? and id=?";
	private static final String setexsjnumadd = "UPDATE tk_task_deploy SET exsjnum=? WHERE compid=? and id=?";
	private static final String insertRetrunID = "INSERT INTO tk_task_deploy (compid,tkname,classid,starttime,endtime,tkdesc,srctaskid,needbacktime,period,resolve,diffdata,exsjnum,progress,newbacktime,userid,username,createtime,updatetime,deletetime,status,remarks1,remarks2,remarks3,remarks4,remarks5,remarks6,publisher,tphone,annex,orgcode,files) VALUES (:compid,:tkname,:classid,:starttime,:endtime,:tkdesc,:srctaskid,:needbacktime,:period,:resolve,:diffdata,:exsjnum,:progress,:newbacktime,:userid,:username,:createtime,:updatetime,:deletetime,:status,:remarks1,:remarks2,:remarks3,:remarks4,:remarks5,:remarks6,:publisher,:tphone,:annex,:orgcode,:files)";
	private static final String updateByInfo = "UPDATE tk_task_deploy SET tkname = :tkname, classid = :classid,  starttime  = :starttime,  endtime  = :endtime,  tkdesc  = :tkdesc,needbacktime = :needbacktime,  period  = :period,  resolve  = :resolve,  diffdata  = :diffdata,  exsjnum  = :exsjnum,  newbacktime  = :newbacktime,  userid  = :userid,  username  = :username ,  status  = :status,  remarks1  = :remarks1,  remarks2  = :remarks2 ,remarks3=:remarks3,remarks6=:remarks6,publisher = :publisher,tphone = :tphone,annex= :annex,files=:files WHERE  id  = :id";
	private static final String updateTkTaskDeployStauts = "UPDATE tk_task_deploy SET status=? , updatetime =? WHERE compid=? and id=? and status != 50";
	private static final String updateProgressAndNewbacktime = "UPDATE tk_task_deploy SET progress = ?, newbacktime = ? WHERE compid=? and id = ?";
	private static final String listCount = "SELECT COUNT(*) FROM tk_task_deploy WHERE compid=? and CLASSID=? AND STATUS IN(30,40,45,50)";
	private static final String COUNT_SQL_BY_STATUS = "select count(if(status =40,true,null)) as '40',count(if(status=30,true,null)) as '30',count(if(status in (45,50),true,null)) as '100',count(if(status in (2,3,5),true,null)) as '60'  from tk_task_deploy where compid=? and userid=?";
	
//	private static final String COUNT_SQL_BY_STATUS_ORG = "select count(if(status =40,true,null)) as '40',count(if(status=30,true,null)) as '30',count(if(status in (45,50),true,null)) as '100',count(if(status in (2,3,5),true,null)) as '60'  from tk_task_deploy a LEFT JOIN pw_user_organ_map p on a.userid=p.userid and a.compid=p.compid where a.compid=? and p.organid=?";
	private static final String COUNT_SQL_BY_STATUS_ORG = "select count(if(status =40,true,null)) as '40',count(if(status=30,true,null)) as '30',count(if(status in (45,50),true,null)) as '100',count(if(status in (2,3,5),true,null)) as '60'  from tk_task_deploy a LEFT JOIN urc_user_org_map p on a.userid=p.userid and a.compid=p.compid where a.compid=? and p.orgid=?";

	
	//"select count(if(status in (30,40),true,null)) as '1',count(if(status=45,true,null)) as '45',count(if(status=50,true,null)) as '50' from tk_task_deploy where compid=? and userid=?";
	public static final String get = "SELECT * FROM tk_task_deploy WHERE compid=? and id=?";
	private static final String GO_BACK_DATA_SQL = "select * from ("
			+ " select c.mytaskid,c.id,c.classid,d.clname,c.tkname,c.subjectid,c.subjectname,c.newtime newbacktime,c.progress,c.status,c.sjtype,c.userid,c.createtime,c.exsjnum,c.period  from tk_task_class d right join"
			+ " (SELECT a.id as mytaskid,b.id as id,b.classid classid,b.tkname tkname,a.subjectid subjectid,a.subjectname subjectname,a.newtime newtime,"
			+ " a.progress progress,a.status status,a.sjtype sjtype,b.userid userid,a.createtime createtime,b.exsjnum exsjnum,b.period period FROM tk_my_task a left join tk_task_deploy b on a.taskid = b.id where a.compid=? and b.compid=? and a.status = 6)c"
			+ " on c.classid = d.id where d.compid=?)e where sjtype = ?  and userid = ?";
//	private static final String PAGE_SQL = "select a.id id,a.tkname tkname,a.exsjnum exsjnum,a.bclassid classid,a.clname clname," +
//			"a.newbacktime newbacktime,a.userid userid,a.status status, a.progress progress,a.period period,a.resolve resolve,a.starttime starttime," +
//			"a.createtime createtime from (select d.*,b.id bclassid ,b.clname"
//			+ " from tk_task_deploy d left join tk_task_class b on d.classid = b.id )a"
//			+ "  where a.userid= ? ";
	private static final String PAGE_SQL = "select a.id id,a.tkname tkname,a.exsjnum exsjnum,a.bclassid classid,a.clname clname,case when a.newtime is null then '' else a.newtime end newbacktime,a.userid userid,"
			+" a.status status, a.progress progress,a.period period,a.resolve resolve,a.starttime starttime,a.createtime createtime from ("
			+" select d.*,b.id bclassid ,b.clname,s.newtime from tk_task_deploy d left join tk_task_class b on d.compid=b.compid and d.classid = b.id "
			+" left join (select compid,taskid,max(opttime) newtime from tk_my_task_log group by compid,taskid) s on s.compid=d.compid and d.id=s.taskid)a"  
			+ " where a.compid=? and a.userid= ?" ; 
	
//	private static final String PAGE_SQL_ORG = "select p.organid, a.id id,a.tkname tkname,a.exsjnum exsjnum,a.bclassid classid,a.clname clname,case when a.newtime is null then '' else a.newtime end newbacktime,a.userid userid,"
//			+" a.status status, a.progress progress,a.period period,a.resolve resolve,a.starttime starttime,a.createtime createtime from ("
//			+" select d.*,b.id bclassid ,b.clname,s.newtime from tk_task_deploy d left join tk_task_class b on d.compid=b.compid and d.classid = b.id "
//			+" left join (select compid,taskid,max(opttime) newtime from tk_my_task_log group by compid,taskid) s on s.compid=d.compid and d.id=s.taskid)a"
//			+ " LEFT JOIN pw_user_organ_map p on a.userid=p.userid and a.compid=p.compid  where a.compid=? and p.organid= ?" ;

	private static final String PAGE_SQL_ORG = "select p.organid, a.id id,a.tkname tkname,a.exsjnum exsjnum,a.bclassid classid,a.clname clname,case when a.newtime is null then '' else a.newtime end newbacktime,a.userid userid,"
			+" a.status status, a.progress progress,a.period period,a.resolve resolve,a.starttime starttime,a.createtime createtime from ("
			+" select d.*,b.id bclassid ,b.clname,s.newtime from tk_task_deploy d left join tk_task_class b on d.compid=b.compid and d.classid = b.id "
			+" left join (select compid,taskid,max(opttime) newtime from tk_my_task_log group by compid,taskid) s on s.compid=d.compid and d.id=s.taskid)a"
			+ " LEFT JOIN urc_user_org_map p on a.userid=p.userid and a.compid=p.compid  where a.compid=? and p.orgid= ?" ;
	
	private static final String PAGE_SQL_MOBILE =" select * from("
			+" select c.id,c.tkname,c.exsjnum,c.classid,c.clname,c.newbacktime,c.progress,c.period,c.resolve,d.subjectid,d.subjectname,c.userid userid,c.status status,c.starttime,c.createtime from"  
			+" (select a.id id,a.tkname tkname,a.exsjnum exsjnum,a.classid classid,b.clname clname,a.newbacktime newbacktime,a.userid userid,a.status status,"  
			+" a.progress progress,a.period period,a.resolve resolve,a.starttime starttime,a.createtime createtime"  
			+" from tk_task_deploy a left join tk_task_class b on a.classid = b.id where a.compid=? and b.compid=? and a.exsjnum>0)c "  
			+" left join (select taskid , group_concat(subjectid) subjectid,group_concat(subjectname) subjectname  from tk_task_subject where compid=? and remarks1!=-1 and flag = 0 and sjtype=0 group by taskid)d on c.id=d.taskid " 
			+" )e where e.userid= ?  and period=0";
//	private static final String getSignLonnum="select t.id,t.subjectid subjectid,subjectname,t.status,o.orgname from pw_organ o"
//			+ "  inner join pw_user_organ_map m on o.id=m.organid"
//			+ "  inner join tk_my_task t ON m.userid = t.subjectid "
//			+ "  where o.compid=? and m.compid=? and t.compid=? and t.taskid=? and o.orgname<>'' ";

	private static final String getSignLonnum="select t.id,t.subjectid subjectid,subjectname,t.status,o.orgname from urc_organization o"
			+ "  inner join urc_user_org_map m on o.orgid=m.orgid"
			+ "  inner join tk_my_task t ON m.userid = t.subjectid "
			+ "  where o.compid=? and m.compid=? and t.compid=? and t.taskid=? and o.orgname<>'' ";
	private static final String PAGE_SQL_DuBan = " select * from( "
			+ " select c.id,c.tkname,c.exsjnum,c.classid,c.clname,c.newbacktime,c.progress,c.period,c.resolve,d.subjectid,d.subjectname,c.userid userid,c.status status,c.starttime,c.createtime ,c.endtime  from "
			+ " (select a.id id,a.tkname tkname,a.exsjnum exsjnum,a.classid classid,b.clname clname,a.newbacktime newbacktime,a.userid userid,a.status status, "
			+ " a.progress progress,a.period period,a.resolve resolve,a.starttime starttime,a.createtime createtime,a.endtime endtime "
			+ " from tk_task_deploy a left join tk_task_class b on a.classid = b.id where a.compid=? and b.compid=? and a.exsjnum=1)c "
			+ " left join tk_task_subject d on c.id=d.taskid and d.flag=0 and remarks1!=-1 AND d.compid=? "
			+ " union all "
			+ " select a.id id,a.tkname tkname,a.exsjnum exsjnum,a.classid classid,b.clname clname,a.newbacktime newbacktime, "
			+ " a.progress progress,a.period,a.resolve,null as subjectid,null as subjectname,a.userid userid,a.status status,a.starttime starttime,a.createtime createtime,a.endtime endtime  "
			+ " from tk_task_deploy a "
			+ " left join tk_task_class b on a.classid = b.id "
			+ " where a.compid=? and b.compid=? and a.exsjnum>1 "
			+ " )e where 1=1 ";
	private static final String CountByStatus_Duban="select count(if(status=40 ,true,null)) as '40',count(if(status=45 ,true,null)) as '45',count(if(status=50 ,true,null)) as '50' from tk_task_deploy ";
	private static final String updateTaskDeployBase=" update  tk_task_deploy "
			+ " set tkname=? , tkdesc=? , classid=?, "
			+ " updatetime=?  where compid=? and id=? ";
	
	private static final String updateTaskDeployBase2=" update  tk_task_deploy "
			+ " set tkname=? , tkdesc=? , classid=?,publisher = ?,tphone = ?,annex = ?, "
			+ " updatetime=?  where compid=? and id=? ";
	
	private static final String updateTaskDeployBaseResolve=" update  tk_task_deploy "
			+ " set tkname=? , tkdesc=? , classid=?, "
			+ " updatetime=? , diffdata=? where compid=? and id=? ";
	private static final String updateStartEndTime=" update  tk_task_deploy  "
			+ " set starttime=? , endtime=?  , diffdata=? , updatetime=?  where compid=? and id=?  ";
	private static final String updateTaskBasePeriod=" update  tk_task_deploy  "
			+ "set tkname=? , tkdesc=?  , classid=? , "
			+ " starttime=? , endtime=?  , updatetime=?,diffdata=?  where compid=? and id=?  ";
	
	/**
	 * 更新任务表状态
	 * @author pantao
	 * @date 2018年11月16日下午2:52:21
	 * 
	 * @param progress
	 * @param id
	 * @return
	 */
	public int updateProgressAndnewbacktime(Long compid,Double progress,String newbacktime,Long id) {
//		return super.update(updateProgressAndNewbacktime, progress,newbacktime,compid,id);
		return generalSqlComponent.getDbComponent().update(updateProgressAndNewbacktime,new Object[]{progress,newbacktime,compid,id} );
	}
	
	public int updateTkTaskDeployStauts(Long compid,int status,Long id) {
//		return super.update(updateTkTaskDeployStauts, status,DateEnum.YYYYMMDDHHMMDD.format(),compid,id);
		return generalSqlComponent.getDbComponent().update(updateTkTaskDeployStauts, new Object[]{status,DateEnum.YYYYMMDDHHMMDD.format(),compid,id});
	}

	public int updateByBean(TkTaskDeploy info){
//		return super.updateByBean(updateByInfo,info);
		return generalSqlComponent.getDbComponent().update(updateByInfo,info);
	}
	
	/**
	 * 根据主键id插入数据
	 * @author pantao
	 * @date 2018年11月16日下午2:52:51
	 * 
	 * @param info
	 * @return
	 */
	public long insertRetrunID(TkTaskDeploy info) {
//		return super.insertRetrunID(insertRetrunID, info);
		return generalSqlComponent.getDbComponent().insertReturnId(insertRetrunID, info,null);
	}
	
	/**
	 * 查询任务分类的数量
	 * @author pantao
	 * @date 2018年11月16日下午2:53:34
	 * 
	 * @param classid
	 * @return
	 */
	public int listCount(Long compid,Long classid) {
//		return super.getInt(listCount,new Object[] {compid,classid});
		return generalSqlComponent.getDbComponent().getSingleColumn(listCount,int.class,new Object[] {compid,classid});
	}
	
	/**
	 * 查询任务部署表
	 * @param id
	 * @return
	 */
	public TkTaskDeploy get(Long compid,Long id) {
//		return super.get(get, TkTaskDeploy.class, compid,id);
		return generalSqlComponent.getDbComponent().getBean(get, TkTaskDeploy.class, new Object[]{compid,id});
	}
	
	public Map<String, Object> get2Map(Long compid,Long userid) {
//		return super.getMap(COUNT_SQL_BY_STATUS, new Object[] { compid,userid });
		return generalSqlComponent.getDbComponent().getMap(COUNT_SQL_BY_STATUS, new Object[] { compid,userid });
	}
	
	public Map<String, Object> get3Map(Long compid,Long orgid) {
//		return super.getMap(COUNT_SQL_BY_STATUS_ORG, new Object[] { compid,orgid });
		return generalSqlComponent.getDbComponent().getMap(COUNT_SQL_BY_STATUS_ORG, new Object[] { compid,orgid });
	}
	
	/**
	 * 已部署任务列表
	 * @author pantao
	 * @date 2018年11月21日下午2:56:20
	 * 
	 * @param request
	 * @param userId
	 * @param pager
	 * @throws LehandException
	 */
	public void page(Session session, TkTaskRequest request, Pager pager, Boolean flag, Long userId) throws LehandException {
		Long compid = session.getCompid();
		Calendar calSmall = Calendar.getInstance();
		calSmall.setTime(calSmall.getTime());
		String sort = pager.getSort();
		String direction = pager.getDirection();
		String createtime = request.getCreatetime();
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = null;
		if(flag) {//PC端已发任务列表
			query_sql = byStatus(compid,request, userId, ps,session.getAccflag()==null?0:session.getAccflag(),session.getCurrorganid());
			query_sql = byClassid(request, query_sql);
			query_sql = byCreateTime(calSmall,createtime,ps,query_sql);
			query_sql = byTkname(request, query_sql);
			query_sql = byProgress(request, ps, query_sql);
			query_sql = bySubjectid(request, ps, query_sql);
			query_sql = byOrder(request, sort, direction, query_sql);
		}else {//小程序端已发任务列表
			if (request.getStatus() == 6) {// 被退回(查询任务子表)
				query_sql = new StringBuilder(GO_BACK_DATA_SQL);
				ps.add(compid);
				ps.add(compid);
				ps.add(compid);
				ps.add(0);// sjtype的值（可由前台传）
				ps.add(userId);
			} else {// 其他
				query_sql = new StringBuilder(PAGE_SQL_MOBILE);
				ps.add(compid);
				ps.add(compid);
				ps.add(compid);
				ps.add(userId);
				if (!StringUtils.isEmpty(request.getStatus())) {
						query_sql.append(" and status = ? ");
						ps.add(request.getStatus());
				}
			}
			if (!StringUtils.isEmpty(request.getTkname())) {// 带任务名/分类名/执行人名称
				String tkname1 = request.getTkname();
				tkname1 = tkname1.replaceAll("\'", "");
				query_sql.append(" and (tkname like '%" + tkname1 + "%' or clname like '%" + tkname1 + "%' or subjectname like '%" + tkname1 + "%')");
			}
			query_sql.append(" order by newbacktime desc ");
		}
//		System.out.println("已发任务列表查询接口：："+query_sql.toString());
//		super.pageMap(query_sql.toString(), ps.toArray(), pager);
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}
	/**
	 * 任务跟踪督办列表查询；目前改回查已部署列表
	 * @param request
	 * @param pager
	 * @throws LehandException
	 * @author yuxianzhu
	 */
	public void overSeePage(TkTaskRequest request, Pager pager,Long userid,Long compid) throws LehandException {
		String time=request.getTime();
		String sort = pager.getSort();
		String direction = pager.getDirection();
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = null;
		query_sql = byClassidOversee(request,userid,compid,ps);		
		query_sql = byStatusDuBan(request, ps,query_sql);
		query_sql = bynewTime(time, ps, query_sql); 
		query_sql = byTkname(request, query_sql);
		query_sql = byProgress(request, ps, query_sql);
		query_sql = bySubjectidOverSee(request, ps, query_sql);
		query_sql = byOrder(request, sort, direction, query_sql);
//		super.pageMap(query_sql.toString(), ps.toArray(), pager);
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}
	
	/**
	 * 任务跟踪督办列表查询；目前改回查已部署列表
	 * @param request
	 * @param pager
	 * @throws LehandException
	 * @author yuxianzhu
	 */
	public void lingDaoPage(TkTaskRequest request,int flag, Pager pager,Long userid,Long compid) throws LehandException {
		String time=request.getTime();
		String sort = pager.getSort();
		String direction = pager.getDirection();
		List<Object> ps = new ArrayList<Object>();
		StringBuilder query_sql = null;
		query_sql = byTaskidLingDao(compid,userid,flag,ps);		
		query_sql = byStatusDuBan(request, ps,query_sql);
		query_sql = bynewTime(time, ps, query_sql); 
		query_sql = byTkname(request, query_sql);
		query_sql = byProgress(request, ps, query_sql);
		query_sql = bySubjectidOverSee(request, ps, query_sql);
		query_sql = byOrder(request, sort, direction, query_sql);
		generalSqlComponent.getDbComponent().pageMap(pager,query_sql.toString(), ps.toArray());
	}
	
	/**
	 * 获取任务督办列表下拉框数据
	 * 			目前只按照分类来查数据,userid暂时不使用，后期可能加
	 * @param userid
	 * @param classid
	 * @return
	 * @author yuxianzhu
	 */
	public Map<String, Object> getStatsCount(Long userid,Long classid,Long compid){
		List<PwAuthOtherMap> treelist = null;
		if(classid!=null) {
			treelist = treeClassList(classid,userid,compid);
		}else {
			treelist = generalSqlComponent.query(SqlCode.listPAOTM, new Object[] {compid,userid});
		}
		String classids = "";
		for (PwAuthOtherMap tkTaskClass : treelist) {
			classids += tkTaskClass.getBizid()+",";
		}
		classids = classids+classid;
//		return super.getMap(CountByStatus_Duban + " where compid=? and exsjnum >0 and classid in ("+classids+") ",new Object[] {compid});
		return generalSqlComponent.getDbComponent().getMap(CountByStatus_Duban + " where compid=? and exsjnum >0 and classid in ("+classids+") ",new Object[] {compid});
	}

	private StringBuilder byCreateTime(Calendar calSmall, String time, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(time)) {
			String timeStr = time.toString();
			String[] times = timeStr.split(",");
			String time1 = times[0]+" 00:00:00";
			String time2 = times[1]+" 23:59:59";
			query_sql.append(" and createtime >= ? and endtime <= ? ");
			ps.add(time1);
			ps.add(time2);
		}
		return query_sql;
	}

	private StringBuilder byOrder(TkTaskRequest request, String sort, String direction, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(direction)) {// 带排序
			String sort2 = sort.toString();
			String direction2 = direction.toString();
			query_sql.append(" order by " + sort2 + " " + direction2);
		} else {
			query_sql.append(" order by newbacktime desc ,createtime desc");
		}
		return query_sql;
	}

	private StringBuilder bySubjectid(TkTaskRequest request, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(request.getSubjectid())) {
			query_sql.append(" and id in (select taskid from tk_task_subject where subjectid =? and flag=0)");
			ps.add(request.getSubjectid());
		}
		return query_sql;
	}
	/**		
	 * 根据subjectid来查
	 * @param request
	 * @param ps
	 * @param query_sql
	 * @return
	 * @author yuxianzhu
	 */
	private StringBuilder bySubjectidOverSee(TkTaskRequest request, List<Object> ps, StringBuilder query_sql){
		if (!StringUtils.isEmpty(request.getSubjectid())) {
			query_sql.append(" and e.id in (select taskid from tk_task_subject where subjectid =? and flag=0 )");
			ps.add(request.getSubjectid());
		}
		return query_sql;
	}

	private StringBuilder byProgress(TkTaskRequest request, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(request.getProgress())) {// 带进度值
			String prog = request.getProgress();
			String[] progs = prog.split(",");
			String progress1 = progs[0];// 进度最小值
			String progress2 = progs[1];// 进度最大值
			query_sql.append(" and progress >= ? and progress <= ? ");
			ps.add(progress1);
			ps.add(progress2);
		}
		return query_sql;
	}

	private StringBuilder byTkname(TkTaskRequest request, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(request.getTkname())) {// 带任务名
			String tkname1 = request.getTkname();
//			tkname1 = tkname1.replaceAll("[^\u4e00-\u9fa5a-zA-Z0-9]", "");//这里暂时这样处理后续再优化//TODO
			tkname1 = tkname1.replaceAll("\'", "");
			query_sql.append(" and tkname like '%" + tkname1 + "%'");
		}
		return query_sql;
	}

	private StringBuilder byClassid(TkTaskRequest request, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(request.getClassid())) {// 带任务分类名
			String classs = request.getClassid();
			String[] str = classs.split(",");
			String clid = "";
			for (String string : str) {
				clid += string + ",";
			}
			clid = clid.substring(0, clid.length() - 1);
			query_sql.append(" and classid in (" + clid + ")");
		}
		return query_sql;
	}

	private StringBuilder byStatus(Long compid,TkTaskRequest request, Long userId, List<Object> ps,int accflag,Long orgId) {
		StringBuilder query_sql;
		if (request.getStatus() == 6) {// 被退回(查询任务子表)
			query_sql = new StringBuilder(GO_BACK_DATA_SQL);
			ps.add(compid);
			ps.add(compid);
			ps.add(compid);
			ps.add(0);// sjtype的值（可由前台传）
			ps.add(userId);
			if (!StringUtils.isEmpty(request.getTime())) {// 带时间
				String timeStr = request.getTime();
				String[] times = timeStr.split(",");
				String time1 = times[0]+" 00:00:00";
				String time2 = times[1]+" 23:59:59";
				query_sql.append(" and newtime >= ? and newtime<=?");
				ps.add(time1);
				ps.add(time2);
			}
		} else {// 其他
			if(accflag==0) {
				query_sql = new StringBuilder(PAGE_SQL);
				ps.add(compid);
				ps.add(userId);
			}else {
				query_sql = new StringBuilder(PAGE_SQL_ORG);
				ps.add(compid);
				ps.add(orgId);
			}
			//状态(0:删除,1:草稿,2:未创建,3:正在创建,30:未签收,40:已签收,45:暂停,50:完成 ，100 暂停，已完成
			if (!StringUtils.isEmpty(request.getStatus())) {
				if (request.getStatus() == 60) {
					Integer status1 = 3;
					Integer status3 = 2;
					Integer status2 = 5;
					query_sql.append(" and status in (" + status1 + "," + status2 +"," + status3 + ") ");
				} else if(request.getStatus() == 100){
					Integer status1 = 45;
					Integer status2 = 50;
					query_sql.append(" and a.status in (" + status1 + "," + status2 + ") ");
				}else {
					query_sql.append(" and a.status = ? ");
					ps.add(request.getStatus());
				}
			}
			if (!StringUtils.isEmpty(request.getTime())) {// 带时间
				String timeStr = request.getTime();
				String[] times = timeStr.split(",");
				String time1 = times[0]+" 00:00:00";
				String time2 = times[1]+" 23:59:59";
				if(request.getStatus() == 60) {
					query_sql.append(" and createtime >= ? and endtime <= ? ");
				}else {
					query_sql.append(" and a.newtime >= ? and a.newtime <= ? ");
				}
				ps.add(time1);
				ps.add(time2);
			}
		}
		return query_sql;
	}
	
	private StringBuilder byStatusDuBan(TkTaskRequest request,  List<Object> ps,StringBuilder query_sql) {
			if (!StringUtils.isEmpty(request.getStatus())) {				
				//办理中（未签收）40，已暂停45，已完成50
					query_sql.append(" and status = ? ");
					ps.add(request.getStatus());
			}
		return query_sql;
	}
	
	private StringBuilder byClassidOversee(TkTaskRequest request,Long userid,Long compid,List<Object> ps) {
		StringBuilder query_sql;
		query_sql = new StringBuilder(PAGE_SQL_DuBan);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		String classs = request.getClassid();
		if (!StringUtils.isEmpty(classs)) {
			int count = generalSqlComponent.query(SqlCode.listPAOTMByPid, new Object[] {compid,Long.valueOf(classs),userid});
			if(count<=0) {//没有子节点
				//步骤一：查询该分类有没有子节点没有就直接用"="否则用"in"
				query_sql.append(" and classid = "+classs);
			}else {
				//步骤二：通过递归查询该分类下面的所有子节点以及其本身，并将id用逗号隔开
				List<PwAuthOtherMap> classids = treeClassList(Long.valueOf(classs),userid,compid);
				String tkclass = "";
				for (PwAuthOtherMap classid : classids) {
					tkclass += classid.getBizid()+",";
				}
				tkclass = tkclass+classs;
				childClass.clear();
				query_sql.append(" and classid in ("+tkclass+")");
			}
		}else {
			List<Map<String, Object>> list = generalSqlComponent.query(SqlCode.listPAOTMUserAndRole, new Object[] {compid,0, userid});
			String cls = "";
			if(list!=null && list.size()>0) {
				for (Map<String, Object> map : list) {
					cls+=map.get("classid")+",";
				}
				cls=cls.substring(0,cls.length()-1);
				query_sql.append(" and classid in ("+cls+")");
			}
		}
		return query_sql;
	}
	
	//flag 4分管领导，3牵头领导，2协办人（这里flag为备用字段防止后面需要将这三个人分开显示）
	private StringBuilder byTaskidLingDao(Long compid,Long userid,int flag,List<Object> ps) {
		StringBuilder query_sql;
		query_sql = new StringBuilder(PAGE_SQL_DuBan);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		ps.add(compid);
		//第一步：根据userid到主体表将flag为3,4的taskid找到
		//第二步：根据taskid找到所有的classid
//		String  info = super.getString("SELECT GROUP_CONCAT(taskid) taskid FROM tk_task_subject where subjectid=? and flag in (2,3,4) ", new Object[] {userid});
		String  info =generalSqlComponent.getDbComponent().getSingleColumn("SELECT GROUP_CONCAT(taskid) taskid FROM tk_task_subject where subjectid=? and flag in (2,3,4) ",
				String.class,
				new Object[] {userid});
		if (!StringUtils.isEmpty(info)) {
			String classs = info;
			query_sql.append(" and id in ("+classs+")");
		}else {
			query_sql.append(" and id = ''");
		}
		return query_sql;
	}
	
	
	
	/**
     * 递归查询任务分类下面所有子分类
     */
    public List<PwAuthOtherMap> treeClassList(Long pid, Long subjectid, Long compid){
    	//-- 首先通过id查找第一级子节点，找到后将结果add到结果集中(查已授权的)
    	List<PwAuthOtherMap> list = generalSqlComponent.query(SqlCode.listPAOTMByPidList, new Object[] {compid,pid,subjectid});
    	//-- 然后将第一级子节点的id变成以逗号隔开的字符串
    	String ids = generalSqlComponent.query(SqlCode.listPAOTMByIdString, new Object[] {compid,pid,subjectid});
    	if(list != null && list.size()>0) {
    		childClass = list;
    		flag = true;
    		data(flag,ids,subjectid,compid);
    	}
	    return childClass;
	 }
    
    private void data(boolean flag,String ids,Long subjectid,Long compid){
    	if (flag) {
    		Map<String,Object> param = new HashMap<String, Object>(3);
    		param.put("compid", compid);
    		param.put("subjectid", subjectid);
    		param.put("ids", ids);
    		//-- 其次将上一步得到的字符串进行下一步查询
        	List<PwAuthOtherMap> list2 = generalSqlComponent.query(SqlCode.listPAOMBySubjectidAndRemarks3, param);
        	String ids2 = tkTaskClassDao.getIdString(compid,ids);
        	if(list2 != null && list2.size()>0) {
        		for (PwAuthOtherMap organ : list2) {
        			childClass.add(organ);
				}
        		data(flag,ids2,subjectid,compid);
        	}else {
        		flag=false;
        	}
		}
    }
	/**
	 * 根据更新时间的条件进行查询，
	 * @param time 如果是已部署任务表是newbacktime,最新反馈时间，我的任务列表是newtime 				
	 * @param ps
	 * @param query_sql
	 * @return
	 * @author yuxianzhu
	 */
	private StringBuilder bynewTime(String time, List<Object> ps, StringBuilder query_sql) {
		if (!StringUtils.isEmpty(time)) {
			String timeStr = time.toString();
			String[] times = timeStr.split(",");
			String time1 = times[0]+" 00:00:00";
			String time2 = times[1]+" 23:59:59";
			query_sql.append(" and newbacktime >= ? and newbacktime <= ? ");
			ps.add(time1);
			ps.add(time2);
		}
		return query_sql;
	}

	//退回时单人任务就将主表状态设置为0
	public void updateStatus(Long compid,Long id,int status) {
//		super.update(updateTkTaskDeployStauts,status,DateEnum.YYYYMMDDHHMMDD.format(),compid,id);
		generalSqlComponent.getDbComponent().update(updateTkTaskDeployStauts,new Object[]{status,DateEnum.YYYYMMDDHHMMDD.format(),compid,id});

	}

	//退回操作将主表任务执行人数量减1
	public int updateExsjnum(Long compid,Long id) {
//		return super.update(setexsjnum, compid,id);
		return generalSqlComponent.getDbComponent().update(setexsjnum, new Object[]{compid,id});
	}
	
	//修改执行个人后要将最新的执行人数量更新到主表中
	public int updateAddExsjnum(Long compid,Long id,int count) {
//		return super.update(setexsjnumadd, count,compid,id);
		return generalSqlComponent.getDbComponent().update(setexsjnumadd, new Object[]{count,compid,id});
	}
	
	/**
	   * 查询任务的执行人数量
	 * @param id
	 * @return
	 */
	public TkTaskDeploy getTask(Long compid,Long id) {
//		return super.get("select exsjnum,period from tk_task_deploy where compid=? and id = ? ", TkTaskDeploy.class, compid,id);
		return generalSqlComponent.getDbComponent().getBean("select exsjnum,period from tk_task_deploy where compid=? and id = ? ",
				TkTaskDeploy.class,
				new Object[]{compid,id});
	}
	
	/**
	  * 获取签收日志的条数
	  * @param status
	  * @param taskid
	  * @param subName
	  * @param orgName
	  * @return
	  * @author yuxianzhu
	  */
	 public List<SignLogResponse> getSignLogNum(Long compid, String status, long taskid, String subName, String orgName, Long userid){
	  List<SignLogResponse> list=null;
	  if (!StringUtils.isEmpty(status)){
	    int sts=Integer.parseInt(status);
	    if (3==sts){
	    	if(userid!=null){
//	       		list=super.list2bean(getSignLonnum+ " and t.status in (3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' and t.subjectid = "+userid+" GROUP BY subjectid", SignLogResponse.class, compid,compid,compid,taskid);
	       		list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status in (3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' and t.subjectid = "+userid+" GROUP BY subjectid",
						SignLogResponse.class,new Object[]{ compid,compid,compid,taskid});
			}else{
//				list=super.list2bean(getSignLonnum+ " and t.status in (3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' GROUP BY subjectid", SignLogResponse.class, compid,compid,compid,taskid);
				list = generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status in (3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' GROUP BY subjectid", SignLogResponse.class,
						new Object[]{compid,compid,compid,taskid});
			}
	    } else if(100==sts){  //已签收 ，未签收
			if(userid!=null){
//				list=super.list2bean(getSignLonnum+ " and t.status in (2,3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' and t.subjectid = "+userid+" GROUP BY subjectid", SignLogResponse.class, compid,compid,compid,taskid);
				list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status in (2,3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' and t.subjectid = "+userid+" GROUP BY subjectid",
						SignLogResponse.class,
						new Object[]{compid,compid,compid,taskid});
			}else{
//				list=super.list2bean(getSignLonnum+ " and t.status in (2,3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' GROUP BY subjectid", SignLogResponse.class, compid,compid,compid,taskid);
				list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status in (2,3,4,5,0) and subjectname like '"+subName+"' and orgname like '"+orgName+"' GROUP BY subjectid",
						SignLogResponse.class,
						new Object[]{compid,compid,compid,taskid});
			}
		}else {
			if(userid!=null){
//				list=super.list2bean(getSignLonnum+ " and t.status=? and subjectname like '"+subName+"' and orgname like '"+orgName+"' and t.subjectid = "+userid+" GROUP BY subjectid", SignLogResponse.class, new Object[]{ compid,compid,compid,taskid ,sts});
				list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status=? and subjectname like '"+subName+"' and orgname like '"+orgName+"' and t.subjectid = "+userid+" GROUP BY subjectid",
						SignLogResponse.class,
						new Object[]{ compid,compid,compid,taskid ,sts});
			}else{
				list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status=? and subjectname like '"+subName+"' and orgname like '"+orgName+"' GROUP BY subjectid", SignLogResponse.class, new Object[]{ compid,compid,compid,taskid ,sts});
			}
	    }
	  }else {
		  if(userid!=null){
			  list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status=2 and subjectname like '"+subName+"' and orgname like '"+orgName+"'  and t.subjectid = "+userid+"  GROUP BY subjectid",
					  SignLogResponse.class,
					  new Object[]{compid,compid,compid,taskid});
		  }else{
			  list=generalSqlComponent.getDbComponent().listBean(getSignLonnum+ " and t.status=2 and subjectname like '"+subName+"' and orgname like '"+orgName+"' GROUP BY subjectid",
					  SignLogResponse.class,
					  new Object[]{compid,compid,compid,taskid});
		  }
	  }
	  return list;
	 }
	
	/**
	 * 删除任务（将任务状态更新为删除状态）
	 * @param id
	 */
	public void delete(Long compid,Long id) {
//		super.update("UPDATE tk_task_deploy SET status = 0 WHERE compid=? and id = ?", compid,id);
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_deploy SET status = 0 WHERE compid=? and id = ?", new Object[]{compid,id});
	}
	
	/**
	 * 修改任务基本信息，任务名称、备注、分类、起始时间等
	 * @param request
	 * @param compid
	 * @author yuxianzhu
	 */
	public void updateTaskBase(Long compid, TkTaskDeployRequest request){
//		super.update(updateTaskDeployBase, 
//				request.getTkname(), request.getTkdesc(),request.getClassid(),
//				DateEnum.YYYYMMDD.format(),compid,
//				request.getTaskid()
//		);
//		super.update(updateTaskDeployBase2,
//				request.getTkname(), request.getTkdesc(),request.getClassid(),request.getPublisher(),request.getTphone(),request.getAnnex(),
//				DateEnum.YYYYMMDD.format(),compid,
//				request.getTaskid()
//				);
		generalSqlComponent.getDbComponent().update(updateTaskDeployBase2,
				new Object[]{ request.getTkname(), request.getTkdesc(),request.getClassid(),request.getPublisher(),request.getTphone(),request.getAnnex(),
				DateEnum.YYYYMMDD.format(),compid,
				request.getTaskid()}
				);
	}
	
	
	public void updateTaskBaseByResolve(Long compid,TkTaskDeployRequest request){
//		super.update(updateTaskDeployBaseResolve,
//				request.getTkname(), request.getTkdesc(),request.getClassid(),
//				DateEnum.YYYYMMDD.format(),request.getTimedata(),compid,
//				request.getTaskid()
//		);
		generalSqlComponent.getDbComponent().update(updateTaskDeployBaseResolve,
				new Object[]{request.getTkname(), request.getTkdesc(),request.getClassid(),
				DateEnum.YYYYMMDD.format(),request.getTimedata(),compid,
				request.getTaskid()}
		);
	}
	
	/**
	 * 仅修改开始结束时间
	 * @param request
	 * @author yuxianzhu
	 */
	public void upadteStartEndTime(Long compid,TkTaskDeployRequest request){
//		super.update(updateStartEndTime,
//			request.getStarttime(),request.getEndtime(), request.getTimedata(),
//			DateEnum.YYYYMMDD.format(),compid,request.getTaskid()
//		);
		generalSqlComponent.getDbComponent().update(updateStartEndTime,
			new Object []{request.getStarttime(),request.getEndtime(), request.getTimedata(),
			DateEnum.YYYYMMDD.format(),compid,request.getTaskid()}
		);
	}
	
	/**
	 * 周期任务修改基本信息（包括时间）
	 * @param request
	 * @author yuxianzhu
	 */
	public void upadteTaskBasePeriod(Long compid,TkTaskDeployRequest request){
//		super.update(updateTaskBasePeriod,
//				request.getTkname(), request.getTkdesc(),request.getClassid(),
//				request.getStarttime(),request.getEndtime(),
//				DateEnum.YYYYMMDDHHMMDD.format(),request.getTimedata(),compid,
//				request.getTaskid()
//		);
		generalSqlComponent.getDbComponent().update(updateTaskBasePeriod,
				new Object[]{request.getTkname(), request.getTkdesc(),request.getClassid(),
				request.getStarttime(),request.getEndtime(),
				DateEnum.YYYYMMDDHHMMDD.format(),request.getTimedata(),compid,
				request.getTaskid()}
		);
	}

	/**
	 * 自定义表单
	 * @return
	 */
	public List<TkTaskFormRespond> listForm(Long compid) {
//		return super.list2bean("SELECT code prop, name label FROM tk_task_attribute where compid=? and checked = 1 order by seqno", TkTaskFormRespond.class,compid);
		return generalSqlComponent.getDbComponent().listBean("SELECT code prop, name label FROM tk_task_attribute where compid=? and checked = 1 order by seqno", TkTaskFormRespond.class,compid);
	}
	
	public List<TkTaskDeploy> listByIds(Long compid,String ids){
//		return super.list2bean("select * from tk_task_deploy where compid=? and status!=0 and id in ("+ids+")", TkTaskDeploy.class,compid);
		return generalSqlComponent.getDbComponent().listBean("select * from tk_task_deploy where compid=? and status!=0 and id in ("+ids+")", TkTaskDeploy.class,compid);
	}

	/**
	 * 专供暂停使用
	 * @param id
	 * @param status
	 * @param format
	 */
	public void updateStatusRemarks5(Long compid,Long id, int status, String format) {
		//super.update("UPDATE tk_task_deploy SET status=? , remarks5 =? WHERE compid=? and id=? and status != 50", status,format,compid,id);
//		super.update("UPDATE tk_task_deploy SET status=?  WHERE compid=? and id=? and status != 50", status,compid,id);
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_deploy SET status=?  WHERE compid=? and id=? and status != 50", new Object[]{status,compid,id});

	}
	
	/**
	 * 专供恢复使用
	 * @param id
	 * @param status
	 * @param format
	 */
	public void updateStatusRemarks6(Long compid,Long id, int status, String format) {
//		super.update("UPDATE tk_task_deploy SET status=? , remarks6 =? WHERE compid=? and id=? and status = 45", status,format,compid,id);
//		super.update("UPDATE tk_task_deploy SET status=?  WHERE compid=? and id=? and status = 45", status,compid,id);//remarks6存储前台的提醒时间json
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_deploy SET status=?  WHERE compid=? and id=? and status = 45",new Object[]{status,compid,id} );//remarks6存储前台的提醒时间json
	}

	public void updateTimeAndDiffdata(Long compid,Long taskid,String starttime, String endtime, String timedataresolve) {
//		super.update("UPDATE tk_task_deploy SET  starttime = ?, endtime = ?, diffdata = ? WHERE compid=? and id=?", starttime,endtime,timedataresolve,compid,taskid);
		generalSqlComponent.getDbComponent().update("UPDATE tk_task_deploy SET  starttime = ?, endtime = ?, diffdata = ? WHERE compid=? and id=?",
				new Object[]{starttime,endtime,timedataresolve,compid,taskid});
	}

	public List<TkTaskDeploy> list2bean(Long compid,String classid) {
//		return super.list2bean("SELECT id FROM tk_task_deploy where compid=? and classid in ("+classid+")", TkTaskDeploy.class,compid);
		return generalSqlComponent.getDbComponent().listBean("SELECT id FROM tk_task_deploy where compid=? and classid in ("+classid+")", TkTaskDeploy.class,compid);

	}

	public void updateByClassid(Long compid,Long oldClassid, Long newClassid) {
		generalSqlComponent.getDbComponent().update("update tk_task_deploy set classid = ? where compid=? and classid = ?", new Object[]{newClassid,compid,oldClassid});
	}
	
	
	
	
	
	public void updateRemarks3AndRemarks6(Session session,TkTaskDeployRequest request) {
		String requestjson = JSON.toJSONString(request);
//		super.update("update tk_task_deploy set remarks3 = ?,remarks6=? where compid=? and id = ?", requestjson,request.getTabTimedata(),session.getCompid(),request.getTaskid());
		generalSqlComponent.getDbComponent().update("update tk_task_deploy set remarks3 = ?,remarks6=? where compid=? and id = ?",
				new Object[]{requestjson,request.getTabTimedata(),session.getCompid(),request.getTaskid()});
	}

}
