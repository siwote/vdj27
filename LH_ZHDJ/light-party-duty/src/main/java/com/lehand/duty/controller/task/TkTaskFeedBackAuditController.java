package com.lehand.duty.controller.task;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.lehand.base.exception.LehandException;
import com.lehand.components.web.ann.OpenApi;
import com.lehand.duty.service.TkTaskFeedBackAuditService;
import com.lehand.duty.dto.FeedDetails;
import com.lehand.duty.dto.TaskRemindTimes;
import com.lehand.duty.dto.TreeSubjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehand.base.common.Message;
import com.lehand.base.common.Pager;
import com.lehand.duty.controller.DutyBaseController;

/**
 * 
 * 反馈审核
 * 
 * @author pantao
 * @date 2019年04月09日 下午14:29:56
 *
 */
@Api(value = "反馈审核", tags = { "反馈审核" })
@RestController
@RequestMapping("/duty/feedbaackaudit")
public class TkTaskFeedBackAuditController extends DutyBaseController {

	private static final Logger logger = LogManager.getLogger(TkTaskFeedBackAuditController.class);
	@Resource
	TkTaskFeedBackAuditService tkTaskFeedBackAuditService;
	
	
	/**
	 * 审核列表页面数据查询
	 * @param status 待审核传0  已审核传2
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 审核列表页面数据查询", notes = " 审核列表页面数据查询", httpMethod = "POST")
	@RequestMapping("/page")
	public Message page(Integer status,String tkname,String starttime,String endtime,String type, Pager pager) {
		Message message = new Message();
		try {	
			Pager page = tkTaskFeedBackAuditService.page(getSession(),status,tkname,starttime,endtime,type,pager);
			message.setData(page);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 反馈详情页面反馈审核
	 * @param auditids 审核表id
	 * @param status 审核状态 1通过，-1拒绝
	 * @param mesg 审核备注
	 * @param feedstatus 对应的反馈的状态： 0正常反馈，1逾期反馈，2逾期未报
	 * @param remindtime 对应的提醒时间点
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 反馈详情页面反馈审核", notes = " 反馈详情页面反馈审核", httpMethod = "POST")
	@RequestMapping("/audit")
	public Message audit(String auditids,int status,String mesg,Integer feedstatus,String remindtime) {
		Message message = new Message();
		try {
			tkTaskFeedBackAuditService.audit(getSession(),auditids, status, mesg,feedstatus,remindtime);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	
	/**
	 * 获取任务的提醒时间点集合(审核详情页面时间点下拉框和下面时间轴接口)
	 * @param taskid
	 * @param subjectid 执行人id
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 获取任务的提醒时间点集合(审核详情页面时间点下拉框和下面时间轴接口)", notes = " 获取任务的提醒时间点集合(审核详情页面时间点下拉框和下面时间轴接口)", httpMethod = "POST")
	@RequestMapping("/listremindtimes")
	public Message listRemindTimes(Long taskid,Long subjectid) {
		Message message = new Message();
		try {	
			List<TaskRemindTimes> data = tkTaskFeedBackAuditService.listRemindTimes(getSession().getCompid(), taskid,subjectid);
			message.success().setData(data);;
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
			LehandException.throwException("查询失败！"+e);
		}
		return message;		
	}
	
	/**
	 * 通过列表中的反馈id查询反馈对象详情
	 * @param feedbackid 反馈id
	 * @param flag 任务审核传1，反馈审核传0
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 通过列表中的反馈id查询反馈对象详情", notes = " 通过列表中的反馈id查询反馈对象详情", httpMethod = "POST")
	@RequestMapping("/getfeeddetials")
	public Message getFeedDetials(Long feedbackid,int flag) {
		Message message = new Message();
		try {	
			FeedDetails data = tkTaskFeedBackAuditService.getFeedDetials(getSession(), feedbackid,flag);
			message.success().setData(data);;
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	   * 反馈详情页面分页查询
	 * @param mytaskid 子任务id
	 * @param remindtime 提醒时间点
	 * @param auditstatus 反馈审核状态 0：待审核，1：通过，-1：驳回
	 * @param flag 我的任务和巡查页面接收的传0，其他的传1
	 * @param pager 分页查询
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 反馈详情页面分页查询", notes = " 反馈详情页面分页查询", httpMethod = "POST")
	@RequestMapping("/listfeeddetails")
	public Message listFeedDetails(Long mytaskid,String remindtime,Integer auditstatus,Integer flag,Long subjectid, Pager pager) {
		Message message = new Message();
		try {	
			Pager page = tkTaskFeedBackAuditService.listFeedDetails(getSession(), mytaskid,remindtime,auditstatus,flag,subjectid, pager);
			message.setData(page);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 查看审核历史详情页面专用接口
	 * @param mytaskid
	 * @param auditstatus
	 * @param auditid
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 查看审核历史详情页面专用接口", notes = " 查看审核历史详情页面专用接口", httpMethod = "POST")
	@RequestMapping("/listfeeddetailsaudit")
	public Message listFeedDetailsAudit(Long mytaskid,Integer auditstatus,Long auditid,Long feedbackid, Pager pager) {
		Message message = new Message();
		try {	
			Pager page = tkTaskFeedBackAuditService.listFeedDetailsAudit(getSession(), mytaskid,auditstatus,auditid,feedbackid, pager);
			message.setData(page);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	/**
	 * 单独获取签收的详情信息
	 * @param mytaskid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 单独获取签收的详情信息", notes = " 单独获取签收的详情信息", httpMethod = "POST")
	@RequestMapping("/listsignin")
	public Message listSignIn(Long mytaskid) {
		Message message = new Message();
		try {	
			List<Map<String, Object>> page = tkTaskFeedBackAuditService.getSignIn(mytaskid, getSession().getCompid());
			message.setData(page);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	/**
	   * 关联任务列表查询
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 关联任务列表查询", notes = " 关联任务列表查询", httpMethod = "POST")
	@RequestMapping("/listRelatTask")
	public Message listRelatTask(Long mytaskid,Long logid ) {
		Message message = new Message();
		try {
			List<Map<String,Object>> relatedTasks = tkTaskFeedBackAuditService.listRelatedTask(getSession(), mytaskid,logid);
			message.setData(relatedTasks);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	/**
	 * 反馈详情页面右侧获取执行人树结构数据
	 * @param taskid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 反馈详情页面右侧获取执行人树结构数据", notes = " 反馈详情页面右侧获取执行人树结构数据", httpMethod = "POST")
	@RequestMapping("/listtreesubjectNew")
	public Message listtreesubjectNew(Long taskid,String username,String noBack,String noDel) {
		Message message = new Message();
		try {
			TreeSubjectResponse data = tkTaskFeedBackAuditService.listTreeSubjectNew(getSession(), taskid,username,noBack,noDel);
			message.setData(data);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;
	}

	/**
	 * 反馈详情页面右侧获取执行人树结构数据
	 * @param taskid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 反馈详情页面右侧获取执行人树结构数据", notes = " 反馈详情页面右侧获取执行人树结构数据", httpMethod = "POST")
	@RequestMapping("/listtreesubject")
	public Message listTreeSubject(Long taskid,String username) {
		Message message = new Message();
		try {	
			TreeSubjectResponse data = tkTaskFeedBackAuditService.listTreeSubject(getSession(), taskid,username);
			message.setData(data);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	/**
	 * 审核日志
	 * @param feedbackid
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 审核日志", notes = " 审核日志", httpMethod = "POST")
	@RequestMapping("/listauditlog")
	public Message listAuditLog(Long feedbackid,Pager pager) {
		Message message = new Message();
		try {	
			Pager data = tkTaskFeedBackAuditService.listAuditLog(getSession(),feedbackid, pager);
			message.setData(data);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}

	
	/**
	   * 与我相关的列表数据
	 * @param tkname
	 * @param starttime
	 * @param endtime 
	 * @param status 办理中40 ，未签收30，暂停或完成  100
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 与我相关的列表数据", notes = " 与我相关的列表数据", httpMethod = "POST")
	@RequestMapping("/listrelatedtome")
	public Message listRelatedToMe(String tkname,String starttime,String endtime,int status,Pager pager) {
		Message message = new Message();
		try {	
			Pager data = tkTaskFeedBackAuditService.listRelatedToMe(getSession(),tkname,starttime,endtime,status, pager);
			message.setData(data);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	   * 任务巡查模块列表数据查询
	 * @param userid
	 * @param tkname
	 * @param starttime
	 * @param endtime
	 * @param classid 为空时 查所有的
	 * @param flag 0发起的，1接收的
	 * @param status 当flag为0时传已发任务的状态，当falg为1时传我的任务的状态
	 * @param pager
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 任务巡查模块列表数据查询", notes = " 任务巡查模块列表数据查询", httpMethod = "POST")
	@RequestMapping("/listworkinspection")
	public Message listWorkInspection(Long userid,String tkname,String starttime,String endtime,String classid,int flag,int status, Pager pager) {
		Message message = new Message();
		try {
			tkTaskFeedBackAuditService.listWorkInspection(getSession(), userid, tkname, starttime, endtime, classid, flag, status, pager);
			message.success().setData(pager);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	/**
	 * 任务链查询
	 * @param taskid
	 * @param flag 0表示查询根节点，1表示点击加号展示其下关联的任务
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 任务链查询", notes = " 任务链查询", httpMethod = "POST")
	@RequestMapping("/listtaskchain")
	public Message listTaskChain(Long taskid,int flag) {
		Message message = new Message();
		try {	
			List<Map<String, Object>> data = tkTaskFeedBackAuditService.listTaskChain(getSession(), taskid,flag);
			message.success().setData(data);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	/**
	  * 根据主任务id和反馈时间点查找对应的反馈时间点要求
	 * @param taskid
	 * @param remindtime
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 根据主任务id和反馈时间点查找对应的反馈时间点要求", notes = " 根据主任务id和反馈时间点查找对应的反馈时间点要求", httpMethod = "POST")
	@RequestMapping("/getfeedrequirement")
	public Message getFeedRequirement(Long taskid,String remindtime) {
		Message message = new Message();
		try {	
			String data = tkTaskFeedBackAuditService.getFeedRequirement(getSession(),taskid,remindtime);
			message.success().setData(data);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	/**
	   * 各个列表页面头部各个状态角标查询
	 * @param flag 1：已发任务,2我的任务,3任务审核,4：我相关的,5工作巡查
	 * @param status 每个页面最多3个状态  第一个状态传 1  第二个状态传2   第三个状态传3
	 * @param status 专门针对任务巡查模块中的 0:发起的，1接收的 （其他模块传0）
	 * @param subjectid 巡查专属传选中的userid
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 各个列表页面头部各个状态角标查询", notes = " 各个列表页面头部各个状态角标查询", httpMethod = "POST")
	@RequestMapping("/pagenum")
	public Message pageNum(int flag, int status,Long subjectid,Pager pager) {
		Message message = new Message();
		try {	
			Map<String,Object> map = tkTaskFeedBackAuditService.pageNum(getSession(),flag,status,subjectid,pager);
			message.success().setData(map);
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
	
	
	
	/**================================================2019-08-05 交控2期新增接口======================================================================*/
	
	
	
	/**
	 * 任务下发审核
	 * @param auditids
	 * @param status
	 * @param mesg
	 * @return
	 */
	@OpenApi(optflag=0)
	@ApiOperation(value = " 任务下发审核", notes = " 任务下发审核", httpMethod = "POST")
	@RequestMapping("/auditTask")
	public Message auditTask(String auditids,int status,String mesg) {
		Message message = new Message();
		try {
			tkTaskFeedBackAuditService.auditTask(getSession(),auditids, status, mesg);
			message.success();
		} catch (Exception e) {
			message.setMessage(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return message;		
	}
}
