package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.component.DutyCacheComponent;
import com.lehand.duty.pojo.TkRemindRule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
   *   提醒规则接口实现类
 * 
 * @author pantao  
 * @date 2018年10月9日 下午9:17:59
 *
 */
@Service
public class TkRemindRuleService {
	
	private static final Logger logger = LogManager.getLogger(TkRemindRuleService.class);
	@Resource private DutyCacheComponent dutyCacheComponent;
	@Resource
	private GeneralSqlComponent generalSqlComponent;
	
	/**
	 * 
	 *   提醒规则 更新 实现类
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 上午10:26:06
	 *  
	 * @param listMap
	 * @return
	 * @throws LehandException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(rollbackFor=Exception.class)
	public void update(Long compid,List<Map> listMap) throws LehandException {
		String code = "";
		String nextData = "'"+"'";
		String endData = "'"+"'";
		updateAllStatus2Zero(compid);
		if(listMap.size()>0) {
			for(Map<String,Object> map : listMap) {									 
				code += map.get("code")+",";
				if("BACK_NEXT_BEFORE".equals(map.get("code"))) {
					nextData =map.get("data").toString();
				}
				if("BACK_END_BEFORE".equals(map.get("code"))) {
					endData = map.get("data").toString();
				}
			}
			code = code.substring(0, code.length()-1);

			generalSqlComponent.update(SqlCode.updateTkRemindRule, new Object[] {compid, code});

			//UPDATE TK_REMIND_RULE SET PARAMS = CASE CODE  WHEN 'BACK_NEXT_BEFORE' THEN "+nextData+" WHEN 'BACK_END_BEFORE' THEN "+endData+" END  WHERE CODE IN ("+code+") and compid="+compid
			generalSqlComponent.update(SqlCode.updateTkRemindRuleOpt, new Object[] {nextData, endData, code, compid});
			logger.info("提醒规则更新成功！");
		}
		dutyCacheComponent.cleanTkRemindRulesCache(compid);
	}
	
	
	/**
	 * 
	   *   提醒规则 查询  
	 * 
	 * @author pantao  
	 * @date 2018年10月10日 上午10:49:42
	 *  
	 * @param isview
	 * @return
	 * @throws LehandException
	 */
	public  List<Map<String, Object>> listByStatus(Long compid,int isview) throws LehandException{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		List<TkRemindRule> lists = findByIsViewOrderSeqno(compid,isview);
		if(lists == null || lists.size()<=0) {
			LehandException.throwException("初始化数据不存在！");
		}
		Map<String, Object> line = null;
		Boolean flag = false;
		for(TkRemindRule list : lists) {
			String  code = list.getCode();
			Integer status = list.getStatus();
			if(status == 1) {
				flag= true;
			}else {
				flag = false;
			}
			String rulename = list.getRulename();
			Object params = JSON.parse(list.getParams());
			line = new HashMap<String, Object>(3);
			line.put("id", code);
			line.put("status", status);
			line.put("rulename", rulename);
			line.put("checked", flag);
			line.put("param", params);
			result.add(line);
		}
		return result;
	}

	public List<TkRemindRule> findByIsViewOrderSeqno(Long compid,int isview){
		return generalSqlComponent.query(SqlCode.findByIsviewOrderSeqno,new Object[] {compid,isview});
	}

	public int updateAllStatus2Zero(Long compid) {
		return generalSqlComponent.update(SqlCode.updateTkRemindRuleStatus, new Object[] {compid});
	}

	/**
	 * 查询所有提醒规则
	 * @return
	 */
	public List<TkRemindRule> findAll(Long compid) {
		return generalSqlComponent.query(SqlCode.queryTkRemindRuleAll, new Object[] {compid});
	}

	/**
	 * 查询提醒规则信息
	 * @param code
	 * @return
	 */
	public TkRemindRule get(Long compid,String code) {
		return generalSqlComponent.query(SqlCode.getTkRemindRule, new Object[] { compid,code});
	}

}
