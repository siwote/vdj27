package com.lehand.duty.service;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.CommentReplyRequest;
import com.lehand.duty.pojo.TkComment;
import com.lehand.duty.pojo.TkCommentReply;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TkCommentReplyService {

	@Resource
	private GeneralSqlComponent generalSqlComponent;

	public List<TkCommentReply> list(Long commid){
		return generalSqlComponent.query(SqlCode.queryTkCommentReplyList, new Object[] {commid});
	}
	
	public TkCommentReply insert(TkCommentReply info) {
		Long id = generalSqlComponent.insert(SqlCode.saveTkCommentReply, info);
		info.setId(id);
		return info;
	}

	public void delete(Long compid,Long id) {
		generalSqlComponent.delete(SqlCode.delTkCommentReply, new Object[] {compid,id});
	}
	
	public TkCommentReply insert(TkComment comment, CommentReplyRequest request, Session session) throws LehandException{
		TkCommentReply info = new TkCommentReply();
		info.setCompid(session.getCompid());
		info.setTaskid(comment.getTaskid());//主任务ID
		info.setMytaskid(comment.getMytaskid());//我的任务ID
		info.setCommid(comment.getId());//某个模块相应的ID,如任务发布
		info.setRepsjtype(0);//回复主体类型
		info.setRepsjid(session.getUserid());//回复主体ID
		info.setRsjname(session.getUsername());//回复人名称
		info.setSjtype(comment.getFsjtype());//评论主体类型(0:用户,1:机构/组/部门,2:角色)
		info.setSubjectid(comment.getFsubjectid());//评论主体ID(用户ID、角色ID、机构ID等等)
		info.setSubjectname(comment.getFsubjectname());//评论主体名称
		info.setCommtime(comment.getCommtime());//评论时间
		info.setReplytime(DateEnum.YYYYMMDDHHMMDD.format());//回复时间
		info.setReplyflag(request.getReplyflag());//回复类型(0:文本,1:语音)
		info.setReplytext(request.getReplytext());//回复内容
		info.setRemarks1(0);//备注1
		info.setRemarks2(0);//备注2
		info.setRemarks3(Constant.EMPTY);//备注3
		info.setRemarks4(Constant.EMPTY);//备注4
		info.setRemarks5(Constant.EMPTY);//备注5
		info.setRemarks6(Constant.EMPTY);//备注6
		insert(info);
		return info;
	}
	
	/**
	 * 获取回复的消息
	 * @param id
	 * @return
	 */
	public TkCommentReply get(Long compid,Long id) {
		return generalSqlComponent.query( SqlCode.queryTkCommentReplyById, new Object[] {compid,id});
	}
	
	public void deleteByTaskid(Long compid,Long taskid) {
		generalSqlComponent.delete(SqlCode.delTkCommentReplyBytId, new Object[] {compid,taskid});
	}

	public List<TkCommentReply> listByTaskid(Long compid,Long taskid) {
		return generalSqlComponent.query(SqlCode.queryTkCommentReplyBytId, new Object[] {compid,taskid});
	}
	
	public void deleteByMyTaskid(Long compid, Long taskid,Long userid) {
		generalSqlComponent.delete(SqlCode.delTkCommentReplyByteid, new Object[] {compid,taskid,userid});
	}
}
