package com.lehand.duty.service;

import com.lehand.base.common.Pager;
import com.lehand.base.common.Session;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dao.*;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.dto.*;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

/**
   * 反馈详情类
 * @author pantao
 * @date 2018年11月29日下午1:29:35
 *
 */
@Service
public class TaskDetailsService {
	
	@Resource private TkMyTaskLogService tkMyTaskLogDao;
	@Resource private TkAttachmentService tkAttachmentService;
	@Resource private TkCommentService tkCommentDao;
	@Resource private TkCommentReplyService tkCommentReplyDao;
	@Resource private TkMyTaskDao tkMyTaskDao;
	@Resource private TkTaskDeployDao tkTaskDeployDao;
	@Resource private TkTaskRemindNoteService tkTaskRemindNoteService;
	@Resource private TkTaskTimeNodeService tkTaskTimeNodeService;
	@Resource private TkTaskSubjectService tkTaskSubjectService;
	@Resource private TkPraiseDetailsService tkPraiseDetailsDao;
	@Resource private TkIntegralDetailsService tkIntegralDetailsService;
	@Resource GeneralSqlComponent generalSqlComponent;
	/**
	 * 我的反馈详情
	 * @author pantao
	 * @date 2018年11月29日下午1:30:12
	 *
	 * @param myid
	 * @param pager
	 * @param session true 转码后  false转码前
	 * @return
	 */
	public List<TkMyTaskLogResponse> page(Long myid, Pager pager, Session session) {
		List<TkMyTaskLogResponse> tkMyTaskLogs = new ArrayList<TkMyTaskLogResponse>();
		List<TkMyTaskLog> listLogs = tkMyTaskLogDao.list(session.getCompid(),myid,pager);
		if(listLogs==null || listLogs.size()<=0) {
			return new ArrayList<TkMyTaskLogResponse>();
		}
		TkMyTaskLogResponse tkMyTaskLogResponse = null;
		for (TkMyTaskLog tkMyTaskLog : listLogs) {
			tkMyTaskLogResponse = oneFeedDetials(myid, session, tkMyTaskLog);
			tkMyTaskLogs.add(tkMyTaskLogResponse);
		}
		return tkMyTaskLogs;
	}

	private TkMyTaskLogResponse oneFeedDetials(Long myid, Session session, TkMyTaskLog tkMyTaskLog) {
		
		//TODO 根据反馈id到反馈审核表中查询（可能会存在多条记录）
		//判断是否存在 待审核状态（即status=0）如果存在 则该反馈的状态设置为 待审核状态，
		//反之判断是否存在拒绝状态（status=-1）如果存在 则该反馈的状态设置为拒绝状态
		//最后既不存在待审核状态也不存在拒绝状态 则该反馈状态设置为通过状态（status=1）
		//if(待审核状态){status=0}else{if(拒绝状态){status=-1}else{status=1}}
		//tkMyTaskLogResponse.setAuditstatus(status);
		
		TkMyTaskLogResponse tkMyTaskLogResponse;
		TkCommentResponse tkCommentResponse;
		List<TkCommentResponse> comments = new ArrayList<TkCommentResponse>();
		//退回原因
		TkMyTask tkMyTask=tkMyTaskDao.get(session.getCompid(),tkMyTaskLog.getMytaskid());
		tkMyTaskLogResponse = new TkMyTaskLogResponse();
		tkMyTaskLogResponse.setBackReason(tkMyTask.getRemarks3());
		tkMyTaskLogResponse.setId(tkMyTaskLog.getId());
		tkMyTaskLogResponse.setProgress(tkMyTaskLog.getProgress());
		tkMyTaskLogResponse.setMesg(tkMyTaskLog.getMesg());
		tkMyTaskLogResponse.setOpttime(tkMyTaskLog.getOpttime());
		tkMyTaskLogResponse.setUsername(tkMyTaskLog.getUsername());
		tkMyTaskLogResponse.setIsshow(false);
		tkMyTaskLogResponse.setIsopen(false);
		tkMyTaskLogResponse.setOpttype(tkMyTaskLog.getOpttype());
		//点赞相关
		int num = tkPraiseDetailsDao.getNum(session.getCompid(),tkMyTaskLog.getId());
		tkMyTaskLogResponse.setParisenum(num);
		int count = tkPraiseDetailsDao.getCount(tkMyTaskLog.getId(), session);
		if(count<=0) {//0表示登录人没有点赞该反馈
			tkMyTaskLogResponse.setFlag(0);
		}else {
			tkMyTaskLogResponse.setFlag(1);
		}
		if(StringUtils.isEmpty(tkMyTaskLog.getFileuuid())) {
			tkMyTaskLogResponse.setAttachment(new ArrayList<TkAttachment>(0));
		}else {
			List<TkAttachment> listAttachments = tkAttachmentService.list(session.getCompid(),tkMyTaskLog.getFileuuid());
			tkMyTaskLogResponse.setAttachment(listAttachments);//这里面的remarks1 被用为语音评论的时长
		}
		tkMyTaskLogResponse.setCommentnum(tkMyTaskLog.getCommcount());
		if(tkMyTaskLog.getCommcount()>0) {
			//控制创建人和督办人只能看到自己的评论，而执行人能看到所有评论
			TkMyTask mytask = tkMyTaskDao.get(session.getCompid(),myid);
			List<TkComment> listCommens = null;
			if(mytask.getSubjectid().equals(session.getUserid())) {//执行人看到的评论
				 listCommens = tkCommentDao.list(session.getCompid(),tkMyTaskLog.getId());
			}else {
				 listCommens = tkCommentDao.listBySession(session.getCompid(),tkMyTaskLog.getId(),session.getUserid());
			}
			if(listCommens==null || listCommens.size()<=0) {
				tkMyTaskLogResponse.setComment(new ArrayList<TkCommentResponse>(0));
			}else {
				for (TkComment tkComment : listCommens) {
					tkCommentResponse = new TkCommentResponse();
					tkCommentResponse.setId(tkComment.getId());
					tkCommentResponse.setCommflag(tkComment.getCommflag());
//						if(tkComment.getCommflag()==1) {//是语音就加.mp3
//							tkCommentResponse.setCommtext(tkComment.getCommtext()+".mp3");
//						}else {
						tkCommentResponse.setCommtext(tkComment.getCommtext());
//						}
					tkCommentResponse.setDuration(tkComment.getRemarks1());
					tkCommentResponse.setCommtime(tkComment.getCommtime());
					tkCommentResponse.setFsubjectname(tkComment.getFsubjectname());
					tkCommentResponse.setModel(tkComment.getModel());
					tkCommentResponse.setModelid(tkComment.getModelid());
					List<TkCommentReply>  listReplys = tkCommentReplyDao.list(tkComment.getId());
					if(listReplys==null || listReplys.size()<=0) {
						tkCommentResponse.setReply(new ArrayList<TkCommentReply>(0));
					}else {
						tkCommentResponse.setReplynum(listReplys.size());
						tkCommentResponse.setReply(listReplys);
					}
					comments.add(tkCommentResponse);
				}
				tkMyTaskLogResponse.setComment(comments);
				tkMyTaskLogResponse.setCommentnum(comments.size());
			}
		}
		return tkMyTaskLogResponse;
	}
	
	/**
	 * 前台局部刷新使用，查询某条反馈以及其下的评论和回复
	 * @param feedid
	 * @param session
	 * @return
	 */
	public TkMyTaskLogResponse refreshPage(Long feedid,Session session) {
		TkMyTaskLog tkMyTaskLog = tkMyTaskLogDao.get(session.getCompid(),feedid);
		TkMyTaskLogResponse tkMyTaskLogResponse = oneFeedDetials(tkMyTaskLog.getMytaskid(), session, tkMyTaskLog);
		return tkMyTaskLogResponse;
	}

	/**
	 * 我的任务查询下一阶段的提醒时间（普通任务反馈详情页面）
	 * @author pantao
	 * @date 2018年11月21日上午11:53:07
	 *
	 * @param myid
	 * @return
	 * @throws ParseException
	 */
	public String get(Long compid,Long myid) throws ParseException {
		TkMyTask tkMyTask=tkMyTaskDao.get(compid,myid);
		List<TkTaskRemindNote> note = tkTaskRemindNoteService.listRemindByTaskId(compid,tkMyTask.getTaskid());
		String time = getNextRemindTime(compid,note,myid);
		return time;
	}

	/**
	 * 我的任务查询周期任务的时间节点
	 * @author pantao
	 * @date 2018年11月21日下午12:53:22
	 *
	 * @param myid
	 * @param pager
	 * @return
	 */
	public TimeNodeResponse list(Long compid, Long myid, Pager pager, Long userid) {
		TkMyTask tkMyTask=tkMyTaskDao.get(compid,myid);
		List<TimeResponse> times = tkMyTaskDao.list(compid,tkMyTask.getTaskid(),pager,userid);
		TimeNodeResponse timeNode = new TimeNodeResponse();
		timeNode.setTimes(times);
		return timeNode;
	}
	
	/**
	 * 我的任务反馈详情周期任务时间节点总条数
	 * @param myid
	 * @param userid
	 * @return
	 */
	public int countByTaskIdAndSubjectId(Long compid,Long myid,Long userid) {
		TkMyTask tkMyTask=tkMyTaskDao.get(compid,myid);
	    int num  = tkMyTaskDao.countBytaskIdAndSubjectId(compid,tkMyTask.getTaskid(),userid);
		return num;
	}

	public int count(Long compid,Long myid) {
		return tkMyTaskLogDao.count(compid,myid);
	}
	
	/**
	 * 已部署任务时间节点集合
	 * @author pantao
	 * @date 2018年11月30日下午4:24:48
	 * 
	 * @param taskid
	 * @param pager
	 * @param compid
	 * @return
	 */
	public TimeNodeResponse deployedList(Long compid,Long taskid, Pager pager) {
		TimeNodeResponse timeNode = new TimeNodeResponse();
		List<TimeResponse> mytasks = tkMyTaskDao.listByTaskid(compid,taskid, pager);
		//办理中（有办理中的并且没有未签收的就为办理中），退回（全部退回就为退回），已完成（全部完成就为完成），未签收（有未签收的就为未签收）
		for (TimeResponse timeResponse : mytasks) {
			//有未签收的就为未签收
			int count1 = tkMyTaskDao.listTaskidAndEndtimeByStatus(compid,taskid,timeResponse.getEndtime(),Constant.TkMyTask.STATUS_NO_SGIN);
			if(count1>0) {
				timeResponse.setStatus(Constant.TkMyTask.STATUS_NO_SGIN);
			}else {
				//前提：没有未签收的
				int count2 = tkMyTaskDao.listTaskidAndEndtimeByStatus(compid,taskid,timeResponse.getEndtime(),Constant.TkMyTask.STATUS_SGIN);
				//存在办理中的
				if(count2>0) {
					timeResponse.setStatus(Constant.TkMyTask.STATUS_SGIN);
				}else {
					//前提：没有未签收也没有办理中的
					int count3 = tkMyTaskDao.listTaskidAndEndtimeByStatus(compid,taskid,timeResponse.getEndtime(),Constant.TkMyTask.STATUS_FINISH);
					//存在已完成的
					if(count3>0) {
						timeResponse.setStatus(Constant.TkMyTask.STATUS_FINISH);
					}else {
						//前提：不存在未签收，办理中，已完成的即全部退回了
						timeResponse.setStatus(Constant.TkMyTask.STATUS_BACK);
					}
				}
			}
		}
		timeNode.setTimes(mytasks);
		return timeNode;
	}
	
	/**
	 * 已部署任务反馈详情中周期任务时间节点总条数
	 * @param taskid
	 * @return
	 */
	public int countByTaskId(Long compid,Long taskid) {
	    int num  = tkMyTaskDao.countBytaskId(compid,taskid);
		return num;
	}
	
	
	/**
	 * 已部署任务反馈详情页面普通任务的下一个提醒时间(PC端和小程序领导版)
	 * @author pantao
	 * @date 2018年12月3日上午10:10:06
	 * 
	 * @param taskid
	 * @return
	 * @throws ParseException
	 */
	public String getRemindTime(Long compid , Long taskid , Long subjectid) throws ParseException {
		TkMyTask tkMyTask=tkMyTaskDao.getMyTaskIdPeriod(compid,taskid, subjectid);		
		List<TkTaskRemindNote> note = tkTaskRemindNoteService.listRemindByTaskId(compid,taskid);
		String time = getNextRemindTime(compid,note,tkMyTask.getId());
		return time;
	}

	private String getNextRemindTime(Long compid,List<TkTaskRemindNote> note,Long mytaskid) throws ParseException {
		if(note==null || note.size()<=0) {
			return "暂无反馈时间";
		}
		for (int i = 0; i < note.size(); i++) {
			Date date = DateEnum.YYYYMMDD.parse(note.get(i).getRemindtime());
			Date enddate = DateEnum.YYYYMMDD.parse(note.get(note.size()-1).getRemindtime());
			Date now = DateEnum.now();
			//提醒时间大于当前时间
			if(date.getTime()>now.getTime()) {
				return note.get(i).getRemindtime().substring(0,10);
			}
			//最后一次提醒时间小于等于当前时间
			else if(enddate.getTime()<=now.getTime()) {
				//查询下是否有反馈记录
				int num = tkMyTaskLogDao.listFeedBack(compid,mytaskid);
				if(num<=0) {
					return "未反馈";
				}
			}
		}
		return "反馈完成";
		
	}
	
	
	/**
	 * 已部署任务反馈详情列表中获取任务执行人集合
	 * @author pantao
	 * @date 2018年12月3日上午10:52:11
	 * 
	 * @param taskid
	 * @return
	 */
	public List<StatusSubject> listDepolySubjects(Long compid,Long taskid,int flag,Pager pager,String name){//TODO
//		List<StatusSubject> subjects = tkTaskSubjectService.findSubjectsPage(compid,taskid,flag,pager,name);
//		return subjects;
		return null;
	}
	
	/**
	 * 查询执行人总数
	 * @param taskid
	 * @param flag
	 * @param name
	 * @return
	 */
	public int count(Long compid,Long taskid,int flag,String name){
		int num = tkTaskSubjectService.count(compid,taskid,flag,name);
		return num;
	}
	
	/**
	 * 通过任务id和执行人id查询我的任务id
	 * @author pantao
	 * @date 2018年12月3日上午11:33:28
	 * 
	 * @param taskid
	 * @param subjectid
	 * @param date
	 * @param period
	 * @return
	 * @throws LehandException
	 */
	public Long getMyTaskId(Long compid,Long taskid,Long subjectid,String date,int period) throws LehandException {
		TkMyTask mytask = null;
		if(period == 1) {//周期
			mytask = tkMyTaskDao.getMyTaskId(compid,taskid,subjectid,date);
		}else if(period==0){//普通
			mytask = tkMyTaskDao.getMyTaskIdPeriod(compid,taskid,subjectid);
		}else {
			LehandException.throwException("任务类型错误");
		}
		return mytask.getId();
	}
	
	/**
	 * 评论
	 * @param request
	 * @param session
	 * @throws LehandException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void insertComment(TaskProcessEnum taskProcessEnum, CommentRequest request, Session session) throws LehandException {
		taskProcessEnum.handle(session, request,session);
	}
	
	/**
	 * 删除评论
	 */
	@Transactional(rollbackFor=Exception.class)
	public void deleteComment(Long compid,Long id) {
		tkCommentDao.delete(compid,id);
	}
	
	/**
	 * 获取签收日志数据，
	 * 传入taskid， 任务id
	 * orgNameLike 组织机构名称（模糊查询）
	 * subName		执行人名称（模糊查询）
	 * @param flag 小程序传1 PC传0
	 */
	public List<SignLogResponse> getSignLog(Long compid,String status,Long taskid, String orgNameLike, String subName,Pager pager,int period,String datenode,int flag,Long userid) {
		List<SignLogResponse> data=new ArrayList<SignLogResponse>();
		if(StringUtils.isEmpty(orgNameLike)){
			orgNameLike="%%";
		}else {
			orgNameLike="%"+orgNameLike+"%";
		}
		if(StringUtils.isEmpty(subName)){
			subName="%%";
		}else {
			subName="%"+subName+"%";
		}
		List<TkMyTask> list=tkMyTaskDao.getBytkidAndlike(compid,status,taskid,subName,orgNameLike,pager,period,datenode,flag,userid);
		for (int i=0;i<list.size();i++){
			SignLogResponse signLog=new SignLogResponse();
			//从我的任务操作日志表获取签收时间;
			TkMyTaskLog TkMyTaskLog=tkMyTaskLogDao.getSignTime(compid,taskid, list.get(i).getId());
			//从组织机构表获取组织机构名称
			String orgName=generalSqlComponent.query(SqlCode.getOrgnameLike, new Object[] {compid,compid,list.get(i).getSubjectid(), orgNameLike});
			//是否已反馈
			Long feedCount = generalSqlComponent.query(SqlCode.getMyFeedCount, new Object[]{compid, taskid, list.get(i).getSubjectid()});
			feedCount = feedCount == null ? 0 : feedCount;
			//往dto对应字段塞值
            signLog.setMytaskId(list.get(i).getId());
			signLog.setSubjectid(list.get(i).getSubjectid());
			signLog.setSubjectname(list.get(i).getSubjectname());
			signLog.setOrgname(orgName);
			Integer logStatus = list.get(i).getStatus();
			signLog.setStatus(logStatus);
			signLog.setHasfeeded(feedCount == 0 ? 0 :1);
			if (StringUtils.isEmpty(TkMyTaskLog)||logStatus == 2){
				signLog.setSigntime(Constant.EMPTY);
			}else {
				signLog.setSigntime(TkMyTaskLog.getOpttime());
			}
			//if (!StringUtils.isEmpty(orgName)){
				data.add(signLog);
			//}		
		}
		return data;
	}
	
	public int getSignLogNum(Long compid,String status ,long taskid,String subNameLike,String orgNameLike,Long userid){
		if(StringUtils.isEmpty(subNameLike)){
			subNameLike="%%";
		}else {
			subNameLike="%"+subNameLike+"%";
		}
		if(StringUtils.isEmpty(orgNameLike)){
			orgNameLike="%%";
		}else {
			orgNameLike="%"+orgNameLike+"%";
		}
		List<SignLogResponse> list =tkTaskDeployDao.getSignLogNum(compid,status,taskid, subNameLike, orgNameLike,userid);
		return list.size();
	}
	/**
	 * 签收日志状态选择下拉框数据提供
	 * @param taskid
	 * @return
	 * @author yuxianzhu
	 */
	public List<Map<String, Object>> getStatusCount(Long compid,Long taskid,int period,String datenode){
		Map<String, Object> map = tkMyTaskDao.getSignStatus(compid,taskid,period,datenode);
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		HashMap<String, Object> allMap = new HashMap<>(1);
		allMap.put("status",100);
		allMap.put("name","全部");
		allMap.put("number",(Long)map.get("2")+(Long)map.get("3"));
		data.add(allMap);
		data.add(getMap("3", "已签收", map));
		data.add(getMap("2", "未签收", map));
		return data;
	}
	
	private static Map<String, Object> getMap(String status, String name, Map<String, Object> map) {
		Map<String, Object> maps = new HashMap<String, Object>(3);
		maps.put("status", status);
		maps.put("name", name);
		maps.put("number", map.get(status));
		return maps;
	}

	/**
	 * 查看退回原因
	 */
	public TkMyTask getRemarks3(Long compid,Long mytaskid) {
		TkMyTask tkMyTask = tkMyTaskDao.get(compid,mytaskid);
		return tkMyTask;
	}
	/**
	 * 新增反馈时要将之前反馈过的最新进度查出来并显示
	 * @param mytaskid
	 * @return
	 */
	public Double getProgress(Long compid,Long mytaskid) {
		TkMyTaskLog tkMyTaskLog=tkMyTaskLogDao.getProgress(compid,mytaskid);
		if(StringUtils.isEmpty(tkMyTaskLog)) {
			return 0.0;
		}
		return tkMyTaskLog.getProgress();
	}

	/**
	 * 反馈获得点赞
	 * @throws LehandException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public void getParise(Long feedid,Session session,TaskProcessEnum taskProcessEnum) throws LehandException {
		TkMyTaskLog tkMyTaskLog=tkMyTaskLogDao.get(session.getCompid(),feedid);
		taskProcessEnum.handle(session, tkMyTaskLog);
	}

	/**
	 * 反馈取消点赞
	 */
	@Transactional(rollbackFor=Exception.class)
	public void awayParise(Long feedid,Session session) {
		//删除点赞表中数据
		tkPraiseDetailsDao.delete(feedid,session);
		//删除积分详情表中数据
		tkIntegralDetailsService.deletePraise(session.getCompid(),feedid);
	}

	/**
	 * 获取点赞数量
	 */
	public Map<String,Object> getNum(Long feedid,Session session) {
		Map<String , Object> map = new HashMap<String, Object>();
		int num =  tkPraiseDetailsDao.getNum(session.getCompid(),feedid);
		int count = tkPraiseDetailsDao.getCount(feedid, session);
		if(count<=0) {
			map.put("flag", 0);
		}else {
			map.put("flag", 1);
		}
		map.put("parisenum", num);
		return map;
	}
	public static void main(String[] args) {
		Long a = 10l;
		Long b = 10l;
		System.out.println(a==b);
	}

}
