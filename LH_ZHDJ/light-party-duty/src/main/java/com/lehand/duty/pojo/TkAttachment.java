package com.lehand.duty.pojo;

public class TkAttachment extends FatherPojo {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.model
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private Integer model;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.id
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private Long id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.attaname
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String attaname;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.path
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String path;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.size
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private Long size;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.suffix
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String suffix;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.remarks1
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private Integer remarks1;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.remarks2
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private Integer remarks2;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.remarks3
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String remarks3;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.remarks4
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String remarks4;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.remarks5
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String remarks5;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tk_attachment.remarks6
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	private String remarks6;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.model
	 * @return  the value of tk_attachment.model
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public Integer getModel() {
		return model;
	}
	
	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.path
	 * @return  the value of tk_attachment.path
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getPath() {
		return path;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.path
	 * @param path  the value for tk_attachment.path
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setPath(String path) {
		this.path = path == null ? null : path.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.size
	 * @return  the value of tk_attachment.size
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public Long getSize() {
		return size;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.size
	 * @param size  the value for tk_attachment.size
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setSize(Long size) {
		this.size = size;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.suffix
	 * @return  the value of tk_attachment.suffix
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.suffix
	 * @param suffix  the value for tk_attachment.suffix
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix == null ? null : suffix.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.remarks1
	 * @return  the value of tk_attachment.remarks1
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public Integer getRemarks1() {
		return remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.remarks1
	 * @param remarks1  the value for tk_attachment.remarks1
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setRemarks1(Integer remarks1) {
		this.remarks1 = remarks1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.remarks2
	 * @return  the value of tk_attachment.remarks2
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public Integer getRemarks2() {
		return remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.remarks2
	 * @param remarks2  the value for tk_attachment.remarks2
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setRemarks2(Integer remarks2) {
		this.remarks2 = remarks2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.remarks3
	 * @return  the value of tk_attachment.remarks3
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getRemarks3() {
		return remarks3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.remarks3
	 * @param remarks3  the value for tk_attachment.remarks3
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3 == null ? null : remarks3.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.remarks4
	 * @return  the value of tk_attachment.remarks4
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getRemarks4() {
		return remarks4;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.remarks4
	 * @param remarks4  the value for tk_attachment.remarks4
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setRemarks4(String remarks4) {
		this.remarks4 = remarks4 == null ? null : remarks4.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.remarks5
	 * @return  the value of tk_attachment.remarks5
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getRemarks5() {
		return remarks5;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.remarks5
	 * @param remarks5  the value for tk_attachment.remarks5
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setRemarks5(String remarks5) {
		this.remarks5 = remarks5 == null ? null : remarks5.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.remarks6
	 * @return  the value of tk_attachment.remarks6
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getRemarks6() {
		return remarks6;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.remarks6
	 * @param remarks6  the value for tk_attachment.remarks6
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setRemarks6(String remarks6) {
		this.remarks6 = remarks6 == null ? null : remarks6.trim();
	}
	
	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.model
	 * @param model  the value for tk_attachment.model
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setModel(Integer model) {
		this.model = model;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.id
	 * @return  the value of tk_attachment.id
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public Long getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.id
	 * @param id  the value for tk_attachment.id
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tk_attachment.attaname
	 * @return  the value of tk_attachment.attaname
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public String getAttaname() {
		return attaname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tk_attachment.attaname
	 * @param attaname  the value for tk_attachment.attaname
	 * @mbg.generated  Tue Oct 16 12:29:57 CST 2018
	 */
	public void setAttaname(String attaname) {
		this.attaname = attaname == null ? null : attaname.trim();
	}
}