package com.lehand.duty.service;

import com.alibaba.fastjson.JSON;
import com.lehand.base.common.Pager;
import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.components.db.GeneralSqlComponent;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.Module;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.common.modulemethod.FilesPreviewAndDownload;
import com.lehand.duty.dao.*;
import com.lehand.duty.dto.*;
import com.lehand.duty.dto.urc.UrcOrgan;
import com.lehand.duty.dto.urc.UrcUser;
import com.lehand.duty.pojo.*;
import com.lehand.todo.constant.Todo;
import com.lehand.todo.pojo.TodoItemDTO;
import com.lehand.todo.service.TodoItemService;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

/**
 * 任务审核模块以及分页查询各模块实现层
 * 
 * @author pantao
 *
 */
@Service
public class TkTaskFeedBackAuditService {

	// 预览和上传的路径
	@Value("${jk.manager.down_ip}")
	private String downIp;
    @Resource
    FilesPreviewAndDownload filesPreviewAndDownload;
	@Resource
	private GeneralSqlComponent generalSqlComponent;
	@Resource
	private TkTaskDeployService tkTaskDeployService;
	@Resource
	private TkMyTaskService tkMyTaskService;
	@Resource
	private TkMyTaskLogService tkMyTaskLogDao;
	@Resource
	private TkTaskDeploySubDao tkTaskDeploySubDao;
	@Resource
	private TkTaskTimeNodeService tkTaskTimeNodeService;
	@Resource
	private TkTaskDeployDao tkTaskDeployDao;
	@Resource
	private TkTaskDeployService tkTaskDeployBusinessImpl;
	@Resource
	private TkTaskClassDao tkTaskClassDao;

	private static List<Map<String, Object>> childPwOrgan = new ArrayList<Map<String, Object>>();
	private boolean flag = false;

	/**
	 * 审核列表页面数据源
	 * 
	 * @author pantao
	 */
	public Pager page(Session session, Integer status, String tkname, String starttime, String endtime, String type,
			Pager pager) {
		// 数据库时间都是19位这里如果不带后面的时分秒当起始时间为同一天时就查不出数据
		if (starttime.length() == Constant.STATUS_10) {
			starttime = starttime + " 00:00:00";
		}
		if (endtime.length() == Constant.STATUS_10) {
			endtime = endtime + " 23:59:59";
		}
		if (StringUtils.isEmpty(type)) {
			Map<String, Object> param = new HashedMap<String, Object>(1);
			param.put("tkname", tkname);
			param.put("starttime", starttime);
			param.put("endtime", endtime);
			param.put("subjectid", session.getUserid());
			param.put("status", status);
			Pager pager1 = generalSqlComponent.pageQuery(SqlCode.listPageTTD, param, pager);
			List<Map<String, Object>> lists = (List<Map<String, Object>>) pager1.getRows();
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			if (lists != null && lists.size() > 0) {
				for (Map<String, Object> fd : lists) {
					// 查询列表中执行对象一栏数据
					// String subjectname =
					// judgmentExecutor(session.getCompid(),Long.valueOf(fd.get("taskid").toString()));
					fd.put("subjectname", fd.get("username"));
					// 说明是任务的退回，退回任务要将操作时间赋值给最新反馈时间newbacktime
					if ("1".equals(fd.get("flag").toString().trim())) {
						TkMyTaskLog log = generalSqlComponent.query(SqlCode.getTMTLByid,
								new Object[] { session.getCompid(), Long.valueOf(fd.get("feedbackid").toString()) });
						if (log != null) {
							fd.put("newbacktime", log.getOpttime());
						}
					}
					list.add(fd);
				}
			}
			pager1.setRows(list);
			return pager1;
		} else {
			Map<String, Object> param = new HashedMap<String, Object>(3);
			param.put("tkname", tkname);
			param.put("starttime", starttime);
			param.put("endtime", endtime);
			param.put("subjectid", session.getUserid());
			param.put("status", status);
			Pager pager1 = generalSqlComponent.pageQuery(SqlCode.listPageTTDTask, param, pager);
			return pager1;
		}
	}

	/**
	 * 审核列表页面表头数量查询
	 * 
	 * @param session
	 * @param status
	 * @param tkname
	 * @param starttime
	 * @param endtime
	 * @author pantao
	 * @return
	 */
	public Integer pageCount(Session session, int status, String tkname, String starttime, String endtime) {
		Map<String, Object> param = new HashedMap<String, Object>(1);
		param.put("subjectid", session.getUserid());
		param.put("status", status);
		int num = generalSqlComponent.query(SqlCode.listPageTTDCount, param);
		return num;
	}

	/**
	 * 反馈审核 和退回任务审核
	 * 
	 * @param status     审核状态 1通过，-1拒绝
	 * @param mesg       审核备注
	 * @param feedstatus 对应的反馈的状态： 0正常反馈，1逾期反馈，2逾期未报
	 * @param remindtime 对应的提醒时间点
	 * @author pantao
	 */
	@Transactional(rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public void audit(Session session, String auditids, Integer status, String mesg, Integer feedstatus,
			String remindtime) {
		String[] strs = auditids.split(",");
		if (null == strs || strs.length <= 0) {
			return;
		}
		boolean pass = status == 1;
		Long compid = session.getCompid();
		// 备注主任务和子任务的状态没有改过来
		for (String auditid : strs) {
			// 1.改变反馈审核表的状态，如果通过则进行下面第2步，如果拒绝则该流程到此结束，
			TkTaskFeedBackAudit info = generalSqlComponent.query(SqlCode.getTTFBAByid,
					new Object[] { auditid, compid });
			if (info == null) {
				LehandException.throwException("审核人已发生变更，该审核记录被删除，请知悉！");
			}
			TkMyTask my = generalSqlComponent.query(SqlCode.getSubjectidByMytaskid,
					new Object[] { compid, info.getMytaskid() });
			Long taskid = info.getTaskid();
			if (my == null) {
				LehandException.throwException("我的任务不存在任务id：" + taskid);
			}
			generalSqlComponent.update(SqlCode.modiTTFBAByid,
					new Object[] { status, mesg, DateEnum.YYYYMMDDHHMMDD.format(), auditid, compid });
			// 任务审核 否则为反馈
			boolean taskAudit = info.getRemarks2() == 1;
			if (taskAudit) {
				if (pass) {
					taskAuditPass(session, compid, info, my, taskid);
				} else {
					taskAuditNoPass(session, compid, info, my, taskid);
				}
				//更新审核人的待办为已办
				generalSqlComponent.getDbComponent().update("update my_to_do_list set ishandle=1 where module=3 and userid=? and busid=?", new Object[] {session.getUserid(),taskid});
				continue;
			}
			if (pass) {
				feedAuditPass(session, mesg, feedstatus, remindtime, auditid, compid, info, my, taskid);

			} else {
				feedAuditNoPass(session, mesg, auditid, info, my);
			}
			//更新审核人的待办为已办
			generalSqlComponent.getDbComponent().update("update my_to_do_list set ishandle=1 where module=2 and userid=? and busid=?", new Object[] {session.getUserid(),taskid});
			
		}
	}

	/**
	 * 下发任务审核
	 * 
	 * @param session
	 * @param auditids
	 * @param status
	 * @param mesg
	 */
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public void auditTask(Session session, String auditids, int status, String mesg) throws Exception {
		String[] strs = auditids.split(",");
		if (strs.length <= 0) {
			LehandException.throwException("参数异常");
		}
		boolean pass = status == 1;
		Long compid = session.getCompid();
		for (String auditid : strs) {
			// 插叙审核记录是否存在
			TkTaskFeedBackAudit info = generalSqlComponent.query(SqlCode.getTTFBAByid,
					new Object[] { auditid, compid });
			if (info == null) {
				LehandException.throwException("审核人已发生变更，该审核记录被删除，请知悉！");
			}
			Long taskid = info.getTaskid();
			// 更新审核表状态
			generalSqlComponent.update(SqlCode.modiTTFBAByid,
					new Object[] { status, mesg, DateEnum.YYYYMMDDHHMMDD.format(), auditid, compid });
			// 任务下发审核
			TkTaskDeploy dep = generalSqlComponent.query(SqlCode.getTTDByid, new Object[] { compid, taskid });
			TkTaskDeployRequest request = JSON.parseObject(dep.getRemarks3(), TkTaskDeployRequest.class);
			Session sessionCreate = request.getSession();
			if (pass) {
				removeTodoItem(Todo.ItemType.TYPE_DEPLOY, String.valueOf(taskid), session);
				removeTodoItem(Todo.ItemType.TYPE_ISSUE_TASK, auditid, session);
                // 判断当前审核人是否是最后一个
                Map<String, Object> map = generalSqlComponent.getDbComponent().getMap("select * from " +
                        "tk_task_audit_order where compid=? and taskid=? and ordernum>(select ordernum from " +
                        "tk_task_audit_order where compid=? and taskid=? and auditid=? ) order by  ordernum asc " +
                                "limit 1",
                        new Object[]{session.getCompid(),taskid,session.getCompid(),taskid, session.getUserid()});

                if (map==null) {// 没有查询到说明是最后一个人审核
                    List<Map> requireMents = tkTaskDeployBusinessImpl.getRequireMentMap(request,
                            request.getTkname());
                    tkTaskDeployBusinessImpl.continueDeployTask(request, sessionCreate, requireMents, dep);
                } else {// 查询到了 审核完成后需要寻找下一个审核人
                    // 更新主任务状态为审核中
                    updateTkTaskDeployStatus(Constant.STATUS_2, compid, taskid);
                    Long subjectid = Long.valueOf(map.get("auditid").toString());
                    // 再往审核表中插入正职审核人的数据
                    TkTaskFeedBackAudit tkTaskFeedBackAudit = tkTaskDeployBusinessImpl
                            .insertTkTaskFeedBackAudit(compid, taskid, null, null, subjectid, 2, 1);
                    Long auditid2 = tkTaskFeedBackAudit.getId();
                    UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid, new Object[] { subjectid });
                    tkTaskDeployBusinessImpl.sendMsg(session, Module.TASK_AUDIT_NOPASS.getValue(), auditid2, "TASK_AUDIT",
                            "任务审核", "有一条新的任务，请审核。", subjectid, user.getUsername(),
                            String.valueOf(tkTaskFeedBackAudit.getTaskid()), generalSqlComponent);

                    //给下一个审核人待办
                    TkTaskClass taskClass = tkTaskClassDao.get(session.getCompid(),request.getClassid(),1);
                    addTodoItem(Todo.ItemType.TYPE_DEPLOY, dep.getTkname() + "(" + taskClass.getClname() + ")", String.valueOf(info.getId()),  Todo.FunctionURL.MY_TASK.getPath(), user.getUserid(), session);


					}
			} else {// 驳回
				// 更新主任务状态为被驳回
				updateTkTaskDeployStatus(Constant.STATUS_5, compid, taskid);
				insert(session, Module.TASK_AUDIT_NOPASS.getValue(), info.getTaskid(), "TASK_AUDIT_NOPASS", "部署任务被驳回",
						"您的部署任务申请已被驳回，请知悉。", dep.getUserid(), dep.getUsername());
//				// 创建短信消息
//				tkTaskDeployBusinessImpl.insertSms("您的下发任务审核被驳回，任务名称：" + request.getTkname(),
//						tkTaskDeployBusinessImpl.getPhones(compid, dep.getUserid()));
//				removeTodoItem(Todo.ItemType.TYPE_DEPLOY, String.valueOf(info.getTaskid()), session);
				removeTodoItem(Todo.ItemType.TYPE_ISSUE_TASK, auditid, session);
			}
			
			// 当审核认为多人并且是两个正职或者两个副职时一个审核通过另外一个待办也要变成已办
			// 首先查出该审核人的待办
			Map<String, Object> map = generalSqlComponent.getDbComponent().getMap(
					"select remind from my_to_do_list where module=0 and userid=? and busid=? and ishandle=0 order by remind desc limit 1",
					new Object[] { session.getUserid(), taskid });
			// 更新审核人的待办为已办
			if (map != null) {
				generalSqlComponent.getDbComponent().update(
						"update my_to_do_list set ishandle=1 where module=0 and busid=? and remind =? ",
						new Object[] { taskid, map.get("remind") });
			}


		}
	}

	/**
	 * 更新主任务状态
	 * 
	 * @param status
	 * @param compid
	 * @param id
	 */
	public void updateTkTaskDeployStatus(int status, Long compid, Long id) {
		generalSqlComponent.update(SqlCode.modiTaskStatus, new Object[] { status, compid, id });
		// tkMyTaskDao.update("update tk_task_deploy set status=? where compid=? and
		// id=?", status,compid,id);
	}

	/**
	 * 反馈审核不通过
	 * 
	 * @param session
	 * @param mesg
	 * @param auditid
	 * @param info
	 * @param my
	 * @author pantao
	 */
	private void feedAuditNoPass(Session session, String mesg, String auditid, TkTaskFeedBackAudit info, TkMyTask my) {
		generalSqlComponent.update(SqlCode.modiTTFBABystatus,
				new Object[] { DateEnum.YYYYMMDDHHMMDD.format(), Constant.statuss, mesg, auditid });
		insert(session, Module.MY_TASK_LOG.getValue(), info.getFeedbackid(), "FEED_BACK", "任务反馈被驳回",
				"您有一条任务反馈被驳回，请重新上报。", my.getSubjectid(), my.getSubjectname());
		
		// 创建短信消息
		TkTaskDeploy tkTaskDeploy = tkTaskDeployDao.get(session.getCompid(),info.getTaskid());
		tkTaskDeployBusinessImpl.insertSms("您上报的任务被驳回，任务名称：" + tkTaskDeploy.getTkname(),
				tkTaskDeployBusinessImpl.getPhones(session.getCompid(), my.getSubjectid()));
		removeTodoItem(Todo.ItemType.TYPE_REPORT_TASK, auditid, session);
	}

	/**
	 * 任务审核不通过
	 * 
	 * @param session
	 * @param compid
	 * @param info
	 * @param my
	 * @param taskid
	 * @author pantao
	 */
	private void taskAuditNoPass(Session session, Long compid, TkTaskFeedBackAudit info, TkMyTask my, Long taskid) {
		// 这里一定要注意同一事务中如果先更新后查询查询到的是更新过后的数据（此时数据库中仍然是更新前的数据因为事务没有提交完成）
		TkTaskDeploySub deploySub = generalSqlComponent.query(SqlCode.getTaskSub, new Object[] { compid, taskid });
		// 去掉退回过渡字段remarks6='tuihui',并修改状态为办理中
		generalSqlComponent.update(SqlCode.modiTMTByStatusAndRemarks6, new Object[] { info.getMytaskid() });
		generalSqlComponent.update(SqlCode.modiTTDSReceivenum, new Object[] { taskid });
		TkTaskDeploy deploy = generalSqlComponent.query(SqlCode.getTask, new Object[] { compid, taskid });
		if (deploy.getExsjnum().equals(deploySub.getReceivenum() + 1)) {
			generalSqlComponent.update(SqlCode.updateExsjnums, new Object[] { Constant.TkTaskDeploy.SIGNED_STATUS,
					DateEnum.YYYYMMDDHHMMDD.format(), info.getCompid(), taskid });
		}
		/**
		 * 更新完子表后需要给执行人发送消息"您的《tkname》任务退回操作被驳回,任务已自动签收！" 该消息自动写入tk_task_remind_list表
		 */
		insert(session, Module.MY_TASK.getValue(), info.getMytaskid(), "SIGN_TASK", "任务退回被驳回", "您有一条任务退回申请被驳回，请按要求反馈。",
				my.getSubjectid(), my.getSubjectname());
		removeTodoItem(Todo.ItemType.TYPE_ISSUE_TASK, String.valueOf(info.getId()), session);
	}

	/**
	 * 任务审核通过
	 * 
	 * @param session
	 * @param compid
	 * @param info
	 * @param my
	 * @param taskid
	 * @author pantao
	 */
	private void taskAuditPass(Session session, Long compid, TkTaskFeedBackAudit info, TkMyTask my, Long taskid) {
		// 第三步：任务执行人数量减1后需要反过来查询下数据库看看任务的执行人数量是否为0
		TkTaskDeploy tkTaskDeploy = generalSqlComponent.query(SqlCode.getTask, new Object[] { compid, taskid });
		Integer num = tkTaskDeploy.getExsjnum() - 1;
		// 退回任务的审核 如果审核通过就进行之前操作类中退回的所有操作 若不通过则可以什么都不做
		// 第一步：将子表任务的状态更新为6退回状态
		generalSqlComponent.update(SqlCode.updateTkMyTaskStatus, new Object[] { Constant.TkMyTask.STATUS_BACK,
				DateEnum.YYYYMMDDHHMMDD.format(), compid, info.getMytaskid(), Constant.TkMyTask.STATUS_BACK });
		// 第二步：将主表中的执行人数量减一
		generalSqlComponent.update(SqlCode.updateExsjnum, new Object[] { compid, taskid });
		// 第四步：如果任务被所有执行人退回了，就将其状态变成草稿状态
		// 事务没提交查出来的仍旧是之前的
		if (tkTaskDeploy.getExsjnum() - 1 <= 0) {
			generalSqlComponent.update(SqlCode.updateExsjnums,
					new Object[] { Constant.statusss, DateEnum.YYYYMMDDHHMMDD.format(), compid, taskid });
			// 删除任务时间 防止更改任务重新发布，主键冲突
			tkTaskTimeNodeService.deleteByTaskid(compid, taskid);
			// 删除反馈时间节点提醒 防止更改任务重新发布，主键冲突
			generalSqlComponent.delete(SqlCode.deleteFeedTime, new Object[] { compid, taskid });
			// 删除任务附属信息表
			tkTaskDeploySubDao.deleteByTaskId(compid, taskid);
		}
		/** 第五步：对于退回任务删除执行人将其的remarks1字段变成-1，这里不用真删除是为了在草稿箱中能够查看退回人退回的详情 */
		// 修改执行人前需要先查出该执行人的id
		TkMyTask mytask = generalSqlComponent.query(SqlCode.getSubjectidByMytaskid,
				new Object[] { compid, info.getMytaskid() });
		// generalSqlComponent.delete(SqlCode.delSubject,new Object[]
		// {session.getCompid(),info.getTaskid(),mytask.getSubjectid()});
		generalSqlComponent.update(SqlCode.updateSubject, new Object[] { compid, taskid, mytask.getSubjectid() });
		/** 判断主任务的执行人数目和附属表中签收数量是否相等，相等就改变主任务的状态 */
		TkTaskDeploySub deploySub = generalSqlComponent.query(SqlCode.getTaskSub, new Object[] { compid, taskid });
		// 排除单人退回导致草稿状态又会变成办理中状态
		if (tkTaskDeploy.getExsjnum() - 1 != 0) {
			num = num == null ? 0 : num;
			// 排除0==0 的情况
			if (num != 0 && num.equals(deploySub.getReceivenum())) {
				generalSqlComponent.update(SqlCode.updateExsjnums, new Object[] { Constant.TkTaskDeploy.SIGNED_STATUS,
						DateEnum.YYYYMMDDHHMMDD.format(), compid, taskid });
			}
		}

		/** 通过后给执行人发消息 */
		// TkTaskDeploy dep = generalSqlComponent.query(SqlCode.getTTDByid, new Object[]
		// {session.getCompid(),info.getTaskid()});
		insert(session, Module.MY_TASK.getValue(), info.getMytaskid(), "RETURN_TASK", "任务退回被通过", "您有一条任务退回申请已通过审核，请知悉。",
				my.getSubjectid(), my.getSubjectname());

	}

	/**
	 * 反馈审核通过
	 * 
	 * @param session
	 * @param mesg
	 * @param feedstatus
	 * @param remindtime
	 * @param auditid
	 * @param compid
	 * @param info
	 * @param my
	 * @param taskid
	 * @author pantao
	 */
	private void feedAuditPass(Session session, String mesg, Integer feedstatus, String remindtime, String auditid,
			Long compid, TkTaskFeedBackAudit info, TkMyTask my, Long taskid) {
		TkTaskDeploy dep = generalSqlComponent.query(SqlCode.getTTDByid, new Object[] { compid, taskid });
		/**
		 * 2.判断该审核人是否是创建人，如果是创建人，那么在审核人点击审核按钮后该审核流程到此结束，反之，继续查询该审核人的父节点id，并向反馈审核表中插入一条数据
		 */
		if (info != null && dep != null) {
			// 审核人不是创建人继续往下找下一个审核人
			if (!info.getSubjectId().toString().equals(dep.getUserid().toString())) {
				// 当前审核人用户表id
				Long subjectid = info.getSubjectId();
				// 第一步：通过登录人的userid找到其组织机构remarks3=userid(这里还需要考虑一种情况就是集团下边的4个内置用户其机构只能通过用户机构关系表找到对应的机构id，再通过机构i的查询机构信息)
//				PwOrgan org = generalSqlComponent.query(SqlCode.getPOByRemarks3, new Object[] { subjectid, compid });
				UrcOrgan org = generalSqlComponent.query(SqlCode.getPOByRemarks3, new Object[] { subjectid, compid });
				// 第二步：通过第一步中的pid找到对应的父节点的组织机构id=pid
//				PwOrgan orgpid = generalSqlComponent.query(SqlCode.getPOByid, new Object[] { org.getPid(), compid });
				UrcOrgan orgpid = generalSqlComponent.query(SqlCode.getPOByid, new Object[] { org.getPorgid(), compid });
				// 判断是否存在父节点，存在就继续插入数据
				if (orgpid != null) {
					// 如果找到了就查询该机构有多少用户pw_user_organ_map,如果存在多个就说明该机构是根节点集团，此时下级审核人就直接是创建人
//					int num = generalSqlComponent.query(SqlCode.getPUOMBynum, new Object[] { compid, orgpid.getId() });
					int num = generalSqlComponent.query(SqlCode.getPUOMBynum, new Object[] { compid, orgpid.getOrgid() });
					Long pidsubjectid = Long.valueOf(orgpid.getRemarks3());
//					PwUser user = generalSqlComponent.query(SqlCode.getPUUserid, new Object[] { pidsubjectid });
					UrcUser user = generalSqlComponent.query(SqlCode.getPUUserid, new Object[] { pidsubjectid });
					TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
					audit.setCompid(info.getCompid());
					audit.setTaskid(taskid);
					audit.setMytaskid(info.getMytaskid());
					audit.setFeedbackid(info.getFeedbackid());
					if (num > 1) {
						audit.setSubjectId(dep.getUserid());
					} else {
						audit.setSubjectId(pidsubjectid);
					}
					// 这里暂时定为0，后续根据需要再修改
					audit.setSubtype(Constant.status);
					audit.setStatus(Constant.status);
					audit.setRemark(StringConstant.EMPTY);
					audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
					audit.setOpttime(StringConstant.EMPTY);
					audit.setRemarks2(Constant.status);
					audit.setRemarks3(StringConstant.EMPTY);
					audit.setRemarks4(StringConstant.EMPTY);
					audit.setRemarks5(StringConstant.EMPTY);
					audit.setRemarks6(StringConstant.EMPTY);
					Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
					audit.setId(id);
					insert(session, Module.MY_TASK.getValue(), info.getMytaskid(), "FEED_AUDIT", "反馈审核",
							"有一条新的任务反馈，请审核。", pidsubjectid, user.getUsername());
//					// 创建短信消息
//					tkTaskDeployBusinessImpl.insertSms("您有一条上报任务待审核，任务名称：" + dep.getTkname(),
//							tkTaskDeployBusinessImpl.getPhones(compid, pidsubjectid));
					addTodoItem(Todo.ItemType.TYPE_REPORT_TASK,dep.getTkname() + "（" + session.getCurrentOrg().getOrgname() + "）", auditid , Todo.FunctionURL.TASK_FEEDBACK_AUDIT.getPath(),audit.getSubjectId(), session);
					removeTodoItem(Todo.ItemType.TYPE_REPORT_TASK, auditid, session);
				}
			} else {// 当前审核人是创建人
				insert(session, Module.MY_TASK_LOG.getValue(), info.getFeedbackid(), "FEED_BACK", "任务反馈被通过",
						"您有一条任务反馈已通过审核，请知悉。", my.getSubjectid(), my.getSubjectname());
//				addTodoItem(Todo.ItemType.TYPE_REPORT_TASK,dep.getTkname() + "（" + session.getCurrentOrg().getOrgname() + "）", auditid , Todo.FunctionURL.TASK_FEEDBACK_AUDIT.getPath(),dep.getUserid(), session);
				removeTodoItem(Todo.ItemType.TYPE_REPORT_TASK, auditid, session);

			}
			
			/**
			 * 通过的情况下需要根据返回feedbackid更新该反馈的状态remarks2为feedstatus且更新ramarks4字段的时间为remindtime,反馈表中remarks2为1代表正常反馈，为2代表逾期反馈
			 */
			// 批量审核时就不更新反馈表
			if (feedstatus != null && !StringUtils.isEmpty(remindtime)) {
				generalSqlComponent.update(SqlCode.modiTMTLByid,
						new Object[] { feedstatus, remindtime, compid, info.getFeedbackid() });
			}
			// 每次审核完成都需要更新审核表中的createtime字段，为了在审核日志中显示审核时间
			generalSqlComponent.update(SqlCode.modiTTFBAByCreatetime,
					new Object[] { DateEnum.YYYYMMDDHHMMDD.format(), compid, auditid });
		} else {
			LehandException.throwException("数据记录不存在！");
		}
		// 更新审核表中的状态
		generalSqlComponent.update(SqlCode.modiTTFBABystatus,
				new Object[] { DateEnum.YYYYMMDDHHMMDD.format(), Constant.statusss, mesg, auditid });
		}

	/**
	 * 消息对象的创建
	 * 
	 * @param session     发送人信息
	 * @param module      模块
	 * @param mid         模块对应的id
	 * @param rulecode    提醒规则
	 * @param rulename    规则名称
	 * @param mesg        提醒消息文本
	 * @param subjectid   被提醒人id
	 * @param subjectname 被提醒人名称
	 * @author pantao
	 * @return
	 */
	public void insert(Session session, int module, Long mid, String rulecode, String rulename, String mesg,
			Long subjectid, String subjectname) {
		TkTaskRemindList ls = new TkTaskRemindList();
		ls.setCompid(session.getCompid());
		ls.setNoteid(-1L);
		// 模块(0:任务发布,1:任务反馈)
		ls.setModule(module);
		// 对应模块的ID
		ls.setMid(mid);
		// 提醒日期
		ls.setRemind(DateEnum.YYYYMMDDHHMMDD.format());
		// 编码
		ls.setRulecode(rulecode);
		ls.setRulename(rulename);
		ls.setOptuserid(session.getUserid());
		ls.setOptusername(session.getUsername());
		// 被提醒人
		ls.setUserid(subjectid);
		ls.setUsername(subjectname);
		ls.setIsread(0);
		ls.setIshandle(0);
		// 是否已提醒(0:未提醒,1:已提醒)
		ls.setIssend(0);
		// 消息
		ls.setMesg(mesg);
		ls.setRemarks1(1);
		ls.setRemarks2(0);
		ls.setRemarks3("工作督查");
		ls.setRemarks4(Constant.EMPTY);
		ls.setRemarks5(Constant.EMPTY);
		ls.setRemarks6(Constant.EMPTY);
		Long id = generalSqlComponent.insert(SqlCode.addTTRLByTuihui, ls);
		ls.setId(id);
	}

	/**
	 * 获取任务的提醒时间点集合
	 * 
	 * @param taskid 主任务id
	 * @author pantao
	 */
	public List<TaskRemindTimes> listRemindTimes(Long compid, Long taskid, Long subjectid) {
		List<TaskRemindTimes> lists = generalSqlComponent.query(SqlCode.listRemindtime,
				new Object[] { compid, taskid });
		// 查询该任务的最新反馈或者该任务下该人的最新反馈对应的时间点
		String newremindtime = "";
		if (subjectid != null) {
			newremindtime = generalSqlComponent.query(SqlCode.listnewRemindtime,
					new Object[] { compid, taskid, subjectid });
		}
		List<TaskRemindTimes> infos = new ArrayList<TaskRemindTimes>();
		if (lists != null && lists.size() > 0) {
			int j = 0;
			for (TaskRemindTimes taskRemindTime : lists) {
				// 当前时间点
				long nowtime = DateEnum.now().getTime();
				int i = 0;
				String remindtime = taskRemindTime.getRemindtime();
				Map<String, Object> param = new HashedMap<String, Object>(3);
				param.put("taskid", taskid);
				param.put("remindtime", remindtime);
				param.put("userid", subjectid);
				List<Integer> datas = generalSqlComponent.query(SqlCode.listTMTLByremarks4, param);
				try {
					/** 对于所有时间节点需要控制是否能点击，要求当前时间之前的节点都可选以及当前时间节点后面一个时间节点也可选，其他的就都不可以选 */
					long remind = DateEnum.YYYYMMDDHHMMDD.parse(remindtime).getTime();
					// 没有查询到数据说明没有反馈记录
					if (datas == null || datas.size() <= 0) {
						// 如果提醒时间点小于当前时间则逾期未反馈
						if (nowtime > remind) {
							taskRemindTime.setFeedstatus(Constant.colourthere);
							taskRemindTime.setRemindtime(remindtime);
							// 1表示可选
							taskRemindTime.setFlag(Constant.statusss);
						} else {
							// 如果当前时间小于提醒时间点则属于正常未反馈这里暂时定为正常未反馈反馈
							taskRemindTime.setFeedstatus(Constant.colourfour);
							taskRemindTime.setRemindtime(remindtime);
							j++;
							if (j <= 1) {
								// 1表示可选
								taskRemindTime.setFlag(Constant.statusss);
							} else {
								// 0表示不可选
								taskRemindTime.setFlag(Constant.status);
							}
						}
					} else {
						// 查询到了记录,区分是正常反馈还是逾期反馈
						for (Integer num : datas) {
							// 存在逾期反馈
							if (num == 2) {
								taskRemindTime.setFeedstatus(Constant.colourtwo);
								taskRemindTime.setRemindtime(remindtime);
								if (nowtime > remind) {
									taskRemindTime.setFlag(Constant.statusss);
								} else {
									j++;
									if (j <= 1) {
										taskRemindTime.setFlag(Constant.statusss);
									} else {
										taskRemindTime.setFlag(Constant.status);
									}
								}
								break;
							} else {
								i++;
								if (i == datas.size()) {
									// 正常反馈
									taskRemindTime.setFeedstatus(Constant.colourone);
									taskRemindTime.setRemindtime(remindtime);
									if (nowtime > remind) {
										// 1表示可选
										taskRemindTime.setFlag(Constant.statusss);
									} else {
										j++;
										if (j <= 1) {
											taskRemindTime.setFlag(Constant.statusss);
										} else {
											taskRemindTime.setFlag(Constant.status);
										}
									}
									break;
								}
							}
						}
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				// 将最新的反馈对应的时间点对象的isShow赋值true
				if (com.alibaba.druid.util.StringUtils.equals(taskRemindTime.getRemindtime(), newremindtime)) {
					taskRemindTime.setIsShow(true);
				}
				infos.add(taskRemindTime);
			}
		}
		return infos;
	}

	/**
	 * 获取任务的提醒时间点集合
	 * 
	 * @param taskid 主任务id
	 * @author zouchuang
	 */
	public List<TaskRemindTimes> listRemindTimesAll(Long compid, Long taskid) {
		Assert.notNull(compid, "compid cant be null");
		Assert.notNull(taskid, "taskid cant be null");

		List<TaskRemindTimes> lists = generalSqlComponent.query(SqlCode.listRemindtime,
				new Object[] { compid, taskid });
		TkTaskDeploy deploy = generalSqlComponent.query(SqlCode.getTask, new Object[] { compid, taskid });
		Integer exsjnum = deploy.getExsjnum();
		List<TaskRemindTimes> infos = new ArrayList<TaskRemindTimes>();
		if (CollectionUtils.isEmpty(lists)) {
			return infos;
		}

		for (TaskRemindTimes taskRemindTime : lists) {
			// 当前时间点
			long nowtime = DateEnum.now().getTime();
			String remindtime = taskRemindTime.getRemindtime();
			Map<String, Object> param = new HashedMap<String, Object>(3);
			param.put("taskid", taskid);
			param.put("remindtime", remindtime);
			List<String> datas = generalSqlComponent.query(SqlCode.listLogStatusNotDel, param);
			Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime,
					new Object[] { compid, taskid, remindtime });
			int size = count.intValue();
			try {
				long remind = DateEnum.YYYYMMDDHHMMDD.parse(remindtime).getTime();
				if (size < exsjnum) { // 有人未报
					if (nowtime > remind) {
						taskRemindTime.setFeedstatus(Constant.colourthere);
					} else {
						taskRemindTime.setFeedstatus(Constant.colourfour);
					}
				} else {
					// 查询到了记录,区分是正常反馈还是逾期反馈
					if (datas.contains(String.valueOf(Constant.colourtwo))) {
						taskRemindTime.setFeedstatus(Constant.colourtwo);
					} else {
						taskRemindTime.setFeedstatus(Constant.colourone);
					}
				}
				taskRemindTime.setRemindtime(remindtime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			infos.add(taskRemindTime);
		}
		return infos;
	}

	/**
	 * 审核页面头部查询反馈详情接口
	 * 
	 * @param session
	 * @param auditid 审核表id
	 * @author pantao
	 */
	public FeedDetails getFeedDetials(Session session, Long auditid, int flag) {
		FeedDetails info = null;
		// 反馈审核查询
		if (flag == 0) {
			info = generalSqlComponent.query(SqlCode.getFeedDetails,
					new Object[] { session.getCompid(), auditid, session.getCompid(), session.getCompid() });
		} else {
			// 任务审核查询
			info = generalSqlComponent.query(SqlCode.getFeedDetailsTask,
					new Object[] { session.getCompid(), auditid, session.getCompid(), session.getCompid() });
		}
		if (info == null) {
			LehandException.throwException("数据不存在！");
		}
		returnAttachment(session, info);

        info.setFiles(filesPreviewAndDownload.listFile(info.getFile(),session.getCompid()));
		return info;
	}

	/**
	 * 返回附件拼装部分字段
	 * 
	 * @param session
	 * @param info
	 * @author pantao
	 */
	private void returnAttachment(Session session, FeedDetails info) {
		if (!StringUtils.isEmpty(info.getUuid())) {
			List<TkAttachment> attachment = generalSqlComponent.query(SqlCode.listTkAttachment,
					new Object[] { info.getUuid(), session.getCompid() });
			if (attachment == null || attachment.size() <= 0) {
				info.setAttachment(new ArrayList<TkAttachment>());
			} else {
				for (TkAttachment fj : attachment) {
					if ("mp4".equals(fj.getSuffix())) {
						fj.setPath(downIp + fj.getRemarks5());
					} else {
						fj.setPath(downIp + fj.getPath());
					}
				}
				info.setAttachment(attachment);
			}
		} else {
			info.setAttachment(new ArrayList<TkAttachment>());
		}
	}

	/**
	 * 查看审核历史详情页面专用接口
	 * 
	 * @param session
	 * @param mytaskid
	 * @param auditstatus
	 * @param auditid
	 * @param pager
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager listFeedDetailsAudit(Session session, Long mytaskid, Integer auditstatus, Long auditid,
			Long feedbackid, Pager pager) {
		Map<String, Object> param = new HashedMap<String, Object>();
		Long compid = session.getCompid();
		param.put("compid", compid);
		param.put("mytaskid", mytaskid);
		param.put("auditstatus", auditstatus);
		param.put("auditid", auditid);
		param.put("feedbackid", feedbackid);
		// 我的任务以及巡查模块中的接收的
		Pager datas = null;
		if (pager.getPageNo() == 1 && pager.getPageSize() == 1) {
			datas = generalSqlComponent.pageQuery(SqlCode.listTMTLByMytaskidAudit, param, pager);
		} else {
			datas = generalSqlComponent.pageQuery(SqlCode.listTMTLByMytaskidAudit2, param, pager);
		}
		List<Map<String, Object>> lists = (List<Map<String, Object>>) datas.getRows();
		List<Map<String, Object>> maps = comment(session, auditstatus, lists);
		delUnwanted(auditstatus, maps);
		datas.setRows(maps);
		return datas;
	}

	/**
	 * 反馈详情页面数据分页查询
	 * 
	 * @author pantao
	 */
	@SuppressWarnings("unchecked")
	public Pager listFeedDetails(Session session, Long mytaskid, String remindtime, Integer auditstatus, Integer flag,
			Long subjectid, Pager pager) {
		Map<String, Object> param = new HashedMap<String, Object>();
		Long compid = session.getCompid();
		param.put("compid", compid);
		param.put("mytaskid", mytaskid);
		param.put("auditstatus", auditstatus);
		// 我的任务以及巡查模块中的接收的
		if (Constant.str_0.equals(flag.toString().trim())) {
			param.put("remindtime", remindtime);
			Pager datas = generalSqlComponent.pageQuery(SqlCode.listTMTLByMytaskid, param, pager);
			List<Map<String, Object>> lists = (List<Map<String, Object>>) datas.getRows();
			List<Map<String, Object>> maps = comment(session, auditstatus, lists);
			delUnwanted(auditstatus, maps);
			datas.setRows(maps);
			return datas;
		} else {
			// 已发任务中每个时间点就是一个模块，每个模块中有多条记录首先根据时间点来分区（分页查询），每个区里面查询所有通过后的反馈或驳回的反馈
			// 通过我的任务id查询主任务id
			TkMyTask my = generalSqlComponent.query(SqlCode.getTaskidBymyid, new Object[] { compid, mytaskid });
			if (my == null) {
				LehandException.throwException("任务不存在！");
			}
			Long taskid = my.getTaskid();
			// Pager datas = generalSqlComponent.pageQuery(SqlCode.listRemindtimePage, new
			// Object[] {compid,taskid,subjectid,compid,taskid},pager);
			// List<Map<String, Object>> times = (List<Map<String, Object>>)
			// datas.getRows();
			List<Map<String, Object>> times = generalSqlComponent.query(SqlCode.listRemindtimeNoPage,
					new Object[] { compid, taskid, subjectid, compid, taskid });
			if (times != null && times.size() > 0) {
				List<Map<String, Object>> maps2 = new ArrayList<Map<String, Object>>();
				for (Map<String, Object> map : times) {
					int i = 0;
					Object value = map.get("remindtime");
					param.put("remindtime", value);
					List<Map<String, Object>> lists = generalSqlComponent.query(SqlCode.listTMTLByMytaskidNoPage,
							param);
					if (lists == null || lists.size() <= 0) {
						continue;
					}
					List<Map<String, Object>> maps = comment(session, auditstatus, lists);
					delUnwanted2(auditstatus, maps);
					Map<String, Object> mapdata = new HashedMap<String, Object>(3);
					String remindrequire = getFeedRequirement(session, taskid, value.toString());
					mapdata.put("remindrequire", remindrequire);
					mapdata.put("endtime", value);
					mapdata.put("feeddata", maps);
					/* 给时间点赋状态 */
					Map<String, Object> param1 = new HashedMap<String, Object>(3);
					param1.put("taskid", taskid);
					param1.put("remindtime", value);
					param1.put("userid", subjectid);
					List<Integer> datas1 = generalSqlComponent.query(SqlCode.listTMTLByremarks4, param1);
					// 没有查询到数据说明没有反馈记录
					// 查到了数据但是每个人该时间点统计一次反馈的总数量小于执行人的总数也算该任务改时间点逾期未报
					Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime,
							new Object[] { compid, taskid, value });
					int count1 = count.intValue();
					// 任务执行人的数量
					int sum = generalSqlComponent.query(SqlCode.getTTDByiDexsjnum, new Object[] { compid, taskid });
					if (datas1 == null || datas1.size() <= 0 || count1 < sum) {
						long nowtime = DateEnum.now().getTime();
						try {
							long remind = DateEnum.YYYYMMDDHHMMDD.parse(value.toString()).getTime();
							// 如果提醒时间点小于当前时间则逾期未反馈
							if (nowtime > remind) {
								mapdata.put("remindtimestatus", Constant.colourthere);
							} else {
								// 如果当前时间小于提醒时间点则属于正常未反馈这里暂时定为正常未反馈反馈
								mapdata.put("remindtimestatus", Constant.colourfour);
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else {
						// 查询到了记录,区分是正常反馈还是逾期反馈
						for (Integer num : datas1) {
							// 存在逾期反馈
							if (num == 2) {
								mapdata.put("remindtimestatus", Constant.colourtwo);
								break;
							} else {
								i++;
								if (i == datas1.size()) {
									mapdata.put("remindtimestatus", Constant.colourone);
								}
							}
						}
					}
					if (maps != null && maps.size() > 0) {
						maps2.add(mapdata);
					}

				}
				pager.setRows(maps2);
				return pager;
			}
		}
		return null;
	}

	/**
	 * 已发任务获取签收或退回模块数据（因为上述分页是通过时间节点来分的所以签收或退回信息需要单独给接口）
	 * 
	 * @param mytaskid
	 * @param compid
	 * @author pantao
	 * @return
	 */
	public List<Map<String, Object>> getSignIn(Long mytaskid, Long compid) {
		List<Map<String, Object>> maps2 = new ArrayList<Map<String, Object>>(1);
		// 再往maps2中塞入签收或者退回
		Map<String, Object> info = generalSqlComponent.query(SqlCode.listTMTLByMytaskidSingin,
				new Object[] { compid, mytaskid });
		List<Map<String, Object>> maps3 = new ArrayList<Map<String, Object>>();
		maps3.add(info);
		Map<String, Object> mapdata1 = new HashedMap<String, Object>(3);
		mapdata1.put("remindrequire", Constant.EMPTY);
		mapdata1.put("endtime", Constant.EMPTY);
		mapdata1.put("opttype", info.get("opttype"));
		mapdata1.put("feeddata", maps3);
		maps2.add(mapdata1);
		return maps2;
	}

	/**
	 * 数据拼装
	 * 
	 * @param session
	 * @param auditstatus
	 * @param lists
	 * @author pantao
	 * @return
	 */
	private List<Map<String, Object>> comment(Session session, Integer auditstatus, List<Map<String, Object>> lists) {
		List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>();
		if (lists != null && lists.size() > 0) {
			for (Map<String, Object> map : lists) {
				// 查询审核表取出该条反馈的最新的反馈记录并将审核状态和审核批注赋值到auditstatus 和 auditremark 字段
				TkTaskFeedBackAudit info = generalSqlComponent.query(SqlCode.getTTFBAByNewone,
						new Object[] { map.get("feedbackid") });
				// 不是退回类型的就将退回原因清空
				if (!"RETURN".equals(map.get("opttype"))) {
					map.put("backreason", StringConstant.EMPTY);
				} else {
					map.put("auditstatus", info.getStatus());
					map.put("auditremark", info.getRemark());
				}
				// 给反馈的添加时间点要求
				if ("FEED_BACK".equals(map.get("opttype"))) {
					// 单条反馈详情信息中需要增加一个参数好让前端判断这个反馈是否应该显示修改反馈按钮
					// 步骤：根据反馈id到审核表中查询该反馈是否存在审核记录如果存在就不让他修改
					int count = generalSqlComponent.query(SqlCode.getTTFBABycount,
							new Object[] { session.getCompid(), map.get("feedbackid") });
					// 存在
					if (count > 0) {
						// 不显示修改反馈按钮
						map.put("isshow", 1);
					} else {
						// 显示修改反馈按钮
						map.put("isshow", 0);
					}
					// 查询提醒时间表tk_task_feed_time
					if (StringUtils.isEmpty(map.get("remindtime"))) {
						map.put("feedrequestment", StringConstant.EMPTY);
					} else {
						String msg = getFeedRequirement(session, Long.valueOf(map.get("taskid").toString()),
								map.get("remindtime").toString());
						map.put("feedrequestment", msg);
					}
					if (info != null) {
						// 查询自身的审核id
						TkTaskFeedBackAudit auditInfo = generalSqlComponent.query(SqlCode.getTTFBAByByUser,
								new Object[] { session.getUserid(), map.get("feedbackid") });
						if (null != auditInfo) {
							map.put("auditid", auditInfo.getId());
						} else {
							map.put("auditid", info.getId());
						}
						// 查询该反馈在审核表中是否还存在待审核状态，如果存在则这里显示待审核状态，如果不存在就说明是终极审核人已经审核了就直接显示最终的审核状态
						int num = generalSqlComponent.query(SqlCode.getTTFBABycount2,
								new Object[] { session.getCompid(), map.get("feedbackid") });
						// 终极审核人审核
						if (num <= 0) {
							map.put("auditstatus", info.getStatus());
							map.put("auditremark", info.getRemark());
						} else {// 中间审核人审核
							map.put("auditstatus", Constant.status);
							map.put("auditremark", StringConstant.EMPTY);
						}
					}
				}
				if (!StringUtils.isEmpty(map.get("uuid"))) {
					List<TkAttachment> attachment = generalSqlComponent.query(SqlCode.listTkAttachment,
							new Object[] { map.get("uuid"), session.getCompid() });
					if (attachment == null || attachment.size() <= 0) {
						map.put("attachment", StringConstant.EMPTY);
					} else {
						for (TkAttachment fj : attachment) {
							if ("mp4".equals(fj.getSuffix())) {
								fj.setPath(downIp + fj.getRemarks5());
							} else {
								fj.setPath(downIp + fj.getPath());
							}
						}
						map.put("attachment", attachment);
					}
				} else {
					map.put("attachment", new ArrayList<TkAttachment>());
				}
				map.put("files",StringUtils.isEmpty(map.get("files"))?new ArrayList<>():
                        filesPreviewAndDownload.listFile(map.get("files").toString(),session.getCompid()));
				map.put("x", map.get("feedbackPerson"));
				map.put("y", map.get("feedbackPhone"));
				maps.add(map);
			}
		}
		return maps;
	}

	/**
	 * 删除集合中不符合要求的 普通遍历，每删除一个集合长度就会减少一个 迭代器遍历就不存在这个问题
	 * 
	 * @author pantao
	 */
	private void delUnwanted(Integer auditstatus, List<Map<String, Object>> maps) {
		Iterator<Map<String, Object>> iterator = maps.iterator();
		while (iterator.hasNext()) {
			Map<String, Object> map = iterator.next();
			if ("FEED_BACK".equals(map.get("opttype").toString().trim())) {
				// 驳回状态下要删除通过和未审核的
				if (auditstatus == -1) {
					if ("0".equals(map.get("auditstatus").toString().trim())
							|| "1".equals(map.get("auditstatus").toString().trim())) {
						iterator.remove();
					}
				} else {
					// 非驳回状态下要删除驳回的
					if ("-1".equals(map.get("auditstatus").toString().trim())) {
						iterator.remove();
					}
				}
			}
		}
	}

	/**
	 * 删除集合中不符合要求的 普通遍历，每删除一个集合长度就会减少一个 迭代器遍历就不存在这个问题
	 * 
	 * @author pantao
	 */
	private void delUnwanted2(Integer auditstatus, List<Map<String, Object>> maps) {
		Iterator<Map<String, Object>> iterator = maps.iterator();
		while (iterator.hasNext()) {
			Map<String, Object> map = iterator.next();
			if ("FEED_BACK".equals(map.get("opttype").toString().trim())) {
				// 驳回状态下要删除通过和未审核的
				if (auditstatus == -1) {
					if ("0".equals(map.get("auditstatus").toString().trim())
							|| "1".equals(map.get("auditstatus").toString().trim())) {
						iterator.remove();
					}
				} else {
					// 非驳回状态下要删除驳回的以及审核中的
					if ("-1".equals(map.get("auditstatus").toString().trim())
							|| "0".equals(map.get("auditstatus").toString().trim())) {
						iterator.remove();
					}
				}
			}
		}
	}

	/**
	 * 反馈详情页面右侧执行人组织结构树接口数据(不包含草稿箱详情)
	 * 
	 * @author pantao
	 */
	@SuppressWarnings("unchecked")
	public TreeSubjectResponse listTreeSubject(Session session, Long taskid, String username) {
		Map<String, Object> param = new HashedMap<String, Object>();
		param.put("taskid", taskid);
		param.put("compid", session.getCompid());
		param.put("username", username);
		List<Map<String, Object>> lists = generalSqlComponent.query(SqlCode.listTreeSubject, param);
		int num = lists.size();
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		if (lists != null && lists.size() > 0) {
			Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(lists.size());
			for (Map<String, Object> map1 : lists) {
				String subjectname = map1.get("subjectname").toString();
				Long subjectid = Long.valueOf(map1.get("subjectid").toString());
				Long organid = Long.valueOf(map1.get("organid").toString());
				Long pid = Long.valueOf(map1.get("pid").toString());
				Integer status = Integer.valueOf(map1.get("status").toString());
				Integer remarks1 = Integer.valueOf(map1.get("remarks1").toString());
				Map<String, Object> line = new HashMap<String, Object>(5);
				line.put("subjectid", subjectid);
				line.put("subjectname", subjectname);
				line.put("organid", organid);
				line.put("pid", pid);
				line.put("status", status);
				line.put("remarks1", remarks1);
				if (map.get(pid) != null) {
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					if (!"-1".equals(line.get("remarks1").toString().trim())) {
						childrens.add(line);
					}
				} else {
					if (!"-1".equals(line.get("remarks1").toString().trim())) {
						result.add(line);
					}
				}
				map.put(organid, line);
			}
		}
		TreeSubjectResponse tsr = new TreeSubjectResponse();
		tsr.setList(result);
		tsr.setNum(num);
		return tsr;
	}

	/**
	 * 反馈审核日志
	 * 
	 * @author pantao
	 */
	public Pager listAuditLog(Session session, Long feedbackid, Pager pager) {
		Pager datas = generalSqlComponent.pageQuery(SqlCode.listTTFBAByFeedbackid,
				new Object[] { session.getCompid(), feedbackid }, pager);
		return datas;
	}

	/**
	 * 根据主任务id查询执行对象 执行规则如下： 1、当执行对象选择下级任务中所有的下级党组织，选择列表中，执行对象显示名称为直属党组织
	 * 2、当执行对象选择下级任务中部分的下级党组织，选择列表中，执行对象显示名称为相关直属党组织
	 * 3、当执行对象选择跨级任务中多个党组织，选择列表中，执行对象显示名称为相关所属党组织
	 * 4、当执行对象选择跨级任务中一个党组织，选择列表中，执行对象显示名称为该党组织的名称
	 * 
	 * @param taskid
	 * @author pantao
	 * @return
	 */
	public String judgmentExecutor(Long compid, Long taskid) {
		TkTaskDeploy info = generalSqlComponent.query(SqlCode.getTTDByid, new Object[] { compid, taskid });
		if (info == null) {
			LehandException.throwException("任务不存在！");
		}
		// 创建人id
		Long userid = info.getUserid();
		// 0：直发任务，1：跨级任务
		int type = info.getRemarks2();
		// 第一步：判断该任务的类型是直发任务还是跨级任务
		// 第二步：通过userid查询出创建人的直属下级所有的id，用逗号隔开，从小到大
		String ids = generalSqlComponent.query(SqlCode.getPOIDByRemarks3, new Object[] { userid });
		// 第三步：通过taskid查询出所有执行对象的userid，用逗号隔开，从小到大
		String userids = generalSqlComponent.query(SqlCode.getTTSUseridByTaskid, new Object[] { info.getId() });
		// 0：直发任务
		if (type == 0) {
			// 两个执行人对象一模一样，说明选择的是其下直属所有人
			if (com.alibaba.druid.util.StringUtils.equals(userids, ids)) {
				return "直属党组织";
			} else {
				return "相关直属党组织";
			}
		} else {
			// 第四步：对于跨级任务首先判断执行对象是单个还是多个，单个就直接显示对象名称，多个就显示相关所属党组织
			List<TkTaskSubject> user = generalSqlComponent.query(SqlCode.getTKSUserid, new Object[] { compid, taskid });
			// 跨级任务多个执行人
			if (user.size() > 1) {
				return "相关所属党组织";
			} else if (user.size() == 1) {
				// 跨级任务单个执行人
				return user.get(0).getSubjectname();
			}

		}
		return "";
	}

	/**
	 * 任务列表页面数据拼接（最新反馈内容，最新反馈时间，任务各个时间点对应的状态）
	 * 
	 * @param taskid
	 * @author pantao
	 * @return
	 */
	public Map<String, Object> getFeedContent(Long compid, Long taskid) {
		Map<String, Object> map = new HashedMap<String, Object>(3);
		List<TaskRemindTimes> lists = generalSqlComponent.query(SqlCode.listRemindtime,
				new Object[] { compid, taskid });
		List<Integer> infos = new ArrayList<Integer>();
		if (lists != null && lists.size() > 0) {
			for (TaskRemindTimes taskRemindTime : lists) {
				int i = 0;
				String remindtime = taskRemindTime.getRemindtime();
				Map<String, Object> param = new HashedMap<String, Object>(3);
				param.put("taskid", taskid);
				param.put("remindtime", remindtime);
				// 查询到的记录是不包括被删除的执行人的反馈记录的
				List<Integer> datas = generalSqlComponent.query(SqlCode.listLogStatusNotDel2, param);
				// 没有查询到数据说明没有反馈记录
				// 查到了数据但是每个人该时间点统计一次反馈的总数量小于执行人的总数也算该任务改时间点逾期未报
				Long count = generalSqlComponent.query(SqlCode.getFeedcountByTime,
						new Object[] { compid, taskid, remindtime });
				int count1 = count.intValue();
				// 任务执行人的数量
				int sum = generalSqlComponent.query(SqlCode.getTTDByiDexsjnum, new Object[] { compid, taskid });
				if (datas == null || datas.size() <= 0 || count1 < sum) {
					long nowtime = DateEnum.now().getTime();
					try {
						long remind = DateEnum.YYYYMMDDHHMMDD.parse(remindtime).getTime();
						// 如果提醒时间点小于当前时间则逾期未反馈
						if (nowtime > remind) {
							infos.add(Constant.colourthere);
						} else {
							// 如果当前时间小于提醒时间点则属于正常未反馈这里暂时定为正常未反馈反馈
							infos.add(Constant.colourfour);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else {
					// 查询到了记录,区分是正常反馈还是逾期反馈
					for (Integer num : datas) {
						// 存在逾期反馈
						if (num == 2) {
							infos.add(Constant.colourtwo);
							break;
						} else {
							i++;
							if (i == datas.size()) {
								infos.add(Constant.colourone);
							}
						}
					}
				}
			}
		}
		map.put("remindtimestatus", infos);
		// 查询最新反馈时间以及最新反馈内容
		TkMyTaskLog log = generalSqlComponent.query(SqlCode.getTMTLMesgAndOpttime,
				new Object[] { compid, compid, taskid });
		if (log != null) {
			map.put("newmesg", log.getMesg());
			map.put("newtime", log.getOpttime());
		} else {
			map.put("newmesg", StringConstant.EMPTY);
			map.put("newtime", StringConstant.EMPTY);
		}
		return map;
	}

	/**
	 * 与我相关的列表页面
	 * 
	 * @author pantao
	 */
	@SuppressWarnings("unchecked")
	public Pager listRelatedToMe(Session session, String tkname, String starttime, String endtime, int status,
			Pager pager) {
		Map<String, Object> param = new HashedMap<String, Object>();
		// 数据库时间都是19位这里如果不带后面的时分秒当起始时间为同一天时就查不出数据
		if (starttime.length() == Constant.STATUS_10) {
			starttime = starttime + " 00:00:00";
		}
		if (endtime.length() == Constant.STATUS_10) {
			endtime = endtime + " 23:59:59";
		}
		param.put("compid", session.getCompid());
		param.put("userid", session.getUserid());
		param.put("starttime", starttime);
		param.put("endtime", endtime);
		param.put("tkname", tkname);
		param.put("status", status);
		Pager datas = generalSqlComponent.pageQuery(SqlCode.listRelatedToMe, param, pager);
		List<Map<String, Object>> lists = (List<Map<String, Object>>) datas.getRows();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (lists != null && lists.size() > 0) {
			for (Map<String, Object> fd : lists) {
				// 查询列表中执行对象一栏数据
				Long taskid = Long.valueOf(fd.get("id").toString());
				String subjectname = judgmentExecutor(session.getCompid(), taskid);
				fd.put("subjectname", subjectname);
				Map<String, Object> map = getFeedContent(session.getCompid(), taskid);
				fd.put("feedcontent", map.get("newmesg"));
//				fd.put("feedtime", map.get("newtime"));
				fd.put("remindtimestatus", map.get("remindtimestatus"));
				if (Long.valueOf(fd.get("status").toString()).intValue() == 30) {
					TkTaskDeploySub tkTaskDeploySub = tkTaskDeploySubDao.get(session.getCompid(),
							Long.valueOf(fd.get("id").toString()));
					if (tkTaskDeploySub != null && tkTaskDeploySub.getReceivenum() > 0 && tkTaskDeploySub
							.getReceivenum() < Long.valueOf(fd.get("exsjnum").toString()).intValue()) {
						fd.put("status", 35);
					}
				}
				list.add(fd);
			}
		}
		datas.setRows(list);
		return datas;
	}

	/**
	 * 与我相关的列表页面头部角标查询
	 * 
	 * @param session
	 * @param tkname
	 * @param starttime
	 * @param endtime
	 * @param status
	 * @author pantao
	 * @return
	 */
	public Integer listRelatedToMeCount(Session session, String tkname, String starttime, String endtime, int status) {
		Map<String, Object> param = new HashedMap<String, Object>();
		param.put("compid", session.getCompid());
		param.put("userid", session.getUserid());
		param.put("status", status);
		Integer num = generalSqlComponent.query(SqlCode.listRelatedToMeCount, param);
		return num;
	}

	/**
	 * 任务巡查列表页面 flag 0:发起的，1接收的
	 * 
	 * @throws ParseException
	 * @throws Exception
	 * @author pantao
	 */
	public void listWorkInspection(Session session, Long userid, String tkname, String starttime, String endtime,
			String classid, int flag, int status, Pager pager) throws Exception {
		String time = StringConstant.EMPTY;
		if (!StringUtils.isEmpty(starttime) && !StringUtils.isEmpty(endtime)) {
			time = starttime.substring(0, 10) + "," + endtime.substring(0, 10);
		}
		if (userid == null) {
			userid = session.getUserid();
		}
		if (flag == 0) {
			session.setAccflag(0);
			// 发起的 等已发任务列表写好后直接调用,根据以上参数自由组装
			TkTaskRequest request = new TkTaskRequest();
			// 任务名称
			request.setTkname(tkname);
			// 任务分类id
			request.setClassid(classid);
			// 查询之间段"time1,time2"
			request.setTime(time);
			// 状态
			request.setStatus(status);
            tkTaskDeployService.page(request, pager, true, userid, session);
		} else {
			// 接受的 等我的任务列表写好后直接调用，根据以上参数自由组装
			Map<String, Object> map = new HashedMap<String, Object>();
			if (status == Constant.STATUS_40) {
				status = 3;
			} else if (status == Constant.STATUS_30) {
				status = 2;
			} else {
				status = 100;
			}
			map.put("status", status);
			map.put("classid", classid);
			map.put("createtime", StringConstant.EMPTY);
			map.put("newtime", time);
			map.put("tkname", tkname);
			map.put("progress", StringConstant.EMPTY);
			tkMyTaskService.page(map, userid, session.getCompid(), pager);
		}
	}

	/**
	 * 任务链查询接口 flag 0表示查询根节点，1表示点击加号展示其下关联的任务
	 * 
	 * @author pantao
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> listTaskChain(Session session, Long id, int flag) {
		Long compid = session.getCompid();
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		// 查询本身对象
		Map<String, Object> maps = generalSqlComponent.query(SqlCode.listTTDByid, new Object[] { compid, id });
		if(maps==null) {
			LehandException.throwException("任务状态已变更请返回");
		}
		List<Map<String, Object>> lists1 = treePwOrganList(compid, Long.valueOf(maps.get("taskid").toString()));
		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
		// 重新排序
		lists.add(maps);
		for (Map<String, Object> map : lists1) {
			lists.add(map);
		}
		if (lists != null && lists.size() > 0) {
			Map<Long, Map<String, Object>> map = new HashMap<Long, Map<String, Object>>(lists.size());
			for (Map<String, Object> map1 : lists) {
				String subjectname = map1.get("username").toString();
				Long subjectid = Long.valueOf(map1.get("userid").toString());
				Long taskid = Long.valueOf(map1.get("taskid").toString());
				Long pid = null;
				String tkname = map1.get("name").toString();
				String remarks6 = String.valueOf(map1.get("remarks6"));
				if (!StringUtils.isEmpty(map1.get("pid"))) {
					pid = Long.valueOf(map1.get("pid").toString());
				}
				Map<String, Object> line = new HashMap<String, Object>(6);
				line.put("subjectid", subjectid);
				line.put("subjectname", subjectname);
				line.put("taskid", taskid);
				line.put("pid", pid);
				line.put("name", tkname);
				line.put("shorthandname", remarks6);
				// 查询看看该任务下面是否还存在转发任务如果存在则显示+，不存在就不显示+
				int num = generalSqlComponent.query(SqlCode.getTTDBySrctaskid, new Object[] { compid, taskid });
				if (num > 0) {
					// 说明存在子节点显示加号
					line.put("son", num);
				} else {
					// 说明不存在子节点不显示加号
					line.put("son", Constant.status);
				}
				if (map.get(pid) != null) {
					Map<String, Object> parent = map.get(pid);
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					childrens.add(line);
				} else {
					result.add(line);
				}
				map.put(taskid, line);
			}
		}
		return result;
	}

	/**
	 * 递归查询任务 下面所有子任务（任务链用）
	 */
	private List<Map<String, Object>> treePwOrganList(Long compid, Long pid) {
		// -- 首先通过id查找第一级子节点，找到后将结果add到结果集中
		List<Map<String, Object>> list = listChildTask(compid, pid);
		// -- 然后将第一级子节点的id变成以逗号隔开的字符串
		String ids = getIdString(compid, pid);
		if (list != null && list.size() > 0) {
			childPwOrgan = list;
			flag = true;
			data(compid, flag, ids);
		}
//    	childPwOrgan.clear();
		return list;
	}

	private void data(Long compid, boolean flag, String ids) {
		if (flag) {
			// -- 其次将上一步得到的字符串进行下一步查询
			List<Map<String, Object>> list2 = listChildTask(compid, ids);
			if (list2 != null && list2.size() > 0) {
				String ids2 = getIdString(compid, ids);
				for (Map<String, Object> organ : list2) {
					childPwOrgan.add(organ);
				}
				data(compid, flag, ids2);
			} else {
				flag = false;
			}
		}
	}

	private List<Map<String, Object>> listChildTask(Long compid, Long pid) {
		return generalSqlComponent.query(SqlCode.getidstring, new Object[] { compid, pid });
	}

	private String getIdString(Long compid, Long pid) {
		return generalSqlComponent.query(SqlCode.listorganstring, new Object[] { compid, pid });
	}

	private List<Map<String, Object>> listChildTask(Long compid, String ids) {
		Map<String, Object> param = param(compid, ids);
		return generalSqlComponent.query(SqlCode.getidstringlong, param);
	}

	private String getIdString(Long compid, String ids) {
		Map<String, Object> param = param(compid, ids);
		return generalSqlComponent.query(SqlCode.listorganlong, param);
	}

	private Map<String, Object> param(Long compid, String ids) {
		Map<String, Object> param = new HashedMap<String, Object>(2);
		param.put("compid", compid);
		param.put("ids", ids);
		return param;
	}

	/**
	 * 根据主任务id和反馈时间点查找对应的反馈时间点要求
	 * 
	 * @param taskid
	 * @param requirement
	 * @return
	 */
	public String getFeedRequirement(Session session, Long taskid, String requirement) {
//		if (taskid == null || StringUtils.isEmpty(requirement)) {
		if (taskid == null ) {
			LehandException.throwException("参数错误！");
		}
		TkTaskFeedTime info = generalSqlComponent.query(SqlCode.getFeedRequirement,
				new Object[] { session.getCompid(), taskid, requirement });
		if (info == null) {
			return "";
		}
		return info.getRequirement();
	}

	/**
	 * 各个列表页面头部各个状态角标查询
	 * 
	 * @param flag      1：已发任务,2我的任务,3任务审核,4：我相关的,5工作巡查
	 * @param           //每个页面最多3个状态 第一个状态传 A 第二个状态传B 第三个状态传C
	 * @param status    专门针对任务巡查模块中的 0:发起的，1接收的 （其他模块传0）
	 * @param subjectid 巡查专属传选中的userid
	 * @return
	 */
	public Map<String, Object> pageNum(Session session, int flag, int status, Long subjectid, Pager pager) {
		// 分别根据flag去调用对应列表的分页查询取出其 totalRows
		Long compid = session.getCompid();
		Long userid = session.getUserid();
		if(StringUtils.isEmpty(subjectid) || "0".equals(subjectid.toString())) {
			subjectid = userid;
		}
		int sjtype = 0;
		String likestr = StringConstant.EMPTY;
		Map<String, Object> map = new HashedMap<String, Object>();
		switch (flag) {
		// 已发任务
		case 1:
			List<Map<String, Object>> list = tkTaskDeployService.listCountByStatus(session,subjectid,sjtype);
			// 办理中
			map.put("A", list.get(0).get("number"));
			// 未签收
			map.put("B", list.get(1).get("number"));
			// 暂停或完成
			map.put("C", list.get(2).get("number"));
			// 审核
			map.put("D", list.get(3).get("number"));
			break;
		// 我的任务
		case 2:
			List<Map<String, Object>> list1 = tkMyTaskService.listCountByStatus(compid, userid, sjtype);
			// 办理中
			map.put("A", list1.get(0).get("number"));
			// 未签收
			map.put("B", list1.get(1).get("number"));
			// 暂停或完成
			map.put("C", list1.get(2).get("number"));
			break;
		// 任务上报审核
		case 3:
			Integer datas1 = pageCount(session, 0, likestr, likestr, likestr);
			// 待审核
			map.put("A", datas1);
			Integer datas2 = pageCount(session, 1, likestr, likestr, likestr);
			// 待审核
			map.put("B", datas2);
			break;
		// 我相关的
		case 4:
			Integer datas3 = listRelatedToMeCount(session, likestr, likestr, likestr, 40);
			// 办理中
			map.put("A", datas3);
			Integer datas4 = listRelatedToMeCount(session, likestr, likestr, likestr, 30);
			// 未签收
			map.put("B", datas4);
			Integer datas5 = listRelatedToMeCount(session, likestr, likestr, likestr, 100);
			// 暂停或完成
			map.put("C", datas5);
			break;
		// 工作巡查
		case 5:
			if (subjectid == null) {
				subjectid = userid;
			}
			// 注意：这里需要和项目经理确认一个问题，就是巡查界面我发起的是不是单纯查询我发起的还是像已发任务列表一样机构账号查询其下所有的
			session.setAccflag(0);
			if (status == 0) {
				List<Map<String, Object>> list2 = tkTaskDeployService.listCountByStatus(session,subjectid, sjtype);
				// 办理中
				map.put("A", list2.get(0).get("number"));
				// 暂停或完成
				map.put("B", list2.get(2).get("number"));
			} else {
				List<Map<String, Object>> list3 = tkMyTaskService.listCountByStatus(compid, subjectid, sjtype);
				// 办理中
				map.put("A", list3.get(0).get("number"));
				// 暂停或完成
				map.put("B", list3.get(2).get("number"));
			}
			break;
		// 任务下发审核
		case 6:
			Pager pager1 = page(session, 0, "", "", "", "task", pager);
			Integer datas6 = pager1.getTotalRows();
			// 待审核
			map.put("A", datas6);
			Pager pager3 = new Pager();
			Pager pager2 = page(session, 1, "", "", "", "task", pager3);
			Integer datas7 = pager2.getTotalRows();
			// 待审核
			map.put("B", datas7);
			break;
		default:
			break;
		}
		return map;
	}

	public List<Map<String, Object>> listRelatedTask(Session session, Long mytaskid, Long logid) {
		Long compid = session.getCompid();
		List<Map<String, Object>> resultMap = new ArrayList<>();
		Assert.notNull(logid, "logid cant be null");
		Assert.notNull(mytaskid, "mytaskid cant be null");
		TkMyTaskLog tkMyTaskLog = tkMyTaskLogDao.get(compid, logid);
		String remarks5 = tkMyTaskLog.getRemarks5();
		HashMap<String, Object> params = new HashMap<>(2);
		params.put("compid", compid);
		params.put("ids", remarks5);
		if (!StringUtils.isEmpty(remarks5)) {
			List<Map<String, Object>> tempMap = generalSqlComponent.query(SqlCode.listRelatedTask, params);
			for (Map<String, Object> next : tempMap) {
				Long feedid = (Long) next.get("feedid");
				int num = generalSqlComponent.query(SqlCode.getTTFBABycount, new Object[] { compid, feedid });
				if (num <= 0) {
					resultMap.add(next);
				}
			}
		}
		return resultMap;
	}

	@SuppressWarnings("unchecked")
	public TreeSubjectResponse listTreeSubjectNew(Session session, Long taskid, String username, String noBack,
			String noDel) {
		Map<String, Object> param = new HashedMap<String, Object>();
		param.put("taskid", taskid);
		param.put("compid", session.getCompid());
		param.put("username", username);
		param.put("noBack", noBack);
		param.put("noDel", noDel);
		List<Map<String, Object>> lists = generalSqlComponent.query(SqlCode.listTreeSubjectNew, param);
		System.out.println("******************************************************"+param);
		int num = lists.size();
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		if (!CollectionUtils.isEmpty(lists)) {
			Map<Object, Map<String, Object>> map = new HashMap<Object, Map<String, Object>>(lists.size());
			for (Map<String, Object> map1 : lists) {
//				String subjectname = map1.get("subjectname").toString();
//				Long subjectid = Long.valueOf(map1.get("subjectid").toString());
				System.out.println("1******************************************************"+map1);
//				Long organid = Long.valueOf(map1.get("organid").toString());
//				Long pid = Long.valueOf(map1.get("porgid").toString());
//				Integer status = Integer.valueOf(map1.get("status").toString());
//				Integer remarks1 = Integer.valueOf(map1.get("remarks1").toString());
				Map<String, Object> line = new HashMap<String, Object>(5);
				line.put("subjectid", map1.get("subjectid"));
				line.put("subjectname", map1.get("subjectname"));
				line.put("organid", map1.get("organid"));
				line.put("pid", map1.get("porgid"));
				line.put("status", map1.get("status"));
				line.put("remarks1", map1.get("remarks1"));
				if (map.get(map1.get("porgid")) != null) {
					Map<String, Object> parent = map.get(map1.get("porgid"));
					// 获取父节点子集中的children信息
					Object object = parent.get("children");
					if (null == object) {
						// 如果为空则再将children添加进去
						object = new ArrayList<Map<String, Object>>();
						parent.put("children", object);
					}
					List<Map<String, Object>> childrens = (List<Map<String, Object>>) object;
					if (!"-1".equals(line.get("remarks1").toString().trim())) {
						childrens.add(line);
					}
				} else {
					if (!"-1".equals(line.get("remarks1").toString().trim())) {
						result.add(line);
					}
				}
				map.put(map1.get("organid"), line);
			}
		}
		TreeSubjectResponse tsr = new TreeSubjectResponse();
		tsr.setList(result);
		tsr.setNum(num);
		return tsr;
	}





	/**待办任务begin****************************************************************************************************************/
	@Resource
	private TodoItemService todoItemService;

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param name 事项名称
	 * @param bizid 该事项对应的业务ID
	 * @param path 资源路径
	 * @param session 当前会话
	 */
	private void addTodoItem(Todo.ItemType itemType, String name, String bizid, String path,Long subjectId, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setBizid(bizid);
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setUserid(session.getUserid());
		itemDTO.setUsername(session.getUsername());
		itemDTO.setOrgid(session.getCurrorganid());
		itemDTO.setName(name);
		itemDTO.setBizid(bizid);
		itemDTO.setTime(new Date());
		itemDTO.setPath(path);
		itemDTO.setState(Todo.STATUS_NO_READ);
		itemDTO.setSubjectid(subjectId);
		todoItemService.createTodoItem(itemDTO);
	}

	/**
	 * 添加待办事项.
	 *
	 * @param itemType 事项类型
	 * @param bizid 该事项对应的业务ID
	 * @param session 当前会话
	 */
	private void removeTodoItem(Todo.ItemType itemType,  String bizid, Session session){
		TodoItemDTO itemDTO = new TodoItemDTO();
		itemDTO.setCategory(itemType.getCode());
		itemDTO.setCompid(session.getCompid());
		itemDTO.setBizid(bizid);
		todoItemService.deleteTodoItem(itemDTO);
	}
	/**待办任务end****************************************************************************************************************/
}
