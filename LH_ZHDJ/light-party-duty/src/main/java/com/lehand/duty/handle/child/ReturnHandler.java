package com.lehand.duty.handle.child;

import com.lehand.base.constant.DateEnum;
import com.lehand.base.constant.StringConstant;
import com.lehand.base.exception.LehandException;
import com.lehand.base.common.Session;
import com.lehand.duty.common.Constant;
import com.lehand.duty.common.SqlCode;
import com.lehand.duty.dto.MyTaskLogRequest;
import com.lehand.duty.handle.BaseHandler;
import com.lehand.duty.handle.TaskProcessData;
import com.lehand.duty.handle.TaskProcessEnum;
import com.lehand.duty.pojo.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
public class ReturnHandler extends BaseHandler {

	@Override
	public void process(TaskProcessEnum processEnum, TaskProcessData data) throws Exception{
		//init
		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
		request.setSynchronizationTime(DateEnum.YYYYMMDDHHMMDD.format());
		Session session = data.getParam(1);
		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
		if(myTask==null) {
			LehandException.throwException("该任务已经被撤销，请知悉！");
		}
		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
		if(deploy==null) {
			LehandException.throwException("该任务已经被撤销，请知悉！");
		}
		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
		data.setDeploySub(deploySub);
		data.setMyTaskLogRequest(request);
		data.setDeploy(deploy);
		data.setMyTask(myTask);
		
		//process
		TkMyTask tkMyTask = data.getMyTask();
		//第一步：更新我的任务表remarks3字段（退回原因）
		if(StringUtils.isEmpty(request.getMessage())) {
			request.setMessage("退回了任务！");
		}
		//退回任务后需要将子任务的remarks6字段更新为"tuihui"这个是为了方便前台在退回任务审核中时不要重复点击退回按钮
		tkMyTaskDao.updateTkMyTaskRemarks(deploy.getCompid(),request.getMessage(),tkMyTask.getId(),"tuihui");
		request.setMessage(processEnum.getName()+"了任务!");
		TkMyTaskLog log = tkMyTaskLogService.insert(deploy.getCompid(),deploy,myTask, request, processEnum, session,Constant.EMPTY,Constant.EMPTY,0);
		data.setMyTaskLog(log);
		//退回需要审核所以这里就不能直接更新状态需要审核完成并通过后才能改变状态（此时需要向审核表中插入一条待审核的记录并标明这是退回任务的审核），而签收就不一样，可以直接改变子任务状态
		//TODO 新增反馈完成后需要查询该执行机构的父节点 ，并在反馈审核表中插入一条数据，状态默认为待审核
		TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
		audit.setCompid(session.getCompid().intValue());
		audit.setTaskid(tkMyTask.getTaskid());
		audit.setMytaskid(tkMyTask.getId());
		audit.setFeedbackid(log.getId());
		audit.setSubjectId(deploy.getUserid());
		audit.setSubtype(Constant.status);//TODO 这里暂时定为0，后续根据需要再修改
		audit.setStatus(Constant.status);
		audit.setRemark(StringConstant.EMPTY);
		audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
		audit.setOpttime(StringConstant.EMPTY);
		audit.setRemarks2(Constant.statusss);//0代表反馈审核，1代表退回任务审核
		audit.setRemarks3(String.valueOf(session.getUserid()));//保存退回发起人id,方便根据任务和人查询对应的审核
		audit.setRemarks4(StringConstant.EMPTY);
		audit.setRemarks5(StringConstant.EMPTY);
		audit.setRemarks6(StringConstant.EMPTY);
		Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
		audit.setId(id);
		
		
		//创建审核人的待办信息 TODO
		insertMyToDoList(3, deploy.getId().toString(),"《"+deploy.getTkname()+"》"+"待您审核。", DateEnum.YYYYMMDDHHMMDD.format(), session.getUserid().toString(), audit.getSubjectId().toString());
		
		
		//remind
		supervise(data,processEnum.getModule(),processEnum.getRemindRule(),request.getMytaskid(),request.getNow());
		remindNoteComponent.register(data.getDeploy().getCompid(),processEnum.getModule(), request.getMytaskid(), processEnum.getRemindRule(), 
				request.getNow(), 
				data.getSessionSubject(), 
				data.getDeploySubject());
	}
	
	
	
	public void insertMyToDoList(int module,String busid,String content,String remind,String optuserid,String userid) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("module", module);
		paramMap.put("busid", busid);
		paramMap.put("content", content);
		paramMap.put("remind", remind);
		paramMap.put("optuserid", optuserid);
		paramMap.put("userid", userid);
		paramMap.put("isread", 0);
		paramMap.put("ishandle", 0);
		generalSqlComponent.getDbComponent().insert("INSERT INTO my_to_do_list (module, busid, content, remind, optuserid, userid, isread, ishandle) VALUES (:module, :busid, :content, :remind, :optuserid, :userid, :isread, :ishandle)", paramMap);
	}

//	@Override
//	protected void init(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
//		Session session = data.getParam(1);
//		TkMyTask myTask = tkMyTaskDao.get(session.getCompid(),request.getMytaskid());
//		TkTaskDeploy deploy = tkTaskDeployDao.get(session.getCompid(),myTask.getTaskid());
//		TkTaskDeploySub deploySub = tkTaskDeploySubDao.get(deploy.getCompid(),deploy.getId());
//		data.setDeploySub(deploySub);
//		data.setMyTaskLogRequest(request);
//		data.setDeploy(deploy);
//		data.setMyTask(myTask);
//	}
	
//	@Override
//	protected void process(TaskProcessData data) throws LehandException {
//		TkMyTask tkMyTask = data.getMyTask();
//		MyTaskLogRequest request = (MyTaskLogRequest) data.getParam(0);
////		TkTaskDeploySub deploySub = data.getDeploySub();
//		TkTaskDeploy deploy = data.getDeploy();
//		//退回需要审核所以这里就不能直接更新状态需要审核完成并通过后才能改变状态（此时需要向审核表中插入一条待审核的记录并标明这是退回任务的审核），而签收就不一样，可以直接改变子任务状态
//		Session session = data.getSession();
//		//TODO 新增反馈完成后需要查询该执行机构的父节点 ，并在反馈审核表中插入一条数据，状态默认为待审核
//		TkTaskFeedBackAudit audit = new TkTaskFeedBackAudit();
//		audit.setCompid(session.getCompid().intValue());
//		audit.setTaskid(tkMyTask.getTaskid());
//		audit.setMytaskid(tkMyTask.getId());
//		audit.setFeedbackid(0L);
//		audit.setSubjectid(deploy.getUserid());
//		audit.setSubtype(Constant.status);//TODO 这里暂时定为0，后续根据需要再修改
//		audit.setStatus(Constant.status);
//		audit.setRemark(StringConstant.EMPTY);
//		audit.setCreatetime(DateEnum.YYYYMMDDHHMMDD.format());
//		audit.setOpttime(StringConstant.EMPTY);
//		audit.setRemarks2(Constant.statusss);//0代表反馈审核，1代表退回任务审核
//		audit.setRemarks3(StringConstant.EMPTY);
//		audit.setRemarks4(StringConstant.EMPTY);
//		audit.setRemarks5(StringConstant.EMPTY);
//		audit.setRemarks6(StringConstant.EMPTY);
//		Long id = generalSqlComponent.insert(SqlCode.addTTFBA, audit);
//		audit.setId(id);
//		//第一步：更新我的任务表remarks3字段（退回原因）
//		if(StringUtils.isEmpty(request.getMessage())) {
//			request.setMessage("退回了任务！");
//		}
//		tkMyTaskDao.updateTkMyTaskRemarks(deploy.getCompid(),request.getMessage(),tkMyTask.getId());
////		if(deploy.getPeriod()==1) {//周期任务
////			//第一步：退回一个就更新附属表中的remarks1字段（周期任务退回数量统计），子任务处于被退回状态
////			deploySub.setRemarks1(deploySub.getRemarks1()+1);
////			//第三步：判断是否所有人所有周期都全部退回了，如果全部退回了就将主表状态设置为0（删除状态）
////			int num1 = deploy.getExsjnum();
////			int num2 = tkTaskTimeNodeDao.getTimeCountByTaskId(deploy.getCompid(),deploy.getId());
////			if(deploySub.getRemarks1()>=num1*num2) {//所有周期下所有人全都退回了
////				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(),0);
////			}
////			//查询该周期任务下是不是有一个周期已经被全部签收
////			int num = tkMyTaskDao.countBySign(deploy.getCompid(),deploy.getId(),deploy.getExsjnum());
////			if(num>0) {
////				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(), Constant.TkTaskDeploy.SIGNED_STATUS);
////			}
////		}else {//普通任务
//			/*//第二步：将主表中的执行人数量减一
//			tkTaskDeployDao.updateExsjnum(deploy.getCompid(),deploy.getId());
//			//第三步：任务执行人数量减1后需要反过来查询下数据库看看任务的执行人数量是否为0 
//			TkTaskDeploy tkTaskDeploy = tkTaskDeployDao.getTask(deploy.getCompid(),deploy.getId());
//			//第四步：如果任务被所有执行人退回了，就将其状态变成删除状态
//			if(tkTaskDeploy.getExsjnum()<=0) {
//				tkTaskDeployDao.updateStatus(deploy.getCompid(),deploy.getId(),1);//现在有了草稿状态了
//			}
//			//第五步：删除该任务下该主体，及将该主体的remarks1设置为-1(目的是是的执行人列表中查询不到该执行人，而单个修改又可以根据remarks1查询到执行人)
//			tkTaskSubjectDao.deleteSubject(deploy.getId(),data.getSession().getUserid());
//			//判断主任务的执行人数目和附属表中签收数量是否相等，相等就改变主任务的状态
//			if(tkTaskDeploy.getExsjnum() != 0) {
//				if(tkTaskDeploy.getExsjnum().equals(deploySub.getReceivenum())) {
//					tkTaskDeployDao.updateTkTaskDeployStauts(deploy.getCompid(),Constant.TkTaskDeploy.SIGNED_STATUS, deploy.getId());
//				}
//			}*/
////		}
//		//第四步：计算平均进度计算公式：子表该任务的进度总和/(执行人数*周期数-退回的数量)
//		// 更新任务发布表进度和最新反馈时间(排除退回的)
////		Double avgprogress = tkMyTaskDao.getAvgProgressByTaskid(tkMyTask.getCompid(),tkMyTask.getTaskid());
////		if(avgprogress != null) {
////			avgprogress = new BigDecimal(avgprogress).setScale(2, RoundingMode.HALF_UP).doubleValue();
////			tkTaskDeployDao.updateProgressAndnewbacktime(deploy.getCompid(),avgprogress, request.getNow(), tkMyTask.getTaskid());
////		}
//	}
	
//	@Override
//	protected void detail(TaskProcessData data) throws LehandException {
//		TkMyTask myTask = data.getMyTask();
//		TkTaskDeploy deploy = data.getDeploy();
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		request.setMessage(this.name+"了任务!");
//		TkMyTaskLog log = tkMyTaskLogDao.insert(deploy.getCompid(),deploy,myTask, request, this, data.getSession(),Constant.EMPTY,Constant.EMPTY,0);
//		data.setMyTaskLog(log);
//	}
	
//	@Override
//	protected void remind(TaskProcessData data) throws LehandException {
//		MyTaskLogRequest request = data.getMyTaskLogRequest();
//		supervise(data,module,remindRule,request.getMytaskid(),request.getNow());
//		remindNoteComponent.register(data.getDeploy().getCompid(),module, request.getMytaskid(), remindRule, 
//				                     request.getNow(), 
//				                     data.getSessionSubject(), 
//				                     data.getDeploySubject());
//	}
//
//	@Override
//	protected void setTaskDeploySub(TaskProcessData data) {
////		TkTaskDeploySub deploySub = data.getDeploySub();
////		deploySub.setReceivenum(deploySub.getReceivenum()+1);
//	}
}
